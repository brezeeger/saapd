# README #

This software is provided freely to help people model and understand photovoltaic devices. It is currently in beta, but provides a different set of tools and options compared to many other PV modelling software presently existing.

Download off the [wiki](https://bitbucket.org/brezeeger/saapd/wiki/). 
Simply extract the .zip file where desired and run the executable. That's it!

### Who do I talk to? ###

Currently maintained by Dan Heinzel. Please report any suggestions/issues [here](https://bitbucket.org/brezeeger/saapd/issues).