#pragma once
/* TODO LIST:
 *
 *	Add all the tabs + selectors/inputs
 *	Add execution of SAAPD in threaded
 *
 *	Add saving + loading of files
 *	Add tooltips
 *	Add progress bar?
 */
#ifndef GUI_H
#define GUI_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/process.h"
#include "wx/progdlg.h"
#include "wx/dir.h"
#include "wx/icon.h"
#endif

#include "guiwindows.h"
#include "resultsPanel.h"


//#include "../kernel_source/model.h"


#include <queue>

#define EXECUTEPANEL 1
class SimulationProgressDialog;
class ModelDescribe;
class devImpurityPanel;
class devLayerTopPanel;
class simulationWindow;

class mainFrame : public wxFrame 
{
public:
	mainFrame(const wxString& title);
	~mainFrame();

	void OnQuit(wxCommandEvent& event);
	
	void RunCurrentSim(wxCommandEvent& event);
	void LoadAndRunSim(wxCommandEvent& event);
	void LoadSimulation(wxCommandEvent& event);

	void SaveSimulation(wxCommandEvent& event);
	void AddBatch(wxCommandEvent& event);
	void InitiateBatch(wxCommandEvent& event);	//when the run batch simulations is clicked event
	void AddBatchFolder(wxCommandEvent& event);
	void AddEventToSubWindow(wxCommandEvent& event);
	void AttemptStartBatch(wxCommandEvent& event);	//event from simulation finishing and checking to start another
	void RemoveBatchSim(wxCommandEvent& event);

	void ShowAbout(wxCommandEvent& event);

	void ChangePanel(wxBookCtrlEvent& event);	//called when panels are changed. Ensures mdesc updates the GUI
	//void SetNotebookPage(int page);
	
	ModelDescribe* getMDesc() { return(mdescGUI); }
	devImpurityPanel* getImpPanel() { return(dimpuritiesWindow); }
	devMaterialPanel* getMatPanel() { return(dmaterialsWindow); }
	

private:


	ModelDescribe* mdescGUI;
	
	
	
	devPropertiesPanel *dpropertiesWindow;
	devLayerTopPanel* layersWindow;
	devMaterialPanel *dmaterialsWindow;
	devImpurityPanel *dimpuritiesWindow;
	executePanel *executeWindow;
	resultsPanel *graphWindow;
//	pointPanel *pointWindow;

	wxMenu *fileMenu;
	wxMenu *helpMenu;
	wxMenuBar *menuBar;
	wxNotebook* topNotebook;

	//TODO: maybe create new inherited obj or move to helper functions for clarity, but then would need new event handler for running code
	simulationWindow* simWindow;
	wxNotebook* deviceWindow;	//holds the properties, materials, impurities, and layers tag.
	wxNotebook* resultsWindow;

	SimulationProgressDialog *progressDiag;
	
	wxStreamToTextRedirector* redirect;
	bool runBatch;

	
	
	
	void LaunchCalc(wxCommandEvent& event);
	void LaunchAbout(wxCommandEvent& event);
	void CreateKernel(wxString filename, int numIters);
	void DestroyKernel(wxCommandEvent& event);
	bool RunBatchSimulation(void);		//the actual act of starting another simulation

	void UpdateDialog(wxCommandEvent& event);

	std::vector<wxString> batchWorkDir;
	std::vector<wxString> filename;

	//wx macro for event handling
	DECLARE_EVENT_TABLE()
};

class SAAPDGUI : public wxApp 
{ 
public:
	virtual bool OnInit();
	int OnExit();
	
private:
};

enum
{
	SAAPD_Quit = wxID_EXIT,
	SAAPD_About = wxID_ABOUT
};

DECLARE_APP(SAAPDGUI)

#endif
