#ifndef SIM_TRANSIENT_H
#define SIM_TRANSIENT_H


#include "wx/wxprec.h"
#include "wx/panel.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;
class TransientSimData;
class simulationWindow;
class TimeDependence;
class Contact;
class SpectraTimes;
class Environment;
class LightSource;
class TransContact;

class TransDataDisplay {
	wxString name_param;
	wxString str_value;	//functional form
	int contactID;	//which contact does this apply to. If negative, doesn't
	int spectraID;	//which spectra does this apply to. If negative, doesn't.
	int value;	//flags, enabled, disable, etc.
	double sTime;
	double eTime;
public:
	TransDataDisplay(int val, double sT, double eT = -1.0, int ctcID = -1, int spcId = -1);
	TransDataDisplay() : contactID(-1), spectraID(-1), value(0), sTime(0.0), eTime(-1), name_param(""), str_value("") { }

	bool Update(TimeDependence* dat, Contact *ct);
	bool Update(const SpectraTimes& dat, LightSource* ltSrc);
	bool Update(double time, bool sv);

	int getContactID() const { return contactID; }
	int getSpectraID() const { return spectraID; }
	int getFlag() const { return value; }
	bool isContact() const { return (contactID > 0); }
	bool isSpectra() const { return (spectraID > 0); }
	bool isNotContactNorSpectra() const{ return (spectraID < 0 && contactID < 0); }
	double getStartTime() const { return sTime; }
	double getEndTime() const { return eTime; }
	wxString getName() const { return name_param; }
	wxString getValue() const { return str_value; }
	void changeEndTime(double time) { eTime = time; }
	
	bool operator <(TransDataDisplay& RHS) { return ((sTime < RHS.sTime) || (sTime == RHS.sTime && eTime < RHS.eTime)); }	//enable my quick sort template to work
};


class simTransientPanel : public wxPanel
{

	wxGrid* gdTimeline;
	PlotWindow* plotTimeline;

	wxButton *btnSave, *btnLoad, *btnRemoveTimeline, *btnAddTimeline;

	wxBoxSizer *szrTimeline;	//vertical on right
	wxFlexGridSizer *szrMidRow;		//things like loading and saving? No, that's all there is!
	wxBoxSizer *szrLeftColumn;
	wxBoxSizer *szrSaveLoad, *szrTimes, *szrLin;

	wxFlexGridSizer *fgszrInitEnd, *fgszrStEndTime, *fgszrCos;

	wxComboBox *cbElementSelection;	//what is currently being added to the transient simulation
	wxComboBox *cbActionSelection;	//basically, what can it do?
	wxComboBox *cbPlotView, *cbTimelineView;	//what to display on the plots, or to show them at all
	wxComboBox *cbInitialConditions;
	wxComboBox *cbMetricGrid, *cbMetricPlot, *cbTimeEntryUnit;

	wxCheckBox *chkSmartSave;
	wxTextCtrl *txtStartTime, *txtEndTime;
	wxTextCtrl *txtLinSlope, *txtLinInitValue;
	wxCheckBox *chkSumLin, *chkSumCos;
	wxTextCtrl *txtAmplitude, *txtFrequency, *txtPhase, *txtOffset;
	
	wxTextCtrl *txtSimEndTime;

	wxRadioBox *rbLinCos;

	wxStaticText *stInitCond, *stEndTimeSim, *stPlot, *stTable, *stStartTime, *stEndTime;
	wxStaticText *stM, *stB, *stA, *stC, *stDelta, *stF;
	wxStaticText *stHelpPlotUnderstand, *stTimeIgnore, *stMaxTStep;
	wxTextCtrl *txtTimeIgnore, *txtMaxTStep;

	wxStaticBoxSizer *szrLinearBox, *szrCosBox;

	
public:
	simTransientPanel(wxWindow* parent, wxWindowID id=wxID_ANY);
	~simTransientPanel();	//going to have to be attaching and detaching things - must be careful with the destructor. It won't auto delete everything!

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI(wxCommandEvent& event);
	Simulation* getSim();
	TransientSimData* getTrans();
private:
	unsigned int firstContactChoice;	//used with cbElement selection.
	unsigned int lastIndexClicked;	//corresponds with tableData index. The display will be ONE higher.
	unsigned int plotTableViewFirstContact;
	unsigned int prevUnitSel;
	Environment *ss_Env;
	wxSizer* szrPrimary;

	std::vector<TransDataDisplay> tableData;	//need an easy way to sort and deal with all that stuff...
	

	void UpdateElementOptions();	//gets the values in cbElementSelection correct, as well as setting the first contact choice

	void UpdateActionOptions(bool setDef=false);
	void UpdateFunctionOptions();

	void AddToTimeline(wxCommandEvent& event);
	void AddTTimeToTimeline();
	void AddContactToTimeline(int ind);
	void AddSpectraToTimeline(int ind);
	void UpdateTable();
	void VerifyInternalVars();

	void getContactOptions(wxArrayString& str);
	int getContactFlag(int sel, bool linear);	//to be used with getContactOptions list
	bool isNumericalContact(int sel);
	bool isBoolContact(int sel);

	void SaveTransient(wxCommandEvent& event);
	void LoadTransient(wxCommandEvent& event);
	void RemoveItem(wxCommandEvent& event);
	void UpdateGridUnits(wxCommandEvent& event);
	void UpdatePlotUnits(wxCommandEvent& event);
	void UpdateProperySelection(wxCommandEvent& event);
	void UpdateMethodSelection(wxCommandEvent& event);
	void UpdateInitialCondition(wxCommandEvent& event);
	void UpdateSmartSave(wxCommandEvent& event);
	void UpdateSimEndTime(wxCommandEvent& event);
	void UpdateStartEndTimes(wxCommandEvent& event);
	void UpdateCosText(wxCommandEvent& event);
	void UpdateCosCheck(wxCommandEvent& event);
	void UpdateLinText(wxCommandEvent& event);
	void UpdateLinCheck(wxCommandEvent& event);
	void UpdateRadioBox(wxCommandEvent& event);
	void UpdateTimeUnits(wxCommandEvent& event);
	void GridClick(wxGridEvent& event);
	void CheckAddButton();
	void UpdatePlot();
	

	bool isInitialized() { return (fgszrCos != nullptr); }
	bool ContactPlot(TransContact* ct, int type, double xScale = 1.0);

	bool PlotSpectra(LightSource *lt, double factor=1.0);
	int PlotContacts(TransContact *ct, double xScale = 1.0);
	


	DECLARE_EVENT_TABLE()	//wx event handling macro
};

std::string GetContactParameterName(int activeValue);
#endif