#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "GUIHelper.h"

#include "SimulationWindow.h"
#include "SimCalculations.h"
#include "SimGeneral.h"
#include "SimSpectra.h"
#include "SimSteadyState.h"
#include  "SimTransient.h"
#include "SimOutputs.h"

#include "../kernel_source/Layers.h"
#include "../kernel_source/load.h"
#include "../kernel_source/model.h"
#include "../kernel_source/BasicTypes.h"
#include "../kernel_source/contacts.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

//These should be kept in sync with their respective wxComboBox

extern double scalarTEN(int index);


simulationWindow::simulationWindow(wxWindow* parent, wxWindowID id)
	: wxNotebook(parent, id), outputPanel(nullptr)
{
	
	calcPanel = new simCalculationPanel(this);
	generalPanel = new simGeneralPanel(this);
	spectraPanel = new simSpectraPanel(this);
	ssPanel = new simSteadyStateBook(this, SS_PANELS);
	TransPanel = new simTransientPanel(this);
	outputPanel = new simOutputPanel(this);

	AddPage(generalPanel, wxT("General"), true, 0);
	AddPage(calcPanel, wxT("Calculations"), false, 1);
	AddPage(outputPanel, wxT("Outputs"), false, 2);
	AddPage(spectraPanel, wxT("Spectra"), false, 3);
	AddPage(ssPanel, wxT("Steady State"), false, 4);
	AddPage(TransPanel, wxT("Transient"), false, 5);
	
}

simulationWindow::~simulationWindow()
{
	delete calcPanel;
	delete generalPanel;
	delete spectraPanel;
	delete ssPanel;
	delete TransPanel;
	delete outputPanel;

	calcPanel = nullptr;
	generalPanel = nullptr;
	spectraPanel = nullptr;
	ssPanel = nullptr;
	TransPanel = nullptr;
	outputPanel = nullptr;
	//	spropertiesSizer->Clear();	this is in the wrapper, so it gets deleted when the wrapper is
	//	delete spropertiesSizer;

	//	spropertiesWrapper->Clear();
	//	delete spropertiesWrapper; //this is assigned to the panel, so it gets auto deleted
}

mainFrame *simulationWindow::getMyClassParent()
{
	return (mainFrame*)GetParent()->GetParent();
}

ModelDescribe* simulationWindow::getMdesc()
{
	return getMyClassParent()->getMDesc();
	//sim props panel -> sim Window -> topNOtebook -> mainFrame
}

Simulation* simulationWindow::getSim()
{
	ModelDescribe* mdesc = getMdesc();
	return mdesc != nullptr ? mdesc->simulation : nullptr;
}

void simulationWindow::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	calcPanel->TransferToMdesc(event);
	generalPanel->TransferToMdesc(event);
	spectraPanel->TransferToMdesc(event);
	ssPanel->TransferToMdescDefine(event);
	TransPanel->TransferToMdesc(event);
	outputPanel->TransferToMdesc(event);
}

void simulationWindow::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	calcPanel->UpdateGUIValues(event);
	generalPanel->UpdateGUIValues(event);
	spectraPanel->UpdateGUIValues(event);
	ssPanel->UpdateGUIValuesDefine(event);
	ssPanel->UpdateGUIValuesSummary(event);
	ssPanel->UpdateGUIValuesDetailed(event);
	TransPanel->UpdateGUIValues(event);
	outputPanel->UpdateGUIValues(event);
}

void simulationWindow::TransferToGUI()
{
	if (!isInitialized())
		return;
	calcPanel->TransferToGUI();
	generalPanel->TransferToGUI();
	spectraPanel->TransferToGUI();
	ssPanel->TransferToGUIDefine();
	ssPanel->UpdateGUIValuesSummary(wxCommandEvent());
	ssPanel->UpdateGUIValuesDetailed(wxCommandEvent());
	TransPanel->TransferToGUI(wxCommandEvent());
	outputPanel->TransferToGUI();
}

void simulationWindow::ChangeSSPanel(wxBookCtrlEvent& event)
{
	if (!isInitialized())
		return;
	if (event.GetEventObject() == ssPanel)
	{
		if (event.GetOldSelection() != event.GetSelection()) {
			if (event.GetSelection() == 0) {	//environments
				ssPanel->TransferToGUIDefine();
			}
			else if (event.GetSelection() == 1) {	//summary
				ssPanel->UpdateGUIValuesSummary(event);
			}
			else if (event.GetSelection() == 2) {
				ssPanel->UpdateGUIValuesDetailed(event);
			}
		}
	}
}

BEGIN_EVENT_TABLE(simulationWindow, wxNotebook)
EVT_NOTEBOOK_PAGE_CHANGED(SS_PANELS, simulationWindow::ChangeSSPanel)
END_EVENT_TABLE()