#ifndef SIM_GENERAL_H
#define SIM_GENERAL_H

#include "wx/wxprec.h"
#include "wx/panel.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;


class simGeneralPanel : public wxPanel
{
	wxTextCtrl *txtMaxIterations, *txtTimeSaveState, *txtTemperature, *txtDivisions, *txtAccuracy;
	wxRadioBox *rbSeedInitialDeviceStart;

	wxStaticText *stMaxIterations, *stTemperature, *stDivisions, *stAccuracy, *stHelp;
	wxCheckBox *chkTimeSaveState;

	wxButton *btnSave, *btnLoad;
	bool isInitialized() { return (stHelp != nullptr); }

public:
	simGeneralPanel(wxWindow* parent);
	~simGeneralPanel();

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI();
	Simulation* getSim();
	void SaveSimulation(wxCommandEvent& event);
	void LoadSimulation(wxCommandEvent& event);
	void UpdateMDescIndiv(wxCommandEvent& event);
	void VerifyInternalVars() { }	//there's nothing to check here!
private:
	wxBoxSizer *szrPrimary;
	wxBoxSizer *szrTopRow;
	wxFlexGridSizer* szrInputs;
	DECLARE_EVENT_TABLE()	//wx event handling macro
};

#endif