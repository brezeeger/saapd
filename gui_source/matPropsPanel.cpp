#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "MatElectricPanel.h"
#include "MatPanel.h"
#include "MatOpticalPanel.h"
#include "MatDefects.h"

#include "../kernel_source/light.h"
#include "../kernel_source/model.h"
#include "../kernel_source/load.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

//These should be kept in sync with their respective wxComboBox

extern double scalarTEN(int index);


devMaterialPanel::devMaterialPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY), currentMat(nullptr)
{
	wxArrayString materialChoices;
	//materialChoices.Add(wxT("Choice 1")); 
	//materialChoices.Add(wxT("Choice 2"));
	materialChooser = new wxComboBox(this, SWITCH_SELECTION, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, materialChoices, wxCB_DROPDOWN | wxCB_READONLY);
	matAdd = new wxButton(this, ADD_MAT, wxT("New Material"));
	matRem = new wxButton(this, REM_MAT, wxT("Remove"));
	matSave = new wxButton(this, wxID_SAVEAS, wxT("Save"));
	matLoad = new wxButton(this, LOAD_MAT, wxT("Load"));
	matDuplicate = new wxButton(this, DUPLICATE_MAT, wxT("Duplicate"));
	matRename = new wxButton(this, RENAME_MAT, wxT("Rename"));
	
	matBar = new wxBoxSizer(wxHORIZONTAL);
	matBar->Add(materialChooser, 1, wxALL, 5);
	matBar->Add(matAdd, 0, wxALL, 5);
	matBar->Add(matRem, 0, wxALL, 5);
	matBar->Add(matSave, 0, wxALL, 5);
	matBar->Add(matLoad, 0, wxALL, 5);
	matBar->Add(matDuplicate, 0, wxALL, 5);
	matBar->Add(matRename, 0, wxALL, 5);

	matOuterSizer = new wxBoxSizer(wxVERTICAL);
	materialsWindow = new wxNotebook(this, wxID_ANY);
	electric = new devMatElectricPanel(materialsWindow);
	opticals = new devMatOpticalsPanel(materialsWindow);
	defect = new devDefectsPanel(materialsWindow);


	materialsWindow->AddPage(electric, wxT("Electrical Properties"), true, 0);
	materialsWindow->AddPage(opticals, wxT("Optical Properties"), false, 1);
	materialsWindow->AddPage(defect, wxT("Defects"), false, 2);

	prevSelection = -1;
	nextID = 0;

	matOuterSizer->Add(matBar, 0, wxEXPAND, 0);
	matOuterSizer->Add(materialsWindow, 1, wxALL | wxEXPAND, 5);

	UpdateGUIValues();
}

devMaterialPanel::~devMaterialPanel()
{
	electric->Clear();
	opticals->Clear();
	defect->Clear();
	materialChooser->Clear();

	delete electric;
	delete opticals;
	delete defect;
	delete materialChooser;
	delete materialsWindow;
	delete matAdd;
	delete matRem;
	delete matSave;
	delete matLoad;
	delete matDuplicate;
	delete matRename;
}

void devMaterialPanel::VerifyInternalVars() {
	ModelDescribe *mdesc = getMdesc();
	if (mdesc == nullptr) {
		currentMat = nullptr;
		return;
	}

	if (mdesc->HasMaterial(currentMat) == false)
		currentMat = nullptr;
}

void devMaterialPanel::UpdateGUIValues()
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc && mdesc->materials.size() > 0) {
		materialChooser->Enable();
		if (materialChooser->GetSelection() < 0 && materialChooser->GetCount() > 0) {
			materialChooser->Select(0);
			currentMat = mdesc->materials[0];
		}

	}
	else
		materialChooser->Disable();

	electric->UpdateGUIValues();
	opticals->UpdateGUIValues(wxCommandEvent());
	defect->UpdateGUIValues();
	if (currentMat == nullptr)
	{
		matRem->Disable();
		matSave->Disable();
		matDuplicate->Disable();
		matRename->Disable();
		materialChooser->SetToolTip(wxT("Select a material to edit/view"));
		materialsWindow->Hide();
	}
	else
	{
		matRem->Enable();
		matSave->Enable();
		matDuplicate->Enable();
		matRename->Enable();
		materialChooser->SetToolTip(currentMat->name);
		materialsWindow->Show();
		matOuterSizer->Layout();
	}
	

}

void devMaterialPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();

	wxArrayString MaterialList;
	ModelDescribe* mdesc = getMdesc();

	materialChooser->Clear();
	bool matGood = false;
	for (unsigned int i = 0; i < mdesc->materials.size(); ++i)	//make sure it stays in line when updating the GUI
	{
		materialChooser->AppendString(mdesc->materials[i]->name);
		if (currentMat == mdesc->materials[i])
		{
			materialChooser->SetSelection(i);
			matGood = true;
		}
	}
	if (!matGood) {
		currentMat = (mdesc->materials.size() > 0) ? mdesc->materials[0] : nullptr;
	}

	electric->TransferToGUI();
	opticals->TransferToGUI();
	defect->TransferToGUI();
	UpdateGUIValues();
}

void devMaterialPanel::TransferToMdesc()
{
	if (!isInitialized())
		return;
	wxCommandEvent emptyEvent;
	electric->TransferToMDesc(emptyEvent);
	opticals->TransferToMdesc(emptyEvent);
	defect->TransferToMdesc(emptyEvent);
}


void devMaterialPanel::SizeRight()
{
	if (!isInitialized())
		return;
	electric->SizeRight();
	opticals->SizeRight();
	defect->SizeRight();
	SetSizer(matOuterSizer);
}

ModelDescribe* devMaterialPanel::getMdesc()
{
	return(getMyClassParent()->getMDesc());
}

mainFrame* devMaterialPanel::getMyClassParent()
{
	return((mainFrame*)GetParent()->GetParent()->GetParent());
	//this -> device -> topNote -> mainFrame
}

void devMaterialPanel::SwitchMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int newSelection = materialChooser->GetSelection();
	std::string matName = materialChooser->GetValue().ToStdString();
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		assert(!"Could not find the model description when switching materials!");

	Material* testMat = nullptr;
	//try to easily find it - the materials vector and this list should always be in sync!
	if (newSelection < (int)mdesc->materials.size())
	{
		testMat = mdesc->materials[newSelection];
		if (testMat->name != matName)
			testMat = nullptr;
	}

	//didn't easily find it
	if (testMat == nullptr)
	{
		for (unsigned int i = 0; i < mdesc->materials.size(); ++i)
		{
			if (mdesc->materials[i]->name == matName)
			{
				testMat = mdesc->materials[i];
				break;
			}
		}
	}

	if (currentMat != testMat) //allow setting nullptr which clears out all the stuff in the forms
	{
		currentMat = testMat;
		UpdateGUIValues();	//make sure everything is properly visible
		TransferToGUI();	//and make sure all the correct values are displayed when the material changes!
	}
	/*
	//save back to the contact
	if (prevSelection >= 0)
	{
		electric->SaveMaterial(prevSelection);
		opticals->SaveMaterial(prevSelection);
	}

	//save current selection #
	prevSelection = materialChooser->GetSelection();

	if (prevSelection >= 0)
	{
		electric->RestoreMaterial(prevSelection);
		opticals->RestoreMaterial(prevSelection);
		electric->EnableMat(true);
		opticals->EnableOpt(true);
		defect->EnableDef(true);
	}
	else
	{
		electric->EnableMat(false);
		opticals->EnableOpt(false);
		defect->EnableDef(false);
	}
	defect->UpdateMatSelection(prevSelection);
	*/
}

void devMaterialPanel::NewMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter the material's name"));
	if (entry.ShowModal() != wxID_OK)
		return;
	
	std::string newName = entry.GetValue().ToStdString();

	Material* mat = new Material();

	mat->name = newName;
	mat->id = -1;
	mdesc->materials.push_back(mat);	//save it to the model!
	mdesc->SetUnknownIDs();
	currentMat = mat;	//global link to all said data
	UpdateGUIValues();
	TransferToGUI();	//makes sure everything is consistent and up to date
	

	/*
	newMaterial.name = entry.GetValue();

	newMaterial.ID = nextID++;
	materials_list.push_back(newMaterial);

	if (prevSelection >= 0)
	{
		electric->SaveMaterial(prevSelection);
	}

	materialChooser->Append(newMaterial.name);
	materialChooser->SetSelection(materialChooser->GetCount() - 1);
	//SwitchMaterial(event);
	prevSelection = materialChooser->GetSelection();
	//Need to do some assignment for ID
	electric->Clear();
	opticals->Clear();
	defect->UpdateMatSelection(prevSelection);
	electric->EnableMat(true);
	opticals->EnableOpt(true);
	defect->EnableDef(true);
	*/
	return;
}

void devMaterialPanel::LoadMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Material files (*.matrl)|*.matrl"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) 
		return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	Material* mat = new Material();
	ModelDescribe* mdesc = getMdesc();
	LoadMaterialChildren(file, mat, *mdesc);	//use the same loading as the kernel. This way the GUI will always be up to date if the kernel loading changes!

	//now make sure it's not loaded already by name and change ID if necessary
	bool duplicatedName = true;
	
	while (duplicatedName)
	{
		duplicatedName = false;
		int highestID = 0;
		for (unsigned int i = 0; i < mdesc->materials.size(); ++i)
		{
			if (mdesc->materials[i]->name == mat->name)
			{
				duplicatedName = true;
				break;
			}
	
		}
		if (duplicatedName)
			mat->name += "_copy";
	}
	
	bool duplicateID = mdesc->isIDUsed(mat->id);
	

	//at this point, everything should be unique and all loaded
	if (duplicateID)
		mat->id = -1;	//mark the id as unknown - don't want interference with what's out there already
	mdesc->materials.push_back(mat);
	if (duplicateID)
		mdesc->SetUnknownIDs();
	currentMat = mat;
	getMdesc()->Validate();
	UpdateGUIValues();
	TransferToGUI();

	return;

	/*
	wxTextFile* iFile = new wxTextFile(openDialog.GetPath());
	iFile->Open();
	std::vector<XMLLine> tags;
	parseFile(iFile, tags);
	if (tags.empty())
	{ //Not an XML file
		wxMessageDialog notXMLDiag(this, wxT("There do not appear to be any XML tags in this file"), wxT("Invalid File"));
		notXMLDiag.ShowModal();
	}
	int toptag = 0;
	if (!tags[0].label.IsSameAs(wxT("Material"), false))
	{ //We don't have a material tag as our first tag
		toptag = tags[0].FindDescendent(wxT("Material"), &tags);
		//find a material tag if it exists
		if (toptag < 0)
		{ //No material tags in the file. Notify the user
			wxMessageDialog noMatDiag(this, wxT("There are no materials in this XML file!"), wxT("No Materials in XML File"));
			noMatDiag.ShowModal();
		}
	}

	int count = 0;

	while (toptag >= 0)
	{
		material mat; //Don't load the ID from file
		mat.name = tags[toptag].ChildValue(wxT("name"), wxT("Material"), &tags);
		mat.description = tags[toptag].ChildValue(wxT("describe"), wxEmptyString, &tags);
		int electricTag = tags[toptag].FindChild(wxT("Electric"), &tags);
		if (electricTag > 0)
		{
			wxString tmpString = tags[electricTag].ChildValue(wxT("type"), wxT("Metal"), &tags);
			mat.type = mat_wxString_to_int(tmpString);
			mat.relativePermittivity = tags[electricTag].ChildValue(wxT("dielectric"), wxEmptyString, &tags);
			mat.intrinsicCarrierDensity = tags[electricTag].ChildValue(wxT("ni"), wxEmptyString, &tags);
			mat.CBEffDensityOfStates = tags[electricTag].ChildValue(wxT("nc"), wxEmptyString, &tags);
			mat.VBEffDensityOfStates = tags[electricTag].ChildValue(wxT("nv"), wxEmptyString, &tags);
			mat.electronMass = tags[electricTag].ChildValue(wxT("efme"), wxEmptyString, &tags);
			mat.holeMass = tags[electricTag].ChildValue(wxT("efmh"), wxEmptyString, &tags);
			mat.electronMobility = tags[electricTag].ChildValue(wxT("mobility_n"), wxEmptyString, &tags);
			mat.holeMobility = tags[electricTag].ChildValue(wxT("mobility_p"), wxEmptyString, &tags);
			mat.electronAffinity = tags[electricTag].ChildValue(wxT("affinity"), wxEmptyString, &tags);
			mat.bandGap = tags[electricTag].ChildValue(wxT("Eg"), wxEmptyString, &tags);

			mat.ID = nextID++;
			materials_list.push_back(mat);
			materialChooser->Append(mat.name);
			count++;
		}
		toptag = tags[toptag].FindLastChild(&tags);
		if (toptag < 0 || toptag >= (signed int)(tags.size()) - 1) break;
		while (!tags[++toptag].label.IsSameAs(wxT("Material"), false))
		{
			if (toptag >= (signed int)(tags.size()) - 1)
			{
				toptag = -1;
				break;
			}
		}
	}
	tags.clear();
	//	delete tags;
	iFile->Close();
	delete iFile;
	if (count > 0)
	{
		materialChooser->SetSelection(materialChooser->GetCount() - 1);
		wxCommandEvent eventBlank;
		SwitchMaterial(eventBlank);
	}
	defect->UpdateMatSelection(prevSelection);
	
	return count;
	*/
}

void devMaterialPanel::RemoveMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;
	if (currentMat == nullptr)
		return;
	
	int index = materialChooser->GetSelection();
	if (index < (int)mdesc->materials.size() && index != wxNOT_FOUND)
	{
		delete mdesc->materials[index];
		mdesc->materials.erase(mdesc->materials.begin() + index);
	}

	currentMat = (unsigned int)index < mdesc->materials.size() ? mdesc->materials[index] : nullptr;
	getMdesc()->Validate();
	UpdateGUIValues();
	TransferToGUI();
		
	/*
	wxString deleted = materialChooser->GetStringSelection();
	materials_list.erase(materials_list.begin() + materialChooser->GetSelection());
	materialChooser->Delete(materialChooser->GetSelection());
	prevSelection = materialChooser->GetSelection();
	if (prevSelection < 0)
	{
		electric->Clear();
		opticals->Clear();
	}
	else
	{
		electric->RestoreMaterial(prevSelection);
		opticals->RestoreMaterial(prevSelection);
	}
	defect->UpdateMatSelection(prevSelection);
	electric->EnableMat(false);
	opticals->EnableOpt(false);
	defect->EnableDef(false);
	*/
	return;
}
void devMaterialPanel::RenameMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (currentMat == nullptr)
		return;

	wxTextEntryDialog getName(this, wxT("Name"), wxT("Rename Material"), currentMat->name);
	if (getName.ShowModal() == wxID_CANCEL)
		return;
	std::string newname = getName.GetValue();
	if (newname != "")
		currentMat->name = newname;

	TransferToGUI();
	return;
}

void devMaterialPanel::duplicateMaterial(wxCommandEvent& evt)
{
	if (!isInitialized())
		return;
	if (currentMat == nullptr)
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;
//	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Material files (*.matrl)|*.matrl"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	wxString newname(currentMat->name);
	newname << "_copy";
	wxTextEntryDialog getName(this, wxT("Enter a new name"), wxT("Duplicate Material"), newname);
	if (getName.ShowModal() == wxID_CANCEL)
		return;

	Material* mat = new Material();
	mat->name = getName.GetValue();// currentMat->name + "_copy";
	mat->describe = currentMat->describe;
	mat->type = currentMat->type;
	mat->epsR = currentMat->epsR;
	mat->muR = currentMat->muR;
	mat->tauThGenRec = currentMat->tauThGenRec;
	mat->ni = currentMat->ni;
	mat->Nc = currentMat->Nc;
	mat->Nv = currentMat->Nv;
	mat->EFMeDens = currentMat->EFMeDens;
	mat->EFMhDens = currentMat->EFMhDens;
	mat->EFMeCond = currentMat->EFMeCond;
	mat->EFMhCond = currentMat->EFMhCond;
	mat->MuN = currentMat->MuN;
	mat->MuP = currentMat->MuP;
	mat->Eg = currentMat->Eg;
	mat->direct = currentMat->direct;
	mat->EcEfi = currentMat->EcEfi;
	mat->Phi = currentMat->Phi;
	mat->SpHeatCap = currentMat->SpHeatCap;
	mat->ThCond = currentMat->ThCond;
	
	mat->urbachVB = currentMat->urbachVB;
	mat->urbachCB = currentMat->urbachCB;
	mat->cfactor = currentMat->cfactor;

	mat->OpticalFluxOut = nullptr;
	mat->OpticalAggregateOut = nullptr;
	mat->id = -1;	//can't have the same ID!! It's gonna need to find a new, good ID
	
	if (currentMat->optics)
	{
		mat->optics = new Optical();
		mat->optics->a = currentMat->optics->a;
		mat->optics->b= currentMat->optics->b;
		mat->optics->n = currentMat->optics->n;
		mat->optics->useAB = currentMat->optics->useAB;
		mat->optics->suppressOpticalOutputs = currentMat->optics->suppressOpticalOutputs;
		mat->optics->disableOpticalEmissions = currentMat->optics->disableOpticalEmissions;
		mat->optics->WavelengthData = currentMat->optics->WavelengthData;
		mat->optics->opticEfficiency = currentMat->optics->opticEfficiency;
	}

	mat->CBCustom = currentMat->CBCustom;
	mat->VBCustom = currentMat->VBCustom;
	
	if (currentMat->defects.size() > 0)
	{
		//damn it. Need to make a copy of all these too!
		mat->defects.reserve(currentMat->defects.size());
		for (unsigned int i = 0; i < currentMat->defects.size(); ++i)
		{
			Impurity* imp = new Impurity();
			*imp = *currentMat->defects[i];	//copy assign
			imp->setID(-1);	//flag to say needs new ID
			mat->defects.push_back(imp);
			mdesc->impurities.push_back(imp);
		}
	}
	
	mdesc->materials.push_back(mat);
	mdesc->SetUnknownIDs();
	currentMat = mat;
	TransferToGUI();

}

void devMaterialPanel::saveMaterial(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (currentMat == nullptr)
//	if (materialChooser->IsListEmpty() || materialChooser->GetSelection() == -1)
	{
		wxMessageDialog diag(this, wxT("There are no materials to save!"), wxT("No Materials"));
		diag.ShowModal();
		return; //empty
	}
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Material files (*.matrl)|*.matrl"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;
//	SwitchMaterial(event);	//basically just making sure it's up to date? Not really necessary...

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		currentMat->SaveToXML(oFile);
		oFile.closeFile();
	}

	/*

	int materialNum = materialChooser->GetSelection();
	wxFile* oFile = new wxFile(saveDialog.GetPath(), wxFile::write);
	if (oFile->IsOpened())
		materialToFile(&(materials_list[materialNum]), oFile);
	oFile->Flush();
	delete oFile;
	*/

	/*wxFileDialog saveDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("DAT files (*.dat)|*.dat"));
	if (saveDialog.ShowModal() == wxID_CANCEL)
	return;
	wxTextFile* oFile = new wxTextFile(saveDialog.GetPath());
	oFile->Open();*/
	//ExtractPoints(oFile);
}

BEGIN_EVENT_TABLE(devMaterialPanel, wxPanel)
EVT_BUTTON(ADD_MAT, devMaterialPanel::NewMaterial)
EVT_BUTTON(REM_MAT, devMaterialPanel::RemoveMaterial)
EVT_BUTTON(LOAD_MAT, devMaterialPanel::LoadMaterial)
EVT_COMBOBOX(SWITCH_SELECTION, devMaterialPanel::SwitchMaterial)
EVT_BUTTON(wxID_SAVEAS, devMaterialPanel::saveMaterial)
EVT_BUTTON(DUPLICATE_MAT, devMaterialPanel::duplicateMaterial)
EVT_BUTTON(RENAME_MAT, devMaterialPanel::RenameMaterial)
//EVT_BUTTON(wxID_OPEN, devMatElectricPanel::LoadMaterial)
END_EVENT_TABLE()
