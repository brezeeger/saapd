
#include "XML.h"

#if (defined __WXMSW__ && defined _DEBUG)
    #include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

//represents number of tabs done. Global b/c laziness
int tabbedin = 0;

XMLItem::XMLItem(wxString label, wxString value, wxFile* output)
{
	for(int i = 0; i < tabbedin; i++) output->Write(wxT("\t"));
	output->Write(wxT("<") + label + wxT(">"));
	output->Write(value);
	output->Write(wxT("</") + label + wxT(">\n"));
}

XMLItem::XMLItem(wxString label, wxString options, wxString value, wxFile* output)
{
	for(int i = 0; i < tabbedin; i++) output->Write(wxT("\t"));
	output->Write(wxT("<") + label + options + wxT(">"));
	output->Write(value);
	output->Write(wxT("</") + label + wxT(">\n"));
}

XMLLabel::XMLLabel(wxString label, wxFile* output)
{
	for(int i = 0; i < tabbedin; i++) output->Write(wxT("\t"));
	output->Write(wxT("<") + label + wxT(" \\>\n"));
}


XMLCat::XMLCat(wxString label, wxFile* output)
{
	for(int i = 0; i < tabbedin; i++) output->Write(wxT("\t"));
	output->Write(wxT("<") + label + wxT(">\n"));
	tabbedin++;
	sLabel = label;
	sOutput = output;
}

XMLCat::XMLCat(wxString first, wxString second, wxFile* output)
{
	for(int i = 0; i < tabbedin; i++) output->Write(wxT("\t"));
	output->Write(wxT("<") + first + second + wxT(">\n"));
	tabbedin++;
	sLabel = first;
	sOutput = output;
}

XMLCat::~XMLCat()
{
	tabbedin--;
	for(int i = 0; i < tabbedin; i++) sOutput->Write(wxT("\t"));
	sOutput->Write(wxT("</") + sLabel + wxT(">\n"));
}

void resettabs()
{
	tabbedin = 0;
}

static wxString distrib_int_to_wxString(int distrib)
{
	switch(distrib)
	{
		case 0:
			return wxT("discrete");
		case 1:
			return wxT("banded");
		case 2:
			return wxT("gaussian");
		case 3:
			return wxT("exponential");
		case 4:
			return wxT("linear");
		case 5:
			return wxT("parabolic");
	}
	return wxEmptyString;
}

static wxString shape_int_to_wxString(int shape)
{
	switch(shape)
	{
		case 0:
			return wxT("point");
		case 1:
			return wxT("line");
		case 2:
			return wxT("rectangle");
		case 3:
			return wxT("quadrilateral");
		case 4:
			return wxT("pentagon");
		case 5:
			return wxT("hexagon");
		case 6:
			return wxT("ellipse");
		case 7:
			return wxT("circle");
		case 8:
			return wxT("square");
		case 9:
		default:
			return wxT("other");
	}
	return wxEmptyString;
}

static wxString mat_int_to_wxString(int type)
{
	switch(type)
	{
		case 0:
			return wxT("Metal");
		case 1:
			return wxT("Semiconductor");
		case 2:
			return wxT("Insulator/Dielectric");
		case 3:
			return wxT("Vacuum");
	}
	return wxEmptyString;
}

char mat_wxString_to_int(wxString& mat)
{
	if(mat.IsSameAs(wxT("Metal"), false))
		return 0;
	if(mat.IsSameAs(wxT("Semiconductor"), false))
		return 1;
	if(mat.IsSameAs(wxT("Insulator/Dielectric"), false))
		return 2;
	if(mat.IsSameAs(wxT("Vacuum"), false))
		return 3;
	return 0; //default case
}

void materialOpticalToFile(material* material, wxFile *oFile)
{
	XMLCat opti(wxT("optical"), oFile);
	switch(material->opt_method)
	{
	case 1:
		for(unsigned int i = 0; i < material->opt_props.size(); i++)
		{
			XMLCat wave(wxT("wavelength"), oFile);
			XMLItem n(wxT("n"), material->opt_props[i].n, oFile);
			XMLItem k(wxT("k"), material->opt_props[i].k, oFile);
			XMLItem lambda(wxT("lambda"), material->opt_props[i].wavelength, oFile);
		}

	break;
	case 2:
		XMLItem(wxT("N"), material->globalN, oFile);
		{
			XMLCat alpha(wxT("alpha"), wxT(" unit=\"m-1\""), oFile);
			for(unsigned int i = 0; i < material->opt_props.size(); i++)
			{
				XMLItem wave1(wxT("wavelength"), wxT(" nm=\"") + material->opt_props[i].wavelength + wxT("\""), material->opt_props[i].alpha, oFile);
			}
		}

	break;
	case 3:
		XMLItem(wxT("A"), material->globalA, oFile);
		XMLItem(wxT("B"), material->globalB, oFile);
	break;
	}
	
}

void materialToFile(material* material, wxFile *oFile)
{
	XMLCat mat(wxT("Material"), wxT(" name=\"") + material->name + wxT("\""), oFile);
	XMLItem name1(wxT("name"), material->name, oFile);
	XMLItem	describe1(wxT("describe"), material->description, oFile);
	XMLItem id1(wxT("id"), wxString::Format(wxT("%i"), material->ID), oFile);
	{
		XMLCat elec(wxT("Electric"), oFile);
		XMLItem type(wxT("type"), mat_int_to_wxString(material->type), oFile);//easy out here, use the strings from the box
		XMLItem dielectric(wxT("dielectric"), material->relativePermittivity, oFile);
		XMLItem ni(wxT("ni"), material->intrinsicCarrierDensity, oFile);
		XMLItem Nc(wxT("Nc"), material->CBEffDensityOfStates, oFile);
		XMLItem Nv(wxT("Nv"), material->VBEffDensityOfStates, oFile);
		XMLItem EFMe(wxT("EFMe"), material->electronMass, oFile);
		XMLItem EFMh(wxT("EFMh"), material->holeMass, oFile);
		XMLItem mobility_n(wxT("mobility_n"), material->electronMobility, oFile);
		XMLItem mobility_p(wxT("mobility_p"), material->holeMobility, oFile);
		XMLItem Affinity(wxT("affinity"), material->electronAffinity, oFile);
		XMLItem Eg(wxT("Eg"), material->bandGap, oFile);
		
	}
	oFile->Write(wxT("\n"));

	materialOpticalToFile(material, oFile);

	for(unsigned int i = 0; i < material->defects.size(); i++)
	{
		oFile->Write(wxT("\n"));
		defectToFile(&(material->defects[i]), oFile);
	}


}

void contactToFile(contact* contact, wxFile* oFile)
{
	XMLCat contactCat(wxT("contact"), oFile);
	XMLItem id(wxT("contact->ID"), wxString::Format(wxT("%d"), contact->ID), oFile);
	for(unsigned int i = 0; i < contact->voltages.size(); i++)
	{
		if(!contact->voltages[i].cos)
		{
			XMLCat fixed(wxT("fixed"), oFile);
			XMLItem voltage(wxT("voltage"),  wxString::Format(wxT("%f"), contact->voltages[i].amp), oFile);
			XMLItem starttime(wxT("starttime"),  wxString::Format(wxT("%f"), contact->voltages[i].time), oFile);
		}
		else
		{
			XMLCat cos(wxT("cos"), oFile);
			XMLItem voltage(wxT("voltage"), wxString::Format(wxT("%f"), contact->voltages[i].amp), oFile);
			XMLItem frequency(wxT("frequency"), wxString::Format(wxT("%f"), contact->voltages[i].freq), oFile);
			XMLItem delta(wxT("delta"), wxString::Format(wxT("%f"), contact->voltages[i].phase_offset), oFile);
			XMLItem dc(wxT("DC"), wxString::Format(wxT("%f"), contact->voltages[i].voltage_offset), oFile);
			XMLItem starttime(wxT("starttime"),  wxString::Format(wxT("%f"), contact->voltages[i].time), oFile);
		}
	}
}


void impurityToFile(impurity* impurity, wxFile *oFile)
{
	XMLCat impurityDef(wxT("Impurity"), oFile);
	XMLLabel accdon(impurity->accdon ? wxT("Acceptor") : wxT("Donor"), oFile);
	XMLItem type(wxT("type"), impurity->reference ? wxT("0") : wxT("1"), oFile);
	XMLItem distribution(wxT("distribution"), wxString::Format(wxT("%i"), impurity->distribution), oFile);
	XMLItem variance(wxT("degeneracy"), impurity->degeneracy, oFile);
	XMLItem density(wxT("density"), impurity->density, oFile);
	XMLItem energy(wxT("energy"), impurity->energy, oFile);
	XMLItem width(wxT("width"), impurity->width, oFile);
	XMLItem depth(wxT("depth"), impurity->depth ? wxT("deep") : wxT("shallow"), oFile);
	XMLItem charge(wxT("charge"), impurity->charge, oFile);
	XMLItem sigN(wxT("sigN"), impurity->sigN, oFile);
	XMLItem sigP(wxT("sigP"), impurity->sigP, oFile);
	XMLItem id(wxT("id"), wxString::Format(wxT("%i"), impurity->ID), oFile);
	XMLItem matl(wxT("matl"), impurity->material, oFile);
	
}

void defectToFile(defect* defect, wxFile *oFile)
{
	if(defect->ID > 0)	//g++ does not like that wxString is not of type const wxChar*, and doesn't select the appropriate constructor
	{
		XMLCat defectNum(wxT("defect"), wxT(" number=\"") + wxString::Format(wxT("%i"), defect->ID) + wxT("\""), oFile);
		{
			XMLCat donorlike(defect->accdon ? wxT("donorlike") : wxT("acceptorlike"), oFile);
			XMLItem type(wxT("Type"), defect->accdon ? wxT("1") : wxT("0"), oFile);
			XMLItem distribution(wxT("Distribution"), wxString::Format(wxT("%i"), defect->distribution), oFile);
			XMLItem density(wxT("density"), defect->density, oFile);
			XMLItem energy(wxT("energy"), defect->energy, oFile);
			XMLItem width(wxT("width"), defect->width, oFile);
			XMLItem depth(wxT("depth"), defect->depth ? wxT("DEEP") : wxT("SHALLOW"), oFile);
			XMLItem sigN(wxT("sigN"), defect->sigN, oFile);
			XMLItem sigP(wxT("sigP"), defect->sigP, oFile);
			XMLItem id(wxT("id"), wxString::Format(wxT("%i"), defect->ID), oFile);
		}
	}
	else
	{
		XMLCat defectNum(wxT("defect"), wxEmptyString, oFile);
		{
			XMLCat donorlike(defect->accdon ? wxT("donorlike") : wxT("acceptorlike"), oFile);
			XMLItem type(wxT("Type"), defect->accdon ? wxT("1") : wxT("0"), oFile);
			XMLItem distribution(wxT("Distribution"), wxString::Format(wxT("%i"), defect->distribution), oFile);
			XMLItem density(wxT("density"), defect->density, oFile);
			XMLItem energy(wxT("energy"), defect->energy, oFile);
			XMLItem width(wxT("width"), defect->width, oFile);
			XMLItem depth(wxT("depth"), defect->depth ? wxT("DEEP") : wxT("SHALLOW"), oFile);
			XMLItem sigN(wxT("sigN"), defect->sigN, oFile);
			XMLItem sigP(wxT("sigP"), defect->sigP, oFile);
			XMLItem id(wxT("id"), wxString::Format(wxT("%i"), defect->ID), oFile);
		}
	}
}


void lDescMatToFile(layer_materials* lay_mat, wxFile *oFile)
{
	XMLCat lDescMat(wxT("lDescMat"), oFile);
	XMLItem flag(wxT("flag"), shape_int_to_wxString(lay_mat->flag), oFile);
	{
		XMLCat PtRange(wxT("PtRange"), oFile);
		{
			XMLCat startpos(wxT("startpos"), oFile);
			{
				XMLCat xSpace(wxT("XSpace"), oFile);
				XMLItem x(wxT("x"), lay_mat->xStartPos, oFile);
			}
		}
		{
			XMLCat stoppos(wxT("stoppos"), oFile);
			{
				XMLCat xSpace(wxT("XSpace"), oFile);
				XMLItem x(wxT("x"), lay_mat->xStopPos, oFile);
			}
			
		}
	}
	XMLItem id(wxT("id"), wxString::Format(wxT("%i"), lay_mat->ID), oFile);
}

void lDescImpToFile(layer_impurities* lay_imp, wxFile *oFile)
{
	XMLCat lDescImp(wxT("LDescImp"), oFile);
	XMLItem flag(wxT("flag"), shape_int_to_wxString(lay_imp->flag), oFile);
	{
		XMLCat ptrange(wxT("PtRange"), oFile);
		{
			XMLCat startpos(wxT("startpos"), oFile);
			{
				XMLCat xspace(wxT("XSpace"), oFile);
				XMLItem x(wxT("x"), lay_imp->xStartPos, oFile);
			}
		}
		{
			XMLCat stoppos(wxT("stoppos"), oFile);
			{
				XMLCat xspace(wxT("XSpace"), oFile);
				XMLItem x(wxT("x"), lay_imp->xStopPos, oFile);
			}
		}
		XMLItem id(wxT("id"), wxString::Format(wxT("%i"), lay_imp->ID), oFile);
	}
}

void vertexToFile(point& vertex, wxFile *oFile)
{
	XMLCat vertexLabel(wxT("vertex"), oFile);
	{
		XMLCat xSpace(wxT("XSpace"), oFile);
		XMLItem x(wxT("x"), vertex.xPos, oFile);
	}
}

void layerToFile(layer* layer, wxFile *oFile)
{
	XMLCat layerName(wxT("Layer"), wxT(" name=\"")+layer->name+wxT("\""), oFile);
	XMLItem id(wxT("id"), wxString::Format(wxT("%i"), layer->ID), oFile);
	XMLItem contactid(wxT("contactid"), wxString::Format(wxT("%i"), layer->contactID), oFile);
	XMLItem AffectMesh(wxT("AffectMesh"), layer->affectMesh ? wxT("true") : wxT("false"), oFile);
	{
		XMLCat shape(wxT("shape"), oFile); //NEED TO ADD STUFF FOR LAYER SHAPE
		XMLItem type(wxT("type"), shape_int_to_wxString(layer->shape), oFile);
		for(unsigned int i = 0; i < layer->vertices.size(); i++)
		{
			vertexToFile(layer->vertices[i], oFile);
		}
	}
	XMLLabel inclusive(layer->exclusive ? wxT("Exclusive") : wxT("Inclusive"), oFile);
	XMLItem edge(wxT("edge"), layer->edge, oFile);
	XMLItem center(wxT("center"), layer->center, oFile);
	XMLItem phi(wxT("phi"), layer->phi, oFile);
	XMLItem theta(wxT("theta"), layer->theta, oFile);
	for(unsigned int i = 0; i < layer->inc_mats.size(); i++)
		lDescMatToFile(&(layer->inc_mats[i]), oFile);
	oFile->Write(wxT("\n"));
	for(unsigned int i = 0; i < layer->inc_imps.size(); i++)
		lDescImpToFile(&(layer->inc_imps[i]),oFile);
	
}

bool checkLine(wxString &buffer, wxTextFile* iFile)
{
	buffer = buffer.Strip(wxString::both);
	while(buffer.IsEmpty())
	{
		if(iFile->Eof()) return false;
		buffer = buffer.Append(iFile->GetNextLine());
		buffer = buffer.Strip(wxString::both);
	}
	return true;
}

int parseFile(wxTextFile* iFile, std::vector<XMLLine>& upTree)
{
//	std::vector<XMLLine>* upTree = new std::vector<XMLLine>;
	wxString inBuffer = iFile->GetFirstLine();
	wxString bufferBackup;
	wxString label;
	wxString options;
	bool closedTag = false;
	int parent = -1;
	while(true)
	{
		if(!checkLine(inBuffer, iFile)) break;
		inBuffer = inBuffer.AfterFirst('<');
		label = inBuffer.BeforeFirst('>', &bufferBackup);
		label = label.BeforeFirst('=', &options);
		inBuffer = bufferBackup.Strip(wxString::both);
		if(!checkLine(inBuffer, iFile)) break;
		
		if(label[0] == '/')
		{
			parent = upTree[parent].parent;
			do{
				if(!checkLine(inBuffer, iFile)) return (0);
				inBuffer = inBuffer.AfterFirst('<'); //clear to next tag
			}while(inBuffer.IsEmpty());
			inBuffer = wxT("<") + inBuffer;
			label.Empty();
			options.Empty();
			continue;
		}
		if(label.Last() == '/')
		{
			closedTag = true;
			label.RemoveLast(1);
			label = label.Strip(wxString::both);
		}

		XMLLine newLine;
		if(!options.IsEmpty())
		{
			label = label.BeforeLast(' ', &bufferBackup);
			options = bufferBackup + wxT("=") + options;
			options.Replace(wxT("\""), wxEmptyString, true);
			newLine.option = options;
		}
		newLine.label = label;
		if(inBuffer[0] != '<')
		{
			while(inBuffer.Find('<') == wxNOT_FOUND)
			{
				if(iFile->Eof()) return (0);
				inBuffer = inBuffer.Append(iFile->GetNextLine());
				inBuffer = inBuffer.Strip(wxString::both);
			}
			newLine.value = inBuffer.BeforeFirst('<', &bufferBackup);
			inBuffer = bufferBackup.Prepend(wxT("<"));
		}
		if(!checkLine(inBuffer, iFile)) break;

		upTree.push_back(newLine);
		upTree.back().AddToParent(parent, upTree.size()-1, &upTree);
		if(!closedTag)
			parent = upTree.size()-1;
		closedTag = false;
	}
	return (0);
}

XMLLine::XMLLine()
{
	parent = sibling = child = -1;
}

//returns -1 if no child found
int XMLLine::FindChild(wxString name, std::vector<XMLLine> *tree)
{
	int cur = child;
	while(cur >= 0)
	{
		if(name.IsSameAs((*tree)[cur].label, false)) return cur;
		cur = (*tree)[cur].sibling;
	}
	return -1;
}

//returns -1 if no child found
int XMLLine::FindDescendent(wxString name, std::vector<XMLLine> *tree)
{
	int cur = child;
	int childRes;
	while(cur >= 0)
	{
		if(name.IsSameAs((*tree)[cur].label, false)) return cur;
		childRes = (*tree)[cur].FindDescendent(name, tree);
		if(childRes >= 0) return childRes;
		cur = (*tree)[cur].sibling;
	}
	return -1;
}

int XMLLine::FindLastChild(std::vector<XMLLine> *tree)
{
	if(child < 0) return -1;
	int cur = child;
	while((*tree)[cur].sibling >= 0) cur = (*tree)[cur].sibling;
	return cur;
}

void XMLLine::AddToParent(int parentIndex, int self, std::vector<XMLLine> *tree)
{
	if(parentIndex < 0 || self < 0) return;
	if((*tree)[parentIndex].child < 0)
	{
		(*tree)[parentIndex].child = self;
	}
	else
	{
		int cur = (*tree)[parentIndex].child;
		while((*tree)[cur].sibling >= 0) cur = (*tree)[cur].sibling;
		(*tree)[cur].sibling = self;
	}
	parent = parentIndex;
}

wxString XMLLine::ChildValue(wxString label, wxString defaultValue, std::vector<XMLLine> *tree)
{
	int childIndex = FindChild(label, tree);
	if(childIndex >= 0) return (*tree)[childIndex].value;
	return defaultValue;
}