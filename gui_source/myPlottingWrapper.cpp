#include "myPlottingWrapper.h"
#include "mathplot.h"
#include "guiwindows.h"


PlotWindow::PlotWindow(wxWindow* parent, wxWindowID id)
{
	plot = new mpWindow(parent, id);
	plot->Hide();

	xAxis = new mpScaleX("X", mpALIGN_BOTTOM, true, mpX_NORMAL);
	yAxis = new mpScaleY("Y", mpALIGN_LEFT, true);
	title = new mpText("Title", 50, 2);
	legend = new mpInfoLegend();

	plot->AddLayer(xAxis);
	plot->AddLayer(yAxis);
	plot->AddLayer(title);
	plot->AddLayer(legend);
}

PlotWindow::~PlotWindow()
{
	delete plot;
	//the above deletes xAxis, yAxis, title, and legend as well!
	for (unsigned int i = 0; i < LData.size(); ++i)
		delete LData[i];
	LData.clear();
	ActivePlots.clear();
	plot = nullptr;
	xAxis = nullptr;
	yAxis = nullptr;
	title = nullptr;
	legend = nullptr;
	plot = nullptr;
}


bool PlotWindow::ScaleAll(double sc)
{
	for (unsigned int i = 0; i < LData.size(); ++i)
		LData[i]->SetScale(sc);
	
	return(true);
}

bool PlotWindow::ScaleData(unsigned int index, double scl)
{
	if (index < LData.size()) {
		LData[index]->SetScale(scl);
		return(true);
	}
	return(false);
}

bool PlotWindow::ScaleData(const wxString& header, double scl)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == header) { 
			LData[i]->SetScale(scl);
			return(true);
		}
	}
	return(false);
}

bool PlotWindow::OffsetAll(double offset)
{
	for (unsigned int i = 0; i < LData.size(); ++i)
		LData[i]->SetOffset(offset);
	return(true);
}
bool PlotWindow::OffsetData(unsigned int index, double offset)
{
	if (index < LData.size()) {
		LData[index]->SetOffset(offset);
		return(true);
	}
	return(false);
}
bool PlotWindow::OffsetData(const wxString& header, double offset)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == header) {
			LData[i]->SetOffset(offset);
			return(true);
		}
	}
	return(false);
}

void PlotWindow::UpdateLegend()
{
	UpdateLegend(wxCommandEvent());	//just create a dummy event
}

void PlotWindow::UpdateLegend(wxEvent& event)
{
	legend->UpdateInfo(*plot, event);
}

bool PlotWindow::GetData(unsigned int indexL, unsigned int indexD, double& dat)
{
	if (indexL < LData.size()) {
		return LData[indexL]->GetData(dat, indexD);
	}
	return(false);
}

LineData* PlotWindow::GetData(const wxString& header)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == header)
			return LData[i];
	}
	return nullptr;
}

bool PlotWindow::RemoveDataVector(const wxString& header)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == header) {
			delete LData[i];
			LData.erase(LData.begin() + i);
			return(true);
		}
	}
	return false;
}

void PlotWindow::RemoveAllData() {
	for (unsigned int i = 0; i < LData.size(); ++i)
		delete LData[i];
	LData.clear();

}

bool PlotWindow::RemoveCommonIndex(unsigned int index)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (index >= LData[i]->size()) {
			return(false);
		}
	}
	for (unsigned int i = 0; i < LData.size(); ++i)	
		LData[i]->RemoveData(index);
	return true;
}

bool PlotWindow::RemoveDataVector(unsigned int index)
{
	if (index < LData.size())
	{
		delete LData[index];
		LData.erase(LData.begin() + index);
		return(true);
	}
	return(false);
}
bool PlotWindow::SetXData(unsigned int index) {
	activeX = index;
	if (index < LData.size())
		LData[index]->SetAsX();
	return (activeX < LData.size());
}
bool PlotWindow::SetXData(const wxString& head)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == head) {
			activeX = i;
			LData[i]->SetAsX();
			return(true);
		}
	}
	return false;
}

bool PlotWindow::SetXDataActive(const wxString& head)
{
	bool success = false;
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == head) {
			activeX = i;
			LData[i]->SetAsActiveX();
			success = true;
		}
		else
			LData[i]->ClearActiveX();
	}
	if (!success && activeX < LData.size())	//restore if not found
	{
		LData[activeX]->SetAsActiveX();
	}
	return success;
}

bool PlotWindow::SetXDataActive(unsigned int index)
{
	bool success = false;
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (i == index) {
			activeX = i;
			LData[i]->SetAsActiveX();
			success = true;
		}
		else
			LData[i]->ClearActiveX();
	}
	if (!success && activeX < LData.size())
	{
		LData[activeX]->SetAsActiveX();
	}
	return success;
}

bool PlotWindow::SetYData(unsigned int index)
{
	if (index < LData.size()) {
		LData[index]->SetAsY();
		return(true);
	}
	return(false);
}

bool PlotWindow::SetYData(const wxString& head)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == head) {
			LData[i]->SetAsY();
			return(true);
		}
	}
	return false;
}

bool PlotWindow::SetYDataActive(unsigned int index)
{
	if (index < LData.size()) {
		LData[index]->SetAsActiveY();
		return(true);
	}
	return(false);
}

bool PlotWindow::SetYDataActive(const wxString& head)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == head) {
			LData[i]->SetAsActiveY();
			return(true);
		}
	}
	return false;
}

unsigned int PlotWindow::GetDataIndex(const wxString& head)
{
	for (unsigned int i = 0; i < LData.size(); ++i)	{
		if (LData[i]->GetHeader() == head) {
			return i;
		}
	}
	return -1;
}

int PlotWindow::UpdateActive()
{
	ActivePlots.clear();
	activeX = -1;

	for (unsigned int i = 0; i < LData.size(); ++i)
	{
		if (activeX == -1)
		{
			if (LData[i]->IsXActive())
				activeX = i;
		}

		if (LData[i]->IsYActive())
			ActivePlots.push_back(i);
	}
	if (activeX == -1) {	//workaround hack now that I allowmultiple x values to be plotted. similar to what I did for the results window custom plots
		for (unsigned int i = 0; i < LData.size(); ++i)
		{
			if (LData[i]->IsX()) {
				activeX = i;
				break;
			}
		}
	}
	return 0;
}

int PlotWindow::UpdatePlot(bool FitToData)
{
	
	//CountAll includes text/axis/legends. Count Layers... does not!
	UpdateActive();
	unsigned int useX = activeX;

	if (activeX < LData.size())	//we have an X coordinate to adjust with
	{
		int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();
		//first make sure all the ActivePLot values are good
		for (unsigned int i = 0; i < ActivePlots.size(); ++i)
		{
			if (ActivePlots[i] >= LData.size())
			{
				ActivePlots.erase(ActivePlots.begin() + i);
				i--;	//maintain the index as everything shifts down
			}
		}
		//first make sure the number of layers in the plot is correct!
		while (plot->CountLayers() > ActivePlots.size())
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

		while (plot->CountLayers() < ActivePlots.size())
		{
			plot->AddLayer(new mpFXYVector(), false);
			plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
		}

		//now load all that data in!
		for (unsigned int i = 0; i < ActivePlots.size(); i++)
		{
			useX = activeX;
			for (unsigned int j = ActivePlots[i] - 1; j < LData.size(); --j) {	//find the nearest added X value before this!
				if (LData[j]->IsXActive()) {
					useX = j;
					break;
				}
			}
			unsigned int col = ActivePlots[i];

			if (LData[col]->IsIgnoreZeros() || LData[col]->GetScale() != 1.0 || LData[col]->GetOffset() != 0.0)
			{
				std::vector<double> newXVals;
				std::vector<double> newYVals;
				double tmp;
				double scl = LData[col]->GetScale();
				double offset = LData[col]->GetOffset();
				bool ignore = LData[col]->IsIgnoreZeros();
				for (unsigned int j = 0; j < LData[col]->size() && j < LData[useX]->size(); ++j)
				{
					tmp = (LData[col]->GetData(j)) * scl + offset;
					if (tmp != offset || !ignore)
					{
						newYVals.push_back(tmp);
						newXVals.push_back(LData[useX]->GetData(j));
					}
				}
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(newXVals, newYVals);
				newXVals.clear();
				newYVals.clear();
			}
			else
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(LData[useX]->GetData(), LData[col]->GetData());
			plot->GetLayer(fGraphLayer + i)->SetPen(LData[col]->GetPen());
			plot->GetLayer(fGraphLayer + i)->SetDrawOutsideMargins(false);
			plot->GetLayer(fGraphLayer + i)->SetName(LData[col]->GetLegend());
			plot->GetLayer(fGraphLayer + i)->ShowName(showLabel);
			plot->GetLayer(fGraphLayer + i)->SetContinuity(LData[col]->IsDataConnected());
			plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
			((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(LData[col]->GetAlignment());
			
			

			
		}
		UpdateLegend();
		legend->SetVisible(showLegend);
		if (FitToData)
			plot->Fit(0.05);
	}
	else //no X coordinate - delete all the plot layers (not the axes)
	{
		while (plot->CountLayers() > 0)
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);
	}
	plot->UpdateAll();	//need to load the x/y_min/max values internally
	return 0;
}

unsigned int PlotWindow::getNumYData() {
	unsigned int ctr = 0;
	for (unsigned int i = 0; i < LData.size(); ++i) {
		if (LData[i]->IsY() || LData[i]->IsYActive())
			ctr++;
	}
	return ctr;
}

LineData* PlotWindow::getYDataIndex(unsigned int index) {
	unsigned int ctr = 0;
	for (unsigned int i = 0; i < LData.size(); ++i) {
		if (LData[i]->IsY() || LData[i]->IsYActive()) {
			if (ctr == index)
				return LData[i];
			ctr++;
		}
	}
	return nullptr;
}

unsigned int PlotWindow::AddUnique(unsigned int index, double dat)
{
	if (index >= LData.size())
		return(-1);	//indicate failure...
	
	std::vector<double>& data = LData[index]->GetData();
	unsigned int ret = data.size();
	for (unsigned int i = 0; i < ret; ++i)
	{
		if (data[i] == dat)
			return(i);
	}
	data.push_back(dat);
	return(ret);
}


/////////////////////////////////////////////


SavePlotImage::SavePlotImage(wxWindow* parent) : wxDialog(parent, wxID_ANY, wxT("Save Plot"))
{
	str_height = wxT("768");
	str_width = wxT("1024");	//default values
	width = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(60, 25), 0, wxTextValidator(wxFILTER_NUMERIC, &str_width));
	height = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(60, 25), 0, wxTextValidator(wxFILTER_NUMERIC, &str_height));
	wLabel = new wxStaticText(this, wxID_ANY, wxT("Width"));
	hLabel = new wxStaticText(this, wxID_ANY, wxT("Height"));
	filename = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, 25), wxTE_READONLY);// , wxTextValidator(wxFILTER_EMPTY, &str_fname));
	btn_Destination = new wxButton(this, SAVE_PLOT_BTN_NAME, "Set File Name");
	btn_Save = new wxButton(this, wxID_OK, wxT("Save"), wxDefaultPosition);
	btn_Cancel = new wxButton(this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition);


	sizerMain = new wxFlexGridSizer(4, 2, 5, 5);

	sizerMain->Add(btn_Destination, 0, wxALL | wxALIGN_RIGHT, 2);
	sizerMain->Add(filename, 0, wxALL | wxALIGN_LEFT, 2);
	sizerMain->Add(wLabel, 0, wxALL | wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 2);
	sizerMain->Add(width, 0, wxALL | wxALIGN_LEFT, 2);
	sizerMain->Add(hLabel, 0, wxALL | wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 2);
	sizerMain->Add(height, 0, wxALL | wxALIGN_LEFT, 2);
	sizerMain->Add(btn_Save, 0, wxALL | wxALIGN_CENTER, 2);
	sizerMain->Add(btn_Cancel, 0, wxALL | wxALIGN_CENTER, 2);


	SetSizerAndFit(sizerMain);

	return;
}

void SavePlotImage::GetFileLocation(wxCommandEvent& event)
{
	/*
	if (diag.str_fname.EndsWith(wxT(".bmp")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_BMP, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".jpg")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_JPEG, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".png")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PNG, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".pcx")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PCX, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".pnm")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PNM, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".tif")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_TIFF, wxSize(diag.wd, diag.ht), false);
	else if (diag.str_fname.EndsWith(wxT(".xpm")))
	plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_XPM, wxSize(diag.wd, diag.ht), false);
	*/
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString,
		wxT("Portable Network Graphics (*.png)|*.png|Bitmap files (*.bmp)|*.bmp|Joint Photographic Experts Group (*.jpg)|*.jpg|PiCture eXchange (*.pcx)|*.pcx|Portable Any Map (*.pnm)|*.pnm|Tag Image File Format (*.tif)|*.tif|X PixMap (*.xpm)|*.xpm"),
		wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	filename->ChangeValue(saveDialog.GetPath());
	filename->SetToolTip(saveDialog.GetPath());

	return;
}

bool SavePlotImage::TransferDataFromWindow()
{

	if (filename->GetValue().IsSameAs(wxEmptyString))
	{
		wxMessageDialog diag(this, wxT("Please choose a file name and destination"), wxT("Lack of Input"));
		diag.ShowModal();
		return false;
	}

	if (!width->GetValue().ToULong(&wd))
	{
		wxMessageDialog diag(this, wxT("The width is not an integer"), wxT("Invalid Input"));
		diag.ShowModal();
		return false;
	}
	if (!height->GetValue().ToULong(&ht))
	{
		wxMessageDialog diag(this, wxT("Height is not an integer"), wxT("Invalid Input"));
		diag.ShowModal();
		return false;
	}

	str_fname = filename->GetValue();



	return true;
}

SavePlotImage::~SavePlotImage()
{
	delete filename;
	delete btn_Destination;
	delete width;
	delete height;
	delete wLabel;
	delete hLabel;
	delete btn_Save;
	delete btn_Cancel;

	sizerMain->Clear();
}

BEGIN_EVENT_TABLE(SavePlotImage, wxDialog)
EVT_BUTTON(SAVE_PLOT_BTN_NAME, SavePlotImage::GetFileLocation)
END_EVENT_TABLE()

bool LineData::GetMaxMinValue(double& max, double& min)
{
	if (data.size() == 0)
		return false;
	max = min = data[0];
	for (unsigned int i = 0; i < data.size(); ++i) {
		if (data[i] > max)
			max = data[i];
		if (data[i] < min)
			min = data[i];
	}
	return true;
}

int AutoRotateColor(wxPen& pen, int color)
{
	color = color % 13;
	switch (color)
	{
	case 0:
		pen.SetColour(0, 0, 0);
		break;
	case 1:
		pen.SetColour(128, 0, 0);
		break;
	case 2:
		pen.SetColour(0, 128, 0);
		break;
	case 3:
		pen.SetColour(0, 0, 128);
		break;
	case 4:
		pen.SetColour(128, 128, 0);
		break;
	case 5:
		pen.SetColour(128, 0, 128);
		break;
	case 6:
		pen.SetColour(0, 128, 128);
		break;
	case 7:
		pen.SetColour(255, 0, 0);
		break;
	case 8:
		pen.SetColour(0, 255, 0);
		break;
	case 9:
		pen.SetColour(0, 0, 255);
		break;
	case 10:
		pen.SetColour(255, 255, 0);
		break;
	case 11:
		pen.SetColour(255, 0, 255);
		break;
	case 12:
		pen.SetColour(0, 255, 255);
		break;
	default:
		pen.SetColour(0, 0, 0);
		break;
	}
	return(0);
}
