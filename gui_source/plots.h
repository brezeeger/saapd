#ifndef PLOTS_H
#define PLOTS_H

#include "mathplot.h"

class ContactPlot : public mpProfile
{
public:
	ContactPlot();
	double GetY(double x);
	void switchContact(int contactIndex);
	bool changeScale(int newScale);
	int getScale();

private:
	int index;
	int scale;
};

#endif