#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"
#include "myWXImageHandler.h"

#include "SimulationWindow.h"
#include "SimCalculations.h"

#include <fstream>

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/grid.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/textfile.h"
#include "wx/colordlg.h"
#include "wx/colourdata.h"
#include "wx/textdlg.h"
#include "wx/dynarray.h"
#endif

#include "../kernel_source/model.h"
#include "ratesFixed.xpm"

#include "wx/image.h"

#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif


simCalculationPanel::simCalculationPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY), stReflections(nullptr)
{
	szrPrimary = new wxBoxSizer(wxHORIZONTAL); //This wrapper is just to make a margin around the panel

	szrCol1 = new wxBoxSizer(wxVERTICAL);
	szrCol2 = new wxBoxSizer(wxVERTICAL);
	szrTopRight = new wxBoxSizer(wxHORIZONTAL);
	fgszrOpticalTexts = new wxFlexGridSizer(2, 3, 3);


	sbGenRec = new wxStaticBoxSizer(wxVERTICAL, this, wxT("Generation and Recombination"));
	
	chkSRH1 = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Shockley-Read-Hall 1"));
	chkSRH1->SetToolTip("Capture of electrons into defects from the conduction band");

	chkSRH2 = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Shockley-Read-Hall 2"));
	chkSRH2->SetToolTip("Emission of electrons from defects into the conduction band");

	chkSRH3 = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Shockley-Read-Hall 3"));
	chkSRH3->SetToolTip("Capture of holes into defects from the valence band");

	chkSRH4 = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Shockley-Read-Hall 4"));
	chkSRH4->SetToolTip("Emission of holes from defects into the valence band");
	
	chkThermalGen = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermal Generation"));
	chkThermalGen->SetToolTip("Generates electrons and holes in the conduction and valence band respectively.");

	chkThermalRec = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Band-Band Recombination"));
	chkThermalRec->SetToolTip("Commonly known as Thermal Recombination. In direct band gap materials, this process emits light. Electrons in the conduction band and holes in the valence band mutually annhiliate one another.");


	

	

	sbCurrents = new wxStaticBoxSizer(wxVERTICAL, this, "Currents");
	chkCurrent = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Drift-Diffusion"));
	chkCurrent->SetToolTip("Currents staying within the appropriate band. Slight modifications to be more in line with the original Boltzmann Transport Equation");
	chkTunnel = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Tunneling (unstable)"));
	chkTunnel->SetToolTip("Tunneling makes no discretion on state or material type (defect->defect, band->defect, band->opposing band, etc.)");
	chkTunnel->Disable();

	
	sbOptical = new wxStaticBoxSizer(wxVERTICAL, this, "Optical Rates");
	sbOptical->GetStaticBox()->SetToolTip("Note, these rates require accurate absorption data to give valid results. If the inputted sub band gap absorption coefficient is zero, the bulk of the defect related rates will be zero (unless it generates hot carriers in the band).");
	chkLightEmissions = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Light Emissions"));
	chkLightEmissions->SetToolTip("When electrons relax to new bands or defects, should light be emitted. Note: This does not require light to occur as everything below does.");
	chkLightPower = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Light Intensity"));
	chkLightPower->SetToolTip("The total intensity of light entering any segment of the device");
	chkLightAbsorbGeneration = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Band-Band Generation"));
	chkLightAbsorbGeneration->SetToolTip("Photons are absorbed and an electron-hole pair is generated in the conduction and valence bands");
	chkLightAbsorbSRH = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Defect-Band generation"));
	chkLightAbsorbSRH->SetToolTip("Photons kick an electron from a defect into the conduction band or a hole from the defect into the valence band");
	chkLightAbsorbDefect = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Defect-Defect Transitions"));
	chkLightAbsorbDefect->SetToolTip("Photons excite electrons from a defect to one with higher energy");
	chkLightSERecombination = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Band-Band Stimulated Emission"));
	chkLightSERecombination->SetToolTip("An electron is relaxed from the conduction band to the valence band by light. An identical photon is created.");
	chkLightSEsrh = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Band-Defect Stimulated Emission"));
	chkLightSEsrh->SetToolTip("Light assists an electron to relax between a band and defect state, generating an identical photon.");
	chkLightSEDefect = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Defect-Defect Stimulated Emission"));
	chkLightSEDefect->SetToolTip("Light assists an electron to relax between two defect states, generating an identical photon.");
	chkRecyclePhotonsModifyRates = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Photon Recycling"));
	chkRecyclePhotonsModifyRates->SetToolTip("CAUTION: SLOW. NUMERICAL INSTABILITY MORE LIKELY. The photons created via light emissions has it's intensity adjusted due to absorption or stimulated emission. When enabled, this contributes to the overall rates of the electron states.");


	sbScatter = new wxStaticBoxSizer(wxVERTICAL, this, "Scattering Processes");
	chkScatter = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Band Scattering"));
	chkScatter->SetToolTip("Simple scattering within the band. Rates based off mobility and conductive effective mass");
	chkThermalizeCB = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Conduction Band"));
	chkThermalizeCB->SetToolTip("Electrons in the conduction band are instantly redistributed to local thermal equilibrium. Renders scattering calculation in CB useless.");
	chkThermalizeVB = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Valence Band"));
	chkThermalizeVB->SetToolTip("Holes in the valence band are instantly redistributed to local thermal equilibrium. Renders scattering calculation in VB useless.");
	chkThermalizeDON = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Donors"));
	chkThermalizeDON->SetToolTip("Each donor is individually thermalized among to local equilibrium its accompanying states.");
	chkThermalizeACP = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Acceptors"));
	chkThermalizeACP->SetToolTip("Each acceptor is individually thermalized to local equilibrium among its accompanying states");
	chkThermalizeUrbachCB = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Urbach States (CB)"));
	chkThermalizeUrbachCB->SetToolTip("The conduction band urbach electrons are instantly redistributed to local thermal equilibrium");
	chkThermalizeUrbachVB = new wxCheckBox(this, SIM_CALC_PANEL_CHK, wxT("Thermalize Urbach States (VB)"));
	chkThermalizeUrbachVB->SetToolTip("The valence band urbach holes are instantly redistributed to local thermal equilibrium");

	
	txtOpticalResolution = new wxTextCtrl(this, SIM_CALC_PANEL_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtOpticalMinEnergy = new wxTextCtrl(this, SIM_CALC_PANEL_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtOpticalMaxEnergy = new wxTextCtrl(this, SIM_CALC_PANEL_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtOpticalIntennsityCutoff = new wxTextCtrl(this, SIM_CALC_PANEL_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtReflections = new wxTextCtrl(this, SIM_CALC_PANEL_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	//calcPicture = new myImagePane(this, "resources/ratesFixed.png", wxBITMAP_TYPE_PNG);
	calcPicture = new myImagePane(this, rateImage);

	stOpticalIntennsityCutoff = new wxStaticText(this, wxID_ANY, wxT("Intensity Cut-off"));
	stOpticalIntennsityCutoff->SetToolTip(wxT("Light is ray traced. If a ray's intensity is beneath this value (mW cm\u207B\u00B2), don't emit the ray."));
	stOpticalResolution = new wxStaticText(this, wxID_ANY, wxT("Resolution (eV)"));
	stOpticalResolution->SetToolTip(wxT("Optical calculations are binned. This is the spacing between each processed wavelength. Small resolutions result in finer data, but the cost is computational time and resources (potentially running out of memory and crashing)."));
	stOpticalMinEnergy = new wxStaticText(this, wxID_ANY, wxT("Min Energy (eV)"));
	stOpticalMinEnergy->SetToolTip(wxT("At what energy does the binning start? Don't waste resources and time on low, irrelevant energies. This cutoff prevents ALL processing even if the defined spectra has wavelengths outside this range."));
	stOpticalMaxEnergy = new wxStaticText(this, wxID_ANY, wxT("Max Energy (eV)"));
	stOpticalMaxEnergy->SetToolTip(wxT("At what energy should the binning end. A (-1) value will choose the difference between the farthest two energy states in the model."));
	stReflections = new wxStaticText(this, wxID_ANY, wxT("Max Reflections"));
	stReflections->SetToolTip(wxT("Light is refracted and reflected through the device as it is raytraced. Set a cap on the number of reflections which may occur."));

	


	sbScatter->Add(chkScatter, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeCB, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeVB, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeACP, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeDON, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeUrbachCB, 0, wxALL, 3);
	sbScatter->Add(chkThermalizeUrbachVB, 0, wxALL, 3);

	fgszrOpticalTexts->Add(stOpticalIntennsityCutoff, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrOpticalTexts->Add(txtOpticalIntennsityCutoff);
	fgszrOpticalTexts->Add(stOpticalResolution, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrOpticalTexts->Add(txtOpticalResolution);
	fgszrOpticalTexts->Add(stOpticalMinEnergy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrOpticalTexts->Add(txtOpticalMinEnergy);
	fgszrOpticalTexts->Add(stOpticalMaxEnergy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrOpticalTexts->Add(txtOpticalMaxEnergy);
	fgszrOpticalTexts->Add(stReflections, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrOpticalTexts->Add(txtReflections);

	sbOptical->Add(chkLightEmissions, 0, wxALL, 3);
	sbOptical->Add(chkLightPower, 0, wxALL, 3);
	sbOptical->Add(chkLightAbsorbGeneration, 0, wxALL, 3);
	sbOptical->Add(chkLightAbsorbSRH, 0, wxALL, 3);
	sbOptical->Add(chkLightAbsorbDefect, 0, wxALL, 3);
	sbOptical->Add(chkLightSERecombination, 0, wxALL, 3);
	sbOptical->Add(chkLightSEsrh, 0, wxALL, 3);
	sbOptical->Add(chkLightSEDefect, 0, wxALL, 3);
	sbOptical->Add(chkRecyclePhotonsModifyRates, 0, wxALL, 3);
	sbOptical->Add(fgszrOpticalTexts, 0, wxALL, 3);

	sbCurrents->Add(chkCurrent, 0, wxALL, 3);
	sbCurrents->Add(chkTunnel, 0, wxALL, 3);

	sbGenRec->Add(chkThermalGen, 0, wxALL, 3);
	sbGenRec->Add(chkThermalRec, 0, wxALL, 3);
	sbGenRec->Add(chkSRH1, 0, wxALL, 3);
	sbGenRec->Add(chkSRH2, 0, wxALL, 3);
	sbGenRec->Add(chkSRH3, 0, wxALL, 3);
	sbGenRec->Add(chkSRH4, 0, wxALL, 3);


	szrCol1->Add(sbCurrents, 0, wxEXPAND | wxBOTTOM, 10);
	szrCol1->Add(sbOptical, 0, 0, 0);
	szrTopRight->Add(sbGenRec, 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	szrTopRight->Add(sbScatter, 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	szrCol2->Add(szrTopRight, 0, wxEXPAND, 0);

	
	szrCol2->Add(calcPicture, 1, wxEXPAND | wxALL, 5);
	
//	szrCol2->Add(picBitmap, 1, wxEXPAND | wxALL, 2);
	
	

	szrPrimary->Add(szrCol1, 0, wxALL, 5);
	szrPrimary->Add(szrCol2, 1, wxALL | wxEXPAND, 5);
	
	

	SetSizer(szrPrimary);
	TransferToGUI();	//make sure all the data contained is accurate to the model description
}

simCalculationPanel::~simCalculationPanel()
{
	delete chkSRH1;
	delete chkSRH2;
	delete chkSRH3;
	delete chkSRH4;
	delete chkThermalGen;
	delete chkThermalRec;
	delete chkCurrent;
	delete chkTunnel;
	delete chkLightPower;
	delete chkLightAbsorbGeneration;
	delete chkLightAbsorbSRH;
	delete chkLightAbsorbDefect;
	delete chkLightSERecombination;
	delete chkLightSEsrh;
	delete chkLightSEDefect;
	delete chkLightEmissions;
	delete chkRecyclePhotonsModifyRates;
	delete txtOpticalResolution;
	delete txtOpticalMinEnergy;
	delete txtOpticalMaxEnergy;
	delete txtOpticalIntennsityCutoff;
	delete chkScatter;
	delete chkThermalizeCB;
	delete chkThermalizeVB;
	delete chkThermalizeACP;
	delete chkThermalizeDON;
	delete chkThermalizeUrbachCB;
	delete chkThermalizeUrbachVB;
	delete calcPicture;
}




void simCalculationPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)	//this should never fail...
		return;

	Calculations& c = sim->EnabledRates;

	c.Tunnel = false;
	c.Current = chkCurrent->GetValue();
	c.Ldef = chkLightAbsorbDefect->GetValue();
	c.Lgen = chkLightAbsorbGeneration->GetValue();
	c.LightEmissions = chkLightEmissions->GetValue();
	c.Lpow = chkLightPower->GetValue();
	c.Lscat = false;// chkCurrent->GetValue();
	c.Lsrh = chkLightAbsorbSRH->GetValue();
	if (txtOpticalMaxEnergy->GetValue().ToDouble(&c.OpticalEMax))
	{
		if (c.OpticalEMax <= 0)
			c.OpticalEMax = -1.0;
	}
	else
		c.OpticalEMax = -1.0;

	if (txtOpticalMinEnergy->GetValue().ToDouble(&c.OpticalEMin))
	{
		if (c.OpticalEMin <= 0)
			c.OpticalEMin = 0.0;
	}
	else
		c.OpticalEMin = 0.0;

	if (txtOpticalResolution->GetValue().ToDouble(&c.OpticalEResolution))
	{
		if (c.OpticalEResolution <= 0)
			c.OpticalEResolution = 0.01;
	}
	else
		c.OpticalEResolution = 0.01;

	if (txtOpticalIntennsityCutoff->GetValue().ToDouble(&c.OpticalIntensityCutoff))
	{
		if (c.OpticalIntensityCutoff <= 0)
			c.OpticalIntensityCutoff = 1e-4;
	}
	else
		c.OpticalIntensityCutoff = 1e-4;
	
	
	c.RecyclePhotonsAffectRates = chkRecyclePhotonsModifyRates->GetValue();
	c.Scatter = chkScatter->GetValue();
	c.SEdef = chkLightSEDefect->GetValue();
	c.SErec = chkLightSERecombination->GetValue();
	c.SEscat = false;// chkCurrent->GetValue();
	c.SEsrh = chkLightSEsrh->GetValue();
	c.SRH1 = chkSRH1->GetValue();
	c.SRH2 = chkSRH2->GetValue();
	c.SRH3 = chkSRH3->GetValue();
	c.SRH4 = chkSRH4->GetValue();
	c.Scatter = chkScatter->GetValue();
	c.ThermalGen = chkThermalGen->GetValue();
	c.ThermalRec = chkThermalRec->GetValue();
	
	int thermalize = 0;
	if (chkThermalizeCB->GetValue())
		thermalize |= THERMALIZE_CB;
	if (chkThermalizeVB->GetValue())
		thermalize |= THERMALIZE_VB;
	if (chkThermalizeACP->GetValue())
		thermalize |= THERMALIZE_ACP;
	if (chkThermalizeDON->GetValue())
		thermalize |= THERMALIZE_DON;
	if (chkThermalizeUrbachCB->GetValue())
		thermalize |= THERMALIZE_CBT;
	if (chkThermalizeUrbachVB->GetValue())
		thermalize |= THERMALIZE_VBT;
	c.Thermalize = thermalize;

	UpdateGUIValues(event);
}

void simCalculationPanel::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)	//this should never fail...
		return;

	int thermalizations = sim->EnabledRates.Thermalize;
	if (TEST_BIT(thermalizations, THERMALIZE_CB | THERMALIZE_VB))
	{
		chkScatter->Disable();
		chkScatter->SetValue(false);
	}
	else
	{
		if (chkScatter->IsEnabled() == false)
		{
			chkScatter->Enable();
			if (event.GetEventObject() == chkThermalizeCB || event.GetEventObject() == chkThermalizeVB)
				chkScatter->SetValue(true);
		}
	}

	if (sim->EnabledRates.LightEmissions)
		chkRecyclePhotonsModifyRates->Enable();
	else
	{
		chkRecyclePhotonsModifyRates->SetValue(false);
		chkRecyclePhotonsModifyRates->Disable();
	}

	chkTunnel->Disable();

	chkSRH1->Enable();
	chkSRH2->Enable();
	chkSRH3->Enable();
	chkSRH4->Enable();
	chkThermalGen->Enable();
	chkThermalRec->Enable();

	chkCurrent->Enable();
	chkLightPower->Enable();
	chkLightAbsorbGeneration->Enable();
	chkLightAbsorbSRH->Enable();
	chkLightAbsorbDefect->Enable();
	chkLightSERecombination->Enable();
	chkLightSEsrh->Enable();
	chkLightSEDefect->Enable();
	chkLightEmissions->Enable();
	chkRecyclePhotonsModifyRates->Enable();
	txtOpticalResolution->Enable();
	txtOpticalMinEnergy->Enable();
	txtOpticalMaxEnergy->Enable();
	txtOpticalIntennsityCutoff->Enable();
	txtReflections->Enable();
	chkThermalizeCB->Enable();
	chkThermalizeVB->Enable();
	chkThermalizeACP->Enable();
	chkThermalizeDON->Enable();
	chkThermalizeUrbachCB->Enable();
	chkThermalizeUrbachVB->Enable();
}

void simCalculationPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)	//this should never fail...
		return;
	
	chkTunnel->SetValue(sim->EnabledRates.Tunnel);

	chkSRH1->SetValue(sim->EnabledRates.SRH1);
	chkSRH2->SetValue(sim->EnabledRates.SRH2);
	chkSRH3->SetValue(sim->EnabledRates.SRH3);
	chkSRH4->SetValue(sim->EnabledRates.SRH4);
	chkThermalGen->SetValue(sim->EnabledRates.ThermalGen);
	chkThermalRec->SetValue(sim->EnabledRates.ThermalRec);

	chkCurrent->SetValue(sim->EnabledRates.Current);
	chkLightPower->SetValue(sim->EnabledRates.Lpow);
	chkLightAbsorbGeneration->SetValue(sim->EnabledRates.Lgen);
	chkLightAbsorbSRH->SetValue(sim->EnabledRates.Lsrh);
	chkLightAbsorbDefect->SetValue(sim->EnabledRates.Ldef);
	chkLightSERecombination->SetValue(sim->EnabledRates.SErec);
	chkLightSEsrh->SetValue(sim->EnabledRates.SEsrh);
	chkLightSEDefect->SetValue(sim->EnabledRates.SEdef);
	chkLightEmissions->SetValue(sim->EnabledRates.LightEmissions);
	chkRecyclePhotonsModifyRates->SetValue(sim->EnabledRates.RecyclePhotonsAffectRates);
	

	chkThermalizeCB->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_CB));
	chkThermalizeVB->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_VB));
	chkThermalizeACP->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_ACP));
	chkThermalizeDON->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_DON));
	chkThermalizeUrbachCB->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_CBT));
	chkThermalizeUrbachVB->SetValue(TEST_BIT(sim->EnabledRates.Thermalize, THERMALIZE_CBT));
	chkScatter->SetValue(sim->EnabledRates.Scatter);

	ValueToTBox(txtOpticalResolution, sim->EnabledRates.OpticalEResolution);
	ValueToTBox(txtOpticalMinEnergy, sim->EnabledRates.OpticalEMin);
	ValueToTBox(txtOpticalMaxEnergy, sim->EnabledRates.OpticalEMax);
	ValueToTBox(txtOpticalIntennsityCutoff, sim->EnabledRates.OpticalIntensityCutoff);
	ValueToTBox(txtReflections, sim->maxLightReflections);
	

	UpdateGUIValues(wxCommandEvent());
}

Simulation* simCalculationPanel::getSim()
{
	return getMyClassParent()->getSim();
}

simulationWindow* simCalculationPanel::getMyClassParent()
{
	return (simulationWindow*)GetParent();
}


BEGIN_EVENT_TABLE(simCalculationPanel, wxPanel)
EVT_CHECKBOX(SIM_CALC_PANEL_CHK, simCalculationPanel::TransferToMdesc)
EVT_TEXT(SIM_CALC_PANEL_TXT, simCalculationPanel::TransferToMdesc)
END_EVENT_TABLE()