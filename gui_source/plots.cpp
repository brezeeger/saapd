#include <vector>
#include <math.h>
#include "gui_structs.h"
#include "plots.h"

#if (defined __WXMSW__ && defined _DEBUG)
    #include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<contact> contacts_list;

double scalarTEN(int index)
{
	double scalar = 1.0;
	while(index-->0) scalar *= 0.001;
	return scalar;
}

ContactPlot::ContactPlot()
: mpProfile(wxT("Contact Voltage"))
{
	index = -1;
	scale = 0;
}

double ContactPlot::GetY(double x)
{
	if(index < 0 || contacts_list[index].voltages.empty() || contacts_list[index].voltages[0].time > x)
		return 0.0; //default case for if invalid contact, empty contact, or no value set before specified time
	int counter = contacts_list[index].voltages.size();
	x *= scalarTEN(scale);
	while(--counter >= 0)
	{
		if(x > contacts_list[index].voltages[counter].time)
		{
			if(contacts_list[index].voltages[counter].cos)
			{
				return contacts_list[index].voltages[counter].amp
						*cos(contacts_list[index].voltages[counter].freq*2*PI*x
						+contacts_list[index].voltages[counter].phase_offset)
						+contacts_list[index].voltages[counter].voltage_offset;
			}
			else
				return contacts_list[index].voltages[counter].amp;
		}
	}
	return 0.0; //should always be short circuited
}

void ContactPlot::switchContact(int contactIndex)
{
	index = contactIndex;	
}

//returns true if new scale differs from old scale.
bool ContactPlot::changeScale(int newScale)
{
	if(scale != newScale)
	{
		scale = newScale;
		return true;
	}
	return false;
}

int ContactPlot::getScale()
{
	return scale;
}
