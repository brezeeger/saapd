#include <vector>

#include "XML.h"
#include "kernel.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "guiwindows.h"
#include "gui.h"
#include "icon.xpm"
#include "MatPanel.h"
#include "MatDefects.h"
#include "ImpurityPanel.h"
#include "LayerPanel.h"
#include "SimulationWindow.h"
#include "SimCalculations.h"
#include "SimGeneral.h"
#include "SimOutputs.h"
#include "SimSpectra.h"
#include "SimSteadyState.h"
#include "SimTransient.h"
#include <wx/notebook.h>

#include "../kernel_source/load.h"

#include "../kernel_source/model.h"

#if (defined __WXMSW__ && defined _DEBUG)
    #include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

/*
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
*/

//implement application object during execution instead of statically, better performance
IMPLEMENT_APP(SAAPDGUI)


//global structs of data... dammit anyway.
std::vector<defect> defects_list;
std::vector<impurity> impurities_list;
std::vector<material> materials_list;
std::vector<layer> layers_list;
std::vector<contact> contacts_list;

//static event table, simpler
BEGIN_EVENT_TABLE(mainFrame, wxFrame)
EVT_MENU(SAAPD_Quit,  mainFrame::OnQuit)
EVT_MENU(SAAPD_CALC, mainFrame::LaunchCalc)
EVT_MENU(SAAPD_ABOUT, mainFrame::LaunchAbout)
EVT_MENU(LOAD_SIM, mainFrame::LoadSimulation)
EVT_MENU(SAVE_SIM, mainFrame::SaveSimulation)
EVT_BUTTON(RUN_CUR, mainFrame::RunCurrentSim)
EVT_BUTTON(LOAD_AND_RUN, mainFrame::LoadAndRunSim)
EVT_BUTTON(ADD_BATCH, mainFrame::AddBatch)
EVT_BUTTON(ADD_FOLDER_BATCH, mainFrame::AddBatchFolder)
EVT_BUTTON(RUN_BATCH, mainFrame::InitiateBatch)
EVT_COMMAND(KERNEL_UPDATE, wxEVT_COMMAND_TEXT_UPDATED, mainFrame::UpdateDialog)
EVT_COMMAND(UPDATE_SUBWINDOW, wxEVT_COMMAND_TEXT_UPDATED, mainFrame::AddEventToSubWindow)
EVT_COMMAND(SIM_FINISHED, wxEVT_COMMAND_TEXT_UPDATED, mainFrame::DestroyKernel)
EVT_COMMAND(ATTEMPT_BATCH, wxEVT_COMMAND_TEXT_UPDATED, mainFrame::AttemptStartBatch)
EVT_COMMAND(REMOVE_BATCH, wxEVT_BUTTON, mainFrame::RemoveBatchSim)
EVT_NOTEBOOK_PAGE_CHANGED(PANEL_CHANGES, mainFrame::ChangePanel)
END_EVENT_TABLE()

bool SAAPDGUI::OnInit()
{
	
	mainFrame *frame = new mainFrame(wxT("SAAPD"));
	
	frame->Centre(); // center window initially
	
	 
	//Note to self: use Enable(false) to disable input
	frame->Show(true);
	frame->Refresh();
	frame->Update();
	
	return true;
}

mainFrame::mainFrame(const wxString& title)
	: wxFrame(NULL, SAAPD_MAIN_FRAME, title, wxDefaultPosition, wxSize(800, 600)) //construct w/ default size 800 x 600
{
	
#if wxUSE_MENUS
    // create a menu bar
    fileMenu = new wxMenu;
	fileMenu->Append(LOAD_SIM, wxT("Load Simulation\tCtrl-L"), wxT("Loads a simulation from file"));
	fileMenu->Append(SAVE_SIM, wxT("Save Simulation\tCtrl-S"), wxT("Saves the current simulation"));
	
    // the "About" item should be in the help menu
    helpMenu = new wxMenu;
//    helpMenu->Append(SAAPD_About, wxT("&About\tF1"), wxT("Show about dialog"));
    helpMenu->Append(SAAPD_CALC, wxT("Launch &Calculator\tF2"), wxT("Launch Calculator"));
	helpMenu->Append(SAAPD_ABOUT, wxT("About\tF1"), wxT("About"));
    fileMenu->Append(SAAPD_Quit, wxT("E&xit\tCtrl-Q"), wxT("Quit this program"));
	
    // now append the freshly created menu to the menu bar...
    menuBar = new wxMenuBar( wxMB_DOCKABLE );
    menuBar->Append(fileMenu, wxT("&File"));
    menuBar->Append(helpMenu, wxT("&Help"));
	
    // ... and attach this menu bar to the frame
    this->SetMenuBar(menuBar);
#endif // wxUSE_MENUS
	
	

	mdescGUI = new ModelDescribe();
	mdescGUI->simulation = new Simulation();
	mdescGUI->simulation->mdesc = mdescGUI;
	
	//I have no idea why I'm not the icon on this main page. Nothing I've done seems to work. :/
//	wxIconBundle iconBundle;
//	iconBundle.AddIcon(wxICON(icon16));
//	iconBundle.AddIcon(wxICON(icon32));
//	iconBundle.AddIcon(wxICON(icon48));

	wxIcon myicon(wxICON(aaaAPP_ICON));

	SetIcon(wxICON(saapdicon));
//	SetIcon(wxICON(icon32_xpm));
//	SetIcon(wxICON(icon16_xpm));
//	SetIcons(iconBundle);
	wxInitAllImageHandlers();

	//Before this line is some default stuff
	progressDiag = NULL;
	//create tabs
	topNotebook = new wxNotebook(this, PANEL_CHANGES, wxDefaultPosition);
	
	//TODO: maybe create new inherited obj or move to helper functions for clarity, but then would need new event handler for running code
	deviceWindow = new wxNotebook(topNotebook, PANEL_CHANGES);
	
	executeWindow = new executePanel(topNotebook);
	resultsWindow = new wxNotebook(topNotebook, wxID_ANY);
	simWindow = new simulationWindow(topNotebook, PANEL_CHANGES);	//wxNotebook inherit

	dpropertiesWindow = new devPropertiesPanel(deviceWindow);
	dmaterialsWindow = new devMaterialPanel(deviceWindow);
	dimpuritiesWindow = new devImpurityPanel(deviceWindow);
	layersWindow = new devLayerTopPanel(deviceWindow);

	graphWindow = new resultsPanel(resultsWindow);
	

	dpropertiesWindow->SizeRight();
	dmaterialsWindow->SizeRight();
	dimpuritiesWindow->SizeRight();
	layersWindow->SizeRight();
	graphWindow->SizeRight();
	
	executeWindow->SizeRight();
	

	deviceWindow->AddPage(dpropertiesWindow, wxT("Properties"), true, 0);
	deviceWindow->AddPage(dmaterialsWindow, wxT("Materials"), false, 1);
	deviceWindow->AddPage(dimpuritiesWindow, wxT("Impurities"), false, 2);
	deviceWindow->AddPage(layersWindow, wxT("Layers"), false, 3);
	
	

	resultsWindow->AddPage(graphWindow, wxT("Graph"), true, 0);

	topNotebook->AddPage(deviceWindow, wxT("Device"), false, 0);
	topNotebook->AddPage(simWindow, wxT("Simulation"), true, 1);
	topNotebook->AddPage(executeWindow, wxT("Run"), false, 2);
	topNotebook->AddPage(resultsWindow, wxT("Results"), false, 3);

	
//	pointWindow = new pointPanel(resultsWindow);

	
//	resultsWindow->AddPage(pointWindow, wxT("Point"), false, 1);


	
//	pointWindow->SizeRight();
	redirect = NULL;
	//wxBoxSizer* matSizer = new wxBoxSizer(wxVERTICAL);
	runBatch = false;
	
	//TODO: make decimal spinctrls
	//wxTextCtrl* toggle = new wxTextCtrl(resultsRunWindow, wxID_ANY, wxT("5.000"), wxPoint(20,50), wxSize(140,wxDefaultCoord)); 
	//toggle->SetEditable(true);
	
	SetMinSize(GetSize());
	topNotebook->SetSelection(0);
	
}

mainFrame::~mainFrame()
{
	/*
	delete spropertiesWindow;
	delete spectrumWindow;
	delete outputWindow;
	delete contactWindow;
	delete dpropertiesWindow;
	delete layersWindow;
	delete dmaterialsWindow;
	delete dimpuritiesWindow;
	delete executeWindow;
	delete graphWindow;
	delete pointWindow;
//	delete fileMenu;
//	delete helpMenu;
//	delete menuBar;
//	delete topNotebook;
//	delete simulationWindow;
//	delete deviceWindow;
//	delete resultsWindow;
//	delete progressDiag;
*/
	delete mdescGUI;
}

void mainFrame::ChangePanel(wxBookCtrlEvent& event)
{
	if (event.GetEventObject() == topNotebook)
	{
		int newSel = event.GetSelection();
		switch (newSel) {
		case 0:	//device window
			dpropertiesWindow->TransferToGUI();
			dmaterialsWindow->TransferToGUI();
			dimpuritiesWindow->TransferToGUI();
			layersWindow->TransferToGUI();
			break;
		case 1:	//sim
			simWindow->getGeneralPanel()->TransferToGUI();
			simWindow->getCalcPanel()->TransferToGUI();
			simWindow->getOutputPanel()->TransferToGUI();
			simWindow->getSpectraPanel()->TransferToGUI();
			simWindow->getSSPanel()->TransferToGUIDefine();
			simWindow->getSSPanel()->UpdateGUIValuesDetailed(event);
			simWindow->getSSPanel()->UpdateGUIValuesSummary(event);
			simWindow->getTransPanel()->TransferToGUI(event);
			break;
		case 2:	//run
			break;
		case 3:	//results
			break;
		default:	// ??
			break;
		}
	}
	else if (event.GetEventObject() == deviceWindow)
	{
		//make sure the data is all correct when updating the view!
		int newSel = event.GetSelection();
		switch (newSel)
		{
		case 0:	//properties
			dpropertiesWindow->TransferToGUI();
			break;
		case 1:	//materials
			dmaterialsWindow->TransferToGUI();
			break;
		case 2:	//impurities
			dimpuritiesWindow->TransferToGUI();
			break;
		case 3:	//layers
			layersWindow->TransferToGUI();
			break;
		default:
			break;
		}
	}
	else if (event.GetEventObject() == simWindow)
	{
		int sel = event.GetSelection();
		switch (sel)
		{
		case 0:	//general
			simWindow->getGeneralPanel()->TransferToGUI();
			break;
		case 1:	//calculations
			simWindow->getCalcPanel()->TransferToGUI();
			break;
		case 2:	//outputs
			simWindow->getOutputPanel()->TransferToGUI();
			break;
		case 3:	//spectra
			simWindow->getSpectraPanel()->TransferToGUI();
			break;
		case 4:	//steady state
			simWindow->getSSPanel()->TransferToGUIDefine();
			simWindow->getSSPanel()->UpdateGUIValuesDetailed(event);
			simWindow->getSSPanel()->UpdateGUIValuesSummary(event);
			break;
		case 5:	//transient
			simWindow->getTransPanel()->TransferToGUI(event);
			simWindow->getTransPanel()->Layout();
			break;
		default:
			break;
		}
	}
}

void mainFrame::LaunchCalc(wxCommandEvent& event)
{
	CalcDialog* diag = new CalcDialog(this);
	diag->Show();

}

void mainFrame::LaunchAbout(wxCommandEvent& event) {
	CreditsDialog *credDiag = new CreditsDialog(this, "About");
	credDiag->Show();
}

void mainFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	printf("bye!");
	if(progressDiag)
	{
		progressDiag->Destroy();	//use instead of delete command
		progressDiag = NULL;
	}
	Close(true);
	

}

void mainFrame::SaveSimulation(wxCommandEvent& event)
{
	if (mdescGUI == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("DEV files (*.dev)|*.dev"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
        return;

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		mdescGUI->SaveToXML(oFile);
		oFile.closeFile();
	}
}


void mainFrame::RunCurrentSim(wxCommandEvent& event)
{
	//Update input values to internal data structures 
	dpropertiesWindow->TransferToMDesc(event);
	layersWindow->TransferToMdesc(event);
	dmaterialsWindow->TransferToMdesc();
	dimpuritiesWindow->TransferToMdesc(event);
	simWindow->TransferToMdesc(event);

	//Write output
	executeWindow->runCurrent->Disable();
	wxString filename = "";// spropertiesWindow->oName->GetValue();
	time_t t = time(NULL);
	tm* time = localtime(&t);

	if(filename.IsEmpty())
		filename.Printf(wxT("_temp%d%d%d%d%d%d.dev"), time->tm_year+1900, time->tm_mon, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec);
	else if(!filename.EndsWith(wxT(".dev")))
	{
		filename.Append(wxT(".dev"));
	}
	graphWindow->curOutputName = filename.BeforeLast('.');

	XMLFileOut oFile(filename.ToStdString());
	if(oFile.isOpen())
	{
		Disable();
		mdescGUI->SaveToXML(oFile);
		oFile.closeFile();
		Enable();
	}
	else
	{
		wxLogError(wxT("Failed to generate input file for simulation!"));
		return;
	}
	//make sure file written
	long iters = 0;
//	spropertiesWindow->iters->GetValue().ToLong(&iters);
	
	CreateKernel(filename, iters);

	executeWindow->runCurrent->Enable(true);
}

void mainFrame::LoadAndRunSim(wxCommandEvent& event)
{
	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("DEV files (*.dev)|*.dev|State Files (*.state)|*.state"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if(openDialog.ShowModal() == wxID_CANCEL) return;
	wxString thePath = openDialog.GetPath();

	int iters = 1000;

	if(!thePath.EndsWith(".dev") && !thePath.EndsWith(".state"))
		return;

	executeWindow->runCurrent->Disable();
	executeWindow->loadAndRun->Disable();
	executeWindow->workingDir->Disable();
	executeWindow->selectDir->Disable();
	executeWindow->runBatch->Disable();
	
	wxString filePath = openDialog.GetPath();
	CreateKernel(filePath, iters);
}

void mainFrame::AddBatch(wxCommandEvent& event)
{
	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("DEV files (*.dev)|*.dev|State Files (*.state)|*.state"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if(openDialog.ShowModal() == wxID_CANCEL) return;
	wxString thePath = openDialog.GetPath();
	if(thePath.EndsWith(".dev") || thePath.EndsWith(".state"))
	{
		
		wxString::size_type pos = thePath.find_last_of("/");
		if(pos == wxString::npos)
			pos = thePath.find_last_of("\\");
		if(pos != wxString::npos)
		{
			filename.push_back(thePath);
			executeWindow->simList->Append(thePath);
			thePath.erase(pos);	//get rid of the filename
			batchWorkDir.push_back(thePath);
			executeWindow->deleteSim->Enable();
			if (executeWindow->loadAndRun->IsEnabled() == true)	//if there are simulations running, don't enable it
				executeWindow->runBatch->Enable();
			executeWindow->simList->Select(executeWindow->simList->GetCount() - 1);
		}
	}
}
void mainFrame::RemoveBatchSim(wxCommandEvent& event)
{
	int selected = executeWindow->simList->GetSelection();
	wxString fname = executeWindow->simList->GetValue();

	for (unsigned int i = 0; i < filename.size(); ++i)
	{
		if (fname == filename[i])
		{
			filename.erase(filename.begin() + i);
			batchWorkDir.erase(batchWorkDir.begin() + i);
		}
	}
	executeWindow->simList->Delete(selected);
	if (filename.size() == 0)
	{
		executeWindow->simList->Clear();
		executeWindow->deleteSim->Disable();
		executeWindow->runBatch->Disable();
	}
	executeWindow->simList->Select(0);

	event.Skip();
	return;
}
void mainFrame::AddBatchFolder(wxCommandEvent& event)
{
	wxArrayString files;
	wxDirDialog dirDialog(this, wxT("Choose Directory to add all .dev files"), wxT("/"), wxDD_NEW_DIR_BUTTON);

	if(dirDialog.ShowModal() != wxID_OK)
		return;
	
	const wxString thePath = dirDialog.GetPath();
	wxDir dir(thePath);
	unsigned int sz = dir.GetAllFiles(thePath, &files, "*.dev");
	for(unsigned int i=0; i<sz; i++)
	{
		wxString::size_type pos = files[i].find_last_of("/");
		if(pos == wxString::npos)
			pos = files[i].find_last_of("\\");
		if(pos != wxString::npos)
		{
			filename.push_back(files[i]);
			executeWindow->simList->Append(files[i]);
			files[i].erase(pos);	//get rid of the filename
			batchWorkDir.push_back(files[i]);
			executeWindow->deleteSim->Enable();
			executeWindow->runBatch->Enable();
		}
	}
	executeWindow->simList->Select(executeWindow->simList->GetCount() - 1);
}

void mainFrame::InitiateBatch(wxCommandEvent& event)
{
	runBatch = true;
	executeWindow->runCurrent->Disable();
	executeWindow->loadAndRun->Disable();
	executeWindow->workingDir->Disable();
	executeWindow->selectDir->Disable();
	executeWindow->runBatch->Disable();

	AttemptStartBatch(event);	//starts running batch simulations

}

//this should be called after any simulation finishes running
void mainFrame::AttemptStartBatch(wxCommandEvent& event)
{
	if (runBatch)
	{
		bool succeed = RunBatchSimulation();
		if (!succeed)
		{
			if (filename.size() > 0 && progressDiag)	//there are batches to run
			{
				//TRY AGAIN. Create event to attempt start batch again
				wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, ATTEMPT_BATCH);
				event.SetInt(0);
				GetEventHandler()->AddPendingEvent(event);
			}
			else if (filename.size() > 0)
			{
				//it is currently still running a simulation, but progressDiag isn't gone yet!
				runBatch = false;	//it is done running batch simulations
				executeWindow->runCurrent->Enable();
				executeWindow->loadAndRun->Enable();
				executeWindow->workingDir->Enable();
				executeWindow->selectDir->Enable();
				executeWindow->runBatch->Enable();
				wxMessageDialog failBatch(this, wxT("Failed to start next simulation in batch. Discontinuing batch processing"), wxT("Batch Failed"));
				failBatch.ShowModal();
			}
			else //there are no more entries to try - re-enable everything
			{
				runBatch = false;	//it is done running batch simulations
				executeWindow->runCurrent->Enable();
				executeWindow->loadAndRun->Enable();
				executeWindow->workingDir->Enable();
				executeWindow->selectDir->Enable();
				executeWindow->runBatch->Enable();
				executeWindow->message->SetLabel(wxT("No Simulation Currently Running"));
			}
		}
		else
		{

		}
		//if it succeeds, do nothing. Wait for kernel to create event saying all is done
	}
	//if runbatch is false, don't do anything!
}

bool mainFrame::RunBatchSimulation(void)
{
	if (progressDiag == NULL && runBatch == true)	//There is currently no running simulation
	{
		if (filename.size() > 0)
		{
			executeWindow->workingDir->SetValue(batchWorkDir.back());
			wxSetWorkingDirectory(batchWorkDir.back());
			CreateKernel(filename.back(), 100);
			if (progressDiag != NULL)	//it succeeded in creating the simulation
			{
				wxString fname = filename.back();
				filename.pop_back();
				batchWorkDir.pop_back();
				int which = executeWindow->simList->FindString(fname);
				if (which != wxNOT_FOUND)
					executeWindow->simList->Delete(which);
				return(true);
			}
		}
	}
	
	return(false);
}

void mainFrame::DestroyKernel(wxCommandEvent& event)
{
	if(progressDiag)
	{
		progressDiag->Destroy();	//this should call the destructor, which should tell the kernel to get out quickly by setting abort to true
		progressDiag = NULL;	//
	}
	if(redirect)
	{
		delete redirect;
		redirect = NULL;
	}

	if (runBatch)
		AttemptStartBatch(event);
	else //it's just done, so re-enable everything
	{
		executeWindow->runCurrent->Enable();
		executeWindow->loadAndRun->Enable();
		executeWindow->workingDir->Enable();
		executeWindow->selectDir->Enable();
		executeWindow->runBatch->Enable();
		executeWindow->message->SetLabel(wxT("No Simulation Currently Running"));
	}

	return;
}


void mainFrame::CreateKernel(wxString filename, int numIters)
{
	//Create + run kernel thread
	if(progressDiag == NULL)	//check mainFrame::RunBatchSimulation if this line is ever changed or more conditions go into making
	{
//		wxString wd = wxGetCwd();
//		kernelThread *kernel = new kernelThread(filename, this, wd);
//		if ( kernel->Create() != wxTHREAD_NO_ERROR ) {
//			wxLogError(wxT("Can’t create thread!"));
//			return;
//		}

		
		//create the progress bar
		wxString simName = filename;
		simName.Replace(wxT(".dev"), wxEmptyString);
		progressDiag = new SimulationProgressDialog(this, simName, numIters, filename);
		progressDiag->Show();
		redirect = new wxStreamToTextRedirector(executeWindow->outputText);
		executeWindow->message->SetLabel(wxT("Simulation Initializing"));
//		kernel->Run();
//		kernel->Wait();	//this just waits for the simulation to end.

//		delete progressDiag;
//		progressDiag = NULL;
//		delete kernel;
	}
	//wait until we're done with the kernel running for now?
}

void mainFrame::UpdateDialog(wxCommandEvent& event)
{
	if(progressDiag)
	{
		progressDiag->Update(event.GetInt());;
	}
}

void mainFrame::LoadSimulation(wxCommandEvent& event)
{

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("DEV files (*.dev)|*.dev"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if(openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}
	if (mdescGUI == nullptr)
		mdescGUI = new ModelDescribe();
	mdescGUI->ClearAll();

	LoadDeviceChildren(file, *mdescGUI, true);
	file.close();

	mdescGUI->ReassociateAll();	//this goes through and updates all the various links/data.

	dpropertiesWindow->TransferToGUI();
	layersWindow->TransferToGUI();
	dmaterialsWindow->TransferToGUI();
	dimpuritiesWindow->TransferToGUI();
	simWindow->TransferToGUI();
}


void mainFrame::AddEventToSubWindow(wxCommandEvent& event)
{
	int whichUpdate = event.GetInt();
	if (whichUpdate == UPDATED_FILE)
	{
		if (executeWindow)
		{
			wxCommandEvent ev(wxEVT_COMMAND_TEXT_UPDATED, UPDATED_FILE);
			executeWindow->GetEventHandler()->AddPendingEvent(ev);
		}
	}
	else if (whichUpdate == CLEAR_MESSAGE)
	{
		if (executeWindow)
		{
			wxCommandEvent ev(wxEVT_COMMAND_TEXT_UPDATED, CLEAR_MESSAGE);
			executeWindow->GetEventHandler()->AddPendingEvent(ev);
		}
	}
	else if(whichUpdate == SIMHASNOTCRASHED)
	{
		if (executeWindow)
		{
			wxCommandEvent ev(wxEVT_COMMAND_TEXT_UPDATED, SIMHASNOTCRASHED);
			executeWindow->GetEventHandler()->AddPendingEvent(ev);
		}
	}
	else
	{
		whichUpdate -= REDO_CALCS;
		if (executeWindow)
		{
			wxCommandEvent ev(wxEVT_COMMAND_TEXT_UPDATED, REDO_CALCS);
			ev.SetInt(whichUpdate);
			executeWindow->GetEventHandler()->AddPendingEvent(ev);
		}
	}
	event.Skip();
	return;
}

int SAAPDGUI::OnExit()
{
//	_CrtDumpMemoryLeaks();	//debug memory leak
//	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF);	//make sure to set this to do a heap debug test
	return(0);
}