#ifndef RESULTSPANEL_H
#define RESULTSPANEL_H


#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/grid.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/textfile.h"
#include "wx/colordlg.h"
#include "wx/colourdata.h"
#include "wx/textdlg.h"
#include "wx/dynarray.h"
#endif

#include "mathplot.h"	//this also has vector and whatnot
#include "plots.h"
#include "myPlottingWrapper.h"



#define FFILES_SET_BAD 0
#define FFILES_SET_NONE 1
#define FFILES_SET_FIRST 2
#define FFILES_SET_LAST 3

#define CPLOT_DEFAULT 0
#define CPLOT_RIGHT 1
#define CPLOT_MANIP 2

#define XAXIS_LAYER 0
#define YAXIS_LAYER 1
#define TITLE_LAYER 2
#define LEGEND_LAYER 3
#define FIRST_GRAPH_LAYER 4 //deprecated. Use: plot->CountAllLayers() - plot->CountLayers();;

enum plID{
	PDATA_UNKNOWN,
	PDATA_BD,
	PDATA_JV,
	PDATA_EMISSIONS,
	PDATA_OPTICS,
	PDATA_MESH,
	PDATA_RAWONLY,
	PDATA_CUSTOM
};

class LineData;
class ShowOutputs;
class PlotData;
class resultsPanel;





class ShowOutputs
{
public:
	bool x;
	bool y;
	bool z;
	bool point;
	bool Ef;
	bool Efn;
	bool Efp;
	bool Ec;
	bool Ev;
	bool Psi;
	bool electrons;
	bool holes;
	bool impCharge;
	bool n;
	bool p;
	bool deflight;
	bool condlight;
	bool genlight;
	bool genthermal;
	bool recthermal;
	bool emitlight, lpow, serec, lsrh, sesrh;
	bool srh1;
	bool srh2;
	bool srh3;
	bool srh4;
	bool rho;
	bool Jn;
	bool Jp;
	bool scatter;
	bool stddev;
	bool stderrBox;
	bool qe;
	//jv plot stuff
	bool j1ctc_x, j2ctc_x, j1_x, j2_x, javg_x;
	bool j1ctc_n_x, j2ctc_n_x, j1_n_x, j2_n_x, javg_n_x;
	bool j1ctc_p_x, j2ctc_p_x, j1_p_x, j2_p_x, javg_p_x;
	bool volt, volt_n, volt_p;
	bool xaxisZero;
	//emission stuff
	bool wavelength, energy, photon, pflux, power, intensity;
	//mesh stuff - keep point
	bool minX, posX, maxX, widthX, minY, posY, maxY, adjP, adjN, widthY, widthZ;
	bool adjXP1, adjXP2, adjXM1, adjXM2;
	bool adjYP1, adjYP2, adjYM1, adjYM2;

	void Reset(void);
	ShowOutputs() { Reset(); }
};

class PlotData //this is in a vector for the results panel
{
public:
	wxString filename;	//what is the file this data is loaded from
	wxString rawData;	//only use if data is type raw. (keep from opening file repeatedly when sliding around)
	ShowOutputs outs;

	mpScaleX* xaxis;
	mpScaleY* yaxis;
	mpText* title;
	mpInfoLegend* legend;
	mpWindow* plot;
	int GUISelection;	//what is currently shown on the ActivePlotData dropdown --> LData index
	std::vector<LineData> LData;	//this corresponds with the data above
	std::vector<unsigned int> ActiveLData;	//which ones hold data that needs to be plotted (Y values)
	int ActiveX;	//which LData holds the X data
	bool ShowLegend;
	bool ShowLabels;
	plID plotType;

	int UpdatePlotBD(resultsPanel* parent, bool FitPlotToData=true);
	int UpdatePlotJV(resultsPanel* parent, bool FitPlotToData=true);
	int UpdatePlotQE(resultsPanel* parent, bool FitPlotToData=true);
	int UpdatePlotMESH(resultsPanel* parent, bool FitPlotToData=true);
	int UpdatePlotRAW(resultsPanel* parent, bool FitPlotToData=true);
	int UpdatePlot(resultsPanel* parent, bool FitPlotToData=true);
	int UpdateCPlotPreview(resultsPanel* parent);	//this will ALMOSt be the same as above
	int UpdatePlotCust(resultsPanel* parent, bool FitPlotToData=true);	//this will ALMOSt be the same as above
	int FindHeader(wxString header);
	int CPlotLayer();
	PlotData();
	~PlotData();
};

class MyListBox : public wxListBox
{
public:
	MyListBox() {}
	MyListBox(wxWindow *parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxListBoxNameStr)
		: wxListBox(parent, id, pos, size, n, choices, style, validator, name) { }

	void OnKeyUp(wxKeyEvent& event);
private:
	DECLARE_EVENT_TABLE()
};

class resultsPanel : public wxPanel
{
public:
	resultsPanel(wxWindow* parent);
	~resultsPanel();
	
	void SizeRight();
	wxString GetFilename();

	void UpdatePlotBoundTxtBoxes(void);
	void LoadExistingSimulation(wxCommandEvent& event);
	void SetCustomPlotPlacement(int pos = CPLOT_DEFAULT);

	wxString fListfName;

	wxStaticBoxSizer* topSizer;
	wxBoxSizer* RVertSizer;
	wxBoxSizer* LVertSizer;
	wxBoxSizer* PlotCustomizeSizer;
	
	//	wxBoxSizer* CoordSizer;
	wxButton* LoadDataButton;
	wxButton* savePlotButton;
	//	mpScaleX* xaxis;
	//	mpScaleY* yaxis;
	//	wxStaticText* FileDisplay;
	wxButton* Btn_ClearFilteredFiles;


	wxStaticBoxSizer* DataSelectionBox;
	wxBoxSizer* GrpOutSizer;
	MyListBox* FileListBox;
	std::vector<int> ActiveFileIndices;	//These values point to what should go in the vector below
	std::vector<wxString> FullFilenames;	//what cpu sees to open file
	std::vector<wxString> AbbrFNames;	//what user sees and used in filter sorting of files
	wxBoxSizer* ActInactSizer;
	wxComboBox* ActivePlotData;
	wxComboBox* InPlotData;
	wxBoxSizer* AddRemSetSizer;
	wxButton* setX;
	wxButton* AddToActive;
	wxButton* RemoveFromActive;
	wxButton* RawPlotButton;
	wxButton* Btn_PlotCustom;
	wxComboBox* FilterFileGroup;	//usually _group_
	wxBoxSizer* iterSizer;
	wxStaticText* IterLabel;
	wxTextCtrl* FilterFileIteration;	// _###_
	wxComboBox* FilterFileType;	// file extension
	wxTextCtrl* fileFilters;
	wxSlider* FileSelectSlider;
	

	wxStaticBoxSizer* PenControl;
	wxBoxSizer* PenRGBControl;
	wxBoxSizer* PenStyleControl;
	wxBoxSizer* legendControl;
	wxStaticText* txtRGBA;
	wxTextCtrl* rPen;
	wxTextCtrl* gPen;
	wxTextCtrl* bPen;
	//	wxTextCtrl* aPen;
	wxComboBox* widthPen;
	wxComboBox* stylePen;
	wxStaticText* legText;
	wxTextCtrl* legendUpdate;

	wxFlexGridSizer* szr_plotLabels;
	wxTextCtrl* txt_XAxisName;
	wxTextCtrl* txt_YAxisName;
	wxStaticText* lbl_XName;
	wxStaticText* lbl_YName;
	wxCheckBox* Chk_labels;
	wxCheckBox* Chk_legend;
	wxSlider* Slider_titleX;
	wxSlider* Slider_titleY;
	wxStaticText* lbl_slidX;
	wxStaticText* lbl_slidY;
	wxTextCtrl* txt_PlotName;
	wxStaticText* lbl_PlotName;

	wxFlexGridSizer* ScaleSizer;
	wxTextCtrl* ScaleActYText;
	wxTextCtrl* OffsetActY;
	wxStaticText * txtOffset;
	wxStaticText* statTxtScaleY;
	wxBoxSizer* NormZeroSzr;
	wxButton* Btn_Normalize;
	wxButton* Btn_ToggleZeros;

	wxStaticBoxSizer* PlotControlSizer;	//get a box
	wxFlexGridSizer* PlControlFlexSizer;	//a 2D grid
	wxBoxSizer* PlControlSizerXLog;	//a 2D grid
	wxBoxSizer* PlControlSizerYLog;	//a 2D grid
	wxStaticText* xLabel;
	wxStaticText* yLabel;
	wxStaticText* xpnLogLabel;
	wxStaticText* ypnLogLabel;
	wxComboBox* PlotLogX;
	wxComboBox* PlotLogY;
	wxTextCtrl* xMin;
	wxTextCtrl* xMax;
	wxTextCtrl* yMin;
	wxTextCtrl* yMax;
	wxTextCtrl* nLogX;
	wxTextCtrl* pLogX;
	wxTextCtrl* nLogY;
	wxTextCtrl* pLogY;

	wxTextCtrl* RawOutput;


	wxString curOutputName;
	unsigned long fileIndex;

	ShowOutputs whichData;

	std::vector<PlotData> Plots;
	int CurPlotDisplay;	//which plot should be displayed currently (vector of Plots above)
	mpWindow* plot;	//use this as a pointer to ny number of the plots

	wxStaticBoxSizer *MergeManipSizer;
	wxGridSizer *CustomBtnSzr;	//this is the last pointer initialized. use it as a check to make sure everything is initialized in various functions
	mpScaleX* cstm_xaxis;
	mpScaleY* cstm_yaxis;
	mpWindow* cstm_plot;
	PlotData cstm_PlotData;
	wxButton* cstmBtn_AddVisible;
	wxButton* cstmBtn_AddActY;
	wxButton* cstmBtn_SavePlot;
	wxButton* cstmBtn_Remove;
	wxComboBox* cstm_Select;
	wxButton* cstm_Clear;
	

	int UpdateDisplay(unsigned int newPlot, bool forceReloadData = false);
	int RemoveFileFromList(wxString fname);
	
private:
	void LoadOutputFiles(wxCommandEvent& event);
	int LoadData(wxString fname, wxString header = wxEmptyString, bool force=false);
	
	int LoadBDPlot(wxString fname, bool force, PlotData* pl = NULL);
	int LoadMESHPlot(wxString fname, bool force,  PlotData* pl = NULL);
	int LoadQEPlot(wxString fname, bool force,  PlotData* pl = NULL);
	int LoadJVPlot(wxString fname, bool force,  PlotData* pl = NULL);
	int LoadRawPlot(wxString fname, bool force,  PlotData* pl = NULL);	//basically make a place holder, but there will be no plot to display!
	int LoadCPlot(wxString fname, bool force,  PlotData* pl = NULL);
	int LoadGenericPlot(wxString fname, bool force,  PlotData* pl=NULL);	//.csv, " " delimited, etc.

	int LoadBDPlot(PlotData* pl, wxString header);	//loads a particular header file in where the data hasn't been loaded yet
	int LoadJVPlot(PlotData* pl, wxString header);
	int LoadQEPlot(PlotData* pl, wxString header);
	int LoadMESHPlot(PlotData* pl, wxString header);
	

	bool CheckStringActiveBD(wxString& label, PlotData* pl = NULL);
	bool CheckStringActiveJV(wxString& label, PlotData* pl = NULL, bool incDependencies = false);
	bool CheckStringActiveQE(wxString& label, PlotData* pl = NULL);
	bool CheckStringActiveMESH(wxString& label, PlotData* pl = NULL);
	
	

	int LoadFList(wxString fname);
	int ClearPlots();
	unsigned int FindActiveFile(wxString fname);
	int FilterFiles(int SetValues = FFILES_SET_NONE, wxString which = wxEmptyString);
	wxSizer* resSizer;
	void FFiles(wxCommandEvent& event);
	void SliderChangeUpdate(wxScrollEvent& event);
	void SliderChangeNoUpdate(wxScrollEvent& event);
	void ListBoxSelect(wxCommandEvent& event);
	void VarSelected(wxCommandEvent& event);
	void UpdateLineColor(wxCommandEvent& event);
	void UpdateLineWidth(wxCommandEvent& event);
	void UpdateLineStyle(wxCommandEvent& event);
	void UpdateLegendText(wxCommandEvent& event);
	int SetPlot(unsigned int selection, bool updateRestGUI);
	void UpdatePlotBounds(wxCommandEvent& event);
	void UpdatePlotBoundTxtBoxesEvt(wxCommandEvent& event);
	
	void MouseReleaseUpdate(wxMouseEvent &event);
	int SwapPlotDisplayIndex(unsigned int select, bool updateRestGUI = true);
	int SwapPlotExactIndex(unsigned int select, bool updateRestGUI = true);
	int UpdateDataDropDowns(unsigned int exactIndex = -1);
	void ToggleRawInput(wxCommandEvent& event);
	int LoadDataIntoRawOut(wxString str, bool byFile=true);
	void ChangeXYPlot(wxCommandEvent& event);
	void RemoveVarPlot(wxCommandEvent& event);
	void AddVarPlot(wxCommandEvent& event);
	int SetShowOutputs(wxString header, bool value, ShowOutputs* outs = NULL);
	void EnableDefaultPlotControls(void);
	void DisableDefaultPlotControls(void);
	int FindDisplayFromExact(int exact);
	void SaveCurrentPlot(wxCommandEvent& event);
	void NormalizeActY(wxCommandEvent& event);
	void TxtChangeScaleY(wxCommandEvent& event);
	void TxtChangeOffsetY(wxCommandEvent& event);
	void BtnToggleZeroes(wxCommandEvent& event);
	void BtnShowCustomPlot(wxCommandEvent& event);
	void ClearFilteredFiles(wxCommandEvent & event);
	void UpdateInactiveToolTip(wxCommandEvent & event);
	void CPlot_AddActive(wxCommandEvent & event);
	void CPlot_AddVisible(wxCommandEvent & event);
	void CPlot_Save(wxCommandEvent & event);
	void CPplot_Select(wxCommandEvent & event);
	void CPlot_Remove(wxCommandEvent & event);
	void CPlot_Clear(wxCommandEvent & event);
	int CPlot_RemoveDuplicateX(void);

	void PlotTitle(wxCommandEvent & event);
	void PlotTitlePos(wxScrollEvent & event);
	void PlotXAxisTitle(wxCommandEvent & event);
	void PlotYAxisTitle(wxCommandEvent & event);
	void PlotShowLegends(wxCommandEvent & event);
	void PlotShowLabels(wxCommandEvent & event);
	

	std::vector<std::vector<double> > data;

	DECLARE_EVENT_TABLE()
};


double find_min_value_nested(std::vector<std::vector<double> > vec);
double find_max_value_nested(std::vector<std::vector<double> > vec);
double find_min_value(std::vector<double> vec);
bool find_maxmin_value_nested(PlotData* pl, double& max, double& min);
bool find_maxmin_value(std::vector<double>& vec, double& max, double& min);
double find_max_value(std::vector<double>& vec);

#endif