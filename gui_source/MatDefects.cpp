#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "MatDefects.h"
#include "MatPanel.h"
#include "MatOpticalPanel.h"

#include "../kernel_source/light.h"
#include "../kernel_source/model.h"
#include "../kernel_source/load.h"
#include "../kernel_source/Materials.h"

#include "GUIHelper.h"


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

extern double scalarTEN(int index);


devDefectsPanel::devDefectsPanel(wxWindow* parent)
	: wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL), curDefect(nullptr)
{
	wxArrayString defectChoices;
	//defectChoices.Add(wxT("Choice 1"));
	///defectChoices.Add(wxT("Choice 2"));
	defectChooser = new wxComboBox(this, SWITCH_SELECTION, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, defectChoices, wxCB_DROPDOWN | wxCB_READONLY);
	defAdd = new wxButton(this, wxID_NEW, wxT("New Defect"));	//these are standards that come with standard text
	defRem = new wxButton(this, wxID_REMOVE), wxT("Delete");
	defSave = new wxButton(this, wxID_SAVEAS, wxT("Save"));
	defLoad = new wxButton(this, DEF_LOAD, wxT("Load"));
	defDuplicate = new wxButton(this, DEF_DUPLICATE, wxT("Duplicate"));
	defRename = new wxButton(this, DEF_RENAME, wxT("Rename"));
	

	wxArrayString reference;
	reference.Add(wxT("CB (Donor)"));
	reference.Add(wxT("VB (Acceptor)"));
	referenceBox = new wxRadioBox(this, DEF_SMART_REF_CHANGE, wxT("Reference"), wxPoint(0, 30), wxDefaultSize, reference, 1, wxRA_SPECIFY_COLS);
	referenceBox->SetToolTip(wxT("Is the defect a donor and referenced to the conduction band, or an acceptor and referenced to the valence band?"));
	wxArrayString depth;
	depth.Add(wxT("Shallow"));
	depth.Add(wxT("Deep"));
	depthBox = new wxRadioBox(this, DEF_UPDATE_RAD, wxT("Depth"), wxPoint(0, 100), wxDefaultSize, depth, 1, wxRA_SPECIFY_COLS);
	depthBox->SetToolTip(wxT("Generally, shallow defects are near the band and trace along with it, while deep defects are usually mid gap. Deep defects don't fit in with the atomic matrix well, and their energy is therefore more independent from the bands regardless of bending. This option currently has zero influence on a simulation as everything is treated as shallow. Some day, that might change."));
	depthBox->Select(0);
	depthBox->Enable(1, false);
//	depthBox->GetToolTip()->SetAutoPop(30000);

	wxArrayString energyDist;
	energyDist.Add(wxT("Discrete"));
	energyDist.Add(wxT("Banded"));
	energyDist.Add(wxT("Gaussian"));
	energyDist.Add(wxT("Exponential"));
	energyDist.Add(wxT("Linear"));
	energyDist.Add(wxT("Parabolic"));
	distBox = new wxRadioBox(this, DEF_CHANGEDIST, wxT("Energy Distribution"), wxPoint(0, 30), wxDefaultSize, energyDist, 1, wxRA_SPECIFY_COLS);
	distBox->SetToolTip(wxT("Set the function type defining how the defect's density of states will be distributed."));

	defEnergy = new wxStaticText(this, wxID_ANY, wxT("Energy:"));
	defEnergy->SetToolTip(wxT("This is the energy (eV) of the defect reference from the bands, with positive values moving into the band gap. This value is either the average or maximum energy"));
	defEn = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	defVariation = new wxStaticText(this, wxID_ANY, wxT("Variation:"));
	defVariation->SetToolTip(wxT("This is the variation in energy (eV) defining the defect. This will either be a half-width of sorts or distance until the density of states goes to zero."));
	defVar = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	defDensity = new wxStaticText(this, wxID_ANY, wxT("Density:"));
	defDensity->SetToolTip(wxT("The volume density (cm^-3) of the defect throughout the material. This is a constant concentration always present whenver this material is in use."));
	defDensityBox = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	//wxStaticText* material = new wxStaticText(this, wxID_ANY, wxT("Material Element: "));
	//wxTextCtrl* matID = new wxTextCtrl(this, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxEXPAND);
	sigman = new wxStaticText(this, wxID_ANY, wxT("\u03C3_n:"));
	sigman->SetToolTip(wxT("Capture cross section to capture electrons from the conduction band. cm^2/(V s)"));
	sigmanTxt = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	sigmap = new wxStaticText(this, wxID_ANY, wxT("\u03C3_p:"));
	sigmap->SetToolTip(wxT("Capture cross section to capture holes from the valence band. cm^2/(V s)"));
	sigmapTxt = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	//wxStaticText* impDegen = new wxStaticText(this, wxID_ANY, wxT("degeneracy:"));
	//wxTextCtrl* impDeg = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	
	
	
	explainDistribution = new wxStaticText(this, wxID_ANY, wxT("Select a distribution to get an explanation of how it works."));
	explainDistribution->SetToolTip(wxT("Select from the energy distribution to change this message."));

	defDegeneracy = new wxStaticText(this, wxID_ANY, wxT("Degeneracy:"));
	defDegeneracy->SetToolTip(wxT("A carrier may take on multiple equivalent states. When the fermi function is applied with this taken into account, the resulting occupation probability resembles a fermi function \
with a slight adjustment factor. While other effects such as electron shielding influence this value, donors generally have a value of (2) and acceptors a value of (4). Donors generally have an electron able to go into \
a completely empty s orbital, having 2 options. Acceptors are usually bonding in sp3 orbitals, and the hole could enter 1 of 4 equivalent states. Note this is only a general rule of thumb, and will vary depending on the underlying crystal structure."));
	defDegeneracy->GetToolTip()->SetAutoPop(60000);
	defDegen = new wxTextCtrl(this, DEF_UPDATE_TXT, wxT("2"), wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	defCharge = new wxStaticText(this, wxID_ANY, wxT("Charge:"));
	defCharge->SetToolTip(wxT("The charge on the defect when it is not occupied by its preferred carrier. In general, for donors, the charge is (+1) so when occupied by an electron, it is neutral. For acceptors, it should generally be (-1) as when occupied with a hole, it becomes a neutral charge."));
	defChargeTxt = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	
	txtOptEffCB = new wxStaticText(this, wxID_ANY, wxT("CB Optical \u03B7:"));
	txtOptEffCB->SetToolTip(wxT("Undergoing Shockley-Read-Hall recombination, light is emitted during electron capture from the conduction band into this defect. Set the efficiency (0-1). Note, if the energy is close to the band, this should be 0 as phonons will dominate the energy dissipation."));
	defOptEffCB = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	txtOptEffVB = new wxStaticText(this, wxID_ANY, wxT("VB Optical \u03B7:"));
	txtOptEffVB->SetToolTip(wxT("Undergoing Shockley-Read-Hall recombination, light is emitted during hole capture from the valence band into this defect. Set the efficiency (0-1). Note, if the energy is close to the band, this should be 0 as phonons will dominate the energy dissipation."));
	defOptEffVB = new wxTextCtrl(this, DEF_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	
	
	defEnergyExplain = new wxBoxSizer(wxVERTICAL);
	defEnergyRBoxInp = new wxBoxSizer(wxHORIZONTAL);
	defRefDep = new wxBoxSizer(wxVERTICAL);
	defopenSpace = new wxBoxSizer(wxVERTICAL);
	defEnergySide = new wxBoxSizer(wxHORIZONTAL);
	defBar = new wxBoxSizer(wxHORIZONTAL);
	defSizer = new wxBoxSizer(wxVERTICAL);
	defEnergyInputs = new wxFlexGridSizer(2, 2, 10, 10);
	defMiscInputs = new wxFlexGridSizer(2, 10, 10);
	

	defMiscInputs->SetFlexibleDirection(wxBOTH);
	defMiscInputs->AddGrowableCol(1, 1);

	defMiscInputs->Add(defDensity, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(defDensityBox, 1, wxEXPAND, 0);
	defMiscInputs->Add(sigman, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(sigmanTxt, 1, wxEXPAND, 0);
	defMiscInputs->Add(sigmap, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(sigmapTxt, 1, wxEXPAND, 0);
	defMiscInputs->Add(defDegeneracy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(defDegen, 1, wxEXPAND, 0);
	defMiscInputs->Add(defCharge, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(defChargeTxt, 1, wxEXPAND, 0);
	defMiscInputs->Add(txtOptEffCB, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(defOptEffCB, 1, wxEXPAND, 0);
	defMiscInputs->Add(txtOptEffVB, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defMiscInputs->Add(defOptEffVB, 1, wxEXPAND, 0);

	defBar->Add(defectChooser, 1, wxALL, 5);
	defBar->Add(defAdd, 0, wxALL, 5);
	defBar->Add(defRem, 0, wxALL, 5);
	defBar->Add(defSave, 0, wxALL, 5);
	defBar->Add(defLoad, 0, wxALL, 5);
	defBar->Add(defDuplicate, 0, wxALL, 5);
	defBar->Add(defRename, 0, wxALL, 5);

	defEnergyInputs->SetFlexibleDirection(wxBOTH);
	defEnergyInputs->AddGrowableCol(1, 1);
	defEnergyInputs->Add(defEnergy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defEnergyInputs->Add(defEn, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);
	defEnergyInputs->Add(defVariation, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	defEnergyInputs->Add(defVar, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);
	
	defEnergyExplain->Add(defEnergyInputs, 0, wxALL, 5);
	defEnergyExplain->Add(explainDistribution, 1, wxALL | wxEXPAND, 5);

	defRefDep->Add(referenceBox, 0, wxALL | wxEXPAND | wxALIGN_TOP, 0);
	defRefDep->AddStretchSpacer();
	defRefDep->Add(depthBox, 0, wxALL | wxEXPAND | wxALIGN_BOTTOM, 0);

	defEnergyRBoxInp->Add(defRefDep, 0, wxALL | wxEXPAND, 2);
	defEnergyRBoxInp->Add(distBox, 0, wxALL, 2);
	defEnergyRBoxInp->Add(defEnergyExplain, 1, wxALL | wxEXPAND, 2);

	defopenSpace->Add(defEnergyRBoxInp, 0, wxALL | wxEXPAND, 2);
	defopenSpace->AddStretchSpacer();

	defEnergySide->Add(defopenSpace, 1, wxALL | wxALIGN_TOP | wxEXPAND, 2);
	defEnergySide->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND, 0);
	defEnergySide->Add(defMiscInputs, 0, wxALL | wxALIGN_RIGHT | wxALIGN_TOP | wxEXPAND, 2);
	
	defSizer->Add(defBar, 0, wxALL | wxEXPAND, 2);
	defSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	defSizer->Add(defEnergySide, 1, wxALL | wxEXPAND, 2);

	explainDistribution->Wrap(defEnergyInputs->GetMinSize().GetWidth());

	prevSelection = -1;
	matSelection = -1;
}

devDefectsPanel::~devDefectsPanel()
{
	defectChooser->Clear();

	delete defectChooser;
	delete referenceBox;
	delete depthBox;
	
	delete sigmanTxt;
	delete sigmapTxt;
	delete defChargeTxt;
	delete defEn;
	delete defVar;
	delete distBox;
	delete defDensityBox;

	delete defAdd;
	delete defRem;
	delete defSave;
	delete defLoad;
	delete defRename;
	
	delete sigman;
	delete sigmap;
	delete defCharge;
	delete defEnergy;
	delete defVariation;

	delete defDensity;
}

ModelDescribe* devDefectsPanel::getMDesc()
{
	return(((devMaterialPanel*)GetParent()->GetParent())->getMdesc());
}

devMaterialPanel* devDefectsPanel::getMyClassParent()
{
	return((devMaterialPanel*)GetParent()->GetParent());
}

Material* devDefectsPanel::getMat()
{
	return(getMyClassParent()->getCurrentMaterial());
}

void devDefectsPanel::SizeRight()
{
	SetScrollRate(20, 20);
	SetSizer(defSizer);
}

void devDefectsPanel::VerifyInternalVars() {
	ModelDescribe *mdesc = getMDesc();
	if (mdesc == nullptr)
	{
		curDefect = nullptr;
		return;
	}
	if (mdesc->HasImpurity(curDefect) == false)
		curDefect = nullptr;
}

void devDefectsPanel::NewDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;
	if (mat == nullptr)
	{
		wxMessageDialog diag(this, wxT("Please select/create a material first"), wxT("No Material Selected"));
		diag.ShowModal();
		return; //empty
	}
	
	wxTextEntryDialog entry(this, wxT("Enter the defect's name"));
	if (entry.ShowModal() != wxID_OK)
		return;
	
	Impurity* imp = new Impurity(-1, entry.GetValue().ToStdString());
	mat->pairWithDefect(imp);	//gets defect links and adds to mdesc if not already exist
	
	mdesc->SetUnknownIDs();
	curDefect = imp;
	TransferToGUI();
//	UpdateGUIValues();
}

void devDefectsPanel::LoadDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	ModelDescribe* mdesc = getMDesc();
	if (mat == nullptr || mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Defect files (*.defect)|*.defect"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	
	//don't get rid of current wavelengths! Let them be added on. If they

	Impurity* def = new Impurity();
	def->LoadFromFile(file);
	mat->pairWithDefect(def);

//	LoadDefectChildren(file, def, *mdesc);
	def->setID(-1);
	
	//when loaded via above, it is inverted for the model. Needs to be converted for the calculation
	def->adjustDonorDegeneracyForOccupationProbability();
	
	mdesc->SetUnknownIDs();
	curDefect = def;

	mdesc->Validate();

	UpdateGUIValues();
	TransferToGUI();
	/*

	wxTextFile* iFile = new wxTextFile(openDialog.GetPath());
	iFile->Open();
	std::vector<XMLLine> tags;
	parseFile(iFile, tags);
	if (tags.empty())
	{ //Not an XML file
		wxMessageDialog notXMLDiag(this, wxT("There do not appear to be any XML tags in this file"), wxT("Invalid File"));
		notXMLDiag.ShowModal();
	}
	int toptag = 0;
	if (!tags[0].label.IsSameAs(wxT("Defect"), false))
	{ //We don't have a material tag as our first tag
		toptag = tags[0].FindDescendent(wxT("Defect"), &tags);
		//find a material tag if it exists
		if (toptag < 0)
		{ //No material tags in the file. Notify the user
			wxMessageDialog noMatDiag(this, wxT("There are no defects in this XML file!"), wxT("No Defects in XML File"));
			noMatDiag.ShowModal();
		}
	}

	int count = 0;

	while (toptag >= 0)
	{
		defect def; //Don't load the ID from file
		def.accdon = tags[toptag].FindChild(wxT("donorlike"), &tags) > 0 ? true : false;
		def.reference = tags[toptag].ChildValue(wxT("type"), wxT("0"), &tags).IsSameAs(wxT("0")) ? 0 : 1;
		unsigned long tempholder;
		if (!tags[toptag].ChildValue(wxT("distribution"), wxT("0"), &tags).ToULong(&(tempholder)))
		{
			def.distribution = 0;
		}
		else if (tempholder > MAX_DISTRIBUTION)
		{
			def.distribution = 0;
		}
		else
		{
			def.distribution = (char)tempholder;
		}

		def.density = tags[toptag].ChildValue(wxT("density"), wxEmptyString, &tags);
		def.energy = tags[toptag].ChildValue(wxT("energy"), wxEmptyString, &tags);
		def.width = tags[toptag].ChildValue(wxT("width"), wxEmptyString, &tags);

		def.depth = tags[toptag].ChildValue(wxT("type"), wxT("shallow"), &tags).IsSameAs(wxT("shallow")) ? 0 : 1;

		def.sigN = tags[toptag].ChildValue(wxT("sigN"), wxEmptyString, &tags);
		def.sigP = tags[toptag].ChildValue(wxT("sigP"), wxEmptyString, &tags);

		def.name = wxT("Defect");

		materials_list[matSelection].defects.push_back(def);
		defectChooser->Append(def.name);

		toptag = tags[toptag].FindLastChild(&tags);
		if (toptag < 0 || toptag >= (signed int)(tags.size()) - 1) break;
		while (!tags[++toptag].label.IsSameAs(wxT("Defect"), false))
		{
			if (toptag >= (signed int)(tags.size()) - 1)
			{
				toptag = -1;
				break;
			}
		}
	}
	tags.clear();
	//	delete tags;
	iFile->Close();
	delete iFile;
	if (count > 0)
	{
		defectChooser->SetSelection(defectChooser->GetCount() - 1);
		wxCommandEvent eventBlank;
		SwitchDefect(eventBlank);
	}
	*/
	return;
}


void devDefectsPanel::UpdateGUIValues()
{
	if (!isInitialized())
		return;
	Material* mat = getMat();

	if (mat == nullptr)
	{
		sigmanTxt->Disable();
		sigmapTxt->Disable();
		defChargeTxt->Disable();
		defDegen->Disable();
		defDensityBox->Disable();
		defEn->Disable();
		defVar->Disable();
		defOptEffCB->Disable();
		defOptEffVB->Disable();
		defectChooser->Disable();
		
		referenceBox->Disable();
		depthBox->Disable();
		distBox->Disable();

		defAdd->Disable();
		defRem->Disable();
		defSave->Disable();
		defLoad->Disable();
		defDuplicate->Disable();
		defRename->Disable();
		explainDistribution->SetLabel(wxT("First select or create a material to add defects to!"));

		defSizer->Hide(defEnergySide);
	//	defSizer->Layout();
	}
	else if (curDefect == nullptr)
	{
		explainDistribution->SetLabel(wxT("Select or create a defect to adjust its values!"));

		sigmanTxt->Disable();
		sigmapTxt->Disable();
		defChargeTxt->Disable();
		defDegen->Disable();
		defDensityBox->Disable();
		defEn->Disable();
		defVar->Disable();
		defOptEffCB->Disable();
		defOptEffVB->Disable();

		referenceBox->Disable();
		depthBox->Disable();
		distBox->Disable();

		defAdd->Enable();
		defRem->Disable();
		defSave->Disable();
		defLoad->Enable();
		defDuplicate->Disable();
		defRename->Disable();

		defSizer->Hide(defEnergySide);
	//	defSizer->Layout();
		defectChooser->SetToolTip(wxT("Select a defect to view/edit"));
	}
	else
	{
		sigmanTxt->Enable();
		sigmapTxt->Enable();
		defChargeTxt->Enable();
		defDegen->Enable();
		defDensityBox->Enable();
		defEn->Enable();
		defectChooser->Enable();

		referenceBox->Enable();
		depthBox->Enable();
		depthBox->Enable(1, false);
		distBox->Enable();
		defAdd->Enable();
		defRem->Enable();
		defSave->Enable();
		defLoad->Enable();
		defDuplicate->Enable();
		defRename->Enable();

		if(curDefect->isEnergyDiscrete())
			defVar->Disable();
		else
			defVar->Enable();
	
		
		if (mat->getOpticalData().isEmissionsEnabled())
		{
			defOptEffCB->Enable();
			defOptEffVB->Enable();
		}
		else //there either are no optical data, or emissions are disabled
		{
			defOptEffCB->Disable();
			defOptEffVB->Disable();
			ValueToTBox(defOptEffCB, 0.0);	//just ensure that it is 0 when sent to
			ValueToTBox(defOptEffVB, 0.0);	//mdesc.
		}

		defectChooser->SetToolTip(curDefect->getName());
		defSizer->Show(defEnergySide, true, true);
		defSizer->Layout();
	}
	if (mat && mat->hasDefects())
		defectChooser->Enable();
	else
		defectChooser->Disable();
	
	
//	explainDistribution->Wrap(defEnergyInputs->GetMinSize().GetWidth());
	
}

void devDefectsPanel::ChangeDistributionType(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	/*
	for reference

	int which = curDefect->distribution;
	switch (which)
	{
	case DISCRETE:
		distBox->SetSelection(0);
		break;
	case BANDED:
		distBox->SetSelection(1);
		break;
	case GAUSSIAN:
		distBox->SetSelection(2);
		break;
	case EXPONENTIAL:
		distBox->SetSelection(3);
		break;
	case LINEAR:
		distBox->SetSelection(4);
		break;
	case PARABOLIC:
		distBox->SetSelection(5);
		break;
	default:
		distBox->SetSelection(0);
	}
	//distBox->SetToolTip(wxT("Discrete (1 level, no variation\n
	Banded (states split into 10 levels from E-1/2Var to E+1/2Var)\n
	Gaussian (Center @ E, variation is distance to half-width\n
	discretized to 2x the variation in both directions)\n
	Exponential (full power @ E, 1/2 power @ E+Var, discretized out\n
	to 95% of the distribution area, then\n
	# of states scaled up by 1/0.95)\n
	Linear (Starts @ E, goes to 0 states @ E+Var linearly)
	\nParabolic(Max @ E, zeros out @ E�1/2Var)"));
	*/
	int selection = distBox->GetSelection();
	switch (selection)
	{
	case 1:
		curDefect->setEnergyBanded();
		explainDistribution->SetLabel(wxT("Energy levels evenly distributed from (E-0.5 var) to (E+0.5 var). Amount of discretization varies as simulation parameter."));
		break;
	case 2:
		curDefect->setEnergyGaussian();
//		explainDistribution->SetLabel(wxT("Gaussian centered on Energy. Note, the variation is NOT the standard deviation. It is the distance required for the magnitude to cut in half of it's maximal value. The distribution is extended twice this distance on both sides to where it is ~6.25% and then abruptly cut off beyond. The discretization is then renormalized to integrate to an area of 1."));
		explainDistribution->SetLabel(wxT("Gaussian centered on energy. Variation is distance to half magnitude. Gaussian cut off after 2x Variation (~6.25% magnitude). Renormalized."));
		break;
	case 3:
		curDefect->setEnergyExponential();
//		explainDistribution->SetLabel(wxT("An exponential distribution with energy occuring at the maximum value. The variation is the distance for the magnitude to cut in half. It is extended out to capture 95% of the distribution, and then the discretization is renormalized to prevent losses."));
		explainDistribution->SetLabel(wxT("Exponential distribution maxed at Energy. Variation is distance to half magnitude. Extends to capture 95%, and is then renormalized."));
		break;
	case 4:
		curDefect->setEnergyLinear();
		explainDistribution->SetLabel(wxT("The linear distribution has a maximum at energy. It varies linearly up to variation away, at which point it hits zero. Normalized appropriately."));
		break;
	case 5:
		curDefect->setEnergyParabolic();
		explainDistribution->SetLabel(wxT("The parabolic distribution is maximal at energy, and zero at (E - 0.5var) and (E + 0.5var). Normalized appropriately."));
		break;
	case 0:
	default:
		curDefect->setEnergyDiscrete();
		explainDistribution->SetLabel(wxT("One discrete energy level. Nominally zero width (occasionally 1meV to prevent divide by 0)"));
		break;
	}
	explainDistribution->Wrap(defEnergyInputs->GetMinSize().GetWidth());
//	UpdateGUIValues();
}

void devDefectsPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	Material* mat = getMat();
	if (mat && curDefect == nullptr && mat->hasDefects())
		curDefect = mat->getDefectByIndex(0);

	if (mat == nullptr)
	{
		//there is no material. EVERYTHING is cleared out. Nothing is known
		sigmanTxt->Clear();
		sigmapTxt->Clear();
		defChargeTxt->Clear();
		defDegen->Clear();
		defDensityBox->Clear();
		defEn->Clear();
		defVar->Clear();
		defOptEffCB->Clear();
		defOptEffVB->Clear();
		defectChooser->Clear();
		UpdateGUIValues();
		return;
	}
	else if (curDefect == nullptr)
	{
		//there is a material, but there is no defect currently selected.
		//clear everything, but leave the defects to be chosen
		sigmanTxt->Clear();
		sigmapTxt->Clear();
		defChargeTxt->Clear();
		defDegen->Clear();
		defDensityBox->Clear();
		defEn->Clear();
		defVar->Clear();
		defOptEffCB->Clear();
		defOptEffVB->Clear();

	}
	else
	{
		//pull the data out of the defect!
		
		ValueToTBox(sigmanTxt, curDefect->getElecCapture());
		ValueToTBox(sigmapTxt, curDefect->getHoleCapture());
		ValueToTBox(defChargeTxt, curDefect->getCharge());
		ValueToTBox(defDegen, curDefect->getDegeneracy());
		ValueToTBox(defDensityBox, curDefect->getDensity());
		ValueToTBox(defEn, curDefect->getRelativeEnergy());
		ValueToTBox(defVar, curDefect->getEnergyVariation());
		ValueToTBox(defOptEffCB, curDefect->getOpticalEfficiencyCB());
		ValueToTBox(defOptEffVB, curDefect->getOpticalEfficiencyVB());
		
		if (curDefect->isShallow())
			depthBox->SetSelection(0);	//corresponds to order of radio box
		else
			depthBox->SetSelection(1);

		if (curDefect->isCBReference())
			referenceBox->SetSelection(0);
		else
			referenceBox->SetSelection(1);

		int which = curDefect->getEnergyDistribution();
		switch (which)
		{
		case Impurity::banded:
			distBox->SetSelection(1);
			break;
		case Impurity::gaussian:
			distBox->SetSelection(2);
			break;
		case Impurity::exponential:
			distBox->SetSelection(3);
			break;
		case Impurity::linear:
			distBox->SetSelection(4);
			break;
		case Impurity::parabolic:
			distBox->SetSelection(5);
			break;
		case Impurity::discrete:
		default:
			distBox->SetSelection(0);
		}
		/*
		energyDist.Add(wxT("Discrete"));
		energyDist.Add(wxT("Banded"));
		energyDist.Add(wxT("Gaussian"));
		energyDist.Add(wxT("Exponential"));
		energyDist.Add(wxT("Linear"));
		energyDist.Add(wxT("Parabolic"));
		*/
	}

	defectChooser->Clear();
	bool defGood = false;
	for (unsigned int i = 0; i < mat->getNumDefects(); ++i)
	{
		defectChooser->AppendString(mat->getDefectByIndex(i)->getName());
		if (mat->getDefectByIndex(i) == curDefect)
		{
			defectChooser->SetSelection(i);
			defGood = true;
		}
	}
	if (!defGood)
		curDefect = nullptr;

	UpdateGUIValues();
}

void devDefectsPanel::UpdateReference(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int tmpInt = referenceBox->GetSelection();
	if (tmpInt == 0)
		curDefect->setDonor();
	else
		curDefect->setAcceptor();

	if (curDefect->isDonor())
	{
		
		curDefect->setDegeneracy(2.0f);
		curDefect->setCharge(1);
		curDefect->setOpticalEfficiencyCB(0.0);
		curDefect->setOpticalEfficiencyVB(1.0);
	}
	else
	{
		curDefect->setDegeneracy(4.0f);
		curDefect->setCharge(-1);
		curDefect->setOpticalEfficiencyCB(1.0);
		curDefect->setOpticalEfficiencyVB(0.0);
	}

	ValueToTBox(defChargeTxt, curDefect->getCharge());
	ValueToTBox(defDegen, curDefect->getDegeneracy());
	ValueToTBox(defOptEffCB, curDefect->getOpticalEfficiencyCB());
	ValueToTBox(defOptEffVB, curDefect->getOpticalEfficiencyVB());

}

void devDefectsPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curDefect == nullptr)
		return;

	double tmpDouble;
	long int tmpInt;
	
	curDefect->setElecCapture((sigmanTxt->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setHoleCapture((sigmapTxt->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setCharge((defChargeTxt->GetValue().ToLong(&tmpInt)) ? tmpInt : 0);
	curDefect->setRelativeEnergy((defEn->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setEnergyVariation((defVar->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setDegeneracy((defDegen->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 1.0);
	curDefect->setOpticalEfficiencyCB((defOptEffCB->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setOpticalEfficiencyVB((defOptEffVB->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curDefect->setDensity((defDensityBox->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);

	

	tmpInt = referenceBox->GetSelection();
	if (tmpInt == 0)
		curDefect->setDonor();
	else
		curDefect->setAcceptor();

	tmpInt = depthBox->GetSelection();
	if (tmpInt == 0)
		curDefect->setShallow();
	else
		curDefect->setDeep();

	tmpInt = distBox->GetSelection();

	switch (tmpInt)
	{
	case 1:
		curDefect->setEnergyBanded();
		break;
	case 2:
		curDefect->setEnergyGaussian();
		break;
	case 3:
		curDefect->setEnergyExponential();
		break;
	case 4:
		curDefect->setEnergyLinear();
		break;
	case 5:
		curDefect->setEnergyParabolic();
		break;
	case 0:
	default:
		curDefect->setEnergyDiscrete();
		break;
	}

	UpdateGUIValues();
}

void devDefectsPanel::RemoveDefect(wxCommandEvent& event)
{
	if (!isInitialized() || curDefect == nullptr)
		return;
	Material* mat = getMat();
	if (mat == nullptr)
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	int index = defectChooser->GetSelection();	//this is going to go along with material defects
	wxString deleted = defectChooser->GetStringSelection();
	std::string defName = deleted.ToStdString();

	//delete from mdesc, remove from mdesc, remove from mat
	mat->unpairWithDefect(curDefect);
	mdesc->removeImpurity(curDefect);	//also deletes from memory
	//so reassign curDefect
	if (index < (int)mat->getNumDefects())
		curDefect = mat->getDefectByIndex(index);
	else {
		index = (int)mat->getNumDefects() - 1;
		curDefect = (index >= 0) ? mat->getDefectByIndex(index) : nullptr;
	}

	mdesc->Validate();

	TransferToGUI();
	UpdateGUIValues();

}

void devDefectsPanel::duplicateDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curDefect == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	Material* mat = getMat();
	if (mdesc == nullptr || mat == nullptr)
		return;

	wxString newname(curDefect->getName());
	newname << "_copy";
	wxTextEntryDialog getName(this, wxT("Enter a new name"), wxT("Duplicate Defect"), newname);
	if (getName.ShowModal() == wxID_CANCEL)
		return;

	Impurity* def = new Impurity(*curDefect);
	def->setName(getName.GetValue().ToStdString());
	def->setID(-1);
	mat->pairWithDefect(def);

	mdesc->SetUnknownIDs();
	curDefect = def;
	TransferToGUI();
}

void devDefectsPanel::Clear()
{
	if (!isInitialized())
		return;
	defDensityBox->SetValue(wxEmptyString);
	sigmanTxt->SetValue(wxEmptyString);
	sigmapTxt->SetValue(wxEmptyString);
	defChargeTxt->SetValue(wxEmptyString);
	defEn->SetValue(wxEmptyString);
	defVar->SetValue(wxEmptyString);
	defDegen->SetValue(wxEmptyString);
	defOptEffCB->SetValue(wxEmptyString);
	defOptEffVB->SetValue(wxEmptyString);
}


void devDefectsPanel::SwitchDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	if (mat == nullptr)
		return;	//gotta have a material if we're switching to it's defect
	int newSelection = defectChooser->GetSelection();
	std::string defName = defectChooser->GetValue().ToStdString();

	Impurity* testdef = nullptr;
	if (newSelection < (int)mat->getNumDefects()) {
		testdef = mat->getDefectByIndex(newSelection);
		if (!testdef->isImpurity(defName))
			testdef = nullptr;
	}
	
	//could not find it easily!
	if (testdef == nullptr)
		testdef = mat->getDefectByName(defName);

	if (testdef != curDefect)
	{
		curDefect = testdef;
		TransferToGUI();	//get all the correct data in there
		ChangeDistributionType(event);
		UpdateGUIValues();	//and then update everything appropriately
	}
}

void devDefectsPanel::RenameDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curDefect == nullptr)
		return;

	wxTextEntryDialog getName(this, wxT("Name"), wxT("Rename Defect"), curDefect->getName());
	if (getName.ShowModal() == wxID_CANCEL)
		return;
	std::string newname = getName.GetValue();
	if (newname != "")
		curDefect->setName(newname);

	TransferToGUI();
	return;
}

void devDefectsPanel::saveDefect(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (defectChooser->IsListEmpty() || defectChooser->GetSelection() == -1)
	{
		wxMessageDialog diag(this, wxT("There are no defects to save!"), wxT("No Defects"));
		diag.ShowModal();
		return; //empty
	}
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Defect files (*.defect)|*.defect"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;
	
	if (curDefect)
	{
		XMLFileOut oFile(saveDialog.GetPath().ToStdString());
		if (oFile.isOpen())
		{
			curDefect->SaveToXML(oFile);
			oFile.closeFile();
		}
	}
	/*
	int defectNum = defectChooser->GetSelection();
	wxFile* oFile = new wxFile(saveDialog.GetPath(), wxFile::write);
	if (oFile->IsOpened())
		defectToFile(&(defects_list[defectNum]), oFile);
	oFile->Flush();
	delete oFile;
	*/
}

BEGIN_EVENT_TABLE(devDefectsPanel, wxScrolledWindow)
EVT_BUTTON(wxID_NEW, devDefectsPanel::NewDefect)
EVT_BUTTON(wxID_REMOVE, devDefectsPanel::RemoveDefect)
EVT_BUTTON(wxID_SAVEAS, devDefectsPanel::saveDefect)

EVT_BUTTON(DEF_LOAD, devDefectsPanel::LoadDefect)
EVT_BUTTON(DEF_DUPLICATE, devDefectsPanel::duplicateDefect)
EVT_BUTTON(DEF_RENAME, devDefectsPanel::RenameDefect)
EVT_RADIOBOX(DEF_CHANGEDIST, devDefectsPanel::ChangeDistributionType)

EVT_COMBOBOX(SWITCH_SELECTION, devDefectsPanel::SwitchDefect)

EVT_TEXT(DEF_UPDATE_TXT, devDefectsPanel::TransferToMdesc)
EVT_RADIOBOX(DEF_UPDATE_RAD, devDefectsPanel::TransferToMdesc)
EVT_RADIOBOX(DEF_SMART_REF_CHANGE, devDefectsPanel::UpdateReference)
END_EVENT_TABLE()