#ifndef MAT_ELECTRIC_PANEL_H
#define MAT_ELECTRIC_PANEL_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/grid.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/textfile.h"
#include "wx/colordlg.h"
#include "wx/colourdata.h"
#include "wx/textdlg.h"
#include "wx/dynarray.h"
#endif

class devMaterialPanel;
class Material;
class ModelDescribe;


class devMatElectricPanel : public wxScrolledWindow
{
public:
	devMatElectricPanel(wxWindow* parent);
	~devMatElectricPanel();
	void SizeRight();
	void Clear();

	void TransferToMDesc(wxCommandEvent& evt);
	void UpdateText(wxCommandEvent& evt);
	void TransferToGUI();
	void UpdateGUIValues();
	void VerifyInternalVars() { }	//no internal vars to check

	void EnableMat(bool enable);

	ModelDescribe* getMDesc();
	devMaterialPanel* getMyClassParent();

private:
	Material* getMat();

	wxSizer *matContentSizer;
	wxTextCtrl* description;
	wxRadioBox* materialType;
	wxTextCtrl* permittivity;
	wxTextCtrl* intrinsicCar;
	wxTextCtrl* cbEffDens;
	wxTextCtrl* vbEffDens;
	wxTextCtrl* eefmass_dos;
	wxTextCtrl* hefmass_dos;
	wxTextCtrl* eefmass_c;
	wxTextCtrl* hefmass_c;
	wxTextCtrl* electronMobility;
	wxTextCtrl* holeMobility;
	wxTextCtrl* energyBandGap;
	wxTextCtrl* electronAffinity;
	wxTextCtrl* heatCapacity;
	wxTextCtrl* thermalConductivity;

	wxButton* customDOS;

	wxTextCtrl* permeability;

	wxTextCtrl* urbachEcE;	//
	wxTextCtrl* urbachEcSigCB;	//
	wxTextCtrl* urbachEcSigVB;	//
	wxTextCtrl* urbachEvE;	//
	wxTextCtrl* urbachEvSigCB;	//
	wxTextCtrl* urbachEvSigVB;	//
	wxTextCtrl* ThGenRecTau;	//

	wxCheckBox* directEg;	//


	wxBoxSizer* matLeftSizer;
	wxBoxSizer* matRightSizer;
	wxFlexGridSizer* nameDescriptSizer;
	wxFlexGridSizer* matInnerSizer;
	wxFlexGridSizer* matInnerSizer1;


	wxStaticText* stat_permeability;	//
	wxStaticText* stat_urbachEcE;	//
	wxStaticText* stat_urbachEcSigCB;	//
	wxStaticText* stat_urbachEcSigVB;	//
	wxStaticText* stat_urbachEvE;	//
	wxStaticText* stat_urbachEvSigCB;	//
	wxStaticText* stat_urbachEvSigVB;	//
	wxStaticText* stat_ThGenRecTau;	//
	wxStaticText* stat_directEg;	//

	wxStaticText* descript;
	wxStaticText* perm;
	wxStaticText* icarrier;
	wxStaticText* cEffective;
	wxStaticText* vEffective;
	wxStaticText* eefm_c;
	wxStaticText* hefm_c;
	wxStaticText* eefm_dos;
	wxStaticText* hefm_dos;
	wxStaticText* emob;
	wxStaticText* hmob;
	wxStaticText* gap;
	wxStaticText* eAffin;
	wxStaticText* heatCap;
	wxStaticText* tCond;

	bool isInitialized() { return (customDOS != nullptr); }

	DECLARE_EVENT_TABLE()
};

#endif