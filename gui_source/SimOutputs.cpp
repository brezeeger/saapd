#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"

#include "SimulationWindow.h"
#include "SimOutputs.h"
#include "../kernel_source/model.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif

simOutputPanel::simOutputPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY), bxszrButtons(nullptr)
{
	

	chkFermi = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Ef");
	chkFermi->SetToolTip("(eV) Fermi energy analyzing all available states. Note, this value is not used in calculations.");
	chkFermiN = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Efn");
	chkFermiN->SetToolTip("(eV) Fermi Energy analyzing only electrons in the conduction band. Note, this value is not used in calculations.");
	chkFermiP = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Efp");
	chkFermiP->SetToolTip("(eV) Fermi energy analyzing only holes in the valence band. Note, this value is not used in calculations.");
	chkEc = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "CB");
	chkEc->SetToolTip("(eV) Conduction band");
	chkEv = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "VB");
	chkEv->SetToolTip("(eV) Valence Band");
	chkPsi = new wxCheckBox(this, SIM_OUT_PANEL_CHK, wxT("\u03A8"));	//psi
	chkPsi->SetToolTip("(eV) Local Vacuum Level");
	chkCarrierCB = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Electron Count");
	chkCarrierCB->SetToolTip("(#) The number of electrons in the conduction band at each position. Note SAAPD does not use concentrations internally.");
	chkCarrierVB = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Hole Count");
	chkCarrierVB->SetToolTip("(#) The number of holes in the valence band at each position. Note SAAPD does not use concentrations internally.");
	chkImpCharge = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Defect Charge Density");
	chkImpCharge->SetToolTip(wxT("(Q/cm\u2083) The local defect/impurity contribution to the charge density. Does not include carriers occupied on the impurity."));
	chkN = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "n");
	chkN->SetToolTip(wxT("(cm\u207B\u00B3) Electron concentration in the conduction band"));
	chkP = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "p");
	chkP->SetToolTip(wxT("(cm\u207B\u00B3) Hole concentration in the valence band"));
	chkLPow = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Light Power");
	chkLPow->SetToolTip("(mW) How much optical power enters each position in the device");
	chkLGen = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Absorption Band Generation");
	chkLGen->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Generation rate from valence band to conduction band resulting from optical absorption"));
	chkLSRH = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Absorption SRH Generation");
	chkLSRH->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Generation rate between defects and bands resulting from optical absorption"));
	chkLDef = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Absorption Defect Generation");
	chkLDef->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Electron excitation between defects resulting from optical absorption"));
	chkSERec = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Stimulated Emission Band Recombination");
	chkSERec->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Optically assisted recombination rate between conduction and valence band, resulting in additional light"));
	chkSESRH = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Stimulated Emission SRH Recombination");
	chkSESRH->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Optically assisted recombination between defects and bands resulting in additional light"));
	chkSEDef = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Stimulated Emission Defect Recombination");
	chkSEDef->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Optically assisted electron relaxation between defects resulting in additional"));
	chkGenTH = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "G");
	chkGenTH->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Thermal Generation. Electron excitation from the valence band to the conduction band, resulting in an electron-hole pair"));
	chkSRH = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "SRH");
	chkSRH->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Shockley-Read-HallGeneration and Recombination rates between the bands and defect states. All 4 separated individually."));
	chkRecTh = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "R");
	chkRecTh->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Thermal Recombination. Electrons in the conduction band falling into holes in the valence band. Mutual annhiliation."));
	chkRho = new wxCheckBox(this, SIM_OUT_PANEL_CHK, wxT("\u03C1"));
	chkRho->SetToolTip(wxT("(Q/cm\u207B\u00B3) Charge Density"));
	chkScatter = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Scattering");
	chkScatter->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) Carriers in the band scatter to different energy states to relax into a Fermi-Dirac distribution."));
	chkJp = new wxCheckBox(this, SIM_OUT_PANEL_CHK, wxT("Jp"));
	chkJp->SetToolTip(wxT("(A cm\u207B\u00B2) Hole Current Density: The travel of charge density (holes) in the valence band."));
	chkJn = new wxCheckBox(this, SIM_OUT_PANEL_CHK, wxT("Jn"));
	chkJn->SetToolTip(wxT("(A cm\u207B\u00B2) Electron Current Density: The travel of charge density (electrons) in the conduction band"));
	chkCBNRin = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "CB Net Rate");
	chkCBNRin->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) At what concentration rate are electrons entering the conduction band"));
	chkVBNRin = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "VB Net Rate");
	chkVBNRin->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) At what concentration rate are holes entering the valence band"));
	chkDonNRin = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Donor Net Rate");
	chkDonNRin->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) At what concentration rate are electrons entering donors"));
	chkAcpNRin = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Acceptor Net Rate");
	chkAcpNRin->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) At what concentration rate are holes entering the acceptors"));
	chkLightEmission = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Optical Emission");
	chkLightEmission->SetToolTip(wxT("(cm\u207B\u00B3 s\u207B\u00B9) How many photons are emitted per volume?"));
	chkStdDev = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Standard Deviations");
	chkStdDev->SetToolTip("Transient: Data is binned over X iterations. Output the standard deviation in addition to the average on all the above values");
	chkError = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Standard Error");
	chkError->SetToolTip("Transient:  Data is binned over X iterations. Output the standard error in addition to the average on all the above values"); 
	chkQE = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Optical Macro Interactions");
	chkQE->SetToolTip("Plots of light incident, reflected, absorbed, and transmitted for the device and materials");
//	chkChgError = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Charge Error");
//	chkChgError->SetToolTip("(DEBUG) ");
	chkEmptyQE = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Output Empty Optical Plots");
	chkEmptyQE->SetToolTip("Example: Material Si had zero emissions. Include a Silicon emission file despite it being filled 0's.");
	chkRatePlots = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Randomized Rate vs Concentration Plots");
	chkRatePlots->SetToolTip("(Debug) Transient: Each iteration it chooses a single random energy state. It calculates the rates over a range of concentrations and outputs a plot.");
	chkAllIndivRates = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Every Individual Rate");
	chkAllIndivRates->SetToolTip("Creates a file for each finite volume and each iteration. Gives each energy state's energy range, density, occupation, fermi level, and its individual rates to every interacting state as total rate out, total rate in, and net rate in both carriers/second (native units) and concentration/second. Generates a lot of data, and there currently is no efficient way to view it.");
	
	chkCapactiance = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Capacitance");
	chkCapactiance->SetToolTip("(Future development) Capacitance data for device.");
	chkJV = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "JV Curve");	//steady state
	chkJV->SetToolTip("Steady State: Standard JV curve.");
	chkContactCharge = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Contact Charge");	//transient
	chkContactCharge->SetToolTip("Transient: Tracks how much charge enters and leaves the device over the iterations (note, device is not required to be charge neutral!). Potentially useful for a manual capacitance calculation.");
	chkContactState = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Full Contact States");
	chkContactState->SetToolTip("Outputs the state of the contacts.");
	chkEnvSummary = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Environment Summary");
	chkEnvSummary->SetToolTip("Steady state: Outputs the external environment used for each calculation and how it is all interpreted.");
	chkEnvResults = new wxCheckBox(this, SIM_OUT_PANEL_CHK, "Environment Results");
	chkEnvResults->SetToolTip("Steady state: Outputs the results of each contact for the different environments");

	stMaxBin = new wxStaticText(this, wxID_ANY, wxT("Maximum Bin Files"));
	txtMaxBin = new wxTextCtrl(this, SIM_OUT_PANEL_TXT, "100", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_DIGITS));
	stFileName = new wxStaticText(this, wxID_ANY, wxT("Simulation Name:"));
	txtFileName = new wxTextCtrl(this, SIM_OUT_PANEL_TXT);

	btnCommon = new wxButton(this, SIM_OUT_COMMON, "Basics");
	btnDark = new wxButton(this, SIM_OUT_DARK, "Dark");
	btnLight = new wxButton(this, SIM_OUT_LIGHT, "Light");
	btnPL = new wxButton(this, SIM_OUT_PL, "Photoluminescence");
	btnFull = new wxButton(this, SIM_OUT_FULL, "Full Insight");
	btnClear = new wxButton(this, SIM_OUT_CLEAR, "Clear All");
	
	bxszrStandard = new wxStaticBoxSizer(wxVERTICAL, this, "Standard Outputs");
	bxszrOptical = new wxStaticBoxSizer(wxVERTICAL, this, "Optical Outputs (Position & Energy Plots)");
	bxszrMiscellaneous = new wxStaticBoxSizer(wxVERTICAL, this, "Miscelaneous Outputs");
//	bxszrOptions = new wxStaticBoxSizer(wxVERTICAL, this, "Options");
	
	bxszrTransient = new wxStaticBoxSizer(wxVERTICAL, this, "Transient Outputs and Options");
	bxszrSteadyState = new wxStaticBoxSizer(wxVERTICAL, this, "Steady State Outputs");
	bxszrHorizMain = new wxBoxSizer(wxHORIZONTAL);
	bxszrSimName = new wxBoxSizer(wxHORIZONTAL);
	bxszrSSTran = new wxBoxSizer(wxVERTICAL);
	bxszrCol = new wxBoxSizer(wxVERTICAL);
	szrPrimary = new wxBoxSizer(wxHORIZONTAL); //This wrapper is just to make a margin around the panel
	bxszrOptMisc = new wxBoxSizer(wxVERTICAL);
	bxszrButtons = new wxBoxSizer(wxVERTICAL);

	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnCommon, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnDark, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnLight, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnPL, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnFull, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();
	bxszrButtons->Add(btnClear, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	bxszrButtons->AddStretchSpacer();

	bxszrStandard->Add(chkPsi, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkEc, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkEv, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkFermi, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkFermiN, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkFermiP, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkN, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkP, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkJp, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkJn, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkRecTh, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkGenTH, 0, wxBOTTOM, 5);
	bxszrStandard->Add(chkRho, 0, wxBOTTOM, 5);
	
	
	bxszrOptical->Add(chkQE, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkLightEmission, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkLPow, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkLGen, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkLSRH, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkLDef, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkSERec, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkSESRH, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkSEDef, 0, wxBOTTOM, 5);
	bxszrOptical->Add(chkEmptyQE, 0, wxBOTTOM, 5);

	bxszrMiscellaneous->Add(chkSRH, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkCarrierCB, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkCarrierVB, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkImpCharge, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkScatter, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkCBNRin, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkVBNRin, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkDonNRin, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkAcpNRin, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkRatePlots, 0, wxBOTTOM, 5);
	bxszrMiscellaneous->Add(chkAllIndivRates, 0, wxBOTTOM, 5);

	bxszrTransient->Add(chkStdDev, 0, wxBOTTOM, 5);
	bxszrTransient->Add(chkError, 0, wxBOTTOM, 5);
	bxszrTransient->Add(chkContactCharge, 0, wxBOTTOM, 5);
	bxszrTransient->AddStretchSpacer();
	bxszrTransient->Add(stMaxBin, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 5);
	bxszrTransient->Add(txtMaxBin, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 5);

	bxszrSteadyState->Add(chkJV, 0, wxBOTTOM, 5);
	bxszrSteadyState->Add(chkEnvSummary, 0, wxBOTTOM, 5);
	bxszrSteadyState->Add(chkEnvResults, 0, wxBOTTOM, 5);
	bxszrSteadyState->Add(chkContactState, 0, wxBOTTOM, 5);
	bxszrSteadyState->Add(chkCapactiance, 0, wxBOTTOM, 5);
	
	bxszrSimName->Add(stFileName, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxRIGHT, 2);
	bxszrSimName->Add(txtFileName, 1, wxALL, 0);

	bxszrSSTran->Add(bxszrSteadyState, 0, wxALL | wxEXPAND, 0);
	bxszrSSTran->Add(bxszrTransient, 1, wxTOP, 5);

	bxszrCol->Add(bxszrSimName, 0, wxALL | wxEXPAND, 5);
	bxszrCol->Add(bxszrHorizMain, 1, wxALL, 5);

	bxszrOptMisc->Add(bxszrOptical, 0, wxTOP, 5);
	bxszrOptMisc->Add(bxszrMiscellaneous, 0, wxTOP | wxEXPAND, 3);

	bxszrHorizMain->Add(bxszrStandard, 0, wxALL, 5);
	bxszrHorizMain->Add(bxszrSSTran, 0, wxALL | wxEXPAND, 5);


	szrPrimary->Add(bxszrCol, 0, wxALL, 5);
	szrPrimary->Add(bxszrOptMisc, 0, wxLEFT | wxEXPAND, 5);
	szrPrimary->Add(bxszrButtons, 0, wxLEFT | wxEXPAND, 5);
	

	chkCapactiance->Disable();
	SizeRight();
	
	TransferToGUI();
}

simOutputPanel::~simOutputPanel()
{
	delete chkFermi;
	delete chkFermiN;
	delete chkFermiP;
	delete chkEc;
	delete chkEv;
	delete chkPsi;
	delete chkN;
	delete chkP;
	delete chkGenTH;
	delete chkRecTh;
	delete chkRho;
	delete chkJp;
	delete chkJn;
	delete chkQE;
	delete chkLGen;
	delete chkLightEmission;
	delete chkLPow;
	delete chkLSRH;
	delete chkLDef;
	delete chkSERec;
	delete chkSESRH;
	delete chkSEDef;
	delete chkSRH;
	delete chkCarrierCB;
	delete chkCarrierVB;
	delete chkImpCharge;
	delete chkScatter;
	delete chkCBNRin;
	delete chkVBNRin;
	delete chkDonNRin;
	delete chkAcpNRin;
	delete chkStdDev;
	delete chkError;
	//	delete chkChgError;		DEPRECATED
	delete chkEmptyQE;
	delete chkRatePlots;
	delete chkAllIndivRates;
	delete chkCapactiance;
	delete chkJV;	//steady state
	delete chkContactCharge;	//transient
	delete chkContactState;
	delete chkEnvSummary;
	delete chkEnvResults;
	delete txtMaxBin;
	delete stMaxBin;
	delete txtFileName;
	delete stFileName;
	delete btnCommon;
	delete btnDark;
	delete btnPL;
	delete btnLight;
	delete btnFull;
	delete btnClear;
}




void simOutputPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(szrPrimary);
}
simulationWindow* simOutputPanel::getMyClassParent()
{
	return (simulationWindow*)GetParent();
}
ModelDescribe* simOutputPanel::getMdesc()
{
	return getMyClassParent()->getMdesc();
}

Simulation* simOutputPanel::getSim()
{
	return getMyClassParent()->getSim();
}

void simOutputPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)	return;
	Outputs& o = sim->outputs;	//i'm just L A Z Y for typing.

	o.fermi = chkFermi->GetValue();
	o.fermin = chkFermiN->GetValue();
	o.fermip = chkFermiP->GetValue();
	o.Ec = chkEc->GetValue();
	o.Ev = chkEv->GetValue();
	o.Psi = chkPsi->GetValue();
	o.n = chkN->GetValue();
	o.p = chkP->GetValue();
	o.genth = chkGenTH->GetValue();
	o.recth = chkRecTh->GetValue();
	o.rho = chkRho->GetValue();
	o.Jp = chkJp->GetValue();
	o.Jn = chkJn->GetValue();
	o.QE = chkQE->GetValue();
	o.Lgen = chkLGen->GetValue();


	o.LightEmission = chkLightEmission->GetValue();
	o.Lpow = chkLPow->GetValue();
	o.Lsrh = chkLSRH->GetValue();
	o.Ldef = chkLDef->GetValue();
	o.Erec = chkSERec->GetValue();
	o.Esrh = chkSESRH->GetValue();
	o.Edef = chkSEDef->GetValue();


	o.SRH = chkSRH->GetValue();
	o.CarrierCB = chkCarrierCB->GetValue();
	o.CarrierVB = chkCarrierVB->GetValue();
	o.impcharge = chkImpCharge->GetValue();
	o.scatter = chkScatter->GetValue();
	o.CBNRin = chkCBNRin->GetValue();
	o.VBNRin = chkVBNRin->GetValue();
	o.DonNRin = chkDonNRin->GetValue();
	o.AcpNRin = chkAcpNRin->GetValue();


	o.stddev = chkStdDev->GetValue();
	o.error = chkError->GetValue();


	//	chkChgError->GetValue();		DEPRECATED

	o.EmptyQEFiles = chkEmptyQE->GetValue();
	o.OutputRatePlots = chkRatePlots->GetValue();
	o.OutputAllIndivRates = chkAllIndivRates->GetValue();

	o.capacitance = chkCapactiance->GetValue();
	o.jvCurves = chkJV->GetValue();	//steady state
	o.contactCharge = chkContactCharge->GetValue();	//transient
	o.contactStates = chkContactState->GetValue();
	o.envSummaries = chkEnvSummary->GetValue();
	o.envResults = chkEnvResults->GetValue();

	unsigned long i;

	if (txtMaxBin->GetValue().ToCULong(&i))
		o.numinbin = i;
	if (o.numinbin <= 0)
		o.numinbin = 1;
	sim->OutputFName = txtFileName->GetValue().ToStdString();
}

void simOutputPanel::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)
	{
		chkFermi->Disable();
		chkFermiN->Disable();
		chkFermiP->Disable();
		chkEc->Disable();
		chkEv->Disable();
		chkPsi->Disable();
		chkN->Disable();
		chkP->Disable();
		chkGenTH->Disable();
		chkRecTh->Disable();
		chkRho->Disable();
		chkJp->Disable();
		chkJn->Disable();
		chkQE->Disable();
		chkLGen->Disable();


		chkLightEmission->Disable();
		chkLPow->Disable();
		chkLSRH->Disable();
		chkLDef->Disable();
		chkSERec->Disable();
		chkSESRH->Disable();
		chkSEDef->Disable();


		chkSRH->Disable();
		chkCarrierCB->Disable();
		chkCarrierVB->Disable();
		chkImpCharge->Disable();
		chkScatter->Disable();
		chkCBNRin->Disable();
		chkVBNRin->Disable();
		chkDonNRin->Disable();
		chkAcpNRin->Disable();


		chkStdDev->Disable();
		chkError->Disable();


		//	chkChgError->Disable();		DEPRECATED

		chkEmptyQE->Disable();
		chkRatePlots->Disable();
		chkAllIndivRates->Disable();

		chkCapactiance->Disable();
		chkJV->Disable();	//steady state
		chkContactCharge->Disable();	//transient
		chkContactState->Disable();
		chkEnvSummary->Disable();
		chkEnvResults->Disable();

		txtMaxBin->Disable();
		txtFileName->Disable();
	}
	else
	{
		Calculations& c = sim->EnabledRates;
		bool anyLight = c.Ldef || c.Lgen || c.Lpow || c.Lsrh || c.LightEmissions || c.SEdef || c.SErec || c.SEsrh;
		chkFermi->Enable();
		chkFermiN->Enable();
		chkFermiP->Enable();
		chkEc->Enable();
		chkEv->Enable();
		chkPsi->Enable();
		chkN->Enable();
		chkP->Enable();
		chkGenTH->Enable(c.ThermalGen);
		chkRecTh->Enable(c.ThermalRec);
		chkRho->Enable();
		chkJp->Enable(c.Current);
		chkJn->Enable(c.Current);
		chkQE->Enable(anyLight);
		chkLGen->Enable(c.Lgen);


		chkLightEmission->Enable(c.LightEmissions);
		chkLPow->Enable(c.Lpow);
		chkLSRH->Enable(c.Lsrh);
		chkLDef->Enable(c.Ldef);
		chkSERec->Enable(c.SErec);
		chkSESRH->Enable(c.SEsrh);
		chkSEDef->Enable(c.SEdef);


		chkSRH->Enable(c.SRH1 || c.SRH2 || c.SRH3 || c.SRH4);
		chkCarrierCB->Enable();
		chkCarrierVB->Enable();
		chkImpCharge->Enable();
		chkScatter->Enable(c.Scatter);
		chkCBNRin->Enable();
		chkVBNRin->Enable();
		chkDonNRin->Enable();
		chkAcpNRin->Enable();


		chkStdDev->Enable();
		chkError->Enable();


		//	chkChgError->Enable();		DEPRECATED

		chkEmptyQE->Enable(anyLight);
		chkRatePlots->Enable();
		chkAllIndivRates->Enable();

		chkCapactiance->Disable();
		chkJV->Enable(c.Current);	//steady state
		chkContactCharge->Enable();	//transient
		chkContactState->Enable();
		chkEnvSummary->Enable();
		chkEnvResults->Enable();

		txtMaxBin->Enable();
		txtFileName->Enable();

		//... I knew there had to be an efficient way to write this!
		wxWindowList children = GetChildren();
		for (wxWindowList::iterator it = children.begin(); it != children.end(); ++it)
		{
			if ((*it)->IsKindOf(CLASSINFO(wxCheckBox)))
			{
				wxCheckBox* tmp = (wxCheckBox*)(*it);
				if (tmp->IsEnabled() == false)
					tmp->SetValue(false);
			}
			
		}
	}
}

void simOutputPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	chkFermi->SetValue(o.fermi);
	chkFermiN->SetValue(o.fermin);
	chkFermiP->SetValue(o.fermip);
	chkEc->SetValue(o.Ec);
	chkEv->SetValue(o.Ev);
	chkPsi->SetValue(o.Psi);
	chkN->SetValue(o.n);
	chkP->SetValue(o.p);
	chkGenTH->SetValue(o.genth);
	chkRecTh->SetValue(o.recth);
	chkRho->SetValue(o.rho);
	chkJp->SetValue(o.Jp);
	chkJn->SetValue(o.Jn);
	chkQE->SetValue(o.QE);
	chkLGen->SetValue(o.Lgen);
	chkLightEmission->SetValue(o.LightEmission);
	chkLPow->SetValue(o.Lpow);
	chkLSRH->SetValue(o.Lsrh);
	chkLDef->SetValue(o.Ldef);
	chkSERec->SetValue(o.Erec);
	chkSESRH->SetValue(o.Esrh);
	chkSEDef->SetValue(o.Edef);
	chkSRH->SetValue(o.SRH);
	chkCarrierCB->SetValue(o.CarrierCB);
	chkCarrierVB->SetValue(o.CarrierVB);
	chkImpCharge->SetValue(o.impcharge);
	chkScatter->SetValue(o.scatter);
	chkCBNRin->SetValue(o.CBNRin);
	chkVBNRin->SetValue(o.VBNRin);
	chkDonNRin->SetValue(o.DonNRin);
	chkAcpNRin->SetValue(o.AcpNRin);
	chkStdDev->SetValue(o.stddev);
	chkError->SetValue(o.error);


	//	chkChgError->SetValue(o.);		DEPRECATED
	chkEmptyQE->SetValue(o.EmptyQEFiles);
	chkRatePlots->SetValue(o.OutputRatePlots);
	chkAllIndivRates->SetValue(o.OutputAllIndivRates);

	chkCapactiance->SetValue(o.capacitance);
	chkJV->SetValue(o.jvCurves);	//steady state
	chkContactCharge->SetValue(o.contactCharge);	//transient
	chkContactState->SetValue(o.contactStates);
	chkEnvSummary->SetValue(o.envSummaries);
	chkEnvResults->SetValue(o.envResults);

	ValueToTBox(txtMaxBin, o.numinbin);
	ValueToTBox(txtFileName, sim->OutputFName);
	
	UpdateGUIValues(wxCommandEvent());
	
}

void simOutputPanel::SetBasics(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = true;
	o.fermin=false;
	o.fermip=false;
	o.Ec = true;
	o.Ev = true;
	o.Psi=false;
	o.CarrierCB=false;
	o.CarrierVB=false;
	o.impcharge=false;
	o.n = true;
	o.p = true;
	o.Lpow=false;
	o.Lgen=false;
	o.Lsrh=false;
	o.Ldef=false;
	o.Lscat=false;
	o.Erec=false;
	o.Esrh=false;
	o.Edef=false;
	o.Escat=false;
	o.genth = true;
	o.SRH = true;
	o.recth = true;
	o.rho = true;
	o.scatter=false;
	o.Jp = true;	//net hole current in each direction
	o.Jn = true;	//net electron current in each direction
	o.CBNRin=false;
	o.VBNRin=false;
	o.DonNRin=false;
	o.AcpNRin=false;
	o.QE=false;
	o.LightEmission=false;
	o.EmptyQEFiles=false;
	o.OutputAllIndivRates=false;	//INPUT - generally false - used mostly for debugging
	o.OutputRatePlots=false;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev=false;	//standard deviation of the data
	o.error=false;	//error of mean
	o.chgError=false;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin=100;		//number of binning to do!
	//steady state
	o.jvCurves = true;
	o.envSummaries = true;
	o.envResults = true;
	o.contactStates = true;
	o.capacitance=false;
	o.contactCharge = false;
	TransferToGUI();
}

void simOutputPanel::SetDark(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = true;
	o.fermin = true;
	o.fermip = true;
	o.Ec = true;
	o.Ev = true;
	o.Psi = false;
	o.CarrierCB = false;
	o.CarrierVB = false;
	o.impcharge = false;
	o.n = true;
	o.p = true;
	o.Lpow = false;
	o.Lgen = false;
	o.Lsrh = false;
	o.Ldef = false;
	o.Lscat = false;
	o.Erec = false;
	o.Esrh = false;
	o.Edef = false;
	o.Escat = false;
	o.genth = true;
	o.SRH = true;
	o.recth = true;
	o.rho = true;
	o.scatter = true;
	o.Jp = true;	//net hole current in each direction
	o.Jn = true;	//net electron current in each direction
	o.CBNRin = false;
	o.VBNRin = false;
	o.DonNRin = false;
	o.AcpNRin = false;
	o.QE = false;
	o.LightEmission = false;
	o.EmptyQEFiles = false;
	o.OutputAllIndivRates = false;	//INPUT - generally false - used mostly for debugging
	o.OutputRatePlots = false;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev = false;	//standard deviation of the data
	o.error = false;	//error of mean
	o.chgError = false;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin = 100;		//number of binning to do!
	//steady state
	o.jvCurves = true;
	o.envSummaries = true;
	o.envResults = true;
	o.contactStates = true;
	o.capacitance = false;
	o.contactCharge = false;
	TransferToGUI();
}

void simOutputPanel::SetLight(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = true;
	o.fermin = true;
	o.fermip = true;
	o.Ec = true;
	o.Ev = true;
	o.Psi = false;
	o.CarrierCB = false;
	o.CarrierVB = false;
	o.impcharge = false;
	o.n = true;
	o.p = true;
	o.Lpow = true;
	o.Lgen = true;
	o.Lsrh = true;
	o.Ldef = true;
	o.Lscat = false;
	o.Erec = true;
	o.Esrh = true;
	o.Edef = true;
	o.Escat = false;
	o.genth = true;
	o.SRH = true;
	o.recth = true;
	o.rho = true;
	o.scatter = true;
	o.Jp = true;	//net hole current in each direction
	o.Jn = true;	//net electron current in each direction
	o.CBNRin = false;
	o.VBNRin = false;
	o.DonNRin = false;
	o.AcpNRin = false;
	o.QE = true;
	o.LightEmission = false;
	o.EmptyQEFiles = false;
	o.OutputAllIndivRates = false;	//INPUT - generally false - used mostly for debugging
	o.OutputRatePlots = false;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev = false;	//standard deviation of the data
	o.error = false;	//error of mean
	o.chgError = false;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin = 100;		//number of binning to do!
	//steady state
	o.jvCurves = true;
	o.envSummaries = true;
	o.envResults = true;
	o.contactStates = true;
	o.capacitance = false;
	o.contactCharge = false;
	TransferToGUI();
}

void simOutputPanel::SetPL(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = false;
	o.fermin = true;
	o.fermip = true;
	o.Ec = true;
	o.Ev = true;
	o.Psi = false;
	o.CarrierCB = false;
	o.CarrierVB = false;
	o.impcharge = false;
	o.n = false;
	o.p = false;
	o.Lpow = true;
	o.Lgen = true;
	o.Lsrh = true;
	o.Ldef = true;
	o.Lscat = false;
	o.Erec = true;
	o.Esrh = true;
	o.Edef = true;
	o.Escat = false;
	o.genth = true;
	o.SRH = true;
	o.recth = true;
	o.rho = false;
	o.scatter = false;
	o.Jp = false;	//net hole current in each direction
	o.Jn = false;	//net electron current in each direction
	o.CBNRin = false;
	o.VBNRin = false;
	o.DonNRin = false;
	o.AcpNRin = false;
	o.QE = true;
	o.LightEmission = true;
	o.EmptyQEFiles = false;
	o.OutputAllIndivRates = false;	//INPUT - generally false - used mostly for debugging
	o.OutputRatePlots = false;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev = false;	//standard deviation of the data
	o.error = false;	//error of mean
	o.chgError = false;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin = 100;		//number of binning to do!
	//steady state
	o.jvCurves = false;
	o.envSummaries = true;
	o.envResults = false;
	o.contactStates = false;
	o.capacitance = false;
	o.contactCharge = false;
	TransferToGUI();
}

void simOutputPanel::SetFull(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = true;
	o.fermin = true;
	o.fermip = true;
	o.Ec = true;
	o.Ev = true;
	o.Psi = true;
	o.CarrierCB = true;
	o.CarrierVB = true;
	o.impcharge = true;
	o.n = true;
	o.p = true;
	o.Lpow = true;
	o.Lgen = true;
	o.Lsrh = true;
	o.Ldef = true;
	o.Lscat = true;
	o.Erec = true;
	o.Esrh = true;
	o.Edef = true;
	o.Escat = true;
	o.genth = true;
	o.SRH = true;
	o.recth = true;
	o.rho = true;
	o.scatter = true;
	o.Jp = true;	//net hole current in each direction
	o.Jn = true;	//net electron current in each direction
	o.CBNRin = true;
	o.VBNRin = true;
	o.DonNRin = true;
	o.AcpNRin = true;
	o.QE = true;
	o.LightEmission = true;
	o.EmptyQEFiles = true;
	o.OutputAllIndivRates = true;	//INPUT - generally true - used mostly for debugging
	o.OutputRatePlots = true;	//INPUT - generally true - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev = true;	//standard deviation of the data
	o.error = true;	//error of mean
	o.chgError = true;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin = 100;		//number of binning to do!
	//steady state
	o.jvCurves = true;
	o.envSummaries = true;
	o.envResults = true;
	o.contactStates = true;
	o.capacitance = false;
	o.contactCharge = true;
	TransferToGUI();
}

void simOutputPanel::ClearChecks(wxCommandEvent& event)
{
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	Outputs& o = sim->outputs;

	o.fermi = false;
	o.fermin = false;
	o.fermip = false;
	o.Ec = false;
	o.Ev = false;
	o.Psi = false;
	o.CarrierCB = false;
	o.CarrierVB = false;
	o.impcharge = false;
	o.n = false;
	o.p = false;
	o.Lpow = false;
	o.Lgen = false;
	o.Lsrh = false;
	o.Ldef = false;
	o.Lscat = false;
	o.Erec = false;
	o.Esrh = false;
	o.Edef = false;
	o.Escat = false;
	o.genth = false;
	o.SRH = false;
	o.recth = false;
	o.rho = false;
	o.scatter = false;
	o.Jp = false;	//net hole current in each direction
	o.Jn = false;	//net electron current in each direction
	o.CBNRin = false;
	o.VBNRin = false;
	o.DonNRin = false;
	o.AcpNRin = false;
	o.QE = false;
	o.LightEmission = false;
	o.EmptyQEFiles = false;
	o.OutputAllIndivRates = false;	//INPUT - generally false - used mostly for debugging
	o.OutputRatePlots = false;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	o.stddev = false;	//standard deviation of the data
	o.error = false;	//error of mean
	o.chgError = false;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
//	o.numinbin = 100;		//number of binning to do!
	//steady state
	o.jvCurves = false;
	o.envSummaries = false;
	o.envResults = false;
	o.contactStates = false;
	o.capacitance = false;
	o.contactCharge = false;

	TransferToGUI();
}

BEGIN_EVENT_TABLE(simOutputPanel, wxPanel)
EVT_CHECKBOX(SIM_OUT_PANEL_CHK, simOutputPanel::TransferToMdesc)
EVT_TEXT(SIM_OUT_PANEL_TXT, simOutputPanel::TransferToMdesc)
EVT_BUTTON(SIM_OUT_COMMON, simOutputPanel::SetBasics)
EVT_BUTTON(SIM_OUT_DARK, simOutputPanel::SetDark)
EVT_BUTTON(SIM_OUT_LIGHT, simOutputPanel::SetLight)
EVT_BUTTON(SIM_OUT_PL, simOutputPanel::SetPL)
EVT_BUTTON(SIM_OUT_FULL, simOutputPanel::SetFull)
EVT_BUTTON(SIM_OUT_CLEAR, simOutputPanel::ClearChecks)
END_EVENT_TABLE()
