#ifndef DRAWING_H
#define DRAWING_H

#include <math.h>
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#define MIN_SCALE 1
#define MAX_SCALE 1000000000000000
#define SCALING_FACTOR 2

#define NUM_BRUSHES 6
#define NUM_COLORS 7

class layerShapesPanel : public wxPanel
{
public:
	layerShapesPanel(wxWindow* parent);
	~layerShapesPanel();
	

	void render(wxDC &dc);
	void paintNow();
	void paintEvent(wxPaintEvent & evt);
	void ZoomIn(wxCommandEvent& event);
	void ZoomOut(wxCommandEvent& event);
	
	wxBoxSizer* zoomSizer;
	wxButton* zoomIn;
	wxButton* zoomOut;
	double userScale;

	DECLARE_EVENT_TABLE()
};


class pointPanel : public wxPanel
{
public:
	pointPanel(wxWindow* parent);
	void paintEvent(wxPaintEvent& evt);
	void paintNow();
	void render(wxDC& dc);
	void SizeRight();

	void drawCarrierArrow(wxDC& dc, wxPoint start, wxPoint end, int num);

	DECLARE_EVENT_TABLE()
};


#endif