#ifndef IMPURITY_PANEL_H
#define IMPURITY_PANEL_H


#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "wx/scrolwin.h"

class wxWindow;
class wxCommandEvent;
class wxComboBox;
class wxRadioBox;
class wxTextCtrl;
class wxButton;
class wxBoxSizer;
class wxStaticText;
class wxFlexGridSizer;
class wxSizer;
class ModelDescribe;
class devMaterialPanel;
class Material;
class Impurity;

class devImpurityPanel : public wxScrolledWindow
{
public:
	devImpurityPanel(wxWindow* parent);
	~devImpurityPanel();

	
	void NewImpurity(wxCommandEvent& event);
	void LoadImpurity(wxCommandEvent& event);
	void RemoveImpurity(wxCommandEvent& event);
	void RenameImpurity(wxCommandEvent& event);
	
	void ChangeDistributionType(wxCommandEvent& event);
	void duplicateImpurity(wxCommandEvent& event);
	void UpdateReference(wxCommandEvent& event);
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues();
	void TransferToGUI();

	void Clear();

	void SizeRight();
	void SwitchImpurity(wxCommandEvent& event);
	void saveImpurity(wxCommandEvent& event);


	wxFlexGridSizer* szrEnergyInputs;	//2x2 energy variation
	wxFlexGridSizer* szrMiscInputs;	// ? x 2 all other shit
	wxBoxSizer* szrEnergyExplain;	//vertical, flex Inp and explain what's going on
	wxBoxSizer* szrEnergyRBoxInp;	//horizontal, all the energy stuff go in this
	wxBoxSizer* szrRefDep;	//vertical, reference and depth
	wxBoxSizer* szropenSpace;	//vertical create a blank space for future stuff under the energy inputs
	wxBoxSizer* szrEnergySide;	//horizontal. connects the energy/open sizer with the misc inputs sizer
	wxBoxSizer* szrBar;		//horizontal. impurity selection, all the buttons

	wxComboBox* impurityChooser;

	wxRadioBox* referenceBox;
	wxRadioBox* depthBox;
	wxRadioBox* distBox;

	wxTextCtrl* impsigman;
	wxTextCtrl* impsigmap;
	wxTextCtrl* impCharge;
	wxTextCtrl* impEn;
	wxTextCtrl* impVar;
	wxTextCtrl* impDegen;
	wxTextCtrl* impOptEffCB;
	wxTextCtrl* impOptEffVB;
	wxTextCtrl* impDensityBox;

	wxButton* impAdd;
	wxButton* impRem;
	wxButton* impSave;
	wxButton* impLoad;
	wxButton* impDuplicate;
	wxButton* impRename;

	
	wxStaticText* txtSigman;
	wxStaticText* txtSigmap;
	wxStaticText* txtCharge;
	wxStaticText* txtEnergy;
	wxStaticText* txtVariation;
	wxStaticText* txtDensity;
	wxStaticText* txtDegeneracy;
	wxStaticText* txtOptEffCB;
	wxStaticText* txtOptEffVB;
	wxStaticText* txtExplainDistribution;

	ModelDescribe* getMDesc();
	mainFrame* getMyClassParent();
	Impurity* getImpurity() { return(curImp); }
	void setImpurity(Impurity* imp = nullptr) { curImp = imp; TransferToGUI(); }
	void VerifyInternalVars();
private:
	wxSizer* impSizer;
	Impurity* curImp;
	bool isInitialized() { return (szrMiscInputs != nullptr); }
	unsigned int ConvertImpIndex(int index);	//the impurity in mdesc shows defects. We don't want to show defects!

	int prevSelection;
	int nextID;

	void EnableImp(bool enable);

	DECLARE_EVENT_TABLE()
};


#endif