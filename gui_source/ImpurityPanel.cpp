#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "ImpurityPanel.h"
#include "MatPanel.h"
#include "MatOpticalPanel.h"

#include "../kernel_source/light.h"
#include "../kernel_source/model.h"
#include "../kernel_source/load.h"

#include "GUIHelper.h"


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

extern double scalarTEN(int index);


devImpurityPanel::devImpurityPanel(wxWindow* parent)
	: wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL), curImp(nullptr)
{
	wxArrayString defectChoices;
	//defectChoices.Add(wxT("Choice 1"));
	///defectChoices.Add(wxT("Choice 2"));
	impurityChooser = new wxComboBox(this, SWITCH_SELECTION, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, defectChoices, wxCB_DROPDOWN | wxCB_READONLY);
	impAdd = new wxButton(this, IMP_ADD, wxT("New Impurity"));	//these are standards that come with standard text
	impRem = new wxButton(this, IMP_REM, wxT("Remove"));
	impSave = new wxButton(this, IMP_SAVE, wxT("Save"));
	impLoad = new wxButton(this, IMP_LOAD, wxT("Load"));
	impDuplicate = new wxButton(this, IMP_DUPLICATE, wxT("Duplicate"));
	impRename = new wxButton(this, IMP_RENAME, wxT("Rename"));


	wxArrayString reference;
	reference.Add(wxT("CB (Donor)"));
	reference.Add(wxT("VB (Acceptor)"));
	referenceBox = new wxRadioBox(this, IMP_SMART_REF_CHANGE, wxT("Reference"), wxPoint(0, 30), wxDefaultSize, reference, 1, wxRA_SPECIFY_COLS);
	referenceBox->SetToolTip(wxT("Is the defect a donor and referenced to the conduction band, or an acceptor and referenced to the valence band?"));
	wxArrayString depth;
	depth.Add(wxT("Shallow"));
	depth.Add(wxT("Deep"));
	depthBox = new wxRadioBox(this, IMP_UPDATE_RAD, wxT("Depth"), wxPoint(0, 100), wxDefaultSize, depth, 1, wxRA_SPECIFY_COLS);
	depthBox->SetToolTip(wxT("Generally, shallow defects are near the band and trace along with it, while deep defects are usually mid gap. Deep defects don't fit in with the atomic matrix well, and their energy is therefore more independent from the bands regardless of bending. This option currently has zero influence on a simulation as everything is treated as shallow. Some day, that might change."));
	depthBox->Enable(1, false);
	depthBox->Select(0);
	//	depthBox->GetToolTip()->SetAutoPop(30000);

	wxArrayString energyDist;
	energyDist.Add(wxT("Discrete"));
	energyDist.Add(wxT("Banded"));
	energyDist.Add(wxT("Gaussian"));
	energyDist.Add(wxT("Exponential"));
	energyDist.Add(wxT("Linear"));
	energyDist.Add(wxT("Parabolic"));
	distBox = new wxRadioBox(this, IMP_CHANGEDIST, wxT("Energy Distribution"), wxPoint(0, 30), wxDefaultSize, energyDist, 1, wxRA_SPECIFY_COLS);
	distBox->SetToolTip(wxT("Set the function type defining how the defect's density of states will be distributed."));

	txtEnergy = new wxStaticText(this, wxID_ANY, wxT("Energy:"));
	txtEnergy->SetToolTip(wxT("This is the energy (eV) of the defect reference from the bands, with positive values moving into the band gap. This value is either the average or maximum energy"));
	impEn = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtVariation = new wxStaticText(this, wxID_ANY, wxT("Variation:"));
	txtVariation->SetToolTip(wxT("This is the variation in energy (eV) defining the defect. This will either be a half-width of sorts or distance until the density of states goes to zero."));
	impVar = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtDensity = new wxStaticText(this, wxID_ANY, wxT("Density:"));
	txtDensity->SetToolTip(wxT("The volume density (cm^-3) of the defect throughout the material. This is a constant concentration always present whenver this material is in use."));
	impDensityBox = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	//wxStaticText* material = new wxStaticText(this, wxID_ANY, wxT("Material Element: "));
	//wxTextCtrl* matID = new wxTextCtrl(this, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxEXPAND);
	txtSigman = new wxStaticText(this, wxID_ANY, wxT("\u03C3_n:"));
	txtSigman->SetToolTip(wxT("Capture cross section to capture electrons from the conduction band. cm^2/(V s)"));
	impsigman = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtSigmap = new wxStaticText(this, wxID_ANY, wxT("\u03C3_p:"));
	txtSigmap->SetToolTip(wxT("Capture cross section to capture holes from the valence band. cm^2/(V s)"));
	impsigmap = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	//wxStaticText* impDegen = new wxStaticText(this, wxID_ANY, wxT("degeneracy:"));
	//wxTextCtrl* impDeg = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));




	txtExplainDistribution = new wxStaticText(this, wxID_ANY, wxT("Select a distribution to get an explanation of how it works."));
	txtExplainDistribution->SetToolTip(wxT("Select from the energy distribution to change this message."));

	txtDegeneracy = new wxStaticText(this, wxID_ANY, wxT("Degeneracy:"));
	txtDegeneracy->SetToolTip(wxT("A carrier may take on multiple equivalent states. When the fermi function is applied with this taken into account, the resulting occupation probability resembles a fermi function \
with a slight adjustment factor. While other effects such as electron shielding influence this value, donors generally have a value of (2) and acceptors a value of (4). Donors generally have an electron able to go into \
a completely empty s orbital, having 2 options. Acceptors are usually bonding in sp3 orbitals, and the hole could enter 1 of 4 equivalent states. Note this is only a general rule of thumb, and will vary depending on the underlying crystal structure."));
	txtDegeneracy->GetToolTip()->SetAutoPop(60000);
	impDegen = new wxTextCtrl(this, IMP_UPDATE_TXT, wxT("2"), wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	txtCharge = new wxStaticText(this, wxID_ANY, wxT("Charge:"));
	txtCharge->SetToolTip(wxT("The charge on the defect when it is not occupied by its preferred carrier. In general, for donors, the charge is (+1) so when occupied by an electron, it is neutral. For acceptors, it should generally be (-1) as when occupied with a hole, it becomes a neutral charge."));
	impCharge = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));


	txtOptEffCB = new wxStaticText(this, wxID_ANY, wxT("CB Optical \u03B7:"));
	txtOptEffCB->SetToolTip(wxT("Undergoing Shockley-Read-Hall recombination, light is emitted during electron capture from the conduction band into this defect. Set the efficiency (0-1). Note, if the energy is close to the band, this should be 0 as phonons will dominate the energy dissipation."));
	impOptEffCB = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	txtOptEffVB = new wxStaticText(this, wxID_ANY, wxT("VB Optical \u03B7:"));
	txtOptEffVB->SetToolTip(wxT("Undergoing Shockley-Read-Hall recombination, light is emitted during hole capture from the valence band into this defect. Set the efficiency (0-1). Note, if the energy is close to the band, this should be 0 as phonons will dominate the energy dissipation."));
	impOptEffVB = new wxTextCtrl(this, IMP_UPDATE_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));



	szrEnergyExplain = new wxBoxSizer(wxVERTICAL);
	szrEnergyRBoxInp = new wxBoxSizer(wxHORIZONTAL);
	szrRefDep = new wxBoxSizer(wxVERTICAL);
	szropenSpace = new wxBoxSizer(wxVERTICAL);
	szrEnergySide = new wxBoxSizer(wxHORIZONTAL);
	szrBar = new wxBoxSizer(wxHORIZONTAL);
	impSizer = new wxBoxSizer(wxVERTICAL);
	szrEnergyInputs = new wxFlexGridSizer(2, 2, 10, 10);
	szrMiscInputs = new wxFlexGridSizer(2, 10, 10);


	szrMiscInputs->SetFlexibleDirection(wxBOTH);
	szrMiscInputs->AddGrowableCol(1, 1);

	szrMiscInputs->Add(txtDensity, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impDensityBox, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtSigman, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impsigman, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtSigmap, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impsigmap, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtDegeneracy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impDegen, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtCharge, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impCharge, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtOptEffCB, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impOptEffCB, 1, wxEXPAND, 0);
	szrMiscInputs->Add(txtOptEffVB, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrMiscInputs->Add(impOptEffVB, 1, wxEXPAND, 0);

	szrBar->Add(impurityChooser, 1, wxALL, 5);
	szrBar->Add(impAdd, 0, wxALL, 5);
	szrBar->Add(impRem, 0, wxALL, 5);
	szrBar->Add(impSave, 0, wxALL, 5);
	szrBar->Add(impLoad, 0, wxALL, 5);
	szrBar->Add(impDuplicate, 0, wxALL, 5);
	szrBar->Add(impRename, 0, wxALL, 5);

	szrEnergyInputs->SetFlexibleDirection(wxBOTH);
	szrEnergyInputs->AddGrowableCol(1, 1);
	szrEnergyInputs->Add(txtEnergy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrEnergyInputs->Add(impEn, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);
	szrEnergyInputs->Add(txtVariation, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrEnergyInputs->Add(impVar, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);

	szrEnergyExplain->Add(szrEnergyInputs, 0, wxALL, 5);
	szrEnergyExplain->Add(txtExplainDistribution, 1, wxALL | wxEXPAND, 5);

	szrRefDep->Add(referenceBox, 0, wxALL | wxEXPAND | wxALIGN_TOP, 0);
	szrRefDep->AddStretchSpacer();
	szrRefDep->Add(depthBox, 0, wxALL | wxEXPAND | wxALIGN_BOTTOM, 0);

	szrEnergyRBoxInp->Add(szrRefDep, 0, wxALL | wxEXPAND, 2);
	szrEnergyRBoxInp->Add(distBox, 0, wxALL, 2);
	szrEnergyRBoxInp->Add(szrEnergyExplain, 1, wxALL | wxEXPAND, 2);

	szropenSpace->Add(szrEnergyRBoxInp, 0, wxALL | wxEXPAND, 2);
	szropenSpace->AddStretchSpacer();

	szrEnergySide->Add(szropenSpace, 1, wxALL | wxALIGN_TOP | wxEXPAND, 2);
	szrEnergySide->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND, 0);
	szrEnergySide->Add(szrMiscInputs, 0, wxALL | wxALIGN_RIGHT | wxALIGN_TOP | wxEXPAND, 2);

	impSizer->Add(szrBar, 0, wxEXPAND, 0);
	impSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	impSizer->Add(szrEnergySide, 1, wxALL | wxEXPAND, 2);

	txtExplainDistribution->Wrap(szrEnergyInputs->GetMinSize().GetWidth());

	UpdateGUIValues();

	prevSelection = -1;
}

devImpurityPanel::~devImpurityPanel()
{
	impurityChooser->Clear();

	delete impurityChooser;
	delete referenceBox;
	delete depthBox;

	delete impsigman;
	delete impsigmap;
	delete impCharge;
	delete impEn;
	delete impVar;
	delete distBox;
	delete impDensityBox;

	delete impAdd;
	delete impRem;
	delete impSave;
	delete impLoad;
	delete impRename;


	delete txtSigman;
	delete txtSigmap;
	delete txtCharge;
	delete txtEnergy;
	delete txtVariation;

	delete txtDensity;
}

ModelDescribe* devImpurityPanel::getMDesc()
{
	return(getMyClassParent()->getMDesc());
}

mainFrame* devImpurityPanel::getMyClassParent()
{
	return((mainFrame*)GetParent()->GetParent()->GetParent());
}

void devImpurityPanel::SizeRight()
{
	SetScrollRate(20, 20);
	SetSizer(impSizer);
}


void devImpurityPanel::NewImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;
	

	wxTextEntryDialog entry(this, wxT("Enter the impurity's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	Impurity* imp = new Impurity();

	imp->setName(entry.GetValue().ToStdString());
	imp->setID(-1);

	mdesc->impurities.push_back(imp);	//mdesc owns the data

	mdesc->SetUnknownIDs();
	bool wasEmpty = impurityChooser->IsListEmpty();
	curImp = imp;
	TransferToGUI();
	if (wasEmpty)
		ChangeDistributionType(event);
}

void devImpurityPanel::LoadImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Impurity files (*.imp)|*.imp"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}


	//don't get rid of current wavelengths! Let them be added on. If they

	Impurity* imp = new Impurity();
	imp->LoadFromFile(file);
	
	imp->setID(-1);

	//when loaded via above, it is inverted for the calculations. We do not want to show said inversion for saving it.
	imp->adjustDonorDegeneracyForOccupationProbability();	//the conversion process is same forward/backward
	

	mdesc->impurities.push_back(imp);
	mdesc->SetUnknownIDs();
	curImp = imp;
	mdesc->Validate();
	UpdateGUIValues();
	TransferToGUI();
	/*

	wxTextFile* iFile = new wxTextFile(openDialog.GetPath());
	iFile->Open();
	std::vector<XMLLine> tags;
	parseFile(iFile, tags);
	if (tags.empty())
	{ //Not an XML file
	wxMessageDialog notXMLDiag(this, wxT("There do not appear to be any XML tags in this file"), wxT("Invalid File"));
	notXMLDiag.ShowModal();
	}
	int toptag = 0;
	if (!tags[0].label.IsSameAs(wxT("Defect"), false))
	{ //We don't have a material tag as our first tag
	toptag = tags[0].FindDescendent(wxT("Defect"), &tags);
	//find a material tag if it exists
	if (toptag < 0)
	{ //No material tags in the file. Notify the user
	wxMessageDialog noMatDiag(this, wxT("There are no defects in this XML file!"), wxT("No Defects in XML File"));
	noMatDiag.ShowModal();
	}
	}

	int count = 0;

	while (toptag >= 0)
	{
	defect def; //Don't load the ID from file
	def.accdon = tags[toptag].FindChild(wxT("donorlike"), &tags) > 0 ? true : false;
	def.reference = tags[toptag].ChildValue(wxT("type"), wxT("0"), &tags).IsSameAs(wxT("0")) ? 0 : 1;
	unsigned long tempholder;
	if (!tags[toptag].ChildValue(wxT("distribution"), wxT("0"), &tags).ToULong(&(tempholder)))
	{
	def.distribution = 0;
	}
	else if (tempholder > MAX_DISTRIBUTION)
	{
	def.distribution = 0;
	}
	else
	{
	def.distribution = (char)tempholder;
	}

	def.density = tags[toptag].ChildValue(wxT("density"), wxEmptyString, &tags);
	def.energy = tags[toptag].ChildValue(wxT("energy"), wxEmptyString, &tags);
	def.width = tags[toptag].ChildValue(wxT("width"), wxEmptyString, &tags);

	def.depth = tags[toptag].ChildValue(wxT("type"), wxT("shallow"), &tags).IsSameAs(wxT("shallow")) ? 0 : 1;

	def.sigN = tags[toptag].ChildValue(wxT("sigN"), wxEmptyString, &tags);
	def.sigP = tags[toptag].ChildValue(wxT("sigP"), wxEmptyString, &tags);

	def.name = wxT("Defect");

	materials_list[matSelection].defects.push_back(def);
	impurityChooser->Append(def.name);

	toptag = tags[toptag].FindLastChild(&tags);
	if (toptag < 0 || toptag >= (signed int)(tags.size()) - 1) break;
	while (!tags[++toptag].label.IsSameAs(wxT("Defect"), false))
	{
	if (toptag >= (signed int)(tags.size()) - 1)
	{
	toptag = -1;
	break;
	}
	}
	}
	tags.clear();
	//	delete tags;
	iFile->Close();
	delete iFile;
	if (count > 0)
	{
	impurityChooser->SetSelection(impurityChooser->GetCount() - 1);
	wxCommandEvent eventBlank;
	SwitchDefect(eventBlank);
	}
	*/
	return;
}


void devImpurityPanel::UpdateGUIValues()
{
	if (!isInitialized())
		return;
	if (curImp == nullptr)
	{
		txtExplainDistribution->SetLabel(wxT("Select or create an impurity to adjust its values!"));

		impsigman->Disable();
		impsigmap->Disable();
		impCharge->Disable();
		impDegen->Disable();
		impDensityBox->Disable();
		impEn->Disable();
		impVar->Disable();
		impOptEffCB->Disable();
		impOptEffVB->Disable();

		impurityChooser->Enable();

		referenceBox->Disable();
		depthBox->Disable();
		distBox->Disable();

		impAdd->Enable();
		impRem->Disable();
		impSave->Disable();
		impLoad->Enable();
		impDuplicate->Disable();
		impRename->Disable();

		impurityChooser->SetToolTip(wxT("Select an impurity to view/modify"));
	}
	else
	{
		impsigman->Enable();
		impsigmap->Enable();
		impCharge->Enable();
		impDegen->Enable();
		impDensityBox->Enable();
		impEn->Enable();
		impurityChooser->Enable();

		referenceBox->Enable();
		depthBox->Enable();
		depthBox->Enable(1, false);
		distBox->Enable();
		impAdd->Enable();
		impRem->Enable();
		impSave->Enable();
		impLoad->Enable();
		impDuplicate->Enable();
		impRename->Enable();

		if (curImp->isEnergyDiscrete())
			impVar->Disable();
		else 
			impVar->Enable();

		impOptEffCB->Enable();
		impOptEffVB->Enable();

		impurityChooser->SetToolTip(curImp->getName());
		
	}

	ModelDescribe* mdesc = getMDesc();
	if (mdesc && mdesc->impurities.size() > 0)
		impurityChooser->Enable();
	else
		impurityChooser->Disable();


	txtExplainDistribution->Wrap(szrEnergyInputs->GetMinSize().GetWidth());
}

void devImpurityPanel::ChangeDistributionType(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	/*
	for reference

	int which = curImp->distribution;
	switch (which)
	{
	case DISCRETE:
	distBox->SetSelection(0);
	break;
	case BANDED:
	distBox->SetSelection(1);
	break;
	case GAUSSIAN:
	distBox->SetSelection(2);
	break;
	case EXPONENTIAL:
	distBox->SetSelection(3);
	break;
	case LINEAR:
	distBox->SetSelection(4);
	break;
	case PARABOLIC:
	distBox->SetSelection(5);
	break;
	default:
	distBox->SetSelection(0);
	}
	//distBox->SetToolTip(wxT("Discrete (1 level, no variation\n
	Banded (states split into 10 levels from E-1/2Var to E+1/2Var)\n
	Gaussian (Center @ E, variation is distance to half-width\n
	discretized to 2x the variation in both directions)\n
	Exponential (full power @ E, 1/2 power @ E+Var, discretized out\n
	to 95% of the distribution area, then\n
	# of states scaled up by 1/0.95)\n
	Linear (Starts @ E, goes to 0 states @ E+Var linearly)
	\nParabolic(Max @ E, zeros out @ E�1/2Var)"));
	*/
	int selection = distBox->GetSelection();
	switch (selection)
	{
	case 1:
		curImp->setEnergyBanded();
		txtExplainDistribution->SetLabel(wxT("Energy levels evenly distributed from (E-0.5 var) to (E+0.5 var). Amount of discretization varies as simulation parameter."));
		break;
	case 2:
		curImp->setEnergyGaussian();
		//		txtExplainDistribution->SetLabel(wxT("Gaussian centered on Energy. Note, the variation is NOT the standard deviation. It is the distance required for the magnitude to cut in half of it's maximal value. The distribution is extended twice this distance on both sides to where it is ~6.25% and then abruptly cut off beyond. The discretization is then renormalized to integrate to an area of 1."));
		txtExplainDistribution->SetLabel(wxT("Gaussian centered on energy. Variation is distance to half magnitude. Gaussian cut off after 2x Variation (~6.25% magnitude). Renormalized."));
		break;
	case 3:
		curImp->setEnergyExponential();
		//		txtExplainDistribution->SetLabel(wxT("An exponential distribution with energy occuring at the maximum value. The variation is the distance for the magnitude to cut in half. It is extended out to capture 95% of the distribution, and then the discretization is renormalized to prevent losses."));
		txtExplainDistribution->SetLabel(wxT("Exponential distribution maxed at Energy. Variation is distance to half magnitude. Extends to capture 95%, and is then renormalized."));
		break;
	case 4:
		curImp->setEnergyLinear();
		txtExplainDistribution->SetLabel(wxT("The linear distribution has a maximum at energy. It varies linearly up to variation away, at which point it hits zero. Normalized appropriately."));
		break;
	case 5:
		curImp->setEnergyParabolic();
		txtExplainDistribution->SetLabel(wxT("The parabolic distribution is maximal at energy, and zero at (E - 0.5var) and (E + 0.5var). Normalized appropriately."));
		break;
	case 0:
	default:
		curImp->setEnergyDiscrete();
		txtExplainDistribution->SetLabel(wxT("One discrete energy level. Nominally zero width (occasionally 1meV to prevent divide by 0)"));
		break;
	}

	UpdateGUIValues();
}

void devImpurityPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;


	if (curImp == nullptr && mdesc)
	{
		unsigned int sel = ConvertImpIndex(0);
		if (sel < mdesc->impurities.size())	//there is a good impurity that's not a defect!
			curImp = mdesc->impurities[sel];
	}


	if (curImp == nullptr)
	{
		//there is a material, but there is no defect currently selected.
		//clear everything, but leave the defects to be chosen
		impsigman->Clear();
		impsigmap->Clear();
		impCharge->Clear();
		impDegen->Clear();
		impDensityBox->Clear();
		impEn->Clear();
		impVar->Clear();
		impOptEffCB->Clear();
		impOptEffVB->Clear();

	}
	else
	{
		//pull the data out of the impurity!
		ValueToTBox(impsigman, curImp->getElecCapture());
		ValueToTBox(impsigmap, curImp->getHoleCapture());
		ValueToTBox(impCharge, curImp->getCharge());
		ValueToTBox(impDegen, curImp->getDegeneracy());
		ValueToTBox(impDensityBox, curImp->getDensity());
		ValueToTBox(impEn, curImp->getRelativeEnergy());
		ValueToTBox(impVar, curImp->getEnergyVariation());
		ValueToTBox(impOptEffCB, curImp->getOpticalEfficiencyCB());
		ValueToTBox(impOptEffVB, curImp->getOpticalEfficiencyVB());

		if (curImp->isShallow())
			depthBox->SetSelection(0);	//corresponds to order of radio box
		else
			depthBox->SetSelection(1);

		if (curImp->isCBReference())
			referenceBox->SetSelection(0);
		else
			referenceBox->SetSelection(1);

		short which = curImp->getEnergyDistribution();
		switch (which)
		{
		case Impurity::banded:
			distBox->SetSelection(1);
			break;
		case Impurity::gaussian:
			distBox->SetSelection(2);
			break;
		case Impurity::exponential:
			distBox->SetSelection(3);
			break;
		case Impurity::linear:
			distBox->SetSelection(4);
			break;
		case Impurity::parabolic:
			distBox->SetSelection(5);
			break;
		case Impurity::discrete:
		default:
			distBox->SetSelection(0);
		}
		/*
		energyDist.Add(wxT("Discrete"));
		energyDist.Add(wxT("Banded"));
		energyDist.Add(wxT("Gaussian"));
		energyDist.Add(wxT("Exponential"));
		energyDist.Add(wxT("Linear"));
		energyDist.Add(wxT("Parabolic"));
		*/
	}

	impurityChooser->Clear();
	bool defGood = false;
	if (mdesc)
	{
		int defCount = 0;
		for (unsigned int i = 0; i < mdesc->impurities.size(); ++i)
		{
			if (mdesc->impurities[i]->isDefect())
			{
				defCount++;
				continue;
			}

			impurityChooser->AppendString(mdesc->impurities[i]->getName());
			if (mdesc->impurities[i] == curImp)
			{
				impurityChooser->SetSelection(i-defCount);
				defGood = true;
			}
		}
	}
	if (!defGood)
		curImp = nullptr;

	UpdateGUIValues();
}

void devImpurityPanel::UpdateReference(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int tmpInt = referenceBox->GetSelection();
	if (tmpInt == 0)
		curImp->setDonor();
	else
		curImp->setAcceptor();

	if (curImp->isDonor())
	{
		curImp->setDegeneracy(2.0f);
		curImp->setCharge(1);
		curImp->setOpticalEfficiencyCB(0.0);
		curImp->setOpticalEfficiencyVB(1.0);
	}
	else
	{
		curImp->setDegeneracy(4.0f);
		curImp->setCharge(-1);
		curImp->setOpticalEfficiencyCB(1.0);
		curImp->setOpticalEfficiencyVB(0.0);
	}

	ValueToTBox(impCharge, curImp->getCharge());
	ValueToTBox(impDegen, curImp->getDegeneracy());
	ValueToTBox(impOptEffCB, curImp->getOpticalEfficiencyCB());
	ValueToTBox(impOptEffVB, curImp->getOpticalEfficiencyVB());

}

void devImpurityPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curImp == nullptr)
		return;

	double tmpDouble;
	long int tmpInt;
	curImp->setElecCapture((impsigman->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setHoleCapture((impsigmap->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setCharge((impCharge->GetValue().ToLong(&tmpInt)) ? tmpInt : 0);
	curImp->setRelativeEnergy((impEn->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setEnergyVariation((impVar->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setDegeneracy((impDegen->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 1.0);
	curImp->setOpticalEfficiencyCB((impOptEffCB->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setOpticalEfficiencyVB((impOptEffVB->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);
	curImp->setDensity((impDensityBox->GetValue().ToDouble(&tmpDouble)) ? tmpDouble : 0.0);

	
	tmpInt = referenceBox->GetSelection();
	if (tmpInt == 0)
		curImp->setDonor();
	else
		curImp->setAcceptor();

	tmpInt = depthBox->GetSelection();
	if (tmpInt == 0)
		curImp->setShallow();
	else
		curImp->setDeep();

	tmpInt = distBox->GetSelection();
	switch (tmpInt)
	{
	case 1:
		curImp->setEnergyBanded();
		break;
	case 2:
		curImp->setEnergyGaussian();
		break;
	case 3:
		curImp->setEnergyExponential();
		break;
	case 4:
		curImp->setEnergyLinear();
		break;
	case 5:
		curImp->setEnergyParabolic();
		break;
	case 0:
	default:
		curImp->setEnergyDiscrete();
		break;
	}

	UpdateGUIValues();
}

unsigned int devImpurityPanel::ConvertImpIndex(int index)
{
	if (!isInitialized())
		return -1;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return(-1);	//impossible to find it.


	//index is the index going with the drop box. We need to translate it into the index for
	//the data structure
	std::vector<Impurity*>& imps = mdesc->impurities;
	int goodCtr = 0;
	for (unsigned int i = 0; i < imps.size(); ++i)
	{
		if (imps[i]->isDefect())
		{
			//then this value wasn't counted towards the good index
			continue;
		}

		if (goodCtr == index)	//they now match
			return(i);

		//this was on the list, but it wasn't the correct index, so move on to the next one
		goodCtr++;
	}
	return(-1);	//didn't find it!
}

void devImpurityPanel::RemoveImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curImp == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;
	

	int indexChoice = impurityChooser->GetSelection();	//this is going to go along with material defects
	unsigned int index = ConvertImpIndex(indexChoice);

	if (index < mdesc->impurities.size())
	{
		delete mdesc->impurities[index];
		mdesc->impurities.erase(mdesc->impurities.begin() + index);
		
		index = ConvertImpIndex(indexChoice);

		if (index < mdesc->impurities.size())	//could have been the last one?
			curImp = mdesc->impurities[index];
		else
		{
			index = ConvertImpIndex(0);
			curImp = (index < mdesc->impurities.size()) ? mdesc->impurities[index] : nullptr;
		}
	}
	mdesc->Validate();
	TransferToGUI();
	UpdateGUIValues();
	
}

void devImpurityPanel::duplicateImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curImp == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	wxString newname(curImp->getName());
	newname << "_copy";
	wxTextEntryDialog getName(this, wxT("Enter a new name"), wxT("Duplicate Impurity"), newname);
	if (getName.ShowModal() == wxID_CANCEL)
		return;

	Impurity* imp = new Impurity(*curImp);
	imp->setName(getName.GetValue().ToStdString());
	imp->setID(-1);

	mdesc->impurities.push_back(imp);
	mdesc->SetUnknownIDs();
	curImp = imp;
	TransferToGUI();
}

void devImpurityPanel::Clear()
{
	if (!isInitialized())
		return;
	impDensityBox->SetValue(wxEmptyString);
	impsigman->SetValue(wxEmptyString);
	impsigmap->SetValue(wxEmptyString);
	impCharge->SetValue(wxEmptyString);
	impEn->SetValue(wxEmptyString);
	impVar->SetValue(wxEmptyString);
	impDegen->SetValue(wxEmptyString);
	impOptEffCB->SetValue(wxEmptyString);
	impOptEffVB->SetValue(wxEmptyString);
}


void devImpurityPanel::SwitchImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	std::vector<Impurity*>& imps = mdesc->impurities;

	unsigned int newSelection = ConvertImpIndex(impurityChooser->GetSelection());
	std::string impName = impurityChooser->GetValue().ToStdString();

	Impurity* testImp = nullptr;
	if (newSelection < imps.size())
	{
		testImp = imps[newSelection];
		if (!testImp->isImpurity(impName))
			testImp = nullptr;
	}

	//could not find it easily! Search through them all!
	if (testImp == nullptr)
	{
		for (unsigned int i = 0; i < imps.size(); ++i)
		{
			if (imps[i]->isImpurity(impName))
			{
				testImp = imps[i];
				break;
			}
		}
	}

	if (testImp != curImp)
	{
		curImp = testImp;
		TransferToGUI();	//get all the correct data in there
		UpdateGUIValues();	//and then update everything appropriately
	}

}

void devImpurityPanel::saveImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (impurityChooser->IsListEmpty() || impurityChooser->GetSelection() == -1)
	{
		wxMessageDialog diag(this, wxT("There are no impurities to save!"), wxT("No Impurities"));
		diag.ShowModal();
		return; //empty
	}
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Impurity files (*.imp)|*.imp"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	if (curImp)
	{
		XMLFileOut oFile(saveDialog.GetPath().ToStdString());
		if (oFile.isOpen())
		{
			curImp->SaveToXML(oFile);
			oFile.closeFile();
		}
	}
	/*
	int defectNum = impurityChooser->GetSelection();
	wxFile* oFile = new wxFile(saveDialog.GetPath(), wxFile::write);
	if (oFile->IsOpened())
	defectToFile(&(defects_list[defectNum]), oFile);
	oFile->Flush();
	delete oFile;
	*/
}

void devImpurityPanel::VerifyInternalVars() {
	ModelDescribe *mdesc = getMDesc();
	if (mdesc == nullptr) {
		curImp = nullptr;
		return;
	}
	if (mdesc->HasImpurity(curImp) == false) {
		curImp = nullptr;
	}
}

void devImpurityPanel::RenameImpurity(wxCommandEvent& event)
{
	if (!isInitialized())
		return;

	if (curImp == nullptr)
		return;

	wxTextEntryDialog getName(this, wxT("Name"), wxT("Rename Defect"), curImp->getName());
	if (getName.ShowModal() == wxID_CANCEL)
		return;
	std::string newname = getName.GetValue();
	if (newname != "")
		curImp->setName(newname);

	TransferToGUI();
	return;
}


BEGIN_EVENT_TABLE(devImpurityPanel, wxScrolledWindow)
EVT_BUTTON(IMP_ADD, devImpurityPanel::NewImpurity)
EVT_BUTTON(IMP_REM, devImpurityPanel::RemoveImpurity)
EVT_BUTTON(IMP_RENAME, devImpurityPanel::RenameImpurity)
EVT_BUTTON(IMP_SAVE, devImpurityPanel::saveImpurity)

EVT_BUTTON(IMP_LOAD, devImpurityPanel::LoadImpurity)
EVT_BUTTON(IMP_DUPLICATE, devImpurityPanel::duplicateImpurity)
EVT_RADIOBOX(IMP_CHANGEDIST, devImpurityPanel::ChangeDistributionType)

EVT_COMBOBOX(SWITCH_SELECTION, devImpurityPanel::SwitchImpurity)

EVT_TEXT(IMP_UPDATE_TXT, devImpurityPanel::TransferToMdesc)
EVT_RADIOBOX(IMP_UPDATE_RAD, devImpurityPanel::TransferToMdesc)
EVT_RADIOBOX(IMP_SMART_REF_CHANGE, devImpurityPanel::UpdateReference)
END_EVENT_TABLE()