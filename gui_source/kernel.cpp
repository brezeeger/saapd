#include "kernel.h"

/*  this group is defined in kernel.h. This seems like it would be needed for the classes defined in kernel.h
// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

*/


#include "../kernel_source/model.h"
#include "../kernel_source/physics.h"
#include "../kernel_source/EquationSolver.h"
#include "../kernel_source/random.h"
#include "../kernel_source/transient.h"
#include "../kernel_source/ss.h"
#include "../kernel_source/output.h"
#include "../kernel_source/transient.h"
#include "../kernel_source/outputdebug.h"
#include "../kernel_source/GenerateModel.h"
#include "../kernel_source/PopulateModel.h"
#include "../kernel_source/load.h"
#include "../kernel_source/CalcBandStruct.h"
#include "../kernel_source/Equilibrium.h"
#include "../kernel_source/LoadState.h"
#include "../kernel_source/light.h"
#include "../kernel_source/SaveState.h"

//#include<vld.h>	//visual leak detector


//Global variable declarations
MyRand SciRandom;	//create a global class to be used for generating random numbers
long long int UniqueIdentifier;	//used to make every task unique.


kernelThread::kernelThread(wxString file, wxWindow* p, wxString WorkDirectory)
: wxThread(wxTHREAD_JOINABLE)
{
	filename = std::string(file.mb_str());
	parent = p;
	shareData.GUIWorkDirectory = std::string(WorkDirectory.mb_str());
	shareData.GUIabortSimulation = false;
	shareData.GUIsaveState = false;
	shareData.GUIstopSimulation = false;
	shareData.GUINext = false;
	shareData.KernelDiverge = false;
	shareData.ss = true;
	shareData.KernelMaxTStep = 0.0;
	shareData.KernelnextTimeTarget = 0.0;
	shareData.KernelsimTime = 0.0;
	shareData.KernelEndTime = 0.0;
	shareData.KernelTolerance = -1.0;
	shareData.KernelMaxTolerance = -1.0;
	shareData.KernelMinTolerance = -1.0;
	shareData.KernelTargetTolerance = -1.0;
	shareData.KernelssnumStates = 1;
	shareData.KernelsscurState = -0;
	shareData.CurSim = file;
	shareData.KernelcurInfoMsg = MSG_THERMALEQUILIBRIUM;
	shareData.KernelshortProgBarPosition = 0;
	shareData.KernelshortProgBarRange = 0;
	shareData.KernelTimeLeft = 0.0;
	shareData.ssFromTransient = false;
}

void *kernelThread::Entry()
{
	ModelDescribe mdesc;	//contains the description as to how to generate device and contains general characteristics
	Model mdl(mdesc);	//contains all characteristics of the device on a point by point basis.
//	string filename;	//filename to load into mdl
	//this is now a param
	int typeload = 0;
	int returns;
	ProgressCheck(-1, true);	//clear out the trace file.
	time_t runtimestart = time(NULL);
//	srand((unsigned int)time(NULL));	//remove the warning, explicit (unsigned int)
//srand(0);
	SciRandom.Seed(106848,24);	//just some random stuff,
	//will eventually change to: t = time(NULL); SciRandom.Seed(t,t);

	//cout << "Please enter in the model description file to load" << endl;
	//cin >> filename;
	
	typeload = Load(filename, mdesc, mdl, false);
	switch(typeload)
	{
		case -1:
			CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, SIM_FINISHED, 0);
			return NULL;
		case 0:	//normal device file load
			DisplayProgressStr(shareData.GUIWorkDirectory + "\\" + mdesc.simulation->OutputFName + "*",0);
			LinkID(mdesc);
			returns = mdesc.CheckModel();
			GlobalizeDescriptors(mdesc);
			if (returns != ModelDescribe::Error_None)
			{
				DisplayProgress(19, returns);
				CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, SIM_FINISHED, 0);
				return NULL;	//exit with failure.s
			}

			GenerateMesh1D(mdesc, mdl);
			CheckMesh(mdl, mdesc.simulation->OutputFName);	//write output of models mesh
			PopulateModel(mdesc, mdl);
			
			
			if(mdesc.simulation->ThEqDeviceStart)
			{
				ThermalEquilibrium(this, mdesc, mdl);	//get a decent answer
	//			AdaptiveMesh(mdl, mdesc);	//refine the mesh, which will screw up the answer
	//			ThermalEquilibrium(mdesc, mdl);	//get the answer with a good mesh
			}
			CalcBandStruct(mdl, mdesc);
			
			OutputContacts(mdesc.simulation, true);	//initialize contact output data (reset file headers)
#ifndef NDEBUG
			DebugTimestep(NULL, true);	//clear out the timestep debug file
			RecordNonSteadyState(mdesc.simulation, true);	//outputdebug.cpp
#endif
			OutputCharge(mdesc.simulation, true);	//output.cpp - initialize output for acquiring capacitance data
			if (mdesc.simulation && mdesc.simulation->TransientData)
				mdesc.simulation->TransientData->RecordTimestep(true);	//outputdebug.cpp
			{
//				ChargeTransfer tmp;	//this isn't even used when clearing out the file. Create it temporarily and have it disappear
//				OutputChargeTransfer(tmp, true);	//clear out the charge transfer debug file
			}
			break;
		case 1:	//loaded device state file
			LoadState(filename, mdl, mdesc);
			DisplayProgressStr(shareData.GUIWorkDirectory + "\\" + mdesc.simulation->OutputFName + "*",0);
			break;
		default:
			break;
	}

	

	WriteImpurityDist(mdl, mdesc.simulation->OutputFName);
	RecTimestepDistribution(mdesc.simulation, true);
#ifndef NDEBUG
	WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
#endif
//	DebugCLink(true, 0, false, false);

//	CarriersOut(mdesc.simulation, true);	//initialize the output data for the carrier outputs - not needed as task system done with
	
	DisplayProgress(34,0);
	


//	mdesc.simulation->InitIteration(mdl, mdesc);	//make sure the first adaptive mesh that occurs will be proper given the starting conditions

	//Update output on GUI. Just put in brackets so it's obviously a code block
	if (mdesc.simulation->SteadyStateData)
	{
		mdesc.simulation->curiter = 0;	//want this to line up with all the environment indices
		SS(mdesc, mdl, this);
		//MonteCarlo_SteadyState(mdl, mdesc, this);
		//SteadyState(mdl, mdesc, this);
		//SSNewton(mdl, mdesc, this);
		//MC_C_SS(mdl, mdesc, this);
		//SteadyStateOld(mdl, mdesc, this);
	}
	if(mdesc.simulation->TransientData)
	{
		if(typeload == 1)
		{
			mdesc.simulation->TransientData->simtime = 0.0;
			mdesc.simulation->TransientData->nexttimestep = 0.0;	//start out REALLY small because all the points are thermalized and there will be a LOT of motion
		}
		this->shareData.KernelsimTime = mdesc.simulation->TransientData->simtime;
		this->shareData.KernelnextTimeTarget = mdesc.simulation->TransientData->curTargettime;
		this->shareData.KernelMaxTStep = -1.0;

		mdesc.simulation->curiter = 1;
		mdesc.simulation->TransientData->avgTStep = 0.0;
		mdesc.simulation->TransientData->tstepctr = 0.0;

		mdesc.simulation->TransientData->percentCarrierTransfer = INIT_PERCENT_TRANSFER;

		this->shareData.KernelEndTime = mdesc.simulation->TransientData->simendtime;
		this->shareData.KernelnextTimeTarget = mdesc.simulation->TransientData->curTargettime;

		wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE);
		event.SetInt(mdesc.simulation->curiter);
		parent->GetEventHandler()->AddPendingEvent(event);

		mdesc.simulation->TransientData->TransMain(this);
//		Transient(mdl, mdesc, this);	//this calls SS(mdesc,mdl) as well
	}
	

	
	
	std::cout << std::endl << "Finished!" << std::endl;
	OutputFileList(mdesc.simulation, mdesc.simulation->OutFiles, mdesc.simulation->groupNames);

	time_t runtimeend = time(NULL);
	double timeelapse = difftime(runtimeend, runtimestart);
	DisplayProgress(63, 0);	//Display message "Simulation took "
	DisplayProgressDoub(20, timeelapse);	//display time as weeks, days, hours, minutes, and seconds
	ProgressDouble(48, timeelapse);	//put the combo of above in the trace file

	

	shareData.KernelcurInfoMsg = MSG_MEMORYCLEANUP;
	shareData.KernelshortProgBarPosition = 0;
	shareData.KernelshortProgBarRange = mdl.numpoints;
	shareData.KernelMaxChange = shareData.KernelMaxTolerance = shareData.KernelMaxTStep = shareData.KernelEndTime =
		shareData.KernelMinTolerance = shareData.KernelnextTimeTarget = shareData.KernelsimTime = shareData.KernelTargetTolerance = shareData.KernelTolerance = 0.0;
	shareData.KernelsscurState = 20; 
	shareData.KernelssnumStates = 21;	//make sure it doesn't auto close

	time_t start;
	start = time(NULL);
	for (std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		curPt->second.Clear();	//a bulk of the memory clearing will happen here. Want the dialog box to display
		shareData.KernelshortProgBarPosition++;
		SSKernelInteract(mdesc.simulation, start);
	}
	
	CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, CLEAR_MESSAGE);
	CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, SIM_FINISHED, 0);

//	finished.Lock();//wait for the main thread to acknowledge the thread will end
	//the finished.lock seems to be preventing the stop/abort from functioning and making the dialogue box go away.
	//what exactly is finished.lock doing? 

//	wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE);
//	event.SetInt(mdesc.simulation->curiter);
//	parent->GetEventHandler()->AddPendingEvent(event);

	
	return NULL;
}


time_t kernelThread::TransientKernelInteract(Simulation* sim, time_t& compTime, time_t& SaveStateTime)
{
	double timeElapse;
	time_t now = time(NULL);
	if(TestDestroy())
	{
		std::cout << "Simulation stopped";
		shareData.GUIabortSimulation = true;
		return(now);
	}
	timeElapse = difftime(now, compTime);

	if(timeElapse > 3.0)	//if tasks are taking a LONG time, notify the user that it's still running every
	{	//10 seconds

		DisplayProgress(28, sim->curiter);
		DisplayProgressDoub(2, sim->TransientData->simtime);
		DisplayProgressDoub(3, sim->TransientData->simendtime);

		shareData.KernelsimTime = sim->TransientData->simtime;
		shareData.KernelnextTimeTarget = sim->TransientData->curTargettime;
		shareData.KernelMaxTolerance = sim->TransientData->tStepControl.GetMaxTolerance();
		shareData.KernelMaxTStep = sim->TransientData->tStepControl.GetMaxTStepUsed();

		sim->TransientData->avgTStep = 0.0;
		sim->TransientData->tstepctr = 0.0;

		

		/*
		if (sim->TransientData->SSByTransient)
		{
			OutputDataInternalPost(mdl, *(sim), 0);	//add it to the tree of final results
			sim->binctr=1;	//make outputdatatofile work by telling it there's one data point
			OutputDataToFile(mdl, mdesc);
			sim->binctr=0;	//now make sure it will adjust the zero index again in the future
			//				DisplayProgress(64,0);
		}
		*/

		CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, sim->curiter);
		CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
		
		compTime = now;

	}

	if(sim->timeSaveState > 0.0)
	{
		if(difftime(now, SaveStateTime) > sim->timeSaveState)
			shareData.GUIsaveState = true;
	}

	return(now);
}


time_t kernelThread::SSKernelInteract(Simulation* sim, time_t& compTime)
{
	time_t now = time(NULL);
	if(TestDestroy())
	{
		std::cout << "Simulation stopped";
		shareData.GUIabortSimulation = true;
		return(now);
	}
	
	double timeElapse;
	
	time(&now);
	timeElapse = difftime(now, compTime);

	if(timeElapse > 1.0)	//if tasks are taking a LONG time, notify the user that it's still running every
	{	//10 seconds

//		CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, sim->curiter);
		CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
		CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
		compTime = now;

	}
	/*
	if(sim->timeSaveState > 0.0)
	{
		if(difftime(now, SaveStateTime) > sim->timeSaveState)
			shareData.GUIsaveState = true;
	}
	*/

	return(now);
}

int kernelThread::CreateParentEvent(wxEventTypeTag<wxCommandEvent> tag, enum wxIDs type, int eventInt)
{
	wxCommandEvent event(tag, type);
	event.SetInt(eventInt);
	parent->GetEventHandler()->AddPendingEvent(event);
	return(0);
}