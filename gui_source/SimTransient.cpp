#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"

#include "SimulationWindow.h"
#include "SimTransient.h"

#include "../kernel_source/model.h"
#include "../kernel_source/transient.h"
#include "../kernel_source/light.h"
#include "../kernel_source/contacts.h"
#include "../kernel_source/ss.h"
#include "../kernel_source/load.h"
#include "SimSteadyState.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif


simTransientPanel::simTransientPanel(wxWindow* parent, wxWindowID id)
	: wxPanel(parent, id), fgszrCos(nullptr)
{
	gdTimeline = new wxGrid(this, TR_GRID, wxDefaultPosition, wxDefaultSize);
	gdTimeline->CreateGrid(0, 5);
	gdTimeline->SetColLabelValue(0, "Object");	//name of contact/spectra
	gdTimeline->SetColLabelValue(1, "Start (s)");	//when it starts
	gdTimeline->SetColLabelValue(2, "End (s)");	//when this condition stops
	gdTimeline->SetColLabelValue(3, "Value");	//true, false, enable, disable, equation (=yada, +yada)
	gdTimeline->SetColLabelValue(4, "Item #");
	gdTimeline->EnableEditing(false);
	gdTimeline->SetRowLabelSize(1);

	plotTimeline = new PlotWindow(this, TR_PLOT);
	plotTimeline->SetTitle("");
	plotTimeline->HideLegend();
//	wxSize minSz(-1, 300);
//	plotTimeline->GetMPWindow()->SetSize(minSz);

	wxSize halfW(80, -1);
	wxSize SaveLoadSz(60, -1);
	btnSave = new wxButton(this, TR_SAVE, "Save", wxDefaultPosition, SaveLoadSz);
	btnLoad = new wxButton(this, TR_LOAD, "Load", wxDefaultPosition, SaveLoadSz);
	btnRemoveTimeline = new wxButton(this, TR_REMOVE, "Remove (X) from timeline");
	btnAddTimeline = new wxButton(this, TR_ADD, "Add to Timeline");

	wxArrayString elOptions;
	elOptions.Add("Select Property");
	elOptions.Add("Target Time");

	cbElementSelection = new wxComboBox(this, TR_PROP_SEL, "", wxDefaultPosition, wxDefaultSize, elOptions, wxCB_READONLY | wxCB_DROPDOWN);
	cbElementSelection->SetToolTip("Select what you want to input into the transient timeline (Contact and spectra interactions)");
	cbElementSelection->Select(0);

	wxArrayString actOptions;
	actOptions.Add("Select Modification");
	cbActionSelection = new wxComboBox(this, TR_MOD_SEL, "", wxDefaultPosition, wxDefaultSize, actOptions, wxCB_READONLY | wxCB_DROPDOWN);
	cbActionSelection->SetToolTip("Which aspect of the object should change?");
	cbActionSelection->Select(0);

	wxArrayString PlotViewOptions;
	PlotViewOptions.Add("Hide");
	PlotViewOptions.Add("Show All");
	PlotViewOptions.Add("All Spectra");
	PlotViewOptions.Add("All Contacts");
	
	cbPlotView = new wxComboBox(this, TR_PLOT_SEL, wxEmptyString, wxDefaultPosition, wxDefaultSize, PlotViewOptions, wxCB_READONLY | wxCB_DROPDOWN);
	cbPlotView->Select(1);

	cbTimelineView = new wxComboBox(this, TR_GRID_SEL, wxEmptyString, wxDefaultPosition, wxDefaultSize, PlotViewOptions, wxCB_READONLY | wxCB_DROPDOWN);
	cbTimelineView->Select(1);

	wxArrayString initCond;
	initCond.Add("Thermal EQ");
	initCond.Add("Abs Zero");
	initCond.Add("Steady State");
//	initCond.Add("Saved State");

	cbInitialConditions = new wxComboBox(this, TR_INIT_COND, wxEmptyString, wxDefaultPosition, wxDefaultSize, initCond, wxCB_READONLY | wxCB_DROPDOWN);
	cbInitialConditions->Select(0);

	wxArrayString metricTime;
	metricTime.Add(wxT("s"));
	metricTime.Add(wxT("ms"));
	metricTime.Add(wxT("\u03BCs"));	//microseconds
	metricTime.Add(wxT("ns"));
	metricTime.Add(wxT("ps"));


	cbMetricGrid = new wxComboBox(this, TR_GRID_UNIT, wxEmptyString, wxDefaultPosition, wxDefaultSize, metricTime, wxCB_READONLY | wxCB_DROPDOWN);
	cbMetricPlot = new wxComboBox(this, TR_PLOT_UNIT, wxEmptyString, wxDefaultPosition, wxDefaultSize, metricTime, wxCB_READONLY | wxCB_DROPDOWN);
	cbTimeEntryUnit = new wxComboBox(this, TR_ENTRY_UNIT, wxEmptyString, wxDefaultPosition, wxDefaultSize, metricTime, wxCB_READONLY | wxCB_DROPDOWN);
	cbTimeEntryUnit->SetToolTip(wxT("The units apply to all text entry times and the linear slope. This does NOT affect the frequency input."));
	cbMetricGrid->Select(0);
	cbMetricPlot->Select(0);
	cbTimeEntryUnit->Select(0);
	prevUnitSel = 0;

	chkSmartSave = new wxCheckBox(this, TR_SMART_SAVE, "Smart Saves");
	chkSmartSave->SetValue(true);
	chkSmartSave->SetToolTip("At every marked time, SAAPD will save the state of the entire device. Simulations may be restarted from these points. Useful if the computer is acting up on long duration simulations.");

	
	txtStartTime = new wxTextCtrl(this, TR_ADDTIMES, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtEndTime = new wxTextCtrl(this, TR_ADDTIMES, "-1.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));


	txtEndTime->SetToolTip("A negative value means hold indefinitely.");
	txtLinSlope = new wxTextCtrl(this, TR_LIN_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLinInitValue = new wxTextCtrl(this, TR_LIN_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	chkSumLin = new wxCheckBox(this, TR_LIN_SUM, "Sum");
	chkSumLin->SetToolTip("Should this add to the pre-existing parameter or reset the paramter?");

	chkSumCos = new wxCheckBox(this, TR_COS_SUM, "Sum");
	chkSumCos->SetToolTip("Should this add to the pre-existing parameter or reset the paramter?");

	txtAmplitude = new wxTextCtrl(this, TR_COS_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtFrequency = new wxTextCtrl(this, TR_COS_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtPhase = new wxTextCtrl(this, TR_COS_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtOffset = new wxTextCtrl(this, TR_COS_TEXT, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtMaxTStep = new wxTextCtrl(this, TR_SIMENDTIME, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtSimEndTime = new wxTextCtrl(this, TR_SIMENDTIME, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtTimeIgnore = new wxTextCtrl(this, TR_SIMENDTIME, "0.0", wxDefaultPosition, halfW, 0, wxTextValidator(wxFILTER_NUMERIC));

	wxArrayString lincos;
	lincos.Add(wxT("y=m(t-t\u2080)+b"));
	lincos.Add(wxT("y=A cos(2\u03C0f(t-t\u2080) + \u03B4) + C"));
	rbLinCos = new wxRadioBox(this, TR_RBOX, wxT("Function (t\u2080 \u2264 t < t\u2093)"), wxDefaultPosition, wxDefaultSize, lincos, 0, wxRA_SPECIFY_ROWS);

	stInitCond = new wxStaticText(this, wxID_ANY, "Initial Conditions");
	stEndTimeSim = new wxStaticText(this, wxID_ANY, "End Time");
	stPlot = new wxStaticText(this, wxID_ANY, "Plot:");
	stTable = new wxStaticText(this, wxID_ANY, "Table:");

	stStartTime = new wxStaticText(this, wxID_ANY, wxT("t\u2080:"));
	stStartTime->SetToolTip("Starting time for the function.");
	stEndTime = new wxStaticText(this, wxID_ANY, wxT("t\u2093:"));
	stEndTime->SetToolTip("Ending time for the function. -1 means never stop unless overwritten.");

	stM = new wxStaticText(this, wxID_ANY, wxT("m:"));
	stM->SetToolTip("Slope for the linear function.");
	stB = new wxStaticText(this, wxID_ANY, wxT("b:"));
	stB->SetToolTip("Initial value for the linear function (at the starting time)");
	stA = new wxStaticText(this, wxID_ANY, wxT("A:"));
	stA->SetToolTip("Amplitude of the cosine");
	stC = new wxStaticText(this, wxID_ANY, wxT("C:"));
	stC->SetToolTip("Offset of the cosine");
	stDelta = new wxStaticText(this, wxID_ANY, wxT("\u03B4:"));
	stDelta->SetToolTip("Phase shift of the cosine.");
	stF = new wxStaticText(this, wxID_ANY, wxT("f:"));
	stF->SetToolTip("(Hz) Frequency of the consine");
	stHelpPlotUnderstand = new wxStaticText(this, wxID_ANY, wxT("For contacts, the value uses the time to find an active segment where it '=' a value. If multiple options exist, it chooses the most recently started segment. Anything active with '+' is then added. As an '=' goes out of scope, the '+' will change what it is adding too, which is not necessarily 0!"));
	stMaxTStep = new wxStaticText(this, wxID_ANY, wxT("Max Step"));
	stMaxTStep->SetToolTip("The maximum timestep allowed the simulation will make. Invalid values will be set to 1/100th of the simulation time");
	stTimeIgnore = new wxStaticText(this, wxID_ANY, wxT("Time Ignore"));
	stTimeIgnore->SetToolTip("Don't output any data until the simulation has gone for this long.");

	szrPrimary = new wxBoxSizer(wxHORIZONTAL);
	szrMidRow = new wxFlexGridSizer(5);
	szrMidRow->AddGrowableCol(4);
	szrSaveLoad = new wxBoxSizer(wxHORIZONTAL);
	szrTimes = new wxBoxSizer(wxHORIZONTAL);
	szrLin = new wxBoxSizer(wxHORIZONTAL);
	szrTimeline = new wxBoxSizer(wxVERTICAL);
	szrLeftColumn = new wxBoxSizer(wxVERTICAL);
	szrLinearBox = new wxStaticBoxSizer(wxVERTICAL, this, "Linear");
	szrCosBox = new wxStaticBoxSizer(wxVERTICAL, this, "Cosine");
	fgszrInitEnd = new wxFlexGridSizer(2, 1, 3);
	fgszrInitEnd->AddGrowableCol(1);
	fgszrStEndTime = new wxFlexGridSizer(2, 1, 3);
	fgszrCos = new wxFlexGridSizer(4, 1, 3);
	fgszrCos->AddGrowableCol(1);
	fgszrCos->AddGrowableCol(3);

	//////////////right side


	szrMidRow->Add(btnLoad, 0, wxLEFT, 2);
	szrMidRow->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	szrMidRow->Add(stPlot, 0, wxALIGN_RIGHT | wxALL | wxALIGN_CENTER_VERTICAL, 2);
	szrMidRow->Add(cbMetricPlot, 0, wxEXPAND | wxALL, 2);
	szrMidRow->Add(cbPlotView, 0, wxEXPAND | wxALL, 2);
	
	szrMidRow->Add(btnSave, 0, wxRIGHT, 2);
	szrMidRow->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	szrMidRow->Add(stTable, 0, wxALIGN_RIGHT | wxALL | wxALIGN_CENTER_VERTICAL, 2);
	szrMidRow->Add(cbMetricGrid, 0, wxEXPAND | wxALL, 2);
	szrMidRow->Add(cbTimelineView, 0, wxEXPAND | wxALL, 2);
	
	
	

	szrTimeline->Add(szrMidRow, 0, wxEXPAND | wxALL, 0);
	szrTimeline->Add(gdTimeline, 0, wxEXPAND | wxALL, 2);
	szrTimeline->Add(plotTimeline->GetMPWindow(), 1, wxEXPAND | wxALL, 2);
	szrTimeline->Add(stHelpPlotUnderstand, 0, wxEXPAND | wxALL, 2);

	////////////////left side

	szrLin->Add(stM, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 1);
	szrLin->Add(txtLinSlope, 1, wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	szrLin->Add(stB, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 1);
	szrLin->Add(txtLinInitValue, 1, wxRIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrLinearBox->Add(szrLin, 1, wxEXPAND, 0);
	szrLinearBox->Add(chkSumLin, 1, wxALIGN_CENTER | wxTOP, 2);

	fgszrCos->Add(stA, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(txtAmplitude, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(stF, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(txtFrequency, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(stDelta, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(txtPhase, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(stC, 0, wxALIGN_CENTER_VERTICAL, 0);
	fgszrCos->Add(txtOffset, 0, wxALIGN_CENTER_VERTICAL, 0);
	szrCosBox->Add(fgszrCos, 0, wxALL, 0);
	szrCosBox->Add(chkSumCos, 0, wxALL | wxALIGN_CENTER, 2);

	fgszrInitEnd->Add(stInitCond, 0, wxALIGN_CENTER, 0);
	fgszrInitEnd->Add(chkSmartSave, 0, wxALIGN_CENTER, 0);
	fgszrInitEnd->Add(cbInitialConditions, 0, wxEXPAND, 0);
	fgszrInitEnd->Add(cbTimeEntryUnit, 0, wxEXPAND, 0);
	fgszrInitEnd->Add(stMaxTStep, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrInitEnd->Add(txtMaxTStep, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 0);
	fgszrInitEnd->Add(stEndTimeSim, 0, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL, 0);
	fgszrInitEnd->Add(stTimeIgnore, 0, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL, 0);
	fgszrInitEnd->Add(txtSimEndTime, 0, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL, 0);
	fgszrInitEnd->Add(txtTimeIgnore, 0, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL, 0);
	
	
	
	

	szrTimes->Add(stStartTime, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 1);
	szrTimes->Add(txtStartTime, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	szrTimes->Add(stEndTime, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 1);
	szrTimes->Add(txtEndTime, 0, wxALIGN_CENTER_VERTICAL, 0);
	
//	szrLeftColumn->Add(szrSaveLoad, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
	szrLeftColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLeftColumn->Add(fgszrInitEnd, 0, wxEXPAND | wxALL, 2);
//	szrLeftColumn->Add(chkSmartSave, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
	szrLeftColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLeftColumn->Add(cbElementSelection, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(cbActionSelection, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLeftColumn->Add(szrTimes, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLeftColumn->Add(rbLinCos, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(szrLinearBox, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(szrCosBox, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLeftColumn->Add(btnAddTimeline, 0, wxEXPAND | wxALL, 2);
	szrLeftColumn->Add(btnRemoveTimeline, 0, wxEXPAND | wxALL, 2);
	
	szrPrimary->Add(szrLeftColumn, 0, wxEXPAND | wxALL, 0);
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	szrPrimary->Add(szrTimeline, 1, wxEXPAND | wxALL, 0);

	lastIndexClicked = -1;

	SetSizer(szrPrimary);
//	stHelpPlotUnderstand->Wrap(gdTimeline->GetSize().GetWidth());
}

simTransientPanel::~simTransientPanel()
{
	delete plotTimeline;	//there's a lot of other stuff that needs to be deleted in there that the window just won't get.
}
simulationWindow* simTransientPanel::getMyClassParent()
{
	return (simulationWindow*)GetParent();
}
ModelDescribe* simTransientPanel::getMdesc()
{
	return getMyClassParent()->getMdesc();
}
Simulation* simTransientPanel::getSim()
{
	return getMdesc()->simulation;
}
TransientSimData* simTransientPanel::getTrans()
{
	Simulation* sim = getSim();
	if (sim->TransientData == nullptr)	//Make sure it exists if requested
		sim->TransientData = new TransientSimData(sim);
	return getSim()->TransientData;
}

void simTransientPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
}

void simTransientPanel::UpdateElementOptions()
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	ModelDescribe *mdesc = sim->mdesc;

	const std::vector<Contact*>& ctList = sim->contacts;
	const std::vector<LightSource*>& ltList = mdesc->spectrum;

	int prevSel = cbElementSelection->GetSelection();
	wxArrayString objOptions;
	objOptions.Add("Select Property");	//always 0
	objOptions.Add("Target Time");	//always 1
	for (unsigned int i = 0; i < ltList.size(); ++i)
		objOptions.Add(ltList[i]->name);
	firstContactChoice = objOptions.GetCount();
	for (unsigned int i = 0; i < ctList.size(); ++i)
		objOptions.Add(ctList[i]->name);

	cbElementSelection->Clear();
	cbElementSelection->Append(objOptions);
	
	if ((unsigned int)prevSel < objOptions.GetCount())
		cbElementSelection->Select(prevSel);
	else
		cbElementSelection->Select(0);
}

void simTransientPanel::UpdateActionOptions(bool setDef)
{
	if (!isInitialized())
		return;
	//assumes the element options is up to snuff
	int sel = cbElementSelection->GetSelection();
	if (sel <= 0) { //the default message saying do this, or nothing chosen (pretty tough to get the select none)
		cbActionSelection->Disable();
		szrLeftColumn->Hide(szrTimes);
		szrLeftColumn->Hide(rbLinCos);
		return;
	}
	if (sel == 1) {
		wxArrayString opts;
		opts.Add("Select Modification");
		opts.Add("Checkpoint");
		opts.Add("Checkpoint w/ Save");
		int prevSel = cbActionSelection->GetSelection();
		wxString pSelStr = cbActionSelection->GetStringSelection();
		
		cbActionSelection->Clear();
		cbActionSelection->Append(opts);

		if (prevSel < 3 && pSelStr == opts[prevSel] && !setDef) {
			cbActionSelection->Select(prevSel);
		}
		else {
			cbActionSelection->Select(1);
		}

		txtStartTime->Enable();
		txtEndTime->Disable();
		szrLeftColumn->Show(szrTimes);
		szrLeftColumn->Hide(rbLinCos);
		cbActionSelection->Enable();
		
		return;
	}
	if ((unsigned int)sel < firstContactChoice) {	//it's a spectra choice

		wxArrayString opts;
		opts.Add("Select Modification");
		opts.Add("Enable");

		int prevSel = cbActionSelection->GetSelection();
		wxString pSelStr = cbActionSelection->GetStringSelection();

		cbActionSelection->Clear();
		cbActionSelection->Append(opts);

		if (prevSel < 2 && pSelStr == opts[prevSel] && !setDef) {
			cbActionSelection->Select(prevSel);
		}
		else {
			cbActionSelection->Select(1);
		}

//		txtStartTime->Enable();	taken care of in update method selection
//		txtEndTime->Enable();
		szrLeftColumn->Show(szrTimes);
		szrLeftColumn->Hide(rbLinCos);
		cbActionSelection->Enable();
		return;
	}
	//at this point, it's a contact box
	wxArrayString opts;
	getContactOptions(opts);

	int prevSel = cbActionSelection->GetSelection();
	wxString pSelStr = cbActionSelection->GetStringSelection();

	cbActionSelection->Clear();
	cbActionSelection->Append(opts);

	if (prevSel < 12 && pSelStr == opts[prevSel] && prevSel != 0) {
		cbActionSelection->Select(prevSel);
	}
	else {
		cbActionSelection->Select(1);
	}
	cbActionSelection->Enable();
	

}

void simTransientPanel::getContactOptions(wxArrayString& str)
{
	str.clear();
	str.Add("Select Modification");
	str.Add("Voltage");
	str.Add("Current Density");
	str.Add("Electric Field");
	str.Add("Current");
	str.Add("Injection C. Density");
	str.Add("Injection E. Field");
	str.Add("Injection Current");
	str.Add("Contact Sink");
	str.Add("External Contact");
	str.Add("Minimize E-Field Energy");
	str.Add("Opposing D-Fields");
	return;
}

bool simTransientPanel::isNumericalContact(int sel)
{
	if (sel <= 0 || sel>7)
		return false;
	return true;
}

bool simTransientPanel::isBoolContact(int sel)
{
	if (sel <= 7 || sel>11)
		return false;
	return true;
}


int simTransientPanel::getContactFlag(int sel, bool linear) {
	if (sel <= 0)
		return CONTACT_INACTIVE;
	else if (sel == 1 && linear)
		return CONTACT_VOLT_LINEAR;
	else if (sel == 1)
		return CONTACT_VOLT_COS;
	else if (sel == 2 && linear)
		return CONTACT_CUR_DENSITY_LINEAR;
	else if (sel == 2)
		return CONTACT_CUR_DENSITY_COS;
	else if (sel == 3 && linear)
		return CONTACT_EFIELD_LINEAR;
	else if (sel == 3)
		return CONTACT_EFIELD_COS;
	else if (sel == 4 && linear)
		return CONTACT_CURRENT_LINEAR;
	else if (sel == 4)
		return CONTACT_CURRENT_COS;
	else if (sel == 5 && linear)
		return CONTACT_CUR_DENSITY_LINEAR_ED;
	else if (sel == 5)
		return CONTACT_CUR_DENSITY_COS_ED;
	else if (sel == 6 && linear)
		return CONTACT_EFIELD_LINEAR_ED;
	else if (sel == 6)
		return CONTACT_EFIELD_COS_ED;
	else if (sel == 7 && linear)
		return CONTACT_CURRENT_LINEAR_ED;
	else if (sel == 7)
		return CONTACT_CURRENT_COS_ED;
	else if (sel == 8)
		return CONTACT_SINK;
	else if (sel == 9)
		return CONTACT_CONNECT_EXTERNAL;
	else if (sel == 10)
		return CONTACT_MINIMIZE_E_ENERGY;
	else if (sel == 11)
		return CONTACT_OPP_DFIELD;
	
	return CONTACT_INACTIVE;
}

void simTransientPanel::UpdateFunctionOptions()
{
	if (!isInitialized())
		return;
	bool dispFunc = rbLinCos->IsShownOnScreen();
	if (dispFunc == false) {
		szrLeftColumn->Hide(szrLinearBox);
		szrLeftColumn->Hide(szrCosBox);
		return;
	}
	int sel = rbLinCos->GetSelection();
	if (sel == 0) {	//the linear one
		szrLeftColumn->Show(szrLinearBox);
		szrLeftColumn->Hide(szrCosBox);
	}
	else {
		szrLeftColumn->Hide(szrLinearBox);
		szrLeftColumn->Show(szrCosBox);
	}
}

void simTransientPanel::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	TransientSimData *tsd = getTrans();
	ModelDescribe *mdesc = sim->mdesc;
	
	//don't let me modify these. I just want easy references.
	const std::vector<Contact*>& ctList = sim->contacts;
	const std::vector<LightSource*>& ltList = mdesc->spectrum;

	UpdateElementOptions();
	UpdateActionOptions();
	UpdateFunctionOptions();

	if (lastIndexClicked < tableData.size()) {
		btnRemoveTimeline->Enable();
		wxString tm = "Remove ";
		tm << (lastIndexClicked + 1) << " from timeline";
		btnRemoveTimeline->SetLabel(tm);
	}
	else {
		btnRemoveTimeline->Disable();
	}

	if (cbPlotView->GetSelection() != 0 && cbTimelineView->GetSelection() != 0) {
		wxSize gr = gdTimeline->GetSize();
		wxSize pl = plotTimeline->GetWXWindow()->GetSize();
		wxPoint pt = plotTimeline->GetWXWindow()->GetPosition();
		int totHeight = gr.GetHeight() + pl.GetHeight();
		if (pl.GetHeight() < 2 * gr.GetHeight()) {
			gr.SetHeight(totHeight / 3);
			int delta = pl.GetHeight();
			pl.SetHeight(totHeight - gr.GetHeight());
			delta -= pl.GetHeight();	//presumably now a negative galue as it grew
			pt.y += delta;


			gdTimeline->SetSize(gr);
			plotTimeline->GetWXWindow()->SetSize(pl);
			plotTimeline->GetWXWindow()->SetPosition(pt);

		}
	}

//	wxSize sz(-1, -1);
	int w = gdTimeline->GetSize().GetWidth();
//	if (sz.GetWidth() < 600)
//		sz.SetWidth(600);
	stHelpPlotUnderstand->SetLabel(wxT("For contacts, the value uses the time to find an active segment where it '=' a value. If multiple options exist, it chooses the most recently started segment. Anything active with '+' is then added. As an '=' goes out of scope, the '+' will change what it is adding too, which is not necessarily 0!"));
	//the wrap function just inserts a bunch of \n's into it, so they will always remain there!
//	stHelpPlotUnderstand->SetSize(sz);
//	sz = stHelpPlotUnderstand->GetSize();
	stHelpPlotUnderstand->Wrap(w);
	szrTimeline->Layout();
}

void simTransientPanel::TransferToGUI(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	VerifyInternalVars();

	ModelDescribe *mdesc = getMdesc();
	TransientSimData *tsd = getTrans();
	if (mdesc == nullptr || tsd == nullptr)
		return;

	wxArrayString PlotViewOptions;
	PlotViewOptions.Add("Hide");
	PlotViewOptions.Add("Show All");
	PlotViewOptions.Add("All Spectra");
	PlotViewOptions.Add("All Contacts");

	TransDataDisplay tmpData(0.0, 0.0);
	tableData.clear();
	for (unsigned int i = 0; i < tsd->SpecTime.size(); ++i) {
		if (tmpData.Update(tsd->SpecTime[i], mdesc->getSpectra(tsd->SpecTime[i].SpecID))) {
			if (i + 1 < tsd->SpecTime.size() && tsd->SpecTime[i+1].SpecID == tmpData.getSpectraID() && tsd->SpecTime[i+1].enable==false) {
				tmpData.changeEndTime(tsd->SpecTime[i + 1].time);
				++i;	//already got this one, so increment past. Spectra tend to come in pairs.
			}
			tableData.push_back(tmpData);	//only add the data if it successfully updated it
		}
	}
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i)
		PlotViewOptions.Add(mdesc->spectrum[i]->name);

	plotTableViewFirstContact = PlotViewOptions.GetCount();

	for (unsigned int i = 0; i < mdesc->simulation->contacts.size(); ++i) {
		PlotViewOptions.Add(mdesc->simulation->contacts[i]->name);
		TransContact *tct = tsd->GetTransContact(mdesc->simulation->contacts[i]);
		if (tct)
			tct->UpdateGridDisplay(tableData);
	}

	//map, double, TargetTimeData
	for (auto it = tsd->TargetTimes.begin(); it != tsd->TargetTimes.end(); ++it) {
		if (tmpData.Update(it->first, it->second.SaveState))
			tableData.push_back(tmpData);
	}

	//gotta maintain the selection after clearing it all out
	unsigned int plotSel = cbPlotView->GetSelection();
	unsigned int gridSel = cbTimelineView->GetSelection();
	if (plotSel >= PlotViewOptions.GetCount())
		plotSel = 0;

	if (gridSel >= PlotViewOptions.GetCount())
		gridSel = 0;

	cbPlotView->Clear();
	cbPlotView->Append(PlotViewOptions);
	cbPlotView->Select(plotSel);

	cbTimelineView->Clear();
	cbTimelineView->Append(PlotViewOptions);
	cbTimelineView->Select(gridSel);

	VectorDoubleShrinkingBubbleSort(tableData);	//now the table is sorted in numerical order of starting times

	UpdateTable();
	UpdatePlot();

	if (event.GetEventObject() != txtMaxTStep)
		ValueToTBox(txtMaxTStep, tsd->tStepControl.GetUserMaxStep());
	if (event.GetEventObject() != txtSimEndTime)
		ValueToTBox(txtSimEndTime, tsd->simendtime);
	if (event.GetEventObject() != txtTimeIgnore)
		ValueToTBox(txtTimeIgnore, tsd->timeignore);
	chkSmartSave->SetValue(tsd->SmartSaveState);

	szrTimeline->Layout();
	UpdateGUIValues(wxCommandEvent());
}

void simTransientPanel::UpdateTable()
{
	if (!isInitialized())
		return;
	ModelDescribe *mdesc = getMdesc();
	TransientSimData *tsd = getTrans();
	if (mdesc == nullptr || tsd == nullptr)
		return;

	unsigned int selection = cbTimelineView->GetSelection();
	int ShowContactID = -1;	//if there is a particular spectra to be shown
	int ShowSpectraID = -1;
	bool ShowAllContacts;
	bool ShowAllSpectra;
	bool ShowAllOther;

	if (selection == 0) {	//hide the table
		gdTimeline->Hide();
		szrTimeline->Layout();
		return;
	}
	else if (selection == 1) {	//show all
		ShowAllContacts = ShowAllOther = ShowAllSpectra = true;
	}
	else if (selection == 2) {	//show all spectra
		ShowAllOther = ShowAllContacts = false;
		ShowAllSpectra = true;
	}
	else if (selection == 3) {	//show all contracts
		ShowAllOther = ShowAllSpectra = false;
		ShowAllContacts = true;
	}
	else if (selection < plotTableViewFirstContact) {	//it's a spectra
		selection -= 4;	//there are 4 default options. Then it goes through the spectra...
		ShowAllOther = ShowAllSpectra = ShowAllContacts = false;
		if (selection < mdesc->spectrum.size())
			ShowSpectraID = mdesc->spectrum[selection]->ID;
	}
	else { //it's a specific contact being viewed.
		selection -= plotTableViewFirstContact;
		ShowAllOther = ShowAllSpectra = ShowAllContacts = false;
		if (selection < mdesc->simulation->contacts.size())
			ShowContactID = mdesc->simulation->contacts[selection]->id;
	}

	gdTimeline->Show();

	if (gdTimeline->GetNumberRows() > 0)
		gdTimeline->DeleteRows(0, gdTimeline->GetNumberRows());
	if (ShowContactID == -1 && ShowSpectraID == -1 && !ShowAllContacts && !ShowAllOther && !ShowAllSpectra)
		return;	//there is nothing else to do here. Nothing is displayed.

	double factor = 1.0f;
	int unitSel = cbMetricGrid->GetSelection();
	wxString unit = "s";
	if (unitSel == 1) {
		factor = 1000.0;
		unit = "ms";
	}
	else if (unitSel == 2) {
		factor = 1.0e6;
		unit = wxT("\u03BCs");
	}
	else if (unitSel == 3) {
		factor = 1.0e9;
		unit = "ns";
	}
	else if (unitSel == 4) {
		factor = 1.0e12;
		unit = "ps";
	}
	wxString tmp = "Start (";
	tmp << unit << ")";
	gdTimeline->SetColLabelValue(1, tmp);
	tmp = "End (";
	tmp << unit << ")";
	gdTimeline->SetColLabelValue(2, tmp);

	//otherwise, time to get cracking.
	for (unsigned int i = 0; i < tableData.size(); ++i) {
		bool add = false;
		if (ShowAllContacts && tableData[i].isContact())
			add = true;
		else if (ShowAllSpectra && tableData[i].isSpectra())
			add = true;
		else if (ShowAllOther && tableData[i].isNotContactNorSpectra())
			add = true;
		else if (ShowContactID != -1 && tableData[i].getContactID() == ShowContactID)
			add = true;
		else if (ShowSpectraID != -1 && tableData[i].getSpectraID() == ShowSpectraID)
			add = true;

		if (add) {
			unsigned int rowNum = gdTimeline->GetNumberRows();
			gdTimeline->AppendRows();	//
			wxString tmp = "";
			double tm;

			gdTimeline->SetCellValue(rowNum, 0, tableData[i].getName());
			tm = tableData[i].getStartTime() * factor;
			tmp << tm << " " << unit;
			gdTimeline->SetCellValue(rowNum, 1, tmp);
			tmp = "";
			tm = tableData[i].getEndTime() * factor;
			if (tm >= 0.0)
			{
				tmp << tm << " " << unit;
				gdTimeline->SetCellValue(rowNum, 2, tmp);
			}
			else
				gdTimeline->SetCellValue(rowNum, 2, "Indefinite");
			gdTimeline->SetCellValue(rowNum, 3, tableData[i].getValue());
			tmp = "";
			tmp << (i+1);
			gdTimeline->SetCellValue(rowNum, 4, tmp);	//an index to make removals SOOO much easier.
		}
	}
	
	SmartResizeGridColumns(gdTimeline);

	szrTimeline->Layout();
}

void simTransientPanel::AddToTimeline(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int objSel = cbElementSelection->GetSelection();
	if (objSel <= 0)	//no object selected
		return;

	if (objSel == 1) {
		AddTTimeToTimeline();
	}
	else if ((unsigned int)objSel < firstContactChoice) {
		//it is a spectra
		objSel -= 2;	//now the indices line up with the spectra
		AddSpectraToTimeline(objSel);
	}
	else {
		objSel -= firstContactChoice;	//now the indices line up with the simulation contacts
		AddContactToTimeline(objSel);
	}
	TransferToGUI(event);
}

void simTransientPanel::AddTTimeToTimeline()
{
	if (!isInitialized())
		return;
	int sel = cbActionSelection->GetSelection();
	if (sel <= 0 || sel > 2)
		return;	//nothing actually selected, or mystery selected?!

	TransientSimData *tSim = getTrans();
	if (tSim == nullptr)
		return;

	int unitSel = cbTimeEntryUnit->GetSelection();
	double factor = 1.0;	//convert to sec
	if (unitSel == 1)	//ms
		factor = 1.0e-3;
	else if (unitSel == 2)	//us
		factor = 1.0e-6;
	else if (unitSel == 3)	//ns
		factor = 1.0e-9;
	else if (unitSel == 4)	//ps
		factor = 1.0e-12;

	TargetTimeData dat;
	dat.SSTarget = -1;
	dat.SaveState = (sel == 2);
	double time;
	if (txtStartTime->GetValue().ToDouble(&time) == true) {
		if (time < 0.0)
			time = 0.0;
		time *= factor;
		MapInsertReplace(tSim->TargetTimes, time, dat);

//		TransferToGUI(wxCommandEvent());
	}


}
void simTransientPanel::AddContactToTimeline(int ind)
{
	if (!isInitialized())
		return;
	Simulation* sim = getSim();
	if (sim == nullptr)
		return;
	if ((unsigned int)ind >= sim->contacts.size())	//not any particular contact
		return;

	Contact* ctc = sim->contacts[ind];
	if (ctc == nullptr)	//this should never happen!
		return;

	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;

	TransContact *ct = tsd->GetTransContact(ctc);
	if (ct == nullptr) {
		ct = new TransContact(ctc);
		tsd->ContactData.push_back(ct);
	}

	int sel = cbActionSelection->GetSelection();

	int unitSel = cbTimeEntryUnit->GetSelection();
	double factor = 1.0;	//convert to sec
	if (unitSel == 1)	// ms
		factor = 1.0e-3;
	else if (unitSel == 2)	//us
		factor = 1.0e-6;
	else if (unitSel == 3)	//ns
		factor = 1.0e-9;
	else if (unitSel == 4)	//ps
		factor = 1.0e-12;

	//now we have the contact we're adding information to. Time to get ze information...
	
	if (isNumericalContact(sel)) {
		int lincos = rbLinCos->GetSelection();
		if (lincos == 0) {	//linear

			
			int curActive = getContactFlag(sel, true);
			double sT, eT, slope, cnst;

			if (txtStartTime->GetValue().ToDouble(&sT) == false)
				return;
			if (txtEndTime->GetValue().ToDouble(&eT) == false)
				return;
			if (txtLinSlope->GetValue().ToDouble(&slope) == false)
				return;
			if (txtLinInitValue->GetValue().ToDouble(&cnst) == false)
				return;

			eT *= factor;

			if (sT < 0.0)
				sT = 0.0;

			if (eT < 0.0)	//less than zero means go forever
				eT = -1.0;

			sT *= factor;
			if (eT <= sT && eT != -1.0)	//check for positive duration
				return;

			bool sum = chkSumLin->GetValue();
			//don't actually care what slope and const are, just that they're legit
			slope /= factor;

			LinearTimeDependence ltd(curActive, sT, eT, cnst, slope, sum, 0.0);
			ct->AddTimeDependence(ltd);
		}
		else if (lincos == 1) {	//cosine
			int curActive = getContactFlag(sel, false);
			double sT, eT, A, offset, phase, freq;
			

			if (txtStartTime->GetValue().ToDouble(&sT) == false)
				return;
			if (txtEndTime->GetValue().ToDouble(&eT) == false)
				return;
			if (txtAmplitude->GetValue().ToDouble(&A) == false)
				return;
			if (txtFrequency->GetValue().ToDouble(&freq) == false)
				return;
			if (txtOffset->GetValue().ToDouble(&offset) == false)
				return;
			if (txtPhase->GetValue().ToDouble(&phase) == false)
				return;
			
			eT *= factor;

			if (sT < 0.0)
				sT = 0.0;

			if (eT < 0.0)	//less than zero means go forever
				eT = -1.0;

			sT *= factor;

			if (eT <= sT && eT != -1.0)	//check for positive duration
				return;

			bool sum = chkSumCos->GetValue();
			CosTimeDependence ctd(A, freq, phase, offset, sT, eT, 0.0, sum, curActive);
			ct->AddTimeDependence(ctd);
		}
	}	//end contact being numerical
	else if (isBoolContact(sel)) {
		int curActive = getContactFlag(sel, false);
		double sT, eT;

		if (txtStartTime->GetValue().ToDouble(&sT) == false)
			return;

		if (txtEndTime->GetValue().ToDouble(&eT) == false)
			return;

		eT *= factor;

		if (sT < 0.0)
			sT = 0.0;

		if (eT < 0.0)	//less than zero means go forever
			eT = -1.0;

		sT *= factor;

		if (eT <= sT && eT != -1.0)	//check for positive duration
			return;

		

		TimeDependence td(sT, eT, curActive);
		ct->AddTimeDependence(td);

	}

	ct->BuildActiveValues();

}
void simTransientPanel::AddSpectraToTimeline(int ind)
{
	if (!isInitialized())
		return;
	ModelDescribe *mdesc = getMdesc();
	TransientSimData *tsd = getTrans();
	if (mdesc == nullptr || tsd==nullptr)
		return;

	if ((unsigned int)ind >= mdesc->spectrum.size())
		return;

	LightSource *lsrc = mdesc->spectrum[ind];
	if (lsrc == nullptr)
		return;

	int sel = cbActionSelection->GetSelection();
	if (sel != 1)	//the enable...
		return;

	//well, we know it's set to enable. That's the only option...
	double sT, eT;

	if (txtStartTime->GetValue().ToDouble(&sT) == false)
		return;
	if (txtEndTime->GetValue().ToDouble(&eT) == false)
		eT=-1.0;	//go on forever
	
	int unitSel = cbTimeEntryUnit->GetSelection();
	double factor = 1.0;	//convert to sec
	if (unitSel == 1)	// ms
		factor = 1.0e-3;
	else if (unitSel == 2)	//us
		factor = 1.0e-6;
	else if (unitSel == 3)	//ns
		factor = 1.0e-9;
	else if (unitSel == 4)	//ps
		factor = 1.0e-12;

	eT *= factor;
	if (sT < 0.0)
		sT = 0.0;

	if (eT < 0.0)	//less than zero means go forever
		eT = -1.0;

	sT *= factor;

	if (eT <= sT && eT != -1.0)	//check for positive duration
		return;



	SpectraTimes tmpSpec;
	tmpSpec.enable = true;
	tmpSpec.SpecID = lsrc->ID;
	tmpSpec.time = sT;

	tsd->SpecTime.push_back(tmpSpec);
	if (eT > 0.0) {
		tmpSpec.enable = false;
		tmpSpec.time = eT;
		tsd->SpecTime.push_back(tmpSpec);
	}

}

void simTransientPanel::SaveTransient(wxCommandEvent& event)
{
	if (!isInitialized())
		return;

	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;
	Simulation * sim = tsd->getSim();

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Transient files (*.trns)|*.trns"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	//it should be entirely up to date... transfer to mdesc won't do much good!

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		tsd->SaveToXML(oFile);
		oFile.closeFile();

	}
}

void simTransientPanel::LoadTransient(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;
	Simulation * sim = tsd->getSim();


	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Transient files (*.trns)|*.trns"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	unsigned int contactStartAdd = sim->contacts.size();
	LoadTransientChildren(file, *mdesc);
	file.close();

	//now need to try and pair with the contacts as best as possible...
	std::vector<int> goodLoadContacts;
	std::vector<unsigned int> badContactDataIndices;
	for (unsigned int i = 0; i < tsd->ContactData.size(); ++i) {	//there can only be one here!
		if (tsd->ContactData[i]->GetContact())
			goodLoadContacts.push_back(tsd->ContactData[i]->GetContactID());
		else 
			badContactDataIndices.push_back(i);
	}

	std::vector<unsigned int> AvailSimContactIndices;
	for (unsigned int i = 0; i < sim->contacts.size(); ++i) {
		if (tsd->GetTransContact(sim->contacts[i]) == nullptr)	//could not find the contact being associated with anything in tsd!
			AvailSimContactIndices.push_back(i);
	}

	unsigned int badSize = badContactDataIndices.size();

	bool displayCautionMessageNotEnough = false;
	bool displayCautionMessageRandom = (badSize > 0 && AvailSimContactIndices.size() > 0);

	for (unsigned int i = 0; i < badSize && i < AvailSimContactIndices.size(); ++i)
		tsd->ContactData[badContactDataIndices[i]]->SetContact(sim->contacts[AvailSimContactIndices[i]]);	//it's just randomly assigned...
	
	//now whatever is left, is bad.
	int numDelete = badSize - AvailSimContactIndices.size();
	if (numDelete > 0) {
		//badContactDataIndices is sorted. So we can safely delete and pop off without screwing up the other idnices
		displayCautionMessageNotEnough = true;
		for (int i = 1; i <= numDelete; ++i) {
			delete tsd->ContactData[badContactDataIndices[badSize - i]];
			tsd->ContactData.erase(tsd->ContactData.begin() + badContactDataIndices[badSize - i]);
		}
	}

	goodLoadContacts.clear();
	badContactDataIndices.clear();
	AvailSimContactIndices.clear();

	//now about the spectra...
	std::vector<int> AvailSpectra, UsedSpectra, needAssignSpectra;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i)
		AvailSpectra.push_back(mdesc->spectrum[i]->ID);
	for (unsigned int i = 0; i < tsd->SpecTime.size(); ++i)
	{
		if (VectorUniqueAdd(UsedSpectra, tsd->SpecTime[i].SpecID)) {	//first time adding it to used
			bool needsNew = true;
			for (unsigned int j = 0; j < AvailSpectra.size(); ++j) {
				if (AvailSpectra[j] == tsd->SpecTime[i].SpecID) {
					AvailSpectra.erase(AvailSpectra.begin() + j);
					needsNew = false;
					break;
				}
			}
			if (needsNew)
				needAssignSpectra.push_back(tsd->SpecTime[i].SpecID);
		}
	}

	//now just going to reassign as much as possible of things that haven't been assigned.
	bool displaySpectraNotEnough = needAssignSpectra.size() > AvailSpectra.size();
	bool displaySpectraRandom = false;

	for (unsigned int i = 0; i < tsd->SpecTime.size(); ++i) {
		bool good = false;
		for (unsigned int j = 0; j < UsedSpectra.size(); ++j) {
			if (UsedSpectra[j] == tsd->SpecTime[i].SpecID) {
				good = true;
				break;
			}
		}
		if (good)
			continue;	//all good. nothing needs to be done.

		for (unsigned int j = 0; j < needAssignSpectra.size(); ++j) {
			if (needAssignSpectra[j] == tsd->SpecTime[i].SpecID) {	//found one that needs changing
				if (j < AvailSpectra.size()) {	//there is something available to change it to
					tsd->SpecTime[i].SpecID = AvailSpectra[j];	//so change it.
					displaySpectraRandom = true;
					break;
				}
				else {
					//can't reassign. So just delete. sigh.
					tsd->SpecTime.erase(tsd->SpecTime.begin() + i);
					--i;	//don't screw up the counter from the removal
					break;	//stop trying to find the one it matches
				}
			}
		}
	}

	if (displayCautionMessageNotEnough) {
		wxMessageDialog mes(this, "CAUTION: This transient file attempted to load a simulation with more contacts than are currently defined.\n\nExcess data is lost, and this simulation requires editing to achieve similar results.");
		mes.ShowModal();
	}
	if (displayCautionMessageRandom) {
		wxMessageDialog mes(this, "CAUTION: This transient file loaded a file where not all contacts matched the present ones. The unmatched file contacts were paired with those in the simulation not associated with the file.\n\nDouble check to make sure this caused the desired results before running the simulation.");
		mes.ShowModal();
	}

	if (displaySpectraNotEnough) {
		wxMessageDialog mes(this, "CAUTION: This transient file attempted to load a simulation with more spectra than are currently defined.\n\nExcess data is lost, and this simulation requires editing to achieve similar results.");
		mes.ShowModal();
	}
	if (displaySpectraRandom) {
		wxMessageDialog mes(this, "CAUTION: This transient file loaded a simulation where not all spectra matched the present ones. The unmatched file spectra were paired with those in the simulation not associated with the file.\n\nDouble check to make sure the spectra in the file was paired correctly with the present simulation.");
		mes.ShowModal();
	}

	mdesc->Validate();
	TransferToGUI(event);

}

void simTransientPanel::VerifyInternalVars() {
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim) {
		sim->Validate();

		if (ss_Env) {
			if (sim->SteadyStateData == nullptr)
				ss_Env = nullptr;
			else {
				bool good = false;
				for (unsigned int i = 0; i < sim->SteadyStateData->ExternalStates.size(); ++i) {
					if (ss_Env == sim->SteadyStateData->ExternalStates[i]) {
						good = true;
						break;
					}
				}
				if (!good)
					ss_Env = nullptr;
			}
		}
		else
			ss_Env = nullptr;
	}
	else {
		ss_Env = nullptr;
		tableData.clear();
	}
	
}

void simTransientPanel::RemoveItem(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (lastIndexClicked >= tableData.size()) {
		btnRemoveTimeline->Disable();
		return;	//it's not valid
	}

	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;

	bool removeVect = false;
	TransDataDisplay& dat = tableData[lastIndexClicked];	//just simplify getting at stuff...
	if (dat.isNotContactNorSpectra()) {
		//need to remove a target time.
		double time = dat.getStartTime();
		std::map<double, TargetTimeData>::iterator it = tsd->TargetTimes.find(time);
		if (it != tsd->TargetTimes.end()) {
			tsd->TargetTimes.erase(it);
			removeVect = true;
		}
	}
	else if (dat.isSpectra()) {
		double sTime = dat.getStartTime();
		double eTime = dat.getEndTime();
		int id = dat.getSpectraID();
		for (unsigned int i = 0; i < tsd->SpecTime.size(); ++i) {
			if (tsd->SpecTime[i].SpecID == id)
			{
				if (tsd->SpecTime[i].enable == true && tsd->SpecTime[i].time == sTime)
				{
					tsd->SpecTime.erase(tsd->SpecTime.begin() + i);
					removeVect = true;
					--i;
					continue;
				}
				if (tsd->SpecTime[i].enable == false && tsd->SpecTime[i].time == eTime)
				{
					tsd->SpecTime.erase(tsd->SpecTime.begin() + i);
					removeVect = true;
					--i;
					continue;
				}
			}
		}
	}
	else if (dat.isContact()) {
		double sTime = dat.getStartTime();
		double eTime = dat.getEndTime();
		int id = dat.getContactID();
		TransContact *ct = tsd->GetTransContact(id);
		if (ct) {
			int flag = dat.getFlag();	//this will tell which thing in contact to look at...
			removeVect = ct->RemoveTimeDependence(sTime, eTime, flag);
		}
	}

	if (removeVect)
		TransferToGUI(event);	//this will reset table data, the plot, and the grid

}

void simTransientPanel::UpdateGridUnits(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	UpdateTable();	//all the values need to be changed. Guess this is just a wrapper.
}

void simTransientPanel::UpdateTimeUnits(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	TransientSimData* tsd = getTrans();
	
	int unitSel = cbTimeEntryUnit->GetSelection();
	if (unitSel == prevUnitSel)
		return;
	double prevFactor = 1.0;	//convert to seconds
	double factor = 1.0;	//convert to other
	if (unitSel == 1)	//ms
		factor = 1000.0;
	else if (unitSel == 2)	//us
		factor = 1.0e6;
	else if (unitSel == 3)	//ns
		factor = 1.0e9;
	else if (unitSel == 4)	//ps
		factor = 1.0e12;

	if (prevUnitSel == 1)	//convert to seconds
		prevFactor = 1e-3;
	else if (prevUnitSel == 2)
		prevFactor = 1.0e-6;
	else if (prevUnitSel == 3)
		prevFactor = 1.0e-9;
	else if (prevUnitSel == 4)
		prevFactor = 1.0e-12;

	prevUnitSel = unitSel;

	double tmp = tsd->simendtime;
	tmp *= factor;
	ValueToTBox(txtSimEndTime, tmp);
	
	if (txtStartTime->GetValue().ToDouble(&tmp)) {
		tmp *= prevFactor * factor;
		ValueToTBox(txtStartTime, tmp);
	}

	if (txtSimEndTime->GetValue().ToDouble(&tmp)) {
		tmp *= prevFactor * factor;
		if (tmp < 0)
			tmp = -1.0;
		ValueToTBox(txtSimEndTime, tmp);
	}

	if (txtLinSlope->GetValue().ToDouble(&tmp)) {
		tmp /= (prevFactor * factor);	//slope has units of 1/s here. Need to divide to display the proper thing
		ValueToTBox(txtLinSlope, tmp);
	}

	if (txtTimeIgnore->GetValue().ToDouble(&tmp)) {
		tmp *= prevFactor * factor;
		ValueToTBox(txtTimeIgnore, tmp);
	}

	if (txtMaxTStep->GetValue().ToDouble(&tmp)) {
		tmp *= prevFactor * factor;
		ValueToTBox(txtMaxTStep, tmp);
	}
	
}

void simTransientPanel::UpdatePlotUnits(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	UpdatePlot();
}

void simTransientPanel::UpdateProperySelection(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	//this occurs when the primary object is changed.
	UpdateActionOptions(true);	//need to update the secondary objects
	UpdateMethodSelection(event);
	szrLeftColumn->Layout();
}

void simTransientPanel::UpdateMethodSelection(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int sel = cbElementSelection->GetSelection();
	if (sel <= 1)	//nothing changes for these values
		return;

	int prevSel = cbActionSelection->GetSelection();
	if ((unsigned int)sel < firstContactChoice) {
		if (prevSel == 0) {
			txtStartTime->Disable();
			txtEndTime->Disable();
		}
		else {
			txtStartTime->Enable();
			txtEndTime->Enable();
		}
	}
	else {
		if (prevSel == 0) {
			szrLeftColumn->Hide(rbLinCos);
			szrLeftColumn->Hide(szrTimes);
			
		}
		else if (prevSel < 8) { // a numerical boundary condition
			szrLeftColumn->Show(rbLinCos);
			szrLeftColumn->Show(szrTimes, true, true);
			txtEndTime->Enable();
			txtStartTime->Enable();
		}
		else { //a contact setting or the e-field/d-field boundary conditions
			szrLeftColumn->Show(szrTimes, true, true);
			szrLeftColumn->Hide(rbLinCos);
			txtEndTime->Enable();
			txtStartTime->Enable();
		}
	}
	UpdateFunctionOptions();
	szrLeftColumn->Layout();
}

void simTransientPanel::UpdateInitialCondition(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;

	int sel = cbInitialConditions->GetSelection();
	if (sel == 0)
		tsd->StartingCondition = START_THEQ;
	else if (sel == 1)
		tsd->StartingCondition = START_ABSZERO;
	else if(sel == 2) {
		ss_Env = getMyClassParent()->getSSPanel()->getCurEnv();
		if (ss_Env == nullptr) {
			tsd->StartingCondition = START_SS;	//then when saving, it will choose the very first one!
			cbInitialConditions->SetToolTip("SAAPD could not determine which steady state you wanted. By default, it will choose the first one loaded. It chooses the selected environment on the steady state define page.");
		}
		else
		{
			tsd->StartingCondition = ss_Env->id;

			if (cbInitialConditions->GetCount() > 3) {
				cbInitialConditions->Select(3);
				cbInitialConditions->SetStringSelection(ss_Env->FileOut);
			}
			else {
				cbInitialConditions->AppendString(ss_Env->FileOut);
				cbInitialConditions->Select(3);
			}
			cbInitialConditions->SetToolTip("Steady state environment successfully chosen.");
		}
	}
	else if (sel == 3) {
		if (ss_Env) {
			tsd->StartingCondition = ss_Env->id;
			cbInitialConditions->SetToolTip("Steady state environment successfully chosen.");
		}
		else {
			tsd->StartingCondition = START_SS;
			cbInitialConditions->Delete(3);
			cbInitialConditions->SetToolTip("SAAPD could not determine which steady state you wanted. By default, it will choose the first one loaded. It chooses the selected environment on the steady state define page.");
		}
	}
	if (sel < 2)
		cbInitialConditions->SetToolTip("");


}

void simTransientPanel::UpdateSmartSave(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;

	tsd->SmartSaveState = chkSmartSave->GetValue();
}

void simTransientPanel::UpdateSimEndTime(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return;

	int unitSel = cbTimeEntryUnit->GetSelection();
	double factor = 1.0;	//convert to sec
	if (unitSel == 1)	//ms
		factor = 1.0e-3;
	else if (unitSel == 2)	//us
		factor = 1.0e-6;
	else if (unitSel == 3)	//ns
		factor = 1.0e-9;
	else if (unitSel == 4)	//ps
		factor = 1.0e-12;

	double tmp;
	if (event.GetEventObject() == txtSimEndTime) {
		if (txtSimEndTime->GetValue().ToDouble(&tmp)) {	//don't really care about error checking here
			if (tmp > 0.0) {
				tsd->simendtime = tmp * factor;
				TransferToGUI(event);	//need to update the plot data. The endpoint just changed on the plot
			}
		}
	}
	else if (event.GetEventObject() == txtMaxTStep) {
		if (txtMaxTStep->GetValue().ToDouble(&tmp)) {	//don't really care about error checking here
			if (tmp > 0.0) {
				tmp *= factor;
				tsd->tStepControl.SetUserMaxTStep(tmp);
			}
		}
	}
	else if (event.GetEventObject() == txtTimeIgnore) {
		if (txtTimeIgnore->GetValue().ToDouble(&tmp)) {	//don't really care about error checking here
			if (tmp > 0.0) {
				tmp *= factor;
				tsd->timeignore = tmp;
			}
		}
	}
	
}

void simTransientPanel::UpdateStartEndTimes(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	CheckAddButton();

	
}

void simTransientPanel::CheckAddButton()
{
	if (!isInitialized())
		return;
	double st, end;

	if (txtStartTime->GetValue().ToDouble(&st) == false) {
		btnAddTimeline->Disable();
		return;
	}
	if (st < 0.0) {
		btnAddTimeline->Disable();
		return;
	}
	//start is good...
	if (txtEndTime->IsEnabled()) {
		if (txtEndTime->GetValue().ToDouble(&end) == false) {
			btnAddTimeline->Disable();
			return;
		}

		if (end < st && end > 0.0) {
			btnAddTimeline->Disable();
			return;
		}
	}
	bool good = true;
	if (szrLeftColumn->IsShown(szrLinearBox)) {
		double tmp;
		good &= txtLinInitValue->GetValue().ToDouble(&tmp);
		good &= txtLinSlope->GetValue().ToDouble(&tmp);
	}
	else if (szrLeftColumn->IsShown(szrCosBox)) {
		double tmp;	//just care that there are valid numbers in there
		good &= txtAmplitude->GetValue().ToDouble(&tmp);
		good &= txtFrequency->GetValue().ToDouble(&tmp);
		good &= txtOffset->GetValue().ToDouble(&tmp);
		good &= txtPhase->GetValue().ToDouble(&tmp);
	}
	

	btnAddTimeline->Enable(good);

	return;
}

void simTransientPanel::UpdateCosText(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	CheckAddButton();

	return;
}

void simTransientPanel::UpdateCosCheck(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	CheckAddButton();
	return;	//doesn't matter.
}

void simTransientPanel::UpdateLinText(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	CheckAddButton();
}

void simTransientPanel::UpdateLinCheck(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	CheckAddButton();
}

void simTransientPanel::UpdateRadioBox(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int sel = rbLinCos->GetSelection();
	if (sel == 0) { //linear 
		szrLeftColumn->Show(szrLinearBox);
		szrLeftColumn->Hide(szrCosBox);
	}
	else if (sel == 1)
	{
		szrLeftColumn->Hide(szrLinearBox);
		szrLeftColumn->Show(szrCosBox);
	}
	szrLeftColumn->Layout();
}

void simTransientPanel::GridClick(wxGridEvent& event) {
	if (!isInitialized())
		return;

	if (event.GetEventObject() != gdTimeline)
		return;

	int row = event.GetRow();
	wxString tmp = gdTimeline->GetCellValue(row, 4);
	unsigned long t;
	if (tmp.ToULong(&t)) {
		lastIndexClicked = t-1;
		tmp = "Remove ";
		tmp << t << " from timeline";
		btnRemoveTimeline->SetLabel(tmp);
		btnRemoveTimeline->Enable();
	}


}

void simTransientPanel::UpdatePlot() {
	if (!isInitialized())
		return;
	//well, this won't be a bitch...
	ModelDescribe *mdesc = getMdesc();
	TransientSimData *tsd = getTrans();
	if (mdesc == nullptr || tsd == nullptr || tsd->simendtime <= 0.0)
		return;

	unsigned int selection = cbPlotView->GetSelection();
	int ShowContactID = -1;	//if there is a particular spectra to be shown
	int ShowSpectraID = -1;
	bool ShowAllContacts;
	bool ShowAllSpectra;
	bool ShowAllOther;

	if (selection == 0) {	//hide the table
		plotTimeline->GetMPWindow()->Hide();
		szrTimeline->Layout();
		return;
	}
	else if (selection == 1) {	//show all
		ShowAllContacts = ShowAllOther = ShowAllSpectra = true;
	}
	else if (selection == 2) {	//show all spectra
		ShowAllOther = ShowAllContacts = false;
		ShowAllSpectra = true;
	}
	else if (selection == 3) {	//show all contracts
		ShowAllOther = ShowAllSpectra = false;
		ShowAllContacts = true;
	}
	else if (selection < plotTableViewFirstContact) {	//it's a spectra
		selection -= 4;	//there are 4 default options. Then it goes through the spectra...
		ShowAllOther = ShowAllSpectra = ShowAllContacts = false;
		if (selection < mdesc->spectrum.size())
			ShowSpectraID = mdesc->spectrum[selection]->ID;
	}
	else { //it's a specific contact being viewed.
		selection -= plotTableViewFirstContact;
		ShowAllOther = ShowAllSpectra = ShowAllContacts = false;
		if (selection < mdesc->simulation->contacts.size())
			ShowContactID = mdesc->simulation->contacts[selection]->id;
	}

	plotTimeline->GetMPWindow()->Show();

	if (ShowContactID == -1 && ShowSpectraID == -1 && !ShowAllContacts && !ShowAllOther && !ShowAllSpectra)
	{
		//clear out all the plot data!
		while (plotTimeline->RemoveDataVector(0))
			continue;	//show this to intentionally clear everything out
		plotTimeline->UpdatePlot();
		return;	//there is nothing else to do here. Nothing is displayed.
	}

	double factor = 1.0f;
	int unitSel = cbMetricPlot->GetSelection();
	wxString unit = "s";
	if (unitSel == 1) {
		factor = 1000.0;
		unit = "ms";
	}
	else if (unitSel == 2) {
		factor = 1.0e6;
		unit = wxT("\u03BCs");
	}
	else if (unitSel == 3) {
		factor = 1.0e9;
		unit = "ns";
	}
	else if (unitSel == 4) {
		factor = 1.0e12;
		unit = "ps";
	}

	plotTimeline->SetXAxisName(unit);


	plotTimeline->RemoveAllData();	//about to put in the updated data
	double startOffset = 0.0;
	//now the fun/hard part.
	double min, max, tmpMin, tmpMax;	//intelligently offset the spectra as needed
	min = 1e300;
	max = -1e300;
	int plotsDone = 0;
	if (ShowAllContacts) {
		for (unsigned int i = 0; i < tsd->ContactData.size(); ++i) {
			unsigned int Add = PlotContacts(tsd->ContactData[i], factor);
			for (unsigned int j = 0; j < Add; ++j) {	//make each contact's a different color for easier differentuation.
				if (plotTimeline->getYDataIndex(plotsDone + j)->GetMaxMinValue(tmpMax, tmpMin))
				{
					if (tmpMax > max)
						max = tmpMax;
					if (tmpMin < min)
						min = tmpMin;
				}
			}
			plotsDone += Add;
		}
	}
	else if (ShowContactID != -1) {
		if (PlotContacts(tsd->GetTransContact(ShowContactID), factor))
			plotsDone++;
	}


	if (ShowAllSpectra) {

		double offset = 0.0;
		double scale = 1.0;
		if (min < 1e299) {	//basically, it found a real value up there!
			scale = max - min;
			offset = max + 0.1*scale;
		}

		for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
			unsigned int colorCtrPre = plotTimeline->getNumYData();
			if (PlotSpectra(mdesc->spectrum[i], factor)) {
				unsigned int colorCtrPost = plotTimeline->getNumYData();
				
				for (; colorCtrPre < colorCtrPost; ++colorCtrPre) {	//make each contact's a different color for easier differentuation.
					AutoRotateColor(plotTimeline->getYDataIndex(colorCtrPre)->GetPen(), plotsDone);
					plotTimeline->getYDataIndex(colorCtrPre)->SetScale(scale);
					plotTimeline->getYDataIndex(colorCtrPre)->SetOffset(offset);
				}
				offset += 1.1*scale;
				plotsDone++;
			}
		}
	}
	else if (ShowSpectraID != -1) {
		if (PlotSpectra(mdesc->getSpectra(ShowSpectraID), factor))
			plotsDone++;
	}

	plotTimeline->UpdatePlot();

}

bool simTransientPanel::ContactPlot(TransContact* ct, int type, double xScale)
{
	if (!isInitialized())
		return false;
	if (ct == nullptr)
		return false;

	

	TransientSimData *tsd = getTrans();
	if (tsd == nullptr)
		return false;

	std::vector<double> xVals;
	std::vector<double> yVals;

	std::vector<double> linTimes;	//need to build up a list of times that things go down.
	std::vector<double> cosTimes;
	std::vector<double> useTimes;
	wxString strtype = " ";

	if (type == CTC_LIN_VOLT || type == CTC_COS_VOLT) {

		ct->GetTimes(CTC_COS_VOLT, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_LIN_VOLT, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_LIN_VOLT;	//just simplify checks i nthe future
		strtype << "voltage";
	}
	else if (type == CTC_COS_CUR || type == CTC_LIN_CUR) {

		ct->GetTimes(CTC_COS_CUR, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_LIN_CUR, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_LIN_CUR;	//just simplify checks i nthe future
		strtype << "current";
	}
	else if (type == CTC_LIN_CURD || type == CTC_COS_CURD) {

		ct->GetTimes(CTC_COS_CURD, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_LIN_CURD, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_LIN_CURD;	//just simplify checks i nthe future
		strtype << "current density";
	}
	else if (type == CTC_LIN_EFIELD || type == CTC_COS_EFIELD) {

		ct->GetTimes(CTC_COS_EFIELD, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_LIN_EFIELD, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_LIN_EFIELD;	//just simplify checks i nthe future
		strtype << "electric field";
	}
	else if (type == CTC_ED_LIN_CUR || type == CTC_ED_COS_CUR) {	//the values are added to

		ct->GetTimes(CTC_ED_COS_CUR, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_ED_LIN_CUR, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_ED_LIN_CUR;	//just simplify checks i nthe future
		strtype << "injection current";
	}
	else if (type == CTC_ED_LIN_CURD || type == CTC_ED_COS_CURD) {

		ct->GetTimes(CTC_ED_COS_CURD, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_ED_LIN_CURD, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_ED_LIN_CURD;	//just simplify checks i nthe future
		strtype << "injection c. density";
	}
	else if (type == CTC_ED_LIN_EFIELD || type == CTC_ED_COS_EFIELD) {

		ct->GetTimes(CTC_ED_COS_EFIELD, cosTimes);	//now has both start and stop times
		RemoveVectorDuplicates(cosTimes);
		ct->GetTimes(CTC_ED_LIN_EFIELD, linTimes);
		RemoveVectorDuplicates(linTimes);
		type = CTC_ED_LIN_EFIELD;	//just simplify checks i nthe future
		strtype << "injection e. field";
	}
	
	else
		return false;
	if (xScale <= 0.0)
		xScale = 1.0;

	//clear out anything past the simEnd time
	bool addSimEndTime = false;
	for (unsigned int i = 0; i < cosTimes.size(); ++i) {
		if (cosTimes[i] < 0.0) {
			cosTimes.erase(cosTimes.begin() + i);
			addSimEndTime = true;
			i--;
		}
		if (i < cosTimes.size() && cosTimes[i] > tsd->simendtime)	//need to test against size because element 0 might be erased
		{
			cosTimes.erase(cosTimes.begin() + i, cosTimes.end());//it's sorted, everything else is bad. Will immediately cause loop to exit
			addSimEndTime = true;
		}
	}
	if (addSimEndTime && cosTimes.size() > 0 && cosTimes.back() < tsd->simendtime)
		cosTimes.push_back(tsd->simendtime);

	//now just do a slight modification to make sure we get abrupt changes.
	double delta = tsd->simendtime * 1.0e-10;
	for (unsigned int i = cosTimes.size() - 1; i < cosTimes.size(); --i)
		cosTimes.push_back(cosTimes[i] - delta);


	addSimEndTime = false;
	for (unsigned int i = 0; i < linTimes.size(); ++i) {
		if (linTimes[i] < 0.0) {
			linTimes.erase(linTimes.begin() + i);
			addSimEndTime = true;
			i--;
		}
		if (i < linTimes.size() && linTimes[i] > tsd->simendtime)	//gotta worry about that double accuracy
		{
			linTimes.erase(linTimes.begin() + i, linTimes.end());//it's sorted, everything else is bad. Will immediately cause loop to exit
			addSimEndTime = true;
		}
	}
	if (addSimEndTime && linTimes.size() > 0 && linTimes.back() < tsd->simendtime)
		linTimes.push_back(tsd->simendtime);

	for (unsigned int i = linTimes.size() - 1; i < linTimes.size(); --i)
		linTimes.push_back(linTimes[i] - delta);

	for (unsigned int i = 0; i < cosTimes.size(); ++i) {
		ct->UpdateActive(cosTimes[i], true);	//this updates the frequencies.
		double dTime;
		if (type == CTC_LIN_VOLT)
			dTime = ct->GetVoltageFrequency();
		else if (type == CTC_LIN_CUR || type == CTC_LIN_CURD)
			dTime = ct->GetCurrentFrequency();
		else if (type == CTC_LIN_EFIELD)
			dTime = ct->GetEfieldFrequency();
		else
			dTime = 0.0;
		
		if (dTime == 0.0) {	//sucky cosine... just an offset voltage basically
			useTimes.push_back(cosTimes[i]);
			continue;
		}
		//now we want to get the voltages through the next point of interest.
		dTime = 1.0 / (8.0 * dTime);	//get it to be the period, and plot 8 points per period.
		double intTime = i < (cosTimes.size() - 1) ? cosTimes[i + 1] + dTime*0.125 : tsd->simendtime + dTime *0.125;
		for (double addTime = cosTimes[i]; addTime < intTime; addTime += dTime)
			useTimes.push_back(addTime);

	}

	for (unsigned int i = 0; i < linTimes.size(); ++i)	//it's linear, so these are straight. No need for lots of points
		useTimes.push_back(linTimes[i]);

	RemoveVectorDuplicates(useTimes);	//none of these should be negative. They should cap out at simEndTime. This also sorts the times.

	//now we can get the voltages
	bool prevGood = false;
	bool plotsAdded = false;
	ct->UpdateActive(0.0, true);	//make sure the internal time starts at 0 so always moving forward in time, can reuse stuff if state hasn't changed
	for (unsigned int i = 0; i < useTimes.size(); ++i) {
		if (ct->GetContact() == nullptr)
			continue;
		ct->UpdateInternalValsPreBD(useTimes[i]);	//calls update active, and then puts all the values in the actual contact
		if (type == CTC_LIN_VOLT && ct->GetContact()->isVoltageActive()) {
			double voltage = ct->GetContact()->GetVoltage();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(voltage);
		}
		else if (type == CTC_LIN_CUR && ct->GetContact()->isCurrentActive()) {
			double cur = ct->GetContact()->GetCurrent();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(cur);
		}
		else if (type == CTC_LIN_EFIELD && ct->GetContact()->isEFieldActive()) {
			double efield = ct->GetContact()->GetEField();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(efield);
		}
		else if (type == CTC_LIN_CURD && ct->GetContact()->isCurrentDensityActive()) {
			double curD = ct->GetContact()->GetCurDensity();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(curD);
		}
		else if (type == CTC_ED_LIN_CUR && ct->GetContact()->isInjectionCurrentActive()) {
			double curED = ct->GetContact()->GetEDCurrent();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(curED);
		}
		else if (type == CTC_ED_LIN_EFIELD && ct->GetContact()->isInjectionEFieldActive()) {
			double efieldED = ct->GetContact()->GetEDEField();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(efieldED);
		}
		else if (type == CTC_ED_LIN_CURD && ct->GetContact()->isInjectionCurrentDensityActive()) {
			double curDED = ct->GetContact()->GetCurEDDensity();
			prevGood = true;
			xVals.push_back(useTimes[i] * xScale);
			yVals.push_back(curDED);
		}
		else if (prevGood == true) {	//time to create a plot on the grid. Don't want plots to connect if they are inactive between. It is not set at 0!
			wxString nm = ct->GetContact()->name + strtype;
			LineData * ld = plotTimeline->CreateData(nm, xVals);
			ld->SetAsActiveX();
			ld = plotTimeline->CreateData(nm, yVals);
			ld->SetAsActiveY();
			plotsAdded = true;
			xVals.clear();
			yVals.clear();
			prevGood = false;
		}
	}
	if (prevGood) {	//gotta add that last one!
		wxString nm = ct->GetContact()->name + strtype;
		LineData * ld = plotTimeline->CreateData(nm, xVals);
		ld->SetAsActiveX();
		ld = plotTimeline->CreateData(nm, yVals);
		ld->SetAsActiveY();
		plotsAdded = true;
		xVals.clear();
		yVals.clear();
	}

	return plotsAdded;
}

int simTransientPanel::PlotContacts(TransContact *ct, double xScale)
{
	if (!isInitialized())
		return 0;
	if (ct == nullptr)
		return 0;
	
	int plotAdded = 0;
	int colorRot = 0;

	unsigned int colorCtrPre = plotTimeline->getNumYData();
	unsigned int start = colorCtrPre;
	ContactPlot(ct, CTC_LIN_VOLT, xScale);
	unsigned int colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i=colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_LIN_EFIELD, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_LIN_CUR, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_LIN_CURD, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_ED_LIN_CUR, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_ED_LIN_EFIELD, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}

	ContactPlot(ct, CTC_ED_LIN_CURD, xScale);
	colorCtrPost = plotTimeline->getNumYData();
	for (unsigned int i = colorCtrPre; i < colorCtrPost; ++i) 	//make each contact's a different color for easier differentuation.
		AutoRotateColor(plotTimeline->getYDataIndex(i)->GetPen(), colorRot);
	if (colorCtrPost != colorCtrPre) {
		plotAdded += (colorCtrPost - colorCtrPre);
		colorCtrPre = colorCtrPost;
		colorRot++;
	}
		
	return plotAdded;
}
bool simTransientPanel::PlotSpectra(LightSource *lt, double factor)
{
	if (!isInitialized())
		return false;
	if (lt == nullptr)
		return false;
	TransientSimData *tsd = getTrans();
	if(tsd == nullptr)
		return false;

	std::vector<double> xVals;
	std::vector<double> yVals;
	bool curOn = false;

	curOn = false;
	int id = lt->ID;
	for (unsigned int j = 0; j < tsd->SpecTime.size(); ++j) {
		if (tsd->SpecTime[j].SpecID == id) {

			if (tsd->SpecTime[j].enable) {
				if (curOn)
					continue;	//do nothing.
				if (xVals.size() == 0) {	//first entry
					if (tsd->SpecTime[j].time > 0.0) {	//didn't start at zero
						xVals.push_back(0.0);
						yVals.push_back(0.0);
					}
				}
				if (xVals.size() != 0 || tsd->SpecTime[j].time != 0.0) {	//let it start at one if it's the first index on 0
					xVals.push_back(tsd->SpecTime[j].time * factor);
					yVals.push_back(0.0);
				}
				xVals.push_back(tsd->SpecTime[j].time * factor);
				yVals.push_back(1.0);
				curOn = true;
			}
			else if (curOn)	{ //disabling, and it is currently on
				xVals.push_back(tsd->SpecTime[j].time * factor);
				xVals.push_back(tsd->SpecTime[j].time * factor);
				yVals.push_back(1.0);
				yVals.push_back(0.0);
				curOn = false;
			}
		}
	}

	if (xVals.size() > 0) {
		if (xVals.back() < tsd->simendtime * factor) {
			xVals.push_back(tsd->simendtime * factor);
			if (curOn)
				yVals.push_back(1.0);
			else
				yVals.push_back(0.0);
		}

		//quick, dirty sort. This is probably pretty close
		for (unsigned int i = 0; i < xVals.size(); ++i)
		{
			for (unsigned int j = i + 1; j < xVals.size(); ++j) {
				if (xVals[j] < xVals[i]) {	//swap both of 'em
					double tmp = xVals[j];
					xVals[j] = xVals[i];
					xVals[i] = tmp;
					tmp = yVals[j];
					yVals[j] = yVals[i];
					yVals[i] = tmp;
				}
			}
			//in the event of overlaps, it goes with the last value. On the plot, these show up incorrectly as diagonal lines
			//no two adjacent vertices can have different x's AND y's - only one
			//note, in this method, the values looked at are guaranteed to be correct as it assigns them lowest first
			if (i > 0) {
				if (xVals[i - 1] != xVals[i] && yVals[i - 1] != yVals[i]) {
					//the proper one is the earlier value
					yVals[i] = yVals[i - 1];
				}
			}
		}

		wxString nm = lt->name;
		LineData * ld = plotTimeline->CreateData(nm, xVals);
		ld->SetAsActiveX();
		ld = plotTimeline->CreateData(nm, yVals);
		ld->SetAsActiveY();
		return true;
	}
	return false;
}

//////////////////////////////////////////////////

TransDataDisplay::TransDataDisplay(int val, double sT, double eT, int ctcID, int spcId) :
value(val), sTime(sT), eTime(eT), contactID(ctcID), spectraID(spcId)
{ }

bool TransDataDisplay::Update(TimeDependence* dat, Contact *ct) {
	if (ct == nullptr || dat==nullptr)
		return false;
	spectraID = -1;
	contactID = ct->id;

	sTime = dat->GetStartTime();
	eTime = dat->GetEndTime();
	value = dat->GetFlags();
	
	name_param = ct->name + " " + GetContactParameterName(value);

	if (dat->isLin()) {
		LinearTimeDependence *ltd = (LinearTimeDependence*)dat;
		double m = ltd->GetSlope();
		double b = ltd->GetInitVal();
		str_value = "";
		if (ltd->IsSum())
			str_value << "+ ";
		else
			str_value << "= ";

		if (m != 0.0) {
			if (m != 1.0) {
				str_value << m;
			}
			if (sTime != 0.0)
				str_value << "(t - (" << sTime << "))";
			else
				str_value << "(t)";

			if (b < 0)
				str_value << " - " << abs(b);
			else if (b>0)
				str_value << " + " << b;
		}
		else
			str_value << b;
	}
	else if (dat->isCos()) {
		CosTimeDependence *ctd = (CosTimeDependence*)dat;
		double A = ctd->GetAmp();
		double d = ctd->GetDelta();
		double offset = ctd->GetDC();
		double f = ctd->GetFreq();
		str_value = "";
		if (ctd->IsSum())
			str_value << "+ ";
		else
			str_value << "= ";
		if (A != 0.0) {
			if (A != 1.0 || (A == 1.0 && (f != 0 || d != 0)))	//show A if not one, or it would be 1cos(0)
				str_value << A;
			if (f != 0 || d != 0) { //should the cosine even show up? It's not just a cosine of 0 == 1
				str_value << " cos[";
				if (f != 0) {
					str_value << wxT(" 2\u03C0");
					if (f != 1.0) {
						str_value << " (" << f << ")";
					}
					str_value << "(t";
					if (sTime != 0.0)
						str_value << " - (" << sTime << "))";
					else
						str_value << ")";
					if (d > 0.0)
						str_value << " + " << d;
					else if (d < 0.0)
						str_value << " - " << abs(d);
				}
				else {	//frequency was 0, so d is not
					str_value << d;
				}
				str_value << " ]";
			}	//end if the cosine shows.
			// A is not zero, so this is an addition of the offset none the less.
			if (offset < 0.0)
				str_value << " - " << abs(offset);
			else if (offset > 0.0)
				str_value << " + " << offset;	//there was an A...
		}
		else //only the offset could be possible!
			str_value << offset;	//nothing before, so just display the offset.

	}
	else {
		//just a normal thingus...
		str_value = "Enabled";
	}
	return true;
}
bool TransDataDisplay::Update(const SpectraTimes& dat, LightSource* ltSrc) {
	if (ltSrc == nullptr)
		return false;
	contactID = -1;
	spectraID = ltSrc->ID;

	sTime = dat.time;
	eTime = -1.0;
	
	value = dat.enable ? 1 : 0;

	name_param = ltSrc->name;

	if (value == 1)
		str_value = "On";
	else
		str_value = "Off";

	return true;
}
bool TransDataDisplay::Update(double time, bool sv) {
	contactID = -1;
	spectraID = -1;
	sTime = time;
	eTime = time;
	value = sv ? 1 : 0;
	name_param = "Checkpoint";
	if (sv)
		str_value = "Save Device State";
	else
		str_value = "---";

	return true;
}

////////////////////////////////////////////////////////

void TransContact::UpdateGridDisplay(std::vector<TransDataDisplay>& disp)
{
	TransDataDisplay tmp(0.0, 0.0);
	//(multi)map iterator. bunch of either cosTimeDependence or LinearTimeDependence in second. first is double starttime, which is also stored in 2nd
	for (auto it = VoltageCos.begin(); it != VoltageCos.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = VoltageLin.begin(); it != VoltageLin.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = EFieldLin.begin(); it != EFieldLin.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = EFieldCos.begin(); it != EFieldCos.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = CurrentDensityLin.begin(); it != CurrentDensityLin.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = CurrentDensityCos.begin(); it != CurrentDensityCos.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = CurrentLin.begin(); it != CurrentLin.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = CurrentCos.begin(); it != CurrentCos.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
	for (auto it = AdditionalFlags.begin(); it != AdditionalFlags.end(); ++it) {
		if (tmp.Update(&it->second, ct)) {
			disp.push_back(tmp);
		}
	}
}

////////////////////////////////////////////////////

std::string GetContactParameterName(int activeValue)
{
	if (activeValue == CONTACT_INACTIVE)
		return "Nothing";

	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_VOLT_COS | CONTACT_VOLT_LINEAR))
		return "Voltage";
	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_EFIELD_COS | CONTACT_EFIELD_LINEAR))
		return "Electric Field";
	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_CURRENT_COS | CONTACT_CURRENT_LINEAR))
		return "Current";
	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_LINEAR))
		return "Current Density";

	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_EFIELD_COS_ED | CONTACT_EFIELD_LINEAR_ED))
		return "Injected E. Field";
	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_CURRENT_COS_ED | CONTACT_CURRENT_LINEAR_ED))
		return "Injected Current";
	if (ANY_SET_BITS_ARE_ONE(activeValue, CONTACT_CUR_DENSITY_COS_ED | CONTACT_CUR_DENSITY_LINEAR_ED))
		return "Injected C. Density";

	if (ALL_SET_BITS_ARE_ONE(activeValue, CONTACT_CONNECT_EXTERNAL))
		return "External Contact";
	if (ALL_SET_BITS_ARE_ONE(activeValue, CONTACT_SINK))
		return "Thermal Sink";
	if (ALL_SET_BITS_ARE_ONE(activeValue, CONTACT_MINIMIZE_E_ENERGY))
		return "Min. E-Field Energy";
	if (ALL_SET_BITS_ARE_ONE(activeValue, CONTACT_OPP_DFIELD))
		return "Opposing D-Fields";
	if (ALL_SET_BITS_ARE_ONE(activeValue, CONTACT_CONSERVE_CURRENT))
		return "Conserve Charge";

	return "UNKNOWN";
}

BEGIN_EVENT_TABLE(simTransientPanel, wxPanel)
EVT_BUTTON(TR_SAVE, simTransientPanel::SaveTransient)
EVT_BUTTON(TR_LOAD, simTransientPanel::LoadTransient)
EVT_BUTTON(TR_ADD, simTransientPanel::AddToTimeline)
EVT_BUTTON(TR_REMOVE, simTransientPanel::RemoveItem)
EVT_COMBOBOX(TR_GRID_UNIT, simTransientPanel::UpdateGridUnits)
EVT_COMBOBOX(TR_PLOT_UNIT, simTransientPanel::UpdatePlotUnits)
EVT_COMBOBOX(TR_GRID_SEL, simTransientPanel::TransferToGUI)
EVT_COMBOBOX(TR_PLOT_SEL, simTransientPanel::TransferToGUI)
EVT_COMBOBOX(TR_PROP_SEL, simTransientPanel::UpdateProperySelection)
EVT_COMBOBOX(TR_MOD_SEL, simTransientPanel::UpdateMethodSelection)
EVT_COMBOBOX(TR_INIT_COND, simTransientPanel::UpdateInitialCondition)
EVT_COMBOBOX(TR_ENTRY_UNIT, simTransientPanel::UpdateTimeUnits)
EVT_TEXT(TR_SIMENDTIME, simTransientPanel::UpdateSimEndTime)
EVT_TEXT(TR_COS_TEXT, simTransientPanel::UpdateCosText)
EVT_TEXT(TR_LIN_TEXT, simTransientPanel::UpdateLinText)
EVT_TEXT(TR_ADDTIMES, simTransientPanel::UpdateStartEndTimes)
EVT_CHECKBOX(TR_SMART_SAVE, simTransientPanel::UpdateSmartSave)
EVT_CHECKBOX(TR_LIN_SUM, simTransientPanel::UpdateLinCheck)
EVT_CHECKBOX(TR_COS_SUM, simTransientPanel::UpdateCosCheck)
EVT_RADIOBOX(TR_RBOX, simTransientPanel::UpdateRadioBox)
EVT_GRID_CMD_SELECT_CELL(TR_GRID, simTransientPanel::GridClick)
END_EVENT_TABLE()