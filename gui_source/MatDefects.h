#ifndef MATERIAL_DEFECTS_H
#define MATERIAL_DEFECTS_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "wx/scrolwin.h"

class wxWindow;
class wxCommandEvent;
class wxComboBox;
class wxRadioBox;
class wxTextCtrl;
class wxButton;
class wxBoxSizer;
class wxStaticText;
class wxFlexGridSizer;
class wxSizer;
class ModelDescribe;
class devMaterialPanel;
class Material;
class Impurity;

class devDefectsPanel : public wxScrolledWindow
{
public:
	devDefectsPanel(wxWindow* parent);
	~devDefectsPanel();
	void SizeRight();
	void NewDefect(wxCommandEvent& event);
	void LoadDefect(wxCommandEvent& event);
	void RemoveDefect(wxCommandEvent& event);
	void Clear();
	void SwitchDefect(wxCommandEvent& event);
	void saveDefect(wxCommandEvent& event);
	void RenameDefect(wxCommandEvent& event);
	void ChangeDistributionType(wxCommandEvent& event);

	void duplicateDefect(wxCommandEvent& event);
	
	void UpdateGUIValues();
	void TransferToGUI();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateReference(wxCommandEvent& event);


	wxComboBox* defectChooser;
	
	wxRadioBox* referenceBox;
	wxRadioBox* depthBox;
	wxRadioBox* distBox;

	wxTextCtrl* sigmanTxt;
	wxTextCtrl* sigmapTxt;
	wxTextCtrl* defChargeTxt;
	wxTextCtrl* defEn;
	wxTextCtrl* defVar;
	wxTextCtrl* defDegen;
	wxTextCtrl* defOptEffCB;
	wxTextCtrl* defOptEffVB;
	wxTextCtrl* defDensityBox;

	wxButton* defAdd;
	wxButton* defRem;
	wxButton* defSave;
	wxButton* defLoad;
	wxButton* defDuplicate;
	wxButton* defRename;

	wxStaticText* sigman;
	wxStaticText* sigmap;
	wxStaticText* defCharge;
	wxStaticText* defEnergy;
	wxStaticText* defVariation;
	wxStaticText* defDensity;
	wxStaticText* defDegeneracy;
	wxStaticText* txtOptEffCB;
	wxStaticText* txtOptEffVB;

	wxStaticText* explainDistribution;

	wxFlexGridSizer* defEnergyInputs;	//2x2 energy variation
	wxFlexGridSizer* defMiscInputs;	// ? x 2 all other shit
	wxBoxSizer* defEnergyExplain;	//vertical, flex Inp and explain what's going on
	wxBoxSizer* defEnergyRBoxInp;	//horizontal, all the energy stuff go in this
	wxBoxSizer* defRefDep;	//vertical, reference and depth
	wxBoxSizer* defopenSpace;	//vertical create a blank space for future stuff under the energy inputs
	wxBoxSizer* defEnergySide;	//horizontal. connects the energy/open sizer with the misc inputs sizer
	wxBoxSizer* defBar;		//horizontal. Defect selection, all the buttons


	ModelDescribe* getMDesc();
	devMaterialPanel* getMyClassParent();
	Impurity* getDefect() { return(curDefect); }
	Material* getMat();
	void VerifyInternalVars();

private:
	wxBoxSizer* defSizer;	//vertical. connects the energy side with the top row
	Impurity* curDefect;
	bool isInitialized() { return (defMiscInputs != nullptr); }
	int prevSelection;
	int matSelection;

	DECLARE_EVENT_TABLE()
};

#endif