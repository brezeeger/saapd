#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "MatElectricPanel.h"
#include "MatPanel.h"

#include "../kernel_source/light.h"
#include "../kernel_source/model.h"
#include "../kernel_source/physics.h"
#include "../kernel_source/Materials.h"
#include "GUIHelper.h"


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

extern double scalarTEN(int index);

devMatElectricPanel::devMatElectricPanel(wxWindow* parent)
	: wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL)
{
	matContentSizer = new wxBoxSizer(wxVERTICAL);
	matLeftSizer = new wxBoxSizer(wxVERTICAL);
	matRightSizer = new wxBoxSizer(wxVERTICAL);
	nameDescriptSizer = new wxFlexGridSizer(1, 3, 10, 10);
	nameDescriptSizer->SetFlexibleDirection(wxBOTH);
	nameDescriptSizer->AddGrowableCol(1, 1);	//makes the input box take up more space
	matInnerSizer = new wxFlexGridSizer(6, 10, 10);
	matInnerSizer->SetFlexibleDirection(wxBOTH);
	matInnerSizer->AddGrowableCol(1, 1);
	matInnerSizer1 = new wxFlexGridSizer(8, 2, 10, 10);
	matInnerSizer1->SetFlexibleDirection(wxBOTH);
	matInnerSizer1->AddGrowableCol(1, 1);
	wxArrayString matType;
	matType.Add(wxT("Semiconductor"));	//model.h: SEMICONDUCTOR = 1
	matType.Add(wxT("Metal"));			//METAL = 2
	matType.Add(wxT("Insulator/Dielectric"));	//DIELECTRIC = 3
	matType.Add(wxT("Vacuum"));	//VACUUM = 4


	
	description = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString);
	materialType = new wxRadioBox(this, MAT_ELEC_PROP_CTRLS, wxT("Type:"), wxPoint(0, 0), wxDefaultSize, matType, 1, wxRA_SPECIFY_COLS);
	materialType->Enable(1, false);
	materialType->Enable(2, false);
	materialType->Enable(3, false);
	materialType->Select(0);
	permittivity = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	cbEffDens = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	eefmass_dos = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	permeability = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	vbEffDens = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	hefmass_dos = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	energyBandGap = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	ThGenRecTau = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	directEg = new wxCheckBox(this, MAT_ELEC_PROP_CTRLS, wxT(""), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	electronAffinity = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	electronMobility = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	eefmass_c = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC)); 
	intrinsicCar = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	holeMobility = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	hefmass_c = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC)); 
	urbachEcE = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	urbachEcSigCB = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	urbachEcSigVB = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	urbachEvE = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	urbachEvSigCB = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	urbachEvSigVB = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	
	
	descript = new wxStaticText(this, wxID_ANY, wxT("Description:"));
	perm = new wxStaticText(this, wxID_ANY, wxT("\u03B5:"));
	perm->SetToolTip(wxT("Relative Permittivity"));
	stat_permeability = new wxStaticText(this, wxID_ANY, wxT("\u03BC_r:"));
	stat_permeability->SetToolTip(wxT("Relative Permeability"));
	icarrier = new wxStaticText(this, wxID_ANY, wxT("n_i:"));
	icarrier->SetToolTip(wxT("Intrinsic Carrier Concentration at 300K. Internally calculated if unknown"));
	cEffective = new wxStaticText(this, wxID_ANY, wxT("N_c:"));
	cEffective->SetToolTip(wxT("Effective Conduction Band Density at 300K. Internally calculated if unknown"));
	vEffective = new wxStaticText(this, wxID_ANY, wxT("N_v:"));
	vEffective->SetToolTip(wxT("Effective Valence Band Density at 300K. Internally calculated if unknown"));
	eefm_c = new wxStaticText(this, wxID_ANY, wxT("e_efm(c):"));
	eefm_c->SetToolTip(wxT("Electron Effective Mass (at intrinsic 300K for conductivity)"));
	hefm_c = new wxStaticText(this, wxID_ANY, wxT("h_efm(c):"));
	hefm_c->SetToolTip(wxT("Hole Effective Mass (at intrinsic 300K for conductivity)"));
	eefm_dos = new wxStaticText(this, wxID_ANY, wxT("e_efm(dos):"));
	eefm_dos->SetToolTip(wxT("Electron Effective Mass (density of states)"));
	hefm_dos = new wxStaticText(this, wxID_ANY, wxT("h_efm(dos):"));
	hefm_dos->SetToolTip(wxT("Hole Effective Mass (density of states)"));
	emob = new wxStaticText(this, wxID_ANY, wxT("\u03BC_n:"));
	emob->SetToolTip(wxT("Electron mobility (cm^2 / Vs) at 300K"));
	stat_ThGenRecTau = new wxStaticText(this, wxID_ANY, wxT("\u03C4"));
	stat_ThGenRecTau->SetToolTip(wxT("Thermal Generation/Recombination time constant (within \u03B1)"));
	stat_directEg = new wxStaticText(this, wxID_ANY, wxT("Direct Eg"));
	stat_directEg->SetToolTip(wxT("Is the band gap of this material direct or indirect?"));
	stat_urbachEcE = new wxStaticText(this, wxID_ANY, wxT("Urbach Energy(Ec):"));	//
	stat_urbachEcE->SetToolTip(wxT("Urbach Energy (eV) for conduction band. 0 means no urbach states"));
	stat_urbachEcSigCB = new wxStaticText(this, wxID_ANY, wxT("Urbach \u03C3_n:"));	//
	stat_urbachEcSigCB->SetToolTip(wxT("Capture cross section for conduction band urbach states to catch electrons"));
	stat_urbachEcSigVB = new wxStaticText(this, wxID_ANY, wxT("Urbach \u03C3_p:"));	//
	stat_urbachEcSigVB->SetToolTip(wxT("Capture cross section for conduction band urbach states to catch holes"));
	stat_urbachEvE = new wxStaticText(this, wxID_ANY, wxT("Urbach Energy(Ev):"));	//
	stat_urbachEvE->SetToolTip(wxT("Urbach Energy (eV) for valence band. 0 means no urbach states"));
	stat_urbachEvSigCB = new wxStaticText(this, wxID_ANY, wxT("Urbach \u03C3_n:"));	//
	stat_urbachEvSigCB->SetToolTip(wxT("Capture cross section for valence band urbach states to catch holes"));
	stat_urbachEvSigVB = new wxStaticText(this, wxID_ANY, wxT("Urbach \u03C3_p:"));	//
	stat_urbachEvSigVB->SetToolTip(wxT("Capture cross section for valence band urbach states to catch electrons"));
	heatCap = new wxStaticText(this, wxID_ANY, wxT("Specific Heat Capacity:"));
	heatCap->SetToolTip(wxT("Dynamic temperatures not implemented yet"));
	tCond = new wxStaticText(this, wxID_ANY, wxT("Thermal Conductivity:"));
	tCond->SetToolTip(wxT("Dynamic temperatures not implemented yet"));
	eAffin = new wxStaticText(this, wxID_ANY, wxT("\u03A7:"));
	eAffin->SetToolTip(wxT("Electron Affinity"));
	gap = new wxStaticText(this, wxID_ANY, wxT("Eg:"));
	gap->SetToolTip(wxT("Band gap at 300K"));
	hmob = new wxStaticText(this, wxID_ANY, wxT("\u03BC_p:"));
	hmob->SetToolTip(wxT("Hole mobility (cm^2 / Vs) at 300K"));
	
	heatCapacity = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	heatCapacity->Disable();
	thermalConductivity = new wxTextCtrl(this, MAT_ELEC_PROP_CTRLS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	thermalConductivity->Disable();
	customDOS = new wxButton(this, wxID_ANY, wxT("Custom Density of States"));
	customDOS->SetToolTip(wxT("GUI aspect not enabled for custom densities of states"));
	customDOS->Disable();

	nameDescriptSizer->Add(descript, 0, 0, 0);
	nameDescriptSizer->Add(description, 1, wxEXPAND, 0);
	nameDescriptSizer->Add(materialType, 1, wxEXPAND, 0);

	////////////////////////////////////

	//////////////////////////
	matInnerSizer->Add(perm, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(permittivity, 1, wxEXPAND, 0);
	matInnerSizer->Add(cEffective, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(cbEffDens, 1, wxEXPAND, 0);
	matInnerSizer->Add(eefm_dos, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(eefmass_dos, 1, wxEXPAND, 0);
	///////////////////////////
	matInnerSizer->Add(stat_permeability, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(permeability, 1, wxEXPAND, 0);
	matInnerSizer->Add(vEffective, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(vbEffDens, 1, wxEXPAND, 0);
	matInnerSizer->Add(hefm_dos, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(hefmass_dos, 1, wxEXPAND, 0);
	////////////////////
	matInnerSizer->Add(gap, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(energyBandGap, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_ThGenRecTau, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(ThGenRecTau, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_directEg, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(directEg, 1, wxALIGN_LEFT, 0);
	/////////////////////
	matInnerSizer->Add(eAffin, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(electronAffinity, 1, wxEXPAND, 0);
	matInnerSizer->Add(emob, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(electronMobility, 1, wxEXPAND, 0);
	matInnerSizer->Add(eefm_c, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(eefmass_c, 1, wxEXPAND, 0);
	//////////////////
	matInnerSizer->Add(icarrier, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(intrinsicCar, 1, wxEXPAND, 0);
	matInnerSizer->Add(hmob, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(holeMobility, 1, wxEXPAND, 0);
	matInnerSizer->Add(hefm_c, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(hefmass_c, 1, wxEXPAND, 0);
	//////////////////
	matInnerSizer->Add(stat_urbachEcE, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEcE, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_urbachEcSigCB, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEcSigCB, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_urbachEcSigVB, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEcSigVB, 1, wxEXPAND, 0);
	//////////////////
	matInnerSizer->Add(stat_urbachEvE, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEvE, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_urbachEvSigCB, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEvSigCB, 1, wxEXPAND, 0);
	matInnerSizer->Add(stat_urbachEvSigVB, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(urbachEvSigVB, 1, wxEXPAND, 0);
	///////////////////
	matInnerSizer->Add(heatCap, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(heatCapacity, 1, wxEXPAND, 0);
	matInnerSizer->Add(tCond, 0, wxALIGN_RIGHT, 0);
	matInnerSizer->Add(thermalConductivity, 1, wxEXPAND, 0);
	matInnerSizer->AddSpacer(1);
	matInnerSizer->Add(customDOS, 1, wxEXPAND, 0);


	matContentSizer->Add(nameDescriptSizer, 0, wxEXPAND, 0);
	matContentSizer->Add(matInnerSizer, 1, wxALL | wxEXPAND, 10);



	UpdateGUIValues();
}

devMatElectricPanel::~devMatElectricPanel()
{
	delete description;
	delete materialType;
	delete permittivity;
	delete intrinsicCar;
	delete cbEffDens;
	delete vbEffDens;
	delete eefmass_c;
	delete hefmass_c;
	delete eefmass_dos;
	delete hefmass_dos;
	delete electronMobility;
	delete holeMobility;
	delete energyBandGap;
	delete electronAffinity;
	delete heatCapacity;
	delete thermalConductivity;
	delete descript;
	delete perm;
	delete icarrier;
	delete cEffective;
	delete vEffective;
	delete eefm_c;
	delete hefm_c;
	delete eefm_dos;
	delete hefm_dos;
	delete emob;
	delete hmob;
	delete gap;
	delete eAffin;
	delete heatCap;
	delete tCond;
	delete customDOS;

	delete permeability;
	delete urbachEcE;	//
	delete urbachEcSigCB;	//
	delete urbachEcSigVB;	//
	delete urbachEvE;	//
	delete urbachEvSigCB;	//
	delete urbachEvSigVB;	//
	delete ThGenRecTau;	//

	delete directEg;	//

	delete stat_permeability;	//
	delete stat_urbachEcE;	//
	delete stat_urbachEcSigCB;	//
	delete stat_urbachEcSigVB;	//
	delete stat_urbachEvE;	//
	delete stat_urbachEvSigCB;	//
	delete stat_urbachEvSigVB;	//
	delete stat_ThGenRecTau;	//
	delete stat_directEg;	//
}

devMaterialPanel* devMatElectricPanel::getMyClassParent()
{
	return((devMaterialPanel*)GetParent()->GetParent());
}

ModelDescribe* devMatElectricPanel::getMDesc()
{
	return(((devMaterialPanel*)GetParent()->GetParent())->getMdesc());
}

void devMatElectricPanel::SizeRight()
{
	SetSizer(matContentSizer);
	SetScrollRate(20, 20);
}

void devMatElectricPanel::Clear()
{
	if (!isInitialized())
		return;
	description->Clear();
	permittivity->Clear();
	permeability->Clear();
	intrinsicCar->Clear();
	cbEffDens->Clear();
	vbEffDens->Clear();
	eefmass_c->Clear();
	hefmass_c->Clear();
	eefmass_dos->Clear();
	hefmass_dos->Clear();
	electronMobility->Clear();
	holeMobility->Clear();
	energyBandGap->Clear();
	electronAffinity->Clear();
	heatCapacity->Clear();
	thermalConductivity->Clear();

	urbachEcE->Clear();	//
	urbachEcSigCB->Clear();	//
	urbachEcSigVB->Clear();	//
	urbachEvE->Clear();	//
	urbachEvSigCB->Clear();	//
	urbachEvSigVB->Clear();	//
	ThGenRecTau->Clear();	//

}


void devMatElectricPanel::UpdateGUIValues()
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	if (mat == nullptr)
	{
		description->Disable();

		materialType->Disable();
		permittivity->Disable();
		intrinsicCar->Disable();
		cbEffDens->Disable();
		vbEffDens->Disable();
		eefmass_c->Disable();
		hefmass_c->Disable();
		eefmass_dos->Disable();
		hefmass_dos->Disable();
		electronMobility->Disable();
		holeMobility->Disable();
		energyBandGap->Disable();
		electronAffinity->Disable();
		heatCapacity->Disable();
		thermalConductivity->Disable();

		permeability->Disable();
		urbachEcE->Disable();	//
		urbachEcSigCB->Disable();	//
		urbachEcSigVB->Disable();	//
		urbachEvE->Disable();	//
		urbachEvSigCB->Disable();	//
		urbachEvSigVB->Disable();	//
		ThGenRecTau->Disable();	//
		directEg->Disable();
	}
	else
	{
		description->Enable();

		materialType->Enable();
		materialType->Enable(1, false);
		materialType->Enable(2, false);
		materialType->Enable(3, false);
		permittivity->Enable();
		intrinsicCar->Enable();
		cbEffDens->Enable();
		vbEffDens->Enable();
		eefmass_c->Enable();
		hefmass_c->Enable();
		eefmass_dos->Enable();
		hefmass_dos->Enable();
		electronMobility->Enable();
		holeMobility->Enable();
		energyBandGap->Enable();
		electronAffinity->Enable();

		permeability->Enable();
		urbachEcE->Enable();	//

		double tmp;
		bool enable = false;
		if (urbachEcE->GetValue().ToDouble(&tmp) == true) {
			if (tmp > 0.0)
				enable = true;
		}

		urbachEcSigCB->Enable(enable);	//
		urbachEcSigVB->Enable(enable);	//
		urbachEvE->Enable();	//

		enable = false;
		if (urbachEvE->GetValue().ToDouble(&tmp) == true) {
			if (tmp > 0.0)
				enable = true;
		}

		urbachEvSigCB->Enable(enable);	//
		urbachEvSigVB->Enable(enable);	//
		ThGenRecTau->Enable();	//
		directEg->Enable();
	}


}

void devMatElectricPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	Material* mat = getMat();
	if (mat)
	{
		ValueToTBox(permittivity, mat->getRelativePermittivity());
		ValueToTBox(intrinsicCar, mat->getIntrinsicCarrierConcentration());
		ValueToTBox(cbEffDens, mat->getCBEffectiveDOS());
		ValueToTBox(vbEffDens, mat->getVBEffectiveDOS());
		ValueToTBox(eefmass_dos, mat->getEFM_E_Density());
		ValueToTBox(hefmass_dos, mat->getEFM_H_Density());
		ValueToTBox(eefmass_c, mat->getEFM_E_Conductance());
		ValueToTBox(hefmass_c, mat->getEFM_H_Conductance());
		ValueToTBox(electronMobility, mat->getElectronMobility());
		ValueToTBox(holeMobility, mat->getHoleMobility());
		ValueToTBox(energyBandGap, mat->getBandGap());
		ValueToTBox(electronAffinity, mat->getAffinity());
		ValueToTBox(heatCapacity, mat->getSpecificHeatCapacity());
		ValueToTBox(thermalConductivity, mat->getThermalConductivity());
		ValueToTBox(urbachEcE, mat->getUrbachCB().getUrbachEnergy());
		ValueToTBox(urbachEcSigCB, mat->getUrbachCB().getCBCaptureCrossSection());
		ValueToTBox(urbachEcSigVB, mat->getUrbachCB().getVBCaptureCrossSection());
		ValueToTBox(urbachEvE, mat->getUrbachVB().getUrbachEnergy());
		ValueToTBox(urbachEvSigCB, mat->getUrbachVB().getCBCaptureCrossSection());
		ValueToTBox(urbachEvSigVB, mat->getUrbachVB().getVBCaptureCrossSection());
		ValueToTBox(ThGenRecTau, mat->getBandGenRecTau());
		ValueToTBox(permeability, mat->getRelativePermeability());
		ValueToTBox(description, mat->getUserDescription());

		if (mat->getType() >= Material::semiconductor && mat->getType() <= Material::vacuum)
			materialType->SetSelection(mat->getType() - 1);
		else
			materialType->SetSelection(0);

		directEg->SetValue(mat->isDirect());
	}
	else
	{
		ValueToTBox(description, "");
		ValueToTBox(permittivity, 1.0f);
		ValueToTBox(intrinsicCar, 0.0);
		ValueToTBox(cbEffDens, 0.0);
		ValueToTBox(vbEffDens, 0.0);
		ValueToTBox(eefmass_dos, 1.0);
		ValueToTBox(hefmass_dos, 1.0);
		ValueToTBox(eefmass_c, 1.0);
		ValueToTBox(hefmass_c, 1.0);
		ValueToTBox(electronMobility, 0.0);
		ValueToTBox(holeMobility, 0.0);
		ValueToTBox(energyBandGap, 0.0);
		ValueToTBox(electronAffinity, 0.0);
		ValueToTBox(heatCapacity, 0.0);
		ValueToTBox(thermalConductivity, 0.0);
		ValueToTBox(urbachEcE, 0.0);
		ValueToTBox(urbachEcSigCB, 0.0);
		ValueToTBox(urbachEcSigVB, 0.0);
		ValueToTBox(urbachEvE, 0.0);
		ValueToTBox(urbachEvSigCB, 0.0);
		ValueToTBox(urbachEvSigVB, 0.0);
		ValueToTBox(ThGenRecTau, 0.0);
		ValueToTBox(permeability, 1.0);
		materialType->SetSelection(0);
		directEg->SetValue(true);
	}
	UpdateGUIValues();

}


void devMatElectricPanel::UpdateText(wxCommandEvent& evt) {
	if (!isInitialized())
		return;
	double tmp1, tmp2, tmp3;

	//update the CB effective density to reflect this carriers effective mass
	if (evt.GetEventObject() == eefmass_dos) {
		bool good = eefmass_dos->GetValue().ToDouble(&tmp1);
		good &= (tmp1 > 0.0);
		if (good) {
			tmp2 = HBAR * HBAR * QCHARGE * PI;
			tmp3 = pow(2.0 * MELECTRON * tmp1 * KB * 300.0 / tmp2, 1.5) * 0.25  * 1.0e-6;
			ValueToTBox(cbEffDens, tmp3);
		}
	}

	//update the VB effective density to reflect this carriers' effective mass
	if (evt.GetEventObject() == hefmass_dos) {
		bool good = hefmass_dos->GetValue().ToDouble(&tmp1);
		good &= (tmp1 > 0.0);
		if (good) {
			tmp2 = HBAR * HBAR * QCHARGE * PI;
			tmp3 = pow(2.0 * MELECTRON * tmp1 * KB * 300.0 / tmp2, 1.5) * 0.25  * 1.0e-6;
			ValueToTBox(vbEffDens, tmp3);
		}
	}

	if (evt.GetEventObject() == cbEffDens) {
		bool good = cbEffDens->GetValue().ToDouble(&tmp1);
		good &= (tmp1 > 0.0);
		if (good) {
			tmp1 = 4.0e6 * tmp1;
			tmp1 = CubeRoot(tmp1*tmp1);
			tmp2 = 2.0 * KB *300.0 * MELECTRON;
			tmp3 = HBAR * HBAR * QCHARGE * PI;
			tmp1 = tmp1 * tmp3 / tmp2;
			ValueToTBox(eefmass_dos, tmp1);
		}
	}

	if (evt.GetEventObject() == vbEffDens) {
		bool good = vbEffDens->GetValue().ToDouble(&tmp1);
		good &= (tmp1 > 0.0);
		if (good) {
			tmp1 = 4.0e6 * tmp1;
			tmp1 = CubeRoot(tmp1*tmp1);
			tmp2 = 2.0 * KB *300.0 * MELECTRON;
			tmp3 = HBAR * HBAR * QCHARGE * PI;
			tmp1 = tmp1 * tmp3 / tmp2;
			ValueToTBox(hefmass_dos, tmp1);
		}
	}

	if (evt.GetEventObject() == cbEffDens || evt.GetEventObject() == vbEffDens || evt.GetEventObject() == energyBandGap || 
		evt.GetEventObject() == eefmass_dos || evt.GetEventObject() == hefmass_dos) {
		bool good = cbEffDens->GetValue().ToDouble(&tmp1);
		good &= vbEffDens->GetValue().ToDouble(&tmp2);
		good &= energyBandGap->GetValue().ToDouble(&tmp3);
		good &= (tmp1 > 0.0 && tmp2 > 0.0 && tmp3 >= 0.0);
		if (good) {
			tmp1 *= tmp2;	//multipliy the densities
			tmp3 = exp(-tmp3 * KBI / 300.0);
			tmp1 = sqrt(tmp1 * tmp3);
			ValueToTBox(intrinsicCar, tmp1);
		}
	}

	TransferToMDesc(evt);
	UpdateGUIValues();
}

void devMatElectricPanel::TransferToMDesc(wxCommandEvent& evt)
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	if (mat == nullptr)
		return;

	double tmpDouble;

	mat->SetDescription(description->GetValue().ToStdString());
	mat->SetRelativePermittivity(permittivity->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);
	mat->SetStandardIntrinsicCarrierConcentration(intrinsicCar->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetCBEffectiveDOS(cbEffDens->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetVBEffectiveDOS(vbEffDens->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetElectronEFMDensity(eefmass_dos->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);
	mat->SetHoleEFMDensity(hefmass_dos->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);
	mat->SetElectronEFMConduction(eefmass_c->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);
	mat->SetHoleEFMConduction(hefmass_c->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);
	mat->SetElectronMobility(electronMobility->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetHoleMobility(holeMobility->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetBandGap(energyBandGap->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetElectronAffinity(electronAffinity->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetGenRecTimeConstant(ThGenRecTau->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetRelativePermeability(permeability->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 1.0);

	mat->SetSpecificHeatCapacity(heatCapacity->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->SetThermalConductivity(thermalConductivity->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	

	mat->getUrbachCB().setUrbachEnergy(urbachEcE->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->getUrbachCB().setCBCaptureCrossSection(urbachEcSigCB->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->getUrbachCB().setVBCaptureCrossSection(urbachEcSigVB->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->getUrbachVB().setUrbachEnergy(urbachEvE->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->getUrbachVB().setCBCaptureCrossSection(urbachEvSigCB->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	mat->getUrbachVB().setVBCaptureCrossSection(urbachEvSigVB->GetValue().ToDouble(&tmpDouble) ? tmpDouble : 0.0);
	
	directEg->GetValue() ? mat->SetDirectBandgap() : mat->SetIndirectBandgap();

	mat->SetType(materialType->GetSelection() + 1);	//get to match with the defines. 0 is undefined. The indices match.

}

Material* devMatElectricPanel::getMat()
{
	return(getMyClassParent()->getCurrentMaterial());
}

void devMatElectricPanel::EnableMat(bool enable)
{
	if (!isInitialized())
		return;
	description->Enable(enable);
	materialType->Enable(enable);
	permittivity->Enable(enable);
	intrinsicCar->Enable(enable);
	cbEffDens->Enable(enable);
	vbEffDens->Enable(enable);
	eefmass_c->Enable(enable);
	hefmass_c->Enable(enable);
	electronMobility->Enable(enable);
	holeMobility->Enable(enable);
	energyBandGap->Enable(enable);
	electronAffinity->Enable(enable);
	heatCapacity->Enable(enable);
	thermalConductivity->Enable(enable);
}

BEGIN_EVENT_TABLE(devMatElectricPanel, wxPanel)
EVT_TEXT(MAT_ELEC_PROP_CTRLS, devMatElectricPanel::UpdateText)	//this then calls transfer to mdesc after allowinv values to be updated appropriately
EVT_CHECKBOX(MAT_ELEC_PROP_CTRLS, devMatElectricPanel::TransferToMDesc)
//EVT_BUTTON(wxID_SAVEAS, devMatElectricPanel::saveMaterial)
END_EVENT_TABLE()