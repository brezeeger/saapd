#ifndef DEV_MATERIAL_PANEL_H
#define DEV_MATERIAL_PANEL_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/grid.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/textfile.h"
#include "wx/colordlg.h"
#include "wx/colourdata.h"
#include "wx/textdlg.h"
#include "wx/dynarray.h"
#endif

class devMatElectricPanel;
class devMatOpticalsPanel;
class devDefectsPanel;
class Material;
class ModelDescribe;
class mainFrame;

class devMaterialPanel : public wxPanel
{
public:
	devMaterialPanel(wxWindow* parent);
	~devMaterialPanel();

	devMatElectricPanel* electric;
	devMatOpticalsPanel* opticals;
	devDefectsPanel* defect;
	wxComboBox* materialChooser;

	wxNotebook* materialsWindow;
	wxButton* matAdd;
	wxButton* matRem;
	wxButton* matSave;
	wxButton* matLoad;
	wxButton* matDuplicate;
	wxButton* matRename;
	wxBoxSizer* matBar;


	void NewMaterial(wxCommandEvent& event);
	void RemoveMaterial(wxCommandEvent& event);
	void SwitchMaterial(wxCommandEvent& event);
	void saveMaterial(wxCommandEvent& event);
	void duplicateMaterial(wxCommandEvent& event);
	void LoadMaterial(wxCommandEvent& event);
	void RenameMaterial(wxCommandEvent& event);

	void SizeRight();
	Material* getCurrentMaterial() { return(currentMat); }
	void setMaterial(Material* mat = nullptr) { currentMat = mat; TransferToGUI(); }

	void UpdateGUIValues();
	void TransferToGUI();
	void TransferToMdesc();

	ModelDescribe* getMdesc();
	mainFrame* getMyClassParent();
private:
	wxSizer *matOuterSizer;
	Material* currentMat;
	void VerifyInternalVars();
	int prevSelection;
	int nextID;
	bool isInitialized() { return (defect != nullptr); }

	DECLARE_EVENT_TABLE()
};

#endif