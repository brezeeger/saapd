#include "MatPanel.h"
#include "MatOpticalPanel.h"
#include "gui.h"
#include "guiwindows.h"
#include "GUIHelper.h"
#include "XML.h"

#include "../kernel_source/light.h"
#include "../kernel_source/model.h"
#include "../kernel_source/helperfunctions.h"
#include "../kernel_source/physics.h"
#include "../kernel_source/load.h"
#include "../kernel_source/Materials.h"


devMatOpticalsPanel::devMatOpticalsPanel(wxWindow* parent)
	: wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL),
	lastWavelengthEntry(-1.0), lastNEntry(-1.0), lastKEntry(-1.0), lastAEntry(-1.0)
{
	gridButtonsSizer = new wxBoxSizer(wxVERTICAL);
	gridSizer = new wxBoxSizer(wxHORIZONTAL);
	globalsSizer = new wxBoxSizer(wxHORIZONTAL);
	matOpticalSizer = new wxBoxSizer(wxVERTICAL);
	AddWaveSizer = new wxBoxSizer(wxHORIZONTAL);
	emmissionSizer = new wxBoxSizer(wxHORIZONTAL);

	optProperties = new wxGrid(this, MOD_WAVE);
	optProperties->CreateGrid(0, 4);
	optProperties->SetRowLabelSize(25);
	optProperties->SetColMinimalAcceptableWidth(100);
	
//	optProperties->SetColF(0);
//	optProperties->SetColFormatNumber(1);
//	optProperties->SetColFormatNumber(2);
//	optProperties->SetColFormatNumber(3);
	optProperties->SetColLabelValue(0, wxT("Wavelength(nm)"));
	optProperties->SetColLabelValue(1, wxT("n"));
	optProperties->SetColLabelValue(2, wxT("k"));
	optProperties->SetColLabelValue(3, wxT("alpha (cm^-1)"));

	addWave = new wxButton(this, NEW_WAVE, wxT("Add Wavelength"));
	remWave = new wxButton(this, REM_WAVE, wxT("Remove Wavelength"));
	GenAB = new wxButton(this, OPT_GEN_FROM_AB, wxT("Generate from A and B"));
	GenAB->SetToolTip(wxT("This calculation relies on the band gap, and bounds are set by the electron affinity, so make sure those are properly set or nothing will show up! Spacing is 0.01eV"));

	btnSave = new wxButton(this, OPT_SAVE, wxT("Save to File"));
	btnLoad = new wxButton(this, OPT_MERGE, wxT("Merge with File"));
	btnLoad->SetToolTip(wxT("Unique data is merged. Duplicate data (same wavelength, A, B, etc.) is overwritten"));
	btnClear = new wxButton(this, OPT_CLEAR_WAVES, wxT("Clear All Wavelengths"));

	WavelengthTimesTen = new wxButton(this, OPT_WAVE_X10, wxT("Wavelengths (x10)"));
	WavelengthDivideTen = new wxButton(this, OPT_WAVE_D10, wxT("Wavelenths (/10)"));

	globalNTxt = new wxStaticText(this, wxID_ANY, wxT("Global n:"));
	globalN = new wxTextCtrl(this, OPT_GLOB_N, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	globalATxt = new wxStaticText(this, wxID_ANY, wxT("A (cm^-1):"));
	globalATxt->SetToolTip(wxT("Used with B to generate absorption coeffieicents. Units is cm^-1"));
	globalA = new wxTextCtrl(this, OPT_GLOB_A, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	globalBTxt = new wxStaticText(this, wxID_ANY, wxT("B:"));
	globalBTxt->SetToolTip(wxT("Used with A to generate absorption coeffieicents. Unitless"));
	globalB = new wxTextCtrl(this, OPT_GLOB_B, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));


	inpWaveLbl = new wxStaticText(this, wxID_ANY, wxT("\u03BB:"));
	inpWaveLbl->SetToolTip(wxT("Wavelength (nm)"));
	inpWavetxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100,-1), 0, wxTextValidator(wxFILTER_NUMERIC));
	inpNLbl = new wxStaticText(this, wxID_ANY, wxT("n:"));
	inpNLbl->SetToolTip(wxT("Index of refraction for this wavelength"));
	inpNTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	inpKLbl = new wxStaticText(this, wxID_ANY, wxT("\u03BA:"));	//kappa
	inpKLbl->SetToolTip(wxT("Extinction coefficient for this wavelength"));
	inpKTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	inpALbl = new wxStaticText(this, wxID_ANY, wxT("\u03B1:"));	//alpha
	inpALbl->SetToolTip(wxT("Absorption coefficient (cm^-1) for this wavelength"));
	inpATxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, -1), 0, wxTextValidator(wxFILTER_NUMERIC));

	condition = new wxStaticText(this, wxID_ANY, wxT("One of these conditions must be fulfilled:"));
	nkTxt = new wxStaticText(this, wxID_ANY, wxT("n and k for each wavelength \u2612"));
	alphanTxt = new wxStaticText(this, wxID_ANY, wxT("alpha for each wavelength plus n \u2612"));
	abTxt = new wxStaticText(this, wxID_ANY, wxT("a and b for the material \u2612"));

	disableEmissions = new wxStaticText(this, wxID_ANY, wxT("Disable Emissions:"));
	disableEmissions->SetToolTip(wxT("Given a recombinative rate, is this material allowed to emit light?"));
	supressEmisOutput = new wxStaticText(this, wxID_ANY, wxT("Ignore Emission Output:"));
	supressEmisOutput->SetToolTip(wxT("Assuming light emissions are allowed and calculated, do not output the data as a separate entity"));
	EmissionEfficiency = new wxStaticText(this, wxID_ANY, wxT("Emission Efficiency:"));
	EmissionEfficiency->SetToolTip(wxT("In band-band recombination, what is the efficiency of generating light (0-1)? Light may be lost to phonons, which \
are not a part of these calculations."));

	chk_disableEmissions = new wxCheckBox(this, OPT_EMIS_CHKS, wxT(""));
	chk_supressEmisOutput = new wxCheckBox(this, OPT_EMIS_CHKS, wxT(""));
	txt_EmissionEfficiency = new wxTextCtrl(this, OPT_EMIS_EFF, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	emmissionSizer->Add(disableEmissions, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	emmissionSizer->Add(chk_disableEmissions, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	emmissionSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND | wxRIGHT, 5);
	emmissionSizer->Add(supressEmisOutput, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	emmissionSizer->Add(chk_supressEmisOutput, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	emmissionSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND | wxRIGHT, 5);
	emmissionSizer->Add(EmissionEfficiency, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	emmissionSizer->Add(txt_EmissionEfficiency, 0, wxRIGHT, 5);

	AddWaveSizer->Add(inpWaveLbl, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	AddWaveSizer->Add(inpWavetxt, 0, wxRIGHT, 8);
	AddWaveSizer->Add(inpNLbl, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	AddWaveSizer->Add(inpNTxt, 0, wxRIGHT, 8);
	AddWaveSizer->Add(inpKLbl, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	AddWaveSizer->Add(inpKTxt, 0, wxRIGHT, 8);
	AddWaveSizer->Add(inpALbl, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	AddWaveSizer->Add(inpATxt, 0, wxRIGHT, 8);
//	AddWaveSizer->Add(addWave, 1, wxEXPAND | wxALL, 5);
	

	globalsSizer->Add(globalNTxt, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	globalsSizer->Add(globalN, 0, wxRIGHT, 8);
	globalsSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	globalsSizer->Add(globalATxt, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	globalsSizer->Add(globalA, 0, wxRIGHT, 8);
	globalsSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	globalsSizer->Add(globalBTxt, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	globalsSizer->Add(globalB, 0, wxRIGHT, 8);
	

	

	gridButtonsSizer->Add(addWave, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(GenAB, 0, wxEXPAND | wxALL, 5);
	gridButtonsSizer->Add(btnLoad, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(btnSave, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(remWave, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(btnClear, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(WavelengthTimesTen, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(WavelengthDivideTen, 0, wxALL | wxEXPAND, 5);
	gridButtonsSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	gridButtonsSizer->Add(condition, 0, wxALL, 5);
	gridButtonsSizer->Add(nkTxt, 0, wxALL, 5);
	gridButtonsSizer->Add(alphanTxt, 0, wxALL, 5);
	gridButtonsSizer->Add(abTxt, 0, wxALL, 5);

	matOpticalSizer->Add(AddWaveSizer, 0, wxALL | wxEXPAND, 5);
	matOpticalSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	matOpticalSizer->Add(globalsSizer, 0, wxALL | wxEXPAND, 5);
	matOpticalSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	matOpticalSizer->Add(emmissionSizer, 0, wxALL | wxEXPAND, 5);
	matOpticalSizer->Add(optProperties, 1, wxALL | wxEXPAND, 5);
	
	gridSizer->Add(gridButtonsSizer, 1, wxEXPAND | wxALL, 5);
	gridSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND, 0);
	gridSizer->Add(matOpticalSizer, 0, wxALL | wxEXPAND, 5);
	
}

devMatOpticalsPanel::~devMatOpticalsPanel()
{
	delete globalN;
	delete globalA;
	delete globalB;
	delete nkTxt;
	delete alphanTxt;
	delete abTxt;
	delete addWave;
	delete remWave;
	delete globalNTxt;
	delete globalATxt;
	delete globalBTxt;
	delete condition;

	delete WavelengthTimesTen;
	delete WavelengthDivideTen;
	delete btnSave;
	delete btnLoad;
	delete btnClear;

	delete disableEmissions;
	delete supressEmisOutput;
	delete EmissionEfficiency;
	delete chk_disableEmissions;
	delete chk_supressEmisOutput;
	delete txt_EmissionEfficiency;
	

}

devMaterialPanel* devMatOpticalsPanel::getMyClassParent()
{
	return((devMaterialPanel*)GetParent()->GetParent());
}

ModelDescribe* devMatOpticalsPanel::getMDesc()
{
	return(getMyClassParent()->getMdesc());
}

Material* devMatOpticalsPanel::getMat()
{
	return(getMyClassParent()->getCurrentMaterial());
}

Optical* devMatOpticalsPanel::getOptical()
{
	Material* mat = getMat();
	if (mat == nullptr)
		return(nullptr);

	return(&mat->getOpticalData());	//creates a new one if no optical data previously exists
}

void devMatOpticalsPanel::AddWavelength(wxCommandEvent &event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;
	double n, k, alpha, lambda;
	bool goodLambda, goodN, goodK, goodA;

	goodLambda = goodN = goodK = goodA = false;
	n = k = alpha = lambda = -1.0;

	if (inpWavetxt->GetValue().ToDouble(&lambda))
	{
		if (lambda > 0.0)
			goodLambda = true;
	}
	if (inpNTxt->GetValue().ToDouble(&n))
	{
		if (n > 0.0)
			goodN = true;
	}
	else if (globalN->GetValue().ToDouble(&n))
	{
		if (n > 0.0)
			goodN = true;
	}
	if (inpKTxt->GetValue().ToDouble(&k))
	{
		if (k >= 0.0)
			goodK = true;
	}
	if (inpATxt->GetValue().ToDouble(&alpha))
	{
		if (alpha >= 0.0)
			goodA = true;
	}

	if (goodLambda && goodN && (goodK || goodA))	//check for good data before adding a wavelength
	{

		double deltaWave, deltaN, deltaK, deltaA;
		deltaWave = deltaN = deltaK = deltaA = 0.0;
		
		deltaWave = lastWavelengthEntry != -1.0 ? lambda - lastWavelengthEntry : lambda * 0.1;
		lastWavelengthEntry = lambda;

		deltaN = lastNEntry != -1.0 ? n - lastNEntry : 0.0;
		lastNEntry = n;

		if (goodK)
		{
			deltaK = lastKEntry > 0.0 ? k / lastKEntry : 1.0;
			lastKEntry = k;
		}
		else
			deltaK = 1.0;

		if (goodA)
		{
			deltaA = lastAEntry > 0.0 ? alpha / lastAEntry : 1.0;
			lastAEntry = alpha;
		}
		else
			deltaA = 1.0;



		opt->AddWavelength(lambda, n, k, alpha);
		UpdateGUIValues(event);	//enable or disable everything as appropriate
		TransferToGUI();	//now make sure what's loaded in the data matches the screen from our modification


		ValueToTBox(inpWavetxt, lambda + deltaWave);	//prep for clicking again!
		ValueToTBox(inpNTxt, n + deltaN);
		if (goodK)
			ValueToTBox(inpKTxt, k * deltaK);
		if (goodA)
			ValueToTBox(inpATxt, alpha * deltaA);
	}
}

void devMatOpticalsPanel::RemoveWavelength(wxCommandEvent& event)
{
	if (!isInitialized())
		return;

	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	if (rowDelete >= 0 && rowDelete < optProperties->GetNumberRows() && rowDelete < opt->getNumWavelengthData()) {
		opt->RemoveWavelengthIndex(rowDelete);
		optProperties->DeleteRows(rowDelete);
		rowDelete = -1;
		UpdateGUIValues(event);
	}

	rowDelete = -1;
	UpdateGUIValues(event);
}

void devMatOpticalsPanel::CheckConditions(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	curMode = 0;
	double n, a, b;
	bool goodA, goodB, goodGlobN, goodN;
	n = a = b = -1.0;
	goodA = goodB = goodGlobN = false;

	globalN->GetValue().ToDouble(&n);
	goodA = globalA->GetValue().ToDouble(&a);
	goodB = globalB->GetValue().ToDouble(&b);

	if (n > 0.0)
		goodGlobN = true;

	if (goodA && goodB)
	{
		abTxt->SetLabel(wxT("\u2611 a and b for the material"));
		curMode = 3;
	}
	else
		abTxt->SetLabel(wxT("\u2610 a and b for the material"));

	if (optProperties->GetNumberRows() == 0)
	{
		nkTxt->SetLabel(wxT("\u2610 n and k for each wavelength"));
		alphanTxt->SetLabel(wxT("\u2610 alpha for each wavelength plus n"));
		return; //check if everything empty
	}
	

	
	for (int row = 0; row < optProperties->GetNumberRows(); row++)
	{
		double wave, alpha, n;
		wave = alpha = n = -1.0;
		bool gW = optProperties->GetCellValue(row, 0).ToDouble(&wave);
		bool gA = optProperties->GetCellValue(row, 3).ToDouble(&alpha);
		bool gN = optProperties->GetCellValue(row, 1).ToDouble(&n);
		if (gN && n <= 0.0)
			gN = goodGlobN;	//but global might be good!

		if (!gW || !gA || !gN || wave <= 0.0 || alpha < 0.0)
		{
			alphanTxt->SetLabel(wxT("\u2610 alpha for each wavelength plus n"));
			break;
		}
		if (row == optProperties->GetNumberRows() - 1)
		{
			alphanTxt->SetLabel(wxT("\u2611 alpha for each wavelength plus n"));
			curMode = 2;
		}
	}

	


	for (int row = 0; row < optProperties->GetNumberRows(); row++)
	{
		double wave, n, k;
		bool goodWave = optProperties->GetCellValue(row, 0).ToDouble(&wave);
		goodN = optProperties->GetCellValue(row, 1).ToDouble(&n);
		
		if (goodN && n <= 0.0)	//this n was bad, but is there a good global N to use instead?
			goodN = goodGlobN;
		if (goodWave && wave <= 0.0)
			goodWave = false;

		bool goodK = optProperties->GetCellValue(row, 2).ToDouble(&k);
		if (!goodWave || !goodN || !goodK)
		{
			nkTxt->SetLabel(wxT("\u2610 n and k for each wavelength"));
			break;
		}
		if (row == optProperties->GetNumberRows() - 1)	//last one and it hasn't failed
		{
			nkTxt->SetLabel(wxT("\u2611 n and k for each wavelength"));
			curMode = 1;
		}
	}
}

void devMatOpticalsPanel::GenFromAB(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Material* mat = getMat();
	if (mat == nullptr)
		return;
	Optical& opt = mat->getOpticalData();

	double a, b, n;
	a = b = n = 0.0;
	bool goodA = globalA->GetValue().ToDouble(&a);
	bool goodB = globalB->GetValue().ToDouble(&b);
	bool goodN = globalN->GetValue().ToDouble(&n);
	if (goodN && n == 0.0)
		goodN = false;

	if (!goodA || !goodB || !goodN)
		return;

	double bandGap = mat->getBandGap();
	if (bandGap < 0.0)
		bandGap = 0.0;

	double start = bandGap - 0.01;
	double end = start + mat->getAffinity() * 2.0;
	
	opt.setABN(a, b, n);
	opt.ClearWavelengths();
	opt.GenerateWavelengthsFromAB(start, end, 0.01, bandGap);	//doesn't actually use n...
	TransferToGUI();
}

void devMatOpticalsPanel::UpdateGUIValues(wxCommandEvent& evt)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();	//only null if there is no material!
	if (opt == nullptr)
	{
		optProperties->Disable();
		globalA->Disable();
		globalN->Disable();
		globalB->Disable();
		remWave->Disable();
		WavelengthDivideTen->Disable();
		WavelengthTimesTen->Disable();
		
		btnSave->Disable();
		btnLoad->Disable();
		btnClear->Disable();

		inpWavetxt->Disable();
		inpNTxt->Disable();
		inpKTxt->Disable();
		inpATxt->Disable();
		addWave->Disable();
		GenAB->Disable();
		chk_disableEmissions->Disable();
		chk_supressEmisOutput->Disable();
		txt_EmissionEfficiency->Disable();
	}
	else
	{
		optProperties->Enable();
		globalA->Enable();
		globalN->Enable();
		globalB->Enable();
		
		btnSave->Enable();
		btnLoad->Enable();
		

		inpWavetxt->Enable();
		inpNTxt->Enable();
		inpKTxt->Enable();
		inpATxt->Enable();

		double n, k, alpha, lambda, nGlob, aGlob, bGlob;
		bool goodLambda, goodN, goodK, goodA, goodNGlob, goodaGlob, goodBglob;
		goodLambda = goodN = goodK = goodA = false;
		if (inpWavetxt->GetValue().ToDouble(&lambda))
		{
			if (lambda > 0.0)
				goodLambda = true;
		}
		if (inpNTxt->GetValue().ToDouble(&n))
		{
			if (n > 0.0)
				goodN = true;
		}
		else if (globalN->GetValue().ToDouble(&n))
		{
			if (n > 0.0)
				goodN = true;
		}
		else if (globalN->GetValue().ToDouble(&n))
		{
			if (n > 0.0)
				goodN = true;
		}
		if (inpKTxt->GetValue().ToDouble(&k))
		{
			if (k >= 0.0)
				goodK = true;
		}
		if (inpATxt->GetValue().ToDouble(&alpha))
		{
			if (alpha >= 0.0)
				goodA = true;
		}

		if (goodLambda && goodN && (goodK || goodA))	//check for good data before adding a wavelength
			addWave->Enable();
		else
			addWave->Disable();


		if (opt->hasWavelengths())
		{
			WavelengthDivideTen->Enable();
			WavelengthTimesTen->Enable();
			btnClear->Enable();
//			wxGridCellCoordsArray selC = optProperties->GetSelectedCells();
//			wxArrayInt selcol = optProperties->GetSelectedCols();
//			wxArrayInt selrow = optProperties->GetSelectedRows();
//			wxGridCellCoordsArray selTL = optProperties->GetSelectionBlockTopLeft();
//			wxGridCellCoordsArray selBR = optProperties->GetSelectionBlockBottomRight();
			
			if (rowDelete < optProperties->GetNumberRows() && rowDelete >= 0 && optProperties->HasFocus())
				remWave->Enable();
			else
				remWave->Disable();

//			int sz = selC.size() + selcol.size() + selrow.size() + selTL.size() + selBR.size();
			//remWave->Enable(sz > 0);	//only remove if they're selected
		}
		else
		{
			btnClear->Disable();
			WavelengthDivideTen->Disable();
			WavelengthTimesTen->Disable();
			remWave->Disable();
		}

		goodaGlob = goodBglob = goodNGlob = false;

		if (globalA->GetValue().ToDouble(&aGlob))
			goodaGlob = true;
		if (globalN->GetValue().ToDouble(&nGlob))
		{
			if (nGlob > 0.0)
				goodNGlob = true;
		}
		if (globalB->GetValue().ToDouble(&bGlob))
			goodBglob = true;

		GenAB->Enable(goodaGlob && goodBglob && goodNGlob);

		chk_disableEmissions->Enable();
		if (chk_disableEmissions->GetValue() == true)
		{
			chk_supressEmisOutput->Disable();
			txt_EmissionEfficiency->Disable();
		}
		else
		{
			chk_supressEmisOutput->Enable();
			txt_EmissionEfficiency->Enable();
		}

	}
	
	CheckConditions(evt);
}

void devMatOpticalsPanel::TransferToMdesc(wxCommandEvent& evt)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;
	UpdateOptEff(evt);
	UpdateA(evt);
	UpdateN(evt);
	UpdateB(evt);
	UpdateCBox(evt);

	opt->ClearWavelengths();

	unsigned int numRows = optProperties->GetNumberRows();
	for (unsigned int i = 0; i < numRows; ++i)
	{
		double wave, n, k, a;
		wave = n = k = a = -1.0;
		float wv = -1.0f;

		optProperties->GetCellValue(i, 0).ToDouble(&wave);
		optProperties->GetCellValue(i, 1).ToDouble(&n);
		optProperties->GetCellValue(i, 2).ToDouble(&k);
		optProperties->GetCellValue(i, 3).ToDouble(&a);

		opt->AddWavelength(float(wave), n, k, a, true, true, false);
	}
	opt->SortWavelengths();

	opt->hasWavelengths() ? opt->clearUseAB() : opt->setUseAB();
}

void devMatOpticalsPanel::ModifyWavelength(wxGridEvent& event)
{
	if (!isInitialized())
		return;
	//avoid updating EVERYTHING!!
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;	//not actually doing anything!
	int row, col;

	row = event.GetRow();	//this will be the index modified
	col = event.GetCol();
	
	if (row >= opt->getNumWavelengthData() || row<0)	//wanna go by index, and this is invalid
		return;
	
	//the wavelengths SHOULD be in order - it's a map!
	//so this is ugly, but if editing wavelength, it may start modifying the wrong one!
	//save old values
	std::pair<float, NK> prevValues = opt->getWaveData(row);
	double newVal;

	wxString valStr = optProperties->GetCellValue(row, col);
	if (valStr.ToDouble(&newVal))
	{
		if (col == 0)	//new wavelength. Ruh Roh. changing the key!
		{
			float newWavelength = (float)newVal;
			if (opt->modifyWaveDataIndex(row, newVal, NK::wavelength))
			{
				//row is no longer pointing to the correct value
				TransferToGUI();	//update all the data, which reorganized the chart
			}
			else if (newWavelength != prevValues.first)//it could not add the new wavelength. the wavelength is invalid or a duplicate of pre-existing
			{
				//therefore, can't change the current wavelength because it's being modified...
				//changes are made specifically when committed! not with each stroke of the button.
				//if this was changed to a pre-existing thing, set it back to what it was!
				wxString oldVal;
				oldVal << prevValues.first;
				optProperties->SetCellValue(oldVal, row, col);
			}

		}
		else if (col == 1)	//new n
		{
			opt->modifyWaveDataIndex(row, newVal, NK::index_refraction);
		}
		else if (col == 2)	//new k
		{
			opt->modifyWaveDataIndex(row, newVal, NK::extinction_coefficient);
			wxString tmpStr = "";
			tmpStr << opt->getWaveData(row).second.getAlpha();
			optProperties->SetCellValue(tmpStr, row, 3);
		}
		else if (col == 3)	//new a
		{
			opt->modifyWaveDataIndex(row, newVal, NK::absorption_coefficient);
			wxString tmpStr = "";
			tmpStr << opt->getWaveData(row).second.getK();
			optProperties->SetCellValue(tmpStr, row, 2);
		}
	}
	else //invalid string committed
	{
		wxString tmpStr = "";
		if (col == 0)
			tmpStr << prevValues.first;
		else if (col == 1)
			tmpStr << prevValues.second.getN();
		else if (col == 2)
			tmpStr << prevValues.second.getK();
		else if (col == 3)
			tmpStr << prevValues.second.getAlpha();
		//change it back!
		optProperties->SetCellValue(tmpStr, row, col);
	}

}

void devMatOpticalsPanel::CheckConditionsWrapper(wxGridEvent& event)
{
	if (!isInitialized())
		return;
	CheckConditions(event);
}

void devMatOpticalsPanel::Clear()
{
	if (!isInitialized())
		return;
	if (optProperties->GetNumberRows() > 0)
		optProperties->DeleteRows(0, optProperties->GetNumberRows());
	globalN->Clear();
	globalA->Clear();
	globalB->Clear();
	wxCommandEvent eventBlank;
	CheckConditions(eventBlank);
}

void devMatOpticalsPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	Optical* opt = getOptical();
	if (opt)
	{
		ValueToTBox(globalN, opt->getGlobalN());
		ValueToTBox(globalA, opt->getGlobalA());
		ValueToTBox(globalB, opt->getGlobalB());
		if (opt->getGlobalN() < 0.0)
			globalN->Clear();
		if (opt->getGlobalA() < 0.0)
			globalA->Clear();
		if (opt->getGlobalB() < 0.0)
			globalB->Clear();

		unsigned int numWL = opt->getNumWavelengthData();
		unsigned int gridSz = optProperties->GetNumberRows();
		if (numWL > gridSz)
			optProperties->AppendRows(numWL - gridSz);
		else if (numWL < gridSz)
			optProperties->DeleteRows(0, gridSz - numWL, false);

		unsigned int row = 0;
		wxString tmpStr = "";

		for (unsigned int index = 0; index < numWL; ++index)
		{
			auto it = opt->getWaveData(index);
			tmpStr = "";
			tmpStr << it.first;
			optProperties->SetCellValue(tmpStr, row, 0);
			tmpStr = "";
			tmpStr << it.second.getN();
			optProperties->SetCellValue(tmpStr, row, 1);
			tmpStr = "";
			tmpStr << it.second.getK();
			optProperties->SetCellValue(tmpStr, row, 2);
			tmpStr = "";
			tmpStr << it.second.getAlpha();
			optProperties->SetCellValue(tmpStr, row, 3);

			row++;
		}

		chk_disableEmissions->SetValue(!opt->isEmissionsEnabled());
		chk_supressEmisOutput->SetValue(!opt->isOutputsShown());
		ValueToTBox(txt_EmissionEfficiency, opt->getOpticEfficiency());
	}
	else
	{
		globalN->Clear();
		globalA->Clear();
		globalB->Clear();
		txt_EmissionEfficiency->Clear();
		if (optProperties->GetNumberRows() > 0)
			optProperties->DeleteRows(0, optProperties->GetNumberRows());
	}
	UpdateGUIValues(wxCommandEvent());
//	CheckConditions(wxCommandEvent());
}

void devMatOpticalsPanel::UpdateOptEff(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;
	double val = 1.0;
	if (txt_EmissionEfficiency->GetValue().ToDouble(&val))
	{
		if (val > 1.0)
			val = 1.0;
		if (val < 0.0)
			val = 0.0;
		opt->opticEfficiency = val;
	}
	else if (opt->disableOpticalEmissions)	//but it should be disabled!
		opt->opticEfficiency = 0.0;	//so just set it to zero
	else if (opt->opticEfficiency == -1.0)	//it hasn't been properly defined
		opt->opticEfficiency = 1.0;	//we'll just say al in if it hasn't been previously set to something
		//at this point, just leave it as is. It can't possibly have an invalid value stored within.
		//almost...

}

void devMatOpticalsPanel::EnableAll(bool en)
{
	if (!isInitialized())
		return;
	optProperties->Enable(en);
	globalN->Enable(en);
	globalA->Enable(en);
	globalB->Enable(en);
	addWave->Enable(en);
	remWave->Enable(en);

	if (en == false)
		Clear();
	else
	{
		Material* mat = ((devMaterialPanel*)GetParent()->GetParent())->getCurrentMaterial();
		if (mat)
		{
			wxString tmpStr = "";
			if (mat->optics)
			{
				tmpStr << mat->optics->n;
				globalN->SetValue(tmpStr);
				tmpStr = "";
				tmpStr << mat->optics->a;
				globalA->SetValue(tmpStr);
				tmpStr = "";
				tmpStr << mat->optics->b;
				globalB->SetValue(tmpStr);

				optProperties->ClearGrid();
				if (mat->optics->WavelengthData.size() > 0)
				{
					if (mat->optics->WavelengthData.size() > (unsigned int)optProperties->GetNumberRows())
						optProperties->AppendRows(mat->optics->WavelengthData.size() - optProperties->GetNumberRows());

					unsigned int row = 0;
					for (std::map<float, NK>::iterator it = mat->optics->WavelengthData.begin(); it != mat->optics->WavelengthData.end(); ++it)
					{
						tmpStr << it->first;
						optProperties->SetCellValue(tmpStr, row, 0);
						tmpStr = "";
						tmpStr << it->second.n;
						optProperties->SetCellValue(tmpStr, row, 1);
						tmpStr = "";
						tmpStr << it->second.k;
						optProperties->SetCellValue(tmpStr, row, 2);
						tmpStr = "";
						tmpStr << it->second.alpha;
						optProperties->SetCellValue(tmpStr, row, 3);

						row++;
					}

				}
			}

		}
	}
}

void devMatOpticalsPanel::WaveX10(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	std::vector<float> waveLengths;
	std::vector<NK> data;

	//it was being pissy with reverse iterator validity.
	AddMapKeysToVector(opt->WavelengthData, waveLengths);
	AddMapValsToVector(opt->WavelengthData, data);

	opt->WavelengthData.clear();

	for (unsigned int i = 0; i < waveLengths.size(); ++i)
	{
		waveLengths[i] *= 10.0f;
		opt->AddWavelength(waveLengths[i], data[i].n, -1.0, data[i].alpha, false, true);
	}

	TransferToGUI();
}

void devMatOpticalsPanel::WaveD10(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	std::map<float, NK>::iterator ItNext;
	for (std::map<float, NK>::iterator It = opt->WavelengthData.begin(); It != opt->WavelengthData.end(); It = ItNext)
	{
		float Wv = It->first / 10.0f;
		NK tmpNK = It->second;
		ItNext = It;
		ItNext++;

		opt->WavelengthData.erase(It);	//funkadelic stuff because reverse iterator not normal iterator and rbegin=end()/rend=before begin()

		//keep alpha the same, so recalculate k. Most people get alpha and not k...
		opt->AddWavelength(Wv, tmpNK.n, -1.0, tmpNK.alpha, false, true);
	}
	TransferToGUI();
}

void devMatOpticalsPanel::UpdateA(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	globalA->GetValue().ToDouble(&opt->a);
	CheckConditions(event);

	double a, b, n;
	a = b = n = 0.0;
	bool goodA = globalA->GetValue().ToDouble(&a);
	bool goodB = globalB->GetValue().ToDouble(&b);
	bool goodN = globalN->GetValue().ToDouble(&n);
	if (goodN && n <= 0.0)
		goodN = false;

	GenAB->Enable(goodN && goodB && goodA);
}

void devMatOpticalsPanel::UpdateB(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	globalB->GetValue().ToDouble(&opt->b);
	CheckConditions(event);

	double a, b, n;
	a = b = n = 0.0;
	bool goodA = globalA->GetValue().ToDouble(&a);
	bool goodB = globalB->GetValue().ToDouble(&b);
	bool goodN = globalN->GetValue().ToDouble(&n);
	if (goodN && n <= 0.0)
		goodN = false;

	GenAB->Enable(goodN && goodB && goodA);

}

void devMatOpticalsPanel::UpdateN(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	globalN->GetValue().ToDouble(&opt->n);
	CheckConditions(event);

	double a, b, n;
	a = b = n = 0.0;
	bool goodA = globalA->GetValue().ToDouble(&a);
	bool goodB = globalB->GetValue().ToDouble(&b);
	bool goodN = globalN->GetValue().ToDouble(&n);
	if (goodN && n <= 0.0)
		goodN = false;

	GenAB->Enable(goodN && goodB && goodA);
}

void devMatOpticalsPanel::CellClicked(wxGridEvent& event)
{
	if (!isInitialized())
		return;
	rowDelete = event.GetRow();
	wxString tmp = "Remove Wavelength (";
	tmp << (rowDelete + 1) << ")";
	remWave->SetLabel(tmp);
	UpdateGUIValues(event);
	
}

void devMatOpticalsPanel::UpdateCBox(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	opt->disableOpticalEmissions = chk_disableEmissions->GetValue();
	opt->suppressOpticalOutputs = chk_supressEmisOutput->GetValue();
	//this has repercussions to the defect page as well
	getMyClassParent()->UpdateGUIValues();
//	UpdateGUIValues(event);
}

void devMatOpticalsPanel::SaveOptics(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Optical files (*.optl)|*.optl"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;
	//	SwitchMaterial(event);	//basically just making sure it's up to date? Not really necessary...

	TransferToMdesc(event);

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		opt->SaveToXML(oFile);
		oFile.closeFile();
	
	}


/*	materials_list[prevSelection].globalN = globalN->GetValue();
	materials_list[prevSelection].globalA = globalA->GetValue();
	materials_list[prevSelection].globalB = globalB->GetValue();
	materials_list[prevSelection].opt_method = curMode;
	materials_list[prevSelection].opt_props.clear();
	for (int row = 0; row < optProperties->GetNumberRows(); row++)
	{
		wave_properties props;
		props.wavelength = optProperties->GetCellValue(row, 0);
		props.n = optProperties->GetCellValue(row, 1);
		props.k = optProperties->GetCellValue(row, 2);
		props.alpha = optProperties->GetCellValue(row, 3);
		materials_list[prevSelection].opt_props.push_back(props);
	}
	*/
}

void devMatOpticalsPanel::LoadOptics(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Optical files (*.optl)|*.optl"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	ModelDescribe* mdesc = getMDesc();
	//don't get rid of current wavelengths! Let them be added on. If they
	LoadOpticalChildren(file, opt, *mdesc);

	file.close();

	mdesc->Validate();

	TransferToGUI();

	
	/*
	globalN->SetValue(materials_list[prevSelection].globalN);
	globalA->SetValue(materials_list[prevSelection].globalA);
	globalB->SetValue(materials_list[prevSelection].globalB);
	curMode = materials_list[prevSelection].opt_method;
	if (optProperties->GetNumberRows() > 0)
		optProperties->DeleteRows(0, optProperties->GetNumberRows());
	optProperties->AppendRows(materials_list[prevSelection].opt_props.size());
	for (unsigned int i = 0; i < materials_list[prevSelection].opt_props.size(); i++)
	{
		optProperties->SetCellValue(i, 0, materials_list[prevSelection].opt_props[i].wavelength);
		optProperties->SetCellValue(i, 1, materials_list[prevSelection].opt_props[i].n);
		optProperties->SetCellValue(i, 2, materials_list[prevSelection].opt_props[i].k);
		optProperties->SetCellValue(i, 3, materials_list[prevSelection].opt_props[i].alpha);
	}

	*/
}

void devMatOpticalsPanel::ClearWavelengths(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Optical* opt = getOptical();
	if (opt == nullptr)
		return;

	opt->WavelengthData.clear();
	TransferToGUI();
}

void devMatOpticalsPanel::EnableOpt(bool enable)
{
	if (!isInitialized())
		return;
	globalN->Enable(enable);
	globalA->Enable(enable);
	globalB->Enable(enable);
	optProperties->Enable(enable);
}

void devMatOpticalsPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(gridSizer);
	optProperties->AutoSize();
}


BEGIN_EVENT_TABLE(devMatOpticalsPanel, wxScrolledWindow)

EVT_TEXT(OPT_GLOB_N, devMatOpticalsPanel::UpdateN)
EVT_TEXT(OPT_GLOB_A, devMatOpticalsPanel::UpdateA)
EVT_TEXT(OPT_GLOB_B, devMatOpticalsPanel::UpdateB)
EVT_TEXT(OPT_EMIS_EFF, devMatOpticalsPanel::UpdateOptEff)
EVT_TEXT(wxID_ANY, devMatOpticalsPanel::UpdateGUIValues)
EVT_GRID_CMD_CELL_CHANGED(MOD_WAVE, devMatOpticalsPanel::ModifyWavelength)
EVT_GRID_CELL_CHANGED(devMatOpticalsPanel::CheckConditionsWrapper)
EVT_GRID_CMD_SELECT_CELL(MOD_WAVE, devMatOpticalsPanel::CellClicked)
EVT_BUTTON(NEW_WAVE, devMatOpticalsPanel::AddWavelength)
EVT_BUTTON(REM_WAVE, devMatOpticalsPanel::RemoveWavelength)
EVT_BUTTON(OPT_WAVE_X10, devMatOpticalsPanel::WaveX10)
EVT_BUTTON(OPT_WAVE_D10, devMatOpticalsPanel::WaveD10)
EVT_BUTTON(OPT_GEN_FROM_AB, devMatOpticalsPanel::GenFromAB)
EVT_BUTTON(OPT_SAVE, devMatOpticalsPanel::SaveOptics)
EVT_BUTTON(OPT_MERGE, devMatOpticalsPanel::LoadOptics)
EVT_BUTTON(OPT_CLEAR_WAVES, devMatOpticalsPanel::ClearWavelengths)
EVT_CHECKBOX(OPT_EMIS_CHKS, devMatOpticalsPanel::UpdateCBox)


END_EVENT_TABLE()