#ifndef SIMULATION_PANEL_H
#define SIMULATION_PANEL_H


#include "wx/wxprec.h"
#include "wx/panel.h"
#include "wx/notebook.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxWindow;
class ModelDescribe;
class wxcommandEvent;

class simTransientPanel;
class simCalculationPanel;
class simGeneralPanel;
class simSpectraPanel;
class simSteadyStateBook;
class simOutputPanel;


class simulationWindow : public wxNotebook
{
	simCalculationPanel* calcPanel;
	simGeneralPanel* generalPanel;
	simSpectraPanel* spectraPanel;
	simSteadyStateBook* ssPanel;
	simTransientPanel* TransPanel;
	simOutputPanel* outputPanel;
public:
	simulationWindow(wxWindow* parent, wxWindowID id);
	virtual ~simulationWindow();
	

	simCalculationPanel* getCalcPanel() { return calcPanel; }
	simGeneralPanel* getGeneralPanel() { return generalPanel; }
	simSpectraPanel* getSpectraPanel() { return spectraPanel; }
	simSteadyStateBook* getSSPanel() { return ssPanel; }
	simTransientPanel* getTransPanel() { return TransPanel; }
	simOutputPanel* getOutputPanel() { return outputPanel; }

	mainFrame* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI();
	void ChangeSSPanel(wxBookCtrlEvent& event);
	Simulation* getSim();
private:
//	wxSizer* szrPrimary;
	bool isInitialized() { return (outputPanel != nullptr); }
	//wx macro for event handling
	DECLARE_EVENT_TABLE()
};

#endif