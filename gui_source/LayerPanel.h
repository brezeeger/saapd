#ifndef LAYER_PANEL_H
#define LAYER_PANEL_H


#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "wx/scrolwin.h"

#include "../kernel_source/Layers.h"

class wxWindow;
class wxCommandEvent;
class wxComboBox;
class wxRadioBox;
class wxCheckBox;
class wxTextCtrl;
class wxButton;
class wxBoxSizer;
class wxStaticText;
class wxFlexGridSizer;
class wxSizer;
class ModelDescribe;
class devMaterialPanel;
class Material;
class Impurity;
class wxNotebook;
class Layer;
class mainFrame;
class PlotWindow;


class devLayerTopPanel : public wxPanel
{
public:
	devLayerTopPanel(wxWindow* parent);
	~devLayerTopPanel();

//	devLpropertiesPanel* lpropertiesWindow;
//	devLmaterialsPanel* lmaterialsWindow;
//	devLimpurityPanel* limpuritiesWindow;

	

	//all go to the layer Bar
	wxComboBox* cbLayerChooser;
	wxButton* btnLayAdd;
	wxButton* btnLayRem;
	wxButton* btnLaySave;
	wxButton* btnLayLoad;
	wxButton* btnLayRename;
	wxButton* btnLayDuplicate;
	
	wxNotebook* nbLayersWindow;	//may cut out completely!


	////////////////////Section 1/////////////////////////

	wxCheckBox* chkAffectMesh;
	wxCheckBox* chkExclusiveMesh;
	wxCheckBox* chkHide;
	wxCheckBox* chkContact;
	wxCheckBox* chkPtEmissions;
	wxCheckBox* chkAllRates;

	wxTextCtrl* txtScaling;
	wxTextCtrl* txtSpacingCenter;
	wxTextCtrl* txtSpacingEdge;

	wxComboBox* cbLayerShape;
	wxComboBox *cbLyrShapeUnits;
	wxTextCtrl *txtLyrStartX, *txtLyrStartY, *txtLyrStartZ;
	wxTextCtrl *txtLyrDim1X, *txtLyrDim1Y, *txtLyrDim1Z;
	wxTextCtrl *txtLyrDim2X, *txtLyrDim2Y, *txtLyrDim2Z;

	wxComboBox *cbLayerShapeVertexList;
	wxTextCtrl *txtLyrShapeVertX, *txtLyrShapeVertY, *txtLyrShapeVertZ;

	wxStaticText *Scaling, *lyrX, *lyrY, *lyrZ;
	wxStaticText *SpacingCenter, *SpacingEdge;
//	wxStaticText *PtEmissions, *AllRates;
	wxStaticText *LyrStart, *LyrDim1, *LyrDim2;
	
	///////////////////////Section 2/////////////////////////////

	wxComboBox *cbAllMats;
	wxButton *btnAddMat;
	wxButton *btnChangeMat;

	wxComboBox *cbMatDescriptors;	//just go with the material name!
	wxButton *btnRemoveMat;

	wxComboBox *cbMatDistType;
	wxComboBox *cbMatUnits;
	wxTextCtrl *txtMatStartX, *txtMatStartY, *txtMatStartZ;
	wxTextCtrl *txtMatHalfX, *txtMatHalfY, *txtMatHalfZ;

	wxButton *btnViewMaterial;

	wxStaticText *MatStart, *MatHalf, *MatX, *MatY, *MatZ;

	/////////////////////////Section 3/////////////////////////////

	wxComboBox *cbAllImps;
	wxButton *btnAddImp;
	wxButton *btnChangeImp;

	wxComboBox *cbImpDescriptors;	//just go with the Imperial name!
	wxButton *btnRemoveImp;

	wxComboBox *cbImpDistType;
	wxComboBox *cbImpUnits;
	wxTextCtrl *txtImpMultiplier;
	wxTextCtrl *txtImpStartX, *txtImpStartY, *txtImpStartZ;
	wxTextCtrl *txtImpHalfX, *txtImpHalfY, *txtImpHalfZ;

	wxStaticText *ImpStart, *ImpHalf, *ImpX, *ImpY, *ImpZ, *ImpMultiplier, *ImpDensity;

	wxButton *btnViewImpurity;

	/////////////////////////////Bottom Section Display ///////////////////////

	PlotWindow *pltWind;	//note, this is the LAST thing to be allocated. Therefore, checks on this will verify that ALL the above has also been initialized.

	///////////////////////////////////////////////////////

	

	void SizeRight();

	void NewLayer(wxCommandEvent& event);
	void RemoveLayer(wxCommandEvent& event);
	void RenameLayer(wxCommandEvent& event);
	void SwitchLayer(wxCommandEvent& event);
	void saveLayer(wxCommandEvent& event);
	void LoadLayer(wxCommandEvent& event);
	void DuplicateLayer(wxCommandEvent& event);
	//need load/duplicate

	void UpdateScales(wxCommandEvent& event);
	void CboxClick(wxCommandEvent& event);
	void TBoxUpdate(wxCommandEvent& event);
	void ShapeUpdate(wxCommandEvent& event);
	void ShapeTextUpdate(wxCommandEvent& event);
	void AlterShapeVertex(wxCommandEvent& event);	//for modifying the vertex itself
	void ChangeVertexView(wxCommandEvent& event);	//for selecting a new vertex to view
	
	void BtnChangeMat(wxCommandEvent& event);
	void DMChangeMat(wxCommandEvent& event);
	void BtnRemoveMat(wxCommandEvent& event);
	void BtnAddMat(wxCommandEvent& event);
	void DMChangeMatFunct(wxCommandEvent& event);
	void BtnViewMat(wxCommandEvent& event);

	void BtnChangeImp(wxCommandEvent& event);
	void DMChangeImp(wxCommandEvent& event);
	void BtnRemoveImp(wxCommandEvent& event);
	void BtnAddImp(wxCommandEvent& event);
	void DMChangeImpFunct(wxCommandEvent& event);
	void BtnViewImp(wxCommandEvent& event);

	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI();
	void DisableDimensionalGUIs();

	void UpdateShapeTextDisplays(wxObject* skip = nullptr);
	void UpdateLayerListDisplay();	//update the layer list selection, while hopefully not generating events unless it actually changes!
	
	void UpdatePlot();

	Layer* getLayer();
	Material* getCurMat();
	Impurity* getCurImp();
	ldescriptor<Material*>* getCurMatDescriptor();
	ldescriptor<Impurity*>* getCurImpDescriptor();
	ModelDescribe* getMDesc();
	mainFrame* getMyClassParent();
	unsigned int ConvertImpIndex(int index);

	double getUnitFactor(bool toMdesc, wxComboBox* which);

private:
	void VerifyInternalVars();
	Layer* curLayer;
	ldescriptor<Material*>* curMatInLayer;
	ldescriptor<Impurity*>* curImpInLayer;
	std::vector<bool> hidePlot;
//	wxSizer* laySizer;

	//SIZERS
	wxBoxSizer *bxszrPrimary;	//vertical, handles everything
	wxBoxSizer *bxszrLayBar;	//horizontal, the bar on top for layer creation and whatnot
	wxBoxSizer *bxszrMainSeparator;	//horizontal, the three main areas
	/////////left side
	wxBoxSizer *bxszrLyrPrim;	//vertical for all the random shit going down it
	wxFlexGridSizer *fgszrLyrShape;	//all the x,y,z crap
	wxFlexGridSizer *fgszrLyrEntries;	//all the x,y,z crap
	//////////center
	wxBoxSizer *bxszMatPrim;	//vertical for all the random shit going down it
	wxBoxSizer *bxszMatListAdd, *bxszMatChangeRemove;
	wxFlexGridSizer *fgszrMatShape;	//all the x,y,z crap
	//////////right side
	wxBoxSizer *bxszImpPrim;	//vertical for all the random shit going down it
	wxBoxSizer *bxszImpListAdd, *bxszImpChangeRemove, *bxSzImpMultiplier;
	wxFlexGridSizer *fgszrImpShape;	//all the x,y,z crap
	///////////////////

	int prevSelection;
	int nextID;

	DECLARE_EVENT_TABLE()
};
#endif