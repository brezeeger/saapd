#include "myWXImageHandler.h"
#include <wx/wx.h>
#include <wx/sizer.h>

//see https://wiki.wxwidgets.org/An_image_panel

myImagePane::myImagePane(wxWindow* parent, std::string imageLoc, wxBitmapType type) :
wxPanel(parent)
{
	picDescription = new wxImage(imageLoc, type);
	if (picDescription->IsOk() == false)
	{
		delete picDescription;
		picDescription = nullptr;
		imageLoc = "Failed to find " + imageLoc;
		wxDialog diag(this, wxID_ANY, imageLoc);
		diag.ShowModal();
	}
	w = h = -1;
}

myImagePane::myImagePane(wxWindow* parent, char *imageData[]) : wxPanel(parent) {
	picDescription = new wxImage(imageData);
	if (picDescription->IsOk() == false)
	{
		delete picDescription;
		picDescription = nullptr;
		wxDialog diag(this, wxID_ANY, "Bad xpm data");
		diag.ShowModal();
	}
	w = h = -1;
}

myImagePane::~myImagePane()
{
	delete picDescription;
}

void myImagePane::OnPaint(wxPaintEvent& event)
{
	if (picDescription)
	{
		wxPaintDC dc(this);
		render(dc);
	}
}

void myImagePane::paint()
{
	if (picDescription)
	{
		wxClientDC dc(this);
		render(dc);
	}
}

void myImagePane::render(wxDC& dc)
{
	if (picDescription)
	{
		int neww, newh;
		dc.GetSize(&neww, &newh);

		if (neww != w || newh != h)
		{
			picBitmap = wxBitmap(picDescription->Scale(neww, newh));	 /*, wxIMAGE_QUALITY_HIGH*/
			w = neww;
			h = newh;
			dc.DrawBitmap(picBitmap, 0, 0, false);
		}
		else{
			dc.DrawBitmap(picBitmap, 0, 0, false);
		}
	}
}

void myImagePane::OnSize(wxSizeEvent& event){
	Refresh();	//says draw again, but that's a new width/height. so it should correct itself?
	//skip the event.
	event.Skip();
}

BEGIN_EVENT_TABLE(myImagePane, wxPanel)
EVT_PAINT(myImagePane::OnPaint)
EVT_SIZE(myImagePane::OnSize)
END_EVENT_TABLE()