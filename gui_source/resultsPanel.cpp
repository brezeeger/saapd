#include "resultsPanel.h"
#include "guiwindows.h"
#include "myPlottingWrapper.h"

resultsPanel::resultsPanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
	resSizer = new wxBoxSizer(wxHORIZONTAL);
	
	whichData.x = true;
	whichData.y = whichData.z = whichData.point = false;
	whichData.Ef = false;
	whichData.Efn = false;
	whichData.Efp = false;
	whichData.Ec = false;
	whichData.Ev = false;
	whichData.Psi = false;
	whichData.electrons = false;
	whichData.holes = false;
	whichData.impCharge = false;
	whichData.n = false;
	whichData.p = false;
	whichData.deflight = false;
	whichData.condlight = false;
	whichData.genlight = false;
	whichData.genthermal = false;
	whichData.recthermal = false;
	whichData.srh1 = false;
	whichData.srh2 = false;
	whichData.srh3 = false;
	whichData.srh4 = false;
	whichData.rho = false;
	whichData.Jn = false;
	whichData.Jp = false;
	whichData.stddev = false;
	whichData.stderrBox = false;
	whichData.qe = false;
	whichData.scatter = false;
	whichData.j1ctc_x = whichData.j2ctc_x = whichData.j1_x = whichData.j2_x = whichData.javg_x = whichData.j1ctc_n_x = whichData.j2ctc_n_x
		= whichData.j1_n_x = whichData.j2_n_x = whichData.javg_n_x = whichData.j1ctc_p_x = whichData.j2ctc_p_x
		= whichData.j1_p_x = whichData.j2_p_x = whichData.javg_p_x = false;
	whichData.volt = whichData.volt_n = whichData.volt_p = false;

	cstm_xaxis = NULL;
	cstm_yaxis = NULL;
	cstm_plot = NULL;

	fileIndex = 0;
	plot = NULL;
	ActivePlotData = InPlotData = NULL;
	setX = AddToActive = RemoveFromActive = NULL;
	fileFilters = NULL;
	FilterFileGroup = FilterFileType = NULL;
	FilterFileIteration = NULL;
	fListfName = wxEmptyString;
	
	
	topSizer = new wxStaticBoxSizer(wxHORIZONTAL, this);
	RVertSizer = new wxBoxSizer(wxVERTICAL);
	//	CoordSizer = new wxBoxSizer(wxHORIZONTAL);
	LVertSizer = new wxBoxSizer(wxVERTICAL);
	DataSelectionBox = new wxStaticBoxSizer(wxVERTICAL, this, "Data Selection");
	PenControl = new wxStaticBoxSizer(wxVERTICAL, this, "Dataset Visual Control");
	PlotControlSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Plot Control");
	
	PenRGBControl = new wxBoxSizer(wxHORIZONTAL);
	PenStyleControl = new wxBoxSizer(wxHORIZONTAL);
	legendControl = new wxBoxSizer(wxHORIZONTAL);
	GrpOutSizer = new wxBoxSizer(wxHORIZONTAL);
	iterSizer = new wxBoxSizer(wxHORIZONTAL);
	ActInactSizer = new wxBoxSizer(wxHORIZONTAL);
	AddRemSetSizer = new wxBoxSizer(wxHORIZONTAL);
	PlotCustomizeSizer = new wxBoxSizer(wxHORIZONTAL);

	LoadDataButton = new wxButton(this, LOAD_RESULTS, wxT("Load Data"));
	savePlotButton = new wxButton(this, SAVE_PLOT_IMAGE, wxT("Save Plot Image"));
	RawPlotButton = new wxButton(this, TOGGLE_RAW_OUT, wxT("View Raw Data"));
	Btn_PlotCustom = new wxButton(this, SHOW_CPLOT_BTN, wxT("Build Custom Plot"));
	Btn_ClearFilteredFiles = new wxButton(this, CLR_FILTFILES, wxT("Unload Filtered Files"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	topSizer->Add(LoadDataButton, 0, wxALL, 2);
	topSizer->Add(savePlotButton, 0, wxALL, 2);
	topSizer->Add(RawPlotButton, 0, wxALL, 2);
	topSizer->Add(Btn_PlotCustom, 0, wxALL, 2);
	topSizer->Add(Btn_ClearFilteredFiles, 0, wxALL | wxALIGN_CENTER_VERTICAL, 2);
	topSizer->AddStretchSpacer();
	FileListBox = new MyListBox(this, FLIST_DCLICK, wxDefaultPosition, wxSize(150, 45));
	
	



	FileSelectSlider = new wxSlider(this, FLIST_SLIDER_CHANGE, 0, 0, 1, wxDefaultPosition, wxSize(166, 25), wxSL_HORIZONTAL);
	wxArrayString FilterGroup;
	FilterGroup.Add(wxT("Group"));
	FilterFileGroup = new wxComboBox(this, UPDATE_FILE_FILTER, wxT("Group"), wxDefaultPosition, wxSize(70, 25), FilterGroup, wxCB_DROPDOWN | wxCB_READONLY);




	wxArrayString FilterOut;
	FilterOut.Add(wxT("Output Type"));
	FilterOut.Add(wxT("Band Diagrams"));
	FilterOut.Add(wxT("JV Curves"));
	FilterOut.Add(wxT("Emissions"));
	FilterOut.Add(wxT("Absorption"));
	FilterOut.Add(wxT("Environments"));
	FilterOut.Add(wxT("Custom Plots"));
	FilterFileType = new wxComboBox(this, UPDATE_FILE_FILTER, wxT("Output Type"), wxDefaultPosition, wxSize(104, 25), FilterOut, wxCB_DROPDOWN | wxCB_READONLY);

	GrpOutSizer->Add(FilterFileGroup, 0, wxALL | wxEXPAND, 2);
	GrpOutSizer->Add(FilterFileType, 0, wxALL | wxEXPAND, 2);
	
	
	fileFilters = new wxTextCtrl(this, UPDATE_FILE_FILTER, wxT("Filter Files"), wxDefaultPosition, wxSize(93, 20));
	FilterFileIteration = new wxTextCtrl(this, UPDATE_FILE_FILTER, wxT(""), wxDefaultPosition, wxSize(30, 20));
	IterLabel = new wxStaticText(this, wxID_ANY, wxT("Iteration:"));
		
	
	iterSizer->Add(IterLabel, 0, wxALL, 2);
	iterSizer->Add(FilterFileIteration, 0, wxALL, 2);
	iterSizer->Add(fileFilters, 0, wxALL | wxEXPAND, 2);

	
	
	wxArrayString VarActive;
	VarActive.Add(wxT("Active"));
	ActivePlotData = new wxComboBox(this, SELECT_VAR_CHANGE, wxT("Active"), wxDefaultPosition, wxSize(87, 25), VarActive, wxCB_DROPDOWN | wxCB_READONLY);

	wxArrayString VarInactive;
	VarInactive.Add(wxT("Inactive"));
	InPlotData = new wxComboBox(this, INACTIVE_DDOWN, wxT("Inactive"), wxDefaultPosition, wxSize(87, 25), VarInactive, wxCB_DROPDOWN | wxCB_READONLY);
	RemoveFromActive = new wxButton(this, REMOVE_VAR_PLOT, wxT("Remove"), wxDefaultPosition, wxSize(50, 25));
	setX = new wxButton(this, CHANGE_XY_PLOT, wxT("Set X"), wxDefaultPosition, wxSize(35, 25));
	AddToActive = new wxButton(this, ADD_VAR_PLOT, wxT("Add"), wxDefaultPosition, wxSize(40, 25));

	
	DataSelectionBox->Add(GrpOutSizer, 0, wxALL, 0);
	DataSelectionBox->Add(iterSizer, 0, wxALL, 0);


	DataSelectionBox->Add(FileSelectSlider, 0, wxALL | wxEXPAND, 0);
	ActInactSizer->Add(ActivePlotData, 0, wxALL, 2);
	ActInactSizer->Add(InPlotData, 0, wxALL, 2);
	DataSelectionBox->Add(ActInactSizer, 0, wxALL, 0);

	AddRemSetSizer->Add(RemoveFromActive, 0, wxALL, 1);
	AddRemSetSizer->Add(setX, 0, wxALL, 1);
	AddRemSetSizer->Add(AddToActive, 0, wxLEFT | wxALIGN_CENTER_VERTICAL, 27);
	DataSelectionBox->Add(AddRemSetSizer, 0, wxALL, 0);

	LVertSizer->Add(DataSelectionBox, 0, wxALL, 0);

	txtRGBA = new wxStaticText(this, wxID_ANY, wxT("Line RGB:"));
	rPen = new wxTextCtrl(this, COLOR_BOX, wxT("R"), wxDefaultPosition, wxSize(37, 20));
	gPen = new wxTextCtrl(this, COLOR_BOX, wxT("G"), wxDefaultPosition, wxSize(37, 20));
	bPen = new wxTextCtrl(this, COLOR_BOX, wxT("B"), wxDefaultPosition, wxSize(37, 20));
	//aPen = new wxTextCtrl(this, COLOR_BOX, wxT("A"), wxDefaultPosition, wxSize(25,20));


	wxArrayString PWidthStrings;
	PWidthStrings.Add(wxT("Pen Width"));
	PWidthStrings.Add(wxT("1"));
	PWidthStrings.Add(wxT("2"));
	PWidthStrings.Add(wxT("3"));
	PWidthStrings.Add(wxT("4"));
	PWidthStrings.Add(wxT("5"));
	PWidthStrings.Add(wxT("7"));
	PWidthStrings.Add(wxT("9"));
	widthPen = new wxComboBox(this, UPDATE_P_WIDTH, wxT("Pen Width"), wxDefaultPosition, wxSize(87, 25), PWidthStrings, wxCB_DROPDOWN | wxCB_READONLY);

	wxArrayString PStyleStrings;
	PStyleStrings.Add(wxT("Pen Style"));
	PStyleStrings.Add(wxT("Solid"));
	PStyleStrings.Add(wxT("Dot"));
	PStyleStrings.Add(wxT("Long Dash"));
	PStyleStrings.Add(wxT("Short Dash"));
	PStyleStrings.Add(wxT("Dot Dash"));
	PStyleStrings.Add(wxT("Transparent"));
	PStyleStrings.Add(wxT("Scatter"));

	stylePen = new wxComboBox(this, UPDATE_P_STYLE, wxT("Pen Style"), wxDefaultPosition, wxSize(87, 25), PStyleStrings, wxCB_DROPDOWN | wxCB_READONLY);

	PenRGBControl->Add(txtRGBA, 0, wxALL, 2);
	PenRGBControl->Add(rPen, 0, wxALL, 2);
	PenRGBControl->Add(gPen, 0, wxALL, 2);
	PenRGBControl->Add(bPen, 0, wxALL, 2);
	//PenRGBControl->Add(aPen,0,wxALL,2);

	PenStyleControl->Add(widthPen, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 2);
	PenStyleControl->Add(stylePen, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 2);



	PenControl->Add(PenRGBControl, 0, wxALL, 0);
	PenControl->Add(PenStyleControl, 0, wxALL, 0);


	

	ScaleSizer = new wxFlexGridSizer(3, 2, 0, 0);
	legText = new wxStaticText(this, wxID_ANY, wxT("Legend Text:"));
	legendUpdate = new wxTextCtrl(this, UPDATE_LINE_LEGEND, wxT(""), wxDefaultPosition, wxSize(107, 20));
	
	statTxtScaleY = new wxStaticText(this, wxID_ANY, wxT("Scale Y:"));
	txtOffset = new wxStaticText(this, wxID_ANY, wxT("Offset Y:"));
	ScaleActYText = new wxTextCtrl(this, SCALE_Y_TXT, wxT("1.0"));
	OffsetActY = new wxTextCtrl(this, OFFSET_Y_TXT, wxT("0.0"));
	Btn_Normalize = new wxButton(this, NORMALIZE_BTN, wxT("Normalize"));
	Btn_ToggleZeros = new wxButton(this, TOGGLE_ZEROES_BTN, wxT("Hide Zeroes"));

	ScaleSizer->Add(legText, 0, wxALL|wxALIGN_RIGHT, 2);
	ScaleSizer->Add(legendUpdate, 0, wxALL | wxALIGN_RIGHT, 2);
	ScaleSizer->Add(statTxtScaleY,0,wxALL|wxALIGN_RIGHT,2);
	ScaleSizer->Add(ScaleActYText, 0, wxALL | wxALIGN_RIGHT, 2);
	ScaleSizer->Add(txtOffset, 0, wxALL | wxALIGN_RIGHT, 2);
	ScaleSizer->Add(OffsetActY, 0, wxALL | wxALIGN_RIGHT, 2);
	PenControl->Add(ScaleSizer, 0, wxALL | wxEXPAND | wxGROW, 0);

	NormZeroSzr = new wxBoxSizer(wxHORIZONTAL);
	NormZeroSzr->Add(Btn_ToggleZeros, 0, wxALL | wxALIGN_CENTER, 2);
	NormZeroSzr->Add(Btn_Normalize, 0, wxALL | wxALIGN_CENTER, 2);
	PenControl->Add(NormZeroSzr, 0, wxALL, 0);
	/////////////////////////////////
	szr_plotLabels = new wxFlexGridSizer(6, 2, 1, 1);
	lbl_XName = new wxStaticText(this, wxID_ANY, wxT("X Axis:"));
	lbl_YName = new wxStaticText(this, wxID_ANY, wxT("Y Axis:"));
	lbl_PlotName = new wxStaticText(this, wxID_ANY, wxT("Plot Title:"));
	lbl_slidX = new wxStaticText(this, wxID_ANY, wxT("Title X:"));
	lbl_slidY = new wxStaticText(this, wxID_ANY, wxT("Title Y:"));
	
	txt_PlotName = new wxTextCtrl(this, TXT_PLOTNAME);
	txt_XAxisName = new wxTextCtrl(this, TXT_XAXIS);
	txt_YAxisName = new wxTextCtrl(this, TXT_YAXIS);

	Chk_labels = new wxCheckBox(this, CHK_LABELS, wxT("Labels"), wxDefaultPosition, wxSize(67,25));	//set to make even with rest of side bar
	Chk_legend = new wxCheckBox(this, CHK_LEGEND, wxT("Legend"));
	Slider_titleX = new wxSlider(this, SCRL_TITLE, 5, 0, 100, wxDefaultPosition, wxSize(100, 25), wxSL_HORIZONTAL); 
	Slider_titleY = new wxSlider(this, SCRL_TITLE, 5, 0, 100, wxDefaultPosition, wxSize(100, 25), wxSL_HORIZONTAL);

	szr_plotLabels->Add(lbl_PlotName, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(txt_PlotName, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(lbl_slidX, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(Slider_titleX, 0, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(lbl_slidY, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(Slider_titleY, 0, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(lbl_XName, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(txt_XAxisName, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(lbl_YName, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(txt_YAxisName, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 1);
	szr_plotLabels->Add(Chk_labels, 0, wxALL|wxALIGN_CENTER, 1);
	szr_plotLabels->Add(Chk_legend, 0, wxALL|wxALIGN_CENTER, 1);
	
	PenControl->Add(szr_plotLabels);

	LVertSizer->Add(PenControl, 0, wxALL, 0);



	wxArrayString PLogX;
	wxArrayString PLogY;
	PLogX.Add(wxT("Linear (X)"));
	PLogX.Add(wxT("Log Positive (X)"));
	PLogX.Add(wxT("Log Negative (X)"));
	PLogX.Add(wxT("Semi-Log Positive (X)"));
	PLogX.Add(wxT("Semi-Log Negative (X)"));
	PLogX.Add(wxT("Semi-Log (X)"));

	PLogY.Add(wxT("Linear (Y)"));
	PLogY.Add(wxT("Log Positive (Y)"));
	PLogY.Add(wxT("Log Negative (Y)"));
	PLogY.Add(wxT("Semi-Log Positive (Y)"));
	PLogY.Add(wxT("Semi-Log Negative (Y)"));
	PLogY.Add(wxT("Semi-Log (Y)"));



	xLabel = new wxStaticText(this, wxID_ANY, wxT("X(min/max)"));
	yLabel = new wxStaticText(this, wxID_ANY, wxT("Y(min/max)"));
	xpnLogLabel = new wxStaticText(this, wxID_ANY, wxT("Linear -> Log"));
	ypnLogLabel = new wxStaticText(this, wxID_ANY, wxT("Linear -> Log"));



	xMin = new wxTextCtrl(this, UPDATE_PLOT_BOUNDS, wxEmptyString, wxDefaultPosition, wxSize(55, 20));
	xMax = new wxTextCtrl(this, UPDATE_PLOT_BOUNDS, wxEmptyString, wxDefaultPosition, wxSize(55, 20));
	yMin = new wxTextCtrl(this, UPDATE_PLOT_BOUNDS, wxEmptyString, wxDefaultPosition, wxSize(55, 20));
	yMax = new wxTextCtrl(this, UPDATE_PLOT_BOUNDS, wxEmptyString, wxDefaultPosition, wxSize(55, 20));
	PlotLogX = new wxComboBox(this, wxID_ANY, wxT("Linear (X)"), wxDefaultPosition, wxDefaultSize, PLogX, wxCB_DROPDOWN | wxCB_READONLY);
	nLogX = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(40, 20));
	pLogX = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(40, 20));
	PlotLogY = new wxComboBox(this, wxID_ANY, wxT("Linear (Y)"), wxDefaultPosition, wxDefaultSize, PLogY, wxCB_DROPDOWN | wxCB_READONLY);
	nLogY = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(40, 20));
	pLogY = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(40, 20));

	PlotLogX->Hide();	//Not supported yet!
	PlotLogY->Hide();	//so hide this!

	PlControlFlexSizer = new wxFlexGridSizer(2, 3, 2, 2);
	PlControlFlexSizer->Add(xLabel);
	PlControlFlexSizer->Add(xMin);
	PlControlFlexSizer->Add(xMax);
	PlControlFlexSizer->Add(yLabel);
	PlControlFlexSizer->Add(yMin);
	PlControlFlexSizer->Add(yMax);
	PlotControlSizer->Add(PlControlFlexSizer, 0, wxALL, 2);
	PlotControlSizer->Add(PlotLogX, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 2);

	PlControlSizerXLog = new wxBoxSizer(wxHORIZONTAL);
	PlControlSizerXLog->Add(xpnLogLabel, 0, wxALL, 1);
	PlControlSizerXLog->Add(nLogX, 0, wxALL, 1);
	PlControlSizerXLog->Add(pLogX, 0, wxALL, 1);

	PlotControlSizer->Add(PlControlSizerXLog, 0, wxALL, 2);
	PlotControlSizer->Add(PlotLogY, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 2);

	PlControlSizerYLog = new wxBoxSizer(wxHORIZONTAL);
	PlControlSizerYLog->Add(ypnLogLabel, 0, wxALL, 1);
	PlControlSizerYLog->Add(nLogY, 0, wxALL, 1);
	PlControlSizerYLog->Add(pLogY, 0, wxALL, 1);
	PlotControlSizer->Add(PlControlSizerYLog, 0, wxALL, 2);


	LVertSizer->Add(PlotControlSizer, 0, wxALL | wxEXPAND, 0);

	resSizer->Add(LVertSizer, 0, wxALL, 1);
	RVertSizer->Add(topSizer, 0, wxALL | wxEXPAND | wxALIGN_TOP, 1);
	RVertSizer->Add(FileListBox, 1, wxALL | wxEXPAND | wxALIGN_TOP, 1);
	RawOutput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxTE_DONTWRAP);
	wxFont Monofont;
	Monofont.SetFamily(wxFONTFAMILY_TELETYPE);
	RawOutput->SetOwnFont(Monofont);
	RVertSizer->Add(RawOutput, 3, wxALL | wxEXPAND | wxALIGN_TOP, 5);

	MergeManipSizer = new wxStaticBoxSizer(wxVERTICAL, this, wxT("Create custom plot - Select data, preview"));

	wxArrayString cstm_down;
	cstm_down.Add(wxT("Select"));

	cstmBtn_AddActY = new wxButton(this, CSTM_ADD_ACTIVE_BTN, wxT("Add Active Data"), wxDefaultPosition, wxSize(110, 25));
	cstmBtn_AddVisible = new wxButton(this, CSTM_ADD_VISIBLE_BTN, wxT("Add Visible Data"), wxDefaultPosition, wxSize(110, 25));
	cstmBtn_SavePlot = new wxButton(this, CSTM_SAVE_BTN, wxT("Save Plot"), wxDefaultPosition, wxSize(110, 25));
	cstm_Clear = new wxButton(this, CSTM_CLEAR, wxT("Reset Plot"), wxDefaultPosition, wxSize(110, 25));
	cstm_Select = new wxComboBox(this, CSTM_SELECTDDDOWN, wxT("Select"), wxDefaultPosition, wxSize(110,25), cstm_down, wxCB_DROPDOWN | wxCB_READONLY);
	cstmBtn_Remove = new wxButton(this, CSTM_REMOVE, wxT("Remove Data"), wxDefaultPosition, wxSize(110, 25));
	CustomBtnSzr = new wxGridSizer(3, 2, 1, 1);

	CustomBtnSzr->Add(cstmBtn_AddActY, 0, wxALL | wxALIGN_CENTER, 1);
	CustomBtnSzr->Add(cstmBtn_AddVisible, 0, wxALL | wxALIGN_CENTER, 1);
	CustomBtnSzr->Add(cstm_Select, 0, wxALL | wxALIGN_CENTER, 1);
	CustomBtnSzr->Add(cstmBtn_Remove, 0, wxALL | wxALIGN_CENTER, 1);
	cstmBtn_Remove->Disable();
	CustomBtnSzr->Add(cstmBtn_SavePlot, 0, wxALL | wxALIGN_CENTER, 1);
	CustomBtnSzr->Add(cstm_Clear, 0, wxALL | wxALIGN_CENTER, 1);
	MergeManipSizer->Add(CustomBtnSzr, 0, wxALL | wxALIGN_CENTER, 1);
	
	PlotCustomizeSizer->Add(MergeManipSizer, 1, wxALL | wxEXPAND | wxALIGN_LEFT, 1);
	PlotCustomizeSizer->Hide(MergeManipSizer,true);	//disable all these things from showing to begin with

	RVertSizer->Add(PlotCustomizeSizer, 6, wxALL | wxEXPAND | wxALIGN_TOP, 5);
	RVertSizer->Hide(PlotCustomizeSizer);	//put it in, but hide it
	resSizer->Add(RVertSizer, 1, wxALL | wxEXPAND, 1);	//first one says it is horizontally stretchable, EXPAND says it can expand vertically
	RawOutput->Hide();


	//now disable a ton of stuff that shouldn't be accessible
	ActivePlotData->Disable();
	InPlotData->Disable();
	setX->Disable();
	AddToActive->Disable();
	RemoveFromActive->Disable();
	Btn_Normalize->Disable();
	Btn_ToggleZeros->Disable();
	OffsetActY->Disable();
	ScaleActYText->Disable();

	Btn_ClearFilteredFiles->Disable();
	rPen->Disable();
	gPen->Disable();
	bPen->Disable();
	widthPen->Disable();
	stylePen->Disable();
	legendUpdate->Disable();
	PlotLogX->Disable();
	PlotLogY->Disable();
	xMin->Disable();
	xMax->Disable();
	yMin->Disable();
	yMax->Disable();
	nLogX->Disable();
	pLogX->Disable();
	nLogY->Disable();
	pLogY->Disable();
	savePlotButton->Disable();
	RawPlotButton->Disable();
	Btn_PlotCustom->Disable();
	FileSelectSlider->Disable();
	nLogX->Hide();
	nLogY->Hide();
	pLogX->Hide();
	pLogY->Hide();
	xpnLogLabel->Hide();
	ypnLogLabel->Hide();
	Btn_Normalize->Disable();
	Btn_ToggleZeros->Disable();
	OffsetActY->Disable();
	ScaleActYText->Disable();

	PenControl->Hide(szr_plotLabels);

	SetSizerAndFit(resSizer);

}



void resultsPanel::NormalizeActY(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;

	if (unsigned int(CurPlotDisplay) >= Plots.size())//if less than zero, it will certainly be larger
		return;	//can't do anything if a plot isn't selected

	if (Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN || Plots[CurPlotDisplay].plotType == PDATA_RAWONLY)
		return;	//doesn't have a plot that this can be done on

	PlotData* pl = &Plots[CurPlotDisplay];
	int whichCol = pl->GUISelection;

	if (unsigned int(whichCol) >= pl->LData.size())	//if less than zero, it will certainly be larger
		return;

	if (pl->LData[whichCol].HasData() == false)	//no data to manipulate
		return;
	double max = pl->LData[whichCol].GetData(0);
	double min = max;
	for (unsigned int i = 1; i < pl->LData[whichCol].size(); ++i)
	{
		if (min > pl->LData[whichCol].GetData(i))
			min = pl->LData[whichCol].GetData(i);
		if (max < pl->LData[whichCol].GetData(i))
			max = pl->LData[whichCol].GetData(i);
	}

	double dif = max - min;
	double newScale = (dif>0.0) ? 1.0/dif : 1.0;
	if (fabs(newScale) < 1.0e-4 || fabs(newScale) > 1.0e4)
		ScaleActYText->ChangeValue(wxString::Format(wxT("%.6E"), newScale));
	else
		ScaleActYText->ChangeValue(wxString::Format(wxT("%f"), newScale));
	
	if (fabs(min) < 1.0e-4 || fabs(min) > 1.0e4)
		OffsetActY->ChangeValue(wxString::Format(wxT("%.6E"), -min));
	else
		OffsetActY->ChangeValue(wxString::Format(wxT("%f"), -min));

	pl->LData[whichCol].SetOffset(-min);
	pl->LData[whichCol].SetScale(newScale);
	
	pl->UpdatePlot(this, true);

	return;
}

void resultsPanel::ListBoxSelect(wxCommandEvent& event)	//only on double click, otherwise it might be a bit obnoxious
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	int pos = event.GetSelection();
	if (FileSelectSlider->GetMax() >= pos)
		FileSelectSlider->SetValue(pos);	//hope this doesn't cause events, or we'll be setting the plot 2x (though the 2nd time should just be like already done, yo)
	SetPlot(pos, true);
	event.Skip();
}

void resultsPanel::SliderChangeUpdate(wxScrollEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	int pos = event.GetPosition();
	if (FileListBox->GetCount() > unsigned int(pos))
		FileListBox->SetSelection(pos);	//does not cause any events!
	SetPlot(pos, true);
	event.Skip();
}

void resultsPanel::SliderChangeNoUpdate(wxScrollEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	int pos = event.GetPosition();
	if (FileListBox->GetCount() > unsigned int(pos))
		FileListBox->SetSelection(pos);	//does not cause any events!
	SetPlot(pos, false);
	event.Skip();
}

int resultsPanel::SetPlot(unsigned int selection, bool updateRestGUI)
{
	return(SwapPlotDisplayIndex(selection, updateRestGUI));
}

void resultsPanel::SizeRight()
{
	SetSizer(resSizer);
}

void resultsPanel::LoadOutputFiles(wxCommandEvent& event)
{
	//SelectGraphDialog diag(this);
	wxFileDialog diag(this, wxT("Open"), wxEmptyString, wxEmptyString,
		wxT("Simulation files (*.flist)|*.flist|Band Diagrams (*.bd)|*.bd|Current-Voltage (*.jv)|*.jv|Optical Outputs (*.qe)|*.qe|Contact Inputs (*.envin)|*.envin|Contact Output (*.envout)|*.envout|SAAPD Custom Plots (*.plot)|*.plot|All Files (*.*)|*.*"),
		wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (diag.ShowModal() == wxID_OK)
	{
		int ret = LoadData(diag.GetPath(),wxEmptyString, true);
		if (ret != FFILES_SET_BAD)
			FilterFiles(ret, diag.GetPath());	//load it up all properly
	}


	event.Skip();
}

int resultsPanel::UpdateDisplay(unsigned int newPlot, bool forceReloadData)
{
	if (newPlot >= Plots.size())
		return(0);
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;


	PlotData* pl = &(Plots[newPlot]);


	CurPlotDisplay = newPlot;


	Layout();
	return(1);
}
int resultsPanel::LoadQEPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}

	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}

	wxString line = oFile->GetFirstLine();
	wxString utility;
	wxString baseLegend;
	if (!line.StartsWith(wxT("Optical Data: "), &utility)) return(0);
	//utility will now be the configuration file name. Confirm it matches
	line = oFile->GetNextLine();	//Light Entered or something like that
	baseLegend = line + wxT("-");
	line = oFile->GetNextLine(); //blank line
	line = oFile->GetNextLine(); // totals
	line = oFile->GetNextLine(); //blank line


	pl->filename = fname;
	pl->plotType = PDATA_OPTICS;


	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);

	//Setup labels
	line = oFile->GetNextLine();	//the header line
	line.Trim(false);	//get rid of spaces in front
	unsigned int numY = 0;
	if (pl->plot == NULL)	//make sure the default strings are active
	{
		pl->outs.energy = pl->outs.pflux = true;
	}
	while (!line.IsEmpty())
	{
		unsigned int index = pl->LData.size();
		pl->LData.push_back(tmp);
		pl->LData.back().SetHeader(line.BeforeFirst(' ', &utility));
		pl->LData.back().SetLegend(baseLegend+pl->LData.back().GetHeader());

		line = utility.Trim(false);
		if (CheckStringActiveQE(pl->LData.back().GetHeader(), pl))
		{
			if (pl->LData.back().GetHeader().IsSameAs(wxT("Energy(eV)"), false))
			{

				pl->LData.back().SetAsActiveX();
				pl->ActiveX = index; //this should always be set after point
			}
			else
			{
				pl->LData.back().SetAsActiveY();
				AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
				pl->ActiveLData.push_back(index);
				numY++;
			}

			pl->LData.back().GetData().reserve(oFile->GetLineCount() - 6);
		}


	}

	//now read the data
	//read that data
	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		unsigned int i = 0, j = 0;
		line.Trim(false);
		while (!line.IsEmpty() && j < pl->ActiveLData.size())
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (pl->ActiveLData[j] == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
				j++;	//move on to the next active Y data (it's in order)
			}
			else if (pl->ActiveX == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
			}
			i++;	//move on to the next LData
			line = remainder.Trim(false);
		}
	}

	//delete file ptr
	oFile->Close();
	delete oFile;

	pl->UpdatePlot(this, true);

	//pl->plot->LockAspect(false);



	return(1);
}

int resultsPanel::LoadQEPlot(PlotData* pl, wxString header)
{
	if (pl == NULL)	//don't know what data we're loading into!
		return(0);
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;

	int which = -1;
	for (unsigned int i = 0; i<pl->LData.size(); ++i)
	{
		if (header == pl->LData[i].GetHeader())
		{
			which = i;
			break;
		}
	}
	if (which == -1)
		return(0);

	if (pl->LData[which].HasData())	//it already has the data loaded
		return(1);

	wxTextFile* oFile = new wxTextFile(pl->filename);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	pl->plotType = PDATA_OPTICS;

	wxString line = oFile->GetFirstLine();	//load Data From
	wxString utility;
	if (!line.StartsWith(wxT("Optical Data: "), &utility)) return(0);
	//utility will now be the configuration file name. Confirm it matches
	line = oFile->GetNextLine();	//Light Incident (or something like that)
	pl->LData[which].SetLegend(line + wxT("-") + header);
	line = oFile->GetNextLine();	//blank line
	line = oFile->GetNextLine();	//Totals
	line = oFile->GetNextLine();	//blank line
	line = oFile->GetNextLine();	//The headers

	pl->LData[which].GetData().reserve(oFile->GetLineCount() - 6);
	//now read the data

	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{

		line.Trim(false);
		for (int i = 0; i <= which; i++)
		{
			utility = line.BeforeFirst(' ', &remainder);
			line = remainder.Trim(false);
		}

		if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
		pl->LData[which].AddData(val);

	}

	//delete file ptr
	oFile->Close();
	delete oFile;
	AutoRotateColor(pl->LData[which].GetPen(), pl->ActiveLData.size());
	pl->LData[which].SetAsActiveY();


	return(1);
}

int resultsPanel::LoadBDPlot(PlotData* pl, wxString header)
{
	if (pl == NULL)	//don't know what data we're loading into!
		return(0);
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	int which = -1;
	for (unsigned int i = 0; i<pl->LData.size(); ++i)
	{
		if (header == pl->LData[i].GetHeader())
		{
			which = i;
			break;
		}
	}
	if (which == -1)
		return(0);

	if (pl->LData[which].HasData())	//it already has the data loaded
		return(1);

	wxTextFile* oFile = new wxTextFile(pl->filename);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	pl->plotType = PDATA_BD;

	wxString line = oFile->GetFirstLine();	//load Data From
	wxString utility;
	int headerCount=2;	//for Data From: and the column labels
	if (!line.StartsWith(wxT("Data from: "), &utility))
	{
		oFile->Close();
		delete oFile;
		return(0);
	}
	//utility will now be the configuration file name. Confirm it matches

	do
	{
		line = oFile->GetNextLine();
		headerCount++;
	}while(!line.StartsWith(wxT("Iteration: "), &utility) && !oFile->Eof());
	line = oFile->GetNextLine();	//load headers

	pl->LData[which].GetData().reserve(oFile->GetLineCount() - headerCount);
	//now read the data

	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{

		line.Trim(false);
		for (int i = 0; i <= which; i++)
		{
			utility = line.BeforeFirst(' ', &remainder);
			line = remainder.Trim(false);
		}

		if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
		pl->LData[which].AddData(val);

	}

	//delete file ptr
	oFile->Close();
	delete oFile;
	AutoRotateColor(pl->LData[which].GetPen(), pl->ActiveLData.size());
	pl->LData[which].SetAsActiveY();// SetAsActiveY();


	return(1);
}


int resultsPanel::LoadMESHPlot(PlotData* pl, wxString header)
{
	if (pl == NULL)	//don't know what data we're loading into!
		return(0);
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	int which = -1;
	for (unsigned int i = 0; i<pl->LData.size(); ++i)
	{
		if (header == pl->LData[i].GetHeader())
		{
			which = i;
			break;
		}
	}
	if (which == -1)
		return(0);

	if (pl->LData[which].HasData())	//it already has the data loaded
		return(1);

	wxTextFile* oFile = new wxTextFile(pl->filename);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	pl->plotType = PDATA_BD;

	wxString line = oFile->GetFirstLine();	//load Data From
	wxString utility;
	if (!line.StartsWith(wxT("Test data for model: "), &utility)) return(0);
	
	
	pl->LData[which].GetData().reserve(oFile->GetLineCount() - 2);
	//now read the data

	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{

		line.Trim(false);
		for (int i = 0; i <= which; i++)
		{
			utility = line.BeforeFirst(' ', &remainder);
			line = remainder.Trim(false);
		}

		if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
		pl->LData[which].GetData().push_back(val);

	}

	//delete file ptr
	oFile->Close();
	delete oFile;
	AutoRotateColor(pl->LData[which].GetPen(), pl->ActiveLData.size());
	pl->LData[which].SetAsActiveY();


	return(1);
}

int resultsPanel::LoadGenericPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}

	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}
	pl->filename = fname;
	pl->plotType = PDATA_CUSTOM;

	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);	
	tmp.SetAsActiveY();	//default active y plotting

	//this is just going to assume the first load is the X variable
	//remaining variables will all be Y and they will all be active
	wxString utility;
	wxString loadBuffer;

	int headerLine = -1;	//this is the 'first' header line
	int legendLine = -1;	//the 'second' header line
	int dataStartLine = -10;	//first line with data
	wxString line = oFile->GetFirstLine();
	char delimiter = ' ';
	if(fname.EndsWith(wxT(".csv")))
		delimiter = ',';
	else
	{
		//analyze file and try to find common delimiters
		if(line.Contains(wxT(",")))
			delimiter = ',';
		else if(line.Contains(wxT(";")))
			delimiter = ';';
		else if(line.Contains(wxT(":")))
			delimiter = ':';
		else if(line.Contains(wxT("~")))
			delimiter = '~';
		else
			delimiter = ' ';
	}

	bool AllNumbers = false;
	int svSz=-1;
	//try to find a row that contains only numbers - the data

	int LinesPastFirst=0;
	int expectedColumns=-1;
	for (line = oFile->GetFirstLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		int sz = 0;
		int numSz = 0;
		double tmp;

		line.Trim(false);	//get rid of white space on left

		while (!line.IsEmpty())
		{
			
			loadBuffer = line.BeforeFirst(delimiter, &utility);
			if(loadBuffer.ToDouble(&tmp))
				numSz++;
			sz++;

			line = utility.Trim(false);
		}
		if(sz == numSz)
		{
			if(LinesPastFirst-1 != dataStartLine)
			{
				dataStartLine = LinesPastFirst;
				svSz = sz;
			}
			else if(svSz!=sz)//the previous line had an equal number of numbers, but a different amount of numbers
			{
				dataStartLine = LinesPastFirst;
				svSz = sz;
			}
			else //the previous line is the start spot, the number of sizes are the same
			{
				AllNumbers = true;
				break;
			}
		}
		LinesPastFirst++;
	}	//end for loop going through file looking for the first consistent row consisting of only numbers
	
	if(!AllNumbers) //it never found a good set of data lines in a row
	{
		oFile->Close();
		pl->plotType = PDATA_UNKNOWN;
		return(0);
	}
	//now we know which line the data starts on
	//time to figure out where the headers are
	LinesPastFirst = 0;
	for(LinesPastFirst = 0; LinesPastFirst < dataStartLine; ++LinesPastFirst)
	{
		if(LinesPastFirst==0)
			line = oFile->GetFirstLine();
		else
			line = oFile->GetNextLine();

		int sz=0;
		line.Trim(false);	//get rid of white space on left
		while (!line.IsEmpty())
		{			
			loadBuffer = line.BeforeFirst(delimiter, &utility);
			line = utility.Trim(false);
			sz++;
		}
		if(sz==svSz)
		{
			if(headerLine == -1)	//the first one that matches is the header line
				headerLine = LinesPastFirst;
			else
				legendLine = LinesPastFirst;	//the last one matching is the legend line
		}
	}
	pl->LData.resize(svSz, tmp);	//allow just setting equal, and preload it with some data
	LinesPastFirst = 0;
	if(headerLine == -1)
	{
		for(int i=0; i<svSz; ++i)
		{
			line = wxEmptyString;
			line << i;	//just get a counting up number in here
			pl->LData[i].SetHeader(line);
			pl->LData[i].SetLegend(line);
		}
	}
	pl->LData[0].SetAsActiveX();// = PL_TYPE_X | PL_TYPE_X_ACTIVE;	//make the first one X by default

	unsigned int dataLineCount = oFile->GetLineCount() - dataStartLine;	//this is probably one over...
	for(int i=0; i<svSz; ++i)
	{
		pl->LData[i].GetData().reserve(dataLineCount);
		AutoRotateColor(pl->LData[i].GetPen(), i-1);	//first one doesn't really matter
	}

	for (line = oFile->GetFirstLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		line.Trim(false);	//get rid of any white space starting it
		if(headerLine == LinesPastFirst)
		{
			for(int i=0;i<svSz; ++i)
			{
				pl->LData[i].SetHeader(line.BeforeFirst(delimiter, &utility));
				if(legendLine==-1)
					pl->LData[i].SetLegend(pl->LData[i].GetHeader());
				line = utility.Trim(false);
			}
		}
		else if(legendLine == LinesPastFirst)
		{
			for(int i=0;i<svSz; ++i)
			{
				pl->LData[i].SetLegend(line.BeforeFirst(delimiter, &utility));
				line = utility.Trim(false);
			}
		}
		else if(LinesPastFirst >= dataStartLine)
		{
			int ct=0;
			double val;
			while(!line.IsEmpty() && ct<svSz)
			{
				loadBuffer = line.BeforeFirst(delimiter, &utility);
				if(!loadBuffer.ToDouble(&val))	 wxLogError(wxT("Non-number value in output file!"));
				pl->LData[ct].AddData(val);
				ct++;
				line = utility.Trim(false);
			}
			for(;ct<svSz;ct++)
				pl->LData[ct].AddData(0.0);	//make sure it will have the same number of values as the x vector
		}
		LinesPastFirst++;
	}


	oFile->Close();
	delete oFile;
	oFile = NULL;

	pl->UpdatePlot(this, true);

	return(1);

}

int resultsPanel::LoadCPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}

	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}
	pl->filename = fname;
	pl->plotType = PDATA_CUSTOM;

	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);
	

	wxString utility;
	wxString loadBuffer;
	//this loads ALL data in!
	int numVariables=0;	//used for color rotation of plot
	for (wxString line = oFile->GetFirstLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		pl->LData.push_back(tmp);
		
		
		line.Trim(false);	//get rid of spaces in front
		loadBuffer = line.BeforeFirst(' ', &utility);
		if (loadBuffer.IsSameAs(wxT("X")))
			pl->LData.back().SetAsActiveX();// = PL_TYPE_X | PL_TYPE_X_ACTIVE;
		else
		{
			pl->LData.back().SetAsActiveY();// = PL_TYPE_Y | PL_TYPE_Y_ACTIVE;
			AutoRotateColor(pl->LData.back().GetPen(), numVariables);
			numVariables++;
		}


		line = utility.Trim(false);	//get to the next one
		loadBuffer = line.BeforeFirst(' ', &utility);
		if (loadBuffer.IsSameAs(wxT("0")))
			pl->LData.back().ShowZeros();// = false;
		else
			pl->LData.back().IgnoreZeros();// = true;

		pl->LData.back().ConnectData();
		
		line = utility.Trim(false);	//get to the next one
		loadBuffer = line.BeforeFirst(' ', &utility);
		if (!loadBuffer.ToDouble(&pl->LData.back().GetRefOffset()))	 wxLogError(wxT("Non-number value in output file!"));

		line = utility.Trim(false);	//get to the next one
		loadBuffer = line.BeforeFirst(' ', &utility);
		if (!loadBuffer.ToDouble(&pl->LData.back().GetRefScale()))	 wxLogError(wxT("Non-number value in output file!"));
		
		unsigned long int num;
		line = utility.Trim(false);	//get to the next one
		loadBuffer = line.BeforeFirst(' ', &utility);
		if (!loadBuffer.ToULong(&num))	 wxLogError(wxT("Non-number value in output file!"));
		//offset, scale, size, data, header/legend

		double val = 0.0;
		pl->LData.back().GetData().reserve(num);
		for (unsigned long int i = 0; i < num; ++i)
		{
			line = utility.Trim(false);	//get to the next one
			loadBuffer = line.BeforeFirst(' ', &utility);
			if (!loadBuffer.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
			pl->LData.back().AddData(val);
		}

		pl->LData.back().SetHeaderLegend(utility.Trim(false));
	}

	oFile->Close();
	pl->UpdatePlot(this, true);
	return(1);
}

int resultsPanel::LoadBDPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}
	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}

	wxString line = oFile->GetFirstLine();
	wxString utility;

	int headerCount=2;	//for Data From: and the column labels
	if (!line.StartsWith(wxT("Data from: "), &utility))
	{
		oFile->Close();
		delete oFile;
		return(0);
	}
	//utility will now be the configuration file name. Confirm it matches

	do
	{
		line = oFile->GetNextLine();
		headerCount++;
	}while(!line.StartsWith(wxT("Iteration: "), &utility) && !oFile->Eof());
	line = oFile->GetNextLine();	//load headers

	//	if(!line.StartsWith(wxT("Data binning: "), &utility)) return;
	//utility = data binning
	//	line = oFile->GetNextLine();
	//now line has the labels loaded


	pl->filename = fname;
	pl->plotType = PDATA_BD;
	

	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);
	

	//Setup labels
	line.Trim(false);	//get rid of spaces in front
	unsigned int numY = 0;
	if (pl->plot == NULL)	//make sure the default strings are active
	{
		pl->outs.Ec = pl->outs.Efn = pl->outs.Efp = pl->outs.Ev = pl->outs.x = true;
	}
	while (!line.IsEmpty())
	{
		pl->LData.push_back(tmp);
		pl->LData.back().SetHeaderLegend(line.BeforeFirst(' ', &utility));

		line = utility.Trim(false);
		if (CheckStringActiveBD(pl->LData.back().GetHeader(), pl))
		{
			if (pl->LData.back().GetHeader().IsSameAs(wxT("X"), false))
			{
				pl->LData.back().SetAsActiveX();// = PL_TYPE_X | PL_TYPE_X_ACTIVE;	//active x is x by default
				pl->ActiveX = pl->LData.size() - 1; //this should always be set after point
			}
			else
			{
				pl->LData.back().SetAsActiveY();// = PL_TYPE_Y | PL_TYPE_Y_ACTIVE;	//active x is x by default
				AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
				pl->ActiveLData.push_back(pl->LData.size() - 1);
				numY++;
			}

			pl->LData.back().GetData().reserve(oFile->GetLineCount() - 3);
		}


	}

	//now read the data
	//read that data
	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		unsigned int i = 0, j = 0;
		line.Trim(false);
		while (!line.IsEmpty() && (j < pl->ActiveLData.size() || (int)i <= pl->ActiveX))
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (pl->ActiveX == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
			}
			else if (j < pl->ActiveLData.size() && pl->ActiveLData[j] == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
				j++;	//move on to the next active Y data (it's in order)
			}
			
			i++;	//move on to the next LData
			line = remainder.Trim(false);
		}
	}

	//delete file ptr
	oFile->Close();
	delete oFile;

	pl->UpdatePlot(this, true);

	//pl->plot->LockAspect(false);



	return(1);
}
int resultsPanel::LoadRawPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			pl->rawData = wxEmptyString;
		}
	}
	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file
	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}


	pl->filename = fname;
	pl->plotType = PDATA_RAWONLY;

	pl->ActiveLData.clear();
	pl->plot = NULL;
	pl->xaxis = NULL;
	pl->yaxis = NULL;
	pl->GUISelection = -1;
	pl->LData.clear();
	pl->ActiveLData.clear();
	pl->ActiveX = -1;
	pl->outs.Reset();	//make all the outs false


	pl->rawData = oFile->GetFirstLine()+"\n";
	for (pl->rawData += (oFile->GetNextLine() + "\n"); !oFile->Eof(); pl->rawData += (oFile->GetNextLine() + "\n"))
		;
	oFile->Close();

	return(1);
}

int resultsPanel::LoadMESHPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for(unsigned int i=0; i<pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}
	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}

	wxString line = oFile->GetFirstLine();
	wxString utility;
	if (!line.StartsWith(wxT("Test data for model:"), &utility)) return(0);
	//utility will now be the configuration file name. Confirm it matches
	line = oFile->GetNextLine();

	pl->filename = fname;
	pl->plotType = PDATA_MESH;


	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);
	

	//Setup labels
	line.Trim(false);	//get rid of spaces in front
	unsigned int numY = 0;
	if (pl->plot == NULL)	//make sure the default strings are active
	{
		pl->outs.point = pl->outs.posX = true;
	}
	while (!line.IsEmpty())
	{
		pl->LData.push_back(tmp);
		pl->LData.back().SetHeaderLegend(line.BeforeFirst(' ', &utility));

		line = utility.Trim(false);
		if (CheckStringActiveMESH(pl->LData.back().GetHeader(), pl))
		{
			if (pl->LData.back().GetHeader().IsSameAs(wxT("Point"), false))
			{
				pl->LData.back().SetAsActiveX();	//active x is x by default
				pl->ActiveX = pl->LData.size() - 1; //this should always be set after point
			}
			else
			{
				pl->LData.back().SetAsActiveY();	//active x is x by default
				AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
				pl->ActiveLData.push_back(pl->LData.size() - 1);
				numY++;
			}

			pl->LData.back().GetData().reserve(oFile->GetLineCount() - 2);
		}


	}

	//now read the data
	//read that data
	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		unsigned int i = 0, j = 0;
		line.Trim(false);
		while (!line.IsEmpty() && j < pl->ActiveLData.size())
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (pl->ActiveLData[j] == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
				j++;	//move on to the next active Y data (it's in order)
			}
			else if (pl->ActiveX == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
			}
			i++;	//move on to the next LData
			line = remainder.Trim(false);
		}
	}

	//delete file ptr
	oFile->Close();
	delete oFile;

	pl->UpdatePlot(this, true);

	//pl->plot->LockAspect(false);



	return(1);
}

int resultsPanel::LoadJVPlot(PlotData* pl, wxString header)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl == NULL)	//don't know what data we're loading into!
		return(0);

	int which = pl->FindHeader(header);
	if (which == -1)
		return(0);

	if (pl->LData[which].HasData())	//already has the data loaded
		return(1);

	wxTextFile* oFile = new wxTextFile(pl->filename);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	pl->plotType = PDATA_JV;
	wxString line = oFile->GetFirstLine();	//group name
	wxString headerline;
	wxString utility;
	line = oFile->GetNextLine();	//contact 1 name
	line = oFile->GetNextLine();	//contact 2 name
	line = oFile->GetNextLine();	//voltage
	line = oFile->GetNextLine();	//accuracy (error bars on voltage)
	line = oFile->GetNextLine();	//empty line
	line = oFile->GetNextLine();	//cdividers
	line = oFile->GetNextLine();	//headers
	line.Trim(false);	//clear white space to the left
	headerline = line;	//store a copy in case it is needed in the future (avg)
	//because of the dividers, need to get an accurate count of which 'column' is the header

	int loadctrx;
	int loadx = 0;
	bool found;
	wxString remainder;

	//determine where the x values will be loaded, if they need to be loaded, and what column this corresponds to

	if (header.Contains(wxT("Jn")))
	{	//need to make sure volt_n is loaded up
		loadx = pl->FindHeader(wxT("Voltage_N"));	//sets to -1 if not found
		if (loadx != -1)	//this is pretty much guaranteed true unless the file gets corrupted
		{
			if (pl->LData[loadx].HasData())	//it already has data loaded, so it does not need loading
				loadx = -2;
		}
		else
		{
			//it failed to find Voltage_N, so put it in
			LineData tmp;
			tmp.SetAsActiveX();
			tmp.SetHeaderLegend(wxT("Voltage_N"));
			pl->LData.push_back(tmp);
			loadx = pl->LData.size() - 1;	//the last entry is the x index
		}
		//at this point, if loadx >= 0 it needs data loaded

		if (loadx >= 0)
		{
			found = false;
			line = headerline;
			loadctrx = 0;
			while (!line.IsEmpty())
			{
				utility = line.BeforeFirst(' ', &remainder);
				if (utility.IsSameAs(wxT("Voltage_N")))
				{
					found = true;
					break;
				}
				line = remainder.Trim(false);
				loadctrx++;
			}
			if (!found)
				loadctrx = -1;	//it never found the column of data
		}
	}
	else if (header.Contains(wxT("Jp")))
	{	//need to make sure volt_n is loaded up
		loadx = pl->FindHeader(wxT("Voltage_P"));	//sets to -1 if not found
		if (loadx != -1)	//this is pretty much guaranteed true unless the file gets corrupted
		{
			if (pl->LData[loadx].HasData())	//it already has data loaded, so it does not need loading
				loadx = -2;
		}
		else
		{
			//it failed to find Voltage_N, so put it in
			LineData tmp;
			tmp.SetAsActiveX();
			tmp.SetHeaderLegend(wxT("Voltage_P"));
			pl->LData.push_back(tmp);
			AutoRotateColor(pl->LData.back().GetPen(), pl->ActiveLData.size());
			loadx = pl->LData.size() - 1;	//the last entry is the x index
		}
		//at this point, if loadx >= 0 it needs data loaded

		if (loadx >= 0)
		{
			found = false;
			line = headerline;
			loadctrx = 0;
			while (!line.IsEmpty())
			{
				utility = line.BeforeFirst(' ', &remainder);
				if (utility.IsSameAs(wxT("Voltage_P")))
				{
					found = true;
					break;
				}
				line = remainder.Trim(false);
				loadctrx++;
			}
			if (!found)
				loadctrx = -1;	//it never found the column of data
		}
	}
	else //it is just the normal voltage
	{	//need to make sure volt_n is loaded up
		loadx = pl->FindHeader(wxT("Voltage"));	//sets to -1 if not found
		if (loadx != -1)	//this is pretty much guaranteed true unless the file gets corrupted
		{
			if (pl->LData[loadx].HasData())	//it already has data loaded, so it does not need loading
				loadx = -2;
		}
		else
		{
			//it failed to find Voltage_N, so put it in
			LineData tmp;
			tmp.SetAsActiveX();
			tmp.SetHeaderLegend(wxT("Voltage"));
			pl->LData.push_back(tmp);
			AutoRotateColor(pl->LData.back().GetPen(), pl->ActiveLData.size());
			loadx = pl->LData.size() - 1;	//the last entry is the x index
		}
		//at this point, if loadx >= 0 it needs data loaded

		if (loadx >= 0)
		{
			found = false;
			line = headerline;
			loadctrx = 0;
			while (!line.IsEmpty())
			{
				utility = line.BeforeFirst(' ', &remainder);
				if (utility.IsSameAs(wxT("Voltage")))
				{
					found = true;
					break;
				}
				line = remainder.Trim(false);
				loadctrx++;
			}
			if (!found)
				loadctrx = -1;	//it never found the column of data
		}
	}

	int loadctry1 = -1;
	int loadctry2 = -1;
	int loady1, loady2, loadyavg;
	//the file is now to the data
	if (header.Contains(wxT("avg")))	//actually needs two bits of data
	{
		loadyavg = which;
		pl->LData[loadyavg].SetAsActiveY();
		bool fnd1 = false;
		bool fnd2 = false;
		wxString strY1, strY2;
		if (header.Contains(wxT("Jn")))
		{
			strY1 = wxT("Jn_ctc_x(1)");
			strY2 = wxT("Jn_ctc_x(2)");
		}
		else if (header.Contains(wxT("Jp")))
		{
			strY1 = wxT("Jp_ctc_x(1)");
			strY2 = wxT("Jp_ctc_x(2)");
		}
		else
		{
			strY1 = wxT("J_ctc_x(1)");
			strY2 = wxT("J_ctc_x(2)");
		}
		loady1 = pl->FindHeader(strY1);
		loady2 = pl->FindHeader(strY2);

		loadctry1 = loadctry2 = 0;
		line = headerline;
		while (!line.IsEmpty())
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (utility.IsSameAs(strY1))
				fnd1 = true;
			if (utility.IsSameAs(strY2))
				fnd2 = true;

			if (!fnd1)
				loadctry1++;
			if (!fnd2)
				loadctry2++;

			if (fnd1 && fnd2)
				break;

			line = remainder.Trim(false);
		}
		if (!fnd1)
			loadctry1 = -1;
		if (!fnd2)
			loadctry2 = -1;
		if (loady1 >= 0)
		{
			if (pl->LData[loady1].HasData())
				loadctry1 = -1;	//data is already loaded, no need to load any more data
			else
				pl->LData[loady1].SetAsY();
		}
		if (loady2 >= 0)
		{
			if (pl->LData[loady2].HasData())
				loadctry2 = -1;	//data is already loaded, no need to load any more data
			else
				pl->LData[loady2].SetAsY();
		}
	}
	else
	{
		line = headerline;	//make sure the header line is all loaded
		bool found = false;
		loadctry1 = 0;
		loady1 = which;
		pl->LData[loady1].SetAsActiveY();
		loady2 = loadyavg = loadctry2 = -1;
		while (!line.IsEmpty())
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (utility.IsSameAs(header))
			{
				found = true;
				break;
			}
			line = remainder.Trim(false);
			loadctry1++;
		}
	}


	line = oFile->GetNextLine();	//cdividers
	double val;
	double defX = 0.0;	//a default x for the voltage if none is defined - basically just a counter
	int lctr = 0;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		int ctr = 0;
		double avg = 0.0;
		while (!line.IsEmpty())
		{
			line.Trim(false);
			utility = line.BeforeFirst(' ', &remainder);	//holds the number of interest
			line = remainder.Trim(false);
			if (ctr == loadctry1)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[loady1].AddData(val);
			}
			if (ctr == loadctry2)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[loady2].AddData(val);
			}
			if (loadx >= 0 /* needs data loaded*/ && ctr == loadctrx)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[loadx].AddData(val);
			}
			ctr++;
		}
		if (loadx >= 0 && loadctrx < 0)	//it doesn't have any useful data
		{
			pl->LData[loadx].AddData(defX);
			defX += 1.0;
		}
		if (loady1 >= 0 && loady2 >= 0 && loadyavg >= 0)
		{
			pl->LData[loadyavg].AddData(0.5 *(pl->LData[loady1].GetData(lctr) + pl->LData[loady2].GetData(lctr)));
		}

		lctr++;

	}
	AutoRotateColor(pl->LData[which].GetPen(), pl->ActiveLData.size());

	return(1);
}

int resultsPanel::LoadJVPlot(wxString fname, bool force, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (pl)
	{
		if (pl->plotType != PDATA_UNKNOWN)
		{
			if(force == false)
				return(1);	//this already has the initial data loaded. It exists as much as it's going to without picking out individual stuff
			for(unsigned int i=0; i<pl->LData.size(); ++i)
				pl->LData[i].ClearData();
			pl->LData.clear();
			pl->ActiveLData.clear();
			pl->ActiveX = pl->GUISelection = -1;
		}
	}
	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	if (pl == NULL) //there was no associated plot, so create the stuff for it as well
	{
		PlotData tmp;
		Plots.push_back(tmp);
		pl = &(Plots.back());
		pl->filename = fname;
		FullFilenames.push_back(fname);
		AbbrFNames.push_back(fname);
		ActiveFileIndices.push_back(AbbrFNames.size() - 1);
		FilterFiles(FFILES_SET_NONE);	//this should update the list box and whatnot
	}

	wxString line = oFile->GetFirstLine();	//group name
	//	pl->plot->SetLabel(line+wxT(" JV"));
	wxString utility;
	//	if(!line.StartsWith(wxT("Data from: "), &utility)) return(0);
	//utility will now be the configuration file name. Confirm it matches
	line = oFile->GetNextLine();	//contact 1 name
	//		if(!line.StartsWith(wxT("Iteration: "), &utility)) return(0);
	line = oFile->GetNextLine();	//contact 2 name
	line = oFile->GetNextLine();	//voltage
	line = oFile->GetNextLine();	//accuracy (error bars on voltage)
	line = oFile->GetNextLine();	//empty line
	line = oFile->GetNextLine();	//cdividers
	line = oFile->GetNextLine();	//headers

	//	if(!line.StartsWith(wxT("Data binning: "), &utility)) return;
	//utility = data binning
	//	line = oFile->GetNextLine();
	//now line has the labels loaded
	pl->plotType = PDATA_JV;

	pl->filename = fname;



	int xActIndex = -1;
	LineData tmp;
	tmp.GetPen().SetWidth(2);


	//Setup labels
	line.Trim(false);	//get rid of spaces in front
	unsigned int numY = 0;
	std::vector<unsigned int> RemoveActive;
	if (pl->plot == NULL)	//make sure the default strings are active
	{
		pl->outs.javg_x = true;
		pl->outs.xaxisZero = true;
		//		pl->outs.volt = true;	//output your typical JV plot
	}
	while (!line.IsEmpty())
	{

		pl->LData.push_back(tmp);
		pl->LData.back().SetHeaderLegend(line.BeforeFirst(' ', &utility));


		line = utility.Trim(false);
		if (pl->LData.back().GetHeader().Contains(wxT("Voltage")))
		{
			pl->LData.back().ClearDataType(); 
			pl->LData.back().SetAsX();	//this |=, want to start  from 0
//			= PL_TYPE_X;	//active x is x by default
			//				pl->ActiveX = pl->LData.size()-1; //this should always be set after point. There might actually be multiple X's
			pl->ActiveLData.push_back(pl->LData.size() - 1);
			RemoveActive.push_back(pl->ActiveLData.back());	//don't let X values be a part of active value (initially), but we do want to pull the associated data
			pl->LData.back().GetData().reserve(oFile->GetLineCount() - 9);
		}
		if (CheckStringActiveJV(pl->LData.back().GetHeader(), pl, true))
		{
			if (CheckStringActiveJV(pl->LData.back().GetHeader(), pl, false)) //is this an EXACT match?
			{
				pl->LData.back().SetAsActiveY();// = PL_TYPE_Y | PL_TYPE_Y_ACTIVE;	//active x is x by default
				AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
				pl->ActiveLData.push_back(pl->LData.size() - 1);
				numY++;
			}
			else //it was only brought in by dependencies
			{
				pl->LData.back().SetAsY();	//active x is x by default
				pl->ActiveLData.push_back(pl->LData.size() - 1);	//it's in the set of data to load... though just ofr averaging purposes
				RemoveActive.push_back(pl->ActiveLData.back());
			}
			pl->LData.back().GetData().reserve(oFile->GetLineCount() - 9);
		}


	}



	line = oFile->GetNextLine();	//division
	//the next line the data starts
	//now read the data
	//read that data
	double val;
	wxString remainder;
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		unsigned int i = 0, j = 0;
		line.Trim(false);
		while (!line.IsEmpty() && j < pl->ActiveLData.size())
		{
			utility = line.BeforeFirst(' ', &remainder);
			if (pl->ActiveLData[j] == i)
			{
				if (!utility.ToDouble(&val)) wxLogError(wxT("Non-number value in output file!"));
				pl->LData[i].AddData(val);
				j++;	//move on to the next active Y data (it's in order)
			}
			i++;	//move on to the next LData
			line = remainder.Trim(false);
		}
	}
	//delete file ptr
	oFile->Close();
	delete oFile;

	//now the three averages must be added into the mix
	pl->LData.push_back(tmp);
	pl->LData.back().SetHeaderLegend(wxT("J_avg_x"));
	
	if (pl->outs.javg_x)
	{
		pl->LData.back().SetAsActiveY();
		unsigned int col1 = -1;
		unsigned int col2 = -1;
		for (unsigned int i = 0; i<pl->LData.size(); ++i)
		{
			if (pl->LData[i].GetHeader().IsSameAs(wxT("J_ctc_x(1)")))
				col1 = i;
			else if (pl->LData[i].GetHeader().IsSameAs(wxT("J_ctc_x(2)")))
				col2 = i;
			if (col1 != -1 && col2 != -1)
				break;	//get out of loop
		}

		if (col1 != -1 && col2 != -1)	//can do averaging
		{
			AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
			pl->ActiveLData.push_back(pl->LData.size() - 1);	//as there is data, add it to the averaging list
			numY++;

			unsigned int smSz = pl->LData[col1].size() < pl->LData[col2].size() ? pl->LData[col1].size() : pl->LData[col2].size();
			//the two sizes should be the same, but being safe
			pl->LData.back().reserve(smSz);
			for (unsigned int i = 0; i<smSz; ++i)
			{
				pl->LData.back().AddData(0.5*(pl->LData[col1].GetData(i) + pl->LData[col2].GetData(i)));
			}

		}

	}

	pl->LData.push_back(tmp);
	pl->LData.back().SetHeaderLegend(wxT("Jn_avg_x"));
	
	if (pl->outs.javg_n_x)
	{
		pl->LData.back().SetAsActiveY();
		unsigned int col1 = -1;
		unsigned int col2 = -1;
		for (unsigned int i = 0; i<pl->LData.size(); ++i)
		{
			if (pl->LData[i].GetHeader().IsSameAs(wxT("Jn_ctc_x(1)")))
				col1 = i;
			else if (pl->LData[i].GetHeader().IsSameAs(wxT("Jn_ctc_x(2)")))
				col2 = i;
			if (col1 != -1 && col2 != -1)
				break;	//get out of loop
		}
		if (col1 != -1 && col2 != -1)	//can't do averaging
		{
			AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
			pl->ActiveLData.push_back(pl->LData.size() - 1);
			numY++;

			unsigned int smSz = pl->LData[col1].size() < pl->LData[col2].size() ? pl->LData[col1].size() : pl->LData[col2].size();
			//the two sizes should be the same, but being safe
			pl->LData.back().reserve(smSz);
			for (unsigned int i = 0; i<smSz; ++i)
			{
				pl->LData.back().AddData(0.5*(pl->LData[col1].GetData(i) + pl->LData[col2].GetData(i)));
			}
		}

	}

	pl->LData.push_back(tmp);
	pl->LData.back().SetHeaderLegend(wxT("Jp_avg_x"));
	

	if (pl->outs.javg_p_x)
	{
		pl->LData.back().SetAsActiveY();

		unsigned int col1 = -1;
		unsigned int col2 = -1;
		for (unsigned int i = 0; i<pl->LData.size(); ++i)
		{
			if (pl->LData[i].GetHeader().IsSameAs(wxT("Jp_ctc_x(1)")))
				col1 = i;
			else if (pl->LData[i].GetHeader().IsSameAs(wxT("Jp_ctc_x(2)")))
				col2 = i;
			if (col1 != -1 && col2 != -1)
				break;	//get out of loop
		}
		if (col1 != -1 && col2 != -1)	//can't do averaging
		{
			AutoRotateColor(pl->LData.back().GetPen(), numY);	//force it to just rotate colors for each pen
			pl->ActiveLData.push_back(pl->LData.size() - 1);
			numY++;
			unsigned int smSz = pl->LData[col1].size() < pl->LData[col2].size() ? pl->LData[col1].size() : pl->LData[col2].size();
			//the two sizes should be the same, but being safe
			pl->LData.back().reserve(smSz);
			for (unsigned int i = 0; i<smSz; ++i)
			{
				pl->LData.back().AddData(0.5*(pl->LData[col1].GetData(i) + pl->LData[col2].GetData(i)));
			}
		}
	}

	pl->LData.push_back(tmp);
	pl->LData.back().SetHeader(wxT("y=0"));
	pl->LData.back().SetLegend(wxT("J=0"));
	pl->LData.back().SetAsActiveY();
	pl->LData.back().GetData().resize(pl->LData[0].size(), 0.0);
	pl->ActiveLData.push_back(pl->LData.size() - 1);
	pl->LData.back().GetPen().SetStyle(wxPENSTYLE_LONG_DASH);
	pl->LData.back().GetPen().SetWidth(1);
	pl->LData.back().GetPen().SetColour(128, 128, 128);


	//now kill the activeLData's that are bad
	for (unsigned int i = 0; i < RemoveActive.size(); ++i)
	{
		for (std::vector<unsigned int>::iterator it = pl->ActiveLData.begin(); it != pl->ActiveLData.end(); ++it)
		{
			if (RemoveActive[i] == *it)
			{
				pl->ActiveLData.erase(it);
				break;	//this loop is invalid, now on to the next one that is bad
			}
		}
	}
	RemoveActive.clear();
	//now for the sake of brevity, kill the "|" entries and shift the ActiveL data appropriately

	//first, shift everything down
	int countRemoved = 0;
	//start from the back so every detection subtracts one from all above it, and no weird logic is needed to keep things in line
	std::vector<unsigned int> rem;
	for (unsigned int i = pl->LData.size() - 1; i<pl->LData.size(); --i)	//loop through each to find the "|"
	{
		if (pl->LData[i].GetHeader().IsSameAs(wxT("|")))
		{
			//now find everything active above it, and decrement by one.
			rem.push_back(i);	//starts from the back
			for (unsigned int j = 0; j<pl->ActiveLData.size(); ++j)
			{
				if (pl->ActiveLData[j] > i)
					pl->ActiveLData[j]--;
			}
		}
	}

	//now actually kill the header rows
	for (unsigned int i = 0; i<rem.size(); ++i)	//loop through each to find the "|"
	{
		//this starts removing from the back, so the indices should stay OK
		std::vector<LineData>::iterator it = pl->LData.begin() + rem[i];
		pl->LData.erase(it);
	}





	pl->UpdatePlot(this, true);

	return(1);
}

int resultsPanel::UpdateDataDropDowns(unsigned int exactIndex)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	ActivePlotData->Clear();
	InPlotData->Clear();
	ActivePlotData->Append(wxT("Active"));
	InPlotData->Append(wxT("Inactive"));

	if (exactIndex >= Plots.size())
	{
		if (unsigned int(CurPlotDisplay) >= Plots.size())
			return(0);
		exactIndex = CurPlotDisplay;
	}

	PlotData* pl = &Plots[exactIndex];

	

	if (pl->plotType == PDATA_BD)
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			if (CheckStringActiveBD(pl->LData[i].GetHeader(), pl))
				ActivePlotData->Append(pl->LData[i].GetHeader());
			else
				InPlotData->Append(pl->LData[i].GetHeader());
		}
	}
	else if (pl->plotType == PDATA_JV)	//check for nonBD header tests
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			if (CheckStringActiveJV(pl->LData[i].GetHeader(), pl))
				ActivePlotData->Append(pl->LData[i].GetHeader());
			else
				InPlotData->Append(pl->LData[i].GetHeader());
		}
	}
	else if (pl->plotType == PDATA_OPTICS)	//check for nonBD header tests
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			if (CheckStringActiveQE(pl->LData[i].GetHeader(), pl))
				ActivePlotData->Append(pl->LData[i].GetHeader());
			else
				InPlotData->Append(pl->LData[i].GetHeader());
		}
	}
	else if (pl->plotType == PDATA_MESH)	//check for nonBD header tests
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			if (CheckStringActiveMESH(pl->LData[i].GetHeader(), pl))
				ActivePlotData->Append(pl->LData[i].GetHeader());
			else
				InPlotData->Append(pl->LData[i].GetHeader());
		}
	}
	else if (pl->plotType == PDATA_CUSTOM)	//check for nonBD header tests
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			if (pl->LData[i].GetDataType() & (PL_TYPE_X_ACTIVE | PL_TYPE_Y_ACTIVE))
				ActivePlotData->Append(pl->LData[i].GetHeader());
			else
				InPlotData->Append(pl->LData[i].GetHeader());
		}
	}
	else
	{
		for (unsigned int i = 0; i < pl->LData.size(); ++i)
		{
			InPlotData->Append(pl->LData[i].GetHeader());
		}
	}


	ActivePlotData->SetSelection(0);
	InPlotData->SetSelection(0);
	rPen->Disable();
	gPen->Disable();
	bPen->Disable();

	stylePen->Disable();
	widthPen->Disable();
	legendUpdate->Disable();
	Btn_Normalize->Disable();
	Btn_ToggleZeros->Disable();
	OffsetActY->Disable();
	ScaleActYText->Disable();
	cstmBtn_AddActY->Disable();

	rPen->Hide();
	gPen->Hide();
	bPen->Hide();
	stylePen->Hide();
	widthPen->Hide();
	legendUpdate->Hide();
	Btn_Normalize->Hide();
	Btn_ToggleZeros->Hide();
	OffsetActY->Hide();
	ScaleActYText->Hide();
	cstmBtn_AddActY->Hide();
	legText->Hide();
	txtRGBA->Hide();
	txtOffset->Hide();
	statTxtScaleY->Hide();


	PenControl->Show(szr_plotLabels);
	PenControl->Layout();

	pl->GUISelection = -1;
	return(1);
}

void resultsPanel::DisableDefaultPlotControls(void)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	FileSelectSlider->SetMax(0);	//allow the slider to have a spot for each file in the list box above
	FileSelectSlider->SetValue(0);	//initialize it to the 0th value
	FileSelectSlider->Disable();

	ActivePlotData->Disable();
	InPlotData->Disable();
	setX->Disable();
	AddToActive->Disable();
	RemoveFromActive->Disable();
	Btn_Normalize->Disable();
	Btn_ToggleZeros->Disable();
	OffsetActY->Disable();
	ScaleActYText->Disable();
	PlotLogX->Disable();
	PlotLogY->Disable();
	xMin->Disable();
	xMax->Disable();
	yMin->Disable();
	yMax->Disable();
	PlotLogX->SetSelection(0);
	PlotLogY->SetSelection(0);
	nLogX->Disable();
	pLogX->Disable();
	nLogY->Disable();
	pLogY->Disable();
	nLogX->Hide();
	pLogX->Hide();
	nLogY->Hide();
	pLogY->Hide();
	xpnLogLabel->Hide();
	ypnLogLabel->Hide();
	savePlotButton->Disable();
	RawPlotButton->Disable();
//	Btn_PlotCustom->Disable();
	Btn_Normalize->Disable();
	Btn_ToggleZeros->Disable();
	OffsetActY->Disable();
	ScaleActYText->Disable();
	return;
}

void resultsPanel::EnableDefaultPlotControls(void)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	FileSelectSlider->Enable();
	ActivePlotData->Enable();
	InPlotData->Enable();
	setX->Enable();
	AddToActive->Enable();
	RemoveFromActive->Enable();
	PlotLogX->Enable();
	PlotLogY->Enable();
	xMin->Enable();
	xMax->Enable();
	yMin->Enable();
	yMax->Enable();
	PlotLogX->SetSelection(0);
	PlotLogY->SetSelection(0);
	nLogX->Disable();
	pLogX->Disable();
	nLogY->Disable();
	pLogY->Disable();
	nLogX->Hide();
	pLogX->Hide();
	nLogY->Hide();
	pLogY->Hide();
	xpnLogLabel->Hide();
	ypnLogLabel->Hide();
	savePlotButton->Enable();
	RawPlotButton->Enable();
	Btn_PlotCustom->Enable();
	return;
}
int resultsPanel::SwapPlotDisplayIndex(unsigned int select, bool updateRestGUI)
{
	if (select < ActiveFileIndices.size())
		return(SwapPlotExactIndex(ActiveFileIndices[select], updateRestGUI));
	return(0);
}


int resultsPanel::SwapPlotExactIndex(unsigned int select, bool updateRestGUI)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (select < Plots.size())
	{
		if (Plots[select].plotType == PDATA_UNKNOWN)	//if it doesn't have a clue what it's dealing with, load some data!
		if (!LoadData(Plots[select].filename))	//load the data for the new plot.
		if (!LoadData(FullFilenames[select]))
			return(0); //if it can't load any data, don't do anything else with this.

		if (RawPlotButton->GetLabel().IsSameAs(wxT("View Raw Data")) && Plots[select].plotType != PDATA_RAWONLY)
			RawOutput->Hide();
	
		if (plot == NULL)	//it hasn't been assigned anything yet
		{
			if (Plots[select].plotType != PDATA_RAWONLY)
			{
				plot = Plots[select].plot;
				if (PlotCustomizeSizer->GetItem(plot)==NULL)
					PlotCustomizeSizer->Prepend(plot, 1, wxALL | wxEXPAND, 5);
				if (RVertSizer->GetItem(PlotCustomizeSizer))
					int test = 0;
				if (PlotCustomizeSizer->GetItem(plot))
					int test = 0;
				if (RVertSizer->GetItem(plot))
					int test = 0;
				if (RVertSizer->GetItem(plot, true))
					int test = 0;
				RVertSizer->Show(PlotCustomizeSizer);
				
				EnableDefaultPlotControls();
				plot->Show();
				//it wasn't initially added to anything, so maybe it's being weird?

				CurPlotDisplay = select;
				if (updateRestGUI)
				{
					UpdateDataDropDowns(CurPlotDisplay);
					if (RawOutput->IsShown())
						LoadDataIntoRawOut(FullFilenames[select]);	//only change it

					double bounds[6];	//documentation says 6. Code says 4...
					plot->GetVisiblePlotBoundary(bounds);
					xMin->ChangeValue(wxString::Format(wxT("%f"), bounds[0]));	//these don't cause new events when the text changes
					xMax->ChangeValue(wxString::Format(wxT("%f"), bounds[1]));
					yMin->ChangeValue(wxString::Format(wxT("%f"), bounds[2]));
					yMax->ChangeValue(wxString::Format(wxT("%f"), bounds[3]));

					switch (Plots[select].plotType)
					{
					case PDATA_JV:
						setX->Disable();
						break;
					case PDATA_UNKNOWN:
					case PDATA_BD:
					case PDATA_EMISSIONS:
					case PDATA_OPTICS:
					case PDATA_MESH:
					default:
						setX->Enable();
					}

					if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
					{
						//then it shouldn't show the other stuff
						PlotCustomizeSizer->Hide(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Hide(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Hide(cstm_plot);
						}
					}
					else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
					{
						//then it should show the custom plot stuff!
						SetCustomPlotPlacement();
						PlotCustomizeSizer->Show(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Show(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Show(cstm_plot);
						}
					}

					txt_XAxisName->Enable();
					txt_YAxisName->Enable();
					txt_PlotName->Enable();
					Chk_labels->Enable();
					Chk_legend->Enable();
					Slider_titleX->Enable();
					Slider_titleY->Enable();

					txt_XAxisName->ChangeValue(Plots[select].plot->GetLayer(XAXIS_LAYER)->GetName());
					txt_YAxisName->ChangeValue(Plots[select].plot->GetLayer(YAXIS_LAYER)->GetName());
					txt_PlotName->ChangeValue(Plots[select].plot->GetLayer(TITLE_LAYER)->GetName());
					Chk_labels->SetValue(Plots[select].ShowLabels);
					Chk_legend->SetValue(Plots[select].ShowLegend);
					int x,y;
					((mpText *)Plots[select].plot->GetLayer(TITLE_LAYER))->GetOffset(x,y);
					Slider_titleX->SetValue(x);
					Slider_titleY->SetValue(y);

				}
				plot->Layout();
				PlotCustomizeSizer->Layout();
				RVertSizer->Layout();
				return(1);
			}
			else //new plot type is raw data only, no previous plot loaded
			{
				//only want to do all this loading/writing into text box (slow) if the mouse is released on the slider or double click/open file
				if (updateRestGUI)
				{

					//it is switching to a raw only layout, and there wasn't any plot showing beforehand
					DisableDefaultPlotControls();	//this should grey most the stuff out
					FileSelectSlider->SetMax(ActiveFileIndices.size() - 1);	//allow the slider to have a spot for each file in the list box above
					int rel = FindDisplayFromExact(select);
					if (rel < int(ActiveFileIndices.size()))
						FileSelectSlider->SetValue(rel);	//initialize it to the current value
					else
						FileSelectSlider->SetValue(ActiveFileIndices.size() - 1);	//initialize it to the current value

					FileSelectSlider->Enable();
					CurPlotDisplay = select;	//this will say that a certain file is currently selected
					if (Plots[select].rawData == wxEmptyString)	//load data if it's currently empty
					if (!LoadData(FullFilenames[select]))
						return(0);
					LoadDataIntoRawOut(Plots[select].rawData, false);
					RawOutput->Show();	//make sure it's showing.
					//plot is null, so no need to hide
					
					if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
					{
						//then it shouldn't show the other stuff
						PlotCustomizeSizer->Hide(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Hide(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Hide(cstm_plot);
						}
					}
					else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
					{
						//then it should show the custom plot stuff!
						SetCustomPlotPlacement();
						PlotCustomizeSizer->Show(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Show(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Show(cstm_plot);

							cstmBtn_AddVisible->Disable();
							cstmBtn_AddActY->Disable();
						}
					}
					txt_XAxisName->Disable();
					txt_YAxisName->Disable();
					txt_PlotName->Disable();
					Chk_labels->Disable();
					Chk_legend->Disable();
					Slider_titleX->Disable();
					Slider_titleY->Disable();

					RVertSizer->Layout();
				}
				return(1);
			}
		}
		else if (CurPlotDisplay != select) //there was a plot loaded in at some point, which is not null. Plot is correct
		{  //
			if (Plots[select].plotType != PDATA_RAWONLY) //and it's actually going to be a new plot!
			{
				if (unsigned int(CurPlotDisplay) < Plots.size())
				{
					if (Plots[CurPlotDisplay].plotType == PDATA_RAWONLY)	//the previous was raw - hide the data to skip loading of files excessively (slow)
					{
						RawOutput->Hide();
						RawPlotButton->SetLabel(wxT("View Raw Data"));
					}
				}
				mpWindow* old = plot;
				PlotCustomizeSizer->Replace(plot, Plots[select].plot);	//take out the old plot and put in the new one
				plot = Plots[select].plot;	//now make sure plot points to the correct data for adjusting below
				//			plot->SetInitialSize();
				plot->Show();


				CurPlotDisplay = select;
				if (updateRestGUI)
				{
					EnableDefaultPlotControls();
					UpdateDataDropDowns(CurPlotDisplay);
					if (RawOutput->IsShown())
						LoadDataIntoRawOut(FullFilenames[select]);	//only change it
					double bounds[6];	//documentation says 6. Code says 4...
					plot->GetVisiblePlotBoundary(bounds);
					xMin->ChangeValue(wxString::Format(wxT("%f"), bounds[0]));	//these don't cause new events when the text changes
					xMax->ChangeValue(wxString::Format(wxT("%f"), bounds[1]));
					yMin->ChangeValue(wxString::Format(wxT("%f"), bounds[2]));
					yMax->ChangeValue(wxString::Format(wxT("%f"), bounds[3]));

					switch (Plots[select].plotType)
					{
					case PDATA_JV:
						setX->Disable();
						break;
					case PDATA_UNKNOWN:
					case PDATA_BD:
					case PDATA_EMISSIONS:
					case PDATA_OPTICS:
					case PDATA_MESH:
					default:
						setX->Enable();
					}
					if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
					{
						//then it shouldn't show the other stuff
						PlotCustomizeSizer->Hide(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Hide(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Hide(cstm_plot);
						}
					}
					else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
					{
						//then it should show the custom plot stuff!
						SetCustomPlotPlacement();
						PlotCustomizeSizer->Show(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Show(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Show(cstm_plot);
						}
						cstmBtn_AddVisible->Enable();
//						cstmBtn_AddActY->Enable();
					}
					txt_XAxisName->Enable();
					txt_YAxisName->Enable();
					txt_PlotName->Enable();
					Chk_labels->Enable();
					Chk_legend->Enable();
					Slider_titleX->Enable();
					Slider_titleY->Enable();

					txt_XAxisName->ChangeValue(Plots[select].plot->GetLayer(XAXIS_LAYER)->GetName());
					txt_YAxisName->ChangeValue(Plots[select].plot->GetLayer(YAXIS_LAYER)->GetName());
					txt_PlotName->ChangeValue(Plots[select].plot->GetLayer(TITLE_LAYER)->GetName());
					Chk_labels->SetValue(Plots[select].ShowLabels);
					Chk_legend->SetValue(Plots[select].ShowLegend);
					int x,y;
					((mpText *)Plots[select].plot->GetLayer(TITLE_LAYER))->GetOffset(x,y);
					Slider_titleX->SetValue(x);
					Slider_titleY->SetValue(y);
				}
				//			Layout();
				RVertSizer->Layout();
				PlotCustomizeSizer->Layout();
				if (old && old!=plot)
					old->Hide();	//it is essential to hide the old plot, or it seems to get really confused
				//as to which plot is active (lots are active)



				return(1);
			}
			else //the new plot is raw data only, but different from old plot (or raw data)
			{
				CurPlotDisplay = select;
				plot->Hide();
				RVertSizer->Hide(PlotCustomizeSizer);
				RawOutput->ChangeValue(wxEmptyString);

				if (updateRestGUI)	//update the rest of the GUI stuff
				{
					DisableDefaultPlotControls();	//this should grey most the stuff out
					FileSelectSlider->SetMax(ActiveFileIndices.size() - 1);	//allow the slider to have a spot for each file in the list box above
					int rel = FindDisplayFromExact(select);
					if (rel < int(ActiveFileIndices.size()))
						FileSelectSlider->SetValue(rel);	//initialize it to the current value
					else
						FileSelectSlider->SetValue(ActiveFileIndices.size() - 1);	//initialize it to the current value

					FileSelectSlider->Enable();
					CurPlotDisplay = select;	//this will say that a certain file is currently selected
					if (Plots[select].rawData == wxEmptyString)	//load data if it's currently empty
					if (!LoadData(FullFilenames[select]))
						return(0);
					LoadDataIntoRawOut(Plots[select].rawData, false);
					//plot is null, so no need to hide

					if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
					{
						//then it shouldn't show the other stuff
						PlotCustomizeSizer->Hide(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Hide(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Hide(cstm_plot);
						}
					}
					else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
					{
						//then it should show the custom plot stuff!
						SetCustomPlotPlacement();
						PlotCustomizeSizer->Show(MergeManipSizer);
						if (cstm_plot)
						{
							if (PlotCustomizeSizer->GetItem(cstm_plot))
								PlotCustomizeSizer->Show(cstm_plot);
							else if (MergeManipSizer->GetItem(cstm_plot))
								MergeManipSizer->Show(cstm_plot);

							cstmBtn_AddVisible->Disable();
							cstmBtn_AddActY->Disable();
						}
					}

					txt_XAxisName->Disable();
					txt_YAxisName->Disable();
					txt_PlotName->Disable();
					Chk_labels->Disable();
					Chk_legend->Disable();
					Slider_titleX->Disable();
					Slider_titleY->Disable();
				}
				RawOutput->Show();
				RVertSizer->Layout();
				return(1);
			}
		}
		else if (updateRestGUI) //CurPlotDisplay == select, select is unsigned <size, so if -1, would not get here. aka, CurPLotDisplay MUST be valid because select is valid
		{
			if (Plots[CurPlotDisplay].plotType != PDATA_RAWONLY)
			{
				EnableDefaultPlotControls();
				UpdateDataDropDowns(CurPlotDisplay);
				if (RawOutput->IsShown())
					LoadDataIntoRawOut(FullFilenames[select]);	//only change it
				double bounds[6];	//documentation says 6. Code says 4...
				plot->GetVisiblePlotBoundary(bounds);
				xMin->ChangeValue(wxString::Format(wxT("%f"), bounds[0]));	//these don't cause new events when the text changes
				xMax->ChangeValue(wxString::Format(wxT("%f"), bounds[1]));
				yMin->ChangeValue(wxString::Format(wxT("%f"), bounds[2]));
				yMax->ChangeValue(wxString::Format(wxT("%f"), bounds[3]));

				switch (Plots[select].plotType)
				{
				case PDATA_JV:
					setX->Disable();
					break;
				case PDATA_UNKNOWN:
				case PDATA_BD:
				case PDATA_EMISSIONS:
				case PDATA_OPTICS:
				case PDATA_MESH:
				default:
					setX->Enable();
				}
				if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
				{
					//then it shouldn't show the other stuff
					PlotCustomizeSizer->Hide(MergeManipSizer);
					if (cstm_plot)
					{
						if (PlotCustomizeSizer->GetItem(cstm_plot))
							PlotCustomizeSizer->Hide(cstm_plot);
						else if (MergeManipSizer->GetItem(cstm_plot))
							MergeManipSizer->Hide(cstm_plot);
					}
				}
				else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
				{
					//then it should show the custom plot stuff!
					SetCustomPlotPlacement();
					PlotCustomizeSizer->Show(MergeManipSizer);
					if (cstm_plot)
					{
						if (PlotCustomizeSizer->GetItem(cstm_plot))
							PlotCustomizeSizer->Show(cstm_plot);
						else if (MergeManipSizer->GetItem(cstm_plot))
							MergeManipSizer->Show(cstm_plot);

						cstmBtn_AddVisible->Enable();
					}
				}
				txt_XAxisName->Enable();
				txt_YAxisName->Enable();
				txt_PlotName->Enable();
				Chk_labels->Enable();
				Chk_legend->Enable();
				Slider_titleX->Enable();
				Slider_titleY->Enable();

				txt_XAxisName->ChangeValue(Plots[select].plot->GetLayer(XAXIS_LAYER)->GetName());
				txt_YAxisName->ChangeValue(Plots[select].plot->GetLayer(YAXIS_LAYER)->GetName());
				txt_PlotName->ChangeValue(Plots[select].plot->GetLayer(TITLE_LAYER)->GetName());
				Chk_labels->SetValue(Plots[select].ShowLabels);
				Chk_legend->SetValue(Plots[select].ShowLegend);
				int x,y;
				((mpText *)Plots[select].plot->GetLayer(TITLE_LAYER))->GetOffset(x,y);
				Slider_titleX->SetValue(x);
				Slider_titleY->SetValue(y);
			}
			else //the gui needs to be updated, and the current display is not only raw data
			{
				plot->Hide();
				RVertSizer->Hide(PlotCustomizeSizer);	//this should also hide the custom part (which holds plot). Can't customize a plot if one isn't a plot!
				DisableDefaultPlotControls();	//this should grey most the stuff out
				FileSelectSlider->SetMax(ActiveFileIndices.size() - 1);	//allow the slider to have a spot for each file in the list box above
				int rel = FindDisplayFromExact(select);
				if (rel < int(ActiveFileIndices.size()))
					FileSelectSlider->SetValue(rel);	//initialize it to the current value
				else
					FileSelectSlider->SetValue(ActiveFileIndices.size() - 1);	//initialize it to the current value

				FileSelectSlider->Enable();
				CurPlotDisplay = select;	//this will say that a certain file is currently selected
				if (Plots[select].rawData == wxEmptyString)	//load data if it's currently empty
				if (!LoadData(FullFilenames[select]))
					return(0);
				LoadDataIntoRawOut(Plots[select].rawData, false);
				RawOutput->Show();	//make sure it's showing.
				//plot is null, so no need to hide

				if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))	//Hide Custom Plot
				{
					//then it shouldn't show the other stuff
					PlotCustomizeSizer->Hide(MergeManipSizer);
					if (cstm_plot)
					{
						if (PlotCustomizeSizer->GetItem(cstm_plot))
							PlotCustomizeSizer->Hide(cstm_plot);
						else if (MergeManipSizer->GetItem(cstm_plot))
							MergeManipSizer->Hide(cstm_plot);
					}
				}
				else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
				{
					//then it should show the custom plot stuff!
					SetCustomPlotPlacement();
					PlotCustomizeSizer->Show(MergeManipSizer);
					if (cstm_plot)
					{
						if (PlotCustomizeSizer->GetItem(cstm_plot))
							PlotCustomizeSizer->Show(cstm_plot);
						else if (MergeManipSizer->GetItem(cstm_plot))
							MergeManipSizer->Show(cstm_plot);
						cstmBtn_AddActY->Disable();
						cstmBtn_AddVisible->Disable();
					}
				}
				txt_XAxisName->Disable();
				txt_YAxisName->Disable();
				txt_PlotName->Disable();
				Chk_labels->Disable();
				Chk_legend->Disable();
				Slider_titleX->Disable();
				Slider_titleY->Disable();
				RVertSizer->Layout();
			}
			return(1);
		}
	}
	else// if (updateRestGUI) //wants to select NO plot. So reset everything appropriately
	{
		if (plot)
		{
			plot->Hide();
			if (PlotCustomizeSizer->GetItem(plot))
				PlotCustomizeSizer->Detach(plot);
			plot = NULL;
		}
		RawOutput->Hide();

		Btn_PlotCustom->SetLabel(wxT("Build Custom Plot"));
		//then it shouldn't show the other stuff
		PlotCustomizeSizer->Hide(MergeManipSizer);
		if (cstm_plot)
		{
			if (PlotCustomizeSizer->GetItem(cstm_plot))
				PlotCustomizeSizer->Hide(cstm_plot);
			else if (MergeManipSizer->GetItem(cstm_plot))
				MergeManipSizer->Hide(cstm_plot);
		}
		Btn_PlotCustom->Disable();

		CurPlotDisplay = -1;	//guarantee it's bad
		plot = NULL;
		DisableDefaultPlotControls();
		UpdateDataDropDowns(CurPlotDisplay);

		RVertSizer->Layout();
	}
	return(0);
}

int resultsPanel::FindDisplayFromExact(int exact)
{
	int ret = -1;
	for (unsigned int i = 0; i < ActiveFileIndices.size(); ++i)
	{
		if (exact == ActiveFileIndices[i])
		{
			ret = i;
			break;
		}
	}
	return(ret);
}

void resultsPanel::UpdatePlotBounds(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (plot != NULL)
	{
		double xn, xx, yn, yx;
		bool goodXMin, goodXMax, goodYMin, goodYMax;
		wxString buffer;
		buffer = xMin->GetValue();
		goodXMin = buffer.ToDouble(&xn);

		buffer = xMax->GetValue();
		goodXMax = buffer.ToDouble(&xx);

		buffer = yMin->GetValue();
		goodYMin = buffer.ToDouble(&yn);

		buffer = yMax->GetValue();
		goodYMax = buffer.ToDouble(&yx);

		if (goodXMin && goodXMax && goodYMin && goodYMax)
		{
			if (xn < xx && yn < yx)
				plot->Fit(xn, xx, yn, yx);	//update the plot with the new bounds
		}
	}

	event.Skip();
	return;
}

void resultsPanel::UpdatePlotBoundTxtBoxesEvt(wxCommandEvent& event)
{
	UpdatePlotBoundTxtBoxes();
	event.Skip();
	return;
}

void resultsPanel::UpdatePlotBoundTxtBoxes(void)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (plot)
	{
		double bounds[6];	//documentation says 6. Code says 4...
		plot->GetVisiblePlotBoundary(bounds);
		////////////////////
		if (fabs(bounds[0]) > 1.0e4 || fabs(bounds[0]) < 1.0e-4)
			xMin->ChangeValue(wxString::Format(wxT("%.3E"), bounds[0]));	//these don't 
		else
			xMin->ChangeValue(wxString::Format(wxT("%f"), bounds[0]));	//these don't 
		////////////////////
		if (fabs(bounds[1]) > 1.0e4 || fabs(bounds[1]) < 1.0e-4)
			yMax->ChangeValue(wxString::Format(wxT("%.3E"), bounds[3]));
		else
			xMax->ChangeValue(wxString::Format(wxT("%f"), bounds[1]));
		////////////////////
		if (fabs(bounds[2]) > 1.0e4 || fabs(bounds[2]) < 1.0e-4)
			yMax->ChangeValue(wxString::Format(wxT("%.3E"), bounds[3]));
		else
			yMin->ChangeValue(wxString::Format(wxT("%f"), bounds[2]));
		////////////////////
		if (fabs(bounds[3]) > 1.0e4 || fabs(bounds[3]) < 1.0e-4)
			yMax->ChangeValue(wxString::Format(wxT("%.3E"), bounds[3]));
		else
			yMax->ChangeValue(wxString::Format(wxT("%f"), bounds[3]));
		////////////////////
	}
	
	return;
}

void resultsPanel::MouseReleaseUpdate(wxMouseEvent &event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (event.WasProcessed() && event.GetPropagatedFrom() == plot)
	{
		if (plot)
		{
			double bounds[6];	//documentation says 6. Code says 4...
			plot->GetVisiblePlotBoundary(bounds);
			xMin->ChangeValue(wxString::Format(wxT("%f"), bounds[0]));	//these don't cause new events when the text changes
			xMax->ChangeValue(wxString::Format(wxT("%f"), bounds[1]));
			yMin->ChangeValue(wxString::Format(wxT("%f"), bounds[2]));
			yMax->ChangeValue(wxString::Format(wxT("%f"), bounds[3]));
		}
		return;
	}
	event.Skip();
}

int resultsPanel::LoadData(wxString fname, wxString header, bool force)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (fname.empty())
		return(0);	//there is no file to be loaded

	if (fname.EndsWith(wxT(".bd")))
	{
		if (header == wxEmptyString)
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if(LoadBDPlot(fname, force, &(Plots[whichPlot])))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadBDPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}
		else
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)	//this should NEVER happen
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if(LoadBDPlot(&(Plots[whichPlot]), header))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadBDPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}

	}
	else if (fname.EndsWith(wxT(".qe")))
	{
		if (header == wxEmptyString)
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if(LoadQEPlot(fname, force, &(Plots[whichPlot])))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadQEPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}
		else
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)	//this should NEVER happen
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if(LoadQEPlot(&(Plots[whichPlot]), header))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadQEPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}
	}
	else if (fname.EndsWith(wxT(".mesh")))
	{
		if (header == wxEmptyString)
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if (LoadMESHPlot(fname, force, &(Plots[whichPlot])))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadMESHPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}
		else
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)	//this should NEVER happen
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if (LoadMESHPlot(&(Plots[whichPlot]), header))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadMESHPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}

		}
		return(0);
	}
	else if (fname.EndsWith(wxT(".flist")))
	{
		if (LoadFList(fname))
			return(FFILES_SET_FIRST);
		return(FFILES_SET_BAD);

	}
	else if (fname.EndsWith(wxT(".jv")))
	{
		if (header == wxEmptyString)
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if(LoadJVPlot(fname, force, &(Plots[whichPlot])))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadJVPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
		}
		else
		{
			unsigned int whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)	//this should NEVER happen
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if (LoadJVPlot(&(Plots[whichPlot]), header))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else
			{
				if (LoadJVPlot(fname, force))
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}

		}
		return(0);
	}
//	else if (fname.EndsWith(wxT(".dat")))
//	{
//		return(0);
//	}
	else if (fname.EndsWith(wxT(".envin")))
	{
		unsigned int whichPlot = FindActiveFile(fname);
		if (whichPlot != -1)	//this should NEVER happen
		{
			if (Plots.size() <= whichPlot)
				Plots.resize(whichPlot + 1);
			if (LoadRawPlot(fname, force, &(Plots[whichPlot])))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
		else
		{
			if (LoadRawPlot(fname, force))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
	}
	else if (fname.EndsWith(wxT(".envout")))
	{
		unsigned int whichPlot = FindActiveFile(fname);
		if (whichPlot != -1)	//this should NEVER happen
		{
			if (Plots.size() <= whichPlot)
				Plots.resize(whichPlot + 1);
			if (LoadRawPlot(fname, force, &(Plots[whichPlot])))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
		else
		{
			if (LoadRawPlot(fname, force))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
	}
	else if (fname.EndsWith(wxT(".plot")))
	{
		unsigned int whichPlot = FindActiveFile(fname);
		if (whichPlot != -1)	//this should NEVER happen
		{
			if (Plots.size() <= whichPlot)
				Plots.resize(whichPlot + 1);
			if (LoadCPlot(fname, force, &(Plots[whichPlot])))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
		else
		{
			if (LoadCPlot(fname, force))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
	}
	else //attempt to load a generic plot type, and if that fails, just do raw data
	{
		unsigned int whichPlot = FindActiveFile(fname);
		if (whichPlot != -1)	//this should NEVER happen
		{
			if (Plots.size() <= whichPlot)
				Plots.resize(whichPlot + 1);
			if (LoadGenericPlot(fname, force, &(Plots[whichPlot])))	//it failed
				return(FFILES_SET_LAST);
			if (LoadRawPlot(fname, force, &(Plots[whichPlot])))	//try doing raw data
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
		else
		{
			if (LoadGenericPlot(fname, force))
				return(FFILES_SET_LAST);
			whichPlot = FindActiveFile(fname);
			if (whichPlot != -1)	//this should NEVER happen
			{
				if (Plots.size() <= whichPlot)
					Plots.resize(whichPlot + 1);
				if (LoadRawPlot(fname, force, &(Plots[whichPlot])))	//it failed
					return(FFILES_SET_LAST);
				return(FFILES_SET_BAD);
			}
			else if(LoadRawPlot(fname, force))
				return(FFILES_SET_LAST);
			return(FFILES_SET_BAD);
		}
	}
	

	return(FFILES_SET_BAD);
}

int resultsPanel::FilterFiles(int SetValues, wxString which)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	//first, see if there is a selection in the flist box
	if(which == wxEmptyString)
		which = FileListBox->GetStringSelection();
	std::vector<wxString> ffiltersOR, ffiltersAND;

	wxString str = FilterFileGroup->GetValue();
	wxString utility, remainder;
	if (FilterFileGroup->GetSelection() > 0)
		ffiltersAND.push_back(wxT("_") + str);

	str = FilterFileType->GetValue();
	if (str.IsSameAs(wxT("Band Diagrams")))
	{
		ffiltersOR.push_back(wxT(".bd"));
		FilterFileIteration->Enable();
	}
	if (str.IsSameAs(wxT("JV Curves")))
	{
		ffiltersOR.push_back(wxT(".jv"));
		FilterFileIteration->Disable();	//we don't care about iterations for jv curves. that will make them not show up.
		FilterFileIteration->ChangeValue(wxT(""));
	}
	if (str.IsSameAs(wxT("Emissions")))
	{
		ffiltersOR.push_back(wxT("Emissions"));	//what leaves the device
		ffiltersOR.push_back(wxT("Generated"));	//everything generated in the device
		ffiltersOR.push_back(wxT(".ptopt"));		//a lot of mini plots
		FilterFileIteration->Enable();
	}
	if (str.IsSameAs(wxT("Absorption")))
	{
		ffiltersOR.push_back(wxT("Incident.qe"));
		ffiltersOR.push_back(wxT("Entered.qe"));
		ffiltersOR.push_back(wxT("Absorbed"));
		ffiltersOR.push_back(wxT("Stimulated Emission"));
		ffiltersOR.push_back(wxT("LA "));
		ffiltersOR.push_back(wxT("SE "));
		
		FilterFileIteration->Enable();
	}
	if (str.IsSameAs(wxT("Environments")))
	{
		ffiltersOR.push_back(wxT(".env"));
		FilterFileIteration->Disable();
		FilterFileIteration->ChangeValue(wxT(""));
	}
	if (str.IsSameAs(wxT("Custom Plots")))
	{
		ffiltersOR.push_back(wxT(".plot"));
		FilterFileIteration->Enable();
	}

	str = FilterFileIteration->GetValue();
	long val = -1;
	if (!str.ToLong(&val))
		val = -1;

	if (val >= 0)
		ffiltersAND.push_back(wxString::Format(wxT("_%i_"), val));

	str = fileFilters->GetValue();
	if (!str.IsSameAs(wxT("Filter Files")))
	{
		str.Trim(false);
		while (!str.IsEmpty())
		{
			utility = str.BeforeFirst(' ', &remainder);
			ffiltersAND.push_back(utility);
			str = remainder.Trim(false);
		}
	}
	

	ActiveFileIndices.clear();
	FileListBox->Clear();
	unsigned int filtSzAND = ffiltersAND.size();
	unsigned int filtSzOR = ffiltersOR.size();
	for (unsigned int i = 0; i<AbbrFNames.size(); ++i)
	{
		bool passOR = filtSzOR > 0 ? false : true;	//ignore it if there's nothing. if there is, it has to match!
		bool passAND = true;

		for (unsigned int j = 0; j < filtSzOR; ++j) {
			if (AbbrFNames[i].Contains(ffiltersOR[j])) {
				passOR = true;
				break;
			}
		}
		if (passOR) {	//got past the first test - all the file selections based by type
			for (unsigned int j = 0; j < filtSzAND; ++j)
			{
				if (!AbbrFNames[i].Contains(ffiltersAND[j]))
				{
					passAND = false;
					break;
				}
			}

			if (passAND)
			{
				ActiveFileIndices.push_back(i);
				FileListBox->Append(AbbrFNames[i]);
			}
		}
		
	}
	if (ActiveFileIndices.size() > 0)
	{
		unsigned int last = ActiveFileIndices.size() - 1;
		FileSelectSlider->SetMax(last);

		bool setNewValue = true;
/*		if(which != wxEmptyString)	//try to maintain the same file!
		{
			if(FileListBox->SetStringSelection(which))
			{
				setNewValue = false;
				FileSelectSlider->SetValue(FileListBox->GetSelection());
				SetPlot(FileListBox->GetSelection(), true);
			}			
		}
*/		
		if(setNewValue)
		{
			if (SetValues == FFILES_SET_FIRST)
			{
				FileSelectSlider->SetValue(0);
				FileListBox->Select(0);
				SetPlot(0, true);
			}
			else if (SetValues == FFILES_SET_LAST)
			{
				FileSelectSlider->SetValue(last);
				FileListBox->Select(last);
				SetPlot(last, true);
			}
		}
		Btn_ClearFilteredFiles->Enable();
	}
	else
	{
		FileSelectSlider->SetMax(0);
		FileSelectSlider->SetValue(0);
		Btn_ClearFilteredFiles->Disable();
	}

	Layout();	//redraw the screen

	return(1);
}

unsigned int resultsPanel::FindActiveFile(wxString fname)
{
	unsigned int ret = -1;
	for (unsigned int i = 0; i < AbbrFNames.size(); i++)
	{
		if (fname.EndsWith(AbbrFNames[i]))
		{
			ret = i;
			break;
		}
	}
	return(ret);
}

int resultsPanel::LoadFList(wxString fname)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	wxTextFile* oFile = new wxTextFile(fname);
	if (!oFile->Open())
		return(0);	//indicate failed to open file

	wxString line = oFile->GetFirstLine();
	wxString utility;
	if (!line.StartsWith(wxT("Simulation Groups"), &utility))
		return(0);

	ClearPlots();
	ActiveFileIndices.clear();
	FullFilenames.clear();
	AbbrFNames.clear();

	FilterFileGroup->Clear();
	FilterFileGroup->Append(wxT("Group"));
	line = oFile->GetNextLine();
	while (!line.StartsWith(wxT("Simulation Output Files")))
	{
		if (!line.IsEmpty())
			FilterFileGroup->Append(line);
		line = oFile->GetNextLine();
	}
	FilterFileGroup->Select(0);	//select the 'Group' as the main bit

	wxString directory = wxEmptyString;
	wxString::size_type pos = fname.find_last_of("/");
	if (pos == wxString::npos)
		pos = fname.find_last_of("\\");
	if (pos != wxString::npos)
	{
		directory = fname;
		directory.erase(pos + 1);	//get rid of the filename
	}
	
	//loading a file list is like opening up a new simulation set. Clear everything out!
	int i = FileListBox->GetCount();
	for (line = oFile->GetNextLine(); !oFile->Eof(); line = oFile->GetNextLine())
	{
		if (!line.IsEmpty())
		{
			wxTextFile* testOpen = new wxTextFile(directory + line);
			if (testOpen->Exists())
			{
				FileListBox->Append(line);
				AbbrFNames.push_back(line);	//this is the one we
				FullFilenames.push_back(directory + line);
				ActiveFileIndices.push_back(i);
				i++;
			}
			else
			{
				wxTextFile* testOpen2 = new wxTextFile(line);
				if (testOpen2->Exists())
				{
					FileListBox->Append(line);
					AbbrFNames.push_back(line);	//this is the one we
					FullFilenames.push_back(line);
					ActiveFileIndices.push_back(i);
					i++;
				}
				delete testOpen2;
				testOpen2 = NULL;
			}
			delete testOpen;
			testOpen = NULL;	//prevent bad stuff
		}
	}
	oFile->Close();
	delete oFile;

	fListfName = fname;	//save it for future additions

	FilterFileIteration->SetLabel(wxEmptyString);	//make sure the iteration is nada
	fileFilters->SetLabel(wxEmptyString);	//make sure other filters is nada
	FilterFileType->Select(0);	//make sure it says output type


	Plots.resize(i);	//just make all the data here to begin with. Prevent future performance hits
	if (i>0)
	{
		FileSelectSlider->SetMax(i - 1);	//allow the slider to have a spot for each file in the list box above
		FileSelectSlider->SetValue(0);	//initialize it to the 0th value
		FileSelectSlider->Enable();
		SetPlot(0, true);	//when a plot is formed for the first time, the plot controls are enabled

	}
	else
	{
		DisableDefaultPlotControls();
	}
	Layout();	//update the GUI to show everything in this panel

	return(1);
}

int resultsPanel::ClearPlots()
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (plot != NULL)	//it hasn't been assigned anything yet
	{
		RVertSizer->Detach(plot);
	}
	for (unsigned int i = 0; i<Plots.size(); ++i)
	{
		for (unsigned int j = 0; j<Plots[i].LData.size(); ++j)
		{
			Plots[i].LData[j].ClearData();
			
		}
		Plots[i].LData.clear();
		Plots[i].ActiveLData.clear();
		delete Plots[i].plot;
		Plots[i].plot = NULL;
		Plots[i].xaxis = NULL;
		Plots[i].yaxis = NULL;
		Plots[i].legend = NULL;
		Plots[i].title = NULL;
	}
	Plots.clear();
	plot = NULL;
	CurPlotDisplay = -1;

	for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
		cstm_PlotData.LData[i].ClearData();
	cstm_PlotData.LData.clear();


	if (cstm_plot)
	{
		if (PlotCustomizeSizer->GetItem(cstm_plot) == NULL)
			PlotCustomizeSizer->Detach(cstm_plot);
		delete cstm_plot;
	}
	
	cstm_plot = NULL;
	cstm_xaxis = NULL;
	cstm_yaxis = NULL;
	DisableDefaultPlotControls();
	return(0);
}

bool resultsPanel::CheckStringActiveJV(wxString& label, PlotData* pl, bool incDependencies)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return false;
	ShowOutputs* outs = NULL;
	if (pl != NULL)
		outs = &(pl->outs);
	else
		outs = &whichData;

	if (label.IsSameAs(wxT("|"), false))
		return false;

	if (label.IsSameAs(wxT("Voltage"), false))
		return outs->volt;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("J_ctc_x(1)"), false))
	{
		if (incDependencies)
			return outs->j1ctc_x | outs->javg_x;	//have this be loaded separately be default
		return outs->j1ctc_x;
	}
	if (label.IsSameAs(wxT("J_ctc_x(2)"), false))
	{
		if (incDependencies)
			return outs->j2ctc_x | outs->javg_x;	//have this be loaded separately be default
		return outs->j2ctc_x;
	}
	if (label.IsSameAs(wxT("J_x(1)"), false))
		return outs->j1_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("J_x(2)"), false))
		return outs->j2_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("J_avg_x"), false))
		return outs->javg_x;	//have this be loaded separately be default

	if (label.IsSameAs(wxT("Voltage_N"), false))
		return outs->volt_n;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jn_ctc_x(1)"), false))
	{
		if (incDependencies)
			return outs->j1ctc_n_x | outs->javg_n_x;	//have this be loaded separately be default
		return outs->j1ctc_n_x;
	}
	if (label.IsSameAs(wxT("Jn_ctc_x(2)"), false))
	{
		if (incDependencies)
			return outs->j2ctc_n_x | outs->javg_n_x;	//have this be loaded separately be default
		return outs->j2ctc_n_x;
	}
	if (label.IsSameAs(wxT("Jn_x(1)"), false))
		return outs->j1_n_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jn_x(2)"), false))
		return outs->j2_n_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jn_avg_x"), false))
		return outs->javg_n_x;	//have this be loaded separately be default

	if (label.IsSameAs(wxT("Voltage_P"), false))
		return outs->volt_p;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jp_ctc_x(1)"), false))
	{
		if (incDependencies)
			return outs->j1ctc_p_x | outs->javg_p_x;	//have this be loaded separately be default
		return outs->j1ctc_p_x;
	}
	if (label.IsSameAs(wxT("Jp_ctc_x(2)"), false))
	{
		if (incDependencies)
			return outs->j2ctc_p_x | outs->javg_p_x;	//have this be loaded separately be default
		return outs->j2ctc_p_x;
	}
	if (label.IsSameAs(wxT("Jp_x(1)"), false))
		return outs->j1_p_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jp_x(2)"), false))
		return outs->j2_p_x;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Jp_avg_x"), false))
		return outs->javg_p_x;	//have this be loaded separately be default

	if (label.IsSameAs(wxT("y=0"), false))
		return outs->xaxisZero;	//have this be loaded separately be default




	return(false);
}

bool resultsPanel::CheckStringActiveBD(wxString& label, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return false;
	ShowOutputs* outs = NULL;
	if (pl != NULL)
		outs = &(pl->outs);
	else
		outs = &whichData;


	if (label.IsSameAs(wxT("Point"), false))
		return outs->point;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("X"), false))
		return outs->x;
	if (label.IsSameAs(wxT("Y"), false))
		return outs->y;
	if (label.IsSameAs(wxT("Z"), false))
		return outs->z;
	if (label.IsSameAs(wxT("Ef"), false))
		return outs->Ef;
	if (label.IsSameAs(wxT("Ef_std_dev"), false))
		return outs->Ef & outs->stddev;
	if (label.IsSameAs(wxT("Efn"), false))
		return outs->Efn;
	if (label.IsSameAs(wxT("Efn_std_dev"), false))
		return outs->Efn & outs->stddev;
	if (label.IsSameAs(wxT("Efp"), false))
		return outs->Efp;
	if (label.IsSameAs(wxT("Efp_std_dev"), false))
		return outs->Efp & outs->stddev;
	if (label.IsSameAs(wxT("Ec"), false))
		return outs->Ec;
	if (label.IsSameAs(wxT("Ec_std_dev"), false))
		return outs->Ec & outs->stddev;
	if (label.IsSameAs(wxT("Ev"), false))
		return outs->Ev;
	if (label.IsSameAs(wxT("Ev_std_dev"), false))
		return outs->Ev & outs->stddev;
	if (label.IsSameAs(wxT("Psi"), false))
		return outs->Psi;
	if (label.IsSameAs(wxT("Psi_std_dev"), false))
		return outs->Psi & outs->stddev;
	if (label.IsSameAs(wxT("Ec_Carriers"), false))
		return outs->electrons;
	if (label.IsSameAs(wxT("EcC_std_dev"), false))
		return outs->electrons & outs->stddev;
	if (label.IsSameAs(wxT("Ev_Carriers"), false))
		return outs->holes;
	if (label.IsSameAs(wxT("EvC_std_dev"), false))
		return outs->holes & outs->stddev;
	if (label.IsSameAs(wxT("Imp_Charge"), false))
		return outs->impCharge;
	if (label.IsSameAs(wxT("Imp_Charge_std_dev"), false))
		return outs->impCharge & outs->stddev;
	if (label.IsSameAs(wxT("n"), false))
		return outs->n;
	if (label.IsSameAs(wxT("n_std_dev"), false))
		return outs->n & outs->stddev;
	if (label.IsSameAs(wxT("p"), false))
		return outs->p;
	if (label.IsSameAs(wxT("p_std_dev"), false))
		return outs->p & outs->stddev;
	//nothing for n*p yet
	if (label.IsSameAs(wxT("SRH_Rate1"), false))
		return outs->srh1;
	if (label.IsSameAs(wxT("SRH1_std_dev"), false))
		return outs->srh1 & outs->stddev;
	if (label.IsSameAs(wxT("SRH_Rate2"), false))
		return outs->srh2;
	if (label.IsSameAs(wxT("SRH2_std_dev"), false))
		return outs->srh2 & outs->stddev;
	if (label.IsSameAs(wxT("SRH_Rate3"), false))
		return outs->srh3;
	if (label.IsSameAs(wxT("SRH3_std_dev"), false))
		return outs->srh3 & outs->stddev;
	if (label.IsSameAs(wxT("SRH_Rate4"), false))
		return outs->srh4;
	if (label.IsSameAs(wxT("SRH4_std_dev"), false))
		return outs->srh4 & outs->stddev;
	if (label.IsSameAs(wxT("Scatter"), false))
		return outs->scatter;
	if (label.IsSameAs(wxT("Sctr_std_dev"), false))
		return outs->scatter & outs->stddev;
	if (label.IsSameAs(wxT("Rho"), false))
		return outs->rho;
	if (label.IsSameAs(wxT("Rho_std_dev"), false))
		return outs->rho & outs->stddev;
	if (label.IsSameAs(wxT("Optical_Gen"), false))
		return outs->genlight;
	if (label.IsSameAs(wxT("LGe_std_dev"), false))
		return outs->genlight & outs->stddev;
	if (label.IsSameAs(wxT("Generation_Th"), false))
		return outs->genthermal;
	if (label.IsSameAs(wxT("GenTh_std_dev"), false))
		return outs->genthermal & outs->stddev;
	if (label.IsSameAs(wxT("Recomb_Thermal"), false))
		return outs->recthermal;
	if (label.IsSameAs(wxT("RecTh_std_dev"), false))
		return outs->recthermal & outs->stddev;
	if (label.IsSameAs(wxT("Jn_x"), false))
		return outs->Jn;
	if (label.IsSameAs(wxT("Jn_x_std_dev"), false))
		return outs->Jn & outs->stddev;
	if (label.IsSameAs(wxT("Jn_y"), false))
		return outs->Jn;
	if (label.IsSameAs(wxT("Jn_y_std_dev"), false))
		return outs->Jn & outs->stddev;
	if (label.IsSameAs(wxT("Jn_z"), false))
		return outs->Jn;
	if (label.IsSameAs(wxT("Jn_z_std_dev"), false))
		return outs->Jn & outs->stddev;
	if (label.IsSameAs(wxT("Jp_x"), false))
		return outs->Jp;
	if (label.IsSameAs(wxT("Jp_x_std_dev"), false))
		return outs->Jp & outs->stddev;
	if (label.IsSameAs(wxT("Jp_y"), false))
		return outs->Jp;
	if (label.IsSameAs(wxT("Jp_y_std_dev"), false))
		return outs->Jp & outs->stddev;
	if (label.IsSameAs(wxT("Jp_z"), false))
		return outs->Jp;
	if (label.IsSameAs(wxT("Jp_z_std_dev"), false))
		return outs->Jp & outs->stddev;

	return false;
}
bool resultsPanel::CheckStringActiveMESH(wxString& label, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return false;
	ShowOutputs* outs = NULL;
	if (pl != NULL)
		outs = &(pl->outs);
	else
		outs = &whichData;

	if (label.IsSameAs(wxT("Point"), false))
		return outs->point;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Min_X"), false))
		return outs->minX;
	if (label.IsSameAs(wxT("Pos_X"), false))
		return outs->posX;
	if (label.IsSameAs(wxT("Max_X"), false))
		return outs->maxX;
	if (label.IsSameAs(wxT("Width_X"), false))
		return outs->widthX;
	if (label.IsSameAs(wxT("Min_Y"), false))
		return outs->minY;
	if (label.IsSameAs(wxT("Pos_Y"), false))
		return outs->posY;
	if (label.IsSameAs(wxT("Max_Y"), false))
		return outs->maxY;
	if (label.IsSameAs(wxT("AdjP"), false))
		return outs->adjP;
	if (label.IsSameAs(wxT("AdjN"), false))
		return outs->adjN;
	if (label.IsSameAs(wxT("Adj+X1"), false))
		return outs->adjXP1;
	if (label.IsSameAs(wxT("Adj+X2"), false))
		return outs->adjXP2;
	if (label.IsSameAs(wxT("Adj-X1"), false))
		return outs->adjXM1;
	if (label.IsSameAs(wxT("Adj-X2"), false))
		return outs->adjXM2;
	if (label.IsSameAs(wxT("Adj+Y1"), false))
		return outs->adjYP1;
	if (label.IsSameAs(wxT("Adj+Y2"), false))
		return outs->adjYP2;
	if (label.IsSameAs(wxT("Adj-Y1"), false))
		return outs->adjYM1;
	if (label.IsSameAs(wxT("Adj-Y2"), false))
		return outs->adjYM2;


	return false;
}

bool resultsPanel::CheckStringActiveQE(wxString& label, PlotData* pl)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return false;
	ShowOutputs* outs = NULL;
	if (pl != NULL)
		outs = &(pl->outs);
	else
		outs = &whichData;


	if (label.IsSameAs(wxT("Wavelength(nm)"), false))
		return outs->wavelength;	//have this be loaded separately be default
	if (label.IsSameAs(wxT("Energy(eV)"), false))
		return outs->energy;
	if (label.IsSameAs(wxT("Photons"), false))
		return outs->photon;
	if (label.IsSameAs(wxT("Photon_Flux"), false))
		return outs->pflux;
	if (label.IsSameAs(wxT("Power(mW)"), false))
		return outs->power;
	if (label.IsSameAs(wxT("Intensity(mW/cm^2)"), false))
		return outs->intensity;
	

	return false;
}

wxString resultsPanel::GetFilename()
{
	return curOutputName + '_' + wxString::Format(wxT("%8i"), fileIndex) + wxT(".dev");
}

void resultsPanel::LoadExistingSimulation(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	wxFileDialog saveDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("DAT files (*.dat)|*.dat"));
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;
	if (saveDialog.GetFilename().IsSameAs(wxT("Poisson.dat"), false) || saveDialog.GetFilename().IsSameAs(wxT("CarriersOutput.dat"), false))
	{
		wxMessageDialog noticeDiag(this, wxT("Please select a file that begins with the configuration name!"));
		LoadExistingSimulation(event); //lazy, should fix
		return;
	}
	else{
		wxMessageDialog noticeDiag(this, wxT("Please select an output file!"));
		LoadExistingSimulation(event); //lazy, should fix
		return;
	}
}

void resultsPanel::FFiles(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	FilterFiles(FFILES_SET_FIRST);
	event.Skip();
}

void resultsPanel::VarSelected(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	//load up the data into the GUI
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	PlotData* pl = &Plots[CurPlotDisplay];
	//figure out what the appropriate GUI selection ought to be
	wxString txt = ActivePlotData->GetStringSelection();	//this is going to be a legend value
	ActivePlotData->SetToolTip(txt);
	int tmp = -1;
	
	pl->GUISelection = pl->FindHeader(txt);
	
	if (pl->GUISelection == -1)
	{
		rPen->Hide();
		gPen->Hide();
		bPen->Hide();
		stylePen->Hide();
		widthPen->Hide();
		legendUpdate->Hide();
		Btn_Normalize->Hide();
		Btn_ToggleZeros->Hide();
		OffsetActY->Hide();
		ScaleActYText->Hide();
		cstmBtn_AddActY->Hide();
		legText->Hide();
		txtRGBA->Hide();
		txtOffset->Hide();
		statTxtScaleY->Hide();


		PenControl->Show(szr_plotLabels);
		event.Skip();
		PenControl->Layout();
		return;
	}

	tmp = pl->GUISelection;	//just use a local variable... no pointer stuff

	if (pl->LData[tmp].IsX() || pl->LData[tmp].IsXActive())
	{

		rPen->Show();
		gPen->Show();
		bPen->Show();
		stylePen->Show();
		widthPen->Show();
		legendUpdate->Show();
		Btn_Normalize->Show();
		Btn_ToggleZeros->Show();
		OffsetActY->Show();
		ScaleActYText->Show();
		
		legText->Show();
		txtRGBA->Show();
		txtOffset->Show();
		statTxtScaleY->Show();


		PenControl->Hide(szr_plotLabels);

		if (MergeManipSizer->IsShown(CustomBtnSzr)) {
			cstmBtn_AddActY->Show();
			cstmBtn_AddActY->Disable();
		}

		rPen->Disable();
		gPen->Disable();
		bPen->Disable();
		stylePen->Disable();
		widthPen->Disable();
		legendUpdate->Disable();
		Btn_Normalize->Disable();
		Btn_ToggleZeros->Disable();
		OffsetActY->Disable();
		ScaleActYText->Disable();
		
		legendUpdate->ChangeValue(pl->LData[tmp].GetLegend());
		setX->SetLabelText(wxT("Set Y"));
		event.Skip();
		PenControl->Layout();
		return;
	}
	else if (pl->LData[tmp].GetDataType() == PL_TYPE_UNDEF)
	{
		rPen->Show();
		gPen->Show();
		bPen->Show();
		stylePen->Show();
		widthPen->Show();
		legendUpdate->Show();
		Btn_Normalize->Show();
		Btn_ToggleZeros->Show();
		OffsetActY->Show();
		ScaleActYText->Show();
		cstmBtn_AddActY->Show();
		legText->Show();
		txtRGBA->Show();
		txtOffset->Show();
		statTxtScaleY->Show();


		PenControl->Hide(szr_plotLabels);

		rPen->Disable();
		gPen->Disable();
		bPen->Disable();
		stylePen->Disable();
		widthPen->Disable();
		legendUpdate->Disable();
		Btn_Normalize->Disable();
		Btn_ToggleZeros->Disable();
		OffsetActY->Disable();
		ScaleActYText->Disable();
		cstmBtn_AddActY->Disable();
		event.Skip();
		PenControl->Layout();
		return;
	}
	else
	{
		rPen->Show();
		gPen->Show();
		bPen->Show();
		stylePen->Show();
		widthPen->Show();
		legendUpdate->Show();
		Btn_Normalize->Show();
		Btn_ToggleZeros->Show();
		OffsetActY->Show();
		ScaleActYText->Show();
		cstmBtn_AddActY->Show();
		legText->Show();
		txtRGBA->Show();
		txtOffset->Show();
		statTxtScaleY->Show();


		rPen->Enable();
		gPen->Enable();
		bPen->Enable();
		stylePen->Enable();
		widthPen->Enable();
		legendUpdate->Enable();
		Btn_Normalize->Enable();
		Btn_ToggleZeros->Enable();
		OffsetActY->Enable();
		ScaleActYText->Enable();
		legendUpdate->ChangeValue(pl->LData[tmp].GetLegend());
		
		if (fabs(pl->LData[tmp].GetScale()) < 1.0e-4 || fabs(pl->LData[tmp].GetScale()) > 1.0e4)
			ScaleActYText->ChangeValue(wxString::Format(wxT("%.6E"), pl->LData[tmp].GetScale()));
		else
			ScaleActYText->ChangeValue(wxString::Format(wxT("%f"), pl->LData[tmp].GetScale()));

		if (fabs(pl->LData[tmp].GetOffset()) < 1.0e-4 || fabs(pl->LData[tmp].GetOffset()) > 1.0e4)
			OffsetActY->ChangeValue(wxString::Format(wxT("%.6E"), -pl->LData[tmp].GetOffset()));
		else
			OffsetActY->ChangeValue(wxString::Format(wxT("%f"), -pl->LData[tmp].GetOffset()));

		setX->SetLabelText(wxT("Set X"));
		if (pl->LData[tmp].IsIgnoreZeros())
			Btn_ToggleZeros->SetLabel(wxT("Show Zeroes"));
		else
			Btn_ToggleZeros->SetLabel(wxT("Hide Zeroes"));

		if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
			cstmBtn_AddActY->Enable();
		else
			cstmBtn_AddActY->Disable();

		PenControl->Hide(szr_plotLabels);
		PenControl->Layout();
	}


	wxColour clr = pl->LData[tmp].GetPen().GetColour();
	unsigned char r, g, b;
	r = clr.Red();
	g = clr.Green();
	b = clr.Blue();
	rPen->ChangeValue(wxString::Format(wxT("%u"), r));	//changevalue does not generate events
	gPen->ChangeValue(wxString::Format(wxT("%u"), g));	//and does not mark the value as changed
	bPen->ChangeValue(wxString::Format(wxT("%u"), b));


	tmp = pl->LData[tmp].GetPen().GetWidth();
	switch (tmp)
	{
	case 1:
		widthPen->Select(1);
		break;
	case 2:
		widthPen->Select(2);
		break;
	case 3:
		widthPen->Select(3);
		break;
	case 4:
		widthPen->Select(4);
		break;
	case 5:
		widthPen->Select(5);
		break;
	case 7:
		widthPen->Select(6);
		break;
	case 9:
		widthPen->Select(7);
		break;
	default:
		widthPen->Select(0);
		break;
	}
	wxPenStyle sty = pl->LData[pl->GUISelection].GetPen().GetStyle();
	switch (sty)
	{
	case wxPENSTYLE_SOLID:
		stylePen->Select(1);
		break;
	case wxPENSTYLE_DOT:
		stylePen->Select(2);
		break;
	case wxPENSTYLE_LONG_DASH:
		stylePen->Select(3);
		break;
	case wxPENSTYLE_SHORT_DASH:
		stylePen->Select(4);
		break;
	case wxPENSTYLE_DOT_DASH:
		stylePen->Select(5);
		break;
	case wxTRANSPARENT:
		stylePen->Select(6);
		break;
	default:
		stylePen->Select(0);
		break;
	}
	if (pl->LData[pl->GUISelection].IsDataConnected() == false)
		stylePen->Select(7);	//tell it to do the scatter plot!

//	if(pl->LData[pl->GUISelection].GetPen().GetCon
	event.Skip();
}

void resultsPanel::UpdateLegendText(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}


	if (plot != NULL)
	{
		wxString newTxt = legendUpdate->GetValue();
		//		int spot = ActivePlotData->FindString(Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].legend);
		//		if(spot != wxNOT_FOUND)
		//			ActivePlotData->SetString(spot, newTxt);

		Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].SetLegend(newTxt);
		if (Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].IsYActive())
		{
			int whichLayer = plot->CountAllLayers() - plot->CountLayers();;
			if (Plots[CurPlotDisplay].plotType != PDATA_CUSTOM)
			{
				for (unsigned int i = 0; i < Plots[CurPlotDisplay].ActiveLData.size(); ++i)
				{
					if (Plots[CurPlotDisplay].ActiveLData[i] == Plots[CurPlotDisplay].GUISelection)
						break;
					if (Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].ActiveLData[i]].IsYActive())
						whichLayer++;
				}
			}
			else
			{
				for (int i = 0; i < Plots[CurPlotDisplay].GUISelection; ++i)
				{
					if (Plots[CurPlotDisplay].LData[i].IsYActive())
						whichLayer++;
				}
			}
			plot->GetLayer(whichLayer)->SetName(newTxt);
		}
		plot->UpdateAll();
	}

	event.Skip();
}

void resultsPanel::UpdateLineColor(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}

	unsigned char r, g, b;
	long tmp;
	if (!rPen->GetValue().ToLong(&tmp))
		r = 0;
	else if (tmp < 0)
		r = 0;
	else if (tmp>255)
		r = 255;
	else
		r = tmp;

	if (!gPen->GetValue().ToLong(&tmp))
		g = 0;
	else if (tmp < 0)
		g = 0;
	else if (tmp>255)
		g = 255;
	else
		g = tmp;

	if (!bPen->GetValue().ToLong(&tmp))
		b = 0;
	else if (tmp < 0)
		b = 0;
	else if (tmp>255)
		b = 255;
	else
		b = tmp;


	wxColour clr;
	clr.Set(r, g, b);
	Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen().SetColour(clr);

	//now need to find it in the appropriate plot. The X data does not get added as a layer, and the X data is not necessarily first in LData.
	//additionally, the first layers are X & Y axis.
	int whichLayer = plot->CountAllLayers() - plot->CountLayers();;	// because that makes it the equiv of 
	/*	for(int i=0; i<Plots[CurPlotDisplay].GUISelection; ++i)
	{
	if(Plots[CurPlotDisplay].LData[i].IsYActive())
	whichLayer++;
	}
	*/
	if(Plots[CurPlotDisplay].plotType != PDATA_CUSTOM)
	{
		for (unsigned int i = 0; i<Plots[CurPlotDisplay].ActiveLData.size(); ++i)
		{
			if (Plots[CurPlotDisplay].ActiveLData[i] == Plots[CurPlotDisplay].GUISelection)
				break;
			if (Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].ActiveLData[i]].IsYActive())
				whichLayer++;
		}
	}
	else
	{
		for (int i = 0; i < Plots[CurPlotDisplay].GUISelection; ++i)
		{
			if (Plots[CurPlotDisplay].LData[i].IsYActive())
				whichLayer++;
		}
	}
	plot->GetLayer(whichLayer)->SetPen(Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen());
	plot->UpdateAll();

	event.Skip();
}

void resultsPanel::UpdateLineWidth(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}

	int selection = widthPen->GetSelection();
	switch (selection)
	{
	case 1:
	case 2:
	case 3:
	case 4:
	case 5: //these all maintain the value
		break;
	case 6:
		selection = 7;
		break;
	case 7:
		selection = 9;
		break;
	case 0:
	default:
		selection = -1;
		break;
	}
	if (selection == -1)
	{
		event.Skip();
		return;
	}
	Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen().SetWidth(selection);

	//now need to find it in the appropriate plot. The X data does not get added as a layer, and the X data is not necessarily first in LData.
	//additionally, the first layers are X & Y axis.
	int whichLayer = plot->CountAllLayers() - plot->CountLayers();;	//-1 because that makes it the equiv of 
	/*	for(int i=0; i<Plots[CurPlotDisplay].GUISelection; ++i)
	{
	if(Plots[CurPlotDisplay].LData[i].IsYActive())
	whichLayer++;
	}
	*/
	if(Plots[CurPlotDisplay].plotType != PDATA_CUSTOM)
	{
		for (unsigned int i = 0; i<Plots[CurPlotDisplay].ActiveLData.size(); ++i)
		{
			if (Plots[CurPlotDisplay].ActiveLData[i] == Plots[CurPlotDisplay].GUISelection)
				break;
			if (Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].ActiveLData[i]].IsYActive())
				whichLayer++;
		}
	}
	else
	{
		for (int i = 0; i < Plots[CurPlotDisplay].GUISelection; ++i)
		{
			if (Plots[CurPlotDisplay].LData[i].IsYActive())
				whichLayer++;
		}
	}
	plot->GetLayer(whichLayer)->SetPen(Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen());
	plot->UpdateAll();

	event.Skip();
}

void resultsPanel::UpdateLineStyle(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}

	int selection = stylePen->GetSelection();
	if(selection == 7)
	{
		Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen().SetStyle(wxPENSTYLE_SOLID);
		Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].ConnectData(false);
	}
	else if(selection != 0)
	{

		switch (selection)
		{
		case 1:
			selection = wxPENSTYLE_SOLID;
			break;
		case 2:
			selection = wxPENSTYLE_DOT;
			break;
		case 3:
			selection = wxPENSTYLE_LONG_DASH;
			break;
		case 4:
			selection = wxPENSTYLE_SHORT_DASH;
			break;
		case 5: //these all maintain the value
			selection = wxPENSTYLE_DOT_DASH;
			break;
		case 6:
			selection = wxPENSTYLE_TRANSPARENT;
			break;
		case 0:
		default:
			selection = -1;
			break;
		}

		if (selection == -1)
		{
			event.Skip();
			return;
		}
		else
		{
			Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen().SetStyle(selection);
			Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].ConnectData();
		}
	}
	//now need to find it in the appropriate plot. The X data does not get added as a layer, and the X data is not necessarily first in LData.
	//additionally, the first layers are X & Y axis.
	int whichLayer = plot->CountAllLayers() - plot->CountLayers();;	//-1 because that makes it the equiv of 
	/*	for(int i=0; i<Plots[CurPlotDisplay].GUISelection; ++i)
	{
	if(Plots[CurPlotDisplay].LData[i].IsYActive())
	whichLayer++;
	}
	*/
	if(Plots[CurPlotDisplay].plotType != PDATA_CUSTOM)
	{
		for (unsigned int i = 0; i<Plots[CurPlotDisplay].ActiveLData.size(); ++i)
		{
			if (Plots[CurPlotDisplay].ActiveLData[i] == Plots[CurPlotDisplay].GUISelection)
				break;
			if (Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].ActiveLData[i]].IsYActive())
				whichLayer++;
		}
	}
	else
	{
		for (int i = 0; i < Plots[CurPlotDisplay].GUISelection; ++i)
		{
			if (Plots[CurPlotDisplay].LData[i].IsYActive())
				whichLayer++;
		}
	}

	plot->GetLayer(whichLayer)->SetPen(Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].GetPen());
	plot->GetLayer(whichLayer)->SetContinuity(Plots[CurPlotDisplay].LData[Plots[CurPlotDisplay].GUISelection].IsDataConnected());
	plot->UpdateAll();

	event.Skip();
}

void resultsPanel::ChangeXYPlot(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}
	PlotData* pl = &Plots[CurPlotDisplay];
	int select = pl->GUISelection;
	if (pl->plotType != PDATA_CUSTOM)
	{

		if (pl->LData[select].IsY())
		{
			pl->LData[select].SetAsActiveX();
			if (pl->ActiveX >= 0)
				pl->LData[pl->ActiveX].SetAsX();
			pl->ActiveX = select;
			//now need to go through all the others and disable the active X

			//it was a Y, so now we need to remove it from the Y vector (activeLData)
			for (std::vector<unsigned int>::iterator it = pl->ActiveLData.begin(); it != pl->ActiveLData.end(); ++it)
			{
				if (*it == select)
				{
					pl->ActiveLData.erase(it);	//this should be a small vector, so no biggie on remake
					break;	//get out of loop. it's all invalid now anyway
				}
			}
			setX->SetLabelText(wxT("Set Y"));
		}
		else if (pl->LData[select].IsXActive())
		{  //it was an active X, now try to find a new X
			pl->LData[select].SetAsActiveY();
			pl->ActiveLData.push_back(select);
			pl->ActiveX = -1;	//need to find a new one
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
			{
				if (pl->LData[i].IsX())
				{
					pl->LData[i].SetAsActiveX();
					pl->ActiveX = i;
					break;	//just one...
				}
			}

			setX->SetLabelText(wxT("Set X"));
		}
		else
		{
			//it was just a normal type X - wasn't plotted
			pl->LData[select].SetAsActiveY();
			pl->ActiveLData.push_back(select);
			setX->SetLabelText(wxT("Set X"));	//now a Y, so it can be set to X now
		}
	}
	else
	{
		if (pl->LData[select].IsY())
		{
			//it is becoming an X active. But this means the position in LData must change.
			int correspondingX = -1;
			for (unsigned int i = select - 1; i < pl->LData.size(); i--)
			{
				if (pl->LData[i].IsX())
				{
					correspondingX = i;
					break;
				}
			}
			if (correspondingX >= 0)
			{
				pl->LData[correspondingX].SetAsActiveY();
				pl->LData[select].SetAsActiveX();
				std::iter_swap(pl->LData.begin() + select, pl->LData.begin() + correspondingX);
			}
		}
		else if(pl->LData[select].IsX())
		{
			int correspondingY = -1;
			for (unsigned int i = select+1; i < pl->LData.size(); i++)
			{
				if (pl->LData[i].IsY())
				{
					correspondingY = i;
					break;
				}
			}
			if (correspondingY >= 0)
			{
				pl->LData[correspondingY].SetAsActiveX();
				pl->LData[select].SetAsActiveY();
				std::iter_swap(pl->LData.begin() + select, pl->LData.begin() + correspondingY);
			}

		}
		UpdateDataDropDowns(CurPlotDisplay);
	}
	pl->UpdatePlot(this, true);



	event.Skip();
}
void resultsPanel::RemoveVarPlot(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
	{
		event.Skip();
		return;
	}
	PlotData* pl = &Plots[CurPlotDisplay];
	int select = pl->GUISelection;
	SetShowOutputs(pl->LData[select].GetHeader(), false, &pl->outs);
	if (pl->plotType != PDATA_CUSTOM)
	{

		if (pl->LData[select].IsXActive())
		{
			//try to find a new active x to plot all the data
			pl->ActiveX = -1;
			pl->LData[select].ClearDataType();
			for (unsigned int i = 0; i < pl->LData.size(); ++i)
			{
				if (pl->LData[i].IsX())
				{
					pl->LData[i].SetAsActiveX();
					pl->ActiveX = i;
					break;
				}
			}
			pl->UpdatePlot(this, false);
		}
		else if (pl->LData[select].IsX())
		{
			//this was just an X, and it didn't affect any of the plots (not active X)
			pl->LData[select].ClearDataType(); //nor was it recorded anywhere special
		}
		else //it was an active Y
		{
			pl->LData[select].ClearDataType();
			for (std::vector<unsigned int>::iterator it = pl->ActiveLData.begin(); it != pl->ActiveLData.end(); ++it)
			{
				if (*it == select)
				{
					pl->ActiveLData.erase(it);	//this should be a small vector, so no biggie on remake
					break;	//get out of loop. it's all invalid now anyway
				}
			}
			pl->UpdatePlot(this, false);
		}

		//now delete the legend entry from the Active side and put it on the inactive side
		int labelId = ActivePlotData->FindString(pl->LData[select].GetHeader());
		if (labelId != wxNOT_FOUND)
		{
			ActivePlotData->Delete(labelId);
			if (ActivePlotData->GetCount() > 0)
				ActivePlotData->SetSelection(0);
		}
		InPlotData->Append(pl->LData[select].GetHeader());
	}
	else
	{
		//this is just slightly obnoxious
		if (pl->LData[select].IsXActive())
		{
			pl->LData[select].SetAsX();
			//now have to remove all the following y's from the plot as well!
			for (unsigned int i = select + 1; i < pl->LData.size(); ++i)
			{
				if (pl->LData[i].IsY())
					pl->LData[i].SetAsY();
				else if (pl->LData[i].IsX())
					break;
			}
		}
		else
		{
			pl->LData[select].SetAsY();
		}
		UpdateDataDropDowns(CurPlotDisplay);
		pl->UpdatePlot(this, false);
	}
	//now remove it from the Active dropdown and add it to the inactive dropdown
	//must find it first

	event.Skip();
}

void resultsPanel::AddVarPlot(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())
	{
		event.Skip();
		return;
	}
	PlotData* pl = &Plots[CurPlotDisplay];
	if (pl->plotType != PDATA_CUSTOM)
	{

		wxString newHead = InPlotData->GetStringSelection();
		unsigned int which = pl->FindHeader(newHead);

		if (SetShowOutputs(newHead, true, &pl->outs) == 0 || which == -1)
		{
			event.Skip();
			return;
		}

		//default to Add as Y
		pl->LData[which].SetAsActiveY();
		pl->ActiveLData.push_back(which);

		InPlotData->Delete(InPlotData->GetSelection());
		InPlotData->SetSelection(0);
		ActivePlotData->Append(pl->LData[which].GetHeader());
		ActivePlotData->SetSelection(ActivePlotData->GetCount() - 1);	//does not cause command events!
		pl->GUISelection = which;
		cstmBtn_AddActY->Enable();	//enable the custom plotting active y variable
		if (pl->LData[which].size() == 0)	//need to attempt loading data just for this one
		{
			LoadData(pl->filename, newHead);
		}

		pl->UpdatePlot(this, true);

		VarSelected(event);
	}
	else //it was a custom plot
	{
		wxString newHead = InPlotData->GetStringSelection();
		unsigned int which = pl->FindHeader(newHead);
		if (which != -1)
		{
			if (pl->LData[which].IsX())
				pl->LData[which].SetAsActiveX();
			else
				pl->LData[which].SetAsActiveY();	//default to add as Y
			
			InPlotData->Delete(InPlotData->GetSelection());
			InPlotData->SetSelection(0);
			ActivePlotData->Append(pl->LData[which].GetHeader());
			ActivePlotData->SetSelection(ActivePlotData->GetCount() - 1);
			pl->GUISelection = which;
			
			//no data needs to be loaded. Custom plots fully loaded every time. They're presumed to want to show all the data!
			pl->UpdatePlot(this, true);
			
			VarSelected(event);
		}
	}
	//event.Skip();	event.Skip is called in VarSelected
}

int resultsPanel::SetShowOutputs(wxString header, bool value, ShowOutputs* outs)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (outs == NULL)
		outs = &whichData;


	if (header.IsSameAs(wxT("Point"), false))
		outs->point = value;	//have this be loaded separately be default
	else if (header.IsSameAs(wxT("X"), false))
		outs->x = value;
	else if (header.IsSameAs(wxT("Y"), false))
		outs->y = value;
	else if (header.IsSameAs(wxT("Z"), false))
		outs->z = value;
	else if (header.IsSameAs(wxT("Ef"), false))
		outs->Ef = value;
	else if (header.IsSameAs(wxT("Ef_std_dev"), false))
		outs->Ef = value;
	else if (header.IsSameAs(wxT("Efn"), false))
		outs->Efn = value;
	else if (header.IsSameAs(wxT("Efn_std_dev"), false))
		outs->Efn = value;
	else if (header.IsSameAs(wxT("Efp"), false))
		outs->Efp = value;
	else if (header.IsSameAs(wxT("Efp_std_dev"), false))
		outs->Efp = value;
	else if (header.IsSameAs(wxT("Ec"), false))
		outs->Ec = value;
	else if (header.IsSameAs(wxT("Ec_std_dev"), false))
		outs->Ec = value;
	else if (header.IsSameAs(wxT("Ev"), false))
		outs->Ev = value;
	else if (header.IsSameAs(wxT("Ev_std_dev"), false))
		outs->Ev = value;
	else if (header.IsSameAs(wxT("Psi"), false))
		outs->Psi = value;
	else if (header.IsSameAs(wxT("Psi_std_dev"), false))
		outs->Psi = value;
	else if (header.IsSameAs(wxT("Ec_Carriers"), false))
		outs->electrons = value;
	else if (header.IsSameAs(wxT("EcC_std_dev"), false))
		outs->electrons = value;
	else if (header.IsSameAs(wxT("Ev_Carriers"), false))
		outs->holes = value;
	else if (header.IsSameAs(wxT("EvC_std_dev"), false))
		outs->holes = value;
	else if (header.IsSameAs(wxT("Imp_Charge"), false))
		outs->impCharge = value;
	else if (header.IsSameAs(wxT("Imp_Charge_std_dev"), false))
		outs->impCharge = value;
	else if (header.IsSameAs(wxT("n"), false))
		outs->n = value;
	else if (header.IsSameAs(wxT("n_std_dev"), false))
		outs->n = value;
	else if (header.IsSameAs(wxT("p"), false))
		outs->p = value;
	else if (header.IsSameAs(wxT("p_std_dev"), false))
		outs->p = value;
	//nothing for n*p yet
	else if (header.IsSameAs(wxT("SRH_Rate1"), false))
		outs->srh1 = value;
	else if (header.IsSameAs(wxT("SRH1_std_dev"), false))
		outs->srh1 = value;
	else if (header.IsSameAs(wxT("SRH_Rate2"), false))
		outs->srh2 = value;
	else if (header.IsSameAs(wxT("SRH2_std_dev"), false))
		outs->srh2 = value;
	else if (header.IsSameAs(wxT("SRH_Rate3"), false))
		outs->srh3 = value;
	else if (header.IsSameAs(wxT("SRH3_std_dev"), false))
		outs->srh3 = value;
	else if (header.IsSameAs(wxT("SRH_Rate4"), false))
		outs->srh4 = value;
	else if (header.IsSameAs(wxT("SRH4_std_dev"), false))
		outs->srh4 = value;
	else if (header.IsSameAs(wxT("Scatter"), false))
		outs->scatter = value;
	else if (header.IsSameAs(wxT("Sctr_std_dev"), false))
		outs->scatter = value;
	else if (header.IsSameAs(wxT("Rho"), false))
		outs->rho = value;
	else if (header.IsSameAs(wxT("Rho_std_dev"), false))
		outs->rho = value;
	else if (header.IsSameAs(wxT("Generation_Th"), false))
		outs->genthermal = value;
	else if (header.IsSameAs(wxT("GenTh_std_dev"), false))
		outs->genthermal = value;
	else if (header.IsSameAs(wxT("Recomb_Thermal"), false))
		outs->recthermal = value;
	else if (header.IsSameAs(wxT("RecTh_std_dev"), false))
		outs->recthermal = value;
	else if (header.IsSameAs(wxT("Jn_x"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jn_x_std_dev"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jn_y"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jn_y_std_dev"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jn_z"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jn_z_std_dev"), false))
		outs->Jn = value;
	else if (header.IsSameAs(wxT("Jp_x"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Jp_x_std_dev"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Jp_y"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Jp_y_std_dev"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Jp_z"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Jp_z_std_dev"), false))
		outs->Jp = value;
	else if (header.IsSameAs(wxT("Lt_Emit"), false))
		outs->emitlight = value;

	else if (header.IsSameAs(wxT("LightPower_mW"), false))
		outs->lpow = value;
	else if (header.IsSameAs(wxT("Optical_Gen"), false))
		outs->genlight = value;
	else if (header.IsSameAs(wxT("Stim_Emis_Rec"), false))
		outs->serec = value;
	else if (header.IsSameAs(wxT("A_SRH"), false))
		outs->lsrh = value;
	else if (header.IsSameAs(wxT("SE_SRH"), false))
		outs->sesrh = value;



	else if (header.IsSameAs(wxT("J_ctc_x(1)"), false))
		outs->j1ctc_x = value;
	else if (header.IsSameAs(wxT("J_ctc_x(2)"), false))
		outs->j2ctc_x = value;
	else if (header.IsSameAs(wxT("J_x(1)"), false))
		outs->j1_x = value;
	else if (header.IsSameAs(wxT("J_x(2)"), false))
		outs->j2_x = value;
	else if (header.IsSameAs(wxT("J_avg_x"), false))
		outs->javg_x = value;
	else if (header.IsSameAs(wxT("Jn_ctc_x(1)"), false))
		outs->j1ctc_n_x = value;
	else if (header.IsSameAs(wxT("Jn_ctc_x(2)"), false))
		outs->j2ctc_n_x = value;
	else if (header.IsSameAs(wxT("Jn_x(1)"), false))
		outs->j1_n_x = value;
	else if (header.IsSameAs(wxT("Jn_x(2)"), false))
		outs->j2_n_x = value;
	else if (header.IsSameAs(wxT("Jn_avg_x"), false))
		outs->javg_n_x = value;
	else if (header.IsSameAs(wxT("Jp_ctc_x(1)"), false))
		outs->j1ctc_p_x = value;
	else if (header.IsSameAs(wxT("Jp_ctc_x(2)"), false))
		outs->j2ctc_p_x = value;
	else if (header.IsSameAs(wxT("Jp_x(1)"), false))
		outs->j1_p_x = value;
	else if (header.IsSameAs(wxT("Jp_x(2)"), false))
		outs->j2_p_x = value;
	else if (header.IsSameAs(wxT("Jp_avg_x"), false))
		outs->javg_p_x = value;
	else if (header.IsSameAs(wxT("y=0"), false))
		outs->xaxisZero = value;
	else if (header.IsSameAs(wxT("Wavelength(nm)"), false))
		outs->wavelength = value;
	else if (header.IsSameAs(wxT("Energy(eV)"), false))
		outs->energy = value;
	else if (header.IsSameAs(wxT("Photons"), false))
		outs->photon = value;
	else if (header.IsSameAs(wxT("Photon_Flux"), false))
		outs->pflux = value;
	else if (header.IsSameAs(wxT("Power(mW)"), false))
		outs->power = value;
	else if (header.IsSameAs(wxT("Intensity(mW/cm^2)"), false))
		outs->intensity = value;
	else if (header.IsSameAs(wxT("Min_X"), false))
		outs->minX = value;
	else if (header.IsSameAs(wxT("Pos_X"), false))
		outs->posX = value;
	else if (header.IsSameAs(wxT("Max_X"), false))
		outs->maxX = value;
	else if (header.IsSameAs(wxT("Width_X"), false))
		outs->widthX = value;
	else if (header.IsSameAs(wxT("Min_Y"), false))
		outs->minY = value;
	else if (header.IsSameAs(wxT("Pos_Y"), false))
		outs->posY = value;
	else if (header.IsSameAs(wxT("Max_Y"), false))
		outs->maxY = value;
	else if (header.IsSameAs(wxT("Width_Y"), false))
		outs->widthY = value;
	else if (header.IsSameAs(wxT("Width_Z"), false))
		outs->widthZ = value;
	else if (header.IsSameAs(wxT("AdjP"), false))
		outs->adjP = value;
	else if (header.IsSameAs(wxT("AdjN"), false))
		outs->adjN = value;
	else if (header.IsSameAs(wxT("Adj+X1"), false))
		outs->adjXP1 = value;
	else if (header.IsSameAs(wxT("Adj+X2"), false))
		outs->adjXP2 = value;
	else if (header.IsSameAs(wxT("Adj-X1"), false))
		outs->adjXM1 = value;
	else if (header.IsSameAs(wxT("Adj-X2"), false))
		outs->adjXM2 = value;
	else if (header.IsSameAs(wxT("Adj+Y1"), false))
		outs->adjYP1 = value;
	else if (header.IsSameAs(wxT("Adj+Y2"), false))
		outs->adjYP2 = value;
	else if (header.IsSameAs(wxT("Adj-Y1"), false))
		outs->adjYM1 = value;
	else if (header.IsSameAs(wxT("Adj-Y2"), false))
		outs->adjYM2 = value;
	else
		return(0);	//failed to set a value

	return(1);	//succeeded in setting value
}

int resultsPanel::LoadDataIntoRawOut(wxString str, bool byFile)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	if (byFile)
	{
		wxTextFile* oFile = new wxTextFile(str);
		if (!oFile->Open())
			return(0);

		RawOutput->ChangeValue(wxEmptyString);	//clear out what's currently in it!	
		RawOutput->Hide();	//prevent seeing everything flash across the screen
		for (wxString line = oFile->GetFirstLine(); !oFile->Eof(); line = oFile->GetNextLine())
			*RawOutput << line << "\n";
		oFile->Close();
	}
	else
	{
		RawOutput->Hide();
		RawOutput->ChangeValue(str);	//clear out what's currently in it!
	}
	RawOutput->ShowPosition(0);	//get it back to the top
	RawOutput->Show();

	return(1);
}
void resultsPanel::ToggleRawInput(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (RawPlotButton->GetLabel().IsSameAs(wxT("View Raw Data")))
	{
		unsigned int sliderVal = FileSelectSlider->GetValue();	//which file needs to be opened?
		if (sliderVal < ActiveFileIndices.size())
		{
			int filePos = ActiveFileIndices[sliderVal];
			LoadDataIntoRawOut(FullFilenames[filePos]);
		}
		RawOutput->Show();
		RawPlotButton->SetLabel(wxT("Hide Raw Data"));
	}
	else
	{
		RawOutput->Hide();
		RawPlotButton->SetLabel(wxT("View Raw Data"));
	}
	SetCustomPlotPlacement();
	RVertSizer->Layout();

	event.Skip();
	return;
}

void resultsPanel::SaveCurrentPlot(wxCommandEvent& event)
{
	if (plot == NULL)	//don't do anything!
		return;
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	/*
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Bitmap files (*.bmp)|*.bmp"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	wxSize custSize;
	custSize.Set(1024, 768);
	plot->SaveScreenshot(saveDialog.GetPath(), wxBITMAP_TYPE_BMP, custSize, false);
	*/
	SavePlotImage diag = new SavePlotImage(this);
	if (diag.ShowModal() == wxID_OK)
	{
		if (diag.str_fname.EndsWith(wxT(".bmp")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_BMP, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".jpg")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_JPEG, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".png")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PNG, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".pcx")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PCX, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".pnm")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_PNM, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".tif")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_TIFF, wxSize(diag.wd, diag.ht), false);
		else if (diag.str_fname.EndsWith(wxT(".xpm")))
			plot->SaveScreenshot(diag.str_fname, wxBITMAP_TYPE_XPM, wxSize(diag.wd, diag.ht), false);
		else
		{
			wxMessageDialog diag(this, wxT("Unrecognized extension"), wxT("Invalid file type"));
			diag.ShowModal();
		}
	}

	return;
}

void resultsPanel::TxtChangeScaleY(wxCommandEvent& event)
{
	double scl;
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (ScaleActYText->GetValue().ToDouble(&scl))
	{
		if (unsigned int(CurPlotDisplay) >= Plots.size())//if less than zero, it will certainly be larger
			return;	//can't do anything if a plot isn't selected

		if (Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN || Plots[CurPlotDisplay].plotType == PDATA_RAWONLY)
			return;	//doesn't have a plot that this can be done on

		PlotData* pl = &Plots[CurPlotDisplay];
		int whichCol = pl->GUISelection;

		if (unsigned int(whichCol) >= pl->LData.size())	//if less than zero, it will certainly be larger
			return;

		pl->LData[whichCol].SetScale(scl);

		pl->UpdatePlot(this, false);
	}
	return;
}

void resultsPanel::TxtChangeOffsetY(wxCommandEvent& event)
{
	double off;
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (OffsetActY->GetValue().ToDouble(&off))
	{
		if (unsigned int(CurPlotDisplay) >= Plots.size())//if less than zero, it will certainly be larger
			return;	//can't do anything if a plot isn't selected

		if (Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN || Plots[CurPlotDisplay].plotType == PDATA_RAWONLY)
			return;	//doesn't have a plot that this can be done on

		PlotData* pl = &Plots[CurPlotDisplay];
		int whichCol = pl->GUISelection;

		if (unsigned int(whichCol) >= pl->LData.size())	//if less than zero, it will certainly be larger
			return;

		pl->LData[whichCol].SetOffset(off);

		pl->UpdatePlot(this, false);
	}
	return;
}

void resultsPanel::BtnToggleZeroes(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	double off;
	if (OffsetActY->GetValue().ToDouble(&off))
	{
		if (unsigned int(CurPlotDisplay) >= Plots.size())//if less than zero, it will certainly be larger
			return;	//can't do anything if a plot isn't selected

		if (Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN || Plots[CurPlotDisplay].plotType == PDATA_RAWONLY)
			return;	//doesn't have a plot that this can be done on

		PlotData* pl = &Plots[CurPlotDisplay];
		int whichCol = pl->GUISelection;

		if (unsigned int(whichCol) >= pl->LData.size())	//if less than zero, it will certainly be larger
			return;

		pl->LData[whichCol].ToggleZeros();	//flip it

		if (pl->LData[whichCol].IsIgnoreZeros())
			Btn_ToggleZeros->SetLabel(wxT("Show Zeroes"));
		else
			Btn_ToggleZeros->SetLabel(wxT("Hide Zeroes"));


		pl->UpdatePlot(this, false);
	}
	return;
}

void resultsPanel::BtnShowCustomPlot(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	/*
	if (PlotCustomizeSizer->GetItem(cstm_plot) == NULL)
		PlotCustomizeSizer->Detach(cstm_plot);

	if (RVertSizer->GetItem(PlotCustomizeSizer))
		int test = 0;
	if (PlotCustomizeSizer->GetItem(plot))
		int test = 0;
	if (RVertSizer->GetItem(plot))
		int test = 0;
	if (RVertSizer->GetItem(plot, true))
		int test = 0;
	*/
	//first check if there is a custom plot yet
	if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Build Custom Plot")))
	{
		PlotCustomizeSizer->Show(MergeManipSizer);
		RVertSizer->Show(PlotCustomizeSizer);

		//long nested if
		if (plot)	//is plot loaded? Hide it if it's supposed to
		{
			if (PlotCustomizeSizer->GetItem(plot) != NULL) //is it attached to the sizer?
			{
				if (unsigned int(CurPlotDisplay) < Plots.size()) //avoid bad index reference
				{
					if (Plots[CurPlotDisplay].plot == plot) //is the current display match the plot
					{
						PlotCustomizeSizer->Show(plot); //then go ahead and show it!
					}
					else
						plot->Hide();
				}
				else
					plot->Hide();
			}
			else if (MergeManipSizer->GetItem(plot) != NULL)
			{
				if (unsigned int(CurPlotDisplay) < Plots.size()) //avoid bad index reference
				{
					if (Plots[CurPlotDisplay].plot == plot) //is the current display match the plot
					{
						MergeManipSizer->Show(plot); //then go ahead and show it!
					}
					else
						plot->Hide();
				}
				else
					plot->Hide();
			}
			else
				plot->Hide();
		}
		Btn_PlotCustom->SetLabel(wxT("Hide Custom Plot"));
		SetCustomPlotPlacement();	//look at what's currently out there, create the plot(if needed) and put it in the correct position if not done so yet
		cstm_plot->Show();
		RVertSizer->Layout();
	}
	else if (Btn_PlotCustom->GetLabel().IsSameAs(wxT("Hide Custom Plot")))
	{
		if (PlotCustomizeSizer->GetItem(plot) == NULL)	//there was no plot previously shown
		{
			RVertSizer->Hide(PlotCustomizeSizer);
//			RVertSizer->Layout();
		}
		else
		{
			if (PlotCustomizeSizer->GetItem(cstm_plot))
				PlotCustomizeSizer->Hide(cstm_plot);
			PlotCustomizeSizer->Hide(MergeManipSizer);
			if (PlotCustomizeSizer->GetItem(plot))
			{
				if (PlotCustomizeSizer->IsShown(plot) == false)
				{
					RVertSizer->Hide(PlotCustomizeSizer);
				}
			}
//			PlotCustomizeSizer->Layout();
		}
		Btn_PlotCustom->SetLabel(wxT("Build Custom Plot"));
		RVertSizer->Layout();
	}

	event.Skip();
	return;
}

void resultsPanel::SetCustomPlotPlacement(int pos)	//default value is it just figures it out...
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	//make sure there is a custom plot to work with!
	if (cstm_plot == NULL)
	{
		cstm_plot = new mpWindow(this, wxID_ANY);
		cstm_plot->SetMargins(0, 0, 5, 4);	//large enough to see ticks, but no numbers
		cstm_plot->Hide();
		cstm_plot->SetToolTip(wxT("Preview of the plot being generated - Save plot to do proper formatting."));
	}
	if (cstm_xaxis == NULL)
	{
		cstm_xaxis = new mpScaleX(wxEmptyString, mpALIGN_BOTTOM, true, mpX_NORMAL);
		cstm_xaxis->SetLabelFormat(wxT("%.1E"));
		cstm_xaxis->SetDrawOutsideMargins(false);	//if true, it draws axis based on a constant seaparation. If false, goes based on set margins
		cstm_plot->AddLayer(cstm_xaxis);
	}
	if (cstm_yaxis == NULL)
	{
		cstm_yaxis = new mpScaleY(wxEmptyString, mpALIGN_LEFT, true);
		cstm_yaxis->SetLabelFormat(wxT("%g"));
		cstm_yaxis->SetDrawOutsideMargins(false);	//if true, it draws axis based on a constant seaparation. If false, goes based on set margins
		cstm_plot->AddLayer(cstm_yaxis);
	}

	if (pos == CPLOT_RIGHT) //it needs to go in the plotcustomize sizer
	{
		if (MergeManipSizer->GetItem(cstm_plot) != NULL)
			MergeManipSizer->Detach(cstm_plot);

		if (PlotCustomizeSizer->GetItem(cstm_plot) == NULL)
			PlotCustomizeSizer->Add(cstm_plot, 1, wxALL | wxEXPAND | wxALIGN_CENTER, 1);
		return;
	}
	else if (pos == CPLOT_MANIP)
	{
		if (PlotCustomizeSizer->GetItem(cstm_plot) != NULL)
			PlotCustomizeSizer->Detach(cstm_plot);
		if (MergeManipSizer->GetItem(cstm_plot) == NULL)
			MergeManipSizer->Add(cstm_plot, 1, wxALL | wxEXPAND | wxALIGN_CENTER, 1);

		return;
	}
	else if (RawOutput->IsShown())//if RAW DATA is showing, it must ALWAYS be in plot customize sizer
	{
		if (MergeManipSizer->GetItem(cstm_plot) != NULL)
			MergeManipSizer->Detach(cstm_plot);

		if (PlotCustomizeSizer->GetItem(cstm_plot) == NULL)
			PlotCustomizeSizer->Add(cstm_plot, 1, wxALL | wxEXPAND | wxALIGN_CENTER, 1);
		return;
	}
	else if (plot != NULL)
	{
		if (plot->IsShown()) //at this point, there is no raw data showing, so we want this to go in mergemanip if a plot is showing!
		{
			if (PlotCustomizeSizer->GetItem(cstm_plot) != NULL)
				PlotCustomizeSizer->Detach(cstm_plot);
			if (MergeManipSizer->GetItem(cstm_plot) == NULL)
				MergeManipSizer->Add(cstm_plot, 1, wxALL | wxEXPAND | wxALIGN_CENTER, 1);
			return;

		}
	}
	else
	{

		//if there is no plot showing, have it be separate again in PlotCustomizeSizer (default solution
		if (MergeManipSizer->GetItem(cstm_plot) != NULL)
			MergeManipSizer->Detach(cstm_plot);

		if (PlotCustomizeSizer->GetItem(cstm_plot) == NULL)
			PlotCustomizeSizer->Add(cstm_plot, 1, wxALL | wxEXPAND | wxALIGN_CENTER, 1);
	}
	return;
}

void resultsPanel::UpdateInactiveToolTip(wxCommandEvent& event)
{
	if (InPlotData == nullptr)	//last thing initialized in constructor
		return;
	InPlotData->SetToolTip(InPlotData->GetStringSelection());
	return;
}

void resultsPanel::CPlot_AddActive(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())	//make sure there is an active plot
		return;

	if (Plots[CurPlotDisplay].plotType == PDATA_RAWONLY || Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN)
		return;

	if (Plots[CurPlotDisplay].plot != plot)	//it is not showing the same selection
		return;

	if (plot == NULL)
		return;

	if (unsigned int(Plots[CurPlotDisplay].GUISelection) >= Plots[CurPlotDisplay].LData.size())
		return;

	LineData newX, newY;
	PlotData* src = &Plots[CurPlotDisplay];
	int yCol = src->GUISelection;
	int xCol = src->ActiveX;

	if (unsigned int(xCol) >= src->LData.size())
	{
		//then this is probably the JV curve (or another custom plot)
		if (src->plotType == PDATA_CUSTOM)
		{
			for (unsigned int col = yCol; col < src->LData.size(); --col)
			{
				if (src->LData[col].IsX())
				{
					xCol = col;
					break;
				}
			}
		}
		else if (src->plotType == PDATA_JV)
		{
			if (src->LData[yCol].GetHeader().Contains(wxT("Jn")))
			{
				xCol = src->FindHeader(wxT("Voltage_N"));	//sets to -1 if not found
			}
			else if (src->LData[yCol].GetHeader().Contains(wxT("Jp")))
			{
				xCol = src->FindHeader(wxT("Voltage_P"));	//sets to -1 if not found
			}
			else
				xCol = src->FindHeader(wxT("Voltage"));	//sets to -1 if not found
		}
	}
	//it still couldn't find it
	if (xCol < 0)
		return;	//no x data, get out

	if (src->LData[xCol].size() != src->LData[yCol].size())	//the data won't line up
		return;

	//now let's copy the data into the proper plot data structure
	cstm_PlotData.ActiveX = -1;	//it doesn't use this setup here


	newX.GetPen().SetColour(0, 0, 0);
	newX.GetPen().SetStyle(wxPENSTYLE_SOLID);
	newX.GetPen().SetWidth(1);



	newX.SetAsActiveX();	//default inactive
	newX.SetHeader(src->LData[xCol].GetHeader() + wxT(" ") + src->filename);
	newX.SetLegend(src->LData[xCol].GetLegend());
	newX.SetScale(src->LData[xCol].GetScale());
	newX.SetOffset(src->LData[xCol].GetOffset());
	newX.IgnoreZeros(src->LData[xCol].IsIgnoreZeros());
	newX.ConnectData(src->LData[xCol].IsDataConnected());
	newX.ClearData();

	newY.GetPen().SetColour(0, 0, 128);
	newY.GetPen().SetStyle(wxPENSTYLE_LONG_DASH);
	newY.GetPen().SetWidth(3);
	
	newY.SetAsActiveY();	//default inactive
	newY.SetHeader(src->LData[yCol].GetHeader() + wxT(" ") + src->filename);
	newY.SetLegend(src->LData[yCol].GetLegend());
	newY.SetScale(src->LData[yCol].GetScale());
	newY.SetOffset(src->LData[yCol].GetOffset());
	newY.IgnoreZeros(src->LData[yCol].IsIgnoreZeros());
	newY.ConnectData(src->LData[yCol].IsDataConnected());
	newY.ClearData();

	int trgxCol = cstm_PlotData.LData.size();
	cstm_PlotData.LData.push_back(newX);
	int trgyCol = cstm_PlotData.LData.size();
//	cstm_PlotData.ActiveLData.push_back(trgyCol);	//everything always active in a custom plot generation!
	cstm_PlotData.LData.push_back(newY);

	cstm_PlotData.LData[trgxCol].GetData().resize(src->LData[yCol].size());
	cstm_PlotData.LData[trgyCol].GetData().resize(src->LData[yCol].size());

	for (unsigned int i = 0; i < src->LData[yCol].size(); ++i)
	{
		cstm_PlotData.LData[trgxCol].SetData(i, src->LData[xCol].GetData(i));
		cstm_PlotData.LData[trgyCol].SetData(i, src->LData[yCol].GetData(i));
	}

	int which = cstm_Select->Append(cstm_PlotData.LData[trgyCol].GetHeader());
	cstm_Select->Select(which);
	cstm_Select->SetToolTip(cstm_Select->GetStringSelection());
	cstmBtn_Remove->Enable();

	for (unsigned int i = 0; i < cstm_PlotData.LData.size()-2; ++i)	//make all the others normal
	{
		cstm_PlotData.LData[i].GetPen().SetColour(0, 0, 0);
		cstm_PlotData.LData[i].GetPen().SetWidth(1);
		cstm_PlotData.LData[i].GetPen().SetStyle(wxPENSTYLE_SOLID);
	}

	cstm_PlotData.UpdateCPlotPreview(this);

	return;
}
void resultsPanel::CPlot_AddVisible(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if (unsigned int(CurPlotDisplay) >= Plots.size())	//make sure there is an active plot
		return;

	if (Plots[CurPlotDisplay].plotType == PDATA_RAWONLY || Plots[CurPlotDisplay].plotType == PDATA_UNKNOWN) //make sure it is a good type of plot to add
		return;

	if (Plots[CurPlotDisplay].plot != plot)	//it is not showing the same selection
		return;

	if (plot == NULL)
		return;

	LineData newX, newY;
	PlotData* src = &Plots[CurPlotDisplay];
	
	newX.GetPen().SetColour(0, 0, 0);
	newX.GetPen().SetStyle(wxPENSTYLE_SOLID);
	newX.GetPen().SetWidth(1);
	newX.SetAsActiveX();	//default inactive
	newX.ClearData();

	newY.GetPen().SetColour(0, 0, 0);
	newY.GetPen().SetStyle(wxPENSTYLE_SOLID);
	newY.GetPen().SetWidth(1);
	newY.SetAsActiveY();	//default inactive
	newY.ClearData();

	if(src->plotType != PDATA_CUSTOM)
	{
		for (unsigned int i = 0; i < src->ActiveLData.size(); ++i)
		{
			unsigned int yCol = src->ActiveLData[i];	//these should all be type PL_TYPE_Y_ACTIVE

			if (yCol >= src->LData.size())	//make sure the data is valid
				continue;

			int xCol = src->ActiveX;	//this may not work for JV files or custom plots.

			if (unsigned int(xCol) >= src->LData.size())
			{
				//then this is probably the JV curve (or another custom plot)
				if (src->plotType == PDATA_CUSTOM)
				{
					for (unsigned int col = yCol; col < src->LData.size(); ++col)
					{
						if (src->LData[col].IsX())
						{
							xCol = col;
							break;
						}
					}
				}
				else if (src->plotType == PDATA_JV)
				{
					if (src->LData[yCol].GetHeader().Contains(wxT("Jn")))
					{
						xCol = src->FindHeader(wxT("Voltage_N"));	//sets to -1 if not found
					}
					else if (src->LData[yCol].GetHeader().Contains(wxT("Jp")))
					{
						xCol = src->FindHeader(wxT("Voltage_P"));	//sets to -1 if not found
					}
					else
						xCol = src->FindHeader(wxT("Voltage"));	//sets to -1 if not found
				}
			}

			if (xCol < 0)
				continue;

			newX.SetHeader(src->LData[xCol].GetHeader() + wxT(" ") + src->filename);
			newX.SetLegend(src->LData[xCol].GetLegend());
			newX.SetScale(src->LData[xCol].GetScale());
			newX.SetOffset(src->LData[xCol].GetOffset());
			newX.IgnoreZeros(src->LData[xCol].IsIgnoreZeros());
			newX.ConnectData(src->LData[xCol].IsDataConnected());

			newY.SetHeader(src->LData[yCol].GetHeader() + wxT(" ") + src->filename);
			newY.SetLegend(src->LData[yCol].GetLegend());
			newY.SetScale(src->LData[yCol].GetScale());
			newY.SetOffset(src->LData[yCol].GetOffset());
			newY.IgnoreZeros(src->LData[yCol].IsIgnoreZeros());
			newY.ConnectData(src->LData[yCol].IsDataConnected());
			newX.ClearData();
			newY.ClearData();

			int trgxCol = cstm_PlotData.LData.size();
			cstm_PlotData.LData.push_back(newX);
			int trgyCol = cstm_PlotData.LData.size();
			//	cstm_PlotData.ActiveLData.push_back(trgyCol);	//everything always active in a custom plot generation!
			cstm_PlotData.LData.push_back(newY);

			cstm_PlotData.LData[trgxCol].GetData().resize(src->LData[yCol].size());
			cstm_PlotData.LData[trgyCol].GetData().resize(src->LData[yCol].size());

			for (unsigned int i = 0; i < src->LData[yCol].size(); ++i)
			{
				cstm_PlotData.LData[trgxCol].SetData(i, src->LData[xCol].GetData(i));
				cstm_PlotData.LData[trgyCol].SetData(i, src->LData[yCol].GetData(i));
			}

			cstm_Select->Append(cstm_PlotData.LData[trgyCol].GetHeader());
		}
	}
	else //plot type is custom!
	{
		unsigned int curX = -1;
		bool XDone=false;
		for (unsigned int i = 0; i < src->LData.size(); ++i)
		{
			if(src->LData[i].IsX())
			{
				curX = i;
				XDone=false;
				continue;
			}
			if(curX >= src->LData.size())
				continue;

			if(src->LData[i].IsYActive())
			{
				newY.SetHeader(src->LData[i].GetHeader());
				newY.SetLegend(src->LData[i].GetLegend());
				newY.SetScale(src->LData[i].GetScale());
				newY.SetOffset(src->LData[i].GetOffset());
				newY.IgnoreZeros(src->LData[i].IsIgnoreZeros());
				newY.ConnectData(src->LData[i].IsDataConnected());
				
				newY.ClearData();

				if(!XDone)
				{
					newX.SetHeader(src->LData[curX].GetHeader());
					newX.SetLegend(src->LData[curX].GetLegend());
					newX.SetScale(src->LData[curX].GetScale());
					newX.SetOffset(src->LData[curX].GetOffset());
					newX.IgnoreZeros(src->LData[curX].IsIgnoreZeros());
					newX.ConnectData(src->LData[curX].IsDataConnected());

					int trgxCol = cstm_PlotData.LData.size();
					newX.ClearData();
					cstm_PlotData.LData.push_back(newX);
					cstm_PlotData.LData.back().GetData().resize(src->LData[curX].size());
					for (unsigned int j = 0; j < src->LData[curX].size(); ++j)
						cstm_PlotData.LData[trgxCol].SetData(j, src->LData[curX].GetData(j));
					

					XDone = true;
				}
				int trgyCol = cstm_PlotData.LData.size();
				//	cstm_PlotData.ActiveLData.push_back(trgyCol);	//everything always active in a custom plot generation!
				cstm_PlotData.LData.push_back(newY);
				cstm_PlotData.LData[trgyCol].GetData().resize(src->LData[i].size());

				for (unsigned int j = 0; j < src->LData[i].size(); ++j)
					cstm_PlotData.LData[trgyCol].SetData(j, src->LData[i].GetData(j));

				cstm_Select->Append(cstm_PlotData.LData[trgyCol].GetHeader());
			}

		}
	}

	int which = cstm_Select->GetCount() - 1;
	if (which > 0)
	{
		cstm_Select->Select(which);
		cstm_Select->SetToolTip(cstm_Select->GetStringSelection());
		cstmBtn_Remove->Enable();
	}
	else
	{
		cstm_Select->Select(0);
		cstm_Select->SetToolTip(wxT("Select which variable you would like to highlight on the plot, and potentially remove"));
		cstmBtn_Remove->Disable();
	}
	

	for (unsigned int i = 0; i < cstm_PlotData.LData.size() - 2; ++i)	//make all the others normal
	{
		cstm_PlotData.LData[i].GetPen().SetColour(0, 0, 0);
		cstm_PlotData.LData[i].GetPen().SetWidth(1);
		cstm_PlotData.LData[i].GetPen().SetStyle(wxPENSTYLE_SOLID);
	}
	unsigned int last = cstm_PlotData.LData.size() - 1;
	cstm_PlotData.LData[last].GetPen().SetColour(0, 0, 128);
	cstm_PlotData.LData[last].GetPen().SetWidth(3);
	cstm_PlotData.LData[last].GetPen().SetStyle(wxPENSTYLE_LONG_DASH);

	cstm_PlotData.UpdateCPlotPreview(this);

	return;
}

int resultsPanel::CPlot_RemoveDuplicateX(void)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	int numRemoved = 0;
	//first, get rid of all the excess data in front that is not an X. These are not associated with anything.
	if (cstm_PlotData.LData.size() > 0)
	{
		while ((cstm_PlotData.LData[0].IsX()) == 0)	//the first one does not have an X data type
		{
			numRemoved++;
			cstm_PlotData.LData.erase(cstm_PlotData.LData.begin());
			if (cstm_PlotData.LData.size() == 0)
				break;
		}
	}
	//now get rid of all the prior neighboring X values, as it is an X that doesn't get applied to any plot
	for (unsigned int col = cstm_PlotData.LData.size() - 2; col < cstm_PlotData.LData.size(); --col)	//col will jump to big when goes < 0
	{
		unsigned int safe = col + 1;
		if ((cstm_PlotData.LData[safe].IsX()) && (cstm_PlotData.LData[col].IsX()))
		{
			//then get rid of the previously unused x
			cstm_PlotData.LData.erase(cstm_PlotData.LData.begin() + col);
		}
	}
	//now get rid of all the X values tagged on the end
	for (unsigned int col = cstm_PlotData.LData.size() - 1; col < cstm_PlotData.LData.size(); --col)	//col will jump to big when goes < 0
	{
		if ((cstm_PlotData.LData[col].IsX()))
			cstm_PlotData.LData.erase(cstm_PlotData.LData.begin() + col);
		else
			break;
	}
	//maintain the X order. Get rid of all future X's that match. Inserts occur BEFORE the specified element. [First, Last)
	std::vector<LineData>::iterator start = cstm_PlotData.LData.begin();	//just to keep things somewhat simpler!
	std::vector<LineData>::iterator curX = cstm_PlotData.LData.begin();
	std::vector<LineData>::iterator nextX = cstm_PlotData.LData.end();	//the X value following it (insert location) (note end is controlled undefined)
	std::vector<LineData>::iterator cmpX = cstm_PlotData.LData.end();	//the x currently being compared
	std::vector<LineData>::iterator CmpYStart = cstm_PlotData.LData.end();	//the y value after the x being compared
	std::vector<LineData>::iterator CmpYEndP1 = cstm_PlotData.LData.end();	//one AFTER the y value of that current segment

	std::vector<LineData> buffer;	//avoid iterators being invalidated and stuff

	unsigned int whichXCur = 0;
	unsigned int whichXCmp = 1;
	unsigned int nexX = 0;

	for (unsigned int srcCol = 0; srcCol < cstm_PlotData.LData.size(); ++srcCol)
	{
		if ((cstm_PlotData.LData[srcCol].IsX()) == 0)	//this isn't one worth checking into
			continue;

		curX = start + srcCol;

		for (nexX = srcCol + 1; nexX < cstm_PlotData.LData.size(); ++nexX)
		{
			if (cstm_PlotData.LData[nexX].IsX())
			{
				nextX = start + nexX;
				whichXCmp = nexX;
				break;
			}
		}
		if (nexX == cstm_PlotData.LData.size())	//then it didn't find a next X
			continue;
		for (unsigned int compX = whichXCmp; compX < cstm_PlotData.LData.size(); ++compX)
		{
			if (cstm_PlotData.LData[compX].IsX())
			{
				cmpX = start + compX;
				//it found an X, now it's time to see if they are a match
				if (curX->size() == cmpX->size())	//this could save a ton of processing time
				{
					bool match = true;
					for (unsigned int i = 0; i < curX->size(); ++i)
					{
						if (curX->GetData(i) != cmpX->GetData(i))
						{
							match = false;
							break;
						}
					}
					if (match)
					{
						CmpYStart = cmpX + 1;
						if (CmpYStart != cstm_PlotData.LData.end())
						{
							CmpYEndP1 = cstm_PlotData.LData.end();	//default value
							for (unsigned int fndEY = compX+1; fndEY < cstm_PlotData.LData.size(); ++fndEY)
							{
								if(cstm_PlotData.LData[fndEY].IsX()) //this will be one beyond the last Y(good) or it won't be found
								{  //which will mean the end of the vector
									CmpYEndP1 = start + fndEY;
									break;
								}
							}
							buffer.clear();	//make sure no baggage data
							buffer.insert(buffer.end(), CmpYStart, CmpYEndP1); //get the data into the buffer
							cstm_PlotData.LData.erase(cmpX, CmpYEndP1); //erase the X value and all associated Y values .erase: [start,end)
							//now all the iterators in cstm_PlotData are invalidated
							//this erased stuff in the back, so the front counters are still valid
							nextX = cstm_PlotData.LData.begin() + nexX;	//this is the insert location. Will always be valid for a given initial X as an insert location, even if it changes into a Y value
							cstm_PlotData.LData.insert(nextX, buffer.begin(), buffer.end());	//put the copied y values in
							buffer.clear(); //no need to hold on to the data anymore
							start = cstm_PlotData.LData.begin();	//reinit the start iterator as it was corrupted again upon insert.
							--compX;	//if it deletes an X without any Y (somehow), it might skip over a needed test
						}
						else //no y values associated with matching X values
						{
							cstm_PlotData.LData.erase(cmpX);	//erase the x coord - there are no y values associated with it
							start = cstm_PlotData.LData.begin();	//reinit the iterator as it was corrupted again
							--compX;	//if it deletes an X without any Y (somehow), it might skip over a needed test
						}


					}	//end a matching X coord was found
				}
			}
		}
	}




	return(numRemoved);
}

void resultsPanel::CPlot_Save(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	CPlot_RemoveDuplicateX();	//get rid of excess data to be saved
	

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString,wxT("SAAPD Custom Plots (*.plot)|*.plot"),wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	wxString fName = saveDialog.GetPath();


	wxTextFile* plotFile = new wxTextFile(fName);
	if (!plotFile->Create())	//if it fails to create the file
	if (!plotFile->Open())		//it might already exist? so open the file
		return;	//it just completely failed...

	wxString lineBuffer;
	for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
	{
		lineBuffer = wxEmptyString;	//start empty
		if (cstm_PlotData.LData[i].IsX())
			lineBuffer << "X ";
		else
			lineBuffer << "Y ";

		if (cstm_PlotData.LData[i].IsIgnoreZeros())
			lineBuffer << "1 ";
		else
			lineBuffer << "0 ";

		lineBuffer << cstm_PlotData.LData[i].GetOffset() << " " << cstm_PlotData.LData[i].GetScale() << " ";

		lineBuffer << cstm_PlotData.LData[i].size() << " ";	//just to make sure the correct number of data points are loaded and it doesn't mix with the header
		
		for (unsigned int j = 0; j < cstm_PlotData.LData[i].size(); ++j)
			lineBuffer << cstm_PlotData.LData[i].GetData(j) << " ";

		lineBuffer << cstm_PlotData.LData[i].GetHeader();

		plotFile->AddLine(lineBuffer);

	}

	plotFile->Write();
	plotFile->Close();
	
	if (fListfName != wxEmptyString)	//add it to the flist so when this set of data is loaded in the future it will do so proprly
	{
		wxTextFile* oFile = new wxTextFile(fListfName);
		if (oFile->Open())
		{
			bool writetoFile = true;
			for (wxString line = oFile->GetFirstLine(); !oFile->Eof(); line = oFile->GetNextLine())
			{
				if (line.IsSameAs(fName))
				{
					writetoFile = false;
					break;
				}
			}
			if (writetoFile)
			{
				oFile->AddLine(fName);	//it will find this plot in the future
				oFile->Write();	//save to disk
			}
			oFile->Close();
		}
	}

	

	CPlot_Clear(event);	//clear everything out
	BtnShowCustomPlot(event);	//Pretend they hit the hide button
//	FullFilenames.push_back(fName);
//	AbbrFNames.push_back(fName);	//taken care of in LoadData
	LoadData(fName);
	return;
}
void resultsPanel::CPplot_Select(wxCommandEvent & event)
{
	int which = cstm_Select->GetSelection();
	if (which == 0)
	{
		cstmBtn_Remove->Disable();
		cstm_Select->SetToolTip(wxT("Select which variable you would like to highlight on the plot, and potentially remove"));
		//now adjust all the pens
		for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
		{
			cstm_PlotData.LData[i].GetPen().SetColour(0, 0, 0);
			cstm_PlotData.LData[i].GetPen().SetStyle(wxPENSTYLE_SOLID);
			cstm_PlotData.LData[i].GetPen().SetWidth(1);
		}
	}
	else //it is not the default entry
	{
		cstmBtn_Remove->Enable();
		wxString str = cstm_Select->GetStringSelection();
		cstm_Select->SetToolTip(str);
		//str contains the header
		which = cstm_PlotData.FindHeader(str);

		if (which != -1)	//valid
		{
			//now adjust all the pens
			for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
			{
				cstm_PlotData.LData[i].GetPen().SetColour(0, 0, 0);
				cstm_PlotData.LData[i].GetPen().SetStyle(wxPENSTYLE_SOLID);
				cstm_PlotData.LData[i].GetPen().SetWidth(1);
			}

			cstm_PlotData.LData[which].GetPen().SetColour(0, 0, 128);
			cstm_PlotData.LData[which].GetPen().SetStyle(wxPENSTYLE_LONG_DASH);
			cstm_PlotData.LData[which].GetPen().SetWidth(3);
		}
	}

	cstm_PlotData.UpdateCPlotPreview(this);
	return;
}
void resultsPanel::CPlot_Remove(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	wxString header = cstm_Select->GetStringSelection();
	int which = cstm_PlotData.FindHeader(header);
	if (which < 0)
		return;

	cstm_PlotData.LData.erase(cstm_PlotData.LData.begin() + which);
	for (unsigned int col = cstm_PlotData.LData.size()-2; col < cstm_PlotData.LData.size(); --col)	//col will jump to big when goes < 0
	{
		unsigned int safe = col+1;
		if ((cstm_PlotData.LData[safe].IsX()) && (cstm_PlotData.LData[col].IsX()))
		{
			//then get rid of the previously unused x
			cstm_PlotData.LData.erase(cstm_PlotData.LData.begin() + col);
		}
	}
	cstm_Select->Delete(cstm_Select->GetSelection());
	cstm_Select->Select(0);
	//reset the pens
	for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
	{
		cstm_PlotData.LData[i].GetPen().SetColour(0, 0, 0);
		cstm_PlotData.LData[i].GetPen().SetStyle(wxPENSTYLE_SOLID);
		cstm_PlotData.LData[i].GetPen().SetWidth(1);
	}

	cstmBtn_Remove->Disable();
	cstm_PlotData.UpdateCPlotPreview(this);
	cstm_plot->UpdateAll();
	

	return;
}

void resultsPanel::CPlot_Clear(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	cstmBtn_Remove->Disable();
	for (unsigned int i = 0; i < cstm_PlotData.LData.size(); ++i)
	{
		cstm_PlotData.LData[i].ClearData();
	}
	cstm_PlotData.LData.clear();
	cstm_PlotData.UpdateCPlotPreview(this);

	cstm_Select->Select(0);
	while (cstm_Select->GetCount()>1)
		cstm_Select->Delete(1);

	return;
}

void resultsPanel::PlotTitle(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	plot->GetLayer(TITLE_LAYER)->SetName(txt_PlotName->GetValue());
	plot->UpdateAll();
	return;
}
void resultsPanel::PlotTitlePos(wxScrollEvent & event)
{
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	int x,y;
	x = Slider_titleX->GetValue();
	y = Slider_titleY->GetValue();

	((mpText *)plot->GetLayer(TITLE_LAYER))->SetOffset(x,y);
	plot->UpdateAll();
	return;

}
void resultsPanel::PlotXAxisTitle(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	plot->GetLayer(XAXIS_LAYER)->SetName(txt_XAxisName->GetValue());
	plot->UpdateAll();
	return;
}
void resultsPanel::PlotYAxisTitle(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	plot->GetLayer(YAXIS_LAYER)->SetName(txt_YAxisName->GetValue());
	plot->UpdateAll();
	return;
}
void resultsPanel::PlotShowLegends(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	Plots[CurPlotDisplay].ShowLegend = Chk_legend->GetValue();

	plot->GetLayer(LEGEND_LAYER)->SetVisible(Plots[CurPlotDisplay].ShowLegend);
	plot->UpdateAll();
	return;
	
}
void resultsPanel::PlotShowLabels(wxCommandEvent & event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	if(unsigned int(CurPlotDisplay) >= Plots.size())
		return;

	if(plot != Plots[CurPlotDisplay].plot)
		return;

	if(!plot)
		return;

	Plots[CurPlotDisplay].ShowLabels = Chk_labels->GetValue();
	bool show = Plots[CurPlotDisplay].ShowLabels;	//cuz i'm laszy typing

	for(unsigned int lyr = FIRST_GRAPH_LAYER; lyr < plot->CountAllLayers(); lyr++)
		plot->GetLayer(lyr)->ShowName(show);

	plot->UpdateAll();
	return;
}

void resultsPanel::ClearFilteredFiles(wxCommandEvent& event)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return;
	unsigned int sz = ActiveFileIndices.size();
	for (unsigned int i=sz-1; i<sz; --i)
	{
		RemoveFileFromList(AbbrFNames[ActiveFileIndices[i]]);
	}
	FileListBox->Clear();
	Btn_ClearFilteredFiles->Disable();
	ActiveFileIndices.clear();
	FileSelectSlider->SetMax(0);
	FileSelectSlider->SetValue(0);
	SwapPlotExactIndex(-1);	//
	if (RawPlotButton->GetLabel().IsSameAs(wxT("Hide Raw Data")))
	{
		RawOutput->Hide();
		RawOutput->ChangeValue(wxEmptyString);
		RawPlotButton->SetLabel(wxT("View Raw Data"));
		SetCustomPlotPlacement();
		RVertSizer->Layout();
	}

	return;
}

int resultsPanel::RemoveFileFromList(wxString fname)
{
	if (CustomBtnSzr == nullptr)	//last thing initialized in constructor
		return 0;
	unsigned int which;
	for(which = 0; which < AbbrFNames.size(); ++which)
	{
		if(fname.IsSameAs(AbbrFNames[which]))
			break;
	}
	if (which == AbbrFNames.size())	//it didn't find it
	{
		for (which = 0; which < FullFilenames.size(); ++which)
		{
			if (fname.IsSameAs(FullFilenames[which]))
				break;
		}
		if (which == FullFilenames.size())
			return(0);
	}
	
	//it found it.

	AbbrFNames.erase(AbbrFNames.begin() + which);
	FullFilenames.erase(FullFilenames.begin() + which);

	unsigned int restorePlot = -1;
	
	if (plot)
	{
		if (Plots[which].plot == plot)	//it is removing the last active plot
		{
			if (MergeManipSizer->GetItem(plot) != NULL)
				MergeManipSizer->Detach(plot);

			if (PlotCustomizeSizer->GetItem(plot) != NULL)
				PlotCustomizeSizer->Detach(plot);

			plot = NULL;	//only set plot to NULL if it gets detached
		}
		else
		{
			bool success = false;
			for (unsigned int i = 0; i < Plots.size(); ++i)
			{
				if (Plots[i].plot == plot)
				{
					restorePlot = (which < i) ? i-1 : i;	//a plot before this is being removed, so the position minus 1. If plot after this is removed, stick with the position
					success = true;
					break;
				}
			}
			if (!success)
			{
				if (MergeManipSizer->GetItem(plot) != NULL)
					MergeManipSizer->Detach(plot);

				if (PlotCustomizeSizer->GetItem(plot) != NULL)
					PlotCustomizeSizer->Detach(plot);

				plot = NULL;
			}
		}
	}
	
	if (CurPlotDisplay < 0 || CurPlotDisplay == which)
	{
		CurPlotDisplay = -1;	//the current display isn't any plot!
		RawOutput->SetValue(wxEmptyString);
	}
	else if(unsigned int(CurPlotDisplay) > which) //the previous if makes this statement safe (< 0)
	{
		CurPlotDisplay--;
	}
	for (unsigned int j = 0; j<Plots[which].LData.size(); ++j)
	{
		Plots[which].LData[j].ClearData();
	}
	Plots[which].LData.clear();
	Plots[which].ActiveLData.clear();

	delete Plots[which].plot;
	Plots[which].plot = NULL;
	Plots[which].xaxis = NULL;
	Plots[which].yaxis = NULL;
	Plots[which].legend =NULL;
	Plots[which].title = NULL;
	Plots[which].rawData = wxEmptyString;

	Plots.erase(Plots.begin() + which);

	for (unsigned int i = 0; i < ActiveFileIndices.size(); i++)
	{
		if (unsigned int(ActiveFileIndices[i]) > which)
			(ActiveFileIndices[i])--;
	}
	//now the plot pointer is invalidated
	if (restorePlot < Plots.size())
	{
		plot = Plots[restorePlot].plot;	//make the pointer good again
	}
		
	

	return(1);
}


resultsPanel::~resultsPanel()
{
	for (unsigned int j = 0; j<data.size(); ++j)
		data[j].clear();
	data.clear();
	/*
	for(unsigned int i=0; i<Plots.size(); ++i)
	{
	for(unsigned int j=0; j<Plots[i].data.size(); ++j)
	Plots[i].data[j].clear();
	Plots[i].ClearData();
	Plots[i].LData.clear();
	delete Plots[i].plot;
	}
	*/
	ClearPlots();	//kill everything - delete plots in each index. Had to take out destructor code, as that also got called on Plots.push_back()
//	Plots.clear();	//apparently this cals the destructor, which does the above delete code

	delete LoadDataButton;
	delete savePlotButton;
	//	delete xaxis;
	//	delete yaxis;

	//	delete plot;

}



BEGIN_EVENT_TABLE(resultsPanel, wxPanel)
EVT_BUTTON(LOAD_RESULTS, resultsPanel::LoadOutputFiles)
EVT_BUTTON(SAVE_PLOT_IMAGE, resultsPanel::SaveCurrentPlot)
//EVT_COMMAND(UPDATE_FILE_FILTER, wxEVT_COMMAND_TEXT_UPDATED, resultsPanel::FFiles)
EVT_TEXT(UPDATE_FILE_FILTER, resultsPanel::FFiles)
EVT_COMMAND_SCROLL_THUMBTRACK(FLIST_SLIDER_CHANGE, resultsPanel::SliderChangeNoUpdate)
EVT_COMMAND_SCROLL_THUMBRELEASE(FLIST_SLIDER_CHANGE, resultsPanel::SliderChangeUpdate)
EVT_COMMAND_SCROLL_THUMBTRACK(SCRL_TITLE, resultsPanel::PlotTitlePos)
EVT_LISTBOX_DCLICK(FLIST_DCLICK, resultsPanel::ListBoxSelect)
EVT_COMBOBOX(SELECT_VAR_CHANGE, resultsPanel::VarSelected)
EVT_COMBOBOX(INACTIVE_DDOWN, resultsPanel::UpdateInactiveToolTip)
EVT_TEXT(COLOR_BOX, resultsPanel::UpdateLineColor)
EVT_TEXT(SCALE_Y_TXT, resultsPanel::TxtChangeScaleY)
EVT_TEXT(OFFSET_Y_TXT, resultsPanel::TxtChangeOffsetY)
EVT_TEXT(TXT_XAXIS, resultsPanel::PlotXAxisTitle)
EVT_TEXT(TXT_YAXIS, resultsPanel::PlotYAxisTitle)
EVT_TEXT(TXT_PLOTNAME, resultsPanel::PlotTitle)
EVT_BUTTON(CLR_FILTFILES, resultsPanel::ClearFilteredFiles)
EVT_BUTTON(CHANGE_XY_PLOT, resultsPanel::ChangeXYPlot)
EVT_BUTTON(REMOVE_VAR_PLOT, resultsPanel::RemoveVarPlot)
EVT_BUTTON(ADD_VAR_PLOT, resultsPanel::AddVarPlot)
EVT_BUTTON(TOGGLE_RAW_OUT, resultsPanel::ToggleRawInput)
EVT_BUTTON(NORMALIZE_BTN, resultsPanel::NormalizeActY)
EVT_BUTTON(SHOW_CPLOT_BTN, resultsPanel::BtnShowCustomPlot)
EVT_BUTTON(TOGGLE_ZEROES_BTN, resultsPanel::BtnToggleZeroes)
EVT_BUTTON(CSTM_ADD_ACTIVE_BTN, resultsPanel::CPlot_AddActive)
EVT_BUTTON(CSTM_ADD_VISIBLE_BTN, resultsPanel::CPlot_AddVisible)
EVT_BUTTON(CSTM_REMOVE, resultsPanel::CPlot_Remove)
EVT_BUTTON(CSTM_SAVE_BTN, resultsPanel::CPlot_Save)
EVT_BUTTON(CSTM_CLEAR, resultsPanel::CPlot_Clear)
EVT_COMBOBOX(UPDATE_P_WIDTH, resultsPanel::UpdateLineWidth)
EVT_COMBOBOX(UPDATE_P_STYLE, resultsPanel::UpdateLineStyle)
EVT_COMBOBOX(CSTM_SELECTDDDOWN, resultsPanel::CPplot_Select)
EVT_TEXT(UPDATE_LINE_LEGEND, resultsPanel::UpdateLegendText)
EVT_TEXT(UPDATE_PLOT_BOUNDS, resultsPanel::UpdatePlotBounds)
EVT_MENU(mpID_CENTER, resultsPanel::UpdatePlotBoundTxtBoxesEvt)
EVT_MENU(mpID_FIT, resultsPanel::UpdatePlotBoundTxtBoxesEvt)
EVT_MENU(mpID_ZOOM_IN, resultsPanel::UpdatePlotBoundTxtBoxesEvt)
EVT_MENU(mpID_ZOOM_OUT, resultsPanel::UpdatePlotBoundTxtBoxesEvt)
EVT_CHECKBOX(CHK_LABELS, resultsPanel::PlotShowLabels)
EVT_CHECKBOX(CHK_LEGEND, resultsPanel::PlotShowLegends)
EVT_RIGHT_UP(resultsPanel::MouseReleaseUpdate)
EVT_LEFT_UP(resultsPanel::MouseReleaseUpdate)

END_EVENT_TABLE()

PlotData::PlotData()
{
	outs.Ef = outs.Efn = outs.Efp = outs.Ec = outs.Ev = outs.Psi = outs.electrons = outs.holes = outs.impCharge
		= outs.n = outs.p = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
		= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho = outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox = outs.qe
		= outs.x = outs.y = outs.z = outs.point = false;
	outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x = outs.javg_x = outs.j1ctc_n_x = outs.j2ctc_n_x
		= outs.j1_n_x = outs.j2_n_x = outs.javg_n_x = outs.j1ctc_p_x = outs.j2ctc_p_x
		= outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
	outs.volt = outs.volt_n = outs.volt_p = false;
	outs.xaxisZero = false;
	outs.wavelength = outs.energy = outs.photon = outs.pflux = outs.power = outs.intensity = false;
	
	outs.minX=outs.posX=outs.maxX=outs.widthX=outs.minY=outs.posY=outs.maxY=outs.adjP=outs.adjN
		= outs.adjXP1=outs.adjXP2=outs.adjXM1=outs.adjXM2
		= outs.adjYP1=outs.adjYP2=outs.adjYM1=outs.adjYM2 = false;

	plot = NULL;
	xaxis = NULL;
	yaxis = NULL;
	title = NULL;
	legend = NULL;
	GUISelection = -1;
	ActiveX = -1;
	plotType = PDATA_UNKNOWN;
	filename = wxEmptyString;
	LData.clear();
	ActiveLData.clear();
	ShowLegend = false;
	ShowLabels = true;

}

PlotData::~PlotData()
{
	;
/*	for (unsigned int i = 0; i<LData.size(); ++i)
		LData[i].ClearData();
	LData.clear();
	ActiveLData.clear();
	delete plot;
	plot = NULL;
	xaxis = NULL;	//deleted when plot is deleted
	yaxis = NULL;	//deleted when plot is deleted
*/
}

int PlotData::FindHeader(wxString header)
{
	for (unsigned int i = 0; i < LData.size(); ++i)
	{
		if (LData[i].GetHeader().IsSameAs(header))
			return(i);
	}
	return(-1);
}

int PlotData::UpdatePlot(resultsPanel* parent, bool FitPlotToData)
{
	int ret = 0;
	switch (plotType)
	{
	case PDATA_BD:
		ret = UpdatePlotBD(parent, FitPlotToData);
		break;
	case PDATA_JV:
		ret=UpdatePlotJV(parent, FitPlotToData);
		break;
	case PDATA_EMISSIONS:
	case PDATA_OPTICS:
		ret=UpdatePlotQE(parent, FitPlotToData);
		break;
	case PDATA_MESH:
		ret=UpdatePlotMESH(parent, FitPlotToData);
		break;
	case PDATA_CUSTOM:
		ret = UpdatePlotCust(parent, FitPlotToData);
		break;
	case PDATA_UNKNOWN:
	default:
		;
	}
	parent->UpdatePlotBoundTxtBoxes();
	return(ret);
}

int PlotData::UpdatePlotJV(resultsPanel* parent, bool FitPlotToData)
{
	if (plot == NULL)
	{
		outs.Ec = outs.Efn = outs.Efp = outs.Ev = outs.x
			= outs.Ef = outs.Psi = outs.electrons = outs.holes = outs.impCharge
			= outs.n = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
			= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho
			= outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox
			= outs.qe = outs.point = outs.y = outs.z = false;
		outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x
			= outs.j1ctc_n_x = outs.j2ctc_n_x = outs.j1_n_x = outs.j2_n_x = outs.javg_n_x
			= outs.j1ctc_p_x = outs.j2ctc_p_x = outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
		outs.volt_n = outs.volt_p = outs.volt = false;
		outs.wavelength = outs.energy = outs.photon = outs.pflux = outs.power = outs.intensity = false;
		outs.javg_x = true;
		outs.xaxisZero = true;
		plot = new mpWindow(parent, wxID_ANY);
		plot->Hide();
	}
	if (xaxis == NULL)
	{
		xaxis = new mpScaleX(wxT("Voltage (V)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
		xaxis->SetLabelFormat(wxT("%g"));
		plot->AddLayer(xaxis);
	}
	if (yaxis == NULL)
	{
		yaxis = new mpScaleY(wxT("Current Density (A cm\u207B\u00B2)"), mpALIGN_LEFT, true);
		yaxis->SetLabelFormat(wxT("%.1E"));
		plot->AddLayer(yaxis);
	}
	if(title == NULL)
	{
		title = new mpText(wxT("Current-Voltage"), 50, 2);
		plot->AddLayer(title);
	}
	if(legend == NULL)
	{
		legend = new mpInfoLegend();
		plot->AddLayer(legend);
		plot->GetLayer(LEGEND_LAYER)->SetVisible(ShowLegend);
	}
	while (plot->CountLayers() > ActiveLData.size())
		plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

	while (plot->CountLayers() < ActiveLData.size())
	{
		plot->AddLayer(new mpFXYVector(), false);
		plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
	}

	//we don't actually care about ActiveX for this kind of plot! We have to find it in the data as multiple x's don't really work here
	int x_Tot = -1;
	int x_n = -1;
	int x_p = -1;
	int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();
	//figure out which columns are the x values
	for (unsigned int i = 0; i<LData.size(); ++i)
	{
		if (LData[i].GetHeader().IsSameAs(wxT("Voltage"), false))
			x_Tot = i;
		if (LData[i].GetHeader().IsSameAs(wxT("Voltage_N"), false))
			x_n = i;
		if (LData[i].GetHeader().IsSameAs(wxT("Voltage_P"), false))
			x_p = i;
	}
	for (unsigned int i = 0; i<ActiveLData.size(); i++)
	{
		//first look at the header and determine which ones fit
		unsigned int col = ActiveLData[i];
		if (col >= LData.size())
			continue;	//prevent overstepping index bounds
		if ((LData[col].IsYActive()) == 0)	//only do active Y values
			continue;
		int curX = x_Tot;
		if (LData[col].GetHeader().Contains(wxT("n_")))
			curX = x_n;
		else if (LData[col].GetHeader().Contains(wxT("p_")))
			curX = x_p;

		if (curX == -1)	//prevent x coord from being bad if it didn't get loaded
			continue;
		if (LData[col].IsIgnoreZeros() || LData[col].GetScale() != 1.0 || LData[col].GetOffset() != 0.0)
		{
			std::vector<double> newXVals;
			std::vector<double> newYVals;
			double tmp;
			double scl = LData[col].GetScale();
			double offset = LData[col].GetOffset();
			bool ignore = LData[col].IsIgnoreZeros();
			for (unsigned int j = 0; j < LData[col].size(); ++j)
			{
				tmp = (offset + LData[col].GetData(j)) * scl;
				if (tmp != offset || !ignore)
				{
					newYVals.push_back(tmp);
					newXVals.push_back(LData[curX].GetData(j));
				}
			}
			((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(newXVals, newYVals);
			newXVals.clear();
			newYVals.clear();
		}
		else
			((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(LData[curX].GetData(), LData[col].GetData());

		plot->GetLayer(fGraphLayer + i)->SetPen(LData[col].GetPen());
		plot->GetLayer(fGraphLayer + i)->SetDrawOutsideMargins(false);
		plot->GetLayer(fGraphLayer + i)->SetName(LData[col].GetLegend());
		plot->GetLayer(fGraphLayer + i)->SetContinuity(LData[col].IsDataConnected());
		plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
		((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(mpALIGN_TOP);


	}
	if(FitPlotToData)
		plot->Fit(0.05);
	plot->UpdateAll();	//need to load the x/y_min/max values internally

	return(0);
}


int PlotData::UpdatePlotQE(resultsPanel* parent, bool FitPlotToData)
{
	if (plot == NULL)
	{
		outs.Ec = outs.Efn = outs.Efp = outs.Ev = outs.x = false;
		outs.Ef = outs.Psi = outs.electrons = outs.holes = outs.impCharge
			= outs.n = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
			= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho
			= outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox
			= outs.qe = outs.point = outs.y = outs.z = false;
		outs.volt = outs.volt_n = outs.volt_p = outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x = outs.javg_x
			= outs.j1ctc_n_x = outs.j2ctc_n_x = outs.j1_n_x = outs.j2_n_x = outs.javg_n_x
			= outs.j1ctc_p_x = outs.j2ctc_p_x = outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
		outs.xaxisZero = false;
		outs.wavelength = outs.photon = outs.power = outs.intensity = false;
		outs.minX = outs.posX = outs.maxX = outs.widthX = outs.minY = outs.posY = outs.maxY = outs.adjP = outs.adjN
			= outs.adjXP1 = outs.adjXP2 = outs.adjXM1 = outs.adjXM2
			= outs.adjYP1 = outs.adjYP2 = outs.adjYM1 = outs.adjYM2 = false;
		outs.pflux = outs.energy = true;
		plot = new mpWindow(parent, wxID_ANY);

		plot->Hide();	//don't want it to show up too early!
	}
	if (xaxis == NULL)
	{
		xaxis = new mpScaleX(wxT("Energy (eV)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
		xaxis->SetLabelFormat(wxT("%.3E"));
		plot->AddLayer(xaxis);
	}
	if (yaxis == NULL)
	{
		yaxis = new mpScaleY(wxT("Arbitrary Units"), mpALIGN_LEFT, true);
		yaxis->SetLabelFormat(wxT("%g"));
		plot->AddLayer(yaxis);
	}
	if(title == NULL)
	{
		title = new mpText(wxT("Optical Data"), 50, 2);
		plot->AddLayer(title);
	}
	if(legend == NULL)
	{
		legend = new mpInfoLegend();
		plot->AddLayer(legend);
		plot->GetLayer(LEGEND_LAYER)->SetVisible(ShowLegend);
	}

	if (ActiveX >= 0)	//there's an X coordinate to plot against
	{
		while (plot->CountLayers() > ActiveLData.size())
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

		while (plot->CountLayers() < ActiveLData.size())
		{
			plot->AddLayer(new mpFXYVector(), false);
			plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
		}
		int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();
		for (unsigned int i = 0; i < ActiveLData.size(); i++)
		{
			unsigned int col = ActiveLData[i];
			if (LData[col].IsIgnoreZeros() || LData[col].GetScale() != 1.0 || LData[col].GetOffset() != 0.0)
			{
				std::vector<double> newXVals;
				std::vector<double> newYVals;
				double tmp;
				double scl = LData[col].GetScale();
				double offset = LData[col].GetOffset();
				bool ignore = LData[col].IsIgnoreZeros();
				for (unsigned int j = 0; j < LData[col].size(); ++j)
				{
					tmp = (offset + LData[col].GetData(j)) * scl;
					if (tmp != offset || !ignore)
					{
						newYVals.push_back(tmp);
						newXVals.push_back(LData[ActiveX].GetData(j));
					}
				}
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(newXVals, newYVals);
				newXVals.clear();
				newYVals.clear();
			}
			else
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(LData[ActiveX].GetData(), LData[col].GetData());
			plot->GetLayer(fGraphLayer + i)->SetPen(LData[col].GetPen());
			plot->GetLayer(fGraphLayer + i)->SetDrawOutsideMargins(false);
			plot->GetLayer(fGraphLayer + i)->SetName(LData[col].GetLegend());
			plot->GetLayer(fGraphLayer + i)->SetContinuity(LData[col].IsDataConnected());
			plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
			((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(mpALIGN_TOP);
		}
		if(FitPlotToData)
			plot->Fit(0.05);
		//		plot->AddLayer(new mpInfoLegend, false);
		//		plot->AddLayer(new mpInfoCoords, false);

	}
	else //no X coordinate - delete all the plot layers (not the axes)
	{
		while (plot->CountLayers() > 0)
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);
	}
	plot->UpdateAll();	//need to load the x/y_min/max values internally

	return(0);
}

int PlotData::UpdatePlotBD(resultsPanel* parent, bool FitPlotToData)
{
	if (plot == NULL)
	{
		outs.Ec = outs.Efn = outs.Efp = outs.Ev = outs.x = true;
		outs.Ef = outs.Psi = outs.electrons = outs.holes = outs.impCharge
			= outs.n = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
			= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho
			= outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox
			= outs.qe = outs.point = outs.y = outs.z = false;
		outs.volt = outs.volt_n = outs.volt_p = outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x = outs.javg_x
			= outs.j1ctc_n_x = outs.j2ctc_n_x = outs.j1_n_x = outs.j2_n_x = outs.javg_n_x
			= outs.j1ctc_p_x = outs.j2ctc_p_x = outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
		outs.xaxisZero = false;
		outs.minX = outs.posX = outs.maxX = outs.widthX = outs.minY = outs.posY = outs.maxY = outs.adjP = outs.adjN
			= outs.adjXP1 = outs.adjXP2 = outs.adjXM1 = outs.adjXM2
			= outs.adjYP1 = outs.adjYP2 = outs.adjYM1 = outs.adjYM2 = false;
		outs.wavelength = outs.energy = outs.photon = outs.pflux = outs.power = outs.intensity = false;
		plot = new mpWindow(parent, wxID_ANY);

		plot->Hide();	//don't want it to show up too early!
	}
	if (xaxis == NULL)
	{
		xaxis = new mpScaleX(wxT("X (cm)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
		xaxis->SetLabelFormat(wxT("%.1E"));
		plot->AddLayer(xaxis);
	}
	if (yaxis == NULL)
	{
		yaxis = new mpScaleY(wxT("Energy (eV)"), mpALIGN_LEFT, true);
		yaxis->SetLabelFormat(wxT("%g"));
		plot->AddLayer(yaxis);
	}
	if(title == NULL)
	{
		title = new mpText(wxT("Band Diagram"), 50, 2);
		plot->AddLayer(title);
	}
	if(legend == NULL)
	{
		legend = new mpInfoLegend();
		plot->AddLayer(legend);
		plot->GetLayer(LEGEND_LAYER)->SetVisible(ShowLegend);
	}
	int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();

	if (ActiveX >= 0)	//there's an X coordinate to plot against
	{
		while (plot->CountLayers() > ActiveLData.size())
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

		while (plot->CountLayers() < ActiveLData.size())
		{
			plot->AddLayer(new mpFXYVector(), false);
			plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
		}

		for (unsigned int i = 0; i < ActiveLData.size(); i++)
		{
			unsigned int col = ActiveLData[i];
			
			if (LData[col].IsIgnoreZeros() || LData[col].GetScale() != 1.0 || LData[col].GetOffset() != 0.0)
			{
				std::vector<double> newXVals;
				std::vector<double> newYVals;
				double tmp;
				double scl = LData[col].GetScale();
				double offset = LData[col].GetOffset();
				bool ignore = LData[col].IsIgnoreZeros();
				for (unsigned int j = 0; j < LData[col].size(); ++j)
				{
					tmp = (offset + LData[col].GetData(j)) * scl;
					if (tmp != offset || !ignore)
					{
						newYVals.push_back(tmp);
						newXVals.push_back(LData[ActiveX].GetData(j));
					}
				}
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(newXVals, newYVals);
				newXVals.clear();
				newYVals.clear();
			}
			else
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(LData[ActiveX].GetData(), LData[col].GetData());
			plot->GetLayer(fGraphLayer + i)->SetPen(LData[col].GetPen());
			plot->GetLayer(fGraphLayer + i)->SetDrawOutsideMargins(false);
			plot->GetLayer(fGraphLayer + i)->SetName(LData[col].GetLegend());
			plot->GetLayer(fGraphLayer + i)->SetContinuity(LData[col].IsDataConnected());
			plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
			if (LData[col].GetHeader().IsSameAs(wxT("Ev")) || LData[col].GetHeader().IsSameAs(wxT("Efp")))
				((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(mpALIGN_BOTTOM);
			else
				((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(mpALIGN_TOP);
		}
		/*
		double Xmin, Xmax, Ymin, Ymax;
		find_maxmin_value(LData[ActiveX].data, Xmax, Xmin);
		find_maxmin_value_nested(this, Ymax, Ymin);
		double dif = Xmax-Xmin;
		dif = dif*0.02;
		Xmin -= dif;
		Xmax += dif;
		dif = Ymax - Ymin;
		dif = dif*0.02;
		Ymax += dif;
		Ymin -= dif;
		*/
		if(FitPlotToData)
			plot->Fit(0.05);
		//		plot->AddLayer(new mpInfoLegend, false);
		//		plot->AddLayer(new mpInfoCoords, false);

	}
	else //no X coordinate - delete all the plot layers (not the axes)
	{
		while (plot->CountLayers() > 0)
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);
	}
	plot->UpdateAll();	//need to load the x/y_min/max values internally

	return(0);
}

int PlotData::UpdatePlotMESH(resultsPanel* parent, bool FitPlotToData)
{
	if (plot == NULL)
	{
		outs.Ec = outs.Efn = outs.Efp = outs.Ev = outs.x = false;
		outs.Ef = outs.Psi = outs.electrons = outs.holes = outs.impCharge
			= outs.n = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
			= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho
			= outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox
			= outs.qe = outs.point = outs.y = outs.z = false;
		outs.volt = outs.volt_n = outs.volt_p = outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x = outs.javg_x
			= outs.j1ctc_n_x = outs.j2ctc_n_x = outs.j1_n_x = outs.j2_n_x = outs.javg_n_x
			= outs.j1ctc_p_x = outs.j2ctc_p_x = outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
		outs.xaxisZero = false;
		outs.minX = outs.maxX = outs.widthX = outs.minY = outs.posY = outs.maxY = outs.adjP = outs.adjN
			= outs.adjXP1 = outs.adjXP2 = outs.adjXM1 = outs.adjXM2
			= outs.adjYP1 = outs.adjYP2 = outs.adjYM1 = outs.adjYM2 = false;
		outs.point = outs.posX = true;
		outs.wavelength = outs.energy = outs.photon = outs.pflux = outs.power = outs.intensity = false;
		plot = new mpWindow(parent, wxID_ANY);

		plot->Hide();	//don't want it to show up too early!
	}
	if (xaxis == NULL)
	{
		xaxis = new mpScaleX(wxT("Point"), mpALIGN_BOTTOM, true, mpX_NORMAL);
		xaxis->SetLabelFormat(wxT("%.0f"));
		plot->AddLayer(xaxis);
	}
	if (yaxis == NULL)
	{
		yaxis = new mpScaleY(wxT("Length (cm)"), mpALIGN_LEFT, true);
		yaxis->SetLabelFormat(wxT("%.1E"));
		plot->AddLayer(yaxis);
	}
	if(title == NULL)
	{
		title = new mpText(wxT("Mesh"), 50, 2);
		plot->AddLayer(title);
	}
	if(legend == NULL)
	{
		legend = new mpInfoLegend();
		plot->AddLayer(legend);
		plot->GetLayer(LEGEND_LAYER)->SetVisible(ShowLegend);
	}

	if (ActiveX >= 0)	//there's an X coordinate to plot against
	{
		while (plot->CountLayers() > ActiveLData.size())
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

		while (plot->CountLayers() < ActiveLData.size())
		{
			plot->AddLayer(new mpFXYVector(), false);
			plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
		}
		int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();
		for (unsigned int i = 0; i < ActiveLData.size(); i++)
		{
			unsigned int col = ActiveLData[i];
			if (LData[col].IsIgnoreZeros() || LData[col].GetScale() != 1.0 || LData[col].GetOffset() != 0.0)
			{
				std::vector<double> newXVals;
				std::vector<double> newYVals;
				double tmp;
				double scl = LData[col].GetScale();
				double offset = LData[col].GetOffset();
				bool ignore = LData[col].IsIgnoreZeros();
				for (unsigned int j = 0; j < LData[col].size(); ++j)
				{
					tmp = (offset + LData[col].GetData(j)) * scl;
					if (tmp != offset || !ignore)
					{
						newYVals.push_back(tmp);
						newXVals.push_back(LData[ActiveX].GetData(j));
					}
				}
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(newXVals, newYVals);
				newXVals.clear();
				newYVals.clear();
			}
			else
				((mpFXYVector *)(plot->GetLayer(fGraphLayer + i)))->SetData(LData[ActiveX].GetData(), LData[col].GetData());
			plot->GetLayer(fGraphLayer + i)->SetPen(LData[col].GetPen());
			plot->GetLayer(fGraphLayer + i)->SetDrawOutsideMargins(false);
			plot->GetLayer(fGraphLayer + i)->SetName(LData[col].GetLegend());
			plot->GetLayer(fGraphLayer + i)->SetContinuity(LData[col].IsDataConnected());
			plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
			((mpFXYVector *)plot->GetLayer(fGraphLayer + i))->SetLabelAlign(mpALIGN_TOP);
		}
		if(FitPlotToData)
			plot->Fit(0.05);
		//		plot->AddLayer(new mpInfoLegend, false);
		//		plot->AddLayer(new mpInfoCoords, false);

	}
	else //no X coordinate - delete all the plot layers (not the axes)
	{
		while (plot->CountLayers() > 0)
			plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);
	}
	plot->UpdateAll();	//need to load the x/y_min/max values internally

	return(0);
}

int PlotData::UpdatePlotCust(resultsPanel* parent, bool FitPlotToData)
{
	
	if (plot == NULL)
	{
		outs.Ec = outs.Efn = outs.Efp = outs.Ev = outs.x = false;
		outs.Ef = outs.Psi = outs.electrons = outs.holes = outs.impCharge
			= outs.n = outs.deflight = outs.condlight = outs.genlight = outs.genthermal = outs.recthermal
			= outs.srh1 = outs.srh2 = outs.srh3 = outs.srh4 = outs.rho
			= outs.Jn = outs.Jp = outs.scatter = outs.stddev = outs.stderrBox
			= outs.qe = outs.point = outs.y = outs.z = false;
		outs.volt = outs.volt_n = outs.volt_p = outs.j1ctc_x = outs.j2ctc_x = outs.j1_x = outs.j2_x = outs.javg_x
			= outs.j1ctc_n_x = outs.j2ctc_n_x = outs.j1_n_x = outs.j2_n_x = outs.javg_n_x
			= outs.j1ctc_p_x = outs.j2ctc_p_x = outs.j1_p_x = outs.j2_p_x = outs.javg_p_x = false;
		outs.xaxisZero = false;
		outs.minX = outs.maxX = outs.widthX = outs.minY = outs.posY = outs.maxY = outs.adjP = outs.adjN
			= outs.adjXP1 = outs.adjXP2 = outs.adjXM1 = outs.adjXM2
			= outs.adjYP1 = outs.adjYP2 = outs.adjYM1 = outs.adjYM2 = false;
		outs.point = outs.posX = false;
		outs.wavelength = outs.energy = outs.photon = outs.pflux = outs.power = outs.intensity = false;
		plot = new mpWindow(parent, wxID_ANY);

		plot->Hide();	//don't want it to show up too early!
	}
	if (xaxis == NULL)
	{
		xaxis = new mpScaleX(wxT("Unknown"), mpALIGN_BOTTOM, true, mpX_NORMAL);
		xaxis->SetLabelFormat(wxT("%.3E"));
		plot->AddLayer(xaxis);
	}
	if (yaxis == NULL)
	{
		yaxis = new mpScaleY(wxT("Unknown"), mpALIGN_LEFT, true);
		yaxis->SetLabelFormat(wxT("%.1E"));
		plot->AddLayer(yaxis);
	}
	if(title == NULL)
	{
		title = new mpText(wxT("Custom Title"), 50, 2);
		plot->AddLayer(title);
	}
	if(legend == NULL)
	{
		legend = new mpInfoLegend();
		plot->AddLayer(legend);
		plot->GetLayer(LEGEND_LAYER)->SetVisible(ShowLegend);
	}

	unsigned int actSz = 0;
	bool foundX = false;
	for (unsigned int i = 0; i<LData.size(); ++i)
	{
		if (!foundX && (LData[i].IsX()))
			foundX = true;
		else if (foundX && (LData[i].IsYActive()))
			actSz++;
	}

	while (plot->CountLayers() > actSz)
		plot->DelLayer(plot->GetLayer(plot->CountAllLayers() - 1), true, false);

	while (plot->CountLayers() < actSz)
	{
		plot->AddLayer(new mpFXYVector(), false);
		plot->GetLayer(plot->CountAllLayers() - 1)->SetContinuity(true);
	}

	//now start drawing everything
	int curX = -1;
	int ctrAdd = 0;
	int fGraphLayer = plot->CountAllLayers() - plot->CountLayers();
	for (unsigned int col = 0; col < LData.size(); ++col)
	{
		if (LData[col].IsX())
		{
			curX = col;
			continue;
		}

		if (curX < 0)	//don't do anything until an X is found - X should always be the first type in PlotData though.
			continue;

		if ((LData[col].IsYActive()) == 0)
			continue;

		if (LData[col].IsIgnoreZeros() || LData[col].GetScale() != 1.0 || LData[col].GetOffset() != 0.0)
		{
			std::vector<double> newXVals;
			std::vector<double> newYVals;
			double tmp;
			double scl = LData[col].GetScale();
			double offset = LData[col].GetOffset();
			bool ignore = LData[col].IsIgnoreZeros();
			for (unsigned int j = 0; j < LData[col].size(); ++j)
			{
				tmp = (offset + LData[col].GetData(j)) * scl;
				if (tmp != offset || !ignore)
				{
					newYVals.push_back(tmp);
					newXVals.push_back(LData[curX].GetData(j));
				}
			}
			((mpFXYVector *)(plot->GetLayer(fGraphLayer + ctrAdd)))->SetData(newXVals, newYVals);
			newXVals.clear();
			newYVals.clear();
		}
		else
			((mpFXYVector *)(plot->GetLayer(fGraphLayer + ctrAdd)))->SetData(LData[curX].GetData(), LData[col].GetData());
		plot->GetLayer(fGraphLayer + ctrAdd)->SetPen(LData[col].GetPen());
		plot->GetLayer(fGraphLayer + ctrAdd)->SetDrawOutsideMargins(false);
		plot->GetLayer(fGraphLayer + ctrAdd)->SetName(LData[col].GetLegend());
		plot->GetLayer(fGraphLayer + ctrAdd)->SetContinuity(LData[col].IsDataConnected());
		plot->SetMargins(0, 0, Y_BORDER_SEPARATION, X_BORDER_SEPARATION);
		((mpFXYVector *)plot->GetLayer(fGraphLayer + ctrAdd))->SetLabelAlign(mpALIGN_TOP);

		ctrAdd++;
	}

	if(FitPlotToData)
		plot->Fit(0.05);

	plot->UpdateAll();
	return(0);
}

int PlotData::UpdateCPlotPreview(resultsPanel* parent)
{
	parent->SetCustomPlotPlacement();	//make sure all the custom plot stuff exists

	unsigned int actSz = 0;
	bool foundX = false;
	for (unsigned int i = 0; i<parent->cstm_PlotData.LData.size(); ++i)
	{
		if (!foundX && (parent->cstm_PlotData.LData[i].IsX()))
			foundX = true;
		else if (foundX && (parent->cstm_PlotData.LData[i].IsY()))
			actSz++;
	}

	while (parent->cstm_plot->CountLayers() > actSz)
		parent->cstm_plot->DelLayer(parent->cstm_plot->GetLayer(parent->cstm_plot->CountAllLayers() - 1), true, false);

	while (parent->cstm_plot->CountLayers() < actSz)
	{
		parent->cstm_plot->AddLayer(new mpFXYVector(), false);
		parent->cstm_plot->GetLayer(parent->cstm_plot->CountAllLayers() - 1)->SetContinuity(true);
	}

	//now start drawing everything
	int curX = -1;
	int ctrAdd = 0;
	int fGraphLayer = parent->cstm_plot->CountAllLayers() - parent->cstm_plot->CountLayers();
	for (unsigned int col = 0; col < parent->cstm_PlotData.LData.size(); ++col)
	{
		if (parent->cstm_PlotData.LData[col].IsX())
		{
			curX = col;
			continue;
		}
			
		if (curX < 0)	//don't do anything until an X is found - X should always be the first type in PlotData though.
			continue;

		if (parent->cstm_PlotData.LData[col].IsIgnoreZeros() || parent->cstm_PlotData.LData[col].GetScale() != 1.0 || parent->cstm_PlotData.LData[col].GetOffset() != 0.0)
		{
			std::vector<double> newXVals;
			std::vector<double> newYVals;
			double tmp;
			double scl = parent->cstm_PlotData.LData[col].GetScale();
			double offset = parent->cstm_PlotData.LData[col].GetOffset();
			bool ignore = parent->cstm_PlotData.LData[col].IsIgnoreZeros();
			for (unsigned int j = 0; j < parent->cstm_PlotData.LData[col].size(); ++j)
			{
				tmp = (offset + parent->cstm_PlotData.LData[col].GetData(j)) * scl;
				if (tmp != offset || !ignore)
				{
					newYVals.push_back(tmp);
					newXVals.push_back(parent->cstm_PlotData.LData[curX].GetData(j));
				}
			}
			((mpFXYVector *)(parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd)))->SetData(newXVals, newYVals);
			newXVals.clear();
			newYVals.clear();
		}
		else
			((mpFXYVector *)(parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd)))->SetData(parent->cstm_PlotData.LData[curX].GetData(), parent->cstm_PlotData.LData[col].GetData());
		parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd)->SetPen(parent->cstm_PlotData.LData[col].GetPen());
		parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd)->SetDrawOutsideMargins(false);
		parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd)->SetName(parent->cstm_PlotData.LData[col].GetLegend());
		parent->cstm_plot->SetMargins(0, 0, 5, 4);
		((mpFXYVector *)parent->cstm_plot->GetLayer(fGraphLayer + ctrAdd))->SetLabelAlign(mpALIGN_TOP);

		ctrAdd++;
	}

	parent->cstm_plot->Fit(0.05);
	parent->cstm_plot->UpdateAll();

	return(0);
}

void ShowOutputs::Reset(void)
{
	Ec = Efn = Efp = Ev = x = false;
	Ef = Psi = electrons = holes = impCharge
		= n = deflight = condlight = genlight = genthermal = recthermal
		= srh1 = srh2 = srh3 = srh4 = rho
		= Jn = Jp = scatter = stddev = stderrBox
		= qe = point = y = z = false;
	volt = volt_n = volt_p = j1ctc_x = j2ctc_x = j1_x = j2_x = javg_x
		= j1ctc_n_x = j2ctc_n_x = j1_n_x = j2_n_x = javg_n_x
		= j1ctc_p_x = j2ctc_p_x = j1_p_x = j2_p_x = javg_p_x = false;
	xaxisZero = false;
	minX = maxX = widthX = minY = posY = maxY = widthY = widthZ = adjP = adjN
		= adjXP1 = adjXP2 = adjXM1 = adjXM2
		= adjYP1 = adjYP2 = adjYM1 = adjYM2 = false;
	point = posX = true;
	wavelength = energy = photon = pflux = power = intensity = false;
	return;
}

void MyListBox::OnKeyUp(wxKeyEvent& event)
{
	int keyPressed = event.GetKeyCode();
	if (keyPressed == WXK_DELETE)
	{
		int selection = GetSelection();
		if (selection != wxNOT_FOUND)
		{
			wxString FSelection = GetStringSelection();
			wxWindow* par = GetParent();
			resultsPanel* res = ((resultsPanel *)par);
			Delete(selection);	//file list box is a 1:1 array with active file indices

			res->RemoveFileFromList(FSelection);

			res->ActiveFileIndices.erase(res->ActiveFileIndices.begin() + selection);
			if (res->ActiveFileIndices.size() == 0)
				res->Btn_ClearFilteredFiles->Disable();
			res->FileSelectSlider->SetMax(res->ActiveFileIndices.size());
		}
		return;
	}
	event.Skip();	//make sure it keeps processing this with the default stuff!
	return;
}

BEGIN_EVENT_TABLE(MyListBox, wxPanel)
EVT_KEY_UP(MyListBox::OnKeyUp)
END_EVENT_TABLE()

double find_max_value(std::vector<double>& vec)
{
	if (vec.empty()) return 0.0;
	double max = vec[0];
	for (unsigned int i = 1; i < vec.size(); i++)
	{
		if (vec[i] > max) max = vec[i];
	}
	return max;
}

bool find_maxmin_value(std::vector<double>& vec, double& max, double& min)
{
	max = min = 0.0;
	if (vec.empty()) return false;
	max = min = vec[0];
	for (unsigned int i = 1; i < vec.size(); i++)
	{
		if (vec[i] > max) max = vec[i];
		if (vec[i] < min) min = vec[i];
	}
	return true;
}

bool find_maxmin_value_nested(PlotData* pl, double& max, double& min)
{
	max = min = 0.0;
	if (pl == NULL) return false;
	if (pl->ActiveLData.size() == 0) return false;
	max = -1e300;
	min = 1e300;

	for (unsigned int i = 0; i < pl->ActiveLData.size(); i++)
	{
		for (unsigned int j = 0; j<pl->LData[pl->ActiveLData[i]].size(); j++)
		{
			double tmp = pl->LData[pl->ActiveLData[i]].GetData(j);
			if (tmp > max) max = tmp;
			if (tmp < min) min = tmp;
		}
	}

	return true;
}

double find_min_value(std::vector<double> vec)
{
	if (vec.empty()) return 0.0;
	double min = vec[0];
	for (unsigned int i = 1; i < vec.size(); i++)
	{
		if (vec[i] < min) min = vec[i];
	}
	return min;
}

double find_max_value_nested(std::vector<std::vector<double> > vec)
{
	if (vec.empty()) return 0.0;
	double max = -DBL_MAX;
	for (unsigned int i = 2; i < vec.size(); i++)
	{
		for (unsigned int j = 0; j < vec[i].size(); j++)
		{
			if (vec[i][j] > max) max = vec[i][j];
		}
	}
	return max;
}

double find_min_value_nested(std::vector<std::vector<double> > vec)
{
	if (vec.empty()) return 0.0;
	double min = DBL_MAX;
	for (unsigned int i = 2; i < vec.size(); i++)
	{
		for (unsigned int j = 0; j < vec[i].size(); j++)
		{
			if (vec[i][j] < min) min = vec[i][j];
		}
	}
	return min;
}
