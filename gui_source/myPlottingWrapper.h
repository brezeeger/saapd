#ifndef SAAPD_PLOTTING_WRAPPER_H
#define SAAPD_PLOTTING_WRAPPER_H

#include "wx/dialog.h"
#include "wx/panel.h"
#include "mathplot.h"

#define PL_TYPE_UNDEF 0
#define PL_TYPE_X 1
#define PL_TYPE_X_ACTIVE 2	//there can only be one active X value
#define PL_TYPE_Y 4
#define PL_TYPE_Y_ACTIVE 8


class wxFlexGridSizer;
class wxTextCtrl;
class wxButton;

int AutoRotateColor(wxPen& pen, int color);


class LineData
{
	unsigned char dataType;	//is this the data set being used for the x axis? Can only be true of one. Moves X over by default
	bool connectData;
	std::vector<double> data;
	wxPen pen;
	wxString header;	//what the header name is for this data - constant and used to match
	wxString legend;
	int alignment;

	double scale;
	bool ignoreZeros;
	double offset;
public:
	LineData(wxString& head, std::vector<double>& dat, unsigned char dType = PL_TYPE_UNDEF, bool Connect = true, double scl = 1.0, double off = 0.0, bool ignoreZ = false) :
		header(head), legend(head), data(dat), dataType(dType), connectData(Connect),
		scale(scl), ignoreZeros(ignoreZ), offset(off), alignment(mpALIGN_NW)
	{ 
		pen = wxPen(wxColour((unsigned long)0), 1, wxPENSTYLE_SOLID);
	}
	LineData(wxString head = wxEmptyString, unsigned char dType = PL_TYPE_UNDEF, bool Connect = true, double scl = 1.0, double off = 0.0, bool ignoreZ = false) :
		header(head), legend(head), dataType(dType), connectData(Connect),
		scale(scl), ignoreZeros(ignoreZ), offset(off)
	{
		pen = wxPen(wxColour((unsigned long)0), 1, wxPENSTYLE_SOLID);
	}
	~LineData() { data.clear(); }

	void AddData(double d) { data.push_back(d); }
	void AddData(std::vector<double>& d) { data.insert(data.end(), d.begin(), d.end()); }
	void ReplaceData(std::vector<double>& d) { data = d; }
	void ReplaceData(unsigned int index, double d) { if (index < data.size()) data[index] = d; }
	void RemoveData(unsigned int index = 0) { if (index < data.size()) data.erase(data.begin() + index); }
	void ClearData() { data.clear(); }
	void ConnectData(bool connect = true) { connectData = connect; }
	bool IsDataConnected() { return(connectData); }
	bool HasData() { return (data.size() > 0); }
	void SetAlignment(int align = mpALIGN_LEFT) { alignment = align; }
	int GetAlignment() { return alignment; }
	std::vector<double>& GetData() { return data; }
	bool GetData(double& dat, unsigned int index) {
		if (index < data.size()) {
			dat = data[index];
			return(true);
		}
		return(false);
	}
	double GetData(unsigned int index) { return (index < data.size()) ? data[index] : 0.0; }
	bool SetData(unsigned int index, double dat) {
		if (index < data.size()) {
			data[index] = dat;
			return true;
		}
		return false;
	}
	
	void SetAsX() { dataType = PL_TYPE_X; }
	void SetAsActiveX() { dataType = (PL_TYPE_X | PL_TYPE_X_ACTIVE); }
	bool IsX() { return ((dataType & PL_TYPE_X) == PL_TYPE_X); }
	bool IsXActive() { return ((dataType & PL_TYPE_X_ACTIVE) == PL_TYPE_X_ACTIVE); }
	void ClearActiveX() { dataType &= (~PL_TYPE_X_ACTIVE); }
	void ClearX() { dataType &= (~(PL_TYPE_X | PL_TYPE_X_ACTIVE)); }

	void SetAsY() { dataType = PL_TYPE_Y; }
	void SetAsActiveY() { dataType = (PL_TYPE_Y | PL_TYPE_Y_ACTIVE); }
	bool IsY() { return ((dataType & PL_TYPE_Y) == PL_TYPE_Y); }
	bool IsYActive() { return ((dataType & PL_TYPE_Y_ACTIVE) == PL_TYPE_Y_ACTIVE); }
	void ClearActiveY() { dataType &= (~PL_TYPE_Y_ACTIVE); }
	void ClearY() { dataType &= (~(PL_TYPE_Y | PL_TYPE_Y_ACTIVE)); }

	void ClearDataType() { dataType = PL_TYPE_UNDEF; }
	unsigned char GetDataType() { return dataType; }

	void SetScale(double scl = 1.0) { scale = scl; }
	double GetScale() { return(scale); }

	void IgnoreZeros(bool ignore = true) { ignoreZeros = ignore; }
	void ShowZeros() { ignoreZeros = false; }
	bool IsIgnoreZeros() { return ignoreZeros; }
	void ToggleZeros() { ignoreZeros = !ignoreZeros; }

	void SetOffset(double off = 0.0) { offset = off; }
	double GetOffset() { return offset; }

	void SetPen(const wxPen& p) { pen = p; }
	wxPen& GetPen() { return pen; }
	
	bool GetMaxMinValue(double& max, double& min);

	
	void SetHeader(const wxString& head) { header = head; }
	wxString GetHeader() { return header; }

	void SetLegend(const wxString& leg) { legend = leg; }
	wxString GetLegend() { return legend; }

	void SetHeaderLegend(const wxString& h) { header = legend = h; }

	unsigned int size() { return data.size(); }

	double& GetRefOffset() { return offset; }
	double& GetRefScale() { return scale;  }

	void reserve(unsigned int i) { data.reserve(i); }

};

#define PLOT_WINDOW_DATA_START 4
/*
To add plot data, Call to subsequent create datas. One with the X values, the second with the Y values.
The plot will graph the Y's using the preceeding X value. Note it needs a call to Update to actually
get the data in the plot!
*/
class PlotWindow
{
private:
	mpScaleX *xAxis;
	mpScaleY *yAxis;
	mpText *title;
	mpInfoLegend* legend;
	mpWindow *plot;			//this is a wxWindow!
protected:
	bool showLegend;
	bool showLabel;
	unsigned int activeX;	//what holds the active X Data
	std::vector<unsigned int> ActivePlots;	//all the Y values. Allows re-ordering
	std::vector<LineData*> LData;	//all the actual data
	int UpdateActive();
public:
	PlotWindow(wxWindow* parent = nullptr, wxWindowID id = wxID_ANY);
	~PlotWindow();

	bool ScaleAll(double scl = 1.0);
	bool ScaleData(unsigned int index, double scl = 1.0);
	bool ScaleData(const wxString& header, double scl = 1.0);
	bool OffsetAll(double offset = 0.0);
	bool OffsetData(unsigned int index, double offset = 0.0);
	bool OffsetData(const wxString& header, double offset = 0.0);

	void UpdateLegend();
	void UpdateLegend(wxEvent& event);
	void ShowLegend(bool show = true) { showLegend = show; }
	void HideLegend() { showLegend = false; }
	void ShowLabel(bool show = true) { showLabel = show; }
	void HideLabel() { showLabel = false; }

	void SetTitle(wxString Title) { title->SetTitle(Title); }
	void SetTitleOffset(int x, int y) { title->SetOffset(x, y); }
	void GetTitleOfset(int& x, int& y) { title->GetOffset(x, y); }

	void SetXAxisAlignment(int align = mpALIGN_BOTTOM) { xAxis->SetAlign(align); }
	void SetXAxisTicks() { xAxis->SetTicks(true); }
	void SetXAxisGrid() { xAxis->SetTicks(false); }
	bool IsXAxisTicks() { return xAxis->GetTicks(); }
	bool IsXAxisGrid() { return !xAxis->GetTicks(); }
	void SetXAxisLabelMode(unsigned int mode = mpX_NORMAL) { xAxis->SetLabelMode(mode); }
	unsigned int GetXAxisLabelMode() { return xAxis->GetLabelMode(); }
	void SetXAxisLabelFormat(const wxString& format) { xAxis->SetLabelFormat(format); }
	void SetXAxisName(const wxString& nm) { xAxis->SetName(nm); }
	const wxString& GetXAxisLabelFormat() { return xAxis->GetLabelFormat(); }
	mpScaleX* getXAxis() { return xAxis; }

	void SetYAxisAlignment(int align = mpALIGN_LEFT) { yAxis->SetAlign(align); }
	void SetYAxisTicks() { yAxis->SetTicks(true); }
	void SetYAxisGrid() { yAxis->SetTicks(false); }
	bool IsYAxisTicks() { return yAxis->GetTicks(); }
	bool IsYAxisGrid() { return !yAxis->GetTicks(); }
	void SetYAxisLabelFormat(const wxString& format) { yAxis->SetLabelFormat(format); }
	void SetYAxisName(const wxString& nm) { yAxis->SetName(nm); }
	const wxString& GetYAxisLabelFormat() { return yAxis->GetLabelFormat(); }
	mpScaleY* getYAxis() { return yAxis; }

	wxWindow* GetWXWindow() { return (wxWindow*)plot; }
	mpWindow* GetMPWindow() { return plot; }	//this is a wxWindow as well!

	LineData* GetData(unsigned int index) { return index < LData.size() ? LData[index] : nullptr; }
	LineData* GetData(const wxString& header);
	bool GetData(unsigned int indexL, unsigned int indexD, double& dat);
	LineData* CreateData(wxString& head, std::vector<double>& dat, unsigned char dType = PL_TYPE_Y, bool Connect = true, double scl = 1.0, double off = 0.0, bool ignoreZ = false)
	{
		LData.push_back(new LineData(head, dat, dType, Connect, scl, off, ignoreZ));
		return LData.back();
	}
	unsigned int getNumData() { return LData.size(); }
	unsigned int getNumYData();
	LineData* getYDataIndex(unsigned int index);
	LineData* CreateData(LineData* lDat) { LData.push_back(lDat); return(lDat); }
	bool RemoveDataVector(const wxString& header);
	bool RemoveDataVector(unsigned int index);
	void RemoveAllData();

	bool RemoveCommonIndex(unsigned int index);

	bool SetXData(unsigned int index);
	bool SetXData(const wxString& head);

	bool SetXDataActive(unsigned int index);
	bool SetXDataActive(const wxString& head);

	bool SetYData(unsigned int index);
	bool SetYData(const wxString& head);

	bool SetYDataActive(unsigned int index);
	bool SetYDataActive(const wxString& head);

	unsigned int GetDataIndex(const wxString& head);

	void RefreshView() { plot->Fit(0.05); plot->Layout(); }

	int UpdatePlot(bool FitToData=true);	//call this to update the data in the plot!

	unsigned int AddUnique(unsigned int index, double dat);

	bool Enable(bool en = true) { return(plot->Enable(en)); }
	void Disable() { plot->Disable(); }
	
};


class SavePlotImage : public wxDialog
{
public:
	SavePlotImage(wxWindow* parent);
	~SavePlotImage();
	bool TransferDataFromWindow();
	unsigned long int wd, ht;
	wxString str_fname;

private:
	wxFlexGridSizer* sizerMain;
	wxTextCtrl* width;
	wxTextCtrl* height;
	wxTextCtrl* filename;
	wxStaticText* wLabel;
	wxStaticText* hLabel;
	wxButton* btn_Destination;
	wxButton* btn_Save;
	wxButton* btn_Cancel;
	wxString str_height, str_width;

	void GetFileLocation(wxCommandEvent& event);

private:
	DECLARE_EVENT_TABLE()
};



#endif