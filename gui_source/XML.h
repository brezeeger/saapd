#ifndef XML_H
#define XML_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/file.h"
#include "wx/textfile.h"
#endif

#include <vector>
#include "gui_structs.h"
#include "../kernel_source/helperfunctions.h"

#include<fstream>
#include<iomanip>

extern void materialToFile(material* material, wxFile *oFile);
extern void impurityToFile(impurity* impurity, wxFile *oFile);
extern void defectToFile(defect* defect, wxFile *oFile);
extern void contactToFile(contact* contact, wxFile *oFile);
extern void lDescMatToFile(layer_materials* lay_mat, wxFile *oFile);
extern void lDescImpToFile(layer_impurities* lay_imp, wxFile *oFile);
extern void layerToFile(layer* layer, wxFile *oFile);

extern void resettabs();

extern char mat_wxString_to_int(wxString& mat);


class XMLFileOut
{
	std::ofstream file;
	std::vector<std::string> openTags;

	bool WriteTabs(unsigned int skip=0)
	{
		if (file.is_open() == false)
			return(false);

		if (skip < openTags.size())
		{
			for (unsigned int i = 0; i < openTags.size()-skip; ++i)
				file << "\t";
		}
		return(true);
	}
public:
	XMLFileOut(std::string fname)
	{
		openFile(fname);
	}
	~XMLFileOut()
	{
		closeFile();
	}
	bool CreateCategory(std::string cat)
	{
		if (WriteTabs() == false)
			return(false);
		openTags.push_back(cat);
		file << "<" << cat << ">" << std::endl;
		return(true);
	}

	bool CloseLastCategory()
	{
		if (openTags.size() > 0)
		{
			if (WriteTabs(1) == false)
				return(false);
			
			file << "</" << openTags.back() << ">" << std::endl;
			openTags.pop_back();
			return(true);
		}
		return(false);
	}

	bool WriteTag(std::string label)
	{
		if (WriteTabs() == false)
			return(false);

		file << "<" << label << " />" << std::endl;
		return(true);
	}

	//make sure the boolean takes priority
	bool WriteTag(std::string label, bool value)
	{
		if (WriteTabs() == false)
			return(false);

		file << "<" << label << ">";
		if (value)
			file << "1";
		else
			file << "0";
		file << "</" << label << ">" << std::endl;
		return(true);
	}

	bool WriteTag(std::string label, double value, unsigned int precision = 12, bool scientific=false)
	{
		if (WriteTabs() == false)
			return(false);

		//defaults to a large precision, and then cuts out the excess
		file << "<" << label << ">";
		if (!scientific) {
			unsigned int prec = getPrecision(value, false);
			if (prec < precision)
				precision = prec;
			if (precision == 0)
				precision = 1;

			file << std::fixed;
		}
		else {
			unsigned int prec = getPrecision(value, true);
			if (prec < precision)
				precision = prec;
			if (precision == 0)
				precision = 1;

			file << std::scientific;
		}
		file << std::setprecision(precision);
		file << value << "</" << label << ">" << std::endl;
		return(true);
	}

	template <typename T>
	bool WriteTag(std::string label, T value)
	{
		if (WriteTabs() == false)
			return(false);

		file << "<" << label << ">" << value << "</" << label << ">" << std::endl;
		return(true);
	}

	

	bool WriteTag(std::string label, unsigned int value, std::vector<std::string>& replaceStrs)
	{
		if (WriteTabs() == false)
			return(false);
		if (value < replaceStrs.size())
			file << "<" << label << ">" << replaceStrs[value] << "</" << label << ">" << std::endl;
		else
			file << "<" << label << ">" << value << "</" << label << ">" << std::endl;
		return(true);
	}

	void closeFile()
	{
		if (file.is_open())
		{
			while (CloseLastCategory())
				;
			file.flush();
			file.close();
		}
		openTags.clear();
	}
	bool openFile(std::string fname)
	{
		if (file.is_open())
		{
			closeFile();
			file.clear();
			openTags.clear();
		}

		file.open(fname.c_str(), std::ofstream::out);
		return(file.is_open());
	}

	bool isOpen() { return(file.is_open()); }
};

class XMLLine
{
public:
	XMLLine();
	int parent;
	int sibling;
	int child; //points to first child
	wxString label;
	wxString value;
	wxString option;

	wxString ChildValue(wxString label, wxString defaultValue, std::vector<XMLLine> *tree);
	int FindChild(wxString name, std::vector<XMLLine> *tree);
	int FindDescendent(wxString name, std::vector<XMLLine> *tree);
	int FindLastChild(std::vector<XMLLine> *tree);
	void AddToParent(int parentIndex, int self, std::vector<XMLLine> *tree);
};

typedef std::vector<XMLLine>* XMLTree;

int parseFile(wxTextFile* iFile, std::vector<XMLLine>& upTree);

//Functor for outputing an XML tag around a specific object
class XMLItem
{
public:
	XMLItem(wxString label, wxString value, wxFile* output);
	XMLItem(wxString label, wxString options, wxString value, wxFile* output);
	//XMLItem(string label, int value, wxFile& output); //alternative for when the value is an integer
	
};

class XMLLabel
{
public:
	XMLLabel(wxString label, wxFile* output);
};


class XMLCat
{
public:
	XMLCat(wxString label, wxFile* output); //The ctor writes the opening tag
	XMLCat(wxString label, wxString options, wxFile* output);
	~XMLCat(); //The dtor writes the closing tag - Just make sure to have the closing brace where you want the code to be
	//Make sure to create them with a dummy variable name so that they are destroyed at the closing brace,
	//not immediately
	
private:
	//save these values for use by the dtor
	wxString sLabel;
	wxFile* sOutput;
	
};

#endif
