#include <vector>
#include "gui_structs.h"
#include "drawing.h"

extern std::vector<layer> layers_list;

wxBrushStyle ourStyles[6] = {
	wxBRUSHSTYLE_BDIAGONAL_HATCH,
	wxBRUSHSTYLE_CROSSDIAG_HATCH,
	wxBRUSHSTYLE_FDIAGONAL_HATCH,
	wxBRUSHSTYLE_CROSS_HATCH,
	wxBRUSHSTYLE_HORIZONTAL_HATCH,
	wxBRUSHSTYLE_VERTICAL_HATCH
};

wxString ourColors[7] = {
	wxT("RED"),
	wxT("BLUE"),
	wxT("GREEN"),
	wxT("ORANGE"),
	wxT("PURPLE"),
	wxT("BROWN"),
	wxT("MAROON")
};

#define NUM_BRUSHES 6
#define NUM_COLORS 7


layerShapesPanel::layerShapesPanel(wxWindow *parent)
: wxPanel(parent, wxID_ANY)
{
	userScale = 10;

	zoomSizer = new wxBoxSizer(wxVERTICAL);

	zoomIn = new wxButton(this, wxID_ZOOM_IN);
	zoomOut = new wxButton(this, wxID_ZOOM_OUT);

	zoomSizer->AddStretchSpacer();
	zoomSizer->Add(zoomIn, 0, wxALIGN_RIGHT|wxALL, 5);
	zoomSizer->Add(zoomOut, 0, wxALIGN_RIGHT|wxALL, 5);

	SetSizer(zoomSizer);
}

layerShapesPanel::~layerShapesPanel()
{
	delete zoomIn;
	delete zoomOut;
}

void layerShapesPanel::paintEvent(wxPaintEvent & evt)
{
	wxPaintDC dc(this);
//	render(dc);
}

void layerShapesPanel::paintNow()
{
	wxClientDC dc(this);
//	render(dc);
}

void layerShapesPanel::render(wxDC& dc)
{
	return;
	for(unsigned int i = 0; i < layers_list.size(); i++)
	{
		if(layers_list[i].hidden) continue;

		dc.SetBrush(*(wxTheBrushList->FindOrCreateBrush(
			wxTheColourDatabase->Find(ourColors[i % NUM_COLORS]),
			ourStyles[i % NUM_BRUSHES]))
		);
		dc.SetPen(*(wxThePenList->FindOrCreatePen(ourColors[i % NUM_COLORS])));

		switch(layers_list[i].shape)
		{
		case LAYER_POINT:
		{
			double x, y;
			layers_list[i].vertices[0].xPos.ToDouble(&x);
			layers_list[i].vertices[0].yPos.ToDouble(&y);
			dc.DrawPoint((int)(x*userScale), (int)(y*userScale));
		}
		break;
		case LAYER_LINE:
		{
			double x1, y1, x2, y2;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			dc.DrawLine((int)(userScale*x1), (int)(userScale*y1), (int)(userScale*x2), (int)(userScale*y2));
		}
		break;
		case LAYER_RECTANGLE:
		case LAYER_QUADRILATERAL:
		case LAYER_SQUARE:
		{
			double x1, y1, x2, y2, x3, y3, x4, y4;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			layers_list[i].vertices[2].xPos.ToDouble(&x3);
			layers_list[i].vertices[2].yPos.ToDouble(&y3);
			layers_list[i].vertices[3].xPos.ToDouble(&x4);
			layers_list[i].vertices[3].yPos.ToDouble(&y4);
			wxPoint points[4] = {wxPoint((int)(userScale*x1), (int)(userScale*y1)),
					wxPoint((int)(userScale*x2), (int)(userScale*y2)),
					wxPoint((int)(userScale*x3), (int)(userScale*y3)),
					wxPoint((int)(userScale*x4), (int)(userScale*y4))};
			dc.DrawPolygon(4, points);
		}
		break;
		case LAYER_PENTAGON:
		{
			double x1, y1, x2, y2, x3, y3, x4, y4, x5, y5;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			layers_list[i].vertices[2].xPos.ToDouble(&x3);
			layers_list[i].vertices[2].yPos.ToDouble(&y3);
			layers_list[i].vertices[3].xPos.ToDouble(&x4);
			layers_list[i].vertices[3].yPos.ToDouble(&y4);
			layers_list[i].vertices[4].xPos.ToDouble(&x5);
			layers_list[i].vertices[4].yPos.ToDouble(&y5);
			wxPoint points[5] = {wxPoint((int)(userScale*x1), (int)(userScale*y1)),
					wxPoint((int)(userScale*x2), (int)(userScale*y2)),
					wxPoint((int)(userScale*x3), (int)(userScale*y3)),
					wxPoint((int)(userScale*x4), (int)(userScale*y4)),
					wxPoint((int)(userScale*x5), (int)(userScale*y5))};
			dc.DrawPolygon(5, points);
		}
		break;
		case LAYER_HEXAGON:
		{
			double x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			layers_list[i].vertices[2].xPos.ToDouble(&x3);
			layers_list[i].vertices[2].yPos.ToDouble(&y3);
			layers_list[i].vertices[3].xPos.ToDouble(&x4);
			layers_list[i].vertices[3].yPos.ToDouble(&y4);
			layers_list[i].vertices[4].xPos.ToDouble(&x5);
			layers_list[i].vertices[4].yPos.ToDouble(&y5);
			layers_list[i].vertices[5].xPos.ToDouble(&x6);
			layers_list[i].vertices[5].yPos.ToDouble(&y6);
			wxPoint points[6] = {wxPoint((int)(10*x1), (int)(10*y1)),
					wxPoint((int)(userScale*x2), (int)(userScale*y2)),
					wxPoint((int)(userScale*x3), (int)(userScale*y3)),
					wxPoint((int)(userScale*x4), (int)(userScale*y4)),
					wxPoint((int)(userScale*x5), (int)(userScale*y5)),
					wxPoint((int)(userScale*x6), (int)(userScale*y6))};
			dc.DrawPolygon(6, points);
		}
		break;
		case LAYER_ELLIPSE:
		{
			double x1, y1, x2, y2, x3, y3;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			layers_list[i].vertices[2].xPos.ToDouble(&x3);
			layers_list[i].vertices[2].yPos.ToDouble(&y3);
			double radius1 = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
			double radius2 = sqrt((x1-x3)*(x1-x3) + (y1-y3)*(y1-y3));
			dc.DrawEllipse((int)(userScale*x1), (int)(userScale*y1), (int)(userScale*radius1), (int)(userScale*radius2));
		}
		break;
		case LAYER_CIRCLE:
		{
			double x1, y1, x2, y2;
			layers_list[i].vertices[0].xPos.ToDouble(&x1);
			layers_list[i].vertices[0].yPos.ToDouble(&y1);
			layers_list[i].vertices[1].xPos.ToDouble(&x2);
			layers_list[i].vertices[1].yPos.ToDouble(&y2);
			double radius = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
			dc.DrawCircle((int)(userScale*x1), (int)(userScale*y1), (int)(userScale*radius));
		}
		break;
		default:
		break;
		}
	}
}

void layerShapesPanel::ZoomIn(wxCommandEvent& event)
{
	if(userScale < MAX_SCALE)
	{
		userScale *= SCALING_FACTOR;
		Update();
	}
}

void layerShapesPanel::ZoomOut(wxCommandEvent& event)
{
	if(userScale > MIN_SCALE)
	{
		userScale /= SCALING_FACTOR;
		Update();
	}
}

BEGIN_EVENT_TABLE(layerShapesPanel, wxPanel)
EVT_PAINT(layerShapesPanel::paintEvent)
EVT_BUTTON(wxID_ZOOM_IN, layerShapesPanel::ZoomIn)
EVT_BUTTON(wxID_ZOOM_OUT, layerShapesPanel::ZoomOut)
END_EVENT_TABLE()

pointPanel::pointPanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
//	paintNow();
}

void pointPanel::paintEvent(wxPaintEvent& evt)
{
	wxPaintDC dc(this);
//	render(dc);
}

void pointPanel::paintNow()
{
	wxClientDC dc(this);
//	render(dc);
}

void pointPanel::render(wxDC& dc)
{
	dc.SetPen(*wxBLACK_PEN);

	//get window size
	wxCoord xSize, ySize;
	dc.GetSize(&xSize, &ySize);

	dc.DrawRectangle(10, 10, 100, 200);
	dc.DrawRectangle(xSize - 110, 10, 100, 200);
	drawCarrierArrow(dc, wxPoint(200, 200), wxPoint(500, 300), 50);
}

void pointPanel::SizeRight()
{
	
}

#define TRANGLE_SIZE 20
#define TEXTPOS_OFFSET 20

void pointPanel::drawCarrierArrow(wxDC& dc, wxPoint start, wxPoint end, int num)
{
	dc.DrawLine(start, end);

	//now determine the angle of the line
	double slope = ((double) (end.y - start.y))/(end.x - start.x);
	double angle = atan(slope);
	if(start.y > end.y)
		angle += PI / 2;
	wxPoint triangle[3];
	triangle[0] = end;
	triangle[1] = wxPoint(end.x - TRANGLE_SIZE * cos(angle + PI / 12), end.y - TRANGLE_SIZE * sin(angle + PI / 12));
	triangle[2] = wxPoint(end.x - TRANGLE_SIZE * cos(angle - PI / 12), end.y - TRANGLE_SIZE * sin(angle - PI / 12));
	dc.DrawPolygon(3, triangle);
	wxPoint textpos((start.x + end.x) / 2 - TEXTPOS_OFFSET, (start.y + end.y) / 2 - TEXTPOS_OFFSET);
	dc.DrawText(wxString::Format("%i", num), textpos);

}

BEGIN_EVENT_TABLE(pointPanel, wxPanel)
EVT_PAINT(pointPanel::paintEvent)
END_EVENT_TABLE()
