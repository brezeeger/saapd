#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"

#include "SimulationWindow.h"
#include "SimSteadyState.h"
#include "../kernel_source/ss.h"
#include "../kernel_source/model.h"
#include "../kernel_source/light.h"
#include "../kernel_source/contacts.h"
#include "../kernel_source/load.h"

#include <wx/grid.h>

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif

simSteadyStateBook::simSteadyStateBook(wxWindow* parent, wxWindowID id)
	: wxNotebook(parent, id), stDetailedWarning(nullptr)
{
	szrPrimaryDefine = new wxBoxSizer(wxVERTICAL); //This wrapper is just to make a margin around the panel

	PanelDefinition = new wxPanel(this, wxID_ANY);
	PanelSummary = new wxPanel(this, wxID_ANY);
	PanelDetailSummary = new wxPanel(this, wxID_ANY);
	AddPage(PanelDefinition, "Define Environments", true, 0);
	AddPage(PanelSummary, "Environment Summary", false, 1);
	AddPage(PanelDetailSummary, "Detailed Environment Summary", false, 2);
	
	wxArrayString emptyStringArray;
	wxArrayString modifySingleAll;
	modifySingleAll.Add("Modify All Group Environments");
	modifySingleAll.Add("Modify Only this Environments");
	wxArrayString algs;
	algs.Add("Transient");
	algs.Add("Monte Carlo");
	algs.Add("Full Newton");
	algs.Add("Full Newton - no light");
	algs.Add("Newton");
	algs.Add("Newton - no light");
	algs.Add("Numerical Newton");
	algs.Add("Smooth Fermi Energy");
	algs.Add("Sweep All to Zero");
	algs.Add("Sweep Defects to Zero");
	algs.Add("Sweep Currents to Zero");

	
	wxArrayString quickAddVars;
	quickAddVars.Add("Voltage (V)");
	quickAddVars.Add("Current (I)");
	quickAddVars.Add("Current Density (J)");
	quickAddVars.Add("Electric Field (E)");
	quickAddVars.Add("Current Injection");
	quickAddVars.Add("E-Field Injection");
	quickAddVars.Add("C. Density Injection");

	wxSize ArrowBtn(30, -1);
///////////////////////first do the entirety of the definition page.
	cbGroupChooser = new wxComboBox(PanelDefinition, SS_CHANGE_GROUP, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbEnvChooser = new wxComboBox(PanelDefinition, SS_CHANGE_ENV, wxT("Choice 1"), wxDefaultPosition, wxSize(50,-1), emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);	//this is literally just numbers 1-X
	cbAvailSpectraChooser = new wxComboBox(PanelDefinition, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY, wxDefaultValidator, wxT("Choose Spectra"));
	cbAvailContactChooser = new wxComboBox(PanelDefinition, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbChangeAll = new wxComboBox(PanelDefinition, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, modifySingleAll, wxCB_DROPDOWN | wxCB_READONLY);
	cbChangeAll->Select(1);
	cbQuickAddVariableList = new wxComboBox(PanelDefinition, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, quickAddVars, wxCB_DROPDOWN | wxCB_READONLY);
	cbQuickAddVariableList->Select(0);
	cbSolMethods = new wxComboBox(PanelDefinition, SS_ALG_CHANGE, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, algs, wxCB_DROPDOWN | wxCB_READONLY);
	cbSolMethods->Select(1);

	lbSpectraUse = new wxListBox(PanelDefinition, SS_SPECTRA_SELECT, wxDefaultPosition, wxDefaultSize, emptyStringArray, wxLB_SINGLE | wxLB_NEEDED_SB);
	lbContactUse = new wxListBox(PanelDefinition, SS_CONTACT_SELECT, wxDefaultPosition, wxDefaultSize, emptyStringArray, wxLB_SINGLE | wxLB_NEEDED_SB);

	btnNewGroup = new wxButton(PanelDefinition, SS_NEW_GROUP, wxT("New Group"));
	btnRemoveGroup = new wxButton(PanelDefinition, SS_REMOVE_GROUP, wxT("Remove"));
	btnSaveGroup = new wxButton(PanelDefinition, SS_SAVE_GROUP, wxT("Save All"));
	btnLoadGroup = new wxButton(PanelDefinition, SS_LOAD_GROUP, wxT("Load"));
	btnRenameGroup = new wxButton(PanelDefinition, SS_RENAME_GROUP, wxT("Rename"));
	btnDuplicateGroup = new wxButton(PanelDefinition, SS_DUPLICATE_GROUP, wxT("Duplicate"));
	btnMoveEnvUp = new wxButton(PanelDefinition, SS_ENV_UP, wxT("\u2191"), wxDefaultPosition, ArrowBtn);
	btnMoveEnvDown = new wxButton(PanelDefinition, SS_ENV_DOWN, wxT("\u2193"), wxDefaultPosition, ArrowBtn);
	btnNewEnv = new wxButton(PanelDefinition, SS_NEW_ENV, wxT("New Environment"));
	btnRemoveEnv = new wxButton(PanelDefinition, SS_REMOVE_ENV, wxT("Remove"));
	btnChangeGroup = new wxButton(PanelDefinition, SS_CHANGE_ENV_GROUP, wxT("Change Group"));
	btnChangeGroup->SetToolTip("This will merge the current environment into an existing group, or create a new one if the group doesn't exist");
	btnQuickEnvDuplicate = new wxButton(PanelDefinition, SS_QUICK_ADD, wxT("Quick Add"));
	btnQuickEnvDuplicate->SetToolTip(wxT("Duplicate the current environment and add it to the data group. However, pick a single variable to scale between two values over X environments. Example: Voltage, 0.1, 0.5, and 5 will create identical environments except the voltage boundary conditions will subsequently be 0.1, 0.2, 0.3, 0.4, and 0.5. (Easy way to generate a JV plot)"));
	btnAddSpectra = new wxButton(PanelDefinition, SS_ADD_SPECTRA, wxT("Add"));
	btnRemoveSpectra = new wxButton(PanelDefinition, SS_REMOVE_SPECTRA, wxT("Remove"));
	btnAddContact = new wxButton(PanelDefinition, SS_ADD_CONTACT, wxT("Add"));
	btnRemoveContact = new wxButton(PanelDefinition, SS_REMOVE_CONTACT, wxT("Remove"));
	btnAddSolutionMethod = new wxButton(PanelDefinition, SS_ADD_ALG, wxT("Add"));
	btnReplaceSolutionMethod = new wxButton(PanelDefinition, SS_REPLACE_ALG, wxT("Replace"));
	btnSoluptionUp = new wxButton(PanelDefinition, SS_ALG_UP, wxT("\u2191"), wxDefaultPosition, ArrowBtn);
	btnSolutionDown = new wxButton(PanelDefinition, SS_ALG_DOWN, wxT("\u2193"), wxDefaultPosition, ArrowBtn);
	btnRemoveSolutionMethod = new wxButton(PanelDefinition, SS_REMOVE_ALG, wxT("Remove"));
	
	gdSolutions = new wxGrid(PanelDefinition, SS_ALG_BOX_CLICK);
	gdSolutions->CreateGrid(0, 3, wxGrid::wxGridSelectRows);
	gdSolutions->SetColLabelValue(0, "Method");
	gdSolutions->SetColLabelValue(1, "Start");
	gdSolutions->SetColLabelValue(2, "End");
	gdSolutions->SetRowLabelSize(20);
	gdSolutions->AutoSizeColumns();
	gdSolutions->EnableEditing(false);
	gdSolutions->SetToolTip(wxT("The order of solving mechanisms that will be used. A tolerance is defined for the start, and once reaching the end, it moves on. Once the last step is achieved, it is considered solved. These all attempt pushing the net rate to zero. The tolerance limits how many carriers may change by scaling to an individual state's Fermi level change for a single iteration."));

	wxSize smallBox(50, -1);

	txtQuickAddFirst = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtQuickAddLast = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtQuickAddNumber = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_DIGITS));
	txtCtcV = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, smallBox, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcI = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, smallBox, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcJ = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, smallBox, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcE = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, smallBox, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcI_ED = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcE_ED = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtCtcJ_ED = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtStartTol = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLastTol = new wxTextCtrl(PanelDefinition, SS_TEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	/*
	stCtcV = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Voltage"));
	stCtcI = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Current"));
	stCtcJ = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Current Density"));
	stCtcE = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Electric Field"));
	stCtcI_ED = new wxStaticText(PanelDefinition, wxID_ANY, wxT("External Current"));
	stCtcE_ED = new wxStaticText(PanelDefinition, wxID_ANY, wxT("External E-Field"));
	stCtcJ_ED = new wxStaticText(PanelDefinition, wxID_ANY, wxT("External C. Density"));
	*/

	wxSize largerY(-1, -1);

	stContactName = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Contact Name"));
	stSolutionMethod = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Algorithms"));
	stSolutionMethod->SetToolTip("Universal to all groups. Each algorithm has it's strengths and weaknesses. Unfortunately, there was no one shoe fits all. Tweaking an algorithm for one problem often meant destroying it as a solution to a different problem. Therefore, you can now adjust things to suit your needs, so if it doesn't converge, you still have options.");
	
	stSolutionAlgorithmDescription = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Generally, the simulation progresses forward by progressively restricting the tolerance. The tolerance restricts how much an individual state's Fermi level may change in a single iteration. A state is considered at steady state values when its rate is 0 or changes sign with changes smaller than the tolerance."), wxDefaultPosition, largerY);
	

	stBCondSuppression = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Suppressed Boundary Conditions"));
	stBCondSuppression->SetToolTip("Just because the boundary condition is suppressed does not mean it has no effect. For example, setting the current will override any current calculations. Boundary condition preferences are given first to voltages, then electric fields, and finally currents.");
	stQuickAddFirst = new wxStaticText(PanelDefinition, wxID_ANY, wxT("First"));
	stQuickAddLast = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Last"));
	stQuickAddNumber = new wxStaticText(PanelDefinition, wxID_ANY, wxT("How many"));
	stStartTolerance = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Starting Tolerance"));
	stFinishTolerance = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Ending Tolerance"));
	stSpectraTitle = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Spectra"));
	stContactsTitle = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Contacts"));
	wxFont lFont = stSpectraTitle->GetFont();
	lFont.SetPointSize(16);
	lFont.MakeBold();
	stSpectraTitle->SetFont(lFont);
	stContactsTitle->SetFont(lFont);
	
	lFont.SetPointSize(12);
	stBCondSuppression->SetFont(lFont);
	stContactName->SetFont(lFont);
	stSolutionMethod->SetFont(lFont);

	cbCtcV = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("V"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcV->SetToolTip(wxT("Voltage (V)"));
	cbCtcI = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("I"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcI->SetToolTip(wxT("Current (A)"));
	cbCtcJ = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("J"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcJ->SetToolTip(wxT("Current Density (A/cm\u00B2)"));
	cbCtcE = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("E"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcE->SetToolTip(wxT("Electric Field (V/cm)"));
	cbCtcI_ED = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("Current Injection"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcI_ED->SetToolTip(wxT("(A) This causes a current from an external source to flow into the device (holes enter, electrons leave). The distribution of carriers will match that of the interaction point."));
	cbCtcE_ED = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("E-Field Injection"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcE_ED->SetToolTip(wxT("(V/cm) An external electric field enters (or exits) the device at this position and flows through the rest of the device. Note, in 1D devices, there is no natural dissipation."));
	cbCtcJ_ED = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("C. Density Injection"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcJ_ED->SetToolTip(wxT("(A/cm\u00B2) Very similar to current injection, except this is normalized to the surface area of where it is entered. This is more immune to scaling device sizes."));

	stSuppressV = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Voltage"), wxDefaultPosition, wxDefaultSize);
	stSupressC = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Currents"), wxDefaultPosition, wxDefaultSize);
	stSupressEfPoisson = new wxStaticText(PanelDefinition, wxID_ANY, wxT("EField"), wxDefaultPosition, wxDefaultSize);
	stSupressEfMinimize = new wxStaticText(PanelDefinition, wxID_ANY, wxT("EField Energy"), wxDefaultPosition, wxDefaultSize);
	stSupressOppDField = new wxStaticText(PanelDefinition, wxID_ANY, wxT("D-Fields"), wxDefaultPosition, wxDefaultSize);
	stSupressCtcChargeNeutral = new wxStaticText(PanelDefinition, wxID_ANY, wxT("Device Charge"), wxDefaultPosition, wxDefaultSize);
	stValidateMessage = new wxStaticText(PanelDefinition, wxID_ANY, wxT("No issues found for this environment"), wxDefaultPosition, wxDefaultSize);
	
	cbCtcExternal = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("Contact External"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcExternal->SetToolTip("CAUTION: Unstable with Newton solving method. The actual contact is considered outside the device, and this layer's outermost point connects to it. Currents and voltages are measured leaving this layer and are outside the device. It's mostly semantics, but there are advantages and disadvantages depending on the devices.");
	cbCtcSink = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("Contact Thermal Sink"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcSink->SetToolTip("Whatever enters the contact from the device is presumed to have an equal amount leaving through an external circuit. Similarly, what enters the device through the contact is replenished externally. It's carrier distribution is fixed at local steady state values. Currents and voltages are measured from within the device at this layer.");

	cbCtcMinEField = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("E-Field Energy"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcMinEField->SetToolTip("This boundary condition adjusts the electric field of this contact to minimize the energy resulting from the electric field. Useful for working with floating voltages");
	cbCtcOppDField = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("D-Field Edges"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcOppDField->SetToolTip("While most simulations use charge neutrality as a restraint, SAAPD does not. This constraint forces the electric field to cause both ends of the device to have opposing displacement fields. This keeps the device relatively stable and attempting to draw in appropriate charge.");
	cbCtcChargeNeutral = new wxCheckBox(PanelDefinition, SS_CBOXES, wxT("Device Charge"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	cbCtcChargeNeutral->SetToolTip("This sets the electric field such that the charge within the device remains constant. Basically, this condition enforces Kirchoff's Current Law as opposed to letting charge accumulate in the device.");

	szrTopRow = new wxBoxSizer(wxHORIZONTAL);
	szrSecTopRow = new wxBoxSizer(wxHORIZONTAL);
	szrQuickAdd = new wxBoxSizer(wxHORIZONTAL);
	szrLColumn = new wxBoxSizer(wxVERTICAL);
	szrMidColumn = new wxBoxSizer(wxVERTICAL);
	szrRightColumn = new wxBoxSizer(wxVERTICAL);
	szrMainCols = new wxBoxSizer(wxHORIZONTAL);
	szrSpecAddRemove = new wxBoxSizer(wxHORIZONTAL);
	szrCtcAddRemove = new wxBoxSizer(wxHORIZONTAL);
	szrSolReorderRemove = new wxBoxSizer(wxHORIZONTAL);
	
	szrBoundaryconditions = new wxStaticBoxSizer(wxVERTICAL, PanelDefinition, "Boundary Conditions");
	szrContactOptions = new wxStaticBoxSizer(wxVERTICAL, PanelDefinition, "Contact Options");

	szrfCtcBCond = new wxFlexGridSizer(5, 3, 3);
//	szrfCtcBCond->AddGrowableCol(1);
//	szrfCtcBCond->AddGrowableCol(3);
	szrfSolInputs = new wxFlexGridSizer(2, 3, 3);

	szrfCtcOptions = new wxFlexGridSizer(2, 3, 3);
	szrfCtcSupressions = new wxFlexGridSizer(3, 3, 3);
	szrfCtcSupressions->SetFlexibleDirection(wxBOTH);
	szrfCtcSupressions->AddGrowableCol(0);
	szrfCtcSupressions->AddGrowableCol(1);
	szrfCtcSupressions->AddGrowableCol(2);

	szrTopRow->Add(cbGroupChooser, 1, wxRIGHT | wxALIGN_CENTER_VERTICAL | wxLEFT, 4);
	szrTopRow->Add(btnNewGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrTopRow->Add(btnRemoveGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrTopRow->Add(btnSaveGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrTopRow->Add(btnLoadGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrTopRow->Add(btnDuplicateGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrTopRow->Add(btnRenameGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);

	szrSecTopRow->Add(cbEnvChooser, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL | wxLEFT, 4);
	szrSecTopRow->Add(btnMoveEnvUp, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrSecTopRow->Add(btnMoveEnvDown, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrSecTopRow->Add(btnNewEnv, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrSecTopRow->Add(btnRemoveEnv, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrSecTopRow->Add(btnChangeGroup, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrSecTopRow->Add(cbChangeAll, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	

	szrQuickAdd->Add(btnQuickEnvDuplicate, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL | wxLEFT, 4);
	szrQuickAdd->Add(cbQuickAddVariableList, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(stQuickAddFirst, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(txtQuickAddFirst, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(stQuickAddLast, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(txtQuickAddLast, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(stQuickAddNumber, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
	szrQuickAdd->Add(txtQuickAddNumber, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);

	szrSpecAddRemove->Add(btnAddSpectra, 0, wxLEFT | wxRIGHT, 2);
	szrSpecAddRemove->Add(btnRemoveSpectra, 0, wxRIGHT, 2);

	szrCtcAddRemove->Add(btnAddContact, 0, wxLEFT | wxRIGHT, 2);
	szrCtcAddRemove->Add(btnRemoveContact, 0, wxRIGHT, 2);

	szrLColumn->Add(stSpectraTitle, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 5);
	szrLColumn->Add(cbAvailSpectraChooser, 0, wxBOTTOM | wxEXPAND, 3);
	szrLColumn->Add(szrSpecAddRemove, 0, wxBOTTOM | wxEXPAND, 3);
	szrLColumn->Add(lbSpectraUse, 0, wxALL | wxEXPAND, 0);
	szrLColumn->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrLColumn->Add(stContactsTitle, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 5);
	szrLColumn->Add(cbAvailContactChooser, 0, wxBOTTOM | wxEXPAND, 3);
	szrLColumn->Add(szrCtcAddRemove, 0, wxBOTTOM | wxEXPAND, 3);
	szrLColumn->Add(lbContactUse, 1, wxBOTTOM | wxEXPAND, 3);

	szrfCtcBCond->Add(cbCtcV, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(txtCtcV, 0, wxALIGN_LEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(cbCtcI, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(txtCtcI, 0, wxALIGN_LEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(cbCtcMinEField, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcBCond->Add(cbCtcJ, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(txtCtcJ, 0, wxALIGN_LEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(cbCtcE, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(txtCtcE, 0, wxALIGN_LEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 2);
	szrfCtcBCond->Add(cbCtcOppDField, 0, wxALIGN_RIGHT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrBoundaryconditions->Add(szrfCtcBCond, 1, wxEXPAND, 0);
	szrBoundaryconditions->Add(cbCtcChargeNeutral, 0, wxALIGN_CENTER_HORIZONTAL | wxTOP, 3);

	

	szrfCtcOptions->Add(cbCtcExternal, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcOptions->Add(cbCtcSink, 0, wxALL | wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcOptions->Add(cbCtcI_ED, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcOptions->Add(txtCtcI_ED, 0, wxALL | wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);	//****txtCtcI_ED_Leak
	szrfCtcOptions->Add(cbCtcE_ED, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcOptions->Add(txtCtcE_ED, 0, wxALL | wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);	//****txtCtcE_ED_Leak
	szrfCtcOptions->Add(cbCtcJ_ED, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
	szrfCtcOptions->Add(txtCtcJ_ED, 0, wxALL | wxEXPAND | wxALIGN_CENTER_VERTICAL, 0);	//txtCtcJ_ED_Leak
	szrContactOptions->Add(szrfCtcOptions, 0, wxEXPAND, 0);

	szrfCtcSupressions->Add(stSuppressV, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak
	szrfCtcSupressions->Add(stSupressEfPoisson, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak
	szrfCtcSupressions->Add(stSupressC, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak
	szrfCtcSupressions->Add(stSupressEfMinimize, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak
	szrfCtcSupressions->Add(stSupressOppDField, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak
	szrfCtcSupressions->Add(stSupressCtcChargeNeutral, 0, wxALL | wxALIGN_CENTER, 0);	//txtCtcJ_ED_Leak

	szrMidColumn->Add(stContactName, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 0);
	szrMidColumn->Add(szrBoundaryconditions, 0, wxALL | wxEXPAND, 0);
	szrMidColumn->Add(szrContactOptions, 0, wxTOP | wxEXPAND, 5);	//txtCtcJ_ED_Leak
	szrMidColumn->AddStretchSpacer();
	szrMidColumn->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxBOTTOM, 2);
	szrMidColumn->Add(stBCondSuppression, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 3);
	szrMidColumn->Add(stValidateMessage, 0, wxBOTTOM | wxALIGN_CENTER_HORIZONTAL, 2);	//txtCtcJ_ED_Leak
	szrMidColumn->Add(szrfCtcSupressions, 0, wxALL | wxEXPAND, 0);	//txtCtcJ_ED_Leak
	
	szrfSolInputs->Add(stStartTolerance, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 0);
	szrfSolInputs->Add(stFinishTolerance, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	szrfSolInputs->Add(txtStartTol, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	szrfSolInputs->Add(txtLastTol, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	szrfSolInputs->Add(btnAddSolutionMethod, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	szrfSolInputs->Add(btnReplaceSolutionMethod, 0, wxALIGN_CENTER_HORIZONTAL, 0);

	szrSolReorderRemove->AddStretchSpacer();
	szrSolReorderRemove->Add(btnSoluptionUp, 0, wxALL, 0);
	szrSolReorderRemove->AddStretchSpacer();
	szrSolReorderRemove->Add(btnRemoveSolutionMethod, 0, wxALL, 0);
	szrSolReorderRemove->AddStretchSpacer();
	szrSolReorderRemove->Add(btnSolutionDown, 0, wxALL, 0);
	szrSolReorderRemove->AddStretchSpacer();

	
	szrRightColumn->Add(stSolutionMethod, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 3);
	szrRightColumn->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrRightColumn->Add(cbSolMethods, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	szrRightColumn->Add(szrfSolInputs, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 0);
	szrRightColumn->Add(gdSolutions, 1, wxALL | wxEXPAND, 0);
	szrRightColumn->Add(szrSolReorderRemove, 0, wxTOP | wxEXPAND | wxBOTTOM, 4);
	szrRightColumn->Add(stSolutionAlgorithmDescription, 0, wxALL | wxEXPAND, 0);

	szrMainCols->Add(szrLColumn, 0, wxALL | wxEXPAND, 0);
	szrMainCols->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
	szrMainCols->Add(szrMidColumn, 0, wxALL | wxEXPAND, 0);
	szrMainCols->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
	szrMainCols->Add(szrRightColumn, 1, wxALL | wxEXPAND, 0);

	
	szrPrimaryDefine->Add(szrTopRow, 0, wxALL | wxEXPAND, 0);
	szrPrimaryDefine->Add(new wxStaticLine(PanelDefinition,wxID_ANY,wxDefaultPosition,wxDefaultSize,wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDefine->Add(szrSecTopRow, 0, wxALL | wxEXPAND, 0);
	szrPrimaryDefine->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDefine->Add(szrMainCols, 1, wxALL | wxEXPAND, 0);
	szrPrimaryDefine->Add(new wxStaticLine(PanelDefinition, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDefine->Add(szrQuickAdd, 0, wxALL | wxEXPAND, 0);
	
	wxSize helpSize(275, 100);// = stSolutionAlgorithmDescription->GetSize();
	//	int approxLines = helpSize.GetWidth() / containerSize.GetWidth() + 1;
	//	helpSize.Set(containerSize.GetWidth(), approxLines * helpSize.GetHeight());
	stSolutionAlgorithmDescription->SetSize(helpSize);
	stSolutionAlgorithmDescription->Wrap(helpSize.GetWidth());

	
	

//	wxSize containerSize = cbSolMethods->GetSize();
	

	
	////////////////////////////////////////////////

	gdSummaryEnv = new wxGrid(PanelSummary, wxID_ANY);
	gdSummaryEnv->CreateGrid(0, 0);
	gdSummaryEnv->EnableEditing(false);

	gdSummaryDif = new wxGrid(PanelSummary, wxID_ANY);
	gdSummaryDif->CreateGrid(0, 0);
	gdSummaryDif->EnableEditing(false);

	cbSumGroup = new wxComboBox(PanelSummary, SS_SUM_GROUP, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbSumEnv = new wxComboBox(PanelSummary, SS_SUM_ENV, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	btnSumEditSelEnv = new wxButton(PanelSummary, SS_SUM_VIEW, wxT("Edit Selected Environment"));

	stSumMainTitle = new wxStaticText(PanelSummary, wxID_ANY, "Group: Environment #");
	stSumDifTitle = new wxStaticText(PanelSummary, wxID_ANY, "Differences");
	stDifExplain = new wxStaticText(PanelSummary, wxID_ANY, wxT("\u25CF Grey environment boxes indicate they will be ignored in terms of the boundary condition, though they may still have an effect on the device.\n\u25CF The active spectra is highlighted light yellow simply to indicate it a separate entity from the contacts.\n\u25CF The differences compare each contacts' functionality from the selection to the rest. Differences are shown in calculation order (ignoring the current environment and anything with the same value).\n\u25CF The ideal display for environment 1 has one variable being modified with one cell showing the start value, end value, and the difference. If this is not the case, the calculations will be inefficient, there is an errant environment somewhere, or you are modifying many things at once.\n\u25CF An \"X\" in the differences indicates the condition is completely removed compared to above.\n\u25CF An \"X\" in the environment indicates the condition is active."));

	wxFont ft = stSumMainTitle->GetFont();
	ft.SetPointSize(16);
	ft.MakeBold();

	stSumMainTitle->SetFont(ft);
	stSumDifTitle->SetFont(ft);

	szrTopRowSum = new wxBoxSizer(wxHORIZONTAL);
	szrPrimarySummary = new wxBoxSizer(wxVERTICAL);

	szrTopRowSum->Add(cbSumGroup, 0, wxALL, 2);
	szrTopRowSum->Add(cbSumEnv, 0, wxALL, 2);
	szrTopRowSum->Add(btnSumEditSelEnv, 0, wxALL, 2);

	szrPrimarySummary->Add(stSumMainTitle, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
	szrPrimarySummary->Add(szrTopRowSum, 0, wxEXPAND | wxALL, 2);
	szrPrimarySummary->Add(gdSummaryEnv, 0, wxEXPAND | wxALL, 2);
	szrPrimarySummary->Add(new wxStaticLine(PanelSummary, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimarySummary->Add(stSumDifTitle, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
	szrPrimarySummary->Add(gdSummaryDif, 1, wxEXPAND | wxALL, 2);
	szrPrimarySummary->Add(stDifExplain, 0, wxEXPAND | wxALL, 2);

	//////////////////////////////////////////////////////
	szrPrimaryDetailed = new wxBoxSizer(wxVERTICAL);
	szrDetailedSwap = new wxBoxSizer(wxHORIZONTAL);

	cbDetailedGroup = new wxComboBox(PanelDetailSummary, SS_DET_GROUP, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	gdDetailed = new wxGrid(PanelDetailSummary, wxID_ANY);
	gdDetailed->CreateGrid(0, 0);
	gdDetailed->EnableEditing(false);

	cbDetailedSwapContacts1 = new wxComboBox(PanelDetailSummary, SS_DET_SWAP, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbDetailedSwapContacts2 = new wxComboBox(PanelDetailSummary, SS_DET_SWAP, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	btnDetailedSwapContacts = new wxButton(PanelDetailSummary, SS_DET_SWAP, wxT("\u2190 Swap Contact Associations \u2192"));
	stDetailedWarning = new wxStaticText(PanelDetailSummary, wxID_ANY, "External Contact check passed.");
	stDetailedWarning->SetToolTip("In order for current to flow out of the device, the contact must be either marked External or as a Sink. The contact will still provide boundary conditions, but will not behave as usually expected. It will act more as a floating contact.");

	szrDetailedSwap->AddStretchSpacer();
	szrDetailedSwap->Add(cbDetailedSwapContacts1, 0, wxALL, 10);
	szrDetailedSwap->Add(btnDetailedSwapContacts, 0, wxALL, 10);
	szrDetailedSwap->Add(cbDetailedSwapContacts2, 0, wxALL, 10);
	szrDetailedSwap->AddStretchSpacer();

	szrPrimaryDetailed->Add(cbDetailedGroup, 0, wxALIGN_CENTER_HORIZONTAL | wxTOP | wxBOTTOM, 2);
	szrPrimaryDetailed->Add(gdDetailed, 1, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDetailed->Add(new wxStaticLine(PanelDetailSummary, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDetailed->Add(stDetailedWarning, 0, wxALIGN_CENTER_HORIZONTAL | wxTOP | wxBOTTOM, 2);
	szrPrimaryDetailed->Add(new wxStaticLine(PanelDetailSummary, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	szrPrimaryDetailed->Add(szrDetailedSwap, 0, wxEXPAND | wxTOP | wxBOTTOM, 2);
	
	/////////////////////

	SizeRight();

	TransferToGUIDefine();

}

void simSteadyStateBook::SwapContactAssociationBtn(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;
	int sel1 = cbDetailedSwapContacts1->GetSelection();
	int sel2 = cbDetailedSwapContacts2->GetSelection();
	if (sel1 < 0 || sel2 < 0 || sel1 == sel2)
		return;	//can't do a swap
	
	std::vector<int> listValues;	//gotta come up with the original list as the
	//names aren't a reliable method and there are not values associated with the listbox.
	for (unsigned int i = 0; i < sumGroups.size(); ++i) {
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			if (sumGroups[i]->ContactEnvironment[j]->ctc)
				VectorUniqueAdd(listValues, sumGroups[i]->ContactEnvironment[j]->ctcID);
		}
	}
	
	//translate these to contact indices that need to be swapped
	if ((unsigned int)sel1 >= listValues.size() || (unsigned int)sel2 >= listValues.size())	//something funkadelic happened
		return;
	sel1 = listValues[sel1];
	sel2 = listValues[sel2];



	for (unsigned int i = 0; i < sumGroups.size(); ++i) {
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			if (sumGroups[i]->ContactEnvironment[j]->ctcID == sel1) {
				sumGroups[i]->ContactEnvironment[j]->ctc = mdesc->simulation->getContact(sel2);
				if (sumGroups[i]->ContactEnvironment[j]->ctc)
					sumGroups[i]->ContactEnvironment[j]->ctcID = sumGroups[i]->ContactEnvironment[j]->ctc->id;
			}
			else if (sumGroups[i]->ContactEnvironment[j]->ctcID == sel2) {
				sumGroups[i]->ContactEnvironment[j]->ctc = mdesc->simulation->getContact(sel1);
				if (sumGroups[i]->ContactEnvironment[j]->ctc)
					sumGroups[i]->ContactEnvironment[j]->ctcID = sumGroups[i]->ContactEnvironment[j]->ctc->id;
			}
		}
	}

	UpdateGUIValuesDetailed(event);

}
void simSteadyStateBook::SwapContactAssociationCB(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	int sel1 = cbDetailedSwapContacts1->GetSelection();
	int sel2 = cbDetailedSwapContacts2->GetSelection();
	if (sel1 < 0 || sel2 < 0 || sel1 == sel2)
		btnDetailedSwapContacts->Disable();
	else
		btnDetailedSwapContacts->Enable();
}

simSteadyStateBook::~simSteadyStateBook()
{ }

void simSteadyStateBook::SizeRight()
{
	if (!isInitialized())
		return;
	PanelDefinition->SetSizer(szrPrimaryDefine);
	PanelSummary->SetSizer(szrPrimarySummary);
	PanelDetailSummary->SetSizer(szrPrimaryDetailed);
}

simulationWindow* simSteadyStateBook::getMyClassParent()
{
	return (simulationWindow*)GetParent();
}
ModelDescribe* simSteadyStateBook::getMdesc()
{
	return getMyClassParent()->getMdesc();
}

void simSteadyStateBook::TransferToMdescDefine(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curContactData)
	{
		if (cbCtcExternal->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CONNECT_EXTERNAL;
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CONNECT_EXTERNAL);
		}

		if (cbCtcSink->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_SINK;
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_SINK);
		}

		if (cbCtcV->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_VOLT_LINEAR;
			txtCtcV->GetValue().ToDouble(&curContactData->voltage);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_VOLT_LINEAR);
			curContactData->voltage = 0.0;
		}
		//it has to do the whole validation thing. So all I have to care about is updating the flags
		if (cbCtcI->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CURRENT_LINEAR;
			txtCtcI->GetValue().ToDouble(&curContactData->current);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CURRENT_LINEAR);
			curContactData->current = 0.0;
		}

		if (cbCtcJ->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR;
			txtCtcJ->GetValue().ToDouble(&curContactData->currentDensity);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CUR_DENSITY_LINEAR);
			curContactData->currentDensity = 0.0;
		}

		if (cbCtcE->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_EFIELD_LINEAR;
			txtCtcE->GetValue().ToDouble(&curContactData->eField);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_EFIELD_LINEAR);
			curContactData->eField = 0.0;
		}

		if (cbCtcI_ED->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CURRENT_LINEAR_ED;
			txtCtcI_ED->GetValue().ToDouble(&curContactData->current_ED);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CURRENT_LINEAR_ED);
			curContactData->current_ED = 0.0;
		}

		if (cbCtcE_ED->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_EFIELD_LINEAR_ED;
			txtCtcE_ED->GetValue().ToDouble(&curContactData->eField_ED);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_EFIELD_LINEAR_ED);
			curContactData->eField_ED = 0.0;
		}

		if (cbCtcJ_ED->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR_ED;
			txtCtcJ_ED->GetValue().ToDouble(&curContactData->currentDensity_ED);
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CUR_DENSITY_LINEAR_ED);
			curContactData->currentDensity_ED = 0.0;
		}

		if (cbCtcMinEField->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_MINIMIZE_E_ENERGY;
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_MINIMIZE_E_ENERGY);
		}
		if (cbCtcOppDField->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_OPP_DFIELD;
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_OPP_DFIELD);
		}
		if (cbCtcChargeNeutral->GetValue()) {
			curContactData->currentActiveValue |= CONTACT_CONSERVE_CURRENT;
		}
		else {
			curContactData->currentActiveValue &= (~CONTACT_CONSERVE_CURRENT);
		}
	}

	//all the other stuff isn't really transferred over en masse from the gui.
}

SS_SimData* simSteadyStateBook::getSSSim()
{
	SS_SimData* s = getMyClassParent()->getSim()->SteadyStateData;
	if (s == nullptr)
	{
		
		s = new SS_SimData(getMyClassParent()->getSim());
		getMyClassParent()->getSim()->SteadyStateData = s;
	}
	return s;
}

bool simSteadyStateBook::modifyAllEnvs()
{
	if (!isInitialized())
		return false;
	int sel = cbChangeAll->GetSelection();
	if (sel == 0)
		return(true);
	return(false);
}

void simSteadyStateBook::ChangeDetailedGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	sumCGroup = cbDetailedGroup->GetStringSelection();
	UpdateGUIValuesDetailed(event);
}

void simSteadyStateBook::UpdateGUIValuesDetailed(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* ss = getSSSim();
	ModelDescribe *mdesc = getMdesc();
	
	if (ss == nullptr || mdesc == nullptr)
	{
		sumGroups.clear();
		if (gdDetailed->GetNumberCols() > 0)
			gdDetailed->DeleteCols(0, gdDetailed->GetNumberCols());

		return;	//there's nothing else to really do...
	}

	VerifyInternalVars();
	ss->Validate();

	if (ss->ExternalStates.size() == 0) {
		unsigned int num = szrPrimaryDetailed->GetItemCount();
		for (unsigned int i = 0; i < num; ++i)
			szrPrimaryDetailed->Hide(i);
		return;
	}
	else {
		unsigned int num = szrPrimaryDetailed->GetItemCount();
		for (unsigned int i = 0; i < num; ++i)
			szrPrimaryDetailed->Show(i);
	}

	//populate the group combo box. First get all the available options, once.
	std::vector<std::string> names;
	sumGroups.clear();
	cbDetailedGroup->Clear();
	for (unsigned int i = 0; i < ss->ExternalStates.size(); ++i) {
		VectorUniqueAdd(names, ss->ExternalStates[i]->FileOut);	//build up the list for the one combo box
		if (sumCGroup == "")	//make sure one gets assigned if there isn't a group yet!
			sumCGroup = ss->ExternalStates[i]->FileOut;

		if (ss->ExternalStates[i]->FileOut == sumCGroup) {
			sumGroups.push_back(ss->ExternalStates[i]);
		}
	}

	//then put them all in, and select as needed
	if (names.size() > 0)
	{
		cbDetailedGroup->Enable();
		cbDetailedGroup->Clear();
		for (unsigned int i = 0; i < names.size(); ++i) {
			cbDetailedGroup->AppendString(names[i]);
			if (names[i] == sumCGroup)
				cbDetailedGroup->Select(i);
		}
	}

	//need to know all contact IDs that will be worked with in the group - and all spectra ids.
	unsigned int expCol = 0;
	unsigned int expRows = 0;
	//and which items we are adding...
	

	std::vector<int> contactIDS;
	std::vector<int> spectraIDS;

	for (unsigned int i = 0; i < sumGroups.size(); ++i) {
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			SSContactData* ct = sumGroups[i]->ContactEnvironment[j];
			if (ct) {
				if ((ct->currentActiveValue & CONTACT_ALLBC) == CONTACT_INACTIVE)
					continue;
				VectorUniqueAdd(contactIDS, ct->ctcID);	//this will usually fail...
			}
		}

		for (unsigned int j = 0; j < sumGroups[i]->ActiveLightEnvironment.size(); ++j)
			VectorUniqueAdd(spectraIDS, sumGroups[i]->ActiveLightEnvironment[j]);	//we'll deal with spectra later...
	}

	

	if (gdDetailed->GetNumberCols() > 0)
		gdDetailed->DeleteCols(0, gdDetailed->GetNumberCols());

	
	int deltaRows = sumGroups.size() - gdDetailed->GetNumberRows();
	if (deltaRows > 0)
		gdDetailed->AppendRows(deltaRows);
	else if (deltaRows < 0)
		gdDetailed->DeleteRows(0, -deltaRows);
	
	//clear everything out column wise, and leaves a row for each environment

	//first let's check the spectra, cuz that's easy.
	int curCol = 0;

	for (unsigned int i = 0; i < spectraIDS.size(); ++i) {
		LightSource *lsrc = mdesc->getSpectra(spectraIDS[i]);
		if (lsrc == nullptr)
			continue;
				
		gdDetailed->AppendCols();
		gdDetailed->SetColLabelValue(curCol, lsrc->name);

		for (unsigned int j = 0; j < sumGroups.size(); ++j) {
			if (sumGroups[j]->hasSpectra(spectraIDS[i])) {
				gdDetailed->SetCellValue(j, curCol, "X");
				gdDetailed->SetCellAlignment(wxALIGN_CENTER, j, curCol);
			}
			else
				gdDetailed->SetCellValue(j, curCol, "");
		}
		curCol++;
	}

	//next do voltages
	AddDetailColumns(contactIDS, curCol, CONTACT_VOLT_LINEAR);
	AddDetailColumns(contactIDS, curCol, CONTACT_EFIELD_LINEAR);
	AddDetailColumns(contactIDS, curCol, CONTACT_CURRENT_LINEAR);
	AddDetailColumns(contactIDS, curCol, CONTACT_CUR_DENSITY_LINEAR);
	AddDetailColumns(contactIDS, curCol, CONTACT_EFIELD_LINEAR_ED);
	AddDetailColumns(contactIDS, curCol, CONTACT_CURRENT_LINEAR_ED);
	AddDetailColumns(contactIDS, curCol, CONTACT_CUR_DENSITY_LINEAR_ED);
	AddDetailColumns(contactIDS, curCol, CONTACT_CONSERVE_CURRENT);
	AddDetailColumns(contactIDS, curCol, CONTACT_OPP_DFIELD);
	AddDetailColumns(contactIDS, curCol, CONTACT_MINIMIZE_E_ENERGY);
	AddDetailColumns(contactIDS, curCol, CONTACT_SINK);
	AddDetailColumns(contactIDS, curCol, CONTACT_CONNECT_EXTERNAL);

	SmartResizeGridColumns(gdDetailed);



	std::vector<int> listValues;	//gotta come up with the original list as the
	//names aren't a reliable method and there are not values associated with the listbox.
	for (unsigned int i = 0; i < sumGroups.size(); ++i) {
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			if (sumGroups[i]->ContactEnvironment[j]->ctc)
				VectorUniqueAdd(listValues, sumGroups[i]->ContactEnvironment[j]->ctcID);
		}
	}
	wxArrayString contactNames;
	Simulation* sim = ss->getSim();
	int sel1 = cbDetailedSwapContacts1->GetSelection();	//maintain selection
	int sel2 = cbDetailedSwapContacts1->GetSelection();	//maintain selection
	for (unsigned int i = 0; i < listValues.size(); ++i) {
		Contact* ct = sim->getContact(listValues[i]);
		if (ct)
			contactNames.Add(ct->name);
	}
	if (sel1 < 0)
		sel1 = 0;
	if (sel2 < 0)
		sel2 = 0;
	if (sel1 == sel2)
		sel2 = sel1 + 1;
	if ((unsigned int)sel2 >= contactNames.GetCount())
		sel2 = contactNames.GetCount() - 1;
	if ((unsigned int)sel1 >= contactNames.GetCount())
		sel1 = 0;

	cbDetailedSwapContacts1->Clear();
	cbDetailedSwapContacts1->Append(contactNames);
	cbDetailedSwapContacts2->Clear();
	cbDetailedSwapContacts2->Append(contactNames);
	if ((unsigned int)sel1 < contactNames.GetCount())
		cbDetailedSwapContacts1->Select(sel1);
	if ((unsigned int)sel2 < contactNames.GetCount())
		cbDetailedSwapContacts2->Select(sel2);

	sel1 = cbDetailedSwapContacts1->GetSelection();	//new selection... now that an attempt at assigning has been made
	sel2 = cbDetailedSwapContacts2->GetSelection();
	if (sel1 < 0 || sel2 < 0 || sel1 == sel2)
		btnDetailedSwapContacts->Disable();
	else
		btnDetailedSwapContacts->Enable();


	bool allContactsExternalSink = true;
	for (unsigned int i = 0; i < sumGroups.size(); ++i) {
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			if (TEST_BIT(sumGroups[i]->ContactEnvironment[j]->currentActiveValue, CONTACT_SINK) == false &&
				TEST_BIT(sumGroups[i]->ContactEnvironment[j]->currentActiveValue, CONTACT_CONNECT_EXTERNAL) == false) {
				allContactsExternalSink = false;
				break;
			}
		}
	}

	if (allContactsExternalSink) {
		stDetailedWarning->SetLabel("External contact connection check passed.");
	}
	else
	{
		stDetailedWarning->SetLabel("CAUTION: Not all defined contacts in this simulation connect to the external world allowing current to flow through them. Is this intentional (example, finding open circuit voltage uses a floating contact)? If not and you always want current to be able to flow, check either \"Contact External\"(default) or \"Contact Thermal Sink.\"");
		int w = PanelDetailSummary->GetSize().GetWidth() - 4;
		stDetailedWarning->SetSize(w, -1);
		stDetailedWarning->Wrap(w);
	}

	szrPrimaryDetailed->Layout();

}

std::string simSteadyStateBook::GetContactTypeName(int type)
{
	if (type == CONTACT_VOLT_LINEAR)
		return "Voltage";

	if (type == CONTACT_EFIELD_LINEAR)
		return "E-Field";

	if (type == CONTACT_CURRENT_LINEAR)
		return "Current";

	if (type == CONTACT_CUR_DENSITY_LINEAR)
		return "Current Density";

	if (type == CONTACT_EFIELD_LINEAR_ED)
		return "E-Field Injection";

	if (type == CONTACT_CURRENT_LINEAR_ED)
		return "Current Injection";

	if (type == CONTACT_CUR_DENSITY_LINEAR_ED)
		return "C. Density Injection";

	if (type == CONTACT_FLOAT_EFIELD_ESCAPE)
		return "Leakage";

	if (type == CONTACT_CONNECT_EXTERNAL)
		return "External";

	if (type == CONTACT_SINK)
		return "Sink";

	if (type == CONTACT_MINIMIZE_E_ENERGY)
		return "Min. E-Field Energy";

	if (type == CONTACT_OPP_DFIELD)
		return "Opposing D-Field";

	if (type == CONTACT_CONSERVE_CURRENT)
		return "Maintain Charge";

	return "";
}
void simSteadyStateBook::AddDetailColumns(const std::vector<int>& ctIDS, int& curCol, const unsigned int which)
{
	if (!isInitialized())
		return;
	if (which == CONTACT_INACTIVE || which == CONTACT_SS)
		return;
	for (unsigned int i = 0; i < ctIDS.size(); ++i) {
		SSContactData* ct;
		Contact *c = nullptr;
		bool useContact = false;
		wxString str;
		for (unsigned int j = 0; j < sumGroups.size(); ++j) {
			ct = sumGroups[j]->getContact(ctIDS[i]);
			if (ct == nullptr)	//don't care. move on to next one
				continue;
			if (ct->ctc == nullptr)	//
				continue;
			if (TEST_BIT(ct->currentActiveValue, which)) {
				useContact = true;
				c = ct->ctc;
				break;
			}
		}
		if (!useContact)	//the contact didn't have any voltages.
			continue;	//so skip it

		//this one has some voltages to report!
		gdDetailed->AppendCols();
		str = c->name;
		str << "\n" << GetContactTypeName(which);
		gdDetailed->SetColLabelValue(curCol, str);

		if (which >= CONTACT_VOLT_COS) {
			for (unsigned int j = 0; j < sumGroups.size(); ++j) {
				ct = sumGroups[j]->getContact(ctIDS[i]);
				if (ct == nullptr ||
					ct->ctc == nullptr ||
					TEST_BIT(ct->currentActiveValue, which)==false) {
					gdDetailed->SetCellValue(j, curCol, "");
				}
				else {
					str = "";
					str << ct->GetValue(which);
					gdDetailed->SetCellValue(j, curCol, str);
					gdDetailed->SetCellAlignment(wxALIGN_LEFT, j, curCol);
				}
			}
		}
		else { //it is the low ones, where it's just a binary check
			for (unsigned int j = 0; j < sumGroups.size(); ++j) {
				ct = sumGroups[j]->getContact(ctIDS[i]);
				if (ct == nullptr ||
					ct->ctc == nullptr ||
					TEST_BIT(ct->currentActiveValue, which)==false) {
					gdDetailed->SetCellValue(j, curCol, "");
				}
				else {
					gdDetailed->SetCellValue(j, curCol, "X");
					gdDetailed->SetCellAlignment(wxALIGN_CENTER, j, curCol);
				}
			}
		}
		curCol++;
	}
}

void simSteadyStateBook::UpdateGUIValuesSummary(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* ss = getSSSim();
	ModelDescribe *mdesc = getMdesc();
	if (ss==nullptr || mdesc == nullptr)
	{
		sumGroups.clear();
		if (gdSummaryEnv->GetNumberCols() > 0)
			gdSummaryEnv->DeleteCols(0, gdSummaryEnv->GetNumberCols());
		if (gdSummaryDif->GetNumberCols() > 0)
			gdSummaryDif->DeleteCols(0, gdSummaryDif->GetNumberCols());
		selEnv = 0;
		btnSumEditSelEnv->Disable();
		cbSumEnv->Disable();
		return;	//there's nothing else to really do...
	}
	VerifyInternalVars();
	ss->Validate();

	if (ss->ExternalStates.size() == 0) {
		unsigned int num = szrPrimarySummary->GetItemCount();
		for (unsigned int i = 0; i < num; ++i)
			szrPrimarySummary->Hide(i);
		return;
	}
	else {
		unsigned int num = szrPrimarySummary->GetItemCount();
		for (unsigned int i = 0; i < num; ++i)
			szrPrimarySummary->Show(i);
	}

	std::vector<std::string> names;
	sumGroups.clear();
	cbSumEnv->Clear();
	for (unsigned int i = 0; i < ss->ExternalStates.size(); ++i) {
		VectorUniqueAdd(names, ss->ExternalStates[i]->FileOut);	//build up the list for the one combo box
		if (sumCGroup == "")	//make sure one gets assigned if there isn't a group yet!
			sumCGroup = ss->ExternalStates[i]->FileOut;

		if (ss->ExternalStates[i]->FileOut == sumCGroup) {
			sumGroups.push_back(ss->ExternalStates[i]);
			wxString tmp = "Environment ";
			tmp << sumGroups.size();
			cbSumEnv->AppendString(tmp);
		}
	}

	if (names.size() > 0)
	{
		cbSumGroup->Enable();
		cbSumGroup->Clear();
		for (unsigned int i = 0; i < names.size(); ++i) {
			cbSumGroup->AppendString(names[i]);
			if (names[i] == sumCGroup)
				cbSumGroup->Select(i);
		}
	}

	Environment *sEnv = selEnv < sumGroups.size() ? sumGroups[selEnv] : nullptr;
	if (sEnv == nullptr && sumGroups.size() > 0) {
		sEnv = sumGroups[0];
		selEnv = 0;
	}

	if (sEnv == nullptr) {	//there is absolutely no data that could be displayed
		cbSumEnv->Disable();
		//nothing to display still!
		if (gdSummaryEnv->GetNumberCols() > 0)
			gdSummaryEnv->DeleteCols(0, gdSummaryEnv->GetNumberCols());
		if (gdSummaryDif->GetNumberCols() > 0)
			gdSummaryDif->DeleteCols(0, gdSummaryDif->GetNumberCols());
		btnSumEditSelEnv->Disable();
		cbSumGroup->Disable();
		return;
	}

	cbSumEnv->Select(selEnv);
	//we have data to work with...
	
	cbSumGroup->Enable();
	cbSumEnv->Enable();
	btnSumEditSelEnv->Enable();
	gdSummaryEnv->Enable();
	gdSummaryDif->Enable();

	//first, we need to know what columns are going to be needed in the environment summary table
	unsigned int expCol = 0;
	unsigned int expRows = 0;
	int lCol, vCol, jCol, iCol, eCol, iinjCol, einjCol, jinjCol, energyCol, dfieldCol, chargeCol, externalCol, sinkCol;
	lCol = vCol = jCol = iCol = eCol = iinjCol = einjCol = jinjCol = energyCol = dfieldCol = chargeCol = externalCol = sinkCol = -1;

	
		

	std::vector<std::string> rowLabels;

	std::vector<SSContactData*>& ssc = sEnv->ContactEnvironment;
	std::vector<int> curContactIDs;
	std::vector<unsigned int> sscIndex;
	for (unsigned int i = 0; i < ssc.size(); ++i) {
		if ((ssc[i]->currentActiveValue & CONTACT_ALLBC) == CONTACT_INACTIVE)
			continue;

		rowLabels.push_back(ssc[i]->ctc->name);
		sscIndex.push_back(i);
		curContactIDs.push_back(ssc[i]->ctcID);

		if (vCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_VOLT_LINEAR)) {
				vCol = expCol;
				expCol++;
			}
		}

		if (jCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR)) {
				jCol = expCol;
				expCol++;
			}
		}

		if (iCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CURRENT_LINEAR)) {
				iCol = expCol;
				expCol++;
			}
		}

		if (eCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_EFIELD_LINEAR)) {
				eCol = expCol;
				expCol++;
			}
		}

		if (iinjCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CURRENT_LINEAR_ED)) {
				iinjCol = expCol;
				expCol++;
			}
		}

		if (einjCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_EFIELD_LINEAR_ED)) {
				einjCol = expCol;
				expCol++;
			}
		}

		if (jinjCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED)) {
				jinjCol = expCol;
				expCol++;
			}
		}

		if (energyCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_MINIMIZE_E_ENERGY)) {
				energyCol = expCol;
				expCol++;
			}
		}

		if (dfieldCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_OPP_DFIELD)) {
				dfieldCol = expCol;
				expCol++;
			}
		}

		if (chargeCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CONSERVE_CURRENT)) {
				chargeCol = expCol;
				expCol++;
			}
		}

		if (externalCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_CONNECT_EXTERNAL)) {
				externalCol = expCol;
				expCol++;
			}
		}

		if (sinkCol == -1) {
			if (TEST_BIT(ssc[i]->currentActiveValue, CONTACT_SINK)) {
				sinkCol = expCol;
				expCol++;
			}
		}
	}

	if (sEnv->ActiveLightEnvironment.size() > sEnv->ContactEnvironment.size())
		expRows = sEnv->ActiveLightEnvironment.size();
	else
		expRows = sEnv->ContactEnvironment.size();

	if (sEnv->ActiveLightEnvironment.size() > 0) {
		lCol = expCol;
		expCol++;
	}

	int deltaCols = expCol - gdSummaryEnv->GetNumberCols();
	if (deltaCols > 0)
		gdSummaryEnv->AppendCols(deltaCols);
	else if (deltaCols < 0)
		gdSummaryEnv->DeleteCols(0, -deltaCols);

	int deltaRows = expRows - gdSummaryEnv->GetNumberRows();
	if (deltaRows > 0)
		gdSummaryEnv->AppendRows(deltaRows);
	else if (deltaRows < 0)
		gdSummaryEnv->DeleteRows(0, -deltaRows);

	// lCol, vCol, jCol, iCol, eCol, iinjCol, einjCol, jinjCol, energyCol, dfieldCol, chargeCol, externalCol, sinkCol;
	if (lCol >= 0)
		gdSummaryEnv->SetColLabelValue(lCol, "Active Spectra");
	if (vCol >= 0)
		gdSummaryEnv->SetColLabelValue(vCol, "Voltage");
	if (jCol >= 0)
		gdSummaryEnv->SetColLabelValue(jCol, "Current Density");
	if (iCol >= 0)
		gdSummaryEnv->SetColLabelValue(iCol, "Current");
	if (eCol >= 0)
		gdSummaryEnv->SetColLabelValue(eCol, "Electric Field");
	if (iinjCol >= 0)
		gdSummaryEnv->SetColLabelValue(iinjCol, "Injection Current");
	if (einjCol >= 0)
		gdSummaryEnv->SetColLabelValue(einjCol, "Injection E-Field");
	if (jinjCol >= 0)
		gdSummaryEnv->SetColLabelValue(jinjCol, "Injection C. Density");
	if (energyCol >= 0)
		gdSummaryEnv->SetColLabelValue(energyCol, "Minimize E-Field Energy");
	if (dfieldCol >= 0)
		gdSummaryEnv->SetColLabelValue(dfieldCol, "Edge D-Field Canceling");
	if (chargeCol >= 0)
		gdSummaryEnv->SetColLabelValue(chargeCol, "Maintain Device Charge");
	if (externalCol >= 0)
		gdSummaryEnv->SetColLabelValue(externalCol, "External Connection");
	if (sinkCol >= 0)
		gdSummaryEnv->SetColLabelValue(sinkCol, "Thermal Sink");

	for (unsigned int i = 0; i < rowLabels.size(); ++i) {
		gdSummaryEnv->SetRowLabelValue(i, rowLabels[i]);

		if (vCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_VOLT_LINEAR)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->voltage;
				gdSummaryEnv->SetCellValue(i, vCol, tmp);
				if (ssc[sscIndex[i]]->suppressVoltagePoisson) {
					gdSummaryEnv->SetCellBackgroundColour(i, vCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, vCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, vCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, vCol, wxColour(255, 255, 255, 255));
			}

		}

		if (jCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->currentDensity;
				gdSummaryEnv->SetCellValue(i, jCol, tmp);
				if (ssc[sscIndex[i]]->suppressCurrentPoisson) {
					gdSummaryEnv->SetCellBackgroundColour(i, jCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, jCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, jCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, jCol, wxColour(255, 255, 255, 255));
			}
			
		}

		if (iCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CURRENT_LINEAR)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->current;
				gdSummaryEnv->SetCellValue(i, iCol, tmp);
				if (ssc[sscIndex[i]]->suppressCurrentPoisson) {
					gdSummaryEnv->SetCellBackgroundColour(i, iCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, iCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, iCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, iCol, wxColour(255, 255, 255, 255));
			}
			
		}

		if (eCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_EFIELD_LINEAR)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->eField;
				gdSummaryEnv->SetCellValue(i, eCol, tmp);
				if (ssc[sscIndex[i]]->suppressEFieldPoisson) {
					gdSummaryEnv->SetCellBackgroundColour(i, eCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, eCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, eCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, eCol, wxColour(255, 255, 255, 255));
			}
			
		}

		if (iinjCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CURRENT_LINEAR_ED)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->current_ED;
				gdSummaryEnv->SetCellValue(i, iinjCol, tmp);

			}

			
		}

		if (einjCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_EFIELD_LINEAR_ED)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->eField_ED;
				gdSummaryEnv->SetCellValue(i, einjCol, tmp);
			}
		
		}

		if (jinjCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED)) {
				wxString tmp;
				tmp << ssc[sscIndex[i]]->currentDensity_ED;
				gdSummaryEnv->SetCellValue(i, jinjCol, tmp);
			}
			
		}

		if (energyCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_MINIMIZE_E_ENERGY)) {
				gdSummaryEnv->SetCellValue(i, energyCol, "X");
				if (ssc[sscIndex[i]]->suppressEFMinPoisson) {
					gdSummaryEnv->SetCellBackgroundColour(i, energyCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, energyCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, energyCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, energyCol, wxColour(255, 255, 255, 255));
			}			
		}

		if (dfieldCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_OPP_DFIELD)) {
				gdSummaryEnv->SetCellValue(i, dfieldCol, "X");
				if (ssc[sscIndex[i]]->suppressOppD) {
					gdSummaryEnv->SetCellBackgroundColour(i, dfieldCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, dfieldCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, dfieldCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, dfieldCol, wxColour(255, 255, 255, 255));
			}
			
		}

		if (chargeCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CONSERVE_CURRENT)) {
				gdSummaryEnv->SetCellValue(i, chargeCol, "X");
				if (ssc[sscIndex[i]]->suppressConserveJ) {
					gdSummaryEnv->SetCellBackgroundColour(i, chargeCol, wxColour(100, 100, 100, 255));
				}
				else
					gdSummaryEnv->SetCellBackgroundColour(i, chargeCol, wxColour(255, 255, 255, 255));
			}
			else {
				gdSummaryEnv->SetCellValue(i, chargeCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, chargeCol, wxColour(255, 255, 255, 255));
			}
			
		}

		if (externalCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_CONNECT_EXTERNAL)) {
				gdSummaryEnv->SetCellValue(i, externalCol, "X");
			}
			else {
				gdSummaryEnv->SetCellValue(i, externalCol, "");
			}
			gdSummaryEnv->SetCellBackgroundColour(i, externalCol, wxColour(255, 255, 255, 255));
			
		}

		if (sinkCol >= 0) {
			if (TEST_BIT(ssc[sscIndex[i]]->currentActiveValue, CONTACT_SINK)) {
				gdSummaryEnv->SetCellValue(i, sinkCol, "X");
			}
			else {
				gdSummaryEnv->SetCellValue(i, sinkCol, "");
			}
			gdSummaryEnv->SetCellBackgroundColour(i, sinkCol, wxColour(255, 255, 255, 255));
		}

	}

	if (lCol >= 0) {
		for (unsigned int i = 0; i < sEnv->ActiveLightEnvironment.size(); ++i) {
			for (unsigned int j = 0; j < mdesc->spectrum.size(); ++j) {
				if (mdesc->spectrum[j]->ID == sEnv->ActiveLightEnvironment[i]) {

					gdSummaryEnv->SetCellValue(i, lCol, mdesc->spectrum[j]->name);
					gdSummaryEnv->SetCellBackgroundColour(i, lCol, wxColour(255, 255, 200, 255));
				}
			}
		}
		//now clear out everything beneath the spectra
		if (sEnv->ActiveLightEnvironment.size() < (unsigned int)gdSummaryEnv->GetNumberRows()) {
			for (int i = sEnv->ActiveLightEnvironment.size(); i < gdSummaryEnv->GetNumberRows(); ++i) {
				gdSummaryEnv->SetCellValue(i, lCol, "");
				gdSummaryEnv->SetCellBackgroundColour(i, lCol, wxColour(255, 255, 255, 255));
			}
		}
	}
	SmartResizeGridColumns(gdSummaryEnv);


	//now for the differences...
	
	
	//we need to identify which columns are different among all of them.
	std::vector<int> allContactIDs;
	
	std::vector< std::vector<ContactCompare> >differences;	//gosh darn it. not cryptic at all.
	//first array index pertains to the particular contact, second is how every single aspect within it is different from the source contact

	for (unsigned int i = 0; i < sumGroups.size(); ++i)
	{
		for (unsigned int j = 0; j < sumGroups[i]->ContactEnvironment.size(); ++j) {
			SSContactData* testCtc = sumGroups[i]->ContactEnvironment[j];
			if (testCtc==nullptr || (testCtc->currentActiveValue & CONTACT_ALLBC) == CONTACT_INACTIVE || testCtc->ctcID < 0)
				continue;	//ignore inactive contacts in the layer.
			if (VectorUniqueAdd(allContactIDs, testCtc->ctcID)) {
				std::vector<ContactCompare> tmp;
				differences.push_back(tmp);	//create the vector that will be associated with all the contact ids.

			}
		}
	}
	//now allContactIDs has a list of what contacts need to be checked in each group.

	std::vector<ContactCompare::ContactValueType> uniqueValueTypes;
	for (unsigned int i = 0; i < allContactIDs.size(); ++i)
	{
		std::vector<ContactCompare>& localDif = differences[i];
		for (unsigned int j = 0; j < sumGroups.size(); ++j) {
			if (sumGroups[j] == sEnv)	//don't compare with self. That's boring and already know the answer...
				continue;
			
			SSContactData *testCtc = sumGroups[j]->getContact(allContactIDs[i]);
			SSContactData *mainCtc = sEnv->getContact(allContactIDs[i]);

			//need to compare four possibilities. If they are both null, then nothing matters.
			//if the main is true and the secondary is not, need to X it.
			//if the main is false and the secondary is true, then just put the different number in
			//if they are both true, then put the number in if the values are different
			bool validTest, validMain;

			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_VOLT_LINEAR) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_VOLT_LINEAR) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::VOLTAGE, false, testCtc->voltage));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::VOLTAGE);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::VOLTAGE, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::VOLTAGE);
			}
			else if (validTest && validMain && testCtc->voltage != mainCtc->voltage) {
				localDif.push_back(ContactCompare(ContactCompare::VOLTAGE, false, testCtc->voltage));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::VOLTAGE);
			}

			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_DENISTY, false, testCtc->currentDensity));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_DENISTY);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_DENISTY, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_DENISTY);
			}
			else if (validTest && validMain && testCtc->currentDensity != mainCtc->currentDensity) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_DENISTY, false, testCtc->currentDensity));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_DENISTY);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CURRENT_LINEAR) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CURRENT_LINEAR) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT, false, testCtc->current));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT);
			}
			else if (validTest && validMain && testCtc->current != mainCtc->current) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT, false, testCtc->current));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_EFIELD_LINEAR) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_EFIELD_LINEAR) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD, false, testCtc->eField));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD);
			}
			else if (validTest && validMain && testCtc->eField != mainCtc->eField) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD, false, testCtc->eField));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CURRENT_LINEAR_ED) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CURRENT_LINEAR_ED) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_ED, false, testCtc->current_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_ED);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_ED, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_ED);
			}
			else if (validTest && validMain && testCtc->current_ED != mainCtc->current_ED) {
				localDif.push_back(ContactCompare(ContactCompare::CURRENT_ED, false, testCtc->current_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CURRENT_ED);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_EFIELD_LINEAR_ED) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_EFIELD_LINEAR_ED) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD_ED, false, testCtc->eField_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD_ED);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD_ED, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD_ED);
			}
			else if (validTest && validMain && testCtc->eField_ED != mainCtc->eField_ED) {
				localDif.push_back(ContactCompare(ContactCompare::EFIELD_ED, false, testCtc->eField_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EFIELD_ED);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::CUR_DENS_ED, false, testCtc->currentDensity_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CUR_DENS_ED);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::CUR_DENS_ED, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CUR_DENS_ED);
			}
			else if (validTest && validMain && testCtc->currentDensity_ED != mainCtc->currentDensity_ED) {
				localDif.push_back(ContactCompare(ContactCompare::CUR_DENS_ED, false, testCtc->currentDensity_ED));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CUR_DENS_ED);
			}
			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_MINIMIZE_E_ENERGY) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_MINIMIZE_E_ENERGY) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::MINIMIZE_EFIELD_ENERGY, false, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::MINIMIZE_EFIELD_ENERGY);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::MINIMIZE_EFIELD_ENERGY, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::MINIMIZE_EFIELD_ENERGY);
			}

			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_OPP_DFIELD) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_OPP_DFIELD) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::OPP_D_FIELD, false, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::OPP_D_FIELD);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::OPP_D_FIELD, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::OPP_D_FIELD);
			}

			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CONSERVE_CURRENT) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CONSERVE_CURRENT) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::CONSERVE_CURRENT, false, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CONSERVE_CURRENT);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::CONSERVE_CURRENT, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::CONSERVE_CURRENT);
			}

			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_CONNECT_EXTERNAL) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_CONNECT_EXTERNAL) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::EXTERNAL, false, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EXTERNAL);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::EXTERNAL, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::EXTERNAL);
			}

			////////////////////////////////////
			validTest = (testCtc != nullptr) ? TEST_BIT(testCtc->currentActiveValue, CONTACT_SINK) : false;
			validMain = (mainCtc != nullptr) ? TEST_BIT(mainCtc->currentActiveValue, CONTACT_SINK) : false;
			if (validTest && !validMain) {
				localDif.push_back(ContactCompare(ContactCompare::SINK, false,0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::SINK);
			}
			else if (validMain && !validTest) {
				localDif.push_back(ContactCompare(ContactCompare::SINK, true, 0.0));
				VectorUniqueAdd(uniqueValueTypes, ContactCompare::SINK);
			}

			
		}
	}
	//so after that mess, unique value types contains the type of each one that may have a difference among ALL the contacts.

	lCol = vCol = jCol = iCol = eCol = iinjCol = einjCol = jinjCol = energyCol = dfieldCol = chargeCol = externalCol = sinkCol = -1;
	expRows = 0;
	expCol = uniqueValueTypes.size();
	int deltaCol = expCol - gdSummaryDif->GetNumberCols();
	if (deltaCol > 0)
		gdSummaryDif->AppendCols(deltaCol, false);
	else if (deltaCol < 0)
		gdSummaryDif->DeleteCols(0, -deltaCol, false);

	//first we need to assign columns to everything now, noting it has the correct number of columns.
	for (unsigned int i = 0; i < uniqueValueTypes.size(); ++i) {
		switch (uniqueValueTypes[i]) {
		case ContactCompare::VOLTAGE:
			vCol = i;
			gdSummaryDif->SetColLabelValue(i, "Voltage");
			break;
		case ContactCompare::CURRENT_DENISTY:
			jCol = i;
			gdSummaryDif->SetColLabelValue(i, "Current Density");
			break;
		case ContactCompare::CURRENT:
			iCol = i;
			gdSummaryDif->SetColLabelValue(i, "Current");
			break;
		case ContactCompare::EFIELD:
			eCol = i;
			gdSummaryDif->SetColLabelValue(i, "Electric Field");
			break;
		case ContactCompare::CURRENT_ED:
			iinjCol = i;
			gdSummaryDif->SetColLabelValue(i, "Injection Current");
			break;
		case ContactCompare::EFIELD_ED:
			einjCol = i;
			gdSummaryDif->SetColLabelValue(i, "Injection E-Field");
			break;
		case ContactCompare::CUR_DENS_ED:
			jinjCol = i;
			gdSummaryDif->SetColLabelValue(i, "Injection C. Density");
			break;
		case ContactCompare::MINIMIZE_EFIELD_ENERGY:
			energyCol = i;
			gdSummaryDif->SetColLabelValue(i, "Minimize E-Field Energy");
			break;
		case ContactCompare::OPP_D_FIELD:
			dfieldCol = i;
			gdSummaryDif->SetColLabelValue(i, "Edge D-Field Canceling");
			break;
		case ContactCompare::CONSERVE_CURRENT:
			chargeCol = i;
			gdSummaryDif->SetColLabelValue(i, "Maintain Device Charge");
			break;
		case ContactCompare::EXTERNAL:
			externalCol = i;
			gdSummaryDif->SetColLabelValue(i, "External Connection");
			break;
		case ContactCompare::SINK:
			sinkCol = i;
			gdSummaryDif->SetColLabelValue(i, "Thermal Sink");
			break;
		default:
			break;
		}
	}
	
	if (gdSummaryDif->GetNumberRows() > 0)
		gdSummaryDif->DeleteRows(0, gdSummaryDif->GetNumberRows());

	int startContactRow = 0;
	int contactNumRows = 1;
	for (unsigned int i = 0; i < differences.size(); ++i) {	//loop through each contact
		if (differences[i].size() == 0)	//this contact had no differences amongst everything
			continue;

		gdSummaryDif->AppendRows();	//create the initial row for the contact
		Contact* ct = mdesc->simulation->getContact(allContactIDs[i]);
		if (ct == nullptr)
			gdSummaryDif->SetRowLabelValue(startContactRow, "Unknown");
		else
			gdSummaryDif->SetRowLabelValue(startContactRow, ct->name);
		
		if (vCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::VOLTAGE, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, vCol, str[i]);

		}

		if (jCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::CURRENT_DENISTY, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, jCol, str[i]);

		}

		if (iCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::CURRENT, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, iCol, str[i]);

		}

		if (eCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::EFIELD, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, eCol, str[i]);

		}

		if (iinjCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::CURRENT_ED, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, iinjCol, str[i]);

		}

		if (einjCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::EFIELD_ED, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, einjCol, str[i]);

		}

		if (jinjCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::CUR_DENS_ED, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, jinjCol, str[i]);

		}

		if (energyCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::MINIMIZE_EFIELD_ENERGY, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, energyCol, str[i]);

		}

		if (dfieldCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::OPP_D_FIELD, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, dfieldCol, str[i]);

		}

		if (chargeCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::CONSERVE_CURRENT, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, chargeCol, str[i]);

		}

		if (externalCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::EXTERNAL, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, externalCol, str[i]);

		}

		if (sinkCol >= 0) {
			wxArrayString str = buildDifferenceString(ContactCompare::SINK, differences[i]);
			UpdateDifTableRows(str, contactNumRows);
			for (unsigned int i = 0; i < str.GetCount(); ++i)
				gdSummaryDif->SetCellValue(startContactRow + i, sinkCol, str[i]);

		}


		startContactRow += contactNumRows;
		contactNumRows = 1;
	}

	//now time to check the light for differences among the environments
	std::vector<int> AllLightSources;
	for (unsigned int i = 0; i < ss->ExternalStates.size(); ++i) {
		for (unsigned int j = 0; j < ss->ExternalStates[i]->ActiveLightEnvironment.size(); ++j) {
			VectorUniqueAdd(AllLightSources, ss->ExternalStates[i]->ActiveLightEnvironment[j]);
		}
	}

	int lightRow = 0;
	for (unsigned int i = 0; i < AllLightSources.size(); ++i) {
		LightSource* src = mdesc->getSpectra(AllLightSources[i]);
		if (src == nullptr)
			continue;
		wxString cellValue = src->name;

		unsigned int counter = 0;
		if (sEnv->hasSpectra(AllLightSources[i])) {
			//looking for environments that do not have it
			cellValue << "(NO: x";
			
			for (unsigned int j = 0; j < sumGroups.size(); ++j) {
				if (j == selEnv)
					continue;
				if (sumGroups[j]->hasSpectra(AllLightSources[i]) == false)
					counter++;
			}
		}
		else { //the current doesn't, so count all those that do
			cellValue << "(YES: x";
			for (unsigned int j = 0; j < sumGroups.size(); ++j) {
				if (j== selEnv)
					continue;
				if (sumGroups[j]->hasSpectra(AllLightSources[i]) == true)
					counter++;
			}
		}
		if (counter == 0)	//nothing is different
			continue;
		cellValue << counter << ")";
		if (lightRow == 0) {	//hasn't been updated yet!
			lCol = gdSummaryDif->GetNumberCols();
			gdSummaryDif->AppendCols();
			gdSummaryDif->SetColLabelValue(lCol, "Spectra");

		}
		if (lightRow >= gdSummaryDif->GetNumberRows())	//make sure there is a spot!
			gdSummaryDif->AppendRows();	//light row starts at 0, so adding 1 will always be safe

		gdSummaryDif->SetCellValue(lightRow, lCol, cellValue);
		gdSummaryDif->SetCellBackgroundColour(lightRow, lCol, wxColour(255, 255, 200, 255));
		lightRow++;
	}
	
	SmartResizeGridColumns(gdSummaryDif);
	int w = PanelSummary->GetSize().GetWidth() - 4;
	stDifExplain->SetSize(w, -1);
	stDifExplain->Wrap(w);

	wxString groupTitle = "";
	groupTitle << cbSumGroup->GetStringSelection() << ": " << cbSumEnv->GetStringSelection();
	stSumMainTitle->SetLabel(groupTitle);

	if (gdSummaryDif->GetNumberRows() == 0) {
		szrPrimarySummary->Hide(gdSummaryDif);
		szrPrimarySummary->Hide(stSumDifTitle);
		
	}
	else {
		szrPrimarySummary->Show(gdSummaryDif);
		szrPrimarySummary->Show(stSumDifTitle);
	}

	szrPrimarySummary->Layout();
}

void simSteadyStateBook::UpdateDifTableRows(wxArrayString& strs, int& curNumRows)
{
	if (!isInitialized())
		return;
	int needMore = strs.GetCount() - curNumRows;
	if (needMore > 0) {
		gdSummaryDif->AppendRows(needMore);
		curNumRows += needMore;
		for (int i = gdSummaryDif->GetNumberRows() - needMore; i < gdSummaryDif->GetNumberRows(); ++i)
			gdSummaryDif->SetRowLabelValue(i, "");
	}
	//if 1 row, and need 2 total, needMore=1, appends 1, curNum=2, i_start = 2-1=1, which is the first index added, set the row label to nothing
	//logic checks
}

wxArrayString simSteadyStateBook::buildDifferenceString(ContactCompare::ContactValueType type, std::vector<ContactCompare>& data)
{
	std::vector<double> dat;
	std::vector<double> deltas;
	std::vector<bool> nullified;
	for (unsigned int i = 0; i < data.size(); ++i) {
		if (data[i].type == type) {
			dat.push_back(data[i].value);
			nullified.push_back(data[i].madeNull);
			if (dat.size() > 1) {
				deltas.push_back(dat.back() - dat[dat.size()-2]);
			}
		}
	}
	wxArrayString retStr;
	if (dat.size() == 0) {
		retStr.Add("Same");
		return(retStr);
	}
	wxString tmpVal;

	if (type > ContactCompare::MAX_VALUED_TYPES) {
		//actually, because it only does differences, these are all going to be the same because it's a boolean value being tested.
		if (nullified.size() > 0) {
			if (nullified[0]) {
				tmpVal = "Inactive (";
				tmpVal << nullified.size() << ")";
				retStr.Add(tmpVal);
			}
			else {
				tmpVal = "Active (";
				tmpVal << nullified.size() << ")";
				retStr.Add(tmpVal);
			}
		}
	}
	else {
		if (dat.size() <= 3) {	//guaranteed to at least be 1 because it didn't return
			if (nullified[0])
				tmpVal << "X";
			else
				tmpVal << dat[0];
			if (dat.size() >= 2) {
				if (nullified[1])
					tmpVal << ", X";
				else
					tmpVal << ", " << dat[1];
			}
			if (dat.size() >= 3) {
				if (nullified[2])
					tmpVal << ", X";
				else
					tmpVal << ", " << dat[2];
			}
			retStr.Add(tmpVal);
			return retStr;
		}
		else {	//there is enough data to justify a start/stop/increment format
			int startDelta = 0;
			int stopDelta = 0;
			for (unsigned int i = 0; i < dat.size() - 1; ++i) {
				if (!nullified[i] && !nullified[i + 1])
				{
					if (i == deltas.size()-1 || 
						(DoubleEqual(deltas[i], deltas[i + 1], 1.0e-9) == false && nullified[i+2]==false))
					{
						stopDelta = i;
						tmpVal = "";
						tmpVal << dat[startDelta] << " to " << dat[stopDelta + 1] << "(" << deltas[startDelta] << ")";
						retStr.Add(tmpVal);
						startDelta = i + 1;
					}
				}
				else if (!nullified[i] && nullified[i + 1]) {
					//force a break!
					stopDelta = i;
					tmpVal = "";
					tmpVal << dat[startDelta] << " to " << dat[stopDelta] << "(" << deltas[startDelta] << ")";
					retStr.Add(tmpVal);
					startDelta = i + 1;
					if (i == dat.size() - 2) {
						tmpVal = "";
						tmpVal << "X (" << (i+1) << ")";	//convert counter from computer 0 to human 1
						retStr.Add(tmpVal);
					}
				}
				else if (nullified[i] && !nullified[i + 1]) {
					//coming off a nullified value! Force a break.
					stopDelta = i;
					tmpVal = "X (";
					tmpVal << (startDelta + 1) << " to " << (stopDelta + 1) << ")";
					retStr.Add(tmpVal);
					startDelta = i + 1;
					if (i == dat.size() - 2) {
						tmpVal = "";
						tmpVal << dat.back();
						retStr.Add(tmpVal);
					}
				}
				else if (nullified[i] && nullified[i + 1]) {
					if (i == dat.size() - 2) {
						tmpVal = "X (";
						tmpVal << (startDelta + 1) << " to " << (i + 2) << ")";	//+1 to get to last nullified. +1 for human readable index.
						retStr.Add(tmpVal);
					}
				}
			}
		}

	}
	return retStr;
}



void simSteadyStateBook::UpdateGUIValuesDefine(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	
	if (s == nullptr)	//very highly unlikely. Would have to run out of memory... which leads to bigger problems.
	{  //but NEVER ASSUME!
		cbGroupChooser->Disable();
		cbEnvChooser->Disable();
		cbAvailSpectraChooser->Disable();
		cbAvailContactChooser->Disable();
		cbChangeAll->Disable();
		cbQuickAddVariableList->Disable();
		cbSolMethods->Disable();

		//top row
		btnNewGroup->Disable();
		btnRemoveGroup->Disable();
		btnSaveGroup->Disable();
		btnLoadGroup->Disable();
		btnRenameGroup->Disable();
		btnDuplicateGroup->Disable();

		//top define row
		btnMoveEnvUp->Disable();
		btnMoveEnvDown->Disable();
		btnNewEnv->Disable();
		btnRemoveEnv->Disable();
		btnQuickEnvDuplicate->Disable();

		btnAddSpectra->Disable();
		btnRemoveSpectra->Disable();
		btnAddContact->Disable();
		btnRemoveContact->Disable();

		btnAddSolutionMethod->Disable();
		btnReplaceSolutionMethod->Disable();
		btnSoluptionUp->Disable();
		btnSolutionDown->Disable();
		btnRemoveSolutionMethod->Disable();
		btnChangeGroup->Disable();

		gdSolutions->Disable();
		
		txtQuickAddFirst->Disable();
		txtQuickAddLast->Disable();
		txtQuickAddNumber->Disable();

		txtCtcV->Disable();
		txtCtcI->Disable();
		txtCtcJ->Disable();
		txtCtcE->Disable();
		txtCtcI_ED->Disable();
		txtCtcE_ED->Disable();
		txtCtcJ_ED->Disable();

		cbCtcV->Disable();
		cbCtcI->Disable();
		cbCtcJ->Disable();
		cbCtcE->Disable();
		cbCtcI_ED->Disable();
		cbCtcE_ED->Disable();
		cbCtcJ_ED->Disable();

		cbCtcExternal->Disable();
		cbCtcSink->Disable();

		cbCtcMinEField->Disable();
		cbCtcOppDField->Disable();
		cbCtcChargeNeutral->Disable();

		lbSpectraUse->Disable();
		lbContactUse->Disable();

		txtStartTol->Disable();
		txtLastTol->Disable();

		stSuppressV->Disable();
		stSupressC->Disable();
		stSupressEfPoisson->Disable();
		stSupressEfMinimize->Disable();
		stSupressOppDField->Disable();
		stSupressCtcChargeNeutral->Disable();
		btnChangeGroup->Disable();

		btnAddSolutionMethod->Disable();
		btnReplaceSolutionMethod->Disable();
		btnSoluptionUp->Disable();
		btnSolutionDown->Disable();
		btnRemoveSolutionMethod->Disable();

		return;
	}
	ModelDescribe* mdesc = s->getSim()->mdesc;
	//now we know s works, and following this route, mdesc better work too!

	//all we care about is enabling or disabling shit. Nothing about data transfers! Assuming all the data is up to date.
	cbGroupChooser->Enable((s->ExternalStates.size() > 0));
	cbEnvChooser->Enable(GroupEnvs.size() > 0);

	if (curGroup != "")
	{
		btnRemoveGroup->Enable();
		btnSaveGroup->Enable();
		btnRenameGroup->Enable();
		btnDuplicateGroup->Enable();
		btnNewEnv->Enable();
		cbEnvChooser->Enable();
	}
	else
	{
		btnRemoveGroup->Disable();
		btnSaveGroup->Disable();
		btnRenameGroup->Disable();
		btnDuplicateGroup->Disable();
		btnNewEnv->Disable();
		cbEnvChooser->Disable();
	}
	
	if (curEnv) {
		btnRemoveEnv->Enable();
		btnChangeGroup->Enable();

		cbChangeAll->Enable();
		unsigned int index = -1;
		for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
			if (GroupEnvs[i] == curEnv) {
				index = i;
				break;
			}
		}

		if (index < GroupEnvs.size()) {	//damn better be
			btnMoveEnvUp->Enable(index > 0);	//note, down actually means higher indices, and up to lower indices
			btnMoveEnvDown->Enable(index < GroupEnvs.size() - 1);
		}
		else {
			btnMoveEnvDown->Disable();
			btnMoveEnvUp->Disable();
		}

		lbContactUse->Enable();
		lbSpectraUse->Enable();	//may not be anything in them, but they will be enabled!

		//if curEnv is true, then the list box sizes are reliable!
		int availSpec = cbAvailSpectraChooser->GetCount();
		cbAvailSpectraChooser->Enable(availSpec > 0);
		btnAddSpectra->Enable(availSpec > 0);

		int availContacts = cbAvailContactChooser->GetCount();
		cbAvailContactChooser->Enable(availContacts > 0);
		btnAddContact->Enable(availContacts > 0);

		btnRemoveSpectra->Enable(curLight != nullptr);
		btnRemoveContact->Enable(curContactData != nullptr);

		
		cbQuickAddVariableList->Enable();
		txtQuickAddFirst->Enable();
		txtQuickAddLast->Enable();
		txtQuickAddNumber->Enable();

		long num;
		double first, last;
		bool good = true;
		if (txtQuickAddFirst->GetValue().ToDouble(&first) == false)
			good = false;
		else if (txtQuickAddLast->GetValue().ToDouble(&last) == false)
			good = false;
		else if (txtQuickAddNumber->GetValue().ToCLong(&num) == false)
			good = false;

		if (good) //there was data in everything!
		{
			if (num <= 0)
				good = false; //need to do at least 1
		}
		btnQuickEnvDuplicate->Enable(good);

	}
	else {
		btnRemoveEnv->Disable();
		cbChangeAll->Disable();
		btnMoveEnvDown->Disable();
		btnMoveEnvUp->Disable();
		lbContactUse->Disable();
		lbSpectraUse->Disable();
		cbAvailSpectraChooser->Disable();
		cbAvailContactChooser->Disable();
		btnRemoveSpectra->Disable();
		btnRemoveContact->Disable();
		btnAddContact->Disable();
		btnAddSpectra->Disable();
		btnQuickEnvDuplicate->Disable();
		cbQuickAddVariableList->Disable();
		txtQuickAddFirst->Disable();
		txtQuickAddLast->Disable();
		txtQuickAddNumber->Disable();
		btnChangeGroup->Disable();
	}

	if (curContactData)
	{
		cbCtcV->Enable();
		cbCtcI->Enable();
		cbCtcJ->Enable();
		cbCtcE->Enable();
		cbCtcI_ED->Enable();
		cbCtcE_ED->Enable();
		cbCtcJ_ED->Enable();

		cbCtcExternal->Enable();
		cbCtcSink->Enable();

		cbCtcMinEField->Enable();
		cbCtcOppDField->Enable();
		cbCtcChargeNeutral->Enable();

		//all checkboxes enabled by default!
		//now only allow editing the text boxes if their associated checkbox is checked.
		txtCtcV->Enable(cbCtcV->GetValue());
		txtCtcI->Enable(cbCtcI->GetValue());
		txtCtcJ->Enable(cbCtcJ->GetValue());
		txtCtcE->Enable(cbCtcE->GetValue());
		txtCtcI_ED->Enable(cbCtcI_ED->GetValue());
		txtCtcE_ED->Enable(cbCtcE_ED->GetValue());
		txtCtcJ_ED->Enable(cbCtcJ_ED->GetValue());

		
		stSuppressV->Enable(curContactData->suppressVoltagePoisson);
		stSupressC->Enable(curContactData->suppressCurrentPoisson);
		stSupressEfPoisson->Enable(curContactData->suppressEFieldPoisson);
		stSupressEfMinimize->Enable(curContactData->suppressEFMinPoisson);
		stSupressOppDField->Enable(curContactData->suppressOppD);
		stSupressCtcChargeNeutral->Enable(curContactData->suppressConserveJ);
		
	}
	else
	{
		txtCtcV->Disable();
		txtCtcI->Disable();
		txtCtcJ->Disable();
		txtCtcE->Disable();
		txtCtcI_ED->Disable();
		txtCtcE_ED->Disable();
		txtCtcJ_ED->Disable();

		cbCtcV->Disable();
		cbCtcI->Disable();
		cbCtcJ->Disable();
		cbCtcE->Disable();
		cbCtcI_ED->Disable();
		cbCtcE_ED->Disable();
		cbCtcJ_ED->Disable();

		cbCtcExternal->Disable();
		cbCtcSink->Disable();

		cbCtcMinEField->Disable();
		cbCtcOppDField->Disable();
		cbCtcChargeNeutral->Disable();

		stSuppressV->Disable();
		stSupressC->Disable();
		stSupressEfPoisson->Disable();
		stSupressEfMinimize->Disable();
		stSupressOppDField->Disable();
		stSupressCtcChargeNeutral->Disable();
	}


	txtStartTol->Enable();
	txtLastTol->Enable();
	cbSolMethods->Enable();
	double tolStart, tolEnd;
	bool good = true;
	if (txtStartTol->GetValue().ToDouble(&tolStart) == false)
		good = false;
	else if (txtLastTol->GetValue().ToDouble(&tolEnd) == false)
		good = false;

	if (good)
	{
		if (tolStart <= tolEnd)
			good = false;
	}
	btnAddSolutionMethod->Enable(good);

	if (RemoveStepRow < s->getNumberCalcSteps())
	{
		btnRemoveSolutionMethod->Enable();
		btnReplaceSolutionMethod->Enable(good);
		//know it's selected. now the question is can we move it down? Down means getting a larger index
		btnSoluptionUp->Enable(RemoveStepRow > 0);
		btnSolutionDown->Enable(RemoveStepRow < s->getNumberCalcSteps() - 1);
	}
	else
	{
		btnRemoveSolutionMethod->Disable();
		btnReplaceSolutionMethod->Disable();
		btnSoluptionUp->Disable();
		btnSolutionDown->Disable();
	}

	
	
	

}

void simSteadyStateBook::TransferToGUIDefine()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	SS_SimData *s = getSSSim();
	ModelDescribe* mdesc = getMdesc();
	if (s == nullptr || mdesc==nullptr)	//can't do jack diddly squat
	{
		UpdateGUIValuesDefine(wxCommandEvent());
		return;
	}
	VerifyInternalVars();
	s->Validate();
	
	//first, update the groups... sorta
	std::vector<std::string> groups;
	GroupEnvs.clear();
	unsigned int curGroupIndex = -1;
	unsigned int curEnvIndex = -1;

	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
	{
		s->ExternalStates[i]->SelfContainedValidate();	//makes sure all the data/pointers are valid!

		if (VectorUniqueAdd(groups, s->ExternalStates[i]->FileOut))
		{
			if (groups.back() == curGroup)
				curGroupIndex = groups.size() - 1;
		}
		if (s->ExternalStates[i]->FileOut == curGroup)
		{
			if (s->ExternalStates[i] == curEnv)
				curEnvIndex = GroupEnvs.size();

			GroupEnvs.push_back(s->ExternalStates[i]);
		}
	}

	cbGroupChooser->Clear();
	for (unsigned int i = 0; i < groups.size(); ++i)
		cbGroupChooser->Append(groups[i]);

	if (groups.size() == 0)
		cbGroupChooser->Append("Create a new group first!");

	if (curGroupIndex < groups.size())
		cbGroupChooser->Select(curGroupIndex);
	else
	{
		cbGroupChooser->Select(0);	//either select no message, or select a brand stankin' new group because the old one no longer exists!
		if (groups.size() > 0)	//this is just a new group
		{
			curGroup = groups[0];	//new group assignment
			//which also means I have to search for new ones again... and there's guaranteed to be at least one.
			GroupEnvs.clear();
			for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
			{
				if (s->ExternalStates[i]->FileOut == curGroup)
				{
					GroupEnvs.push_back(s->ExternalStates[i]);
				}
			}
			curEnvIndex = 0;
			curEnv = GroupEnvs[0];
		}
		else
			curGroup = "";	//clear out the group
	}

	//if a group was selected, there's gotta be at least one option in groupenvs.
	cbEnvChooser->Clear();
	if (groups.size() > 0)
	{
		for (unsigned int i = 0; i < GroupEnvs.size(); ++i)
			cbEnvChooser->AppendString(std::to_string((i + 1)));;
	}
	else
		cbEnvChooser->AppendString("N/A");

	if (curEnvIndex < GroupEnvs.size())
	{
		cbEnvChooser->Select(curEnvIndex);
		curEnv = GroupEnvs[curEnvIndex];
	}
	else if (GroupEnvs.size() > 0) {
		cbEnvChooser->Select(0);
		curEnv = GroupEnvs[0];
	}
	else
	{
		cbEnvChooser->Select(0);	//display error message
		curEnv = nullptr;
		curContactData = nullptr;	//by extension...
		curLight = nullptr;
	}

	//so at this point, GroupEnvs is defined, curGroup is defined, curEnv is defined.

	//but first, start taking care of the global variables - the solution algorithms.
	unsigned int solSize = s->getNumberCalcSteps();
	int delta = solSize - gdSolutions->GetNumberRows();
	if (delta > 0)
		gdSolutions->AppendRows(delta);
	else if (delta < 0)
		gdSolutions->DeleteRows(0, -delta);

	if (solSize > 0)
	{
		int which;
		double init, end;
		wxString tmpName, tmpInit, tmpEnd;
		s->SetCalcIndex();	//get it started at zero.
		which = s->GetCalcType(false);
		init = s->GetCalcInitAccuracy();
		end = s->GetCalcEndAccuracy();

		tmpInit << init;
		tmpEnd << end;
		tmpName = GetSSMethodName(which);

		gdSolutions->SetCellValue(0, 0, tmpName);
		gdSolutions->SetCellValue(0, 1, tmpInit);
		gdSolutions->SetCellValue(0, 2, tmpEnd);

		for (unsigned int row = 1; row < solSize; ++row)
		{
			which = s->GetCalcType(true);	//true moves it to the next index (stored internally cuz I'm an idiot like that)
			init = s->GetCalcInitAccuracy();
			end = s->GetCalcEndAccuracy();
			tmpInit = tmpEnd = wxEmptyString;
			tmpInit << init;
			tmpEnd << end;
			tmpName = GetSSMethodName(which);

			gdSolutions->SetCellValue(row, 0, tmpName);
			gdSolutions->SetCellValue(row, 1, tmpInit);
			gdSolutions->SetCellValue(row, 2, tmpEnd);
		}
		SmartResizeGridColumns(gdSolutions);
	}

	if (RemoveStepRow < solSize)
	{
		wxString tmp = "Remove ";
		tmp << (RemoveStepRow+1);
		btnRemoveSolutionMethod->SetLabel(tmp);
		tmp = "Replace ";
		tmp << (RemoveStepRow+1);
		btnReplaceSolutionMethod->SetLabel(tmp);
	}

	if (curEnv)
	{
		//test updating the light
		cbAvailSpectraChooser->Clear();
		lbSpectraUse->Clear();
		for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i)
		{
			bool active = false;
			for (unsigned int j = 0; j < curEnv->ActiveLightEnvironment.size(); ++j)
			{
				if (mdesc->spectrum[i]->ID == curEnv->ActiveLightEnvironment[j])
				{
					active = true;
					break;
				}
			}
			if (active) {
				lbSpectraUse->AppendString(mdesc->spectrum[i]->name);
				if (curLight == nullptr)
					curLight = mdesc->spectrum[i];
			}
			else
				cbAvailSpectraChooser->AppendString(mdesc->spectrum[i]->name);

			if (curLight == mdesc->spectrum[i])
				lbSpectraUse->Select(lbSpectraUse->GetCount() - 1);
		}

		if (cbAvailSpectraChooser->GetCount() > 0)
			cbAvailSpectraChooser->Select(0);

		//now test updating the contact
		//similar setup to before...
		cbAvailContactChooser->Clear();
		lbContactUse->Clear();
		for (unsigned int i = 0; i < mdesc->simulation->contacts.size(); ++i) {
			unsigned int sel = -1;
			for (unsigned int j = 0; j < curEnv->ContactEnvironment.size(); ++j) {
				if (mdesc->simulation->contacts[i] == curEnv->ContactEnvironment[j]->ctc)
				{
					sel = j;
					break;
				}
			}
			if (sel < curEnv->ContactEnvironment.size())
			{
				lbContactUse->AppendString(mdesc->simulation->contacts[i]->name);
				if (curEnv->ContactEnvironment[sel] == curContactData)
					lbContactUse->Select(lbContactUse->GetCount() - 1);
				else if (curContactData == nullptr) {
					curContactData = curEnv->ContactEnvironment[sel];
					lbContactUse->Select(lbContactUse->GetCount() - 1);
				}
			}
			else
				cbAvailContactChooser->AppendString(mdesc->simulation->contacts[i]->name);
		}

		if (cbAvailContactChooser->GetCount() > 0)
			cbAvailContactChooser->Select(0);

		

		int messages = curEnv->SelfContainedValidate();	//redundant... but want to make sure this is done on the correct one. Could have changed
		if (messages == ENV_CONT_VALID_GOOD)
			stValidateMessage->SetLabel(wxT("No issues found for this environment"));
		else
		{
			wxString message;
			if (TEST_BIT(messages, ENV_CONT_VALID_NOTPOSSIBLE))
				message << "Impossible to determine issues for environment. ";
			if (TEST_BIT(messages, ENV_CONT_VALID_NEED_MORE))
				message << "Needs more boundary conditions. ";
			if (TEST_BIT(messages, ENV_CONT_VALID_NO_VOLT))
				message << "Requires voltage. ";
			if (TEST_BIT(messages, ENV_CONT_VALID_TOO_MUCH))
				message << "Too many boundary conditions. ";
			if (TEST_BIT(messages, ENV_CONT_VALID_OVERLAP))
				message << "Conflicting overlapping boundary conditions. ";
			stValidateMessage->SetLabel(message);
			stValidateMessage->Wrap(stValidateMessage->GetClientSize().GetWidth());
		}
		szrMidColumn->Layout();
	}
	else
	{
		//there was no environment, so clear out some boxes!
		//the environment chooser is already done. That's how we know there's nothing here!
		cbAvailSpectraChooser->Clear();
		cbAvailSpectraChooser->AppendString("Need Environment");
		cbAvailContactChooser->Clear();
		cbAvailContactChooser->AppendString("Need Environment");
		cbAvailSpectraChooser->Select(0);
		cbAvailContactChooser->Select(0);
		lbSpectraUse->Clear();
		lbContactUse->Clear();
		curLight = nullptr;
		
		curContactData = nullptr;	//make sure this is going to fail no matter what if there's no curEnv.
		
		stValidateMessage->SetLabel(wxT("Indeterminable"));
	}
		
	//at this point, light should be set. The contacts should have all their data up...
	//the overall suppression has been modified
	if (curContactData)
	{
		//remember. ONLY care about assigning data. Not enabling/disabling.
		int active = curContactData->currentActiveValue;
		if (TEST_BIT(active, CONTACT_MINIMIZE_E_ENERGY)) {
			cbCtcMinEField->SetValue(true);
		}
		else {
			cbCtcMinEField->SetValue(false);
		}

		if (TEST_BIT(active, CONTACT_OPP_DFIELD)) {
			cbCtcOppDField->SetValue(true);
		}
		else {
			cbCtcOppDField->SetValue(false);
		}

		if (TEST_BIT(active, CONTACT_CONSERVE_CURRENT)) {
			cbCtcChargeNeutral->SetValue(true);
		}
		else {
			cbCtcChargeNeutral->SetValue(false);
		}

		if (TEST_BIT(active, CONTACT_VOLT_LINEAR)) {
			cbCtcV->SetValue(true);
			ValueToTBox(txtCtcV, curContactData->voltage);
		}
		else {
			cbCtcV->SetValue(false);
			txtCtcV->Clear();
		}

		if (TEST_BIT(active, CONTACT_EFIELD_LINEAR)) {
			cbCtcE->SetValue(true);
			ValueToTBox(txtCtcE, curContactData->eField);
		}
		else {
			cbCtcE->SetValue(false);
			txtCtcE->Clear();
		}

		if (TEST_BIT(active, CONTACT_CURRENT_LINEAR)) {
			cbCtcI->SetValue(true);
			ValueToTBox(txtCtcI, curContactData->current);
		}
		else {
			cbCtcI->SetValue(false);
			txtCtcI->Clear();
		}

		if (TEST_BIT(active, CONTACT_CUR_DENSITY_LINEAR)) {
			cbCtcJ->SetValue(true);
			ValueToTBox(txtCtcJ, curContactData->currentDensity);
		}
		else {
			cbCtcJ->SetValue(false);
			txtCtcJ->Clear();
		}

		if (TEST_BIT(active, CONTACT_EFIELD_LINEAR_ED)) {
			cbCtcE_ED->SetValue(true);
			ValueToTBox(txtCtcE_ED, curContactData->eField_ED);
		}
		else {
			cbCtcE_ED->SetValue(false);
			txtCtcE_ED->Clear();
		}

		if (TEST_BIT(active, CONTACT_CURRENT_LINEAR_ED)) {
			cbCtcI_ED->SetValue(true);
			ValueToTBox(txtCtcI_ED, curContactData->current_ED);
		}
		else {
			cbCtcI_ED->SetValue(false);
			txtCtcI_ED->Clear();
		}

		if (TEST_BIT(active, CONTACT_CUR_DENSITY_LINEAR_ED)) {
			cbCtcJ_ED->SetValue(true);
			ValueToTBox(txtCtcJ_ED, curContactData->currentDensity_ED);
		}
		else {
			cbCtcJ_ED->SetValue(false);
			txtCtcJ_ED->Clear();
		}

		if (TEST_BIT(active, CONTACT_SINK)) {
			cbCtcSink->SetValue(true);
		}
		else {
			cbCtcSink->SetValue(false);
		}

		if (TEST_BIT(active, CONTACT_CONNECT_EXTERNAL)) {
			cbCtcExternal->SetValue(true);
		}
		else {
			cbCtcExternal->SetValue(false);
		}

		//if there is data in there... who cares!
		stSuppressV->Enable(curContactData->suppressVoltagePoisson);
		stSupressC->Enable(curContactData->suppressCurrentPoisson);
		stSupressEfPoisson->Enable(curContactData->suppressEFieldPoisson);
		stSupressEfMinimize->Enable(curContactData->suppressEFMinPoisson);
		stSupressOppDField->Enable(curContactData->suppressOppD);
		stSupressCtcChargeNeutral->Enable(curContactData->suppressConserveJ);

		if (curContactData->ctc)
			stContactName->SetLabel(curContactData->ctc->name);
		else
			stContactName->SetLabel("Needs contact Assignment");
	}
	else
	{
		stContactName->SetLabel("Unknown Contact");
	}

	UpdateGUIValuesDefine(wxCommandEvent());
	

}

wxString simSteadyStateBook::GetSSMethodName(int type)
{
	wxString tmpName;
	switch (type)
	{
	case SS_NO_SRH:
		tmpName = "Sweep Currents";
		break;
	case SS_MATCH_IMPS:
		tmpName = "Sweep Defects";
		break;
	case SS_ALL:
		tmpName = "Sweep All";
		break;
	case SS_SLOWPTS:
		tmpName = "Target Slow points";
		break;
	case SS_TRANSLIKE:
		tmpName = "Transient - ish";
		break;
	case SS_NEWTON_NOLIGHT:
		tmpName = "Simple Newton, No Light";
		break;
	case SS_NEWTON_ALL:
		tmpName = "Simple Newton";
		break;
	case SS_NEWTON_NL_FULL:
		tmpName = "Full Newton, No Light";
		break;
	case SS_NEWTON_L_FULL:
		tmpName = "Full Newton, Light";
		break;
	case SS_TRANS_ACTUAL:
		tmpName = "Transient";
		break;
	case SS_SMOOTH_BAD:
		tmpName = "Smooth";
		break;
	case SS_MONTECARLO:
		tmpName = "Monte Carlo";
		break;
	case SS_NEWTON_NUMERICAL:
		tmpName = "Numerical Newton";
		break;
	case SS_TRANS_CUTOFF:
		tmpName = "Modified Transient";
		break;
	default:
		tmpName = "Unknown Method";
	}


	return(tmpName);
}


void simSteadyStateBook::NewGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter the group's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	std::string nm = entry.GetValue().ToStdString();
	if (nm == "")
		return;

	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
	{
		if (s->ExternalStates[i]->FileOut == nm)
		{
			wxString message = nm + " already is a group name!";
			wxDialog bad(this, wxID_ANY, message);
			bad.ShowModal();
			return;
		}
	}

	Environment *env = new Environment(s);
	env->FileOut = nm;
	env->id = -1;	//don't know what this hsould be
	s->ExternalStates.push_back(env);	//it's now a part of the simulation
	s->UpdateUnknownIDs();	//update the id to be somethign unique
	curEnv = env;		//going to show this
	curLight = nullptr;	//it has no other data...
	curContactData = nullptr;
	curGroup = nm;	//this is now the current group that we'll be targeting
	GroupEnvs.clear();	//get rid of any old baggage

	TransferToGUIDefine();	//update everything!
}

void simSteadyStateBook::RemoveGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curGroup == "")
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;

	//first, let's see what the index is
	int nextGroupIndex = cbGroupChooser->GetSelection() - 1;	//less logic if you do the one before... you know the one before exists as long as it wasn't the first.
	if (nextGroupIndex == -1)	//can't go before the first, so just grab the next
		nextGroupIndex = 1;

	//delete all that schtuff!
	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
	{
		if (s->ExternalStates[i]->FileOut == curGroup)
		{
			delete s->ExternalStates[i];
			s->ExternalStates.erase(s->ExternalStates.begin() + i);
			--i;
			continue;
		}
	}

	if (nextGroupIndex >= 0 && (unsigned int)nextGroupIndex < cbGroupChooser->GetCount())
		curGroup = cbGroupChooser->GetString(nextGroupIndex).ToStdString();
	else
		curGroup = "";

	GroupEnvs.clear();
	curEnv = nullptr;
	curLight = nullptr;
	curContactData = nullptr;
	getMdesc()->Validate();
	TransferToGUIDefine();
}

void simSteadyStateBook::VerifyInternalVars() {
	if (!isInitialized())
		return;
	SS_SimData *ss = getSSSim();
	ModelDescribe *mdesc = getMdesc();
	//first verify a valid group
	bool good1 = false;
	bool good2 = false;
	bool good3 = false;
	for (unsigned int i = 0; i < ss->ExternalStates.size(); ++i) {
		if (ss->ExternalStates[i]->FileOut == curGroup)
			good1 = true;

		if (ss->ExternalStates[i]->FileOut == sumCGroup)
			good3 = true;

		if (curEnv == ss->ExternalStates[i])
			good2 = true;
	}
	if (!good1)
		curGroup = "";
	if (!good2)
		curEnv = nullptr;
	if (!good3) {
		sumCGroup = "";
		sumGroups.clear();
	}
	
	good1 = good2 = false;
	if (curLight) {
		for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
			if (mdesc->spectrum[i] == curLight) {
				good1 = true;
				break;
			}
		}
		if (!good1)
			curLight = nullptr;
	}

	if (curContactData && curEnv == nullptr)
		curContactData = nullptr;
	else if (curEnv && curContactData) {
		good1 = false;
		for (unsigned int i = 0; i < curEnv->ContactEnvironment.size(); ++i) {
			if (curEnv->ContactEnvironment[i] == curContactData) {
				good1 = true;
				break;
			}
		}
		if (!good1)
			curContactData = nullptr;
	}
}

void simSteadyStateBook::SaveAll(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *ss = getSSSim();
	if (ss == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Steady State files (*.ssf)|*.ssf"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	//it should be entirely up to date... transfer to mdesc won't do much good!

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		ss->SaveToXML(oFile);
		oFile.closeFile();

	}

}

void simSteadyStateBook::LoadGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Steady State files (*.ssf)|*.ssf"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	SS_SimData* ss = getSSSim();
	Simulation* sim = ss->getSim();
	unsigned int contactStartAdd = sim->contacts.size();
	unsigned int startIndexEnvironments = ss->ExternalStates.size();
	LoadSteadyStateChildren(file, *mdesc, true);
	file.close();

	//only want contacts that are associated with layers!
	std::vector<int> addedUnknownContactIDs;	//going to want to associate contacts that don't match up
	//with a current contact ID to a new one. Hopefully the correct one? Otherwise it needs to be deleted and nullified
	for (unsigned int i = contactStartAdd; i < sim->contacts.size(); ++i) {
		addedUnknownContactIDs.push_back(sim->contacts[i]->id);
		delete sim->contacts[i];
	}
	sim->contacts.erase(sim->contacts.begin() + contactStartAdd, sim->contacts.end());
	//now we need to go through the added external environments and make sure we nullify any bad contact pointers in there

	std::vector<int> goodLoadedContacts;
	std::vector<int> goodLoadedSpectra, badSpectra;
	for (unsigned int i = startIndexEnvironments; i < ss->ExternalStates.size(); ++i) {
		ss->ExternalStates[i]->id = -1;
		for (unsigned int j = 0; j < ss->ExternalStates[i]->ContactEnvironment.size(); ++j) {
			bool wasUnknown = false;
			for (unsigned int k = 0; k < addedUnknownContactIDs.size(); ++k) {
				if (ss->ExternalStates[i]->ContactEnvironment[j]->ctcID == addedUnknownContactIDs[k]) {
					ss->ExternalStates[i]->ContactEnvironment[j]->ctc = nullptr;
					wasUnknown = true;
					break;
				}
			}

			if (!wasUnknown) {
				//this environment contact associated with a known contact
				VectorUniqueAdd(goodLoadedContacts, ss->ExternalStates[i]->ContactEnvironment[j]->ctcID);
			}
		}	//end for loop through all this environments contacts
		for (unsigned int j = 0; j < ss->ExternalStates[i]->ActiveLightEnvironment.size(); ++j) {
			if (mdesc->getSpectra(ss->ExternalStates[i]->ActiveLightEnvironment[j]) == nullptr)
				VectorUniqueAdd(badSpectra, ss->ExternalStates[i]->ActiveLightEnvironment[j]);
			else
				VectorUniqueAdd(goodLoadedSpectra, ss->ExternalStates[i]->ActiveLightEnvironment[j]);
		}

	}	//end for loop through all the NEW environments

	std::vector<Contact*> AvailContacts;	//don't link contacts to stuff that was already linked by ID
	for (unsigned int i = 0; i < sim->contacts.size(); ++i) {
		bool okAdd = true;
		for (unsigned int j = 0; j < goodLoadedContacts.size(); ++j) {
			if (sim->contacts[i]->id == goodLoadedContacts[j]) {
				okAdd = false;
				break;
			}
		}

		if (okAdd) {
			AvailContacts.push_back(sim->contacts[i]);	//will just line this up with the unknowns.
		}	//there is no rhyme or reason to make any choice at this point
	}

	//now just assign them to a contact and hope for the best. Add a replace button to contacts
	//if can't, then just set the ctcid to -1.
	bool displayCautionMessageNotEnough = false;
	bool displayCautionMessageRandom = false;
	for (unsigned int i = 0; i < addedUnknownContactIDs.size(); ++i) {
		for (unsigned int j = startIndexEnvironments; j < ss->ExternalStates.size(); ++j) {
			for (unsigned int k = 0; k < ss->ExternalStates[j]->ContactEnvironment.size(); ++k) {

				if (ss->ExternalStates[j]->ContactEnvironment[k]->ctcID == addedUnknownContactIDs[i]) {
					if (i < AvailContacts.size()) {
						ss->ExternalStates[j]->ContactEnvironment[k]->ctc = AvailContacts[i];
						ss->ExternalStates[j]->ContactEnvironment[k]->ctcID = AvailContacts[i]->id;
						displayCautionMessageRandom = true;
					}
					else {
						ss->ExternalStates[j]->ContactEnvironment[k]->ctc = nullptr;
						ss->ExternalStates[j]->ContactEnvironment[k]->ctcID = -1;
						displayCautionMessageNotEnough = true;
					}	//end else that there's no more contacts left it could be assigned to
				}	//end test that this is the contact we don't know where it's going
			}	//end loop through all the environments contacts
		}	//end loop through all the new environments
	}	//end loop through all the unknown contacts
	
	///now double check to make sure the spectra got associated correctly
	std::vector<int> AvailSpectra;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
		bool specAvail = true;
		for (unsigned int j = 0; j < goodLoadedSpectra.size(); ++j) {
			if (mdesc->spectrum[i]->ID == goodLoadedSpectra[j]) {
				specAvail = false;
				break;
			}
		}
		if (specAvail)
			AvailSpectra.push_back(mdesc->spectrum[i]->ID);
	}

	bool displaySpectraNotEnough = false;
	bool displaySpectraRandom = false;

	for (unsigned int i = 0; i < badSpectra.size(); ++i) {
		for (unsigned int j = startIndexEnvironments; j < ss->ExternalStates.size(); ++j) {
			for (unsigned int k = 0; k < ss->ExternalStates[j]->ActiveLightEnvironment.size(); ++k) {

				if (ss->ExternalStates[j]->ActiveLightEnvironment[k] == badSpectra[i]) {
					if (i < AvailSpectra.size()) {
						ss->ExternalStates[j]->ActiveLightEnvironment[k] = AvailSpectra[i];
						displaySpectraRandom = true;
					}
					else {
						ss->ExternalStates[j]->ActiveLightEnvironment[k] = -1;
						displaySpectraNotEnough = true;
					}	//end else that there's no more contacts left it could be assigned to
				}	//end test that this is the contact we don't know where it's going
			}	//end loop through all the environments contacts
		}	//end loop through all the new environments
	}	//end loop through all the unknown contacts

	ss->UpdateUnknownIDs();
	if (displayCautionMessageNotEnough) {
		wxMessageDialog mes(this, "CAUTION: This steady state file attempted to load a steady state simulation with more contacts than are currently defined.\n\nExcess data is lost, and this simulation requires editing to achieve similar results.");
		mes.ShowModal();
	}
	if (displayCautionMessageRandom) {
		wxMessageDialog mes(this, "CAUTION: This steady state file loaded a steady state file where not all contacts matched the present ones. The unmatched file contacts were paired with those in the simulation not associated with the file.\n\nDouble check on the Detailed Summary tab to ensure they were correlated correctly before running the simulation.");
		mes.ShowModal();
	}

	if (displaySpectraNotEnough) {
		wxMessageDialog mes(this, "CAUTION: This steady state file attempted to load a steady state simulation with more spectra than are currently defined.\n\nExcess data is lost, and this simulation requires editing to achieve similar results.");
		mes.ShowModal();
	}
	if (displaySpectraRandom) {
		wxMessageDialog mes(this, "CAUTION: This steady state file loaded a steady state file where not all spectra matched the present ones. The unmatched file spectra were paired with those in the simulation not associated with the file.\n\nDouble check on the Detailed Summary tab to ensure they were correlated correctly before running the simulation.");
		mes.ShowModal();
	}

	


	//make it show the loaded files first!
	if (startIndexEnvironments < ss->ExternalStates.size()) {	//if it successfully loaded anything, grab the first thing loaded!
		curEnv = ss->ExternalStates[startIndexEnvironments];
		curEnv->SelfContainedValidate();	//make sure this data is all up to snuff before pointers get assigned to it!
		curGroup = curEnv->FileOut;
		curContactData = (curEnv->ContactEnvironment.size() > 0) ? curEnv->ContactEnvironment[0] : nullptr;
		curLight = (curEnv->ActiveLightEnvironment.size() > 0) ? mdesc->getSpectra(curEnv->ActiveLightEnvironment[0]) : nullptr;
	}
	mdesc->Validate();

	TransferToGUIDefine();
}

void simSteadyStateBook::DuplicateGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	//implement later...
	if (curGroup == "")
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter a new group name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	std::string nm = entry.GetValue().ToStdString();
	if (nm == "")
		return;

	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
	{
		if (s->ExternalStates[i]->FileOut == nm)
		{
			wxString message = nm + " already is a group name!";
			wxDialog bad(this, wxID_ANY, message);
			bad.ShowModal();
			return;
		}
	}

	int startIndex = s->ExternalStates.size();
	for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
		Environment *env = new Environment(*GroupEnvs[i]);
		env->FileOut = nm;
		env->id = -1;
		s->ExternalStates.push_back(env);
	}
	s->UpdateUnknownIDs();
	curGroup = nm;
	unsigned int curEnvIndex = -1;
	for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
		if (GroupEnvs[i] == curEnv) {
			curEnvIndex = i;
		}
	}
	int contactID = -1;
	if (curContactData)
		contactID = curContactData->ctcID;
	if (curEnvIndex < GroupEnvs.size()){
		curEnv = s->ExternalStates[startIndex + curEnvIndex];
		curContactData = curEnv->getContact(contactID);
	}
	else {
		curEnv = nullptr;
		curContactData = nullptr;
		curLight = nullptr;
	}
		
	GroupEnvs.clear();
	TransferToGUIDefine();
}

void simSteadyStateBook::RenameGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter the group's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	std::string nm = entry.GetValue().ToStdString();
	if (nm == "")
		return;

	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
	{
		if (s->ExternalStates[i]->FileOut == nm)
		{
			wxDialog bad(this, wxID_ANY, "Click OK to merge with pre-existing group");
			if(bad.ShowModal() != wxID_OK)
				return;
			break;
		}
	}

	for (unsigned int i = 0; i < GroupEnvs.size(); ++i)
		GroupEnvs[i]->FileOut = nm;

	ReorderSims();

	TransferToGUIDefine();
}

void simSteadyStateBook::SwitchEnv(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	Environment* oldEnv = curEnv;
	Contact* prevContact = (curContactData!=nullptr) ? curContactData->ctc : nullptr;

	if (event.GetEventObject() == cbGroupChooser)
	{
		std::string oldGroup = curGroup;
		curGroup = cbGroupChooser->GetStringSelection();
		if (curGroup == oldGroup)	//do nothing. same group.
			return;

		curEnv = nullptr;
		for (unsigned int i = 0; i < s->ExternalStates.size(); ++i)
		{
			if (s->ExternalStates[i]->FileOut == curGroup)
			{
				curEnv = s->ExternalStates[i];
				curLight = (curEnv->ActiveLightEnvironment.size() > 0) ? getLightSource(curEnv->ActiveLightEnvironment[0]) : nullptr;
				break;
			}
		}
		if (curEnv == nullptr)
		{
			curContactData = nullptr;
			curLight = nullptr;
		}
		
	}
	else if (event.GetEventObject() == cbEnvChooser)
	{
		unsigned int sel = cbEnvChooser->GetSelection();
		if (sel < GroupEnvs.size())
		{
			curEnv = GroupEnvs[sel];
			if (curEnv != oldEnv)
			{
				curLight = (curEnv->ActiveLightEnvironment.size() > 0) ? getLightSource(curEnv->ActiveLightEnvironment[0]) : nullptr;
			}
		}
		else
		{
			curEnv = nullptr;
			curContactData = nullptr;
			curLight = nullptr;
		}
	}

	if (oldEnv != curEnv)
	{
		curContactData = nullptr;
		if (prevContact && curEnv)
		{
			for (unsigned int i = 0; i < curEnv->ContactEnvironment.size(); ++i) {
				if (curEnv->ContactEnvironment[i]->ctc == prevContact) {
					curContactData = curEnv->ContactEnvironment[i];
					break;
				}
			}
		}
			
		TransferToGUIDefine();
	}

}

LightSource* simSteadyStateBook::getLightSource(int id)
{
	if (!isInitialized())
		return nullptr;
	ModelDescribe* mdesc = getMdesc();
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
		if (mdesc->spectrum[i]->ID == id)
			return mdesc->spectrum[i];
	}
	return nullptr;
}

void simSteadyStateBook::MoveEnvUp(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (curEnv == nullptr || s == nullptr)
		return;

	int sel = cbEnvChooser->GetSelection();	//this correlates with the index of GroupEnvs.
	if (sel < 1 || (unsigned int)sel >= GroupEnvs.size())	//not sure what's going on, but the logic won't be valid.
		return;

	unsigned int overallIndexPrior, overallIndexCurrent;
	overallIndexCurrent = overallIndexPrior = -1;	//(unsigned)
	unsigned int sz = s->ExternalStates.size();
	for (unsigned int i = 0; i < sz && (overallIndexCurrent > sz || overallIndexPrior > sz); ++i)
	{
		if (s->ExternalStates[i] == GroupEnvs[sel])
			overallIndexCurrent = i;
		if (s->ExternalStates[i] == GroupEnvs[sel - 1])
			overallIndexPrior = i;
	}

	//gotta swap two sets... no need for a temp variable then!
	s->ExternalStates[overallIndexPrior] = GroupEnvs[sel];	//the earlier one now has the current
	s->ExternalStates[overallIndexCurrent] = GroupEnvs[sel - 1];	//and the later one now has the earlier.
	//now need to swap the groupEnvs. Just make sure the = are different from above!
	GroupEnvs[sel] = s->ExternalStates[overallIndexCurrent];
	GroupEnvs[sel - 1] = s->ExternalStates[overallIndexPrior];

	//now update the numeric list
	sel--;
	cbEnvChooser->SetSelection(sel);

	//nothign else changed, so do NOTHING!
}

void simSteadyStateBook::MoveEnvDown(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (curEnv == nullptr || s == nullptr)
		return;

	int sel = cbEnvChooser->GetSelection();	//this correlates with the index of GroupEnvs.
	if (sel < 0 || (unsigned int)sel >= GroupEnvs.size()-1)	//not sure what's going on, but the logic won't be valid.
		return;

	unsigned int overallIndexPrior, overallIndexCurrent;
	overallIndexCurrent = overallIndexPrior = -1;	//(unsigned)
	unsigned int sz = s->ExternalStates.size();
	for (unsigned int i = 0; i < sz && (overallIndexCurrent > sz || overallIndexPrior > sz); ++i)
	{
		if (s->ExternalStates[i] == GroupEnvs[sel])
			overallIndexCurrent = i;
		if (s->ExternalStates[i] == GroupEnvs[sel + 1])
			overallIndexPrior = i;
	}

	//gotta swap two sets... no need for a temp variable then!
	s->ExternalStates[overallIndexPrior] = GroupEnvs[sel];	//the earlier one now has the current
	s->ExternalStates[overallIndexCurrent] = GroupEnvs[sel + 1];	//and the later one now has the earlier.
	//now need to swap the groupEnvs. Just make sure the = are different from above!
	GroupEnvs[sel] = s->ExternalStates[overallIndexCurrent];
	GroupEnvs[sel + 1] = s->ExternalStates[overallIndexPrior];

	//now update the numeric list
	sel++;
	cbEnvChooser->SetSelection(sel);	//easy peasy.

}

void simSteadyStateBook::NewEnv(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter the Environment's group"));
	if (entry.ShowModal() != wxID_OK)
		return;

	std::string nm = entry.GetValue().ToStdString();
	if (nm == "")
		return;


	Environment *env = new Environment(s);
	env->FileOut = nm;
	env->id = -1;	//don't know what this hsould be
	s->ExternalStates.push_back(env);	//it's now a part of the simulation
	s->UpdateUnknownIDs();	//update the id to be somethign unique
	curEnv = env;		//going to show this
	curLight = nullptr;	//it has no other data...
	curContactData = nullptr;
	curGroup = nm;	//this is now the current group that we'll be targeting
	GroupEnvs.clear();	//get rid of any old baggage

	TransferToGUIDefine();	//update everything!

}

void simSteadyStateBook::RemoveEnv(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (curEnv == nullptr || s==nullptr)
		return;

	unsigned int grpInd = -1;
	for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
		if (GroupEnvs[i] == curEnv) {
			grpInd = i;
			break;
		}
	}

	for (unsigned int i = 0; i < s->ExternalStates.size(); ++i) {
		if (s->ExternalStates[i] == curEnv) {
			s->ExternalStates.erase(s->ExternalStates.begin() + i);
			delete curEnv;
			curEnv = nullptr;
			break;
		}
	}

	if (grpInd < GroupEnvs.size()) {
		GroupEnvs.erase(GroupEnvs.begin() + grpInd);
	}

	//now make sure this index is A-OK!
	if (grpInd >= GroupEnvs.size())
		grpInd--;	//might've just gotten rid of the last one.

	if (grpInd < GroupEnvs.size())
	{
		curEnv = GroupEnvs[grpInd];
		curContactData = (curEnv->ContactEnvironment.size() > 0) ? curEnv->ContactEnvironment[0] : nullptr;
		curLight = (curEnv->ActiveLightEnvironment.size() > 0) ? getLightSource(curEnv->ActiveLightEnvironment[0]) : nullptr;
	}
	else {
		curEnv = nullptr;
		curContactData = nullptr;
		curLight = nullptr;
		curGroup = "";
	}
	getMdesc()->Validate();
	TransferToGUIDefine();
}

void simSteadyStateBook::ChangeEnvGroup(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Change this environment's group to: "));
	if (entry.ShowModal() != wxID_OK)
		return;

	std::string nm = entry.GetValue().ToStdString();
	if (nm == "")
		return;

	curEnv->FileOut = nm;
	curGroup = nm;
	GroupEnvs.clear();

	ReorderSims();

	TransferToGUIDefine();
}

void simSteadyStateBook::QuickAddEnvironments(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr || curEnv == nullptr || curContactData == nullptr)
		return;
	long num;
	double first, last, delta;

	if (txtQuickAddNumber->GetValue().ToLong(&num) == false)
		return;

	if (num <= 0)
		return;

	if (txtQuickAddFirst->GetValue().ToDouble(&first) == false)
		return;
	if (txtQuickAddLast->GetValue().ToDouble(&last) == false)
		return;

	unsigned int ContactIndex = -1;
	for (unsigned int i = 0; i < curEnv->ContactEnvironment.size(); ++i) {
		if (curEnv->ContactEnvironment[i] == curContactData) {
			ContactIndex = i;	//this will be the same index
			break;
		}
	}
	if (ContactIndex >= curEnv->ContactEnvironment.size())	//failed to find it for whatever reason
		return;

	if (first == last)
		num = 1;

	std::vector<double> inputs;
	delta = last - first;
	if (num > 1)
		delta = delta / double(num - 1);

	inputs.push_back(first);
	for (long i = 1; i < num; ++i)
		inputs.push_back(inputs.back() + delta);
	//potential rounding error
	inputs.back() = last;

	int sel = cbQuickAddVariableList->GetSelection();
	if (sel < 0 || sel > 6)
		return;	//bad selection.
	

	for (unsigned int i = 0; i < inputs.size(); ++i)
	{
		Environment *env = new Environment(*curEnv);
		env->id = -1;

		switch (sel)
		{
		case 0:	//voltage
			env->ContactEnvironment[ContactIndex]->voltage = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_VOLT_LINEAR;
			break;
		case 1:	//current
			env->ContactEnvironment[ContactIndex]->current = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_CURRENT_LINEAR;
			break;
		case 2:	//current density
			env->ContactEnvironment[ContactIndex]->currentDensity = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR;
			break;
		case 3: //electric field
			env->ContactEnvironment[ContactIndex]->eField = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_EFIELD_LINEAR;
			break;
		case 4:	//current injection
			env->ContactEnvironment[ContactIndex]->current_ED = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_CURRENT_LINEAR_ED;
			break;
		case 5:	//efield injection
			env->ContactEnvironment[ContactIndex]->eField_ED = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_EFIELD_LINEAR_ED;
			break;
		case 6: //current density injection
			env->ContactEnvironment[ContactIndex]->currentDensity_ED = inputs[i];
			env->ContactEnvironment[ContactIndex]->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR_ED;
			break;
		default:
			break;
		}
		s->ExternalStates.push_back(env);
	}
	s->UpdateUnknownIDs();
	ReorderSims();
	//no need to change internal state variables... too much
	TransferToGUIDefine();	//but everything changed for drop downs and whatnot
}

void simSteadyStateBook::ReorderSims()
{
	if (!isInitialized())
		return;
	SS_SimData* s = getSSSim();
	if (s == nullptr)
		return;
	std::vector<Environment*>& master = s->ExternalStates;
	std::vector<std::string> groups;
	std::vector<unsigned int> groupCount;

	for (unsigned int i = 0; i < master.size(); ++i) {
		VectorUniqueAdd(groups, master[i]->FileOut);
	}

	//this is not efficient, but it is simple to read and understand...
	std::vector<Environment*> newMaster;	//just build a new list. 
	newMaster.reserve(master.size());

	for (unsigned int i = 0; i < groups.size(); ++i) {
		for (unsigned int j = 0; j < master.size(); ++j) {
			if (master[j]->FileOut == groups[i])
				newMaster.push_back(master[j]);
		}
	}

	master.swap(newMaster);	//and put the re-ordered list back into the external states
}

void simSteadyStateBook::AddSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr)
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	int sel = cbAvailSpectraChooser->GetSelection();
	if (sel < 0)	//there was no spectra selected...
		return;

	//can't just go by name! there might be multiple spectra with the same name, and that would totally trick it...
	curLight = nullptr;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
		if (curEnv->hasSpectra(mdesc->spectrum[i])) {
			//if it has it, it would have been added to the available list as opposed to this selection chooser.
			continue;
		}

		if (sel == 0) {
			curLight = mdesc->spectrum[i];
			break;
		}
		else
			sel--;
	}
	if (curLight == nullptr)	//doubtful...
		return;

	if (modifyAllEnvs()) {
		for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
			if (GroupEnvs[i]->hasSpectra(curLight) == false)
				GroupEnvs[i]->ActiveLightEnvironment.push_back(curLight->ID);
		}
	}
	else
		curEnv->ActiveLightEnvironment.push_back(curLight->ID);
	TransferToGUIDefine();	//gui laziness...

}

void simSteadyStateBook::RemoveSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr)
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	int sel = lbSpectraUse->GetSelection();
	if (sel < 0)	//there was no spectra selected...
		return;

	//this should match up directly with... mdesc. not the vector.

	//can't just go by name! there might be multiple spectra with the same name, and that would totally trick it...
	curLight = nullptr;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
		if (curEnv->hasSpectra(mdesc->spectrum[i])==false) {
			//if it has it, it would have been added to the available list as opposed to this selection chooser.
			continue;
		}

		if (sel == 0) {
			curLight = mdesc->spectrum[i];
			break;
		}
		else
			sel--;
	}
	if (curLight == nullptr)	//doubtful...
		return;

	if (modifyAllEnvs()) {
		for (unsigned int j = 0; j < GroupEnvs.size(); ++j) {
			for (unsigned int i = 0; i < GroupEnvs[j]->ActiveLightEnvironment.size(); ++i) {
				if (curLight->ID == GroupEnvs[j]->ActiveLightEnvironment[i]) {
					GroupEnvs[j]->ActiveLightEnvironment.erase(GroupEnvs[j]->ActiveLightEnvironment.begin() + i);
					break;
				}
			}
		}
	}
	else {
		for (unsigned int i = 0; i < curEnv->ActiveLightEnvironment.size(); ++i) {
			if (curLight->ID == curEnv->ActiveLightEnvironment[i]) {
				curEnv->ActiveLightEnvironment.erase(curEnv->ActiveLightEnvironment.begin() + i);
				break;
			}
		}
	}
	curLight = nullptr;
	mdesc->Validate();
	TransferToGUIDefine();	//gui laziness...

}

void simSteadyStateBook::SelectSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	//when clicked in the selection box...
	if (curEnv == nullptr)
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	int sel = lbSpectraUse->GetSelection();
	if (sel < 0)	//there was no spectra selected...
		return;

	//this should match up directly with... mdesc. not the vector.

	//can't just go by name! there might be multiple spectra with the same name, and that would totally trick it...
	curLight = nullptr;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i) {
		if (curEnv->hasSpectra(mdesc->spectrum[i]) == false) {
			//if it has it, it would have been added to the available list as opposed to this selection chooser.
			continue;
		}

		if (sel == 0) {
			curLight = mdesc->spectrum[i];
			break;
		}
		else
			sel--;
	}
	
	TransferToGUIDefine();

}

void simSteadyStateBook::AddContact(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr)
		return;
	SS_SimData* s = getSSSim();
	if (s==nullptr)
		return;

	Simulation* sim = s->getSim();
	if (sim == nullptr)
		return;

	int sel = cbAvailContactChooser->GetSelection();
	if (sel < 0)	//there was no contacts selected
		return;

	//can't reliably go by name. Have to go by index, but need to recreate the index.

	std::vector<Contact*>& cts = sim->contacts;	//don't like writing all kinds of crazy stuff out!
	curContactData = nullptr;
	Contact* selCtc = nullptr;
	for (unsigned int i = 0; i < cts.size(); ++i) {
		if (curEnv->hasContact(cts[i])) //if the environment has the calculation, then it was added to the list box
			continue;	//not part of the counter we care about

		if (sel == 0) {	//nailed it
			selCtc = cts[i];
			break;
		}
		else
			sel--;
	}

	if (selCtc == nullptr)	//not sure why the algorithm failed... but be safe.
		return;

	if (modifyAllEnvs()) {
		for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
			if (GroupEnvs[i]->hasContact(selCtc)) //don't add what already exists!
				continue;

			curContactData->currentActiveValue |= CONTACT_SINK;	//default that it connects to the outside world
			GroupEnvs[i]->ContactEnvironment.push_back(new SSContactData(selCtc));
		}
		curContactData = curEnv->ContactEnvironment.back();
		
	}
	else {
		if (curEnv->hasContact(selCtc)) //something is funkadelic.
			return;

		curContactData = new SSContactData(selCtc);
		curContactData->currentActiveValue |= CONTACT_SINK;
		curEnv->ContactEnvironment.push_back(curContactData);
	}
	TransferToGUIDefine();	//gui laziness...

}

void simSteadyStateBook::RemoveContact(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr || curContactData == nullptr)
		return;
	

	
	if (modifyAllEnvs() == false) {
		for (unsigned int i = 0; i < curEnv->ContactEnvironment.size(); ++i) {
			if (curEnv->ContactEnvironment[i] == curContactData) {
				curEnv->ContactEnvironment.erase(curEnv->ContactEnvironment.begin() + i);
				delete curContactData;
				if (i < curEnv->ContactEnvironment.size())
					curContactData = curEnv->ContactEnvironment[i];
				else {
					i--;
					if (i < curEnv->ContactEnvironment.size())
						curContactData = curEnv->ContactEnvironment[i];
					else if (curEnv->ContactEnvironment.size() > 0)
						curContactData = curEnv->ContactEnvironment[0];
					else
						curContactData = nullptr;
				}
				break;
			}
		}
	}
	else {
		Contact* ct = curContactData->ctc;
		curContactData = nullptr;
		for (unsigned int j = 0; j < GroupEnvs.size(); ++j) {
			for (unsigned int i = 0; i < GroupEnvs[j]->ContactEnvironment.size(); ++i) {
				if (GroupEnvs[j]->ContactEnvironment[i]->ctc == ct) {
					delete GroupEnvs[j]->ContactEnvironment[i];
					GroupEnvs[j]->ContactEnvironment.erase(GroupEnvs[j]->ContactEnvironment.begin() + i);

					if (GroupEnvs[j] == curEnv) {
						if (i < GroupEnvs[j]->ContactEnvironment.size())
							curContactData = GroupEnvs[j]->ContactEnvironment[i];
						else {
							i--;
							if (i < GroupEnvs[j]->ContactEnvironment.size())
								curContactData = GroupEnvs[j]->ContactEnvironment[i];
							else if (GroupEnvs[j]->ContactEnvironment.size() > 0)
								curContactData = GroupEnvs[j]->ContactEnvironment[0];
							else
								curContactData = nullptr;
						}
					}
					break;
				}
			}
		}
	}

	getMdesc()->Validate();

	TransferToGUIDefine();	//gui laziness...

}

void simSteadyStateBook::SelectContact(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curEnv == nullptr)
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	curContactData = nullptr;

	int sel = lbContactUse->GetSelection();
	if (sel < 0) {
		TransferToGUIDefine();
		return;
	}

	std::vector<Contact*>& cts = s->getSim()->contacts;	//save typing
	Contact* ct = nullptr;
	for (unsigned int i = 0; i < cts.size(); ++i) {
		if (curEnv->hasContact(cts[i])==false) { //want to pull from the list box. ignore what went to the command box.
			continue;
		}

		if (sel == 0) {
			ct = cts[i];
			break;
		}
		else
			sel--;
	}
	if (ct == nullptr)
		return;

	for (unsigned int i = 0; i < curEnv->ContactEnvironment.size(); ++i) {
		if (curEnv->ContactEnvironment[i]->ctc == ct) {
			curContactData = curEnv->ContactEnvironment[i];
			break;
		}
	}
	
	TransferToGUIDefine();
}

void simSteadyStateBook::ContactCBoxes(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curContactData == nullptr)
		return;
	//all ye checkboxes...

	std::vector<SSContactData*> modify;	//make ths contact the same for all environments!
	if (modifyAllEnvs()) {
		for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
			for (unsigned int j = 0; j < GroupEnvs[i]->ContactEnvironment.size(); ++j) {
				if (curContactData->ctcID == GroupEnvs[i]->ContactEnvironment[j]->ctcID) {
					modify.push_back(GroupEnvs[i]->ContactEnvironment[j]);
				}
			}

		}
	}
	else
		modify.push_back(curContactData);
	
	if (event.GetEventObject() == cbCtcExternal) {	//these two cannot be simultaneously be on
		if (cbCtcExternal->GetValue())	//but they're not a toggle either.
			cbCtcSink->SetValue(false);
	}
	else if (event.GetEventObject() == cbCtcSink) {
		if (cbCtcSink->GetValue())
			cbCtcExternal->SetValue(false);
	}

	if (cbCtcExternal->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CONNECT_EXTERNAL;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
		modify[i]->currentActiveValue &= (~CONTACT_CONNECT_EXTERNAL);
	}

	if (cbCtcSink->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
		modify[i]->currentActiveValue |= CONTACT_SINK;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
		modify[i]->currentActiveValue &= (~CONTACT_SINK);
	}

	if (cbCtcV->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_VOLT_LINEAR;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_VOLT_LINEAR);
	}
	//it has to do the whole validation thing. So all I have to care about is updating the flags
	if (cbCtcI->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CURRENT_LINEAR;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_CURRENT_LINEAR);
	}

	if (cbCtcJ->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_CUR_DENSITY_LINEAR);
	}

	if (cbCtcE->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_EFIELD_LINEAR;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_EFIELD_LINEAR);
	}

	if (cbCtcI_ED->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CURRENT_LINEAR_ED;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_CURRENT_LINEAR_ED);
	}

	if (cbCtcE_ED->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_EFIELD_LINEAR_ED;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_EFIELD_LINEAR_ED);
	}

	if (cbCtcJ_ED->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CUR_DENSITY_LINEAR_ED;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_CUR_DENSITY_LINEAR_ED);
	}

	if (cbCtcMinEField->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_MINIMIZE_E_ENERGY;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_MINIMIZE_E_ENERGY);
	}
	if (cbCtcOppDField->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_OPP_DFIELD;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_OPP_DFIELD);
	}
	if (cbCtcChargeNeutral->GetValue()) {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue |= CONTACT_CONSERVE_CURRENT;
	}
	else {
		for (unsigned int i = 0; i < modify.size(); ++i)
			modify[i]->currentActiveValue &= (~CONTACT_CONSERVE_CURRENT);
	}

	TransferToGUIDefine();	
}

void simSteadyStateBook::TextCBoxes(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
//	if (curEnv == nullptr)
//		return;

	//this is NOT one that you transfer data to the gui afterwards.
	//this is a response to the gui.
	
	if (curContactData) {
		//updates if a good value, otherwise... doesn't.
		//the clears call all this to be done, so they need to be individually checked or they'll overwrite
		std::vector<SSContactData*> modify;	//make ths contact the same for all environments!
		if (modifyAllEnvs()) {
			for (unsigned int i = 0; i < GroupEnvs.size(); ++i) {
				for (unsigned int j = 0; j < GroupEnvs[i]->ContactEnvironment.size(); ++j) {
					if (curContactData->ctcID == GroupEnvs[i]->ContactEnvironment[j]->ctcID) {
						modify.push_back(GroupEnvs[i]->ContactEnvironment[j]);
					}
				}

			}
		}
		else
			modify.push_back(curContactData);


		if (event.GetEventObject() == txtCtcV) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcV->GetValue().ToDouble(&modify[i]->voltage);
		}
		if (event.GetEventObject() == txtCtcI) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcI->GetValue().ToDouble(&modify[i]->current);
		}
		if (event.GetEventObject() == txtCtcJ) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcJ->GetValue().ToDouble(&modify[i]->currentDensity);
		}
		if (event.GetEventObject() == txtCtcE) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcE->GetValue().ToDouble(&modify[i]->eField);
		}
		if (event.GetEventObject() == txtCtcI_ED) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcI_ED->GetValue().ToDouble(&modify[i]->current_ED);
		}
		if (event.GetEventObject() == txtCtcE_ED) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcE_ED->GetValue().ToDouble(&modify[i]->eField_ED);
		}
		if (event.GetEventObject() == txtCtcJ_ED) {
			for (unsigned int i = 0; i < modify.size(); ++i)
				txtCtcJ_ED->GetValue().ToDouble(&modify[i]->currentDensity_ED);
		}
	}

	//all the other text... i don't really care about processessing. Just if it does the right toggles
	UpdateGUIValuesDefine(event);

}

void simSteadyStateBook::AddAlgorithm(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	double start, end;
	
	
	if (txtStartTol->GetValue().ToDouble(&start) == false)
		return;
	if (txtLastTol->GetValue().ToDouble(&end) == false)
		return;

	if (start < end || start <= 1e-12 || end <= 1e-12)	{ //12 digits accuracy is overkill. doubles only go to 17.
		wxGenericMessageDialog t(this, wxT("Unless you get unusually lucky, the simulation will never converge to a tolerance less than 10\u207B\u00B9\u00B2"), wxT("Choose higher tolerances"));
		t.ShowModal();
		return;
	}

	int which = cbSolMethods->GetSelection();
	if (which < 0)
		return;
//	wxString str = cbSolMethods->GetStringSelection();
	switch (which) {
	case 0:	//transient
		which = SS_TRANS_ACTUAL;
		break;
	case 1:	//monte carlo
		which = SS_MONTECARLO;
		break;
	case 2: //Full newton
		which = SS_NEWTON_L_FULL;
		break;
	case 3: //full newton no light
		which = SS_NEWTON_NL_FULL;
		break;
	case 4: //simple newton
		which = SS_NEWTON_ALL;
		break;
	case 5: //simple newton no ligh
		which = SS_NEWTON_NOLIGHT;
		break;
	case 6: //numerical newton
		which = SS_NEWTON_NUMERICAL;
		break;
	case 7: //smooth fermi
		which = SS_SMOOTH_BAD;
		break;
	case 8:	//sweep all
		which = SS_ALL;
		break;
	case 9: //sweep defects
		which = SS_MATCH_IMPS;
		break;
	case 10: //sweep currents
		which = SS_NO_SRH;
		break;
	default:
		which = -1;
	}
	if (which < 0)
		return;

	wxString str = GetSSMethodName(which);
	//ok, at this point, we're good to go!
	s->AddCalcOrder(which, start, end);
	int row = gdSolutions->GetNumberRows();
	gdSolutions->AppendRows(1);
	gdSolutions->SetCellValue(row, 0, str);
	str = "";
	str << start;
	gdSolutions->SetCellValue(row, 1, str);
	str = "";
	str << end;
	gdSolutions->SetCellValue(row, 2, str);

	SmartResizeGridColumns(gdSolutions);
	/*
	int width = gdSolutions->GetSize().GetWidth();
	int w0 = gdSolutions->GetColumnWidth(0);
	int w1 = gdSolutions->GetColumnWidth(1);
	int w2 = gdSolutions->GetColumnWidth(2);
	gdSolutions->AutoSizeColumns();
	int postW = gdSolutions->GetSize().GetWidth();
	int pw0 = gdSolutions->GetColumnWidth(0);
	int pw1 = gdSolutions->GetColumnWidth(1);
	int pw2 = gdSolutions->GetColumnWidth(2);
	int vw = gdSolutions->GetVirtualSize().GetWidth();
	int vw2 = gdSolutions->GetViewWidth();
	int clW = gdSolutions->GetClientSize().GetWidth();
	int mxClW = gdSolutions->GetMaxClientSize().GetWidth();
	*/
	//no need to update everything. It all WAS updated. Almost.
	UpdateGUIValuesDefine(event);	//some buttons might need to change. But no data needs updating.
}


void simSteadyStateBook::ReplaceAlgorithm(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (RemoveStepRow >= (unsigned int)gdSolutions->GetNumberRows()) {
		UpdateGUIValuesDefine(event);	//it shouldn't have been able to do this. fix that.
		return;
	}
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	double start, end;

	start = end = -1.0;
	txtStartTol->GetValue().ToDouble(&start);
	txtLastTol->GetValue().ToDouble(&end);
	int which = cbSolMethods->GetSelection();

//	wxString str = cbSolMethods->GetStringSelection();
	switch (which) {
	case 0:	//transient
		which = SS_TRANS_ACTUAL;
		break;
	case 1:	//monte carlo
		which = SS_MONTECARLO;
		break;
	case 2: //Full newton
		which = SS_NEWTON_L_FULL;
		break;
	case 3: //full newton no light
		which = SS_NEWTON_NL_FULL;
		break;
	case 4: //simple newton
		which = SS_NEWTON_ALL;
		break;
	case 5: //simple newton no ligh
		which = SS_NEWTON_NOLIGHT;
		break;
	case 6: //numerical newton
		which = SS_NEWTON_NUMERICAL;
		break;
	case 7: //smooth fermi
		which = SS_SMOOTH_BAD;
		break;
	case 8:	//sweep all
		which = SS_ALL;
		break;
	case 9: //sweep defects
		which = SS_MATCH_IMPS;
		break;
	case 10: //sweep currents
		which = SS_NO_SRH;
		break;
	default:
		which = -1;
	}
	wxString str = GetSSMethodName(which);

	s->UpdateCalcOrder(RemoveStepRow, which, start, end);	//if they're bad values, they don't update!
	SmartResizeGridColumns(gdSolutions);
	TransferToGUIDefine();	//the values might not end up equaling what was sent in. This one line is easier than figuring it all out...

}

void simSteadyStateBook::RemoveAlgorithm(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (RemoveStepRow >= (unsigned int)gdSolutions->GetNumberRows()) {
		UpdateGUIValuesDefine(event);	//it shouldn't have been able to do this. fix that.
		return;
	}

	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	s->RemoveCalcOrder(RemoveStepRow);
	gdSolutions->DeleteRows(RemoveStepRow);
	SmartResizeGridColumns(gdSolutions);

}

void simSteadyStateBook::MoveAlgorithmUp(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	if (s->SwapCalcOrder(RemoveStepRow, RemoveStepRow - 1)) {
		gdSolutions->SelectRow(RemoveStepRow - 1);
		TransferToGUIDefine();
	}
	return;
}

void simSteadyStateBook::MoveAlgorithmDown(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	SS_SimData *s = getSSSim();
	if (s == nullptr)
		return;

	if (s->SwapCalcOrder(RemoveStepRow, RemoveStepRow + 1)) {
		gdSolutions->SelectRow(RemoveStepRow + 1);
		TransferToGUIDefine();
	}
	return;
}


void simSteadyStateBook::ChangeAlgorithm(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (event.GetEventObject() != cbSolMethods)
		return;
	/*
	algs.Add("Transient");
	algs.Add("Monte Carlo");
	algs.Add("Full Newton");
	algs.Add("Full Newton - no light");
	algs.Add("Simplified Newton");
	algs.Add("Simplified Newton - no light");
	algs.Add("Numerical Newton");
	algs.Add("Smoothen Fermi Energy");
	algs.Add("Sweep All to Zero");
	algs.Add("Sweep Impurity/Defects to Zero");
	algs.Add("Sweep Currents to Zero");
	*/
	int sel = cbSolMethods->GetSelection();
	wxString message;
	if (sel < 0)
		return;
	switch (sel) {
	case 0:
		message = wxT("The transient method follows the same process as transient simulations. It calculates all the rates, determines a timestep, and then progresses forward until the individual states' calculated Fermi level changes at the accepted tolerance. The further away from steady-state, the more accurate this method is, making it good for generating an \"Initial Guess.\"");
		break;
	case 1:
		message = wxT("The Monte Carlo method picks random states, adjusts the occupation randomly within tolerance limits, and accepts if the rates improve. Note: NOT a good way to end a calculation. It takes a very long time to reduce the tolerance to the accuracy needed.");
		break;
	case 2:
		message = wxT("The Newton method analytically calculates the jacobian of the rates with respect to all other states' occupancy changes. Dense matrix. The optical rates are not constant, so it takes some time to compute. If close to convergence, a few iterations of this are good for a final answer.");
		break;
	case 3:
		message = wxT("This Newton method analytically calculates the jacobian as before, but without light. It runs quicker. Sometimes no light is a good mid-solution for converging towards one with light.");
		break;
	case 4:
		message = wxT("This Newton method is almost the same as the Full Newton. Post processing on the Jacobian simplifies and improves computational time with minimal accuracy loss. It is also good as a final convergence step.");
		break;
	case 5:
		message = wxT("This Newton method post processes the Jacobian to improve calculation times and does not include light. This is a good choice for final convergence on dark simulations.");
		break;
	case 6:
		message = wxT("The Numerical Newton method calculates the Jacobian by adjusting each occupancy and calculating the rates. It is slow, and brute force for when a reasonable analytical solution cannot be calculated. Other Newton methods defer to this if the analytical solution requirements are not met.");
		break;
	case 7:
		message = wxT("Smoothen Fermi Energy is the beginning of some ad hoc methods. This finds similar energy states adjacent to one another, and averages them out. Thus if a random state spikes in occupation, this will bring it back to more appropriate levels quickly without wasting many iterations.");
		break;
	case 8:
		message = wxT("This method sweeps back and forth through the device and adjusts the concentration to move the net rate closer to zero. Best used for initial guesses.");
		break;
	case 9:
		message = wxT("This sweeps through the device adjusting the occupation level of defect and impurity states to get the net rate to zero. This very quickly gets the impurities in local equilibrium with the bands.");
		break;
	case 10:
		message = wxT("This sweeping method ignores the defects and adjusts the bands. As with the other sweep methods, this is best for generating an initial guess.");
		break;
	default:
		message = wxT("Generally, the simulation progresses forward by progressively restricting the tolerance. The tolerance restricts how much an individual state's Fermi level may change in a single iteration. A state is considered at steady state values when its rate is 0 or changes sign with changes smaller than the tolerance.");
		break;
	}
	stSolutionAlgorithmDescription->SetLabel(message);
	//stSolutionAlgorithmDescription->Wrap(stSolutionAlgorithmDescription->GetClientSize().GetWidth());
	stSolutionAlgorithmDescription->Wrap(275);
	szrRightColumn->Layout();

	return;
}

void simSteadyStateBook::ClickAlgBox(wxGridEvent& event)
{
	if (!isInitialized())
		return;
	if (event.GetEventObject() == gdSolutions)
	{
		RemoveStepRow = event.GetRow();
		wxString tmp = gdSolutions->GetCellValue(RemoveStepRow, 1);
		if (tmp != "")
			txtStartTol->ChangeValue(tmp);
		tmp = gdSolutions->GetCellValue(RemoveStepRow, 2);
		if (tmp != "")
			txtLastTol->ChangeValue(tmp);

		TransferToGUIDefine();
	}
}

void simSteadyStateBook::EditSelectedEnv(wxCommandEvent& event) {
	if (!isInitialized())
		return;
	if (selEnv >= sumGroups.size())
		return;

	curGroup = sumCGroup;
	curEnv = sumGroups[selEnv];
	if (curEnv->ActiveLightEnvironment.size() > 0)
		curLight = getMdesc()->getSpectra(curEnv->ActiveLightEnvironment[0]);
	else
		curLight = nullptr;

	if (curEnv->ContactEnvironment.size() > 0)
		curContactData = curEnv->ContactEnvironment[0];
	else
		curContactData = nullptr;
	SetSelection(0);//generates a page change event, which will update the display!

}

void simSteadyStateBook::ChangeSummaryGroup(wxCommandEvent& event) {
	if (!isInitialized())
		return;
	if (sumCGroup != cbSumGroup->GetStringSelection()) {
		sumCGroup = cbSumGroup->GetStringSelection();
		UpdateGUIValuesSummary(event);
	}
}

void simSteadyStateBook::ChangeSummaryEnv(wxCommandEvent& event) {
	if (!isInitialized())
		return;
	if (selEnv != cbSumEnv->GetSelection()) {
		selEnv = cbSumEnv->GetSelection();
		UpdateGUIValuesSummary(event);
	}

}

BEGIN_EVENT_TABLE(simSteadyStateBook, wxNotebook)
EVT_BUTTON(SS_NEW_GROUP, simSteadyStateBook::NewGroup)
EVT_BUTTON(SS_REMOVE_GROUP, simSteadyStateBook::RemoveGroup)
EVT_BUTTON(SS_SAVE_GROUP, simSteadyStateBook::SaveAll)
EVT_BUTTON(SS_LOAD_GROUP, simSteadyStateBook::LoadGroup)
EVT_BUTTON(SS_DUPLICATE_GROUP, simSteadyStateBook::DuplicateGroup)
EVT_BUTTON(SS_RENAME_GROUP, simSteadyStateBook::RenameGroup)
EVT_COMBOBOX(SS_CHANGE_ENV, simSteadyStateBook::SwitchEnv)
EVT_BUTTON(SS_ENV_UP, simSteadyStateBook::MoveEnvUp)
EVT_BUTTON(SS_ENV_DOWN, simSteadyStateBook::MoveEnvDown)
EVT_BUTTON(SS_NEW_ENV, simSteadyStateBook::NewEnv)
EVT_BUTTON(SS_REMOVE_ENV, simSteadyStateBook::RemoveEnv)
EVT_BUTTON(SS_CHANGE_ENV_GROUP, simSteadyStateBook::ChangeEnvGroup)
EVT_BUTTON(SS_QUICK_ADD, simSteadyStateBook::QuickAddEnvironments)
EVT_BUTTON(SS_ADD_SPECTRA, simSteadyStateBook::AddSpectra)
EVT_BUTTON(SS_REMOVE_SPECTRA, simSteadyStateBook::RemoveSpectra)
EVT_LISTBOX(SS_SPECTRA_SELECT, simSteadyStateBook::SelectSpectra)
EVT_BUTTON(SS_ADD_CONTACT, simSteadyStateBook::AddContact)
EVT_BUTTON(SS_REMOVE_CONTACT, simSteadyStateBook::RemoveContact)
EVT_LISTBOX(SS_CONTACT_SELECT, simSteadyStateBook::SelectContact)
EVT_CHECKBOX(SS_CBOXES, simSteadyStateBook::ContactCBoxes)
EVT_TEXT(SS_TEXT, simSteadyStateBook::TextCBoxes)
EVT_BUTTON(SS_ADD_ALG, simSteadyStateBook::AddAlgorithm)
EVT_BUTTON(SS_REPLACE_ALG, simSteadyStateBook::ReplaceAlgorithm)
EVT_BUTTON(SS_REMOVE_ALG, simSteadyStateBook::RemoveAlgorithm)
EVT_BUTTON(SS_ALG_UP, simSteadyStateBook::MoveAlgorithmUp)
EVT_BUTTON(SS_ALG_DOWN, simSteadyStateBook::MoveAlgorithmDown)
EVT_GRID_CMD_SELECT_CELL(SS_ALG_BOX_CLICK, simSteadyStateBook::ClickAlgBox)
EVT_COMBOBOX(SS_ALG_CHANGE, simSteadyStateBook::ChangeAlgorithm)
EVT_COMBOBOX(SS_CHANGE_GROUP, simSteadyStateBook::SwitchEnv)
////////////////////summary page
EVT_COMBOBOX(SS_SUM_ENV, simSteadyStateBook::ChangeSummaryEnv)
EVT_COMBOBOX(SS_SUM_GROUP, simSteadyStateBook::ChangeSummaryGroup)
EVT_BUTTON(SS_SUM_VIEW, simSteadyStateBook::EditSelectedEnv)
////////////////////detail page
EVT_COMBOBOX(SS_DET_GROUP, simSteadyStateBook::ChangeDetailedGroup)
EVT_BUTTON(SS_DET_SWAP, simSteadyStateBook::SwapContactAssociationBtn)
EVT_COMBOBOX(SS_DET_SWAP, simSteadyStateBook::SwapContactAssociationCB)


END_EVENT_TABLE()