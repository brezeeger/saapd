#ifndef GUIDIALOGS_H
#define GUIDIALOGS_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/valnum.h"
#include "wx/textdlg.h"
#endif

#include "kernel.h"

class CalcDialog : public wxDialog
{
public:
	CalcDialog(wxWindow* parent);
	~CalcDialog();
	void Calculate(wxCommandEvent& event);
	void CloseWindow(wxCloseEvent& event);
private:
	wxTextCtrl* input;
	wxTextCtrl* output;
	wxBoxSizer* calcSizer;
	wxStaticText* timesPi;
//	wxButton* calc;

private:
	DECLARE_EVENT_TABLE()
};

class AddFixedVoltageDialog : public wxDialog
{
public:
	AddFixedVoltageDialog(simContactsPanel* parent);
	~AddFixedVoltageDialog();
	bool TransferDataFromWindow();

private:
	bool diag_inval_input(wxString error_msg);

	wxTextCtrl* timeTxt;
	wxTextCtrl* voltageTxt;
	wxButton* ok;
	wxButton* cancel;
	wxStaticText* voltageLabel;
	wxStaticText* timeLabel;
	wxFlexGridSizer* fixedSizer;

	int contactIndex;
};

class AddCosVoltageDialog : public wxDialog
{
public:
	AddCosVoltageDialog(simContactsPanel* parent);
	~AddCosVoltageDialog();
	bool TransferDataFromWindow();

private:
	bool diag_inval_input(wxString error_msg);

	wxTextCtrl* timeTxt;
	wxTextCtrl* ampTxt;
	wxTextCtrl* freqTxt;
	wxTextCtrl* phaseTxt;
	wxTextCtrl* voltageOffsetTxt;
	wxFlexGridSizer* cosSizer;
	wxStaticText* timeLabel;
	wxStaticText* ampLabel;
	wxStaticText* freqLabel;
	wxStaticText* phaseLabel;
	wxStaticText* voltageOffsetLabel;
	wxButton* ok;
	wxButton* cancel;

	int contactIndex;
};

class RemoveVoltageDialog : public wxDialog
{
public:
	RemoveVoltageDialog(simContactsPanel* parent);
	~RemoveVoltageDialog();
	wxBoxSizer* remSizer;
	wxBoxSizer* buttonSizer;
	wxButton* ok;
	wxButton* cancel;

	bool TransferDataFromWindow();

private:
	wxListCtrl* voltageList;

	int contactIndex;
};
/*
class SelectGraphDialog : public wxDialog
{
public:
	SelectGraphDialog(resultsPanel* result);
	~SelectGraphDialog();
	bool TransferDataFromWindow();
	bool TransferDataToWindow();

private:
	resultsPanel* parent;

	wxBoxSizer* remSizer;
	wxBoxSizer* buttonSizer;

	wxCheckBox* Ef;
	wxCheckBox* Efn;
	wxCheckBox* Efp;
	wxCheckBox* Ec;
	wxCheckBox* Ev;
	wxCheckBox* Psi;
	wxCheckBox* electrons;
	wxCheckBox* holes;
	wxCheckBox* impCharge;
	wxCheckBox* n;
	wxCheckBox* p;
	wxCheckBox* deflight;
	wxCheckBox* condlight;
	wxCheckBox* genlight;
	wxCheckBox* genthermal;
	wxCheckBox* recthermal;
	wxCheckBox* srh;
	wxCheckBox* rho;
	wxCheckBox* Jn;
	wxCheckBox* Jp;
	wxCheckBox* stddev;
	wxCheckBox* stderrBox;
	wxCheckBox* qe;

	wxSizer* outputsSizer;
};
*/
class SampleLine : public wxPanel
{
public:
	SampleLine(wxWindow* parent);
	void UpdatePen(wxPen& newPen);

	void paintEvent(wxPaintEvent & evt);
	void paintNow();
	void render(wxDC &dc);

private:
	wxPen pen;

	DECLARE_EVENT_TABLE()
};
/*
class GraphPickerDialog : public wxDialog
{
public:
	GraphPickerDialog(resultsPanel* result);
	~GraphPickerDialog();
	void changeColor(wxCommandEvent& event);
	void changeStyle(wxCommandEvent& event);
	void changePick(wxCommandEvent& event);

private:
	wxPen curPen;
	int prevSelection;
	wxComboBox* dialogPicker;
	wxComboBox* stylePicker;
	resultsPanel* parent;
	SampleLine* sample;
	wxBoxSizer* size;
	wxButton* changeButton;
	DECLARE_EVENT_TABLE()
};
*/

class CreditsDialog : public wxDialog {
public:
	CreditsDialog(wxWindow* parent, wxString name);
	~CreditsDialog();
	void CloseWindow(wxCloseEvent& event);

	wxTextCtrl *licensing;

	wxBoxSizer* topSizer;
private:
	DECLARE_EVENT_TABLE()
};

class SimulationProgressDialog : public wxDialog
{
public:
	SimulationProgressDialog(wxWindow* parent, wxString name, int range, wxString filename);
	~SimulationProgressDialog();
	void OnClose(wxCloseEvent& event);
	void Update(int value);
	void Pause(wxCommandEvent& event);
	void Stop(wxCommandEvent& event);
	void Abort(wxCommandEvent& event);
	void SaveState(wxCommandEvent& event);
	void NextState(wxCommandEvent& event);

private:
	bool paused;
	wxWindow* p;	//parent
	wxStaticText* info;
	wxGauge* progressBar;
	wxStaticText* shortInfo;
	wxGauge* shortProgBar;
	wxButton* pause;
	kernelThread* simulation;
	wxTimer* time;
	wxStaticText* infoLow1;
	wxStaticText* infoLow2;
	wxStaticText* infoLow3;
	wxStaticText* infoLow4;
	wxStaticText* taskTime;

	wxButton* stop;
	wxButton* abortButton;
	wxButton* saveStateButton;
	wxButton* nextDataSetButton;	//force it to go on to the next data set. As user essentially say good enough
	wxBoxSizer* topSizer;
	wxBoxSizer* buttonSizer1;
	wxBoxSizer* buttonSizer2;
	wxBoxSizer* buttonSizer;
	wxBoxSizer* horizSizer;
	wxBoxSizer* textSizer;
	
	bool isInitialized() { return (simulation != nullptr); }

	DECLARE_EVENT_TABLE()
};

#endif
