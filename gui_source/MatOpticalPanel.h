#ifndef MAT_OPTICAL_PANEL_H
#define MAT_OPTICAL_PANEL_H


#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/scrolwin.h"
#endif

class ModelDescribe;
class devMaterialPanel;
class wxGrid;
class wxTextCtrl;
class wxStaticText;
class wxSizer;
class wxBoxSizer;
class wxButton;
class wxCommandEvent;
class wxWindow;
class wxGridEvent;
class Optical;

class devMatOpticalsPanel : public wxScrolledWindow
{
public:
	devMatOpticalsPanel(wxWindow* parent);
	~devMatOpticalsPanel();
	void CheckConditions(wxCommandEvent& event);
	void CheckConditionsWrapper(wxGridEvent& event);
	void AddWavelength(wxCommandEvent& event);
	void RemoveWavelength(wxCommandEvent& event);
	void ModifyWavelength(wxGridEvent& event);
	void UpdateA(wxCommandEvent& event);
	void UpdateB(wxCommandEvent& event);
	void UpdateN(wxCommandEvent& event);
	void UpdateOptEff(wxCommandEvent& event);
	void UpdateCBox(wxCommandEvent& event);
	void WaveX10(wxCommandEvent& event);
	void WaveD10(wxCommandEvent& event);
	void GenFromAB(wxCommandEvent& event);
	void CellClicked(wxGridEvent& event);
	void ClearWavelengths(wxCommandEvent& event);

	void Clear();
	void SaveOptics(wxCommandEvent& event);	//old news
	void LoadOptics(wxCommandEvent& event);	//old news
	void EnableOpt(bool enable);
	void EnableAll(bool en = true);
	void SizeRight();

	ModelDescribe* getMDesc();
	devMaterialPanel* getMyClassParent();
	Material* getMat();
	Optical* getOptical();

	void VerifyInternalVars() { }	//nothing to actually check

	void UpdateGUIValues(wxCommandEvent& evt);
	void TransferToGUI();
	void TransferToMdesc(wxCommandEvent& evt);

private:
	wxGrid* optProperties;
	wxTextCtrl* globalN;
	wxTextCtrl* globalA;
	wxTextCtrl* globalB;
	wxStaticText* nkTxt;
	wxStaticText* alphanTxt;
	wxStaticText* abTxt;
	wxSizer *matOpticalSizer;

	wxBoxSizer* gridButtonsSizer;
	wxBoxSizer* gridSizer;
	wxBoxSizer* globalsSizer;
	wxBoxSizer* emmissionSizer;

	
	
	wxButton* remWave;

	wxButton* WavelengthTimesTen;
	wxButton* WavelengthDivideTen;
	wxButton* GenAB;
	wxButton* btnSave;
	wxButton* btnLoad;
	wxButton* btnClear;

	wxBoxSizer* AddWaveSizer;
	wxStaticText* inpWaveLbl;
	wxStaticText* inpNLbl;
	wxStaticText* inpKLbl;
	wxStaticText* inpALbl;
	wxTextCtrl* inpWavetxt;
	wxTextCtrl* inpNTxt;
	wxTextCtrl* inpKTxt;
	wxTextCtrl* inpATxt;
	wxButton* addWave;

	wxStaticText* globalNTxt;
	wxStaticText* globalATxt;
	wxStaticText* globalBTxt;
	wxStaticText* condition;

	wxStaticText* disableEmissions;
	wxStaticText* supressEmisOutput;
	wxStaticText* EmissionEfficiency;
	wxCheckBox* chk_disableEmissions;
	wxCheckBox* chk_supressEmisOutput;
	wxTextCtrl* txt_EmissionEfficiency;

	double lastWavelengthEntry, lastNEntry, lastKEntry, lastAEntry;
	int curMode;
	int rowDelete;

	bool isInitialized() { return (txt_EmissionEfficiency != nullptr); }

	DECLARE_EVENT_TABLE()
};

#endif