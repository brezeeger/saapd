#ifndef MY_WX_IMAGE_HANDLER_H
#define MY_WX_IMAGE_HANDLER_H

// see https://wiki.wxwidgets.org/An_image_panel

#include "wx/wxprec.h"
#include "wx/panel.h"
#include "wx/image.h"
#include "wx/bitmap.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif


#include "wx/image.h"
#include <string>

class wxImage;
class wxBitmap;

class myImagePane : public wxPanel
{
	wxImage* picDescription;
	wxBitmap picBitmap;
	int w, h;
public:
	myImagePane(wxWindow* parent, std::string imageLoc, wxBitmapType type = wxBITMAP_TYPE_ANY);
	myImagePane(wxWindow* parent, char *imageData[]);
	~myImagePane();
	void OnPaint(wxPaintEvent& event);
	void render(wxDC& dc);
	void OnSize(wxSizeEvent& event);
	void paint();
private:
	DECLARE_EVENT_TABLE()
};



#endif