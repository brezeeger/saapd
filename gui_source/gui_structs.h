#ifndef GUI_STRUCTS_H
#define GUI_STRUCTS_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <vector>

#define PI 3.14159265358979323846

enum layerShapes{
	LAYER_POINT = 0,
	LAYER_LINE = 1,
	LAYER_RECTANGLE = 2,
	LAYER_QUADRILATERAL = 3,
	LAYER_PENTAGON = 4,
	LAYER_HEXAGON = 5,
	LAYER_ELLIPSE = 6,
	LAYER_CIRCLE = 7,
	LAYER_SQUARE = 8,
	LAYER_OTHER = 9
};

struct xycoord{
	int x;
	int y;
};

struct impurity{
	wxString name;
	bool accdon; //true if acceptor, false if donor
	bool reference; //true if CB, false if VB
	char distribution;
	wxString degeneracy;
	wxString density;
	wxString energy;
	wxString width;
	bool depth; //true if shallow, false if deep
	wxString charge;
	wxString sigN;
	wxString sigP;
	int ID;
	wxString material;
};

struct defect{
	wxString name;
	bool accdon; //true if acceptor, false if donor
	bool reference; //true if CB, false if VB
	char distribution;
	wxString density;
	wxString energy;
	wxString width;
	bool depth; //true if shallow, false if deep
	wxString charge;
	wxString sigN;
	wxString sigP;
	int ID;
};

struct wavelengths{
	double wavelength;
	double intensity;
	double angleVariation;
	double xPos;
	double yPos;
	double zPos;
};

struct spectrum{
	std::vector<std::pair<double, bool> >times;
	std::vector<wavelengths> waves;
};

struct wave_properties{
	wxString wavelength;
	wxString n;
	wxString k;
	wxString alpha;
};

struct material{
	wxString name;
	wxString description;
	int ID;
	char type;
	wxString relativePermittivity;
	wxString intrinsicCarrierDensity;
	wxString CBEffDensityOfStates;
	wxString VBEffDensityOfStates;
	wxString electronMass;
	wxString holeMass;
	wxString electronMobility;
	wxString holeMobility;
	wxString electronAffinity;
	wxString bandGap;
	wxString specificHeatCapacity;
	wxString thermalConductivity;
	std::vector<defect> defects;
	wxString globalN;
	wxString globalA;
	wxString globalB;
	std::vector<wave_properties> opt_props;
	int opt_method;
};

struct point{
	wxString xPos;
	wxString yPos;
	wxString zPos;
};

struct layer_materials{
	unsigned int flag;
	wxString xStartPos;
	wxString xStopPos;
	int ID;
};

struct layer_impurities{
	unsigned int flag;
	wxString xStartPos;
	wxString xStopPos;
	int ID;
};


struct layer{
	wxString name;
	int ID;
	int contactID;
	char shape;
	std::vector<point> vertices;
	bool affectMesh;
	bool exclusive;
	bool priority;
	wxString center;
	wxString edge;
	wxString phi;
	wxString theta;
	std::vector<layer_materials> inc_mats;
	std::vector<layer_impurities> inc_imps;
	bool hidden;
};

struct voltagePoint{
	double time;
	bool cos; //indicates whether fixed or a cosine
	double amp; //is the value for "fixed" points
	double freq;
	double phase_offset;
	double voltage_offset;
};

//We will actually store the values as a double since they shouldn't have to be reloaded
struct contact{
	wxString name;
	int ID;
	std::vector<voltagePoint > voltages;
};


#endif
