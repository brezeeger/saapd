#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"

#include "MatElectricPanel.h"
#include "MatPanel.h"
#include "MatOpticalPanel.h"
#include "MatDefects.h"
#include "LayerPanel.h"
#include "GUIHelper.h"
#include "ImpurityPanel.h"

#include "../kernel_source/Layers.h"
#include "../kernel_source/load.h"
#include "../kernel_source/model.h"
#include "../kernel_source/BasicTypes.h"
#include "../kernel_source/contacts.h"
#include "../kernel_source/Materials.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;

//These should be kept in sync with their respective wxComboBox

extern double scalarTEN(int index);

devLayerTopPanel::devLayerTopPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY),
	cbLayerChooser(nullptr),nbLayersWindow(nullptr), chkAffectMesh(nullptr), chkExclusiveMesh(nullptr),
chkHide(nullptr), chkContact(nullptr), chkPtEmissions(nullptr), chkAllRates(nullptr),
txtScaling(nullptr), txtSpacingCenter(nullptr), txtSpacingEdge(nullptr),
cbLayerShape(nullptr), cbLyrShapeUnits(nullptr),
txtLyrStartX(nullptr), txtLyrStartY(nullptr), txtLyrStartZ(nullptr),
txtLyrDim1X(nullptr), txtLyrDim1Y(nullptr), txtLyrDim1Z(nullptr),
txtLyrDim2X(nullptr), txtLyrDim2Y(nullptr), txtLyrDim2Z(nullptr),
cbLayerShapeVertexList(nullptr),
txtLyrShapeVertX(nullptr), txtLyrShapeVertY(nullptr), txtLyrShapeVertZ(nullptr),
cbAllMats(nullptr),
cbMatDescriptors(nullptr),
cbMatDistType(nullptr),
cbMatUnits(nullptr),
txtMatStartX(nullptr), txtMatStartY(nullptr), txtMatStartZ(nullptr),
txtMatHalfX(nullptr), txtMatHalfY(nullptr), txtMatHalfZ(nullptr),
cbAllImps(nullptr),
cbImpDescriptors(nullptr),
cbImpDistType(nullptr),
cbImpUnits(nullptr),
txtImpMultiplier(nullptr),
txtImpStartX(nullptr), txtImpStartY(nullptr), txtImpStartZ(nullptr),
txtImpHalfX(nullptr), txtImpHalfY(nullptr), txtImpHalfZ(nullptr),
pltWind(nullptr), curLayer(nullptr), curMatInLayer(nullptr), curImpInLayer(nullptr)
{
	//first initialize all the sizers
	bxszrPrimary = new wxBoxSizer(wxVERTICAL);
	
	bxszrLayBar = new wxBoxSizer(wxHORIZONTAL);
	bxszrMainSeparator = new wxBoxSizer(wxHORIZONTAL);
	bxszrLyrPrim = new wxBoxSizer(wxVERTICAL);
	bxszMatPrim = new wxBoxSizer(wxVERTICAL);
	bxszMatListAdd = new wxBoxSizer(wxHORIZONTAL);
	bxszMatChangeRemove = new wxBoxSizer(wxHORIZONTAL);
	bxszImpPrim = new wxBoxSizer(wxVERTICAL);
	bxszImpListAdd = new wxBoxSizer(wxHORIZONTAL);
	bxszImpChangeRemove = new wxBoxSizer(wxHORIZONTAL);
	bxSzImpMultiplier = new wxBoxSizer(wxHORIZONTAL);

	fgszrLyrShape = new wxFlexGridSizer(4, 2, 5);
	fgszrMatShape = new wxFlexGridSizer(4, 2, 5);
	fgszrImpShape = new wxFlexGridSizer(4, 2, 5);
	fgszrLyrEntries = new wxFlexGridSizer(2, 2, 5);

	wxSize XYZSize(50, -1);
	
	wxArrayString emptyStringArray;
	
	cbLayerChooser = new wxComboBox(this, LAY_SWITCH, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	btnLayAdd = new wxButton(this, LAY_NEW, wxT("New Layer"));
	btnLayRem = new wxButton(this, LAY_REMOVE, wxT("Remove"));
	btnLaySave = new wxButton(this, LAY_SAVE, wxT("Save"));
	btnLayLoad = new wxButton(this, LAY_LOAD, wxT("Load"));
	btnLayDuplicate = new wxButton(this, LAY_DUPLICATE, wxT("Duplicate"));
	btnLayRename = new wxButton(this, LAY_RENAME, wxT("Rename"));

	chkPtEmissions = new wxCheckBox(this, LAY_CBOXES, wxT("Output All Emissions"));
	chkPtEmissions->SetToolTip(wxT("See how all the emissions are generated within the device. Presently no good way to visualize data"));
	chkAllRates = new wxCheckBox(this, LAY_CBOXES, wxT("Output All Rates"));
	chkAllRates->SetToolTip(wxT("See all internal rates calculated to individual energy states. Presently no good way to visualize data"));
	chkContact = new wxCheckBox(this, LAY_CBOXES, wxT("Contact"));
	chkContact->SetToolTip(wxT("Is this layer a contact? Contact uses are defined as simulation parameters. Not all contacts may be active during a simulation. Contacts allow customizable interventions into the model in addition to boundary conditions."));
	chkAffectMesh = new wxCheckBox(this, LAY_CBOXES, wxT("Affect Mesh"));
	chkAffectMesh->SetToolTip(wxT("A discretized model is formed based off all the (overlapping) layers. Should this layer affect the discretization? Undefined behavior for areas which no layers affect the mesh."));
	chkExclusiveMesh = new wxCheckBox(this, LAY_CBOXES, wxT("Exclusive Layer"));
	chkExclusiveMesh->SetToolTip(wxT("Any discretization in this layer will ignore all other layers. If multiple exclusive layers overlap, it will choose the first encountered. First encountered is likely - but not guaranteed - the order of the layer selection list."));
	chkHide = new wxCheckBox(this, LAY_CBOXES, wxT("Hide"));
	chkHide->SetToolTip(wxT("Don't show this layer in the visual below. The visual resizes to fit the shown contents."));
	
	Scaling = new wxStaticText(this, wxID_ANY, wxT("Scaling:"));
	Scaling->SetToolTip(wxT("HIGHLY EXPERIMENTAL. Discretizations have their area/volume scaled down, reducing the number of carriers, etc. Additionally, for highly reflective/absorbtive materials (such as metal finger contacts), a percentage of the light will consider this material to be air and pass through."));
	txtScaling = new wxTextCtrl(this, LAY_TBOX, wxT("1.0"), wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	SpacingCenter = new wxStaticText(this, wxID_ANY, wxT("Mid Spacing:"));
	SpacingCenter->SetToolTip(wxT("(nm) In mesh development, the discretizations at the center of this layer will have this spacing, and gradually adjust to the edge spacing."));
	txtSpacingCenter = new wxTextCtrl(this, LAY_TBOX, wxT(""), wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	SpacingEdge = new wxStaticText(this, wxID_ANY, wxT("Edge Spacing:"));
	SpacingEdge->SetToolTip(wxT("(nm) In mesh development, the discretizations at the edges of this layer will have this spacing, and gradually adjust to the center spacing."));
	txtSpacingEdge = new wxTextCtrl(this, LAY_TBOX, wxT(""), wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	wxArrayString layerShapes;
	layerShapes.Add(wxT("Point"));
	layerShapes.Add(wxT("Line"));
	cbLayerShape = new wxComboBox(this, LAY_CHANGE_SHAPE, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, layerShapes, wxCB_DROPDOWN | wxCB_READONLY);
	cbLayerShape->SetSelection(1);

	wxArrayString units;
	units.Add(wxT("m"));
	units.Add(wxT("cm"));
	units.Add(wxT("mm"));
	units.Add(wxT("\u03BCm"));	//microns
	units.Add(wxT("nm"));
	cbLyrShapeUnits = new wxComboBox(this, LAY_SCALES, wxT("Units"), wxDefaultPosition, wxDefaultSize, units, wxCB_DROPDOWN | wxCB_READONLY);
	cbLyrShapeUnits->SetSelection(3);	//um

	lyrX = new wxStaticText(this, wxID_ANY, wxT("X"));
	lyrY = new wxStaticText(this, wxID_ANY, wxT("Y"));
	lyrZ = new wxStaticText(this, wxID_ANY, wxT("Z"));
	
	
	LyrStart = new wxStaticText(this, wxID_ANY, wxT("Start Position:"));
	txtLyrStartX = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrStartY = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrStartZ = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	LyrDim1 = new wxStaticText(this, wxID_ANY, wxT("Dimensions:"));
	txtLyrDim1X = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrDim1Y = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrDim1Z = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	LyrDim2 = new wxStaticText(this, wxID_ANY, wxT("Dimensions(2):"));
	txtLyrDim2X = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrDim2Y = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrDim2Z = new wxTextCtrl(this, LAY_SHAPE_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	wxArrayString vertices;
	vertices.Add(wxT("Vertices"));
	cbLayerShapeVertexList = new wxComboBox(this, LAY_SHAPE_SWITCH_VERTEX, wxT("Vertices"), wxDefaultPosition, wxDefaultSize, vertices, wxCB_DROPDOWN | wxCB_READONLY);
	txtLyrShapeVertX = new wxTextCtrl(this, LAY_SHAPE_ALTER_VERTEX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrShapeVertY = new wxTextCtrl(this, LAY_SHAPE_ALTER_VERTEX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtLyrShapeVertZ = new wxTextCtrl(this, LAY_SHAPE_ALTER_VERTEX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	
	/////////////////////////////////////////////end layer definitions of objects (no sizers)

	cbAllMats = new wxComboBox(this, LAY_MISC, wxT("All Materials"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbAllMats->SetToolTip(wxT("A listing of all the defined materials which can be applied to this layer via a positionally based probability distriubtion."));
	btnAddMat = new wxButton(this, LAY_ADD_MAT, wxT("Add Material"));
	btnAddMat->SetToolTip(wxT("Add this material to the selected layer"));
	btnChangeMat = new wxButton(this, LAY_BTN_CHANGE_MAT, wxT("Change Material"));
	btnChangeMat->SetToolTip(wxT("Keep the data below the same, but change the material to the selection above"));
	btnRemoveMat = new wxButton(this, LAY_REMOVE_MAT, wxT("Remove Material"));
	btnRemoveMat->SetToolTip(wxT("Remove this material description from the layer entirely"));

	cbMatDescriptors = new wxComboBox(this, LAY_CHANGE_MAT, wxT("Selected Materials"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbMatDescriptors->SetToolTip(wxT("Each of these materials are applied to the layer as described below"));

	wxArrayString distribution;
	distribution.Add(wxT("Unity"));	//discrete - everywhere is one
	distribution.Add(wxT("Normalized Area"));	//banded
	distribution.Add(wxT("Gaussian"));	//over whole area
	distribution.Add(wxT("Exponential"));	//over whole area in direction of start to stop
	distribution.Add(wxT("Linear"));	//LINEAR
	distribution.Add(wxT("Parabolic"));	//LINEAR

	cbMatDistType = new wxComboBox(this, LAY_MAT_FUNCTION, wxT("Material Distribution"), wxDefaultPosition, wxDefaultSize, distribution, wxCB_DROPDOWN | wxCB_READONLY);
	cbMatDistType->SetSelection(0);
	cbMatDistType->SetToolTip(wxT("How is the material going to be distributed across the layer?"));
	cbMatUnits = new wxComboBox(this, LAY_SCALES, wxT("Units"), wxDefaultPosition, wxDefaultSize, units, wxCB_DROPDOWN | wxCB_READONLY);
	cbMatUnits->SetSelection(1);
	
	MatX = new wxStaticText(this, wxID_ANY, wxT("X"));
	MatY = new wxStaticText(this, wxID_ANY, wxT("Y"));
	MatZ = new wxStaticText(this, wxID_ANY, wxT("Z"));

	MatStart = new wxStaticText(this, wxID_ANY, wxT("Start Position:"));
	MatStart->SetToolTip(wxT("Local position as to where this descriptor starts describing the material placement"));
	txtMatStartX = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtMatStartY = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtMatStartZ = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	MatHalf = new wxStaticText(this, wxID_ANY, wxT("Half Position:"));
	MatHalf->SetToolTip(wxT("Local position where the probability goes to either half the magnitude(Gaussian/Exponential) or zero(Linear/Parabolic/Normalized)."));
	txtMatHalfX = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtMatHalfY = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtMatHalfZ = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	btnViewMaterial = new wxButton(this, LAY_VIEW_MAT, wxT("View Material Details"));
	
	////////////////////////////////////end material descriptor definitions

	cbAllImps = new wxComboBox(this, LAY_MISC, wxT("All Impurities"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbAllImps->SetToolTip(wxT("A listing of all the defined impurities which can be applied to this layer via a positionally based probability distriubtion."));
	btnAddImp = new wxButton(this, LAY_ADD_IMP, wxT("Add Impurity"));
	btnAddImp->SetToolTip(wxT("Add this impurity to the selected layer"));
	btnChangeImp = new wxButton(this, LAY_BTN_CHANGE_IMP, wxT("Change Impurity"));
	btnChangeImp->SetToolTip(wxT("Keep the data below the same, but change the impurity to the selection above"));
	btnRemoveImp = new wxButton(this, LAY_REMOVE_IMP, wxT("Remove Impurity"));
	btnRemoveImp->SetToolTip(wxT("Remove this impurity description from the layer entirely"));

	cbImpDescriptors = new wxComboBox(this, LAY_CHANGE_IMP, wxT("Selected Impurities"), wxDefaultPosition, wxDefaultSize, emptyStringArray, wxCB_DROPDOWN | wxCB_READONLY);
	cbImpDescriptors->SetToolTip(wxT("Each of these impurities are applied to the layer as described below"));
	
	cbImpDistType = new wxComboBox(this, LAY_IMP_FUNCTION, wxT("Impurity Distribution"), wxDefaultPosition, wxDefaultSize, distribution, wxCB_DROPDOWN | wxCB_READONLY);
	cbImpDistType->SetSelection(0);
	cbImpUnits = new wxComboBox(this, LAY_SCALES, wxT("Units"), wxDefaultPosition, wxDefaultSize, units, wxCB_DROPDOWN | wxCB_READONLY);
	cbImpUnits->SetSelection(1);

	ImpX = new wxStaticText(this, wxID_ANY, wxT("X"));
	ImpY = new wxStaticText(this, wxID_ANY, wxT("Y"));
	ImpZ = new wxStaticText(this, wxID_ANY, wxT("Z"));

	ImpMultiplier = new wxStaticText(this, wxID_ANY, wxT("Multiplier:"));
	txtImpMultiplier = new wxTextCtrl(this, LAY_TBOX, wxT("1.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	ImpDensity = new wxStaticText(this, wxID_ANY, wxT("# cm^-3"));
	ImpDensity->SetToolTip(wxT("The maximum concentration found in any finite volume."));
	

	ImpStart = new wxStaticText(this, wxID_ANY, wxT("Start Position:"));
	ImpStart->SetToolTip(wxT("Local position as to where this descriptor starts describing the impurity placement"));
	txtImpStartX = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtImpStartY = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtImpStartZ = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	ImpHalf = new wxStaticText(this, wxID_ANY, wxT("Half Position:"));
	ImpHalf->SetToolTip(wxT("Local position where the probability goes to either half the magnitude(Gaussian/Exponential) or zero(Linear/Parabolic/Normalized)."));
	txtImpHalfX = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtImpHalfY = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtImpHalfZ = new wxTextCtrl(this, LAY_TBOX, wxT("0.0"), wxDefaultPosition, XYZSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	btnViewImpurity = new wxButton(this, LAY_VIEW_IMP, wxT("View Impurity Details"));
	
	/////////////////////////// pltWind must be the LAST object in the class initialized.

	pltWind = new PlotWindow(this, wxID_ANY);
	mpWindow* mp = pltWind->GetMPWindow();
	pltWind->ShowLabel();
	pltWind->HideLegend();
	pltWind->SetTitle("");
	mp->SetMargins(0, 0, 0, 0);
	mpScaleX *sclX = (mpScaleX*)mp->GetLayer(0);
	sclX->SetAlign(mpALIGN_BORDER_BOTTOM);
	pltWind->SetXAxisName("cm");
	mp->EnablePopMenu(false);
//	mp->SetLayerVisible(0, false);	//hide x axis
	mp->SetLayerVisible(1, false);	//hide y axis
//	mp->SetLayerVisible(2, false);	//hide title
	mp->EnableMousePanZoom(false);

	///////////////////////////////////////////// now add everything to their sizers...

	bxszrLayBar->Add(cbLayerChooser, 1, wxEXPAND | wxALL, 5);
	bxszrLayBar->Add(btnLayAdd, 0, wxALL, 5);
	bxszrLayBar->Add(btnLayRem, 0, wxALL, 5);
	bxszrLayBar->Add(btnLaySave, 0, wxALL, 5);
	bxszrLayBar->Add(btnLayLoad, 0, wxALL, 5);
	bxszrLayBar->Add(btnLayDuplicate, 0, wxALL, 5);
	bxszrLayBar->Add(btnLayRename, 0, wxALL, 5);

	//////////////////////////////////////////////////

	//left side
	fgszrLyrEntries->Add(chkContact, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);
	fgszrLyrEntries->Add(chkAllRates, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);

	fgszrLyrEntries->Add(chkAffectMesh, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);
	fgszrLyrEntries->Add(chkPtEmissions, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);

	fgszrLyrEntries->Add(chkHide, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);
	fgszrLyrEntries->Add(chkExclusiveMesh, 0, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);
	
	fgszrLyrEntries->Add(SpacingCenter, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxBOTTOM, 5);
	fgszrLyrEntries->Add(txtSpacingCenter, 1, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);

	fgszrLyrEntries->Add(SpacingEdge, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxBOTTOM, 5);
	fgszrLyrEntries->Add(txtSpacingEdge, 1, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);

	fgszrLyrEntries->Add(Scaling, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxBOTTOM, 5);
	fgszrLyrEntries->Add(txtScaling, 1, wxALIGN_CENTER_VERTICAL | wxBOTTOM, 5);

	///////////////////////////////////
	fgszrLyrShape->Add(cbLyrShapeUnits, 0, wxALIGN_CENTER, 0);
	fgszrLyrShape->Add(lyrX, 0, wxALIGN_CENTER, 0);
	fgszrLyrShape->Add(lyrY, 0, wxALIGN_CENTER, 0);
	fgszrLyrShape->Add(lyrZ, 0, wxALIGN_CENTER, 0);
	/////
	fgszrLyrShape->Add(LyrStart, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrLyrShape->Add(txtLyrStartX, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrStartY, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrStartZ, 0, 0, 0);
	///
	fgszrLyrShape->Add(LyrDim1, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrLyrShape->Add(txtLyrDim1X, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrDim1Y, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrDim1Z, 0, 0, 0);
	///
	fgszrLyrShape->Add(LyrDim2, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrLyrShape->Add(txtLyrDim2X, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrDim2Y, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrDim2Z, 0, 0, 0);
	///
	fgszrLyrShape->Add(cbLayerShapeVertexList, wxALIGN_CENTER, 0, 0);
	fgszrLyrShape->Add(txtLyrShapeVertX, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrShapeVertY, 0, 0, 0);
	fgszrLyrShape->Add(txtLyrShapeVertZ, 0, 0, 0);
	///////////////////////
	bxszrLyrPrim->Add(fgszrLyrEntries, 0, wxALL | wxEXPAND, 5);
	bxszrLyrPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszrLyrPrim->Add(cbLayerShape, 0, wxALL | wxALIGN_CENTER, 5);
	bxszrLyrPrim->Add(fgszrLyrShape, 0, wxALL | wxEXPAND, 5);
	///////////////////ends the left side!///////////////////////////////

	//now to do the materials stuff/////////////////////////////////////////
	bxszMatListAdd->Add(cbAllMats, 1, wxEXPAND | wxALL, 5);
	bxszMatListAdd->Add(btnAddMat, 0, wxALL, 5);
	//////////////
	bxszMatChangeRemove->Add(btnChangeMat, 1, wxALL, 2);
	bxszMatChangeRemove->Add(btnRemoveMat, 1, wxALL, 2);
	/////////////////////
	fgszrMatShape->Add(cbMatUnits, 0, wxALIGN_CENTER, 0);
	fgszrMatShape->Add(MatX, 0, wxALIGN_CENTER, 0);
	fgszrMatShape->Add(MatY, 0, wxALIGN_CENTER, 0);
	fgszrMatShape->Add(MatZ, 0, wxALIGN_CENTER, 0);
	/////
	fgszrMatShape->Add(MatStart, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrMatShape->Add(txtMatStartX, 0, 0, 0);
	fgszrMatShape->Add(txtMatStartY, 0, 0, 0);
	fgszrMatShape->Add(txtMatStartZ, 0, 0, 0);
	///
	fgszrMatShape->Add(MatHalf, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrMatShape->Add(txtMatHalfX, 0, 0, 0);
	fgszrMatShape->Add(txtMatHalfY, 0, 0, 0);
	fgszrMatShape->Add(txtMatHalfZ, 0, 0, 0);
	///////////////////
	bxszMatPrim->Add(bxszMatListAdd, 0, wxEXPAND | wxALL, 5);
	bxszMatPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszMatPrim->Add(cbMatDescriptors, 0, wxEXPAND | wxALL, 5);
	bxszMatPrim->Add(bxszMatChangeRemove, 0, wxEXPAND | wxALL, 5);
	bxszMatPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszMatPrim->Add(cbMatDistType, 0, wxALL | wxALIGN_CENTER, 5);
	bxszMatPrim->Add(fgszrMatShape, 0, wxEXPAND | wxALL, 5);
	bxszMatPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszMatPrim->AddStretchSpacer();
	bxszMatPrim->Add(btnViewMaterial, 0, wxALIGN_CENTER | wxALL, 5);
	/////////////////////////////////////end the material selection stuff

	/////////now the impurity stuff////////////////////////

	bxszImpListAdd->Add(cbAllImps, 1, wxEXPAND | wxALL, 5);
	bxszImpListAdd->Add(btnAddImp, 0, wxALL, 5);
	//////////////
	bxszImpChangeRemove->Add(btnChangeImp, 0, wxEXPAND | wxALL, 2);
	bxszImpChangeRemove->Add(btnRemoveImp, 0, wxEXPAND | wxALL, 2);
	///////////////
	bxSzImpMultiplier->Add(ImpMultiplier, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxRIGHT, 1);
	bxSzImpMultiplier->Add(txtImpMultiplier, 0, wxRIGHT, 2);
	bxSzImpMultiplier->Add(ImpDensity, 1, wxALIGN_CENTER_VERTICAL | wxALIGN_LEFT, 0);

	/////////////////////
	fgszrImpShape->Add(cbImpUnits, 0, wxALIGN_CENTER, 0);
	fgszrImpShape->Add(ImpX, 0, wxALIGN_CENTER, 0);
	fgszrImpShape->Add(ImpY, 0, wxALIGN_CENTER, 0);
	fgszrImpShape->Add(ImpZ, 0, wxALIGN_CENTER, 0);
	/////
	fgszrImpShape->Add(ImpStart, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrImpShape->Add(txtImpStartX, 0, 0, 0);
	fgszrImpShape->Add(txtImpStartY, 0, 0, 0);
	fgszrImpShape->Add(txtImpStartZ, 0, 0, 0);
	///
	fgszrImpShape->Add(ImpHalf, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT, 0);
	fgszrImpShape->Add(txtImpHalfX, 0, 0, 0);
	fgszrImpShape->Add(txtImpHalfY, 0, 0, 0);
	fgszrImpShape->Add(txtImpHalfZ, 0, 0, 0);
	///////////////////
	bxszImpPrim->Add(bxszImpListAdd, 0, wxEXPAND | wxALL, 5);
	bxszImpPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszImpPrim->Add(cbImpDescriptors, 0, wxEXPAND | wxALL, 5);
	bxszImpPrim->Add(bxszImpChangeRemove, 0, wxEXPAND | wxALL, 5);
	bxszImpPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszImpPrim->Add(bxSzImpMultiplier, 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	bxszImpPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszImpPrim->Add(cbImpDistType, 0, wxALL | wxALIGN_CENTER, 5);
	bxszImpPrim->Add(fgszrImpShape, 0, wxEXPAND | wxALL, 5);
	bxszImpPrim->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszImpPrim->AddStretchSpacer();
	bxszImpPrim->Add(btnViewImpurity, 0, wxALIGN_CENTER | wxALL, 5);
	///////////////////////////
	//now put it all together!
	bxszrMainSeparator->Add(bxszrLyrPrim, 0, wxEXPAND | wxALL, 5);
	bxszrMainSeparator->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0, wxEXPAND, 0);	//note these are safe because attached to window. Will be deleted when window deleted!
	bxszrMainSeparator->Add(bxszMatPrim, 1, wxEXPAND | wxALL, 5);
	bxszrMainSeparator->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 0 , wxEXPAND ,0);
	bxszrMainSeparator->Add(bxszImpPrim, 1, wxEXPAND | wxALL, 5);

	////////////////////////////
	bxszrPrimary->Add(bxszrLayBar, 0, wxEXPAND, 0);
	bxszrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszrPrimary->Add(bxszrMainSeparator, 0, wxEXPAND | wxALL, 2);
	bxszrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	bxszrPrimary->Add(pltWind->GetMPWindow(), 1, wxEXPAND | wxALL, 1);	//fill the bottom with empty space

	
	TransferToGUI();

	
	SetSizer(bxszrPrimary);
	curLayer = nullptr;
	curMatInLayer = nullptr;
	curImpInLayer = nullptr;
	
	
}

devLayerTopPanel::~devLayerTopPanel()
{
/*	delete lpropertiesWindow;
	delete lmaterialsWindow;
	delete limpuritiesWindow;

	delete layAdd;
	delete layRem;
	delete laySave;
	delete layLoad;
	delete layersWindow;
	*/
	delete pltWind;

	delete cbLayerChooser;
	delete btnLayAdd;
	delete btnLayRem;
	delete btnLaySave;
	delete btnLayLoad;
	delete btnLayRename;
	delete btnLayDuplicate;

//	wxNotebook* nbLayersWindow;	//may cut out completely!


	////////////////////Section 1/////////////////////////

	delete chkAffectMesh;
	delete chkExclusiveMesh;
	delete chkHide;
	delete chkContact;
	delete chkPtEmissions;
	delete chkAllRates;

	delete txtScaling;
	delete txtSpacingCenter;
	delete txtSpacingEdge;

	delete cbLayerShape;
	delete cbLyrShapeUnits;
	delete txtLyrStartX;
	delete txtLyrStartY;
	delete txtLyrStartZ;
	delete txtLyrDim1X;
	delete txtLyrDim1Y;
	delete txtLyrDim1Z;
	delete txtLyrDim2X;
	delete txtLyrDim2Y;
	delete txtLyrDim2Z;

	delete cbLayerShapeVertexList;
	delete txtLyrShapeVertX;
	delete txtLyrShapeVertY;
	delete txtLyrShapeVertZ;

	delete Scaling;
	delete lyrX;
	delete lyrY;
	delete lyrZ;
	delete SpacingCenter;
	delete SpacingEdge;
	
	delete LyrStart;
	delete LyrDim1;
	delete LyrDim2;

	///////////////////////Section 2/////////////////////////////

	delete cbAllMats;
	delete btnAddMat;
	delete btnChangeMat;

	delete cbMatDescriptors;	//just go with the material name!
	delete btnRemoveMat;

	delete cbMatDistType;
	delete cbMatUnits;
	delete txtMatStartX;
	delete txtMatStartY;
	delete txtMatStartZ;
	delete txtMatHalfX;
	delete txtMatHalfY;
	delete txtMatHalfZ;

	delete btnViewMaterial;

	delete MatStart;
	delete MatHalf;
	delete MatX;
	delete MatY;
	delete MatZ;

	/////////////////////////Section 3/////////////////////////////

	delete cbAllImps;
	delete btnAddImp;
	delete btnChangeImp;

	delete cbImpDescriptors;	//just go with the Imperial name!
	delete btnRemoveImp;

	delete cbImpDistType;
	delete cbImpUnits;
	delete txtImpMultiplier;
	delete txtImpStartX;
	delete txtImpStartY;
	delete txtImpStartZ;
	delete txtImpHalfX;
	delete txtImpHalfY;
	delete txtImpHalfZ;

	delete ImpStart;
	delete ImpHalf;
	delete ImpX;
	delete ImpY;
	delete ImpZ;
	delete ImpMultiplier;
	delete ImpDensity;

	delete btnViewImpurity;
}

Layer* devLayerTopPanel::getLayer()
{
	return(curLayer);
}

Material* devLayerTopPanel::getCurMat()
{
	if (curMatInLayer)
		return(curMatInLayer->data);
	return(nullptr);
}

Impurity* devLayerTopPanel::getCurImp()
{
	if (curImpInLayer)
		return(curImpInLayer->data);
	return(nullptr);
}

ldescriptor<Material*>* devLayerTopPanel::getCurMatDescriptor()
{
	return(curMatInLayer);
}

ldescriptor<Impurity*>* devLayerTopPanel::getCurImpDescriptor()
{
	return (curImpInLayer);
}

ModelDescribe* devLayerTopPanel::getMDesc()
{
	return(getMyClassParent()->getMDesc());
}

mainFrame* devLayerTopPanel::getMyClassParent()
{
	return((mainFrame*)GetParent()->GetParent()->GetParent());
}

void devLayerTopPanel::VerifyInternalVars() {
	ModelDescribe *mdesc = getMDesc();
	if (mdesc == nullptr) {
		curLayer = nullptr;
		curMatInLayer = nullptr;
		curImpInLayer = nullptr;
	}

	if (mdesc->HasLayer(curLayer) == false) {
		curLayer = nullptr;
		curMatInLayer = nullptr;
		curImpInLayer = nullptr;
	}

	if (hidePlot.size() != mdesc->layers.size()) {
		hidePlot.resize(mdesc->layers.size(), false);
	}

	if (curLayer) {
		if (FindValInVector(curLayer->impurities, curImpInLayer) >= curLayer->impurities.size()) {
			if (curLayer->impurities.size() > 0)
				curImpInLayer = curLayer->impurities[0];
			else
				curImpInLayer = nullptr;
		}

		if (FindValInVector(curLayer->matls, curMatInLayer) >= curLayer->matls.size()) {
			if (curLayer->matls.size() > 0)
				curMatInLayer = curLayer->matls[0];
			else
				curMatInLayer = nullptr;
		}
	}
	mdesc->UpdateDimensions();
}


void devLayerTopPanel::NewLayer(wxCommandEvent& event)
{
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;
	wxTextEntryDialog entry(this, wxT("Enter the layer's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	Layer* lyr = new Layer();
	lyr->id = -1;
	lyr->name = entry.GetValue();
	lyr->AffectMesh = true;
	lyr->contactid = 0;
	lyr->exclusive = false;
	lyr->out.AllRates = false;
	lyr->out.OpticalPointEmissions = false;
	lyr->priority = false;
	lyr->shape = new Shape();
	lyr->shape->type = SHPLINE;

	//let's be smart about adding a layer!
	double maxX = 0.0;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i) {
		if (mdesc->layers[i]->shape) {
			XSpace max = mdesc->layers[i]->shape->max;
			if (max.x > maxX)
				maxX = max.x;
		}
	}

	lyr->shape->AddPoint(XSpace(maxX));
	lyr->shape->AddPoint(XSpace(maxX + 1.0e-4));	//arbitrarily a micron. Shit, i dunno.
	lyr->sizeScale = 1.0;
	lyr->spacingboundary = 1e-6;
	lyr->spacingcenter = 1e-5;

	mdesc->layers.push_back(lyr);
	mdesc->SetUnknownIDs();
	curLayer = lyr;


	wxArrayString LayerList;
	unsigned int masterLayerSel = -1;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i)
	{
		LayerList.Add(mdesc->layers[i]->name);
		if (mdesc->layers[i] == curLayer)
			masterLayerSel = i;
	}
	cbLayerChooser->Clear();
	cbLayerChooser->Append(LayerList);
	cbLayerChooser->SetSelection(masterLayerSel);
	hidePlot.push_back(false);

	mdesc->UpdateDimensions();

	TransferToGUI();
}


void devLayerTopPanel::UpdatePlot() {
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr || pltWind==nullptr)
		return;

	double factor = getUnitFactor(false, cbLyrShapeUnits);

	pltWind->RemoveAllData();
	std::vector<double> x, y;
	double resolutionX = mdesc->resolution.x;
	if (resolutionX < 1.0e-10)
		resolutionX = 1.0e-10;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i) {
		if (i < hidePlot.size() && hidePlot[i] == true)
			continue;
		x.clear();
		y.clear();
		Layer* lyr = mdesc->layers[i];
		if (lyr->shape) {
			int type = lyr->shape->type;
			if (type == SHPPOINT) {
				//make a rectangular area for the point.
				XSpace vert = lyr->shape->getPoint(0);
				vert.x *= factor;
				x.push_back(vert.x);
				y.push_back(0.0);

				x.push_back(vert.x);
				y.push_back(1.0);

				x.push_back(vert.x + resolutionX);
				y.push_back(1.0);

				x.push_back(vert.x + resolutionX);
				y.push_back(0.0);

				x.push_back(vert.x);
				y.push_back(0.0);
			}
			else if (type == SHPLINE) {
				XSpace v1, v2;
				v1 = lyr->shape->getPoint(0);
				v2 = lyr->shape->getPoint(1);
				v1.x *= factor;
				v2.x *= factor;

				x.push_back(v1.x);
				y.push_back(0.0);

				x.push_back(v1.x);
				y.push_back(1.0);

				x.push_back(v2.x);
				y.push_back(1.0);

				x.push_back(v2.x);
				y.push_back(0.0);

				x.push_back(v1.x);
				y.push_back(0.0);
			}
			wxString nm = lyr->name;
			LineData *ldat = pltWind->CreateData(nm, x, PL_TYPE_X);
			ldat->SetAsActiveX();
			if (lyr == curLayer) {
				ldat->GetPen().SetWidth(5);
				AutoRotateColor(ldat->GetPen(), 1);
			}
			else {
				ldat->GetPen().SetWidth(2);
				AutoRotateColor(ldat->GetPen(), 0);
			}

			ldat = pltWind->CreateData(nm, y, PL_TYPE_Y);
			ldat->SetAsActiveY();
			if (lyr == curLayer) {
				ldat->GetPen().SetWidth(5);
				AutoRotateColor(ldat->GetPen(), 1);
			}
			else {
				ldat->GetPen().SetWidth(2);
				AutoRotateColor(ldat->GetPen(), 0);
			}
			int align = i % 2;
			if (align == 0)
				align = mpALIGN_NW;
			else if (align == 1)
				align = mpALIGN_SE;
//			else if (align == 2)
//				align = mpALIGN_TOP;
			ldat->SetAlignment(align);


		}
	}
	pltWind->Enable();
	pltWind->GetWXWindow()->Show();
	pltWind->UpdatePlot();
	mpWindow *mp = pltWind->GetMPWindow();
	mp->SetMargins(20, 0, 20, 0);

	bxszrPrimary->Layout();
}

void devLayerTopPanel::RemoveLayer(wxCommandEvent& event)
{
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr || curLayer == nullptr || cbLayerChooser==nullptr)
		return;

	if (pltWind == nullptr)
		return;

	int index = cbLayerChooser->GetSelection();
	if (index < (int)mdesc->layers.size() && index != wxNOT_FOUND)
	{
		delete mdesc->layers[index];
		mdesc->layers.erase(mdesc->layers.begin() + index);

		if (index < (int)hidePlot.size())
			hidePlot.erase(hidePlot.begin() + index);
	}

	mdesc->Validate();

	curLayer = (unsigned int)index < mdesc->layers.size() ? mdesc->layers[index] : nullptr;

	wxArrayString LayerList;
	unsigned int masterLayerSel = -1;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i)
	{
		LayerList.Add(mdesc->layers[i]->name);
		if (mdesc->layers[i] == curLayer)
			masterLayerSel = i;
	}
	cbLayerChooser->Clear();
	cbLayerChooser->Append(LayerList);
	cbLayerChooser->SetSelection(masterLayerSel);

	mdesc->UpdateDimensions();

	TransferToGUI();	//sends all the data to the gui, then enables/disables gui as needed.
}

void devLayerTopPanel::RenameLayer(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;

	wxTextEntryDialog getName(this, wxT("Name"), wxT("Rename Layer"), curLayer->name);
	if (getName.ShowModal() == wxID_CANCEL)
		return;
	std::string newname = getName.GetValue();
	

	int sel = cbLayerChooser->GetSelection();
	if (newname != "")
	{
		if (cbLayerChooser->GetValue().ToStdString() == curLayer->name)
			cbLayerChooser->SetValue(newname);

		curLayer->name = newname;

		if (curLayer->contactid > 0) {
			ModelDescribe * mdesc = getMDesc();
			if (mdesc) {
				for (unsigned int i = 0; i < mdesc->simulation->contacts.size(); ++i)
				{
					if (mdesc->simulation->contacts[i]->id == curLayer->contactid) {
						mdesc->simulation->contacts[i]->name = newname + " contact";
						break;
					}
				}
			}
		}
	}

	UpdateLayerListDisplay();
	UpdatePlot();
	return;
}


void devLayerTopPanel::SwitchLayer(wxCommandEvent& event)
{
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr || cbLayerChooser == nullptr)
		return;

	if (pltWind == nullptr)
		return;

	int newSelection = cbLayerChooser->GetSelection();
	std::string layName = cbLayerChooser->GetValue().ToStdString();

	Layer* testLay = nullptr;	//try to find it the simple way...
	if (newSelection < (int)mdesc->layers.size() && newSelection > 0)
	{
		testLay = mdesc->layers[newSelection];
		if (testLay->name != layName)
			testLay = nullptr;
	}

	//no easy find, search though everything
	if (testLay == nullptr)
	{
		for (unsigned int i = 0; i < mdesc->layers.size(); ++i)
		{
			if (mdesc->layers[i]->name == layName)
			{
				testLay = mdesc->layers[i];
				break;
			}
		}
	}

	if (curLayer != testLay)
	{
		curLayer = testLay;
		curMatInLayer = nullptr;	//let the transfer update things appropraitely!
		curImpInLayer = nullptr;
		TransferToGUI();
	}
	return;

}

void devLayerTopPanel::SizeRight()
{
//	lpropertiesWindow->SizeRight();
//	lmaterialsWindow->SizeRight();
//	limpuritiesWindow->SizeRight();
	SetSizer(bxszrPrimary);
}

void devLayerTopPanel::saveLayer(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Layer files (*.lyr)|*.lyr"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		curLayer->SaveToXML(oFile);
		oFile.closeFile();
	}
	/*
	if (layerChooser->IsListEmpty() || layerChooser->GetSelection() == -1)
	{
		wxMessageDialog diag(this, wxT("There are no layers to save!"), wxT("No Layers"));
		diag.ShowModal();
		return; //empty
	}
	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("DEV files (*.dev)|*.dev"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;
	SwitchLayer(event);
	int layerNum = layerChooser->GetSelection();
	wxFile* oFile = new wxFile(saveDialog.GetPath(), wxFile::write);
	if (oFile->IsOpened())
		layerToFile(&(layers_list[layerNum]), oFile);
	oFile->Flush();
	delete oFile;
	*/
}
void devLayerTopPanel::LoadLayer(wxCommandEvent& event)
{
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Layer files (*.lyr)|*.lyr"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	Layer* lay = new Layer();
	LoadLayerChildren(file, lay, *mdesc);
	lay->id = -1;	//don't modify the contact, which will maintain the positive ID if there was one.
	mdesc->layers.push_back(lay);
	mdesc->SetUnknownIDs();
	curLayer = lay;

	//now try and make sure all the things are as updated as possible!
	for (unsigned int i = 0; i < lay->impurities.size(); ++i)
	{
		int idTest = lay->impurities[i]->id;
		for (unsigned int j = 0; j < mdesc->impurities.size(); ++j)
		{
			if (mdesc->impurities[j]->isDefect())	//don't let defects be a part of this! This is strictly impurities!
				continue;
			if (mdesc->impurities[j]->isImpurity(idTest))
			{
				lay->impurities[i]->data = mdesc->impurities[j];
				break;
			}
		}
	}

	for (unsigned int i = 0; i < lay->matls.size(); ++i)
	{
		int idTest = lay->matls[i]->id;
		lay->matls[i]->data = mdesc->getMaterial(lay->matls[i]->id);
	}

	wxArrayString LayerList;
	unsigned int masterLayerSel = -1;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i)
	{
		LayerList.Add(mdesc->layers[i]->name);
		if (mdesc->layers[i] == curLayer)
			masterLayerSel = i;
	}
	cbLayerChooser->Clear();
	cbLayerChooser->Append(LayerList);
	cbLayerChooser->SetSelection(masterLayerSel);

	mdesc->Validate();
	hidePlot.push_back(false);

	mdesc->UpdateDimensions();

	TransferToGUI();

}

void devLayerTopPanel::DuplicateLayer(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	wxString newname(curLayer->name);
	newname << "_copy";
	wxTextEntryDialog getName(this, wxT("Enter a new name"), wxT("Duplicate Layer"), newname);
	if (getName.ShowModal() == wxID_CANCEL)
		return;

	Layer* lay = new Layer(*curLayer);
	lay->name = getName.GetValue();
	lay->id = -1;

	std::vector<ldescriptor<Material*>*> newMats;
	std::vector<ldescriptor<Impurity*>*> newImps;

	for (unsigned int i = 0; i < lay->impurities.size(); ++i)
		newImps.push_back(new ldescriptor<Impurity*>(*lay->impurities[i]));	//only one pointer in there, and that's what we want it to be.
		//don't want to modify the new stuff! The vector of pointers is currently pointing at the previous layer's data!
	for (unsigned int i = 0; i < lay->matls.size(); ++i)
		newMats.push_back(new ldescriptor<Material*>(*lay->matls[i]));

	lay->impurities.swap(newImps);	//swap the contents. Now this layer has pointers to the copies of the data
	lay->matls.swap(newMats);
	newMats.clear();	//and these have pointers to the other layer's original data.
	newImps.clear();

	mdesc->layers.push_back(lay);
	mdesc->SetUnknownIDs();
	if (lay->contactid > 0)
	{
		std::vector<Contact*>& ctcs = mdesc->simulation->contacts;
		bool found = false;
		for (unsigned int i = 0; i < ctcs.size(); ++i)
		{
			if (ctcs[i]->id == lay->contactid)
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			Contact* ct = new Contact;
			ct->id = lay->id;
			ct->name = lay->name + " contact";
			ctcs.push_back(ct);
		}
	}
	curLayer = lay;
	curMatInLayer = nullptr;
	curImpInLayer = nullptr;

	wxArrayString LayerList;
	unsigned int masterLayerSel = -1;
	for (unsigned int i = 0; i < mdesc->layers.size(); ++i)
	{
		LayerList.Add(mdesc->layers[i]->name);
		if (mdesc->layers[i] == curLayer)
			masterLayerSel = i;
	}
	cbLayerChooser->Clear();
	cbLayerChooser->Append(LayerList);
	cbLayerChooser->SetSelection(masterLayerSel);

	mdesc->UpdateDimensions();

	TransferToGUI();
}

unsigned int devLayerTopPanel::ConvertImpIndex(int index)
{
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return(-1);	//impossible to find it.


	//index is the index going with the drop box. We need to translate it into the index for
	//the data structure
	std::vector<Impurity*>& imps = mdesc->impurities;
	int goodCtr = 0;
	for (unsigned int i = 0; i < imps.size(); ++i)
	{
		if (imps[i]->isDefect())
		{
			//then this value wasn't counted towards the good index
			continue;
		}

		if (goodCtr == index)	//they now match
			return(i);

		//this was on the list, but it wasn't the correct index, so move on to the next one
		goodCtr++;
	}
	return(-1);	//didn't find it!
}

void devLayerTopPanel::UpdateGUIValues(wxCommandEvent& event)
{
	ModelDescribe* mdesc = getMDesc();
	if (pltWind == nullptr)
		return;

	bool needUpdateGUINumbers = false;

	if (curLayer == nullptr)
	{
		if (mdesc && mdesc->layers.size() > 0)
		{
			curLayer = mdesc->layers[0];
			needUpdateGUINumbers = true;
		}
	}

	if (curMatInLayer == nullptr && curLayer)
	{
		if (curLayer->matls.size() > 0)
		{
			curMatInLayer = curLayer->matls[0];
			needUpdateGUINumbers = true;
		}
	}

	if (curImpInLayer == nullptr && curLayer)
	{
		unsigned int sel = ConvertImpIndex(0);
		if (sel < curLayer->impurities.size())
		{
			curImpInLayer = curLayer->impurities[sel];
			needUpdateGUINumbers = true;
		}
	}

	if (needUpdateGUINumbers)
	{
		TransferToGUI();	//this calls this function again after all the data is proper.
		return;
	}

	UpdateLayerListDisplay();

	if (curLayer == nullptr || mdesc==nullptr)
	{
		btnLayAdd->Enable();
		btnLayRem->Disable();
		btnLaySave->Disable();
		btnLayLoad->Enable();
		btnLayRename->Disable();
		btnLayDuplicate->Disable();
		
		cbLayerChooser->Disable();	//if curlayer is nullptr, there is no mdesc or anything in mdesc. so this ought to be disabled as well
		//end top section

		chkAffectMesh->Disable();
		chkExclusiveMesh->Disable();
		chkHide->Disable();
		chkContact->Disable();
		chkPtEmissions->Disable();
		chkAllRates->Disable();

		txtScaling->Disable();
		txtSpacingCenter->Disable();
		txtSpacingEdge->Disable();

		cbLayerShape->Disable();
		cbLyrShapeUnits->Disable();
		txtLyrStartX->Disable();
		txtLyrStartY->Disable();
		txtLyrStartZ->Disable();
		txtLyrDim1X->Disable();
		txtLyrDim1Y->Disable();
		txtLyrDim1Z->Disable();
		txtLyrDim2X->Disable();
		txtLyrDim2Y->Disable();
		txtLyrDim2Z->Disable();

		cbLayerShapeVertexList->Disable();
		txtLyrShapeVertX->Disable();
		txtLyrShapeVertY->Disable();
		txtLyrShapeVertZ->Disable();

		/////////////end left section

		cbAllMats->Disable();
		btnAddMat->Disable();
		btnChangeMat->Disable();
		cbMatDescriptors->Disable();
		btnRemoveMat->Disable();
		cbMatDistType->Disable();
		cbMatUnits->Disable();
		txtMatStartX->Disable();
		txtMatStartY->Disable();
		txtMatStartZ->Disable();
		txtMatHalfX->Disable();
		txtMatHalfY->Disable();
		txtMatHalfZ->Disable();
		btnViewMaterial->Enable();	//alow the user to go to said panels!

		///////////end center section

		cbAllImps->Disable();
		btnAddImp->Disable();
		btnChangeImp->Disable();
		cbImpDescriptors->Disable();
		btnRemoveImp->Disable();
		cbImpDistType->Disable();
		cbImpUnits->Disable();
		txtImpMultiplier->Disable();
		txtImpStartX->Disable();
		txtImpStartY->Disable();
		txtImpStartZ->Disable();
		txtImpHalfX->Disable();
		txtImpHalfY->Disable();
		txtImpHalfZ->Disable();
		pltWind->Disable();
		btnViewImpurity->Enable();	//allow the user to go to said panels!
	}
	else //there is a currently selected layer! Mdesc exists.
	{
		btnLayAdd->Enable();
		btnLayRem->Enable();
		btnLaySave->Enable();
		btnLayLoad->Enable();
		btnLayRename->Enable();
		btnLayDuplicate->Enable();
		cbLayerChooser->Enable();
		pltWind->Enable();
		pltWind->GetMPWindow()->Show();

		if (curLayer->shape && curLayer->shape->getNumVertices() > 0)
			cbLayerShapeVertexList->Enable();
		else
			cbLayerShapeVertexList->Disable();
		/////////////////////
		chkContact->Enable();
		chkAffectMesh->Enable();
		chkPtEmissions->Enable();
		chkAllRates->Enable();

		btnViewImpurity->Enable();	//always allow user to go to view the stuff!
		btnViewMaterial->Enable();
		txtScaling->Enable();
		cbLayerShape->Enable();
		cbLyrShapeUnits->Enable();



		unsigned int which = FindValInVector(mdesc->layers, curLayer);
		if (which < mdesc->layers.size()) {
			if (which >= hidePlot.size()) {
				hidePlot.resize(which + 1, false);
			}
			chkHide->SetValue(hidePlot[which]);
		}


		if (curLayer->AffectMesh)
		{
			chkExclusiveMesh->Enable();
			chkHide->Enable();
			txtSpacingCenter->Enable();
			txtSpacingEdge->Enable();
		}
		else
		{
			curLayer->exclusive = false;
			curLayer->priority = false;
			chkExclusiveMesh->SetValue(false);
			chkHide->SetValue(false);

			chkExclusiveMesh->Disable();
			chkHide->Disable();
			txtSpacingCenter->Disable();
			txtSpacingEdge->Disable();
		}
		
		int selection = cbLayerShape->GetSelection();
		if (selection == 0)	//it's the point shape!
		{
			//everything is disabled except except start and vertices!
			txtLyrStartX->Enable();
			txtLyrStartY->Enable();
			txtLyrStartZ->Enable();
			txtLyrShapeVertX->Enable();
			txtLyrShapeVertY->Enable();
			txtLyrShapeVertZ->Enable();

			txtLyrDim1X->Disable();
			txtLyrDim1Y->Disable();
			txtLyrDim1Z->Disable();
			txtLyrDim2X->Disable();
			txtLyrDim2Y->Disable();
			txtLyrDim2Z->Disable();
		}
		else if (selection == 1) //just a line!
		{
			txtLyrStartX->Enable();
			txtLyrStartY->Enable();
			txtLyrStartZ->Enable();
			txtLyrShapeVertX->Enable();
			txtLyrShapeVertY->Enable();
			txtLyrShapeVertZ->Enable();
			txtLyrDim1X->Enable();
			txtLyrDim1Y->Enable();
			txtLyrDim1Z->Enable();

			txtLyrDim2X->Disable();
			txtLyrDim2Y->Disable();
			txtLyrDim2Z->Disable();
		}
		else
		{
			//unknown, so disable everything to make sure I fix it when more things are properly added
			txtLyrStartX->Disable();
			txtLyrStartY->Disable();
			txtLyrStartZ->Disable();
			txtLyrShapeVertX->Disable();
			txtLyrShapeVertY->Disable();
			txtLyrShapeVertZ->Disable();
			txtLyrDim1X->Disable();
			txtLyrDim1Y->Disable();
			txtLyrDim1Z->Disable();
			txtLyrDim2X->Disable();
			txtLyrDim2Y->Disable();
			txtLyrDim2Z->Disable();
		}


		//now figure out what's going on with the materials!

		if (mdesc->materials.size() == 0)
		{
			cbAllMats->Disable();
			btnAddMat->Disable();
			btnChangeMat->Disable();
		}
		else
		{
			cbAllMats->Enable();
			btnAddMat->Enable();
		}

		if (curMatInLayer == nullptr)
		{
			btnRemoveMat->Disable();
			btnChangeMat->Disable();
			cbMatDescriptors->Disable();//there are no materials that can be selected at this point for the layer... or it would have assigned one by default
			cbMatDistType->Disable();
			cbMatUnits->Disable();
			txtMatStartX->Disable();
			txtMatStartY->Disable();
			txtMatStartZ->Disable();
			txtMatHalfX->Disable();
			txtMatHalfY->Disable();
			txtMatHalfZ->Disable();
		}
		else // there is material selected in the mar mat layer...
		{
			btnRemoveMat->Enable();
			
			selection = cbAllMats->GetSelection();
			Material* allMatSelection = nullptr;
			if ((unsigned int)selection < mdesc->materials.size())
				allMatSelection = mdesc->materials[selection];

			if (allMatSelection == getCurMat())
				btnChangeMat->Disable();
			else
				btnChangeMat->Enable();

			cbMatDescriptors->Enable();//there are no materials that can be selected at this point for the layer... or it would have assigned one by default
			cbMatDistType->Enable();

			selection = cbMatDistType->GetSelection();
			if (selection == 0)	//unity. no values need to be added! The code just assumes probability 1 everywhere.
			{
				MatStart->SetLabel(wxT("Entire"));
				MatHalf->SetLabel(wxT("Layer"));
				cbMatUnits->Disable();
				txtMatStartX->Disable();
				txtMatStartY->Disable();
				txtMatStartZ->Disable();
				txtMatHalfX->Disable();
				txtMatHalfY->Disable();
				txtMatHalfZ->Disable();
			}
			else if (selection == 1)
			{
				MatStart->SetLabel(wxT("Start Position:"));
				MatHalf->SetLabel(wxT("Stop Position:"));
				cbMatUnits->Enable();
				txtMatStartX->Enable();
				txtMatStartY->Enable();
				txtMatStartZ->Enable();
				txtMatHalfX->Enable();
				txtMatHalfY->Enable();
				txtMatHalfZ->Enable();
			}
			else if (selection == 2)
			{
				MatStart->SetLabel(wxT("Center Position:"));
				MatHalf->SetLabel(wxT("Half Position:"));
				cbMatUnits->Enable();
				txtMatStartX->Enable();
				txtMatStartY->Enable();
				txtMatStartZ->Enable();
				txtMatHalfX->Enable();
				txtMatHalfY->Enable();
				txtMatHalfZ->Enable();
			}
			else if (selection == 3)
			{
				MatStart->SetLabel(wxT("Center Position:"));
				MatHalf->SetLabel(wxT("Half Position:"));
				cbMatUnits->Enable();
				txtMatStartX->Enable();
				txtMatStartY->Enable();
				txtMatStartZ->Enable();
				txtMatHalfX->Enable();
				txtMatHalfY->Enable();
				txtMatHalfZ->Enable();
			}
			else if (selection == 4)
			{
				MatStart->SetLabel(wxT("Start Position:"));
				MatHalf->SetLabel(wxT("Zero Position:"));
				cbMatUnits->Enable();
				txtMatStartX->Enable();
				txtMatStartY->Enable();
				txtMatStartZ->Enable();
				txtMatHalfX->Enable();
				txtMatHalfY->Enable();
				txtMatHalfZ->Enable();
			}
			else if (selection == 5)
			{
				MatStart->SetLabel(wxT("Center Position:"));
				MatHalf->SetLabel(wxT("Zero Position:"));
				cbMatUnits->Enable();
				txtMatStartX->Enable();
				txtMatStartY->Enable();
				txtMatStartZ->Enable();
				txtMatHalfX->Enable();
				txtMatHalfY->Enable();
				txtMatHalfZ->Enable();
			}
			else
			{
				//disable everything so I catch it later if/when I add things
				cbMatUnits->Disable();
				txtMatStartX->Disable();
				txtMatStartY->Disable();
				txtMatStartZ->Disable();
				txtMatHalfX->Disable();
				txtMatHalfY->Disable();
				txtMatHalfZ->Disable();
			}
		}

		//////now figure out what's going on with the impurities
		unsigned int translatedImpSelection = ConvertImpIndex(0);
		//less straight forward because materials store defects in the mdesc->impurities section!
		if (translatedImpSelection >= mdesc->impurities.size())
		{
			cbAllImps->Disable();
			btnAddImp->Disable();
			btnChangeImp->Disable();
		}
		else
		{
			cbAllImps->Enable();
			btnAddImp->Enable();
		}

		if (curImpInLayer == nullptr)
		{
			btnRemoveImp->Disable();
			btnChangeImp->Disable();
			cbImpDescriptors->Disable();//there are no materials that can be selected at this point for the layer... or it would have assigned one by default
			cbImpDistType->Disable();
			cbImpUnits->Disable();
			txtImpStartX->Disable();
			txtImpStartY->Disable();
			txtImpStartZ->Disable();
			txtImpHalfX->Disable();
			txtImpHalfY->Disable();
			txtImpHalfZ->Disable();
			txtImpMultiplier->Disable();
		}
		else // there is material selected in the mar mat layer...
		{
			btnRemoveImp->Enable();
			txtImpMultiplier->Enable();
			selection = cbAllImps->GetSelection();
			translatedImpSelection = ConvertImpIndex(selection);
			Impurity* allImpSelection = nullptr;
			if (translatedImpSelection < mdesc->impurities.size())
				allImpSelection = mdesc->impurities[translatedImpSelection];

			if (allImpSelection == getCurImp())
				btnChangeImp->Disable();
			else
				btnChangeImp->Enable();

			cbImpDescriptors->Enable();//there are no materials that can be selected at this point for the layer... or it would have assigned one by default
			cbImpDistType->Enable();

			selection = cbImpDistType->GetSelection();
			if (selection == 0)	//unity. no values need to be added! The code just assumes probability 1 everywhere.
			{
				ImpStart->SetLabel(wxT("Entire"));
				ImpHalf->SetLabel(wxT("Layer"));
				cbImpUnits->Disable();
				txtImpStartX->Disable();
				txtImpStartY->Disable();
				txtImpStartZ->Disable();
				txtImpHalfX->Disable();
				txtImpHalfY->Disable();
				txtImpHalfZ->Disable();
			}
			else if (selection == 1)
			{
				ImpStart->SetLabel(wxT("Start Position:"));
				ImpHalf->SetLabel(wxT("Stop Position:"));
				cbImpUnits->Enable();
				txtImpStartX->Enable();
				txtImpStartY->Enable();
				txtImpStartZ->Enable();
				txtImpHalfX->Enable();
				txtImpHalfY->Enable();
				txtImpHalfZ->Enable();
			}
			else if (selection == 2)
			{
				ImpStart->SetLabel(wxT("Center Position:"));
				ImpHalf->SetLabel(wxT("Half Position:"));
				cbImpUnits->Enable();
				txtImpStartX->Enable();
				txtImpStartY->Enable();
				txtImpStartZ->Enable();
				txtImpHalfX->Enable();
				txtImpHalfY->Enable();
				txtImpHalfZ->Enable();
			}
			else if (selection == 3)
			{
				ImpStart->SetLabel(wxT("Center Position:"));
				ImpHalf->SetLabel(wxT("Half Position:"));
				cbImpUnits->Enable();
				txtImpStartX->Enable();
				txtImpStartY->Enable();
				txtImpStartZ->Enable();
				txtImpHalfX->Enable();
				txtImpHalfY->Enable();
				txtImpHalfZ->Enable();
			}
			else if (selection == 4)
			{
				ImpStart->SetLabel(wxT("Start Position:"));
				ImpHalf->SetLabel(wxT("Zero Position:"));
				cbImpUnits->Enable();
				txtImpStartX->Enable();
				txtImpStartY->Enable();
				txtImpStartZ->Enable();
				txtImpHalfX->Enable();
				txtImpHalfY->Enable();
				txtImpHalfZ->Enable();
			}
			else if (selection == 5)
			{
				ImpStart->SetLabel(wxT("Center Position:"));
				ImpHalf->SetLabel(wxT("Zero Position:"));
				cbImpUnits->Enable();
				txtImpStartX->Enable();
				txtImpStartY->Enable();
				txtImpStartZ->Enable();
				txtImpHalfX->Enable();
				txtImpHalfY->Enable();
				txtImpHalfZ->Enable();
			}
			else
			{
				//disable everything so I catch it later if/when I add things
				cbImpUnits->Disable();
				txtImpStartX->Disable();
				txtImpStartY->Disable();
				txtImpStartZ->Disable();
				txtImpHalfX->Disable();
				txtImpHalfY->Disable();
				txtImpHalfZ->Disable();
			}
		}

		//if it's a 2D/3D dimension... make sure it stays as such
		DisableDimensionalGUIs();
	}
	return;
}

void devLayerTopPanel::DisableDimensionalGUIs()
{
	if (pltWind == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	int numdim = 0;
	if (mdesc)
		numdim = mdesc->NDim;

	switch (numdim)
	{
	default: //if it has no clue what numdim is... disable everything
	case 0:
		txtImpStartX->Disable();
		txtImpHalfX->Disable();
		txtMatStartX->Disable();
		txtMatHalfX->Disable();
		txtLyrStartX->Disable();
		txtLyrDim1X->Disable();
		txtLyrDim2X->Disable();
		txtLyrShapeVertX->Disable();
	case 1:
		txtImpStartY->Disable();
		txtImpHalfY->Disable();
		txtMatStartY->Disable();
		txtMatHalfY->Disable();
		txtLyrStartY->Disable();
		txtLyrDim1Y->Disable();
		txtLyrDim2Y->Disable();
		txtLyrShapeVertY->Disable();
	case 2:
		txtImpStartZ->Disable();
		txtImpHalfZ->Disable();
		txtMatStartZ->Disable();
		txtMatHalfZ->Disable();
		txtLyrStartZ->Disable();
		txtLyrDim1Z->Disable();
		txtLyrDim2Z->Disable();
		txtLyrShapeVertZ->Disable();
	case 3:
		break;
	}
}

void devLayerTopPanel::UpdateLayerListDisplay()
{
	if (pltWind == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	//doing this differently to try and avoid a selection call/events from being generated.
	int numItems = cbLayerChooser->GetCount();
	int numLayers = mdesc->layers.size();

	//remove the excess items.
	if (numItems > numLayers)
	{
		int remove = numItems - numLayers;
		while (remove > 0)
		{
			cbLayerChooser->Delete(0);
			remove--;
		}
	}

	int selection = wxNOT_FOUND;
	for (int i = 0; i < numLayers; ++i)
	{
		if (i < numItems)
			cbLayerChooser->SetString(i, mdesc->layers[i]->name);
		else
			cbLayerChooser->AppendString(mdesc->layers[i]->name);

		if (mdesc->layers[i] == curLayer)
			selection = i;
	}

	if (selection == wxNOT_FOUND)
	{
		if (numLayers > 0)
			selection = 0;
	}

	//it needs to change it!
	if (selection != cbLayerChooser->GetSelection())
		cbLayerChooser->SetSelection(selection);
}

void devLayerTopPanel::TransferToGUI()
{
	if (pltWind == nullptr)
		return;

	VerifyInternalVars();
	ModelDescribe* mdesc = getMDesc();

	if (curLayer == nullptr && mdesc && mdesc->layers.size() > 0)
		curLayer = mdesc->layers[0];

	if (curMatInLayer == nullptr && curLayer && curLayer->matls.size() > 0)
		curMatInLayer = curLayer->matls[0];

	
	if (curImpInLayer == nullptr)
	{
		unsigned int sel = ConvertImpIndex(0);
		if (curLayer && curLayer->impurities.size() > sel)
			curImpInLayer = curLayer->impurities[sel];
	}



	if (curLayer == nullptr)	//absolutely diddly squat to update
	{
		UpdateGUIValues(wxCommandEvent());	//just make sure everything is disabled.
		return;
	}

	

	chkAffectMesh->SetValue(curLayer->AffectMesh);
	chkExclusiveMesh->SetValue(curLayer->exclusive);
	chkHide->SetValue(curLayer->priority);
	chkAllRates->SetValue(curLayer->out.AllRates);
	chkPtEmissions->SetValue(curLayer->out.OpticalPointEmissions);
	chkContact->SetValue((curLayer->contactid > 0));

	ValueToTBox(txtSpacingCenter, curLayer->spacingcenter * 1e7);	//these are stored in the layer as cm, but viewed as nm
	ValueToTBox(txtSpacingEdge, curLayer->spacingboundary * 1e7);
	ValueToTBox(txtScaling, curLayer->sizeScale);

	if (curLayer->shape == nullptr)
	{
		curLayer->shape = new Shape();
		curLayer->shape->type = SHPPOINT;
		curLayer->shape->AddPoint(XSpace());
	}
	
	
	double factor = getUnitFactor(false, cbLyrShapeUnits);
	pltWind->SetXAxisName(cbLyrShapeUnits->GetStringSelection());

	std::vector<XSpace> verts;	//need an efficient way to view all the vertices!
	int numPoints = curLayer->shape->getNumVertices();

	verts.reserve(numPoints);
	wxArrayString vertDispArray;
	for (int i = 0; i < numPoints; ++i)
	{
		verts.push_back(curLayer->shape->getPoint(i) * factor);
		wxString tmp = "V";// +(i + 1);
		tmp << (i + 1);
		vertDispArray.Add(tmp);
	}
	
	int selection = cbLayerShapeVertexList->GetSelection();
	if (cbLayerShapeVertexList->GetCount() != numPoints)
	{
		cbLayerShapeVertexList->Clear();
		if (numPoints == 0)
			cbLayerShapeVertexList->ChangeValue(wxT("Vertices"));
		else
			cbLayerShapeVertexList->Append(vertDispArray);

		if (selection != wxNOT_FOUND && selection < numPoints)
			cbLayerShapeVertexList->Select(selection);
	}
	
	if (selection < numPoints && selection != wxNOT_FOUND)
	{
		ValueToTBox(txtLyrShapeVertX, verts[selection].x);
		ValueToTBox(txtLyrShapeVertY, verts[selection].y);
		ValueToTBox(txtLyrShapeVertZ, verts[selection].z);
	}
	else
	{
		txtLyrShapeVertX->Clear();
		txtLyrShapeVertY->Clear();
		txtLyrShapeVertZ->Clear();
	}

	XSpace tmpDim;

	switch (curLayer->shape->type)
	{
	case SHPPOINT:			//1		the layer consists of a point... not a big layer...
		if (numPoints > 0)
		{
			ValueToTBox(txtLyrStartX, verts[0].x);
			ValueToTBox(txtLyrStartY, verts[0].y);
			ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		cbLayerShape->SetSelection(0);
		break;
	case SHPLINE:			//2		the layer is situated as a line
		if (numPoints > 0)
		{
			ValueToTBox(txtLyrStartX, verts[0].x);
			ValueToTBox(txtLyrStartY, verts[0].y);
			ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		if (numPoints > 1)
		{
			tmpDim = verts[1] - verts[0];
			ValueToTBox(txtLyrDim1X, tmpDim.x);
			ValueToTBox(txtLyrDim1Y, tmpDim.y);
			ValueToTBox(txtLyrDim1Z, tmpDim.z);
		}
		else
		{
			ValueToTBox(txtLyrDim1X, 0.0);
			ValueToTBox(txtLyrDim1Y, 0.0);
			ValueToTBox(txtLyrDim1Z, 0.0);
		}
		cbLayerShape->SetSelection(1);
		break;
	case TRIANGLE:			//3		the layer is in the shape of a triangle
	case QUAD:				//4	
	case RECTANGLE:			//5		the layer is in the shape of a rectangle
	case PENTAGON:			//6		the layer is in the shape of a pentagon
	case HEXAGON:			//7		the " " " " "  hexagon
	case SQUARE:			//8		 " square
	case RIGID2D:			//9
	case ELLIPSE:			//10	" " " " "  ellipse
	case CIRCLE:			//11	" circle
	case SHAPE2DMAX:		//12	just a reference to the highest shape defined
	case SHAPEMAX:			//13
	default:
		ValueToTBox(txtLyrStartX, 0.0);
		ValueToTBox(txtLyrStartY, 0.0);
		ValueToTBox(txtLyrStartZ, 0.0);
		ValueToTBox(txtLyrDim1X, 0.0);
		ValueToTBox(txtLyrDim1Y, 0.0);
		ValueToTBox(txtLyrDim1Z, 0.0);
		ValueToTBox(txtLyrDim2X, 0.0);
		ValueToTBox(txtLyrDim2Y, 0.0);
		ValueToTBox(txtLyrDim2Z, 0.0);
		break;
	}

	verts.clear();

	////////////////////////////////////////////done transferring data to the layer segment.

	////////////////////transfering data to the materials section

	////first up the layer list
	wxArrayString materialList;
	selection = cbAllMats->GetSelection();	//maintain the selection after clearing it!
	for (unsigned int i = 0; i < mdesc->materials.size(); ++i)
		materialList.Add(mdesc->materials[i]->getName());
		
	cbAllMats->Clear();
	cbAllMats->Append(materialList);
	if ((unsigned int)selection < mdesc->materials.size())
		cbAllMats->SetSelection(selection);
	else if (materialList.size() > 0)
		cbAllMats->SetSelection(0);

	//now do stuff particular to the selected data!
	materialList.clear();
	//we know curLayer exists...
	selection = -1;
	for (unsigned int i = 0; i < curLayer->matls.size(); ++i)
	{
		if (curLayer->matls[i]->data)
			materialList.Add(curLayer->matls[i]->data->getName());
		else
			materialList.Add("Requires material assignment!");

		if (curLayer->matls[i] == curMatInLayer)
			selection = i;

	}
	cbMatDescriptors->Clear();
	cbMatDescriptors->Append(materialList);
	if (selection >= 0)
		cbMatDescriptors->SetSelection(selection);

	if (curMatInLayer)
	{
		switch (curMatInLayer->flag)
		{
		case BANDED:
			cbMatDistType->SetSelection(1);
			break;
		case GAUSSIAN:
			cbMatDistType->SetSelection(2);
			break;
		case EXPONENTIAL:
			cbMatDistType->SetSelection(3);
			break;
		case LINEAR:
			cbMatDistType->SetSelection(4);
			break;
		case PARABOLIC:
			cbMatDistType->SetSelection(5);
			break;
		case DISCRETE:
		default:
		case GAUSSIAN2:
		case GAUSSIAN3:
		case EXPONENTIAL2:
		case EXPONENTIAL3:
		case PARABOLIC2:
		case PARABOLIC3:
			cbMatDistType->SetSelection(0);
			break;
		}
		//don't change the units!
		factor = getUnitFactor(false, cbMatUnits);

		XSpace tmpXSP = curMatInLayer->range.startpos * factor;

		ValueToTBox(txtMatStartX, tmpXSP.x);
		ValueToTBox(txtMatStartY, tmpXSP.y);
		ValueToTBox(txtMatStartZ, tmpXSP.z);

		tmpXSP = curMatInLayer->range.stoppos * factor;
		ValueToTBox(txtMatHalfX, tmpXSP.x);
		ValueToTBox(txtMatHalfY, tmpXSP.y);
		ValueToTBox(txtMatHalfZ, tmpXSP.z);
	}
	else
	{
		txtMatStartX->Clear();
		txtMatStartY->Clear();
		txtMatStartZ->Clear();

		txtMatHalfX->Clear();
		txtMatHalfY->Clear();
		txtMatHalfZ->Clear();
	}
	////////////////////////////////////////material data all done!

	//////////////////now transfer all the data from the impurities!

	wxArrayString impurityList;
	selection = cbAllImps->GetSelection();	//maintain the selection after clearing it!
//	selection = ConvertImpIndex(selection);
	for (unsigned int i = 0; i < mdesc->impurities.size(); ++i)
	{
		if (mdesc->impurities[i]->isDefect() == false)
			impurityList.Add(mdesc->impurities[i]->getName());
	}

	cbAllImps->Clear();
	cbAllImps->Append(impurityList);
	if ((unsigned int)selection < impurityList.size())
		cbAllImps->SetSelection(selection);
	else if (impurityList.size() > 0)
		cbAllImps->SetSelection(0);

	//now do stuff particular to the selected data!
	impurityList.clear();
	//we know curLayer exists...
	selection = -1;
	for (unsigned int i = 0; i < curLayer->impurities.size(); ++i)
	{
		if (curLayer->impurities[i]->data)
			impurityList.Add(curLayer->impurities[i]->data->getName());
		else
			impurityList.Add("Requires impurity assignment!");

		if (curLayer->impurities[i] == curImpInLayer)
			selection = i;

	}
	cbImpDescriptors->Clear();
	cbImpDescriptors->Append(impurityList);
	if (selection >= 0)
		cbImpDescriptors->SetSelection(selection);

	if (curImpInLayer)
	{
		switch (curImpInLayer->flag)
		{
		case BANDED:
			cbImpDistType->SetSelection(1);
			break;
		case GAUSSIAN:
			cbImpDistType->SetSelection(2);
			break;
		case EXPONENTIAL:
			cbImpDistType->SetSelection(3);
			break;
		case LINEAR:
			cbImpDistType->SetSelection(4);
			break;
		case PARABOLIC:
			cbImpDistType->SetSelection(5);
			break;
		case DISCRETE:
		default:
		case GAUSSIAN2:
		case GAUSSIAN3:
		case EXPONENTIAL2:
		case EXPONENTIAL3:
		case PARABOLIC2:
		case PARABOLIC3:
			cbImpDistType->SetSelection(0);
			break;
		}
		//don't change the units!
		factor = getUnitFactor(false, cbImpUnits);
		XSpace tmpXSP = curImpInLayer->range.startpos * factor;

		ValueToTBox(txtImpStartX, tmpXSP.x);
		ValueToTBox(txtImpStartY, tmpXSP.y);
		ValueToTBox(txtImpStartZ, tmpXSP.z);

		tmpXSP = curImpInLayer->range.stoppos * factor;
		ValueToTBox(txtImpHalfX, tmpXSP.x);
		ValueToTBox(txtImpHalfY, tmpXSP.y);
		ValueToTBox(txtImpHalfZ, tmpXSP.z);

		ValueToTBox(txtImpMultiplier, curImpInLayer->multiplier);
		if (curImpInLayer->data)
		{
			factor = curImpInLayer->data->getDensity() * curImpInLayer->multiplier;
			wxString tmpstr;
			tmpstr << factor << " cm" << UNC_SUPER_MIN << UNC_SUPER_3;
			ImpDensity->SetLabel(tmpstr);
		}
	}
	else
	{
		txtImpStartX->Clear();
		txtImpStartY->Clear();
		txtImpStartZ->Clear();

		txtImpHalfX->Clear();
		txtImpHalfY->Clear();
		txtImpHalfZ->Clear();
		wxString tmpstr;
		tmpstr << "# cm" << UNC_SUPER_MIN << UNC_SUPER_3;
		ImpDensity->SetLabel(tmpstr);
	}

	UpdatePlot();

	UpdateGUIValues(wxCommandEvent());
}

void devLayerTopPanel::UpdateShapeTextDisplays(wxObject* skip)
{
	if (curLayer == nullptr)
		return;
	if (curLayer->shape == nullptr)
	{
		txtLyrStartX->Clear();
		txtLyrStartY->Clear();
		txtLyrStartZ->Clear();

		txtLyrDim1X->Clear();
		txtLyrDim1Y->Clear();
		txtLyrDim1Z->Clear();

		txtLyrDim2X->Clear();
		txtLyrDim2Y->Clear();
		txtLyrDim2Z->Clear();

		txtLyrShapeVertX->Clear();
		txtLyrShapeVertY->Clear();
		txtLyrShapeVertZ->Clear();
		return;
	}
	double factor = getUnitFactor(false, cbLyrShapeUnits);


	std::vector<XSpace> verts;	//need an efficient way to view all the vertices!
	int numPoints = curLayer->shape->getNumVertices();
	verts.reserve(numPoints);

	for (int i = 0; i < numPoints; ++i)
		verts.push_back(curLayer->shape->getPoint(i) * factor);

	int selection = cbLayerShapeVertexList->GetSelection();
	if (selection < numPoints && selection != wxNOT_FOUND)
	{
		if (skip != txtLyrShapeVertX)
			ValueToTBox(txtLyrShapeVertX, verts[selection].x);
		if (skip != txtLyrShapeVertY)
			ValueToTBox(txtLyrShapeVertY, verts[selection].y);
		if (skip != txtLyrShapeVertZ)
			ValueToTBox(txtLyrShapeVertZ, verts[selection].z);
	}
	else
	{
		txtLyrShapeVertX->Clear();
		txtLyrShapeVertY->Clear();
		txtLyrShapeVertZ->Clear();
	}

	XSpace tmpDim;

	switch (curLayer->shape->type)
	{
	case SHPPOINT:			//1		the layer consists of a point... not a big layer...
		if (numPoints > 0)
		{
			if (skip != txtLyrStartX)
				ValueToTBox(txtLyrStartX, verts[0].x);
			if (skip != txtLyrStartY)
				ValueToTBox(txtLyrStartY, verts[0].y);
			if (skip != txtLyrStartZ)
				ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		cbLayerShape->SetSelection(0);
		break;
	case SHPLINE:			//2		the layer is situated as a line
		if (numPoints > 0)
		{
			if (skip != txtLyrStartX)
				ValueToTBox(txtLyrStartX, verts[0].x);
			if (skip != txtLyrStartY)
				ValueToTBox(txtLyrStartY, verts[0].y);
			if (skip != txtLyrStartZ)
				ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		if (numPoints > 1)
		{
			tmpDim = verts[1] - verts[0];
			if (skip != txtLyrDim1X)
				ValueToTBox(txtLyrDim1X, tmpDim.x);
			if (skip != txtLyrDim1Y)
				ValueToTBox(txtLyrDim1Y, tmpDim.y);
			if (skip != txtLyrDim1Z)
				ValueToTBox(txtLyrDim1Z, tmpDim.z);
		}
		else
		{
			ValueToTBox(txtLyrDim1X, 0.0);
			ValueToTBox(txtLyrDim1Y, 0.0);
			ValueToTBox(txtLyrDim1Z, 0.0);
		}
		cbLayerShape->SetSelection(1);
		break;
	case TRIANGLE:			//3		the layer is in the shape of a triangle
	case QUAD:				//4	
	case RECTANGLE:			//5		the layer is in the shape of a rectangle
	case PENTAGON:			//6		the layer is in the shape of a pentagon
	case HEXAGON:			//7		the " " " " "  hexagon
	case SQUARE:			//8		 " square
	case RIGID2D:			//9
	case ELLIPSE:			//10	" " " " "  ellipse
	case CIRCLE:			//11	" circle
	case SHAPE2DMAX:		//12	just a reference to the highest shape defined
	case SHAPEMAX:			//13
	default:
		ValueToTBox(txtLyrStartX, 0.0);
		ValueToTBox(txtLyrStartY, 0.0);
		ValueToTBox(txtLyrStartZ, 0.0);
		ValueToTBox(txtLyrDim1X, 0.0);
		ValueToTBox(txtLyrDim1Y, 0.0);
		ValueToTBox(txtLyrDim1Z, 0.0);
		ValueToTBox(txtLyrDim2X, 0.0);
		ValueToTBox(txtLyrDim2Y, 0.0);
		ValueToTBox(txtLyrDim2Z, 0.0);
		break;
	}

	UpdatePlot();
}

void devLayerTopPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (pltWind == nullptr)
		return;

	if (curLayer == nullptr)
		return;

	ModelDescribe *mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	if (chkContact->GetValue() == true)
		curLayer->contactid = curLayer->id;	//will always be unique to the layer!
	else
		curLayer->contactid = 0;

	curLayer->out.AllRates = chkAllRates->GetValue();
	curLayer->out.OpticalPointEmissions = chkPtEmissions->GetValue();
	curLayer->AffectMesh = chkAffectMesh->GetValue();
	curLayer->priority = chkHide->GetValue();
	curLayer->exclusive = chkExclusiveMesh->GetValue();

	if (txtScaling->GetValue().ToDouble(&curLayer->sizeScale) == true)
	{
		if (curLayer->sizeScale <= 0.0 || curLayer->sizeScale > 1.0)
			curLayer->sizeScale = 1.0;
	}
	else
		curLayer->sizeScale = 1.0;

	if (txtSpacingCenter->GetValue().ToDouble(&curLayer->spacingcenter) == true)
	{
		curLayer->spacingcenter *= 1e-7;
		if (curLayer->spacingcenter <= 0.0)
			curLayer->spacingcenter = 1e-5;
	}
	else
		curLayer->spacingcenter = 1e-5;

	if (txtSpacingEdge->GetValue().ToDouble(&curLayer->spacingboundary) == true)
	{
		curLayer->spacingboundary *= 1e-7;
		if (curLayer->spacingboundary <= 0.0)
			curLayer->spacingboundary = 1e-6;
	}
	else
		curLayer->spacingboundary = 1e-6;

	XSpace start, dim, dim2;
	double factor = getUnitFactor(true, cbLyrShapeUnits);
	

	if (txtLyrStartX->GetValue().ToDouble(&start.x) == false)
		start.x = 0.0;
	if (txtLyrStartY->GetValue().ToDouble(&start.y) == false)
		start.y = 0.0;
	if (txtLyrStartZ->GetValue().ToDouble(&start.z) == false)
		start.z = 0.0;
	start = start * factor;

	if (txtLyrDim1X->GetValue().ToDouble(&dim.x) == false)
		dim.x = 0.0;
	if (txtLyrDim1Y->GetValue().ToDouble(&dim.y) == false)
		dim.y = 0.0;
	if (txtLyrDim1Z->GetValue().ToDouble(&dim.z) == false)
		dim.z = 0.0;
	dim = dim * factor;

	if (txtLyrDim2X->GetValue().ToDouble(&dim2.x) == false)
		dim2.x = 0.0;
	if (txtLyrDim2Y->GetValue().ToDouble(&dim2.y) == false)
		dim2.y = 0.0;
	if (txtLyrDim2Z->GetValue().ToDouble(&dim2.z) == false)
		dim2.z = 0.0;
	dim2 = dim2 * factor;

	int selection = cbLayerShape->GetSelection();
	if (selection == 0)	//point
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			curLayer->shape->type = SHPPOINT;
			curLayer->shape->AddPoint(XSpace());
		}

		curLayer->shape->modifyVertex(0, start);
	}
	else if (selection == 1)	//line
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			curLayer->shape->type = SHPLINE;
			curLayer->shape->AddPoint(XSpace());
			curLayer->shape->AddPoint(XSpace());
		}
		curLayer->shape->modifyVertex(0, start);
		curLayer->shape->modifyVertex(1, start + dim);
	}

	///////////don't bother with deviation at this point...


	if (curMatInLayer)
	{
		selection = cbMatDistType->GetSelection();
		switch (selection)
		{
		case 1:
			curMatInLayer->flag = BANDED;
			break;
		case 2:
			curMatInLayer->flag = GAUSSIAN;
			break;
		case 3:
			curMatInLayer->flag = EXPONENTIAL;
			break;
		case 4:
			curMatInLayer->flag = LINEAR;
			break;
		case 5:
			curMatInLayer->flag = PARABOLIC;
			break;
		case 0:
		default:
			curMatInLayer->flag = DISCRETE;
			break;
		}

		factor = getUnitFactor(true, cbMatUnits);
		
		if (txtMatStartX->GetValue().ToDouble(&start.x) == false)
			start.x = 0.0;
		if (txtMatStartY->GetValue().ToDouble(&start.y) == false)
			start.y = 0.0;
		if (txtMatStartZ->GetValue().ToDouble(&start.z) == false)
			start.z = 0.0;
		start = start * factor;

		if (txtMatHalfX->GetValue().ToDouble(&dim.x) == false)
			dim.x = 0.0;
		if (txtMatHalfY->GetValue().ToDouble(&dim.y) == false)
			dim.y = 0.0;
		if (txtMatHalfZ->GetValue().ToDouble(&dim.z) == false)
			dim.z = 0.0;
		dim = dim * factor;

		curMatInLayer->range.startpos = start;
		curMatInLayer->range.stoppos = dim;
		curMatInLayer->multiplier = 1.0;	//just be safe. This really shouldn't ever be used in this case, at least currently anyway.
	}

	if (curImpInLayer)
	{
		selection = cbImpDistType->GetSelection();
		switch (selection)
		{
		case 1:
			curImpInLayer->flag = BANDED;
			break;
		case 2:
			curImpInLayer->flag = GAUSSIAN;
			break;
		case 3:
			curImpInLayer->flag = EXPONENTIAL;
			break;
		case 4:
			curImpInLayer->flag = LINEAR;
			break;
		case 5:
			curImpInLayer->flag = PARABOLIC;
			break;
		case 0:
		default:
			curImpInLayer->flag = DISCRETE;
			break;
		}

		factor = getUnitFactor(true, cbImpUnits);

		if (txtImpStartX->GetValue().ToDouble(&start.x) == false)
			start.x = 0.0;
		if (txtImpStartY->GetValue().ToDouble(&start.y) == false)
			start.y = 0.0;
		if (txtImpStartZ->GetValue().ToDouble(&start.z) == false)
			start.z = 0.0;
		start = start * factor;

		if (txtImpHalfX->GetValue().ToDouble(&dim.x) == false)
			dim.x = 0.0;
		if (txtImpHalfY->GetValue().ToDouble(&dim.y) == false)
			dim.y = 0.0;
		if (txtImpHalfZ->GetValue().ToDouble(&dim.z) == false)
			dim.z = 0.0;
		dim = dim * factor;

		curImpInLayer->range.startpos = start;
		curImpInLayer->range.stoppos = dim;

		if (txtImpMultiplier->GetValue().ToDouble(&curImpInLayer->multiplier) == true)
		{
			if (curImpInLayer->multiplier <= 0.0)	//invalid number!
			{
				curImpInLayer->multiplier = 1.0;
				txtImpMultiplier->ChangeValue(wxT("1.0"));
			}
		}
		else
			curImpInLayer->multiplier = 1.0;
	}

	mdesc->UpdateDimensions();

}

void devLayerTopPanel::UpdateScales(wxCommandEvent& event)
{
	if (pltWind == nullptr)
		return;
	if (curLayer == nullptr)
		return;

	double factor = getUnitFactor(false, cbLyrShapeUnits);
	pltWind->SetXAxisName(cbLyrShapeUnits->GetStringSelection());

	if (curLayer->shape == nullptr)
	{
		curLayer->shape = new Shape();
		curLayer->shape->type = SHPPOINT;
		curLayer->shape->AddPoint(XSpace());
	}

	std::vector<XSpace> verts;	//need an efficient way to view all the vertices!
	int numPoints = curLayer->shape->getNumVertices();
	verts.reserve(numPoints);
	for (int i = 0; i < numPoints; ++i)
		verts.push_back(curLayer->shape->getPoint(i) * factor);


	XSpace tmpDim;

	switch (curLayer->shape->type)
	{
	case SHPPOINT:			//1		the layer consists of a point... not a big layer...
		if (numPoints > 0)
		{
			ValueToTBox(txtLyrStartX, verts[0].x);
			ValueToTBox(txtLyrStartY, verts[0].y);
			ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		break;
	case SHPLINE:			//2		the layer is situated as a line
		if (numPoints > 0)
		{
			ValueToTBox(txtLyrStartX, verts[0].x);
			ValueToTBox(txtLyrStartY, verts[0].y);
			ValueToTBox(txtLyrStartZ, verts[0].z);
		}
		else
		{
			ValueToTBox(txtLyrStartX, 0.0);
			ValueToTBox(txtLyrStartY, 0.0);
			ValueToTBox(txtLyrStartZ, 0.0);
		}
		if (numPoints > 1)
		{
			tmpDim = verts[1] - verts[0];
			ValueToTBox(txtLyrDim1X, tmpDim.x);
			ValueToTBox(txtLyrDim1Y, tmpDim.y);
			ValueToTBox(txtLyrDim1Z, tmpDim.z);
		}
		else
		{
			ValueToTBox(txtLyrDim1X, 0.0);
			ValueToTBox(txtLyrDim1Y, 0.0);
			ValueToTBox(txtLyrDim1Z, 0.0);
		}
		break;
	case TRIANGLE:			//3		the layer is in the shape of a triangle
	case QUAD:				//4	
	case RECTANGLE:			//5		the layer is in the shape of a rectangle
	case PENTAGON:			//6		the layer is in the shape of a pentagon
	case HEXAGON:			//7		the " " " " "  hexagon
	case SQUARE:			//8		 " square
	case RIGID2D:			//9
	case ELLIPSE:			//10	" " " " "  ellipse
	case CIRCLE:			//11	" circle
	case SHAPE2DMAX:		//12	just a reference to the highest shape defined
	case SHAPEMAX:			//13
	default:
		ValueToTBox(txtLyrStartX, 0.0);
		ValueToTBox(txtLyrStartY, 0.0);
		ValueToTBox(txtLyrStartZ, 0.0);
		ValueToTBox(txtLyrDim1X, 0.0);
		ValueToTBox(txtLyrDim1Y, 0.0);
		ValueToTBox(txtLyrDim1Z, 0.0);
		ValueToTBox(txtLyrDim2X, 0.0);
		ValueToTBox(txtLyrDim2Y, 0.0);
		ValueToTBox(txtLyrDim2Z, 0.0);
		break;
	}

	int selection = cbLayerShapeVertexList->GetSelection();
	if (selection < numPoints)
	{
		ValueToTBox(txtLyrShapeVertX, verts[selection].x);
		ValueToTBox(txtLyrShapeVertY, verts[selection].y);
		ValueToTBox(txtLyrShapeVertZ, verts[selection].z);
	}
	else
	{
		txtLyrShapeVertX->Clear();
		txtLyrShapeVertY->Clear();
		txtLyrShapeVertZ->Clear();
	}

	verts.clear();


	////////now do it all over again for the other guys!

	if (curMatInLayer)
	{
		factor = getUnitFactor(false, cbMatUnits);
		XSpace tmpXSP = curMatInLayer->range.startpos * factor;

		ValueToTBox(txtMatStartX, tmpXSP.x);
		ValueToTBox(txtMatStartY, tmpXSP.y);
		ValueToTBox(txtMatStartZ, tmpXSP.z);

		tmpXSP = curMatInLayer->range.stoppos * factor;
		ValueToTBox(txtMatHalfX, tmpXSP.x);
		ValueToTBox(txtMatHalfY, tmpXSP.y);
		ValueToTBox(txtMatHalfZ, tmpXSP.z);
	}
	else
	{
		txtMatStartX->Clear();
		txtMatStartY->Clear();
		txtMatStartZ->Clear();

		txtMatHalfX->Clear();
		txtMatHalfY->Clear();
		txtMatHalfZ->Clear();
	}

	if (curImpInLayer)
	{
		factor = getUnitFactor(false, cbImpUnits);
		XSpace tmpXSP = curImpInLayer->range.startpos * factor;

		ValueToTBox(txtImpStartX, tmpXSP.x);
		ValueToTBox(txtImpStartY, tmpXSP.y);
		ValueToTBox(txtImpStartZ, tmpXSP.z);

		tmpXSP = curImpInLayer->range.stoppos * factor;
		ValueToTBox(txtImpHalfX, tmpXSP.x);
		ValueToTBox(txtImpHalfY, tmpXSP.y);
		ValueToTBox(txtImpHalfZ, tmpXSP.z);
	}
	else
	{
		txtImpStartX->Clear();
		txtImpStartY->Clear();
		txtImpStartZ->Clear();

		txtImpHalfX->Clear();
		txtImpHalfY->Clear();
		txtImpHalfZ->Clear();
	}
	UpdatePlot();
}


void devLayerTopPanel::CboxClick(wxCommandEvent& event)
{
	if (pltWind == nullptr)
		return;
	if (curLayer == nullptr)
		return;

	curLayer->out.AllRates = chkAllRates->GetValue();
	curLayer->out.OpticalPointEmissions = chkPtEmissions->GetValue();
	curLayer->AffectMesh = chkAffectMesh->GetValue();
	curLayer->priority = false;// chkHide->GetValue();
	curLayer->exclusive = chkExclusiveMesh->GetValue();

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	if (event.GetEventObject() == chkHide) {
		if (curLayer) {
			unsigned int which = FindValInVector(mdesc->layers, curLayer);
			if (which < mdesc->layers.size()) {
				if (which >= hidePlot.size()) {
					hidePlot.resize(which + 1, false);
				}
				hidePlot[which] = chkHide->GetValue();
			}
		}
		UpdatePlot();
	}

	if (chkContact->GetValue() == true)
	{
		if (curLayer->contactid == 0)
		{
			curLayer->contactid = curLayer->id;	//will always be unique to the layer!

			std::vector<Contact*>& ctcs = mdesc->simulation->contacts;
			bool found = false;
			for (unsigned int i = 0; i < ctcs.size(); ++i)
			{
				if (ctcs[i]->id == curLayer->contactid)
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				Contact* ct = new Contact;
				ct->id = curLayer->id;
				ct->name = curLayer->name + " contact";
				ctcs.push_back(ct);
			}
		}
	}
	else
	{
		if (curLayer->contactid != 0)
		{
			//remove it from the simulation contacts!
			std::vector<Contact*>& ctcs = mdesc->simulation->contacts;
			for (unsigned int i = 0; i < ctcs.size(); ++i)
			{
				if (ctcs[i]->id == curLayer->contactid)
				{
					delete ctcs[i];
					ctcs.erase(ctcs.begin() + i);
				}
			}
		}
		curLayer->contactid = 0;
	}

	



	UpdateGUIValues(event);	//clicking affect mesh might change some things!
}


void devLayerTopPanel::TBoxUpdate(wxCommandEvent& event)
{
	if (pltWind == nullptr)
		return;
	if (curLayer == nullptr)
		return;

	wxObject* changed = event.GetEventObject();

	/////////////the left side ---- not the shape shit
	if (changed == txtScaling)
	{
		if (txtScaling->GetValue().ToDouble(&curLayer->sizeScale) == true)
		{
			if (curLayer->sizeScale <= 0.0 || curLayer->sizeScale > 1.0)
				curLayer->sizeScale = 1.0;
		}
		else
			curLayer->sizeScale = 1.0;
	}

	if (changed == txtSpacingCenter)
	{
		if (txtSpacingCenter->GetValue().ToDouble(&curLayer->spacingcenter) == true)
		{
			curLayer->spacingcenter *= 1e-7;
			if (curLayer->spacingcenter <= 0.0)
				curLayer->spacingcenter = 1e-5;
		}
		else
			curLayer->spacingcenter = 1e-5;
	}

	if (changed == txtSpacingEdge)
	{
		if (txtSpacingEdge->GetValue().ToDouble(&curLayer->spacingboundary) == true)
		{
			curLayer->spacingboundary *= 1e-7;
			if (curLayer->spacingboundary <= 0.0)
				curLayer->spacingboundary = 1e-6;
		}
		else
			curLayer->spacingboundary = 1e-6;
	}
	/////////////////////////////////////

	if (curImpInLayer)
	{
		double factor;
		if (changed == txtImpMultiplier)
		{
			if (txtImpMultiplier->GetValue().ToDouble(&curImpInLayer->multiplier) == true)
			{
				if (curImpInLayer->multiplier < 0.0)	//invalid number!
				{
					curImpInLayer->multiplier = 1.0;
					txtImpMultiplier->ChangeValue(wxT("1.0"));
				}
			}
			else
				curImpInLayer->multiplier = 1.0;


			if (curImpInLayer->data)
			{
				factor = curImpInLayer->data->getDensity() * curImpInLayer->multiplier;
				wxString tmpstr;
				tmpstr << factor << " cm" << UNC_SUPER_MIN << UNC_SUPER_3;
				ImpDensity->SetLabel(tmpstr);
			}
		}
		factor = getUnitFactor(true, cbImpUnits);

		XSpace start, dim;

		if (changed == txtImpStartX || changed == txtImpStartY || changed == txtImpStartZ)
		{
			if (txtImpStartX->GetValue().ToDouble(&start.x) == false)
				start.x = 0.0;
			if (txtImpStartY->GetValue().ToDouble(&start.y) == false)
				start.y = 0.0;
			if (txtImpStartZ->GetValue().ToDouble(&start.z) == false)
				start.z = 0.0;
			start = start * factor;
			curImpInLayer->range.startpos = start;
		}
		if (changed == txtImpHalfX || changed == txtImpHalfY || changed == txtImpHalfZ)
		{
			if (txtImpHalfX->GetValue().ToDouble(&dim.x) == false)
				dim.x = 0.0;
			if (txtImpHalfY->GetValue().ToDouble(&dim.y) == false)
				dim.y = 0.0;
			if (txtImpHalfZ->GetValue().ToDouble(&dim.z) == false)
				dim.z = 0.0;
			dim = dim * factor;
			curImpInLayer->range.stoppos = dim;
		}
	}

	//////////////////

	if (curMatInLayer)
	{
		
		double factor = getUnitFactor(true, cbMatUnits);
		XSpace start, dim;
		if (changed == txtMatStartX || changed == txtMatStartY || changed == txtMatStartZ)
		{
			if (txtMatStartX->GetValue().ToDouble(&start.x) == false)
				start.x = 0.0;
			if (txtMatStartY->GetValue().ToDouble(&start.y) == false)
				start.y = 0.0;
			if (txtMatStartZ->GetValue().ToDouble(&start.z) == false)
				start.z = 0.0;
			start = start * factor;
			curMatInLayer->range.startpos = start;
		}
		if (changed == txtMatHalfX || changed == txtMatHalfY || changed == txtMatHalfZ)
		{
			if (txtMatHalfX->GetValue().ToDouble(&dim.x) == false)
				dim.x = 0.0;
			if (txtMatHalfY->GetValue().ToDouble(&dim.y) == false)
				dim.y = 0.0;
			if (txtMatHalfZ->GetValue().ToDouble(&dim.z) == false)
				dim.z = 0.0;
			dim = dim * factor;
			curMatInLayer->range.stoppos = dim;
		}
		
		
	}
}

void devLayerTopPanel::ChangeVertexView(wxCommandEvent& event)
{
	if (pltWind == nullptr)
		return;
	if (curLayer == nullptr || curLayer->shape == nullptr)
		return;

	double factor = getUnitFactor(false, cbLyrShapeUnits);
	int selection = cbLayerShapeVertexList->GetSelection();
	if (selection < curLayer->shape->getNumVertices())
	{
		XSpace vertexEntry = curLayer->shape->getPoint(selection) * factor;
		ValueToTBox(txtLyrShapeVertX, vertexEntry.x);
		ValueToTBox(txtLyrShapeVertY, vertexEntry.y);
		ValueToTBox(txtLyrShapeVertZ, vertexEntry.z);
	}

}
double devLayerTopPanel::getUnitFactor(bool toMdesc, wxComboBox* which)
{
	if (pltWind == nullptr)
		return 1.0;
	if (curLayer == nullptr || which == nullptr)
		return(1.0);	//default to no change!

	double factor;
	int selection = which->GetSelection();
	if (toMdesc)
	{
		switch (selection)
		{
		default:
		case 1:	//cm
			factor = 1.0;
			break;
		case 0:	//meters
			factor = 100.0;
			break;
		case 2:	//mm
			factor = 0.1;
			break;
		case 3:	//um
			factor = 0.0001;
			break;
		case 4:	//nm
			factor = 1.0e-7;
			break;
		}
	}
	else //sending to gui
	{
		switch (selection)
		{
		default:
		case 1:	//cm
			factor = 1.0;
			break;
		case 0:	//meters
			factor = 0.01;
			break;
		case 2:	//mm
			factor = 10.0;
			break;
		case 3:	//um
			factor = 1e4;
			break;
		case 4:	//nm
			factor = 1.0e7;
			break;
		}
	}
	return(factor);
}

void devLayerTopPanel::ShapeTextUpdate(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;

	XSpace start, dim, dim2;
	double factor = getUnitFactor(true, cbLyrShapeUnits);
		
	if (txtLyrStartX == nullptr || txtLyrStartY == nullptr || txtLyrStartZ == nullptr ||
		txtLyrDim1X == nullptr || txtLyrDim1Y == nullptr || txtLyrDim1Z == nullptr ||
		txtLyrDim2X == nullptr || txtLyrDim2Y == nullptr || txtLyrDim2Z == nullptr ||
		cbLayerShape == nullptr)
		return;	//it's initializing. It appears that on the development computer,
	//this doesn't crash when initializing. On end user, this gets called with each
	//part being initialized. so when doing the second one, this apparently gets called
	//and not all the text boxes have been initialized yet. Very pesky.

	if (txtLyrStartX->GetValue().ToDouble(&start.x) == false)
		start.x = 0.0;
	if (txtLyrStartY->GetValue().ToDouble(&start.y) == false)
		start.y = 0.0;
	if (txtLyrStartZ->GetValue().ToDouble(&start.z) == false)
		start.z = 0.0;
	start = start * factor;

	if (txtLyrDim1X->GetValue().ToDouble(&dim.x) == false)
		dim.x = 0.0;
	if (txtLyrDim1Y->GetValue().ToDouble(&dim.y) == false)
		dim.y = 0.0;
	if (txtLyrDim1Z->GetValue().ToDouble(&dim.z) == false)
		dim.z = 0.0;
	dim = dim * factor;

	if (txtLyrDim2X->GetValue().ToDouble(&dim2.x) == false)
		dim2.x = 0.0;
	if (txtLyrDim2Y->GetValue().ToDouble(&dim2.y) == false)
		dim2.y = 0.0;
	if (txtLyrDim2Z->GetValue().ToDouble(&dim2.z) == false)
		dim2.z = 0.0;
	dim2 = dim2 * factor;

	int selection = cbLayerShape->GetSelection();
	if (selection == 0)	//point
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			curLayer->shape->type = SHPPOINT;
			curLayer->shape->AddPoint(XSpace());
		}

		curLayer->shape->modifyVertex(0, start);
	}
	else if (selection == 1)	//line
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			curLayer->shape->type = SHPLINE;
			curLayer->shape->AddPoint(XSpace());
			curLayer->shape->AddPoint(XSpace());
		}
		curLayer->shape->modifyVertex(0, start);
		curLayer->shape->modifyVertex(1, start + dim);
	}

	UpdateShapeTextDisplays(event.GetEventObject());
	ModelDescribe* mdesc = getMDesc();
	if (mdesc)
		mdesc->UpdateDimensions();
}

void devLayerTopPanel::AlterShapeVertex(wxCommandEvent& event)
{
	if (curLayer == nullptr || txtLyrShapeVertX == nullptr || txtLyrShapeVertY == nullptr || txtLyrShapeVertZ == nullptr)
		return;
	if (pltWind == nullptr)
		return;

	if (curLayer->shape == nullptr)
		return;

	int selection = cbLayerShapeVertexList->GetSelection();
	if (selection == wxNOT_FOUND)
		return;

	int numVerts = curLayer->shape->getNumVertices();
	if (selection >= numVerts)
		return;

	
	XSpace value(0.0);
	double factor = getUnitFactor(true, cbLyrShapeUnits);

	txtLyrShapeVertX->GetValue().ToDouble(&value.x);
	txtLyrShapeVertY->GetValue().ToDouble(&value.y);
	txtLyrShapeVertZ->GetValue().ToDouble(&value.z);
	value = value * factor;
	curLayer->shape->modifyVertex(selection, value);
	
	UpdateShapeTextDisplays(event.GetEventObject());
	ModelDescribe* mdesc = getMDesc();
	if (mdesc)
		mdesc->UpdateDimensions();
//	TransferToGUI();	//a lot of the other numbers may have changed by doing this, and I frankly don't feel like coding it all again...
}

void devLayerTopPanel::ShapeUpdate(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;

	if (curLayer->shape == nullptr)
		curLayer->shape = new Shape();

	int selection = cbLayerShape->GetSelection();
	int desiredPoints = 0;
	if (selection == 0)	//point
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			
		}
		curLayer->shape->type = SHPPOINT;
		desiredPoints = 1;
		
	}
	else if (selection == 1)	//line
	{
		if (curLayer->shape == nullptr)
		{
			curLayer->shape = new Shape();
			curLayer->shape->AddPoint(XSpace());
			curLayer->shape->AddPoint(XSpace());
		}
		curLayer->shape->type = SHPLINE;
		desiredPoints = 2;
	}
	
	int numVerts = curLayer->shape->getNumVertices();
	int change = desiredPoints - numVerts;
	if (change > 0)
	{
		for (int i = 0; i < change; ++i)
			curLayer->shape->AddPoint(XSpace());
	}
	else if (change < 0)
	{
		for (int i = 0; i > change; --i)
			curLayer->shape->RemoveLastPoint();
	}
	ModelDescribe* mdesc = getMDesc();
	if (mdesc)
		mdesc->UpdateDimensions();
	TransferToGUI();	//just update everything... it's a gui. not computationally intensive. I can afford to be lax here.

}



void devLayerTopPanel::BtnChangeMat(wxCommandEvent& event)
{
	if (curLayer == nullptr || curMatInLayer == nullptr)
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	Material* newMat = nullptr;
	int sel = cbAllMats->GetSelection();
	if ((unsigned int)sel < mdesc->materials.size())
		newMat = mdesc->materials[sel];

	if (newMat == curMatInLayer->data)	//not changing anything!
		return;

	curMatInLayer->data = newMat;	//point to the new data.
	curMatInLayer->id = (newMat) ? newMat->getID() : -1;
	TransferToGUI();	//transfer all the data to the gui.
}


void devLayerTopPanel::DMChangeMat(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;
	int sel = cbMatDescriptors->GetSelection();
	
	if ((unsigned int)sel < curLayer->matls.size())
	{
		if (curMatInLayer == curLayer->matls[sel])
			return;	//not actually changing anything

		curMatInLayer = curLayer->matls[sel];
		TransferToGUI();
	}

}


void devLayerTopPanel::BtnRemoveMat(wxCommandEvent& event)
{
	if (curLayer == nullptr || curMatInLayer == nullptr)
		return;

	int sel = cbMatDescriptors->GetSelection();	//this SHOULD point to curMatInLayer due to ChangeMat. Should doesn't mean does...
	if ((unsigned int)sel < curLayer->matls.size())
	{
		if (curLayer->matls[sel] != curMatInLayer)
			sel = wxNOT_FOUND;
	}
	if (sel == wxNOT_FOUND)
	{
		for (unsigned int i = 0; i < curLayer->matls.size(); ++i)
		{
			if (curLayer->matls[i] == curMatInLayer)
			{
				sel = i;
				break;
			}
		}
	}

	if (sel != wxNOT_FOUND)
	{
		curLayer->matls.erase(curLayer->matls.begin() + sel);
		delete curMatInLayer;
		curMatInLayer = nullptr;
		TransferToGUI();
	}

}


void devLayerTopPanel::BtnAddMat(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	int sel = cbAllMats->GetSelection();
	if ((unsigned int)sel < mdesc->materials.size())
	{
		Material* mat = mdesc->materials[sel];
		ldescriptor<Material*>* desc = new ldescriptor<Material*>;
		desc->data = mat;
		desc->id = mat->getID();
		desc->flag = DISCRETE;
		desc->multiplier = 1.0;
		desc->range.startpos = desc->range.stoppos = XSpace(0.0);
		curLayer->matls.push_back(desc);
		curMatInLayer = desc;
		TransferToGUI();
	}

}

void devLayerTopPanel::DMChangeMatFunct(wxCommandEvent& event)
{
	if (curMatInLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;
	int sel = cbMatDistType->GetSelection();
	switch (sel)
	{
	case 1:
		curMatInLayer->flag = BANDED;
		break;
	case 2:
		curMatInLayer->flag = GAUSSIAN;
		break;
	case 3:
		curMatInLayer->flag = EXPONENTIAL;
		break;
	case 4:
		curMatInLayer->flag = LINEAR;
		break;
	case 5:
		curMatInLayer->flag = PARABOLIC;
		break;
	case 0:
	default:
		curMatInLayer->flag = DISCRETE;
		break;
	}
	UpdateGUIValues(event);
}

void devLayerTopPanel::BtnViewMat(wxCommandEvent& event)
{
	Material* trgMat = nullptr;
	if (curMatInLayer)
		trgMat = curMatInLayer->data;
	
	wxNotebook* parent = (wxNotebook*)GetParent();
	parent->SetSelection(1);	//properties is 0, materials 1, impurities 2, layers 3

	getMyClassParent()->getMatPanel()->setMaterial(trgMat);	//update everything in the gui appropriately!
}

void devLayerTopPanel::BtnChangeImp(wxCommandEvent& event)
{
	if (curLayer == nullptr || curImpInLayer == nullptr)
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	Impurity* newImp = nullptr;
	int sel = cbAllImps->GetSelection();
	unsigned int s = ConvertImpIndex(sel);
	if (s < mdesc->impurities.size())
		newImp = mdesc->impurities[s];

	if (newImp == curImpInLayer->data)	//not changing anything!
		return;

	curImpInLayer->data = newImp;	//point to the new data.
	curImpInLayer->id = ((newImp) ? newImp->getID() : -1);
	TransferToGUI();	//transfer all the data to the gui.

}

void devLayerTopPanel::DMChangeImp(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;

	int sel = cbImpDescriptors->GetSelection();
	if ((unsigned int)sel < curLayer->impurities.size())
	{
		if (curImpInLayer != curLayer->impurities[sel])
		{
			curImpInLayer = curLayer->impurities[sel];
			TransferToGUI();
		}
	}

}

void devLayerTopPanel::BtnRemoveImp(wxCommandEvent& event)
{
	if (curLayer == nullptr || curImpInLayer == nullptr)
		return;

	int sel = cbImpDescriptors->GetSelection();	//this SHOULD point to curImpInLayer due to ChangeImp. Should doesn't mean does...
	if ((unsigned int)sel < curLayer->impurities.size())
	{
		if (curLayer->impurities[sel] != curImpInLayer)
			sel = wxNOT_FOUND;
	}
	if (sel == wxNOT_FOUND)
	{
		for (unsigned int i = 0; i < curLayer->impurities.size(); ++i)
		{
			if (curLayer->impurities[i] == curImpInLayer)
			{
				sel = i;
				break;
			}
		}
	}

	if (sel != wxNOT_FOUND)
	{
		curLayer->impurities.erase(curLayer->impurities.begin() + sel);
		delete curImpInLayer;
		curImpInLayer = nullptr;
		TransferToGUI();
	}

}


void devLayerTopPanel::BtnAddImp(wxCommandEvent& event)
{
	if (curLayer == nullptr)
		return;

	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	int sel = cbAllImps->GetSelection();
	unsigned int s = ConvertImpIndex(sel);
	if (s < mdesc->impurities.size())
	{
		Impurity* imp = mdesc->impurities[s];
		ldescriptor<Impurity*>* desc = new ldescriptor<Impurity*>;
		desc->data = imp;
		desc->id = imp->getID();
		desc->flag = DISCRETE;
		desc->multiplier = 1.0;
		desc->range.startpos = desc->range.stoppos = XSpace(0.0);
		curLayer->impurities.push_back(desc);
		curImpInLayer = desc;
		TransferToGUI();
	}

}


void devLayerTopPanel::DMChangeImpFunct(wxCommandEvent& event)
{
	if (curImpInLayer == nullptr)
		return;
	if (pltWind == nullptr)
		return;
	int sel = cbImpDistType->GetSelection();
	switch (sel)
	{
	case 1:
		curImpInLayer->flag = BANDED;
		break;
	case 2:
		curImpInLayer->flag = GAUSSIAN;
		break;
	case 3:
		curImpInLayer->flag = EXPONENTIAL;
		break;
	case 4:
		curImpInLayer->flag = LINEAR;
		break;
	case 5:
		curImpInLayer->flag = PARABOLIC;
		break;
	case 0:
	default:
		curImpInLayer->flag = DISCRETE;
		break;
	}
	UpdateGUIValues(event);
}


void devLayerTopPanel::BtnViewImp(wxCommandEvent& event)
{
	Impurity* trgImp = nullptr;
	if (curImpInLayer)
		trgImp = curImpInLayer->data;

	wxNotebook* parent = (wxNotebook*)GetParent();
	parent->SetSelection(2);	//properties is 0, materials 1, impurities 2, layers 3

	getMyClassParent()->getImpPanel()->setImpurity(trgImp);	//update everything in the gui appropriately!

}








BEGIN_EVENT_TABLE(devLayerTopPanel, wxPanel)
EVT_BUTTON(LAY_NEW, devLayerTopPanel::NewLayer)
EVT_BUTTON(LAY_REMOVE, devLayerTopPanel::RemoveLayer)
EVT_BUTTON(LAY_RENAME, devLayerTopPanel::RenameLayer)
EVT_BUTTON(LAY_DUPLICATE, devLayerTopPanel::DuplicateLayer)
EVT_BUTTON(LAY_LOAD, devLayerTopPanel::LoadLayer)
EVT_COMBOBOX(LAY_SWITCH, devLayerTopPanel::SwitchLayer)
EVT_BUTTON(LAY_SAVE, devLayerTopPanel::saveLayer)
EVT_COMBOBOX(LAY_SCALES, devLayerTopPanel::UpdateScales)
EVT_CHECKBOX(LAY_CBOXES, devLayerTopPanel::CboxClick)
EVT_TEXT(LAY_TBOX, devLayerTopPanel::TBoxUpdate)
EVT_TEXT(LAY_SHAPE_TBOX, devLayerTopPanel::ShapeTextUpdate)
EVT_TEXT(LAY_SHAPE_ALTER_VERTEX, devLayerTopPanel::AlterShapeVertex)
EVT_COMBOBOX(LAY_SHAPE_SWITCH_VERTEX, devLayerTopPanel::ChangeVertexView)
EVT_COMBOBOX(LAY_CHANGE_SHAPE, devLayerTopPanel::ShapeUpdate)
EVT_BUTTON(LAY_BTN_CHANGE_MAT, devLayerTopPanel::BtnChangeMat)
EVT_COMBOBOX(LAY_CHANGE_MAT, devLayerTopPanel::DMChangeMat)
EVT_BUTTON(LAY_REMOVE_MAT, devLayerTopPanel::BtnRemoveMat)
EVT_BUTTON(LAY_ADD_MAT, devLayerTopPanel::BtnAddMat)
EVT_COMBOBOX(LAY_MAT_FUNCTION, devLayerTopPanel::DMChangeMatFunct)
EVT_BUTTON(LAY_VIEW_MAT, devLayerTopPanel::BtnViewMat)
EVT_BUTTON(LAY_BTN_CHANGE_IMP, devLayerTopPanel::BtnChangeImp)
EVT_COMBOBOX(LAY_CHANGE_IMP, devLayerTopPanel::DMChangeImp)
EVT_BUTTON(LAY_REMOVE_IMP, devLayerTopPanel::BtnRemoveImp)
EVT_BUTTON(LAY_ADD_IMP, devLayerTopPanel::BtnAddImp)
EVT_COMBOBOX(LAY_IMP_FUNCTION, devLayerTopPanel::DMChangeImpFunct)
EVT_BUTTON(LAY_VIEW_IMP, devLayerTopPanel::BtnViewImp)
EVT_COMBOBOX(LAY_MISC, devLayerTopPanel::UpdateGUIValues)

END_EVENT_TABLE()


/*
old code
devLayerTopPanel::devLayerTopPanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
laySizer = new wxBoxSizer(wxVERTICAL);
layBar = new wxBoxSizer(wxHORIZONTAL);

wxArrayString layerChoices;
//materialChoices.Add(wxT("Choice 1"));
//materialChoices.Add(wxT("Choice 2"));
layerChooser = new wxComboBox(this, SWITCH_LAYER, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, layerChoices, wxCB_DROPDOWN | wxCB_READONLY);
layAdd = new wxButton(this, wxID_NEW);
layRem = new wxButton(this, wxID_REMOVE);
//Does it make sense to save a layer
laySave = new wxButton(this, wxID_SAVEAS);
layLoad = new wxButton(this, wxID_OPEN);

layersWindow =  new wxNotebook(this, wxID_ANY);
lpropertiesWindow = new devLpropertiesPanel(layersWindow);
lmaterialsWindow = new devLmaterialsPanel(layersWindow);
limpuritiesWindow = new devLimpurityPanel(layersWindow);

layersWindow->AddPage(lpropertiesWindow, wxT("Layer Properties"), true, 0);
layersWindow->AddPage(lmaterialsWindow, wxT("Layer Materials"), false, 1);
layersWindow->AddPage(limpuritiesWindow, wxT("Layer Impurities"), false, 2);

layBar->Add(layerChooser, 1, wxALL, 5);
layBar->Add(layAdd, 1, wxALL, 5);
layBar->Add(layRem, 1, wxALL, 5);
layBar->Add(laySave, 1, wxALL, 5);
layBar->Add(layLoad, 1, wxALL, 5);

laySizer->Add(layBar, 0, 0, 0);
laySizer->Add(layersWindow, 1, wxEXPAND | wxALL, 5);

prevSelection = -1;
nextID = 0;
}
*/