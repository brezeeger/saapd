#ifndef SIM_CALCULATIONS_H
#define SIM_CALCULATIONS_H


#include "wx/wxprec.h"
#include "wx/panel.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;
class wxImage;
class myImagePane;


class simCalculationPanel : public wxPanel
{
protected:	
	wxStaticBoxSizer* sbGenRec;
	wxCheckBox* chkSRH1;
	wxCheckBox* chkSRH2;
	wxCheckBox* chkSRH3;
	wxCheckBox* chkSRH4;
	wxCheckBox* chkThermalGen;
	wxCheckBox* chkThermalRec;

	wxStaticBoxSizer* sbCurrents;
	wxCheckBox* chkCurrent;
	wxCheckBox* chkTunnel;

	wxStaticBoxSizer* sbOptical;
	wxCheckBox* chkLightPower;	//calculate the intensity of the light going through the device
	wxCheckBox* chkLightAbsorbGeneration;
	wxCheckBox* chkLightAbsorbSRH;	//bands to defect
	wxCheckBox* chkLightAbsorbDefect;	//defect to defect
	wxCheckBox* chkLightSERecombination;
	wxCheckBox* chkLightSEsrh;
	wxCheckBox* chkLightSEDefect;
	wxCheckBox* chkLightEmissions;
	wxCheckBox* chkRecyclePhotonsModifyRates;
	wxTextCtrl* txtOpticalResolution;
	wxTextCtrl* txtOpticalMinEnergy;
	wxTextCtrl* txtOpticalMaxEnergy;	//-1 is no limit!
	wxTextCtrl* txtOpticalIntennsityCutoff;
	wxTextCtrl* txtReflections;

	wxStaticText* stOpticalResolution;
	wxStaticText* stOpticalMinEnergy;
	wxStaticText* stOpticalMaxEnergy;	//-1 is no limit!
	wxStaticText* stOpticalIntennsityCutoff;
	wxStaticText* stReflections;

	wxStaticBoxSizer* sbScatter;
	wxCheckBox* chkScatter;	//calculate scattering rates
	wxCheckBox* chkThermalizeCB;		//THERMALIZE_CB	1
	wxCheckBox* chkThermalizeVB;		//THERMALIZE_VB		2
	wxCheckBox* chkThermalizeACP;		//THERMALIZE_ACP	4
	wxCheckBox* chkThermalizeDON;		//THERMALIZE_DON	8
	wxCheckBox* chkThermalizeUrbachCB;	//THERMALIZE_CBT	16
	wxCheckBox* chkThermalizeUrbachVB;	//THERMALIZE_VBT	32

	myImagePane* calcPicture;

	wxFlexGridSizer* fgszrOpticalTexts;
	wxBoxSizer *szrCol1, *szrCol2, *szrTopRight;

public:
	simCalculationPanel(wxWindow* parent);
	~simCalculationPanel();

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI();
	void VerifyInternalVars() { }	//there's nothing to check here!

	Simulation* getSim();
private:
	bool isInitialized() { return (stReflections != nullptr); }
	wxSizer* szrPrimary;
	DECLARE_EVENT_TABLE()	//wx event handling macro
};

#endif