#ifndef SIM_STEADY_STATE_H
#define SIM_STEADY_STATE_H


#include "wx/wxprec.h"
#include "wx/notebook.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;
class wxPanel;
class SS_SimData;
class Environment;
class SSContactData;
class LightSource;

class ContactCompare {
public:
	enum ContactValueType {
		UNDEFINED_CONTACT_VALS,
		VOLTAGE,
		CURRENT_DENISTY,
		CURRENT,
		EFIELD,
		CURRENT_ED,
		EFIELD_ED,
		CUR_DENS_ED,
		MAX_VALUED_TYPES,
		MINIMIZE_EFIELD_ENERGY,	//don't change order
		OPP_D_FIELD,
		CONSERVE_CURRENT,
		EXTERNAL,
		SINK
	};

	ContactValueType type;
	bool madeNull;	//did this exist in the source and now not exist?
	double value;
	ContactCompare() : type(UNDEFINED_CONTACT_VALS), madeNull(false), value(0.0) { }
	ContactCompare(ContactValueType t, bool nu, double v) : type(t), madeNull(nu), value(v) { }
};

class simSteadyStateBook : public wxNotebook
{
	

	wxPanel *PanelDefinition, *PanelSummary, *PanelDetailSummary;
	

	wxComboBox *cbGroupChooser, *cbEnvChooser, *cbAvailSpectraChooser, *cbAvailContactChooser, *cbChangeAll, *cbQuickAddVariableList, *cbSolMethods;

	//top row
	wxButton *btnNewGroup, *btnRemoveGroup, *btnSaveGroup, *btnLoadGroup, *btnRenameGroup, *btnDuplicateGroup;
	//top define row
	wxButton *btnMoveEnvUp, *btnMoveEnvDown, *btnNewEnv, *btnRemoveEnv, *btnQuickEnvDuplicate, *btnChangeGroup;
	wxButton *btnAddSpectra, *btnRemoveSpectra, *btnAddContact, *btnRemoveContact;
	wxButton *btnAddSolutionMethod, *btnReplaceSolutionMethod, *btnSoluptionUp, *btnSolutionDown, *btnRemoveSolutionMethod;

	wxGrid *gdSolutions;

	wxTextCtrl *txtQuickAddFirst, *txtQuickAddLast, *txtQuickAddNumber;
	wxStaticText *stQuickAddFirst, *stQuickAddLast, *stQuickAddNumber;
	wxTextCtrl *txtCtcV, *txtCtcI, *txtCtcJ, *txtCtcE, *txtCtcI_ED, *txtCtcE_ED, *txtCtcJ_ED;
	wxCheckBox *cbCtcV, *cbCtcI, *cbCtcJ, *cbCtcE, *cbCtcI_ED, *cbCtcE_ED, *cbCtcJ_ED;
	wxStaticText *stSuppressV, *stSupressC, *stSupressEfPoisson, *stSupressEfMinimize, *stSupressOppDField, *stSupressCtcChargeNeutral;
	wxCheckBox *cbCtcExternal, *cbCtcSink;
	wxCheckBox *cbCtcMinEField, *cbCtcOppDField, *cbCtcChargeNeutral;

	wxListBox *lbSpectraUse, *lbContactUse;

	wxTextCtrl *txtStartTol, *txtLastTol;

	wxStaticText *stContactName, *stSolutionMethod, *stSolutionAlgorithmDescription, *stBCondSuppression, *stStartTolerance, *stFinishTolerance;
	wxStaticText *stSpectraTitle, *stContactsTitle, *stValidateMessage;

	wxBoxSizer *szrTopRow, *szrSecTopRow, *szrQuickAdd, *szrLColumn, *szrMidColumn, *szrRightColumn, *szrMainCols,
		*szrSpecAddRemove, *szrCtcAddRemove, *szrSolReorderRemove;
	wxFlexGridSizer *szrfCtcBCond, *szrfSolInputs, *szrfCtcOptions, *szrfCtcSupressions;
	wxStaticBoxSizer *szrBoundaryconditions, *szrContactOptions;

	//////////////////////////////////// Summary

	wxGrid *gdSummaryEnv, *gdSummaryDif;
	wxComboBox *cbSumGroup, *cbSumEnv;
	wxButton *btnSumEditSelEnv;
	wxStaticText *stSumMainTitle, *stSumDifTitle, *stDifExplain;

	wxBoxSizer *szrTopRowSum;

	//////////////////////////////////// Detailed Summary
	wxGrid *gdDetailed;
	wxComboBox *cbDetailedGroup;
	wxComboBox *cbDetailedSwapContacts1;
	wxComboBox *cbDetailedSwapContacts2;
	wxButton *btnDetailedSwapContacts;
	wxBoxSizer *szrDetailedSwap;
	wxStaticText *stDetailedWarning;

	wxString GetSSMethodName(int type);
	
public:
	simSteadyStateBook(wxWindow* parent, wxWindowID id=wxID_ANY);
	~simSteadyStateBook();

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdescDefine(wxCommandEvent& event);
	void UpdateGUIValuesDefine(wxCommandEvent& event);


	void NewGroup(wxCommandEvent& event);
	void RemoveGroup(wxCommandEvent& event);
	void SaveAll(wxCommandEvent& event);
	void LoadGroup(wxCommandEvent& event);
	void DuplicateGroup(wxCommandEvent& event);
	void RenameGroup(wxCommandEvent& event);
	void SwitchEnv(wxCommandEvent& event);
	void MoveEnvUp(wxCommandEvent& event);
	void MoveEnvDown(wxCommandEvent& event);
	void NewEnv(wxCommandEvent& event);
	void RemoveEnv(wxCommandEvent& event);
	void ChangeEnvGroup(wxCommandEvent& event);
	void QuickAddEnvironments(wxCommandEvent& event);
	void AddSpectra(wxCommandEvent& event);
	void RemoveSpectra(wxCommandEvent& event);
	void SelectSpectra(wxCommandEvent& event);
	void AddContact(wxCommandEvent& event);
	void RemoveContact(wxCommandEvent& event);
	void SelectContact(wxCommandEvent& event);
	void ContactCBoxes(wxCommandEvent& event);
	void TextCBoxes(wxCommandEvent& event);
	void AddAlgorithm(wxCommandEvent& event);
	void ReplaceAlgorithm(wxCommandEvent& event);
	void RemoveAlgorithm(wxCommandEvent& event);
	void MoveAlgorithmUp(wxCommandEvent& event);
	void MoveAlgorithmDown(wxCommandEvent& event);
	void ChangeAlgorithm(wxCommandEvent& event);
	
	void ClickAlgBox(wxGridEvent& event);



	void TransferToGUIDefine();

	void UpdateGUIValuesSummary(wxCommandEvent& event);
	void UpdateGUIValuesDetailed(wxCommandEvent& event);
	void ChangeSummaryGroup(wxCommandEvent& event);
	void ChangeSummaryEnv(wxCommandEvent& event);
	void ChangeDetailedGroup(wxCommandEvent& event);
	void SwapContactAssociationBtn(wxCommandEvent& event);
	void SwapContactAssociationCB(wxCommandEvent& event);
	

	SS_SimData* getSSSim();
	Environment* getCurEnv() { return curEnv; }
	
private:
	wxBoxSizer* szrPrimaryDefine, *szrPrimarySummary, *szrPrimaryDetailed;
	std::vector<Environment*> GroupEnvs;
	std::string curGroup;	//a group is defined by the name.
	Environment* curEnv;
	LightSource* curLight;
	SSContactData* curContactData;
	unsigned int RemoveStepRow;

	////////////smmary page
	std::string sumCGroup;
	std::vector<Environment*> sumGroups;
	unsigned int selEnv;

	//////////////detailed page
	//use the same values as the summary page... Make life easier
	void VerifyInternalVars();
	wxArrayString buildDifferenceString(ContactCompare::ContactValueType type, std::vector<ContactCompare>& data);
	void UpdateDifTableRows(wxArrayString& strs, int& curNumRows);
	void AddDetailColumns(const std::vector<int>& ctIDS, int& curCol, const unsigned int which);
	void EditSelectedEnv(wxCommandEvent& event);
	bool modifyAllEnvs();
	void ReorderSims();	//make sure everything in a group is simulated together!
	std::string GetContactTypeName(int type);
	LightSource* getLightSource(int id);
	bool isInitialized() { return (stDetailedWarning != nullptr); }
	
	DECLARE_EVENT_TABLE()	//wx event handling macro
};

#endif