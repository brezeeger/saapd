#ifndef GUI_HELPER_H
#define GUI_HELPER_H

#include "wx/wxprec.h"
#include "wx/textctrl.h"
#include "wx/stattext.h"
class wxGrid;

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#define UNC_SUPER_MIN wxT("\u207B")
#define UNC_SUPER_PLUS wxT("\u207A")
#define UNC_SUPER_3 wxT("\u00B3")
#define UNC_SUPER_2 wxT("\u00B2")
#define UNC_SUPER_1 wxT("\u00B9")

#define TEST_BIT(val, bit) (((val) & (bit)) == (bit))

template <typename T>
void ValueToTBox(wxTextCtrl* ctrl, T value)
{
	if (ctrl)
	{
		wxString tmpStr = "";
		tmpStr << value;
		ctrl->ChangeValue(tmpStr);	//prevents updating stuff due to change
	}
	else
	{
		assert(!"Failed to pass in proper control box!");
	}
}

template <typename T>
void ValueToTBox(wxStaticText* ctrl, T value)
{
	if (ctrl)
	{
		wxString tmpStr = "";
		tmpStr << value;
		ctrl->SetLabel(tmpStr);	//prevents updating stuff due to change
	}
	else
	{
		assert(!"Failed to pass in proper static box!");
	}
}

void SmartResizeGridColumns(wxGrid* grid);

#endif

/*
subscript
a	2090
b	
c	
d	
e	
f	
g	
h	2095
i	1D62
j	2C7C
k	
l	
m	2098
n	2099
o	2092
p	209A
q	
r	1D63
s	209B
t	
u	
v	
w	
x	
y	
z	
1	2081
2	2082
3	2083
4	2084
5	2085
6	
7	
8	
9	
0	2080
Phi	
Psi	03A8	03C8
rho 03c1
(cm\u207B\u00B3) -- (cm^-3)
s\u207B\u00B9				--	s^-1
(cm\u207B\u00B3 s\u207B\u00B9)
*/