
#ifndef SAAPD_KERNEL_H
#define SAAPD_KERNEL_H

#include "wx/wxprec.h"
#include "../gui_source/guiwindows.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/file.h"
#include "wx/process.h"
#endif

//just need A number
//const int KERNEL_UPDATE = wxID_LOWEST-1;

#define MSG_THERMALEQUILIBRIUM 0
#define MSG_THEQ_REFINE 1
#define MSG_TOLERANCE 2
#define MSG_CALCJACOBIAN 3
#define MSG_PROCESSJACOBIAN 4
#define MSG_PROCESSJACOBIANFULL 5
#define MSG_LIGHTSPECTRUM 6
#define MSG_LIGHTEMISSIONS 7
#define MSG_MONTECARLO 8
#define MSG_SS_TRANS 9
#define MSG_MONTECARLO2 10
#define MSG_MEMORYCLEANUP 11
#define MSG_STOPPED 12
#define MSG_GENOUTPUT 13


class mainFrame;
class Simulation;
//this data is both accessible in the GUI and kernel. The start of the variable indicates which can write.
class GUIKernelSharedData
{
public:
	double KernelMaxTStep;	//updated every 10 seconds in kernel
	double KernelsimTime;		//updated every 10 seconds in kernel
	double KernelnextTimeTarget;	//updated upon change in kernel
	double KernelEndTime;	//initialized in kernel...
	bool GUIstopSimulation;	//gracefully end the simulation. Break out of simulation by acting like it finished
	bool GUIabortSimulation;	//kill the simulation, hopefully cleanly.
	bool GUIsaveState;	//should the simulation save the device state (maybe finished running and want to pick up where left off)
	bool GUINext;	//tell it to move on to the next situation
	bool ss;	//is the simulation a steady state simulation? It should update differently otherwise
	bool ssFromTransient;	//is the simulation in steady state as a result of the transient? If so, it will still have the transient to run afterwards
	bool KernelDiverge;
	double KernelTolerance;	//what is the current tolerance
	double KernelMaxTolerance;	//what is the maximum tolerance
	double KernelMinTolerance;	//what is the minimum tolerance
	double KernelTargetTolerance;	//what is the desired tolerance
	int KernelssnumStates;	//how many different calculations are there
	int KernelsscurState;		//which one is currently being calculated
	std::string CurSim;
	std::string GUIWorkDirectory;
	int KernelcurInfoMsg;		//which message to display for the current task at hand
	int KernelshortProgBarRange;	//what is the maximum range here
	int KernelshortProgBarPosition;
	double KernelTimeLeft;	//how much time estimated left
	double KernelMaxChange;
};

class kernelThread : public wxThread
{
public:
	kernelThread(wxString file, wxWindow* p, wxString WorkDirectory=wxT(""));
	time_t TransientKernelInteract(Simulation* sim, time_t& compTime, time_t& SaveStateTime);
	time_t SSKernelInteract(Simulation* sim, time_t& compTime);
	void *Entry();
	
	GUIKernelSharedData shareData;
	int CreateParentEvent(wxEventTypeTag<wxCommandEvent> tag, enum wxIDs type, int eventInt);

private:
	std::string filename;
	wxWindow* parent;
};






#endif