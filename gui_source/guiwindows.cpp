#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "../kernel_source/light.h"

#include "GUIHelper.h"
#include "MatPanel.h"

#include "../kernel_source/model.h"


#if (defined __WXMSW__ && defined _DEBUG)
    #include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

extern std::vector<impurity> impurities_list;
extern std::vector<material> materials_list;
extern std::vector<defect> defects_list;
extern std::vector<layer> layers_list;
extern std::vector<contact> contacts_list;
//These should be kept in sync with their respective wxComboBox

extern double scalarTEN(int index);

wxString scalarName[11]={wxEmptyString, wxT("milli"), wxT("micro"), wxT("nano"), wxT("pico"), wxT("femto"), wxT("10\u2212\u00B9\u2078"), wxT("10\u2212\u00B2\u00B9"), wxT("10\u2212\u00B2\u2074"), wxT("10\u2212\u00B2\u2077"), wxT("10\u2212\u00B3\u2070")};

int layer_selection = -1;

/* In general, the format for the ctors of classes derived from wxPanel here goes as
 * Construct wxSizers
 * Construct window elements
 * Add elements/other wxSizers to wxSizers'
 * Set Sizers + initalize some internal variables in the classs
 */

//BEGIN_EVENT_TABLE(simPropertiesPanel, wxPanel)
//EVT_TEXT(SIM_PROP_TEXT_CTRLS, simPropertiesPanel::UpdateMdesc)
//END_EVENT_TABLE()



simSpectrumPanel::simSpectrumPanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
	spectrumSizer = new wxBoxSizer(wxVERTICAL);
	nameSizer = new wxBoxSizer(wxHORIZONTAL);
	gridSizer = new wxBoxSizer(wxHORIZONTAL);
	buttonSizer = new wxBoxSizer(wxHORIZONTAL);

	spectrumBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxEmptyString);
	onOffBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxEmptyString);

	spectrum = new wxGrid(spectrumBox->GetStaticBox(), wxID_ANY);
	spectrum->CreateGrid(0, 6);
	spectrum->SetRowLabelSize(25);
	spectrum->SetColMinimalAcceptableWidth(100);
	spectrum->SetColFormatFloat(0);
	spectrum->SetColFormatFloat(1);
	spectrum->SetColFormatFloat(2);
	spectrum->SetColFormatFloat(3);
	spectrum->SetColFormatFloat(4);
	spectrum->SetColFormatFloat(5);
	spectrum->SetColLabelValue(0, wxT("Wavelength(nm)"));
	spectrum->SetColLabelValue(1, wxT("Intensity (mW/cm^2)"));
	spectrum->SetColLabelValue(2, wxT("Spread (degrees)"));
	spectrum->SetColLabelValue(3, wxT("X Component"));
	spectrum->SetColLabelValue(4, wxT("Y Component"));
	spectrum->SetColLabelValue(5, wxT("Z Component"));

	onOffTimes = new wxGrid(onOffBox->GetStaticBox(), wxID_ANY);
	onOffTimes->CreateGrid(0, 2);
	spectrum->SetRowLabelSize(25);
	onOffTimes->SetColFormatFloat(0);
	onOffTimes->SetColFormatBool(1);
	onOffTimes->SetColLabelValue(0, wxT("Time(sec)"));
	onOffTimes->SetColLabelValue(1, wxT("Enabled?"));

	nameTxt = new wxStaticText(this, wxID_ANY, wxT("Name: "));
	name = new wxTextCtrl(this, wxID_ANY, wxEmptyString);

	addWave = new wxButton(this, NEW_WAVE, wxT("Add Wavelength"));
	remWave = new wxButton(this, REM_WAVE, wxT("Remove Selected Wavelength(s)"));
	addTime = new wxButton(this, NEW_TIME, wxT("Add Time"));
	remTime = new wxButton(this, REM_TIME, wxT("Remove Selected Time(s)"));
	
	nameSizer->Add(nameTxt, 0, wxALL, 5);
	nameSizer->Add(name, 0, wxALL, 5);

	spectrumBox->Add(spectrum, 1, wxALL | wxEXPAND, 5);

	onOffBox->Add(onOffTimes, 1, wxALL | wxEXPAND, 5);

	gridSizer->Add(spectrumBox, 1, wxALL | wxEXPAND, 5);
	gridSizer->Add(onOffBox, 0, wxALL | wxEXPAND, 5);
	
	buttonSizer->Add(addWave, 0, wxALL, 5);
	buttonSizer->Add(remWave, 0, wxALL, 5);
	buttonSizer->Add(addTime, 0, wxALL, 5);
	buttonSizer->Add(remTime, 0, wxALL, 5);

	spectrumSizer->Add(nameSizer, 0, 0, 0);
	spectrumSizer->Add(gridSizer, 1, wxEXPAND, 0);
	spectrumSizer->Add(buttonSizer, 0, 0, 0);
	//spectrumSizer->Add(selectorSizer, 0, 0, 0);
}

simSpectrumPanel::~simSpectrumPanel()
{
	delete name;
	delete addWave;
	delete remWave;
	delete addTime;
	delete remTime;
	delete nameTxt;
//	spectrumSizer->Clear();	all the sizers are cleared and deleted on window close
//	delete spectrumSizer;
}

void simSpectrumPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(spectrumSizer);
	spectrum->AutoSize();
	onOffTimes->AutoSize();
}

void simSpectrumPanel::NewSpectrum(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	spectrum->AppendRows(1);
}

void simSpectrumPanel::RemoveSpectrum(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	wxArrayInt rows = spectrum->GetSelectedRows();
	for(int i = rows.GetCount()-1; i >= 0; i--)
	{
		spectrum->DeleteRows(rows.Item(i), 1);
	}
}

void simSpectrumPanel::NewTime(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	onOffTimes->AppendRows(1);
}

void simSpectrumPanel::RemoveTime(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	wxArrayInt rows = onOffTimes->GetSelectedRows();
	for(int i = rows.GetCount()-1; i >= 0; i--)
	{
		onOffTimes->DeleteRows(rows.Item(i), 1);
	}
}

BEGIN_EVENT_TABLE(simSpectrumPanel, wxPanel)
EVT_BUTTON(NEW_WAVE, simSpectrumPanel::NewSpectrum)
EVT_BUTTON(REM_WAVE, simSpectrumPanel::RemoveSpectrum)
EVT_BUTTON(NEW_TIME, simSpectrumPanel::NewTime)
EVT_BUTTON(REM_TIME, simSpectrumPanel::RemoveTime)
END_EVENT_TABLE()

simOutputsPanel::simOutputsPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY)
{
	simulationOutputs = new wxStaticText(this, wxID_ANY, wxT("Simulation Outputs"), wxPoint(0,0), wxDefaultSize, wxALIGN_RIGHT);
	staticLine = new wxStaticLine(this, wxID_STATIC, wxPoint(0, 18), wxSize(400, -1), wxLI_HORIZONTAL);
	Ef = new wxCheckBox(this, wxID_ANY, wxT("Ef (Fermi Level)"), wxPoint(0, 80), wxDefaultSize, wxALIGN_RIGHT);
	Efn = new wxCheckBox(this, wxID_ANY, wxT("Efn (Electron quasi-Fermi Level)"), wxPoint(0, 100), wxDefaultSize, wxALIGN_RIGHT);
	Efp = new wxCheckBox(this, wxID_ANY, wxT("Efp (Hole quasi-Fermi Level)"), wxPoint(0, 120), wxDefaultSize, wxALIGN_RIGHT);
	Ec = new wxCheckBox(this, wxID_ANY, wxT("Ec (Conduction band level)"), wxPoint(0, 20), wxDefaultSize, wxALIGN_RIGHT);
	Ev = new wxCheckBox(this, wxID_ANY, wxT("Ev (Valence band level)"), wxPoint(0, 40), wxDefaultSize, wxALIGN_RIGHT);
	Psi = new wxCheckBox(this, wxID_ANY, wxT("\u03D5 (Local Vacuum Level)"), wxPoint(0, 60), wxDefaultSize, wxALIGN_RIGHT);
	electrons = new wxCheckBox(this, wxID_ANY, wxT("Total electrons present"), wxPoint(100, 20), wxDefaultSize, wxALIGN_RIGHT);
	holes = new wxCheckBox(this, wxID_ANY, wxT("Total holes present"), wxPoint(100, 40), wxDefaultSize, wxALIGN_RIGHT);
	impCharge = new wxCheckBox(this, wxID_ANY, wxT("Charge from impurities"), wxPoint(100, 20), wxDefaultSize, wxALIGN_RIGHT);
	n = new wxCheckBox(this, wxID_ANY, wxT("n (Electron Concentration)"), wxPoint(0, 140), wxDefaultSize, wxALIGN_RIGHT);
	p = new wxCheckBox(this, wxID_ANY, wxT("p (Hole Concentration)"), wxPoint(0, 160), wxDefaultSize, wxALIGN_RIGHT);
	deflight = new wxCheckBox(this, wxID_ANY, wxT("Impurity generations"), wxPoint(100, 140), wxDefaultSize, wxALIGN_RIGHT);
	
	condlight = new wxCheckBox(this, wxID_ANY, wxT("Carriers heated"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	genlight = new wxCheckBox(this, wxID_ANY, wxT("Optical Generation"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	genthermal = new wxCheckBox(this, wxID_ANY, wxT("Thermal Generation"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	recthermal = new wxCheckBox(this, wxID_ANY, wxT("Thermal Recombination"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	srh = new wxCheckBox(this, wxID_ANY, wxT("SRH (Schockley-Read-Hall)"), wxPoint(100, 60), wxDefaultSize, wxALIGN_RIGHT);
	rho = new wxCheckBox(this, wxID_ANY, wxT("\u03C1 (Charge Density)"), wxPoint(100, 80), wxDefaultSize, wxALIGN_RIGHT);
	Jn = new wxCheckBox(this, wxID_ANY, wxT("Jn (Electron Current Density)"), wxPoint(100, 100), wxDefaultSize, wxALIGN_RIGHT);
	Jp = new wxCheckBox(this, wxID_ANY, wxT("Jp (Hole Current Density)"), wxPoint(100, 120), wxDefaultSize, wxALIGN_RIGHT);
	stddev = new wxCheckBox(this, wxID_ANY, wxT("stddev (Standard Deviation)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	stderrBox = new wxCheckBox(this, wxID_ANY, wxT("stderr (Standard Error)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	qe = new wxCheckBox(this, wxID_ANY, wxT("qe (Quantum Efficiency)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);

	wxGridSizer* checkboxSizer = new wxGridSizer(12, 2, 10, 10);
	checkboxSizer->Add(Ef, 1, 0, 0);
	checkboxSizer->Add(condlight, 1, 0, 0);
	checkboxSizer->Add(Efn, 1, 0, 0);
	checkboxSizer->Add(genlight, 1, 0, 0);
	checkboxSizer->Add(Efp, 1, 0, 0);
	checkboxSizer->Add(genthermal, 1, 0, 0);
	checkboxSizer->Add(Ec, 1, 0, 0);
	checkboxSizer->Add(recthermal, 1, 0, 0);
	checkboxSizer->Add(Ev, 1, 0, 0);
	checkboxSizer->Add(srh, 1, 0, 0);
	checkboxSizer->Add(Psi, 1, 0, 0);
	checkboxSizer->Add(rho, 1, 0, 0);
	checkboxSizer->Add(electrons, 1, 0, 0);
	checkboxSizer->Add(Jn, 1, 0, 0);
	checkboxSizer->Add(holes, 1, 0, 0);
	checkboxSizer->Add(Jp, 1, 0, 0);
	checkboxSizer->Add(impCharge, 1, 0, 0);
	checkboxSizer->Add(stddev, 1, 0, 0);
	checkboxSizer->Add(n, 1, 0, 0);
	checkboxSizer->Add(stderrBox, 1, 0, 0);
	checkboxSizer->Add(p, 1, 0, 0);
	checkboxSizer->Add(qe, 1, 0, 0);
	checkboxSizer->Add(deflight, 1, 0, 0);
	outputsSizer = new wxBoxSizer(wxVERTICAL);
	outputsSizer->Add(simulationOutputs, 0, wxALL, 10);
	outputsSizer->Add(staticLine, 0, wxEXPAND | wxALL, 5);
	outputsSizer->Add(checkboxSizer, 1, wxEXPAND | wxALL, 5);
}

simOutputsPanel::~simOutputsPanel()
{
	delete Ef;
	delete Efn;
	delete Efp;
	delete Ec;
	delete Ev;
	delete Psi;
	delete electrons;
	delete holes;
	delete impCharge;
	delete n;
	delete p;
	delete deflight;
	delete condlight;
	delete genlight;
	delete genthermal;
	delete recthermal;
	delete srh;
	delete rho;
	delete Jn;
	delete Jp;
	delete stddev;
	delete stderrBox;
	delete qe;
	delete simulationOutputs;
	delete staticLine;
	
	//all sizer's cleared at } when the rest of the generic destructor is called
}

void simOutputsPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(outputsSizer);
}

void simOutputsPanel::outputsToFile(wxFile* oFile)
{
	if (!isInitialized())
		return;
	XMLCat valid(wxT("validoutputs"), oFile);
	if(Ef->GetValue()) XMLLabel(wxT("fermi"), oFile);
	if(Efn->GetValue()) XMLLabel(wxT("efn"), oFile);
	if(Efp->GetValue()) XMLLabel(wxT("efp"), oFile);
	if(Ec->GetValue()) XMLLabel(wxT("conductionband"), oFile);
	if(Ev->GetValue()) XMLLabel(wxT("valenceband"), oFile);
	if(Psi->GetValue()) XMLLabel(wxT("localvacuum"), oFile);
	if(electrons->GetValue()) XMLLabel(wxT("cbcarrier"), oFile);
	if(holes->GetValue()) XMLLabel(wxT("vbcarrier"), oFile);
	if(impCharge->GetValue()) XMLLabel(wxT("impcharge"), oFile);
	if(n->GetValue()) XMLLabel(wxT("nconc"), oFile);
	if(p->GetValue()) XMLLabel(wxT("pconc"), oFile);
	if(deflight->GetValue()) XMLLabel(wxT("deflight"), oFile);
	if(condlight->GetValue()) XMLLabel(wxT("condlight"), oFile);
	if(genlight->GetValue()) XMLLabel(wxT("genlight"), oFile);
	if(genthermal->GetValue()) XMLLabel(wxT("genthermal"), oFile);
	if(recthermal->GetValue()) XMLLabel(wxT("recthermal"), oFile);
	if(srh->GetValue()) XMLLabel(wxT("srh"), oFile);
	if(rho->GetValue()) XMLLabel(wxT("rho"), oFile);
	if(Jn->GetValue()) XMLLabel(wxT("jn"), oFile);
	if(Jp->GetValue()) XMLLabel(wxT("jp"), oFile);
	if(stddev->GetValue()) XMLLabel(wxT("stddev"), oFile);
	if(stderrBox->GetValue()) XMLLabel(wxT("stderr"), oFile);
	if(qe->GetValue()) XMLLabel(wxT("qe"), oFile);
}

simContactsPanel::simContactsPanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
	//wxStaticBoxSizer* contactInfo = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("Contact"));
	//wxFlexGridSizer* fixedVoltage = new wxFlexGridSizer(4, 2, 10, 10);
	//wxFlexGridSizer* cosEquation = new wxFlexGridSizer(5, 2, 10, 10);
	optsSizer = new wxBoxSizer(wxHORIZONTAL);
	graphSizer = new wxStaticBoxSizer(wxHORIZONTAL, this);
	contactsSizer = new wxBoxSizer(wxVERTICAL);
	
	plot = new mpWindow(this, wxID_ANY);
	graph = new ContactPlot();
	xaxis = new mpScaleX(wxT("X (seconds)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
    yaxis = new mpScaleY(wxT("Y (volts)"), mpALIGN_LEFT, true);

	plot->AddLayer(xaxis);
	plot->AddLayer(yaxis);
	plot->AddLayer(graph);
	graphSizer->Add(plot, 1, wxEXPAND, 0);
	contactsSizer->Add(graphSizer, 1, wxALL | wxEXPAND, 5);

	
	wxArrayString contactChoices;
	contactChooser = new wxComboBox(this, SWITCH_SELECTION, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, contactChoices, wxCB_DROPDOWN | wxCB_READONLY);
	conAdd = new wxButton(this, ADD_CON, wxT("New"));
	conRem = new wxButton(this, REM_CON, wxT("Remove"));
	conSave = new wxButton(this, wxID_SAVEAS);
	conLoad = new wxButton(this, wxID_OPEN);
	conBar = new wxBoxSizer(wxHORIZONTAL);
	conBar->Add(contactChooser, 1, wxALL, 5);
	conBar->Add(conAdd, 1, wxALL, 5);
	conBar->Add(conRem, 1, wxALL, 5);
	conBar->Add(conSave, 1, wxALL, 5);
	conBar->Add(conLoad, 1, wxALL, 5);

	addFixed = new wxButton(this, SAVE_FIXED, wxT("Add Fixed Voltage"));
	addCosine = new wxButton(this, SAVE_COS, wxT("Add Cosine Voltage"));
	removeVoltage = new wxButton(this, REMOVE_VOLTAGE, wxT("Remove Voltage"));
	resetView = new wxButton(this, RESET_VIEW, wxT("Reset View"));
	
	optsSizer->Add(addFixed, 0, wxALL, 5);
	optsSizer->Add(addCosine, 0, wxALL, 5);
	optsSizer->Add(removeVoltage, 0, wxALL, 5);
	optsSizer->Add(resetView, 0, wxALL, 5);
	
	contactsSizer->Add(conBar, 0, 0, 0);
	contactsSizer->Add(optsSizer, 0, 0, 0);
	wxCommandEvent blankEvent;
	UpdatePlot(blankEvent); //This must be called after the chooser is initialized
}

simContactsPanel::~simContactsPanel()
{
	contactChooser->Clear();
	delete contactChooser;
//	delete graph;
//	delete xaxis;
//	delete yaxis;	deleting plot takes care of these three
	delete plot;
	
	
	
	delete conAdd;
	delete conRem;
	delete conSave;
	delete conLoad;
	
	delete addFixed;
	delete addCosine;
	delete removeVoltage;
	delete resetView;
	
}

void simContactsPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(contactsSizer);
}

wxString simContactsPanel::NewContact()
{
	if (!isInitialized())
		return wxEmptyString;
	contact newContact;
	wxTextEntryDialog entry(this, wxT("Enter the material's name"));
	if(entry.ShowModal() != wxID_OK)
	{
		return wxEmptyString;
	}
	newContact.name = entry.GetValue();
	contacts_list.push_back(newContact);
	contactChooser->Append(newContact.name);
	int newSelection = contactChooser->GetCount()-1;
	contactChooser->SetSelection(newSelection);
	graph->switchContact(newSelection);
	wxCommandEvent blankEvent;
	UpdatePlot(blankEvent);
	return newContact.name;
}

wxString simContactsPanel::RemoveContact()
{
	if(contactChooser->IsListEmpty()) return wxEmptyString; //empty
	wxString name = contacts_list[contactChooser->GetSelection()].name;
	contacts_list.erase(contacts_list.begin() + contactChooser->GetSelection());
	contactChooser->Delete(contactChooser->GetSelection());
	graph->switchContact(-1);
	wxCommandEvent blankEvent;
	UpdatePlot(blankEvent);
	return name;
}

void simContactsPanel::SwitchContact(wxCommandEvent& event)
{
	graph->switchContact(contactChooser->GetSelection());
	UpdatePlot(event);
}

void simContactsPanel::AddFixed(wxCommandEvent& event)
{
	if(contactChooser->GetSelection() < 0)
	{
		wxMessageDialog diag(this, wxT("Please select a contact first!"), wxT("No Contact Selected"));
		diag.ShowModal();
	}
	else
	{
		AddFixedVoltageDialog diag(this);
		diag.ShowModal();
		UpdatePlot(event);
	}
}

void simContactsPanel::AddCos(wxCommandEvent& event)
{
	if(contactChooser->GetSelection() < 0)
	{
		wxMessageDialog diag(this, wxT("Please select a contact first!"), wxT("No Contact Selected"));
		diag.ShowModal();
	}
	else
	{
		AddCosVoltageDialog diag(this);
		diag.ShowModal();
		UpdatePlot(event);
	}
}

void simContactsPanel::RemoveVoltage(wxCommandEvent& event)
{
	if(contactChooser->GetSelection() < 0)
	{
		wxMessageDialog diag(this, wxT("Please select a contact first!"), wxT("No Contact Selected"));
		diag.ShowModal();
	}
	else
	{
		RemoveVoltageDialog diag(this);
		diag.ShowModal();
		UpdatePlot(event);
	}
}


void simContactsPanel::UpdatePlot(wxCommandEvent& event)
{
	int contactIndex = contactChooser->GetSelection();
	if(contactIndex < 0)
	{
		plot->Fit(0, 1, -1, 1);
		if(graph->changeScale(3))
		{
			plot->DelLayer(xaxis, true, false);
			xaxis = new mpScaleX(wxT("X (") + scalarName[3] + wxT("seconds)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
			plot->AddLayer(xaxis);
		}
		plot->UpdateAll();
		return;
	}
	unsigned int voltageSize = contacts_list[contactIndex].voltages.size();
	switch(voltageSize)
	{
		case 0: //no voltages, just go to a default
			plot->Fit(0, 1, -1, 1);
			if(graph->changeScale(3))
			{
				plot->DelLayer(xaxis, true, false);
				xaxis = new mpScaleX(wxT("X (") + scalarName[3] + wxT("seconds)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
				plot->AddLayer(xaxis);
			}
			break;
		case 1: //a single voltage, go around the value
		{
			voltagePoint* voltage = &contacts_list[contactIndex].voltages[0];
			if(voltage->cos)
			{
				plot->Fit(voltage->time-0.0000000005, voltage->time+0.0000000005,
						voltage->voltage_offset - 2*voltage->amp, voltage->voltage_offset + 2*voltage->amp);
			}
			else
			{
				plot->Fit(voltage->time-0.0000000005, voltage->time+0.0000000005,
						voltage->amp-1, voltage->amp+1);
			}
			if(graph->changeScale(3))
			{
				plot->DelLayer(xaxis, true, false);
				xaxis = new mpScaleX(wxT("X (") + scalarName[3] + wxT("seconds)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
				plot->AddLayer(xaxis);
			}
			break;
		}
		default:
		{
			double voltageMin = 0.0;
			double voltageMax = 0.0;
			for(unsigned int i = 0; i < contacts_list[contactIndex].voltages.size(); i++)
			{
				voltagePoint* voltage = &contacts_list[contactIndex].voltages[i];
				if(voltage->cos)
				{
					if(voltageMax < voltage->amp + voltage->voltage_offset) voltageMax = voltage->amp + voltage->voltage_offset;
					if(voltageMin > -voltage->amp + voltage->voltage_offset) voltageMin = -voltage->amp + voltage->voltage_offset;
				}
				else
				{
					if(voltageMax < voltage->amp) voltageMax = voltage->amp;
					if(voltageMin > voltage->amp) voltageMin = voltage->amp;
				}
			}
			double voltageRange = voltageMax - voltageMin;
			double timeMax = contacts_list[contactIndex].voltages[contacts_list[contactIndex].voltages.size()-1].time;
			double timeMin = contacts_list[contactIndex].voltages[0].time;
			if(timeMin > 0.0) timeMin = 0.0;
			double timeRange = timeMax - timeMin;

			int scale = graph->getScale();
			while(timeRange/scalarTEN(scale) < 1.0) scale++;
			while(scale > 0 && timeRange/scalarTEN(scale) > 1000.0) scale--;
			if(graph->changeScale(scale))
			{
				plot->DelLayer(xaxis, true, false);
				xaxis = new mpScaleX(wxT("X (") + scalarName[scale] + wxT("seconds)"), mpALIGN_BOTTOM, true, mpX_NORMAL);
				plot->AddLayer(xaxis);
			}

			plot->Fit((timeMin - 0.2*timeRange)/scalarTEN(scale), (timeMax + 0.2*timeRange)/scalarTEN(scale), voltageMin-0.2*voltageRange, voltageMax+0.2*voltageRange);
			break;
		}
	}
	plot->UpdateAll();
}

BEGIN_EVENT_TABLE(simContactsPanel, wxPanel)
//EVT_BUTTON(wxID_NEW, simContactsPanel::NewContact)
//EVT_BUTTON(wxID_REMOVE, simContactsPanel::RemoveContact)
EVT_BUTTON(SAVE_FIXED, simContactsPanel::AddFixed)
EVT_BUTTON(SAVE_COS, simContactsPanel::AddCos)
EVT_BUTTON(REMOVE_VOLTAGE, simContactsPanel::RemoveVoltage)
EVT_BUTTON(RESET_VIEW, simContactsPanel::UpdatePlot)
EVT_COMBOBOX(SWITCH_SELECTION, simContactsPanel::SwitchContact)
END_EVENT_TABLE()

devPropertiesPanel::devPropertiesPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY)
{
	propSizer = new wxBoxSizer(wxVERTICAL);
	szrHorizTop = new wxBoxSizer(wxHORIZONTAL);
//	wxFlexGridSizer* lengthSizer = new wxFlexGridSizer(3, 2, 5, 5);
//	wxFlexGridSizer* resolutonSizer = new wxFlexGridSizer(3, 2, 5, 5);

	

	wxArrayString numDims;
	numDims.Add(wxT("1D"));
	numDims.Add(wxT("2D"));
	numDims.Add(wxT("3D"));
	
	rbDimensions = new wxRadioBox(this, PROP_NDIM, wxT("Dimensions"), wxDefaultPosition, wxDefaultSize, numDims, 1, wxRA_SPECIFY_ROWS);
	rbDimensions->Enable(1, false);
	rbDimensions->Enable(2, false);

	//does not seem to work with radio box!
	rbDimensions->SetToolTip(wxT("Is this a 1D, 2D, or 3D device? Currently 2D/3D are disabled until 1D has consistently stable code"));
	
	btnDonate = new wxButton(this, DONATE_BTN, "Donate");
	btnDonate->SetToolTip("Opens your web browser to the developer's paypal.me page.");

	note = new wxStaticText(this, wxID_ANY, wxT("\u25CF In general when building a simulation, work left to right through the tabs, moving on upon completion. Future tabs may rely on this data.\n\n\
\u25CF This software is in BETA.\n\
\u25CF Please report bugs/crashes and the circumstances leading to it to saapd.PV@gmail.com. Please use subject BUG.\n\
\u25CF Feature requests may also be sent to saapd.PV@gmail.com. Please use subject FEATURE.\n\n\
\u25CF This software should be FREE as it was initially funded by the National Science Foundation and that is one of the conditions. If anyone charges you to obtain this version, please notify saapd.PV@gmail.com immediately. Please use subject THEFT.\n\n\
\u25CF Some aspects of SAAPD are disabled. Usually, this is planning ahead for future features. Stages of development may vary from conceptual to completed without proper testing.\n\n\
\u25CF If you find this useful, and particularly want it to become more useful, please consider donating to support the developer."));

	

	szrHorizTop->Add(rbDimensions, 0, wxALL | wxALIGN_TOP, 4);
	szrHorizTop->AddStretchSpacer();
	szrHorizTop->Add(btnDonate, 0, wxALL | wxALIGN_TOP, 4);
	propSizer->Add(szrHorizTop, 0, wxALL | wxEXPAND, 10);
	propSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
	propSizer->Add(note, 0, wxALL | wxEXPAND, 10);
	propSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL), 0, wxEXPAND, 0);
//	propSizer->Add(resolutonSizer, 0, 0, 0);

	SizeRight();

	propSizer->Layout();
//	wxSize sz = this->GetSize();
//	note->SetSize(note->GetSizeFromTextSize(sz));
}

void devPropertiesPanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(propSizer);
}

void devPropertiesPanel::OnDonate(wxCommandEvent& event) {
	wxLaunchDefaultBrowser("http://www.paypal.me/brezeeger");
	wxMessageDialog msg(this, "Thank you for your much needed generosity!", "Thank you!");
	msg.ShowModal();
}


void devPropertiesPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	rbDimensions->SetSelection(mdesc->NDim - 1);
//	note->SetSize(note->GetSizeFromTextSize(-1));
}
void devPropertiesPanel::TransferToMDesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMDesc();
	if (mdesc == nullptr)
		return;

	
	mdesc->NDim = rbDimensions->GetSelection() + 1;

}

ModelDescribe* devPropertiesPanel::getMDesc()
{ 
	return getMyClassParent()->getMDesc();
}

mainFrame* devPropertiesPanel::getMyClassParent()
{
	return (mainFrame*)GetParent()->GetParent()->GetParent();
}

BEGIN_EVENT_TABLE(devPropertiesPanel, wxPanel)
EVT_TEXT(PROP_TEXT, devPropertiesPanel::TransferToMDesc)
EVT_RADIOBOX(PROP_NDIM, devPropertiesPanel::TransferToMDesc)
EVT_BUTTON(DONATE_BTN, devPropertiesPanel::OnDonate)
END_EVENT_TABLE()


executePanel::executePanel(wxWindow* parent)
: wxPanel(parent, wxID_ANY)
{
	resWrapper = new wxBoxSizer(wxVERTICAL);
	resSizer = new wxFlexGridSizer(2, 4, 10, 10);
	resSizer->SetFlexibleDirection(wxHORIZONTAL);
	resSizer->AddGrowableCol(3, 1);
	dirSizer = new wxBoxSizer(wxHORIZONTAL);

	working = new wxStaticText(this, wxID_ANY, wxT("Output Directory:"));
	workingDir = new wxTextCtrl(this, wxID_ANY, wxGetCwd());
	selectDir = new wxButton(this, CHANGE_DIR, wxT("Select Working Directory"));
	
	runCurrent = new wxButton(this, RUN_CUR, wxT("Run current simulation"));
	/*wxButton* pause = new wxButton(this, wxID_ANY, wxT("Pause simulation"));
	wxButton* stop = new wxButton(this, wxID_ANY, wxT("Stop simulation"));
	wxButton* cancel = new wxButton(this, wxID_ANY, wxT("Cancel simulation"));*/
	loadAndRun = new wxButton(this, LOAD_AND_RUN, wxT("Load and run existing simulation"));
	msg=0;
	simRunning = 0;
	wxArrayString simSelected;
	
	runBatch = new wxButton(this, RUN_BATCH, wxT("Run batch simulations"));
	simList = new wxComboBox(this, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, simSelected, wxCB_DROPDOWN | wxCB_READONLY);
	loadSim = new wxButton(this, ADD_BATCH, wxT("Append Simulation to Batch"));
	AddFolderBatch = new wxButton(this, ADD_FOLDER_BATCH, wxT("Add Folder to Batch"));
	deleteSim = new wxButton(this, REMOVE_BATCH, wxT("Remove Simulation from Batch"));
	
	outputText = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY);
	message = new wxStaticText(this, wxID_ANY, wxT("No Simulation Currently Running"));

	dirSizer->Add(working, 0, wxALL, 5);
	dirSizer->Add(workingDir, 1, wxEXPAND | wxALL, 5);
	dirSizer->Add(selectDir, 0, wxALL, 5);

	resSizer->Add(runCurrent, 0, wxALIGN_CENTER, 0);
	/*resSizer->Add(pause, 1, 0, 0);
	resSizer->Add(stop, 1, 0, 0);
	resSizer->Add(cancel, 1, 0, 0);*/
	resSizer->Add(loadAndRun, 0, wxALIGN_CENTER, 0);
	//resSizer->AddStretchSpacer();
	resSizer->Add(loadSim, 0, wxALIGN_CENTER, 0);
	resSizer->Add(simList, 1, wxEXPAND | wxGROW | wxRIGHT, 10);
	
	
	resSizer->Add(runBatch, 0, wxALIGN_CENTER, 0);
	resSizer->Add(AddFolderBatch, 0, wxALIGN_CENTER/*wxSizerFlags().Center()*/, 0);
	resSizer->Add(deleteSim, 0, wxALIGN_CENTER, 0);
	resSizer->Add(message, 1, wxEXPAND | wxGROW | wxRIGHT | wxALIGN_CENTER, 5);
	

	resWrapper->Add(dirSizer, 0, wxEXPAND, 0);
	resWrapper->Add(resSizer, 0, wxTOP | wxEXPAND, 5);
	resWrapper->Add(outputText, 1, wxEXPAND | wxALL, 5);

	runBatch->Disable();
	deleteSim->Disable();
}

executePanel::~executePanel()
{
	simList->Clear();
	
	delete working;
	delete selectDir;
	delete runBatch;
	delete simList;
	delete loadSim;
	delete AddFolderBatch;
	delete deleteSim;
	delete workingDir;
	delete outputText;
	delete message;
	delete runCurrent;
	delete loadAndRun;
	
}

void executePanel::changeDir(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	wxDirDialog dirDialog(this, wxT("Select Output Directory"), wxT("/"), wxDD_NEW_DIR_BUTTON);

	if(dirDialog.ShowModal() == wxID_OK)
	{
		workingDir->SetValue(dirDialog.GetPath());
		wxSetWorkingDirectory(dirDialog.GetPath());
	}
}

void executePanel::SizeRight()
{
	if (!isInitialized())
		return;
	SetSizer(resWrapper);
}

void executePanel::UpdateMessage(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	msg=0;
	wxString tmp;

	switch(simRunning)
	{
	case 1:
		tmp = wxT("Simulation Running -");
		break;
	case 2:
		tmp = wxT("Simulation Running /");
		break;
	case 3:
		tmp = wxT("Simulation Running | ");
		break;
	case 4:
		tmp = wxT("Simulation Running \\");
		break;
	default:
		tmp = wxT("Simulation Running   ");
		break;
	}

	if(msg < 4)
		tmp = tmp + wxT(" Updated output file");
	

	message->SetLabel(tmp);

	Update();
	event.Skip();
	return;
}

void executePanel::SimIsRunning(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	simRunning++;
	if(simRunning>4)
		simRunning=1;
	
	wxString tmp;
	switch(simRunning)
	{
	case 1:
		tmp = wxT("Simulation Running -");
		break;
	case 2:
		tmp = wxT("Simulation Running /");
		break;
	case 3:
		tmp = wxT("Simulation Running | ");
		break;
	case 4:
		tmp = wxT("Simulation Running \\");
		break;
	default:
		tmp = wxT("Simulation Running  ");
		break;
	}
	
	msg++;
	if(msg < 4)
		tmp = tmp + wxT(" Updated output file");

	message->SetLabel(tmp);
	Update();
	event.Skip();
	return;
}

void executePanel::ClearMessage(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	msg = 0;
	message->SetLabelText(wxT(""));
	Update();
	event.Skip();
	return;
}
void executePanel::MessageRecalcs(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	message->SetLabelText(wxT("Recalculations: "+event.GetInt()));
	Update();
	event.Skip();
	return;

}



BEGIN_EVENT_TABLE(executePanel, wxPanel)
EVT_BUTTON(CHANGE_DIR, executePanel::changeDir)
EVT_COMMAND(UPDATED_FILE, wxEVT_COMMAND_TEXT_UPDATED, executePanel::UpdateMessage)
EVT_COMMAND(CLEAR_MESSAGE, wxEVT_COMMAND_TEXT_UPDATED, executePanel::ClearMessage)
EVT_COMMAND(REDO_CALCS, wxEVT_COMMAND_TEXT_UPDATED, executePanel::MessageRecalcs)
EVT_COMMAND(SIMHASNOTCRASHED, wxEVT_COMMAND_TEXT_UPDATED, executePanel::SimIsRunning)
END_EVENT_TABLE()