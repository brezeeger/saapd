#ifndef SIM_OUTPUTS_H
#define SIM_OUTPUTS_H

#include "wx/wxprec.h"
#include "wx/panel.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;


class simOutputPanel : public wxPanel
{
protected:
	wxCheckBox *chkFermi;
	wxCheckBox *chkFermiN;
	wxCheckBox *chkFermiP;
	wxCheckBox *chkEc;
	wxCheckBox *chkEv;
	wxCheckBox *chkPsi;
	wxCheckBox *chkN;
	wxCheckBox *chkP;
	wxCheckBox *chkGenTH;
	wxCheckBox *chkRecTh;
	wxCheckBox *chkRho;
	wxCheckBox *chkJp;
	wxCheckBox *chkJn;
	wxCheckBox *chkQE;
	wxCheckBox *chkLGen;


	wxCheckBox *chkLightEmission;
	wxCheckBox *chkLPow;
	wxCheckBox *chkLSRH;
	wxCheckBox *chkLDef;
	wxCheckBox *chkSERec;
	wxCheckBox *chkSESRH;
	wxCheckBox *chkSEDef;

	
	wxCheckBox *chkSRH;
	wxCheckBox *chkCarrierCB;
	wxCheckBox *chkCarrierVB;
	wxCheckBox *chkImpCharge;
	wxCheckBox *chkScatter;
	wxCheckBox *chkCBNRin;
	wxCheckBox *chkVBNRin;
	wxCheckBox *chkDonNRin;
	wxCheckBox *chkAcpNRin;

	wxCheckBox *chkStdDev;
	wxCheckBox *chkError;
//	wxCheckBox *chkChgError;		DEPRECATED
	wxCheckBox *chkEmptyQE;
	wxCheckBox *chkRatePlots;
	wxCheckBox *chkAllIndivRates;

	wxCheckBox *chkCapactiance;
	wxCheckBox *chkJV;	//steady state
	wxCheckBox *chkContactCharge;	//transient
	wxCheckBox *chkContactState;
	wxCheckBox *chkEnvSummary;
	wxCheckBox *chkEnvResults;

	wxTextCtrl *txtMaxBin;
	wxStaticText *stMaxBin;

	wxTextCtrl *txtFileName;
	wxStaticText *stFileName;

	wxStaticBoxSizer *bxszrStandard;
	wxStaticBoxSizer *bxszrOptical;
	wxStaticBoxSizer *bxszrMiscellaneous;
	wxStaticBoxSizer *bxszrTransient;
	wxStaticBoxSizer *bxszrSteadyState;

	wxBoxSizer *bxszrHorizMain;
	wxBoxSizer *bxszrSimName;
	wxBoxSizer *bxszrSSTran;
	wxBoxSizer *bxszrOptMisc;
	wxBoxSizer *bxszrCol;
	wxBoxSizer *bxszrButtons;

	wxButton *btnCommon, *btnDark, *btnPL, *btnLight, *btnFull, *btnClear;

public:
	simOutputPanel(wxWindow* parent);
	~simOutputPanel();

	void SetBasics(wxCommandEvent& event);
	void SetDark(wxCommandEvent& event);
	void SetLight(wxCommandEvent& event);
	void SetPL(wxCommandEvent& event);
	void SetFull(wxCommandEvent& event);
	void ClearChecks(wxCommandEvent& event);

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);
	void TransferToGUI();
	Simulation* getSim();
	void VerifyInternalVars() { }	//there's nothing to check here!
private:
	wxSizer* szrPrimary;
	bool isInitialized() { return (bxszrButtons != nullptr); }
	DECLARE_EVENT_TABLE()	//wx event handling macro
};

#endif