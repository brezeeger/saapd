#include "gui_structs.h"
#include "guiwindows.h"
#include "guidialogs.h"
#include "kernel.h"
#include "GUIHelper.h"
#include "gui.h"

#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif 

#define PI 3.14159265358979323846


extern std::vector<contact> contacts_list;

CalcDialog::CalcDialog(wxWindow* parent)
	: wxDialog(parent, wxID_ANY, wxT("Pi Calculator"), wxDefaultPosition, wxSize(400,200))
{
	calcSizer = new wxBoxSizer(wxHORIZONTAL);

	input = new wxTextCtrl(this, CALC_BUTTON, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	timesPi = new wxStaticText(this, wxID_ANY, wxT("* \u03C0 ="));
	output = new wxTextCtrl(this, CALC_BUTTON, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
//	calc = new wxButton(this, CALC_BUTTON);

	calcSizer->Add(input, 0, wxALL, 5);
	calcSizer->Add(timesPi, 0, wxALL, 5);
	calcSizer->Add(output, 0, wxALL, 5);
//	calcSizer->Add(calc, 0, wxALL, 5);

	SetSizerAndFit(calcSizer);

}

CalcDialog::~CalcDialog()
{
	delete input;
	delete output;
	delete timesPi;
//	delete calc;

}

void CalcDialog::Calculate(wxCommandEvent& event)
{
	double value;
	if (event.GetEventObject() == input && output!=nullptr) {
		if (input->GetValue().ToDouble(&value) == true) {
			value *= PI;
			ValueToTBox(output, value);
		}
	}
	else if (event.GetEventObject() == output && input!=nullptr) {
		if (output->GetValue().ToDouble(&value) == true) {
			value /= PI;
			ValueToTBox(input, value);
		}
	}
//	output->SetValue(wxString::Format(wxT("%E"), value*PI));
}

void CalcDialog::CloseWindow(wxCloseEvent& event) {
	Destroy();
}

BEGIN_EVENT_TABLE(CalcDialog, wxDialog)
EVT_TEXT(CALC_BUTTON, CalcDialog::Calculate)
EVT_CLOSE(CalcDialog::CloseWindow)
END_EVENT_TABLE()

AddFixedVoltageDialog::AddFixedVoltageDialog(simContactsPanel* parent)
: wxDialog(parent, wxID_ANY, wxT("Add Fixed"))
{
	contactIndex = parent->contactChooser->GetSelection(); //save this b/c won't change if modal

	fixedSizer = new wxFlexGridSizer(3, 2, 0, 0);

	timeLabel = new wxStaticText(this, wxID_ANY, wxT("Time:"));
	timeTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	voltageLabel = new wxStaticText(this, wxID_ANY, wxT("Voltage:"));
	voltageTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	ok = new wxButton(this, wxID_OK);
	cancel = new wxButton(this, wxID_CANCEL);

	fixedSizer->Add(timeLabel, 0, wxALL, 5);
	fixedSizer->Add(timeTxt, 0, wxALL, 5);
	fixedSizer->Add(voltageLabel, 0, wxALL, 5);
	fixedSizer->Add(voltageTxt, 0, wxALL, 5);
	fixedSizer->Add(ok, 0, wxALL, 5);
	fixedSizer->Add(cancel, 0, wxALL, 5);

	SetSizerAndFit(fixedSizer);
}

AddFixedVoltageDialog::~AddFixedVoltageDialog()
{
	fixedSizer->Clear();
	
	delete timeTxt;
	delete voltageTxt;
	delete ok;
	delete cancel;
	delete voltageLabel;
	delete timeLabel;
	delete fixedSizer;
}

bool AddFixedVoltageDialog::TransferDataFromWindow()
{
	voltagePoint newPoint;
	newPoint.cos = false;
	if(!voltageTxt->GetValue().ToDouble(&newPoint.amp)) return diag_inval_input(wxT("The voltage is not a number"));
	if(!timeTxt->GetValue().ToDouble(&newPoint.time)) return diag_inval_input(wxT("The time is not a number"));
	//if(newPoint.time < 0) return diag_inval_input(wxT("The start time must be non-negative"));
	std::vector<voltagePoint>::iterator it = contacts_list[contactIndex].voltages.begin();
	while (it < contacts_list[contactIndex].voltages.end() && it->time < newPoint.time) it++;
	contacts_list[contactIndex].voltages.insert(it, newPoint);
	return true;
}

bool AddFixedVoltageDialog::diag_inval_input(wxString error_msg)
{
	wxMessageDialog diag(this, error_msg, wxT("Invalid Input"));
	diag.ShowModal();
	return false; //always return false so that the original dialog stays up
}

AddCosVoltageDialog::AddCosVoltageDialog(simContactsPanel* parent)
: wxDialog(parent, wxID_ANY, wxT("Add Cosine"))
{
	contactIndex = parent->contactChooser->GetSelection(); //save this b/c won't change if modal

	wxFlexGridSizer* cosSizer = new wxFlexGridSizer(6, 2, 0, 0);

	wxStaticText* timeLabel = new wxStaticText(this, wxID_ANY, wxT("Time:"));
	timeTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	wxStaticText* ampLabel = new wxStaticText(this, wxID_ANY, wxT("Amplitude:"));
	ampTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	wxStaticText* freqLabel = new wxStaticText(this, wxID_ANY, wxT("Frequency:"));
	freqTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	wxStaticText* phaseLabel = new wxStaticText(this, wxID_ANY, wxT("Phase Offset:"));
	phaseTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	wxStaticText* voltageOffsetLabel = new wxStaticText(this, wxID_ANY, wxT("Voltage Offset:"));
	voltageOffsetTxt = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	wxButton* ok = new wxButton(this, wxID_OK);
	wxButton* cancel = new wxButton(this, wxID_CANCEL);

	cosSizer->Add(timeLabel, 0, wxALL, 5);
	cosSizer->Add(timeTxt, 0, wxALL, 5);
	cosSizer->Add(ampLabel, 0, wxALL, 5);
	cosSizer->Add(ampTxt, 0, wxALL, 5);
	cosSizer->Add(freqLabel, 0, wxALL, 5);
	cosSizer->Add(freqTxt, 0, wxALL, 5);
	cosSizer->Add(phaseLabel, 0, wxALL, 5);
	cosSizer->Add(phaseTxt, 0, wxALL, 5);
	cosSizer->Add(voltageOffsetLabel, 0, wxALL, 5);
	cosSizer->Add(voltageOffsetTxt, 0, wxALL, 5);
	cosSizer->Add(ok, 0, wxALL, 5);
	cosSizer->Add(cancel, 0, wxALL, 5);

	SetSizerAndFit(cosSizer);
}

AddCosVoltageDialog::~AddCosVoltageDialog()
{
	cosSizer->Clear();
	
	delete timeTxt;
	delete ampTxt;
	delete freqTxt;
	delete phaseTxt;
	delete voltageOffsetTxt;
	delete cosSizer;
	delete timeLabel;
	delete ampLabel;
	delete freqLabel;
	delete phaseLabel;
	delete voltageOffsetLabel;
	delete ok;
	delete cancel;
}

bool AddCosVoltageDialog::TransferDataFromWindow()
{
	voltagePoint newPoint;
	newPoint.cos = true;
	if(!ampTxt->GetValue().ToDouble(&newPoint.amp)) return diag_inval_input(wxT("The amplitude is not a number"));
	if(!timeTxt->GetValue().ToDouble(&newPoint.time)) return diag_inval_input(wxT("The amplitude is not a number"));
	if(!freqTxt->GetValue().ToDouble(&newPoint.freq)) return diag_inval_input(wxT("The frequency is not a number"));
	if(!phaseTxt->GetValue().ToDouble(&newPoint.phase_offset)) return diag_inval_input(wxT("The phase offset is not a number"));
	if(!voltageOffsetTxt->GetValue().ToDouble(&newPoint.voltage_offset)) return diag_inval_input(wxT("The voltage offset is not a number"));
	//if(newPoint.time < 0) return diag_inval_input(wxT("The start time must be non-negative"));
	std::vector<voltagePoint>::iterator it = contacts_list[contactIndex].voltages.begin();
	while (it < contacts_list[contactIndex].voltages.end() && it->time < newPoint.time) it++;
	contacts_list[contactIndex].voltages.insert(it, newPoint);
	return true;
}

bool AddCosVoltageDialog::diag_inval_input(wxString error_msg)
{
	wxMessageDialog diag(this, error_msg, wxT("Invalid Input"));
	diag.ShowModal();
	return false; //always return false so that the original dialog stays up
}

RemoveVoltageDialog::RemoveVoltageDialog(simContactsPanel* parent)
: wxDialog(parent, wxID_ANY, wxT("Remove Voltage"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX)
{
	contactIndex = parent->contactChooser->GetSelection(); //save this b/c won't change if modal

	remSizer = new wxBoxSizer(wxVERTICAL);
	buttonSizer = new wxBoxSizer(wxHORIZONTAL);

	voltageList = new wxListCtrl(this, wxID_ANY, wxPoint(0, 0), wxDefaultSize, wxLC_REPORT | wxLC_HRULES | wxLC_VRULES);
	voltageList->AppendColumn(wxT("Time"));
	voltageList->AppendColumn(wxT("Amplitude"));
	voltageList->AppendColumn(wxT("Frequency"));
	voltageList->AppendColumn(wxT("Phase Offset"));
	voltageList->AppendColumn(wxT("Voltage Offset"));

	for(unsigned int i = 0; i < contacts_list[contactIndex].voltages.size(); i++)
	{
		voltageList->InsertItem(i, wxString::Format(wxT("%g"), contacts_list[contactIndex].voltages[i].time));
		voltageList->SetItem(i, 1, wxString::Format(wxT("%g"), contacts_list[contactIndex].voltages[i].amp));
		if(contacts_list[contactIndex].voltages[i].cos)
		{
			voltageList->SetItem(i, 2, wxString::Format(wxT("%g"), contacts_list[contactIndex].voltages[i].freq));
			voltageList->SetItem(i, 3, wxString::Format(wxT("%g"), contacts_list[contactIndex].voltages[i].phase_offset));
			voltageList->SetItem(i, 4, wxString::Format(wxT("%g"), contacts_list[contactIndex].voltages[i].voltage_offset));
		}
	}

	ok = new wxButton(this, wxID_OK);
	cancel = new wxButton(this, wxID_CANCEL);

	buttonSizer->Add(ok, 0, wxALL, 5);
	buttonSizer->Add(cancel, 0, wxALL, 5);

	remSizer->Add(voltageList, 1, wxEXPAND | wxALL, 5);
	remSizer->Add(buttonSizer, 0, 0, 0);

	SetSizerAndFit(remSizer);
}
RemoveVoltageDialog::~RemoveVoltageDialog()
{
	delete ok;
	delete cancel;
}
/*
SelectGraphDialog::~SelectGraphDialog()
{
	parent=NULL;	//don't delete the parent! The parent deletion will cause this one to delete

	delete Ef;
	delete Efn;
	delete Efp;
	delete Ec;
	delete Ev;
	delete Psi;
	delete electrons;
	delete holes;
	delete impCharge;
	delete n;
	delete p;
	delete deflight;
	delete condlight;
	delete genlight;
	delete genthermal;
	delete recthermal;
	delete srh;
	delete rho;
	delete Jn;
	delete Jp;
	delete stddev;
	delete stderrBox;
	delete qe;


}
*/
bool RemoveVoltageDialog::TransferDataFromWindow()
{
	long item = -1;
	while(true)
	{
		item = voltageList->GetNextItem(item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
		if (item == -1 )
			break;

		contacts_list[contactIndex].voltages.erase(contacts_list[contactIndex].voltages.begin() + item);
		voltageList->DeleteItem(item); //we do this to ensure the indicies that we want to delete
		//match up with the vector's
    }
	return true;
}
/*
SelectGraphDialog::SelectGraphDialog(resultsPanel* result)
: wxDialog(result, wxID_ANY, wxT("Select Variables"))
{
	parent = result;

	outputsSizer = new wxBoxSizer(wxVERTICAL);
	wxStaticText* simulationOutputs = new wxStaticText(this, wxID_ANY, wxT("Simulation Outputs"), wxPoint(0,0), wxDefaultSize, wxALIGN_RIGHT);
	wxStaticLine* staticLine = new wxStaticLine(this, wxID_STATIC, wxPoint(0, 18), wxSize(400, -1), wxLI_HORIZONTAL);
	Ef = new wxCheckBox(this, wxID_ANY, wxT("Ef (Fermi Level)"), wxPoint(0, 80), wxDefaultSize, wxALIGN_RIGHT);
	Efn = new wxCheckBox(this, wxID_ANY, wxT("Efn (Electron quasi-Fermi Level)"), wxPoint(0, 100), wxDefaultSize, wxALIGN_RIGHT);
	Efp = new wxCheckBox(this, wxID_ANY, wxT("Efp (Hole quasi-Fermi Level)"), wxPoint(0, 120), wxDefaultSize, wxALIGN_RIGHT);
	Ec = new wxCheckBox(this, wxID_ANY, wxT("Ec (Conduction band level)"), wxPoint(0, 20), wxDefaultSize, wxALIGN_RIGHT);
	Ev = new wxCheckBox(this, wxID_ANY, wxT("Ev (Valence band level)"), wxPoint(0, 40), wxDefaultSize, wxALIGN_RIGHT);
	Psi = new wxCheckBox(this, wxID_ANY, wxT("\u03D5 (Local Vacuum Level)"), wxPoint(0, 60), wxDefaultSize, wxALIGN_RIGHT);
	electrons = new wxCheckBox(this, wxID_ANY, wxT("Total electrons present"), wxPoint(100, 20), wxDefaultSize, wxALIGN_RIGHT);
	holes = new wxCheckBox(this, wxID_ANY, wxT("Total holes present"), wxPoint(100, 40), wxDefaultSize, wxALIGN_RIGHT);
	impCharge = new wxCheckBox(this, wxID_ANY, wxT("Charge from impurities"), wxPoint(100, 20), wxDefaultSize, wxALIGN_RIGHT);
	n = new wxCheckBox(this, wxID_ANY, wxT("n (Electron Concentration)"), wxPoint(0, 140), wxDefaultSize, wxALIGN_RIGHT);
	p = new wxCheckBox(this, wxID_ANY, wxT("p (Hole Concentration)"), wxPoint(0, 160), wxDefaultSize, wxALIGN_RIGHT);
	deflight = new wxCheckBox(this, wxID_ANY, wxT("Impurity generations"), wxPoint(100, 140), wxDefaultSize, wxALIGN_RIGHT);
	
	condlight = new wxCheckBox(this, wxID_ANY, wxT("Carriers heated"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	genlight = new wxCheckBox(this, wxID_ANY, wxT("Optical Generation"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	genthermal = new wxCheckBox(this, wxID_ANY, wxT("Thermal Generation"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	recthermal = new wxCheckBox(this, wxID_ANY, wxT("Thermal Recombination"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	srh = new wxCheckBox(this, wxID_ANY, wxT("SRH (Schockley-Read-Hall)"), wxPoint(100, 60), wxDefaultSize, wxALIGN_RIGHT);
	rho = new wxCheckBox(this, wxID_ANY, wxT("\u03C1 (Charge Density)"), wxPoint(100, 80), wxDefaultSize, wxALIGN_RIGHT);
	Jn = new wxCheckBox(this, wxID_ANY, wxT("Jn (Electron Current Density)"), wxPoint(100, 100), wxDefaultSize, wxALIGN_RIGHT);
	Jp = new wxCheckBox(this, wxID_ANY, wxT("Jp (Hole Current Density)"), wxPoint(100, 120), wxDefaultSize, wxALIGN_RIGHT);
	stddev = new wxCheckBox(this, wxID_ANY, wxT("stddev (Standard Deviation)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	stderrBox = new wxCheckBox(this, wxID_ANY, wxT("stderr (Standard Error)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	qe = new wxCheckBox(this, wxID_ANY, wxT("qe (Quantum Efficiency)"), wxPoint(100, 160), wxDefaultSize, wxALIGN_RIGHT);
	wxSizer* buttonsSizer = CreateButtonSizer(wxOK | wxCANCEL);	


	wxGridSizer* checkboxSizer = new wxGridSizer(12, 2, 10, 10);
	checkboxSizer->Add(Ef, 1, 0, 0);
	checkboxSizer->Add(condlight, 1, 0, 0);
	checkboxSizer->Add(Efn, 1, 0, 0);
	checkboxSizer->Add(genlight, 1, 0, 0);
	checkboxSizer->Add(Efp, 1, 0, 0);
	checkboxSizer->Add(genthermal, 1, 0, 0);
	checkboxSizer->Add(Ec, 1, 0, 0);
	checkboxSizer->Add(recthermal, 1, 0, 0);
	checkboxSizer->Add(Ev, 1, 0, 0);
	checkboxSizer->Add(srh, 1, 0, 0);
	checkboxSizer->Add(Psi, 1, 0, 0);
	checkboxSizer->Add(rho, 1, 0, 0);
	checkboxSizer->Add(electrons, 1, 0, 0);
	checkboxSizer->Add(Jn, 1, 0, 0);
	checkboxSizer->Add(holes, 1, 0, 0);
	checkboxSizer->Add(Jp, 1, 0, 0);
	checkboxSizer->Add(impCharge, 1, 0, 0);
	checkboxSizer->Add(stddev, 1, 0, 0);
	checkboxSizer->Add(n, 1, 0, 0);
	checkboxSizer->Add(stderrBox, 1, 0, 0);
	checkboxSizer->Add(p, 1, 0, 0);
	checkboxSizer->Add(qe, 1, 0, 0);
	checkboxSizer->Add(deflight, 1, 0, 0);
	outputsSizer->Add(simulationOutputs, 0, wxALL, 5);
	outputsSizer->Add(staticLine, 0, wxEXPAND | wxALL, 0);
	outputsSizer->Add(checkboxSizer, 1, wxEXPAND | wxALL, 5);
	outputsSizer->Add(buttonsSizer, 0, wxALL | wxCENTER, 5);
	SetSizerAndFit(outputsSizer);
}

bool SelectGraphDialog::TransferDataToWindow()
{
	if(!parent) return false;

	Ef->SetValue(parent->whichData.Ef);
	Efn->SetValue(parent->whichData.Efn);
	Efp->SetValue(parent->whichData.Efp);
	Ec->SetValue(parent->whichData.Ec);
	Ev->SetValue(parent->whichData.Ev);
	Psi->SetValue(parent->whichData.Psi);
	electrons->SetValue(parent->whichData.electrons);
	holes->SetValue(parent->whichData.holes);
	impCharge->SetValue(parent->whichData.impCharge);
	n->SetValue(parent->whichData.n);
	p->SetValue(parent->whichData.p);
	deflight->SetValue(parent->whichData.deflight);
	condlight->SetValue(parent->whichData.condlight);
	genlight->SetValue(parent->whichData.genlight);
	genthermal->SetValue(parent->whichData.genthermal);
	recthermal->SetValue(parent->whichData.recthermal);
	srh->SetValue(parent->whichData.srh1);
	srh->SetValue(parent->whichData.srh2);
	srh->SetValue(parent->whichData.srh3);
	srh->SetValue(parent->whichData.srh4);
	rho->SetValue(parent->whichData.rho);
	Jn->SetValue(parent->whichData.Jn);
	Jp->SetValue(parent->whichData.Jp);
	stddev->SetValue(parent->whichData.stddev);
	stderrBox->SetValue(parent->whichData.stderrBox);
	qe->SetValue(parent->whichData.qe);

	return true;
}

bool SelectGraphDialog::TransferDataFromWindow()
{
	if(!parent) return false;

	parent->whichData.Ef = Ef->GetValue();
	parent->whichData.Efn = Efn->GetValue();
	parent->whichData.Efp = Efp->GetValue();
	parent->whichData.Ec = Ec->GetValue();
	parent->whichData.Ev = Ev->GetValue();
	parent->whichData.Psi = Psi->GetValue();
	parent->whichData.electrons = electrons->GetValue();
	parent->whichData.holes = holes->GetValue();
	parent->whichData.impCharge = impCharge->GetValue();
	parent->whichData.n = n->GetValue();
	parent->whichData.p = p->GetValue();
	parent->whichData.deflight = deflight->GetValue();
	parent->whichData.genlight = genlight->GetValue();
	parent->whichData.genthermal = genthermal->GetValue();
	parent->whichData.recthermal = recthermal->GetValue();
	parent->whichData.srh1 = srh->GetValue();
	parent->whichData.srh2 = srh->GetValue();
	parent->whichData.srh3 = srh->GetValue();
	parent->whichData.srh4 = srh->GetValue();
	parent->whichData.rho = rho->GetValue();
	parent->whichData.Jn = Jn->GetValue();
	parent->whichData.Jp = Jp->GetValue();
	parent->whichData.stddev = stddev->GetValue();
	parent->whichData.stderrBox = stderrBox->GetValue();
	parent->whichData.qe = qe->GetValue();
	
	return true;
}

GraphPickerDialog::GraphPickerDialog(resultsPanel* result)
: wxDialog(result, wxID_ANY, wxT("Select Colors"))
{
	parent = result;
	size = new wxBoxSizer(wxHORIZONTAL);
	dialogPicker = new wxComboBox(this, wxID_ANY, wxT("Choice 1"), wxDefaultPosition, wxDefaultSize, wxArrayString(), wxCB_DROPDOWN | wxCB_READONLY);
	if(result->whichData.Psi) dialogPicker->Append(wxT("Psi"));
	if(result->whichData.Ec) dialogPicker->Append(wxT("Ec"));
	if(result->whichData.Ev) dialogPicker->Append(wxT("Ev"));
	if(result->whichData.Ef) dialogPicker->Append(wxT("Ef"));
	if(result->whichData.Efn) dialogPicker->Append(wxT("Efn"));
	if(result->whichData.Efp) dialogPicker->Append(wxT("Efp"));
	if(result->whichData.n) dialogPicker->Append(wxT("n"));
	if(result->whichData.p) dialogPicker->Append(wxT("p"));
	if(result->whichData.rho) dialogPicker->Append(wxT("rho"));
	if(result->whichData.electrons) dialogPicker->Append(wxT("electrons"));
	if(result->whichData.holes) dialogPicker->Append(wxT("holes"));
	if(result->whichData.impCharge) dialogPicker->Append(wxT("impCharge"));
	if(result->whichData.deflight) dialogPicker->Append(wxT("deflight"));
	if(result->whichData.condlight) dialogPicker->Append(wxT("condlight"));
	if(result->whichData.genlight) dialogPicker->Append(wxT("genlight"));
	if(result->whichData.genthermal) dialogPicker->Append(wxT("genthermal"));
	if(result->whichData.recthermal) dialogPicker->Append(wxT("recthermal"));
	if(result->whichData.srh1) dialogPicker->Append(wxT("srh"));
	if(result->whichData.Jn) dialogPicker->Append(wxT("Jn"));
	if(result->whichData.Jp) dialogPicker->Append(wxT("Jp"));
	if(result->whichData.stddev) dialogPicker->Append(wxT("stddev"));
	if(result->whichData.stderrBox) dialogPicker->Append(wxT("stderrBox"));
	if(result->whichData.qe) dialogPicker->Append(wxT("qe"));
	
	int numElems = dialogPicker->GetCount();
	if(numElems >= 0) dialogPicker->SetSelection(numElems);
	changeButton = new wxButton(this, OPEN_COLOR_DIAG, wxT("Open Color Dialog"));
	wxArrayString styles;
	styles.Add(wxT("Solid"));
	styles.Add(wxT("Dots"));
	styles.Add(wxT("Long Dashes"));
	styles.Add(wxT("Short Dashes"));
	styles.Add(wxT("Dots and Dashes"));
	stylePicker = new wxComboBox(this, PEN_STYLE, wxT("Solid"), wxDefaultPosition, wxDefaultSize, styles, wxCB_DROPDOWN| wxCB_READONLY);
	sample = new SampleLine(this);

	size->Add(dialogPicker, 0, wxALL, 5);
	size->Add(changeButton, 0, wxALL, 5);
	size->Add(stylePicker, 0, wxALL, 5);
	size->Add(sample, 0, wxALL, 5);

	SetSizerAndFit(size);

	prevSelection = -1;
}

GraphPickerDialog::~GraphPickerDialog()
{
	parent = NULL;
	
	delete dialogPicker;
	delete stylePicker;
	delete sample;
	delete changeButton;
}

void GraphPickerDialog::changeColor(wxCommandEvent& event)
{
	int selection = dialogPicker->GetSelection();
	if(selection < 0) return;
	wxColourData* colors = new wxColourData();
	if(curPen.IsOk())
		colors->SetColour(curPen.GetColour());
	else
	{
		colors->SetColour("black");
	}
	wxColourDialog colorDiag(this, colors);
	if(colorDiag.ShowModal() == wxID_CANCEL) return;
	curPen.SetColour(colorDiag.GetColourData().GetColour());
	sample->UpdatePen(curPen);
	parent->plot->GetLayer(2+selection)->SetPen(curPen);
	parent->plot->UpdateAll();
	delete colors;
}

void GraphPickerDialog::changeStyle(wxCommandEvent& event)
{
	int selection = dialogPicker->GetSelection();
	if(selection < 0) return;
	switch(stylePicker->GetSelection())
	{
		case 0:
			curPen.SetStyle(wxPENSTYLE_SOLID);
			break;
		case 1:
			curPen.SetStyle(wxPENSTYLE_DOT);
			break;
		case 2:
			curPen.SetStyle(wxPENSTYLE_LONG_DASH);
			break;
		case 3:
			curPen.SetStyle(wxPENSTYLE_SHORT_DASH);
			break;
		case 4:
			curPen.SetStyle(wxPENSTYLE_DOT_DASH);
			break;
		case 5:
			curPen.SetStyle(wxPENSTYLE_TRANSPARENT);
			break;
	}
	sample->UpdatePen(curPen);
	parent->plot->GetLayer(2+selection)->SetPen(curPen);
	parent->plot->UpdateAll();
}

void GraphPickerDialog::changePick(wxCommandEvent& event)
{
	//save previous pen
	parent->plot->GetLayer(2+prevSelection)->SetPen(curPen);

	int selection = dialogPicker->GetSelection();
	if(selection < 0) return;
	curPen = parent->plot->GetLayer(2+selection)->GetPen();
	sample->UpdatePen(curPen);
	switch(curPen.GetStyle())
	{
		case wxPENSTYLE_SOLID:
			stylePicker->SetSelection(0);
		break;
		case wxPENSTYLE_DOT:
			stylePicker->SetSelection(1);
		break;
		case wxPENSTYLE_LONG_DASH:
			stylePicker->SetSelection(2);
		break;
		case wxPENSTYLE_SHORT_DASH:
			stylePicker->SetSelection(3);
		break;
		case wxPENSTYLE_DOT_DASH:
			stylePicker->SetSelection(4);
		break;
		default:
			stylePicker->SetSelection(-1);
		break;
	}
	prevSelection = selection;
}

BEGIN_EVENT_TABLE(GraphPickerDialog, wxDialog)
EVT_BUTTON(OPEN_COLOR_DIAG, GraphPickerDialog::changeColor)
EVT_COMBOBOX(PEN_STYLE, GraphPickerDialog::changeStyle)
EVT_COMBOBOX(SELECT_PEN, GraphPickerDialog::changePick)
END_EVENT_TABLE()
*/
SampleLine::SampleLine(wxWindow* parent)
: wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(80, 30))
{
	pen = *wxBLACK_PEN;
	paintNow();
}

void SampleLine::UpdatePen(wxPen& newPen)
{
	pen = newPen;
	paintNow();
}

void SampleLine::paintEvent(wxPaintEvent& evt)
{
	wxPaintDC dc(this);
	render(dc);
}

void SampleLine::paintNow()
{
	wxClientDC dc(this);
	render(dc);
}

void SampleLine::render(wxDC& dc)
{
	dc.Clear();
	dc.SetPen(pen);
	dc.DrawLine(10, 15, 70, 15);
}

BEGIN_EVENT_TABLE(SampleLine, wxPanel)
EVT_PAINT(SampleLine::paintEvent)
END_EVENT_TABLE()

CreditsDialog::CreditsDialog(wxWindow* parent, wxString name) : wxDialog(parent, wxID_ANY, name)
{
	topSizer = new wxBoxSizer(wxVERTICAL);

	wxString genSupport = "\
This application developed by Dan Heinzel under the guidance of Prof. Angus Rockett at the University of Illinois: Urbana-Champaign.\n\n\
This software is provided AS IS without warranty of any kind and in no event shall Dan Heinzel, Angus Rockett, or the University of Illinois be liable for any damages arising out of or in connection with this software.\n\n\
That being said, this software aims to achieve the most accurate results possible, and if any issues arise, please notify us.\n\
This application is currently maintained by Dan Heinzel.\n\n";

	wxString tifSupport = "\
--------------TIF SUPPORT---------------------\n\
Copyright(c) 1988 - 1997 Sam Leffler\n\
Copyright(c) 1991 - 1997 Silicon Graphics, Inc.\n\
\n\
Permission to use, copy, modify, distribute, and sell this software and its documentation for any purpose is hereby granted without fee, provided that(i) the above copyright notices and this permission notice appear in all copies of the software and related documentation, and(ii) the names of Sam Leffler and Silicon Graphics may not be used in any advertising or publicity relating to the software without the specific, prior written permission of Sam Leffler and Silicon Graphics.\n\n\
THE SOFTWARE IS PROVIDED \"AS-IS\" AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.\n\n\
IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.";

	wxString jpgSupprt = "\n\n\
---------------JPEG SUPPORT---------------------\n\
Provided by the Independent JPEG Group: copyright (C) 1991-1998, Thomas G. Lane\n";

	wxString mathplotSupport = "\n\n\
----------------PLOTTING---------------------\n\
Plotting was originally developed via wxMathPlot and made available via the wxWindows license, with modifications made to work with SAAPD.\n\
https://sourceforge.net/projects/wxmathplot \n\
Authors and contributors include: David Schalig, Davide Rondini, Jose Luis Blanco, and Val Greene\
";

	wxString licenses = genSupport + tifSupport + jpgSupprt + mathplotSupport;
	licensing = new wxTextCtrl(this, wxID_ANY, licenses, wxDefaultPosition, wxSize(390,190), wxTE_READONLY | wxTE_MULTILINE | wxTE_AUTO_URL);

	topSizer->Add(licensing, 1, wxEXPAND | wxALL, 5);

	SetSizerAndFit(topSizer);

}

void CreditsDialog::CloseWindow(wxCloseEvent& event) {
	Destroy();
}

CreditsDialog::~CreditsDialog() {

}

BEGIN_EVENT_TABLE(CreditsDialog, wxDialog)
EVT_CLOSE(CreditsDialog::CloseWindow)
END_EVENT_TABLE()

SimulationProgressDialog::SimulationProgressDialog(wxWindow* parent, wxString name, int range, wxString filename)
: wxDialog(parent, wxID_ANY, wxT("Simulation of ") + name + wxT(" in progress"))
{
	p=parent;
	topSizer = new wxBoxSizer(wxVERTICAL);
	buttonSizer = new wxBoxSizer(wxVERTICAL);
	buttonSizer1 = new wxBoxSizer(wxHORIZONTAL);
	buttonSizer2 = new wxBoxSizer(wxHORIZONTAL);
	horizSizer = new wxBoxSizer(wxHORIZONTAL);
	textSizer = new wxBoxSizer(wxVERTICAL);
	info = new wxStaticText(this, wxID_ANY, wxT("Initializing simulation..."));
	shortInfo = new wxStaticText(this, wxID_ANY, wxT(""));
	infoLow1 = new wxStaticText(this, wxID_ANY, wxT("                                                        "));
	infoLow2 = new wxStaticText(this, wxID_ANY, wxT("                                                        "));
	infoLow3 = new wxStaticText(this, wxID_ANY, wxT("                                                        "));
	infoLow4 = new wxStaticText(this, wxID_ANY, wxT("                                                        "));
	taskTime = new wxStaticText(this, wxID_ANY, wxT(""));

	shortProgBar = new wxGauge(this, wxID_ANY, 1);
	progressBar = new wxGauge(this, wxID_ANY, range);
	pause = new wxButton(this, PAUSE, wxT("Pause"));
	stop = new wxButton(this, STOP, wxT("Stop"));
	abortButton = new wxButton(this, ABORT, wxT("Abort"));
	saveStateButton = new wxButton(this, SAVE_STATE, wxT("Save State"));
	nextDataSetButton = new wxButton(this, NEXT_STATE, wxT("Next Data Set"));
	time = NULL;	//just be safe

	buttonSizer1->Add(saveStateButton, 0, wxALL, 5);
	buttonSizer1->Add(nextDataSetButton, 0, wxALL, 5);
	buttonSizer2->Add(pause, 0, wxALL, 5);
	buttonSizer2->Add(stop, 0, wxALL, 5);
	buttonSizer->Add(buttonSizer1, 0, wxALL, 2);
	buttonSizer->Add(buttonSizer2, 0, wxALL, 2);
	buttonSizer->Add(abortButton, 0, wxALL | wxALIGN_CENTER, 2);
	
	
	textSizer->Add(infoLow1, 0, wxALIGN_LEFT | wxEXPAND, 2);
	textSizer->Add(infoLow2, 0, wxALIGN_LEFT | wxEXPAND, 2);
	textSizer->Add(infoLow3, 0, wxALIGN_LEFT | wxEXPAND, 2);
	textSizer->Add(infoLow4, 0, wxALIGN_LEFT | wxEXPAND, 2);

	horizSizer->Add(textSizer, 0, wxALL | wxEXPAND, 5);
	horizSizer->Add(buttonSizer, 0, wxALL, 5);

	topSizer->Add(info, 0, wxALL | wxALIGN_CENTER | wxEXPAND, 2);
	topSizer->Add(progressBar, 0, wxALL | wxALIGN_CENTER, 5);
	topSizer->Add(shortInfo, 0, wxALL | wxALIGN_CENTER | wxEXPAND, 2);
	topSizer->Add(shortProgBar, 0, wxALL | wxALIGN_CENTER, 2);
	topSizer->Add(taskTime, 0, wxALL | wxALIGN_CENTER | wxEXPAND, 2);
	topSizer->Add(horizSizer, 0, wxALIGN_CENTER, 3);
	

	SetSizerAndFit(topSizer);

	wxString wd = wxGetCwd();
	simulation = new kernelThread(filename, parent, wd);	//it uses the parent... for some weird reason, which is mainFrame
	if ( simulation->Create() != wxTHREAD_NO_ERROR ) {
		wxLogError(wxT("Can�t create thread!"));
		return;
	}
	simulation->Run();
	paused = false;
	

}

SimulationProgressDialog::~SimulationProgressDialog()
{
	/*
	buttonSizer->Clear();
	delete buttonSizer;
	buttonSizer = NULL;
	These are cleared out by topSizer->Clear() as those are inside of topSizer
	*/
//	topSizer->Clear();
	if(simulation)	//only query it if it is not null
	{
		if(simulation->IsAlive())
		{
			simulation->shareData.GUIabortSimulation = true;	//this should kill it pretty quick mid calculation
		}
		simulation->Wait();	//now wait for it to end as it was given signal to abort
		delete simulation;
		simulation = NULL;
	}
//	delete topSizer;

	delete info;
	delete progressBar;
	delete shortProgBar;
	delete shortInfo;
	delete pause;
	delete time;
	delete stop;
	delete abortButton;
	delete saveStateButton;
	delete nextDataSetButton;
	delete infoLow1;
	delete infoLow2;
	delete infoLow3;
	delete infoLow4;
	delete taskTime;
	p=NULL;
	

	//if the dialog box is closed, tell the simulation to stop.
	//DO NOT DELETE/NULL KERNEL_THREAD (simulation). It will totally destroy the simulation
	//note, kernel_Thread is passed in from a different function. It is not created here.

	
}


void SimulationProgressDialog::Update(int value)
{
	if (!isInitialized())
		return;
	//progressBar->SetValue(value);
	//if(value >= progressBar->GetRange())
	//{
	if(simulation->shareData.GUIsaveState)
		saveStateButton->Disable();
	else
		saveStateButton->Enable();

	if(simulation->shareData.ss)
	{
		progressBar->SetRange(simulation->shareData.KernelssnumStates);
		progressBar->SetValue(simulation->shareData.KernelsscurState);
		if(simulation->shareData.KernelTargetTolerance > 0.0)
			infoLow1->SetLabelText(wxString::Format(wxT("Target Tolerance: %.2e"), simulation->shareData.KernelTargetTolerance));
		else
			infoLow1->SetLabelText(wxT("Target Tolerance: N/A"));

		if(simulation->shareData.KernelTolerance > 0.0)
			infoLow2->SetLabelText(wxString::Format(wxT("Current Tolerance: %.2e"), simulation->shareData.KernelTolerance));
		else
			infoLow2->SetLabelText(wxT("Current Tolerance: N/A"));

		if(simulation->shareData.KernelMaxChange > 0.0)
			infoLow3->SetLabelText(wxString::Format(wxT("Maximum Change: %.2e"), simulation->shareData.KernelMaxChange));
		else
			infoLow3->SetLabelText(wxT("Maximum Change: N/A"));

		if(simulation->shareData.KernelshortProgBarPosition > 0)
			infoLow4->SetLabelText(wxString::Format(wxT("Progress Bar Value: %d / %d"), simulation->shareData.KernelshortProgBarPosition, simulation->shareData.KernelshortProgBarRange));
		else
			infoLow4->SetLabelText(wxT("Progress Bar Value: N/A"));

		SetLabel(wxString::Format(wxT("Current Simulation: %s"), simulation->shareData.CurSim.c_str()));
		if(simulation->shareData.KernelDiverge==false)
			info->SetLabel(wxString::Format(wxT("Current Simulation: %s"), simulation->shareData.CurSim.c_str()));
		else
			info->SetLabelText(wxT("Simulation not converging. Slow & safer convergence algorithm implemented"));
		info->Center(wxHORIZONTAL);
		info->Update();
		infoLow2->Update();
		infoLow3->Update();
		infoLow4->Update();
		nextDataSetButton->Enable();
		if(simulation->shareData.KernelsscurState == simulation->shareData.KernelssnumStates && simulation->shareData.ssFromTransient==false)
		{
			info->SetLabel(wxT("Simulation finished"));
			Disable();
			Close();
		}

		switch(simulation->shareData.KernelcurInfoMsg)
		{
			case MSG_THERMALEQUILIBRIUM:
				shortInfo->SetLabelText(wxT("Calculating thermal equilibrium guess"));
				break;
			case MSG_THEQ_REFINE:
				shortInfo->SetLabelText(wxT("Refining thermal equilibrium guess"));
				break;
			case MSG_TOLERANCE:
				shortInfo->SetLabelText(wxT("Approximating rough solution"));
				break;
			case MSG_CALCJACOBIAN:
				shortInfo->SetLabelText(wxT("Calculating Jacobian Matrix Elements"));
				break;
			case MSG_PROCESSJACOBIAN:
				shortInfo->SetLabelText(wxT("Solving Jacobian Matrix"));
				break;
			case MSG_PROCESSJACOBIANFULL:
				shortInfo->SetLabelText(wxT("Solving Full Jacobian Matrix"));
				break;
			case MSG_LIGHTSPECTRUM:
				shortInfo->SetLabelText(wxT("Processing light spectrum"));
				break;
			case MSG_LIGHTEMISSIONS:
				shortInfo->SetLabelText(wxT("Processing light emissions"));
				break;
			case MSG_MONTECARLO:
				shortInfo->SetLabelText(wxT("Acquiring Monte Carlo Statistics"));
				break;
			case MSG_MONTECARLO2:
				shortInfo->SetLabelText(wxT("Converging Monte Carlo Statistics"));
				break;
			case MSG_SS_TRANS:
				shortInfo->SetLabelText(wxT("Approximating via transient"));
				break;
			case MSG_MEMORYCLEANUP:
				shortInfo->SetLabelText(wxT("Cleaning up memory"));
				break;
			case MSG_STOPPED:
				shortInfo->SetLabelText(wxT("Simulation Stopped. Generating Output data"));
				break;
			case MSG_GENOUTPUT:
				shortInfo->SetLabelText(wxT("Generating output files"));
				break;
			default:
				shortInfo->SetLabelText(wxT(""));
		}

		shortProgBar->Show();
		shortProgBar->SetRange(simulation->shareData.KernelshortProgBarRange);
		shortProgBar->SetValue(simulation->shareData.KernelshortProgBarPosition);
		if(simulation->shareData.KernelTimeLeft > 0.0)
		{
			double t = simulation->shareData.KernelTimeLeft;
			int days = int(floor(t/86400.0));
			t = (t - double(days)*86400.0);
			int hours = int(floor(t/3600.0));
			t=(t - 3600.0*double(hours));
			int minutes = int(floor(t/60.0));
			int seconds = int(ceil(t-60.0*double(minutes)));
			if(seconds==60)	//the ceil might cause it to convert 59.### = 60
			{
				minutes++;
				seconds=0;
			}
			wxString BuildStr="";
			if(days>0.0)
				taskTime->SetLabelText(wxString::Format(wxT("ETA on Task: %d:%02d:%02d:%02d"), days, hours, minutes, seconds));
			else if(hours>0.0)
				taskTime->SetLabelText(wxString::Format(wxT("ETA on Task: %d:%02d:%02d"), hours, minutes, seconds));
			else if(minutes>0.0)
				taskTime->SetLabelText(wxString::Format(wxT("ETA on Task: %d:%02d"), minutes, seconds));
			else if(seconds>0.0)
				taskTime->SetLabelText(wxString::Format(wxT("ETA on Task: %02d"),seconds));
		}
		else
			taskTime->SetLabelText(wxT(""));
		
		shortInfo->CenterOnParent(wxHORIZONTAL);
		taskTime->CenterOnParent(wxHORIZONTAL);
		shortInfo->Update();
		taskTime->Update();
	}
	else
	{

		double percent = simulation->shareData.KernelsimTime / simulation->shareData.KernelEndTime;
		int intpercent = int(percent * 10000);	//go to 0.01%...
		progressBar->SetRange(10000);
		progressBar->SetValue(intpercent);
		nextDataSetButton->Disable();

		ValueToTBox(shortInfo, "");
		shortProgBar->Hide();

		if(simulation->shareData.KernelsimTime >= simulation->shareData.KernelEndTime
			|| simulation->shareData.GUIabortSimulation || simulation->shareData.GUIstopSimulation)
		{
			info->SetLabel(wxT("Simulation done, cleaning up..."));
			Disable();
			Close();
		}
		else
		{
			info->SetLabelText(wxT("Simulation Running..."));
			infoLow1->SetLabelText(wxString::Format(wxT("Time: %.8e"), simulation->shareData.KernelsimTime));
			infoLow2->SetLabelText(wxString::Format(wxT("Next Target Time: %.2e"), simulation->shareData.KernelnextTimeTarget));
			infoLow3->SetLabelText(wxString::Format(wxT("Current Tolerance: %.2e"), simulation->shareData.KernelMaxTolerance));
			infoLow3->SetToolTip("The tolerance and time step are used to cut out fast rates, with the tolerance defining the allowed fermi level change.");
			infoLow4->SetLabelText(wxString::Format(wxT("Max Time Step: %.2e"), simulation->shareData.KernelMaxTStep));
			infoLow4->SetToolTip("Note, simulation does a maximum time step, and then shorter iterations to correct any resulting over adjustments.");
			
			info->CenterOnParent(wxHORIZONTAL);
		}
	}
}

void SimulationProgressDialog::Pause(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if(paused)
	{
		simulation->Resume();
		pause->SetLabel("Pause");
		info->SetLabel(wxT("Resuming Simulation"));
	}
	else
	{
		simulation->Pause();
		pause->SetLabel("Resume");
		info->SetLabel(wxT("Simulation is paused"));
	}
	paused = !paused;
}

void SimulationProgressDialog::Stop(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	simulation->shareData.GUIstopSimulation = true;
//	simulation->Delete(NULL, wxTHREAD_WAIT_BLOCK);	//this function will not work. Thread is joinable, not detached.
//	wxMessageDialog diag(this, wxT("Simulation successfully stopped"));
//	diag.ShowModal();
}

void SimulationProgressDialog::Abort(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	simulation->shareData.GUIabortSimulation = true;
//	simulation->Kill();		//not sure why this one does not work...
//	wxMessageDialog diag(this, wxT("Simulation aborted"));
//	diag.ShowModal();
}

void SimulationProgressDialog::SaveState(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	simulation->shareData.GUIsaveState = true;
	saveStateButton->Disable();
}

void SimulationProgressDialog::NextState(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	simulation->shareData.GUINext = true;
}

void SimulationProgressDialog::OnClose(wxCloseEvent& event)
{
//	wxCommandEvent evt(wxEVT_COMMAND_TEXT_UPDATED, SIM_FINISHED);	//a flag to say it's 'done'
	if(simulation)
		simulation->shareData.GUIabortSimulation = true;	//make sure it finishes quickly

//	p->GetEventHandler()->AddPendingEvent(evt);	it shouldn't need the event. The kernel should just exit!

}

BEGIN_EVENT_TABLE(SimulationProgressDialog, wxDialog)
EVT_BUTTON(PAUSE, SimulationProgressDialog::Pause)
EVT_BUTTON(STOP, SimulationProgressDialog::Stop)
EVT_BUTTON(ABORT, SimulationProgressDialog::Abort)
EVT_BUTTON(SAVE_STATE, SimulationProgressDialog::SaveState)
EVT_BUTTON(NEXT_STATE, SimulationProgressDialog::NextState)
EVT_CLOSE(SimulationProgressDialog::OnClose)
END_EVENT_TABLE()