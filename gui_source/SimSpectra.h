#ifndef SIM_SPECTRA_H
#define SIM_SPECTRA_H


#include "wx/wxprec.h"
#include "wx/panel.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif



class wxTextCtrl;
class wxWindow;
class wxStaticText;
class ModelDescribe;
class wxSizer;
class wxFlexGridSizer;
class wxNotebook;
class wCcommandEvent;
class Simulation;
class LightSource;
class PlotWindow;

class simSpectraPanel : public wxPanel
{
	wxButton *btnNew, *btnSave, *btnLoad, *btnRemove, *btnDuplicate, *btnRename;

	wxButton *btnAM0, *btnAM15;

	wxButton *btnAddWave, *btnRemoveWave, *btnClearWaves;

	wxTextCtrl *txtIntensity, *txtDirX, *txtDirY, *txtDirZ, *txtWavelength;
	wxStaticText *stIntensity, *stX, *stY, *stZ, *stWavelength, *stDir;

	wxComboBox *cbSpectraChooser;

	wxRadioBox *rbInputType;

	wxGrid *grdDataTable;

	wxBoxSizer *bxszrTopRow, *bxszrAMRow, *bxszrLColumn, *bxszrDataPlot, *bxszrRemoveClear;

	wxBoxSizer *bxszrWaveIntensCol, *bxszrWaveIntensRow;

	wxFlexGridSizer *fgszrDirection;// , *fgszrWaveIntensity;

	PlotWindow *plotSpectra;

	bool isInitialized() { return (bxszrWaveIntensRow != nullptr); }
	void UpdateWavelengthList();
public:
	simSpectraPanel(wxWindow* parent);
	~simSpectraPanel();

	void SizeRight();
	simulationWindow* getMyClassParent();
	ModelDescribe* getMdesc();
	
	void TransferToGUI();
	void TransferToMdesc(wxCommandEvent& event);
	void UpdateGUIValues(wxCommandEvent& event);

	void CreateSpectra(wxCommandEvent& event);
	void SaveSpectra(wxCommandEvent& event);
	void LoadSpectra(wxCommandEvent& event);
	void RemoveSpectra(wxCommandEvent& event);
	void DuplicateSpectra(wxCommandEvent& event);
	void RenameSpectra(wxCommandEvent& event);
	void AddWavelength(wxCommandEvent& event);
	void RemoveWavelength(wxCommandEvent& event);
	void ClearWavelengths(wxCommandEvent& event);

	void SwitchSpectra(wxCommandEvent& event);
	
	void RadBoxModifed(wxCommandEvent& event);
	void DirectionTextModified(wxCommandEvent& event);
	void GridClick(wxGridEvent& event);
	void GridText(wxGridEvent& event);
	void SetAM0(wxCommandEvent& event);
	void SetAM15(wxCommandEvent& event);

	void VerifyInternalVars();

	Simulation* getSim();
	
private:
	int eraseRow;
	float prevWave;
	double prevIntensity;
	LightSource *curLightSource;
	wxSizer* szrPrimary;
	DECLARE_EVENT_TABLE()	//wx event handling macro
};

#endif