#include "GUIHelper.h"

#include<vector>

#include<wx/grid.h>

void SmartResizeGridColumns(wxGrid* grid)
{
	if (grid == nullptr)
		return;
	int numCols = grid->GetNumberCols();
	if (numCols == 0)
		return;	//also pointless

	int totWidth = grid->GetSize().GetWidth();
	int RowLabelWidth = grid->GetRowLabelSize();
	int sBarWidth = wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
	std::vector<int>MinWidths;
	std::vector<bool>set;
	std::vector<int>setWidths;
	int widthAvail = totWidth - RowLabelWidth - sBarWidth;

	grid->AutoSizeColumns();	//shrinks everything down to a minimum
	int totColWidth = 0;
	for (int i = 0; i < numCols; ++i) {
		MinWidths.push_back(grid->GetColumnWidth(i));
		totColWidth += MinWidths[i];
		set.push_back(false);
		setWidths.push_back(0);
	}
	if (totColWidth >= widthAvail)
		return;	//can't do anything about it! As good as we're gonna get
	int AvgWidth = widthAvail / numCols;	//this is our target with for all the columns

	//go through and get all the big boys done.
	int adjWidthAvail = widthAvail;
	int colLeft = numCols;
	int newAvgWidth = AvgWidth;
	bool modified = false;
	do {
		modified = false;
		for (int i = 0; i < numCols; ++i) {
			if (!set[i]) {
				if (MinWidths[i] > newAvgWidth) {
					modified = true;
					set[i] = true;
					setWidths[i] = MinWidths[i];
					adjWidthAvail -= setWidths[i];
					colLeft--;
					newAvgWidth = colLeft != 0 ? adjWidthAvail / colLeft : 0;
				}
			}
		}
	} while (modified && colLeft > 0);

	//presumably at this point, we now have an average width which is greater than all the present minWidths.
	//want those all to have room to grow, so assign 'em
	//now what we want

	for (int i = 0; i < numCols; ++i) {
		if (!set[i])
			grid->SetColumnWidth(i, newAvgWidth);
		else
			grid->SetColumnWidth(i, setWidths[i]);
	}
}