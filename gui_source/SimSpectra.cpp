#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"
#include "myPlottingWrapper.h"

#include "SimulationWindow.h"
#include "SimSpectra.h"

#include "../kernel_source/light.h"
#include "../kernel_source/physics.h"
#include "../kernel_source/helperfunctions.h"
#include "../kernel_source/model.h"
#include "../kernel_source/load.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif


simSpectraPanel::simSpectraPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY), bxszrWaveIntensRow(nullptr)
{

	wxArrayString emptyList;
	cbSpectraChooser = new wxComboBox(this, SPEC_CHOOSE, "Spectra", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);

	btnNew = new wxButton(this, SPEC_NEW, "New Spectra");
	btnRemove = new wxButton(this, SPEC_REMOVE, "Remove");
	btnSave = new wxButton(this, SPEC_SAVE, "Save");
	btnLoad = new wxButton(this, SPEC_LOAD, "Load");
	btnDuplicate = new wxButton(this, SPEC_DUPLICATE, "Duplicate");
	btnRename = new wxButton(this, SPEC_RENAME, "Rename");

	btnAM0 = new wxButton(this, SPEC_AM0, "AM 0", wxDefaultPosition, wxSize(50, -1));
	btnAM0->SetToolTip("Replace the current spectra with AM 0 conditions (outer space). Data downloaded from NREL.");
	btnAM15 = new wxButton(this, SPEC_AM15, "AM 1.5", wxDefaultPosition, wxSize(50, -1));
	btnAM15->SetToolTip("Replace the current spectra with AM 1.5 direct normal conditions (typical sunlight conditions). Data downloaded from NREL.");

	txtDirX = new wxTextCtrl(this, SPEC_DIR_TEXT, wxT("1.0"), wxDefaultPosition, wxSize(50, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	txtDirY = new wxTextCtrl(this, SPEC_DIR_TEXT, wxT("0.0"), wxDefaultPosition, wxSize(50, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	txtDirZ = new wxTextCtrl(this, SPEC_DIR_TEXT, wxT("0.0"), wxDefaultPosition, wxSize(50, -1), 0, wxTextValidator(wxFILTER_NUMERIC));

	wxArrayString unitChoices;
	unitChoices.Add(wxT("mW cm\u207B\u00B2 nm\u207B\u00B9"));
	unitChoices.Add(wxT("mW cm\u207B\u00B2"));
	rbInputType = new wxRadioBox(this, SPEC_RAD, "Intensity Units", wxDefaultPosition, wxDefaultSize, unitChoices, 1, wxRA_SPECIFY_COLS);
	rbInputType->SetToolTip("Should the spectra data be treated as intensity or an intensity normalized with respect to the wavelength?");

	txtWavelength = new wxTextCtrl(this, SPEC_ADD_TEXT, wxT(""), wxDefaultPosition, wxSize(75, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	txtIntensity = new wxTextCtrl(this, SPEC_ADD_TEXT, wxT(""), wxDefaultPosition, wxSize(75, -1), 0, wxTextValidator(wxFILTER_NUMERIC));
	

	btnAddWave = new wxButton(this, SPEC_ADD_WAVE, "Add Wavelength");
	btnAddWave->SetToolTip(wxT("Add (or modify) a wavelength into the current spectra"));

	btnRemoveWave = new wxButton(this, SPEC_REM_WAVE, "Remove");
	btnRemoveWave->SetToolTip(wxT("Remove the last selected wavelength from the spectra."));
	btnClearWaves = new wxButton(this, SPEC_CLEAR_WAVES, "Clear");
	btnClearWaves->SetToolTip(wxT("Clear all wavelengths from the spectra"));

	grdDataTable = new wxGrid(this, SPEC_GRID);
	grdDataTable->CreateGrid(0, 3);
	grdDataTable->SetRowLabelSize(40);
	grdDataTable->SetColMinimalAcceptableWidth(50);
	grdDataTable->SetColLabelValue(0, "Wavelength");
	grdDataTable->SetColLabelValue(1, "Energy");
	grdDataTable->SetColLabelValue(2, "Intensity");
	

	stDir = new wxStaticText(this, wxID_ANY, "Direction");
	stDir->SetToolTip(wxT("What direction is the light travelling? For 1D, x\u22600 or the light will never hit the device!"));
	stX = new wxStaticText(this, wxID_ANY, "X");
	stY = new wxStaticText(this, wxID_ANY, "Y");
	stZ = new wxStaticText(this, wxID_ANY, "Z");


	stIntensity = new wxStaticText(this, wxID_ANY, "Intensity");
	stIntensity->SetToolTip("Units are described to the left. Is this data actual values, or is it the derivative with respect to wavelength?");
	
	stWavelength = new wxStaticText(this, wxID_ANY, "Wavelength (nm)");
	stWavelength->SetToolTip("Don't forget! Under calculations there is an optical resolution. While the data is piece-wise integrated, the most efficient results will occur if the energy differences align with the resolution.");
	
	

	plotSpectra = new PlotWindow(this, wxID_ANY);
	plotSpectra->SetTitle("Spectra");
	plotSpectra->SetXAxisName("Wavelength (nm)");
	plotSpectra->SetYAxisName("Intensity");
	LineData* tmp = plotSpectra->CreateData(new LineData("Wavelength (nm)"));
	tmp->SetAsActiveX();
	tmp = plotSpectra->CreateData(new LineData("Energy (eV)"));
	tmp->SetAsX();	//have it there, but don't plot it. Mark it for future x values if desired
	tmp = plotSpectra->CreateData(new LineData("Intensity"));
	tmp->SetAsActiveY();
	plotSpectra->HideLegend();	//these just get in the way.
	plotSpectra->HideLabel();	//we know what it is. There's only one thing plotted!

	fgszrDirection = new wxFlexGridSizer(4, 5, 1);
//	fgszrWaveIntensity = new wxFlexGridSizer(2, 3, 5);
	bxszrTopRow = new wxBoxSizer(wxHORIZONTAL);
	bxszrAMRow = new wxBoxSizer(wxHORIZONTAL);
	bxszrDataPlot = new wxBoxSizer(wxHORIZONTAL);
	bxszrLColumn = new wxBoxSizer(wxVERTICAL);
	szrPrimary = new wxBoxSizer(wxVERTICAL);
	bxszrRemoveClear = new wxBoxSizer(wxHORIZONTAL);

	bxszrWaveIntensCol = new wxBoxSizer(wxVERTICAL);
	bxszrWaveIntensRow = new wxBoxSizer(wxHORIZONTAL);

	bxszrTopRow->Add(cbSpectraChooser, 1, wxALL, 5);
	bxszrTopRow->Add(btnNew, 0, wxALL, 5);
	bxszrTopRow->Add(btnRemove, 0, wxALL, 5);
	bxszrTopRow->Add(btnSave, 0, wxALL, 5);
	bxszrTopRow->Add(btnLoad, 0, wxALL, 5);
	bxszrTopRow->Add(btnDuplicate, 0, wxALL, 5);
	bxszrTopRow->Add(btnRename, 0, wxALL, 5);

	bxszrAMRow->AddStretchSpacer();
	bxszrAMRow->Add(btnAM15, 0, wxALL, 0);
	bxszrAMRow->AddStretchSpacer();
	bxszrAMRow->Add(btnAM0, 0, wxALL, 0);
	bxszrAMRow->AddStretchSpacer();

	fgszrDirection->AddSpacer(0);
	fgszrDirection->Add(stX, 0, wxALIGN_CENTER, 0);
	fgszrDirection->Add(stY, 0, wxALIGN_CENTER, 0);
	fgszrDirection->Add(stZ, 0, wxALIGN_CENTER, 0);
	fgszrDirection->Add(stDir, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 0);
	fgszrDirection->Add(txtDirX, 0, wxALIGN_CENTER, 0);
	fgszrDirection->Add(txtDirY, 0, wxALIGN_CENTER, 0);
	fgszrDirection->Add(txtDirZ, 0, wxALIGN_CENTER, 0);

	bxszrWaveIntensCol->Add(stWavelength, 0, wxALIGN_CENTER, 0);
	bxszrWaveIntensCol->Add(txtWavelength, 0, wxALIGN_CENTER | wxBOTTOM | wxEXPAND, 2);
	bxszrWaveIntensCol->Add(stIntensity, 0, wxALIGN_CENTER, 0);
	bxszrWaveIntensCol->Add(txtIntensity, 0, wxALIGN_CENTER | wxEXPAND, 0);

	bxszrWaveIntensRow->Add(rbInputType, 0, wxALIGN_CENTER | wxEXPAND | wxRIGHT, 3);
	bxszrWaveIntensRow->Add(bxszrWaveIntensCol, 1, wxALIGN_CENTER, 0);


	bxszrRemoveClear->Add(btnRemoveWave, 1, wxALIGN_CENTER | wxLEFT, 10);
	bxszrRemoveClear->Add(btnClearWaves, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT, 10);


	bxszrLColumn->Add(bxszrAMRow, 0, wxTOP | wxEXPAND, 2);
	bxszrLColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxTOP | wxBOTTOM | wxEXPAND, 2);
	bxszrLColumn->Add(fgszrDirection, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 1);
	bxszrLColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxTOP | wxBOTTOM | wxEXPAND, 2);
	bxszrLColumn->Add(btnAddWave, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 2);
	bxszrLColumn->Add(bxszrWaveIntensRow, 0, wxALL | wxEXPAND, 2);
	bxszrLColumn->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxTOP | wxBOTTOM | wxEXPAND, 2);
	bxszrLColumn->Add(grdDataTable, 1, wxALL | wxEXPAND, 2);
	bxszrLColumn->Add(bxszrRemoveClear, 0, wxALL | wxEXPAND, 2);
	
	plotSpectra->GetMPWindow()->Show();
	bxszrDataPlot->Add(bxszrLColumn, 0, wxALL | wxEXPAND, 0);
	bxszrDataPlot->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL), 0, wxALL | wxEXPAND, 0);
	bxszrDataPlot->Add(plotSpectra->GetMPWindow(), 1, wxALL | wxEXPAND, 0);
	

	szrPrimary->Add(bxszrTopRow, 0, wxALL | wxEXPAND, 2);
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxALL | wxEXPAND, 0);
	szrPrimary->Add(bxszrDataPlot, 1, wxALL | wxEXPAND, 2);


	prevWave = 0.0f;
	prevIntensity = 0.0;
	curLightSource = nullptr;
	SetSizer(szrPrimary);
	TransferToGUI();
}

simSpectraPanel::~simSpectraPanel()
{
	delete plotSpectra;
}

simulationWindow* simSpectraPanel::getMyClassParent()
{
	return (simulationWindow*)GetParent();
}

ModelDescribe* simSpectraPanel::getMdesc()
{
	return getMyClassParent()->getMdesc();
}

Simulation* simSpectraPanel::getSim()
{
	return getMdesc()->simulation;
}
void simSpectraPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (curLightSource == nullptr)
		return;
	if (!isInitialized())
		return;
}

void simSpectraPanel::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
	{
		btnNew->Disable();
		btnLoad->Disable();
		btnSave->Disable();
		btnRemove->Disable();
		btnDuplicate->Disable();
		btnRename->Disable();
		btnAM0->Disable();
		btnAM15->Disable();
		txtIntensity->Disable();
		txtDirX->Disable();
		txtDirY->Disable();
		txtDirZ->Disable();
		txtWavelength->Disable();
		cbSpectraChooser->Disable();
		rbInputType->Disable();
		grdDataTable->Disable();
		plotSpectra->Disable();
		btnRemoveWave->Disable();
		btnAddWave->Disable();
		btnClearWaves->Disable();
		return;
	}
	if (curLightSource == nullptr)
	{
		if (mdesc->spectrum.size() > 0)
		{
			curLightSource = mdesc->spectrum[0];
			TransferToGUI();	//this will then call updateguivalues and go through to completion.
			return;
		}
	}

	if (curLightSource == nullptr)	//there is an mdesc, and there is nothing to choose from!
	{
		btnNew->Enable();
		btnLoad->Enable();

		btnSave->Disable();
		btnRemove->Disable();
		btnDuplicate->Disable();
		btnRename->Disable();
		btnAM0->Disable();
		btnAM15->Disable();
		txtIntensity->Disable();
		txtDirX->Disable();
		txtDirY->Disable();
		txtDirZ->Disable();
		txtWavelength->Disable();
		cbSpectraChooser->Disable();
		rbInputType->Disable();
		grdDataTable->Disable();
		plotSpectra->Disable();
		btnRemoveWave->Disable();
		btnAddWave->Disable();
		btnClearWaves->Disable();
	}
	else
	{
		btnNew->Enable();
		btnLoad->Enable();

		btnSave->Enable();
		btnRemove->Enable();
		btnDuplicate->Enable();
		btnRename->Enable();
		btnAM0->Enable();
		btnAM15->Enable();
		txtIntensity->Enable();
		txtDirX->Enable();
		txtDirY->Enable();
		txtDirZ->Enable();
		txtWavelength->Enable();
		cbSpectraChooser->Enable();
		rbInputType->Enable();
		grdDataTable->Enable();
		plotSpectra->Enable();


		
		btnRemoveWave->Enable(eraseRow < grdDataTable->GetNumberRows() && eraseRow >= 0);
		btnClearWaves->Enable(grdDataTable->GetNumberRows() > 0);

		bool AddWave = true;
		double tmpVal;
		if (txtWavelength->GetValue().ToDouble(&tmpVal) == true)
			AddWave = (tmpVal > 0.0);
		else
			AddWave = false;

		if (AddWave && txtIntensity->GetValue().ToDouble(&tmpVal) == true)
			AddWave = (tmpVal >= 0.0);
		else
			AddWave = false;

		btnAddWave->Enable(AddWave);
	}
}

void simSpectraPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	VerifyInternalVars();
	if (curLightSource == nullptr)
	{
		UpdateGUIValues(wxCommandEvent());
		return;	//nothign to do/see here
	}

	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	

	cbSpectraChooser->Clear();
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i)
	{
		cbSpectraChooser->AppendString(mdesc->spectrum[i]->name);
		if (mdesc->spectrum[i] == curLightSource)
			cbSpectraChooser->SetSelection(i);
	}

	ValueToTBox(txtDirX, curLightSource->direction.x);
	ValueToTBox(txtDirY, curLightSource->direction.y);
	ValueToTBox(txtDirZ, curLightSource->direction.z);

	if (curLightSource->inputType == SPECTRUM_INTENSITY)
	{
		rbInputType->Select(1);
	}
	else if (curLightSource->inputType == SPECTRUM_INTENS_DERIV)
	{
		rbInputType->Select(0);
	}

	std::vector<Wavelength> spData;
	AddMapValsToVector(curLightSource->light, spData);
	//get it all in one nice, easy, place.

	//it is sorted by energy, we want sorted by wavelength
	plotSpectra->GetData(0)->ClearData();
	plotSpectra->GetData(1)->ClearData();
	plotSpectra->GetData(2)->ClearData();
	int adj = spData.size() - grdDataTable->GetNumberRows();
	if (adj < 0)
		grdDataTable->DeleteRows(0, -adj);
	else if (adj > 0)
		grdDataTable->AppendRows(adj);

	wxString tmpStr;
	unsigned int row = 0;
	for (unsigned int dat = spData.size() - 1; dat < spData.size(); --dat, ++row)
	{
		tmpStr = "";
		tmpStr << spData[dat].lambda;
		grdDataTable->SetCellValue(row, 0, tmpStr);
		tmpStr = "";
		tmpStr << spData[dat].energyphoton;
		grdDataTable->SetCellValue(row, 1, tmpStr);
		tmpStr = "";
		tmpStr << spData[dat].intensity;
		grdDataTable->SetCellValue(row, 2, tmpStr);
		
		plotSpectra->GetData(0)->AddData(spData[dat].lambda);
		plotSpectra->GetData(1)->AddData(spData[dat].energyphoton);
		plotSpectra->GetData(2)->AddData(spData[dat].intensity);
	}

	plotSpectra->SetTitle(curLightSource->name);

	plotSpectra->UpdatePlot();	//all the data just changed!

	UpdateGUIValues(wxCommandEvent());
}



void simSpectraPanel::CreateSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Enter the spectra's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	LightSource* lsrc = new LightSource();
	lsrc->ID = -1;
	lsrc->name = entry.GetValue();
	lsrc->direction.x = 1.0;
	
	mdesc->spectrum.push_back(lsrc);
	mdesc->SetUnknownIDs();

	curLightSource = lsrc;
	TransferToGUI();
}

void simSpectraPanel::AddWavelength(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	double wavelength, value;
	if (txtWavelength->GetValue().ToDouble(&wavelength) == false)
		return;
	if (wavelength <= 0.0)
		return;

	if (txtIntensity->GetValue().ToDouble(&value) == false)
		return;
	if (value < 0.0)
		value = 0.0;

	Wavelength wave;
	wave.lambda = (float)wavelength;
	wave.energyphoton = PHOTONENERGYCONST / wavelength;
	wave.intensity = value;
	wave.IsCutOffPowerInit = false;
	wave.DirEmitted = 0;


	//test this on the plot before adding anything!
	unsigned int sz = plotSpectra->GetData(0)->size();
	unsigned int properIndex = plotSpectra->AddUnique(0, wavelength);
	if (sz == properIndex)	//it added on to the end. Actually add stuff in!
	{
		curLightSource->light.insert(std::pair<double, Wavelength>(wave.energyphoton, wave));
		plotSpectra->GetData(1)->AddData(wave.energyphoton);
		plotSpectra->GetData(2)->AddData(wave.intensity);
	}
	else if (properIndex < sz) //it previously existed. Update the intensity of the nearest value!
	{
		

		//update the light source. MAke sure we get the right one.
		auto lowerE = MapFindFirstLessEqual(curLightSource->light, wave.energyphoton);
		auto higherE = MapFindFirstGreater(curLightSource->light, wave.energyphoton);
		double lowE = -10.0;
		double highE = -10.0;
		bool goodLow, goodHigh;
		goodLow = goodHigh = false;
		if (lowerE != curLightSource->light.end())
		{
			lowE = lowerE->first;
			goodLow = true;
		}
		if (higherE != curLightSource->light.end())
		{
			highE = higherE->first;
			goodHigh = true;
		}

		//one of these should pretty much be exact, so it should be ok to be slightly sloppy.
		if (!goodLow && !goodHigh)
			return; //both are bad, don't actually change anything. This really shouldn't happen in this scenario.

		if (goodLow && goodHigh)
		{
			//choose the closer one.
			lowE = fabs(wave.energyphoton - lowE);
			highE = fabs(wave.energyphoton - highE);
			//they are now both positively represented deltas
			if (lowE <= highE)
				goodHigh = false;
			else
				goodLow = false;
		}

		if (goodLow)
		{
			wave.energyphoton = lowerE->second.energyphoton;
			wave.lambda = lowerE->second.lambda;
			//keep the data consistent in the light source with the map.
			lowerE->second = wave;
		}
		else if (goodHigh)
		{
			wave.energyphoton = higherE->second.energyphoton;
			wave.lambda = higherE->second.lambda;
			//keep the data consistent in the light source with the map.
			higherE->second = wave;
		}
		
		//now update the plot.

		//and we have the index. The only thing that is changing is the wave intensity.
		plotSpectra->GetData(2)->ReplaceData(properIndex, wave.intensity);
	}

	//now try and update the energy and intensity so they can presumably keep clicking
	float delta = wave.lambda - prevWave;
	prevWave = wave.lambda;
	double newWave = prevWave + delta;
	ValueToTBox(txtWavelength, newWave);
	double deltaD = wave.intensity - prevIntensity;
	prevIntensity = wave.intensity;
	newWave = prevIntensity + deltaD;
	ValueToTBox(txtIntensity, newWave);

	TransferToGUI();	//update all the values here!

	grdDataTable->AutoSizeColumns();
//	wxSize size = grdDataTable->GetSize();
//	size.SetWidth(280 + wxSystemSettings::GetMetric(wxSYS_VSCROLL_X));
//	bxszrDataPlot->Layout();

/*	int x, y;
	wxSize curSize = grdDataTable->GetSize();
//	x = grdDataTable->GetVirtualSize().GetWidth();// +grdDataTable->GetRowLabelSize();
//	grdDataTable->HasScrollbar ALWAYS TRUE
//	if (grdDataTable->IsScrollbarShown(wxVERTICAL))	ALWAYS TRUE
//	if (grdDataTable->Scro(wxVERTICAL))
	x = 280;
	x += wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
	curSize.SetWidth(x);
	grdDataTable->SetMinSize(curSize);

//	bxszrLColumn->Layout();
	bxszrDataPlot->Layout();
//	szrPrimary->Layout();
*/
	return;
}

void simSpectraPanel::VerifyInternalVars() {
	ModelDescribe *mdesc = getMdesc();
	if (mdesc == nullptr) {
		curLightSource = nullptr;
		return;
	}



	if (mdesc->HasSpectrum(curLightSource) == false)
		curLightSource = nullptr;
}

void simSpectraPanel::UpdateWavelengthList()
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
	{
		while (grdDataTable->GetNumberRows() > 0)
			grdDataTable->DeleteRows();
		return;
	}

	int Adjustment = curLightSource->light.size() - grdDataTable->GetNumberRows();
	if (Adjustment > 0)
		grdDataTable->AppendRows(Adjustment);
	else if (Adjustment < 0)
		grdDataTable->DeleteRows(0, -Adjustment);

	//now the number of rows matches the light size!
	unsigned int row = 0;
	wxString tmpStr;
	for (auto lt = curLightSource->light.rbegin(); lt != curLightSource->light.rend(); ++lt)
	{
		tmpStr = "";
		tmpStr << lt->second.lambda;
		grdDataTable->SetCellValue(row, 0, tmpStr);
		tmpStr = "";
		tmpStr << lt->second.energyphoton;
		grdDataTable->SetCellValue(row, 1, tmpStr);
		tmpStr = "";
		tmpStr << lt->second.intensity;
		grdDataTable->SetCellValue(row, 2, tmpStr);
		row++;
	}
}


void simSpectraPanel::SaveSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Spectrum files (*.spec)|*.spec"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		curLightSource->SaveToXML(oFile);
		oFile.closeFile();
	}

}
void simSpectraPanel::LoadSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Spectrum files (*.spec)|*.spec"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}

	LightSource* lt = new LightSource();
	LoadSpectrumChildren(file, *mdesc, lt, true);
	lt->ID = -1;

	mdesc->spectrum.push_back(lt);
	mdesc->SetUnknownIDs();
	curLightSource = lt;
	TransferToGUI();

}
void simSpectraPanel::RenameSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	wxTextEntryDialog entry(this, wxT("Rename Spectra"));
	if (entry.ShowModal() != wxID_OK)
		return;

	curLightSource->name = entry.GetValue();
	TransferToGUI();
}
void simSpectraPanel::RemoveSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (curLightSource == nullptr || mdesc == nullptr)
		return;

	unsigned int newIndex = -1;
	for (unsigned int i = 0; i < mdesc->spectrum.size(); ++i)
	{
		if (mdesc->spectrum[i] == curLightSource)
		{
			newIndex = i;	//hopefully... requires testing.
			mdesc->spectrum.erase(mdesc->spectrum.begin() + i);
			delete curLightSource;
			curLightSource = nullptr;
		}
	}
	if (newIndex >= mdesc->spectrum.size())	//if size is 0...
		newIndex = mdesc->spectrum.size() - 1;	//this is now invalid and very large.
	
	if (newIndex < mdesc->spectrum.size())
		curLightSource = mdesc->spectrum[newIndex];

	TransferToGUI();

}
void simSpectraPanel::RemoveWavelength(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;
	if ((unsigned int)eraseRow >= curLightSource->light.size())
		return;

	auto rit = curLightSource->light.rbegin();
	int ctr = 0;
	for (rit = curLightSource->light.rbegin(); rit != curLightSource->light.rend() && ctr < eraseRow; ++rit)
		ctr++;

	//need a forward iterator for erase. The rbegin pointer points at .end(), but the first/second access go to the previous element.
	//so we need to go to the next one, so the pointer actually goes to the proper spot.
	curLightSource->light.erase(std::next(rit).base());

	TransferToGUI();	//now just update everything!

}
void simSpectraPanel::ClearWavelengths(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	curLightSource->light.clear();
	TransferToGUI();
}
void simSpectraPanel::DuplicateSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe * mdesc = getMdesc();
	if (curLightSource == nullptr || mdesc == nullptr)	//can't do said operation
		return;

	wxTextEntryDialog entry(this, wxT("Enter the Spectra's name"));
	if (entry.ShowModal() != wxID_OK)
		return;

	LightSource *ltSrc = new LightSource(*curLightSource);
	ltSrc->ID = -1;
	ltSrc->name = entry.GetValue();
	mdesc->spectrum.push_back(ltSrc);
	mdesc->SetUnknownIDs();

	curLightSource = ltSrc;
	TransferToGUI();
}

void simSpectraPanel::RadBoxModifed(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;
	int Sel = rbInputType->GetSelection();
	if (Sel == 0)
		curLightSource->inputType = SPECTRUM_INTENS_DERIV;
	else
		curLightSource->inputType = SPECTRUM_INTENSITY;

}
void simSpectraPanel::DirectionTextModified(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return; 
	
	txtDirX->GetValue().ToDouble(&curLightSource->direction.x);	//any value is valid!
	txtDirY->GetValue().ToDouble(&curLightSource->direction.y);
	txtDirZ->GetValue().ToDouble(&curLightSource->direction.z);
		 

}
void simSpectraPanel::GridClick(wxGridEvent& event)
{
	if (!isInitialized())
		return;
	eraseRow = event.GetRow();
	wxString tmp = "Remove Wavelength (";
	tmp << (eraseRow + 1) << ")";
	btnRemoveWave->SetLabel(tmp);
	UpdateGUIValues(event);
}
void simSpectraPanel::GridText(wxGridEvent& event)	//note, this is called when the text cell changes focus!
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	int row, col;
	row = event.GetRow();
	col = event.GetCol();

	std::vector<Wavelength> dat;
	AddMapValsToVector(curLightSource->light, dat);	//the map is stored in eV, but the rows are ordered by nm.

	int indexModified = dat.size() - row - 1;	//so we need to go backwards!
	if (indexModified < 0)	//problem. perhaps called with no data?
		return;

	Wavelength tmp = dat[indexModified];	//needed for finding stuff...
	std::map<double, Wavelength>::iterator it = curLightSource->light.find(tmp.energyphoton);
	if (it == curLightSource->light.end())	//this should also fail...
		return;
	//now know which one needs to modify!
	bool replace = false;	//the key changes!
	bool modify = false;	//just the intensity changes!
	bool revert = false;	//bad data, put it back to the way it was!
	if (col == 0)	//modifying the wavelength
	{
		double wave;
		if (grdDataTable->GetCellValue(row, 0).ToDouble(&wave) == true)
		{
			if (wave > 0.0 && (float)wave != tmp.lambda)	//valid, and it changed
			{
				tmp.lambda = (float)wave;
				tmp.energyphoton = PHOTONENERGYCONST / wave;
				replace = true;
			}
			else
				revert = true;
		}
		else
			revert = true;
	}
	else if (col == 1)	//modifying the energy
	{
		double energy;
		if (grdDataTable->GetCellValue(row, 1).ToDouble(&energy) == true)
		{
			if (energy > 0.0 && energy != tmp.energyphoton)
			{
				tmp.energyphoton = energy;
				tmp.lambda = PHOTONENERGYCONST / energy;
				replace = true;
			}
			else
				revert = true;
		}
		else
			revert = true;
	}
	else if (col == 2)	//modifying the intensity
	{
		double intens;
		if (grdDataTable->GetCellValue(row, 2).ToDouble(&intens) == true)
		{
			if (intens >= 0.0 && intens != tmp.intensity)
			{
				tmp.intensity = intens;
				modify = true;
			}
			else
				revert = true;
		}
		else
			revert = true;
	}
	else //wtf
		revert = true;

	if (modify)	//energy stayed the same! Valid value, keep what's in the cell.
	{
		it->second = tmp;
		TransferToGUI();	//gotta update the plot data, and this is the best way to do that!
	}
	else if (replace)
	{
		curLightSource->light.erase(it);
		curLightSource->light.insert(std::pair<double, Wavelength>(tmp.energyphoton, tmp));
		TransferToGUI();	//update plot data...
	}
	else if (revert)
	{
		wxString tmpStr;
		tmpStr << tmp.lambda;
		grdDataTable->SetCellValue(row, 0, tmpStr);
		tmpStr = "";
		tmpStr << tmp.energyphoton;
		grdDataTable->SetCellValue(row, 1, tmpStr);
		tmpStr = "";
		tmpStr << tmp.intensity;
		grdDataTable->SetCellValue(row, 2, tmpStr);
	}
	return;
}

void simSpectraPanel::SwitchSpectra(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	unsigned int getSel = cbSpectraChooser->GetSelection();
	if (getSel < mdesc->spectrum.size())
	{
		if (curLightSource != mdesc->spectrum[getSel])
		{
			curLightSource = mdesc->spectrum[getSel];
			TransferToGUI();
		}
	}
}

void simSpectraPanel::SetAM0(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	curLightSource->name = "AM 0.0";
	ClearWavelengths(event);
	curLightSource->inputType = SPECTRUM_INTENS_DERIV;

	curLightSource->AddWavelength(199.5f,0.0005);
	curLightSource->AddWavelength(200.5f,0.0007);
	curLightSource->AddWavelength(201.5f,0.0007);
	curLightSource->AddWavelength(202.5f,0.0008);
	curLightSource->AddWavelength(203.5f,0.0009);
	curLightSource->AddWavelength(204.5f,0.0009);
	curLightSource->AddWavelength(205.5f,0.001);
	curLightSource->AddWavelength(206.5f,0.001);
	curLightSource->AddWavelength(207.5f,0.0011);
	curLightSource->AddWavelength(208.5f,0.0015);
	curLightSource->AddWavelength(209.5f,0.0024);
	curLightSource->AddWavelength(210.5f,0.0028);
	curLightSource->AddWavelength(211.5f,0.0034);
	curLightSource->AddWavelength(212.5f,0.003);
	curLightSource->AddWavelength(213.5f,0.0032);
	curLightSource->AddWavelength(214.5f,0.0041);
	curLightSource->AddWavelength(215.5f,0.0037);
	curLightSource->AddWavelength(216.5f,0.0034);
	curLightSource->AddWavelength(217.5f,0.0036);
	curLightSource->AddWavelength(218.5f,0.0045);
	curLightSource->AddWavelength(219.5f,0.0048);
	curLightSource->AddWavelength(220.5f,0.0048);
	curLightSource->AddWavelength(221.5f,0.0039);
	curLightSource->AddWavelength(222.5f,0.0051);
	curLightSource->AddWavelength(223.5f,0.0066);
	curLightSource->AddWavelength(224.5f,0.0058);
	curLightSource->AddWavelength(225.5f,0.0054);
	curLightSource->AddWavelength(226.5f,0.0041);
	curLightSource->AddWavelength(227.5f,0.0041);
	curLightSource->AddWavelength(228.5f,0.0054);
	curLightSource->AddWavelength(229.5f,0.0048);
	curLightSource->AddWavelength(230.5f,0.0056);
	curLightSource->AddWavelength(231.5f,0.005);
	curLightSource->AddWavelength(232.5f,0.0055);
	curLightSource->AddWavelength(233.5f,0.0046);
	curLightSource->AddWavelength(234.5f,0.0039);
	curLightSource->AddWavelength(235.5f,0.0057);
	curLightSource->AddWavelength(236.5f,0.0049);
	curLightSource->AddWavelength(237.5f,0.0053);
	curLightSource->AddWavelength(238.5f,0.0042);
	curLightSource->AddWavelength(239.5f,0.0046);
	curLightSource->AddWavelength(240.5f,0.0043);
	curLightSource->AddWavelength(241.5f,0.0052);
	curLightSource->AddWavelength(242.5f,0.0072);
	curLightSource->AddWavelength(243.5f,0.0065);
	curLightSource->AddWavelength(244.5f,0.0062);
	curLightSource->AddWavelength(245.5f,0.0051);
	curLightSource->AddWavelength(246.5f,0.0051);
	curLightSource->AddWavelength(247.5f,0.0057);
	curLightSource->AddWavelength(248.5f,0.0045);
	curLightSource->AddWavelength(249.5f,0.0058);
	curLightSource->AddWavelength(250.5f,0.0059);
	curLightSource->AddWavelength(251.5f,0.0047);
	curLightSource->AddWavelength(252.5f,0.0044);
	curLightSource->AddWavelength(253.5f,0.0055);
	curLightSource->AddWavelength(254.5f,0.0061);
	curLightSource->AddWavelength(255.5f,0.0089);
	curLightSource->AddWavelength(256.5f,0.0107);
	curLightSource->AddWavelength(257.5f,0.0129);
	curLightSource->AddWavelength(258.5f,0.0134);
	curLightSource->AddWavelength(259.5f,0.0108);
	curLightSource->AddWavelength(260.5f,0.0102);
	curLightSource->AddWavelength(261.5f,0.0103);
	curLightSource->AddWavelength(262.5f,0.0121);
	curLightSource->AddWavelength(263.5f,0.0175);
	curLightSource->AddWavelength(264.5f,0.0274);
	curLightSource->AddWavelength(265.5f,0.028);
	curLightSource->AddWavelength(266.5f,0.026);
	curLightSource->AddWavelength(267.5f,0.027);
	curLightSource->AddWavelength(268.5f,0.026);
	curLightSource->AddWavelength(269.5f,0.0252);
	curLightSource->AddWavelength(270.5f,0.0293);
	curLightSource->AddWavelength(271.5f,0.0232);
	curLightSource->AddWavelength(272.5f,0.0215);
	curLightSource->AddWavelength(273.5f,0.0204);
	curLightSource->AddWavelength(274.5f,0.0137);
	curLightSource->AddWavelength(275.5f,0.02);
	curLightSource->AddWavelength(276.5f,0.0258);
	curLightSource->AddWavelength(277.5f,0.024);
	curLightSource->AddWavelength(278.5f,0.0166);
	curLightSource->AddWavelength(279.5f,0.0089);
	curLightSource->AddWavelength(280.5f,0.0112);
	curLightSource->AddWavelength(281.5f,0.0231);
	curLightSource->AddWavelength(282.5f,0.0307);
	curLightSource->AddWavelength(283.5f,0.033);
	curLightSource->AddWavelength(284.5f,0.0244);
	curLightSource->AddWavelength(285.5f,0.0141);
	curLightSource->AddWavelength(286.5f,0.032);
	curLightSource->AddWavelength(287.5f,0.0371);
	curLightSource->AddWavelength(288.5f,0.0307);
	curLightSource->AddWavelength(289.5f,0.0456);
	curLightSource->AddWavelength(290.5f,0.0623);
	curLightSource->AddWavelength(291.5f,0.06);
	curLightSource->AddWavelength(292.5f,0.0545);
	curLightSource->AddWavelength(293.5f,0.0545);
	curLightSource->AddWavelength(294.5f,0.0509);
	curLightSource->AddWavelength(295.5f,0.0548);
	curLightSource->AddWavelength(296.5f,0.0492);
	curLightSource->AddWavelength(297.5f,0.0531);
	curLightSource->AddWavelength(298.5f,0.0413);
	curLightSource->AddWavelength(299.5f,0.0485);
	curLightSource->AddWavelength(300.5f,0.0403);
	curLightSource->AddWavelength(301.5f,0.0445);
	curLightSource->AddWavelength(302.5f,0.0484);
	curLightSource->AddWavelength(303.5f,0.0631);
	curLightSource->AddWavelength(304.5f,0.061);
	curLightSource->AddWavelength(305.5f,0.058);
	curLightSource->AddWavelength(306.5f,0.0575);
	curLightSource->AddWavelength(307.5f,0.0645);
	curLightSource->AddWavelength(308.5f,0.0613);
	curLightSource->AddWavelength(309.5f,0.0484);
	curLightSource->AddWavelength(310.0f,0.0495);
	curLightSource->AddWavelength(310.4f,0.0507);
	curLightSource->AddWavelength(310.8f,0.0588);
	curLightSource->AddWavelength(311.2f,0.0707);
	curLightSource->AddWavelength(311.6f,0.0747);
	curLightSource->AddWavelength(312.0f,0.0707);
	curLightSource->AddWavelength(312.4f,0.0644);
	curLightSource->AddWavelength(312.8f,0.0663);
	curLightSource->AddWavelength(313.2f,0.071);
	curLightSource->AddWavelength(313.6f,0.0691);
	curLightSource->AddWavelength(314.0f,0.0689);
	curLightSource->AddWavelength(314.4f,0.0722);
	curLightSource->AddWavelength(314.8f,0.0673);
	curLightSource->AddWavelength(315.2f,0.0695);
	curLightSource->AddWavelength(315.6f,0.0765);
	curLightSource->AddWavelength(316.0f,0.0675);
	curLightSource->AddWavelength(316.4f,0.0569);
	curLightSource->AddWavelength(316.8f,0.0623);
	curLightSource->AddWavelength(317.2f,0.0749);
	curLightSource->AddWavelength(317.6f,0.083);
	curLightSource->AddWavelength(318.0f,0.0813);
	curLightSource->AddWavelength(318.4f,0.0673);
	curLightSource->AddWavelength(318.8f,0.0642);
	curLightSource->AddWavelength(319.2f,0.0768);
	curLightSource->AddWavelength(319.6f,0.0759);
	curLightSource->AddWavelength(320.0f,0.0712);
	curLightSource->AddWavelength(320.4f,0.0778);
	curLightSource->AddWavelength(320.8f,0.0844);
	curLightSource->AddWavelength(321.2f,0.0847);
	curLightSource->AddWavelength(321.6f,0.0736);
	curLightSource->AddWavelength(322.0f,0.0695);
	curLightSource->AddWavelength(322.4f,0.0773);
	curLightSource->AddWavelength(322.8f,0.0758);
	curLightSource->AddWavelength(323.2f,0.0646);
	curLightSource->AddWavelength(323.6f,0.0603);
	curLightSource->AddWavelength(324.0f,0.0604);
	curLightSource->AddWavelength(324.4f,0.0618);
	curLightSource->AddWavelength(324.8f,0.0654);
	curLightSource->AddWavelength(325.2f,0.0646);
	curLightSource->AddWavelength(325.6f,0.0682);
	curLightSource->AddWavelength(326.0f,0.0852);
	curLightSource->AddWavelength(326.4f,0.1049);
	curLightSource->AddWavelength(326.8f,0.1111);
	curLightSource->AddWavelength(327.2f,0.1108);
	curLightSource->AddWavelength(327.6f,0.105);
	curLightSource->AddWavelength(328.0f,0.0965);
	curLightSource->AddWavelength(328.4f,0.0914);
	curLightSource->AddWavelength(328.8f,0.0913);
	curLightSource->AddWavelength(329.2f,0.0952);
	curLightSource->AddWavelength(329.6f,0.1043);
	curLightSource->AddWavelength(330.0f,0.1144);
	curLightSource->AddWavelength(330.4f,0.1137);
	curLightSource->AddWavelength(330.5f,0.1006);
	curLightSource->AddWavelength(331.5f,0.0968);
	curLightSource->AddWavelength(332.5f,0.0921);
	curLightSource->AddWavelength(333.5f,0.0905);
	curLightSource->AddWavelength(334.5f,0.094);
	curLightSource->AddWavelength(335.5f,0.0982);
	curLightSource->AddWavelength(336.5f,0.0765);
	curLightSource->AddWavelength(337.5f,0.0866);
	curLightSource->AddWavelength(338.5f,0.0916);
	curLightSource->AddWavelength(339.5f,0.0937);
	curLightSource->AddWavelength(340.5f,0.0992);
	curLightSource->AddWavelength(341.5f,0.0936);
	curLightSource->AddWavelength(342.5f,0.0995);
	curLightSource->AddWavelength(343.5f,0.0985);
	curLightSource->AddWavelength(344.5f,0.0719);
	curLightSource->AddWavelength(345.5f,0.0967);
	curLightSource->AddWavelength(346.5f,0.0919);
	curLightSource->AddWavelength(347.5f,0.0902);
	curLightSource->AddWavelength(348.5f,0.0948);
	curLightSource->AddWavelength(349.5f,0.0865);
	curLightSource->AddWavelength(350.5f,0.1119);
	curLightSource->AddWavelength(351.5f,0.0993);
	curLightSource->AddWavelength(352.5f,0.0871);
	curLightSource->AddWavelength(353.5f,0.1115);
	curLightSource->AddWavelength(354.5f,0.1133);
	curLightSource->AddWavelength(355.5f,0.1058);
	curLightSource->AddWavelength(356.5f,0.0938);
	curLightSource->AddWavelength(357.5f,0.0891);
	curLightSource->AddWavelength(358.5f,0.0627);
	curLightSource->AddWavelength(359.5f,0.1136);
	curLightSource->AddWavelength(360.5f,0.0979);
	curLightSource->AddWavelength(361.5f,0.0894);
	curLightSource->AddWavelength(362.5f,0.1175);
	curLightSource->AddWavelength(363.5f,0.0958);
	curLightSource->AddWavelength(364.5f,0.1015);
	curLightSource->AddWavelength(365.5f,0.1263);
	curLightSource->AddWavelength(366.5f,0.1249);
	curLightSource->AddWavelength(367.5f,0.1214);
	curLightSource->AddWavelength(368.5f,0.1088);
	curLightSource->AddWavelength(369.5f,0.1331);
	curLightSource->AddWavelength(370.5f,0.1075);
	curLightSource->AddWavelength(371.5f,0.1307);
	curLightSource->AddWavelength(372.5f,0.1065);
	curLightSource->AddWavelength(373.5f,0.0838);
	curLightSource->AddWavelength(374.5f,0.0878);
	curLightSource->AddWavelength(375.5f,0.1141);
	curLightSource->AddWavelength(376.5f,0.1101);
	curLightSource->AddWavelength(377.5f,0.1291);
	curLightSource->AddWavelength(378.5f,0.1341);
	curLightSource->AddWavelength(379.5f,0.1);
	curLightSource->AddWavelength(380.5f,0.1289);
	curLightSource->AddWavelength(381.5f,0.1096);
	curLightSource->AddWavelength(382.5f,0.0733);
	curLightSource->AddWavelength(383.5f,0.0684);
	curLightSource->AddWavelength(384.5f,0.1027);
	curLightSource->AddWavelength(385.5f,0.0954);
	curLightSource->AddWavelength(386.5f,0.1071);
	curLightSource->AddWavelength(387.5f,0.0966);
	curLightSource->AddWavelength(388.5f,0.0912);
	curLightSource->AddWavelength(389.5f,0.1227);
	curLightSource->AddWavelength(390.5f,0.1223);
	curLightSource->AddWavelength(391.5f,0.1398);
	curLightSource->AddWavelength(392.5f,0.0955);
	curLightSource->AddWavelength(393.5f,0.0489);
	curLightSource->AddWavelength(394.5f,0.1101);
	curLightSource->AddWavelength(395.5f,0.1378);
	curLightSource->AddWavelength(396.5f,0.065);
	curLightSource->AddWavelength(397.5f,0.104);
	curLightSource->AddWavelength(398.5f,0.1538);
	curLightSource->AddWavelength(399.5f,0.1655);
	curLightSource->AddWavelength(400.5f,0.1649);
	curLightSource->AddWavelength(401.5f,0.1796);
	curLightSource->AddWavelength(402.5f,0.1803);
	curLightSource->AddWavelength(403.5f,0.1658);
	curLightSource->AddWavelength(404.5f,0.1602);
	curLightSource->AddWavelength(405.5f,0.1672);
	curLightSource->AddWavelength(406.5f,0.1624);
	curLightSource->AddWavelength(407.5f,0.1545);
	curLightSource->AddWavelength(408.5f,0.1824);
	curLightSource->AddWavelength(409.5f,0.1706);
	curLightSource->AddWavelength(410.5f,0.1502);
	curLightSource->AddWavelength(411.5f,0.1819);
	curLightSource->AddWavelength(412.5f,0.1791);
	curLightSource->AddWavelength(413.5f,0.1758);
	curLightSource->AddWavelength(414.5f,0.1739);
	curLightSource->AddWavelength(415.5f,0.1736);
	curLightSource->AddWavelength(416.5f,0.1844);
	curLightSource->AddWavelength(417.5f,0.1667);
	curLightSource->AddWavelength(418.5f,0.1686);
	curLightSource->AddWavelength(419.5f,0.1703);
	curLightSource->AddWavelength(420.5f,0.176);
	curLightSource->AddWavelength(421.5f,0.1799);
	curLightSource->AddWavelength(422.5f,0.1584);
	curLightSource->AddWavelength(423.5f,0.1713);
	curLightSource->AddWavelength(424.5f,0.177);
	curLightSource->AddWavelength(425.5f,0.1697);
	curLightSource->AddWavelength(426.5f,0.17);
	curLightSource->AddWavelength(427.5f,0.1571);
	curLightSource->AddWavelength(428.5f,0.1589);
	curLightSource->AddWavelength(429.5f,0.1477);
	curLightSource->AddWavelength(430.5f,0.1136);
	curLightSource->AddWavelength(431.5f,0.1688);
	curLightSource->AddWavelength(432.5f,0.1648);
	curLightSource->AddWavelength(433.5f,0.1733);
	curLightSource->AddWavelength(434.5f,0.1672);
	curLightSource->AddWavelength(435.5f,0.1725);
	curLightSource->AddWavelength(436.5f,0.1931);
	curLightSource->AddWavelength(437.5f,0.1808);
	curLightSource->AddWavelength(438.5f,0.1569);
	curLightSource->AddWavelength(439.5f,0.1827);
	curLightSource->AddWavelength(440.5f,0.1715);
	curLightSource->AddWavelength(441.5f,0.1933);
	curLightSource->AddWavelength(442.5f,0.1982);
	curLightSource->AddWavelength(443.5f,0.1911);
	curLightSource->AddWavelength(444.5f,0.1975);
	curLightSource->AddWavelength(445.5f,0.1823);
	curLightSource->AddWavelength(446.5f,0.1893);
	curLightSource->AddWavelength(447.5f,0.2079);
	curLightSource->AddWavelength(448.5f,0.1975);
	curLightSource->AddWavelength(449.5f,0.2029);
	curLightSource->AddWavelength(450.5f,0.2146);
	curLightSource->AddWavelength(451.5f,0.2111);
	curLightSource->AddWavelength(452.5f,0.1943);
	curLightSource->AddWavelength(453.5f,0.1972);
	curLightSource->AddWavelength(454.5f,0.1981);
	curLightSource->AddWavelength(455.5f,0.2036);
	curLightSource->AddWavelength(456.5f,0.2079);
	curLightSource->AddWavelength(457.5f,0.2102);
	curLightSource->AddWavelength(458.5f,0.1973);
	curLightSource->AddWavelength(459.5f,0.2011);
	curLightSource->AddWavelength(460.5f,0.2042);
	curLightSource->AddWavelength(461.5f,0.2057);
	curLightSource->AddWavelength(462.5f,0.2106);
	curLightSource->AddWavelength(463.5f,0.2042);
	curLightSource->AddWavelength(464.5f,0.1978);
	curLightSource->AddWavelength(465.5f,0.2044);
	curLightSource->AddWavelength(466.5f,0.1923);
	curLightSource->AddWavelength(467.5f,0.2017);
	curLightSource->AddWavelength(468.5f,0.1996);
	curLightSource->AddWavelength(469.5f,0.1992);
	curLightSource->AddWavelength(470.5f,0.1879);
	curLightSource->AddWavelength(471.5f,0.202);
	curLightSource->AddWavelength(472.5f,0.2043);
	curLightSource->AddWavelength(473.5f,0.1993);
	curLightSource->AddWavelength(474.5f,0.2053);
	curLightSource->AddWavelength(475.5f,0.2018);
	curLightSource->AddWavelength(476.5f,0.1958);
	curLightSource->AddWavelength(477.5f,0.2077);
	curLightSource->AddWavelength(478.5f,0.2011);
	curLightSource->AddWavelength(479.5f,0.2078);
	curLightSource->AddWavelength(480.5f,0.2037);
	curLightSource->AddWavelength(481.5f,0.2092);
	curLightSource->AddWavelength(482.5f,0.2025);
	curLightSource->AddWavelength(483.5f,0.2021);
	curLightSource->AddWavelength(484.5f,0.1971);
	curLightSource->AddWavelength(485.5f,0.1832);
	curLightSource->AddWavelength(486.5f,0.1627);
	curLightSource->AddWavelength(487.5f,0.1832);
	curLightSource->AddWavelength(488.5f,0.1916);
	curLightSource->AddWavelength(489.5f,0.1962);
	curLightSource->AddWavelength(490.5f,0.2009);
	curLightSource->AddWavelength(491.5f,0.1898);
	curLightSource->AddWavelength(492.5f,0.1898);
	curLightSource->AddWavelength(493.5f,0.189);
	curLightSource->AddWavelength(494.5f,0.206);
	curLightSource->AddWavelength(495.5f,0.1928);
	curLightSource->AddWavelength(496.5f,0.2019);
	curLightSource->AddWavelength(497.5f,0.202);
	curLightSource->AddWavelength(498.5f,0.1868);
	curLightSource->AddWavelength(499.5f,0.1972);
	curLightSource->AddWavelength(500.5f,0.1859);
	curLightSource->AddWavelength(501.5f,0.1814);
	curLightSource->AddWavelength(502.5f,0.1896);
	curLightSource->AddWavelength(503.5f,0.1936);
	curLightSource->AddWavelength(504.5f,0.1871);
	curLightSource->AddWavelength(505.5f,0.1995);
	curLightSource->AddWavelength(506.5f,0.1963);
	curLightSource->AddWavelength(507.5f,0.1908);
	curLightSource->AddWavelength(508.5f,0.1921);
	curLightSource->AddWavelength(509.5f,0.1918);
	curLightSource->AddWavelength(510.5f,0.1949);
	curLightSource->AddWavelength(511.5f,0.1999);
	curLightSource->AddWavelength(512.5f,0.1869);
	curLightSource->AddWavelength(513.5f,0.1863);
	curLightSource->AddWavelength(514.5f,0.1876);
	curLightSource->AddWavelength(515.5f,0.1902);
	curLightSource->AddWavelength(516.5f,0.1671);
	curLightSource->AddWavelength(517.5f,0.1728);
	curLightSource->AddWavelength(518.5f,0.1656);
	curLightSource->AddWavelength(519.5f,0.183);
	curLightSource->AddWavelength(520.5f,0.1833);
	curLightSource->AddWavelength(521.5f,0.1908);
	curLightSource->AddWavelength(522.5f,0.1825);
	curLightSource->AddWavelength(523.5f,0.1896);
	curLightSource->AddWavelength(524.5f,0.196);
	curLightSource->AddWavelength(525.5f,0.1932);
	curLightSource->AddWavelength(526.5f,0.1676);
	curLightSource->AddWavelength(527.5f,0.183);
	curLightSource->AddWavelength(528.5f,0.1899);
	curLightSource->AddWavelength(529.5f,0.192);
	curLightSource->AddWavelength(530.5f,0.1954);
	curLightSource->AddWavelength(531.5f,0.1965);
	curLightSource->AddWavelength(532.5f,0.1773);
	curLightSource->AddWavelength(533.5f,0.1925);
	curLightSource->AddWavelength(534.5f,0.186);
	curLightSource->AddWavelength(535.5f,0.1992);
	curLightSource->AddWavelength(536.5f,0.1873);
	curLightSource->AddWavelength(537.5f,0.1884);
	curLightSource->AddWavelength(538.5f,0.1906);
	curLightSource->AddWavelength(539.5f,0.1834);
	curLightSource->AddWavelength(540.5f,0.1772);
	curLightSource->AddWavelength(541.5f,0.1883);
	curLightSource->AddWavelength(542.5f,0.1827);
	curLightSource->AddWavelength(543.5f,0.1881);
	curLightSource->AddWavelength(544.5f,0.1881);
	curLightSource->AddWavelength(545.5f,0.1903);
	curLightSource->AddWavelength(546.5f,0.1881);
	curLightSource->AddWavelength(547.5f,0.1835);
	curLightSource->AddWavelength(548.5f,0.1865);
	curLightSource->AddWavelength(549.5f,0.1897);
	curLightSource->AddWavelength(550.5f,0.1864);
	curLightSource->AddWavelength(551.5f,0.1873);
	curLightSource->AddWavelength(552.5f,0.1848);
	curLightSource->AddWavelength(553.5f,0.1884);
	curLightSource->AddWavelength(554.5f,0.19);
	curLightSource->AddWavelength(555.5f,0.1899);
	curLightSource->AddWavelength(556.5f,0.1823);
	curLightSource->AddWavelength(557.5f,0.1848);
	curLightSource->AddWavelength(558.5f,0.1789);
	curLightSource->AddWavelength(559.5f,0.181);
	curLightSource->AddWavelength(560.5f,0.1845);
	curLightSource->AddWavelength(561.5f,0.1826);
	curLightSource->AddWavelength(562.5f,0.1852);
	curLightSource->AddWavelength(563.5f,0.1863);
	curLightSource->AddWavelength(564.5f,0.1856);
	curLightSource->AddWavelength(565.5f,0.18);
	curLightSource->AddWavelength(566.5f,0.1831);
	curLightSource->AddWavelength(567.5f,0.1889);
	curLightSource->AddWavelength(568.5f,0.1812);
	curLightSource->AddWavelength(569.5f,0.1862);
	curLightSource->AddWavelength(570.5f,0.1772);
	curLightSource->AddWavelength(571.5f,0.1825);
	curLightSource->AddWavelength(572.5f,0.1894);
	curLightSource->AddWavelength(573.5f,0.1878);
	curLightSource->AddWavelength(574.5f,0.1869);
	curLightSource->AddWavelength(575.5f,0.1832);
	curLightSource->AddWavelength(576.5f,0.1848);
	curLightSource->AddWavelength(577.5f,0.1859);
	curLightSource->AddWavelength(578.5f,0.1786);
	curLightSource->AddWavelength(579.5f,0.183);
	curLightSource->AddWavelength(580.5f,0.184);
	curLightSource->AddWavelength(581.5f,0.1855);
	curLightSource->AddWavelength(582.5f,0.1875);
	curLightSource->AddWavelength(583.5f,0.1859);
	curLightSource->AddWavelength(584.5f,0.1862);
	curLightSource->AddWavelength(585.5f,0.1786);
	curLightSource->AddWavelength(586.5f,0.1832);
	curLightSource->AddWavelength(587.5f,0.185);
	curLightSource->AddWavelength(588.5f,0.1752);
	curLightSource->AddWavelength(589.5f,0.1614);
	curLightSource->AddWavelength(590.5f,0.1815);
	curLightSource->AddWavelength(591.5f,0.1789);
	curLightSource->AddWavelength(592.5f,0.181);
	curLightSource->AddWavelength(593.5f,0.1798);
	curLightSource->AddWavelength(594.5f,0.1776);
	curLightSource->AddWavelength(595.5f,0.1785);
	curLightSource->AddWavelength(596.5f,0.1807);
	curLightSource->AddWavelength(597.5f,0.1783);
	curLightSource->AddWavelength(598.5f,0.176);
	curLightSource->AddWavelength(599.5f,0.1777);
	curLightSource->AddWavelength(600.5f,0.1748);
	curLightSource->AddWavelength(601.5f,0.1753);
	curLightSource->AddWavelength(602.5f,0.1721);
	curLightSource->AddWavelength(603.5f,0.1789);
	curLightSource->AddWavelength(604.5f,0.1779);
	curLightSource->AddWavelength(605.5f,0.1766);
	curLightSource->AddWavelength(606.5f,0.1762);
	curLightSource->AddWavelength(607.5f,0.176);
	curLightSource->AddWavelength(608.5f,0.1745);
	curLightSource->AddWavelength(609.5f,0.1746);
	curLightSource->AddWavelength(610.5f,0.1705);
	curLightSource->AddWavelength(611.5f,0.1748);
	curLightSource->AddWavelength(612.5f,0.1707);
	curLightSource->AddWavelength(613.5f,0.1685);
	curLightSource->AddWavelength(614.5f,0.1715);
	curLightSource->AddWavelength(615.5f,0.1715);
	curLightSource->AddWavelength(616.5f,0.1611);
	curLightSource->AddWavelength(617.5f,0.1709);
	curLightSource->AddWavelength(618.5f,0.1726);
	curLightSource->AddWavelength(619.5f,0.1709);
	curLightSource->AddWavelength(620.5f,0.1736);
	curLightSource->AddWavelength(621.5f,0.1692);
	curLightSource->AddWavelength(622.5f,0.1715);
	curLightSource->AddWavelength(623.5f,0.1668);
	curLightSource->AddWavelength(624.5f,0.1658);
	curLightSource->AddWavelength(625.5f,0.1634);
	curLightSource->AddWavelength(626.5f,0.1699);
	curLightSource->AddWavelength(627.5f,0.1699);
	curLightSource->AddWavelength(628.5f,0.1699);
	curLightSource->AddWavelength(629.5f,0.1679);
	curLightSource->AddWavelength(631.0f,0.1641);
	curLightSource->AddWavelength(633.0f,0.1653);
	curLightSource->AddWavelength(635.0f,0.1658);
	curLightSource->AddWavelength(637.0f,0.1656);
	curLightSource->AddWavelength(639.0f,0.1653);
	curLightSource->AddWavelength(641.0f,0.1616);
	curLightSource->AddWavelength(643.0f,0.1623);
	curLightSource->AddWavelength(645.0f,0.1629);
	curLightSource->AddWavelength(647.0f,0.1605);
	curLightSource->AddWavelength(649.0f,0.156);
	curLightSource->AddWavelength(651.0f,0.1608);
	curLightSource->AddWavelength(653.0f,0.1601);
	curLightSource->AddWavelength(655.0f,0.1534);
	curLightSource->AddWavelength(657.0f,0.1386);
	curLightSource->AddWavelength(659.0f,0.1551);
	curLightSource->AddWavelength(661.0f,0.1573);
	curLightSource->AddWavelength(663.0f,0.1557);
	curLightSource->AddWavelength(665.0f,0.1562);
	curLightSource->AddWavelength(667.0f,0.1537);
	curLightSource->AddWavelength(669.0f,0.1548);
	curLightSource->AddWavelength(671.0f,0.1518);
	curLightSource->AddWavelength(673.0f,0.1523);
	curLightSource->AddWavelength(675.0f,0.1512);
	curLightSource->AddWavelength(677.0f,0.151);
	curLightSource->AddWavelength(679.0f,0.15);
	curLightSource->AddWavelength(681.0f,0.1494);
	curLightSource->AddWavelength(683.0f,0.1481);
	curLightSource->AddWavelength(685.0f,0.1457);
	curLightSource->AddWavelength(687.0f,0.1469);
	curLightSource->AddWavelength(689.0f,0.1463);
	curLightSource->AddWavelength(691.0f,0.145);
	curLightSource->AddWavelength(693.0f,0.145);
	curLightSource->AddWavelength(695.0f,0.1438);
	curLightSource->AddWavelength(697.0f,0.1418);
	curLightSource->AddWavelength(699.0f,0.1427);
	curLightSource->AddWavelength(701.0f,0.1388);
	curLightSource->AddWavelength(703.0f,0.139);
	curLightSource->AddWavelength(705.0f,0.1417);
	curLightSource->AddWavelength(707.0f,0.1402);
	curLightSource->AddWavelength(709.0f,0.1386);
	curLightSource->AddWavelength(711.0f,0.1387);
	curLightSource->AddWavelength(713.0f,0.1375);
	curLightSource->AddWavelength(715.0f,0.1368);
	curLightSource->AddWavelength(717.0f,0.1355);
	curLightSource->AddWavelength(719.0f,0.1329);
	curLightSource->AddWavelength(721.0f,0.1332);
	curLightSource->AddWavelength(723.0f,0.1349);
	curLightSource->AddWavelength(725.0f,0.1351);
	curLightSource->AddWavelength(727.0f,0.1347);
	curLightSource->AddWavelength(729.0f,0.132);
	curLightSource->AddWavelength(731.0f,0.1327);
	curLightSource->AddWavelength(733.0f,0.1319);
	curLightSource->AddWavelength(735.0f,0.131);
	curLightSource->AddWavelength(737.0f,0.1308);
	curLightSource->AddWavelength(739.0f,0.1279);
	curLightSource->AddWavelength(741.0f,0.1259);
	curLightSource->AddWavelength(743.0f,0.1287);
	curLightSource->AddWavelength(745.0f,0.128);
	curLightSource->AddWavelength(747.0f,0.1284);
	curLightSource->AddWavelength(749.0f,0.1271);
	curLightSource->AddWavelength(751.0f,0.1263);
	curLightSource->AddWavelength(753.0f,0.126);
	curLightSource->AddWavelength(755.0f,0.1256);
	curLightSource->AddWavelength(757.0f,0.1249);
	curLightSource->AddWavelength(759.0f,0.1241);
	curLightSource->AddWavelength(761.0f,0.1238);
	curLightSource->AddWavelength(763.0f,0.1242);
	curLightSource->AddWavelength(765.0f,0.1222);
	curLightSource->AddWavelength(767.0f,0.1186);
	curLightSource->AddWavelength(769.0f,0.1204);
	curLightSource->AddWavelength(771.0f,0.1205);
	curLightSource->AddWavelength(773.0f,0.1209);
	curLightSource->AddWavelength(775.0f,0.1189);
	curLightSource->AddWavelength(777.0f,0.1197);
	curLightSource->AddWavelength(779.0f,0.1188);
	curLightSource->AddWavelength(781.0f,0.1188);
	curLightSource->AddWavelength(783.0f,0.1177);
	curLightSource->AddWavelength(785.0f,0.1181);
	curLightSource->AddWavelength(787.0f,0.1178);
	curLightSource->AddWavelength(789.0f,0.1175);
	curLightSource->AddWavelength(791.0f,0.1159);
	curLightSource->AddWavelength(793.0f,0.1144);
	curLightSource->AddWavelength(795.0f,0.1135);
	curLightSource->AddWavelength(797.0f,0.1153);
	curLightSource->AddWavelength(799.0f,0.1136);
	curLightSource->AddWavelength(801.0f,0.1143);
	curLightSource->AddWavelength(803.0f,0.113);
	curLightSource->AddWavelength(805.0f,0.1116);
	curLightSource->AddWavelength(807.0f,0.1121);
	curLightSource->AddWavelength(809.0f,0.1096);
	curLightSource->AddWavelength(811.0f,0.1115);
	curLightSource->AddWavelength(813.0f,0.1116);
	curLightSource->AddWavelength(815.0f,0.1108);
	curLightSource->AddWavelength(817.0f,0.1105);
	curLightSource->AddWavelength(819.0f,0.1065);
	curLightSource->AddWavelength(821.0f,0.1081);
	curLightSource->AddWavelength(823.0f,0.1074);
	curLightSource->AddWavelength(825.0f,0.1076);
	curLightSource->AddWavelength(827.0f,0.1077);
	curLightSource->AddWavelength(829.0f,0.1073);
	curLightSource->AddWavelength(831.0f,0.1069);
	curLightSource->AddWavelength(833.0f,0.1034);
	curLightSource->AddWavelength(835.0f,0.1053);
	curLightSource->AddWavelength(837.0f,0.1052);
	curLightSource->AddWavelength(839.0f,0.1042);
	curLightSource->AddWavelength(841.0f,0.1045);
	curLightSource->AddWavelength(843.0f,0.1028);
	curLightSource->AddWavelength(845.0f,0.1033);
	curLightSource->AddWavelength(847.0f,0.1025);
	curLightSource->AddWavelength(849.0f,0.0971);
	curLightSource->AddWavelength(851.0f,0.1003);
	curLightSource->AddWavelength(853.0f,0.0973);
	curLightSource->AddWavelength(855.0f,0.0877);
	curLightSource->AddWavelength(857.0f,0.1011);
	curLightSource->AddWavelength(859.0f,0.0997);
	curLightSource->AddWavelength(861.0f,0.0997);
	curLightSource->AddWavelength(863.0f,0.0999);
	curLightSource->AddWavelength(865.0f,0.097);
	curLightSource->AddWavelength(867.0f,0.088);
	curLightSource->AddWavelength(869.0f,0.0967);
	curLightSource->AddWavelength(871.0f,0.0986);
	curLightSource->AddWavelength(873.0f,0.0978);
	curLightSource->AddWavelength(875.0f,0.0981);
	curLightSource->AddWavelength(877.0f,0.0984);
	curLightSource->AddWavelength(879.0f,0.0959);
	curLightSource->AddWavelength(881.0f,0.096);
	curLightSource->AddWavelength(883.0f,0.0948);
	curLightSource->AddWavelength(885.0f,0.0963);
	curLightSource->AddWavelength(887.0f,0.0947);
	curLightSource->AddWavelength(889.0f,0.0949);
	curLightSource->AddWavelength(891.0f,0.0944);
	curLightSource->AddWavelength(893.0f,0.0934);
	curLightSource->AddWavelength(895.0f,0.0936);
	curLightSource->AddWavelength(897.0f,0.0939);
	curLightSource->AddWavelength(899.0f,0.0912);
	curLightSource->AddWavelength(901.0f,0.0905);
	curLightSource->AddWavelength(903.0f,0.0905);
	curLightSource->AddWavelength(905.0f,0.0893);
	curLightSource->AddWavelength(907.0f,0.0891);
	curLightSource->AddWavelength(909.0f,0.0861);
	curLightSource->AddWavelength(911.0f,0.087);
	curLightSource->AddWavelength(913.0f,0.0876);
	curLightSource->AddWavelength(915.0f,0.0866);
	curLightSource->AddWavelength(917.0f,0.0859);
	curLightSource->AddWavelength(919.0f,0.0858);
	curLightSource->AddWavelength(921.0f,0.083);
	curLightSource->AddWavelength(923.0f,0.0821);
	curLightSource->AddWavelength(925.0f,0.0825);
	curLightSource->AddWavelength(927.0f,0.0828);
	curLightSource->AddWavelength(929.0f,0.0833);
	curLightSource->AddWavelength(931.0f,0.0826);
	curLightSource->AddWavelength(933.0f,0.0832);
	curLightSource->AddWavelength(935.0f,0.0818);
	curLightSource->AddWavelength(937.0f,0.0802);
	curLightSource->AddWavelength(939.0f,0.0808);
	curLightSource->AddWavelength(941.0f,0.08);
	curLightSource->AddWavelength(943.0f,0.0784);
	curLightSource->AddWavelength(945.0f,0.0799);
	curLightSource->AddWavelength(947.0f,0.0793);
	curLightSource->AddWavelength(949.0f,0.0777);
	curLightSource->AddWavelength(951.0f,0.0778);
	curLightSource->AddWavelength(953.0f,0.0771);
	curLightSource->AddWavelength(955.0f,0.076);
	curLightSource->AddWavelength(957.0f,0.0774);
	curLightSource->AddWavelength(959.0f,0.0771);
	curLightSource->AddWavelength(961.0f,0.0767);
	curLightSource->AddWavelength(963.0f,0.0767);
	curLightSource->AddWavelength(965.0f,0.0764);
	curLightSource->AddWavelength(967.0f,0.0757);
	curLightSource->AddWavelength(969.0f,0.0776);
	curLightSource->AddWavelength(971.0f,0.0763);
	curLightSource->AddWavelength(973.0f,0.0764);
	curLightSource->AddWavelength(975.0f,0.075);
	curLightSource->AddWavelength(977.0f,0.0768);
	curLightSource->AddWavelength(979.0f,0.0768);
	curLightSource->AddWavelength(981.0f,0.0762);
	curLightSource->AddWavelength(983.0f,0.0766);
	curLightSource->AddWavelength(985.0f,0.0771);
	curLightSource->AddWavelength(987.0f,0.0756);
	curLightSource->AddWavelength(989.0f,0.0767);
	curLightSource->AddWavelength(991.0f,0.0764);
	curLightSource->AddWavelength(993.0f,0.0755);
	curLightSource->AddWavelength(995.0f,0.0756);
	curLightSource->AddWavelength(997.0f,0.0743);
	curLightSource->AddWavelength(999.0f,0.0743);
	curLightSource->AddWavelength(1002.5f,0.0745);
	curLightSource->AddWavelength(1007.5f,0.0737);
	curLightSource->AddWavelength(1012.5f,0.0734);
	curLightSource->AddWavelength(1017.5f,0.0721);
	curLightSource->AddWavelength(1022.5f,0.0704);
	curLightSource->AddWavelength(1027.5f,0.0708);
	curLightSource->AddWavelength(1032.5f,0.0688);
	curLightSource->AddWavelength(1037.5f,0.0692);
	curLightSource->AddWavelength(1042.5f,0.0681);
	curLightSource->AddWavelength(1047.5f,0.0685);
	curLightSource->AddWavelength(1052.5f,0.0661);
	curLightSource->AddWavelength(1057.5f,0.065);
	curLightSource->AddWavelength(1062.5f,0.0642);
	curLightSource->AddWavelength(1067.5f,0.0643);
	curLightSource->AddWavelength(1072.5f,0.0638);
	curLightSource->AddWavelength(1077.5f,0.063);
	curLightSource->AddWavelength(1082.5f,0.062);
	curLightSource->AddWavelength(1087.5f,0.0614);
	curLightSource->AddWavelength(1092.5f,0.0612);
	curLightSource->AddWavelength(1097.5f,0.0599);
	curLightSource->AddWavelength(1102.5f,0.0608);
	curLightSource->AddWavelength(1107.5f,0.0601);
	curLightSource->AddWavelength(1112.5f,0.0603);
	curLightSource->AddWavelength(1117.5f,0.0589);
	curLightSource->AddWavelength(1122.5f,0.0579);
	curLightSource->AddWavelength(1127.5f,0.0569);
	curLightSource->AddWavelength(1132.5f,0.0566);
	curLightSource->AddWavelength(1137.5f,0.0563);
	curLightSource->AddWavelength(1142.5f,0.0557);
	curLightSource->AddWavelength(1147.5f,0.0556);
	curLightSource->AddWavelength(1152.5f,0.0545);
	curLightSource->AddWavelength(1157.5f,0.0554);
	curLightSource->AddWavelength(1162.5f,0.054);
	curLightSource->AddWavelength(1167.5f,0.053);
	curLightSource->AddWavelength(1172.5f,0.0533);
	curLightSource->AddWavelength(1177.5f,0.0525);
	curLightSource->AddWavelength(1182.5f,0.0514);
	curLightSource->AddWavelength(1187.5f,0.0512);
	curLightSource->AddWavelength(1192.5f,0.0511);
	curLightSource->AddWavelength(1197.5f,0.0502);
	curLightSource->AddWavelength(1202.5f,0.0496);
	curLightSource->AddWavelength(1207.5f,0.0494);
	curLightSource->AddWavelength(1212.5f,0.0489);
	curLightSource->AddWavelength(1217.5f,0.05);
	curLightSource->AddWavelength(1222.5f,0.0481);
	curLightSource->AddWavelength(1227.5f,0.0481);
	curLightSource->AddWavelength(1232.5f,0.0484);
	curLightSource->AddWavelength(1237.5f,0.0477);
	curLightSource->AddWavelength(1242.5f,0.0477);
	curLightSource->AddWavelength(1247.5f,0.0466);
	curLightSource->AddWavelength(1252.5f,0.0474);
	curLightSource->AddWavelength(1257.5f,0.0463);
	curLightSource->AddWavelength(1262.5f,0.0444);
	curLightSource->AddWavelength(1267.5f,0.0438);
	curLightSource->AddWavelength(1272.5f,0.0439);
	curLightSource->AddWavelength(1277.5f,0.0453);
	curLightSource->AddWavelength(1282.5f,0.0435);
	curLightSource->AddWavelength(1287.5f,0.0437);
	curLightSource->AddWavelength(1292.5f,0.0442);
	curLightSource->AddWavelength(1297.5f,0.0438);
	curLightSource->AddWavelength(1302.5f,0.0438);
	curLightSource->AddWavelength(1307.5f,0.0429);
	curLightSource->AddWavelength(1312.5f,0.0419);
	curLightSource->AddWavelength(1317.5f,0.0416);
	curLightSource->AddWavelength(1322.5f,0.0416);
	curLightSource->AddWavelength(1327.5f,0.0411);
	curLightSource->AddWavelength(1332.5f,0.0405);
	curLightSource->AddWavelength(1337.5f,0.04);
	curLightSource->AddWavelength(1342.5f,0.0398);
	curLightSource->AddWavelength(1347.5f,0.0394);
	curLightSource->AddWavelength(1352.5f,0.0387);
	curLightSource->AddWavelength(1357.5f,0.0382);
	curLightSource->AddWavelength(1362.5f,0.0378);
	curLightSource->AddWavelength(1367.5f,0.037);
	curLightSource->AddWavelength(1372.5f,0.0369);
	curLightSource->AddWavelength(1377.5f,0.0368);
	curLightSource->AddWavelength(1382.5f,0.0364);
	curLightSource->AddWavelength(1387.5f,0.0364);
	curLightSource->AddWavelength(1392.5f,0.0358);
	curLightSource->AddWavelength(1397.5f,0.0357);
	curLightSource->AddWavelength(1402.5f,0.0353);
	curLightSource->AddWavelength(1407.5f,0.035);
	curLightSource->AddWavelength(1412.5f,0.0346);
	curLightSource->AddWavelength(1417.5f,0.0344);
	curLightSource->AddWavelength(1422.5f,0.0343);
	curLightSource->AddWavelength(1427.5f,0.0348);
	curLightSource->AddWavelength(1432.5f,0.0337);
	curLightSource->AddWavelength(1437.5f,0.0331);
	curLightSource->AddWavelength(1442.5f,0.0327);
	curLightSource->AddWavelength(1447.5f,0.0318);
	curLightSource->AddWavelength(1452.5f,0.0323);
	curLightSource->AddWavelength(1457.5f,0.0307);
	curLightSource->AddWavelength(1462.5f,0.0317);
	curLightSource->AddWavelength(1467.5f,0.0311);
	curLightSource->AddWavelength(1472.5f,0.0311);
	curLightSource->AddWavelength(1477.5f,0.0307);
	curLightSource->AddWavelength(1482.5f,0.0303);
	curLightSource->AddWavelength(1487.5f,0.0298);
	curLightSource->AddWavelength(1492.5f,0.0303);
	curLightSource->AddWavelength(1497.5f,0.03);
	curLightSource->AddWavelength(1502.5f,0.0296);
	curLightSource->AddWavelength(1507.5f,0.0295);
	curLightSource->AddWavelength(1512.5f,0.029);
	curLightSource->AddWavelength(1517.5f,0.029);
	curLightSource->AddWavelength(1522.5f,0.0286);
	curLightSource->AddWavelength(1527.5f,0.029);
	curLightSource->AddWavelength(1532.5f,0.0282);
	curLightSource->AddWavelength(1537.5f,0.0274);
	curLightSource->AddWavelength(1542.5f,0.0275);
	curLightSource->AddWavelength(1547.5f,0.0274);
	curLightSource->AddWavelength(1552.5f,0.0273);
	curLightSource->AddWavelength(1557.5f,0.0272);
	curLightSource->AddWavelength(1562.5f,0.0269);
	curLightSource->AddWavelength(1567.5f,0.0263);
	curLightSource->AddWavelength(1572.5f,0.026);
	curLightSource->AddWavelength(1577.5f,0.0259);
	curLightSource->AddWavelength(1582.5f,0.0255);
	curLightSource->AddWavelength(1587.5f,0.0252);
	curLightSource->AddWavelength(1592.5f,0.0246);
	curLightSource->AddWavelength(1597.5f,0.0246);
	curLightSource->AddWavelength(1602.5f,0.0247);
	curLightSource->AddWavelength(1607.5f,0.0242);
	curLightSource->AddWavelength(1612.5f,0.0244);
	curLightSource->AddWavelength(1617.5f,0.0243);
	curLightSource->AddWavelength(1622.5f,0.024);
	curLightSource->AddWavelength(1627.5f,0.0244);
	curLightSource->AddWavelength(1632.5f,0.0241);
	curLightSource->AddWavelength(1637.5f,0.0237);
	curLightSource->AddWavelength(1642.5f,0.0234);
	curLightSource->AddWavelength(1647.5f,0.0235);
	curLightSource->AddWavelength(1652.5f,0.0234);
	curLightSource->AddWavelength(1657.5f,0.0234);
	curLightSource->AddWavelength(1662.5f,0.0233);
	curLightSource->AddWavelength(1667.5f,0.0229);
	curLightSource->AddWavelength(1672.5f,0.0228);
	curLightSource->AddWavelength(1677.5f,0.022);
	curLightSource->AddWavelength(1682.5f,0.0221);
	curLightSource->AddWavelength(1687.5f,0.0219);
	curLightSource->AddWavelength(1692.5f,0.0219);
	curLightSource->AddWavelength(1697.5f,0.0214);
	curLightSource->AddWavelength(1702.5f,0.0217);
	curLightSource->AddWavelength(1707.5f,0.0212);
	curLightSource->AddWavelength(1712.5f,0.0203);
	curLightSource->AddWavelength(1717.5f,0.0212);
	curLightSource->AddWavelength(1722.5f,0.0205);
	curLightSource->AddWavelength(1727.5f,0.0196);
	curLightSource->AddWavelength(1732.5f,0.019);
	curLightSource->AddWavelength(1737.5f,0.0189);
	curLightSource->AddWavelength(1742.5f,0.0191);
	curLightSource->AddWavelength(1747.5f,0.0185);
	curLightSource->AddWavelength(1752.5f,0.0187);
	curLightSource->AddWavelength(1757.5f,0.0189);
	curLightSource->AddWavelength(1762.5f,0.0184);
	curLightSource->AddWavelength(1767.5f,0.0182);
	curLightSource->AddWavelength(1772.5f,0.0177);
	curLightSource->AddWavelength(1777.5f,0.0173);
	curLightSource->AddWavelength(1782.5f,0.0171);
	curLightSource->AddWavelength(1787.5f,0.017);
	curLightSource->AddWavelength(1792.5f,0.0169);
	curLightSource->AddWavelength(1797.5f,0.0173);
	curLightSource->AddWavelength(1802.5f,0.0169);
	curLightSource->AddWavelength(1807.5f,0.0168);
	curLightSource->AddWavelength(1812.5f,0.016);
	curLightSource->AddWavelength(1817.5f,0.016);
	curLightSource->AddWavelength(1822.5f,0.0159);
	curLightSource->AddWavelength(1827.5f,0.0156);
	curLightSource->AddWavelength(1832.5f,0.0156);
	curLightSource->AddWavelength(1837.5f,0.015);
	curLightSource->AddWavelength(1842.5f,0.0153);
	curLightSource->AddWavelength(1847.5f,0.0151);
	curLightSource->AddWavelength(1852.5f,0.0148);
	curLightSource->AddWavelength(1857.5f,0.0145);
	curLightSource->AddWavelength(1862.5f,0.0143);
	curLightSource->AddWavelength(1867.5f,0.0143);
	curLightSource->AddWavelength(1872.5f,0.0135);
	curLightSource->AddWavelength(1877.5f,0.0135);
	curLightSource->AddWavelength(1882.5f,0.014);
	curLightSource->AddWavelength(1887.5f,0.0138);
	curLightSource->AddWavelength(1892.5f,0.0137);
	curLightSource->AddWavelength(1897.5f,0.0138);
	curLightSource->AddWavelength(1902.5f,0.0133);
	curLightSource->AddWavelength(1907.5f,0.0136);
	curLightSource->AddWavelength(1912.5f,0.0138);
	curLightSource->AddWavelength(1917.5f,0.0136);
	curLightSource->AddWavelength(1922.5f,0.0134);
	curLightSource->AddWavelength(1927.5f,0.0132);
	curLightSource->AddWavelength(1932.5f,0.0132);
	curLightSource->AddWavelength(1937.5f,0.0131);
	curLightSource->AddWavelength(1942.5f,0.0129);
	curLightSource->AddWavelength(1947.5f,0.0127);
	curLightSource->AddWavelength(1952.5f,0.0126);
	curLightSource->AddWavelength(1957.5f,0.0122);
	curLightSource->AddWavelength(1962.5f,0.0126);
	curLightSource->AddWavelength(1967.5f,0.0125);
	curLightSource->AddWavelength(1972.5f,0.0125);
	curLightSource->AddWavelength(1977.5f,0.0129);
	curLightSource->AddWavelength(1982.5f,0.0125);
	curLightSource->AddWavelength(1987.5f,0.0123);
	curLightSource->AddWavelength(1992.5f,0.0121);
	curLightSource->AddWavelength(1997.5f,0.0123);
	curLightSource->AddWavelength(2002.5f,0.0116);
	curLightSource->AddWavelength(2012.5f,0.0114);
	curLightSource->AddWavelength(2022.5f,0.0113);
	curLightSource->AddWavelength(2032.5f,0.011);
	curLightSource->AddWavelength(2042.5f,0.0107);
	curLightSource->AddWavelength(2052.5f,0.0104);
	curLightSource->AddWavelength(2062.5f,0.01);
	curLightSource->AddWavelength(2077.5f,0.0101);
	curLightSource->AddWavelength(2092.5f,0.0098);
	curLightSource->AddWavelength(2107.5f,0.0093);
	curLightSource->AddWavelength(2122.5f,0.0087);
	curLightSource->AddWavelength(2137.5f,0.0085);
	curLightSource->AddWavelength(2152.5f,0.0081);
	curLightSource->AddWavelength(2167.5f,0.008);
	curLightSource->AddWavelength(2182.5f,0.0075);
	curLightSource->AddWavelength(2197.5f,0.0073);
	curLightSource->AddWavelength(2212.5f,0.0075);
	curLightSource->AddWavelength(2227.5f,0.0075);
	curLightSource->AddWavelength(2247.5f,0.0072);
	curLightSource->AddWavelength(2262.5f,0.0071);
	curLightSource->AddWavelength(2282.5f,0.0069);
	curLightSource->AddWavelength(2302.5f,0.0066);
	curLightSource->AddWavelength(2322.5f,0.0053);
	curLightSource->AddWavelength(2342.5f,0.0058);
	curLightSource->AddWavelength(2362.5f,0.0065);
	curLightSource->AddWavelength(2382.5f,0.0055);
	curLightSource->AddWavelength(2402.5f,0.0054);
	curLightSource->AddWavelength(2422.5f,0.0057);
	curLightSource->AddWavelength(2442.5f,0.0051);
	curLightSource->AddWavelength(2467.5f,0.0053);
	curLightSource->AddWavelength(2492.5f,0.0054);
	curLightSource->AddWavelength(2517.5f,0.0047);
	curLightSource->AddWavelength(2542.5f,0.0046);
	curLightSource->AddWavelength(2567.5f,0.0044);
	curLightSource->AddWavelength(2592.5f,0.0042);
	curLightSource->AddWavelength(2617.5f,0.0041);
	curLightSource->AddWavelength(2642.5f,0.0039);
	curLightSource->AddWavelength(2672.5f,0.0038);
	curLightSource->AddWavelength(2702.5f,0.0036);
	curLightSource->AddWavelength(2732.5f,0.0035);
	curLightSource->AddWavelength(2762.5f,0.0034);
	curLightSource->AddWavelength(2797.5f,0.0032);
	curLightSource->AddWavelength(2832.5f,0.0031);
	curLightSource->AddWavelength(2867.5f,0.0029);
	curLightSource->AddWavelength(2907.5f,0.0028);
	curLightSource->AddWavelength(2947.5f,0.0026);
	curLightSource->AddWavelength(2987.5f,0.0025);
	curLightSource->AddWavelength(3025.0f,0.0024);
	curLightSource->AddWavelength(3075.0f,0.0023);
	curLightSource->AddWavelength(3125.0f,0.0021);
	curLightSource->AddWavelength(3175.0f,0.002);
	curLightSource->AddWavelength(3235.0f,0.0019);
	curLightSource->AddWavelength(3295.0f,0.0018);
	curLightSource->AddWavelength(3355.0f,0.0016);
	curLightSource->AddWavelength(3425.0f,0.0015);
	curLightSource->AddWavelength(3495.0f,0.0014);
	curLightSource->AddWavelength(3575.0f,0.0013);
	curLightSource->AddWavelength(3665.0f,0.0012);
	curLightSource->AddWavelength(3755.0f,0.0011);
	curLightSource->AddWavelength(3855.0f,0.001);
	curLightSource->AddWavelength(3965.0f,0.0009);
	curLightSource->AddWavelength(4085.0f,0.0008);
	curLightSource->AddWavelength(4225.0f,0.0007);
	curLightSource->AddWavelength(4385.0f,0.0006);
	curLightSource->AddWavelength(4575.0f,0.0005);
	curLightSource->AddWavelength(4805.0f,0.0004);
	curLightSource->AddWavelength(5085.0f,0.0003);
	curLightSource->AddWavelength(5445.0f,0.0002);
	curLightSource->AddWavelength(5925.0f,0.0002);
	curLightSource->AddWavelength(6615.0f,0.0001);
	curLightSource->AddWavelength(7785.0f,0.0001);
	curLightSource->AddWavelength(10075.0f,0);

	TransferToGUI();
	
	grdDataTable->AutoSizeColumns();
	/*
	wxSize size = grdDataTable->GetSize();
	size.SetWidth(280 + wxSystemSettings::GetMetric(wxSYS_VSCROLL_X));
	bxszrDataPlot->Layout();
	*/
//	bxszrLColumn->Layout();
//	szrPrimary->Layout();
}

void simSpectraPanel::SetAM15(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	if (curLightSource == nullptr)
		return;

	curLightSource->name = "AM 1.5 Direct Normal";
	ClearWavelengths(event);
	curLightSource->inputType = SPECTRUM_INTENS_DERIV;
	
	curLightSource->AddWavelength(280.0f,2.5361E-27);
	curLightSource->AddWavelength(280.5f,1.0917E-25);
	curLightSource->AddWavelength(281.0f,6.1253E-25);
	curLightSource->AddWavelength(281.5f,2.7479E-23);
	curLightSource->AddWavelength(282.0f,2.8346E-22);
	curLightSource->AddWavelength(282.5f,1.3271E-21);
	curLightSource->AddWavelength(283.0f,6.7646E-21);
	curLightSource->AddWavelength(283.5f,1.4614E-20);
	curLightSource->AddWavelength(284.0f,4.9838E-19);
	curLightSource->AddWavelength(284.5f,2.1624E-18);
	curLightSource->AddWavelength(285.0f,8.9998E-18);
	curLightSource->AddWavelength(285.5f,6.4424E-17);
	curLightSource->AddWavelength(286.0f,2.3503E-16);
	curLightSource->AddWavelength(286.5f,1.8458E-15);
	curLightSource->AddWavelength(287.0f,7.2547E-15);
	curLightSource->AddWavelength(287.5f,3.6618E-14);
	curLightSource->AddWavelength(288.0f,2.8061E-13);
	curLightSource->AddWavelength(288.5f,9.0651E-13);
	curLightSource->AddWavelength(289.0f,3.4978E-12);
	curLightSource->AddWavelength(289.5f,1.5368E-11);
	curLightSource->AddWavelength(290.0f,5.1454E-11);
	curLightSource->AddWavelength(290.5f,1.3303E-10);
	curLightSource->AddWavelength(291.0f,3.8965E-10);
	curLightSource->AddWavelength(291.5f,1.4425E-09);
	curLightSource->AddWavelength(292.0f,4.0789E-09);
	curLightSource->AddWavelength(292.5f,7.0414E-09);
	curLightSource->AddWavelength(293.0f,1.5760E-08);
	curLightSource->AddWavelength(293.5f,4.7095E-08);
	curLightSource->AddWavelength(294.0f,9.4558E-08);
	curLightSource->AddWavelength(294.5f,1.5965E-07);
	curLightSource->AddWavelength(295.0f,3.2246E-07);
	curLightSource->AddWavelength(295.5f,8.0206E-07);
	curLightSource->AddWavelength(296.0f,1.4737E-06);
	curLightSource->AddWavelength(296.5f,2.3312E-06);
	curLightSource->AddWavelength(297.0f,3.3187E-06);
	curLightSource->AddWavelength(297.5f,6.7912E-06);
	curLightSource->AddWavelength(298.0f,1.1127E-05);
	curLightSource->AddWavelength(298.5f,1.4270E-05);
	curLightSource->AddWavelength(299.0f,2.0323E-05);
	curLightSource->AddWavelength(299.5f,3.7386E-05);
	curLightSource->AddWavelength(300.0f,4.5631E-05);
	curLightSource->AddWavelength(300.5f,5.7207E-05);
	curLightSource->AddWavelength(301.0f,9.1926E-05);
	curLightSource->AddWavelength(301.5f,1.3201E-04);
	curLightSource->AddWavelength(302.0f,1.4570E-04);
	curLightSource->AddWavelength(302.5f,2.1910E-04);
	curLightSource->AddWavelength(303.0f,3.7332E-04);
	curLightSource->AddWavelength(303.5f,4.8044E-04);
	curLightSource->AddWavelength(304.0f,5.0973E-04);
	curLightSource->AddWavelength(304.5f,6.4675E-04);
	curLightSource->AddWavelength(305.0f,8.9336E-04);
	curLightSource->AddWavelength(305.5f,1.0186E-03);
	curLightSource->AddWavelength(306.0f,1.0150E-03);
	curLightSource->AddWavelength(306.5f,1.1568E-03);
	curLightSource->AddWavelength(307.0f,1.5246E-03);
	curLightSource->AddWavelength(307.5f,1.9468E-03);
	curLightSource->AddWavelength(308.0f,2.0753E-03);
	curLightSource->AddWavelength(308.5f,2.2750E-03);
	curLightSource->AddWavelength(309.0f,2.2298E-03);
	curLightSource->AddWavelength(309.5f,2.3672E-03);
	curLightSource->AddWavelength(310.0f,2.7826E-03);
	curLightSource->AddWavelength(310.5f,3.5879E-03);
	curLightSource->AddWavelength(311.0f,4.5392E-03);
	curLightSource->AddWavelength(311.5f,4.6156E-03);
	curLightSource->AddWavelength(312.0f,5.0898E-03);
	curLightSource->AddWavelength(312.5f,5.3766E-03);
	curLightSource->AddWavelength(313.0f,5.8323E-03);
	curLightSource->AddWavelength(313.5f,5.8999E-03);
	curLightSource->AddWavelength(314.0f,6.5266E-03);
	curLightSource->AddWavelength(314.5f,7.0476E-03);
	curLightSource->AddWavelength(315.0f,7.3686E-03);
	curLightSource->AddWavelength(315.5f,6.4833E-03);
	curLightSource->AddWavelength(316.0f,6.7088E-03);
	curLightSource->AddWavelength(316.5f,8.1118E-03);
	curLightSource->AddWavelength(317.0f,9.3020E-03);
	curLightSource->AddWavelength(317.5f,9.9712E-03);
	curLightSource->AddWavelength(318.0f,9.5815E-03);
	curLightSource->AddWavelength(318.5f,1.0005E-02);
	curLightSource->AddWavelength(319.0f,1.0971E-02);
	curLightSource->AddWavelength(319.5f,1.0693E-02);
	curLightSource->AddWavelength(320.0f,1.1277E-02);
	curLightSource->AddWavelength(320.5f,1.3305E-02);
	curLightSource->AddWavelength(321.0f,1.3414E-02);
	curLightSource->AddWavelength(321.5f,1.2817E-02);
	curLightSource->AddWavelength(322.0f,1.2200E-02);
	curLightSource->AddWavelength(322.5f,1.1970E-02);
	curLightSource->AddWavelength(323.0f,1.1623E-02);
	curLightSource->AddWavelength(323.5f,1.3393E-02);
	curLightSource->AddWavelength(324.0f,1.4852E-02);
	curLightSource->AddWavelength(324.5f,1.5467E-02);
	curLightSource->AddWavelength(325.0f,1.5504E-02);
	curLightSource->AddWavelength(325.5f,1.7936E-02);
	curLightSource->AddWavelength(326.0f,2.0868E-02);
	curLightSource->AddWavelength(326.5f,2.2162E-02);
	curLightSource->AddWavelength(327.0f,2.1834E-02);
	curLightSource->AddWavelength(327.5f,2.1285E-02);
	curLightSource->AddWavelength(328.0f,1.9773E-02);
	curLightSource->AddWavelength(328.5f,2.0675E-02);
	curLightSource->AddWavelength(329.0f,2.3297E-02);
	curLightSource->AddWavelength(329.5f,2.5864E-02);
	curLightSource->AddWavelength(330.0f,2.6192E-02);
	curLightSource->AddWavelength(330.5f,2.4103E-02);
	curLightSource->AddWavelength(331.0f,2.2835E-02);
	curLightSource->AddWavelength(331.5f,2.3635E-02);
	curLightSource->AddWavelength(332.0f,2.4508E-02);
	curLightSource->AddWavelength(332.5f,2.4655E-02);
	curLightSource->AddWavelength(333.0f,2.4263E-02);
	curLightSource->AddWavelength(333.5f,2.3265E-02);
	curLightSource->AddWavelength(334.0f,2.3823E-02);
	curLightSource->AddWavelength(334.5f,2.5434E-02);
	curLightSource->AddWavelength(335.0f,2.6477E-02);
	curLightSource->AddWavelength(335.5f,2.5894E-02);
	curLightSource->AddWavelength(336.0f,2.3813E-02);
	curLightSource->AddWavelength(336.5f,2.2099E-02);
	curLightSource->AddWavelength(337.0f,2.1767E-02);
	curLightSource->AddWavelength(337.5f,2.3434E-02);
	curLightSource->AddWavelength(338.0f,2.5321E-02);
	curLightSource->AddWavelength(338.5f,2.6549E-02);
	curLightSource->AddWavelength(339.0f,2.7096E-02);
	curLightSource->AddWavelength(339.5f,2.7847E-02);
	curLightSource->AddWavelength(340.0f,2.9659E-02);
	curLightSource->AddWavelength(340.5f,2.9674E-02);
	curLightSource->AddWavelength(341.0f,2.7932E-02);
	curLightSource->AddWavelength(341.5f,2.7853E-02);
	curLightSource->AddWavelength(342.0f,2.9121E-02);
	curLightSource->AddWavelength(342.5f,3.0296E-02);
	curLightSource->AddWavelength(343.0f,3.0857E-02);
	curLightSource->AddWavelength(343.5f,2.9246E-02);
	curLightSource->AddWavelength(344.0f,2.5352E-02);
	curLightSource->AddWavelength(344.5f,2.4439E-02);
	curLightSource->AddWavelength(345.0f,2.7854E-02);
	curLightSource->AddWavelength(345.5f,2.9761E-02);
	curLightSource->AddWavelength(346.0f,2.9132E-02);
	curLightSource->AddWavelength(346.5f,2.9747E-02);
	curLightSource->AddWavelength(347.0f,3.0318E-02);
	curLightSource->AddWavelength(347.5f,2.9351E-02);
	curLightSource->AddWavelength(348.0f,2.9306E-02);
	curLightSource->AddWavelength(348.5f,2.9884E-02);
	curLightSource->AddWavelength(349.0f,2.8864E-02);
	curLightSource->AddWavelength(349.5f,2.9720E-02);
	curLightSource->AddWavelength(350.0f,3.2913E-02);
	curLightSource->AddWavelength(350.5f,3.5471E-02);
	curLightSource->AddWavelength(351.0f,3.4603E-02);
	curLightSource->AddWavelength(351.5f,3.3388E-02);
	curLightSource->AddWavelength(352.0f,3.2674E-02);
	curLightSource->AddWavelength(352.5f,3.0954E-02);
	curLightSource->AddWavelength(353.0f,3.2975E-02);
	curLightSource->AddWavelength(353.5f,3.6351E-02);
	curLightSource->AddWavelength(354.0f,3.8523E-02);
	curLightSource->AddWavelength(354.5f,3.9043E-02);
	curLightSource->AddWavelength(355.0f,3.9140E-02);
	curLightSource->AddWavelength(355.5f,3.7878E-02);
	curLightSource->AddWavelength(356.0f,3.5627E-02);
	curLightSource->AddWavelength(356.5f,3.3495E-02);
	curLightSource->AddWavelength(357.0f,2.9527E-02);
	curLightSource->AddWavelength(357.5f,2.9950E-02);
	curLightSource->AddWavelength(358.0f,2.7936E-02);
	curLightSource->AddWavelength(358.5f,2.5998E-02);
	curLightSource->AddWavelength(359.0f,3.0650E-02);
	curLightSource->AddWavelength(359.5f,3.7013E-02);
	curLightSource->AddWavelength(360.0f,3.9240E-02);
	curLightSource->AddWavelength(360.5f,3.7167E-02);
	curLightSource->AddWavelength(361.0f,3.4278E-02);
	curLightSource->AddWavelength(361.5f,3.3647E-02);
	curLightSource->AddWavelength(362.0f,3.5350E-02);
	curLightSource->AddWavelength(362.5f,3.8804E-02);
	curLightSource->AddWavelength(363.0f,4.0005E-02);
	curLightSource->AddWavelength(363.5f,3.8994E-02);
	curLightSource->AddWavelength(364.0f,4.0472E-02);
	curLightSource->AddWavelength(364.5f,4.0179E-02);
	curLightSource->AddWavelength(365.0f,4.1810E-02);
	curLightSource->AddWavelength(365.5f,4.6110E-02);
	curLightSource->AddWavelength(366.0f,4.9508E-02);
	curLightSource->AddWavelength(366.5f,4.9694E-02);
	curLightSource->AddWavelength(367.0f,4.8869E-02);
	curLightSource->AddWavelength(367.5f,4.8041E-02);
	curLightSource->AddWavelength(368.0f,4.5319E-02);
	curLightSource->AddWavelength(368.5f,4.5106E-02);
	curLightSource->AddWavelength(369.0f,4.7244E-02);
	curLightSource->AddWavelength(369.5f,5.0856E-02);
	curLightSource->AddWavelength(370.0f,5.1666E-02);
	curLightSource->AddWavelength(370.5f,4.6798E-02);
	curLightSource->AddWavelength(371.0f,4.7628E-02);
	curLightSource->AddWavelength(371.5f,4.9587E-02);
	curLightSource->AddWavelength(372.0f,4.6506E-02);
	curLightSource->AddWavelength(372.5f,4.4389E-02);
	curLightSource->AddWavelength(373.0f,4.2833E-02);
	curLightSource->AddWavelength(373.5f,3.8682E-02);
	curLightSource->AddWavelength(374.0f,3.8651E-02);
	curLightSource->AddWavelength(374.5f,3.8435E-02);
	curLightSource->AddWavelength(375.0f,4.1087E-02);
	curLightSource->AddWavelength(375.5f,4.5514E-02);
	curLightSource->AddWavelength(376.0f,4.7218E-02);
	curLightSource->AddWavelength(376.5f,4.6538E-02);
	curLightSource->AddWavelength(377.0f,5.0014E-02);
	curLightSource->AddWavelength(377.5f,5.5890E-02);
	curLightSource->AddWavelength(378.0f,6.0314E-02);
	curLightSource->AddWavelength(378.5f,5.8892E-02);
	curLightSource->AddWavelength(379.0f,5.2616E-02);
	curLightSource->AddWavelength(379.5f,4.7255E-02);
	curLightSource->AddWavelength(380.0f,4.9751E-02);
	curLightSource->AddWavelength(380.5f,5.3396E-02);
	curLightSource->AddWavelength(381.0f,5.4424E-02);
	curLightSource->AddWavelength(381.5f,4.9135E-02);
	curLightSource->AddWavelength(382.0f,4.1958E-02);
	curLightSource->AddWavelength(382.5f,3.6361E-02);
	curLightSource->AddWavelength(383.0f,3.2648E-02);
	curLightSource->AddWavelength(383.5f,3.1658E-02);
	curLightSource->AddWavelength(384.0f,3.6689E-02);
	curLightSource->AddWavelength(384.5f,4.4239E-02);
	curLightSource->AddWavelength(385.0f,4.8638E-02);
	curLightSource->AddWavelength(385.5f,4.6549E-02);
	curLightSource->AddWavelength(386.0f,4.4984E-02);
	curLightSource->AddWavelength(386.5f,4.6848E-02);
	curLightSource->AddWavelength(387.0f,4.7343E-02);
	curLightSource->AddWavelength(387.5f,4.6731E-02);
	curLightSource->AddWavelength(388.0f,4.6350E-02);
	curLightSource->AddWavelength(388.5f,4.6096E-02);
	curLightSource->AddWavelength(389.0f,5.0121E-02);
	curLightSource->AddWavelength(389.5f,5.5637E-02);
	curLightSource->AddWavelength(390.0f,5.8457E-02);
	curLightSource->AddWavelength(390.5f,5.9038E-02);
	curLightSource->AddWavelength(391.0f,6.2634E-02);
	curLightSource->AddWavelength(391.5f,6.3617E-02);
	curLightSource->AddWavelength(392.0f,5.8656E-02);
	curLightSource->AddWavelength(392.5f,4.8961E-02);
	curLightSource->AddWavelength(393.0f,3.5502E-02);
	curLightSource->AddWavelength(393.5f,2.8272E-02);
	curLightSource->AddWavelength(394.0f,3.6780E-02);
	curLightSource->AddWavelength(394.5f,5.0811E-02);
	curLightSource->AddWavelength(395.0f,6.0096E-02);
	curLightSource->AddWavelength(395.5f,6.4101E-02);
	curLightSource->AddWavelength(396.0f,5.6443E-02);
	curLightSource->AddWavelength(396.5f,4.1101E-02);
	curLightSource->AddWavelength(397.0f,3.1882E-02);
	curLightSource->AddWavelength(397.5f,4.7151E-02);
	curLightSource->AddWavelength(398.0f,6.3944E-02);
	curLightSource->AddWavelength(398.5f,7.5622E-02);
	curLightSource->AddWavelength(399.0f,8.0408E-02);
	curLightSource->AddWavelength(399.5f,8.2980E-02);
	curLightSource->AddWavelength(400.0f,8.3989E-02);
	curLightSource->AddWavelength(401.0f,8.7691E-02);
	curLightSource->AddWavelength(402.0f,9.1387E-02);
	curLightSource->AddWavelength(403.0f,8.8211E-02);
	curLightSource->AddWavelength(404.0f,8.9849E-02);
	curLightSource->AddWavelength(405.0f,8.7849E-02);
	curLightSource->AddWavelength(406.0f,8.5878E-02);
	curLightSource->AddWavelength(407.0f,8.4545E-02);
	curLightSource->AddWavelength(408.0f,8.8488E-02);
	curLightSource->AddWavelength(409.0f,9.4717E-02);
	curLightSource->AddWavelength(410.0f,8.0910E-02);
	curLightSource->AddWavelength(411.0f,9.0770E-02);
	curLightSource->AddWavelength(412.0f,9.6686E-02);
	curLightSource->AddWavelength(413.0f,9.2951E-02);
	curLightSource->AddWavelength(414.0f,9.2134E-02);
	curLightSource->AddWavelength(415.0f,9.5569E-02);
	curLightSource->AddWavelength(416.0f,9.8628E-02);
	curLightSource->AddWavelength(417.0f,9.6392E-02);
	curLightSource->AddWavelength(418.0f,9.2392E-02);
	curLightSource->AddWavelength(419.0f,9.6354E-02);
	curLightSource->AddWavelength(420.0f,8.8467E-02);
	curLightSource->AddWavelength(421.0f,1.0067E-01);
	curLightSource->AddWavelength(422.0f,9.9499E-02);
	curLightSource->AddWavelength(423.0f,9.6531E-02);
	curLightSource->AddWavelength(424.0f,9.6182E-02);
	curLightSource->AddWavelength(425.0f,9.9312E-02);
	curLightSource->AddWavelength(426.0f,9.6667E-02);
	curLightSource->AddWavelength(427.0f,9.3550E-02);
	curLightSource->AddWavelength(428.0f,9.4625E-02);
	curLightSource->AddWavelength(429.0f,8.7766E-02);
	curLightSource->AddWavelength(430.0f,7.0134E-02);
	curLightSource->AddWavelength(431.0f,6.3779E-02);
	curLightSource->AddWavelength(432.0f,1.0628E-01);
	curLightSource->AddWavelength(433.0f,9.9050E-02);
	curLightSource->AddWavelength(434.0f,9.1653E-02);
	curLightSource->AddWavelength(435.0f,1.0070E-01);
	curLightSource->AddWavelength(436.0f,1.1061E-01);
	curLightSource->AddWavelength(437.0f,1.1306E-01);
	curLightSource->AddWavelength(438.0f,9.9368E-02);
	curLightSource->AddWavelength(439.0f,9.5753E-02);
	curLightSource->AddWavelength(440.0f,1.0993E-01);
	curLightSource->AddWavelength(441.0f,1.0859E-01);
	curLightSource->AddWavelength(442.0f,1.1640E-01);
	curLightSource->AddWavelength(443.0f,1.1823E-01);
	curLightSource->AddWavelength(444.0f,1.1537E-01);
	curLightSource->AddWavelength(445.0f,1.1992E-01);
	curLightSource->AddWavelength(446.0f,1.0766E-01);
	curLightSource->AddWavelength(447.0f,1.2257E-01);
	curLightSource->AddWavelength(448.0f,1.2422E-01);
	curLightSource->AddWavelength(449.0f,1.2409E-01);
	curLightSource->AddWavelength(450.0f,1.2881E-01);
	curLightSource->AddWavelength(451.0f,1.3376E-01);
	curLightSource->AddWavelength(452.0f,1.2822E-01);
	curLightSource->AddWavelength(453.0f,1.1854E-01);
	curLightSource->AddWavelength(454.0f,1.2730E-01);
	curLightSource->AddWavelength(455.0f,1.2655E-01);
	curLightSource->AddWavelength(456.0f,1.3088E-01);
	curLightSource->AddWavelength(457.0f,1.3213E-01);
	curLightSource->AddWavelength(458.0f,1.2946E-01);
	curLightSource->AddWavelength(459.0f,1.2859E-01);
	curLightSource->AddWavelength(460.0f,1.2791E-01);
	curLightSource->AddWavelength(461.0f,1.3255E-01);
	curLightSource->AddWavelength(462.0f,1.3392E-01);
	curLightSource->AddWavelength(463.0f,1.3452E-01);
	curLightSource->AddWavelength(464.0f,1.3055E-01);
	curLightSource->AddWavelength(465.0f,1.2905E-01);
	curLightSource->AddWavelength(466.0f,1.3190E-01);
	curLightSource->AddWavelength(467.0f,1.2616E-01);
	curLightSource->AddWavelength(468.0f,1.3178E-01);
	curLightSource->AddWavelength(469.0f,1.3247E-01);
	curLightSource->AddWavelength(470.0f,1.2749E-01);
	curLightSource->AddWavelength(471.0f,1.2975E-01);
	curLightSource->AddWavelength(472.0f,1.3661E-01);
	curLightSource->AddWavelength(473.0f,1.3144E-01);
	curLightSource->AddWavelength(474.0f,1.3304E-01);
	curLightSource->AddWavelength(475.0f,1.3755E-01);
	curLightSource->AddWavelength(476.0f,1.3299E-01);
	curLightSource->AddWavelength(477.0f,1.3392E-01);
	curLightSource->AddWavelength(478.0f,1.3839E-01);
	curLightSource->AddWavelength(479.0f,1.3586E-01);
	curLightSource->AddWavelength(480.0f,1.3825E-01);
	curLightSource->AddWavelength(481.0f,1.3836E-01);
	curLightSource->AddWavelength(482.0f,1.3899E-01);
	curLightSource->AddWavelength(483.0f,1.3742E-01);
	curLightSource->AddWavelength(484.0f,1.3492E-01);
	curLightSource->AddWavelength(485.0f,1.3457E-01);
	curLightSource->AddWavelength(486.0f,1.0918E-01);
	curLightSource->AddWavelength(487.0f,1.2235E-01);
	curLightSource->AddWavelength(488.0f,1.3252E-01);
	curLightSource->AddWavelength(489.0f,1.2492E-01);
	curLightSource->AddWavelength(490.0f,1.3968E-01);
	curLightSource->AddWavelength(491.0f,1.3435E-01);
	curLightSource->AddWavelength(492.0f,1.2818E-01);
	curLightSource->AddWavelength(493.0f,1.3719E-01);
	curLightSource->AddWavelength(494.0f,1.3402E-01);
	curLightSource->AddWavelength(495.0f,1.4238E-01);
	curLightSource->AddWavelength(496.0f,1.3548E-01);
	curLightSource->AddWavelength(497.0f,1.3788E-01);
	curLightSource->AddWavelength(498.0f,1.3421E-01);
	curLightSource->AddWavelength(499.0f,1.3429E-01);
	curLightSource->AddWavelength(500.0f,1.3391E-01);
	curLightSource->AddWavelength(501.0f,1.2990E-01);
	curLightSource->AddWavelength(502.0f,1.2991E-01);
	curLightSource->AddWavelength(503.0f,1.3597E-01);
	curLightSource->AddWavelength(504.0f,1.2682E-01);
	curLightSource->AddWavelength(505.0f,1.3598E-01);
	curLightSource->AddWavelength(506.0f,1.4153E-01);
	curLightSource->AddWavelength(507.0f,1.3548E-01);
	curLightSource->AddWavelength(508.0f,1.3210E-01);
	curLightSource->AddWavelength(509.0f,1.3850E-01);
	curLightSource->AddWavelength(510.0f,1.3497E-01);
	curLightSource->AddWavelength(511.0f,1.3753E-01);
	curLightSource->AddWavelength(512.0f,1.4125E-01);
	curLightSource->AddWavelength(513.0f,1.3277E-01);
	curLightSource->AddWavelength(514.0f,1.3003E-01);
	curLightSource->AddWavelength(515.0f,1.3385E-01);
	curLightSource->AddWavelength(516.0f,1.3514E-01);
	curLightSource->AddWavelength(517.0f,1.1017E-01);
	curLightSource->AddWavelength(518.0f,1.2605E-01);
	curLightSource->AddWavelength(519.0f,1.2222E-01);
	curLightSource->AddWavelength(520.0f,1.3349E-01);
	curLightSource->AddWavelength(521.0f,1.3452E-01);
	curLightSource->AddWavelength(522.0f,1.3760E-01);
	curLightSource->AddWavelength(523.0f,1.2976E-01);
	curLightSource->AddWavelength(524.0f,1.3962E-01);
	curLightSource->AddWavelength(525.0f,1.3859E-01);
	curLightSource->AddWavelength(526.0f,1.3479E-01);
	curLightSource->AddWavelength(527.0f,1.1795E-01);
	curLightSource->AddWavelength(528.0f,1.3508E-01);
	curLightSource->AddWavelength(529.0f,1.4142E-01);
	curLightSource->AddWavelength(530.0f,1.3598E-01);
	curLightSource->AddWavelength(531.0f,1.4348E-01);
	curLightSource->AddWavelength(532.0f,1.4094E-01);
	curLightSource->AddWavelength(533.0f,1.2590E-01);
	curLightSource->AddWavelength(534.0f,1.3491E-01);
	curLightSource->AddWavelength(535.0f,1.3701E-01);
	curLightSource->AddWavelength(536.0f,1.4292E-01);
	curLightSource->AddWavelength(537.0f,1.3229E-01);
	curLightSource->AddWavelength(538.0f,1.3896E-01);
	curLightSource->AddWavelength(539.0f,1.3558E-01);
	curLightSource->AddWavelength(540.0f,1.3096E-01);
	curLightSource->AddWavelength(541.0f,1.2595E-01);
	curLightSource->AddWavelength(542.0f,1.3714E-01);
	curLightSource->AddWavelength(543.0f,1.3493E-01);
	curLightSource->AddWavelength(544.0f,1.3971E-01);
	curLightSource->AddWavelength(545.0f,1.3657E-01);
	curLightSource->AddWavelength(546.0f,1.3536E-01);
	curLightSource->AddWavelength(547.0f,1.3717E-01);
	curLightSource->AddWavelength(548.0f,1.3331E-01);
	curLightSource->AddWavelength(549.0f,1.3752E-01);
	curLightSource->AddWavelength(550.0f,1.3648E-01);
	curLightSource->AddWavelength(551.0f,1.3639E-01);
	curLightSource->AddWavelength(552.0f,1.3923E-01);
	curLightSource->AddWavelength(553.0f,1.3533E-01);
	curLightSource->AddWavelength(554.0f,1.3802E-01);
	curLightSource->AddWavelength(555.0f,1.3883E-01);
	curLightSource->AddWavelength(556.0f,1.3651E-01);
	curLightSource->AddWavelength(557.0f,1.3321E-01);
	curLightSource->AddWavelength(558.0f,1.3613E-01);
	curLightSource->AddWavelength(559.0f,1.2885E-01);
	curLightSource->AddWavelength(560.0f,1.3118E-01);
	curLightSource->AddWavelength(561.0f,1.3885E-01);
	curLightSource->AddWavelength(562.0f,1.3225E-01);
	curLightSource->AddWavelength(563.0f,1.3731E-01);
	curLightSource->AddWavelength(564.0f,1.3466E-01);
	curLightSource->AddWavelength(565.0f,1.3555E-01);
	curLightSource->AddWavelength(566.0f,1.2823E-01);
	curLightSource->AddWavelength(567.0f,1.3673E-01);
	curLightSource->AddWavelength(568.0f,1.3554E-01);
	curLightSource->AddWavelength(569.0f,1.3228E-01);
	curLightSource->AddWavelength(570.0f,1.3240E-01);
	curLightSource->AddWavelength(571.0f,1.2810E-01);
	curLightSource->AddWavelength(572.0f,1.3534E-01);
	curLightSource->AddWavelength(573.0f,1.3595E-01);
	curLightSource->AddWavelength(574.0f,1.3527E-01);
	curLightSource->AddWavelength(575.0f,1.3225E-01);
	curLightSource->AddWavelength(576.0f,1.3118E-01);
	curLightSource->AddWavelength(577.0f,1.3452E-01);
	curLightSource->AddWavelength(578.0f,1.3040E-01);
	curLightSource->AddWavelength(579.0f,1.3230E-01);
	curLightSource->AddWavelength(580.0f,1.3455E-01);
	curLightSource->AddWavelength(581.0f,1.3518E-01);
	curLightSource->AddWavelength(582.0f,1.3729E-01);
	curLightSource->AddWavelength(583.0f,1.3872E-01);
	curLightSource->AddWavelength(584.0f,1.3845E-01);
	curLightSource->AddWavelength(585.0f,1.3737E-01);
	curLightSource->AddWavelength(586.0f,1.3409E-01);
	curLightSource->AddWavelength(587.0f,1.3708E-01);
	curLightSource->AddWavelength(588.0f,1.3403E-01);
	curLightSource->AddWavelength(589.0f,1.1582E-01);
	curLightSource->AddWavelength(590.0f,1.2316E-01);
	curLightSource->AddWavelength(591.0f,1.3171E-01);
	curLightSource->AddWavelength(592.0f,1.2900E-01);
	curLightSource->AddWavelength(593.0f,1.3086E-01);
	curLightSource->AddWavelength(594.0f,1.3029E-01);
	curLightSource->AddWavelength(595.0f,1.2870E-01);
	curLightSource->AddWavelength(596.0f,1.3260E-01);
	curLightSource->AddWavelength(597.0f,1.3303E-01);
	curLightSource->AddWavelength(598.0f,1.3142E-01);
	curLightSource->AddWavelength(599.0f,1.3145E-01);
	curLightSource->AddWavelength(600.0f,1.3278E-01);
	curLightSource->AddWavelength(601.0f,1.3123E-01);
	curLightSource->AddWavelength(602.0f,1.2928E-01);
	curLightSource->AddWavelength(603.0f,1.3205E-01);
	curLightSource->AddWavelength(604.0f,1.3439E-01);
	curLightSource->AddWavelength(605.0f,1.3418E-01);
	curLightSource->AddWavelength(606.0f,1.3353E-01);
	curLightSource->AddWavelength(607.0f,1.3434E-01);
	curLightSource->AddWavelength(608.0f,1.3392E-01);
	curLightSource->AddWavelength(609.0f,1.3292E-01);
	curLightSource->AddWavelength(610.0f,1.3237E-01);
	curLightSource->AddWavelength(611.0f,1.3170E-01);
	curLightSource->AddWavelength(612.0f,1.3370E-01);
	curLightSource->AddWavelength(613.0f,1.3182E-01);
	curLightSource->AddWavelength(614.0f,1.2783E-01);
	curLightSource->AddWavelength(615.0f,1.3254E-01);
	curLightSource->AddWavelength(616.0f,1.2906E-01);
	curLightSource->AddWavelength(617.0f,1.2744E-01);
	curLightSource->AddWavelength(618.0f,1.3228E-01);
	curLightSource->AddWavelength(619.0f,1.3292E-01);
	curLightSource->AddWavelength(620.0f,1.3299E-01);
	curLightSource->AddWavelength(621.0f,1.3359E-01);
	curLightSource->AddWavelength(622.0f,1.2882E-01);
	curLightSource->AddWavelength(623.0f,1.2793E-01);
	curLightSource->AddWavelength(624.0f,1.2751E-01);
	curLightSource->AddWavelength(625.0f,1.2667E-01);
	curLightSource->AddWavelength(626.0f,1.2655E-01);
	curLightSource->AddWavelength(627.0f,1.3022E-01);
	curLightSource->AddWavelength(628.0f,1.2328E-01);
	curLightSource->AddWavelength(629.0f,1.2758E-01);
	curLightSource->AddWavelength(630.0f,1.2589E-01);
	curLightSource->AddWavelength(631.0f,1.2799E-01);
	curLightSource->AddWavelength(632.0f,1.2327E-01);
	curLightSource->AddWavelength(633.0f,1.3110E-01);
	curLightSource->AddWavelength(634.0f,1.2907E-01);
	curLightSource->AddWavelength(635.0f,1.3065E-01);
	curLightSource->AddWavelength(636.0f,1.2768E-01);
	curLightSource->AddWavelength(637.0f,1.3204E-01);
	curLightSource->AddWavelength(638.0f,1.3292E-01);
	curLightSource->AddWavelength(639.0f,1.3238E-01);
	curLightSource->AddWavelength(640.0f,1.2962E-01);
	curLightSource->AddWavelength(641.0f,1.2970E-01);
	curLightSource->AddWavelength(642.0f,1.2995E-01);
	curLightSource->AddWavelength(643.0f,1.3130E-01);
	curLightSource->AddWavelength(644.0f,1.3074E-01);
	curLightSource->AddWavelength(645.0f,1.3170E-01);
	curLightSource->AddWavelength(646.0f,1.2797E-01);
	curLightSource->AddWavelength(647.0f,1.2744E-01);
	curLightSource->AddWavelength(648.0f,1.2625E-01);
	curLightSource->AddWavelength(649.0f,1.2234E-01);
	curLightSource->AddWavelength(650.0f,1.2299E-01);
	curLightSource->AddWavelength(651.0f,1.3071E-01);
	curLightSource->AddWavelength(652.0f,1.2558E-01);
	curLightSource->AddWavelength(653.0f,1.2950E-01);
	curLightSource->AddWavelength(654.0f,1.2807E-01);
	curLightSource->AddWavelength(655.0f,1.2220E-01);
	curLightSource->AddWavelength(656.0f,1.0727E-01);
	curLightSource->AddWavelength(657.0f,1.1218E-01);
	curLightSource->AddWavelength(658.0f,1.2540E-01);
	curLightSource->AddWavelength(659.0f,1.2586E-01);
	curLightSource->AddWavelength(660.0f,1.2668E-01);
	curLightSource->AddWavelength(661.0f,1.2618E-01);
	curLightSource->AddWavelength(662.0f,1.2518E-01);
	curLightSource->AddWavelength(663.0f,1.2539E-01);
	curLightSource->AddWavelength(664.0f,1.2647E-01);
	curLightSource->AddWavelength(665.0f,1.2871E-01);
	curLightSource->AddWavelength(666.0f,1.2860E-01);
	curLightSource->AddWavelength(667.0f,1.2767E-01);
	curLightSource->AddWavelength(668.0f,1.2810E-01);
	curLightSource->AddWavelength(669.0f,1.3032E-01);
	curLightSource->AddWavelength(670.0f,1.2853E-01);
	curLightSource->AddWavelength(671.0f,1.2829E-01);
	curLightSource->AddWavelength(672.0f,1.2651E-01);
	curLightSource->AddWavelength(673.0f,1.2760E-01);
	curLightSource->AddWavelength(674.0f,1.2742E-01);
	curLightSource->AddWavelength(675.0f,1.2639E-01);
	curLightSource->AddWavelength(676.0f,1.2786E-01);
	curLightSource->AddWavelength(677.0f,1.2669E-01);
	curLightSource->AddWavelength(678.0f,1.2737E-01);
	curLightSource->AddWavelength(679.0f,1.2629E-01);
	curLightSource->AddWavelength(680.0f,1.2650E-01);
	curLightSource->AddWavelength(681.0f,1.2601E-01);
	curLightSource->AddWavelength(682.0f,1.2662E-01);
	curLightSource->AddWavelength(683.0f,1.2526E-01);
	curLightSource->AddWavelength(684.0f,1.2445E-01);
	curLightSource->AddWavelength(685.0f,1.2454E-01);
	curLightSource->AddWavelength(686.0f,1.2174E-01);
	curLightSource->AddWavelength(687.0f,8.8285E-02);
	curLightSource->AddWavelength(688.0f,1.0195E-01);
	curLightSource->AddWavelength(689.0f,1.0260E-01);
	curLightSource->AddWavelength(690.0f,1.0746E-01);
	curLightSource->AddWavelength(691.0f,1.1201E-01);
	curLightSource->AddWavelength(692.0f,1.1516E-01);
	curLightSource->AddWavelength(693.0f,1.1446E-01);
	curLightSource->AddWavelength(694.0f,1.1318E-01);
	curLightSource->AddWavelength(695.0f,1.1538E-01);
	curLightSource->AddWavelength(696.0f,1.1513E-01);
	curLightSource->AddWavelength(697.0f,1.2151E-01);
	curLightSource->AddWavelength(698.0f,1.1961E-01);
	curLightSource->AddWavelength(699.0f,1.1721E-01);
	curLightSource->AddWavelength(700.0f,1.1636E-01);
	curLightSource->AddWavelength(701.0f,1.1489E-01);
	curLightSource->AddWavelength(702.0f,1.1500E-01);
	curLightSource->AddWavelength(703.0f,1.1567E-01);
	curLightSource->AddWavelength(704.0f,1.1864E-01);
	curLightSource->AddWavelength(705.0f,1.1989E-01);
	curLightSource->AddWavelength(706.0f,1.1925E-01);
	curLightSource->AddWavelength(707.0f,1.1875E-01);
	curLightSource->AddWavelength(708.0f,1.1839E-01);
	curLightSource->AddWavelength(709.0f,1.1880E-01);
	curLightSource->AddWavelength(710.0f,1.1954E-01);
	curLightSource->AddWavelength(711.0f,1.1934E-01);
	curLightSource->AddWavelength(712.0f,1.1856E-01);
	curLightSource->AddWavelength(713.0f,1.1719E-01);
	curLightSource->AddWavelength(714.0f,1.1823E-01);
	curLightSource->AddWavelength(715.0f,1.1428E-01);
	curLightSource->AddWavelength(716.0f,1.1548E-01);
	curLightSource->AddWavelength(717.0f,1.0081E-01);
	curLightSource->AddWavelength(718.0f,9.3873E-02);
	curLightSource->AddWavelength(719.0f,8.4274E-02);
	curLightSource->AddWavelength(720.0f,8.9940E-02);
	curLightSource->AddWavelength(721.0f,9.8967E-02);
	curLightSource->AddWavelength(722.0f,1.1281E-01);
	curLightSource->AddWavelength(723.0f,1.0423E-01);
	curLightSource->AddWavelength(724.0f,9.6305E-02);
	curLightSource->AddWavelength(725.0f,9.4741E-02);
	curLightSource->AddWavelength(726.0f,9.8638E-02);
	curLightSource->AddWavelength(727.0f,9.8988E-02);
	curLightSource->AddWavelength(728.0f,9.4968E-02);
	curLightSource->AddWavelength(729.0f,9.5503E-02);
	curLightSource->AddWavelength(730.0f,1.0294E-01);
	curLightSource->AddWavelength(731.0f,9.7702E-02);
	curLightSource->AddWavelength(732.0f,1.0520E-01);
	curLightSource->AddWavelength(733.0f,1.0901E-01);
	curLightSource->AddWavelength(734.0f,1.1261E-01);
	curLightSource->AddWavelength(735.0f,1.1101E-01);
	curLightSource->AddWavelength(736.0f,1.0994E-01);
	curLightSource->AddWavelength(737.0f,1.0978E-01);
	curLightSource->AddWavelength(738.0f,1.1184E-01);
	curLightSource->AddWavelength(739.0f,1.0855E-01);
	curLightSource->AddWavelength(740.0f,1.1119E-01);
	curLightSource->AddWavelength(741.0f,1.1078E-01);
	curLightSource->AddWavelength(742.0f,1.1084E-01);
	curLightSource->AddWavelength(743.0f,1.1316E-01);
	curLightSource->AddWavelength(744.0f,1.1408E-01);
	curLightSource->AddWavelength(745.0f,1.1404E-01);
	curLightSource->AddWavelength(746.0f,1.1381E-01);
	curLightSource->AddWavelength(747.0f,1.1389E-01);
	curLightSource->AddWavelength(748.0f,1.1323E-01);
	curLightSource->AddWavelength(749.0f,1.1286E-01);
	curLightSource->AddWavelength(750.0f,1.1273E-01);
	curLightSource->AddWavelength(751.0f,1.1224E-01);
	curLightSource->AddWavelength(752.0f,1.1265E-01);
	curLightSource->AddWavelength(753.0f,1.1210E-01);
	curLightSource->AddWavelength(754.0f,1.1353E-01);
	curLightSource->AddWavelength(755.0f,1.1321E-01);
	curLightSource->AddWavelength(756.0f,1.1185E-01);
	curLightSource->AddWavelength(757.0f,1.1176E-01);
	curLightSource->AddWavelength(758.0f,1.1246E-01);
	curLightSource->AddWavelength(759.0f,1.0932E-01);
	curLightSource->AddWavelength(760.0f,2.4716E-02);
	curLightSource->AddWavelength(761.0f,1.4328E-02);
	curLightSource->AddWavelength(762.0f,6.3491E-02);
	curLightSource->AddWavelength(763.0f,3.5217E-02);
	curLightSource->AddWavelength(764.0f,4.9885E-02);
	curLightSource->AddWavelength(765.0f,6.3377E-02);
	curLightSource->AddWavelength(766.0f,7.5080E-02);
	curLightSource->AddWavelength(767.0f,8.9574E-02);
	curLightSource->AddWavelength(768.0f,1.0222E-01);
	curLightSource->AddWavelength(769.0f,1.0347E-01);
	curLightSource->AddWavelength(770.0f,1.0646E-01);
	curLightSource->AddWavelength(771.0f,1.0716E-01);
	curLightSource->AddWavelength(772.0f,1.0802E-01);
	curLightSource->AddWavelength(773.0f,1.0797E-01);
	curLightSource->AddWavelength(774.0f,1.0800E-01);
	curLightSource->AddWavelength(775.0f,1.0801E-01);
	curLightSource->AddWavelength(776.0f,1.0827E-01);
	curLightSource->AddWavelength(777.0f,1.0764E-01);
	curLightSource->AddWavelength(778.0f,1.0754E-01);
	curLightSource->AddWavelength(779.0f,1.0803E-01);
	curLightSource->AddWavelength(780.0f,1.0687E-01);
	curLightSource->AddWavelength(781.0f,1.0662E-01);
	curLightSource->AddWavelength(782.0f,1.0714E-01);
	curLightSource->AddWavelength(783.0f,1.0672E-01);
	curLightSource->AddWavelength(784.0f,1.0602E-01);
	curLightSource->AddWavelength(785.0f,1.0649E-01);
	curLightSource->AddWavelength(786.0f,1.0656E-01);
	curLightSource->AddWavelength(787.0f,1.0530E-01);
	curLightSource->AddWavelength(788.0f,1.0399E-01);
	curLightSource->AddWavelength(789.0f,1.0359E-01);
	curLightSource->AddWavelength(790.0f,1.0045E-01);
	curLightSource->AddWavelength(791.0f,1.0179E-01);
	curLightSource->AddWavelength(792.0f,1.0084E-01);
	curLightSource->AddWavelength(793.0f,1.0015E-01);
	curLightSource->AddWavelength(794.0f,1.0101E-01);
	curLightSource->AddWavelength(795.0f,1.0066E-01);
	curLightSource->AddWavelength(796.0f,9.8985E-02);
	curLightSource->AddWavelength(797.0f,1.0057E-01);
	curLightSource->AddWavelength(798.0f,1.0245E-01);
	curLightSource->AddWavelength(799.0f,1.0048E-01);
	curLightSource->AddWavelength(800.0f,9.8859E-02);
	curLightSource->AddWavelength(801.0f,9.9978E-02);
	curLightSource->AddWavelength(802.0f,1.0011E-01);
	curLightSource->AddWavelength(803.0f,9.8288E-02);
	curLightSource->AddWavelength(804.0f,9.9452E-02);
	curLightSource->AddWavelength(805.0f,9.7270E-02);
	curLightSource->AddWavelength(806.0f,1.0122E-01);
	curLightSource->AddWavelength(807.0f,1.0018E-01);
	curLightSource->AddWavelength(808.0f,9.9844E-02);
	curLightSource->AddWavelength(809.0f,9.7353E-02);
	curLightSource->AddWavelength(810.0f,9.7488E-02);
	curLightSource->AddWavelength(811.0f,9.7273E-02);
	curLightSource->AddWavelength(812.0f,9.4882E-02);
	curLightSource->AddWavelength(813.0f,9.3236E-02);
	curLightSource->AddWavelength(814.0f,8.3681E-02);
	curLightSource->AddWavelength(815.0f,8.2927E-02);
	curLightSource->AddWavelength(816.0f,7.7171E-02);
	curLightSource->AddWavelength(817.0f,7.8984E-02);
	curLightSource->AddWavelength(818.0f,7.6299E-02);
	curLightSource->AddWavelength(819.0f,8.3844E-02);
	curLightSource->AddWavelength(820.0f,7.9899E-02);
	curLightSource->AddWavelength(821.0f,9.2292E-02);
	curLightSource->AddWavelength(822.0f,8.8081E-02);
	curLightSource->AddWavelength(823.0f,6.2576E-02);
	curLightSource->AddWavelength(824.0f,8.6619E-02);
	curLightSource->AddWavelength(825.0f,8.9752E-02);
	curLightSource->AddWavelength(826.0f,8.6530E-02);
	curLightSource->AddWavelength(827.0f,9.1178E-02);
	curLightSource->AddWavelength(828.0f,7.8870E-02);
	curLightSource->AddWavelength(829.0f,8.6135E-02);
	curLightSource->AddWavelength(830.0f,8.4930E-02);
	curLightSource->AddWavelength(831.0f,8.5666E-02);
	curLightSource->AddWavelength(832.0f,8.2961E-02);
	curLightSource->AddWavelength(833.0f,8.8622E-02);
	curLightSource->AddWavelength(834.0f,8.6608E-02);
	curLightSource->AddWavelength(835.0f,9.2917E-02);
	curLightSource->AddWavelength(836.0f,9.0135E-02);
	curLightSource->AddWavelength(837.0f,9.3493E-02);
	curLightSource->AddWavelength(838.0f,9.2585E-02);
	curLightSource->AddWavelength(839.0f,9.2783E-02);
	curLightSource->AddWavelength(840.0f,9.4124E-02);
	curLightSource->AddWavelength(841.0f,9.3626E-02);
	curLightSource->AddWavelength(842.0f,9.2411E-02);
	curLightSource->AddWavelength(843.0f,9.3171E-02);
	curLightSource->AddWavelength(844.0f,9.1434E-02);
	curLightSource->AddWavelength(845.0f,9.4226E-02);
	curLightSource->AddWavelength(846.0f,9.4447E-02);
	curLightSource->AddWavelength(847.0f,9.1947E-02);
	curLightSource->AddWavelength(848.0f,9.2010E-02);
	curLightSource->AddWavelength(849.0f,9.1447E-02);
	curLightSource->AddWavelength(850.0f,8.2900E-02);
	curLightSource->AddWavelength(851.0f,9.0454E-02);
	curLightSource->AddWavelength(852.0f,8.9942E-02);
	curLightSource->AddWavelength(853.0f,8.9540E-02);
	curLightSource->AddWavelength(854.0f,7.9000E-02);
	curLightSource->AddWavelength(855.0f,8.4746E-02);
	curLightSource->AddWavelength(856.0f,9.0343E-02);
	curLightSource->AddWavelength(857.0f,9.2059E-02);
	curLightSource->AddWavelength(858.0f,9.2094E-02);
	curLightSource->AddWavelength(859.0f,9.2081E-02);
	curLightSource->AddWavelength(860.0f,9.1764E-02);
	curLightSource->AddWavelength(861.0f,9.1648E-02);
	curLightSource->AddWavelength(862.0f,9.2367E-02);
	curLightSource->AddWavelength(863.0f,9.2934E-02);
	curLightSource->AddWavelength(864.0f,9.0956E-02);
	curLightSource->AddWavelength(865.0f,8.9487E-02);
	curLightSource->AddWavelength(866.0f,7.8882E-02);
	curLightSource->AddWavelength(867.0f,8.5066E-02);
	curLightSource->AddWavelength(868.0f,8.9140E-02);
	curLightSource->AddWavelength(869.0f,8.8252E-02);
	curLightSource->AddWavelength(870.0f,8.9933E-02);
	curLightSource->AddWavelength(871.0f,8.8671E-02);
	curLightSource->AddWavelength(872.0f,8.9887E-02);
	curLightSource->AddWavelength(873.0f,8.8999E-02);
	curLightSource->AddWavelength(874.0f,8.7451E-02);
	curLightSource->AddWavelength(875.0f,8.6204E-02);
	curLightSource->AddWavelength(876.0f,8.8625E-02);
	curLightSource->AddWavelength(877.0f,8.8948E-02);
	curLightSource->AddWavelength(878.0f,8.8607E-02);
	curLightSource->AddWavelength(879.0f,8.7144E-02);
	curLightSource->AddWavelength(880.0f,8.7434E-02);
	curLightSource->AddWavelength(881.0f,8.4563E-02);
	curLightSource->AddWavelength(882.0f,8.6787E-02);
	curLightSource->AddWavelength(883.0f,8.6494E-02);
	curLightSource->AddWavelength(884.0f,8.6859E-02);
	curLightSource->AddWavelength(885.0f,8.7913E-02);
	curLightSource->AddWavelength(886.0f,8.4515E-02);
	curLightSource->AddWavelength(887.0f,8.4799E-02);
	curLightSource->AddWavelength(888.0f,8.5899E-02);
	curLightSource->AddWavelength(889.0f,8.7041E-02);
	curLightSource->AddWavelength(890.0f,8.6078E-02);
	curLightSource->AddWavelength(891.0f,8.6255E-02);
	curLightSource->AddWavelength(892.0f,8.4688E-02);
	curLightSource->AddWavelength(893.0f,8.1412E-02);
	curLightSource->AddWavelength(894.0f,7.9413E-02);
	curLightSource->AddWavelength(895.0f,7.5956E-02);
	curLightSource->AddWavelength(896.0f,7.1265E-02);
	curLightSource->AddWavelength(897.0f,6.2310E-02);
	curLightSource->AddWavelength(898.0f,6.7137E-02);
	curLightSource->AddWavelength(899.0f,5.1461E-02);
	curLightSource->AddWavelength(900.0f,6.9429E-02);
	curLightSource->AddWavelength(901.0f,5.6162E-02);
	curLightSource->AddWavelength(902.0f,6.2500E-02);
	curLightSource->AddWavelength(903.0f,6.4483E-02);
	curLightSource->AddWavelength(904.0f,7.8862E-02);
	curLightSource->AddWavelength(905.0f,7.6337E-02);
	curLightSource->AddWavelength(906.0f,7.2502E-02);
	curLightSource->AddWavelength(907.0f,5.9833E-02);
	curLightSource->AddWavelength(908.0f,6.1091E-02);
	curLightSource->AddWavelength(909.0f,6.5912E-02);
	curLightSource->AddWavelength(910.0f,5.8553E-02);
	curLightSource->AddWavelength(911.0f,6.2580E-02);
	curLightSource->AddWavelength(912.0f,6.4508E-02);
	curLightSource->AddWavelength(913.0f,5.8906E-02);
	curLightSource->AddWavelength(914.0f,5.8740E-02);
	curLightSource->AddWavelength(915.0f,6.3550E-02);
	curLightSource->AddWavelength(916.0f,5.4099E-02);
	curLightSource->AddWavelength(917.0f,6.8350E-02);
	curLightSource->AddWavelength(918.0f,5.5612E-02);
	curLightSource->AddWavelength(919.0f,6.9161E-02);
	curLightSource->AddWavelength(920.0f,6.9657E-02);
	curLightSource->AddWavelength(921.0f,7.3004E-02);
	curLightSource->AddWavelength(922.0f,6.5584E-02);
	curLightSource->AddWavelength(923.0f,6.9698E-02);
	curLightSource->AddWavelength(924.0f,6.7571E-02);
	curLightSource->AddWavelength(925.0f,6.6621E-02);
	curLightSource->AddWavelength(926.0f,6.5875E-02);
	curLightSource->AddWavelength(927.0f,7.3684E-02);
	curLightSource->AddWavelength(928.0f,5.5363E-02);
	curLightSource->AddWavelength(929.0f,5.1792E-02);
	curLightSource->AddWavelength(930.0f,4.0679E-02);
	curLightSource->AddWavelength(931.0f,3.8540E-02);
	curLightSource->AddWavelength(932.0f,2.8386E-02);
	curLightSource->AddWavelength(933.0f,2.3459E-02);
	curLightSource->AddWavelength(934.0f,1.3604E-02);
	curLightSource->AddWavelength(935.0f,2.3690E-02);
	curLightSource->AddWavelength(936.0f,1.5267E-02);
	curLightSource->AddWavelength(937.0f,1.5453E-02);
	curLightSource->AddWavelength(938.0f,1.8962E-02);
	curLightSource->AddWavelength(939.0f,3.7591E-02);
	curLightSource->AddWavelength(940.0f,4.4411E-02);
	curLightSource->AddWavelength(941.0f,3.5071E-02);
	curLightSource->AddWavelength(942.0f,3.8192E-02);
	curLightSource->AddWavelength(943.0f,2.6289E-02);
	curLightSource->AddWavelength(944.0f,2.6987E-02);
	curLightSource->AddWavelength(945.0f,3.4729E-02);
	curLightSource->AddWavelength(946.0f,1.8409E-02);
	curLightSource->AddWavelength(947.0f,3.5010E-02);
	curLightSource->AddWavelength(948.0f,2.5911E-02);
	curLightSource->AddWavelength(949.0f,4.6514E-02);
	curLightSource->AddWavelength(950.0f,1.3944E-02);
	curLightSource->AddWavelength(951.0f,4.5563E-02);
	curLightSource->AddWavelength(952.0f,2.5418E-02);
	curLightSource->AddWavelength(953.0f,3.2442E-02);
	curLightSource->AddWavelength(954.0f,3.9988E-02);
	curLightSource->AddWavelength(955.0f,3.2204E-02);
	curLightSource->AddWavelength(956.0f,3.0996E-02);
	curLightSource->AddWavelength(957.0f,2.5591E-02);
	curLightSource->AddWavelength(958.0f,4.3453E-02);
	curLightSource->AddWavelength(959.0f,3.5294E-02);
	curLightSource->AddWavelength(960.0f,3.9685E-02);
	curLightSource->AddWavelength(961.0f,4.3481E-02);
	curLightSource->AddWavelength(962.0f,4.1664E-02);
	curLightSource->AddWavelength(963.0f,4.7585E-02);
	curLightSource->AddWavelength(964.0f,4.3242E-02);
	curLightSource->AddWavelength(965.0f,4.7469E-02);
	curLightSource->AddWavelength(966.0f,4.7377E-02);
	curLightSource->AddWavelength(967.0f,4.7353E-02);
	curLightSource->AddWavelength(968.0f,6.1301E-02);
	curLightSource->AddWavelength(969.0f,6.4480E-02);
	curLightSource->AddWavelength(970.0f,5.9689E-02);
	curLightSource->AddWavelength(971.0f,6.7059E-02);
	curLightSource->AddWavelength(972.0f,6.4629E-02);
	curLightSource->AddWavelength(973.0f,5.7081E-02);
	curLightSource->AddWavelength(974.0f,5.4170E-02);
	curLightSource->AddWavelength(975.0f,5.5536E-02);
	curLightSource->AddWavelength(976.0f,5.3872E-02);
	curLightSource->AddWavelength(977.0f,6.0084E-02);
	curLightSource->AddWavelength(978.0f,5.7903E-02);
	curLightSource->AddWavelength(979.0f,6.0046E-02);
	curLightSource->AddWavelength(980.0f,5.6941E-02);
	curLightSource->AddWavelength(981.0f,6.7058E-02);
	curLightSource->AddWavelength(982.0f,6.5102E-02);
	curLightSource->AddWavelength(983.0f,6.2915E-02);
	curLightSource->AddWavelength(984.0f,6.9290E-02);
	curLightSource->AddWavelength(985.0f,6.4734E-02);
	curLightSource->AddWavelength(986.0f,7.0553E-02);
	curLightSource->AddWavelength(987.0f,6.9489E-02);
	curLightSource->AddWavelength(988.0f,6.9059E-02);
	curLightSource->AddWavelength(989.0f,7.0391E-02);
	curLightSource->AddWavelength(990.0f,6.8843E-02);
	curLightSource->AddWavelength(991.0f,7.0833E-02);
	curLightSource->AddWavelength(992.0f,7.0597E-02);
	curLightSource->AddWavelength(993.0f,6.9325E-02);
	curLightSource->AddWavelength(994.0f,7.0891E-02);
	curLightSource->AddWavelength(995.0f,7.0673E-02);
	curLightSource->AddWavelength(996.0f,7.0409E-02);
	curLightSource->AddWavelength(997.0f,6.9555E-02);
	curLightSource->AddWavelength(998.0f,6.9481E-02);
	curLightSource->AddWavelength(999.0f,6.9455E-02);
	curLightSource->AddWavelength(1000.0f,6.9159E-02);
	curLightSource->AddWavelength(1001.0f,7.0013E-02);
	curLightSource->AddWavelength(1002.0f,6.8498E-02);
	curLightSource->AddWavelength(1003.0f,6.9086E-02);
	curLightSource->AddWavelength(1004.0f,6.8056E-02);
	curLightSource->AddWavelength(1005.0f,6.4140E-02);
	curLightSource->AddWavelength(1006.0f,6.7047E-02);
	curLightSource->AddWavelength(1007.0f,6.8463E-02);
	curLightSource->AddWavelength(1008.0f,6.8407E-02);
	curLightSource->AddWavelength(1009.0f,6.7742E-02);
	curLightSource->AddWavelength(1010.0f,6.7695E-02);
	curLightSource->AddWavelength(1011.0f,6.8034E-02);
	curLightSource->AddWavelength(1012.0f,6.7667E-02);
	curLightSource->AddWavelength(1013.0f,6.7556E-02);
	curLightSource->AddWavelength(1014.0f,6.7848E-02);
	curLightSource->AddWavelength(1015.0f,6.6676E-02);
	curLightSource->AddWavelength(1016.0f,6.6976E-02);
	curLightSource->AddWavelength(1017.0f,6.6237E-02);
	curLightSource->AddWavelength(1018.0f,6.7263E-02);
	curLightSource->AddWavelength(1019.0f,6.4880E-02);
	curLightSource->AddWavelength(1020.0f,6.5839E-02);
	curLightSource->AddWavelength(1021.0f,6.6107E-02);
	curLightSource->AddWavelength(1022.0f,6.4980E-02);
	curLightSource->AddWavelength(1023.0f,6.5490E-02);
	curLightSource->AddWavelength(1024.0f,6.5077E-02);
	curLightSource->AddWavelength(1025.0f,6.5727E-02);
	curLightSource->AddWavelength(1026.0f,6.5625E-02);
	curLightSource->AddWavelength(1027.0f,6.5318E-02);
	curLightSource->AddWavelength(1028.0f,6.5398E-02);
	curLightSource->AddWavelength(1029.0f,6.4687E-02);
	curLightSource->AddWavelength(1030.0f,6.5092E-02);
	curLightSource->AddWavelength(1031.0f,6.4799E-02);
	curLightSource->AddWavelength(1032.0f,6.4852E-02);
	curLightSource->AddWavelength(1033.0f,6.3751E-02);
	curLightSource->AddWavelength(1034.0f,6.4136E-02);
	curLightSource->AddWavelength(1035.0f,6.4348E-02);
	curLightSource->AddWavelength(1036.0f,6.4323E-02);
	curLightSource->AddWavelength(1037.0f,6.3664E-02);
	curLightSource->AddWavelength(1038.0f,6.3361E-02);
	curLightSource->AddWavelength(1039.0f,6.3802E-02);
	curLightSource->AddWavelength(1040.0f,6.3366E-02);
	curLightSource->AddWavelength(1041.0f,6.3379E-02);
	curLightSource->AddWavelength(1042.0f,6.3406E-02);
	curLightSource->AddWavelength(1043.0f,6.2773E-02);
	curLightSource->AddWavelength(1044.0f,6.3067E-02);
	curLightSource->AddWavelength(1045.0f,6.2712E-02);
	curLightSource->AddWavelength(1046.0f,6.1078E-02);
	curLightSource->AddWavelength(1047.0f,6.2008E-02);
	curLightSource->AddWavelength(1048.0f,6.2559E-02);
	curLightSource->AddWavelength(1049.0f,6.2206E-02);
	curLightSource->AddWavelength(1050.0f,6.1802E-02);
	curLightSource->AddWavelength(1051.0f,6.1862E-02);
	curLightSource->AddWavelength(1052.0f,6.1487E-02);
	curLightSource->AddWavelength(1053.0f,6.1302E-02);
	curLightSource->AddWavelength(1054.0f,6.1048E-02);
	curLightSource->AddWavelength(1055.0f,6.1242E-02);
	curLightSource->AddWavelength(1056.0f,6.1055E-02);
	curLightSource->AddWavelength(1057.0f,6.0910E-02);
	curLightSource->AddWavelength(1058.0f,6.0286E-02);
	curLightSource->AddWavelength(1059.0f,5.8453E-02);
	curLightSource->AddWavelength(1060.0f,6.0073E-02);
	curLightSource->AddWavelength(1061.0f,5.8694E-02);
	curLightSource->AddWavelength(1062.0f,5.9782E-02);
	curLightSource->AddWavelength(1063.0f,5.8815E-02);
	curLightSource->AddWavelength(1064.0f,5.9722E-02);
	curLightSource->AddWavelength(1065.0f,5.9461E-02);
	curLightSource->AddWavelength(1066.0f,5.8330E-02);
	curLightSource->AddWavelength(1067.0f,5.8637E-02);
	curLightSource->AddWavelength(1068.0f,5.8561E-02);
	curLightSource->AddWavelength(1069.0f,5.5428E-02);
	curLightSource->AddWavelength(1070.0f,5.7178E-02);
	curLightSource->AddWavelength(1071.0f,5.8304E-02);
	curLightSource->AddWavelength(1072.0f,5.8194E-02);
	curLightSource->AddWavelength(1073.0f,5.7086E-02);
	curLightSource->AddWavelength(1074.0f,5.8780E-02);
	curLightSource->AddWavelength(1075.0f,5.6054E-02);
	curLightSource->AddWavelength(1076.0f,5.8141E-02);
	curLightSource->AddWavelength(1077.0f,5.7175E-02);
	curLightSource->AddWavelength(1078.0f,5.7076E-02);
	curLightSource->AddWavelength(1079.0f,5.7210E-02);
	curLightSource->AddWavelength(1080.0f,5.6519E-02);
	curLightSource->AddWavelength(1081.0f,5.4973E-02);
	curLightSource->AddWavelength(1082.0f,5.5773E-02);
	curLightSource->AddWavelength(1083.0f,5.6603E-02);
	curLightSource->AddWavelength(1084.0f,5.4775E-02);
	curLightSource->AddWavelength(1085.0f,5.6163E-02);
	curLightSource->AddWavelength(1086.0f,5.2496E-02);
	curLightSource->AddWavelength(1087.0f,5.3685E-02);
	curLightSource->AddWavelength(1088.0f,5.6159E-02);
	curLightSource->AddWavelength(1089.0f,5.4856E-02);
	curLightSource->AddWavelength(1090.0f,5.2656E-02);
	curLightSource->AddWavelength(1091.0f,5.5722E-02);
	curLightSource->AddWavelength(1092.0f,5.5048E-02);
	curLightSource->AddWavelength(1093.0f,4.8417E-02);
	curLightSource->AddWavelength(1094.0f,5.1120E-02);
	curLightSource->AddWavelength(1095.0f,4.9363E-02);
	curLightSource->AddWavelength(1096.0f,4.7731E-02);
	curLightSource->AddWavelength(1097.0f,5.4805E-02);
	curLightSource->AddWavelength(1098.0f,4.7709E-02);
	curLightSource->AddWavelength(1099.0f,4.8161E-02);
	curLightSource->AddWavelength(1100.0f,4.6113E-02);
	curLightSource->AddWavelength(1101.0f,4.7169E-02);
	curLightSource->AddWavelength(1102.0f,4.4513E-02);
	curLightSource->AddWavelength(1103.0f,4.4291E-02);
	curLightSource->AddWavelength(1104.0f,4.4412E-02);
	curLightSource->AddWavelength(1105.0f,4.8065E-02);
	curLightSource->AddWavelength(1106.0f,3.7840E-02);
	curLightSource->AddWavelength(1107.0f,4.5866E-02);
	curLightSource->AddWavelength(1108.0f,3.9517E-02);
	curLightSource->AddWavelength(1109.0f,3.9249E-02);
	curLightSource->AddWavelength(1110.0f,4.5496E-02);
	curLightSource->AddWavelength(1111.0f,3.1572E-02);
	curLightSource->AddWavelength(1112.0f,3.9330E-02);
	curLightSource->AddWavelength(1113.0f,2.5599E-02);
	curLightSource->AddWavelength(1114.0f,2.8576E-02);
	curLightSource->AddWavelength(1115.0f,2.3833E-02);
	curLightSource->AddWavelength(1116.0f,1.9223E-02);
	curLightSource->AddWavelength(1117.0f,7.6164E-03);
	curLightSource->AddWavelength(1118.0f,2.0763E-02);
	curLightSource->AddWavelength(1119.0f,1.0821E-02);
	curLightSource->AddWavelength(1120.0f,1.3562E-02);
	curLightSource->AddWavelength(1121.0f,1.7753E-02);
	curLightSource->AddWavelength(1122.0f,7.8159E-03);
	curLightSource->AddWavelength(1123.0f,1.2255E-02);
	curLightSource->AddWavelength(1124.0f,1.0397E-02);
	curLightSource->AddWavelength(1125.0f,1.3794E-02);
	curLightSource->AddWavelength(1126.0f,4.9394E-03);
	curLightSource->AddWavelength(1127.0f,1.5032E-02);
	curLightSource->AddWavelength(1128.0f,9.4946E-03);
	curLightSource->AddWavelength(1129.0f,1.0133E-02);
	curLightSource->AddWavelength(1130.0f,6.7568E-03);
	curLightSource->AddWavelength(1131.0f,2.8201E-02);
	curLightSource->AddWavelength(1132.0f,2.2359E-02);
	curLightSource->AddWavelength(1133.0f,1.4661E-02);
	curLightSource->AddWavelength(1134.0f,3.9988E-03);
	curLightSource->AddWavelength(1135.0f,1.4819E-03);
	curLightSource->AddWavelength(1136.0f,1.2320E-02);
	curLightSource->AddWavelength(1137.0f,2.7472E-02);
	curLightSource->AddWavelength(1138.0f,1.9428E-02);
	curLightSource->AddWavelength(1139.0f,2.8484E-02);
	curLightSource->AddWavelength(1140.0f,2.4447E-02);
	curLightSource->AddWavelength(1141.0f,1.8486E-02);
	curLightSource->AddWavelength(1142.0f,2.1481E-02);
	curLightSource->AddWavelength(1143.0f,2.9758E-02);
	curLightSource->AddWavelength(1144.0f,1.0843E-02);
	curLightSource->AddWavelength(1145.0f,1.3976E-02);
	curLightSource->AddWavelength(1146.0f,1.5085E-02);
	curLightSource->AddWavelength(1147.0f,5.6715E-03);
	curLightSource->AddWavelength(1148.0f,2.5898E-02);
	curLightSource->AddWavelength(1149.0f,2.0894E-02);
	curLightSource->AddWavelength(1150.0f,1.1648E-02);
	curLightSource->AddWavelength(1151.0f,1.9453E-02);
	curLightSource->AddWavelength(1152.0f,2.3666E-02);
	curLightSource->AddWavelength(1153.0f,2.2762E-02);
	curLightSource->AddWavelength(1154.0f,1.3643E-02);
	curLightSource->AddWavelength(1155.0f,2.9903E-02);
	curLightSource->AddWavelength(1156.0f,2.6837E-02);
	curLightSource->AddWavelength(1157.0f,3.0040E-02);
	curLightSource->AddWavelength(1158.0f,2.9770E-02);
	curLightSource->AddWavelength(1159.0f,3.2163E-02);
	curLightSource->AddWavelength(1160.0f,2.7371E-02);
	curLightSource->AddWavelength(1161.0f,3.3167E-02);
	curLightSource->AddWavelength(1162.0f,3.3412E-02);
	curLightSource->AddWavelength(1163.0f,4.4644E-02);
	curLightSource->AddWavelength(1164.0f,3.8332E-02);
	curLightSource->AddWavelength(1165.0f,3.7080E-02);
	curLightSource->AddWavelength(1166.0f,3.5781E-02);
	curLightSource->AddWavelength(1167.0f,3.9105E-02);
	curLightSource->AddWavelength(1168.0f,4.0018E-02);
	curLightSource->AddWavelength(1169.0f,4.0348E-02);
	curLightSource->AddWavelength(1170.0f,4.3731E-02);
	curLightSource->AddWavelength(1171.0f,4.2750E-02);
	curLightSource->AddWavelength(1172.0f,4.3370E-02);
	curLightSource->AddWavelength(1173.0f,4.3522E-02);
	curLightSource->AddWavelength(1174.0f,3.2193E-02);
	curLightSource->AddWavelength(1175.0f,4.3142E-02);
	curLightSource->AddWavelength(1176.0f,4.5447E-02);
	curLightSource->AddWavelength(1177.0f,4.5040E-02);
	curLightSource->AddWavelength(1178.0f,3.4391E-02);
	curLightSource->AddWavelength(1179.0f,4.6114E-02);
	curLightSource->AddWavelength(1180.0f,4.2052E-02);
	curLightSource->AddWavelength(1181.0f,4.3423E-02);
	curLightSource->AddWavelength(1182.0f,3.0903E-02);
	curLightSource->AddWavelength(1183.0f,4.1853E-02);
	curLightSource->AddWavelength(1184.0f,4.0083E-02);
	curLightSource->AddWavelength(1185.0f,3.8908E-02);
	curLightSource->AddWavelength(1186.0f,4.5518E-02);
	curLightSource->AddWavelength(1187.0f,4.3494E-02);
	curLightSource->AddWavelength(1188.0f,3.2039E-02);
	curLightSource->AddWavelength(1189.0f,3.9685E-02);
	curLightSource->AddWavelength(1190.0f,4.4124E-02);
	curLightSource->AddWavelength(1191.0f,4.2638E-02);
	curLightSource->AddWavelength(1192.0f,4.5171E-02);
	curLightSource->AddWavelength(1193.0f,4.3370E-02);
	curLightSource->AddWavelength(1194.0f,4.4751E-02);
	curLightSource->AddWavelength(1195.0f,4.2671E-02);
	curLightSource->AddWavelength(1196.0f,4.1188E-02);
	curLightSource->AddWavelength(1197.0f,4.5527E-02);
	curLightSource->AddWavelength(1198.0f,4.1426E-02);
	curLightSource->AddWavelength(1199.0f,3.4869E-02);
	curLightSource->AddWavelength(1200.0f,4.2789E-02);
	curLightSource->AddWavelength(1201.0f,4.1735E-02);
	curLightSource->AddWavelength(1202.0f,4.1743E-02);
	curLightSource->AddWavelength(1203.0f,4.1441E-02);
	curLightSource->AddWavelength(1204.0f,3.4645E-02);
	curLightSource->AddWavelength(1205.0f,4.1714E-02);
	curLightSource->AddWavelength(1206.0f,4.5872E-02);
	curLightSource->AddWavelength(1207.0f,4.1044E-02);
	curLightSource->AddWavelength(1208.0f,4.1379E-02);
	curLightSource->AddWavelength(1209.0f,3.9548E-02);
	curLightSource->AddWavelength(1210.0f,4.3267E-02);
	curLightSource->AddWavelength(1211.0f,4.0319E-02);
	curLightSource->AddWavelength(1212.0f,4.0572E-02);
	curLightSource->AddWavelength(1213.0f,4.4804E-02);
	curLightSource->AddWavelength(1214.0f,4.1443E-02);
	curLightSource->AddWavelength(1215.0f,4.0851E-02);
	curLightSource->AddWavelength(1216.0f,4.4509E-02);
	curLightSource->AddWavelength(1217.0f,4.3457E-02);
	curLightSource->AddWavelength(1218.0f,4.3842E-02);
	curLightSource->AddWavelength(1219.0f,4.2639E-02);
	curLightSource->AddWavelength(1220.0f,4.3724E-02);
	curLightSource->AddWavelength(1221.0f,4.4413E-02);
	curLightSource->AddWavelength(1222.0f,4.3096E-02);
	curLightSource->AddWavelength(1223.0f,4.2400E-02);
	curLightSource->AddWavelength(1224.0f,4.2788E-02);
	curLightSource->AddWavelength(1225.0f,4.4141E-02);
	curLightSource->AddWavelength(1226.0f,4.4696E-02);
	curLightSource->AddWavelength(1227.0f,4.1360E-02);
	curLightSource->AddWavelength(1228.0f,4.4544E-02);
	curLightSource->AddWavelength(1229.0f,4.4608E-02);
	curLightSource->AddWavelength(1230.0f,4.3928E-02);
	curLightSource->AddWavelength(1231.0f,4.5067E-02);
	curLightSource->AddWavelength(1232.0f,4.4525E-02);
	curLightSource->AddWavelength(1233.0f,4.3359E-02);
	curLightSource->AddWavelength(1234.0f,4.4893E-02);
	curLightSource->AddWavelength(1235.0f,4.4409E-02);
	curLightSource->AddWavelength(1236.0f,4.4795E-02);
	curLightSource->AddWavelength(1237.0f,4.4259E-02);
	curLightSource->AddWavelength(1238.0f,4.4694E-02);
	curLightSource->AddWavelength(1239.0f,4.4194E-02);
	curLightSource->AddWavelength(1240.0f,4.4011E-02);
	curLightSource->AddWavelength(1241.0f,4.4130E-02);
	curLightSource->AddWavelength(1242.0f,4.4179E-02);
	curLightSource->AddWavelength(1243.0f,4.3712E-02);
	curLightSource->AddWavelength(1244.0f,4.3499E-02);
	curLightSource->AddWavelength(1245.0f,4.3622E-02);
	curLightSource->AddWavelength(1246.0f,4.3902E-02);
	curLightSource->AddWavelength(1247.0f,4.3715E-02);
	curLightSource->AddWavelength(1248.0f,4.3828E-02);
	curLightSource->AddWavelength(1249.0f,4.3930E-02);
	curLightSource->AddWavelength(1250.0f,4.3684E-02);
	curLightSource->AddWavelength(1251.0f,4.3260E-02);
	curLightSource->AddWavelength(1252.0f,4.3106E-02);
	curLightSource->AddWavelength(1253.0f,4.2803E-02);
	curLightSource->AddWavelength(1254.0f,4.2416E-02);
	curLightSource->AddWavelength(1255.0f,4.3088E-02);
	curLightSource->AddWavelength(1256.0f,4.2096E-02);
	curLightSource->AddWavelength(1257.0f,4.1630E-02);
	curLightSource->AddWavelength(1258.0f,4.2549E-02);
	curLightSource->AddWavelength(1259.0f,4.0868E-02);
	curLightSource->AddWavelength(1260.0f,4.1235E-02);
	curLightSource->AddWavelength(1261.0f,3.9371E-02);
	curLightSource->AddWavelength(1262.0f,3.7867E-02);
	curLightSource->AddWavelength(1263.0f,3.8300E-02);
	curLightSource->AddWavelength(1264.0f,3.5568E-02);
	curLightSource->AddWavelength(1265.0f,3.7871E-02);
	curLightSource->AddWavelength(1266.0f,3.6881E-02);
	curLightSource->AddWavelength(1267.0f,3.7159E-02);
	curLightSource->AddWavelength(1268.0f,3.5475E-02);
	curLightSource->AddWavelength(1269.0f,2.3656E-02);
	curLightSource->AddWavelength(1270.0f,3.7087E-02);
	curLightSource->AddWavelength(1271.0f,3.9062E-02);
	curLightSource->AddWavelength(1272.0f,3.9114E-02);
	curLightSource->AddWavelength(1273.0f,3.8874E-02);
	curLightSource->AddWavelength(1274.0f,3.8864E-02);
	curLightSource->AddWavelength(1275.0f,3.9455E-02);
	curLightSource->AddWavelength(1276.0f,3.9895E-02);
	curLightSource->AddWavelength(1277.0f,4.0191E-02);
	curLightSource->AddWavelength(1278.0f,4.0916E-02);
	curLightSource->AddWavelength(1279.0f,4.0626E-02);
	curLightSource->AddWavelength(1280.0f,4.0387E-02);
	curLightSource->AddWavelength(1281.0f,3.9554E-02);
	curLightSource->AddWavelength(1282.0f,3.5695E-02);
	curLightSource->AddWavelength(1283.0f,3.8978E-02);
	curLightSource->AddWavelength(1284.0f,4.0268E-02);
	curLightSource->AddWavelength(1285.0f,4.0577E-02);
	curLightSource->AddWavelength(1286.0f,4.0878E-02);
	curLightSource->AddWavelength(1287.0f,4.0405E-02);
	curLightSource->AddWavelength(1288.0f,4.0192E-02);
	curLightSource->AddWavelength(1289.0f,3.9194E-02);
	curLightSource->AddWavelength(1290.0f,3.9522E-02);
	curLightSource->AddWavelength(1291.0f,4.0004E-02);
	curLightSource->AddWavelength(1292.0f,3.7946E-02);
	curLightSource->AddWavelength(1293.0f,3.9506E-02);
	curLightSource->AddWavelength(1294.0f,3.8709E-02);
	curLightSource->AddWavelength(1295.0f,3.8801E-02);
	curLightSource->AddWavelength(1296.0f,3.7322E-02);
	curLightSource->AddWavelength(1297.0f,3.5583E-02);
	curLightSource->AddWavelength(1298.0f,3.7536E-02);
	curLightSource->AddWavelength(1299.0f,3.9127E-02);
	curLightSource->AddWavelength(1300.0f,3.3855E-02);
	curLightSource->AddWavelength(1301.0f,3.4728E-02);
	curLightSource->AddWavelength(1302.0f,3.7539E-02);
	curLightSource->AddWavelength(1303.0f,3.3197E-02);
	curLightSource->AddWavelength(1304.0f,2.8849E-02);
	curLightSource->AddWavelength(1305.0f,3.6783E-02);
	curLightSource->AddWavelength(1306.0f,3.6853E-02);
	curLightSource->AddWavelength(1307.0f,2.9362E-02);
	curLightSource->AddWavelength(1308.0f,3.3277E-02);
	curLightSource->AddWavelength(1309.0f,3.6822E-02);
	curLightSource->AddWavelength(1310.0f,2.8908E-02);
	curLightSource->AddWavelength(1311.0f,3.2012E-02);
	curLightSource->AddWavelength(1312.0f,3.1986E-02);
	curLightSource->AddWavelength(1313.0f,3.0089E-02);
	curLightSource->AddWavelength(1314.0f,2.7690E-02);
	curLightSource->AddWavelength(1315.0f,2.7447E-02);
	curLightSource->AddWavelength(1316.0f,3.1113E-02);
	curLightSource->AddWavelength(1317.0f,2.9969E-02);
	curLightSource->AddWavelength(1318.0f,3.1984E-02);
	curLightSource->AddWavelength(1319.0f,2.5803E-02);
	curLightSource->AddWavelength(1320.0f,2.4864E-02);
	curLightSource->AddWavelength(1321.0f,2.8684E-02);
	curLightSource->AddWavelength(1322.0f,2.9023E-02);
	curLightSource->AddWavelength(1323.0f,2.2386E-02);
	curLightSource->AddWavelength(1324.0f,2.5231E-02);
	curLightSource->AddWavelength(1325.0f,3.0943E-02);
	curLightSource->AddWavelength(1326.0f,2.6956E-02);
	curLightSource->AddWavelength(1327.0f,2.5593E-02);
	curLightSource->AddWavelength(1328.0f,2.2555E-02);
	curLightSource->AddWavelength(1329.0f,1.7097E-02);
	curLightSource->AddWavelength(1330.0f,2.2052E-02);
	curLightSource->AddWavelength(1331.0f,1.3951E-02);
	curLightSource->AddWavelength(1332.0f,1.4046E-02);
	curLightSource->AddWavelength(1333.0f,1.9545E-02);
	curLightSource->AddWavelength(1334.0f,1.6302E-02);
	curLightSource->AddWavelength(1335.0f,2.2244E-02);
	curLightSource->AddWavelength(1336.0f,1.7670E-02);
	curLightSource->AddWavelength(1337.0f,1.5852E-02);
	curLightSource->AddWavelength(1338.0f,1.7151E-02);
	curLightSource->AddWavelength(1339.0f,1.7033E-02);
	curLightSource->AddWavelength(1340.0f,1.6216E-02);
	curLightSource->AddWavelength(1341.0f,1.6419E-02);
	curLightSource->AddWavelength(1342.0f,1.7149E-02);
	curLightSource->AddWavelength(1343.0f,1.2259E-02);
	curLightSource->AddWavelength(1344.0f,7.3018E-03);
	curLightSource->AddWavelength(1345.0f,1.0521E-02);
	curLightSource->AddWavelength(1346.0f,5.6189E-03);
	curLightSource->AddWavelength(1347.0f,5.8058E-03);
	curLightSource->AddWavelength(1348.0f,4.5862E-04);
	curLightSource->AddWavelength(1349.0f,1.5617E-03);
	curLightSource->AddWavelength(1350.0f,1.5488E-03);
	curLightSource->AddWavelength(1351.0f,4.4759E-04);
	curLightSource->AddWavelength(1352.0f,1.4661E-04);
	curLightSource->AddWavelength(1353.0f,9.2918E-06);
	curLightSource->AddWavelength(1354.0f,2.8051E-05);
	curLightSource->AddWavelength(1355.0f,3.4847E-07);
	curLightSource->AddWavelength(1356.0f,4.6489E-06);
	curLightSource->AddWavelength(1357.0f,6.9429E-06);
	curLightSource->AddWavelength(1358.0f,4.0575E-07);
	curLightSource->AddWavelength(1359.0f,7.1040E-08);
	curLightSource->AddWavelength(1360.0f,2.0706E-07);
	curLightSource->AddWavelength(1361.0f,4.6566E-10);
	curLightSource->AddWavelength(1362.0f,1.7489E-12);
	curLightSource->AddWavelength(1363.0f,3.0540E-07);
	curLightSource->AddWavelength(1364.0f,1.3150E-07);
	curLightSource->AddWavelength(1365.0f,8.7833E-13);
	curLightSource->AddWavelength(1366.0f,1.2379E-06);
	curLightSource->AddWavelength(1367.0f,4.8161E-07);
	curLightSource->AddWavelength(1368.0f,1.4311E-14);
	curLightSource->AddWavelength(1369.0f,5.0008E-08);
	curLightSource->AddWavelength(1370.0f,2.8266E-08);
	curLightSource->AddWavelength(1371.0f,1.9101E-09);
	curLightSource->AddWavelength(1372.0f,2.6623E-07);
	curLightSource->AddWavelength(1373.0f,4.2991E-06);
	curLightSource->AddWavelength(1374.0f,1.7350E-05);
	curLightSource->AddWavelength(1375.0f,3.1309E-05);
	curLightSource->AddWavelength(1376.0f,2.4935E-05);
	curLightSource->AddWavelength(1377.0f,1.1883E-05);
	curLightSource->AddWavelength(1378.0f,1.0741E-04);
	curLightSource->AddWavelength(1379.0f,5.0533E-06);
	curLightSource->AddWavelength(1380.0f,7.9042E-06);
	curLightSource->AddWavelength(1381.0f,2.2978E-07);
	curLightSource->AddWavelength(1382.0f,2.4874E-07);
	curLightSource->AddWavelength(1383.0f,4.2653E-09);
	curLightSource->AddWavelength(1384.0f,5.9782E-08);
	curLightSource->AddWavelength(1385.0f,2.0255E-07);
	curLightSource->AddWavelength(1386.0f,2.4441E-07);
	curLightSource->AddWavelength(1387.0f,1.9288E-05);
	curLightSource->AddWavelength(1388.0f,3.9037E-07);
	curLightSource->AddWavelength(1389.0f,5.6338E-05);
	curLightSource->AddWavelength(1390.0f,4.7836E-05);
	curLightSource->AddWavelength(1391.0f,3.3345E-05);
	curLightSource->AddWavelength(1392.0f,2.3065E-06);
	curLightSource->AddWavelength(1393.0f,1.1238E-05);
	curLightSource->AddWavelength(1394.0f,7.3268E-06);
	curLightSource->AddWavelength(1395.0f,6.5137E-08);
	curLightSource->AddWavelength(1396.0f,6.1338E-10);
	curLightSource->AddWavelength(1397.0f,4.7605E-06);
	curLightSource->AddWavelength(1398.0f,1.2329E-04);
	curLightSource->AddWavelength(1399.0f,7.8835E-05);
	curLightSource->AddWavelength(1400.0f,3.1513E-10);
	curLightSource->AddWavelength(1401.0f,1.0219E-09);
	curLightSource->AddWavelength(1402.0f,1.7817E-04);
	curLightSource->AddWavelength(1403.0f,2.3108E-04);
	curLightSource->AddWavelength(1404.0f,7.1755E-05);
	curLightSource->AddWavelength(1405.0f,3.5395E-08);
	curLightSource->AddWavelength(1406.0f,1.9861E-04);
	curLightSource->AddWavelength(1407.0f,1.6957E-05);
	curLightSource->AddWavelength(1408.0f,1.6023E-04);
	curLightSource->AddWavelength(1409.0f,6.0159E-05);
	curLightSource->AddWavelength(1410.0f,4.5332E-05);
	curLightSource->AddWavelength(1411.0f,2.0544E-04);
	curLightSource->AddWavelength(1412.0f,2.5650E-04);
	curLightSource->AddWavelength(1413.0f,2.2690E-03);
	curLightSource->AddWavelength(1414.0f,3.5359E-05);
	curLightSource->AddWavelength(1415.0f,1.7854E-05);
	curLightSource->AddWavelength(1416.0f,3.4561E-03);
	curLightSource->AddWavelength(1417.0f,1.1431E-03);
	curLightSource->AddWavelength(1418.0f,1.3182E-03);
	curLightSource->AddWavelength(1419.0f,2.0851E-04);
	curLightSource->AddWavelength(1420.0f,8.0437E-04);
	curLightSource->AddWavelength(1421.0f,8.9117E-04);
	curLightSource->AddWavelength(1422.0f,4.5023E-03);
	curLightSource->AddWavelength(1423.0f,8.9676E-04);
	curLightSource->AddWavelength(1424.0f,1.6511E-03);
	curLightSource->AddWavelength(1425.0f,2.5142E-03);
	curLightSource->AddWavelength(1426.0f,2.7032E-03);
	curLightSource->AddWavelength(1427.0f,4.8180E-03);
	curLightSource->AddWavelength(1428.0f,4.4360E-04);
	curLightSource->AddWavelength(1429.0f,3.6985E-03);
	curLightSource->AddWavelength(1430.0f,5.9912E-03);
	curLightSource->AddWavelength(1431.0f,4.8792E-03);
	curLightSource->AddWavelength(1432.0f,2.4524E-04);
	curLightSource->AddWavelength(1433.0f,3.4870E-03);
	curLightSource->AddWavelength(1434.0f,2.0403E-03);
	curLightSource->AddWavelength(1435.0f,2.0847E-03);
	curLightSource->AddWavelength(1436.0f,3.7326E-03);
	curLightSource->AddWavelength(1437.0f,2.9085E-03);
	curLightSource->AddWavelength(1438.0f,1.2913E-03);
	curLightSource->AddWavelength(1439.0f,4.9672E-03);
	curLightSource->AddWavelength(1440.0f,3.8547E-03);
	curLightSource->AddWavelength(1441.0f,3.0959E-03);
	curLightSource->AddWavelength(1442.0f,3.5356E-03);
	curLightSource->AddWavelength(1443.0f,4.3868E-03);
	curLightSource->AddWavelength(1444.0f,6.0143E-03);
	curLightSource->AddWavelength(1445.0f,4.8434E-03);
	curLightSource->AddWavelength(1446.0f,2.2492E-03);
	curLightSource->AddWavelength(1447.0f,3.5265E-03);
	curLightSource->AddWavelength(1448.0f,1.1254E-02);
	curLightSource->AddWavelength(1449.0f,9.9374E-03);
	curLightSource->AddWavelength(1450.0f,2.6699E-03);
	curLightSource->AddWavelength(1451.0f,1.0980E-03);
	curLightSource->AddWavelength(1452.0f,6.0718E-03);
	curLightSource->AddWavelength(1453.0f,7.9803E-03);
	curLightSource->AddWavelength(1454.0f,1.3384E-02);
	curLightSource->AddWavelength(1455.0f,6.4409E-03);
	curLightSource->AddWavelength(1456.0f,8.6158E-03);
	curLightSource->AddWavelength(1457.0f,1.1386E-02);
	curLightSource->AddWavelength(1458.0f,1.3273E-02);
	curLightSource->AddWavelength(1459.0f,1.5860E-02);
	curLightSource->AddWavelength(1460.0f,8.3161E-03);
	curLightSource->AddWavelength(1461.0f,8.7886E-03);
	curLightSource->AddWavelength(1462.0f,1.2708E-02);
	curLightSource->AddWavelength(1463.0f,4.2106E-03);
	curLightSource->AddWavelength(1464.0f,1.4770E-02);
	curLightSource->AddWavelength(1465.0f,9.0911E-03);
	curLightSource->AddWavelength(1466.0f,6.3498E-03);
	curLightSource->AddWavelength(1467.0f,3.5128E-03);
	curLightSource->AddWavelength(1468.0f,7.4928E-03);
	curLightSource->AddWavelength(1469.0f,9.2344E-03);
	curLightSource->AddWavelength(1470.0f,4.8397E-03);
	curLightSource->AddWavelength(1471.0f,1.7394E-03);
	curLightSource->AddWavelength(1472.0f,4.5566E-03);
	curLightSource->AddWavelength(1473.0f,6.8368E-03);
	curLightSource->AddWavelength(1474.0f,9.4765E-03);
	curLightSource->AddWavelength(1475.0f,1.7954E-02);
	curLightSource->AddWavelength(1476.0f,6.6987E-03);
	curLightSource->AddWavelength(1477.0f,6.7916E-03);
	curLightSource->AddWavelength(1478.0f,6.1825E-03);
	curLightSource->AddWavelength(1479.0f,1.1680E-02);
	curLightSource->AddWavelength(1480.0f,5.9063E-03);
	curLightSource->AddWavelength(1481.0f,1.1221E-02);
	curLightSource->AddWavelength(1482.0f,5.6967E-03);
	curLightSource->AddWavelength(1483.0f,1.4454E-02);
	curLightSource->AddWavelength(1484.0f,1.3375E-02);
	curLightSource->AddWavelength(1485.0f,1.2168E-02);
	curLightSource->AddWavelength(1486.0f,1.2008E-02);
	curLightSource->AddWavelength(1487.0f,5.9042E-03);
	curLightSource->AddWavelength(1488.0f,9.1654E-03);
	curLightSource->AddWavelength(1489.0f,1.8443E-02);
	curLightSource->AddWavelength(1490.0f,1.6993E-02);
	curLightSource->AddWavelength(1491.0f,1.9222E-02);
	curLightSource->AddWavelength(1492.0f,1.5986E-02);
	curLightSource->AddWavelength(1493.0f,1.7651E-02);
	curLightSource->AddWavelength(1494.0f,1.9794E-02);
	curLightSource->AddWavelength(1495.0f,1.7745E-02);
	curLightSource->AddWavelength(1496.0f,1.6385E-02);
	curLightSource->AddWavelength(1497.0f,2.2198E-02);
	curLightSource->AddWavelength(1498.0f,1.8437E-02);
	curLightSource->AddWavelength(1499.0f,2.1141E-02);
	curLightSource->AddWavelength(1500.0f,2.4339E-02);
	curLightSource->AddWavelength(1501.0f,2.5782E-02);
	curLightSource->AddWavelength(1502.0f,2.2688E-02);
	curLightSource->AddWavelength(1503.0f,1.7972E-02);
	curLightSource->AddWavelength(1504.0f,1.5586E-02);
	curLightSource->AddWavelength(1505.0f,1.7885E-02);
	curLightSource->AddWavelength(1506.0f,2.5026E-02);
	curLightSource->AddWavelength(1507.0f,2.4779E-02);
	curLightSource->AddWavelength(1508.0f,2.3606E-02);
	curLightSource->AddWavelength(1509.0f,1.8169E-02);
	curLightSource->AddWavelength(1510.0f,2.6269E-02);
	curLightSource->AddWavelength(1511.0f,2.5710E-02);
	curLightSource->AddWavelength(1512.0f,2.5315E-02);
	curLightSource->AddWavelength(1513.0f,2.3544E-02);
	curLightSource->AddWavelength(1514.0f,2.1930E-02);
	curLightSource->AddWavelength(1515.0f,2.5804E-02);
	curLightSource->AddWavelength(1516.0f,2.4943E-02);
	curLightSource->AddWavelength(1517.0f,2.4214E-02);
	curLightSource->AddWavelength(1518.0f,2.4489E-02);
	curLightSource->AddWavelength(1519.0f,2.3739E-02);
	curLightSource->AddWavelength(1520.0f,2.5688E-02);
	curLightSource->AddWavelength(1521.0f,2.6707E-02);
	curLightSource->AddWavelength(1522.0f,2.5617E-02);
	curLightSource->AddWavelength(1523.0f,2.7192E-02);
	curLightSource->AddWavelength(1524.0f,2.6743E-02);
	curLightSource->AddWavelength(1525.0f,2.5140E-02);
	curLightSource->AddWavelength(1526.0f,2.5977E-02);
	curLightSource->AddWavelength(1527.0f,2.5468E-02);
	curLightSource->AddWavelength(1528.0f,2.7122E-02);
	curLightSource->AddWavelength(1529.0f,2.6460E-02);
	curLightSource->AddWavelength(1530.0f,2.4789E-02);
	curLightSource->AddWavelength(1531.0f,2.6200E-02);
	curLightSource->AddWavelength(1532.0f,2.7039E-02);
	curLightSource->AddWavelength(1533.0f,2.6918E-02);
	curLightSource->AddWavelength(1534.0f,2.6122E-02);
	curLightSource->AddWavelength(1535.0f,2.5924E-02);
	curLightSource->AddWavelength(1536.0f,2.6679E-02);
	curLightSource->AddWavelength(1537.0f,2.6558E-02);
	curLightSource->AddWavelength(1538.0f,2.6427E-02);
	curLightSource->AddWavelength(1539.0f,2.6519E-02);
	curLightSource->AddWavelength(1540.0f,2.5737E-02);
	curLightSource->AddWavelength(1541.0f,2.6141E-02);
	curLightSource->AddWavelength(1542.0f,2.6165E-02);
	curLightSource->AddWavelength(1543.0f,2.6439E-02);
	curLightSource->AddWavelength(1544.0f,2.6443E-02);
	curLightSource->AddWavelength(1545.0f,2.6922E-02);
	curLightSource->AddWavelength(1546.0f,2.6705E-02);
	curLightSource->AddWavelength(1547.0f,2.6537E-02);
	curLightSource->AddWavelength(1548.0f,2.5920E-02);
	curLightSource->AddWavelength(1549.0f,2.6568E-02);
	curLightSource->AddWavelength(1550.0f,2.6226E-02);
	curLightSource->AddWavelength(1551.0f,2.6293E-02);
	curLightSource->AddWavelength(1552.0f,2.6415E-02);
	curLightSource->AddWavelength(1553.0f,2.6366E-02);
	curLightSource->AddWavelength(1554.0f,2.5727E-02);
	curLightSource->AddWavelength(1555.0f,2.6005E-02);
	curLightSource->AddWavelength(1556.0f,2.5569E-02);
	curLightSource->AddWavelength(1557.0f,2.6301E-02);
	curLightSource->AddWavelength(1558.0f,2.6093E-02);
	curLightSource->AddWavelength(1559.0f,2.6054E-02);
	curLightSource->AddWavelength(1560.0f,2.5821E-02);
	curLightSource->AddWavelength(1561.0f,2.6242E-02);
	curLightSource->AddWavelength(1562.0f,2.6003E-02);
	curLightSource->AddWavelength(1563.0f,2.5917E-02);
	curLightSource->AddWavelength(1564.0f,2.5525E-02);
	curLightSource->AddWavelength(1565.0f,2.5975E-02);
	curLightSource->AddWavelength(1566.0f,2.5506E-02);
	curLightSource->AddWavelength(1567.0f,2.5566E-02);
	curLightSource->AddWavelength(1568.0f,2.4997E-02);
	curLightSource->AddWavelength(1569.0f,2.4736E-02);
	curLightSource->AddWavelength(1570.0f,2.3497E-02);
	curLightSource->AddWavelength(1571.0f,2.2850E-02);
	curLightSource->AddWavelength(1572.0f,2.3108E-02);
	curLightSource->AddWavelength(1573.0f,2.2750E-02);
	curLightSource->AddWavelength(1574.0f,2.3464E-02);
	curLightSource->AddWavelength(1575.0f,2.3294E-02);
	curLightSource->AddWavelength(1576.0f,2.3980E-02);
	curLightSource->AddWavelength(1577.0f,2.0994E-02);
	curLightSource->AddWavelength(1578.0f,2.2854E-02);
	curLightSource->AddWavelength(1579.0f,2.3005E-02);
	curLightSource->AddWavelength(1580.0f,2.3772E-02);
	curLightSource->AddWavelength(1581.0f,2.4165E-02);
	curLightSource->AddWavelength(1582.0f,2.3508E-02);
	curLightSource->AddWavelength(1583.0f,2.4051E-02);
	curLightSource->AddWavelength(1584.0f,2.4194E-02);
	curLightSource->AddWavelength(1585.0f,2.5135E-02);
	curLightSource->AddWavelength(1586.0f,2.4838E-02);
	curLightSource->AddWavelength(1587.0f,2.4579E-02);
	curLightSource->AddWavelength(1588.0f,2.4388E-02);
	curLightSource->AddWavelength(1589.0f,2.2567E-02);
	curLightSource->AddWavelength(1590.0f,2.3486E-02);
	curLightSource->AddWavelength(1591.0f,2.3503E-02);
	curLightSource->AddWavelength(1592.0f,2.4502E-02);
	curLightSource->AddWavelength(1593.0f,2.5092E-02);
	curLightSource->AddWavelength(1594.0f,2.4890E-02);
	curLightSource->AddWavelength(1595.0f,2.5083E-02);
	curLightSource->AddWavelength(1596.0f,2.3751E-02);
	curLightSource->AddWavelength(1597.0f,2.3985E-02);
	curLightSource->AddWavelength(1598.0f,2.4693E-02);
	curLightSource->AddWavelength(1599.0f,2.3511E-02);
	curLightSource->AddWavelength(1600.0f,2.3133E-02);
	curLightSource->AddWavelength(1601.0f,2.1691E-02);
	curLightSource->AddWavelength(1602.0f,2.1780E-02);
	curLightSource->AddWavelength(1603.0f,2.1765E-02);
	curLightSource->AddWavelength(1604.0f,2.2197E-02);
	curLightSource->AddWavelength(1605.0f,2.3010E-02);
	curLightSource->AddWavelength(1606.0f,2.3453E-02);
	curLightSource->AddWavelength(1607.0f,2.2637E-02);
	curLightSource->AddWavelength(1608.0f,2.2343E-02);
	curLightSource->AddWavelength(1609.0f,2.2088E-02);
	curLightSource->AddWavelength(1610.0f,2.1146E-02);
	curLightSource->AddWavelength(1611.0f,2.2039E-02);
	curLightSource->AddWavelength(1612.0f,2.2422E-02);
	curLightSource->AddWavelength(1613.0f,2.3047E-02);
	curLightSource->AddWavelength(1614.0f,2.3162E-02);
	curLightSource->AddWavelength(1615.0f,2.3420E-02);
	curLightSource->AddWavelength(1616.0f,2.2395E-02);
	curLightSource->AddWavelength(1617.0f,2.2798E-02);
	curLightSource->AddWavelength(1618.0f,2.3660E-02);
	curLightSource->AddWavelength(1619.0f,2.3415E-02);
	curLightSource->AddWavelength(1620.0f,2.2783E-02);
	curLightSource->AddWavelength(1621.0f,2.2765E-02);
	curLightSource->AddWavelength(1622.0f,2.3081E-02);
	curLightSource->AddWavelength(1623.0f,2.3559E-02);
	curLightSource->AddWavelength(1624.0f,2.3582E-02);
	curLightSource->AddWavelength(1625.0f,2.3109E-02);
	curLightSource->AddWavelength(1626.0f,2.3294E-02);
	curLightSource->AddWavelength(1627.0f,2.3398E-02);
	curLightSource->AddWavelength(1628.0f,2.3446E-02);
	curLightSource->AddWavelength(1629.0f,2.3456E-02);
	curLightSource->AddWavelength(1630.0f,2.2984E-02);
	curLightSource->AddWavelength(1631.0f,2.3136E-02);
	curLightSource->AddWavelength(1632.0f,2.3151E-02);
	curLightSource->AddWavelength(1633.0f,2.2614E-02);
	curLightSource->AddWavelength(1634.0f,2.2628E-02);
	curLightSource->AddWavelength(1635.0f,2.2712E-02);
	curLightSource->AddWavelength(1636.0f,2.2879E-02);
	curLightSource->AddWavelength(1637.0f,2.2064E-02);
	curLightSource->AddWavelength(1638.0f,2.1393E-02);
	curLightSource->AddWavelength(1639.0f,2.1410E-02);
	curLightSource->AddWavelength(1640.0f,2.0913E-02);
	curLightSource->AddWavelength(1641.0f,2.1348E-02);
	curLightSource->AddWavelength(1642.0f,2.1467E-02);
	curLightSource->AddWavelength(1643.0f,2.0939E-02);
	curLightSource->AddWavelength(1644.0f,2.1733E-02);
	curLightSource->AddWavelength(1645.0f,2.1216E-02);
	curLightSource->AddWavelength(1646.0f,2.1147E-02);
	curLightSource->AddWavelength(1647.0f,2.2135E-02);
	curLightSource->AddWavelength(1648.0f,2.1057E-02);
	curLightSource->AddWavelength(1649.0f,2.1261E-02);
	curLightSource->AddWavelength(1650.0f,2.1902E-02);
	curLightSource->AddWavelength(1651.0f,2.0281E-02);
	curLightSource->AddWavelength(1652.0f,2.1754E-02);
	curLightSource->AddWavelength(1653.0f,2.1661E-02);
	curLightSource->AddWavelength(1654.0f,2.0991E-02);
	curLightSource->AddWavelength(1655.0f,2.1619E-02);
	curLightSource->AddWavelength(1656.0f,2.1494E-02);
	curLightSource->AddWavelength(1657.0f,2.1613E-02);
	curLightSource->AddWavelength(1658.0f,2.1870E-02);
	curLightSource->AddWavelength(1659.0f,2.1514E-02);
	curLightSource->AddWavelength(1660.0f,2.1721E-02);
	curLightSource->AddWavelength(1661.0f,2.1774E-02);
	curLightSource->AddWavelength(1662.0f,2.1313E-02);
	curLightSource->AddWavelength(1663.0f,2.1630E-02);
	curLightSource->AddWavelength(1664.0f,2.1498E-02);
	curLightSource->AddWavelength(1665.0f,2.0607E-02);
	curLightSource->AddWavelength(1666.0f,1.7411E-02);
	curLightSource->AddWavelength(1667.0f,2.0502E-02);
	curLightSource->AddWavelength(1668.0f,2.0881E-02);
	curLightSource->AddWavelength(1669.0f,2.0939E-02);
	curLightSource->AddWavelength(1670.0f,2.1573E-02);
	curLightSource->AddWavelength(1671.0f,2.1294E-02);
	curLightSource->AddWavelength(1672.0f,2.0582E-02);
	curLightSource->AddWavelength(1673.0f,2.1052E-02);
	curLightSource->AddWavelength(1674.0f,2.1002E-02);
	curLightSource->AddWavelength(1675.0f,2.0793E-02);
	curLightSource->AddWavelength(1676.0f,2.0584E-02);
	curLightSource->AddWavelength(1677.0f,2.0668E-02);
	curLightSource->AddWavelength(1678.0f,2.0365E-02);
	curLightSource->AddWavelength(1679.0f,2.0741E-02);
	curLightSource->AddWavelength(1680.0f,2.0017E-02);
	curLightSource->AddWavelength(1681.0f,1.8936E-02);
	curLightSource->AddWavelength(1682.0f,1.9830E-02);
	curLightSource->AddWavelength(1683.0f,2.0357E-02);
	curLightSource->AddWavelength(1684.0f,1.9283E-02);
	curLightSource->AddWavelength(1685.0f,2.0763E-02);
	curLightSource->AddWavelength(1686.0f,2.0476E-02);
	curLightSource->AddWavelength(1687.0f,1.9951E-02);
	curLightSource->AddWavelength(1688.0f,2.0465E-02);
	curLightSource->AddWavelength(1689.0f,2.0178E-02);
	curLightSource->AddWavelength(1690.0f,1.9991E-02);
	curLightSource->AddWavelength(1691.0f,1.8808E-02);
	curLightSource->AddWavelength(1692.0f,2.0174E-02);
	curLightSource->AddWavelength(1693.0f,2.0587E-02);
	curLightSource->AddWavelength(1694.0f,1.9950E-02);
	curLightSource->AddWavelength(1695.0f,2.0427E-02);
	curLightSource->AddWavelength(1696.0f,2.0383E-02);
	curLightSource->AddWavelength(1697.0f,1.7649E-02);
	curLightSource->AddWavelength(1698.0f,2.0207E-02);
	curLightSource->AddWavelength(1699.0f,2.0024E-02);
	curLightSource->AddWavelength(1700.0f,1.9464E-02);
	curLightSource->AddWavelength(1702.0f,1.9874E-02);
	curLightSource->AddWavelength(1705.0f,1.9275E-02);
	curLightSource->AddWavelength(1710.0f,1.8316E-02);
	curLightSource->AddWavelength(1715.0f,1.8490E-02);
	curLightSource->AddWavelength(1720.0f,1.8231E-02);
	curLightSource->AddWavelength(1725.0f,1.7367E-02);
	curLightSource->AddWavelength(1730.0f,1.6979E-02);
	curLightSource->AddWavelength(1735.0f,1.5758E-02);
	curLightSource->AddWavelength(1740.0f,1.6405E-02);
	curLightSource->AddWavelength(1745.0f,1.5105E-02);
	curLightSource->AddWavelength(1750.0f,1.6162E-02);
	curLightSource->AddWavelength(1755.0f,1.4931E-02);
	curLightSource->AddWavelength(1760.0f,1.5608E-02);
	curLightSource->AddWavelength(1765.0f,1.2967E-02);
	curLightSource->AddWavelength(1770.0f,1.3831E-02);
	curLightSource->AddWavelength(1775.0f,1.1213E-02);
	curLightSource->AddWavelength(1780.0f,9.8143E-03);
	curLightSource->AddWavelength(1785.0f,7.5201E-03);
	curLightSource->AddWavelength(1790.0f,8.6831E-03);
	curLightSource->AddWavelength(1795.0f,4.5864E-03);
	curLightSource->AddWavelength(1800.0f,3.1112E-03);
	curLightSource->AddWavelength(1805.0f,1.4485E-03);
	curLightSource->AddWavelength(1810.0f,9.4762E-04);
	curLightSource->AddWavelength(1815.0f,3.2093E-04);
	curLightSource->AddWavelength(1820.0f,9.6578E-05);
	curLightSource->AddWavelength(1825.0f,1.2463E-04);
	curLightSource->AddWavelength(1830.0f,5.0896E-07);
	curLightSource->AddWavelength(1835.0f,6.2784E-07);
	curLightSource->AddWavelength(1840.0f,6.1337E-09);
	curLightSource->AddWavelength(1845.0f,6.1298E-07);
	curLightSource->AddWavelength(1850.0f,2.9348E-07);
	curLightSource->AddWavelength(1855.0f,2.7795E-08);
	curLightSource->AddWavelength(1860.0f,1.0920E-06);
	curLightSource->AddWavelength(1865.0f,1.6644E-06);
	curLightSource->AddWavelength(1870.0f,2.6148E-11);
	curLightSource->AddWavelength(1875.0f,4.4296E-11);
	curLightSource->AddWavelength(1880.0f,7.6123E-06);
	curLightSource->AddWavelength(1885.0f,4.3129E-06);
	curLightSource->AddWavelength(1890.0f,2.1956E-05);
	curLightSource->AddWavelength(1895.0f,1.2743E-05);
	curLightSource->AddWavelength(1900.0f,8.4916E-08);
	curLightSource->AddWavelength(1905.0f,5.5798E-08);
	curLightSource->AddWavelength(1910.0f,2.2726E-06);
	curLightSource->AddWavelength(1915.0f,1.9673E-06);
	curLightSource->AddWavelength(1920.0f,4.4451E-05);
	curLightSource->AddWavelength(1925.0f,9.2326E-05);
	curLightSource->AddWavelength(1930.0f,5.4474E-05);
	curLightSource->AddWavelength(1935.0f,3.5428E-04);
	curLightSource->AddWavelength(1940.0f,3.2357E-04);
	curLightSource->AddWavelength(1945.0f,1.0707E-03);
	curLightSource->AddWavelength(1950.0f,1.6482E-03);
	curLightSource->AddWavelength(1955.0f,9.8860E-04);
	curLightSource->AddWavelength(1960.0f,2.1569E-03);
	curLightSource->AddWavelength(1965.0f,2.8114E-03);
	curLightSource->AddWavelength(1970.0f,4.8055E-03);
	curLightSource->AddWavelength(1975.0f,6.6730E-03);
	curLightSource->AddWavelength(1980.0f,7.4234E-03);
	curLightSource->AddWavelength(1985.0f,8.1625E-03);
	curLightSource->AddWavelength(1990.0f,8.4124E-03);
	curLightSource->AddWavelength(1995.0f,7.9787E-03);
	curLightSource->AddWavelength(2000.0f,3.7491E-03);
	curLightSource->AddWavelength(2005.0f,1.4747E-03);
	curLightSource->AddWavelength(2010.0f,3.9071E-03);
	curLightSource->AddWavelength(2015.0f,2.6208E-03);
	curLightSource->AddWavelength(2020.0f,4.4239E-03);
	curLightSource->AddWavelength(2025.0f,7.2779E-03);
	curLightSource->AddWavelength(2030.0f,8.3460E-03);
	curLightSource->AddWavelength(2035.0f,9.4808E-03);
	curLightSource->AddWavelength(2040.0f,8.8344E-03);
	curLightSource->AddWavelength(2045.0f,8.9636E-03);
	curLightSource->AddWavelength(2050.0f,6.6892E-03);
	curLightSource->AddWavelength(2055.0f,5.4090E-03);
	curLightSource->AddWavelength(2060.0f,6.8157E-03);
	curLightSource->AddWavelength(2065.0f,6.0962E-03);
	curLightSource->AddWavelength(2070.0f,6.4715E-03);
	curLightSource->AddWavelength(2075.0f,7.6305E-03);
	curLightSource->AddWavelength(2080.0f,8.5528E-03);
	curLightSource->AddWavelength(2085.0f,8.3847E-03);
	curLightSource->AddWavelength(2090.0f,8.7779E-03);
	curLightSource->AddWavelength(2095.0f,8.8421E-03);
	curLightSource->AddWavelength(2100.0f,8.4869E-03);
	curLightSource->AddWavelength(2105.0f,9.1771E-03);
	curLightSource->AddWavelength(2110.0f,8.8320E-03);
	curLightSource->AddWavelength(2115.0f,9.0308E-03);
	curLightSource->AddWavelength(2120.0f,8.6281E-03);
	curLightSource->AddWavelength(2125.0f,8.7303E-03);
	curLightSource->AddWavelength(2130.0f,8.8422E-03);
	curLightSource->AddWavelength(2135.0f,8.8679E-03);
	curLightSource->AddWavelength(2140.0f,8.9390E-03);
	curLightSource->AddWavelength(2145.0f,8.8132E-03);
	curLightSource->AddWavelength(2150.0f,8.3369E-03);
	curLightSource->AddWavelength(2155.0f,8.3566E-03);
	curLightSource->AddWavelength(2160.0f,8.2912E-03);
	curLightSource->AddWavelength(2165.0f,7.5175E-03);
	curLightSource->AddWavelength(2170.0f,8.0776E-03);
	curLightSource->AddWavelength(2175.0f,7.9257E-03);
	curLightSource->AddWavelength(2180.0f,8.0597E-03);
	curLightSource->AddWavelength(2185.0f,7.3458E-03);
	curLightSource->AddWavelength(2190.0f,7.7905E-03);
	curLightSource->AddWavelength(2195.0f,7.7833E-03);
	curLightSource->AddWavelength(2200.0f,7.0175E-03);
	curLightSource->AddWavelength(2205.0f,7.2947E-03);
	curLightSource->AddWavelength(2210.0f,7.8174E-03);
	curLightSource->AddWavelength(2215.0f,7.5189E-03);
	curLightSource->AddWavelength(2220.0f,7.6631E-03);
	curLightSource->AddWavelength(2225.0f,7.4400E-03);
	curLightSource->AddWavelength(2230.0f,7.4727E-03);
	curLightSource->AddWavelength(2235.0f,7.3290E-03);
	curLightSource->AddWavelength(2240.0f,7.2140E-03);
	curLightSource->AddWavelength(2245.0f,6.9911E-03);
	curLightSource->AddWavelength(2250.0f,7.1034E-03);
	curLightSource->AddWavelength(2255.0f,6.6865E-03);
	curLightSource->AddWavelength(2260.0f,6.6143E-03);
	curLightSource->AddWavelength(2265.0f,6.7355E-03);
	curLightSource->AddWavelength(2270.0f,6.4138E-03);
	curLightSource->AddWavelength(2275.0f,6.3309E-03);
	curLightSource->AddWavelength(2280.0f,6.5551E-03);
	curLightSource->AddWavelength(2285.0f,6.2389E-03);
	curLightSource->AddWavelength(2290.0f,6.2534E-03);
	curLightSource->AddWavelength(2295.0f,6.0603E-03);
	curLightSource->AddWavelength(2300.0f,5.8193E-03);
	curLightSource->AddWavelength(2305.0f,5.8544E-03);
	curLightSource->AddWavelength(2310.0f,6.3189E-03);
	curLightSource->AddWavelength(2315.0f,5.7528E-03);
	curLightSource->AddWavelength(2320.0f,5.1489E-03);
	curLightSource->AddWavelength(2325.0f,5.5626E-03);
	curLightSource->AddWavelength(2330.0f,5.6231E-03);
	curLightSource->AddWavelength(2335.0f,5.7362E-03);
	curLightSource->AddWavelength(2340.0f,4.5366E-03);
	curLightSource->AddWavelength(2345.0f,5.0869E-03);
	curLightSource->AddWavelength(2350.0f,4.1115E-03);
	curLightSource->AddWavelength(2355.0f,4.6988E-03);
	curLightSource->AddWavelength(2360.0f,4.9724E-03);
	curLightSource->AddWavelength(2365.0f,4.8909E-03);
	curLightSource->AddWavelength(2370.0f,3.0514E-03);
	curLightSource->AddWavelength(2375.0f,4.3704E-03);
	curLightSource->AddWavelength(2380.0f,4.2128E-03);
	curLightSource->AddWavelength(2385.0f,3.0525E-03);
	curLightSource->AddWavelength(2390.0f,3.6748E-03);
	curLightSource->AddWavelength(2395.0f,4.0199E-03);
	curLightSource->AddWavelength(2400.0f,4.3726E-03);
	curLightSource->AddWavelength(2405.0f,3.3286E-03);
	curLightSource->AddWavelength(2410.0f,3.3504E-03);
	curLightSource->AddWavelength(2415.0f,2.7058E-03);
	curLightSource->AddWavelength(2420.0f,2.6358E-03);
	curLightSource->AddWavelength(2425.0f,3.2802E-03);
	curLightSource->AddWavelength(2430.0f,4.4725E-03);
	curLightSource->AddWavelength(2435.0f,1.4765E-03);
	curLightSource->AddWavelength(2440.0f,4.2926E-03);
	curLightSource->AddWavelength(2445.0f,2.0657E-03);
	curLightSource->AddWavelength(2450.0f,1.3523E-03);
	curLightSource->AddWavelength(2455.0f,2.4695E-03);
	curLightSource->AddWavelength(2460.0f,3.3157E-03);
	curLightSource->AddWavelength(2465.0f,2.4009E-03);
	curLightSource->AddWavelength(2470.0f,1.6635E-03);
	curLightSource->AddWavelength(2475.0f,1.6368E-03);
	curLightSource->AddWavelength(2480.0f,7.9996E-04);
	curLightSource->AddWavelength(2485.0f,5.5840E-04);
	curLightSource->AddWavelength(2490.0f,3.4957E-04);
	curLightSource->AddWavelength(2495.0f,2.8647E-04);
	curLightSource->AddWavelength(2500.0f,7.0328E-04);
	curLightSource->AddWavelength(2505.0f,1.5124E-04);
	curLightSource->AddWavelength(2510.0f,2.2063E-04);
	curLightSource->AddWavelength(2515.0f,5.1644E-05);
	curLightSource->AddWavelength(2520.0f,3.6879E-05);
	curLightSource->AddWavelength(2525.0f,4.1194E-06);
	curLightSource->AddWavelength(2530.0f,6.3279E-08);
	curLightSource->AddWavelength(2535.0f,1.7415E-08);
	curLightSource->AddWavelength(2540.0f,3.7521E-08);
	curLightSource->AddWavelength(2545.0f,5.3469E-12);
	curLightSource->AddWavelength(2550.0f,2.8066E-14);
	curLightSource->AddWavelength(2555.0f,1.0377E-10);
	curLightSource->AddWavelength(2560.0f,3.0842E-12);
	curLightSource->AddWavelength(2565.0f,1.5846E-15);
	curLightSource->AddWavelength(2570.0f,1.5151E-19);
	curLightSource->AddWavelength(2575.0f,1.0708E-28);
	curLightSource->AddWavelength(2580.0f,3.7933E-23);
	curLightSource->AddWavelength(2585.0f,1.7064E-35);
	curLightSource->AddWavelength(2590.0f,5.4369E-32);
	curLightSource->AddWavelength(2595.0f,2.2666E-34);
	curLightSource->AddWavelength(2600.0f,4.4556E-29);
	curLightSource->AddWavelength(2605.0f,5.7592E-36);
	curLightSource->AddWavelength(2610.0f,5.8970E-35);
	curLightSource->AddWavelength(2615.0f,1.1106E-38);
	curLightSource->AddWavelength(2620.0f,5.6056E-30);
	curLightSource->AddWavelength(2625.0f,3.8378E-29);
	curLightSource->AddWavelength(2630.0f,2.8026E-46);
	curLightSource->AddWavelength(2635.0f,3.8719E-17);
	curLightSource->AddWavelength(2640.0f,1.1657E-17);
	curLightSource->AddWavelength(2645.0f,8.9292E-20);
	curLightSource->AddWavelength(2650.0f,1.4186E-20);
	curLightSource->AddWavelength(2655.0f,1.3036E-28);
	curLightSource->AddWavelength(2660.0f,2.5880E-26);
	curLightSource->AddWavelength(2665.0f,1.1045E-38);
	curLightSource->AddWavelength(2670.0f,0.0000E+00);
	curLightSource->AddWavelength(2675.0f,0.0000E+00);
	curLightSource->AddWavelength(2680.0f,0.0000E+00);
	curLightSource->AddWavelength(2685.0f,0.0000E+00);
	curLightSource->AddWavelength(2690.0f,1.0178E-30);
	curLightSource->AddWavelength(2695.0f,7.1014E-34);
	curLightSource->AddWavelength(2700.0f,0.0000E+00);
	curLightSource->AddWavelength(2705.0f,2.9273E-43);
	curLightSource->AddWavelength(2710.0f,1.1239E-36);
	curLightSource->AddWavelength(2715.0f,3.8549E-27);
	curLightSource->AddWavelength(2720.0f,5.6052E-46);
	curLightSource->AddWavelength(2725.0f,7.3094E-23);
	curLightSource->AddWavelength(2730.0f,6.0929E-20);
	curLightSource->AddWavelength(2735.0f,5.5121E-22);
	curLightSource->AddWavelength(2740.0f,2.3435E-28);
	curLightSource->AddWavelength(2745.0f,1.3224E-24);
	curLightSource->AddWavelength(2750.0f,1.6758E-29);
	curLightSource->AddWavelength(2755.0f,6.7262E-45);
	curLightSource->AddWavelength(2760.0f,0.0000E+00);
	curLightSource->AddWavelength(2765.0f,2.7001E-28);
	curLightSource->AddWavelength(2770.0f,8.4528E-25);
	curLightSource->AddWavelength(2775.0f,4.0360E-39);
	curLightSource->AddWavelength(2780.0f,4.8532E-35);
	curLightSource->AddWavelength(2785.0f,3.9255E-28);
	curLightSource->AddWavelength(2790.0f,1.2295E-17);
	curLightSource->AddWavelength(2795.0f,3.6591E-17);
	curLightSource->AddWavelength(2800.0f,1.6665E-13);
	curLightSource->AddWavelength(2805.0f,6.8228E-15);
	curLightSource->AddWavelength(2810.0f,4.0695E-11);
	curLightSource->AddWavelength(2815.0f,2.9018E-11);
	curLightSource->AddWavelength(2820.0f,2.0789E-12);
	curLightSource->AddWavelength(2825.0f,1.7814E-08);
	curLightSource->AddWavelength(2830.0f,3.9475E-07);
	curLightSource->AddWavelength(2835.0f,2.1533E-11);
	curLightSource->AddWavelength(2840.0f,1.9849E-08);
	curLightSource->AddWavelength(2845.0f,4.1074E-06);
	curLightSource->AddWavelength(2850.0f,1.1708E-07);
	curLightSource->AddWavelength(2855.0f,4.5425E-08);
	curLightSource->AddWavelength(2860.0f,2.5672E-06);
	curLightSource->AddWavelength(2865.0f,1.6974E-05);
	curLightSource->AddWavelength(2870.0f,6.3922E-07);
	curLightSource->AddWavelength(2875.0f,3.9663E-05);
	curLightSource->AddWavelength(2880.0f,2.5037E-05);
	curLightSource->AddWavelength(2885.0f,4.5906E-05);
	curLightSource->AddWavelength(2890.0f,1.8860E-05);
	curLightSource->AddWavelength(2895.0f,2.6982E-04);
	curLightSource->AddWavelength(2900.0f,8.2183E-05);
	curLightSource->AddWavelength(2905.0f,1.1237E-05);
	curLightSource->AddWavelength(2910.0f,2.7566E-04);
	curLightSource->AddWavelength(2915.0f,1.2742E-04);
	curLightSource->AddWavelength(2920.0f,2.9316E-04);
	curLightSource->AddWavelength(2925.0f,1.0973E-04);
	curLightSource->AddWavelength(2930.0f,5.9606E-04);
	curLightSource->AddWavelength(2935.0f,6.5725E-04);
	curLightSource->AddWavelength(2940.0f,1.6479E-04);
	curLightSource->AddWavelength(2945.0f,1.4673E-04);
	curLightSource->AddWavelength(2950.0f,5.2935E-04);
	curLightSource->AddWavelength(2955.0f,2.3655E-04);
	curLightSource->AddWavelength(2960.0f,4.6549E-04);
	curLightSource->AddWavelength(2965.0f,7.5313E-04);
	curLightSource->AddWavelength(2970.0f,3.5676E-05);
	curLightSource->AddWavelength(2975.0f,8.6499E-05);
	curLightSource->AddWavelength(2980.0f,1.3548E-04);
	curLightSource->AddWavelength(2985.0f,7.0495E-04);
	curLightSource->AddWavelength(2990.0f,1.0407E-03);
	curLightSource->AddWavelength(2995.0f,4.3286E-04);
	curLightSource->AddWavelength(3000.0f,7.9442E-04);
	curLightSource->AddWavelength(3005.0f,2.9261E-04);
	curLightSource->AddWavelength(3010.0f,6.9320E-04);
	curLightSource->AddWavelength(3015.0f,5.6232E-04);
	curLightSource->AddWavelength(3020.0f,6.4143E-05);
	curLightSource->AddWavelength(3025.0f,7.5948E-04);
	curLightSource->AddWavelength(3030.0f,6.1491E-04);
	curLightSource->AddWavelength(3035.0f,2.5290E-04);
	curLightSource->AddWavelength(3040.0f,2.0487E-04);
	curLightSource->AddWavelength(3045.0f,4.2599E-04);
	curLightSource->AddWavelength(3050.0f,1.0446E-04);
	curLightSource->AddWavelength(3055.0f,2.9295E-05);
	curLightSource->AddWavelength(3060.0f,6.3761E-04);
	curLightSource->AddWavelength(3065.0f,2.9458E-04);
	curLightSource->AddWavelength(3070.0f,1.7699E-04);
	curLightSource->AddWavelength(3075.0f,6.0928E-04);
	curLightSource->AddWavelength(3080.0f,3.6646E-04);
	curLightSource->AddWavelength(3085.0f,1.7876E-04);
	curLightSource->AddWavelength(3090.0f,2.4080E-04);
	curLightSource->AddWavelength(3095.0f,6.6266E-05);
	curLightSource->AddWavelength(3100.0f,4.4513E-04);
	curLightSource->AddWavelength(3105.0f,9.3203E-05);
	curLightSource->AddWavelength(3110.0f,8.5524E-05);
	curLightSource->AddWavelength(3115.0f,2.2932E-04);
	curLightSource->AddWavelength(3120.0f,9.9294E-04);
	curLightSource->AddWavelength(3125.0f,3.0625E-04);
	curLightSource->AddWavelength(3130.0f,5.8246E-04);
	curLightSource->AddWavelength(3135.0f,1.1571E-03);
	curLightSource->AddWavelength(3140.0f,3.3603E-04);
	curLightSource->AddWavelength(3145.0f,3.2869E-04);
	curLightSource->AddWavelength(3150.0f,6.7457E-04);
	curLightSource->AddWavelength(3155.0f,5.6965E-04);
	curLightSource->AddWavelength(3160.0f,9.3294E-04);
	curLightSource->AddWavelength(3165.0f,1.4163E-03);
	curLightSource->AddWavelength(3170.0f,1.2645E-03);
	curLightSource->AddWavelength(3175.0f,9.3252E-04);
	curLightSource->AddWavelength(3180.0f,1.0730E-03);
	curLightSource->AddWavelength(3185.0f,8.1644E-04);
	curLightSource->AddWavelength(3190.0f,4.2817E-04);
	curLightSource->AddWavelength(3195.0f,2.7197E-04);
	curLightSource->AddWavelength(3200.0f,4.4280E-05);
	curLightSource->AddWavelength(3205.0f,3.1280E-05);
	curLightSource->AddWavelength(3210.0f,1.3768E-05);
	curLightSource->AddWavelength(3215.0f,5.0237E-05);
	curLightSource->AddWavelength(3220.0f,1.6245E-04);
	curLightSource->AddWavelength(3225.0f,2.0066E-05);
	curLightSource->AddWavelength(3230.0f,3.4404E-05);
	curLightSource->AddWavelength(3235.0f,7.3625E-04);
	curLightSource->AddWavelength(3240.0f,3.7816E-04);
	curLightSource->AddWavelength(3245.0f,7.4096E-05);
	curLightSource->AddWavelength(3250.0f,2.6308E-04);
	curLightSource->AddWavelength(3255.0f,1.0029E-03);
	curLightSource->AddWavelength(3260.0f,1.2360E-04);
	curLightSource->AddWavelength(3265.0f,2.4686E-04);
	curLightSource->AddWavelength(3270.0f,1.2292E-04);
	curLightSource->AddWavelength(3275.0f,5.9758E-04);
	curLightSource->AddWavelength(3280.0f,2.8890E-04);
	curLightSource->AddWavelength(3285.0f,1.1223E-03);
	curLightSource->AddWavelength(3290.0f,8.8310E-04);
	curLightSource->AddWavelength(3295.0f,1.2337E-04);
	curLightSource->AddWavelength(3300.0f,1.7944E-04);
	curLightSource->AddWavelength(3305.0f,3.9746E-04);
	curLightSource->AddWavelength(3310.0f,3.9565E-04);
	curLightSource->AddWavelength(3315.0f,1.6269E-06);
	curLightSource->AddWavelength(3320.0f,6.0496E-06);
	curLightSource->AddWavelength(3325.0f,3.5486E-04);
	curLightSource->AddWavelength(3330.0f,4.7014E-04);
	curLightSource->AddWavelength(3335.0f,9.1470E-04);
	curLightSource->AddWavelength(3340.0f,3.4903E-04);
	curLightSource->AddWavelength(3345.0f,3.5722E-04);
	curLightSource->AddWavelength(3350.0f,8.0998E-04);
	curLightSource->AddWavelength(3355.0f,3.6642E-04);
	curLightSource->AddWavelength(3360.0f,5.2892E-04);
	curLightSource->AddWavelength(3365.0f,7.2591E-04);
	curLightSource->AddWavelength(3370.0f,3.9770E-04);
	curLightSource->AddWavelength(3375.0f,8.5388E-04);
	curLightSource->AddWavelength(3380.0f,5.1624E-04);
	curLightSource->AddWavelength(3385.0f,7.5650E-04);
	curLightSource->AddWavelength(3390.0f,9.9552E-04);
	curLightSource->AddWavelength(3395.0f,9.6446E-04);
	curLightSource->AddWavelength(3400.0f,1.2638E-03);
	curLightSource->AddWavelength(3405.0f,4.5061E-04);
	curLightSource->AddWavelength(3410.0f,7.1547E-04);
	curLightSource->AddWavelength(3415.0f,7.3543E-04);
	curLightSource->AddWavelength(3420.0f,1.3305E-03);
	curLightSource->AddWavelength(3425.0f,1.0112E-03);
	curLightSource->AddWavelength(3430.0f,8.7810E-04);
	curLightSource->AddWavelength(3435.0f,1.1673E-03);
	curLightSource->AddWavelength(3440.0f,8.1181E-04);
	curLightSource->AddWavelength(3445.0f,1.1434E-03);
	curLightSource->AddWavelength(3450.0f,1.1267E-03);
	curLightSource->AddWavelength(3455.0f,8.3938E-04);
	curLightSource->AddWavelength(3460.0f,1.2657E-03);
	curLightSource->AddWavelength(3465.0f,9.9163E-04);
	curLightSource->AddWavelength(3470.0f,1.2386E-03);
	curLightSource->AddWavelength(3475.0f,1.1051E-03);
	curLightSource->AddWavelength(3480.0f,1.1335E-03);
	curLightSource->AddWavelength(3485.0f,1.2212E-03);
	curLightSource->AddWavelength(3490.0f,1.0520E-03);
	curLightSource->AddWavelength(3495.0f,1.2383E-03);
	curLightSource->AddWavelength(3500.0f,1.2032E-03);
	curLightSource->AddWavelength(3505.0f,1.1923E-03);
	curLightSource->AddWavelength(3510.0f,1.2077E-03);
	curLightSource->AddWavelength(3515.0f,1.1598E-03);
	curLightSource->AddWavelength(3520.0f,1.2226E-03);
	curLightSource->AddWavelength(3525.0f,1.1523E-03);
	curLightSource->AddWavelength(3530.0f,1.1217E-03);
	curLightSource->AddWavelength(3535.0f,9.5305E-04);
	curLightSource->AddWavelength(3540.0f,9.1011E-04);
	curLightSource->AddWavelength(3545.0f,9.6154E-04);
	curLightSource->AddWavelength(3550.0f,1.0616E-03);
	curLightSource->AddWavelength(3555.0f,9.1245E-04);
	curLightSource->AddWavelength(3560.0f,1.0872E-03);
	curLightSource->AddWavelength(3565.0f,1.0928E-03);
	curLightSource->AddWavelength(3570.0f,8.3951E-04);
	curLightSource->AddWavelength(3575.0f,8.7028E-04);
	curLightSource->AddWavelength(3580.0f,1.0255E-03);
	curLightSource->AddWavelength(3585.0f,9.2282E-04);
	curLightSource->AddWavelength(3590.0f,9.5145E-04);
	curLightSource->AddWavelength(3595.0f,9.7329E-04);
	curLightSource->AddWavelength(3600.0f,1.0328E-03);
	curLightSource->AddWavelength(3605.0f,1.0425E-03);
	curLightSource->AddWavelength(3610.0f,9.5385E-04);
	curLightSource->AddWavelength(3615.0f,9.5318E-04);
	curLightSource->AddWavelength(3620.0f,1.1686E-03);
	curLightSource->AddWavelength(3625.0f,1.0302E-03);
	curLightSource->AddWavelength(3630.0f,1.0016E-03);
	curLightSource->AddWavelength(3635.0f,1.0362E-03);
	curLightSource->AddWavelength(3640.0f,1.1549E-03);
	curLightSource->AddWavelength(3645.0f,1.0663E-03);
	curLightSource->AddWavelength(3650.0f,1.0183E-03);
	curLightSource->AddWavelength(3655.0f,1.1042E-03);
	curLightSource->AddWavelength(3660.0f,1.0978E-03);
	curLightSource->AddWavelength(3665.0f,1.0313E-03);
	curLightSource->AddWavelength(3670.0f,7.9464E-04);
	curLightSource->AddWavelength(3675.0f,4.8569E-04);
	curLightSource->AddWavelength(3680.0f,8.3789E-04);
	curLightSource->AddWavelength(3685.0f,9.4906E-04);
	curLightSource->AddWavelength(3690.0f,9.7458E-04);
	curLightSource->AddWavelength(3695.0f,1.0188E-03);
	curLightSource->AddWavelength(3700.0f,1.0937E-03);
	curLightSource->AddWavelength(3705.0f,1.0827E-03);
	curLightSource->AddWavelength(3710.0f,9.4130E-04);
	curLightSource->AddWavelength(3715.0f,9.2731E-04);
	curLightSource->AddWavelength(3720.0f,1.0429E-03);
	curLightSource->AddWavelength(3725.0f,1.0751E-03);
	curLightSource->AddWavelength(3730.0f,9.3169E-04);
	curLightSource->AddWavelength(3735.0f,8.6260E-04);
	curLightSource->AddWavelength(3740.0f,8.8923E-04);
	curLightSource->AddWavelength(3745.0f,1.0380E-03);
	curLightSource->AddWavelength(3750.0f,9.3341E-04);
	curLightSource->AddWavelength(3755.0f,9.0336E-04);
	curLightSource->AddWavelength(3760.0f,8.9041E-04);
	curLightSource->AddWavelength(3765.0f,8.5891E-04);
	curLightSource->AddWavelength(3770.0f,9.1649E-04);
	curLightSource->AddWavelength(3775.0f,9.0918E-04);
	curLightSource->AddWavelength(3780.0f,9.6158E-04);
	curLightSource->AddWavelength(3785.0f,8.8498E-04);
	curLightSource->AddWavelength(3790.0f,7.7891E-04);
	curLightSource->AddWavelength(3795.0f,8.9056E-04);
	curLightSource->AddWavelength(3800.0f,9.8988E-04);
	curLightSource->AddWavelength(3805.0f,9.3414E-04);
	curLightSource->AddWavelength(3810.0f,8.2771E-04);
	curLightSource->AddWavelength(3815.0f,7.7867E-04);
	curLightSource->AddWavelength(3820.0f,9.6908E-04);
	curLightSource->AddWavelength(3825.0f,9.5402E-04);
	curLightSource->AddWavelength(3830.0f,9.6268E-04);
	curLightSource->AddWavelength(3835.0f,7.7190E-04);
	curLightSource->AddWavelength(3840.0f,9.0066E-04);
	curLightSource->AddWavelength(3845.0f,8.8097E-04);
	curLightSource->AddWavelength(3850.0f,8.8569E-04);
	curLightSource->AddWavelength(3855.0f,8.5365E-04);
	curLightSource->AddWavelength(3860.0f,8.0197E-04);
	curLightSource->AddWavelength(3865.0f,8.1248E-04);
	curLightSource->AddWavelength(3870.0f,7.3839E-04);
	curLightSource->AddWavelength(3875.0f,6.7832E-04);
	curLightSource->AddWavelength(3880.0f,6.5543E-04);
	curLightSource->AddWavelength(3885.0f,6.7926E-04);
	curLightSource->AddWavelength(3890.0f,6.9028E-04);
	curLightSource->AddWavelength(3895.0f,7.4983E-04);
	curLightSource->AddWavelength(3900.0f,7.9487E-04);
	curLightSource->AddWavelength(3905.0f,7.9500E-04);
	curLightSource->AddWavelength(3910.0f,7.1560E-04);
	curLightSource->AddWavelength(3915.0f,7.0070E-04);
	curLightSource->AddWavelength(3920.0f,6.9665E-04);
	curLightSource->AddWavelength(3925.0f,6.8714E-04);
	curLightSource->AddWavelength(3930.0f,7.0699E-04);
	curLightSource->AddWavelength(3935.0f,7.3743E-04);
	curLightSource->AddWavelength(3940.0f,7.4228E-04);
	curLightSource->AddWavelength(3945.0f,7.5613E-04);
	curLightSource->AddWavelength(3950.0f,7.6475E-04);
	curLightSource->AddWavelength(3955.0f,7.7399E-04);
	curLightSource->AddWavelength(3960.0f,7.7679E-04);
	curLightSource->AddWavelength(3965.0f,7.8253E-04);
	curLightSource->AddWavelength(3970.0f,7.6997E-04);
	curLightSource->AddWavelength(3975.0f,7.5280E-04);
	curLightSource->AddWavelength(3980.0f,7.4049E-04);
	curLightSource->AddWavelength(3985.0f,7.4503E-04);
	curLightSource->AddWavelength(3990.0f,7.3894E-04);
	curLightSource->AddWavelength(3995.0f,7.2263E-04);
	curLightSource->AddWavelength(4000.0f,7.1199E-04);
	
	TransferToGUI();
	grdDataTable->AutoSizeColumns();
	/*
	wxSize size = grdDataTable->GetSize();
	size.SetWidth(280 + wxSystemSettings::GetMetric(wxSYS_VSCROLL_X));
	bxszrDataPlot->Layout();
	*/
//	szrPrimary->Layout();
}

BEGIN_EVENT_TABLE(simSpectraPanel, wxPanel)
EVT_GRID_CMD_CELL_CHANGED(SPEC_GRID, simSpectraPanel::GridText)
EVT_GRID_CMD_SELECT_CELL(SPEC_GRID, simSpectraPanel::GridClick)
EVT_BUTTON(SPEC_ADD_WAVE, simSpectraPanel::AddWavelength)
EVT_BUTTON(SPEC_NEW, simSpectraPanel::CreateSpectra)
EVT_BUTTON(SPEC_SAVE, simSpectraPanel::SaveSpectra)
EVT_BUTTON(SPEC_LOAD, simSpectraPanel::LoadSpectra)
EVT_BUTTON(SPEC_RENAME, simSpectraPanel::RenameSpectra)
EVT_BUTTON(SPEC_REMOVE, simSpectraPanel::RemoveSpectra)
EVT_BUTTON(SPEC_REM_WAVE, simSpectraPanel::RemoveWavelength)
EVT_BUTTON(SPEC_CLEAR_WAVES, simSpectraPanel::ClearWavelengths)
EVT_BUTTON(SPEC_DUPLICATE, simSpectraPanel::DuplicateSpectra)
EVT_BUTTON(SPEC_AM0, simSpectraPanel::SetAM0)
EVT_BUTTON(SPEC_AM15, simSpectraPanel::SetAM15)
EVT_RADIOBOX(SPEC_RAD, simSpectraPanel::RadBoxModifed)
EVT_TEXT(SPEC_ADD_TEXT, simSpectraPanel::UpdateGUIValues)
EVT_TEXT(SPEC_DIR_TEXT, simSpectraPanel::DirectionTextModified)
EVT_COMBOBOX(SPEC_CHOOSE, simSpectraPanel::SwitchSpectra)
//EVT_CHECKBOX(OPT_EMIS_CHKS, devMatOpticalsPanel::UpdateCBox)
END_EVENT_TABLE()