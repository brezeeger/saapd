#ifndef GUIWINDOWS_H
#define GUIWINDOWS_H

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/grid.h"
#include "wx/notebook.h"
#include "wx/checkbox.h"
#include "wx/radiobox.h"
#include "wx/stattext.h"
#include "wx/statline.h"
#include "wx/combobox.h"
#include "wx/listctrl.h"
#include "wx/file.h"
#include "wx/textfile.h"
#include "wx/colordlg.h"
#include "wx/colourdata.h"
#include "wx/textdlg.h"
#include "wx/dynarray.h"
#endif

#include "plots.h"
#include "drawing.h"

#define MAX_DISTRIBUTION 5

enum wxIDs{
	RUN_CUR = wxID_HIGHEST+1,
	LOAD_AND_RUN,
	SAVE_FIXED,
	SAVE_COS,
	REMOVE_VOLTAGE,
	RESET_VIEW,
	SWITCH_SELECTION,
	ADD_MAT,
	REM_MAT,
	ADD_CON,
	REM_CON,
	CALC_BUTTON,
	SAAPD_CALC,
	SAAPD_ABOUT,
	PANEL_CHANGES,
	PROP_TEXT,
	PROP_NDIM,
	CHANGE_DIR,
	ADD_BATCH,
	ADD_FOLDER_BATCH,
	REMOVE_BATCH,
	RUN_BATCH,
	LOAD_RESULTS,
	LOAD_SIM,
	SAVE_SIM,
	LOAD_MAT,
	DUPLICATE_MAT,
	RENAME_MAT,
	SAVE_PLOT_IMAGE,
	OPEN_COLOR_DIAG,
	PEN_STYLE,
	SELECT_PEN,
	NEW_VERTEX,
	REM_VERTEX,
	SHAPE_SELECTOR,
	NEW_WAVE,
	REM_WAVE,
	MOD_WAVE,
	OPT_GLOB_N,
	OPT_GLOB_A,
	OPT_GLOB_B,
	OPT_WAVE_X10,
	OPT_WAVE_D10,
	OPT_GEN_FROM_AB,
	OPT_EMIS_EFF,
	OPT_EMIS_CHKS,
	OPT_SAVE,
	OPT_MERGE,
	OPT_CLEAR_WAVES,
	DEF_LOAD,
	DEF_DUPLICATE,
	DEF_CHANGEDIST,
	DEF_UPDATE_TXT,
	DEF_UPDATE_RAD,
	DEF_SMART_REF_CHANGE,
	DEF_RENAME,
	IMP_ADD,
	IMP_REM,
	IMP_RENAME,
	IMP_LOAD,
	IMP_SAVE,
	IMP_DUPLICATE,
	IMP_CHANGEDIST,
	IMP_UPDATE_TXT,
	IMP_UPDATE_RAD,
	IMP_SMART_REF_CHANGE,
	LAY_NEW,
	LAY_REMOVE,
	LAY_RENAME,
	LAY_DUPLICATE,
	LAY_LOAD,
	LAY_SWITCH,
	LAY_SAVE,
	LAY_SCALES,
	LAY_CBOXES,
	LAY_TBOX,
	LAY_SHAPE_TBOX,
	LAY_SHAPE_ALTER_VERTEX,
	LAY_SHAPE_SWITCH_VERTEX,
	LAY_CHANGE_SHAPE,
	LAY_BTN_CHANGE_MAT,
	LAY_CHANGE_MAT,
	LAY_REMOVE_MAT,
	LAY_ADD_MAT,
	LAY_MAT_FUNCTION,
	LAY_VIEW_MAT,
	LAY_BTN_CHANGE_IMP,
	LAY_CHANGE_IMP,
	LAY_REMOVE_IMP,
	LAY_ADD_IMP,
	LAY_IMP_FUNCTION,
	LAY_VIEW_IMP,
	LAY_MISC,
	SIM_SAVE,
	SIM_LOAD,
	SIM_MISC,
	SPEC_NEW,
	SPEC_SAVE,
	SPEC_LOAD,
	SPEC_RENAME,
	SPEC_REMOVE,
	SPEC_ADD_WAVE,
	SPEC_REM_WAVE,
	SPEC_CLEAR_WAVES,
	SPEC_DUPLICATE,
	SPEC_ADD_TEXT,
	SPEC_DIR_TEXT,
	SPEC_RAD,
	SPEC_GRID,
	SPEC_AM0,
	SPEC_AM15,
	SPEC_CHOOSE,
	SS_NEW_GROUP,
	SS_REMOVE_GROUP,
	SS_SAVE_GROUP,
	SS_LOAD_GROUP,
	SS_DUPLICATE_GROUP,
	SS_RENAME_GROUP,
	SS_CHANGE_GROUP,
	SS_ENV_UP,
	SS_ENV_DOWN,
	SS_NEW_ENV,
	SS_REMOVE_ENV,
	SS_CHANGE_ENV_GROUP,
	SS_CHANGE_ENV,
	SS_ADD_SPECTRA,
	SS_REMOVE_SPECTRA,
	SS_ADD_CONTACT,
	SS_REMOVE_CONTACT,
	SS_CBOXES,
	SS_TEXT,
	SS_QUICK_ADD,
	SS_SPECTRA_SELECT,
	SS_CONTACT_SELECT,
	SS_ALG_CHANGE,
	SS_ADD_ALG,
	SS_REPLACE_ALG,
	SS_REMOVE_ALG,
	SS_ALG_UP,
	SS_ALG_DOWN,
	SS_ALG_BOX_CLICK,
	SS_PANELS,
	SS_SUM_GROUP,
	SS_SUM_ENV,
	SS_SUM_VIEW,
	SS_DET_GROUP,
	SS_DET_SWAP,
	TR_SAVE,
	TR_LOAD,
	TR_ADD,
	TR_REMOVE,
	TR_GRID_UNIT,
	TR_PLOT_UNIT,
	TR_ENTRY_UNIT,
	TR_GRID_SEL,
	TR_PLOT_SEL,
	TR_PROP_SEL,
	TR_MOD_SEL,
	TR_INIT_COND,
	TR_SMART_SAVE,
	TR_SIMENDTIME,
	TR_ADDTIMES,
	TR_COS_TEXT,
	TR_COS_SUM,
	TR_LIN_TEXT,
	TR_LIN_SUM,
	TR_RBOX,
	TR_GRID,
	TR_PLOT,
	NEW_TIME,
	REM_TIME,
	VERTEX,
	PAUSE,
	STOP,
	ABORT,
	SAVE_STATE,
	NEXT_STATE,
	UPDATE_SUBWINDOW,
	UPDATED_FILE,
	CLEAR_MESSAGE,
	SIMHASNOTCRASHED,
	REDO_CALCS,
	KERNEL_UPDATE,
	SIM_FINISHED,
	UPDATE_FILE_FILTER,
	FLIST_DCLICK,
	FLIST_SLIDER_CHANGE,
	SELECT_VAR_CHANGE,	//dropdown to select the active variable in the plot changes
	COLOR_BOX,
	REMOVE_VAR_PLOT,
	ADD_VAR_PLOT,
	CHANGE_XY_PLOT,
	UPDATE_P_WIDTH,
	UPDATE_P_STYLE,
	UPDATE_LINE_LEGEND,
	UPDATE_PLOT_BOUNDS,
	TOGGLE_RAW_OUT,
	SAVE_PLOT_FNAME,
	SAVE_PLOT_BTN_NAME,
	SCALE_Y_TXT,
	OFFSET_Y_TXT,
	TOGGLE_ZEROES_BTN,
	NORMALIZE_BTN,
	SHOW_CPLOT_BTN,
	INACTIVE_DDOWN,
	CSTM_ADD_ACTIVE_BTN,
	CSTM_ADD_VISIBLE_BTN,
	CSTM_REMOVE,
	CSTM_SAVE_BTN,
	CSTM_SELECTDDDOWN,
	CSTM_CLEAR,
	TXT_XAXIS,
	TXT_YAXIS,
	TXT_PLOTNAME,
	CHK_LABELS,
	CHK_LEGEND,
	SCRL_TITLE,
	CLR_FILTFILES,
	ATTEMPT_BATCH,
	SAAPD_MAIN_FRAME,
	SIM_PROP_TEXT_CTRLS,
	MAT_ELEC_PROP_CTRLS,
	SIM_PANEL,
	SIM_CALC_PANEL_CHK,
	SIM_CALC_PANEL_TXT,
	SIM_OUT_PANEL_CHK,
	SIM_OUT_PANEL_TXT,
	SIM_OUT_COMMON,
	SIM_OUT_DARK,
	SIM_OUT_LIGHT,
	SIM_OUT_PL,
	SIM_OUT_FULL,
	SIM_OUT_CLEAR,
	DONATE_BTN
};

class ModelDescribe;
class Material;
class devMaterialPanel;
class devMatElectricPanel;
class devMatOpticalsPanel;
class mainFrame;
class devDefectsPanel;


class simSpectrumPanel : public wxPanel
{
public:
	simSpectrumPanel(wxWindow* parent);
	~simSpectrumPanel();
	void SizeRight();
	
	wxTextCtrl* name;

	wxGrid* spectrum;
	wxGrid* onOffTimes;

	wxBoxSizer* nameSizer;
	wxBoxSizer* gridSizer;
	wxBoxSizer* buttonSizer;

	wxStaticBoxSizer* spectrumBox;
	wxStaticBoxSizer* onOffBox;
	wxButton* addWave;
	wxButton* remWave;
	wxButton* addTime;
	wxButton* remTime;
	wxStaticText* nameTxt;
	
private:
	wxBoxSizer* spectrumSizer;
	void NewSpectrum(wxCommandEvent& event);
	void RemoveSpectrum(wxCommandEvent& event);
	void NewTime(wxCommandEvent& event);
	void RemoveTime(wxCommandEvent& event);

	bool isInitialized() { return (remTime != nullptr); }

	DECLARE_EVENT_TABLE()
};

class simOutputsPanel : public wxPanel
{
public:
	simOutputsPanel(wxWindow* parent);
	~simOutputsPanel();
	void SizeRight();
	void outputsToFile(wxFile* oFile);
	
	wxCheckBox* Ef;
	wxCheckBox* Efn;
	wxCheckBox* Efp;
	wxCheckBox* Ec;
	wxCheckBox* Ev;
	wxCheckBox* Psi;
	wxCheckBox* electrons;
	wxCheckBox* holes;
	wxCheckBox* impCharge;
	wxCheckBox* n;
	wxCheckBox* p;
	wxCheckBox* deflight;
	wxCheckBox* condlight;
	wxCheckBox* genlight;
	wxCheckBox* genthermal;
	wxCheckBox* recthermal;
	wxCheckBox* srh;
	wxCheckBox* rho;
	wxCheckBox* Jn;
	wxCheckBox* Jp;
	wxCheckBox* stddev;
	wxCheckBox* stderrBox; //Don't rename this to stderr because bad things will happen.
	wxCheckBox* qe;

	wxStaticText* simulationOutputs;
	wxStaticLine* staticLine;
	wxGridSizer* checkboxSizer;
	
private:
	wxSizer* outputsSizer;
	bool isInitialized() { return (outputsSizer != nullptr); }
};

class simContactsPanel : public wxPanel
{
public:
	simContactsPanel(wxWindow* parent);
	~simContactsPanel();
	void SizeRight();
	wxString NewContact();
	wxString RemoveContact();
	void SwitchContact(wxCommandEvent& event);
	void AddFixed(wxCommandEvent& event);
	void AddCos(wxCommandEvent& event);
	void RemoveVoltage(wxCommandEvent& event);
	void UpdatePlot(wxCommandEvent& event);
	
	wxComboBox* contactChooser;

	ContactPlot* graph;
	mpWindow* plot;
	mpScaleX* xaxis;
	mpScaleY* yaxis;

	wxBoxSizer* optsSizer;
	wxStaticBoxSizer* graphSizer;

	wxButton* conAdd;
	wxButton* conRem;
	wxButton* conSave;
	wxButton* conLoad;
	wxBoxSizer* conBar;
	wxButton* addFixed;
	wxButton* addCosine;
	wxButton* removeVoltage;
	wxButton* resetView;

	
private:
	wxSizer* contactsSizer;
	bool isInitialized() { return (resetView != nullptr); }
	
	DECLARE_EVENT_TABLE()
};

class devPropertiesPanel : public wxPanel
{
public:
	devPropertiesPanel(wxWindow* parent);
	void SizeRight();
	
	
	wxButton *btnDonate;
	wxRadioBox* rbDimensions;
	wxStaticText* note;

	void TransferToGUI();
	void TransferToMDesc(wxCommandEvent& event);
	void OnDonate(wxCommandEvent& event);
	ModelDescribe* getMDesc();
	mainFrame* getMyClassParent();
private:
	wxBoxSizer* propSizer;
	wxBoxSizer *szrHorizTop;
	bool isInitialized() { return (btnDonate != nullptr); }
	DECLARE_EVENT_TABLE()
};

/*
class devLpropertiesPanel : public wxPanel
{
public:
	devLpropertiesPanel(wxWindow* parent);
	~devLpropertiesPanel();
	void SizeRight();
	void SwitchLayer(int oldIndex);
	void AddContact(wxString name);
	void RemoveContact(wxString name);
	void SwitchShape(wxCommandEvent& theevent);
	void SaveLayer(int oldIndex);
	void LoadLayer();
	void EnableLay(bool enable);
	void UpdateLayerVisualization(wxCommandEvent& theevent);

	layerShapesPanel* drawing;
	wxScrolledWindow* controls;

	wxTextCtrl* centerPointSpacing;
	wxTextCtrl* edgePointSpacing;
	wxTextCtrl* theta;
	wxTextCtrl* phi;
	wxCheckBox* affectMesh;
	wxCheckBox* exclusive;
	wxCheckBox* priority;
	wxCheckBox* hidden;
	wxComboBox* shapeSelector;
	wxComboBox* contactSelector;

	wxStaticText* vertexTxt1;
	wxStaticText* vertexTxt2;
	wxStaticText* vertexTxt3;
	wxStaticText* vertexTxt4;
	wxStaticText* vertexTxt5;
	wxStaticText* vertexTxt6;
	wxTextCtrl* vertex1x;
	wxTextCtrl* vertex1y;
	wxTextCtrl* vertex2x;
	wxTextCtrl* vertex2y;
	wxTextCtrl* vertex3x;
	wxTextCtrl* vertex3y;
	wxTextCtrl* vertex4x;
	wxTextCtrl* vertex4y;
	wxTextCtrl* vertex5x;
	wxTextCtrl* vertex5y;
	wxTextCtrl* vertex6x;
	wxTextCtrl* vertex6y;

	wxFlexGridSizer *textCtrlSizer;
	wxFlexGridSizer *comboBoxSizer;
	wxBoxSizer* checkboxSizer;
	wxStaticLine* staticLine;
	wxStaticText* cpSpacing;
	wxStaticText* epSpacing;
	wxStaticText* thetatxt;
	wxStaticText* phitxt;
	wxStaticText* contactTxt;
	wxStaticText* shapeTxt;
	
	

private:
	
	wxSizer* meshSizer;
	wxSizer* vertexSizer;
	wxSizer* controlsSizer;

	unsigned int numVertices;

	DECLARE_EVENT_TABLE()
};

class devLmaterialsPanel : public wxPanel
{
public:
	devLmaterialsPanel(wxWindow* parent);
	~devLmaterialsPanel();
	void SizeRight();
	
	wxListBox* inactive;
	wxListBox* active;
	
	wxRadioBox* distBox;
	wxTextCtrl* xStart;
	wxTextCtrl* yStart;
	wxTextCtrl* zStart;
	wxTextCtrl* xEnd;
	wxTextCtrl* yEnd;
	wxTextCtrl* zEnd;

	wxFlexGridSizer* ptsSizer;
	wxBoxSizer* buttons;
	wxBoxSizer* selector;
	wxBoxSizer* avaliableSizer;
	wxBoxSizer* presentSizer;
	wxStaticText* avaliable;
	wxStaticText* present;
	wxButton* addActive;
	wxButton* remActive;
	wxStaticText* startPt;
	wxStaticText* xStartLabel;
	wxStaticText* yStartLabel;
	wxStaticText* zStartLabel;
	wxStaticText* endPt;
	wxStaticText* xEndLabel;
	wxStaticText* yEndLabel;
	wxStaticText* zEndLabel;
	
	void AddMaterial(wxString name);
	void RemoveMaterial(wxString name);
	void SwitchLayer(int old);

private:
	void AddMatToLayer(wxCommandEvent& event);
	void RemMatFromLayer(wxCommandEvent& event);
	void SwitchMatSelection(wxCommandEvent& event);

	int prevSelection;
	
	wxSizer *matSizer;

	DECLARE_EVENT_TABLE()
};

class devLimpurityPanel : public wxPanel
{
public:
	devLimpurityPanel(wxWindow* parent);
	~devLimpurityPanel();
	void SizeRight();
	
	wxListBox* inactive;
	wxListBox* active;
	
	wxRadioBox* distBox;
	wxTextCtrl* xStart;
	wxTextCtrl* yStart;
	wxTextCtrl* zStart;
	wxTextCtrl* xEnd;
	wxTextCtrl* yEnd;
	wxTextCtrl* zEnd;

	wxFlexGridSizer* ptsSizer;
	wxBoxSizer* buttons;
	wxBoxSizer* selector;
	wxBoxSizer* avaliableSizer;
	wxBoxSizer* presentSizer;
	wxStaticText* avaliable;
	wxStaticText* present;
	wxButton* addActive;
	wxButton* remActive;
	wxStaticText* startPt;
	wxStaticText* xStartLabel;
	wxStaticText* yStartLabel;
	wxStaticText* zStartLabel;
	wxStaticText* endPt;
	wxStaticText* xEndLabel;
	wxStaticText* yEndLabel;
	wxStaticText* zEndLabel;

	void AddImpurity(wxString name);
	void RemoveImpurity(wxString name);
	void SwitchLayer(int old);
	
private:
	void AddImpToLayer(wxCommandEvent& event);
	void RemImpFromLayer(wxCommandEvent& event);
	void SwitchImpSelection(wxCommandEvent& event);
	
	int prevSelection;

	wxSizer *impSizer;
	
	DECLARE_EVENT_TABLE()
};
*/

class executePanel : public wxPanel
{
public:
	executePanel(wxWindow* parent);
	~executePanel();

	void changeDir(wxCommandEvent& event);
	void SizeRight();
	void UpdateMessage(wxCommandEvent& event);
	void ClearMessage(wxCommandEvent& event);
	void MessageRecalcs(wxCommandEvent& event);
	void SimIsRunning(wxCommandEvent& event);
	

	wxFlexGridSizer* resSizer;
	wxBoxSizer* dirSizer;
	wxStaticText* working;
	wxButton* selectDir;
	wxButton* runBatch;
	wxComboBox* simList;
	wxButton* loadSim;
	wxButton* AddFolderBatch;
	wxButton* deleteSim;
	wxTextCtrl* workingDir;
	wxTextCtrl* outputText;
	wxStaticText* message;

	int msg;	//cycles between 0 (nothing, updated file, updated file., updated file.., updated file...)
	int simRunning;

	wxButton* runCurrent;
	wxButton* loadAndRun;
	
private:
	wxSizer* resWrapper;
	bool isInitialized() { return (message != nullptr); }

	DECLARE_EVENT_TABLE()
};


#endif

