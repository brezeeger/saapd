#include "gui.h"
#include <vector>
#include <cfloat>
#include "XML.h"
#include "gui_structs.h"
#include "guidialogs.h"
#include "plots.h"
#include "guiwindows.h"
#include "GUIHelper.h"

#include "SimulationWindow.h"
#include "SimGeneral.h"

#include "../kernel_source/model.h"
#include "../kernel_source/transient.h"
#include "../kernel_source/ss.h"
#include "../kernel_source/load.h"

#include <fstream>


#if (defined __WXMSW__ && defined _DEBUG)
#include <wx/msw/msvcrt.h>      // redefines the new() operator 
#endif

simGeneralPanel::simGeneralPanel(wxWindow* parent)
	: wxPanel(parent, wxID_ANY), stHelp(nullptr)
{
	szrPrimary = new wxBoxSizer(wxVERTICAL);
	szrTopRow = new wxBoxSizer(wxHORIZONTAL);
	szrInputs = new wxFlexGridSizer(4, 5, 5);
	szrInputs->AddGrowableCol(1);
	szrInputs->AddGrowableCol(3);


	txtMaxIterations = new wxTextCtrl(this, SIM_MISC, "0", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_DIGITS));
	txtTimeSaveState = new wxTextCtrl(this, SIM_MISC, "0", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtTemperature = new wxTextCtrl(this, SIM_MISC, "300", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));
	txtDivisions = new wxTextCtrl(this, SIM_MISC, "10", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_DIGITS));
	txtAccuracy = new wxTextCtrl(this, SIM_MISC, "1.0e-6", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC));

	btnSave = new wxButton(this, SIM_SAVE, "Save All Simulation Parameters");
	btnLoad = new wxButton(this, SIM_LOAD, "Load All Simulation Parameters");
	
	wxArrayString choices;
	choices.Add("Neither");
	choices.Add("Thermal Equilibrium");
	choices.Add("Local Thermalization and Charge Neutral");

	rbSeedInitialDeviceStart = new wxRadioBox(this, SIM_MISC, "Rough Starting Condition", wxDefaultPosition, wxDefaultSize, choices, 3, wxRA_SPECIFY_COLS);
	rbSeedInitialDeviceStart->SetToolTip("Before the simulation technically starts, the device must be initialized somehow. Neither leaves the bands empty of electrons and holes with the defects having the carrier to be neutral. Local thermalization involves no lateral charge transfer, but every state will have the same fermi level within a given position. Thermal equilibrium has all the states with a fermi level of roughly 0. This calculation will not be final output accurate, but more like providing an educated guess.");
	rbSeedInitialDeviceStart->Select(1);

	chkTimeSaveState = new wxCheckBox(this, SIM_MISC, "Save State Timer");
	chkTimeSaveState->SetToolTip("(seconds) If the simulation will be running a long time, you can have it automatically save it's state and then resume the simulation later. This is a particularly useful work around if your computer has a tendency to crash...");

	stMaxIterations = new wxStaticText(this, wxID_ANY, "Maximum iterations");
	stMaxIterations->SetToolTip(wxT("If the simulation exceeds this number of iterations, it will just stop. \u22640 is no limit"));
	stTemperature = new wxStaticText(this, wxID_ANY, "Device Temperature");
	stTemperature->SetToolTip("The temperature in Kelvin");

	stDivisions = new wxStaticText(this, wxID_ANY, "Band discretizations");
	stDivisions->SetToolTip("The bands have a density of states function with respect to energy. This is discretized, with a focus on the more interesting lower energy states (~3kT). How many states total should there be? Excessively large numbers will slow down simulation times.");
	stAccuracy = new wxStaticText(this, wxID_ANY, "Accuracy");
	stAccuracy->SetToolTip(wxT("This is used to determine when the simulation finishes. It indicates the relative variability allowed in the calculated relative Fermi level. So if this were 1e-3 and the relative Fermi level were 0.2, the state would be considered stable if the net rate changed signs between relative Fermi levels of 0.2001 and 0.1999 (a delta of 0.0002). Think of 10\u207B\u207F where n is roughly the number of accurate digits in scientific notation."));

	stHelp = new wxStaticText(this, wxID_ANY, wxT("\u25CF If you are here, the device should be completely defined. Making changes to the device after modifying the simulation tabs will result in unpredictable behaviors. For example, loading contact parameters, and then going back to the device->layers tab and unchecking the contact.\n\
\u25CF As before, proceed left to right through the tabs.\n\
\u25CF For typical spectra, create a new spectra and then click the AM 1.5 or AM 0 buttons.\n\
\u25CF For lasers, make sure the units are mW cm\u207B\u00B2 and you normalize the power rating with the spot size.\n\
\u25CF If the system has too many boundary conditions, it gives priority such that voltage > electric fields > currents.\n\
\u25CF In steady state, don't forget to create an algorithm! A combination of Transient, Monte Carlo, and Newton tend to work well.\n\
\u25CF Steady state simulations are organized into groups. Groups are plotted together. Make use of the Quick Add button at the bottom to duplicate the current environment, but continuously vary one particular parameter (such as voltage to get a JV curve)\n\
\u25CF In the transient simulation don't forget to set the end simulation time.\n\
\u25CF When setting the transient initial condition to steady state, it will choose the last viewed environment being defined.\n\
\u25CF Be careful when loading and/or saving the transient and steady state files individually. IDs are internally stored, and they may not necessarily match what you have defined in the device, particularly if you built the device in a different order.\n\
\u25CF The safest Save/Load occurs from the file menu, which combines both the device and simulation parameters, so it is internally consistent.\n\
\u25CF Simulations may take awhile to complete. The goal was not to solve simple devices quickly. The goal is to consistently converge regardless of the device and environment."));

	stHelp->Wrap(GetParent()->GetSize().GetWidth() - 20);
	
	szrTopRow->Add(btnLoad, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);
	szrTopRow->Add(btnSave, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);

	
	

	szrInputs->Add(stTemperature, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 2);
	szrInputs->Add(txtTemperature, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);
	szrInputs->Add(stDivisions, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 2);
	szrInputs->Add(txtDivisions, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10); 
	
	szrInputs->Add(stMaxIterations, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 2);
	szrInputs->Add(txtMaxIterations, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);
	szrInputs->Add(stAccuracy, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 2);
	szrInputs->Add(txtAccuracy, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);

	szrInputs->AddSpacer(0);
	szrInputs->Add(chkTimeSaveState, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 2);
	szrInputs->Add(txtTimeSaveState, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxRIGHT, 10);
	szrInputs->AddSpacer(0);
	
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrPrimary->Add(szrTopRow, 0, wxTOP | wxBOTTOM | wxALIGN_CENTER, 5);
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrPrimary->Add(szrInputs, 0, wxEXPAND | wxBOTTOM | wxTOP, 5);
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrPrimary->Add(rbSeedInitialDeviceStart, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 5);
	szrPrimary->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL), 0, wxEXPAND | wxTOP | wxBOTTOM, 5);
	szrPrimary->Add(stHelp, 1, wxEXPAND | wxALL, 5);


	SetSizer(szrPrimary);
}

simGeneralPanel::~simGeneralPanel()
{ }


simulationWindow* simGeneralPanel::getMyClassParent() { return (simulationWindow*)GetParent(); }

ModelDescribe* simGeneralPanel::getMdesc() { return getMyClassParent()->getMdesc(); }
Simulation* simGeneralPanel::getSim() { return getMyClassParent()->getSim(); }

void simGeneralPanel::UpdateMDescIndiv(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim == nullptr)
		return;
	long int tmp;
	double tmpD;
	if (event.GetEventObject() == txtMaxIterations) {
		if (txtMaxIterations->GetValue().ToLong(&tmp)) {
			if (tmp < 0)
				tmp = 0;
			sim->maxiter = tmp;
		}
	}

	if (event.GetEventObject() == chkTimeSaveState || event.GetEventObject() == txtTimeSaveState) {
		bool b = chkTimeSaveState->GetValue();
		if (b) {
			if (txtTimeSaveState->GetValue().ToDouble(&tmpD)) {
				if (tmpD < 0.0)
					tmpD = 0.0;
				sim->timeSaveState = tmpD;
			}
		}
		else
			sim->timeSaveState = 0.0;
	}

	if (event.GetEventObject() == txtTemperature) {
		if (txtTemperature->GetValue().ToDouble(&tmpD)) {
			if (tmpD < 0.0)
				tmpD = 0.0;
			sim->T = tmpD;
		}
	}

	if (event.GetEventObject() == txtDivisions) {
		if (txtDivisions->GetValue().ToLong(&tmp)) {
			if (tmp < 5)
				tmp = 5;
			sim->statedivisions = tmp;
		}
	}

	if (event.GetEventObject() == txtAccuracy) {
		if (txtAccuracy->GetValue().ToDouble(&tmpD)) {
			if (tmpD < 1.0e-13)
				tmpD = 1.0e-13;
			sim->accuracy = tmpD;
		}
	}

	if (event.GetEventObject() == rbSeedInitialDeviceStart) {
		tmp = rbSeedInitialDeviceStart->GetSelection();
		if (tmp == 0) {
			sim->ThEqDeviceStart = false;
			sim->ThEqPtStart = false;
		}
		else if (tmp == 1) {
			sim->ThEqDeviceStart = true;
			sim->ThEqPtStart = false;
		}
		else {
			sim->ThEqDeviceStart = false;
			sim->ThEqPtStart = true;
		}
	}
}

void simGeneralPanel::TransferToMdesc(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim == nullptr)
		return;
	long int tmp;
	double tmpD;
	if (txtMaxIterations->GetValue().ToLong(&tmp)) {
		if (tmp < 0)
			tmp = 0;
		sim->maxiter = tmp;
	}
	bool b = chkTimeSaveState->GetValue();
	if (b) {
		if (txtTimeSaveState->GetValue().ToDouble(&tmpD)) {
			if (tmpD < 0.0)
				tmpD = 0.0;
			sim->timeSaveState = tmpD;
		}
	}
	else
		sim->timeSaveState = 0.0;

	if (txtTemperature->GetValue().ToDouble(&tmpD)) {
		if (tmpD < 0.0)
			tmpD = 0.0;
		sim->T = tmpD;
	}

	if (txtDivisions->GetValue().ToLong(&tmp)) {
		if (tmp < 5)
			tmp = 5;
		sim->statedivisions = tmp;
	}

	if (txtAccuracy->GetValue().ToDouble(&tmpD)) {
		if (tmpD < 1.0e-13)
			tmpD = 1.0e-13;
		sim->accuracy = tmpD;
	}

	tmp = rbSeedInitialDeviceStart->GetSelection();
	if (tmp == 0) {
		sim->ThEqDeviceStart = false;
		sim->ThEqPtStart = false;
	}
	else if (tmp == 1) {
		sim->ThEqDeviceStart = true;
		sim->ThEqPtStart = false;
	}
	else {
		sim->ThEqDeviceStart = false;
		sim->ThEqPtStart = true;
	}
}

void simGeneralPanel::UpdateGUIValues(wxCommandEvent& event)
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim == nullptr) {
		txtMaxIterations->Disable();
		txtTimeSaveState->Disable();
		txtTemperature->Disable();
		txtDivisions->Disable();
		txtAccuracy->Disable();
		rbSeedInitialDeviceStart->Disable();
		chkTimeSaveState->Disable();
		btnSave->Disable();
		btnLoad->Disable();
		return;
	}
	chkTimeSaveState->Enable();
	if (chkTimeSaveState->GetValue())
		txtTimeSaveState->Enable();
	else {
		ValueToTBox(txtTimeSaveState, 0.0);
		txtTimeSaveState->Disable();
	}
	txtMaxIterations->Enable();

	txtTemperature->Enable();
	txtDivisions->Enable();
	txtAccuracy->Enable();
	rbSeedInitialDeviceStart->Enable();
	btnSave->Enable();
	btnLoad->Enable();
}

void simGeneralPanel::TransferToGUI()
{
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim == nullptr)
		return;

	ValueToTBox(txtMaxIterations, sim->maxiter);
	ValueToTBox(txtTimeSaveState, sim->timeSaveState);
	ValueToTBox(txtTemperature, sim->T);
	ValueToTBox(txtDivisions, sim->statedivisions);
	ValueToTBox(txtAccuracy, sim->accuracy);
	
	if (sim->ThEqDeviceStart) {
		rbSeedInitialDeviceStart->SetSelection(1);
	}
	else if (sim->ThEqPtStart) {
		rbSeedInitialDeviceStart->SetSelection(2);
	}
	else
		rbSeedInitialDeviceStart->SetSelection(0);
	
	if (sim->timeSaveState > 0.0)
		chkTimeSaveState->SetValue(true);

	UpdateGUIValues(wxCommandEvent());
}

void simGeneralPanel::SaveSimulation(wxCommandEvent& event) {
	if (!isInitialized())
		return;
	Simulation *sim = getSim();
	if (sim == nullptr)
		return;

	wxFileDialog saveDialog(this, wxT("Save"), wxEmptyString, wxEmptyString, wxT("Simulation files (*.sim)|*.sim"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveDialog.ShowModal() == wxID_CANCEL)
		return;

	

	XMLFileOut oFile(saveDialog.GetPath().ToStdString());
	if (oFile.isOpen())
	{
		sim->SaveToXML(oFile);
		oFile.closeFile();
	}
}
void simGeneralPanel::LoadSimulation(wxCommandEvent& event) {
	if (!isInitialized())
		return;
	ModelDescribe* mdesc = getMdesc();
	if (mdesc == nullptr)
		return;

	wxFileDialog openDialog(this, wxT("Open"), wxEmptyString, wxEmptyString, wxT("Simulation files (*.sim)|*.sim"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_CANCEL) return;

	std::string fname = openDialog.GetPath().ToStdString();

	std::ifstream file(fname.c_str());
	if (file.is_open() == false)
	{
		wxMessageDialog notXMLDiag(this, wxT("Failed to open file!"), wxT("File not found!"));
		notXMLDiag.ShowModal();
		return;
	}
	//first things first...
	delete mdesc->simulation;	//clear it all out
	mdesc->simulation = new Simulation();
	Simulation *s = mdesc->simulation;
	s->mdesc = mdesc;
	s->TransientData = new TransientSimData(s);
	s->SteadyStateData = new SS_SimData(s);

	LoadSimulationChildren(file, *mdesc);
	file.close();

	int changed = mdesc->ReassociateAll();
	
	if (changed > 0) {
		wxString message = "There are ";
		message << changed << " inconsistencies between the loaded simulation file and the current model described!\nSAAPD attempted to match the data appropriately, but references may have been entirely removed.";
		wxMessageDialog fileissues(this, wxT("File-device inconsistencies!"), message);
		fileissues.ShowModal();
	}


	mdesc->Validate();
	getMyClassParent()->TransferToGUI();	//update ALL the simulation parameters. We just loaded everything!
}

BEGIN_EVENT_TABLE(simGeneralPanel, wxPanel)
EVT_BUTTON(SIM_SAVE, simGeneralPanel::SaveSimulation)
EVT_BUTTON(SIM_LOAD, simGeneralPanel::LoadSimulation)
EVT_TEXT(SIM_MISC, simGeneralPanel::UpdateMDescIndiv)
EVT_CHECKBOX(SIM_MISC, simGeneralPanel::UpdateGUIValues)
EVT_RADIOBOX(SIM_MISC, simGeneralPanel::UpdateMDescIndiv)
END_EVENT_TABLE()