#ifndef SAAPD_FERMIBAND
#define SAAPD_FERMIBAND



#define FERMI_LOAD_CB 1
#define FERMI_LOAD_VB 2
#define FERMI_LOAD_DONOR 4
#define FERMI_LOAD_ACCEPTOR 8
#define FERMI_LOAD_CBTAILS 16
#define FERMI_LOAD_VBTAILS 32

class Point;
class Simulation;

void FindFermiPt(Point& pt, double accuracy);
bool FindQuasiFermiPt(Point& pt, double accuracy, bool which);
unsigned int LoadData(float* degen, double* occupancy, double* total, double* Energies, bool* type, double& EfMax, double& EfMin, Point& pt, int active);
void CalcLeastSquaresGraphData(Point& pt, Simulation& simul);


#endif