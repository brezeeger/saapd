/*
This file will be used predominantly in the future steady state solver, but also in the transient.
The timestep selection and the amount of carriers to transfer is less than ideal for
the simulation to progress rapidly. This will also probably be very useful when doing the whole
look-ahead thing to allow slow-rates to move carriers into multiple energy states by use of
excessive carriers beyond what it receives.

It may also be used to "adjust" rates to be slower for long time steps if it's going to go over
the value. This isn't necessarily wrong, if you view it as a time average, which all the data is 
viewed as anyway.

What's the difference going to be between rates looking like when they all get averaged over time anyway?
|__|__|__ and -------

Although, any additional carriers transferred would have to be added with the timestep to get a proper rate.

*/

#include "FindELevelSS.h"

#include <vector>

#include "RateGeneric.h"
#include "model.h"
#include "contacts.h"

//this will update the maxOcc, minOcc, and targetOcc
int FindELevelSS(ELevel& eLev, Simulation* sim, Model& mdl, double refinement)
{
	
	if(refinement < 1e-14 && refinement > 0.0)	//doubles have limited accuracy of 16 or 17 digits.
		refinement = 1e-14;	//don't try to refine beyond the accuracy of a double!
	else if(refinement >= 1.0)	//it should be <1
		refinement = 0.01;	//default to say within 1% of each other for bad values input
	//if it's negative, we want to figure out how much refinement we want.

	//the optical rates are constant
	double fixedOpticalRate = 0.0;
	for(std::vector<ELevelRate>::iterator opt = eLev.OpticalRates.begin(); opt != eLev.OpticalRates.end(); ++opt)
		fixedOpticalRate += (opt->RateIn - opt->RateOut);

	//contact rates are constant
//	double fixedContactRate = 0.0;
	double netRate = 0.0;	//want to know where to start!
	for(std::vector<ELevelRate>::iterator rt = eLev.Rates.begin(); rt != eLev.Rates.end(); ++rt)
	{
//		if(eLev.ptrPoint)
//		{
//			if(eLev.ptrPoint->contactData)
//			{
//				if(rt->type >= RATE_CONTACT_MIN && rt->type <= RATE_CONTACT_MAX)
//					fixedContactRate += (rt->RateIn - rt->RateOut);
//			}
//		}
		netRate += (rt->RateIn - rt->RateOut);
	}
	netRate += fixedOpticalRate;

	double saveBeginOcc = eLev.GetBeginOccStates();
	if(netRate == 0.0)
	{
		eLev.minOcc = saveBeginOcc;
		eLev.maxOcc = saveBeginOcc;
		eLev.targetOcc = saveBeginOcc;
		return(0);
	}
	
//	double saveEndOcc = eLev.GetEndOccStates();
	double nStates = eLev.GetNumstates();
//	vector<ELevelRate> saveRates;
//	saveRates.swap(eLev.Rates);	//store them here safely for now by just changing some pointers...

	//now we play with the data
	bool RateCalc;
	
	eLev.maxOcc = nStates;
	eLev.minOcc = 0.0;
	if(netRate > 0.0)
	{
		RateCalc = F_E_SS_POSITIVE;
		eLev.targetOcc = saveBeginOcc * 16.0;
		eLev.minOcc = saveBeginOcc;
		eLev.minOccRate = netRate;
	}
	else
	{
		RateCalc = F_E_SS_NEGATIVE;
		eLev.targetOcc = saveBeginOcc * 0.0625;
		eLev.maxOcc = saveBeginOcc;
		eLev.maxOccRate = netRate;
	}
	int keepSearching = F_E_SS_SEARCHMAG;

	if(eLev.targetOcc >= nStates)
		eLev.targetOcc = 0.5 * (saveBeginOcc + nStates);//put it halfway between
	if(eLev.targetOcc <= 0.0)
		eLev.targetOcc = 0.5 * (saveBeginOcc);	// + 0.0

	PosNegSum nRate;
	double targetRate = saveBeginOcc*sim->accuracy*0.001;	//the carrier concentration would change by sim.accuracy % in 1,000 seconds is close enough to say "steady state" for this kind of approximation
	//also allows if found anumber "close" to just get out of the loop and move on quickly.
	//this will be used once the other numerical issues are taken care of
	while(keepSearching != F_E_SS_STOPSEARCH)
	{
		
		netRate = 0.0;

		//recalculate all rates!
		for(std::vector<ELevel*>::iterator trg = eLev.TargetELev.begin(); trg != eLev.TargetELev.end(); ++trg)
		{
			CalcFastRate(&eLev, *trg, sim, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);	//calculate the rate, but tell it to use the test number of carriers in the source
			nRate.AddValue(netRate);
		}

		for(std::vector<TargetE>::iterator trg = eLev.TargetELevDD.begin(); trg != eLev.TargetELevDD.end(); ++trg)
		{
			CalcFastRate(&eLev, trg->target, sim, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);
			nRate.AddValue(netRate);
		}

		netRate = 0.0;	//initialze value
		nRate.AddValue(eLev.FastELevelContactProcess(RATE_OCC_TARGET, RATE_OCC_BEGIN));	//recalculate the contacts as quickly as possible
		//it will do nothing if it is not a contact point


//		nRate.AddValue(fixedContactRate);
		nRate.AddValue(fixedOpticalRate);
		netRate = nRate.GetNetSumClear();	//get the value and then clear the data

		if(fabs(netRate) <= targetRate)	//you win! wowzers!
		{
			eLev.minOcc = eLev.targetOcc;
			eLev.maxOcc = eLev.targetOcc;
			eLev.maxOccRate = netRate;
			eLev.minOccRate = netRate;
//			keepSearching = F_E_SS_STOPSEARCH;
			break;
		}
		else if(keepSearching == F_E_SS_SEARCHMAG)
		{
			if(netRate > 0.0 && RateCalc == F_E_SS_POSITIVE)
			{
				//keep going
				eLev.minOcc = eLev.targetOcc;
				eLev.minOccRate = netRate;
				eLev.targetOcc = eLev.targetOcc * 16.0;
				if(eLev.targetOcc >= nStates)
				{
					eLev.targetOcc = eLev.minOcc;	//make sure minocc doesn't change
					eLev.maxOcc = nStates;
					eLev.maxOccRate = -netRate;	//force the first guess to be an average
					keepSearching = F_E_SS_SEARCHREFINE;
				}
			}
			else if(netRate < 0.0 && RateCalc == F_E_SS_NEGATIVE)
			{
				
				eLev.maxOcc = eLev.targetOcc;
				eLev.maxOccRate = netRate;
				eLev.targetOcc = eLev.targetOcc * 0.0625;
				if(eLev.targetOcc <= 0.0)	//not sure how this happened!
				{
					eLev.minOcc = 0.0;
					eLev.targetOcc = eLev.maxOcc;	//make sure maxOcc doesn't change
					eLev.minOccRate = -netRate;	//just force the first guess to be the average
					keepSearching = F_E_SS_SEARCHREFINE;
				}
			}
			else if(netRate < 0.0 && RateCalc == F_E_SS_POSITIVE)
			{
				
				eLev.maxOcc = eLev.targetOcc;
				eLev.maxOccRate = netRate;
				keepSearching = F_E_SS_SEARCHREFINE;
			}
			else //if(netRate > 0.0 && RateCalc == F_E_SS_NEGATIVE)
			{
				eLev.minOcc = eLev.targetOcc;
				eLev.minOccRate = netRate;
				keepSearching = F_E_SS_SEARCHREFINE;
			}
		}
		if(refinement <= 0.0 && keepSearching == F_E_SS_SEARCHREFINE)
		{
			//compare the minocc/maxocc with the start. If it started far away, we won't need
			//too much refinement - but if it's really close to the start, we should run a tight ship
			double difMin = fabs(saveBeginOcc - eLev.minOcc);
			double difMax = fabs(saveBeginOcc - eLev.maxOcc);
			double dif = difMin<difMax ? difMin : difMax;	//grab the distance to the smaller one
			//this will be a value from 0 to 1, and represents a percentage distance from
			//the number of states to get to a valid range
			//double range = eLev.maxOcc - eLev.minOcc;	//this is the scale we are working with
//			refinement = dif  * range / (nStates * nStates);	//just do one division, really dif/nStates and range/nStates
			refinement = dif / nStates;	//if it's close, then make sure it hones in on the correct
			//answer without going over.
			//if it's far away, let's not waste processing time. A lot can change on the way there.

			if(refinement <= sim->accuracy)	//can't really get more accurate than this
				refinement = sim->accuracy;	//get them accurate to whatever the simulation accuracy is so it will not be an infinite loop


		}
		//not else if on purpose! First time through, it will redefine the values as the same
		if(keepSearching == F_E_SS_SEARCHREFINE)
		{
			if(netRate > 0.0)
			{
				eLev.minOcc = eLev.targetOcc;
				eLev.minOccRate = netRate;
			}
			else
			{
				eLev.maxOcc = eLev.targetOcc;
				eLev.maxOccRate = netRate;
			}

			double difOcc = eLev.maxOcc - eLev.minOcc;	//this BETTER be positive
			//we want to stop when they are within X digits of one another... as based on refinement
			if(difOcc <= eLev.maxOcc * refinement)
			{
				//we are good enough!
				keepSearching = F_E_SS_STOPSEARCH;
				netRate = fabs(netRate);	//compare it against the positive rate (minOcc)
				if(netRate > eLev.minOccRate)
				{
					eLev.targetOcc = eLev.minOcc;
					netRate = eLev.minOccRate;
				}
				if(netRate > -eLev.maxOccRate)	//the current target occ is worse than the maxOcc
				{
					eLev.targetOcc = eLev.maxOcc;

				}
				break;	//just get out o' the loop
			}
			//now guess with the secant method
			double difRates = eLev.maxOccRate - eLev.minOccRate;	//this SHOULD be negative (neg - pos)
			double change =  -eLev.minOccRate * difOcc / difRates;	//should always be positive
			double changeMax = eLev.maxOcc - eLev.minOcc - change;	//this should always be positive
			if(difOcc > eLev.minOcc * 4.0 && fabs(change) < difOcc * 1.0e-5)	//try and speed up convergence
				change = changeMax*0.125;	//the two are of vastly different magnitudes, and one of them is reasonably close to the answer. We want to hone in on the far answer, guess from there
			else if(changeMax < difOcc * 1.0e-5)
			{
				change = change*0.875;	//it's close to max, but we just need to close the gap so the net rates are comparable
//				changeMax = changeMax * 8.0;
//				change = eLev.maxOcc - eLev.minOcc - changeMax;
			}
			//over guess to move the other rate a lot closer
			//new value = start - (rate to zero) * (-change in occ/carrier)
			eLev.targetOcc = eLev.minOcc + change;

			if(eLev.targetOcc == eLev.minOcc || eLev.targetOcc == eLev.maxOcc)	//in case difOcc is large, but change is basically nonexistent compared to min/max
			{
				if(change > changeMax)	//it is closer to maxOcc, so do a weighted average favoring maxOcc
					eLev.targetOcc = 0.75 * eLev.maxOcc + 0.25 * eLev.minOcc;
				else
					eLev.targetOcc = 0.25 * eLev.maxOcc + 0.75 * eLev.minOcc;
			}
			
		}
	}

	//restore the data! -- the data is not corrupted anymore
//	eLev.SetBeginOccStates(saveBeginOcc);
//	eLev.SetEndOccStates(saveEndOcc);
//	saveRates.swap(eLev.Rates);	//put the rates back in!
//	saveRates.clear();
	return(0);
}