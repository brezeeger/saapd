#ifndef RATEGENERIC_H
#define RATEGENERIC_H

#include <vector>
#include <map>

#define RATE_UNDEFINED 0
#define RATE_GENERATION 1
#define RATE_RECOMBINATION 2
#define RATE_SRH1 4
#define RATE_SRH2 8
#define RATE_SRH3 16
#define RATE_SRH4 32
#define RATE_SCATTER 64
#define RATE_LIGHTMIN 128
#define RATE_LIGHTGEN 128
#define RATE_LIGHTSCAT 256
#define RATE_LIGHTDEF 512
#define RATE_LIGHTSRH 1024
#define RATE_STIMEMISREC 2048
#define RATE_STIMEMISDEF 4096
#define RATE_STIMEMISSRH 8192
#define RATE_STIMEMISSCAT 16384
#define RATE_LIGHTMAX 16384
#define RATE_DRIFTDIFFUSE 32768
#define RATE_TUNNEL 65536
#define RATE_CONTACT_MIN 131072
#define RATE_CONTACT_CURRENT_EXT 131072	//current going to an external dimension. Positive rate entry
#define RATE_CONTACT_CURRENT_INT 262144	//current that is solely internal to the device. Positive rate in positive direction - essentially same as DRIFT_DIFFUSE
#define RATE_CONTACT_CURRENT_LINK 524288	//current due to linking between contacts. Positive rate entry
#define RATE_CONTACT_CURRENT_INTEXT 1048576	//current in dimension of device that connects to external portion. Positive rate in positive direction
#define RATE_CONTACT_SOURCE 1	//note, these line up with CONTACT_NONE, CONTACT_SRC, CONTACT_TRG, and CONTACT_BOTH in contacts.h
#define RATE_CONTACT_TARGET 2	//note, these line up with CONTACT_NONE, CONTACT_SRC, CONTACT_TRG, and CONTACT_BOTH in contacts.h
#define RATE_CONTACT_MAX 1048576

#define RATE_NOTADJACENT 0
#define RATE_MX 1
#define RATE_MX2 2
#define RATE_PX 3
#define RATE_PX2 4
#define RATE_MY 5
#define RATE_MY2 6
#define RATE_PY 7
#define RATE_PY2 8

#define BEGINOCCTYPE 0
#define ENDOCCTYPE 1
#define PREVOCCTYPE 2

#define RATE_OCC_BEGIN 0
#define RATE_OCC_END 1
#define RATE_OCC_MAX 2
#define RATE_OCC_MIN 3
#define RATE_OCC_TARGET 4
#define RATE_OCC_ENDADJUST 5
#define RATE_OCC_PREV 6
#define RATE_OCC_THEQ 7

//forward declarations
class ELevel;
class Simulation;
class Point;
class ELevelRate;
class Model;
class ModelDescribe;
class SS_CurrentDerivFactors;
class PosNegSum;

class RateCount
{
public:
	unsigned long int gen;
	unsigned long int rec;
	unsigned long int srh1;
	unsigned long int srh2;
	unsigned long int srh3;
	unsigned long int srh4;
	unsigned long int scat;
	unsigned long int dd;
	unsigned long int tun;
	unsigned long int LTsrh;
	unsigned long int LTgen;
	unsigned long int LTdef;
	unsigned long int LTscat;
	unsigned long int EMrec;
	unsigned long int EMsrh;
	unsigned long int EMdef;
	unsigned long int EMscat;
};

class ChargeTransfer
{
public:
	double simtime;
	double dd;
	double scat;
	double srh1;
	double srh2;
	double srh3;
	double srh4;
	double gen;
	double rec;
	double LTsrh;	//light cause srh
	double LTgen;	//optical gen
	double LTdef;	//light cause def->def
	double LTscat;	//light cause sca
	double EMsrh;	//stimulated emission srh
	double EMrec;	//stimulated emission bands
	double EMdef;	//stimulated emission defects
	double EMscat;	//stimulated emission scattering
	double ddTrans;
	double scatTrans;
	double srh1Trans;
	double srh2Trans;
	double srh3Trans;
	double srh4Trans;
	double genTrans;
	double recTrans;
	double LTsrhTrans;	//light cause srh
	double LTgenTrans;	//optical gen
	double LTdefTrans;	//light cause def->def
	double LTscatTrans;	//light cause sca
	double EMsrhTrans;	//stimulated emission srh
	double EMrecTrans;	//stimulated emission bands
	double EMdefTrans;	//stimulated emission defects
	double EMscatTrans;	//stimulated emission scattering


	void MultiplyTimeStep(double tStep);
	int Init(Simulation* sim);
	int OutputTransfer(bool clear);
};


class ELevelRate
{
public:
	ELevel* source;	//this presumably is going to be irrelevant info, but might be good to ahve
	ELevel* target;
	ELevelRate* CancellingRate;	//what if the rate is at steady state with another - want this to cancel it out to prevent
	//any spikes in carrier transfer
	int type;	//is this SRH, etc.
	bool carrier;	//what kind of carrier is being transferred (eletron or hole)
	bool sourcecarrier;	//is it coming from a state that uses electrons or targets
	bool targetcarrier;	//is it going into a state that uses electrons or targets
	double netrate;			//the net rate for the type of rate this is
	double RateOut;		//this might not actually line up with netrate as In-Out
	double RateIn;	//this might not actually line up with netrate as In-Out
	double RateType;	//what the rate should be as viewed by the type
	double desiredTStep;	//what this rate wants the timestep to be
	double deriv;		//what is the derivative of this rate with respect to the source carrier
	double deriv2;	//what is the second derivative of this rate with respect to the source carrier
	double derivsourceonly;	//derivative of rate based solely on just the carrier moving. Assuming the target does not change.

	bool IncludeRate(int which);	//when summing up the net rates, does this one pass the slow-fast rate test?

	bool DetermineNumCarrierTransfer(double& numCar, double NRate);
	ELevelRate operator +(const ELevelRate &b);
	ELevelRate& operator +=(const ELevelRate &b);

	double GetStateNetRate();

	int RecordRate();
	int RecordDDRate();
	ELevelRate();
	ELevelRate(ELevel* src, ELevel* trg, int typei, bool carrieri, bool sourcecarrieri, bool targetcarrieri, double netratei, double rout, double rin, double rtype, double tstep, double dt, double dt2, double ds);
	~ELevelRate();


};

bool CalcFastRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSourceSrc, int whichSourceTrg, double& rate, bool GenLight=false, PosNegSum* accuRate=NULL);
std::vector<ELevelRate>::iterator CalcRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource=RATE_OCC_BEGIN);

int CalcFastDeriv(ELevel* srcE, ELevel* trgE, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, Simulation* sim, int whichsourceSrc, int whichsourceTrg);

int RecordRate(Point* pt, ELevelRate& rate, ChargeTransfer& chgT, Simulation* sim);	//for normal rates
//int RecordRate(Point* pt, vector<ELevelOpticalRate>::iterator& rate, ChargeTransfer& chgT, Simulation* sim);	//for optical rates

//int RecordLightEmission(Point* pt, Tree<double, ELevelOpticalRate>* rate, Simulation* sim);
int FindAllTargets(ELevel& eLev, Point* pt, Model& mdl);
int FindELevelAdj(ELevel& ESource, Point* ptT);

template <class KEY>
int FindTargets(ELevel& ESource, std::multimap<KEY,ELevel>& EnergyTargetStates);

int FindTargetOvershoot(ELevel& ESource, Point* ptT, bool band);
double CalcFermiSplitConc(ELevel* eLev, bool setRelEF, double& relEf, double absEMax, double absEMin, double absESrcMax, double absESrcMin);
int AdjacentPoint(ELevel* src, ELevel* trg);
double GetOverlapArea(Point* source, Point* target);
int ContactCurrent(Point& pt, ModelDescribe& mdesc, Model& mdl);

#endif