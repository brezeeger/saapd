#include "SaveState.h"

#include <iomanip>

#include "model.h"
#include "transient.h"
#include "ss.h"
#include "light.h"
#include "contacts.h"

/*
This code will save the state of the ENTIRE device. It should be used relatively sparingly, but it will allow testing
of the code to be much quicker. It will allow you to see what is actually happening.

First must save:
Materials
Defects
Impurities
Simulation Data
Contact Data
Model stuff
MDesc Stuff

Then:
Point Data
CB - ELevels
VB - ELevels
Extra - ELevels

*/

int SaveState(Model& mdl, ModelDescribe& mdesc)
{
	std::ofstream file;
	std::stringstream filename;
	filename.str("");
	filename.clear();

	filename << mdesc.simulation->OutputFName << "_State_" << mdesc.simulation->TransientData->iterEndtime << ".state";

	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);	//open new file for writing

	if(file.is_open() == false)
		return(-1);


	file << "SAAPDState 1 " << mdesc.simulation->OutputFName << " " <<mdesc.simulation->curiter << " " << mdesc.simulation->TransientData->simtime << std::endl;
	//this is more for the user to see what it is at the very start of the file
	//the only thing that really mattered was SAAPDState 1
	WriteMDescData(file, mdesc);
	WriteModelData(file, mdl);

	file << "End" << std::endl;

	file.close();
	return(0);
}

int WriteMatlData(std::ofstream& file, Material* mat)
{
	file << "Material" << std::endl;
	file << mat->id << " " << mat->name << std::endl;
	file << mat->describe << std::endl;
	file << " " << std::setw(17) << std::setprecision(0) << std::fixed << mat->type;
	file << " " << std::setw(13) << std::setprecision(5) << std::scientific << mat->epsR;
	file << " " << std::setw(13) << std::setprecision(5) << std::scientific << mat->muR;
	file << " " << std::setw(17) << std::setprecision(10) << std::scientific << mat->ni;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->Nc;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->Nv;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->EFMeDens;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->EFMhDens;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->EFMeCond;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->EFMhCond;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->MuN;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->MuP;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->Eg;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->EcEfi;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->Phi;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->SpHeatCap;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->ThCond;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->tauThGenRec;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachCB.energy;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachCB.captureCB;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachCB.captureVB;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachVB.energy;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachVB.captureCB;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->urbachVB.captureVB;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mat->cfactor.ni;
	mat->direct ? file << " 1" : file << " 0";

	file << std::endl;
	//now load some material specific stuff

	if(mat->defects.size() > 0)
	{
		file << "Defects ";
		for(unsigned int i=0; i<mat->defects.size(); i++)
			file << " " << mat->defects[i]->getID();
		file << std::endl;
	}
	

	WriteOpticalData(file, mat->optics);	//ends with a new line

	file << "End_Material" << std::endl;

	return(0);
}

int WriteOpticalData(std::ofstream& file, Optical* opt)
{
	file << "Optical" << std::endl;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << opt->n;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << opt->a;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << opt->b;
	opt->useAB ? file << " 1" : file << " 0";
	opt->suppressOpticalOutputs ? file << " 1" : file << " 0";
	opt->disableOpticalEmissions ? file << " 1" << std::endl : file << " 0" << std::endl;

	for(std::map<float,NK>::iterator it = opt->WavelengthData.begin(); it != opt->WavelengthData.end(); ++it)
	{
		file << std::setw(10) << std::setprecision(7) << std::scientific << it->first << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << it->second.n << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << it->second.k << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << it->second.alpha << " ";
	}

	file << std::endl;
	return(0);
}

int WriteLayerData(std::ofstream& file, Layer* lay)
{
	file << "Layer" << std::endl;
	file << lay->id << " " << lay->contactid << " " << lay->name << std::endl;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->spacingboundary;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->spacingcenter;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->deviation.x;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->deviation.y;
	file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->deviation.z;
	if(lay->exclusive)
		file << " 1";
	else
		file << " 0";
	if(lay->AffectMesh)
		file << " 1";
	else
		file << " 0";
	if(lay->priority)
		file << " 1";
	else
		file << " 0";
	if(lay->out.AllRates)
		file << " 1";
	else
		file << " 0";
	if(lay->out.OpticalPointEmissions)
		file << " 1";
	else
		file << " 0";

	file << std::endl;
	if(lay->shape != NULL)
	{
		file << "Shape " << lay->shape->type << std::endl;//" " << lay->shape->getNumVertices() << std::endl;

		Vertex* cur = lay->shape->start;
		for(int i=0; i<lay->shape->getNumVertices(); i++)
		{
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << cur->pt.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << cur->pt.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << cur->pt.z;
			cur = cur->next;
			if(cur == NULL)	//just be safe.
				break;
		}
		file << std::endl;
	}
	
	if(lay->impurities.size() > 0)
	{
		for(unsigned int i=0; i<lay->impurities.size(); i++)
		{
			file << "ImpDesc " << lay->impurities[i]->id << " " << lay->impurities[i]->flag;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->multiplier;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.startpos.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.startpos.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.startpos.z;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.stoppos.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.stoppos.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->impurities[i]->range.stoppos.z << std::endl;
		}
	}

	if(lay->matls.size() > 0)
	{
		for(unsigned int i=0; i<lay->matls.size(); i++)
		{
			file << "MatDesc " << lay->matls[i]->id << " " << lay->matls[i]->flag;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->multiplier;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.startpos.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.startpos.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.startpos.z;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.stoppos.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.stoppos.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << lay->matls[i]->range.stoppos.z << std::endl;
		}
	}
	file << "End_Layer" << std::endl;

	return(0);
}

int WriteSpectrumData(std::ofstream& file, std::vector<LightSource*>& spec)
{
	unsigned int sz = spec.size();

	for(unsigned int i=0; i<sz; i++)
	{
		if(spec[i])
		{
			file << "Spectrum " << spec[i]->ID;
			if(spec[i]->enabled)
				file << " 1 ";
			else
				file << " 0 ";

			file << " " << spec[i]->times.size();
			for(std::map<double, bool>::iterator tmv = spec[i]->times.begin(); tmv != spec[i]->times.end(); ++tmv)
			{
				if(tmv->second)
					file << " 1 ";
				else
					file << " 0 ";
				file << std::setw(17) << std::setprecision(14) << std::scientific << tmv->first;
			}

			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << spec[i]->direction.x;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << spec[i]->direction.y;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << spec[i]->direction.z;

			if(spec[i]->sourceShape == NULL)
				file << " 0";
			else
			{
				file << " " << spec[i]->sourceShape->getNumVertices();
				file << " " << spec[i]->sourceShape->type;
				Vertex* ptrV = spec[i]->sourceShape->start;
				for(int j=0; j<spec[i]->sourceShape->getNumVertices(); j++)
				{
					file << " " << std::setw(17) << std::setprecision(14) << std::scientific << ptrV->pt.x;
					file << " " << std::setw(17) << std::setprecision(14) << std::scientific << ptrV->pt.y;
					file << " " << std::setw(17) << std::setprecision(14) << std::scientific << ptrV->pt.z;
					ptrV = ptrV->next;
				}
			}

			file << " " << spec[i]->IncidentPoints.size();

			for(unsigned int j=0; j<spec[i]->IncidentPoints.size(); j++)
			{
				file << " " << spec[i]->IncidentPoints[j]->adj.self;
			}

			file << " " << spec[i]->light.size();

			for(std::map<double,Wavelength>::iterator it = spec[i]->light.begin(); it != spec[i]->light.end(); ++it)
			{
				file << " " <<  std::setw(10) << std::setprecision(7) << std::scientific << it->second.lambda;
				file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.intensity;
//LIGHT_OLD_VARIABLES
//				file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.collimation;
//				file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.coherence1;
//				file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.coherence2;
				file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.energyphoton;
				
			}

			file << " " << spec[i]->name << std::endl;	//end first line - second line is wavelength data
		}
	}

	return(0);
}

int Impurity::WriteStateFile(std::ofstream& file)
{
	file << "Impurity " << ID << " " << charge << " " << distribution << " ";
	if(depth)
		file << "1 ";
	else
		file << "0 ";
	if(reference)
		file << "1 ";
	else
		file << "0 ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << energy << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << energyvar << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << density << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << sigman << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << sigmap << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << degeneracy << " ";
	file << name << std::endl;
	
	return(0);
}

int WriteTransientData(std::ofstream& file, TransientSimData* trans)
{
	if(trans == NULL)
		return(0);

	file << "Transient ";

	
	if(trans->SSByTransient)
		file << "1 ";
	else
		file << "0 ";

	if(trans->SmartSaveState)
		file << "1 ";
	else
		file << "0 ";

	file << std::fixed << trans->IntermediateSteps << " ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->SSbyTTime << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->percentCarrierTransfer << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->timeignore << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->simtime << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->timestep << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->nexttimestep << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->iterEndtime << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->simendtime << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->storevalueStartTime << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->mints << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->avgts << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->maxts << " ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->prevMNRate << " ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.deltaTM1Inv << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.deltaTM2Inv << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.prev2timestep << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.prevtimestep << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.TM1PlusTM2 << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << trans->tstepConsts.TM1PlusTM2Inv << " ";
	
	file << trans->TargetTimes.size() << " ";
	for (std::map<double, TargetTimeData>::iterator it = trans->TargetTimes.begin(); it != trans->TargetTimes.end(); ++it)
	{
		if(it->second.SaveState)
			file << "1 ";
		else
			file << "0 ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << it->first << " ";
	}
	file << std::endl;

	file << trans->ContactData.size() << std::endl;
	for (unsigned int i = 0; i < trans->ContactData.size(); ++i)
		trans->ContactData[i]->SavePrivateData(file);

	return(0);
}

int WriteSteadyStateData(std::ofstream& file, SS_SimData* ss)
{
	if(ss == NULL)
		return(0);

	file << "SS " << ss->CurrentData;
	file << std::setw(17) << std::setprecision(14) << std::scientific << ss->steadyStateClampReduction;
	file << " " << ss->ExternalStates.size() << std::endl;

	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		file << ss->ExternalStates[i]->id << " " << ss->ExternalStates[i]->flags << " "
			<< ss->ExternalStates[i]->reusePrevContactEnvironment << " "
			<< ss->ExternalStates[i]->reusePrevLightEnvironment << " "
			<< ss->ExternalStates[i]->ContactEnvironment.size() << " "
			<< ss->ExternalStates[i]->ActiveLightEnvironment.size() << " "
			<< ss->ExternalStates[i]->FileOut << std::endl;
		for(unsigned int ctc = 0; ctc<ss->ExternalStates[i]->ContactEnvironment.size(); ++ctc)
		{
			file << ss->ExternalStates[i]->ContactEnvironment[ctc]->ctcID << " ";
			file << ss->ExternalStates[i]->ContactEnvironment[ctc]->currentActiveValue << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->voltage << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->current << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->currentDensity << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->eField << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->current_ED << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->eField_ED << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->currentDensity_ED << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->eField_ED_Leak << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->current_ED_Leak << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << ss->ExternalStates[i]->ContactEnvironment[ctc]->currentD_ED_Leak << std::endl;
		}
		file << "light";
		for(unsigned int l=0; l<ss->ExternalStates[i]->ActiveLightEnvironment.size(); ++l)
			file << " " << ss->ExternalStates[i]->ActiveLightEnvironment[l];
		file << std::endl;
	}
//unneeded	file << "End_SS" << std::endl;

	return(0);
}

int WriteSimulationData(std::ofstream& file, Simulation* sim)
{
	file << "Simulation " << sim->OutputFName << std::endl;
	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT) //make it start on a clean number if simulation resumed
	{
		unsigned long int iterWrite = (sim->outputs.numinbin - (sim->curiter % sim->outputs.numinbin)) + sim->curiter + 1;	//when restarting, make the filenames stay nice and even... iterations start at 1, not 0.
		file << iterWrite << " ";
	}
	else
		file << sim->curiter << " ";
	file << sim->maxiter << " " << 
		sim->statedivisions << " " << 
		sim->outputs.numinbin << " " <<
		sim->lightincidentdir << std::endl;
	file << std::setw(9) << std::setprecision(0) << std::fixed << sim->timeSaveState << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << sim->T << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << sim->Vol << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << sim->accuracy << " ";
	
	if(sim->SignificantMilestone)
		file << "1 ";
	else
		file << "0 ";
	if(sim->transient)
		file << "1 ";
	else
		file << "0 ";
	
	if(sim->outputs.fermi)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.fermin)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.fermip)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Ec)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Ev)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Psi)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.CarrierCB)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.CarrierVB)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.n)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.p)
		file << "1 ";
	else
		file << "0 ";
	
	if(sim->outputs.genth)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.SRH)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.recth)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.rho)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Jp)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Jn)
		file << "1 ";
	else
		file << "0 ";

	if(sim->outputs.Lpow)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Lgen)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Lsrh)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Ldef)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Lscat)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Erec)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Esrh)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Edef)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.Escat)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.LightEmission)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.stddev)
		file << "1 ";
	else
		file << "0 ";
	if(sim->outputs.error)
		file << "1 ";
	else
		file << "0 ";

	if(sim->outputs.chgError)
		file << "1 ";
	else
		file << "0 ";

	//now put the enabled rates in

	if(sim->EnabledRates.SRH1)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SRH2)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SRH3)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SRH4)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.ThermalGen)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.ThermalRec)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Scatter)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Current)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Tunnel)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Lpow)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Lgen)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Lsrh)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Ldef)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.Lscat)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SErec)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SEsrh)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SEdef)
		file << "1 ";
	else
		file << "0 ";
	if(sim->EnabledRates.SEscat)
		file << "1 ";
	else
		file << "0 ";

	if(sim->EnabledRates.LightEmissions)
		file << "1 ";
	else
		file << "0 ";

	file << sim->EnabledRates.Thermalize << " ";
	
	if (sim->outputs.OutputAllIndivRates)
		file << "1 ";
	else
		file << "0 ";

	

	file << sim->EnabledRates.OpticalEMin << " ";
	file << sim->EnabledRates.OpticalEMax << " ";
	file << sim->EnabledRates.OpticalEResolution << " ";
	file << sim->EnabledRates.OpticalIntensityCutoff << std::endl;
	
	for(unsigned int i=0; i<sim->contacts.size(); i++)
		WriteContactData(file, sim->contacts[i]);

	WriteTransientData(file, sim->TransientData);
	WriteSteadyStateData(file, sim->SteadyStateData);
	
	file << "End_Simulation" << std::endl;

	return(0);
}

int WriteContactData(std::ofstream& file, Contact* ct)
{
	file << "Contact ";
	
	if(ct->isolated)
		file << "1 ";
	else
		file << "0 ";

	file << ct->id << " " << ct->ContactEnvironmentIndex << " " << ct->name << std::endl;

	ct->SavePrivateData(file);
	
	file << "End_Contact" << std::endl;
	return(0);
}

int TransContact::PrivateCosSave(std::ofstream& file, std::multimap<double, CosTimeDependence>& data)
{
	
	for(std::multimap<double,CosTimeDependence>::iterator node = data.begin(); node != data.end(); ++node)
	{
		file << " ";	//you can't terminate a line with a space, or it might confuse the load state and think there's more stuff to load.
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetStartTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetEndTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetAmp() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetFreq() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetDelta() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetDC() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetLeak() << " ";
		file << std::fixed << node->second.GetFlags() << " ";
		if(node->second.IsSum())
			file << "1";
		else
			file << "0";
	}
	
	return(0);
}

int TransContact::PrivateLinSave(std::ofstream& file, std::multimap<double, LinearTimeDependence>& data)
{
	for (std::multimap<double, LinearTimeDependence>::iterator node = data.begin(); node != data.end(); ++node)
	{
		file << " ";	//you can't terminate a line with a space, or it might confuse the load state and think there's more stuff to load.
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetStartTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetEndTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetInitVal() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetSlope() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetLeak() << " ";
		file << std::fixed << node->second.GetFlags() << " ";
		if (node->second.IsSum())
			file << "1";
		else
			file << "0";
	}

	return(0);
}


int TransContact::PrivateTimeDepSave(std::ofstream& file, std::map<double, TimeDependence>& data)
{
	for (std::multimap<double, TimeDependence>::iterator node = data.begin(); node != data.end(); ++node)
	{
		file << " ";	//you can't terminate a line with a space, or it might confuse the load state and think there's more stuff to load.
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetStartTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetEndTime() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << node->second.GetLeak() << " ";
		file << std::fixed << node->second.GetFlags() << " ";
		if (node->second.IsSum())
			file << "1";
		else
			file << "0";
	}

	return(0);
}

int TransContact::SavePrivateData(std::ofstream& file)
{
	file << "CID " << ctcID;
	if (VoltageCos.size() > 0)
	{
		file << "VCos";
		PrivateCosSave(file, VoltageCos);
		file << std::endl;
	}

	if (VoltageLin.size() > 0)
	{
		file << "VLin";
		PrivateLinSave(file, VoltageLin);
		file << std::endl;
	}

	//save the electric fields
	if (EFieldCos.size() > 0)
	{
		file << "ECos";
		PrivateCosSave(file, EFieldCos);
		file << std::endl;
	}

	if (EFieldLin.size() > 0)
	{
		file << "ELin";
		PrivateLinSave(file, EFieldLin);
		file << std::endl;
	}

	//save the current density
	if (CurrentDensityCos.size() > 0)
	{
		file << "CDCos";
		PrivateCosSave(file, CurrentDensityCos);
		file << std::endl;
	}

	if (CurrentDensityLin.size() > 0)
	{
		file << "CDLin";
		PrivateLinSave(file, CurrentDensityLin);
		file << std::endl;
	}

	//save the currents
	if (CurrentCos.size() > 0)
	{
		file << "CCos";
		PrivateCosSave(file, CurrentCos);
		file << std::endl;
	}

	if (CurrentLin.size() > 0)
	{
		file << "CLin";
		PrivateLinSave(file, CurrentLin);
		file << std::endl;
	}

	if (AdditionalFlags.size() > 0)
	{
		file << "flgs";
		PrivateTimeDepSave(file, AdditionalFlags);
		file << std::endl;
	}
	file << "End_Trans_Ctc" << std::endl;
	return 0;
}

int Contact::SavePrivateData(std::ofstream& file)
{
	//Save the voltages
	

	if(CLinks.size() > 0)
	{
		file << "lnks";
		for(std::map<int,ContactLink>::iterator it = CLinks.begin(); it != CLinks.end(); ++it)
		{
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.current;
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << it->second.resistance;
			file << " " << std::fixed << it->second.targetContact->id;
		}
		file << std::endl;
	}


	return(0);
}

int WriteMDescData(std::ofstream& file, ModelDescribe& mdesc)
{
	file << "ModelDescription" << std::endl;
	file << mdesc.NDim << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.Ldimensions.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.Ldimensions.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.Ldimensions.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.resolution.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.resolution.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdesc.resolution.z << std::endl;

	if(mdesc.simulation != NULL)
		WriteSimulationData(file, mdesc.simulation);	//ends with an std::endl

	for(unsigned int i=0; i<mdesc.impurities.size(); i++)
		mdesc.impurities[i]->WriteStateFile(file);	//ends with std::endl

	for(unsigned int i=0; i<mdesc.layers.size(); i++)
		WriteLayerData(file, mdesc.layers[i]);	//ends with an std::endl

	for(unsigned int i=0; i<mdesc.materials.size(); i++)
		WriteMatlData(file, mdesc.materials[i]);	//ends with std::endl

	WriteSpectrumData(file, mdesc.spectrum);	//ends with std::endl
	
	file << "End_ModelDescribe" << std::endl;

	return(0);
}

int WritePointData(std::ofstream& file, Point& pt)
{
	file << "Point " << pt.adj.self << " ";

	if(pt.adj.adjN)
		file << pt.adj.adjN->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjP)
		file << pt.adj.adjP->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjMX)
		file << pt.adj.adjMX->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjMX2)
		file << pt.adj.adjMX2->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjPX)
		file << pt.adj.adjPX->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjPX2)
		file << pt.adj.adjPX2->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjMY)
		file << pt.adj.adjMY->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjMY2)
		file << pt.adj.adjMY2->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjPY)
		file << pt.adj.adjPY->adj.self << " ";
	else
		file << "-1 ";

	if(pt.adj.adjPY2)
		file << pt.adj.adjPY2->adj.self << " ";
	else
		file << "-1" << " ";

	if(pt.Output.DetailedOptical)
		file << " 1" << std::endl;
	else
		file << " 0" << std::endl;

	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.position.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.position.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.position.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.pxpy.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.pxpy.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.pxpy.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mxmy.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mxmy.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mxmy.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.sizeScaling << std::endl;
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.Psi << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.eg << " ";
	file << std::setw(10) << std::setprecision(7) << std::scientific << pt.eps << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.niCarrier << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.n << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.p << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.rho << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.fermi << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.fermin << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.fermip << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.temp << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.heat << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mintime.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mintime.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.mintime.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.scatterCBConst << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << pt.scatterVBConst << std::endl;

	if(pt.MatProps != NULL)
		file << "PtMatl " << pt.MatProps->id << std::endl;

	if(pt.impurity.size() > 0)
	{
		file << "PtImp ";
		for(unsigned int i=0; i<pt.impurity.size(); i++)
			file << pt.impurity[i]->getID() << " ";
		file << std::endl;
	}
	if(pt.contactData != NULL)
		file << "PtCt " << pt.contactData->id << std::endl;

	file << "PtPoisDep " << pt.pDsize;
	for(unsigned int i=0; i<pt.pDsize; i++)
		file << " " << pt.poissonDependency[i].pt << " " << std::setw(17) << std::setprecision(14) << std::scientific << pt.poissonDependency[i].data;
	file << std::endl;

	file << "PtPsiCf " << pt.PsiCf.size() << " ";
	for(unsigned int i=0; i<pt.PsiCf.size(); i++)
		file << pt.PsiCf[i].pt->adj.self << " " << std::setw(17) << std::setprecision(14) << std::scientific << pt.PsiCf[i].data << " ";

	file << std::endl;

	if(pt.AngleData.size() > 0)
	{
		file << "PtAngle ";
		for(unsigned int i=0; i<pt.AngleData.size(); i++)
		{
			file << pt.AngleData[i].targetPt->adj.self << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].theta[0] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].theta[1] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].theta[2] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].theta[3] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].sin01 << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].cos01 << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].phi[0] << " ";
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].phi[1] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].phi[2] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].phi[3] << " ";	
			file << std::setw(17) << std::setprecision(14) << std::scientific << pt.AngleData[i].proportion << " ";	
		}
		file << std::endl;
	}
	WriteBand(file, pt.cb);
	WriteBand(file, pt.vb);
	WriteImpStates(file, pt.donorlike);	//this MUST be after CB/VB for calculating SRH values
	WriteImpStates(file, pt.acceptorlike);
	WriteImpStates(file, pt.CBTails);	//this will also work with band tails! i imp==null, then it sets a flag to -1 to say tail
	WriteImpStates(file, pt.VBTails);
	
	file << "End_Point" << std::endl;
	return(0);
}

int WriteBand(std::ofstream& file, Band& band)
{
	if(band.type == CONDUCTION)
		file << "CB ";
	else
		file << "VB ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << band.bandmin << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << band.partitioni << " ";
	file << band.GetNumParticles() << " " << band.numparticlesold << " " << 
		std::setw(17) << std::setprecision(14) << std::scientific << band.totalstates << " " <<
		std::setw(17) << std::setprecision(14) << std::scientific << band.EffectiveDensity << std::endl;

	for(std::multimap<MaxMin<double>,ELevel>::iterator it = band.energies.begin(); it!=band.energies.end(); ++it)
		WriteEState(file, it->second);

	file << "End_band" << std::endl;
	return(0);
}

int WriteEState(std::ofstream& file, ELevel& state)
{
	file << "EState ";
	if(state.carriertype == ELECTRON)
		file << "1 ";
	else
		file << "0 ";

	if(state.includeFermiAbove)
		file << "1 ";
	else
		file << "0 ";

	if(state.includeFermiBelow)
		file << "1 ";
	else
		file << "0 ";

	file << state.uniqueID;
		
	file << std::endl;

	file << std::setw(17) << std::setprecision(14) << std::scientific << state.min << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.max << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.eUse << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.GetNumstates() << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.GetEndOccStates() << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.targetTransfer << " ";
	file << state.charge << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.boltzeUse << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.boltzmax << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.efm << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.tau << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.velocity << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.fermi << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.FermiProb << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.AbsFermiEnergy << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.scatter.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.scatter.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.scatter.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.time.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.time.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.time.z << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << state.percentTransfer << " ";
//	state.tStepCtrl->WriteDataToFile(file);
	file << std::endl;
	

	return(0);
}

int WriteImpStates(std::ofstream& file, std::multimap<MaxMin<double>,ELevel>& states)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator st = states.begin(); st != states.end(); ++st)
	{
		file << "EImpState ";
		if(st->second.carriertype == ELECTRON)
			file << "1 ";
		else
			file << "0 ";
		if(st->second.includeFermiAbove)
			file << "1 ";
		else
			file << "0 ";
		if(st->second.includeFermiBelow)
			file << "1 ";
		else
			file << "0 ";
		file << st->second.uniqueID << " ";
		if(st->second.imp != NULL)
			file << st->second.imp->getID() << " ";
		else
			file << "-1 ";	//then this must be a band tail

		if(st->second.carriertype == CONDUCTION)	//maintain backwards compatibility
			file << "1";
		else
			file << "0";
		file << std::endl;
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.min << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.max << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.eUse << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.GetNumstates() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.GetEndOccStates() << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.targetTransfer << " ";
		file << st->second.charge << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.boltzeUse << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.boltzmax << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.efm << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.tau << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.velocity << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.fermi << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.FermiProb << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.AbsFermiEnergy << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.scatter.x << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.scatter.y << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.scatter.z << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.time.x << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.time.y << " ";
		file << std::setw(17) << std::setprecision(14) << std::scientific << st->second.time.z << " ";
//		st->second.tStepCtrl->WriteDataToFile(file);

		file << std::endl;
	}
	return(0);
}

int WriteModelData(std::ofstream& file, Model& mdl)
{
	file << "Model " << mdl.numpoints << " " << mdl.NDim << " ";
	
	if(mdl.disableTunneling)
		file << "1 ";
	else
		file << "0 ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << mdl.width.x << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdl.width.y << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << mdl.width.z << std::endl;
	
	if(mdl.PoissonDependencyCalculated)
	{
		file << "1";
		for(unsigned long int i=0; i<mdl.numpoints; i++)
			file << " " << std::setw(17) << std::setprecision(14) << std::scientific << mdl.poissonRho[i];
		//no need for poissonBC. It is retrieved and calculated if not found.
		file << std::endl;
	}
	else
		file << "0" << std::endl;

	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
		WritePointData(file, pt->second);
	

	file << "End_Model" << std::endl;
	return(0);
}

int TStepELevel::WriteDataToFile(std::ofstream& file)
{
	file << std::setw(17) << std::setprecision(14) << std::scientific << occ0  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << occ1  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << occ2  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << occ3  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << r0  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << r1  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << r2  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << r3  << " ";
	if(istype0)
		file << "1 ";
	else
		file << "0 ";

	if(istype1)
		file << "1 ";
	else
		file << "0 ";

	if(istype2)
		file << "1 ";
	else
		file << "0 ";

	if(istype3)
		file << "1 ";
	else
		file << "0 ";

	file << std::setw(17) << std::setprecision(14) << std::scientific << minOcc  << " ";
	file << std::setw(17) << std::setprecision(14) << std::scientific << maxOcc  << " ";

	if(minMatch)
		file << "1 ";
	else
		file << "0 ";

	if(maxMatch)
		file << "1";
	else
		file << "0";

	return(0);
}
