#include "load.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>


#include "physics.h"
#include "model.h"
#include "light.h"
#include "contacts.h"
#include "transient.h"
#include "ss.h"

#include "outputdebug.h"

/*
Additional support needed for:
Band Tails


This file loads all the data needed to generate pretty much everything...
With the exception of spectrum files, which alone are not XML files, simply call
load, and the top level checks for each subbranch. Therefore, it will detect individual
layers, materials, impurities, simulation data, etc. and load it into the file.
Otherwise, it will all fall under loading of the device, which has a specific hierarchy.
How the tags are defined to affect the loading procedure are defined by the functions, and each possible
parent tag has a function devoted to it.  In this manner, the loader will ignore any extra data from other
applications for a given tag.

NOTE! File order:
x1) impurities
x2) materials
x3) layers (depends on materials and impurities)
simulation, spectrum, resolution, dimensions, etc. order does not matter
*/

int Load(std::string filename, ModelDescribe& mdl, Model& model, bool append)	//loads any of generic files and calls the appropriate functions for loading
{
	if(filename.find(".state") != std::string::npos)
	{	//want to load a device state.
		return(1);
	}


	std::ifstream file;
	std::stringstream myStream;
	file.open(filename.c_str(), std::fstream::in);
	if(file.is_open() == false)
		return(-1);

	if(!append)
	{
		mdl.ClearAll();
		model.gridp.clear();
	}

	size_t posL;
	size_t posR;
	size_t findpos = 0;

	std::string line="";
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;

	while(file.good())	//main read function
	{
		getline(file,line);	//clear it out for the next load
		posL = 0;
		posR = 0;
		ToLower(line);
		FindTag(line, tag, 0);
		if(tag == "")
			foundTag = true;	//there was no tag to output an error
		

			//first identify what tag this is
		
		posL = tag.find("device");	//the way the code is setup, for valid XML files, it should never
		if(posL != std::string::npos)		//find /material first.
		{
			foundTag = true;
			posL = tag.find("layers=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 8;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> size;	//put the number of layers in size
				mdl.layers.reserve(size);	//reserve enough layers in the model

			}
			//now load particulars of the material subgroup
			LoadDeviceChildren(file, mdl);	//mater should still be pointing to the new material...

		}	//end device load

		findpos = tag.find("layer");
		if(findpos == 0 && tag.size() < 8)	//prevent <device layers> from being found
		{
			foundTag = true;
			Layer* lyr = new Layer;
			mdl.layers.push_back(lyr);

			posL = tag.find("name=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				lyr->name = attribute;	//load the layer name into name
			}

			//now load particulars of the material subgroup
			LoadLayerChildren(file, lyr, mdl);	//mater should still be pointing to the new material...
		}	//end layer load

		posL = tag.find("material");	//the way the code is setup, for valid XML files, it should never
		if(posL != std::string::npos && tag.size()<9)		//find /material first.
		{
			foundTag = true;
			Material* mater = new Material;
			mdl.materials.push_back(mater);

			posL = tag.find("name=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				mater->name = attribute;	//load the material name into name
		
			}
			posL = tag.find("thickness=\"");	//form a new layer with this material
			if(posL != std::string::npos)	//found a thickness tag
			{
				posL += 11;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");	//clear out the stream
				myStream << attribute;	//load the info into the stream

				Layer* lyr = new Layer;
				mdl.layers.push_back(lyr);	//keep a pointer to the layer
				double thickness;
				myStream >> thickness;	//pass the distance into the layer
				thickness = thickness*(1e-4);	//thickness is in microns, but our set is in cm
				lyr->shape->type = SHPLINE;	//this layer is 1D
				int region = mdl.layers.size()-2;
				if(region >= 0)
				{
					XSpace pt;
					pt.y = 0;
					pt.x = mdl.layers[region]->shape->max.x;
					lyr->shape->AddPoint(pt);
					pt.x += thickness;
					lyr->shape->AddPoint(pt);
				}
				else
				{
					XSpace pt;
					pt.x = 0;
					pt.y = 0;
					lyr->shape->AddPoint(pt);
					pt.x = thickness;
					lyr->shape->AddPoint(pt);
				}
				
				ldescriptor<Material*>* LMat = new ldescriptor<Material*>;
				lyr->matls.push_back(LMat);	//add the descriptor to the new layer
				LMat->data = mater;		//make the new descriptor point to the new material
				LMat->range.startpos.x = 0.0;	//starting point for the material in the layer
				LMat->range.startpos.y = 0.0;
				LMat->range.stoppos.x = thickness;	//assume the layer goes in x direction
				LMat->range.stoppos.y = 0.0;
				LMat->id = -1;	//it has already been assigned a material, so no need to assign it something
				LMat->flag = DISCRETE;
			}
			//now load particulars of the material subgroup
			LoadMaterialChildren(file, mater, mdl);	//mater should still be pointing to the new material...

		}	//end material load

		findpos = tag.find("simulation");
		if(findpos == 0)
		{
			foundTag = true;
			LoadSimulationChildren(file, mdl);
		}
		
		findpos = tag.find("impurity");
		if(findpos == 0)
		{
			foundTag = true;
			Impurity* impur = new Impurity;
			mdl.impurities.push_back(impur);
			impur->LoadFromFile(file);
		}

		findpos = tag.find("spectrum");
		if(findpos == 0)
		{
			foundTag = true;
			LightSource* spec = new LightSource;
			mdl.spectrum.push_back(spec);
			posL = tag.find("size=\"");
			if(posL != std::string::npos)	//found a size tag
			{
				posL += 6;	//point to first part of unit
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");	//clear out the stream
				myStream << attribute;	//load the info into the stream
				myStream >> size;	//pass the distance into the layer

			}

			LoadSpectrumChildren(file, mdl, spec);
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - base: "+tag);

	}
	file.close();

	//now assign duplicated variables
	model.NDim = mdl.NDim;
	mdl.simulation->mdlData = &model;

	//now create all the necessary data...
	if(mdl.simulation->outputs.LightEmission
		|| mdl.simulation->outputs.QE
		|| mdl.simulation->outputs.Edef
		|| mdl.simulation->outputs.Erec
		|| mdl.simulation->outputs.Escat
		|| mdl.simulation->outputs.Esrh
		|| mdl.simulation->outputs.Ldef
		|| mdl.simulation->outputs.Lgen
		|| mdl.simulation->outputs.Lscat
		|| mdl.simulation->outputs.Lsrh)
	{
		mdl.simulation->OpticalFluxOut = new BulkOpticalData;
		mdl.simulation->OpticalAggregateOut = new BulkOpticalData;
		for(unsigned int i=0; i<mdl.materials.size(); ++i)
		{
			mdl.materials[i]->OpticalFluxOut = new BulkOpticalData;
			mdl.materials[i]->OpticalAggregateOut = new BulkOpticalData;
		}
	}

	//make sure it if the spectra is loaded later, it gets the times
	TransientSimData *tsd = mdl.simulation->TransientData;
	if(tsd)
	{
		for(unsigned int i=0; i<tsd->SpecTime.size(); ++i)
		{
			for(unsigned int spec=0; spec<mdl.spectrum.size(); ++spec)
			{
				if(mdl.spectrum[spec]->ID == tsd->SpecTime[i].SpecID)
				{
					mdl.spectrum[spec]->times.insert(std::pair<double, bool>(tsd->SpecTime[i].time, tsd->SpecTime[i].enable));
					break;
				}
			}
		}
		mdl.simulation->simType = SIMTYPE_TRANSIENT;

		TargetTimeData ttd;
		ttd.SaveState = tsd->SmartSaveState;
		ttd.SSTarget = -1;
		tsd->TargetTimes.insert(std::pair<double, TargetTimeData>(tsd->simendtime, ttd));
		//insert a target time at the very end of the simulation, and save the state there if it is a smartsavestate checkbox.
		tsd->curTargettime = tsd->TargetTimes.begin()->first;	//get the first targettime

	}
	if(mdl.simulation->SteadyStateData)	//it can be both. do steady state first
		mdl.simulation->simType = SIMTYPE_SS;

	return(0);

}

void LoadDeviceChildren(std::ifstream& file, ModelDescribe& mdl, bool forGUI)
{
	std::string line;
	std::stringstream myStream;
	size_t posL = 0;
	size_t posR = 0;

	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);	//make sure will find tag and read special cases appropriately
		
		FindTag(line, tag, 0);

		if(tag=="")
			foundTag = true;	//there was no tag, so an invalid tag was not used

		findpos = tag.find("device");	//avoid error message
		if (findpos == 0)
			foundTag = true;

		findpos = tag.find("/device");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading the device
		}

		findpos = tag.find("1d");	//describes a 1 dimensional model
		if(findpos == 0)
		{
			foundTag = true;
			mdl.NDim = 1;
		}

		findpos = tag.find("2d");	//describes a 2 dimensional model
		if(findpos == 0)
		{
			foundTag = true;
			mdl.NDim = 2;
		}

		findpos = tag.find("3d");	//describes a 3 dimensional model
		if(findpos == 0)
		{
			foundTag = true;
			mdl.NDim = 3;
		}

		findpos = tag.find("resolution");	//load the resolution of the device (min delta x,y,z)
		if(findpos == 0)
		{
			foundTag = true;
//			LoadResolution(file, mdl);
			LoadXSpace(file, "resolution");
		}

		findpos = tag.find("dimensions");	//load the total dimensions of the device.
		if(findpos == 0)
		{
			foundTag = true;
//			LoadDimension(file, mdl);
			LoadXSpace(file, "dimensions");
		}

		findpos = tag.find("simulation");
		if(findpos == 0)
		{
			foundTag = true;
			Simulation* simul = new Simulation;
			if(mdl.simulation != NULL)
				delete mdl.simulation;
			mdl.simulation = simul;
			simul->mdesc = &mdl;
			LoadSimulationChildren(file, mdl);
		}

		

		findpos = tag.find("material");
		if(findpos == 0)	//found the electric
		{
			foundTag = true;
			Material* mater = new Material;
			mdl.materials.push_back(mater);

			posL = tag.find("name=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				
				if(attribute.size() > 19)
					attribute.resize(19);
				mater->name = attribute;	//load the material name into name
			}

			posL = tag.find("thickness=\"");
			if(posL != std::string::npos)	//found a thickness tag
			{
				posL += 11;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");	//clear out the stream
				myStream << attribute;	//load the info into the stream

				Layer* lyr = new Layer;
				mdl.layers.push_back(lyr);	//keep a pointer to the layer

				double thickness;

				myStream >> thickness;	//pass the distance into the layer
				thickness = thickness*(1e-4);	//thickness is in microns, but our set is in cm
							//divide by two because half goes to distance between points
				lyr->shape->type = SHPLINE;	//this layer is 1D
				int region = mdl.layers.size()-2;	//point to previous layer
				if(region >= 0)
				{
					XSpace pt;
					pt.y = 0;
					pt.x = mdl.layers[region]->shape->max.x;
					lyr->shape->AddPoint(pt);
					pt.x += thickness;
					lyr->shape->AddPoint(pt);
				}
				else
				{
					XSpace pt;
					pt.x = 0;
					pt.y = 0;
					lyr->shape->AddPoint(pt);
					pt.x = thickness;
					lyr->shape->AddPoint(pt);
				}
				
				
				ldescriptor<Material*>* LMat = new ldescriptor<Material*>;
				lyr->matls.push_back(LMat);	//add the descriptor to the new layer
				LMat->data = mater;		//make the new descriptor point to the new material
				LMat->range.startpos.x = 0.0;	//starting point for the material in the layer
				LMat->range.startpos.y = 0.0;
				LMat->range.stoppos.x = thickness;
				LMat->range.stoppos.y = 0.0;	//assume the layer goes in x direction
				LMat->id = -1;	//it has already been assigned a material, so no need to assign it something
				mater->id = -1;	//flag that the material is tied to a specified layer at init, unless later changed
				LMat->flag = DISCRETE;
			}
			//now load particulars of the material subgroup
			LoadMaterialChildren(file, mater, mdl);	//mater should still be pointing to the new material...
		}

		findpos = tag.find("layer");
		if(findpos == 0)	//found the electric
		{
			foundTag = true;
			Layer* lyr = new Layer;
			mdl.layers.push_back(lyr);

			posL = tag.find("name=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				if(attribute.size() > 19)
					attribute.resize(19);
				lyr->name = attribute;	//load the layer name into name
			}

			//now load particulars of the material subgroup
			LoadLayerChildren(file, lyr, mdl);	//mater should still be pointing to the new material...
		}

		findpos = tag.find("impurity");
		if(findpos == 0)
		{
			foundTag = true;
			Impurity* impur = new Impurity;
			mdl.impurities.push_back(impur);
			impur->LoadFromFile(file);
		}

		findpos = tag.find("spectrum");
		if(findpos == 0)
		{
			foundTag = true;
			LightSource* spec = new LightSource;
			posL = tag.find("size=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of unit
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");	//clear out the stream
				myStream << attribute;	//load the info into the stream
				myStream >> size;	//pass the distance into the layer
			}
			
			if (LoadSpectrumChildren(file, mdl, spec, forGUI))
				mdl.spectrum.push_back(spec);
			else
				delete spec;
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - device: "+tag);
	}	//end while file good
	ProgressString("Invalid XML Load file in device tag");
	return;
}

void LoadAddFlagsChildren(std::ifstream& file, TransContact *ctc)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag = "";
	std::string attribute = "";

	bool sum = false;
	double stime = 0.0;
	double etime = CONTACT_HOLD_INDEFINITELY;
	int flag = 0;
	bool foundTag = false;
	while (file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);

		next = FindTag(line, tag, 0);
		if (tag == "")
			foundTag = true;	//no tag found, so don't output error message for blank space

		//avoid error messages
		findpos = tag.find("additional");
		if (findpos == 0)
			foundTag = true;
		findpos = tag.find("/additional");	//always check if need to leave first
		if (findpos == 0)	//end this sub section
		{
			foundTag = true;
			TimeDependence tmp(stime, etime, flag, sum);
			ctc->AddTimeDependence(tmp, stime);
			
			return;	//done loading contact data into pointer.  It is committed in the earlier function
		}
	
		findpos = tag.find("starttime");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</starttime>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> stime;
		}

		findpos = tag.find("endtime");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</endtime>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> etime;
		}

		findpos = tag.find("flag");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</flag>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> size;
			flag |= size;
		}

		findpos = tag.find("sum");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sum>", next);	//find the ending tag
			int size = posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if (attribute == "1" || attribute == "enable" || attribute == "true")
				sum = true;
			else
				sum = false;
		}

		if (foundTag == false)
			ProgressString("Unrecognized tag in load file - contact additional: " + tag);
	}
	ProgressString("Invalid XML Load file in fixed voltage tag");
	return;
}

//contact fixed voltage data
void LoadLinearChildren(std::ifstream& file, TransContact* ctc)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";

	double initial = 0.0;	//set some defaults
	double slope = 0.0;
	bool sum = false;
	double stime = 0.0;
	double etime = CONTACT_HOLD_INDEFINITELY;
	double leak = 0.0;
	int flag = 0;
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;	//no tag found, so don't output error message for blank space

		findpos = tag.find("/linear");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			LinearTimeDependence temp;
			temp.SetValues(initial, slope, stime, etime, leak, flag, sum);

			if(flag & CONTACT_VOLT_LINEAR)
				ctc->AddLinearDependence(temp, CTC_LIN_VOLT, stime);
			if(flag & (CONTACT_EFIELD_LINEAR | CONTACT_EFIELD_LINEAR_ED))
				ctc->AddLinearDependence(temp, CTC_LIN_EFIELD, stime);
			if(flag & (CONTACT_CURRENT_LINEAR | CONTACT_CURRENT_LINEAR_ED))
				ctc->AddLinearDependence(temp, CTC_LIN_CUR, stime);
			if(flag & (CONTACT_CUR_DENSITY_LINEAR | CONTACT_CUR_DENSITY_LINEAR_ED))
				ctc->AddLinearDependence(temp, CTC_LIN_CURD, stime);
			if(flag & CONTACT_CONNECT_EXTERNAL)
			{
				TimeDependence tmp(stime, etime, flag, sum);

				ctc->AddTimeDependence(tmp, stime);
			}
			return;	//done loading contact data into pointer.  It is committed in the earlier function
		}

		findpos = tag.find("initial");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</initial>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> initial;
		}

		findpos = tag.find("starttime");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</starttime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> stime;
		}

		findpos = tag.find("endtime");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</endtime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> etime;
		}

		findpos = tag.find("slope");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</slope>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> slope;
		}

		findpos = tag.find("leak");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</leak>");
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> leak;
			if(leak > 0.0)
				flag = flag | CONTACT_LEAK;
		}

		findpos = tag.find("sum");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sum>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute == "1" || attribute == "enable" || attribute == "true")
				sum = true;
			else 
			{
				sum = false;
//				flag = flag | CONTACT_RESET;
			}
		}

		findpos = tag.find("flag");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</flag>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> size;
			flag |= size;
		}

		findpos = tag.find("volt");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_VOLT_LINEAR;
		}

		findpos = tag.find("efield");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_EFIELD_LINEAR;
		}

		findpos = tag.find("densitycurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CUR_DENSITY_LINEAR;
		}

		findpos = tag.find("current");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CURRENT_LINEAR;
		}

		findpos = tag.find("edefield");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_EFIELD_LINEAR_ED;
		}

		findpos = tag.find("eddensitycurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CUR_DENSITY_LINEAR_ED;
		}

		findpos = tag.find("edcurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CURRENT_LINEAR_ED;
		}

		findpos = tag.find("currentout");	//it should treat it as a floating value that can have a current out
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CONNECT_EXTERNAL;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - contact linear: "+tag);
	}
	ProgressString("Invalid XML Load file in fixed voltage tag");
	return;
}

//contact cosine voltage source
void LoadCosChildren(std::ifstream& file, TransContact* ctc)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";

	double amplitude = 0.0;
	double freq = 0.0;
	double start = 0.0;
	double delta = 0.0;
	double fixed = 0.0;
	bool sum=false;	//default to replace
	double end = CONTACT_HOLD_INDEFINITELY;
	double leak = 0.0;
	int flag = 0;
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/cos");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			CosTimeDependence temp;
			temp.initialize(amplitude, freq, delta, fixed, start, end, leak, sum, flag);
			if(flag & CONTACT_VOLT_COS)
				ctc->AddCosTimeDependence(temp, CTC_COS_VOLT, start);
			if(flag & (CONTACT_EFIELD_COS | CONTACT_EFIELD_COS_ED))
				ctc->AddCosTimeDependence(temp, CTC_COS_EFIELD, start);
			if(flag & (CONTACT_CURRENT_COS | CONTACT_CURRENT_COS_ED))
				ctc->AddCosTimeDependence(temp, CTC_COS_CUR, start);
			if(flag & (CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_COS_ED))
				ctc->AddCosTimeDependence(temp, CTC_COS_CURD, start);
			return;	//done loading contact data into pointer.  It is committed in the earlier function
		}

		findpos = tag.find("amplitude");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</amplitude>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> amplitude;
		}

		findpos = tag.find("starttime");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</starttime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> start;
		}

		findpos = tag.find("endtime");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</endtime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			if(attribute != "indefinite")
			{
				myStream << attribute;
				myStream >> end;
				if(end < 0.0)
					end = CONTACT_HOLD_INDEFINITELY;
			}
			else
				end = CONTACT_HOLD_INDEFINITELY;
		}

		findpos = tag.find("frequency");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</frequency>");
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> freq;
		}

		findpos = tag.find("delta");	//difference in cosine
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</delta>");
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> delta;
		}

		findpos = tag.find("fixed");	//"dc bias" or fixed value that the cos should be added to
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</fixed>");
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> fixed;
		}
	
		findpos = tag.find("leak");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</leak>");
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> leak;
			if(leak > 0.0)
				flag = flag | CONTACT_LEAK;
		}

		findpos = tag.find("sum");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sum>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute == "1" || attribute == "enable" || attribute == "true")
				sum = true;
			else 
			{
				sum = false;
//				flag = flag | CONTACT_RESET;
			}
		}

		findpos = tag.find("flag");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</flag>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> size;
			flag |= size;
		}

		findpos = tag.find("volt");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_VOLT_COS;
		}

		findpos = tag.find("efield");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_EFIELD_COS;
		}

		findpos = tag.find("densitycurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CUR_DENSITY_COS;
		}

		findpos = tag.find("current");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CURRENT_COS;
		}

		findpos = tag.find("edefield");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_EFIELD_COS_ED;
		}

		findpos = tag.find("eddensitycurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CUR_DENSITY_COS_ED;
		}

		findpos = tag.find("edcurrent");
		if(findpos == 0)
		{
			foundTag = true;
			flag = flag | CONTACT_CURRENT_COS_ED;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - contact cos: "+tag);
	}
	ProgressString("Invalid XML Load file in cos voltage tag");
	return;
}

void LoadTransContactChildren(std::ifstream& file, TransientSimData* tSim)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag = "";
	std::string attribute = "";

	TransContact* tC = new TransContact();
	
	double tmp;
	bool foundTag = false;
	while (file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);

		next = FindTag(line, tag, 0);
		if (tag == "")
			foundTag = true;
		findpos = tag.find("contact");
		if (findpos == 0)	//prevent errors on initial load
			foundTag = true;

		findpos = tag.find("/contact");	//always check if need to leave first
		if (findpos == 0)	//end this sub section
		{
			foundTag = true;
			//search through all previously existing contacts
			tSim->ContactData.push_back(tC);
			tC->BuildActiveValues();
			tC->UpdateActive(0.0, true);	//force updating the current Active value
			Contact *ct = tSim->getSim()->getContact(tC->GetContactID());
			if (ct)
				tC->SetContact(ct);	//don't want to accidentally erase the contact ID if it hasn't been found yet!

			return;	//done loading contact data into pointer.  It is committed in the earlier function
		}

		findpos = tag.find("id");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> size;	//just use as a temp variable
			tC->SetContactID(size);
		}

		findpos = tag.find("linear");	//loads any of the linear voltages, electric fields, currents, current densities - in or out of the device
		if (findpos == 0)
		{
			foundTag = true;
			LoadLinearChildren(file, tC);
		}

		findpos = tag.find("cos");
		if (findpos == 0)
		{
			foundTag = true;
			LoadCosChildren(file, tC);
		}

		findpos = tag.find("additional");	//loads any of the linear voltages, electric fields, currents, current densities - in or out of the device
		if (findpos == 0)
		{
			foundTag = true;
			LoadAddFlagsChildren(file, tC);
		}


		findpos = tag.find("thermalbath");	//a thermal bath contact acts as if it were the outside world, and does not change
		if (findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</thermalbath>", next);	//find the ending tag
			int size = posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_SINK, tmp);
		}

		findpos = tag.find("efieldescape");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</efieldescape>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_FLOAT_EFIELD_ESCAPE, tmp);
		}

		findpos = tag.find("currentout");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</currentout>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_CONNECT_EXTERNAL, tmp);
		}

		findpos = tag.find("external");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</external>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_CONNECT_EXTERNAL, tmp);
		}


		findpos = tag.find("minimizefieldenergy");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</minimizefieldenergy>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_MINIMIZE_E_ENERGY, tmp);
		}

		findpos = tag.find("reset");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</reset>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_RESET, tmp);
		}

		findpos = tag.find("oppdfield");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</oppdfield>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmp;	//time to start
			tC->AddAdditionalFlags(CONTACT_OPP_DFIELD, tmp);
		}

		if (foundTag == false)
			ProgressString("Unrecognized tag in load file - contact: " + tag);

	}
	ProgressString("Invalid XML Load file in contact tag");
	delete tC;	//cleanup if bad input file
	return;
	
}



void LoadContactChildren(std::ifstream& file, Simulation* sim)
{
	std::string line;
	std::stringstream myStream;

	
	Contact tmpCtc;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";

	tmpCtc.name = "tempName";

	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/contact");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			//search through all previously existing contacts
			Contact* ctc = sim->getContact(tmpCtc.id);
			if(ctc==NULL)
			{
				ctc = new Contact;
				sim->contacts.push_back(ctc);	//store in primary vector
			}
			else
			{
				//need to add all this stuff into the contact data
				if(ctc->name == "tempName" || ctc->name == "")
					ctc->name = tmpCtc.name;
			}
			
			tmpCtc.TransferDataToNewContact(ctc);
			
			return;	//done loading contact data into pointer.  It is committed in the earlier function
		}

		findpos = tag.find("id");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpCtc.id;
		}


		findpos = tag.find("name");	//is this the material's name
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</name>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute.size() > 19)
					attribute.resize(19);
			tmpCtc.name = attribute;
		}

		findpos = tag.find("isolation");	//an isolated contact can change, but does not go to outside world
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</isolation>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute == "1" || attribute == "enable" || attribute == "true")
				tmpCtc.isolated = true;
			else 
				tmpCtc.isolated = false;
		}




		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - contact: "+tag);

	}
	ProgressString("Invalid XML Load file in contact tag");
	return;
}

void LoadContactLink(std::ifstream& file, Simulation* sim)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	ContactLink tmpLink;
	tmpLink.resistance = -1.0;	//basically disable the link by default
	tmpLink.current = 0.0;
	tmpLink.targetContact = NULL;
	Contact* srcCtc = NULL;
	int tmpval;
	
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/clink");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			if(tmpLink.targetContact != NULL && srcCtc != NULL)
			{
				srcCtc->AddCLink(tmpLink);
				tmpLink.current = -tmpLink.current;	//make sure the current is consistent
				//the resistance stays the same
				Contact* tmpC = srcCtc;	//now swap the contacts
				srcCtc = tmpLink.targetContact;
				tmpLink.targetContact = tmpC;
				srcCtc->AddCLink(tmpLink);	//and put the link in the other contact
			}
			else
			{
				ProgressString("Invalid contact link. Make sure XML links are defined after contacts have been defined.");
			}
			return;	//done loading simulation
		}

		findpos = tag.find("srcid");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</srcid>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpval;
			for(unsigned int i=0; i<sim->contacts.size(); i++)
			{
				if(sim->contacts[i]->id == tmpval)
				{
					srcCtc = sim->contacts[i];
					break;
				}
			}
			
		}

		findpos = tag.find("trgid");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</trgid>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpval;
			for(unsigned int i=0; i<sim->contacts.size(); i++)
			{
				if(sim->contacts[i]->id == tmpval)
				{
					tmpLink.targetContact = sim->contacts[i];
					break;
				}
			}
		}

		findpos = tag.find("resistance");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</resistance>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpLink.resistance;
		}

		findpos = tag.find("current");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</current>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpLink.current;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - contact link: "+tag);
	}

	ProgressString("Invalid XML file in contact link");
	return;
}

void LoadSSContactChildren(std::ifstream& file, Simulation* sim, Environment& env)
{
	
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag;
	bool isolated = false;
	int tmpI;
	std::string ctcName="tempName";

	SSContactData* ctc = new SSContactData();
	ctc->Clear();
	ctc->currentActiveValue = CONTACT_SS;	//make usre it starts with the SS flag

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/contact");	//always check if need to leave first
		
		if(findpos == 0)	//end this sub section
		{
			//search through all previously existing contacts
			foundTag = false;	//just use this as a flag
			for(unsigned int i=0; i<sim->contacts.size(); ++i)
			{
				if(sim->contacts[i]->id == ctc->ctcID)
				{
					foundTag = true;
					ctc->ctc = sim->contacts[i];
					if(ctc->ctc->name == "tempName" || ctc->ctc->name == "")
						ctc->ctc->name = ctcName;
					break;
				}
			}

			if(!foundTag)
			{
				Contact* ct = new Contact;
				sim->contacts.push_back(ct);	//store in primary vector
				ct->id = ctc->ctcID;	//set the new contact to have the id that matches the SS
				ctc->ctc = ct;	//make sure the ss links to this new contact
				ct->name = ctcName;
			}
			if (isolated || (ctc->currentActiveValue & CONTACT_SINK))
				ctc->currentActiveValue = ctc->currentActiveValue & (~CONTACT_CONNECT_EXTERNAL);

			if(ctc->ctc->ContactEnvironmentIndex == -1)	//not defined yet
			{
				ctc->ctc->ContactEnvironmentIndex = env.ContactEnvironment.size();	//make sure the contact data links to what will be the index in the environment data
				env.ContactEnvironment.push_back(ctc);
			}
			else
			{
				int sz = env.ContactEnvironment.size();
				if(ctc->ctc->ContactEnvironmentIndex < sz)
				{
					if (env.ContactEnvironment[ctc->ctc->ContactEnvironmentIndex] != nullptr)
						delete env.ContactEnvironment[ctc->ctc->ContactEnvironmentIndex];	//no memory leaks if it already exists and we're replacing it with something new!
					env.ContactEnvironment[ctc->ctc->ContactEnvironmentIndex] = ctc;
				}
				else
				{
					env.ContactEnvironment.resize(ctc->ctc->ContactEnvironmentIndex+1);

					for(unsigned int i=sz; i<env.ContactEnvironment.size()-1; ++i)	//stop one short of what we care about!
						env.ContactEnvironment[i] = new SSContactData();
					env.ContactEnvironment[ctc->ctc->ContactEnvironmentIndex] = ctc;	//and then put in the one we just defined
				}
			}
			return;	//done loading simulation
		}

		findpos = tag.find("id");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			
			ctc->ctcID = tmpI;
		}

		findpos = tag.find("name");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</name>");	//it should find this no problem
			int size = posR-next;
			ctcName = line.substr(next, size);
			if(ctcName.size() > 19)
				ctcName.resize(19);
		}

		findpos = tag.find("volt");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</volt>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->voltage;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_VOLT_LINEAR;
		}

		findpos = tag.find("efield");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</efield>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->eField;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_EFIELD_LINEAR;
		}

		findpos = tag.find("current");
		if(findpos == 0 && tag.size() < 8)
		{
			foundTag = true;
			posR = line.find("</current>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->current;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CURRENT_LINEAR | CONTACT_CONNECT_EXTERNAL;
		}

		findpos = tag.find("densitycurrent");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</densitycurrent>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->currentDensity;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CUR_DENSITY_LINEAR | CONTACT_CONNECT_EXTERNAL;
		}

		findpos = tag.find("edefield");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</edefield>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->eField_ED;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_EFIELD_LINEAR_ED;
		}

		findpos = tag.find("edleakefield");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</edleakefield>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->eField_ED_Leak;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_FLOAT_EFIELD_ESCAPE;
		}

		findpos = tag.find("edcurrent");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</edcurrent>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->current_ED;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CURRENT_LINEAR_ED;
		}

		findpos = tag.find("eddenscurrent");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</eddenscurrent>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->currentDensity_ED;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CUR_DENSITY_LINEAR_ED;
		}

		findpos = tag.find("eddensleakcurrent");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</eddensleakcurrent>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->currentD_ED_Leak;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_FLOAT_EFIELD_ESCAPE;
		}

		findpos = tag.find("edleakcurrent");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</edleakcurrent>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ctc->current_ED_Leak;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_FLOAT_EFIELD_ESCAPE;
		}

		findpos = tag.find("thermalbath");	//note, this is incompatible with external circuits
		if(findpos == 0)
		{
			foundTag = true;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_SINK;
		}

		findpos = tag.find("isolate");
		if (findpos == 0)
		{
			isolated = true;
			foundTag = true;
		}
		
		findpos = tag.find("currentout");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CONNECT_EXTERNAL;
		}

		findpos = tag.find("minimizefieldenergy");
		if (findpos == 0)
		{
			foundTag = true;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_MINIMIZE_E_ENERGY;
		}

		findpos = tag.find("external");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_CONNECT_EXTERNAL;
		}

		findpos = tag.find("oppdfield");	//note, this is incompatible with contact sink
		if (findpos == 0)
		{
			foundTag = true;
			ctc->currentActiveValue = ctc->currentActiveValue | CONTACT_OPP_DFIELD;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - SSContact: "+tag);

	}	//end while
	ProgressString("Invalid XML Load file in SSContact tag");
	delete ctc;	//no memory leaks if it's a bad file and can't get out!
	return;
}

void LoadSSEnvironmentChildren(std::ifstream& file, Simulation* sim)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag;
	int tmpI;

	SS_SimData* ss = sim->SteadyStateData;	//avoid typing...
	Environment* env = new Environment(ss);
	env->id = -1;
	env->reusePrevContactEnvironment = -1;
	env->reusePrevLightEnvironment = -1;
	env->flags = 0;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		if (tag == "environment")
			foundTag = true;
		findpos = tag.find("/environment");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			if(env->id == -1)	//it was never set.
			{
				//give it a unique ID - easiest check is just to do the size
				env->id = ss->ExternalStates.size();	//default ID to be index in array - order entered in
				bool good;
				do
				{
					good =true;
					for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)	//env->id is size
					{
						if(env->id == ss->ExternalStates[i]->id)
						{
							good = false;
							break;
						}
					}
					if(!good)	//this has to work eventually! one of the numbers over zero wasn't used
						(env->id)--;
				}while(!good);
				
			}
			else //env->id was assigned
			{
				//what if it assigns it to one that already exists (generated or them being stupid
				int conflictIndex = -1;
				for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)	//env->id is size
				{
					if(env->id == ss->ExternalStates[i]->id)
					{
						conflictIndex = i;
						break;
					}
				}
				if(conflictIndex != -1)	//it's bad, so find a new one
				{
					//conflctIndex needs a new id # that is not env->id;
					Environment* repId = (ss->ExternalStates[conflictIndex]);
					repId->id = ss->ExternalStates.size();	//default ID to be index in array - order entered in
					bool good;
					do
					{
						if(repId->id == env->id) //the new env isn't in there yet as is
							(repId->id)--;

						good =true;
						for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)	//env->id is size
						{
							if(i==conflictIndex)	//skip over itself!
								continue;

							if(repId->id == ss->ExternalStates[i]->id)
							{
								good = false;
								break;
							}
						}
						if(!good)	//this has to work eventually! one of the numbers over zero wasn't used
							(repId->id)--;
					}while(!good);
				}
			}

			ss->ExternalStates.push_back(env);

			return;	//done loading simulation
		}

		findpos = tag.find("spectraid");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</spectraid>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			
			env->ActiveLightEnvironment.push_back(tmpI);
		}

		findpos = tag.find("reusecontact");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</reusecontact>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			
			env->reusePrevContactEnvironment = tmpI;
		}

		findpos = tag.find("reuselight");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</reuselight>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			
			env->reusePrevLightEnvironment = tmpI;
		}

		findpos = tag.find("flags");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</flags>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			
			env->flags = tmpI;
		}

		findpos = tag.find("id");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpI;
			if(tmpI >= 0)
				env->id = tmpI;
		}

		findpos = tag.find("outname");	//load the direction the light comes from
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</outname>");	//it should find this no problem
			int size = posR-next;
			env->FileOut = line.substr(next, size);
			if(env->FileOut.size() > 19)
				env->FileOut.resize(19);

		}	//end load output file name

		



		findpos = tag.find("contact");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			foundTag = true;
			LoadSSContactChildren(file, sim, *env);	//load the data into this position

		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - SSEnvironment: "+tag);

	}	//end while
	ProgressString("Invalid XML Load file in SSEnvironment tag");

	delete env;
	//no memory leaks if this fails!
	return;
}

void LoadCalculationInitEndAccuracy(std::ifstream& file, std::string endTag, double& init, double& end)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag = "";
	std::string attribute = "";
	bool foundTag;
	
	init = end = 0.0;
	while (file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if (tag == "")
			foundTag = true;
		findpos = tag.find(endTag);	//always check if need to leave first
		if (findpos == 0)	//end this sub section
		{
			if (init < 0.0)
				init = 0.0;
			if (end < 0.0)
				end = 0.0;

			return;	//done loading simulation
		}

		findpos = tag.find("init");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</init>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> init;
		}

		findpos = tag.find("end");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</end>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> end;
		}

		if (foundTag == false)
			ProgressString("Unrecognized tag in load file - LoadCalculationInitEndAccuracy " + endTag + ": " + tag);
	}
	//end while
	ProgressString("Invalid XML Load file in LoadCalculationInitEndAccuracy " + endTag + " tag");
	return;
}

void LoadCalculationOrderChildren(std::ifstream& file, SS_SimData* ss)
{
	if (ss == NULL)
	{
		ProgressString("No simulation data passed in to add calculation order to");
		return;
	}

	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag = "";
	std::string attribute = "";
	bool foundTag;
	double init, end;

	ss->ClearCalcOrder();

	while (file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if (tag == "")
			foundTag = true;
		findpos = tag.find("/calcorder");	//always check if need to leave first
		if (findpos == 0)	//end this sub section
		{
			return;	//done loading simulation
		}

		findpos = tag.find("zero_no_srh");	
		if (findpos == 0)			
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/zero_no_srh", init, end);

			ss->AddCalcOrder(SS_NO_SRH, init, end);
		}


		findpos = tag.find("matchimps");	
		if (findpos == 0)		
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/matchimps", init, end);

			ss->AddCalcOrder(SS_MATCH_IMPS, init, end);
		}

		findpos = tag.find("zero_all");	
		if (findpos == 0)		
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/zero_all", init, end);

			ss->AddCalcOrder(SS_ALL, init, end);
		}

		findpos = tag.find("zero_slow");	
		if (findpos == 0)		
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/zero_slow", init, end);

			ss->AddCalcOrder(SS_SLOWPTS, init, end);
		}

		findpos = tag.find("zero_trans");
		if (findpos == 0)		
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/zero_trans", init, end);

			ss->AddCalcOrder(SS_TRANSLIKE, init, end);
		}

		findpos = tag.find("newton_nl");
		if (findpos == 0)	
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/newton_nl", init, end);

			ss->AddCalcOrder(SS_NEWTON_NOLIGHT, init, end);
		}

		findpos = tag.find("newton");
		if (findpos == 0 && tag.size() < 8)	
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/newton", init, end);

			ss->AddCalcOrder(SS_NEWTON_ALL, init, end);
		}

		findpos = tag.find("newton_f_nl");
		if (findpos == 0)	
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/newton_f_nl", init, end);

			ss->AddCalcOrder(SS_NEWTON_NL_FULL, init, end);
		}

		findpos = tag.find("newton_f");
		if (findpos == 0 && tag.size() < 10)
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/newton_f", init, end);

			ss->AddCalcOrder(SS_NEWTON_L_FULL, init, end);
		}

		findpos = tag.find("transient");
		if (findpos == 0)	
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/transient", init, end);

			ss->AddCalcOrder(SS_TRANS_ACTUAL, init, end);
		}

		findpos = tag.find("smooth_bad");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/smooth_bad", init, end);

			ss->AddCalcOrder(SS_SMOOTH_BAD, init, end);
		}

		findpos = tag.find("montecarlo");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/montecarlo", init, end);

			ss->AddCalcOrder(SS_MONTECARLO, init, end);
		}

		findpos = tag.find("newton_num");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/newton_num", init, end);

			ss->AddCalcOrder(SS_NEWTON_NUMERICAL, init, end);
		}

		findpos = tag.find("trans_cutoff");
		if (findpos == 0)	
		{
			foundTag = true;
			posR = line.find("/");	//returns a number if this was just a single listing and using the default values for simulation accuracy
			init = end = 0.0;
			if (posR == std::string::npos)	//it was not a single use tag
				LoadCalculationInitEndAccuracy(file, "/trans_cutoff", init, end);

			ss->AddCalcOrder(SS_TRANS_CUTOFF, init, end);
		}

		if (foundTag == false)
			ProgressString("Unrecognized tag in load file - LoadCalculationOrderChildren: " + tag);
	}
	//end while
	ProgressString("Invalid XML Load file in LoadCalculationOrderChildren tag");

return;
		
}

void LoadSteadyStateChildren(std::ifstream& file, ModelDescribe& mdesc, bool merge)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag;


	if (!merge && mdesc.simulation->SteadyStateData)
	{
		delete mdesc.simulation->SteadyStateData;
		mdesc.simulation->SteadyStateData = new SS_SimData(mdesc.simulation);
	}
	else if (mdesc.simulation->SteadyStateData == nullptr)
		mdesc.simulation->SteadyStateData = new SS_SimData(mdesc.simulation);

	SS_SimData* ss = mdesc.simulation->SteadyStateData;	//avoid typing...

	ss->CurrentData = 0;	//make it start as the first currentData - guarantee first iteration calcs are done
	ss->LastBDUpdated = -1;	//being different from CurrentData guarantees that first iteration calcs are done
	ss->steadyStateClampReduction = SS_EF_ALTER;
	ss->PercentGood = MC_PERCENTGOOD;
	ss->NumTrack = MC_NUMTRACK_MIN;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/steadystate");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			return;	//done loading simulation
		}

		findpos = tag.find("steadystate");	//always check if need to leave first
		if (findpos == 0)
			foundTag = true;	//suppress warnings for loading directly from this file!

		
		findpos = tag.find("ssclamp");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</ssclamp>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ss->steadyStateClampReduction;
			if (ss->steadyStateClampReduction > 1.0)
				ss->steadyStateClampReduction = 1.0;
			if (ss->steadyStateClampReduction <= 0.0)
				ss->steadyStateClampReduction = 0.9;

		}

		findpos = tag.find("mcpgood");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</mcpgood>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ss->PercentGood;
			if(ss->PercentGood > 1.0)
				ss->PercentGood = 1.0;	//this will probably be a complete fail
			if(ss->PercentGood <= 0.0)
				ss->PercentGood = MC_PERCENTGOOD;

		}

		findpos = tag.find("mctrack");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</mctrack>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ss->NumTrack;
			if(ss->NumTrack > MC_NUMTRACK_MAX)
				ss->NumTrack = MC_NUMTRACK_MAX;	//this will probably be a complete fail
			if(ss->NumTrack < MC_NUMTRACK_MIN)	//minimum value
				ss->NumTrack = MC_NUMTRACK_MIN;

		}

		findpos = tag.find("environment");	//load the number of iterations it should go through
		if (findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			LoadSSEnvironmentChildren(file, mdesc.simulation);
		}

		findpos = tag.find("calcorder");	//load the number of iterations it should go through
		if (findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			LoadCalculationOrderChildren(file, ss);
		}



		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - steadystate: "+tag);

	}	//end while
	ProgressString("Invalid XML Load file in steadystate tag");

	return;
}

void LoadSpectraTimeChildren(std::ifstream& file, TransientSimData* tSim)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	SpectraTimes spec;
	spec.enable = false;
	spec.time = 0.0;
	spec.SpecID = -1;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;	//no tag found, so don't output error message for blank space


		findpos = tag.find("/spectime");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			tSim->SpecTime.push_back(spec);
			return;
		}

		findpos = tag.find("time");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</time>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> spec.time;
		}

		findpos = tag.find("id");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> spec.SpecID;
		}
		findpos = tag.find("enable");	// mW/cm^2
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			spec.enable = true;
		}

		findpos = tag.find("disable");	//spread of variation: 0-90 degrees
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			spec.enable = false;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - SpectraTime: "+tag);
	}	//end while
	ProgressString("Invalid XML Load file in SpectraTime tag");
	return;
}

void LoadTransientChildren(std::ifstream& file, ModelDescribe& mdesc, bool merge)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	double tmp;
	bool foundTag;

	if (merge) {
		if (mdesc.simulation->TransientData==nullptr)
			mdesc.simulation->TransientData = new TransientSimData(mdesc.simulation);
	}
	else {
		if (mdesc.simulation->TransientData)
			delete mdesc.simulation->TransientData;
		mdesc.simulation->TransientData = new TransientSimData(mdesc.simulation);
	}
	
	//this is created all zeroed out
	TransientSimData* tsd = mdesc.simulation->TransientData;	//just don't feel like typing that all out all the time

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("transient");	//suppress warning messages for entry into this function at the top
		if (findpos == 0)
			foundTag = true;

		findpos = tag.find("/transient");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			TargetTimeData ttd;
			ttd.SaveState = tsd->SmartSaveState;
			ttd.SSTarget = -1;
			

			if(tsd->SSByTransient==true)
				mdesc.simulation->outputs.numinbin = 1;	//don't store excess data - just rewrite stuff

			return;	//done loading simulation
		}

		


		findpos = tag.find("intermedstep");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</intermedstep>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tsd->IntermediateSteps;
			if (tsd->IntermediateSteps < 0)	//make sure it does some!
				tsd->IntermediateSteps = 0;
		}

		findpos = tag.find("statesave");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</statesave>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			if(attribute == "smart")
				tsd->SmartSaveState = true;
			else
			{
				myStream >> tmp;
				TargetTimeData ttd;
				ttd.SaveState = true;
				ttd.SSTarget = -1;
				tsd->TargetTimes.insert(std::pair<double, TargetTimeData>(tmp, ttd));
			}
			
		}

		findpos = tag.find("ssbyttime");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</ssbyttime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tsd->SSbyTTime;
			if(tsd->SSbyTTime <= 0)	//make sure it's something
				tsd->SSbyTTime = 1.0;	//prefereably a second default
		}

		findpos = tag.find("contact");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			foundTag = true;
			LoadTransContactChildren(file, tsd);	//load the data into this position
		}

		findpos = tag.find("spectime");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			foundTag = true;
			LoadSpectraTimeChildren(file, tsd);	//load the data into this position
		}

		findpos = tag.find("simendtime");	//load the timestep of the device
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</simendtime>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >>tsd->simendtime;
		}

		findpos = tag.find("maxtstep");	//load the timestep of the device
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</maxtstep>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			double tmp;
			myStream >> tmp;
			tsd->tStepControl.SetUserMaxTStep(tmp);
		}

		findpos = tag.find("timesteprepeat");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</timesteprepeat>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			int tmp;	//probably not the most elegant way of doing things...
			myStream >> tmp;
			tsd->tStepControl.SetNumRepeat(tmp);
		}

		findpos = tag.find("timeignore");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</timeignore>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tsd->timeignore;
		}

		findpos = tag.find("startcond");	//load the number of iterations it should go through
		if(findpos == 0)			//before it starts tracking averages for output
		{
			foundTag = true;
			posR = line.find("</startcond>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute == "sstimezero")
			{
				tsd->StartingCondition = START_SS_TZERO;
			}
			else if(attribute == "theq")
			{
				tsd->StartingCondition = START_THEQ;
			}
			else if(attribute == "abszero")
			{
				tsd->StartingCondition = START_ABSZERO;
			}
			else
			{
				tsd->StartingCondition = START_SS;
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> tsd->StartingCondition;
			}
			
		}

		findpos = tag.find("ssbytrans");
		if(findpos == 0)
		{
			foundTag = true;
			tsd->SSByTransient = true;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - transient: "+tag);

	}	//end while
	ProgressString("Invalid XML Load file in transient tag");
	return;
}

void LoadSimulationChildren(std::ifstream& file, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	mdl.simulation->EnabledRates.Current = true;	//if they don't set them, go with default values
	mdl.simulation->EnabledRates.Lpow = true;
	mdl.simulation->EnabledRates.Lgen = true;
	mdl.simulation->EnabledRates.Lsrh = true;
	mdl.simulation->EnabledRates.Lscat = true;
	mdl.simulation->EnabledRates.Ldef = true;
	mdl.simulation->EnabledRates.SErec = true;
	mdl.simulation->EnabledRates.SEsrh = true;
	mdl.simulation->EnabledRates.SEscat = true;
	mdl.simulation->EnabledRates.SEdef = true;
	mdl.simulation->EnabledRates.Scatter = true;
	mdl.simulation->EnabledRates.SRH1 = true;
	mdl.simulation->EnabledRates.SRH2 = true;
	mdl.simulation->EnabledRates.SRH3 = true;
	mdl.simulation->EnabledRates.SRH4 = true;
	mdl.simulation->EnabledRates.ThermalGen = true;
	mdl.simulation->EnabledRates.ThermalRec = true;
	mdl.simulation->EnabledRates.Tunnel = false;
	
	
	mdl.simulation->transient = true;

	mdl.simulation->ThEqPtStart = true;
	mdl.simulation->ThEqDeviceStart = true;
	
	
	mdl.simulation->timeSaveState = 0.0;
	
	
	

	while(file.good())
	{
		getline(file, line);
		ToLower(line);
		foundTag = false;
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/simulation");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading simulation
		}

		

		findpos = tag.find("accuracy");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</accuracy>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->accuracy;
		}

		findpos = tag.find("statediv");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</statediv>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->statedivisions;
		}

		

		
		

		findpos = tag.find("timestatesave");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</timestatesave>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->timeSaveState;
			mdl.simulation->timeSaveState = ceil(mdl.simulation->timeSaveState);	//make it an "integer"
		}

		findpos = tag.find("contact");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			foundTag = true;
			
			LoadContactChildren(file, mdl.simulation);	//load the data into this position
		}

		findpos = tag.find("transient");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			mdl.simulation->transient = true;
			foundTag = true;
			LoadTransientChildren(file, mdl);	//load the data into this position

		}

		findpos = tag.find("steadystate");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			mdl.simulation->transient = false;
			foundTag = true;
			LoadSteadyStateChildren(file, mdl);	//load the data into this position

		}



		findpos = tag.find("clink");	//load the fixed voltages for the specified layers
		if(findpos == 0)
		{
			foundTag = true;
			LoadContactLink(file, mdl.simulation);
		}

		findpos = tag.find("temperature");	//load the temperature of the device
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</temperature>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->T;
		}

		findpos = tag.find("maxlightreflect");	//load the temperature of the device
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</maxlightreflect>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->maxLightReflections;
		}

/*		findpos = tag.find("timestep");	//load the timestep of the device
		if(findpos == 0)
		{
			posR = line.find("</timestep>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->timestep;
			mdl.simulation->maxtimestep = mdl.simulation->timestep;
		}*/

		

		findpos = tag.find("maxiter");	//load the maximum number of iterations in the device
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</maxiter>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> mdl.simulation->maxiter;
		}

		

		

		findpos = tag.find("lightincident");	//load the direction the light comes from
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</lightincident>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute== "posx")	//the light is incident from the positve x area, or to the right
					mdl.simulation->lightincidentdir = RIGHT;
			else if(attribute == "negx")
					mdl.simulation->lightincidentdir = LEFT;
			else if(attribute == "posy")
					mdl.simulation->lightincidentdir = BACKWARD;
			else if(attribute == "negy")
					mdl.simulation->lightincidentdir = FORWARD;
			else if(attribute == "posz")
					mdl.simulation->lightincidentdir = UP;
			else if(attribute == "negz")
					mdl.simulation->lightincidentdir = DOWN;				
			else
			{
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> mdl.simulation->lightincidentdir;
			}	//end if-else group
		}	//end light incident from direction

		findpos = tag.find("outname");	//load the direction the light comes from
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</outname>");	//it should find this no problem
			int size = posR-next;
			mdl.simulation->OutputFName = line.substr(next, size);
			if(mdl.simulation->OutputFName.size() > 19)
				mdl.simulation->OutputFName.resize(19);

		}	//end load output file name

		findpos = tag.find("validoutputs");	//load the direction the light comes from
		if(findpos == 0)
		{
			foundTag = true;
			LoadValidOut(file, mdl.simulation);
		}	//end load output file name

		findpos = tag.find("calcrates");
		if(findpos == 0)
		{
			foundTag = true;
			LoadCalcs(file, mdl.simulation);
		}	//end load output file name

		findpos = tag.find("steadystate");
		if(findpos == 0)
		{
			foundTag = true;
			mdl.simulation->transient = false;
		}

		findpos = tag.find("disablestartdevicetheq");
		if(findpos == 0)
		{
			foundTag = true;
			mdl.simulation->ThEqDeviceStart = false;
		}

		findpos = tag.find("disablestartpointtheq");
		if(findpos == 0)
		{
			mdl.simulation->ThEqPtStart = false;
			foundTag = true;
		}

		findpos = tag.find("enablestartdevicetheq");
		if(findpos == 0)
		{
			foundTag = true;
			mdl.simulation->ThEqDeviceStart = true;
		}

		findpos = tag.find("enablestartpointtheq");
		if(findpos == 0)
		{
			mdl.simulation->ThEqPtStart = true;
			foundTag = true;
		}

		

		

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - simulation: "+tag);

	}	//end while
	ProgressString("Invalid XML Load file in simulation tag");
	return;
}

void LoadCalcs(std::ifstream& file, Simulation* sim)
{
	std::string line;

	
	size_t findpos = 0;
	std::string tag="";
	size_t next, posR;
	std::string attribute;
	std::stringstream myStream;
	bool foundTag = false;
	sim->EnabledRates.DisableAll();
	sim->EnabledRates.OpticalEMax = -1.0;
	sim->EnabledRates.OpticalEMin = 0.0;
	sim->EnabledRates.OpticalEResolution = 0.001;
	sim->EnabledRates.OpticalIntensityCutoff = 1e-4;
	
	
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/calcrates");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading simulation valid outputs files
		}

		findpos = tag.find("current");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Current = true;
		}

		findpos = tag.find("lightemission");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.LightEmissions = true;
		}
		
		findpos = tag.find("lightpower");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Lpow = true;
		}

		findpos = tag.find("lightgen");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Lgen = true;
		}

		findpos = tag.find("lightscat");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Lscat = true;
		}

		findpos = tag.find("lightdef");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Ldef = true;
		}

		findpos = tag.find("lightsrh");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Lsrh = true;
		}

		findpos = tag.find("serec");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SErec = true;
		}

		findpos = tag.find("sescat");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SEscat = true;
		}

		findpos = tag.find("sedef");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SEdef = true;
		}

		findpos = tag.find("sesrh");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SEsrh = true;
		}

		findpos = tag.find("scatter");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Scatter = true;
		}

		findpos = tag.find("srh1");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SRH1 = true;
		}

		findpos = tag.find("srh2");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SRH2 = true;
		}

		findpos = tag.find("srh3");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SRH3 = true;
		}

		findpos = tag.find("srh4");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.SRH4 = true;
		}

		findpos = tag.find("thermalgen");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.ThermalGen = true;
		}

		findpos = tag.find("thermalrec");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.ThermalRec = true;
		}

		findpos = tag.find("tunnel");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Tunnel = true;
		}
		findpos = tag.find("recyclephotons");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.RecyclePhotonsAffectRates = true;
		}

		findpos = tag.find("thermalizeall");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = THERMALIZE_ALL;
		}
		findpos = tag.find("thermalizebands");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_CB | THERMALIZE_VB;
		}

		findpos = tag.find("thermalizetails");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_CBT | THERMALIZE_VBT;
		}

		findpos = tag.find("thermalizeimps");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_DON | THERMALIZE_ACP;
		}

		findpos = tag.find("thermalizecb");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_CB;
		}
		findpos = tag.find("thermalizevb");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_VB;
		}

		findpos = tag.find("thermalizecbtail");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_CBT;
		}
		findpos = tag.find("thermalizevbtail");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_VBT;
		}
		findpos = tag.find("thermalizeacp");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_ACP;
		}
		findpos = tag.find("thermalizedon");
		if(findpos == 0)
		{
			foundTag = true;
			sim->EnabledRates.Thermalize = sim->EnabledRates.Thermalize | THERMALIZE_DON;
		}
		

		findpos = tag.find("opticalemax");
		if(findpos == 0)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</opticalemax>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sim->EnabledRates.OpticalEMax;	//if <= 0.0, then there is no max
		}

		findpos = tag.find("opticalemin");
		if(findpos == 0)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</opticalemin>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sim->EnabledRates.OpticalEMin;
			if(sim->EnabledRates.OpticalEMin < 0.0)
				sim->EnabledRates.OpticalEMin = 0.0;
		}

		findpos = tag.find("opticaleres");
		if(findpos == 0)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</opticaleres>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sim->EnabledRates.OpticalEResolution;
			if(sim->EnabledRates.OpticalEResolution <= 0.0)
				sim->EnabledRates.OpticalEResolution = 0.001;
		}
		findpos = tag.find("opticalintensity");
		if(findpos == 0)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</opticalintensity>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sim->EnabledRates.OpticalIntensityCutoff;
			if(sim->EnabledRates.OpticalIntensityCutoff < 0.0)
				sim->EnabledRates.OpticalIntensityCutoff = 1e-4;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - calculations: "+tag);
	}
	ProgressString("Invalid XML Load file in calcrates tag");
	return;
}

void LoadValidOut(std::ifstream& file, Simulation* sim)
{
	std::string line;

	
	size_t findpos = 0;
	std::string tag="";
	std::stringstream myStream;
	std::string attribute="";
	size_t posR = 0;
	size_t next;
	bool foundTag = false;
	sim->outputs.ClearAll();

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/validoutputs");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading simulation valid outputs files
		}

		findpos = tag.find("numinbin");
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</numinbin>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sim->outputs.numinbin;
		}

		findpos = tag.find("nrcb");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.CBNRin = true;
		}

		findpos = tag.find("emptyqe");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.EmptyQEFiles = true;
		}

		findpos = tag.find("nrvb");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.VBNRin = true;
		}

		findpos = tag.find("nracp");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.AcpNRin = true;
		}

		findpos = tag.find("nrdon");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.DonNRin = true;
		}

		findpos = tag.find("lightemission");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.LightEmission = true;
		}

		findpos = tag.find("opticpower");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Lpow = true;
		}

		findpos = tag.find("opticgen");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Lgen = true;
		}

		findpos = tag.find("absorbsrh");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Lsrh = true;
		}

		findpos = tag.find("absorbdef");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Ldef = true;
		}

		findpos = tag.find("absorbscat");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Lscat = true;
		}

		findpos = tag.find("stimemission");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Erec = true;
		}

		findpos = tag.find("sesrh");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Esrh = true;
		}

		findpos = tag.find("sedef");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Edef = true;
		}

		findpos = tag.find("sescat");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Escat = true;
		}
	
		findpos = tag.find("cbcarrier");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.CarrierCB = true;
		}

		findpos = tag.find("vbcarrier");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.CarrierVB = true;
		}

		findpos = tag.find("conductionband");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Ec = true;
		}

		findpos = tag.find("stderr");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.error = true;
		}

		findpos = tag.find("valenceband");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Ev = true;
		}

		findpos = tag.find("fermi");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.fermi = true;
		}

		findpos = tag.find("efn");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.fermin = true;
		}

		findpos = tag.find("efp");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.fermip = true;
		}

		findpos = tag.find("genthermal");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.genth = true;
		}

		findpos = tag.find("impcharge");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.impcharge = true;
		}

		findpos = tag.find("jn");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Jn = true;
		}

		findpos = tag.find("jp");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Jp = true;
		}

		findpos = tag.find("nconc");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.n = true;
		}

		findpos = tag.find("pconc");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.p = true;
		}

		findpos = tag.find("localvaccuum");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.Psi = true;
		}

		findpos = tag.find("recthermal");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.recth = true;
		}

		findpos = tag.find("rho");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.rho = true;
		}
		
		findpos = tag.find("srh");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.SRH = true;
		}

		findpos = tag.find("scatter");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.scatter = true;
		}

		findpos = tag.find("stddev");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.stddev = true;
		}

		findpos = tag.find("qe");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.QE = true;
		}

		findpos = tag.find("chgerr");
		if(findpos == 0)
		{
			foundTag = true;
			sim->outputs.chgError = true;
		}

		findpos = tag.find("outputallrates");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.OutputAllIndivRates = true;
		}

		findpos = tag.find("rateplots");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.OutputRatePlots = true;
		}

		findpos = tag.find("jvcurves");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.jvCurves = true;
		}

		findpos = tag.find("envsummaries");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.envSummaries = true;
		}

		findpos = tag.find("envresults");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.envResults = true;
		}

		findpos = tag.find("contactstates");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.contactStates = true;
		}

		findpos = tag.find("capacitance");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.capacitance = true;
		}

		findpos = tag.find("contactcharge");
		if (findpos == 0)
		{
			foundTag = true;
			sim->outputs.contactCharge = true;
		}

		
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - outputs: "+tag);

	}
	ProgressString("Invalid XML Load file in valid out tag");
	return;
}

void LoadResolution(std::ifstream& file, ModelDescribe& mdl)
{
	std::string line;
	size_t findpos = 0;
	std::string tag="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/resolution");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading the resolution
		}

		findpos = tag.find("xspace");
		if(findpos == 0)
		{
			foundTag = true;
			mdl.resolution = LoadXSpace(file);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - resolution: "+tag);

	}	//end loading file
	ProgressString("Invalid XML Load file in resolution tag");
	return;
}

void LoadDimension(std::ifstream& file, ModelDescribe& mdl)
{
	std::string line;
	size_t findpos = 0;
	std::string tag="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/dimensions");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading the resolution
		}

		findpos = tag.find("xspace");
		if(findpos == 0)
		{
			foundTag = true;
			mdl.Ldimensions = LoadXSpace(file);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - dimension: "+tag);
	}	//end loading file
	ProgressString("Invalid XML Load file in dimension tag");
	return;
}

void LoadVertex(std::ifstream& file, Shape* shape)
{
	std::string line;
	size_t findpos = 0;
	std::string tag="";
	bool foundTag = false;
	XSpace tempXS;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/vertex");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			shape->AddPoint(tempXS);
			return;	//done loading the resolution
		}

		findpos = tag.find("xspace");
		if(findpos == 0)
		{
			foundTag = true;
			tempXS = LoadXSpace(file);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - vertex: "+tag);

	}	//end loading file
	ProgressString("Invalid XML Load file in vertex tag");
	return;
}

XSpace LoadXSpace(std::ifstream& file, std::string closetag)
{
	XSpace retval;
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	retval.x = 0.0;
	retval.y = 0.0;

	if (closetag == "")
		closetag = "xspace";

	std::string closetagslash = "/" + closetag;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if (tag == "" || tag == closetag)	//it preemptively entered this loop
			foundTag = true;

		findpos = tag.find(closetagslash);	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return(retval);	//done loading XSpace
		}

		findpos = tag.find("x");
		if(findpos == 0 && tag.size() < 3)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</x>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.x;
		}

		
		findpos = tag.find("y");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</y>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.y;
		}


		
		findpos = tag.find("z");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</z>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.z;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - xspace: "+tag + " -- " + closetag);

	}	//end while file good
	ProgressString("Invalid XML Load file in xspace tag: " + closetag);
	return(retval);
}
void LoadLayerChildren(std::ifstream& file, Layer* lyr, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;
	size_t posL = 0;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/layer");
		if (findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading the layer
		}

		findpos = tag.find("layer");	//just prevent error messaging when loading from a smaller segment of the file!
		if (findpos == 0)	//end this sub section
			foundTag = true;

		findpos = tag.find("material");
		if(findpos == 0)	//found the electric
		{
			foundTag = true;
			Material* mater = new Material;		//set a new material up on the heap
			mdl.materials.push_back(mater);		//update the master pointer with this material
			
			ldescriptor<Material*>* ldesctemp = new ldescriptor<Material*>;	//create a dummy to push back
			ldesctemp->data = mater;
			lyr->matls.push_back(ldesctemp);

			posL = tag.find("name=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of name
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				if(attribute.size() > 19)
					attribute.resize(19);
				mater->name = attribute;	//load the material name into name
			}
			//now load particulars of the material subgroup
			LoadMaterialChildren(file, mater, mdl);	//mater should still be pointing to the new material...
		}

		findpos = tag.find("impurity");
		if(findpos == 0)
		{
			foundTag = true;
			Impurity* impur = new Impurity;
			mdl.impurities.push_back(impur);

			ldescriptor<Impurity*>* ldesctemp = new ldescriptor<Impurity*>;	//create a dummy to push back
			ldesctemp->data = impur;
			lyr->impurities.push_back(ldesctemp);	//make sure the descriptor is tied to the impurity even if
													//nothing is known about it yet...
			impur->LoadFromFile(file);
		}

		findpos = tag.find("ldescmat");
		if(findpos == 0)
		{
			foundTag = true;
			ldescriptor<Material*>* ldescmat = new ldescriptor<Material*>;	//create a dummy pointer to the heap
			lyr->matls.push_back(ldescmat);			//store it in the layer materials description pointer
			LoadLDescMatChildren(file, ldescmat, mdl);	//now modify the data using said pointer
		}

		findpos = tag.find("ldescimp");
		if(findpos == 0)
		{
			foundTag = true;
			ldescriptor<Impurity*>* ldescmat = new ldescriptor<Impurity*>;	//create a dummy pointer to the heap
			lyr->impurities.push_back(ldescmat);			//store it in the layer materials description pointer
			LoadLDescImpChildren(file, ldescmat, mdl);	//now modify the data using said pointer
		}

		findpos = tag.find("exclusive");
		if(findpos == 0)
		{
			foundTag = true;
			lyr->exclusive = true;
		}

		findpos = tag.find("outoptical");
		if(findpos == 0)
		{
			foundTag = true;
			lyr->out.OpticalPointEmissions = true;
		}

		findpos = tag.find("priority");
		if(findpos == 0)
		{
			foundTag = true;
			lyr->priority = true;
		}

		findpos = tag.find("inclusive");
		if(findpos == 0)
		{
			foundTag = true;
			lyr->exclusive = false;
		}

		findpos = tag.find("edge");	//is this the a tag...
		if (findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</edge>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->spacingboundary;
			lyr->spacingboundary = lyr->spacingboundary * 1e-7;	//convert inputted nanometers to cm equivalent
		}

		findpos = tag.find("name");	//is this the a tag...
		if (findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</name>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->name;
		}

		findpos = tag.find("affectmesh");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</affectmesh>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute == "no" || attribute == "n" || attribute == "0" || attribute == "false")
				lyr->AffectMesh = false;
			else
				lyr->AffectMesh = true;
		}

		findpos = tag.find("center");	//is this the a tag...
		if(findpos == 0 && tag.size() < 7)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</center>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->spacingcenter;
			lyr->spacingcenter = lyr->spacingcenter * 1e-7;	//convert inputted nanometers to cm equivalent
		}

		findpos = tag.find("id");	//is this the id tag...
		if(findpos == 0 && tag.size() < 4)	//extract the id data
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->id;
		}

		findpos = tag.find("contactid");	//is this the id tag...
		if(findpos == 0)	//extract the id data
		{
			foundTag = true;
			posR = line.find("</contactid>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->contactid;
		}

		findpos = tag.find("sizescale");	//is this the id tag...
		if(findpos == 0)	//extract the id data
		{
			foundTag = true;
			posR = line.find("</sizescale>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lyr->sizeScale;
			if(lyr->sizeScale > 1.0 || lyr->sizeScale <= 0.0)
				lyr->sizeScale = 1.0;
		}

		findpos = tag.find("shape");	//is this the id tag...
		if(findpos == 0)	//extract the id data
		{
			foundTag = true;
			Shape* shape = new Shape;
			if(lyr->shape != NULL)
			{
				delete lyr->shape;	//unallocate old stuff
			}
			lyr->shape = shape;
			LoadShapeChildren(file, shape);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - layer: "+tag);

	}	//end while loop
	ProgressString("Invalid XML Load file in layer tag");
	return;
}

void LoadShapeChildren(std::ifstream& file, Shape* shape)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/shape");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;	//done loading the layer
		}

		findpos = tag.find("type");	//is this the id tag...
		if(findpos == 0)	//extract the id data
		{
			foundTag = true;
			posR = line.find("</type>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute == "point")
				shape->type = SHPPOINT;
			else if(attribute == "line")
				shape->type = SHPLINE;
			else if(attribute == "triangle")
				shape->type = TRIANGLE;
			else if(attribute == "rectangle")
				shape->type = RECTANGLE;
			else if(attribute == "quadrilateral")
				shape->type = QUAD;
			else if(attribute == "pentagon")
				shape->type = PENTAGON;
			else if(attribute == "hexagon")
				shape->type = HEXAGON;
			else if(attribute == "ellipse")
				shape->type = ELLIPSE;
			else if(attribute == "circle")
				shape->type = CIRCLE;
			else if(attribute == "square")
				shape->type = SQUARE;
			else	//its a number code
			{
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> shape->type;
			}
		}	//end type tag

		findpos = tag.find("vertex");
		if(findpos == 0)
		{
			foundTag = true;
			XSpace tmp = LoadXSpace(file, "vertex");
			shape->AddPoint(tmp);
//			LoadVertex(file, shape);
		}	//finish loading vertex data

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - shape: "+tag);
	}	//end while file good loop
	ProgressString("Invalid XML Load file in shape tag");
	return;
}

void LoadLDescMatChildren(std::ifstream& file, ldescriptor<Material*>* ldesc, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/ldescmat");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("flag");	//load how this particular material is distribute throughout
		if(findpos == 0)	//note, this is fairly irrelevant for materials, IMHO, but you never know!
		{
			foundTag = true;
			int flag = DISCRETE;		//let this just mean everywhere!
			posR = line.find("</flag>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute == "discrete")
				flag = DISCRETE;
			else if(attribute == "banded")
				flag = BANDED;
			else if(attribute == "gaussian")
				flag = GAUSSIAN;
			else if(attribute == "gaussian2")
				flag = GAUSSIAN2;	//2D gaussian
			else if(attribute == "gaussian3")
				flag = GAUSSIAN3;
			else if(attribute == "exponential")
				flag = EXPONENTIAL;
			else if(attribute == "exponential2")
				flag = EXPONENTIAL2;
			else if(attribute == "exponential3")
				flag = EXPONENTIAL3;
			else if(attribute == "linear")
				flag = LINEAR;
			else if(attribute == "parabolic")
				flag = PARABOLIC;
			else if(attribute == "parabolic2")
				flag = PARABOLIC2;
			else if(attribute == "parabolic3")
				flag = PARABOLIC3;
			else
			{
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> flag;
			}
			ldesc->flag = flag;
		}

		findpos = tag.find("ptrange");
		if(findpos == 0)
		{
			foundTag = true;
			ldesc->range = LoadPtRange(file, mdl);
		}

		findpos = tag.find("id");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");	//clear buffer of stream
			myStream << attribute;
			myStream >> ldesc->id;
		}

		findpos = tag.find("multiplier");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</multiplier>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");	//clear buffer of stream
			myStream << attribute;
			myStream >> ldesc->multiplier;
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - material descriptor: "+tag);

	}	//end while file good
	ProgressString("Invalid XML Load file in material description tag");
	return;
}

void LoadLDescImpChildren(std::ifstream& file, ldescriptor<Impurity*>* ldesc, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/ldescimp");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("flag");	//load how this particular material is distribute throughout
		if(findpos == 0)
		{
			foundTag = true;
			int flag = DISCRETE;		//let this just mean everywhere!
			posR = line.find("</flag>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			if(attribute == "discrete")
				flag = DISCRETE;
			else if(attribute == "banded")
				flag = BANDED;
			else if(attribute == "gaussian")
				flag = GAUSSIAN;
			else if(attribute == "gaussian2")
				flag = GAUSSIAN2;	//2D gaussian
			else if(attribute == "gaussian3")
				flag = GAUSSIAN3;
			else if(attribute == "exponential")
				flag = EXPONENTIAL;
			else if(attribute == "exponential2")
				flag = EXPONENTIAL2;
			else if(attribute == "exponential3")
				flag = EXPONENTIAL3;
			else if(attribute == "linear")
				flag = LINEAR;
			else if(attribute == "parabolic")
				flag = PARABOLIC;
			else if(attribute == "parabolic2")
				flag = PARABOLIC2;
			else if(attribute == "parabolic3")
				flag = PARABOLIC3;
			else
			{
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> flag;
			}
			ldesc->flag = flag;
		}

		findpos = tag.find("ptrange");
		if(findpos == 0)
		{
			foundTag = true;
			ldesc->range = LoadPtRange(file, mdl);
		}

		findpos = tag.find("id");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</id>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");	//clear buffer of stream
			myStream << attribute;
			myStream >> ldesc->id;
		}

		findpos = tag.find("multiplier");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</multiplier>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");	//clear buffer of stream
			myStream << attribute;
			myStream >> ldesc->multiplier;
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - impurity descriptor: "+tag);

	}	//end while file good
	ProgressString("Invalid XML Load file in impurity description tag");
	return;
}

XSpace LoadStopPos(std::ifstream& file)
{
	std::string line;
	size_t findpos = 0;
	std::string tag="";
	bool foundTag = false;
	XSpace tmp;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/stoppos");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return(tmp);	//done loading the resolution
		}

		findpos = tag.find("xspace");
		if(findpos == 0)
		{
			foundTag = true;
			tmp = LoadXSpace(file);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - StopPos: "+tag);

	}	//end loading file
	ProgressString("Invalid XML Load file in StopPos tag");
	return(tmp);
}

XSpace LoadStartPos(std::ifstream& file)
{
	std::string line;
	size_t findpos = 0;
	std::string tag="";
	bool foundTag = false;
	XSpace tmp;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/startpos");	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return(tmp);	//done loading the resolution
		}

		findpos = tag.find("xspace");
		if(findpos == 0)
		{
			foundTag = true;
			tmp = LoadXSpace(file);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - StartPos: "+tag);

	}	//end loading file
	ProgressString("Invalid XML Load file in StartPos tag");
	return(tmp);
}

PtRange LoadPtRange(std::ifstream& file, ModelDescribe& mdl)
{
	PtRange tempPtR;
	XSpace tempXS;

	//initialize data used
	tempXS.x = 0;
	tempXS.y = 0;
	tempPtR.startpos = tempXS;
	tempPtR.stoppos = tempXS;

	std::string line;
	std::stringstream myStream;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);	//make sure will find tag properly
		FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/ptrange");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return(tempPtR);
		}


		findpos = tag.find("startpos");
		if(findpos == 0)
		{
			foundTag = true;
			tempPtR.startpos = LoadXSpace(file, "startpos");
			// LoadStartPos(file);
		}

		findpos = tag.find("stoppos");
		if(findpos == 0)
		{
			foundTag = true;
			tempPtR.stoppos = LoadXSpace(file, "stoppos");
			//LoadStopPos(file);
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - ptrange: "+tag);

	}	//end while file good tag
	ProgressString("Invalid XML Load file in point range tag");
	return(tempPtR);
}
void LoadMaterialChildren(std::ifstream& file, Material* matl, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;
	size_t posL = 0;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);	//make sure will find tag properly
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/material");
		if (findpos == 0)	//end this sub section
		{
			foundTag = true;
			
			return;
		}

		findpos = tag.find("material");
		if (findpos == 0)	//could have been the beginning of this sub section
		{
			foundTag = true;
			//basically just don't want to output an error message by calling this for a load on just the material!
		}

		findpos = tag.find("electric");
		if(findpos == 0)	//found the electric
		{
			foundTag = true;
			LoadElectricalChildren(file, matl, mdl);
		}

		findpos = tag.find("defect");
		if(findpos == 0)	//found the defect info
		{
			foundTag = true;
			int num;
			
			posL = tag.find("number=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 8;	//point to first part of number
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> num;

				matl->defects.reserve(matl->defects.capacity() + num);	//allocate enough space for the defects
			}

			LoadDefects(file, matl, mdl);

		}
		findpos = tag.find("optical");
		if(findpos == 0)	//found the optical info
		{
			foundTag = true;
			if(matl->optics==NULL)
				matl->optics = new Optical;

			LoadOpticalChildren(file, matl->optics, mdl);
		}
		
		

		findpos = tag.find("id");	//is this the a tag... - use the if to not confuse with alpha
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</id>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->id;
		}

		findpos = tag.find("describe");	//is this the material's description
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</describe>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			matl->describe = attribute;
		}

		findpos = tag.find("name");	//is this the material's name
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</name>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute.size() > 19)
				attribute.resize(19);
			matl->name = attribute;
		}

		findpos = tag.find("edge");	//is this the a tag... - use the if to not confuse with alpha
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</edge>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			int index = mdl.layers.size()-1;	//if an edge or center in material, it is supposed to go to a layer
	
			myStream >> mdl.layers[index]->spacingboundary;
			mdl.layers[index]->spacingboundary = mdl.layers[index]->spacingboundary * 1e-7;	//convert nm to cm
		}

		findpos = tag.find("center");	//is this the a tag... - use the if to not confuse with alpha
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</center>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			int index = mdl.layers.size()-1;	//if an edge or center in material, it is supposed to go to a layer

			myStream >> mdl.layers[index]->spacingcenter;
			mdl.layers[index]->spacingcenter = mdl.layers[index]->spacingcenter * 1e-7;	//convert nm to cm
		}
		

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - material: "+tag);

	}	//end while file is good.

	ProgressString("Invalid XML Load file in material tag");

	return;
}

void LoadDefects(std::ifstream& file, Material* matl, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;

	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);	//make sure will find tag properly
		FindTag(line, tag, 0);		
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/defect");
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("acceptorlike");
		if(findpos == 0)
		{
			foundTag = true;
			Impurity* impur = new Impurity;
			impur->setDefectMaterial(matl);
			matl->defects.push_back(impur);	//push back the pointer. Relate this defect(held in class impurity) to the material
			mdl.impurities.push_back(impur);	//put the defect into the master list of impurities/defects.

			impur->LoadFromFile(file);
			impur->setAcceptor();	//flag the type of defect

		}

		findpos = tag.find("donorlike");
		if(findpos == 0)
		{
			foundTag = true;
			Impurity* impur = new Impurity;	//create the new defect
			impur->setDefectMaterial(matl);
			matl->defects.push_back(impur);	//push back the pointer. Relate this defect(held in class impurity) to the material
			mdl.impurities.push_back(impur);	//put the defect into the master list of impurities/defects.
			impur->LoadFromFile(file);
			impur->setDonor();	//flag the type of defect

		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - defect: "+tag);

	}

	ProgressString("Invalid XML Load file in defect tag");
	return;
}

void LoadOpticalChildren(std::ifstream& file, Optical* optics, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;
	size_t posL = 0;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	optics->a = 0.0;
	optics->b = 0.0;
	optics->useAB = false;
	optics->disableOpticalEmissions = false;
	optics->suppressOpticalOutputs = false;
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);	//make sure will find tag properly
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/optical");
		if(findpos == 0)	//reached the end of the optical data
		{
			foundTag = true;
			if (optics->opticEfficiency == -1.0)
				optics->opticEfficiency = 1.0;	//set by number, direct, or indirect. Not set at all=assumed set
			return;
		}

		findpos = tag.find("suppressoptout");
		if(findpos == 0)
		{
			foundTag = true;
			optics->suppressOpticalOutputs = true;
		}

		findpos = tag.find("disableemissions");
		if(findpos == 0)
		{
			optics->disableOpticalEmissions = true;
			foundTag = true;
		}

		findpos = tag.find("opticefficiency");	//is this the a tag... - use the if to not confuse with alpha
		if (findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</opticefficiency>");	//it should find this no problem
			int size = posR - next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> optics->opticEfficiency;

			if (optics->opticEfficiency > 1.0)
				optics->opticEfficiency = 1.0;
			else if (optics->opticEfficiency < 0.0)
				optics->opticEfficiency = 0.0;
		}


		findpos = tag.find("a");	//is this the a tag... - use the if to not confuse with alpha
		if(findpos == 0 && tag.size() < 3)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</a>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> optics->a;
			optics->useAB = true;
		}

		findpos = tag.find("b");	//is this the a tag...
		if(findpos == 0 && tag.size()<2)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</b>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> optics->b;
			optics->useAB = true;
		}

		findpos = tag.find("n");	//is this the n tag...
		if(findpos == 0 && tag.size()<2)	//extract the n data
		{
			foundTag = true;
			posR = line.find("</n>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> optics->n;
		}

		findpos = tag.find("wavelength");	//is this the wavelength tag...
		if(findpos == 0)	
		{
			foundTag = true;
			LoadMaterialNKData(file, optics);
			optics->useAB = false;
		}

		findpos = tag.find("alpha");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			double factor = 1.0;
			posL = tag.find("unit=\"");
			if(posL != std::string::npos)	//found a name tag
			{
				posL += 6;	//point to first part of unit
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the name in attribute
				if(attribute=="m-1")	//want to convert from ?? to inverse meters.
					factor = 1.0e-2;
				else if(attribute=="um-1")	//if the units are 1/um, multiply by (1e6um)/(1e2cm) to get proper units
					factor = 1.0e4;
				else if(attribute=="nm-1")
					factor = 1.0e7;
				else if(attribute=="A-1")
					factor = 1.0e8;
				else if(attribute=="cm-1")
					factor = 1.0;
				else if(attribute=="mm-1")
					factor = 10.0;
			}
			LoadAlphaChildren(file, optics, mdl, factor);
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - optical: "+tag);

	}	//end while file is still good
	ProgressString("Invalid XML Load file in optical tag");
	return;
}


void LoadMaterialNKData(std::ifstream& file, Optical* optics)
{
	std::string line;
	std::stringstream myStream;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	NK tmpNK;
	tmpNK.alpha = tmpNK.k = tmpNK.n = -1.0;
	float lambda=0.0;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/wavelength");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			optics->AddWavelength(lambda, tmpNK.n, tmpNK.k, tmpNK.alpha, true, false);
			/*
			if(lambda > 0.0 && tmpNK.k > 0.0 && tmpNK.alpha < 0.0)
			{
				tmpNK.alpha = 4.0e7 * PI * tmpNK.k / double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
				optics->WavelengthData.insert(std::pair<float, NK>(lambda, tmpNK));
			}
			else if(lambda > 0.0 && tmpNK.k < 0.0 && tmpNK.alpha > 0.0)
			{
				tmpNK.k = tmpNK.alpha * 2.5e-8 * PII * double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
				optics->WavelengthData.insert(std::pair<float, NK>(lambda, tmpNK));
			}
			else if(lambda < 0.0 && tmpNK.k > 0.0 && tmpNK.alpha > 0.0)
			{
				lambda = float(4.0e7 * PI * tmpNK.k / tmpNK.alpha);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
				optics->WavelengthData.insert(std::pair<float, NK>(lambda, tmpNK));
			}
			else if(lambda > 0.0 && tmpNK.k > 0.0 && tmpNK.alpha > 0.0)
			{
				double tmpAlpha = 4.0e7 * PI * tmpNK.k / double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
				if(fabs((tmpAlpha - tmpNK.alpha)/tmpNK.alpha) < 0.01)
					tmpNK.alpha = tmpAlpha;
				optics->WavelengthData.insert(std::pair<float, NK>(lambda, tmpNK));
			}
			*/
			return;
		}

		findpos = tag.find("lambda");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			
			posR = line.find("</lambda>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> lambda;
		}
		findpos = tag.find("n");	// index of refraction
		if(findpos == 0 && tag.size() < 2)	//extract the A data
		{	
			foundTag = true;
			posR = line.find("</n>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpNK.n;
		}

		findpos = tag.find("k");	// index of refraction
		if(findpos == 0 && tag.size() < 2)	//extract the A data
		{	
			foundTag = true;
			posR = line.find("</k>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpNK.k;
		}

		findpos = tag.find("alpha");	// index of refraction
		if(findpos == 0)	//extract the A data
		{	
			foundTag = true;
			posR = line.find("</alpha>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpNK.alpha;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - nk: "+tag);

	}	// end while

	ProgressString("Invalid XML Load file in wavelength(absorb) tag");
	return;
}


void LoadAlphaChildren(std::ifstream& file, Optical* optics, ModelDescribe& mdl, double factor)
{
	std::string line;
	std::stringstream myStream;
	size_t posL = 0;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	NK tmpNK;
	tmpNK.n = (float)optics->n;	//signal that this needs to be fixed and to use the material parameter index of refraction
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("/alpha");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("wavelength");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			float wavelength = 0;
			double alpha;
			posL = tag.find("nm=\"");
			if(posL != std::string::npos)	//should always find a nm tag
			{
				posL += 4;	//point to first part of wavelength
				posR = tag.find("\"", posL);	//find the ending quote
				int size= posR -posL;	//size of the attribute
				attribute = tag.substr(posL, size);	//put the wavelength in attribute
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> wavelength;
			}
			posR = line.find("</wavelength>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> alpha;

			tmpNK.alpha = alpha*factor;
			tmpNK.k = tmpNK.alpha * 0.25 * PII * wavelength * 1e-7;	//cancel out the cm-1 in alpha

			optics->WavelengthData.insert(std::pair<float, NK>(wavelength, tmpNK));	//add alpha data for the particular material
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - alpha: "+tag);

	}	// end while
	ProgressString("Invalid XML Load file in alpha tag");
	return;
}

void LoadWavelength(std::ifstream& file, std::map<double, Wavelength>& WvVect)
{
	std::string line;
	std::stringstream myStream;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	Wavelength tmpWave;	//create a wave to have data copied into
	tmpWave.lambda = 0.0;	//load up some default values - this wavelength is essentially nonexistent - infinite energy
	tmpWave.intensity = 0.0;
	//LIGHT_OLD_VARIABLES
//	tmpWave.collimation = 0;
//	tmpWave.coherence1 = 0.0;
//	tmpWave.coherence2 = 0.0;
	tmpWave.energyphoton = 0.0;
	tmpWave.cutOffPower = 0.0;
	tmpWave.IsCutOffPowerInit = false;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("wavelength");	//just useful to prevent error messages when done individually
		if (findpos == 0)
			foundTag = true;

		findpos = tag.find("/wavelength");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			if(tmpWave.energyphoton > 0.0 && tmpWave.intensity >= 0.0)	//make sure a valid wavelength/intensity was loaded
				MapAdd(WvVect,tmpWave.energyphoton,tmpWave);
//				WvVect.push_back(tmpWave);

			return;
		}

		findpos = tag.find("lambda");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</lambda>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpWave.lambda;
			if (tmpWave.lambda > 0.0)
				tmpWave.energyphoton = PHOTONENERGYCONST / double(tmpWave.lambda);
			else
				tmpWave.energyphoton = -1.0;	//prevent this from being added
		}
		findpos = tag.find("intensity");	// mW/cm^2
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</intensity>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpWave.intensity;
		}

		findpos = tag.find("collimation");	//spread of variation: 0-90 degrees
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</collimation>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
//LIGHT_OLD_VARIABLES
//			myStream >> tmpWave.collimation;

//			if(tmpWave.collimation < 0)
//				tmpWave.collimation = 0;
//			else if(tmpWave.collimation > PI)
//				tmpWave.collimation = PI;
		}

		findpos = tag.find("coher1");	//spread of variation: 0-90 degrees
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</coher1>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			//LIGHT_OLD_VARIABLES
//			myStream >> tmpWave.coherence1;

//			if(tmpWave.coherence1 < 0)
//				tmpWave.coherence1 = 0;
//			else if(tmpWave.coherence1 > PI)
//				tmpWave.coherence1 = PI;
		}

		findpos = tag.find("coher2");	//spread of variation: 0-90 degrees
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</coher2>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			//LIGHT_OLD_VARIABLES
//			myStream >> tmpWave.coherence2;

//			if(tmpWave.coherence2 < 0)
//				tmpWave.coherence2 = 0;
//			else if(tmpWave.coherence2 > PI)
//				tmpWave.coherence2 = PI;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - wavelength: "+tag);

	}	// end while
	ProgressString("Invalid XML Load file in wavelength(spectrum) tag");
	return;
}

void LoadTimeActive(std::ifstream& file, LightSource* spec)
{
	std::string line;
	std::stringstream myStream;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";

	double time = 0.0;
	bool enable = true;
	bool foundTag = false;
	
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/timeactive");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			spec->times.insert(std::pair<double, bool>(time, enable));
			return;
		}

		findpos = tag.find("time");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			posR = line.find("</time>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> time;
		}
		findpos = tag.find("enable");	// mW/cm^2
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			enable = true;
		}

		findpos = tag.find("disable");	//spread of variation: 0-90 degrees
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			enable = false;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - timeactive: "+tag);

	}	// end while
	ProgressString("Invalid XML Load file in spectrum time active tag");
	return;
}


bool LoadSpectrumChildren(std::ifstream& file, ModelDescribe& mdl, LightSource* spec, bool preventConversion)
{
	std::string line;
	std::stringstream myStream;
	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	spec->light.clear();
	spec->times.clear();
	spec->enabled = true;
	spec->sourceShape = NULL;
	spec->inputType = SPECTRUM_INTENSITY;	//default to mW/cm^2 - good for laser
	bool foundTag = false;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("spectrum");	//just prevent bad loads
		if (findpos == 0)
			foundTag = true;

		findpos = tag.find("/spectrum");
		if(findpos == 0)	//reached the end of the alpha data
		{
			foundTag = true;
			spec->IncidentPoints.clear();
			//now convert the spectrum from mW cm^-2 nm to mW cm^-2

			unsigned int szSpec = spec->light.size();
			if(szSpec == 0)
			{
				delete spec;	//there's no light, so this is a useless spectrum. Clear data, and don't add it to mdesc.spectrum
				return(false);
			}
			else if(szSpec == 1)
			{
				return(true);	//we are assuming the spectrum is a point source and the intensity is in mW/cm^2. The derivative is meaningless with only one value
			}

			if(spec->inputType == SPECTRUM_INTENS_DERIV && !preventConversion)
			{
				double deltaNM;
				// sort spectrum by wavelength
				std::map<double, Wavelength>::iterator it, next;
				it = spec->light.begin();
				next = spec->light.begin();
				if(next != spec->light.end())
					++next;
				for(; next != spec->light.end();)
				{
					deltaNM = fabs(double(next->second.lambda - it->second.lambda));
					it->second.intensity = it->second.intensity * deltaNM;
					
					it = next;
					if(next != spec->light.end())
						++next;
				}
				it->second.intensity = it->second.intensity * deltaNM;	//use previous delta
				spec->inputType = SPECTRUM_INTENSITY;	//it is no longer the derivative!
			}


			return(true);
		}


		findpos = tag.find("wavelength");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			LoadWavelength(file, spec->light);
		}

		findpos = tag.find("timeactive");	//is this the a tag...
		if(findpos == 0)	//extract the A data
		{
			foundTag = true;
			LoadTimeActive(file, spec);
		}
		
		findpos = tag.find("name");	//the spectrum's name
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</name>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute.size() > 19)
				attribute.resize(19);
			spec->name = attribute;
		}

		findpos = tag.find("id");	//the spectrum's id
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</id>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> spec->ID;
		}

		findpos = tag.find("position");	//the spectrum's id
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			spec->center = LoadXSpace(file, "position");
			spec->centerinit = true;
		}

		findpos = tag.find("direction");	//the spectrum's id
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			spec->direction = LoadXSpace(file, "direction");
			spec->direction.Normalize();
		}

		findpos = tag.find("shape");	//is this the id tag...
		if(findpos == 0)	//extract the id data
		{
			foundTag = true;
			Shape* shape = new Shape;
			if(spec->sourceShape != NULL)
			{
				delete spec->sourceShape;	//unallocate old stuff
			}
			spec->sourceShape = shape;
			LoadShapeChildren(file, shape);
			if(shape->getNumVertices() == 0)
			{
				//empty shape. Try and make the best of it - basically set up so code will place
				delete shape; //the source on the appropriate edge of the device so it shines on it
				spec->sourceShape = NULL;
			}
			else if (shape->getNumVertices() == 1)
			{
				spec->center = shape->start->pt;	//grab the point to get the position, and then treat it like an
				spec->centerinit = true;
			}
			else if (shape->getNumVertices() > 1)
			{
				spec->center = shape->center;
				spec->centerinit = true;
			}
		}
	
		findpos = tag.find("intensity");
		if(findpos == 0)
		{
			foundTag = true;
			spec->inputType = SPECTRUM_INTENSITY;
		}

		findpos = tag.find("wavederiv");
		if(findpos == 0)
		{
			foundTag = true;
			spec->inputType = SPECTRUM_INTENS_DERIV;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - spectrum: "+tag);

	}	// end while

	delete spec;	//bad spectrum file, clear memory and don't add it to model
	ProgressString("Invalid XML Load file in spectrum tag");
	return(false);
}

XSpace LoadGenericXSpace(std::ifstream& file, std::string closeTag)
{
	XSpace retval;
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	retval.x = 0.0;
	retval.y = 0.0;
	retval.z = 0.0;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find(closeTag);	//always check if need to leave first
		if(findpos == 0)	//end this sub section
		{
			foundTag = true;
			return(retval);	//done loading XSpace
		}

		findpos = tag.find("x");
		if(findpos == 0 && tag.size() < 3)	//don't accidentally include XSpace if called early
		{
			foundTag = true;
			posR = line.find("</x>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.x;
		}

		
		findpos = tag.find("y");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</y>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.y;
		}


		
		findpos = tag.find("z");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</z>");	//it should find this no problem
			int size = posR-next;
			attribute = line.substr(next, size);
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> retval.z;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - generic xspace: "+tag);

	}	//end while file good
	ProgressString("Invalid XML Load file in generic xyz tag");
	return(retval);
}

void Impurity::LoadFromFile(std::ifstream& file)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/impurity");	//or the end of the impurity data.
		if(findpos == 0)
		{
			adjustDonorDegeneracyForOccupationProbability();
			foundTag = true;
			return;	//stop loading this impurity
		}

		findpos = tag.find("impurity");	//just so this can be called on an individual load without displaying error message
		if (findpos == 0)
			foundTag = true;


		findpos = tag.find("acceptor");	//the impurity is acceptorlike
		if(findpos == 0)
		{
			foundTag = true;
			setAcceptor();
		}

		findpos = tag.find("donor");	//the impurity is donorlike
		if(findpos == 0)
		{
			foundTag = true;
			setDonor();
		}

		findpos = tag.find("type");	//what is the type
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</type>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if (attribute == "0")
				setAcceptor();
			else if (attribute == "1")
				setDonor();

		}	//end if found tag

		findpos = tag.find("distribution");	//what is the distribution
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</distribution>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if (attribute == "discrete")
				setEnergyDiscrete();
			else if (attribute == "banded")
				setEnergyBanded();
			else if (attribute == "gaussian")
				setEnergyGaussian();
			else if (attribute == "exponential")
				setEnergyExponential();
			else if (attribute == "linear")
				setEnergyLinear();
			else if (attribute == "parabolic")
				setEnergyParabolic();
			else
			{
				myStream.clear();
				myStream.str("");
				myStream << attribute;
				myStream >> distribution;
				setEnergyDistribution(distribution);	//verify it by resetting it
			}
		}	//end if found tag

		findpos = tag.find("density");	//what is the density
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</density>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> density;

		}	//end if found tag

		findpos = tag.find("energy");	//what is the energy level
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</energy>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> energy;
		}	//end if found tag

		findpos = tag.find("degeneracy");	//what is the degeneracy
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</degeneracy>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> degeneracy;
			setDegeneracy(degeneracy);
		}	//end if found tag

		findpos = tag.find("width");	//what is the energy width
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</width>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> energyvar;
		}	//end if found tag

		findpos = tag.find("depth");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</depth>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if (attribute == "shallow")
				setShallow();
			else if (attribute == "deep")
				setDeep();
			else if (attribute == "0")
				setShallow();
			else if (attribute == "1")
				setDeep();
			else if (attribute == "true")
				setDeep();
			else
				setShallow();	//shallow - more dopants are shallow, so default to this
		}

		findpos = tag.find("sign");	//what is the electron capture velocity
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sign>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sigman;
			setElecCapture(sigman);	//verify by resetting it
		}	//end if found tag

		findpos = tag.find("sigp");	//what is the hole capture velocity
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sigp>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> sigmap;
			setHoleCapture(sigmap);	//verify by resetting it
		}	//end if found tag

		
		findpos = tag.find("id");	//what is the id of the defect?
		if(findpos == 0 && tag.size() < 3)
		{
			foundTag = true;
			posR = line.find("</id>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> ID;
		}	//end if found tag

		findpos = tag.find("charge");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</charge>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> charge;
		}	//end if found tag

		findpos = tag.find("cbopticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</cbopticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> OpticalEfficiencyCB;
			setOpticalEfficiencyCB(OpticalEfficiencyCB);

		}	//end if found tag

		findpos = tag.find("vbopticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</vbopticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> OpticalEfficiencyVB;
			setOpticalEfficiencyVB(OpticalEfficiencyVB);
			
		}	//end if found tag

		findpos = tag.find("opticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</opticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			double tmp;
			myStream >> tmp;
			setOpticalEfficiencyCB(tmp);
			setOpticalEfficiencyVB(tmp);
		}	//end if found tag


		
		findpos = tag.find("name");	//is this the impurity's name
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</name>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			setName(attribute);
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - impurity: "+tag);
	}	//end while file is good
	ProgressString("Invalid XML Load file in impurity tag");
	return;
}
/*
void LoadDefectChildren(std::ifstream& file, Impurity* impur, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);
		if(tag=="")
			foundTag = true;

		findpos = tag.find("acceptorlike");	//for loading individually
		if (findpos == 0)
		{
			foundTag = true;
			impur->reference = ACCEPTOR;
		}
		findpos = tag.find("donorlike");	//for loading individually
		if (findpos == 0)
		{
			foundTag = true;
			impur->reference = DONOR;
		}

		findpos = tag.find("/acceptorlike");	//end of the defect data.
		if(findpos == 0)
		{
			foundTag = true;
			if(impur->OpticalEfficiencyVB==-1.0)
				impur->OpticalEfficiencyVB = 1.0;	//default to on at full power
			if(impur->OpticalEfficiencyCB==-1.0)
				impur->OpticalEfficiencyCB = 1.0;

			impur->reference = ACCEPTOR;

			if (impur->degeneracy == 0.0f)
				impur->degeneracy = 1.0f;

			//this should seriously NEVER happen
	//		if (impur->reference == DONOR)	//fermi level statistics to allow a factor multiplied by to get e- concentration
	//			impur->degeneracy = float(1.0) / impur->degeneracy; //in a donor, the degeneracy is used to calculate
			return;	//stop loading this defect
		}

		findpos = tag.find("/donorlike");	//end of this defect data
		if(findpos == 0)
		{
			foundTag = true;
			if(impur->OpticalEfficiencyVB==-1.0)
				impur->OpticalEfficiencyVB = 1.0;	//default to on at full power
			if(impur->OpticalEfficiencyCB==-1.0)
				impur->OpticalEfficiencyCB = 1.0;

			if (impur->degeneracy == 0.0f)
				impur->degeneracy = 1.0f;

			impur->reference = DONOR;

			//this should ALWAYS be true
//			if (impur->reference == DONOR)	//fermi level statistics to allow a factor multiplied by to get e- concentration
			impur->degeneracy = float(1.0) / impur->degeneracy; //in a donor, the degeneracy is used to calculate

			return;	//so stop loading this defect
		}

		findpos = tag.find("type");	//what is the type
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</type>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute=="0")
				impur->reference = ACCEPTORLIKE;
			else if(attribute=="1")
				impur->reference = DONORLIKE;

		}	//end if found tag

		findpos = tag.find("degeneracy");	//what is the degeneracy
		if (findpos == 0)
		{
			foundTag = true;
			posR = line.find("</degeneracy>", next);	//find the ending tag
			int size = posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->degeneracy;
		}	//end if found tag

		findpos = tag.find("distribution");	//what is the distribution
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</distribution>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->distribution;
		}	//end if found tag

		findpos = tag.find("density");	//what is the density
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</density>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->density;
		}	//end if found tag

		findpos = tag.find("energy");	//what is the energy level
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</energy>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->energy;
		}	//end if found tag

		findpos = tag.find("width");	//what is the energy width
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</width>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->energyvar;
		}	//end if found tag

		findpos = tag.find("charge");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</charge>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->charge;
		}	//end if found tag

		findpos = tag.find("depth");
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</depth>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute == "shallow")
				impur->depth = SHALLOW;
			else if(attribute == "deep")
				impur->depth = DEEP;
			else if(attribute == "0")
				impur->depth = false;
			else if(attribute == "1")
				impur->depth = true;
			else if(attribute == "true")
				impur->depth = true;
			else
				impur->depth = false;	//shallow - more dopants are shallow, so default to this
		}

		findpos = tag.find("sign");	//what is the electron capture velocity
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sign>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->sigman;
		}	//end if found tag

		findpos = tag.find("sigp");	//what is the hole capture velocity
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</sigp>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->sigmap;
		}	//end if found tag

		
		findpos = tag.find("id");	//what is the id of the defect?
		if(findpos == 0 && tag.size()<4)
		{
			foundTag = true;
			posR = line.find("</id>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->ID;
		}	//end if found tag

		findpos = tag.find("cbopticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</cbopticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->OpticalEfficiencyCB;
			
			if(impur->OpticalEfficiencyCB > 1.0)
				impur->OpticalEfficiencyCB = 1.0;
			else if(impur->OpticalEfficiencyCB < 0.0)
				impur->OpticalEfficiencyCB = 0.0;
		}	//end if found tag

		findpos = tag.find("vbopticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</vbopticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->OpticalEfficiencyVB;

			if(impur->OpticalEfficiencyVB > 1.0)
				impur->OpticalEfficiencyVB = 1.0;
			else if(impur->OpticalEfficiencyVB < 0.0)
				impur->OpticalEfficiencyVB = 0.0;
		}	//end if found tag

		findpos = tag.find("opticefficiency");	//what is the id of the defect?
		if(findpos == 0)
		{
			foundTag = true;
			posR = line.find("</opticefficiency>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			double tmp;
			myStream >> tmp;
			if(tmp > 1.0)
				tmp = 1.0;
			else if(tmp < 0.0)
				tmp = 0.0;
			if(impur->OpticalEfficiencyCB==-1.0)
				impur->OpticalEfficiencyCB = tmp;
			if(impur->OpticalEfficiencyVB==-1.0)
				impur->OpticalEfficiencyVB = tmp;
		}	//end if found tag
		
		findpos = tag.find("matl");	//what is the material making up the defect?
		if(findpos == 0)	//kind of irrelevant...
		{
			foundTag = true;
			posR = line.find("</matl>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> impur->matl;
		}	//end if found tag

		findpos = tag.find("name");	//what is the material making up the defect?
		if(findpos == 0)	//kind of irrelevant...
		{
			foundTag = true;
			posR = line.find("</name>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			impur->name = line.substr(next, size);	//put the type in attribute
		}	//end if found tag
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - defect: "+tag);
	}	//end while file is good
	ProgressString("Invalid XML Load file in donor/acceptor-like tag");
	return;
}
*/
void LoadCustomDOS(std::ifstream& file, std::map<double, CustomDensStates>& band)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	CustomDensStates tmpVal;
	tmpVal.density = 0.0;
	tmpVal.EFMCond = 1.0;
	double energy = 0.0;

	band.clear();	//make sure it starts out fresh

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);	//next points to one after >
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/doscb");
		if(findpos == 0)	//reached the end of the conduction band density of states
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("/dosvb");
		if(findpos == 0)	//reached the end of the valence band density of states
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("state");	//just resets the numbers, but it shouldn't really do much anything else
		if(findpos == 0)	//reached the end of the valence band density of states
		{
			foundTag = true;
			tmpVal.density = 0.0;
			tmpVal.EFMCond = 0.0;
			energy = 0.0;
		}

		findpos = tag.find("/state");
		if(findpos == 0)	//reached the end of the valence band density of states
		{
			foundTag = true;
			if(tmpVal.density > 0.0)
				band.insert(std::pair<double, CustomDensStates>(energy, tmpVal));
		}

		findpos = tag.find("energy");	//is this the band gap tag...
		if(findpos == 0)	//extract the band gap data
		{
			foundTag = true;
			posR = line.find("</energy>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> energy;
		}

		findpos = tag.find("density");	//is this the band gap tag...
		if(findpos == 0)	//extract the band gap data
		{
			foundTag = true;
			posR = line.find("</density>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpVal.density;
		}

		findpos = tag.find("efm");	//is this the band gap tag...
		if(findpos == 0)	//extract the band gap data
		{
			foundTag = true;
			posR = line.find("</efm>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> tmpVal.EFMCond;
		}
		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - custom dos: "+tag);
	}

	ProgressString("Invalid XML Load file for custom density of states");
	return;
}

void LoadElectricalChildren(std::ifstream& file, Material* matl, ModelDescribe& mdl)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	double tmp;
	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);	//next points to one after >
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/electric");
		if(findpos == 0)	//reached the end of the electric data
		{
			foundTag = true;
			return;
		}

		findpos = tag.find("dielectric");	//is this the dielectric tag...
		if(findpos == 0)	//extract the dielectric data
		{
			foundTag = true;
			posR = line.find("</dielectric>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->epsR;
		}

		findpos = tag.find("eg");	//is this the band gap tag...
		if(findpos == 0)	//extract the band gap data
		{
			foundTag = true;
			posR = line.find("</eg>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->Eg;
		}

		findpos = tag.find("affinity");	//is this the electron affinity tag...
		if(findpos == 0)	//extract the e- affinity data
		{
			foundTag = true;
			posR = line.find("</affinity>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->Phi;
		}

		findpos = tag.find("nv");	//is this the DOS at valence edge tag...
		if(findpos == 0)	//extract the data
		{
			foundTag = true;
			posR = line.find("</nv>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->Nv;
		}

		findpos = tag.find("nc");	//is this the DOS at conduction edge tag...
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</nc>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->Nc;
		}

		findpos = tag.find("nd");	//is this the DOS at conduction edge tag...
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</nd>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;

			if(attribute != "" && attribute != "0")
			{
				//now go crazy with loading stuff...
				Impurity* impur = new Impurity;
				mdl.impurities.push_back(impur);
				matl->defects.push_back(impur);	//this is specific to the material, so treat as impurity
				impur->setDonor();
				impur->setShallow();
				impur->setEnergyDiscrete();
				impur->setRelativeEnergy(1.0e-4);
				impur->setEnergyVariation(0.0);
				impur->setDefaultDegeneracy();
				impur->testDefaultCharge();
				impur->setElecCapture(1.0e-12);
				impur->setHoleCapture(1.0e-18);

				myStream >> tmp;
				impur->setDensity(tmp);	//load the desired density
			}
		}

		findpos = tag.find("na");	//is this the DOS at conduction edge tag...
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</na>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			if(attribute != "" && attribute != "0")
			{
				//now go crazy with loading stuff...
				Impurity* impur = new Impurity;
				mdl.impurities.push_back(impur);
				matl->defects.push_back(impur);	//this is specific to the material, so treat as impurity
				impur->setAcceptor();
				impur->setShallow();
				impur->setEnergyDiscrete();
				impur->setRelativeEnergy(1.0e-4);
				impur->setEnergyVariation(0.0);
				impur->setDefaultDegeneracy();
				impur->testDefaultCharge();
				impur->setElecCapture(1.0e-18);
				impur->setHoleCapture(1.0e-12);

				myStream >> tmp;
				impur->setDensity(tmp);	//load the desired density
			}
		}

		findpos = tag.find("mobility_n");	//is this the electron mobility tag...
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</mobility_n>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->MuN;
		}
		
		findpos = tag.find("mobility_p");	//is this the hole mobility tag...
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</mobility_p>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->MuP;
		}

		findpos = tag.find("efmec");	//is this the e-'s effective mass
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</efmec>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->EFMeCond;
		}

		findpos = tag.find("efmhc");	//is this the hole's effective mass
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</efmhc>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->EFMhCond;
		}

		findpos = tag.find("efmed");	//is this the e-'s effective mass
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</efmed>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->EFMeDens;
		}

		findpos = tag.find("efmhd");	//is this the hole's effective mass
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</efmhd>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->EFMhDens;
		}

		findpos = tag.find("doscb");	//load a custum density of states for the conduction band
		if(findpos == 0)
		{
			foundTag = true;
			LoadCustomDOS(file, matl->CBCustom);
		}

		findpos = tag.find("dosvb");	//load a custum density of states for the conduction band
		if(findpos == 0)
		{
			foundTag = true;
			LoadCustomDOS(file, matl->VBCustom);
		}

		
		findpos = tag.find("ni");	//is this the intrinsic carrier concentration
		if(findpos == 0 && tag.size()<4)	//extract the  data - ( <4 - don't confuse with affiNIty)
		{
			foundTag = true;
			posR = line.find("</ni>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->ni;
		}

		findpos = tag.find("thermalgenrectau");	//is this the intrinsic carrier concentration
		if(findpos == 0)	//extract the  data - ( <4 - don't confuse with affiNIty)
		{
			foundTag = true;
			posR = line.find("</thermalgenrectau>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			myStream >> matl->tauThGenRec;
			matl->tauThGenRec = (matl->tauThGenRec > 0.0) ? 1.0/matl->tauThGenRec : 0.0;
		}

		
		findpos = tag.find("type");	//is this the e-'s effective mass
		if(findpos == 0)	//extract the  data
		{
			foundTag = true;
			posR = line.find("</type>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			if(attribute=="metal")
				matl->type = METAL;
			else if(attribute=="semiconductor")
				matl->type = SEMICONDUCTOR;
			else if(attribute=="dielectric")
				matl->type = DIELECTRIC;
			else if(attribute=="vacuum")
				matl->type = VACUUM;
			else
				matl->type = UNDEF;
		}

		findpos = tag.find("urbachcb");	//is this the intrinsic carrier concentration
		if(findpos == 0)	//extract the  data - ( <4 - don't confuse with affiNIty)
		{
			foundTag = true;
			LoadUrbachChildren(file, matl, CONDUCTION);
		}

		findpos = tag.find("urbachvb");	//is this the intrinsic carrier concentration
		if(findpos == 0)	//extract the  data - ( <4 - don't confuse with affiNIty)
		{
			foundTag = true;
			LoadUrbachChildren(file, matl, VALENCE);
		}

		findpos = tag.find("direct");
		if(findpos == 0)
		{
			foundTag = true;
			matl->direct = true;
			if(matl->optics && matl->optics->opticEfficiency == -1.0)
				matl->optics->opticEfficiency=1.0;
		}

		findpos = tag.find("indirect");
		if(findpos == 0)
		{
			foundTag = true;
			matl->direct = false;
			if(matl->optics && matl->optics->opticEfficiency == -1.0)
				matl->optics->opticEfficiency=0.0;
		}

		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - electrical: "+tag);
	}
	ProgressString("Invalid XML Load file in material's electrical tag");
	return;
}

void LoadUrbachChildren(std::ifstream& file, Material* matl, bool band)
{
	std::string line;
	std::stringstream myStream;

	size_t posR = 0;
	size_t next = 0;
	size_t findpos = 0;
	std::string tag="";
	std::string attribute="";
	bool foundTag = false;
	if(band==CONDUCTION)
	{
		matl->urbachCB.band = CONDUCTION;
		matl->urbachCB.captureCB = 0.0;
		matl->urbachCB.captureVB = 0.0;
		matl->urbachCB.energy = 0.0;
	}
	else
	{
		matl->urbachVB.band = VALENCE;
		matl->urbachVB.captureCB = 0.0;
		matl->urbachVB.captureVB = 0.0;
		matl->urbachVB.energy = 0.0;
	}

	while(file.good())
	{
		foundTag = false;
		getline(file, line);
		ToLower(line);
		next = FindTag(line, tag, 0);	//next points to one after >
		if(tag=="")
			foundTag = true;
		findpos = tag.find("/urbachcb");
		if(findpos == 0)	//reached the end of the electric data
		{
			matl->urbachCB.band = CONDUCTION;
			foundTag = true;
			return;
		}
		findpos = tag.find("/urbachvb");
		if(findpos == 0)	//reached the end of the electric data
		{
			matl->urbachVB.band = VALENCE;
			foundTag = true;
			return;
		}

		findpos = tag.find("energy");	//is this the dielectric tag...
		if(findpos == 0)	//extract the dielectric data
		{
			foundTag = true;
			posR = line.find("</energy>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			if(band==CONDUCTION)
			{
				myStream >> matl->urbachCB.energy;
				if(matl->urbachCB.energy <= 0.0)
				{
					ProgressString("Invalid urbach energy for conduction band tails. Disabled.");
					matl->urbachCB.energy = 0.0;
				}
			}
			else
			{
				myStream >> matl->urbachVB.energy;
				if(matl->urbachVB.energy <= 0.0)
				{
					ProgressString("Invalid urbach energy for conduction band tails. Disabled.");
					matl->urbachVB.energy = 0.0;
				}
			}
		}

		findpos = tag.find("sign");	//is this the band gap tag...
		if(findpos == 0)	//extract the band gap data
		{
			foundTag = true;
			posR = line.find("</sign>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			if(band==CONDUCTION)
			{
				myStream >> matl->urbachCB.captureCB;
				if(matl->urbachCB.captureCB <= 0.0)
				{
					ProgressString("Invalid urbach capture cross section for conduction band tails to CB. Disabled.");
					matl->urbachCB.captureCB = 0.0;
				}
			}
			else
			{
				myStream >> matl->urbachVB.captureCB;
				if(matl->urbachVB.captureCB <= 0.0)
				{
					ProgressString("Invalid urbach capture cross section for valence band tails to CB. Disabled.");
					matl->urbachVB.captureCB = 0.0;
				}
			}
		}

		findpos = tag.find("sigp");	//is this the electron affinity tag...
		if(findpos == 0)	//extract the e- affinity data
		{
			foundTag = true;
			posR = line.find("</sigp>", next);	//find the ending tag
			int size= posR - next;	//size of the attribute
			attribute = line.substr(next, size);	//put the type in attribute
			myStream.clear();
			myStream.str("");
			myStream << attribute;
			if(band==CONDUCTION)
			{
				myStream >> matl->urbachCB.captureVB;
				if(matl->urbachCB.captureVB <= 0.0)
				{
					ProgressString("Invalid urbach capture cross section for conduction band tails to VB. Disabled.");
					matl->urbachCB.captureVB = 0.0;
				}
			}
			else
			{
				myStream >> matl->urbachVB.captureVB;
				if(matl->urbachVB.captureVB <= 0.0)
				{
					ProgressString("Invalid urbach capture cross section for valence band tails to VB. Disabled.");
					matl->urbachVB.captureVB = 0.0;
				}
			}
		}


		if(foundTag == false)
			ProgressString("Unrecognized tag in load file - Urbach energy: "+tag);
	}
	ProgressString("Invalid XML Load file in material's urbach tag");
	return;
}



LightSource LoadSpectrum(std::string filename)	//send the spectrum file in without the 
{
	std::fstream fspectrum;
	filename.append(".spe");
	fspectrum.open(filename.c_str(), std::fstream::in);
	if(fspectrum.is_open() == false)
	{
		LightSource ptr;
		ptr.enabled = false;
		ptr.IncidentPoints.clear();
		ptr.light.clear();
		ptr.name="Invalid Spectrum file";
		ptr.times.insert(std::pair<double, bool>(0.0,false));
		return(ptr);
	}

	int isnum = fspectrum.peek();
	char temp[256];	//used for getting through all the lines at the beginning
	while(isnum < '0' && isnum > '9' && fspectrum.eof() == false)
	{
		fspectrum.getline(temp, 256);
		isnum = fspectrum.peek();
	}
	//now it's at the actual data.
	LightSource NewSpectrum;	//vector's are already put on the heap! No need for new command.
	NewSpectrum.enabled = true;
	NewSpectrum.name="DefaultSpecName";
	NewSpectrum.times.insert(std::pair<double, bool>(0.0,false));
	NewSpectrum.IncidentPoints.clear();
	NewSpectrum.direction.x = 1.0;
	NewSpectrum.direction.y = NewSpectrum.direction.z = 0.0;
	NewSpectrum.sourceShape = NULL;
	NewSpectrum.light.clear();
	NewSpectrum.center.clear();
	NewSpectrum.centerinit = false;

	
	do
	{
		Wavelength tmpWave;
		//LIGHT_OLD_VARIABLES
//		tmpWave.collimation = 0;
//		tmpWave.coherence1 = tmpWave.coherence2 = 0.0;
		tmpWave.IsCutOffPowerInit = false;
		fspectrum >> tmpWave.lambda >> tmpWave.intensity;
		tmpWave.energyphoton = PHOTONENERGYCONST / double(tmpWave.lambda);
		MapAdd(NewSpectrum.light, tmpWave.energyphoton, tmpWave);
	}while(fspectrum.eof() == false);
	
	fspectrum.close();

	return(NewSpectrum);	//returns the vector object containing the desired spectrum
}


size_t FindTag(std::string input, std::string& output, size_t start)
{
	size_t posL = start;
	size_t posR = start;
	posL = input.find("<", start);
	if(posL != std::string::npos)
	{
		posR = input.find(">");
		int size = posR-posL-1;	//the size contents of the tag		
		output = input.substr(posL+1, size);	//make the tag a separate piece
	}
	else
		output = "";
	return(posR+1);

}