#include "basis.h"
#include <cmath>


Basis::Basis(XSpace dif)
{
	basis1 = dif;
	basis3.x = 0;
	basis3.y = 0;
	SetOrthogonal();
	Normalize();
}
Basis::~Basis()
{
	basis1.x = 0;
	basis1.y = 0;
	basis2.x = 0;
	basis2.y = 0;
	basis3.x = 0;
	basis3.y = 0;
}
Basis::Basis()
{
	basis1.x = 0;
	basis1.y = 0;
	basis2.x = 0;
	basis2.y = 0;
	basis3.x = 0;
	basis3.y = 0;
}
void Basis::SetBasis(XSpace pos)
{
	basis1 = pos;
	double mag = basis1.Magnitude();	//must normalize right away
	if(mag != 0.0)	//or the direction will be lost in sig digits
	{
		basis1.x = basis1.x / mag;
		basis1.y = basis1.y / mag;
	}

	basis3.x = 0;
	basis3.y = 0;
	SetOrthogonal();
	Normalize();
	return;
}

void Basis::SetOrthogonal(void)
{
	if(basis1.y == 0)
	{
		basis2.y = 1.0;	//have the second one be vertical
		basis2.x = 0.0;
	}
	else if(basis1.x == 0)	//the vector was horizontal
	{
		basis2.x = 1.0;
		basis2.y = 0.0;
	}
	else	//not vertical or horizontal
	{
		basis2.y = -basis1.x/basis1.y;	//new slope of basis, move along x by 1, so this is new y;
		basis2.x = 1.0;
	}


	return;
}

void Basis::Normalize(void)
{
	double mag = basis1.Magnitude();
	if(mag != 0.0)
	{
		basis1.x = basis1.x / mag;
		basis1.y = basis1.y / mag;
	}
	//else both x and y are zero

	mag = basis2.Magnitude();
	if(mag != 0.0)
	{
		basis2.x = basis2.x / mag;
		basis2.y = basis2.y / mag;
	}
}


//transofrms a position in (x,y) into (bas1,bas2)
XSpace Basis::ReturnInBasis(XSpace pos)
{
	XSpace solution;	//defaults to 0,0
	//solution.x is how much to multiply by basis1. think alpha
	//solution.y is how much to multiply by basis2. think beta
	if(basis1.x == 0.0)	//avoid many divide by zeroes... This basis has no effect on the x coordinate
	{
		if(basis2.x == 0.0)	//the two basis are equivalent, being along the y direction only
		{
			if(pos.x == 0.0)	//could possibly work if it's over the origin
			{
				if(basis1.y != 0)
				{
					solution.x = pos.y/basis1.y;
				}
				else if(basis2.y != 0)
				{
					solution.y = pos.y/basis2.y;
				}
			}
			//otherwise indicate failure by returning (0,0)
			return(solution);
		}
		//so in this position, 1x=0, and 2x != 0
		//therefore all the x must occur in 2nd basis, or sol.y
		solution.y = pos.x / basis2.x;
		//this sets 2y's value
		//alpha * b1.y + beta * b2.y = y
		double y = solution.y * basis2.y;
		y = pos.y - y;
		if(basis1.y != 0)
			solution.x = y / basis1.y;
		else	//basis 1 is (0,0)
		{
			if(fabs(y) < EQUIVZERO_POSITION)	//it already happens to lie on the line
			{
				solution.x = 0;	//stick with the y solution, but make sure first basis is 0'd.
			}
			else	//cannot reach this point in this basis
			{
				solution.y = 0;	//so signify that it can't be reached.
				solution.x = 0;
			}
		}
		return(solution);
	}

	//we know the first basis has a legitimate x value (a1 in notes)
	//now make sure the two basis aren't parallel (would cause divide by zero)
	if(fabs(basis2.y - basis1.y * basis2.x / basis1.x) < EQUIVZERO_POSITION)
	{	//they are parallel, so check and see if basis1 will reach the point. Ignore basis2
		//check to see if one will work... know basis1.x is a valid number, so work
		solution.x = pos.x / basis1.x;
		if(fabs(solution.x * basis1.y - pos.y) < EQUIVZERO_POSITION)
		{	//success
			solution.y = 0;
			return(solution);
		}
		else
		{	//failed
			solution.x = 0.0;
			solution.y = 0.0;
			return(solution);
		}
	}

	//now know that basis1 has an x component, and the two basis are not parallel.
	//therefore, the long icky equation will work
	//aka, there will be no divide by zero's in these equations

	double num = pos.y - basis1.y * (pos.x / basis1.x);
	double den = basis2.y - basis1.y * (basis2.x / basis1.x);

	solution.y = num / den;
	solution.x = pos.x - solution.y * basis2.x;
	solution.x = solution.x / basis1.x;

	return(solution);
}

XSpace Basis::ReturnBasis(int num)
{
	if(num == 1)
		return(basis1);
	if(num == 2)
		return(basis2);
	return(basis3);
}