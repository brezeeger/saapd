#ifndef SAAPD_FINDELEVELSS
#define SAAPD_FINDELEVELSS

#define F_E_SS_MAXOCC true
#define F_E_SS_MINOCC false
#define F_E_SS_POSITIVE true
#define F_E_SS_NEGATIVE false

#define F_E_SS_STOPSEARCH 0
#define F_E_SS_SEARCHMAG 1
#define F_E_SS_SEARCHREFINE 2

class ELevel;
class Simulation;
class Model;

int FindELevelSS(ELevel& eLev, Simulation* sim, Model& mdl, double refinement = -1.0);

#endif