#ifndef ADAPTIVEMESH_H
#define ADAPTIVEMESH_H

#include <map>
#include <vector>


#define TARGETPSI (0.1875 * KB)	//about 0.2, but a clean double... Multiply by kT to see the allowed difference in Psi between two points
#define MERGEPSI (0.015625 * KB)	//multiply by kT to see when these points should be merged together
#define SPLITPSI (0.5 * KB)		//multiply by kT to see when these points should be split apart.
#define EDGEPOINT 1		//used to prevent points from being deleted - this point position cannot be altered
#define BOUNDARYPOINT 2	//this type of point is added
#define TEMPPOINT 3	//these are the filler points and may be removed
#define POINTSREPLACED 1
#define POINTSNOTREPLACED 0
#define MINPOSITIONCHANGE 1.0e-8	//1 angstrom ~ 1 atom.

//forward declarations
//model.h
class Model;
class ModelDescribe;
class Point;
class Simulation;
class ELevel;

int AdaptiveMesh(Model& mdl, ModelDescribe& mdesc);
int CheckAdjustMesh(Model& mdl, ModelDescribe& mdesc);
bool CompareLayers(Point* testPt, Point* trgPt, ModelDescribe& mdesc);
bool ComparePsi(ModelDescribe& mdesc, double SplitCutoff, double MergeCutoff, Point* testPt, Point* trgPt,  std::map<long int, int>&  MergeSplit);
int ReplaceID(int FindID, int ReplaceWith, std::map<long int, int>&  MergeSplit);
int ReplacePoints(Model& mdl, ModelDescribe& mdesc, std::map<long int, int>& BadPoints, std::vector<Point*>& AllReplacedPoints, bool merge);
int CopyPointData(Point* NewPt, Point* OldPt, Simulation* sim, double pointOverlap);
int ZeroOutOccupancy(Point* pt);
double CalcPointPercentageOverlap(Point* NewPt, Point* OldPt);
double CheckELevOverlap(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt);
int CopyELevel(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt, double pointOverlap);
int InitializeNewPoints(ModelDescribe& mdesc, Model& mdl, std::vector<Point*> CurrentPoints, std::vector<Point*> ReplacedPoints, double temp);
int PopulateCurrentRange(Model& mdl, std::vector<Point*>& currentPts, int ID, std::map<long int, int>& BadPoints, bool ignoreID);
int RenumberPoints(Model& mdl, ModelDescribe& mdesc, std::vector<Point*> AllReplacedPoints);
int ClearContacts(std::vector<Point*> AllReplacedPoints);
int GetAvgFermi(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt, double pointOverlap, double& TotFermi, double& TotOverlap);
int RemovedInvalidReplacedPoints(std::vector<Point*>& AllReplacedPoints, Point* Search);




#endif