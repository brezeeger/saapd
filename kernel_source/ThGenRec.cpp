#include "ThGenRec.h"

#include "BasicTypes.h"
#include "model.h"
#include "RateGeneric.h"
#include "physics.h"
#include "light.h"
#include "transient.h"

double CalcGenRateFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* accuRate)
{
	if(srcE == NULL || trgE == NULL)
		return(0.0);
	
	Point* pt = &srcE->getPoint();

	double sourceHole, sourceElec, targetElec, targetAvail, nSource, nTarget;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetAvail);
	nSource = srcE->GetNumstates();
	nTarget = trgE->GetNumstates();

	double beta = KBI / pt->temp;

	double delE = -(pt->eg + srcE->eUse + trgE->eUse);
	//but better make sure it is just in case the bands overlap and it's a metal!
	if(delE > 0.0)
		delE = -delE;
	delE = exp(beta * delE);
	double difsq = sourceHole - targetElec;
	difsq = difsq * difsq;	//essentially (n-p)^2, alpha = 1/tau/sqrt [ (n-p)^2 + 4ni^2 ]
	double sqrtterm = difsq + 4.0 * pt->niCarrier * pt->niCarrier;
	sqrtterm = sqrt(sqrtterm);

	double taui = pt->MatProps->tauThGenRec > 0.0 ? pt->MatProps->tauThGenRec : 0.5 * (srcE->taui + trgE->taui);

	double alpha = sqrtterm!=0.0 ? taui / sqrtterm:0.0;	//generation typically alpha n_i^2 at equilibrium
	//equivalent to Nc Nv e_-delE)
	//further generalized as Nc is mostly empty and Nv is mostly full
	//from the number of empty states in CB and number of elec in VB

	double gen, recomb;
	if(accuRate == NULL)
	{
		if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
		{
			gen = targetAvail * sourceElec / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
			gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			gen = exp(gen);
		}
		else if(delE == 0.0)
		{
			gen = log(alpha * targetAvail * sourceElec)-(pt->eg + srcE->eUse + trgE->eUse)*beta;
			gen=exp(gen);
		}
		else
		{
			gen = alpha * targetAvail * sourceElec * delE;
		}

		if(targetElec==0.0 || sourceHole == 0.0)
		{
			recomb = 0.0;
		}
		else if(sqrtterm == 0.0)
		{
			recomb = targetElec * sourceHole / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
			recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			recomb = exp(recomb);
		}
		else
		{
			recomb = alpha * targetElec * sourceHole;	//alpha * p * n (* vol^2)
		}
	}
	else
	{
		//calculating the gen term by term
		//the question becomes does targetElec or sourceHole need to be split into multiple terms
		//it must be close to numStates so the accuracy is lost.
		bool splitTarget = (targetAvail > targetElec && targetElec * targetAvail <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceElec > sourceHole && sourceElec * sourceHole <= nSource * nSource * 0.0009765625);	//just shift exponent bits

		// (c/n) * (a/n) = 0.5 max

		if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
		{
			gen = targetAvail * sourceElec / sqrt(4.0 * nSource * nTarget);
			gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			gen = exp(gen);
			accuRate->AddValue(gen);
		}
		else if(delE == 0.0)	//try to decrease the exponent by the other stuff
		{
			gen = log(alpha * targetAvail * sourceElec) -(pt->eg + srcE->eUse + trgE->eUse)*beta;
			gen = exp(gen);
			accuRate->AddValue(gen);
		}
		else 
		{
			if(!splitTarget && !splitSource)
			{
				gen = alpha * targetAvail * sourceElec * delE;
				accuRate->AddValue(gen);
			}
			else if(splitTarget && splitSource)
			{
				gen = alpha * delE;
				accuRate->AddValue(gen * nTarget * nSource);
				accuRate->SubtractValue(gen * nTarget * sourceHole);
				accuRate->SubtractValue(gen * targetElec * nSource);
				accuRate->AddValue(gen * targetElec * sourceHole);
				gen = gen * targetAvail * sourceElec;
			}
			else if(splitTarget)
			{
				gen = alpha * sourceElec * delE;
				accuRate->AddValue(gen * nTarget);
				accuRate->SubtractValue(gen * targetElec);
				gen = gen * targetElec;
			}
			else//if(splitSource)
			{
				gen = alpha * targetAvail * delE;
				accuRate->AddValue(gen * nSource);
				accuRate->SubtractValue(gen * sourceHole);
				gen = gen * sourceHole;
			}
			
		}
		//end the generation portion


		if(targetElec==0.0 || sourceHole == 0.0)
		{
			recomb = 0.0;
		}
		else if(sqrtterm == 0.0)
		{
			recomb = targetElec * sourceHole / sqrt(4.0 * nSource * nTarget);
			recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			recomb = exp(recomb);
			accuRate->SubtractValue(recomb);
		}
		else
		{
			//update which ones to split because now the variables if interest are targetHole and sourceElec instead.
			splitTarget = (targetElec > targetAvail && targetElec * targetAvail <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
			splitSource = (sourceHole > sourceElec && sourceElec * sourceHole <= nSource * nSource * 0.0009765625);	//just shift exponent bits

			if(!splitSource && !splitTarget)
			{
				recomb = alpha * targetElec * sourceHole;	//alpha * p * n (* vol^2)
				accuRate->SubtractValue(recomb);
			}
			else if(splitSource && splitTarget)
			{
				accuRate->SubtractValue(alpha * nTarget * nSource);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * targetAvail * nSource);	//a negative rate out
				accuRate->AddValue(alpha * nTarget * sourceElec);	//negative rate out
				accuRate->SubtractValue(alpha * targetAvail * sourceElec);	//negative negative rate out (subtract)
				recomb = alpha * targetAvail * sourceElec;
			}
			else if(splitSource)
			{
				accuRate->SubtractValue(alpha * targetElec * nSource);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * targetElec * sourceElec);	//a negative rate out
				recomb = alpha * targetAvail * sourceHole;
			}
			else//if(splitTarget)
			{
				accuRate->SubtractValue(alpha * sourceHole * nTarget);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * sourceHole * targetAvail);	//a negative rate out
				recomb = alpha * targetElec * sourceHole;
			}
		}

	}
	return(DoubleSubtract(gen,recomb));	//return the net rate in

}

int CalcGenRecDerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget)	//how does modifying the source energy level occupancy change the net rate in 
{
	derivSource = derivTarget = 0.0;

	if(srcE == NULL || trgE == NULL)
		return(-1);
	
	Point* pt = &srcE->getPoint();

	double sourceAvail, sourceOcc, targetOcc, targetAvail;
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);

	double beta = KBI / pt->temp;

	double delE = -(pt->eg + srcE->eUse + trgE->eUse);
	if(delE > 0.0)
		delE=-delE;
	//but better make sure it is just in case the bands overlap and it's a metal!
	//the source and target may actually be flipped, so just make sure it's negative to be the
	//appropriate recombinative process - this is the ONLY  difference when switching the states

	delE = exp(beta * delE);
	double difsq = sourceOcc - targetOcc;	//assuming c_s - c_t for alpha
	difsq = difsq * difsq;	//essentially (n-p)^2, alpha = 1/tau/sqrt [ (n-p)^2 + 4ni^2 ]
	double sqrtterm = difsq + 4.0 * pt->niCarrier * pt->niCarrier;
	sqrtterm = sqrt(sqrtterm);

	double taui = pt->MatProps->tauThGenRec > 0.0 ? pt->MatProps->tauThGenRec : 0.5 * (srcE->taui + trgE->taui);

	double alpha = 1.0 / sqrtterm;	//generation typically alpha n_i^2 at equilibrium - this does NOT have the taui included

	double derivAlpha = taui * (targetOcc - sourceOcc) * alpha * alpha;// * alpha;	//order matters. Try to prevent infinities
	//note derivAlpha/c_t = -derivAlpha/c_s --- this one is dA/dc_s

	//going to factor out one alpha from deriv alpha, and then put an alpha in below, which
	//lets me factor it out, and potentially avoid some indeterminatve values (infinity - infinity types)
//	double deriv = (derivAlpha * targetAvail * sourceAvail * delE
//		- taui * targetAvail * delE
//		- derivAlpha * sourceOcc * targetOcc
//		- taui * targetOcc) * alpha;

	//note, all derivAlpha's here are wrt to target, so negative!
	derivTarget = (-derivAlpha * targetAvail * sourceAvail * delE // dA/dc_t(=-dA/dc_s) * a_s * a_t * e^B
		- taui * sourceAvail * delE	//alpha * a_s * e^B
		+ derivAlpha * sourceOcc * targetOcc	// - dA/dc_t(=-dA/dc_s) * c_s * c_t
		- taui * sourceOcc) * alpha;	//alpha * c_s

	// deriv of the rate in the source state with respect to the carrier concentration
	derivSource = (derivAlpha * targetAvail * sourceAvail * delE	///dA/dc_s * a_t * a*s * e^B, dA/dc_s missing alpha (But has taui)
		- taui * targetAvail * delE	//alpha * a_t * e^B, alpha factored out is missing taui
		- derivAlpha * sourceOcc * targetOcc	//dA/d_cs * c_s * c_t
		- taui * targetOcc) * alpha;	// alpha (factored out) * c_t


//	derivSource = -taui * alpha * (targetAvail * delE + targetOcc);
//	derivTarget = -taui * alpha * (sourceAvail * delE + sourceOcc);

	return(0);	//return the net rate in

}


double CalcRecRateFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim, bool GenLight, PosNegSum* accuRate)
{
	if(srcE == NULL || trgE == NULL)
		return(0.0);
	
	Point* pt = &srcE->getPoint();
	
	//note, if this is a Rec Rate, the source is an electorn (CB) and the target is a hole (VB)

	double sourceHole, sourceElec, targetElec, targetHole, nSource, nTarget;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetHole);
	nSource = srcE->GetNumstates();
	nTarget = trgE->GetNumstates();

//	double sourceAbsEnergy = pt->cb.bandmin + srcE->eUse;
//	double targetAbsEnergy = pt->vb.bandmin - trgE->eUse;

	//this is CLEARLY a bad way to do this. As the band structure shifts in bandmin,
	//error gets introduced the further away it is from zero.

	double beta = KBI / pt->temp;
	
	double delE = -(pt->eg + srcE->eUse + trgE->eUse);	//DO RELATIVE! DO NOT DO ABSENERGY, drifting band structure
	//introduces error the further away from zero, which when mixed with an exponential causes all kinds of bad shit to happen
	if(delE>0.0)
		delE=-delE;
	delE = exp(beta * delE);
	double difsq = targetHole - sourceElec;
	difsq = difsq * difsq;	//essentially (n-p)^2, alpha = 1/tau/sqrt [ (n-p)^2 + 4ni^2 ]
	double sqrtterm = difsq + 4.0 * pt->niCarrier * pt->niCarrier;
	sqrtterm = sqrt(sqrtterm);
	double taui = pt->MatProps->tauThGenRec > 0.0 ? pt->MatProps->tauThGenRec : 0.5 * (srcE->taui + trgE->taui);
	double alpha = sqrtterm!= 0.0 ? taui / sqrtterm : 0.0;	// 1/tau_effective = 1/tau_1 + 1/tau_2 + ...
	//generation typically alpha n_i^2 at equilibrium
	//equivalent to Nc Nv e_-delE)
	//further generalized as Nc is mostly empty and Nv is mostly full
	//from the number of empty states in CB and number of elec in VB

	double gen,recomb;
	if(accuRate==NULL)
	{
		if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
		{
			//but they are really far apart. There's no carriers, and the intrinsic carrier concentration is beyond recognition
			//try to do some math ln/exp stuff to combine terms, etc. that avoids Inf * 0.
			gen = targetElec * sourceHole / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
			gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			gen = exp(gen);
		}
		else if(delE == 0.0)
		{
			gen = log(alpha * targetElec * sourceHole) -(pt->eg + srcE->eUse + trgE->eUse)*beta;
			gen = exp(gen);
		}
		else 
		{
			gen = alpha * targetElec * sourceHole * delE;
		}
		if(targetHole==0.0 || sourceElec == 0.0)
		{
			recomb = 0.0;
		}
		else if(sqrtterm == 0.0)
		{
			recomb = targetHole * sourceElec / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
			recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			recomb = exp(recomb);
		}
		else
		{
			recomb = alpha * targetHole * sourceElec;	//alpha * p * n (* vol^2)
		}

		
	}
	else
	{
		//calculating the gen term by term
		//the question becomes does targetElec or sourceHole need to be split into multiple terms
		//it must be close to numStates so the accuracy is lost.
		bool splitTarget = (targetElec > targetHole && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceHole > sourceElec && sourceElec * sourceHole <= nSource * nSource * 0.0009765625);	//just shift exponent bits

		// (c/n) * (a/n) = 0.5 max

		if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
		{
			gen = targetElec * sourceHole / sqrt(4.0 * nSource * nTarget);
			gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			gen = exp(gen);
			accuRate->AddValue(gen);
		}
		else if(delE == 0.0)	//try to decrease the exponent by the other stuff
		{
			gen = log(alpha * targetElec * sourceHole) -(pt->eg + srcE->eUse + trgE->eUse)*beta;
			gen = exp(gen);
			accuRate->AddValue(gen);
		}
		else 
		{
			if(!splitTarget && !splitSource)
			{
				gen = alpha * targetElec * sourceHole * delE;
				accuRate->AddValue(gen);
			}
			else if(splitTarget && splitSource)
			{
				gen = alpha * delE;
				accuRate->AddValue(gen * nTarget * nSource);
				accuRate->SubtractValue(gen * nTarget * sourceElec);
				accuRate->SubtractValue(gen * targetHole * nSource);
				accuRate->AddValue(gen * targetHole * sourceElec);
				gen = gen * targetElec * sourceHole;
			}
			else if(splitTarget)
			{
				gen = alpha * sourceHole * delE;
				accuRate->AddValue(gen * nTarget);
				accuRate->SubtractValue(gen * targetHole);
				gen = gen * targetElec;
			}
			else//if(splitSource)
			{
				gen = alpha * targetElec * delE;
				accuRate->AddValue(gen * nSource);
				accuRate->SubtractValue(gen * sourceElec);
				gen = gen * sourceHole;
			}
			
		}
		//end the generation portion


		if(targetHole==0.0 || sourceElec == 0.0)
		{
			recomb = 0.0;
		}
		else if(sqrtterm == 0.0)
		{
			recomb = targetHole * sourceElec / sqrt(4.0 * nSource * nTarget);
			recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
			recomb = exp(recomb);
			accuRate->SubtractValue(recomb);
		}
		else
		{
			//update which ones to split because now the variables if interest are targetHole and sourceElec instead.
			splitTarget = (targetHole > targetElec && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
			splitSource = (sourceElec > sourceHole && sourceElec * sourceHole <= nSource * nSource * 0.0009765625);	//just shift exponent bits

			if(!splitSource && !splitTarget)
			{
				recomb = alpha * targetHole * sourceElec;	//alpha * p * n (* vol^2)
				accuRate->SubtractValue(recomb);
			}
			else if(splitSource && splitTarget)
			{
				accuRate->SubtractValue(alpha * nTarget * nSource);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * targetElec * nSource);	//a negative rate out
				accuRate->AddValue(alpha * nTarget * sourceHole);	//negative rate out
				accuRate->SubtractValue(alpha * targetElec * sourceHole);	//negative negative rate out (subtract)
				recomb = alpha * targetHole * sourceElec;
			}
			else if(splitSource)
			{
				accuRate->SubtractValue(alpha * targetHole * nSource);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * targetHole * sourceHole);	//a negative rate out
				recomb = alpha * targetHole * sourceElec;
			}
			else//if(splitTarget)
			{
				accuRate->SubtractValue(alpha * sourceElec * nTarget);	//subtractRate because this is a rate OUT
				accuRate->AddValue(alpha * sourceElec * targetElec);	//a negative rate out
				recomb = alpha * targetHole * sourceElec;
			}
		}

	}

	if (GenLight)
	{
		ELevelRate rateData(srcE, trgE, RATE_RECOMBINATION, ELECTRON, ELECTRON, HOLE, DoubleSubtract(recomb, gen), recomb, gen, recomb, -1.0, 0.0, 0.0, 0.0);
		if (pt->MatProps->direct)	//this is a direct band gap material, so it can generate light from recombination.
			GenerateLight(sim, rateData);
	}
	return(DoubleSubtract(gen,recomb));	//return the net rate in

}

std::vector<ELevelRate>::iterator CalculateGenerationRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	//need to calculate forward rate (generation)
	//need to calculate backward rate (recombination)
	//need to calculate RateIn
	//need to put RateOut in in srcELevel
	//need to calculate the equilibrium value
	//need to calculate the equilibrium rate out

	//srcE is in the VB, and trgE is in the CB
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	Point* pt = &srcE->getPoint();

	double sourceHole, sourceElec, targetElec, targetAvail;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSource, targetElec, targetAvail);
	
	double beta = KBI / pt->temp;

	double delE = -(pt->eg + srcE->eUse + trgE->eUse);
	//but better make sure it is just in case the bands overlap and it's a metal!
	if(delE > 0.0)
		delE = -delE;
	delE = exp(beta * delE);
	double difsq = sourceHole - targetElec;
	difsq = difsq * difsq;	//essentially (n-p)^2, alpha = 1/tau/sqrt [ (n-p)^2 + 4ni^2 ]
	double sqrtterm = difsq + 4.0 * pt->niCarrier * pt->niCarrier;
	sqrtterm = sqrt(sqrtterm);

	double derivSonly = 1.0/(sqrtterm * sqrtterm * sqrtterm);	//^-3/2
	double taui = pt->MatProps->tauThGenRec > 0.0 ? pt->MatProps->tauThGenRec : 0.5 * (srcE->taui + trgE->taui);

	double alpha = sqrtterm!=0.0 ? taui / sqrtterm : 0.0;	//generation typically alpha n_i^2 at equilibrium
	//equivalent to Nc Nv e_-delE)
	//further generalized as Nc is mostly empty and Nv is mostly full
	//from the number of empty states in CB and number of elec in VB

	if(MY_IS_FINITE(derivSonly)==true)
		derivSonly = taui * (
			derivSonly * (targetElec - sourceHole) * (sourceElec * targetAvail * delE - targetElec * sourceHole)
		  - (targetAvail * delE + targetElec) / sqrtterm
			);
	else
		derivSonly = 0.0;

	double gen, recomb;
	if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
	{
		gen = targetAvail * sourceElec / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
		gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
		gen = exp(gen);
	}
	else if(delE == 0.0)
	{
		gen = log(alpha * targetAvail * sourceElec) - (pt->eg + srcE->eUse + trgE->eUse)*beta;
		gen=exp(gen);
	}
	else
	{
		gen = alpha * targetAvail * sourceElec * delE;
	}

	if(targetElec==0.0 || sourceHole == 0.0)
	{
		recomb = 0.0;
	}
	else if(sqrtterm == 0.0)
	{
		recomb = targetElec * sourceHole / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
		recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse))*beta;
		recomb = exp(recomb);
	}
	else
	{
		recomb = alpha * targetElec * sourceHole;	//alpha * p * n (* vol^2)
	}
//	RecordGenRecomb(false, targetAbsEnergy, sourceAbsEnergy, gen, recomb, true);

	double deriv = -alpha * (sourceHole + targetElec + delE * (sourceElec + targetAvail));
	double deriv2 = 2.0 * alpha * (delE - 1.0);
	
	/*
	double transferElecVBCB1 = 0.0;
	double transferElecVBCB2= 0.0;

	//need to determine the equilibrium number of carriers to transfer
	double A = delE * (targetAvail + sourceElec) + sourceHole + targetElec;
	double den = 2.0 * (delE - 1.0);
	
	double equilsqrt = 2.0 * den * (delE * targetAvail * sourceElec - targetElec * sourceHole);
	equilsqrt = A * A - equilsqrt;
	if(den == 0.0)	//this can only happen if delE=1, so change in energy is 0
	{
		den = targetNum + sourceNum;
		double num = targetAvail * sourceElec - targetElec * sourceHole;
		transferElecVBCB1 = num / den;
		transferElecVBCB2 = transferElecVBCB1;
	}
	else if(equilsqrt == 0.0)
	{
		transferElecVBCB1 = A / den;
		transferElecVBCB2 = transferElecVBCB1;
	}
	else if(equilsqrt > 0.0)
	{
		equilsqrt = sqrt(equilsqrt);
		den = 1.0/den;
		transferElecVBCB1 = (A - equilsqrt) * den;
		transferElecVBCB2 = (A + equilsqrt) * den;
	}
	else
	{
		//there are 2 negative terms when expanded. One is guaranteed to be positive with two terms (A-B)^2,
		//the other is -2 c_h v_e delE^2, which when combined with 4 c_h v_e delE results in 2 c_h v_e delE * (2 - delE)
		// 0 <= delE <= 1, therefore it is still positive. This code should never execute

		ProgressDouble(33, equilsqrt);	//Output error message
	}


	double transferElecVBCBUse = 0.0;
	double test1S = sourceElec - transferElecVBCB1;		//VB e- after transfer 1
	double test1T = targetElec + transferElecVBCB1;	//imp e- after transfer 1
	double test2T = transferElecVBCB2 + targetElec;	//VB e- after transfer 2
	double test2S = sourceElec - transferElecVBCB2;	//imp e- after transfer 2
	bool onegood = false;
	bool twogood = false;
	if(test1S <= sourceNum && test1S >= 0.0 && test1T <= targetNum && test1S >= 0.0)	//first is valid answer
		onegood = true;
	if(test2S <= sourceNum && test2S >= 0.0 && test2T <= targetNum && test2T >= 0.0)	//second is valid answer
		twogood = true;

	if(onegood && twogood)	//both are valid, choose the more center of the road answer
	{
		double origS = sourceElec/sourceNum;
		double origT = targetElec/targetNum;
		double percentS = test1S / sourceNum;
		double percentT = test1T / targetNum;
		double percent1 = fabs(percentS - origS) + fabs(percentT - origT);

		percentS = test2S / sourceNum;
		percentT = test2T / targetNum;
		double percent2 = fabs(percentS - origS) + fabs(percentT - origT);	//measure of distance away from the starting position (trying to avoid extremes)

		//we want the smaller distance from the middle of the road, so the smaller of the two percents
		if(percent1 <= percent2)
			transferElecVBCBUse = transferElecVBCB1;
		else
			transferElecVBCBUse = transferElecVBCB2;
	}
	else if(onegood)
		transferElecVBCBUse = transferElecVBCB1;
	else if(twogood)
		transferElecVBCBUse = transferElecVBCB2;
	else
		transferElecVBCBUse = 0.0;	//prevent any transfers if it couldn't find an equilibrium value


	//now calculate the rate out for equilibrium. Use recombination rate (equal) to cut out a multiply by delE
	double equilRateOut = alpha * (targetElec + transferElecVBCBUse) * (sourceHole + transferElecVBCBUse);
	*/
	ELevelRate rateData(srcE,trgE,RATE_GENERATION,ELECTRON,HOLE,ELECTRON,DoubleSubtract(gen, recomb),recomb,gen,gen,-1.0,deriv,deriv2,derivSonly);
	/*
	ELevelOpticalRate rateData;
	ELevelOpticalRateSort rateSort;
	rateSort.carrier = ELECTRON;
	rateSort.source = srcE;
	rateData.sourcecarrier = HOLE;
	rateSort.target = trgE;
	rateData.targetcarrier = ELECTRON;
	rateSort.type = RATE_GENERATION;
	rateData.RateIn = gen;
	rateData.RateOut = recomb;
	rateData.netrate = DoubleSubtract(gen, recomb);
	rateData.RateType = gen;
	//want netrate in, so gen-rec, d_gen/dc_s - d_rec/dc_s, note that dc_s = dc_t, so signs are good. Note derivGen is negative, so this still fits the SRH pattern
	rateData.deriv = deriv;
	rateData.deriv2 = deriv2;
	rateData.derivsourceonly = derivSonly;
	*/
	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;
//	RecordRDataLimits(rateData, srcE, gen, recomb, sourceHole + targetElec, 1.0); //this tests to see if it is close to equilibrium, adds in the necessary rateIn/rateOut, 1.0 is to make sign correct for equiltransfer, netrate, and timestep
	ret = srcE->AddELevelRate(rateData, false);	//add the rate... but this is kind of backwards because we want to add rateOut
	//but we care about the generation rate!
	//it will add
	
	return(ret);
}

std::vector<ELevelRate>::iterator CalculateRecombinationRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//need to calculate forward rate (recombination)
	//need to calculate backward rate (generation)
	//need to calculate RateIn
	//need to put RateOut in in srcELevel
	//need to calculate the equilibrium value
	//need to calculate the equilibrium rate out

	//srcE is in the CB, and trgE is in the VB
	Point* pt = &srcE->getPoint();

	double sourceElec, sourceHole, targetElec, targetHole;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSource, targetElec, targetHole);
		
	double beta = KBI / pt->temp;

	double delE = -(pt->eg + srcE->eUse + trgE->eUse);
	if(delE > 0.0)
		delE = -delE;
	delE = exp(beta * delE);
	double difsq = targetHole - sourceElec;
	difsq = difsq * difsq;	//essentially (n-p)^2, alpha = 1/tau/sqrt [ (n-p)^2 + 4ni^2 ]
	double sqrtterm = difsq + 4.0 * pt->niCarrier * pt->niCarrier;
	sqrtterm = sqrt(sqrtterm);
	double derivSonly = 1.0/(sqrtterm * sqrtterm * sqrtterm);	//^-3/2
	double taui = pt->MatProps->tauThGenRec > 0.0 ? pt->MatProps->tauThGenRec : 0.5 * (srcE->taui + trgE->taui);
	

	double alpha = sqrtterm!=0.0?taui / sqrtterm:0.0;	// 1/tau_effective = 1/tau_1 + 1/tau_2 + ...
	//generation typically alpha n_i^2 at equilibrium
	//equivalent to Nc Nv e_-delE)
	//further generalized as Nc is mostly empty and Nv is mostly full
	//from the number of empty states in CB and number of elec in VB

	if(MY_IS_FINITE(derivSonly)==true)
		derivSonly = taui * (
			derivSonly * (sourceElec - targetHole) * (sourceHole * targetElec * delE - targetHole * sourceElec)
		  - (targetElec * delE + targetHole) / sqrtterm
			);
	else
		derivSonly = 0.0;	//it's basically stuck at zero anyway...

	double gen,recomb;
	if(delE == 0.0 && sqrtterm == 0.0) //targetelec (e- in VB) and sourceHole (h+ in CB) are never gonna be zero - generation.
	{
		//but they are really far apart. There's no carriers, and the intrinsic carrier concentration is beyond recognition
		//try to do some math ln/exp stuff to combine terms, etc. that avoids Inf * 0.
		gen = targetElec * sourceHole / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
		gen = log(gen) + (-0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
		gen = exp(gen);
	}
	else if(delE == 0.0)
	{
		gen = log(alpha * targetElec * sourceHole) - (pt->eg + srcE->eUse + trgE->eUse)*beta;
		gen = exp(gen);
	}
	else 
	{
		gen = alpha * targetElec * sourceHole * delE;
	}
	if(targetHole==0.0 || sourceElec == 0.0)
	{
		recomb = 0.0;
	}
	else if(sqrtterm == 0.0)
	{
		recomb = targetHole * sourceElec / sqrt(4.0 * srcE->GetNumstates() * trgE->GetNumstates());
		recomb = log(recomb) + (0.5 * (pt->eg + srcE->eUse + trgE->eUse)*beta);
		recomb = exp(recomb);
	}
	else
	{
		recomb = alpha * targetHole * sourceElec;	//alpha * p * n (* vol^2)
	}
	double deriv = -alpha * (sourceElec + targetHole + delE * (targetElec + sourceHole));
	double deriv2 = 2.0 * alpha * (delE - 1.0);

//	RecordGenRecomb(false, sourceAbsEnergy, targetAbsEnergy, gen, recomb, false);

	/*
	

	double transferElecCBVB1 = 0.0;
	double transferElecCBVB2= 0.0;

	//need to determine the equilibrium number of carriers to transfer
	double A = -(delE * (targetElec + sourceHole) + sourceElec + targetHole);	//b...
	double den = 2.0 * (delE - 1.0);	//2a
	
	double equilsqrt = 2.0 * den * (delE * targetElec * sourceHole - sourceElec * targetHole);	//4ac
	equilsqrt = A * A - equilsqrt;	//b^2 - 4ac
	if(den == 0.0)	//this can only happen if delE=1, so change in energy is 0
	{
		den = targetNum + sourceNum;
		double num = targetHole * sourceElec - targetElec * sourceHole;
		transferElecCBVB1 = num / den;
		transferElecCBVB2 = transferElecCBVB1;
	}
	else if(equilsqrt == 0.0)
	{
		transferElecCBVB1 = A / den;
		transferElecCBVB2 = transferElecCBVB1;
	}
	else if(equilsqrt > 0.0)
	{
		equilsqrt = sqrt(equilsqrt);	//sqrt(b^2-4AC)
		den = 1.0/den;	// 1/2a
		transferElecCBVB1 = (A - equilsqrt) * den;
		transferElecCBVB2 = (A + equilsqrt) * den;
	}
	else
	{
		//there are 2 negative terms when expanded. One is guaranteed to be positive with two terms (A-B)^2,
		//the other is -2 c_h v_e delE^2, which when combined with 4 c_h v_e delE results in 2 c_h v_e delE * (2 - delE)
		// 0 <= delE <= 1, therefore it is still positive. This code should never execute

		ProgressDouble(33, equilsqrt);	//Output error message
	}


	double transferElecCBVBUse = 0.0;
	double test1S = sourceElec - transferElecCBVB1;		//CB e- after transfer 1
	double test1T = targetElec + transferElecCBVB1;	//VB e- after transfer 1
	double test2T = transferElecCBVB2 + targetElec;	//VB e- after transfer 2
	double test2S = sourceElec - transferElecCBVB2;	//CB e- after transfer 2
	bool onegood = false;
	bool twogood = false;
	if(test1S <= sourceNum && test1S >= 0.0 && test1T <= targetNum && test1S >= 0.0)	//first is valid answer
		onegood = true;
	if(test2S <= sourceNum && test2S >= 0.0 && test2T <= targetNum && test2S >= 0.0)	//second is valid answer
		twogood = true;

	if(onegood && twogood)	//both are valid, choose the more center of the road answer
	{
		double origS = sourceElec/sourceNum;
		double origT = targetElec/targetNum;
		double percentS = test1S / sourceNum;
		double percentT = test1T / targetNum;
		double percent1 = fabs(percentS - origS) + fabs(percentT - origT);

		percentS = test2S / sourceNum;
		percentT = test2T / targetNum;
		double percent2 = fabs(percentS - origS) + fabs(percentT - origT);	//measure of distance away from the starting position (trying to avoid extremes)

		//we want the smaller distance from the middle of the road, so the smaller of the two percents
		if(percent1 <= percent2)
			transferElecCBVBUse = transferElecCBVB1;
		else
			transferElecCBVBUse = transferElecCBVB2;
	}
	else if(onegood)
		transferElecCBVBUse = transferElecCBVB1;
	else if(twogood)
		transferElecCBVBUse = transferElecCBVB2;
	else
		transferElecCBVBUse = 0.0;	//prevent any transfers if it couldn't find an equilibrium value



	//now calculate the rate out for equilibrium. Because rates are equal, will use recombination rate to cut out delE
	double equilRateOut = alpha * (targetHole - transferElecCBVBUse) * (sourceElec - transferElecCBVBUse);
	*/
	ELevelRate rateData(srcE,trgE,RATE_RECOMBINATION,ELECTRON,ELECTRON,HOLE,DoubleSubtract(recomb,gen),recomb,gen,recomb,-1.0,deriv,deriv2,derivSonly);
	/*
	ELevelOpticalRateSort rateSort;
	rateSort.carrier = ELECTRON;
	rateSort.source = srcE;
	rateData.sourcecarrier = ELECTRON;
	rateSort.target = trgE;
	rateData.targetcarrier = HOLE;
	rateSort.type = RATE_RECOMBINATION;
	rateData.RateIn = gen;
	rateData.RateOut = recomb;
	rateData.netrate = DoubleSubtract(recomb, gen);
	rateData.RateType = recomb;
	//want deriv net rate in, so d_gen/dc_s - d_rec/dc_s. Here dc_s = dc_t being a gen/rec pair, so signs stay the same
	rateData.deriv = deriv;
	rateData.deriv2 = deriv2;
	rateData.derivsourceonly = derivSonly;
	*/
	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;

	//positive net rate loses carriers (- change), positve transfer loses carriers (- change), +1.0
// this is no longer a good thing to do...	RecordRDataLimits(rateData, srcE, gen, recomb, sourceElec + targetHole, 1.0); //this tests to see if it is close to equilibrium, adds in the necessary rateIn/rateOut, 1.0 is to make sign correct for equiltransfer, netrate, and timestep
	ret = srcE->AddELevelRate(rateData, false);	//add the rate... but this is kind of backwards because we want to add rateOut
	
	if(pt->MatProps->direct)	//this is a direct band gap material, so it can generate light from recombination.
		GenerateLight(sim, rateData);

	/*
	//but we care about the generation rate!
	if(sim->EnabledRates.LightEmissions)
	{
		double EnergyPhoton = sourceAbsEnergy - targetAbsEnergy;
		if(EnergyPhoton > ENERGY_CUTOFF_EMIT_PHOTON && rateData.netrate > 0.0)
		{
			double intensity = rateData.netrate * EnergyPhoton * QCHARGE * 1000 * pt->areai.x;
			//carriers(photons)/sec * eV/photon * J/eV = J/s = W * mW/W * cm^-2 = mW/cm^2
			if(intensity > INTENSITY_EMIT_CUTOFF)
			{
				Wavelength tmp;
				tmp.IsCutOffPowerInit = false;	//this will cause the cut-off power to be calculated in light.cpp
				tmp.energyphoton = EnergyPhoton;
				tmp.lambda = float(PHOTONENERGYCONST / EnergyPhoton);	//eV*nm / eV = nm
				tmp.coherence1 = tmp.coherence2 = 0.0;
				tmp.collimation = PI;
				tmp.intensity = intensity;
				pt->LightEmissions.light.push_back(tmp);
				DetermineEmissionCoefficients(pt->LightEmissions.light.back(), rateSort.source, rateSort.target);
				//this is after so the destructor of the temporary wavelength doesn't immediately delete the data
				//upon exiting the { } when it loses scope
				//DetermineEmissionCoeffs creates the emitdata on the heap
				sim->LightVarData.push_back(tmp.emitdata);	//put it in sim, so then it can be removed from the heap later
				if(sim->outputs.LightEmission == true)
				{
					OpSort ops;
					ops.LSpec = pt->LightEmissions.light.back().emitdata;
					ops.wavelength = tmp.lambda;
					MapAdd(sim->OpticalFluxOut.Emissions,ops,recomb);
					if(pt->MatProps)
						MapAdd(pt->MatProps->OpticalFluxOut.Emissions,ops,recomb);
				}
			}
		}
	}
	*/
	
	return(ret);
}