#ifndef SAAPD_PHYSICS_H
#define SAAPD_PHYSICS_H

#define KB 8.617343E-5	//boltzmann constant in eV
#define KBI (1.0 / KB)	//inverse of boltzmann constant for 1/(kT)
#define MELECTRON 9.10938188E-31	//mass of electron in KG
#define MELECTRONI (1.0/MELECTRON)	//inverse of electron mass in kg
#define QCHARGE 1.60217646E-19	//charge per electron
#define QI (1.0/QCHARGE)		//inverse of charge, avoid dividing by Q
#define Q2 (QCHARGE*QCHARGE)			//charge squared
#define Q3 (Q2 * QCHARGE)		//charge cubed
#define EPSNOT 8.85418782E-14	//permittivity of free space: F/CM
#define EPSNOTI (1.0 / EPSNOT)	//inverse...

#ifndef PI
#define PI	3.14159265358979323846	//pi... 
#endif

#define PII (1.0/PI)	// 1/pi
#define PI2 (2.0*PI)	//2 pi...
#define PI2I (1.0/PI2)		// 1.0 / (2pi)
#define PID2 (PI*0.5)	// pi/2
#define RADTODEGREE (180.0 * PII)	// 180 degrees = pi radians
#define DEGREETORAD (PI / 180.0)	
#define PLANCKEVS 4.13566733E-15	//planck's constant eV s
#define HBAR (PLANCKEVS * PI2I)	//planck's constant bar eV s (h/2pi)
#define THVEL 1e7	//thermal velocity, 10^7 cm/s
#define KTDQ (KB*300.0)	// for kT/q (except k was in joules).
#define SQRT2 1.4142135623731	// SQRT(2)
#define SQRT3 1.7320508075689	// SQRT(3)
#define SQRT2I (1.0/SQRT2)
#define SQRT3I (1.0/SQRT3)
#define DIST2 (12.0 * SQRT2I)		//used in rho calculation of energy charges - 2nd order distances
#define DIST3 (8 * SQRT3I)		//used in rho calculation of energy changes - 3rd order distances - First order is just 6
#define ONETHIRD (1.0/3.0)	//this will be used a fair amount, and I want it to the fullest accuracy.
#define FOURTHIRDS (4.0/3.0)	//this will alsop be used a fair amount
#define TWONEGELEV 0.00048828125	//used to see if within kT/1024 of energy
#define SPEEDLIGHT 299792458.1	// m/s
#define PHOTONENERGYCONST (PLANCKEVS * SPEEDLIGHT * 1.0e9) //wavelengths are given in nm: ev*s m/s nm/m -> eV*nm
#endif