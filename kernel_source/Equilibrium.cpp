#include "Equilibrium.h"

#include <vector>
#include <ctime>
#include <map>

#include "BasicTypes.h"
#include "model.h"
#include "contacts.h"

#include "output.h"
#include "outputdebug.h"
#include "transient.h"
#include "physics.h"
#include "scatter.h"
#include "PopulateModel.h"
#include "CalcBandStruct.h"
#include "ss.h"

#include "../gui_source/kernel.h"

//this starts at the contact and then keeps making the point go to equilibrium point by point, and sets a cancelling rho in the
//adjacent point to avoid excessive band bending.Note - this will probably only accurately work for 1D
int ThermalEquilibrium(kernelThread* kThread, ModelDescribe& mdesc, Model& mdl)
{
	DisplayProgress(46,0);
	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	//first, get the point contacts. They need to be thermalized and have fixed values
	std::map<long int, Contact*> contactPts;
	std::map<long int, Point*> ctPtPtr;
/*	for(unsigned int i=0; i<mdesc.simulation->contacts.size(); i++)
	{
		for(unsigned int j=0; j<mdesc.simulation->contacts[i]->point.size(); j++)
		{
			//create the keys or just put them in
			
		}
	}*/

	Contact* savePtCtLeft = NULL;
	Contact* savePtCtRight = NULL;
	Point* LPt = &mdl.gridp.begin()->second;
	Point* RPt = LPt;
	
	while(LPt->adj.adjMX != NULL)
		LPt = LPt->adj.adjMX;

	while(RPt->adj.adjPX != NULL)
		RPt = RPt->adj.adjPX;

	Contact ThEqLeft;
	Contact ThEqRight;

	ThEqLeft.SetThEq();
	ThEqRight.SetThEq();

	savePtCtLeft = LPt->contactData;
	savePtCtRight = RPt->contactData;
	LPt->contactData = &ThEqLeft;
	RPt->contactData = &ThEqRight;
	ThEqLeft.point.push_back(LPt);
	ThEqRight.point.push_back(RPt);
	CreateExternalContactPoints(&ThEqLeft, mdl);
	CreateExternalContactPoints(&ThEqRight, mdl);
	ThEqLeft.UpdatePlaneRecPoints(mdesc.Ldimensions);
	ThEqRight.UpdatePlaneRecPoints(mdesc.Ldimensions);

	


	for(unsigned int i=0; i<mdesc.simulation->contacts.size(); ++i)
		mdesc.simulation->contacts[i]->SetInactive();	//make sure nothing but these two contacts have any effect

	mdl.PoissonDependencyCalculated = false;	//make sure it does this particular value

	ThEqLeft.id = 1;
	ThEqRight.id = 2;

	

	

	

	for(unsigned long int j=0; j<mdl.numpoints; j++)
		mdl.poissonRho[j] = 0.0;	//make sure this is cleared out beforehand

	contactPts[LPt->adj.self] = &ThEqLeft;
	ctPtPtr[LPt->adj.self] = LPt;

	contactPts[RPt->adj.self] = &ThEqRight;
	ctPtPtr[RPt->adj.self] = RPt;

	CalcBandStruct(mdl, mdesc, true);	//this should get things initialized for ThEq going
	

//	vector<Point*> ctcPts;
//	vector<unsigned long> ptID;
//	ctPtPtr.GenerateSort(ctcPts);
//	ctPtPtr.GenerateSortS(ptID);
	
//	double kbTI = KBI / mdesc.simulation->T;	unsused

	

	//make sure all the fermi levels are zeroed out and they are appropriately done...
	bool EFieldContact = false;
	for (std::map<unsigned long int, BoundaryCondition>::iterator BCs = mdl.poissonBC.begin(); BCs != mdl.poissonBC.end(); ++BCs)
	{
		EquilibratePoint(BCs->second.pt);	//This should zero out the contact, which should have no net charge for thermal equilbrium
		BCs->second.ProcessBC(mdesc.simulation, 0.0, true);	//force the boundary condition to be zero, whether that's Voltage, EField, etc.
		//this is the bug right here... The voltage may be forced to zero, but the electric field is NOT necessarily going to be zero. Probably just *close*
		//to zero. If the electric field is zero, we are basically forcing the system into a state that might not result in practical
		//values for the e-field to be zero.
		if (BCs->second.type != BC_VOLT)
			EFieldContact = true;

		/*
	//		ctcPts[ctr]->ResetFermiEnergies(false, OCC, mdesc.simulation->accuracy, true);	//only update the main/quasi fermi levels, not individual energy states.
		//FindFermiPt(pt, *this);	//put the value of Ef in there based on present Ev and Ec.
		double level = 0.0;	//the voltage Ef was set at: Chem potential = -Voltage
		//so Ef needs to get set to level, but everything else must change first....
		*/
		//now shift everything over
		BCs->second.pt->vb.bandmin = BCs->second.pt->vb.bandmin - BCs->second.pt->fermi;
		BCs->second.pt->cb.bandmin = BCs->second.pt->cb.bandmin - BCs->second.pt->fermi;
		BCs->second.pt->Psi = BCs->second.pt->Psi - BCs->second.pt->fermi;
		BCs->second.pt->fermin -= BCs->second.pt->fermi;
		BCs->second.pt->fermip -= BCs->second.pt->fermi;
		BCs->second.pt->fermi = 0.0;	//curpt->fermi + difference
		
		/*
		//make sure to either add the value or replace the value
		BoundaryCondition tmp;
		Tree<BoundaryCondition, unsigned int>* ptrpoiBC = mdl.poissonBC.Find(ptID[ctr]);
		if(ptrpoiBC == NULL)
		{
			mdl.poissonBC.Insert(ctcPts[ctr]->Psi, ptID[ctr]);	//Put the value of Psi into the given boundary condition
		}
		else
			ptrpoiBC->Data = ctcPts[ctr]->Psi;
			*/

		mdl.poissonRho[BCs->second.pt->adj.self] = GetPointRho(*(BCs->second.pt), 0.0);
	}

	if(EFieldContact)
	{
		EFieldThermalEq(mdesc, mdl);
		return(0);
	}
	//all the contacts are now set to have a zero voltage or zeroed out Ef and their appropriate charge is loaded up.
	
	Point* lastContact = NULL;
	if(ctPtPtr.size() > 0)
		lastContact = ctPtPtr.rbegin()->second;	//will use this point a fair amount
	double PsiBack = lastContact->Psi;
	double xBack = lastContact->position.x;

	double prevCharge = 0.0;	//used to get a rough guess to maintain a stable band diagram
	unsigned long int nextuse = 0;
	double chargeAffectItself;
//	double guessCharge = 0.0; unused
//	double prev1Ef = 0.0; unused
//	double prev2Ef = 0.0; unused
//	double curEf = 0.0; unused
//	double changeEf = 0.0; unused
//	double prevChangeEf = 0.0; unused
	double distEnd;	//distance to get from this point to the end
	double distanceLeft;	//distance between this point and the one to the left
	double distanceRight;
	double chargeAffectNeighbor;	//used to calculate the E-Field to make it constant to the end of the device
	double max = double(mdl.numpoints * (mdl.numpoints + 1) * (mdl.numpoints + 2)) / 3.0;
	double maxinv = 1.0 / max;	//this is used to display the current % of the simulation done.
	double donesofar=0.0;
//	double prevdone; set but not used
//	bool message = true; unused	//just alternate messages

	Point* nextPt = NULL;
	std::map<long int, Point>::iterator tempFind = mdl.gridp.end();

	kThread->shareData.KernelcurInfoMsg = MSG_THERMALEQUILIBRIUM;
	kThread->shareData.KernelshortProgBarRange = mdl.numpoints;
	kThread->shareData.KernelTimeLeft = 0.0;
	
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		long int i = curPt->second.adj.self;
		
		if(ctPtPtr.count(i) > 0)	//this is a contact point, it's value is FIXED.
			continue;

		nextuse = i+1;	//best guess at what the next point that we'll be working on
		do
		{
			if(ctPtPtr.count(nextuse))	//if it is a contact, try the next point
				nextuse++;
			else	//this is a good point next, so use it
				break;	//by getting out
		}while(nextuse < mdl.numpoints);	//make sure nextuse is either good or =mdl.numpoints
		//will error out if nextuse = mdl.numpoints
		kThread->shareData.KernelshortProgBarPosition = i;

		tempFind = mdl.gridp.find(nextuse);
		if(tempFind != mdl.gridp.end())
		{
			nextPt = &(tempFind->second);
		}
		else
		{
			nextPt = NULL;
			nextuse = mdl.numpoints;
		}

		prevCharge = mdl.poissonRho[i];	//this is the point we're solving for, so it has the rho that was in there beforehand
		mdl.poissonRho[i] = 0.0;


		if(nextuse < mdl.numpoints)	//put the same amount of charge in the next adjacent point to try and correct the value
		{
			mdl.poissonRho[nextuse] = prevCharge * curPt->second.vol * nextPt->voli;
		}
		//if there is no next use, then it is the last point and the extra charge may be removed.

		//now get the appropriate charge
//		chargeAffectItself = GetPoissonDependency(mdl.gridp[i], mdl.gridp[i]);

		GetEfZero(curPt->second, mdl, *(mdesc.simulation));	//keep setting Ef to zero and adjusting rho which then shifts the point

		//now we need to calculate the charge in the next point to keep the band diagram from varying wildly.
		//this charge is calculated to get (roughly - possible changing materials) a constant electric field from the adjacent
		//point to the end contact point. This does take into account that the rho moves the current and previous points, and the contact
		//is fixed...

		//let's see what happens if we don't axe out the rest of the band diagram...

		
		if(nextuse < mdl.numpoints)
		{
			CalcPointBandFast(*nextPt, mdl, false, mdesc.simulation->accuracy);
			distEnd = xBack - nextPt->position.x;
			distanceLeft = nextPt->position.x - curPt->second.position.x;
			chargeAffectItself = GetPoissonDependency(nextPt,nextPt);
			chargeAffectNeighbor = GetPoissonDependency(nextPt, &(curPt->second));
			double eps = EPSNOT * nextPt->eps;
			distanceRight = 2.0 * (nextPt->pxpy.x - nextPt->position.x);	//boundaries are halfway between points

			double den  = 0.5 * (distanceRight + distanceLeft) / eps;
			distanceLeft = 1.0 / distanceLeft;	//multiplication is faster...
			distEnd = 1.0 / distEnd;

			den += chargeAffectItself * distanceLeft;
			den += chargeAffectNeighbor * (distEnd + distanceLeft);

			double num = PsiBack - curPt->second.Psi;
			num = num * distEnd;
			num += (curPt->second.Psi - nextPt->Psi) * distanceLeft;

			mdl.poissonRho[nextuse] = num / den;
		}
		

		//let's see if this makes the solution go any faster...
		for(std::map<long int, Point>::iterator cur2Pt = mdl.gridp.begin(); cur2Pt != curPt && cur2Pt != mdl.gridp.end(); ++cur2Pt)
		{	//now that this point has been added
			if(contactPts.count(cur2Pt->second.adj.self))	//this is a contact point, it's value is FIXED.
				continue;

			GetEfZero(cur2Pt->second,mdl, *(mdesc.simulation));	//adjust the rest of the band diagram now that this has been done
			//to get them all approximately equal to zero


		}

		
		time(&end);	//load the end time for dif
		timeelapse = difftime(end, start);
		if(timeelapse > 0.25)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
//			prevdone = donesofar;
			donesofar = double(i * (i + 1) * (i + 2)) / 3.0;
	//		if(message)
		//	{
		//		message = false;
//				DisplayProgressDoub(9, donesofar * 100.0 * maxinv);
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
		//	}
		//	else
		//	{
			//	double num = max - donesofar;
			//	double den = donesofar - prevdone;
			//	DisplayProgressDoub(10, num/den * timeelapse);
			//	message = true;
				
			//}
			
			start = end;
			
		}
	}
#ifndef NDEBUG
	WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
#endif
	//refine the guess a bit (just 3 iterations) now that the entire "solution" has been found
	double MaxChange;
	double prevChange = 1e8;	//ridiculously high so it doesn't fail the first time through
	double tmpChange;
	kThread->shareData.KernelcurInfoMsg = MSG_THEQ_REFINE;
	kThread->shareData.KernelshortProgBarRange = 210;	// ceil ( ln(1e9) ). Assume max change <1 always
	kThread->shareData.KernelshortProgBarPosition = 0;
	kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
	short ctr2 = 0;
	do
	{			
		if(ctr2 >= 10)
		{
			ctr2 = 0;
			if(MaxChange > 1.125*prevChange)	//it is significantly diverging...
			{
				DisplayProgress(48,0);	//tell user it is diverging and exit the loop
				break;
			}
			prevChange = MaxChange;
			time(&end);	//load the end time for dif
			timeelapse = difftime(end, start);
			if(timeelapse > 1.0)	//if tasks are taking a LONG time, notify the user that it's still running every
			{	//10 seconds		
//				DisplayProgressDoub(11, MaxChange);
				kThread->shareData.KernelshortProgBarPosition = 210 - int(10.0*log(MaxChange/1.0e-9));
				start = end;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
			}
		}
		MaxChange = 0.0;
		for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
		{	//now that this point has been added
			if(ctPtPtr.count(curPt->second.adj.self) > 0)	//this is a contact point, it's value is FIXED.
				continue;
			//tmpChange = fabs(mdl.gridp[i2].fermi);	//must get it before the change, because of course it will otherwise be zero...
			tmpChange=fabs(GetEfZero(curPt->second,mdl, *(mdesc.simulation)));	//adjust the rest of the band diagram now that this has been done
			
			if(tmpChange > MaxChange && curPt->second.MatProps->type != VACUUM)
				MaxChange = tmpChange;
		}
		ctr2++;
		
	}while(MaxChange > 1e-9);

	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		if(curPt->second.contactData)
			EquilibratePoint(&(curPt->second));
		else
		{
			InitializePoint(curPt->second);
			ContactCurrent(curPt->second, mdesc, mdl);	//just get the charge to be zero
		}
	}

	//at this point, I would think they are pretty darn good guesses... every point added should have made a small adjustment
	//and then it corrected for it and it threw in terms to cancel any net charge buildup issues.
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
		CalcPointBandFast(curPt->second, mdl, false, mdesc.simulation->accuracy);
#ifndef NDEBUG
	WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
	CompareRho(mdl, mdesc.simulation->OutputFName);
#endif
	DisplayProgress(47,0);

	
	LPt->contactData = savePtCtLeft;
	RPt->contactData = savePtCtRight;
	mdl.PoissonDependencyCalculated = false;
	if(mdesc.simulation->SteadyStateData && mdesc.simulation->simType == SIMTYPE_SS)
	{
		mdesc.simulation->SteadyStateData->LastBDUpdated = -1;	//flag to load in new data for ss
	}
	else if(mdesc.simulation->TransientData && mdesc.simulation->simType == SIMTYPE_TRANSIENT)
	{

		for (unsigned int i = 0; i<mdesc.simulation->TransientData->ContactData.size(); ++i)
		{
			mdesc.simulation->TransientData->ContactData[i]->UpdateActive(mdesc.simulation->TransientData->simtime, true);
		}
	}

	return(0);
}

int EFieldThermalEq(ModelDescribe& mdesc, Model& mdl)
{
	//this code is going to be *very* simple.
	//assume charge neutrality.
	//set the starting voltage to zero.
	//set the electric field to zero.
	//this should define the entire band diagram in it's entirety.
	//cycle through the band diagram

	/*
	if EField and V apply to same point, then none of the points will rely on their own charge.
	Branch out from the contact point in both directions.  Both sides will be independent of one another

	If they are different points, the inner portions will not depend on the outer rho's.
	The outer points will not rely on their own rho, but will rely on the other rho's until crossing both(?) boundary conditions. I don't think they will have an effect on anything past them, but they might.

	Think of it as propagating a solution over from the known value (contact point).  Once the voltage/Efield have both propagated (think as moving in the direction from the far opposing end), there will be no reliance on it's own rho or anything else beyond it.
	*/


	//this is actually going to be really complicated... and we're not going to get thermal equilibrium from it simply.
	//we're just going to skip the start in ThEq. step and save it for a later date.
	//this will at least result in something semi-acceptable to the code
	for(unsigned int i=0; i<mdl.numpoints; i++)
		mdl.poissonRho[i] = 0.0;
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		CalcPointBandFast(curPt->second, mdl, true, mdesc.simulation->accuracy);
		EquilibratePoint(&(curPt->second));
		curPt->second.ResetData();
		mdl.poissonRho[curPt->second.adj.self] = curPt->second.rho;
	}
#ifndef NDEBUG
	WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
	CompareRho(mdl, mdesc.simulation->OutputFName);
#endif
	DisplayProgress(47,0);
	

	if(mdesc.simulation->TransientData && mdesc.simulation->simType == SIMTYPE_TRANSIENT)
	{
		mdesc.simulation->TransientData->ResetAlterPercent = true;		//flag to update the percent alter.
		mdesc.simulation->TransientData->percentCarrierTransfer = 1.0e-5;	//we just got it close to the answer. Let's not significantly jar it!
	}
	return(0);
}


double GetEfZero(Point& pt, Model& mdl, Simulation& sim)
{
		double chargeAffectItself = GetPoissonDependency(&pt, &pt);
		unsigned long int index = pt.adj.self;
		//get the point in it's proper position according to the band diagram now that all the charge within it has been shifted over
		CalcPointBandFast(pt, mdl);
//		double startPsi = pt.Psi; unused	//this is the starting point for psi - DO NOT CHANGE
//		double startEc = pt.cb.bandmin; unused
//		double startEv = pt.vb.bandmin; unused

		double startRho = mdl.poissonRho[index];	//and the starting point for rho - DO NOT CHANGE
		double mintolerance = 1e-6;
		if(pt.eg < 0.05)
			mintolerance = 1e-10;	//this will take forever on the metals... but it is necessary.
		else if(pt.eg < 0.10)
			mintolerance = 5e-10;
		else if(pt.eg < 0.20)
			mintolerance = 1e-9;
		else if(pt.eg < 0.30)
			mintolerance = 1e-8;
		else if(pt.eg < 0.50)
			mintolerance = 1e-7;

		if(sim.accuracy > 1.0e-13 && sim.accuracy < mintolerance)
			mintolerance = sim.accuracy;


		double guessCharge = startRho;
//		bool prevsign = POSITIVE; unused
		double clamp = 0.25;	//it can only decrease in value
		double adjustPsi = 0.0;
		double prevadjustPsi = 0.0;
		double changeAdjust = 10.0;	//make sure prevChAdjust fails the first time through the do loop on the else if
		double prevChAdjust = 0.0;
//		double pprevChAdjust = 0.0; set, but unused
		double prevCharge = 0.0;
		double changeCharge = 0.0;
//		double poschangeAdjust = 1e10;	//make it well beyond the bounds of anything that should ever happen.
//		double negchangeAdjust = -1e10;
		bool keepgoing = true;
		double adjustToEc = -pt.MatProps->Phi;
		double adjustToEv = -pt.eg;
		double MaxPsi, MinPsi;
		MinPsi = 0.0;	//this puts the fermi level right at the top of the conduction band
		MaxPsi = -(adjustToEc + adjustToEc + adjustToEv);	//this puts the fermi level at the bottom of the valence band
		
//		double netCharge;
		double PsiZeroCharge = pt.Psi - startRho * chargeAffectItself;	//the base local vacuum level
		double PsiStart = pt.Psi;
		double PsiOrig = pt.Psi;
		double PsiCharge;
		//this loop should stop when rho stabilizes for a given changeAdjust 
		do
		{	//otherwise, keep going until bad things happen
//			prevCharge = guessCharge;
			PsiStart = pt.Psi;	//this is our initial guess at Psi
			guessCharge = GetPointRho(pt, 0.0);	//there is an associated charge with this value of Psi
			PsiCharge = PsiZeroCharge + chargeAffectItself * guessCharge;	//with this amount of charge, this is where it expects Psi to be
			
			//now we need to try and adjust PsiStart so it approaches PsiCharge
			
			/*
			chargeAffectItself is always negative. Otherwise everything would diverge.
			if PsiStart>PsiCharge, this means PsiCharge has more positive charge.
			But PsiCharge is where psi would be if it had the charge present in PsiStart
			Therefore, this means PsiStart has more negative charge than it needs.
			if PsiStart has too much negative charge, and Ef=0, that means Psi is too low
			*/
			if(DoubleEqual(PsiCharge, PsiStart, mintolerance))	//these are the same value, so it's done. A consistent solution has been found
			{
				pt.Psi = PsiCharge;
				keepgoing=false;
//				MaxPsi = MinPsi = PsiCharge;	//prevent overwrite a few lines down
			}
			else if(PsiStart < PsiCharge)
				MinPsi = PsiStart;
			else
				MaxPsi = PsiStart;

			//the next guess is going to be PsiCharge, unless it pushes it beyond whatever bounds are set
			if(PsiCharge < MaxPsi && PsiCharge > MinPsi)
				pt.Psi = PsiCharge;
			else
				pt.Psi = 0.5 * (MaxPsi + MinPsi);

			pt.cb.bandmin = pt.Psi + adjustToEc;
			pt.vb.bandmin = pt.cb.bandmin + adjustToEv;

			if(DoubleEqual(MaxPsi, MinPsi, mintolerance))
				keepgoing=false;

/*
keep comment block if doesn't work
			netCharge = guessCharge-startRho;
			prevadjustPsi = adjustPsi;
			prevChAdjust = changeAdjust;	//Infinite loops can actually happen where changeAdjust the CB/VB shift in equal and opposite directions continuously.
			adjustPsi = chargeAffectItself * netCharge;	//this is how much it needs to shift from it's starting psi position
			
			changeAdjust = adjustPsi - prevadjustPsi;	//it originally has the term from rho included within it

			if(changeAdjust > 0.0)
			{
				if(changeAdjust > clamp)
					clamp = clamp * 0.5;
			}
			if(changeAdjust < 0.0)
			{
				if(changeAdjust < -clamp)
					clamp = clamp * 0.5;
			}
	*/

/*
			if(changeAdjust >= clamp)
			{
				changeAdjust = clamp;
				pt.Psi +=  changeAdjust;
				pt.cb.bandmin += changeAdjust;
				pt.vb.bandmin += changeAdjust;
				adjustPsi = changeAdjust + prevadjustPsi;	//correct it for the next loop
				if(prevsign == NEGATIVE)
					clamp = clamp * 0.5;
				prevsign = POSITIVE;
				continue;	//it tried to move a lot... clamp it!
			}
			else if(changeAdjust <= -clamp)
			{
				changeAdjust = -clamp;
				pt.Psi +=  changeAdjust;
				pt.cb.bandmin += changeAdjust;
				pt.vb.bandmin += changeAdjust;
				adjustPsi = changeAdjust + prevadjustPsi; //correct it for the next loop
				if(prevsign == POSITIVE)	//it is alternating
					clamp = clamp * 0.5;
				prevsign = NEGATIVE;
				continue;	//it tried to move a lot... clamp it!
			}
			else if(fabs(prevChAdjust) <= fabs(changeAdjust))	//seems to be diverging a bit or in an infinite loop
			{
				clamp = fabs(prevChAdjust);	//put a clamp in that will be effective
				changeAdjust = prevChAdjust;	//and redo the loop
				adjustPsi = prevadjustPsi;
				continue;
			}

			if(changeAdjust > 0)
				prevsign = POSITIVE;
			else
				prevsign = NEGATIVE;
*/

			/*
			undo comment block if doesn't work

			if(fabs(changeAdjust + prevChAdjust) <= fabs(changeAdjust) * 1.0e-7)
			{
				clamp = 0.75 * fabs(changeAdjust);
			}
			if(changeAdjust > clamp || changeAdjust < -clamp)	//clamp it
			{
				if(fabs(prevChAdjust) <= fabs(changeAdjust))	//seems to be diverging a bit or in an infinite loop
				{
					clamp = 0.75 * fabs(prevChAdjust);	//put a clamp in that will be effective
				}

				if(changeAdjust > clamp)
					changeAdjust = clamp;
				if(changeAdjust < -clamp)
					changeAdjust = -clamp;

				adjustPsi = changeAdjust + prevadjustPsi;
			}

			pt.Psi += changeAdjust;
			pt.cb.bandmin += changeAdjust;
			pt.vb.bandmin += changeAdjust;

			//first, the rho's must be the same sign to be considered done
			if((guessCharge >= 0.0 && prevCharge >= 0.0) || (guessCharge <=0.0 && prevCharge <= 0.0))
			{	//they are the same sign - should fix some metal issues
				changeCharge= fabs(guessCharge - prevCharge);	//they are the same sign...
				//the charge cannot be changing very much between iterations
				if(changeCharge <= mintolerance * fabs(guessCharge) && changeCharge < 1.0)
				{
					//the adjustment to psi cannot be relatively changing very much.
					if(fabs(changeAdjust) <= mintolerance * fabs(adjustPsi))
					{
						keepgoing = false;
					}

				}	//essentially say to stop, the charge and band diagram may not be changing very much with each iteration

			}
			*/
		}while(keepgoing);	//this actually takes care of the infinite loop part.
		//in the case of a metal, this much of a tolerance will result in HUGE rho's.
		mdl.poissonRho[index] = guessCharge;
		pt.fermi = pt.Psi-PsiStart;	//the fermi level was calculated at zero for PsiStart, and then the band shifted a bit
		return(pt.Psi-PsiOrig);
}


double GetPoissonDependency(Point* sourcePt, Point* targetPt)
{
	//just loop through all the point's poisson dependencies. Can't really do a "fast" search for it
	unsigned long int targetID = targetPt->adj.self;

	if((unsigned long)targetID < sourcePt->pDsize)	//let's try to make an educated two guesses first to cut out massive for loops
	{
		long int difference = targetID - sourcePt->poissonDependency[targetID].pt;
		if(difference == 0)
		{
			return(sourcePt->poissonDependency[targetID].data);
		}
		difference += targetID;	//difference now holds the guess
		if((unsigned long)difference < sourcePt->pDsize && difference >= 0 && difference != EXT)
		{
			if(targetID == sourcePt->poissonDependency[difference].pt)
				return(sourcePt->poissonDependency[difference].data);
		}
	}

	for(unsigned int i=0; i < sourcePt->pDsize; i++)
	{
		if(sourcePt->poissonDependency[i].pt == targetID)
			return(sourcePt->poissonDependency[i].data);
	}


	return(0.0);

}

double GetPointRho(Point& pt, double Ef)	//presumably, Ef is going to be zero everhy time...
{
	double rho = 0.0;
	double kbTI = KBI / pt.temp;
	double tmpVal;

	bool oppOcc;	//set in CalcOccByFermiBand
	//get the amount of charge resulting from all the electrons and holes in the band diagram.
	for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt.cb.energies.begin(); cbit != pt.cb.energies.end(); ++cbit)
	{
		tmpVal = CalcOccByFermiBand(&(cbit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho-tmpVal : rho-cbit->second.GetNumstates()+tmpVal;
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt.vb.energies.begin(); vbit != pt.vb.energies.end(); ++vbit)
	{
		tmpVal = CalcOccByFermiBand(&(vbit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho+tmpVal : rho+vbit->second.GetNumstates()-tmpVal;
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.acceptorlike.begin(); esit != pt.acceptorlike.end(); ++esit)
	{
		tmpVal = CalcOccByFermiBand(&(esit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho+tmpVal : rho+esit->second.GetNumstates()-tmpVal;
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.donorlike.begin(); esit != pt.donorlike.end(); ++esit)
	{
		tmpVal = CalcOccByFermiBand(&(esit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho-tmpVal : rho-esit->second.GetNumstates()+tmpVal;
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.CBTails.begin(); esit != pt.CBTails.end(); ++esit)
	{
		tmpVal = CalcOccByFermiBand(&(esit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho-tmpVal : rho-esit->second.GetNumstates()+tmpVal;
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.VBTails.begin(); esit != pt.VBTails.end(); ++esit)
	{
		tmpVal = CalcOccByFermiBand(&(esit->second), Ef, kbTI, pt.cb.bandmin, pt.vb.bandmin, oppOcc);
		rho = (oppOcc==false) ? rho+tmpVal : rho+esit->second.GetNumstates()-tmpVal;
	}

	rho += pt.impdefcharge;	//don't forget all the charge from the impurities if there are any

	rho = rho * QCHARGE * pt.voli;

	return(rho);
}


/*
//note, this assumes the device has already had poisson's equation solved with the contacts at zero voltage
int ThermalEquilibrium2(ModelDescribe& mdesc, Model& mdl)
{
	if(mdl.numpoints == 0)
		return(0);
	DisplayProgress(46,0);
	Point* srcPt = NULL;
	Point* trgPt = NULL;
	double difFermiMax=0.0;
	double min = mdl.gridp[0].fermi;	//change in the min and max
	double max = min;
	double delta = 1.0;
	double FermiDifCutoff = 0.01;

	//range between 0.1 and 0.001

	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;
//	double chargeCoefficient;
	double targetChargeTransfer;
	long long oldSourceCharge;	//for reverting back if it's a bad change
	long long oldTargetCharge;
	long long changeCharge;
	double sourceFermi;	//fermi of the osurce point (always higher)
	double targetFermi;	//fermi level of the target point (always lower)
	unsigned long int ctrClearIgnore=0;
	double difOldFermi;
	


//	TreeRoot<long int, long int> IgnoreIndices;	//just in case it can't find it
	long int trgPti;
	long int srcPti;
	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	long int index=0;
	int delindex = 1;
	short state=CALCPX;
	double transferattemptconst = 0.05 * QI;	//attempt to transfer 0.05 rho out of the point
	bool allowChange = false;	//prevent changing states to start... enabled on increment of index
	do
	{
		if(mdl.gridp[index].fermi > max)
			max = mdl.gridp[index].fermi;
		if(mdl.gridp[index].fermi < min)
			min = mdl.gridp[index].fermi;

		switch(state)
		{
			case CALCSTART:
			case CALCPX:
				trgPti = mdl.gridp[index].adj.coordPx;
				break;
			case CALCMX:
				trgPti = mdl.gridp[index].adj.coordMx;
				break;
			case CALCPX2:
				trgPti = mdl.gridp[index].adj.coordPx2;
				break;
			case CALCMX2:
				trgPti = mdl.gridp[index].adj.coordMx2;
				break;
			case CALCPY:
				trgPti = mdl.gridp[index].adj.coordPy;
				break;
			case CALCMY:
				trgPti = mdl.gridp[index].adj.coordMy;
				break;
			case CALCPY2:
				trgPti = mdl.gridp[index].adj.coordPy2;
				break;
			case CALCMY2:
				trgPti = mdl.gridp[index].adj.coordMy2;
				break;
			case CALCTIME:
				time(&end);	//load the end time for dif
				timeelapse = difftime(end, start);
				if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
				{	//10 seconds
					delta = max-min;
					FermiDifCutoff = 0.01 * delta;
					DisplayProgressDoub(8, delta);
					start = end;
					WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
					max = mdl.gridp[0].fermi;	//reset max and min
					min = max;
				}
				allowChange = true;
				trgPti = EXT;
				break;
			default:
				trgPti = EXT;
				break;
		}
		
		srcPti = index;
	//	difFermiMax = FindMaxFermiDif(mdl, srcPti, trgPti, min, max, IgnoreIndices);
		
		if(trgPti != EXT)	//don't do anything if the target doesn't exist. We KNOW the source exists
		{

			if(mdl.gridp[trgPti].fermi > mdl.gridp[srcPti].fermi)	//always want to assume transferring electrons just to make things simple
			{
				long int tmp = trgPti;
				trgPti = srcPti;
				srcPti = tmp;
			}
			

			//the source has a higher fermi level, so the charge transfer is electrons to the target.
			srcPt = &(mdl.gridp[srcPti]);
			trgPt = &(mdl.gridp[trgPti]);
			sourceFermi = srcPt->fermi;
			targetFermi = trgPt->fermi;

			difOldFermi = sourceFermi - targetFermi;
			targetChargeTransfer = 0.0;
			if(difOldFermi > 0.005)	//ignore this point!
			{
				targetChargeTransfer = transferattemptconst * srcPt->vol;	//skip that calculation... just attempt to do 16, then 8, 4, 2, or 1
				if(targetChargeTransfer < 16.0)
					targetChargeTransfer = 16.0;
	
				FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
				FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
				FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
				FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);
	
				double adjustSource = SourceAffectSource - TargetAffectSource;	//if receiving a positive charge.
				double adjustTarget = SourceAffectTarget - TargetAffectTarget;
	
				double newsrcF;
				double newtrgF;
	
				oldSourceCharge = srcPt->netChargeCarriers;
				oldTargetCharge= trgPt->netChargeCarriers;
				do
				{
					srcPt->netChargeCarriers = oldSourceCharge;
					trgPt->netChargeCarriers = oldTargetCharge;
		
					targetChargeTransfer = ceil(targetChargeTransfer);	//always round up - only going a quarter of the way as is...
				
					changeCharge = (long long)targetChargeTransfer;
					srcPt->netChargeCarriers += changeCharge;
					trgPt->netChargeCarriers -= changeCharge;
						
					newsrcF = CalcFermiByNetCharge(srcPt,sourceFermi+0.05,targetFermi-0.05);	//source/target are max/min.
					newtrgF = CalcFermiByNetCharge(trgPt, sourceFermi+0.05, targetFermi-0.05);	// the +/- is to allow it to final answer outside
					//the previous bounds so if one exceeds the other, it may be caught
	
					newsrcF += adjustSource * targetChargeTransfer;	//now take into account band structure change
					newtrgF += adjustTarget * targetChargeTransfer;	//for both points.
				
					if(targetChargeTransfer <= 1.1)	//make sure it's 1.0... - last possible move
					{
						if(newsrcF < newtrgF)	//this is bad...
						{
//							difNewFermi = newsrcF - newtrgF;
//							if(difNewFermi < difOldFermi)	//accept the move because at least
//								break;			//the fermi levels are closer
							targetChargeTransfer = 0.0;
							break;
						}
						else	//still good move
							break;
		
						targetChargeTransfer = 0.0;	//it fails if it gets to this point
					}
					else if(newsrcF >=	newtrgF)	//if the transfer still has the source fermi higher
						break;	//it's good!

					targetChargeTransfer = 0.5 * targetChargeTransfer;	//cut it in half...
	
				}while(targetChargeTransfer > 0.75);	//failed or only transferred one carrier;
			
	
				if(targetChargeTransfer != 0.0)	//carriers successfully moved, apply the updates.
				{
					if(srcPt->contactData == NULL)
					{
						srcPt->fermi = newsrcF;	//this line MUST go first
						UpdateBD(srcPt, mdl, targetChargeTransfer);	//this shifts all the points due to the change in charge
					}
					else
						srcPt->netChargeCarriers = oldSourceCharge;
	
					if(trgPt->contactData == NULL)
					{
						trgPt->fermi = newtrgF;
						UpdateBD(trgPt, mdl, -targetChargeTransfer);
					}
					else
						trgPt->netChargeCarriers = oldTargetCharge;
				}	//end if carriers to transfer
			}	//end large enough fermi difference
		}	//end valid target point
		//now change the state
		if((index == 0 || index == mdl.numpoints-1) && allowChange)
		{
			allowChange = false;
			switch(state)
			{
				case CALCSTART:
					state = CALCPX;
					delindex = 1;
					index = 1;	//setup for next point - it would've skipped the increment before
					break;
				case CALCPX:
					state = CALCMX;
					delindex = -1;
					index = mdl.numpoints-1;
					break;
				case CALCMX:
					if(mdl.NDim == 1)
					{
						state = CALCTIME;
						delindex = 0;	///not sure if this matters, but just in case
					}
					else
					{
						state = CALCPX2;
						delindex = 1;
						index = 0;
					}
					break;
				case CALCPX2:
					state = CALCMX2;
					delindex = -1;
					index = mdl.numpoints -1;
					break;
				case CALCMX2:
					state = CALCPY;
					delindex = 1;
					index = 0;
					break;
				case CALCPY:
					state = CALCMY;
					delindex = -1;
					index = mdl.numpoints-1;
					break;
				case CALCMY:
					state= CALCPY2;
					delindex = 1;
					index = 0;
					break;
				case CALCPY2:
					state = CALCMY2;
					delindex = -1;
					index = mdl.numpoints-1;
					break;
				case CALCMY2:
					state = CALCTIME;
					delindex = 0;
					break;
				case CALCTIME:
					state = CALCPX;
					index = 0;
					delindex = 1;
					break;
				default:
					state = CALCPX;
					index = 0;
					delindex = 1;
					break;
			}
		}	//end time to change state
		else	//don't change index if it goes through a state change
		{
			allowChange = true;
			index += delindex;	//the loop increment and count back
		}


	}while(delta > 0.01);

	DisplayProgress(47,0);

	//At this point, it should be close enough to thermal equilibrium. Now initialize the point
	mdesc.simulation->netcharge = 0;
	for(unsigned int i=0; i<mdl.numpoints; i++)
	{
		InitializePoint(mdl.gridp[i]);
		mdesc.simulation->netcharge += mdl.gridp[i].netChargeCarriers;
	}

	WriteDeviceBand(mdl, mdesc.simulation->OutputFName);

	return(0);
}
*/
int UpdateBD(Point& pt, Model& mdl, double netChargeChange)	//only uses the net charge variable to determine the band structure
{	//whatmore, just calculates the change to all the other points and put shtem in
	double change;
//	unsigned long int index;
	for(unsigned int i=0; i < pt.PsiCf.size(); i++)	//add up all the contributions to psi from this point
	{
		if(pt.PsiCf[i].pt)
		{
			change = pt.PsiCf[i].data * netChargeChange;
			pt.PsiCf[i].pt->Psi += change;
			pt.PsiCf[i].pt->cb.bandmin += change;
			pt.PsiCf[i].pt->vb.bandmin += change;
			pt.PsiCf[i].pt->fermi += change;
		}
	}
	//psi is now in the proper location

	return(0);
}


int InitializePoint(Point& pt)
{
	//it should have a proper Psi, a proper rho, and other schtuff...
	if(pt.MatProps->type != VACUUM)
	{
		pt.fermin = pt.fermi;	//this may not be true in the future if I get a initialization capable of handling currents
		pt.fermip = pt.fermi;
		
		double occupancy;
		double kbTI = 1.0 / (KB * pt.temp);
		bool oppOcc;	//set in CalcOccByFermiBand
		pt.cb.SetNumParticles(0.0);

		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt.cb.energies.begin(); cbit != pt.cb.energies.end(); ++cbit)
		{
			occupancy = CalcOccByFermiBand(&(cbit->second), pt.fermin, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			//occupancy = ProbFraction(occupancy);
			if(oppOcc==false)
			{
				cbit->second.SetEndOccStates(occupancy);
				cbit->second.SetBeginOccStates(occupancy);	
				cbit->second.SetPrevOcc(occupancy);
				cbit->second.SetThermalOcc(occupancy);
			}
			else
			{
				cbit->second.SetEndOccStatesOpposite(occupancy);
				cbit->second.SetBeginOccStatesOpposite(occupancy);
				cbit->second.SetPrevOccOpposite(occupancy);
				cbit->second.SetThermalAvail(occupancy);
				occupancy = cbit->second.GetBeginOccStates();	//this will return the proper number no matter what
			}

			pt.cb.AddNumParticles(occupancy);
			SetabsEUseFromFermi(&(cbit->second), pt.fermi);
		}
		pt.n = pt.cb.GetNumParticles();

		pt.vb.SetNumParticles(0.0);
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt.vb.energies.begin(); vbit != pt.vb.energies.end(); ++vbit)
		{
			occupancy = CalcOccByFermiBand(&(vbit->second), pt.fermip, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			if(oppOcc==false)
			{
				vbit->second.SetEndOccStates(occupancy);
				vbit->second.SetBeginOccStates(occupancy);
				vbit->second.SetPrevOcc(occupancy);
				vbit->second.SetThermalOcc(occupancy);
			}
			else
			{
				vbit->second.SetEndOccStatesOpposite(occupancy);
				vbit->second.SetBeginOccStatesOpposite(occupancy);
				vbit->second.SetPrevOccOpposite(occupancy);
				vbit->second.SetThermalAvail(occupancy);
				occupancy = vbit->second.GetBeginOccStates();
			}
			pt.vb.AddNumParticles(occupancy);
			SetabsEUseFromFermi(&(vbit->second), pt.fermi);
		}
		pt.p = pt.vb.GetNumParticles();

		pt.elecindonor = 0.0;
		pt.holeinacceptor = 0.0;
		pt.elecTails = 0.0;
		pt.holeTails = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.acceptorlike.begin(); esit != pt.acceptorlike.end(); ++esit)
		{
			occupancy = CalcOccByFermiBand(&(esit->second), pt.fermi, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			if(oppOcc==false)
			{
				esit->second.SetEndOccStates(occupancy);
				esit->second.SetBeginOccStates(occupancy);
				esit->second.SetPrevOcc(occupancy);
				esit->second.SetThermalOcc(occupancy);
			}
			else
			{
				esit->second.SetEndOccStatesOpposite(occupancy);
				esit->second.SetBeginOccStatesOpposite(occupancy);
				esit->second.SetPrevOccOpposite(occupancy);
				esit->second.SetThermalAvail(occupancy);
				occupancy = esit->second.GetBeginOccStates();
			}
			pt.holeinacceptor += occupancy;
			SetabsEUseFromFermi(&(esit->second), pt.fermi);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.donorlike.begin(); esit != pt.donorlike.end(); ++esit)
		{
			occupancy = CalcOccByFermiBand(&(esit->second), pt.fermi, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			if(oppOcc==false)
			{
				esit->second.SetEndOccStates(occupancy);
				esit->second.SetBeginOccStates(occupancy);
				esit->second.SetPrevOcc(occupancy);
				esit->second.SetThermalOcc(occupancy);
			}
			else
			{
				esit->second.SetEndOccStatesOpposite(occupancy);
				esit->second.SetBeginOccStatesOpposite(occupancy);
				esit->second.SetPrevOccOpposite(occupancy);
				esit->second.SetThermalAvail(occupancy);
				occupancy = esit->second.GetBeginOccStates();
			}
			pt.elecindonor += occupancy;
			SetabsEUseFromFermi(&(esit->second), pt.fermi);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.CBTails.begin(); esit != pt.CBTails.end(); ++esit)
		{
			occupancy = CalcOccByFermiBand(&(esit->second), pt.fermi, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			if(oppOcc==false)
			{
				esit->second.SetEndOccStates(occupancy);
				esit->second.SetBeginOccStates(occupancy);
				esit->second.SetPrevOcc(occupancy);
				esit->second.SetThermalOcc(occupancy);
			}
			else
			{
				esit->second.SetEndOccStatesOpposite(occupancy);
				esit->second.SetBeginOccStatesOpposite(occupancy);
				esit->second.SetPrevOccOpposite(occupancy);
				esit->second.SetThermalAvail(occupancy);
				occupancy = esit->second.GetBeginOccStates();
			}
			pt.elecTails += occupancy;
			SetabsEUseFromFermi(&(esit->second), pt.fermi);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator esit = pt.VBTails.begin(); esit != pt.VBTails.end(); ++esit)
		{
			occupancy = CalcOccByFermiBand(&(esit->second), pt.fermi, kbTI, pt.cb.bandmin, pt.vb.bandmin,oppOcc);
			if(oppOcc==false)
			{
				esit->second.SetEndOccStates(occupancy);
				esit->second.SetBeginOccStates(occupancy);
				esit->second.SetPrevOcc(occupancy);
				esit->second.SetThermalOcc(occupancy);
			}
			else
			{
				esit->second.SetEndOccStatesOpposite(occupancy);
				esit->second.SetBeginOccStatesOpposite(occupancy);
				esit->second.SetPrevOccOpposite(occupancy);
				esit->second.SetThermalAvail(occupancy);
				occupancy = esit->second.GetBeginOccStates();
			}
			pt.holeTails += occupancy;
			SetabsEUseFromFermi(&(esit->second), pt.fermi);
		}

		pt.ResetData();
		

		
	}
	else
	{
		pt.cb.bandmin = pt.Psi;
		pt.vb.bandmin = pt.Psi;
		pt.fermi = pt.Psi;
		pt.fermin = pt.Psi;
		pt.fermip = pt.Psi;
		pt.rho = 0.0;
	}

	return(0);
}

double CalcOccByFermiBand(ELevel* eLev, double Fermi, double kbTi, double CBMin, double VBMin, bool& oppOcc)
{
	double exponent;
	double Charge;
	oppOcc = false;
	if(eLev->imp != NULL)
	{
		if(eLev->imp->isCBReference())
		{
			exponent = (CBMin - eLev->eUse) - Fermi;
			exponent = exponent * kbTi;
			if(exponent > -4.6)	//this is an OK calculation to do
				exponent = exp(exponent) * eLev->imp->getDegeneracy() + 1.0;
			else
			{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation.
				if (eLev->imp->getDegeneracy() != 0.0)
					exponent = exp(-exponent) / eLev->imp->getDegeneracy() + 1.0;
				else
					exponent = exp(-exponent) + 1.0;
			
				oppOcc = true;
			}
		}
		else
		{
			exponent = Fermi - (VBMin + eLev->eUse);
			exponent = exponent * kbTi;

			if(exponent > -4.6)	//this is an OK calculation to do
				exponent = exp(exponent) / eLev->imp->getDegeneracy() + 1.0;
			else
			{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation, which is actually the number of electrons
				if (eLev->imp->getDegeneracy() != 0.0) //this just inverses the energy, and flips the degeneracy over
					exponent = exp(-exponent) * eLev->imp->getDegeneracy() + 1.0;
				else
					exponent = exp(-exponent) + 1.0;
			
				oppOcc = true;
			}
		}

		
		
	}
	else if(eLev->carriertype == ELECTRON && eLev->bandtail == false)
	{
		exponent = (eLev->eUse + CBMin) - Fermi;

		exponent = exponent * kbTi;
		if(exponent > -4.6)	//this is an OK calculation to do
			exponent = exp(exponent) + 1.0;
		else
		{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation.
			exponent = exp(-exponent) + 1.0;
			oppOcc = true;
		}
	}
	else if(eLev->carriertype == ELECTRON && eLev->bandtail == true)
	{
		exponent = (CBMin - eLev->eUse) - Fermi;
		exponent = exponent * kbTi;
		if(exponent > -4.6)	//this is an OK calculation to do
			exponent = exp(exponent) + 1.0;
		else
		{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation.
			exponent = exp(-exponent) + 1.0;
			oppOcc = true;
		}
	}
	else if(eLev->carriertype == HOLE && eLev->bandtail == false)
	{
//		return(CalculateOccFromFermi(eLev, Fermi));	//this looks at the entire band and integrates a boltzmann function over it
		//to get a value for the occupancy.

		exponent = Fermi - (VBMin - eLev->eUse);
		exponent = exponent * kbTi;
		if(exponent > -4.6)	//this is an OK calculation to do
			exponent = exp(exponent) + 1.0;
		else
		{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation.
			exponent = exp(-exponent) + 1.0;
			oppOcc = true;
		}
	}
	else //it's a hole and band tail
	{
		exponent = Fermi - (VBMin + eLev->eUse);
		exponent = exponent * kbTi;
		if(exponent > -4.6)	//this is an OK calculation to do
			exponent = exp(exponent) + 1.0;
		else
		{
				//this is going to be approaching 1.0, or the exponent will have basically no effect
				//therefore, calculate the reverse occupation.
			exponent = exp(-exponent) + 1.0;
			oppOcc = true;
		}
	}
	

	Charge = eLev->GetNumstates() / exponent;
	
	return(Charge);
}
/*
double CalcFermiByNetCharge(Point* pt, double maxFermi, double minFermi)
{
	if(pt->MatProps->type == VACUUM)	//it shouldn't change - this point can't receive carriers
		return(0);

	double targetchargemin = (double)pt->netChargeCarriers;
	double targetchargemax = targetchargemin + 0.5;
	targetchargemin -= 0.5;
	double pointNetCharge = 0.0;	//what would the new net charge be

	vector<ELevel*> CBStates;
	vector<ELevel*> VBStates;
	vector<ELevel*> EStates;

	pt->cb.energies.GenerateSortPtr(CBStates);
	pt->vb.energies.GenerateSortPtr(VBStates);
	pt->extrastates.GenerateSortPtr(EStates);

	unsigned int cbSize = pt->cb.energies.size;
	unsigned int vbSize = pt->vb.energies.size;
	unsigned int esSize = pt->extrastates.size;

	double constCharge = pt->impdefcharge;

	double GuessFermi = 0.5*(maxFermi + minFermi);
	double prevGuess;
	double kbTI = KB * pt->temp;
	kbTI = 1.0 / kbTI;

	/_*
	pointNetCharge = constCharge;	//reset to the point's charge - check the extremities
	for(unsigned int i=0; i<cbSize; i++)
		pointNetCharge -= CalcOccByFermiBand(CBStates[i], maxFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	for(unsigned int i=0; i<vbSize; i++)
		pointNetCharge += CalcOccByFermiBand(VBStates[i], maxFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	for(unsigned int i=0; i<esSize; i++)
		pointNetCharge -= CalcOccByFermiBand(EStates[i], maxFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	//this tested the more negative end of the charge
	if(pointNetCharge >= targetchargemax)	//but there was too much positive charge
	{
		return(-1);	//indicate failure and that a smaller amount needs to be transferred
	}

	pointNetCharge = constCharge;	//reset to the point's charge - check the extremities
	for(unsigned int i=0; i<cbSize; i++)
		pointNetCharge -= CalcOccByFermiBand(CBStates[i], minFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	for(unsigned int i=0; i<vbSize; i++)
		pointNetCharge += CalcOccByFermiBand(VBStates[i], minFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	for(unsigned int i=0; i<esSize; i++)
		pointNetCharge -= CalcOccByFermiBand(EStates[i], minFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

	//this tested the more positive end, and it was too negative
	if(pointNetCharge <= targetchargemin)
	{
		return(-1);	//indicate failure and that a smaller amount needs to be transferred for it to be a safe move
	}
* /
	double difmaxmin;
	do
	{
		prevGuess=GuessFermi;
		pointNetCharge = constCharge;	//reset to the point's charge
		for(unsigned int i=0; i<cbSize; i++)
			pointNetCharge -= CalcOccByFermiBand(CBStates[i], GuessFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

		for(unsigned int i=0; i<vbSize; i++)
			pointNetCharge += CalcOccByFermiBand(VBStates[i], GuessFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

		for(unsigned int i=0; i<esSize; i++)
			pointNetCharge -= CalcOccByFermiBand(EStates[i], GuessFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin);

		if(pointNetCharge >= targetchargemax)	//there is too much positive charge in the point, it needs to set a new guess
		{	//that has a higher fermi level
			minFermi = GuessFermi;
		}
		if(pointNetCharge <= targetchargemin)	//there is too much negative charge in the point, it needs to set a new guess
		{	//that has a lower fermi level
			maxFermi = GuessFermi;
		}
		GuessFermi = 0.5*(minFermi + maxFermi);
		difmaxmin = maxFermi - minFermi;	//check if it's close enough for government work...
	}while((pointNetCharge <= targetchargemin || pointNetCharge >= targetchargemax) && difmaxmin > 0.0001);


	return(GuessFermi);
}
*/
/*
double FindMaxFermiDif(Model& mdl, long int& srcPti, long int& trgPti, double& min, double& max)
{
	double difFermiTemp;	//temporary value used for calculating differences in fermi levels
	double difFermiMax = 0.0;
	int adj, adj2;

	max = mdl.gridp[0].fermi;
	min = max;
	Point* srcPt = &(mdl.gridp[srcPti]);
	Point* trgPt = &(mdl.gridp[trgPti]);
	long int maxPt=0;
	long int minPt=0;

	for(unsigned long int i=0; i<mdl.numpoints; i++)
	{

		if(mdl.gridp[i].MatProps->type == VACUUM)	//vacuum, will have very high fermi, ignore this point
			continue;

		if(mdl.gridp[i].fermi < min)
		{
			min = mdl.gridp[i].fermi;
			minPt = i;
		}

		if(mdl.gridp[i].fermi > max)
		{
			max = mdl.gridp[i].fermi;
			maxPt = i;
		}
		//////////// CHECK MX
		adj = mdl.gridp[i].adj.coordMx;
		adj2 = mdl.gridp[i].adj.coordMx2;
		if(adj != EXT)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj;
			}
		}
		if(adj2 != EXT && adj2 != adj)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj2].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj2;
			}
		}

		//////////////////CHECK PX

		adj = mdl.gridp[i].adj.coordPx;
		adj2 = mdl.gridp[i].adj.coordPx2;
		if(adj != EXT)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj;
			}
		}
		if(adj2 != EXT && adj2 != adj)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj2].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj2;
			}
		}


		//////////// CHECK MY
		adj = mdl.gridp[i].adj.coordMy;
		adj2 = mdl.gridp[i].adj.coordMy2;
		if(adj != EXT)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj;
			}
		}
		if(adj2 != EXT && adj2 != adj)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj2].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj2;
			}
		}

		//////////////////CHECK PY

		adj = mdl.gridp[i].adj.coordPy;
		adj2 = mdl.gridp[i].adj.coordPy2;
		if(adj != EXT)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj;
			}
		}
		if(adj2 != EXT && adj2 != adj)
		{
			difFermiTemp = mdl.gridp[i].fermi - mdl.gridp[adj2].fermi;	//this checks all directions, so no need for fabs
			if(difFermiTemp > difFermiMax)
			{
				difFermiMax = difFermiTemp;
				srcPti = i;
				trgPti = adj2;
			}
		}
	}
	srcPti = maxPt;
	trgPti = minPt;
	return(difFermiMax);
}
*/

	
double CalcPointBandFast(Point& pt, Model& mdl, bool IncludeFermi, double accuracy)
{
	double change = pt.Psi;
	pt.Psi = 0.0;	//reset Psi for the calculation

	for(unsigned int i=0; i < pt.pDsize; i++)	//add up all the contributions to psi
	{
		if(pt.poissonDependency[i].pt < mdl.numpoints)
			pt.Psi += pt.poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl.poissonRho[pt.poissonDependency[i].pt];	//what that rho/BC is
		else	//this is a boundary condition point. Just pull the particular boundary condition in from a given point
		{	//the boundary condition values area always stored with the point j+mdl.numpoints. Therefore, look for the voltage
			//on the particular contact point
			std::map<unsigned long int, BoundaryCondition>::iterator ContactPsi = mdl.poissonBC.find(pt.poissonDependency[i].pt);
			if(ContactPsi != mdl.poissonBC.end())
				pt.Psi += pt.poissonDependency[i].data * ContactPsi->second.value;

		}
	}
	//psi is now in the proper location
	change = pt.Psi - change;	//new psi - old psi
	if(pt.MatProps->type != VACUUM)
	{
		pt.cb.bandmin = pt.Psi - pt.MatProps->Phi;	//now put Ec
		pt.vb.bandmin = pt.cb.bandmin - pt.MatProps->Eg;	//and Ev in their places
	
		//recalculate fermi levels for the associated energies, but not the point as a whole (that is irrelevant, and numerically solved)
		pt.fermin += change;
		pt.fermip += change;
		if(IncludeFermi)
			FindFermiPt(pt, accuracy);	//just get Ef loaded - no need for Efn or Efp.
		else
			pt.fermi += change;
	}
	else
	{
		pt.cb.bandmin = pt.Psi;
		pt.vb.bandmin = pt.Psi;
		pt.fermin = pt.Psi;
		pt.fermi = pt.Psi;
		pt.fermip = pt.Psi;
	}

	return(0.0);
}


// This function find the thermal equilibrium point for the carrier to speed up the convergence process, and stores in endoccstates
int EquilibratePoint(Point* pt)
{
	//first get all the energy states and sum up the total number of carriers in the point
	//assign a rough fermi energy.
	//calculate the percentage of electrons which should be present in each of the levels.
	//if too many electrons were placed, move the fermi level down
	//if not enough electrons were placed, move the fermi level up
	//if the exact number of electrons are placed, keep this distribution and set all fermi levels at this point.

	std::vector<ELevel*> EnergyStates;


	double tooHighFermi = pt->Psi + 2.0;	//the lowest fermi level which is too high, so keep pushing it down.
	double tooLowFermi;
	if (pt->MatProps->type != VACUUM)
		tooLowFermi = pt->Psi - pt->eg - pt->MatProps->Phi - pt->MatProps->Phi - 2.0;	//2ev below the bottom of the valence band
	else
		tooLowFermi = pt->Psi - 30.0;	//something that should be below...

	double currentFermi = 0.5 * (tooHighFermi + tooLowFermi);	//the present guess for the fermi level
	double numelec = 0.0;	//total number of electrons
	double numhole = 0.0;
	double usedelec = 0.0;	//the number of electrons used in this cycle.
	double usedhole = 0.0;
	double totelec = 0.0;
	double numelecMhole;

	EnergyStates.reserve(pt->cb.energies.size() + pt->vb.energies.size() + pt->donorlike.size() + pt->acceptorlike.size() + pt->CBTails.size() + pt->VBTails.size());

	//DO NOT CHANGE ORDER OF THESE FOR LOOPS - WANT STATES PRODUCING ELECTRONS TO OCCUR FIRST
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->cb.energies.begin(); it != pt->cb.energies.end(); ++it)
		EnergyStates.push_back(&(it->second));
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->donorlike.begin(); it != pt->donorlike.end(); ++it)
		EnergyStates.push_back(&(it->second));
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->CBTails.begin(); it != pt->CBTails.end(); ++it)
		EnergyStates.push_back(&(it->second));
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->vb.energies.begin(); it != pt->vb.energies.end(); ++it)
		EnergyStates.push_back(&(it->second));
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->acceptorlike.begin(); it != pt->acceptorlike.end(); ++it)
		EnergyStates.push_back(&(it->second));
	for (std::map<MaxMin<double>, ELevel>::iterator it = pt->VBTails.begin(); it != pt->VBTails.end(); ++it)
		EnergyStates.push_back(&(it->second));


	unsigned int numstates = EnergyStates.size();
	double totalstates = 0.0;
	std::vector<double> EStateSize;	//just a vector to hold numstates
	std::vector<double> EOccupy;		//a vector to hold the number of electrons to be in each state. Just trying to remove
	std::vector<double> Energies;
	//a number of pointer references and function access calls. Local temp quick access

	EStateSize.resize(numstates);
	EOccupy.resize(numstates);
	Energies.resize(numstates);

	for (unsigned int i = 0; i<numstates; i++)
	{
		EStateSize[i] = EnergyStates[i]->GetNumstates();
		totalstates += EStateSize[i];
		if (EnergyStates[i]->carriertype == HOLE)
		{
			if (EnergyStates[i]->imp != NULL || EnergyStates[i]->bandtail == true)
			{
				Energies[i] = pt->vb.bandmin + EnergyStates[i]->eUse;
			}
			else
			{
				Energies[i] = pt->vb.bandmin - EnergyStates[i]->eUse;
			}
			if (EnergyStates[i]->imp != NULL)
				numhole += EStateSize[i];	//if it's the valence band or band tails, so these energy states contribute electrons - balance the number of electrons and holes by fermi level
			else
				totelec += EStateSize[i];
		}
		else	//this could be CB or an impurity.
		{

			if (EnergyStates[i]->imp != NULL || EnergyStates[i]->bandtail)	//it is an impurity or band tail
			{
				Energies[i] = pt->cb.bandmin - EnergyStates[i]->eUse;
			}
			else //it is an impurity
			{
				Energies[i] = pt->cb.bandmin + EnergyStates[i]->eUse;
			}
			if (EnergyStates[i]->imp != NULL)	//electrons only added in donor states - not cb or band tails
			{
				numelec += EStateSize[i];
				totelec += EStateSize[i];
			}
		}
	}

	double fermiprob = 0.0;
	double kTi = 0.0;
	bool absZero = false;

	if (pt->temp > 0.0)
		kTi = KBI / pt->temp;
	else
		absZero = true;

	double difference = 0.0;
	double degen = 1.0;
	numelecMhole = numelec - numhole;	//essentially, what is the charge?
	if (totelec > 0.0 && totelec < totalstates)	//if numelec = totalstates or 0, special/fast calculation,
	{
		while (!DoubleEqual(tooHighFermi, tooLowFermi))	//keep going while the number of electrons in the point does not equal the number of electrons allotted
		{	//this loop exits due to an if statement after the calculations. Further processing occurs afterwards if it can continue

			usedelec = 0.0;
			usedhole = 0.0;
			for (unsigned int i = 0; i<numstates; i++)
			{
				if (EnergyStates[i]->imp)
					degen = (double)EnergyStates[i]->imp->getDegeneracy();
				else
					degen = 1.0;
				if (EnergyStates[i]->carriertype == ELECTRON)
				{
					fermiprob = FermiFn(currentFermi, Energies[i], kTi, false, absZero, degen);
					EOccupy[i] = fermiprob * EStateSize[i];
					usedelec += EOccupy[i];
				}
				else
				{
					fermiprob = FermiFn(currentFermi, Energies[i], kTi, true, absZero, degen);	//true means 1 - f(E)
					EOccupy[i] = fermiprob * EStateSize[i];
					usedhole += EOccupy[i];
					usedelec += FermiFn(currentFermi, Energies[i], kTi, false, absZero, degen) * EStateSize[i];	//new methodology of getting the difference in holes and electrons to match
				}
				//the first rates will all increase usedelec, and the last half of rates will all increase usedhole.
				//the last half makes the difference go more negative. So if it is already too negative, just get out.
				//				if(usedelec - usedhole < numelecMhole && EnergyStates[i]->carriertype == HOLE)	//get out of this calculation right away
				//					break;
			}
			difference = DoubleSubtract(usedelec, totelec); //this will knock down to 0 if it's close enough
			//difference is the 
			if (difference == 0.0)	//could it just have been probability induced and it's actually really close to being correct?
				break;	//get out of the loop. Found the right fermi level to a close enough approximation.
			else if (difference > 0.0)	//the fermi level is too high - it must be lower, max
				tooHighFermi = currentFermi;
			else //the fermi level is too low - it must be higher, min
				tooLowFermi = currentFermi;

			currentFermi = 0.5 * (tooLowFermi + tooHighFermi);
		}
		if (fabs(difference) > TRANSFER_ABSOLUTE_MIN)	//we have a problem.
		{
			std::multimap<double, int> Fix;
			double change;	//how much that level needs to change to be accurate
			for (unsigned int i = 0; i<numstates; i++)
			{
				if (EnergyStates[i]->imp)
					degen = (double)EnergyStates[i]->imp->getDegeneracy();
				else
					degen = 1.0;

				if (EnergyStates[i]->carriertype == ELECTRON)
				{
					fermiprob = FermiFn(currentFermi, Energies[i], kTi, false, absZero, degen);
				}
				else
				{
					fermiprob = FermiFn(currentFermi, Energies[i], kTi, true, absZero, degen);
				}

				change = EOccupy[i] - fermiprob * EStateSize[i];	//a large value means it has too much in there
				Fix.insert(std::pair<double, int>(change, i));	//want it sorted by the largest difference.
			}


			std::multimap<double, int>::iterator itSmall;
			std::multimap<double, int>::reverse_iterator itLarge;
			//because this is probability(rounding) based, it should only ever be off by one carrier
			int index;

			while (difference > TRANSFER_ABSOLUTE_MIN || difference < -TRANSFER_ABSOLUTE_MIN)	//there are too many electrons in there
			{
				itLarge = Fix.rbegin();	//grab the energy level that is furthest too much
				if (itLarge != Fix.rend())
				{
					index = itLarge->second;
					change = itLarge->first;

					if (change > 0.0 && EnergyStates[index]->carriertype == ELECTRON)
					{
						difference -= change;
						EOccupy[index] -= change;
					}
					else if (change < 0.0 && EnergyStates[index]->carriertype == HOLE)
					{
						difference += change;
						EOccupy[index] += change;
					}
					Fix.erase(std::next(itLarge).base());	//reverse iterators are weird.
					//need to do base to get the forward iterator equivalent, but reverse iterators
					//physically point to one element greater, and when it reads, it reads the element before.
					//therefore, the next puts it to be one less, so it physically points to the iterator when dereferenced, and the base
					//puts it in proper forward iterator form
					//remember, rbegin actually points to end()!! so the next one points to the last entry
				}
				else //there were no nodes left
					break; // get out of the while loop

				itSmall = Fix.begin();	//grab the energy level that is furthest hole wise
				if (itSmall != Fix.end())
				{
					index = itSmall->second;
					change = itSmall->first;

					if (change > 0.0 && EnergyStates[index]->carriertype == ELECTRON)
					{
						difference -= change;
						EOccupy[index] -= change;
					}
					else if (change < 0.0 && EnergyStates[index]->carriertype == HOLE)
					{
						difference += change;
						EOccupy[index] += change;
					}
					Fix.erase(itSmall);
				}
				else //there were no nodes left
					break; // get out of the while loop
			}	//end while

			Fix.clear();	//make sure that memory gets cleared
		}	//end if difference is too large
	}	//end if normal usage (0 < number electrons < number of states
	else if (numelec == totalstates)	//set the fermi level so high that every state is guaranteed to be full
	{
		currentFermi = pt->Psi + 0.0029763248*pt->temp;	//The starting energy is the very top of the band
		//this seemingly random number assumes the occupancy is just a hair under the total occupancy. To keep the math nice,
		//that hair is 10^-15 below the total occupancy (double is accurate to ~16 digits). This function starts off from the fermi distribution
		//and solves ity backwards assuming the probability of occupancy is essentially 1-1e-15. All the constants are rolled into that 0.00297...

		tooHighFermi = currentFermi;
		tooLowFermi = currentFermi;
		for (unsigned int i = 0; i<numstates; i++)
			EOccupy[i] = EStateSize[i];

		difference = 0.0;
	}
	else
	{
		//now the number of electrons is zero in the material.
		currentFermi = tooLowFermi + 2.0 + 0.0029763248*pt->temp;	//get it to the bottom of the VB
		tooHighFermi = currentFermi;
		tooLowFermi = currentFermi;
		for (unsigned int i = 0; i<numstates; i++)
			EOccupy[i] = 0.0;
		difference = 0.0;
	}
	//now put the difference in starting at the bottom of the valence band to maintain charge neutrality. This assumes there will be an
	//opening in the valence band, and then tries extra states. If it gets beyond those two, it unfortunately looks at the top of the
	//conduction band. But this is just to get close to a thermal equilibrium point for initialization, so it shouldn't be bad.

	/*
	if(difference < 0.0)	// not enough electrons were placed, so place them in the valence band, starting at the bottom
	{
	unsigned int cbsize = pt->cb.energies.size;	//first group in EStates
	unsigned int vbsize = pt->vb.energies.size;  //last group in EStates
	unsigned int essize = pt->extrastates.size;  //middle of EStates
	//first, go through the valence band to see if you can add electrons starting at the bottom of the band
	for(unsigned int i=cbsize+vbsize+essize-1; i >= cbsize + essize && i < cbsize+vbsize+essize; i--)
	{
	if(difference >= 0.0)
	break;
	double open = DoubleSubtract(EStateSize[i],EOccupy[i]);
	if(open > 0.0)
	{
	if(-difference >= open)
	{
	EOccupy[i] = EStateSize[i];
	difference += open;
	}
	else
	{
	EOccupy[i] -= difference;	//note, difference is negative, and we're trying to add electrons.
	difference = 0.0;
	break;
	}
	}
	}
	//next, check the extra states. No particular order here in extra states.
	for(unsigned int i=cbsize; i<cbsize + essize; i++)
	{
	if(difference >= 0.0)
	break;
	double open = DoubleSubtract(EStateSize[i],EOccupy[i]);
	if(open > 0.0)
	{
	if(-difference >= open)
	{
	EOccupy[i] = EStateSize[i];
	difference += open;
	}
	else
	{
	EOccupy[i] -= difference;
	difference = 0.0;
	break;
	}
	}
	}
	//now check the conduction band, trying to add electrons to the bottom of it
	for(unsigned int i=0; i<cbsize; i++)
	{
	if(difference >= 0.0)
	break;
	double open = DoubleSubtract(EStateSize[i],EOccupy[i]);
	if(open > 0.0)
	{
	if(-difference >= open)
	{
	EOccupy[i] = EStateSize[i];
	difference += open;
	}
	else
	{
	EOccupy[i] -= difference;
	difference = 0.0;
	break;
	}
	}
	}
	}
	else if(difference > 0.0)	//there were too many electrons placed. Remove them starting at the top of the band
	{
	unsigned int cbsize = pt->cb.energies.size;	//first group in EStates
	unsigned int vbsize = pt->vb.energies.size;  //last group in EStates
	unsigned int essize = pt->extrastates.size;  //middle of EStates

	for(unsigned int i=cbsize-1; i<cbsize; i--)	//this will pop up really high when it reaches 0 due to unsigned
	{
	if(difference <= 0.0)
	break;

	if(EOccupy[i] > 0.0)
	{
	if(difference >= EOccupy[i])
	{
	difference -= EOccupy[i];	//knock down the difference
	EOccupy[i] = 0;
	}
	else
	{
	EOccupy[i] -= difference;
	difference = 0.0;
	break;
	}
	}
	}
	//now check the impurities
	for(unsigned int i=cbsize; i<cbsize + essize; i++)	//this will pop up really high when it reaches 0 due to unsigned
	{
	if(difference <= 0.0)
	break;

	if(EOccupy[i] > 0.0)
	{
	if(difference >= EOccupy[i])
	{
	difference -= EOccupy[i];	//knock down the difference
	EOccupy[i] = 0;
	}
	else
	{
	EOccupy[i] -= difference;
	difference = 0.0;
	break;
	}
	}
	}
	//now remove electrons starting at the top of the VB
	for(unsigned int i=cbsize + essize; i<cbsize + essize + vbsize; i++)	//this will pop up really high when it reaches 0 due to unsigned
	{
	if(difference <= 0.0)
	break;

	if(EOccupy[i] > 0.0)
	{
	if(difference >= EOccupy[i])
	{
	difference -= EOccupy[i];	//knock down the difference
	EOccupy[i] = 0;
	}
	else
	{
	EOccupy[i] -= difference;
	difference = 0.0;
	break;
	}
	}
	}
	}
	*/
	//now put this data back into the energy levels...
	pt->cb.SetNumParticles(0.0);	//clear out any old data - this might not necessarily have been called from a blank point
	pt->vb.SetNumParticles(0.0);
	pt->elecindonor = 0.0;
	pt->elecTails = 0.0;
	pt->holeinacceptor = 0.0;
	pt->holeTails = 0.0;
	bool oppOcc;
	double kbTI = KBI / pt->temp;
	for (unsigned int i = 0; i<numstates; i++)
	{
		double occ = CalcOccByFermiBand(EnergyStates[i], currentFermi, kbTI, pt->cb.bandmin, pt->vb.bandmin, oppOcc);
		EnergyStates[i]->AbsFermiEnergy = currentFermi;
		if (oppOcc == false)
		{
			EnergyStates[i]->SetEndOccStates(occ);
			EnergyStates[i]->SetBeginOccStates(occ);
			EnergyStates[i]->SetPrevOcc(occ);
			EnergyStates[i]->SetThermalOcc(occ);
		}
		else
		{
			EnergyStates[i]->SetEndOccStatesOpposite(occ);
			EnergyStates[i]->SetBeginOccStatesOpposite(occ);
			EnergyStates[i]->SetPrevOccOpposite(occ);
			EnergyStates[i]->SetThermalAvail(occ);
			occ = EnergyStates[i]->GetBeginOccStates();	//this will return the proper number no matter what
		}

		if (EnergyStates[i]->carriertype == HOLE)
		{
			if (EnergyStates[i]->imp)
				pt->holeinacceptor += occ;
			else if (EnergyStates[i]->bandtail)
				pt->holeTails += occ;
			else
				pt->vb.AddNumParticles(occ);
		}
		else
		{
			if (EnergyStates[i]->imp)
				pt->elecindonor += occ;
			else if (EnergyStates[i]->bandtail)
				pt->elecTails += occ;
			else
				pt->cb.AddNumParticles(occ);
		}
	}

	pt->n = pt->cb.GetNumParticles();
	pt->p = pt->vb.GetNumParticles();

	pt->fermi = currentFermi;
	pt->fermin = currentFermi;
	pt->fermip = currentFermi;

	EnergyStates.clear();
	EStateSize.clear();
	EOccupy.clear();
	Energies.clear();

	double nbeta = -KBI / pt->temp;
	CalcScatterConsts(pt->scatterCBConst, pt->cb.energies, nbeta);
	CalcScatterConsts(pt->scatterVBConst, pt->vb.energies, nbeta);

	//	WritePtEStates(pt, "Equilibrate");

	return(0);
}

