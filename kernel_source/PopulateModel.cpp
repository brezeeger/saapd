#include "PopulateModel.h"

#include "BasicTypes.h"
#include "model.h"
#include "physics.h"
#include "basis.h"
#include "helperfunctions.h"
#include "contacts.h"
#include "light.h"
#include "Equilibrium.h"
#include "scatter.h"
#include "output.h"
#include "outputdebug.h"
#include "srh.h"
#include "transient.h"

double FindSigmaSqGaussian(XSpace center, XSpace halfwidthPos, int dim)
{
	double sigsquared = 1.0;
	if(dim == 1)
	{
		double distance = AbsDistance(center, halfwidthPos);
		sigsquared = distance*distance/2.0/log(2.0);	//the sigma along a single direction
	}
	else if(dim == 2 || dim == 3)
	{
		double dx = center.x - halfwidthPos.x;
		double dy = center.y - halfwidthPos.y;
		sigsquared = dx*dx + dy*dy;
		sigsquared = sigsquared / 2.0 / log(2.0);
	}

	return(sigsquared);
}

/*
sometimes, the contact may be considered multiple points.  This will really mess with solving poisson's equation
and maintaining charge neutrality in the device.
Preferred contacts: 1D, a point contacting the exterior of the device.
1D: If no such point exists, then the most central point in the layer.

2D: All points exposed to exterior
2D: Only the center point

3D: You can't have a contact unless it is on the exterior. Otherwise, it is not a contact...

Overall, this can be reduced to either: Center point or any point on the exterior of the device (which would allow the flux to leave)

If Multiple contacts:
Must decouple each of the regions. Technically, a charge would build up and smooth things out. This should be handled in
poisson's equation prep.
*/
int SelectContactPoints(Model& mdl, ModelDescribe& mdesc)
{
	std::vector<Contact*>::iterator itCon;
	Contact* curCt;
	XSpace min;
	XSpace max;
	
//	bool foundEXT; set and not used	//a flag for the particular contact saying an outside point was found.
	std::vector<Point*> ExternalPoints;

	for(itCon = mdesc.simulation->contacts.begin(); itCon < mdesc.simulation->contacts.end(); itCon++)	//loop through all the contact points
	{
//		foundEXT = false;
		curCt = (*itCon);	//just make the syntax a little nicer...
		unsigned int numpoints = curCt->point.size();
		ExternalPoints.clear();
		min = mdesc.Ldimensions;	//the minimum is set to all the possible max values
		max.x = 0.0;	//max is set to minimum of all possible values. Guarantee that it will find the right values
		max.y = 0.0;
		max.z = 0.0;
		for(unsigned int pt = 0; pt < numpoints; pt++)	//cycle through all the points for this particular contact
		{
			Point* curpt = curCt->point[pt];	//load index of mdl.gridp[index] into curpt
			
			//first loop portion determines what the layers min/max is to determine what the center will be.

			if(curpt->mxmy.x < min.x)
				min.x = curpt->mxmy.x;
			
			if(curpt->mxmy.y < min.y)
				min.y = curpt->mxmy.y;
			
			if(curpt->mxmy.z < min.z)
				min.z = curpt->mxmy.z;

			if(curpt->pxpy.x > max.x)
				max.x = curpt->pxpy.x;

			if(curpt->pxpy.x > max.y)
				max.y = curpt->pxpy.y;

			if(curpt->pxpy.z > max.z)
				max.z = curpt->pxpy.z;

			switch(mdesc.NDim)	//NO BREAK STATEMENTS.
			{
				case 3:	//in a 3D case, all possible external values are applicable
					if (curpt->adj.adjMZ == NULL && curpt->adj.adjMZ2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
					else if (curpt->adj.adjPZ == NULL && curpt->adj.adjPZ2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
				case 2:	//in a 2D case, X & Y external is valid
					if(curpt->adj.adjMY == NULL && curpt->adj.adjMY2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
					else if(curpt->adj.adjPY == NULL && curpt->adj.adjPY2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
				case 1:	//this is a 1D case, only X is valid
					if(curpt->adj.adjMX == NULL && curpt->adj.adjMX2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
					else if(curpt->adj.adjPX == NULL && curpt->adj.adjPX2 == NULL)
					{
						ExternalPoints.push_back(curpt);
						break;
					}
			}
		}	//end for loop for initial calculations.

		if(ExternalPoints.size() == 0)	//then go with the center position...
		{
			XSpace center;
			center.x = 0.5 * (min.x + max.x);
			center.y = 0.5 * (min.y + max.y);
			center.z = 0.5 * (min.z + max.z);

			double shortestD = mdesc.Ldimensions.x + mdesc.Ldimensions.y + mdesc.Ldimensions.z;	//ensure the longest dimensions possible
			Point* targetpoint = NULL;	//output or the single point that will be considered a contact.
			double tempD;	//temporary distance for comparison
			for(unsigned int pt = 0; pt < numpoints; pt++)	//cycle through all the points for this particular contact
			{
				Point* curpt = curCt->point[pt];	//load index of mdl.gridp[index] into curpt
				if(curpt == NULL)	//shouldn't ever happen, but don't let the program crash in case it does.
					continue;
				tempD = AbsDistance(curpt->position, center);
				if(tempD < shortestD)
				{
					shortestD = tempD;
					targetpoint = curpt;
				}
				//while going through this loop, clear out the points link to the contact...
				curpt->contactData = NULL;
			}
			curCt->point.clear();	//and now clear out all the possible points
			if(targetpoint)	//this should be true...
			{
				curCt->point.push_back(targetpoint);	//and put the one valid contact point back in
				targetpoint->contactData = curCt;	//and re-establish the link
			}
			
		}	//end if there are no external points
		else	//there ARE other external points
		{
			std::vector<Point*>::iterator itExtPt;
			//must clear out all the point links
			for(unsigned int pt = 0; pt < numpoints; pt++)	//cycle through all the points for this particular contact
				curCt->point[pt]->contactData = NULL;
			//and clear out the contact links...
			curCt->point.clear();
							//now establish the links.
			for(itExtPt = ExternalPoints.begin(); itExtPt < ExternalPoints.end(); itExtPt++)
			{
				(*itExtPt)->contactData = curCt;
				curCt->point.push_back(*itExtPt);
			}	//end for loop establishing the selected points
		}	//end else saying points lie on the device boundary that are contacts
		CreateExternalContactPoints(curCt, mdl);
		curCt->UpdatePlaneRecPoints(mdesc.Ldimensions);
	}	// end for loop going through each contact layer.
	
	return(0);
}

int CreateExternalContactPoints(Contact* ctc, Model& mdl)
{
	//first, delete any potential existing data
	ctc->endPtCalcs.clear();
	
	Point dataFillPt(mdl);
	Point* cpyPt;
	Point* newPt;
	

	dataFillPt.adj.adjMX = dataFillPt.adj.adjMX2 = 
		dataFillPt.adj.adjPX = dataFillPt.adj.adjPX2 = 
		dataFillPt.adj.adjMY = dataFillPt.adj.adjMY2 = 
		dataFillPt.adj.adjPY = dataFillPt.adj.adjPY2 = 
		dataFillPt.adj.adjMZ = dataFillPt.adj.adjMZ2 = 
		dataFillPt.adj.adjPZ = dataFillPt.adj.adjPZ2 = NULL;
	dataFillPt.adj.self = EXT;	//signal that this is an external point

	dataFillPt.AngleData.clear();
	dataFillPt.contactData = NULL;	//this actually isn't used anywhere. It may screw it up because
	//the contact data doesn't apply to this point.  This is just a buffer point of sorts
	
	dataFillPt.srhR4 =
	dataFillPt.srhR3 =
	dataFillPt.srhR2 =
	dataFillPt.srhR1 =
	dataFillPt.scatterR =
	dataFillPt.recth =
	dataFillPt.LightPowerEntry =
	dataFillPt.LightEmitSRH =
	dataFillPt.LightEmitScat =
	dataFillPt.LightEmitRec =
	dataFillPt.LightEmitDef =
	dataFillPt.LightEmission =
	dataFillPt.LightAbsorbSRH =
	dataFillPt.LightAbsorbScat =
	dataFillPt.LightAbsorbGen =
	dataFillPt.LightAbsorbDef =
	dataFillPt.largestUnOccES =
	dataFillPt.largestPsiCf =
	dataFillPt.largestOccVB =
	dataFillPt.largestOccES =
	dataFillPt.largestOccCB = 
	dataFillPt.heat =
	dataFillPt.genth = 
	dataFillPt.EFieldESqChange =
	dataFillPt.EFieldEChange = 
	dataFillPt.desireTStep = 
	dataFillPt.chargeRate = 
	dataFillPt.cb.numparticlesold =
	dataFillPt.vb.numparticlesold = 
	dataFillPt.BandEnergyChange = 0.0;

	dataFillPt.LightEmissions = NULL;
	

	dataFillPt.EChangesCalc = false;
	dataFillPt.EField.clear();
	dataFillPt.Jn.clear();
	dataFillPt.Jp.clear();
	dataFillPt.mintime.clear();
	dataFillPt.OpticalOut = NULL;
	
	dataFillPt.OpticTransitions = NULL;// clear();
	dataFillPt.Output.Deallocate();	//should already be nulled
	dataFillPt.poissonDependency = NULL;
	dataFillPt.pDsize = 0;
	dataFillPt.sizeScaling = 1.0;
	

	double distance;
	ctc->endPtCalcs.reserve(ctc->point.size());	//prevent vector reallocations calling destructors
	for(unsigned int i=0; i<ctc->point.size(); ++i)
	{
		cpyPt = ctc->point[i];
		if(cpyPt == NULL)
			continue;
		ctc->endPtCalcs.push_back(dataFillPt);
		newPt = &(ctc->endPtCalcs.back());
		ELevel tmpELev(*newPt);
//		newPt->elecindonor; updated in reset data
//		newPt->elecTails; updated in reset data
//		newPt->holeinacceptor;	updated in reset 
//		newPt->holeTails; updated in reset data
		
//		newPt->holeTails;	updated in reset data
//		newPt->impdefcharge; updated in reset data
//		newPt->n; updated in reset data
//		newPt->rho; updated in reset data
//		newPt->netCharge; updated in reset data
//		newPt->p; updated in reset data
		

		//these are all the main things that can be copied over
		newPt->LightEmissions = new LightSource;
		newPt->LightEmissions->enabled = false;
		newPt->LightEmissions->light.clear();

		newPt->OpticalOut = new OpticalStorage;
		newPt->OpticalOut->Clear();

		newPt->area = cpyPt->area;
		newPt->areai = cpyPt->areai;
		newPt->eg = cpyPt->eg;
		newPt->eps = cpyPt->eps;
		newPt->impdefcharge = cpyPt->impdefcharge;
		newPt->impurity = cpyPt->impurity;
		newPt->MatProps = cpyPt->MatProps;
		newPt->niCarrier = cpyPt->niCarrier;
		newPt->Psi=cpyPt->Psi;	//just get it in the ball park
		newPt->scatterCBConst = cpyPt->scatterCBConst;
		newPt->scatterVBConst = cpyPt->scatterVBConst;
		newPt->temp = cpyPt->temp;
		newPt->vol = cpyPt->vol;
		newPt->voli = cpyPt->voli;
		newPt->position = cpyPt->position;
		newPt->pxpy = cpyPt->pxpy;
		newPt->mxmy = cpyPt->mxmy;
		newPt->width = cpyPt->width;
		newPt->widthi = cpyPt->widthi;
		

		//now let's figure out where this point is going to go. Priority is on x.
		if(cpyPt->adj.adjMX == NULL)	//it's goin' to the left,
		{  //but we only need to update the x values
			newPt->pxpy.x = cpyPt->mxmy.x;
			distance = cpyPt->position.x - cpyPt->mxmy.x;
			newPt->position.x = cpyPt->mxmy.x - distance;	//keep the symmetry rules
			newPt->mxmy.x = newPt->pxpy.x - newPt->width.x;
		}
		else if(cpyPt->adj.adjPX == NULL)	//it's goin' to the right
		{
			newPt->mxmy.x = cpyPt->pxpy.x;
			distance = cpyPt->pxpy.x - cpyPt->position.x;
			newPt->position.x = cpyPt->pxpy.x + distance;
			newPt->pxpy.x = newPt->mxmy.x + newPt->width.x;
		}
		else if(cpyPt->adj.adjMY == NULL)	//it be going to the back
		{ //now we only update the y values
			newPt->pxpy.y = cpyPt->mxmy.y;
			distance = cpyPt->position.y - cpyPt->mxmy.y;
			newPt->position.y = cpyPt->mxmy.y - distance;	//keep the symmetry rules
			newPt->mxmy.y = newPt->pxpy.y - newPt->width.y;
		}
		else if(cpyPt->adj.adjPY == NULL)	//it be goin' to the front
		{
			newPt->mxmy.y = cpyPt->pxpy.y;
			distance = cpyPt->pxpy.y - cpyPt->position.y;
			newPt->position.y = cpyPt->pxpy.y + distance;
			newPt->pxpy.y = newPt->mxmy.y + newPt->width.y;
		}
		else if(cpyPt->adj.adjMZ == NULL)	//it be gettin' down, bow shicka bow wow
		{ //now we only update the y values
			newPt->pxpy.y = cpyPt->mxmy.y;
			distance = cpyPt->position.y - cpyPt->mxmy.y;
			newPt->position.y = cpyPt->mxmy.y - distance;	//keep the symmetry rules
			newPt->mxmy.y = newPt->pxpy.y - newPt->width.y;
		}
		else //goin' up
		{
			newPt->mxmy.z = cpyPt->pxpy.z;
			distance = cpyPt->pxpy.z - cpyPt->position.z;
			newPt->position.z = cpyPt->pxpy.z + distance;
			newPt->pxpy.z = newPt->mxmy.z + newPt->width.z;
		}
		

		


		//copy conduction band states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->cb.energies.begin(); it != cpyPt->cb.energies.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory
			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
//			tmpELev.tStepCtrl = NULL;
			newPt->cb.energies.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}
		newPt->cb.bandmin = cpyPt->cb.bandmin;
		newPt->cb.EffectiveDensity = cpyPt->cb.EffectiveDensity;
		newPt->cb.totalstates = cpyPt->cb.totalstates;
		newPt->cb.type = cpyPt->cb.type;
		newPt->cb.partitioni = cpyPt->cb.partitioni;
		

		//copy CB tail states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->CBTails.begin(); it != cpyPt->CBTails.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory
			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
//			tmpELev.tStepCtrl = NULL;
			newPt->CBTails.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}

		//copy donor states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->donorlike.begin(); it != cpyPt->donorlike.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory

			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
	//		tmpELev.tStepCtrl = NULL;
			newPt->donorlike.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}

		//copy acceptorlike states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->acceptorlike.begin(); it != cpyPt->acceptorlike.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory
			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
//			tmpELev.tStepCtrl = NULL;
			newPt->acceptorlike.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}

		//copy VB Tail states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->VBTails.begin(); it != cpyPt->VBTails.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory
			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
//			tmpELev.tStepCtrl = NULL;
			newPt->VBTails.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}
		
		//copy valence band states over
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cpyPt->vb.energies.begin(); it != cpyPt->vb.energies.end(); ++it)
		{
			tmpELev = it->second;	//just copy EVERYTHING over
			tmpELev.OpticalRates.clear();	//and erase all the unwanted stuff to clear memory
			tmpELev.Rates.clear();
			tmpELev.RateSums.Clear();
			tmpELev.SRHConstant.clear();
			tmpELev.TargetELev.clear();
			tmpELev.TargetELevDD.clear();
//			tmpELev.tStepCtrl = NULL;
			newPt->vb.energies.insert(std::pair<MaxMin<double>,ELevel>(it->first,tmpELev));
		}
		newPt->vb.bandmin = cpyPt->vb.bandmin;
		newPt->vb.EffectiveDensity = cpyPt->vb.EffectiveDensity;
		newPt->vb.totalstates = cpyPt->vb.totalstates;
		newPt->vb.type = cpyPt->vb.type;
		newPt->vb.partitioni = cpyPt->vb.partitioni;

		EquilibratePoint(newPt);
		newPt->TransferEndToBegin();
//		newPt->fermi;	set in equilibrate point
//		newPt->fermin; set in eq point
//		newPt->fermip; set in eq point

		newPt->ResetData();
		
		double Nbeta = 0.0;
		if(newPt->temp > 0.0)
			Nbeta = -KBI / newPt->temp;

		CalcScatterConsts(newPt->scatterCBConst, newPt->cb.energies, Nbeta);	//sums up all exponents
		CalcScatterConsts(newPt->scatterVBConst, newPt->vb.energies, Nbeta);
	}

	

	return(0);
}



int VacuumMaterial(ModelDescribe& mdesc)
{
	//first, search to see if a vacuum material already exists. It will have an ID of -1.
	for(unsigned int i=0; i<mdesc.materials.size(); i++)
	{
		if(mdesc.materials[i]->id == -1)
			return(i); //this material has already been created. Get out of here.
	}
	Material* vacuum = new Material;	//create the new vacuum material...

	mdesc.materials.push_back(vacuum);

	vacuum->name = "Default Vacuum";
	vacuum->describe = "This is an automatically generated vacuum material to represent conditions in free space.";
	vacuum->cfactor.ni = 0.0;
	vacuum->defects.clear();
	vacuum->EcEfi = 0.0;
	vacuum->EFMeDens = 1.0;
	vacuum->EFMhDens = 1.0;
	vacuum->EFMeCond = 0.0;
	vacuum->EFMhCond = 0.0;
	vacuum->Eg = 0.0;	//may want to say a large number...?
	vacuum->epsR = 1.0;
	vacuum->id = -1;
	vacuum->MuN = 0.0;
	vacuum->MuP = 0.0;
	vacuum->Nc = 0.0;
	vacuum->Nv = 0.0;
	vacuum->ni = 0.0;
	//alpha = 4 pi k / lambda --- absorption coefficient
	//alpha = (A + B/hv) sqrt(hv - Eg)
	vacuum->optics = new Optical;
	vacuum->optics->n = 1.0;	//real refractive index
	vacuum->optics->a = 0.0;
	vacuum->optics->b = 0.0;
	NK tmpNK;
	tmpNK.alpha = 0.0;
	tmpNK.k = 0.0;	//imaginary refractive index (dielectric = recfractive^2)
	tmpNK.n = 1.0;
	vacuum->optics->WavelengthData.insert(std::pair<float,NK>( float(0.03175),tmpNK));	//just say there is no absorption, and this should go out to everything else
	vacuum->optics->WavelengthData.insert(std::pair<float,NK>(float(100000),tmpNK));	//just say there is no absorption, and this should go out to everything else
	vacuum->Phi = 0.0;
	vacuum->SpHeatCap = 0.0;
	vacuum->ThCond = 0.0;	//there is no lattice thermal conductivity
	vacuum->type = VACUUM;
	vacuum->urbachCB.energy = 0.0;
	vacuum->urbachVB.energy = 0.0;
	if(mdesc.simulation->outputs.LightEmission
		|| mdesc.simulation->outputs.QE
		|| mdesc.simulation->outputs.Edef
		|| mdesc.simulation->outputs.Erec
		|| mdesc.simulation->outputs.Escat
		|| mdesc.simulation->outputs.Esrh
		|| mdesc.simulation->outputs.Ldef
		|| mdesc.simulation->outputs.Lgen
		|| mdesc.simulation->outputs.Lscat
		|| mdesc.simulation->outputs.Lsrh)
	{
		vacuum->OpticalFluxOut = new BulkOpticalData;	//yeah, this is stupid. But it will crash when trying
		vacuum->OpticalAggregateOut = new BulkOpticalData;	//to cycle through all materials and nothing is there to add nothing to nothing to
	}


	return(mdesc.materials.size() -1);	//this will be the last index just added, aka the vacuum
}

int PopulateModel(ModelDescribe& mdesc, Model& mdl)
{
	DisplayProgress(32,0);
	
	int vacID = 0;	//index of the vacuum material
	double kT = KB * mdesc.simulation->T;
	double beta;
	if(mdesc.simulation->T != 0.0)
		beta = KBI / mdesc.simulation->T;
	else
		beta = 1e20;	//just a big number!

	unsigned int numinbin;	//used in for loop for points when creating output data.

	if(mdesc.simulation->TransientData)
	{
		mdesc.simulation->outputs.numinbin = 1;	//force no binning
		numinbin = mdesc.simulation->outputs.numinbin;
//		if(numinbin <= 0)
			numinbin = 1;
		mdesc.simulation->TransientData->timesteps.clear();// = new double[numinbin];
	}
	else
		numinbin = 1;


	mdesc.simulation->Vol = 0.0;
	vacID = VacuumMaterial(mdesc);	//create a vacuum material if one does not exist and add it to mdesc with ID -1
	
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		//initiate point data
		pt->second.Populate(vacID, numinbin, beta);
		
		mdesc.simulation->Vol += pt->second.vol;
		//now set the carriers in it all proper...
		if(mdesc.simulation->ThEqPtStart)
			EquilibratePoint(&(pt->second));	//adjust the carriers to have the proper distribution for the given temperature
		pt->second.Output.Allocate(numinbin);

		
	}	//end loop for each point in the model

	//the targets need to be done now that there is a complete mesh around them. And they should know they're equivalent energy states next to them for stability purposes
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		for (std::multimap<MaxMin<double>, ELevel>::iterator cb = pt->second.cb.energies.begin(); cb != pt->second.cb.energies.end(); ++cb)
		{
			FindAllTargets(cb->second, &(pt->second), mdl);
			cb->second.FindAdjEquivELevels();	//would have been more efficient to insert in find all targets, but it would've made it hard to read... this is only done once!
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = pt->second.vb.energies.begin(); vb != pt->second.vb.energies.end(); ++vb)
		{
			FindAllTargets(vb->second, &(pt->second), mdl);
			vb->second.FindAdjEquivELevels();
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator es = pt->second.donorlike.begin(); es != pt->second.donorlike.end(); ++es)
		{
			FindAllTargets(es->second, &(pt->second), mdl);
			es->second.FindAdjEquivELevels();
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator es = pt->second.acceptorlike.begin(); es != pt->second.acceptorlike.end(); ++es)
		{
			FindAllTargets(es->second, &(pt->second), mdl);
			es->second.FindAdjEquivELevels();
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator es = pt->second.CBTails.begin(); es != pt->second.CBTails.end(); ++es)
		{
			FindAllTargets(es->second, &(pt->second), mdl);
			es->second.FindAdjEquivELevels();
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator es = pt->second.VBTails.begin(); es != pt->second.VBTails.end(); ++es)
		{
			FindAllTargets(es->second, &(pt->second), mdl);
			es->second.FindAdjEquivELevels();
		}
	}
	
	

	SelectContactPoints(mdl, mdesc);	//choose which points will be considered the contacts in the device within the contact layers
	//tries to choose points that are adjacent to points external to the device (they could be connected then to an external circuit)
	SmartSaveState(mdesc);
	SpectrumIncidence(mdesc, mdl);

	DisplayProgress(25, 0);

	return(0);
}

int ELevel::FindAdjEquivELevels()
{
	int type = CARTYPE_UNKNOWN;
	if (imp)
		type = (imp->isDonor()) ? CARTYPE_DON : CARTYPE_ACP;
	else if (bandtail)
		type = (carriertype == ELECTRON) ? CARTYPE_BTC : CARTYPE_BTV;
	else
		type = (carriertype == ELECTRON) ? CARTYPE_CB : CARTYPE_VB;

	CheckAdj(pt.adj.adjMX, type);	//this returns right away if it's a bad point (or bad source)
	CheckAdj(pt.adj.adjMX2, type);
	CheckAdj(pt.adj.adjPX, type);
	CheckAdj(pt.adj.adjPX2, type);
	CheckAdj(pt.adj.adjMY, type);
	CheckAdj(pt.adj.adjMY2, type);
	CheckAdj(pt.adj.adjPY, type);
	CheckAdj(pt.adj.adjPY2, type);
	CheckAdj(pt.adj.adjMZ, type);
	CheckAdj(pt.adj.adjMZ2, type);
	CheckAdj(pt.adj.adjPZ, type);
	CheckAdj(pt.adj.adjPZ2, type);
	
	return(0);
}

int ELevel::CheckAdj(Point* trgPt, int type)
{
	if (!trgPt)
		return(0);

	if (type == CARTYPE_UNKNOWN)
	{
		if (imp)
			type = (imp->isDonor() == CONDUCTION) ? CARTYPE_DON : CARTYPE_ACP;
		else if (bandtail)
			type = (carriertype == ELECTRON) ? CARTYPE_BTC : CARTYPE_BTV;
		else
			type = (carriertype == ELECTRON) ? CARTYPE_CB : CARTYPE_VB;
	}

	double sMax = max;
	double sMin = min;

	if (type == CARTYPE_CB)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = pt.MatProps->Phi - trgPt->MatProps->Phi;
				sMax -= difEf;
				sMin -= difEf;	//the source values are now relative to the target CB
			}	
		}	
		for (std::multimap<MaxMin<double>, ELevel>::iterator cb = trgPt->cb.energies.begin(); cb != trgPt->cb.energies.end(); ++cb)
		{
			double max = cb->second.max;
			double min = cb->second.min;
			//sMax and sMin will line up with max/min assuming ZERO electric field.
			//basically, just check if they are contained within one another/line up
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&cb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&cb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&cb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&cb->second);
		}
	}
	else if (type == CARTYPE_VB)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = (pt.MatProps->Phi + pt.eg) - (trgPt->MatProps->Phi - trgPt->eg);
				sMax += difEf;
				sMin += difEf;
			}
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = trgPt->vb.energies.begin(); vb != trgPt->vb.energies.end(); ++vb)
		{
			double max = vb->second.max;
			double min = vb->second.min;
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&vb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&vb->second);
		}
	}
	else if (type == CARTYPE_BTC)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = (pt.MatProps->Phi) - (trgPt->MatProps->Phi);
				sMax -= difEf;
				sMin -= difEf;
			}
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = trgPt->CBTails.begin(); vb != trgPt->CBTails.end(); ++vb)
		{
			double max = vb->second.max;
			double min = vb->second.min;
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&vb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&vb->second);
		}
	}
	else if (type == CARTYPE_BTV)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = (pt.MatProps->Phi + pt.eg) - (trgPt->MatProps->Phi - trgPt->eg);
				sMax += difEf;
				sMin += difEf;
			}
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = trgPt->VBTails.begin(); vb != trgPt->VBTails.end(); ++vb)
		{
			double max = vb->second.max;
			double min = vb->second.min;
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&vb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&vb->second);
		}
	}
	else if (type == CARTYPE_DON)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = (pt.MatProps->Phi) - (trgPt->MatProps->Phi);
				sMax -= difEf;
				sMin -= difEf;
			}
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = trgPt->donorlike.begin(); vb != trgPt->donorlike.end(); ++vb)
		{
			double max = vb->second.max;
			double min = vb->second.min;
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&vb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&vb->second);
		}
	}
	else if (type == CARTYPE_ACP)
	{
		if (trgPt->MatProps != pt.MatProps) {
			if (trgPt->MatProps && pt.MatProps)
			{
				double difEf = (pt.MatProps->Phi + pt.eg) - (trgPt->MatProps->Phi - trgPt->eg);
				sMax += difEf;
				sMin += difEf;
			}
		}
		for (std::multimap<MaxMin<double>, ELevel>::iterator vb = trgPt->acceptorlike.begin(); vb != trgPt->acceptorlike.end(); ++vb)
		{
			double max = vb->second.max;
			double min = vb->second.min;
			if (max <= sMax && min >= sMin) //the first obvious one - entirely contained in source
				NeighborEquivs.push_back(&vb->second);
			else if (min <= sMin && max > sMin) //straddles source min, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (min < sMax && max >= sMax) //straddles source max, and extends into it
				NeighborEquivs.push_back(&vb->second);
			else if (max >= sMax && min <= sMin) // completely encloses source
				NeighborEquivs.push_back(&vb->second);
		}
	}

	return(0);

}

//these energies are relative to the band minimum, moving in the direction of the band minimum
CustomDensStates GetStateDensity(double startE, double endE, std::map<double,CustomDensStates>& band, double& retEnergy, double& densMin, double& densUse, double& densMax)
{
	CustomDensStates retVal;
	retVal.density = 0.0;
	retVal.EFMCond = 1.0;
	densMin = densUse = densMax = 0.0;
	retEnergy = (startE + endE) * 0.5;

	
	if(band.size() == 0)
		return(retVal);

//	double tmpdensity = 0.0; unused
	double prevEnergy = 0.0;	//assume it starts at the band minimum with zero states
	double prevDensity = 0.0;
	double prevEFM = 1.0;
	double eWidth;
	double emin, emax;
	double midDOSM, midEFMM, midDOSP, midEFMP;	//mid locations minus and plus
	bool first = true;
	
	for(std::map<double,CustomDensStates>::iterator Dens = band.begin(); Dens != band.end(); ++Dens)
	{
		//first is energy, second is density and efm
		eWidth = Dens->first - prevEnergy;
		if(eWidth <= 0.0 || Dens->first <= startE)	//this is the first one, and this is essentially just a data point to compare to later
		{  //it could also be skipping calculations if it hasn't gotten to the area yet
			prevEnergy = Dens->first;
			prevDensity = Dens->second.density;
			prevEFM = Dens->second.EFMCond;
			continue;
		}

		if(prevEnergy >= endE)	//it has gone all the way through
			break;

		//figur eout the starting values
		if(prevEnergy < startE && startE < Dens->first)
		{
			emin = startE;	//get the starting point, and it's approximate value
			midDOSM = prevDensity + (Dens->second.density - prevDensity) / (startE - prevEnergy);
			midEFMM = prevEFM + (Dens->second.EFMCond - prevEFM) / (startE - prevEnergy);
		}
		else// if(prevEnergy >= startE) always gonna be true. if Dens->first < startE, it hasn't started yet
		{  //so this section of code hasn't been reached
			emin = prevEnergy;
			midDOSM = prevDensity;
			midEFMM = prevEFM;
		}

		if(prevEnergy < endE && endE < Dens->first)
		{
			emax = endE;
			midDOSP = prevDensity + (Dens->second.density - prevDensity) / (endE - prevEnergy);
			midEFMP = prevEFM + (Dens->second.EFMCond - prevEFM) / (endE - prevEnergy);
		}
		else
		{
			emax = Dens->first;
			midDOSP = Dens->second.density;
			midEFMP = Dens->second.EFMCond;
		}
		
		eWidth = emax - emin;
		retVal.density += 0.5 * eWidth * (midDOSM + midDOSP);	//trapezoidal sum integration
		retVal.EFMCond += 0.5 * eWidth * (midEFMM + midEFMP);	//trapezoidal sum integration to then get average (need division)
		if (first)
			densMin = midDOSM;
		first = false;
	}
	densMax = midDOSP;	//this will be the last point on the custom density
	//now get the middle density so they all match up

	retVal.EFMCond = retVal.EFMCond / (endE - startE);
	

	densUse = (retVal.density - (retEnergy - startE)*densMin - (endE - retEnergy)*densMax) / (endE - startE);	//choose a value to make the total density
	//match given the density on the left and right border of the state

	return(retVal);
}

int Point::Populate(int vacID, unsigned int numinbin, double beta)	//get all the non-occupancy stuff taken care of for the point
{
	std::vector<Layer*> inLayers;
	std::vector<Layer*>::iterator itLay;
	std::vector<Material*> inMats;
	std::vector<Material*>::iterator itMats;

	ModelDescribe& mdesc = getMdesc();
	Model& mdl = getModel();
	
	long int i = adj.self;	
	//set the areas for each of the grid points
	inLayers.clear();	//get rid of all the layers associated with this point
	inLayers.reserve(mdesc.layers.size());	//make sure there are enough spaces
	for(itLay = mdesc.layers.begin(); itLay != mdesc.layers.end(); itLay++)
	{
		if((*itLay)->InLayer(position))
		{
			if((*itLay)->out.OpticalPointEmissions)
				Output.DetailedOptical = true;

			inLayers.push_back((*itLay));
			if((*itLay)->exclusive)
			{
				inLayers.clear();	//clear out all the other layers
				inLayers.push_back(*itLay);	//put this one back in
				break;	//now don't look for any more layers
			}
		}
	}

	std::vector<ldescriptor<Material*>*>::iterator itLDesc;
	PartitionFunc<int> probabilities;
	sizeScaling = 1.0;

	//now for each layer its in, check for accompanying materials
	probabilities.Clear();	//used to determine the material at this point
	contactData = NULL;	//make sure contact is null beforehand
	for(itLay = inLayers.begin(); itLay != inLayers.end(); itLay++)
	{	//cycle through each layer
		sizeScaling = sizeScaling * (*itLay)->sizeScale;

		for(itLDesc = (*itLay)->matls.begin(); itLDesc != (*itLay)->matls.end(); itLDesc++)
		{	//cycle through each material present in layer. Should usually be one or zero, but we'll see...
			if((*itLDesc)->flag == DISCRETE)
			{
				//uniformly distributed. proceed as if probability one.
				//double prob = ProbDiscrete(); prob = 1.0
				probabilities.AddProbability(1.0, (*itLDesc)->data->id);
			}
			else if((*itLDesc)->flag == BANDED)
			{
				//uniform probability across two points that total one.
				double prob = ProbBanded((*itLDesc)->range, this, false);
				probabilities.AddProbability(prob, (*itLDesc)->data->id);
			}
			else if((*itLDesc)->flag == GAUSSIAN)	//1D Gaussian
			{
				double prob = ProbGaussian((*itLDesc)->range, this, false);
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);				
			}
			else if((*itLDesc)->flag == GAUSSIAN2 || (*itLDesc)->flag == GAUSSIAN3)
			{	//2 or 3D gaussian. mdl is only 2d so far, so treat as 2D gaussian
									double prob = ProbGaussian2((*itLDesc)->range, this, false);
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);
				
			}
			else if((*itLDesc)->flag == EXPONENTIAL)	//1 dimensional exponential, only goes in direction of halfwidth
			{
				double prob = ProbExponential((*itLDesc)->range, this, false);		
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);					
			}
			else if((*itLDesc)->flag == EXPONENTIAL2 || (*itLDesc)->flag == EXPONENTIAL3)	//2d or 3d exponential
			{	
				double prob = ProbExponential2((*itLDesc)->range, this, false);		
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);					
			}
			else if((*itLDesc)->flag == LINEAR)
			{	//nice and straightforward...
				double prob = ProbLinear((*itLDesc)->range, this, false);		
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);
			}
			else if((*itLDesc)->flag == PARABOLIC)
			{
				double prob = ProbParabolic((*itLDesc)->range, this, false);
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);
			}
			else if((*itLDesc)->flag == PARABOLIC2 || (*itLDesc)->flag == PARABOLIC3)
				{
				double prob = ProbParabolic2((*itLDesc)->range, this, false);
				if(prob > 0.0)
					probabilities.AddProbability(prob, (*itLDesc)->data->id);
			}
			else
			{
				DisplayProgress(24, (*itLDesc)->flag);
			}
				//need to load the initial simulation data
			
		}	//end for loop going through all the materials contained in the layer
			
		if((*itLay)->contactid > 0)	//it is a contact layer
		{	//must find the contact it should be associated with
			int id = (*itLay)->contactid;
			std::vector<Contact*>::iterator itCt;

			for(itCt = mdesc.simulation->contacts.begin(); itCt != mdesc.simulation->contacts.end(); itCt++)
			{
				if((*itCt)->id == id)	//found the appropriate contact
				{
					contactData = *itCt;
					(*itCt)->point.push_back(this);	//and point the contact back to the point
					break;	//exit the for loop
				}
			}
		}	//end contact layer setup.

	}	//end for loop going through all the layers this point is in.

		//now actually set the material..

	probabilities.ProbToNumLine();
	int MatID = -1;	//-1 is a default uh-oh value
	probabilities.Choose(MatID);	//assign it assuming it passes
	probabilities.Clear();	//make sure it's ready for next time in case forget to clear before use
	//now find this material ID

	std::vector<Material*>::iterator itMat;
	for(itMat = mdesc.materials.begin(); itMat != mdesc.materials.end(); itMat++)
	{
		if((*itMat)->id == MatID)
		{
			MatProps = (*itMat);
				break;	//stop searching through the materials in the device
		}
	}
	fermi = 0.0;	//just default everything to 0. It will be filled in correctly
	fermin = 0.0;	//after the first poisson solution.
	fermip = 0.0;
	Psi = 0;	//initiate all local vacuums to zero, this ensures that the local vacuum
	temp = mdesc.simulation->T;	//set the local temperature of each point. This will vary in the future...
	

	if(MatProps->type != VACUUM)	//it found a material to point to...
	{
		cb.EffectiveDensity = 0.0;
		vb.EffectiveDensity = 0.0;
		//start assigning basic things in the material.
		eg = MatProps->Eg;	//this can vary with temperature, but no import of data
		//incorrect... high doping causes the formation of a new band just beneath. will be handled by impurities.
		eps = MatProps->epsR;
		cb.bandmin = Psi - MatProps->Phi;
		vb.bandmin = cb.bandmin - eg;
		cb.type = CONDUCTION;
		vb.type = VALENCE;
		rho = 0.0;	//the current charge density is zero because all the electrons/ions are in the same spot
		n = 0.0;	//everything starts out as empty
		p = 0.0;
		width.x = pxpy.x - mxmy.x;
		width.y = pxpy.y - mxmy.y;
		width.z = pxpy.z - mxmy.z;
		widthi.x = 1.0 / width.x;
		widthi.y = 1.0 / width.y;
		widthi.z = 1.0 / width.z;
		vol =width.x * width.y * width.z;	//there are waaay to many calls to Volume() to end up dividing by volume

		//the line below may potentially BREAK everyhthing.
		vol = vol * sizeScaling;	//the point still takes up the whole "position" enclosed by dx/dy/dz, but the active electronic area is reduced.
		
		area.x = width.y * width.z;	//same for calculating various densities...
		area.y = width.x * width.z;
		area.z = width.y * width.x;
		if(vol != 0.0)
			voli = 1.0 / vol;			//so just store that number...

		if(area.x != 0.0)
			areai.x = 1.0 / area.x;
		if(area.y != 0.0)
			areai.y = 1.0 / area.y;
		if(area.z != 0.0)
			areai.z = 1.0 / area.z;

		mintime.x = width.x / THERMALVELOCITY;
		mintime.y = width.y / THERMALVELOCITY;
		mintime.z = width.z / THERMALVELOCITY;

	//	niCarrier = MatProps->ni * vol;	//don't rely on this... the EFMe/EFMh may not actually correlate.
		//actually build it up from effective density of states (exponential weighting going up the states) ni^2=Nc Nv e^(-Eg/kT)

		//populate the number of states in the bands up to a distance of the local vacuum level
		double minb;
		double maxb;
		double prefcb = 16.0/3.0 * PI * pow(2,0.5) * pow(MatProps->EFMeDens * MELECTRON, 1.5);
		double space = vol;
		
		prefcb = prefcb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefcb = prefcb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefcb = prefcb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefcb = prefcb / pow(QCHARGE,1.5);	//using eV in H introduces coulomb^-1.5. 
		prefcb = prefcb / 1E6;	//m^3 to cm^3
		//note, 1C is 1/elementary charge. So to multiply by coulomb^1.5, it involes dividing by elementary charge^1.5

		double prefvb = 16.0/3.0 * PI * pow(2,0.5) * pow(MatProps->EFMhDens * MELECTRON, 1.5);
		prefvb = prefvb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefvb = prefvb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefvb = prefvb / PLANCKEVS;	//split this up to avoid divide by zero!
		prefvb = prefvb / pow(QCHARGE,1.5);	//using eV in H introduces coulomb^-1.5. 
		prefvb = prefvb / 1E6;	//m^3 to cm^3

		int numbands = mdesc.simulation->statedivisions;	//the number of discretizations of the band
		int thermalstop = (int)(numbands * 0.6);	//have 60% of the bands be below 4kT and 40% be above
		double rangek = KB*mdesc.simulation->T * 4.0;	//typical thermal energy of electron to have finest divisions (range)
		double rangeE = MatProps->Phi - rangek;	//remaining range in the grid pointa
		if(rangeE < 0)
		{
			thermalstop = 0;	//make sure not to do this part
			rangek = 0.0;		//set this down to zero so it has no effect
			rangeE = MatProps->Phi;	//reset this...
		}
			//do a total of ten states. Follow a parabolic distribution (more breaks at smaller states, which are more interesting)
		ELevel dummycb(*this);	//dummy CB elevel to be put into tree
		ELevel dummyvb(*this);	//dummy VB elevel to be put into tree
			//momentum auto 0
			//momentum deviation auto 0

			//load constants that will be for each EBand in this present state of code.
		dummycb.SetEndOccStates(0.0);	//default all band states to unoccupied. Don't know impurities,
		dummyvb.SetEndOccStates(0.0);	//so couldn't possibly know the distribution or guess at it
		dummycb.imp = NULL;
		dummyvb.imp = NULL;
		dummycb.bandtail = false;
		dummyvb.bandtail = false;
		dummycb.efm = MatProps->EFMeCond * MELECTRON;	//assume constant
		dummyvb.efm = MatProps->EFMhCond * MELECTRON;
		dummycb.tau = MatProps->MuN * dummycb.efm * QI;	//do NOT multiply by the number of bands
		dummycb.taui = 1.0 / dummycb.tau;
		dummyvb.tau = MatProps->MuP * dummyvb.efm * QI;
		dummyvb.taui = 1.0 / dummyvb.tau;
		dummycb.carriertype = ELECTRON;
		dummyvb.carriertype = HOLE;
		dummycb.charge = 0;	//charge for not occupied by electron
		dummyvb.charge = 0;	//charge for not occupied by a hole
		if(mdesc.simulation->TransientData)
		{
			dummycb.percentTransfer = mdesc.simulation->TransientData->percentCarrierTransfer;
			dummyvb.percentTransfer = mdesc.simulation->TransientData->percentCarrierTransfer;
		}
		else
		{
			dummycb.percentTransfer = 0.0;
			dummyvb.percentTransfer = 0.0;
		}
			
		double difE;
		double nbandt = (double)(numbands - thermalstop);	//number of bands above kT
		//Tree<ELevel, double>* elevpos;	//position in the ELevel tree for quick access.
		//need for creating a pointer to the spot in the tree. a pointer to a dummy variable is useless...
		MaxMin<double> stateSort;
		for(int j=0; j<numbands; j++)
		{	//note, the ordering from bottom of band to top of band is essential for scattering probability distribution. One time calculation.
			double jt = (double)(j-thermalstop);	//center parabola so finest division is around kT
			double adj = jt+1.0;
			if(j < thermalstop)	//jt is negative, adj is negative or zero.
			{
				minb = rangek * (1 - jt*jt/(double)thermalstop/(double)thermalstop);
				maxb = rangek * (1 - adj*adj/(double)thermalstop/(double)thermalstop);
			}
			else
			{
				minb = rangek + rangeE*(jt*jt/nbandt/nbandt);
				maxb = rangek + rangeE*(adj*adj/nbandt/nbandt);
			}
			dummycb.min = minb;	//minimum energy in this arrangement
			dummycb.max = maxb;	//maximum energy in this arrangement
			dummyvb.min = minb;	//minimum energy in this arrangement
			dummyvb.max = maxb;	//maximum energy in this arrangement
//				dummycb.eUse = 0.5*(minb+maxb);	//for now, just do half. Can figure out statistically correct average later
/*
finding the average KE. Assumption (invalid): The states are uniformly distributed across the state. While incorrect
//for these bands, in the future, I plan on allowing completely custom energy bands. These could both be increasing or
//descreasing.
<KE> = ( INT E * exp(-E/kT) from E_bot to E_top ) / (INT exp(-E/kT) from E_bot to E_top)
<KE> = (kT * (kT + Emin)exp(-Emin/kT) - kT * (kT + Emax)exp(-Emax/kT) ) / (-kT * (exp(-Emax/kT) - exp(-Emin/kT)))
<KE> = kT + (Emin * exp(-Emin/kT) - Emax * exp(-Emax/kT)) / (exp(-Emin/kT) - exp(-EMax/kT))
*/
			double expmin = -minb * beta;
			double expmax = -maxb * beta;
			expmin = exp(expmin);
			expmax = exp(expmax);
			dummycb.boltzmax = 1.0 / (expmax - expmin);	//for determining how many carriers overlap when two ranges overlap
			dummyvb.boltzmax = 1.0 / (expmax - expmin);

			dummycb.eUse=(minb + maxb) * 0.5;	//so go with the middle of the road, which is likely the same
			dummyvb.eUse=(minb + maxb) * 0.5;

			dummycb.SetDensity(sqrt(dummycb.min), sqrt(dummycb.eUse), sqrt(dummycb.max));
			dummyvb.SetDensity(sqrt(dummyvb.min), sqrt(dummyvb.eUse), sqrt(dummyvb.max));
			

			
			difE = pow(maxb, 1.5) - pow(minb, 1.5);	//change in energy, eV.

			if(MatProps->CBCustom.size() == 0)
				dummycb.SetNumstates(prefcb * difE * space);
			else
			{
				double minD, maxD, useD;
				CustomDensStates stateData = GetStateDensity(minb, maxb, MatProps->CBCustom, dummycb.eUse, minD, useD, maxD);
				dummycb.SetNumstates(stateData.density * space);
				dummycb.SetDensity(minD, useD, maxD);
				dummycb.efm = MELECTRON * stateData.EFMCond;
				//dummycb.eUse = stateData.energy;  the energy is returned in GetSTateEnergy in the last variable passed in. Energy is now the sort value for the std::map
			}

			if(MatProps->VBCustom.size() == 0)
				dummyvb.SetNumstates(prefvb * difE * space);
			else
			{
				double minD, maxD, useD;
				CustomDensStates stateData = GetStateDensity(minb, maxb, MatProps->VBCustom, dummyvb.eUse, minD, useD, maxD);
				dummyvb.SetNumstates(stateData.density * space);
				dummyvb.SetDensity(minD, useD, maxD);
				dummyvb.efm = MELECTRON * stateData.EFMCond;
			}

				//KE = 0.5 mv^2, v = sqrt(2 * KE / m*), note KE is in eV, convert it to joules by multiplying by e- charge.
			dummycb.velocity = sqrt(2.0 * dummycb.eUse * QCHARGE / dummycb.efm) * 100.0;	//100 to get cm/s
			dummyvb.velocity = sqrt(2.0 * dummyvb.eUse * QCHARGE / dummyvb.efm) * 100.0;	//ditto...
			if(dummycb.velocity > THERMALVELOCITY)
				dummycb.velocity = THERMALVELOCITY;
			if(dummyvb.velocity > THERMALVELOCITY)
				dummyvb.velocity = THERMALVELOCITY;

			dummycb.targetTransfer = dummycb.GetNumstates() * PERCENT_TRANSFER_MIN;	//multiple of 2^-1, but approximately .01%
			dummyvb.targetTransfer = dummyvb.GetNumstates() * PERCENT_TRANSFER_MIN;

//CalcDelT and CalcScatter put data in that does not even get used ANYWHERE in the code
//			CalcDelT(dummycb, this);	//put the proper q*delT in each
//			CalcDelT(dummyvb, this);

//			CalcScatter(dummycb, mdesc.simulation->TransientData->timestep);
//			CalcScatter(dummyvb, mdesc.simulation->TransientData->timestep);

			//determine the constant for light functional emissions. Calculated by assuming a constant density of states in the width of the energy level, and assuming the number of carriers
			//takes on a boltzman distribution through the states.  The number of states will determine the boltzmann coefficient(temperature). The derivative is taken at the minimum energy, and if that is
			//multiplied by the bin width and results n the exponential jumping negatively by more than one (e^0=1), then a majority of the carriers are all in the first bin, and I should not be wasting the 70%
			//of simulation resources determining those functions...
			dummycb.carrierspectrumcutoff = (1.0 - exp(-(maxb-minb)/mdesc.simulation->EnabledRates.OpticalEResolution)) / ((maxb-minb) * mdesc.simulation->EnabledRates.OpticalEResolution);
			dummyvb.carrierspectrumcutoff = dummycb.carrierspectrumcutoff * dummyvb.GetNumstates();
			dummycb.carrierspectrumcutoff = dummycb.carrierspectrumcutoff * dummycb.GetNumstates();
			
			dummycb.uniqueID = mdl.uniqueIDctr++;	//store the current value and increment one
			stateSort.max = maxb;
			stateSort.min = minb;
			cb.energies.insert(std::pair<MaxMin<double>,ELevel>(stateSort, dummycb));	//will insert if it does not exist...
			cb.totalstates += dummycb.GetNumstates();

			dummyvb.uniqueID = mdl.uniqueIDctr++;	//store the current value
			vb.energies.insert(std::pair<MaxMin<double>,ELevel>(stateSort, dummyvb));	//it shouldn't ever exist, unless some funny impurities happened
			vb.totalstates += dummyvb.GetNumstates();

			cb.EffectiveDensity += dummycb.GetNumstates() * exp(-dummycb.eUse * beta);	//note eUse is relative to band minimum
			vb.EffectiveDensity += dummyvb.GetNumstates() * exp(-dummyvb.eUse * beta);

				
		}	//end for loop for setting up the bands

		niCarrier = sqrt(cb.EffectiveDensity*vb.EffectiveDensity*exp(-MatProps->Eg * beta));	//don't multiply by volume because Nc/Nveff aren't multiplied by volume
			
		CalcScatterConsts(scatterCBConst, cb.energies, -beta);	//sums up all exponents
		CalcScatterConsts(scatterVBConst, vb.energies, -beta);	//sums up all exponents


		for(itLay = inLayers.begin(); itLay != inLayers.end(); itLay++)
			PointImpurities(*(*itLay));	//load all the impurities into the material

		std::vector<Impurity*>::iterator itImp;
		for(itImp = MatProps->defects.begin(); itImp != MatProps->defects.end(); itImp++)
			PointDefects(*itImp);	//add the defects present in the material into the impurity

		BandTails(mdesc.simulation, *this);	//put the band tails if an urbach energy is present - must be before linking SRH

		LinkSRH(this);	//link all the SRH data. IF vectors are large for SRHConstants, the links would invalidate themselves as more are added.

		if(LightEmissions==NULL)
			LightEmissions = new LightSource;

		LightEmissions->light.clear();
		LightEmissions->enabled = mdesc.simulation->EnabledRates.LightEmissions;
		LightEmissions->times.insert(std::pair<double,bool>(0.0,LightEmissions->enabled));
		LightEmissions->center = position;
		LightEmissions->direction.x = LightEmissions->direction.y = LightEmissions->direction.z = 0.0;	//all directions
		LightEmissions->name = "PointLEmission";
		LightEmissions->centerinit = true;
		LightEmissions->sourceShape = NULL;
		LightEmissions->ID = -adj.self;	//should be a unique ID...
		LightEmissions->IncidentPoints.clear();
		if(mdesc.simulation->EnabledRates.Lgen == true		//there is no light whatsoever
			|| mdesc.simulation->EnabledRates.Ldef == true
			|| mdesc.simulation->EnabledRates.Lscat == true
			|| mdesc.simulation->EnabledRates.Lsrh == true
			|| mdesc.simulation->EnabledRates.Lpow == true
			|| mdesc.simulation->EnabledRates.SEdef == true
			|| mdesc.simulation->EnabledRates.SErec == true
			|| mdesc.simulation->EnabledRates.SEscat == true
			|| mdesc.simulation->EnabledRates.SEsrh == true
			|| mdesc.simulation->EnabledRates.LightEmissions == true)
		{
			delete OpticalOut;	//it should be null, but if not, clear out the old stuff
			OpticalOut = new OpticalStorage;
			OpticalOut->Allocate(mdesc.simulation->GetMaxOpticalSize());	//this may be huge and take awhile... but it's a one and done!
		}

//		WritePtEStates(this, "Initialize");
		//now load up the data for the targets/transitions. This will be done twice...
		//if this is the first time point's have data loaded, this will be kind of redundant.
		//In the event of an adaptive mesh, this is absolutely necessary to make sure it gets loaded.
		//it's a one time bit though, so it shouldn't be too bad.
/*		for(std::multimap<MaxMin<double>,ELevel>::iterator cb = cb.energies.begin(); cb != cb.energies.end(); ++cb)
			FindAllTargets(cb->second, this, mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator vb = vb.energies.begin(); cb != vb.energies.end(); ++vb)
			FindAllTargets(vb->second, this, mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = acceptorlike.begin(); es != acceptorlike.end(); ++es)
			FindAllTargets(es->second, this, mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = donorlike.begin(); es != donorlike.end(); ++es)
			FindAllTargets(es->second, this, mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = CBTails.begin(); es != CBTails.end(); ++es)
			FindAllTargets(es->second, this, mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = VBTails.begin(); es != VBTails.end(); ++es)
			FindAllTargets(es->second, this, mdl);
*/

		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_CB)
		{
			GroupThermalizationData tmp;
			ThermData.push_back(tmp);
			ThermData.back().Initialize(THERMALIZE_CB, mdesc.simulation->EnabledRates.Thermalize, this);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_CBT)
		{
			if(CBTails.size() > 0)
			{
				GroupThermalizationData tmp;
				ThermData.push_back(tmp);
				ThermData.back().Initialize(THERMALIZE_CBT, mdesc.simulation->EnabledRates.Thermalize, this);
			}
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_VB)
		{
			GroupThermalizationData tmp;
			ThermData.push_back(tmp);
			ThermData.back().Initialize(THERMALIZE_VB, mdesc.simulation->EnabledRates.Thermalize, this);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_VBT)
		{
			if(VBTails.size() > 0)
			{
				GroupThermalizationData tmp;
				ThermData.push_back(tmp);
				ThermData.back().Initialize(THERMALIZE_VBT, mdesc.simulation->EnabledRates.Thermalize, this);
			}
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_DON)
		{
			GroupThermalizationData tmp;
			for(unsigned int i=0; i<impurity.size(); ++i)
			{
				if (impurity[i]->isDonor())
				{
					ThermData.push_back(tmp);
					ThermData.back().Initialize(THERMALIZE_DON, mdesc.simulation->EnabledRates.Thermalize, this, impurity[i]);
				}
			}
			for(unsigned int i=0; i<MatProps->defects.size(); ++i)
			{
				if (MatProps->defects[i]->isDonor())
				{
					ThermData.push_back(tmp);
					ThermData.back().Initialize(THERMALIZE_DON, mdesc.simulation->EnabledRates.Thermalize, this, MatProps->defects[i]);
				}
			}
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_ACP)
		{
			GroupThermalizationData tmp;
			for(unsigned int i=0; i<impurity.size(); ++i)
			{
				if(impurity[i]->isAcceptor())
				{
					ThermData.push_back(tmp);
					ThermData.back().Initialize(THERMALIZE_ACP, mdesc.simulation->EnabledRates.Thermalize, this, impurity[i]);
				}
			}
			for(unsigned int i=0; i<MatProps->defects.size(); ++i)
			{
				if (MatProps->defects[i]->isAcceptor())
				{
					ThermData.push_back(tmp);
					ThermData.back().Initialize(THERMALIZE_ACP, mdesc.simulation->EnabledRates.Thermalize, this, MatProps->defects[i]);
				}
			}
		}

/*
		for (std::map<MaxMin<double>, ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}

		for (std::map<MaxMin<double>, ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}

		for (std::map<MaxMin<double>, ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}

		for (std::map<MaxMin<double>, ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}

		for (std::map<MaxMin<double>, ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}

		for (std::map<MaxMin<double>, ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
		{
			if(it->second.tStepCtrl==NULL)
				it->second.tStepCtrl = new TStepELevel;
		}
		//now create tStepCtrls for all the states

*/

	}	//end if it loaded a material (it seems like it should have...)
	else
	{
		//there is no material assigned to this point. Treat it like a vacuum...? It is now possible for
		//points to exist that are not within a layer, so this is needed.
		eg = 0.0;
		eps = 1.0;
		fermi = 0.0;
		fermin = 0.0;
		fermip = 0.0;
		impurity.clear();
		MatProps = mdesc.materials[vacID];
		n = 0.0;
		p = 0.0;
		niCarrier = 0.0;
		rho = 0.0;
		width.x = pxpy.x - mxmy.x;
		width.y = pxpy.y - mxmy.y;
		width.z = pxpy.z - mxmy.z;
		vol =width.x * width.y * width.z;	//there are waaay to many calls to Volume() to end up dividing by volume
		
		area.x = width.y * width.z;	//same for calculating various densities...
		area.y = width.x * width.z;
		area.z = width.y * width.x;

		if(vol != 0.0)
			voli = 1.0 / vol;			//so just store that number...

		if(area.x != 0.0)
			areai.x = 1.0 / area.x;

		if(area.y != 0.0)
			areai.y = 1.0 / area.y;

		if(area.z != 0.0)
			areai.z = 1.0 / area.z;

		ProgressInt(17, i);
		//CB and VB are already loaded as null.

	}

		
	return(0);
}

int BandTails(Simulation* sim, Point& pt)
{
	if(pt.MatProps == NULL)
		return(0);

	if(pt.MatProps->urbachCB.energy > 0.0)
	{
		double urbach = pt.MatProps->urbachCB.energy;
		double beta = -1.0*KBI / sim->T;
		MaxMin<double> key;
		ELevel tmpE(pt);
		std::multimap<MaxMin<double>, ELevel>::iterator node;
		tmpE.bandtail = true;
		tmpE.efm = 1.0;
		
		tmpE.imp = NULL;
		tmpE.percentTransfer = sim->TransientData ? sim->TransientData->percentCarrierTransfer : 0.0;
		tmpE.charge = 0;
		tmpE.carriertype = ELECTRON;
		tmpE.efm = pt.MatProps->EFMeCond * MELECTRON;
		tmpE.tau = 0.0;
		tmpE.taui = 0.0;
		tmpE.scatter.clear();
		tmpE.time.clear();

		double invUrb = -1.0/urbach;
		double nStates, densMin, densMax, densUse;
		
		tmpE.SetBeginOccStates(0.0);
		tmpE.SetEndOccStates(0.0);
		//going to just go ten energies out
		for(int i=0; i<10; ++i)
		{
			int j = i+1;
			tmpE.min = double(i) * urbach*0.5;	//going an order of magnitude over the urbach energy seems ridiculaous. This will get over 99% of the states.
			tmpE.max = double(j) * urbach*0.5;
			if(tmpE.max >= pt.MatProps->Eg)	//that's far enough...
				break;
			key.min = tmpE.min;
			key.max = tmpE.max;
			tmpE.eUse = 0.5* (tmpE.min + tmpE.max);
			
			densMin = pt.MatProps->Nc * pt.vol;
			densMax = densMin * exp(invUrb * tmpE.max);
			densUse = densMin * exp(invUrb * tmpE.min);
			densMin = densMin * exp(invUrb * tmpE.eUse);
			tmpE.SetDensity(densMin, densUse, densMax);

			nStates = pt.MatProps->Nc * pt.vol * urbach * (exp(invUrb * tmpE.min) - exp(invUrb * tmpE.max));	//g(e) = Nc exp(-E/U) -- integrate over E
			tmpE.SetNumstates(nStates);	
			tmpE.targetTransfer = nStates * PERCENT_TRANSFER_MIN;
			tmpE.boltzmax = 1.0 / (exp(beta * tmpE.min) - exp(beta * tmpE.max));
			tmpE.uniqueID = sim->mdlData->uniqueIDctr++;
			tmpE.carrierspectrumcutoff = (1.0 - exp(-(tmpE.max-tmpE.min)/sim->EnabledRates.OpticalEResolution)) * nStates / ((tmpE.max - tmpE.min) * sim->EnabledRates.OpticalEResolution);
			node = pt.CBTails.insert(std::pair<MaxMin<double>, ELevel>(key, tmpE));	//add the impurity in	
			CalcSRH(node->second, NULL, &pt, sim->T);
		}
	}


	if(pt.MatProps->urbachVB.energy > 0.0)
	{
		double urbach = pt.MatProps->urbachVB.energy;
		double beta = -1.0/(KB * sim->T);
		MaxMin<double> key;
		ELevel tmpE(pt);
		std::map<MaxMin<double>, ELevel>::iterator node;
		tmpE.bandtail = true;
		tmpE.efm = 1.0;
		
		tmpE.imp = NULL;
		tmpE.percentTransfer = sim->TransientData ? sim->TransientData->percentCarrierTransfer : 0.0;
		tmpE.charge = 0;

		tmpE.carriertype = HOLE;
		tmpE.efm = pt.MatProps->EFMhCond * MELECTRON;
		tmpE.tau = 0.0;
		tmpE.taui = 0.0;
		tmpE.scatter.clear();
		tmpE.time.clear();

		double invUrb = -1.0/urbach;
		double nStates;
		
		tmpE.SetBeginOccStates(0.0);
		tmpE.SetEndOccStates(0.0);
		//going to just go ten energies out
		for(int i=0; i<10; ++i)
		{
			int j = i+1;
			tmpE.min = double(i) * urbach;
			tmpE.max = double(j) * urbach;
			if(tmpE.max >= pt.MatProps->Eg)	//that's far enough...
				break;
			key.min = tmpE.min;
			key.max = tmpE.max;
			tmpE.eUse = 0.5* (tmpE.min + tmpE.max);
			nStates = pt.MatProps->Nv * pt.vol * urbach * (exp(invUrb * tmpE.min) - exp(invUrb * tmpE.max));	//g(e) = Nc exp(-E/U) -- integrate over E
			tmpE.SetNumstates(nStates);	
			tmpE.targetTransfer = nStates * PERCENT_TRANSFER_MIN;
			tmpE.boltzmax = 1.0 / (exp(beta * tmpE.max) - exp(beta * tmpE.min));
			tmpE.uniqueID = sim->mdlData->uniqueIDctr++;
			tmpE.carrierspectrumcutoff = (1.0 - exp(-(tmpE.max-tmpE.min)/sim->EnabledRates.OpticalEResolution)) * nStates / ((tmpE.max - tmpE.min) * sim->EnabledRates.OpticalEResolution);
			node = pt.VBTails.insert(std::pair<MaxMin<double>, ELevel>(key, tmpE));	//add the impurity in	
			CalcSRH(node->second, NULL, &pt, sim->T);
		}
	}

	return(0);
}

bool FoundPoint(std::vector<Point*>& PtList, long int ID)
{
	for(unsigned int i=0; i<PtList.size(); i++)
	{
		if(PtList[i]->adj.self == ID)
			return(true);
	}
	return(false);
}

//this function goes through all the spectrum lights and marks what points they will be incident on
int SpectrumIncidence(ModelDescribe& mdesc, Model& mdl)
{
	if(mdl.NDim == 1)	//must be along X's
	{
		for(unsigned int i=0; i<mdesc.spectrum.size(); i++)
		{

			if(mdesc.spectrum[i]->direction.x > 0)	//it is coming in from the left, use point zero.
			{
				if(FoundPoint(mdesc.spectrum[i]->IncidentPoints, 0) == false)
				{
					mdesc.spectrum[i]->IncidentPoints.push_back(&(mdl.gridp.begin()->second));
				}
			}	//end if incoming from the left, or negative x side
			else if(mdesc.spectrum[i]->direction.x < 0)	//it is coming in from the right, use furthest point.
			{
				if(FoundPoint(mdesc.spectrum[i]->IncidentPoints, mdl.numpoints-1) == false)
				{
					mdesc.spectrum[i]->IncidentPoints.push_back(&(mdl.gridp.rbegin()->second));
				}
			}	//end if incoming from the right or positive X side.


			//set the enable/disable flag
			if(mdesc.spectrum[i]->times.size() > 0)
			{
				mdesc.spectrum[i]->enabled = mdesc.spectrum[i]->times.begin()->second;
			}
			else
				mdesc.spectrum[i]->enabled = true;	//default to on if a spectrum is loaded

		}	//end going through all possible spectrums
	}	//end if it is one dimension
	return(0);
}

// this function goes through and puts in all the smart save state points
int SmartSaveState(ModelDescribe& mdesc)
{
	if(mdesc.simulation->TransientData==NULL) //these are irrelevant unless if it's a transient calc
		return(0);
	bool SaveState = mdesc.simulation->TransientData->SmartSaveState;
	//load all the light spectrum changes, contact voltage changes, etc.
	//first, the light:
	std::map<double,TargetTimeData>::iterator ptrTarget;
	std::vector<double> times;
	TargetTimeData ttd;
	ttd.SSTarget=-1;
	ttd.SaveState = SaveState;
	//first, a smart spot would be the point where to start taking data.
	ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(mdesc.simulation->TransientData->timeignore);
	if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
		mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(mdesc.simulation->TransientData->timeignore, ttd));

	for(unsigned int i=0; i<mdesc.spectrum.size(); i++)
	{

		for(std::map<double,bool>::iterator times = mdesc.spectrum[i]->times.begin(); times != mdesc.spectrum[i]->times.end(); ++times)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(times->first);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(times->first, ttd));
			}
			//it otherwise exists for this time, and a save state would not change
		}
	}
	//go through all the contact voltages
	std::vector<double> contactTimes;
	for (unsigned int i = 0; i<mdesc.simulation->TransientData->ContactData.size(); i++)
	{
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_COS_VOLT, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_COS_EFIELD, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_COS_CUR, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_COS_CURD, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_LIN_VOLT, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_LIN_EFIELD, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_LIN_CUR, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
		mdesc.simulation->TransientData->ContactData[i]->GetTimes(CTC_LIN_CURD, contactTimes);	//all the time changes for a fixed voltage dependence
		for(unsigned int j=0; j<contactTimes.size(); j++)
		{
			ptrTarget = mdesc.simulation->TransientData->TargetTimes.find(contactTimes[j]);
			if (ptrTarget == mdesc.simulation->TransientData->TargetTimes.end())
			{
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(contactTimes[j], ttd));
			}
		}
	}
	mdesc.simulation->TransientData->TargetTimes.erase(0.0);	//remove all instances where target time is zero
	mdesc.simulation->TransientData->TargetTimes.erase(-1.0);	//remove all instances where the target is forever

	//now  go through all the times and put targets in them to split up the solution convergence times
	if(mdesc.simulation->TransientData->SSByTransient==false) //if true, it's just gonna keep looping anyway!
	{
		std::vector<double> TTimes;
		TargetTimeData ttd;
		ttd.SaveState = false;
		ttd.SSTarget = -1;
		TTimes.push_back(0.0);	//make sure it starts at zero (all the zeroes were removed!)
		AddMapKeysToVector(mdesc.simulation->TransientData->TargetTimes, TTimes);
		if(TTimes.size() == 1)	//only the first one is there
			TTimes.push_back(mdesc.simulation->TransientData->simendtime);	//add a second bit
	
		for(unsigned int i=0; i<TTimes.size()-1; ++i)
		{
			double delta = (TTimes[i + 1] - TTimes[i]) / double(mdesc.simulation->TransientData->IntermediateSteps + 1);
			if(delta==0.0)	//don't do this multiple times
				continue;
			double curTime = TTimes[i];
			for (int j = 0; j<mdesc.simulation->TransientData->IntermediateSteps; ++j)
			{
				curTime+=delta;
				mdesc.simulation->TransientData->TargetTimes.insert(std::pair<double, TargetTimeData>(curTime, ttd));	//just creating points that will reset the min/maxocc
			}
		
		}
	}

	if (mdesc.simulation->TransientData->TargetTimes.size() > 0)	//should be true...
		mdesc.simulation->TransientData->curTargettime = mdesc.simulation->TransientData->TargetTimes.begin()->first;
	else
		mdesc.simulation->TransientData->curTargettime = mdesc.simulation->TransientData->simendtime;
	return(0);
}

//this will be called when a point is within a layer and needs to load the layer's impurities
int Point::PointDefects(Impurity* imp)
{
	if (imp == nullptr)
		return 0;

	ELevel dummy(*this);	//just a dummy elevel for this point's impurities
//	int div = mdesc.simulation->statedivisions;	//if the bands get broken up... into how many
	Simulation* sim = getSim();
	if(sim->TransientData)
		dummy.percentTransfer = sim->TransientData->percentCarrierTransfer;
	else
		dummy.percentTransfer = 0.0;

	dummy.SetNumstates(imp->getDensity() * vol);
	if(dummy.GetNumstates() <= 0.0)
		return(0);
	
	dummy.targetTransfer = dummy.GetNumstates() * PERCENT_TRANSFER_MIN;	//multiple of 2^-1, but approximately .01%
//	if(dummy.targetTransfer < TRANSFER_ABSOLUTE_MIN)
//		dummy.targetTransfer = TRANSFER_ABSOLUTE_MIN;


//	dummy.numstates = dummy.numstates * pt->vol;
	//concentration for this point based on position and size of the point
	dummy.imp = imp;	//have the ELEVEL know it is referencing an impurity and have access to its data		

	dummy.charge = imp->getCharge();
	dummy.bandtail = false;
	

	if(imp->isDonor())
	{
		dummy.carriertype = ELECTRON;
		dummy.efm = MatProps->EFMeCond * MELECTRON;	//electrons most likely to move around here
		dummy.tau = MatProps->MuN * dummy.efm / QCHARGE;
		dummy.taui = 1.0 / dummy.tau;
		if(dummy.charge < 1)	//an unoccupied donor must have at least +1 charge
			dummy.charge = 1;
	}
	else
	{
		dummy.carriertype = HOLE;
		dummy.efm = MatProps->EFMhCond * MELECTRON;	//holes most likely to move around
		dummy.tau = MatProps->MuP * dummy.efm / QCHARGE;
		dummy.taui = 1.0 / dummy.tau;
		if(dummy.charge > -1)	//acceptors contribute a negative charge when no hole is present
			dummy.charge = -1;	//otherwise (aka, a hole hasn't been formed), it is neutral
	}

	//THESE DON'T MATTER ANYMORE
//	CalcDelT(dummy, pt);	//put in proper q_delt for impurities, which is just 0.0's for now...
//	CalcScatter(dummy, mdesc.simulation->TransientData->timestep);

	switch (imp->getEnergyDistribution())	//how is this impurity distributed within the band
	{
	case Impurity::discrete:
		DiscreteImp(dummy, *imp, this, sim);
		break;
	case Impurity::banded:
		BandedImp(dummy, *imp, this, sim);
		break;
	case Impurity::gaussian:
		GaussianImp(dummy, *imp, this, sim);
		break;
	case Impurity::exponential:
		ExponentialImp(dummy, *imp, this, sim);
		break;
	case Impurity::linear:
		LinearImp(dummy, *imp, this, sim);
		break;
	case Impurity::parabolic:
		ParabolicImp(dummy, *imp, this, sim);
		break;
	default:
		break;
	}	//end switch statement

	//note, dummy is added to the states in the above functions

	return(0);
}

//used in OpticalWeightELevel
double ELevel::CalculateDensStateFactor(double mincurEnergy, double maxcurEnergy)
{
	//relMin is the lowest energy for the state (relative to VB edge)
	//relMax is the highest energy for the state (relative to VB edge)
	//curEnergy is the current energy it's being calculated for (relative to VB edge)
	double factor = 1.0;
	double relMax, relMin;
	CalculateRelativeHighLowEnergy(relMax, relMin);
	if(mincurEnergy > relMax || maxcurEnergy < relMin)	//this energy is not in the state
		return(0.0);

	if (bandtail == true)
	{
		if (carriertype == ELECTRON)	//this is an electron band tail. That means it follows an exponential distribution. The entire thing gets normalized
		{ //so having the right coefficient A in front of exp(A * (Energy-curEnergy)) is irrelevant, right? Wrong, the derivative changes. Need to pull the urbach energy.
			double bandmin = pt.eg;	// pt.cb.bandmin; This is now relative to the VB minimum
			if(maxcurEnergy > relMax)
				maxcurEnergy = relMax;
			if(mincurEnergy < relMin)
				mincurEnergy = relMin;
			double difEmin = bandmin - maxcurEnergy;	//this should be a positive number, and smaller in magnitude than difEmax
			double difEmax = bandmin - mincurEnergy;
			double urbach = pt.MatProps->urbachCB.energy;
			if (urbach <= 0.0)
				factor = 1.0;
			else if(maxcurEnergy == mincurEnergy)
				factor = exp(-difEmin/urbach);
			else
				factor = exp(-difEmin / urbach) - exp(-difEmax / urbach);
		}
		else //the carrier is a hole, so it's the VB Bandtails
		{
			double bandmin = 0.0;// pt.vb.bandmin; now relative to VB minimum
			if(maxcurEnergy > relMax)
				maxcurEnergy = relMax;
			if(mincurEnergy < relMin)
				mincurEnergy = relMin;

			double difEmin = mincurEnergy - bandmin;	//this should be a positive number
			double difEmax = maxcurEnergy - bandmin;	//this should be a positive number
			double urbach = pt.MatProps->urbachVB.energy;
			if (urbach <= 0.0)
				factor = 1.0;
			else if(maxcurEnergy == mincurEnergy)
				factor = exp(-difEmin / urbach);
			else
				factor = exp(-difEmin / urbach) - exp(-difEmax / urbach);
		}
	}
	else if (imp == NULL)
	{
		//then it is either the conduction or valence band
		if (carriertype == ELECTRON) //conduction band
		{
			if (pt.MatProps->CBCustom.size() == 0)
			{
				if(mincurEnergy < relMin)
					mincurEnergy = relMin;

				double relEnergy = mincurEnergy - relMin + min;	//relative energy to the conduction band minimum. min is relative to the band minimum, curE-relMin(which is min) is distance away
				if(maxcurEnergy > relMax)
					maxcurEnergy = relMax;

				double relEMax = relEnergy + (maxcurEnergy - mincurEnergy);

				if(relEMax > 0 && relEnergy < 0.0)
					relEnergy = 0.0;

				if (relEMax > 0.0)
					factor = pow(relEMax, 1.5) - pow(relEnergy, 1.5);
				else
					factor = 0.0;
			}
			else
			{
				double relEnergy = mincurEnergy - relMin + min;	//relative energy to the valence band minimum. min is relative to the band minimum, curE-relMin(which is min) is distance away
				std::map<double, CustomDensStates>::iterator it = pt.MatProps->CBCustom.lower_bound(relEnergy);
				if (it != pt.MatProps->CBCustom.end())
					factor = it->second.density;
				else
					factor = 0.0;
			}
		}
		else
		{
			if (pt.MatProps->VBCustom.size() == 0)
			{
				if(maxcurEnergy > relMax)	//if photon top energy > energy level max energy
					maxcurEnergy = relMax;	//only go as high as energy level max

				double relEnergy = relMax - maxcurEnergy + min;	//relative energy to the valence band minimum. min is relative to the band minimum going down, relMax(which is min) - curEnergy is distance away

				if(mincurEnergy < relMin)
					mincurEnergy = relMin;
				
				double relEMax = relEnergy + (maxcurEnergy - mincurEnergy);
				if(relEnergy < 0.0)
					relEnergy = 0.0;

				if (relEMax >= 0.0)
					factor = pow(relEMax, 1.5) - pow(relEnergy, 1.5);
				else
					factor = 0.0;
			}
			else
			{
				double relEnergy = relMax - mincurEnergy + min;	//relative energy to the valence band minimum. min is relative to the band minimum going down, relMax(which is min) - curEnergy is distance away
				std::map<double, CustomDensStates>::iterator it = pt.MatProps->VBCustom.lower_bound(relEnergy);
				if (it != pt.MatProps->VBCustom.end())
					factor = it->second.density;
				else
					factor = 0.0;
			}
		}
	}
	else //it is a defect or an impurity
	{
		double centerE, dif, dist, startE, endE, probl, probr, probc;
		XSpace start, stop;
		switch (imp->getEnergyDistribution())
		{
		case Impurity::discrete:
		case Impurity::banded:
			if(maxcurEnergy > relMax)
				maxcurEnergy = relMax;
			if(mincurEnergy < relMin)
				mincurEnergy = relMin;

			factor = (relMax == relMin) ? 1.0:maxcurEnergy - mincurEnergy;
			break;
		case GAUSSIAN:
			if (imp->getEnergyVariation() <= 0.0)
			{
				factor = 1.0;
				break;
			}
			
			start.x = imp->getRelativeEnergy();
			stop.x = start.x + imp->getEnergyVariation();
			dif = 1.0/FindSigmaSqGaussian(start, stop, 1);	//1D - this will be OK as long as energyvar != 0.0, which it doesn't if it got here
			
			centerE = (imp->isCBReference()) ? pt.eg - imp->getRelativeEnergy() : imp->getRelativeEnergy();

			if(mincurEnergy < relMin)
				mincurEnergy = relMin;
			if(maxcurEnergy > relMax)
				maxcurEnergy = relMax;

			dist = centerE - mincurEnergy;	//gets squared anyway, don't care about sign
			probl = exp(-dist*dist*0.5*dif);
			dist = centerE - 0.5 * (maxcurEnergy + mincurEnergy);	//gets squared anyway, don't care about sign
			probc = exp(-dist*dist*0.5*dif);
			dist = centerE - maxcurEnergy;	//gets squared anyway, don't care about sign
			probr = exp(-dist*dist*0.5*dif);
			factor = probl + probc + probc + probr;	//two trapezoids, but the width is constant (maxcurEnergy- mincurEnergy) / 2, and no need to cut in half - it all gets normalized anyway
			factor = factor * (maxcurEnergy - mincurEnergy);	//this is actually needed for the higher energy levels that will leak beyond - they would have way too high of a proportion
			//usually, this would be the same for all of them, but not in this particular case.

			break;
		case EXPONENTIAL:
			if (imp->getEnergyVariation() == 0.0)
			{
				factor = 1.0;
				break;
			}
			dif = log(2.0) / imp->getEnergyVariation();	//attenuate

			startE = (imp->isCBReference()) ? pt.eg - imp->getRelativeEnergy() : imp->getRelativeEnergy();

			if ((imp->getEnergyVariation() > 0.0 && carriertype == CONDUCTION) || (imp->getEnergyVariation() < 0.0 && carriertype == HOLE))
			{
				//this means the exponential gets smaller as the absolute energy in the band diagram goes down
				if(mincurEnergy > startE) //this is not even overlapping!
					factor = 0.0;
				else
				{
					dist = startE - mincurEnergy;	//this one is guaranteed to be OK
					probl = exp(-dif * dist);	//this should have a very large dist
					if(maxcurEnergy > startE)	//don't go beyond the top!
						maxcurEnergy = startE;
					dist = startE - maxcurEnergy;
					probr = exp(-dif * dist);	//this should have a small dist, therefore the larger prob
					factor = probr - probl;
				}
			}
			else //for this one, the exponential gets smaller as the absolute energy in the band diagram goes up!
			{
				if(maxcurEnergy < startE) //this does not overlap at all
					factor = 0.0;
				else
				{
					dist = maxcurEnergy - startE;	//this one is guaranteed to be OK
					probl = exp(-dif * dist);	//this should have a very large dist
					if(mincurEnergy < startE)	//don't go beyond the top!
						mincurEnergy = startE;
					dist = mincurEnergy - startE;
					probr = exp(-dif * dist);	//this should have a smaller dist, therefore the larger prob
					factor = probr - probl; 
				}
			}
			break;
		case LINEAR:
			if (imp->getEnergyVariation() == 0.0)
			{
				factor = 1.0;
				break;
			}
			startE = (imp->isCBReference()) ? pt.eg - imp->getRelativeEnergy() : imp->getRelativeEnergy();
			if ((imp->getEnergyVariation() > 0.0 && carriertype == CONDUCTION) || (imp->getEnergyVariation() < 0.0 && carriertype == HOLE))
			{
				//this means that it starts at the impurity energy and is adjusted linearly to equal 0 at e+energyvar (0 is below)
				//literally just y=mx+b, 
				endE = startE - fabs(imp->getEnergyVariation());	//the lowest value
				if(mincurEnergy > startE)	//then nothing happens, because it is completely above the states for this impurity
					factor = 0.0;
				else
				{
					if(mincurEnergy < endE)
						mincurEnergy = endE;
					dist = startE - mincurEnergy;
					probl = 1.0 - dist / fabs(imp->getEnergyVariation());
					if(maxcurEnergy > startE)
						maxcurEnergy = startE;
					dist = startE - maxcurEnergy;
					probr = 1.0 - dist / fabs(imp->getEnergyVariation());
					factor = (probl + probr) * (maxcurEnergy - mincurEnergy);
				}
			}
			else
			{
				//in this case, the distrubtion decreases at higher energies
				endE = startE + fabs(imp->getEnergyVariation());
				if(maxcurEnergy < startE)
					factor = 0.0;
				else
				{
					if(mincurEnergy < startE)
						mincurEnergy = startE;
					dist = mincurEnergy - startE;
					probl = 1.0 - dist / fabs(imp->getEnergyVariation());
					if(maxcurEnergy > endE)	//it has gone too far
						maxcurEnergy = endE;
					dist = maxcurEnergy - startE;
					probr = 1.0 / dist / fabs(imp->getEnergyVariation());
					factor = (probl + probr) * (maxcurEnergy - mincurEnergy);
				}
			}
			break;
		case PARABOLIC:
			if (imp->getEnergyVariation() == 0.0)
			{
				factor = 1.0;
				break;
			}
			if (carriertype == ELECTRON)
			{
				startE = pt.eg - imp->getRelativeEnergy() - fabs(0.5 * imp->getEnergyVariation());
				endE = startE + fabs(imp->getEnergyVariation());
			}
			else
			{
				startE = imp->getRelativeEnergy() - fabs(0.5 * imp->getEnergyVariation());
				endE = startE + fabs(imp->getEnergyVariation());
			}
			//now startE is the lowest 0 value, endE is the highest 0 value.
			if(mincurEnergy > endE || maxcurEnergy < startE)	//it does not overlap at all
			{
				factor = 0.0;
				break;
			}
			if(mincurEnergy < startE)
				mincurEnergy = startE;
			if(maxcurEnergy > endE)
				maxcurEnergy = endE;

			if(mincurEnergy < relMin)
				mincurEnergy = relMin;
			if(maxcurEnergy > relMax)
				maxcurEnergy = relMax;

			//integrate f(E) = -(E-minImp)(E-maxImp) with lower bounds as the lower/upper curEnergies.
			//but we are going to reference everything so minImp is zero as that will simplify the equations and make them more numerically accurate
			mincurEnergy -= startE;
			maxcurEnergy -= startE;
			endE -= startE;
			startE = 0.0;
			
			dif = maxcurEnergy * maxcurEnergy;	//just a place holder using a previously defined double - can't declare double's in switch
			probl = 0.5 * endE * dif - dif * maxcurEnergy / 3.0;
			dif = mincurEnergy * mincurEnergy;
			probr = 0.5 * endE * dif - dif * mincurEnergy / 3.0;
			factor = probl - probr;
			break;
		default:
			factor = 1.0;
			break;
		}
	}

	return(factor);	//default to return no effect
}

//this will be called when a point is within a layer and needs to load the layer's impurities
int Point::PointImpurities(Layer& lyr)
{
	std::vector<ldescriptor<Impurity*>*>::iterator itImp;
	double fraction;	//how much of the concentration should be used at this point
	ELevel dummy(*this);	//just a dummy elevel for this point's impurities
//	int div = mdesc.simulation->statedivisions;	//if the bands get broken up... into how many
	Simulation* sim = getSim();

	if(sim->TransientData)
		dummy.percentTransfer = sim->TransientData->percentCarrierTransfer;
	else
		dummy.percentTransfer = 0.0;
	for(itImp = lyr.impurities.begin(); itImp != lyr.impurities.end(); itImp++)
	{	//the impurity exists...
		//need to see if the impurity is actually valid for the point.	

		switch((*itImp)->flag)
		{
			case DISCRETE:
				fraction = 1.0;	//fraction is how the bit is spatially distributed
				break;
			case BANDED:	//want full power at high point, so don't normalize. Make = 1 at high point
				fraction = ProbBanded((*itImp)->range, this, false);
				break;
			case GAUSSIAN:	//by saying false.
				fraction = ProbGaussian((*itImp)->range, this, false);
				break;
			case GAUSSIAN2:
			case GAUSSIAN3:
				fraction = ProbGaussian2((*itImp)->range, this, false);
				break;
			case EXPONENTIAL:
				fraction = ProbExponential((*itImp)->range, this, false);
				break;
			case EXPONENTIAL2:
			case EXPONENTIAL3:
				fraction = ProbExponential2((*itImp)->range, this, false);
				break;
			case LINEAR:
				fraction = ProbLinear((*itImp)->range, this, false);
				break;
			case PARABOLIC:
				fraction = ProbParabolic((*itImp)->range, this, false);
				break;
			case PARABOLIC2:
			case PARABOLIC3:
				fraction = ProbParabolic2((*itImp)->range, this, false);
				break;
			default:
				fraction = 0.0;
		}
		if(fraction == 0.0)	//outside of the range within the layer
			continue;	//don't add any spots with 0 states...

		

		dummy.SetNumstates((*itImp)->data->getDensity() * fraction * (*itImp)->multiplier * vol);	//get the density for this point
//		dummy.numstates = dummy.numstates * pt->vol;	//and now that an appropriate density is present, use it...
		//dimensions of the model.
		if(dummy.GetNumstates() <= 0.0)
			continue;

		impurity.push_back((*itImp)->data);

		dummy.targetTransfer = dummy.GetNumstates() * PERCENT_TRANSFER_MIN;	//multiple of 2^-1, but approximately .01%
//		if(dummy.targetTransfer < TRANSFER_ABSOLUTE_MIN)
//			dummy.targetTransfer = TRANSFER_ABSOLUTE_MIN;

		//concentration for this point based on f(x,y,z), size, and number of dimensions
		dummy.imp = (*itImp)->data;	//have the ELEVEL know it is referencing an impurity and have access to its data		

		dummy.charge = (*itImp)->data->getCharge();
		dummy.bandtail = false;	//this is not a band tail...
		// not anymore... dummy.carriertype = ELECTRON;	//all impurities are treated as electrons! 

		if((*itImp)->data->isDonor())
		{
			dummy.carriertype = ELECTRON;
			dummy.efm = MatProps->EFMeCond * MELECTRON;	//electrons most likely to move around here
			dummy.tau = MatProps->MuN * dummy.efm / QCHARGE;
			dummy.taui = 1.0 / dummy.tau;

			if(dummy.charge < 1)	//an unoccupied donor must have at least +1 charge
				dummy.charge = 1;
		}
		else
		{
			dummy.carriertype = HOLE;
			dummy.efm = MatProps->EFMhCond * MELECTRON;	//holes most likely to move around
			dummy.tau = MatProps->MuP * dummy.efm / QCHARGE;
			dummy.taui = 1.0 / dummy.tau;
			
			if(dummy.charge > -1)	//accethisors contribute a negative charge when an electron is present
				dummy.charge = -1;	//otherwise (aka, a hole hasn't been formed), it is neutral
		}
// THESE DON'T MATTER ANYMORE
//		CalcScatter(dummy, sim->TransientData->timestep);
//		CalcDelT(dummy, this);	//put in proper q_delt for impurities, which is just 0.0's for now...

		switch((*itImp)->data->getEnergyDistribution())	//how is this impurity distributed within the band
		{
		case Impurity::discrete:
				DiscreteImp(dummy, *((*itImp)->data), this, sim);
				break;
		case Impurity::banded:
				BandedImp(dummy, *((*itImp)->data), this, sim);
				break;
		case Impurity::gaussian:
				GaussianImp(dummy, *((*itImp)->data), this, sim);
				break;
		case Impurity::exponential:
				ExponentialImp(dummy, *((*itImp)->data), this, sim);
				break;
		case Impurity::linear:
				LinearImp(dummy, *((*itImp))->data, this, sim);
				break;
		case Impurity::parabolic:
				ParabolicImp(dummy, *((*itImp))->data, this, sim);
				break;
			default:
				break;
		}	//end switch statement
		
		
	}


	return(0);
}
void ParabolicImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)
{
	double e = imp.getRelativeEnergy();
	double range = imp.getEnergyVariation();
	double start = e - range*0.5;
	double stop = e + range*0.5;
	std::map<MaxMin<double>, ELevel>::iterator node;
	double beta = -1.0 / (KB * sim->T);

	double prob = 0.0;
	double totalstates = dummy.GetNumstates();
	MaxMin<double> dummysort;
	if(range == 0.0)
	{
		dummy.max = e;
		dummy.min = e;	//same thing
		dummy.eUse = e;
		dummysort.min = dummy.min;
		dummysort.max = dummy.max;
		dummy.boltzmax = 1.0;
		dummy.SetDensity(totalstates, totalstates, totalstates);
		pt->impdefcharge += dummy.charge * dummy.GetNumstates();
		dummy.uniqueID = sim->mdlData->uniqueIDctr++;
		dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
		if(imp.isDonor())
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
			dummy.carriertype = ELECTRON;
			node = pt->donorlike.insert(std::pair<MaxMin<double>,ELevel>(dummysort, dummy));	//add the impurity in
		}
		else
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron
			dummy.carriertype = HOLE;
			node = pt->acceptorlike.insert(std::pair<MaxMin<double>,ELevel>(dummysort, dummy));	//add the impurity in
		}
		
		
		//because of the back links, can't fix it up until after it is added because it would be a new location in memory
		CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
		//energy levels to do SRH back to it
		
	}
	else	//cover the range...
	{
		double a2 = start * start;
		double a3 = a2 * start;
		double b2 = stop * stop;
		double b3 = b2 * stop;
		double normalize = fabs(6.0 / (a3 - b3 + 3*b2*start - 3*a2*stop));
		
		for(int i=0; i<10; i++)	//split into 10 spaces. #5 is the point at max.
		{
			int j = i + 1;
			dummy.min = start + (double)i/10.0*range;
			dummy.max = start + (double)j/10.0*range;
			if(dummy.max < dummy.min)	//always want the lower energy in the band diagrams
			{
				double temp = dummy.max;
				dummy.max = dummy.min;
				dummy.min = temp;
			}
			dummy.boltzmax = 1.0 / (exp(beta * dummy.max) - exp(beta * dummy.min));
			dummysort.min = dummy.min;
			dummysort.max = dummy.max;
			double center = (dummy.max + dummy.min)/2.0;	//center of the bit
			dummy.eUse = center;
			//normalizing bit
			double probl = normalize * (start - dummy.min) * (dummy.min - stop);
			double probc = normalize * (start-center) * (center-stop);
			double probr = normalize * (start - dummy.max) * (dummy.max - stop);
			dummy.SetDensity(probl, probc, probr);
			prob = (probl + probc)*(center - dummy.min)/2.0;
			prob += (probc + probr)*(dummy.max - center)/2.0;
			//parabola with 0 at the two start and stop points. normalize is the constant needed to make the area
			//underneath that parabola 1.


			dummy.SetNumstates(totalstates * prob);
			pt->impdefcharge += dummy.charge * dummy.GetNumstates();
			dummy.uniqueID = sim->mdlData->uniqueIDctr++;
			dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
			if(imp.isDonor())
			{
				dummy.carriertype = ELECTRON;
				dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
				dummy.boltzmax = -dummy.boltzmax;	//this is referenced to CB, but upside down. so flip the emax...
				node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
			}
			else
			{
				dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron
				dummy.carriertype = HOLE;
				node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
			}
			
			
			//because of the back links, can't fix it up until after it is added because it would be a new location in memory
			CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
			//energy levels to do SRH back to it


		}	//end for loop going through all the extra states
	}
	return;
}

void LinearImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)
{
	std::multimap<MaxMin<double>,ELevel>::iterator node;
	double range = imp.getEnergyVariation();	//start - bottom
	double e = imp.getRelativeEnergy();
	double totalstates = dummy.GetNumstates();
	double beta = 1.0 / (KB * sim->T);
	MaxMin<double> dummysort;
	if(range == 0.0)
	{
		dummy.max = e;
		dummy.min = e;
		dummy.eUse = e;
		dummysort.max = e;
		dummysort.min = e;
		dummy.SetDensity(totalstates, totalstates, totalstates);
		dummy.boltzmax = 1.0;
		pt->impdefcharge += dummy.charge * dummy.GetNumstates();
		dummy.uniqueID = sim->mdlData->uniqueIDctr++;
		dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
		if(imp.isDonor())
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
			dummy.carriertype = ELECTRON;
			node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		else
		{
			dummy.SetEndOccStates(0.0);	//acceptors don't start with an electron
			dummy.carriertype = HOLE;
			node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		
		
		//because of the back links, can't fix it up until after it is added because it would be a new location in memory
		CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
		//energy levels to do SRH back to it
	}
	else	//cover the range...
	{
		double delta = range;
		
		for(int i=0; i<10; i++)	//split into 10 spaces. #5 is the point at max.
		{
			int j = i + 1;
			dummy.min = e + (double)i/10.0*delta;
			dummy.max = e + (double)j/10.0*delta;
			if(dummy.max < dummy.min)	//always want the lower energy in the band diagrams
			{
				double temp = dummy.max;
				dummy.max = dummy.min;
				dummy.min = temp;
			}
			dummy.boltzmax = 1.0 / (exp(beta * dummy.max) - exp(beta * dummy.min));
			dummysort.min = dummy.min;
			dummysort.max = dummy.max;
			double center = (dummy.max + dummy.min)*0.5;	//center of the bit
			dummy.eUse = center;
			double distance = center - e;
			double prob = 1.0 - (distance)/range;
			prob = fabs(prob*2.0/range) * (dummy.max - dummy.min);	//normalize the probability, and get the area

			dummy.SetDensity(1.0-(dummy.min-e)/range,prob,1.0-(dummy.max-e)/range);
			//if range>0 --> distance>0 --> prob1>0 --> prob2>0
			//if range<0 --> distance<0 --> prob1<0 --> prob2<0... fabs.

			//this is linear, so the trapezoid trick of before is equivalent to a rectangle at the center.

			dummy.SetNumstates(totalstates * prob);

			if(dummy.GetNumstates() == 0.0)
				continue;	//don't add dead states
			
			pt->impdefcharge += dummy.charge * dummy.GetNumstates();
			dummy.uniqueID = sim->mdlData->uniqueIDctr++;
			dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);

			if(imp.isDonor())
			{
				dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
				dummy.carriertype = ELECTRON;
				dummy.boltzmax = -dummy.boltzmax;	//this is referenced to CB, but upside down. so flip the emax...
				node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
			}
			else
			{
				dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron
				dummy.carriertype = HOLE;
				node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
			}
			
			
			//because of the back links, can't fix it up until after it is added because it would be a new location in memory
			CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
			//energy levels to do SRH back to it


		}	//end for loop going through all the extra states
	}
	return;
}

void ExponentialImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)
{
	double start = imp.getRelativeEnergy();
	double halfdist = imp.getEnergyVariation();
	double totalstates = dummy.GetNumstates();
	MaxMin<double> dummysort;
	std::map<MaxMin<double>, ELevel>::iterator node;
	double beta = -1.0/(KB * sim->T);

	if(halfdist == 0.0)	//discrete point
	{
		dummy.max = start;
		dummy.min = start;	//same thing
		dummysort.min = start;
		dummysort.max = start;
		dummy.eUse = start;
		dummy.SetDensity(totalstates, totalstates, totalstates);
		dummy.boltzmax = 1.0;
		pt->impdefcharge += dummy.charge * dummy.GetNumstates();
		dummy.uniqueID = sim->mdlData->uniqueIDctr++;
		dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);

		if(imp.isDonor())
		{
			dummy.SetEndOccStates(totalstates);	//all the donors start with an electron in them
			dummy.carriertype = ELECTRON;
			node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		else
		{
			dummy.SetEndOccStates(totalstates);	//acceptors don't start with an electron
			dummy.carriertype = HOLE;
			node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		
		
		//because of the back links, can't fix it up until after it is added because it would be a new location in memory
		CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
		//energy levels to do SRH back to it
	}
	else	//follow an exponential pattern
	{
		double attenuate = log(2.0) / halfdist;
		double distance;
		double delta = -log(0.05)/attenuate;	//use up 95% of the points in the distribution
		totalstates = totalstates/0.95;	//increase the total states to adjust so 100% of the actual is used in the entire width
		
		for(int i=0; i<10; i++)
		{
			int j = i + 1;
			dummy.min = start + (double)i/10.0*delta;
			dummy.max = start + (double)j/10.0*delta;
			if(dummy.max < dummy.min)	//always want the lower energy in the band diagrams
			{
				double temp = dummy.max;
				dummy.max = dummy.min;
				dummy.min = temp;
			}
			dummy.boltzmax = 1.0 / (exp(beta * dummy.max) - exp(beta * dummy.min));

			dummysort.min = dummy.min;
			dummysort.max = dummy.max;
			double center = (dummy.max + dummy.min)*0.5;	//center of the bit
			dummy.eUse = center;

			distance = dummy.min - start;
			double probl = attenuate * exp(-attenuate * distance);
			distance = center - start;
			double probc = attenuate * exp(-attenuate * distance);
			distance = dummy.max - start;
			double probr = attenuate * exp(-attenuate * distance);	//do the trapezoidal area of exponent

			double totalprob = (probl + probc)*(center-dummy.min)*0.5;
			totalprob += (probc + probr)*(dummy.max-center)*0.5;

			dummy.SetDensity(probl, probc, probr);

			dummy.SetNumstates(totalstates * fabs(totalprob));

			//ProbFraction returns a whole number as a double, using probability to round up or down. (4.4 has a 40% chance of being 5)
			//two ways:
			//halfdist>0 --> delta>0 --> distance>0 --> attenuate>0, exp(-##). Check
			//halfdist<0 --> delta<9 --> distance<0 --> attenuate<0 exp(-(-#)(-#)) = exp(-#). and Check
			pt->impdefcharge += dummy.charge * dummy.GetNumstates();
			dummy.uniqueID = sim->mdlData->uniqueIDctr++;
			dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
			if(imp.isDonor())
			{
				dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
				dummy.carriertype = ELECTRON;
				dummy.boltzmax = -dummy.boltzmax;	//this is referenced to CB, but upside down. so flip the emax...
				node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in	
			}
			else
			{
				dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron- but they do start with holes
				dummy.carriertype = HOLE;
				node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in	
			}
			//normalized to 1.0
			//because of the back links, can't fix it up until after it is added because it would be a new location in memory
			CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
			//energy levels to do SRH back to it
		}	//end for loop
		
	}	//end else saying follow an exponential pattern
	
	return;
}

void GaussianImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)
{
	XSpace centerE;	//default 0's loaded in constructor
	XSpace halfWidthEPos;
	MaxMin<double> dummysort;
	std::multimap<MaxMin<double>,ELevel>::iterator node;
	double halfW = imp.getEnergyVariation();
	centerE.x = imp.getRelativeEnergy();	//max spot
	halfWidthEPos.x = centerE.x + halfW;	//half width

	double sigsq = FindSigmaSqGaussian(centerE, halfWidthEPos, 1);	//1D
	double totalstates = dummy.GetNumstates();

	if(sigsq == 0.0)
	{
		DiscreteImp(dummy, imp, pt, sim);
		return;
	}

	double startenergy = centerE.x - 2.0*halfW;	//the starting energy of the gaussian
	double delta = 4.0*halfW;	//two standard deviations (ish) in either direction
	double center;	//the point to get the probability from
	double dist; //the distance to the center of the bell curve
	double beta = -1.0 /(KB * sim->T);

	for(int i=0; i<11; i++)	//split into 11 spaces. #5 is the point at max.
	{
		int j = i + 1;
		dummy.min = startenergy + (double)i/11.0*delta;
		dummy.max = startenergy + (double)j/11.0*delta;
		if(dummy.max < dummy.min)	//always want the lower energy in the band diagrams
		{
			double temp = dummy.max;
			dummy.max = dummy.min;
			dummy.min = temp;
		}
		if(dummy.max <= 0.0)	//prevent impurities from being in the band for now. Screwing up testing
			continue;
		else if(dummy.min < 0.0)	//but dummy.max is >0
			dummy.min = 0.0;

		dummy.boltzmax = 1.0 / (exp(beta * dummy.max) - exp(beta * dummy.min));
		dummysort.min = dummy.min;
		dummysort.max = dummy.max;

		center = (dummy.max + dummy.min)*0.5;
		dummy.eUse = center;

		//cannot approximate this as just the probability at this point, need to get the CDF 
		dist = centerE.x - dummy.min;	//gets squared anyway, don't care about sign
		double probl = exp(-dist*dist/2.0/sigsq)/pow(2.0*PI*sigsq,0.5);
		dist = centerE.x - center;	//gets squared anyway, don't care about sign
		double probc = exp(-dist*dist/2.0/sigsq)/pow(2.0*PI*sigsq,0.5);
		dist = centerE.x - dummy.max;	//gets squared anyway, don't care about sign
		double probr = exp(-dist*dist/2.0/sigsq)/pow(2.0*PI*sigsq,0.5);

		dummy.SetDensity(probl, probc, probr);

		//add two trapezoids together for total probability contained within
		double totalprob = (probl + probc)*(center - dummy.min)/2.0;
		totalprob += (probc + probr)*(dummy.max-center)/2.0;

		dummy.SetNumstates(totalstates * totalprob);
		pt->impdefcharge += dummy.charge * dummy.GetNumstates();
		dummy.uniqueID = sim->mdlData->uniqueIDctr++;
		dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
		if(imp.isDonor())
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
			dummy.carriertype = ELECTRON;
			dummy.boltzmax = -dummy.boltzmax;	//this is referenced to CB, but upside down. so flip the emax...
			node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		else
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron
			dummy.carriertype = HOLE;
			node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		//because of the back links, can't fix it up until after it is added because it would be a new location in memory
		CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
		//energy levels to do SRH back to it
	}
	
	return;
}
//split the band of states up into 10 evenly distributed bands.
void BandedImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)	//the prelim dummy stuff is done...
{		//now set stuff to specific variables...
	double numstate = dummy.GetNumstates();
	double delta = imp.getEnergyVariation();
	std::multimap<MaxMin<double>,ELevel>::iterator node;
	MaxMin<double> dummysort;
	double beta = -1.0/(KB * sim->T);
	double dens = (delta!=0.0) ? numstate / delta: numstate;
	dummy.SetDensity(dens, dens, dens);

	for(int i=0; i<10; i++)
	{
		int j = i + 1;
		dummy.min = imp.getRelativeEnergy() - delta*0.5 + (double)i*0.1*delta;
		dummy.max = imp.getRelativeEnergy() - delta*0.5 + (double)j*0.1*delta;
		if(dummy.max < dummy.min)	//always want the lower energy in the band diagrams
		{
			double temp = dummy.max;
			dummy.max = dummy.min;
			dummy.min = temp;
		}
		dummy.boltzmax = 1.0 / (exp(beta * dummy.max) - exp(beta * dummy.min));
		
		dummy.eUse = 0.5*(dummy.min + dummy.max);
		dummysort.min = dummy.min;
		dummysort.max = dummy.max;

		dummy.SetNumstates(numstate * 0.1);	//keep it semi alive here...
		dummy.uniqueID = sim->mdlData->uniqueIDctr++;
		dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max-dummy.min)/sim->EnabledRates.OpticalEResolution)) * dummy.GetNumstates() / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
		pt->impdefcharge += dummy.charge * dummy.GetNumstates();
		//normalize the number of states - there's going to be 10 evenly distributed states here
		if(imp.isDonor())
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//all the donors start with an electron in them
			dummy.carriertype = ELECTRON;
			dummy.boltzmax = -dummy.boltzmax;	//this is referenced to CB, but upside down. so flip the emax...
			node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
		else
		{
			dummy.SetEndOccStates(dummy.GetNumstates());	//acceptors don't start with an electron, but they do with holes...
			dummy.carriertype = HOLE;
			node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the impurity in
		}
				
		//because of the back links, can't fix it up until after it is added because it would be a new location in memory
		CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
		//energy levels to do SRH back to it
	}
	return;
}

int LinkSRH(Point* pt)
{	
	SRHRateConstants* esSRHlink;
	SRHRateConstants* bsSRHlink;
	for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->acceptorlike.begin(); es != pt->acceptorlike.end(); ++es)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->cb.energies.begin(); bs != pt->cb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}

		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->vb.energies.begin(); bs != pt->vb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}
	}
	//donors
	for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->donorlike.begin(); es != pt->donorlike.end(); ++es)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->cb.energies.begin(); bs != pt->cb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}

		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->vb.energies.begin(); bs != pt->vb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}
	}

	//cb tails
	for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->CBTails.begin(); es != pt->CBTails.end(); ++es)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->cb.energies.begin(); bs != pt->cb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}

		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->vb.energies.begin(); bs != pt->vb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}
	}

	//vb tails
	for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->VBTails.begin(); es != pt->VBTails.end(); ++es)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->cb.energies.begin(); bs != pt->cb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}

		for(std::multimap<MaxMin<double>,ELevel>::iterator bs = pt->vb.energies.begin(); bs != pt->vb.energies.end(); ++bs)
		{
			esSRHlink = GetSRHConsts(&(es->second), (bs->second.uniqueID));
			bsSRHlink = GetSRHConsts(&(bs->second), (es->second.uniqueID));
			if(esSRHlink != NULL && bsSRHlink != NULL)
			{
				esSRHlink->invRate = bsSRHlink;
				bsSRHlink->invRate = esSRHlink;
			}
		}
	}



	return(0);
}

//calculates the SRH constants so doing SRH during tasks
//is simply multiplication of these constants by the concentrations (and time and volume to discretize)
int CalcSRH(ELevel& defectState, Impurity* imp, Point* pt, double temp)
{
//sigma_p = sigma_p(300) * (K300)^2 / (E_v - E_h+)^2
//sigma_p = (zq/(4eps(E_v - E_h+)))^2 / pi		// if no sigma_p at 300 k given
	if(defectState.imp == NULL && defectState.bandtail == false)
		return(0);	//this is not a state that has SRH
	if(pt == NULL)
		return(0);	//what the hell is going on
	if(pt->MatProps == NULL)
		return(0);	//seriously, this is messed up
	SRHRateConstants holder;	//temporary holder for the information which gets pushed into the vectors
	//using vectors because it's created once, and I will want fast, iterative traversal through all of them

//	double kt2 = KB*KB*90000.0;	use was commented out // (KT)^2 where T=300. Input data as if T=300K.
	
	double beta = 1e40;	//just a really really large number to practically ensure carriers move downward
	if(temp != 0.0)
		beta = KBI / temp;
		
	//first, make pointers to each of the states in this particular point
	//first do the conduction band, so rates 1 & 2
	defectState.SRHConstant.clear();	//it is only called upon creation, so don't allow inadvertent creation of duplicates
	

	double eps = EPSNOT * double(pt->MatProps->epsR);
	double degen, sigman, sigmap;
	short charge;
	if(imp != NULL)
	{
			degen = imp->getDegeneracy();
			sigman = imp->getElecCapture();
			sigmap = imp->getHoleCapture();
			charge = imp->getCharge();
	}
	else //it is a band tail
	{
		degen = 1.0;
		charge = 0;
		if(defectState.carriertype == ELECTRON)
		{
			sigman = pt->MatProps->urbachCB.captureCB;
			sigmap = pt->MatProps->urbachCB.captureVB;
		}
		else
		{
			sigman = pt->MatProps->urbachVB.captureCB;
			sigmap = pt->MatProps->urbachVB.captureVB;
		}
	}

	holder.invRate = NULL;	//links have to be done later. vectors change memory positions when added to or erased.
	
	for(std::multimap<MaxMin<double>,ELevel>::iterator cbS = pt->cb.energies.begin(); cbS != pt->cb.energies.end(); ++cbS)
	{
		//first find the capture cross section

		double sign = 0;
		double ve = 0;
		double expE = 1;

		if(sigman == 0.0)	//test to see if sigma has been calculated
		{
			int z = charge;	// its attraction when it has given up
			double num = double(z)*0.25*QCHARGE;	//include divide by 4 in denominator
			double den = cbS->second.eUse * eps;	// the energy is stored relatively
			if(den <= 0.0)
			{
				den = 0.5 * ( cbS->second.min +  cbS->second.max) * eps;
			}
			sign = num / den;
			sign = sign * sign * PII;
		}
		else	//translate from the 300 K equivalent
		{
			sign = sigman;	//average value at 300 Kelvin
	/*		doing so causes about 40% higher values than expected. Having a constant number provides a value significantly
	closer to various approximations.

			double den = bandStates[i]->eUse * bandStates[i]->eUse;
			if(den == 0.0)
			{
				den = (bandStates[i]->min + bandStates[i]->max);
				den = 0.25 * den * den;
			}
			if(den > 0.0)	//just assume it's the 300k equivalent...
				sign = sign * kt2 / den;
				*/
		}

		// now do the electron velocities
		// v_e = sqrt(2*(E - E_c)q/m_e) * 100 cm/m
		ve = 2.0 * cbS->second.eUse * QCHARGE / cbS->second.efm;
		//note, no *2 because the average of the max/min energy involves *0.5.
		ve = sqrt(ve) * 100.0;

		if(ve > THERMALVELOCITY)
			ve = THERMALVELOCITY;

		double BGdistance = 0.0;
		if(defectState.carriertype == ELECTRON)	//this is referenced to the conduction band, as is the CB
			BGdistance = -(cbS->second.eUse + defectState.eUse);
		else	//referenced to valence band, but going to the CB
			BGdistance = defectState.eUse - pt->MatProps->Eg - cbS->second.eUse;

		//want exp(-(Ec - Et)*beta) or VB: exp(-(Et - Ev)*beta)

		expE = exp(BGdistance * beta);

		//now use these values in assigning rates

		//for rate 1 (electrons fall into SRH)
		holder.sigmavelocity = ve * sign;	//cm/s * cm^2 = cm^3/sec
		holder.targetState = &defectState;
		
		holder.trgID = defectState.uniqueID;
		cbS->second.SRHConstant.push_back(holder);
		//for rate 2 (electrons emitted from defect)
		holder.sigmavelocity = holder.sigmavelocity * expE * degen;
		holder.targetState = &(cbS->second);
		holder.trgID = cbS->second.uniqueID;
		defectState.SRHConstant.push_back(holder);
	}	//end the conduction band states...

	//now do the valence band
	for(std::multimap<MaxMin<double>,ELevel>::iterator vbS = pt->vb.energies.begin(); vbS != pt->vb.energies.end(); ++vbS)
	{
		//first find the capture cross section

		double sigp = 0;
		double vh = 0;
		double expE = 1;

		if(sigmap == 0.0)	//test to see if sigma has been calculated
		{
			int z = charge;	// its attraction when it has given up
			double num = double(z)*0.25*QCHARGE;	//include divide by 4 in denominator
			double den = vbS->second.eUse * eps;	// the energy is stored relatively
			if(den <= 0.0)
			{
				den = 0.5 * (vbS->second.min + vbS->second.max) * eps;
			}
			sigp = num / den;
			sigp = sigp * sigp * PII;
		}
		else	//translate from the 300 K equivalent
		{
			sigp = sigmap;	//average value at 300 Kelvin
/*		see note on sign
			double den = bandStates[i]->eUse * bandStates[i]->eUse;
			if(den == 0.0)
			{
				den = (bandStates[i]->min + bandStates[i]->max);
				den = 0.25 * den * den;
			}
			if(den > 0.0)	//just assume it's the 300k equivalent...
				sigp = sigp * kt2 / den;		
*/
		}

		// now do the hole velocities
		// v_h = sqrt(2*(E_v - E)q/m_hj) * 100 cm/m
		vh = 2.0 * vbS->second.eUse * QCHARGE / vbS->second.efm;
		//note, no *2 because the average of the max/min energy involves *0.5.
		vh = sqrt(vh) * 100.0;

		if(vh > THERMALVELOCITY)
			vh = THERMALVELOCITY;

		double BGdistance = 0.0;
		if(defectState.carriertype == VALENCE)	//this is referenced to the valence band, as is the VB
			BGdistance = -(vbS->second.eUse + defectState.eUse);
		else	//referenced to CB band, but going to the VB
			BGdistance = defectState.eUse - pt->MatProps->Eg - vbS->second.eUse;

		//want exp(-(Ec - Et)*beta) or VB: exp(-(Et - Ev)*beta)

		expE = exp(BGdistance * beta);

		//now use these values in assigning rates

		//for rate 3. holes captured in defect by impurity
		holder.sigmavelocity = vh * sigp;
		holder.targetState = &defectState;
		holder.trgID = defectState.uniqueID;
		vbS->second.SRHConstant.push_back(holder);
		//for rate 4 (hole emission from trap to VB)
		holder.sigmavelocity = holder.sigmavelocity * expE / degen;
		holder.targetState = &(vbS->second);
		holder.trgID = vbS->second.uniqueID;
		//holder.invRate = &(defectState.SRHConstant[defectState.SRHConstant.size() - 1]);	//it was the last SRHConstant put in
		defectState.SRHConstant.push_back(holder);	//this originates in the VB and goes to the defect

	}	//end the valence band states...



	return(0);
}

void DiscreteImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim)
{
	std::map<MaxMin<double>, ELevel>::iterator node;
	MaxMin<double> dummysort;
	double NStates = dummy.GetNumstates();
	dummy.max = imp.getRelativeEnergy();
	dummy.min = imp.getRelativeEnergy();
	dummy.eUse = imp.getRelativeEnergy();
	dummysort.min = imp.getRelativeEnergy();
	dummysort.max = imp.getRelativeEnergy();
	dummy.boltzmax = 1.0;
	

	dummy.SetDensity(NStates, NStates, NStates);
	
	dummy.carrierspectrumcutoff = (1.0 - exp(-(dummy.max - dummy.min) / sim->EnabledRates.OpticalEResolution)) *NStates / ((dummy.max - dummy.min) * sim->EnabledRates.OpticalEResolution);
	if(imp.isDonor())
	{
		dummy.SetEndOccStates(NStates);	//all the donors start with an electron in them
		dummy.carriertype = ELECTRON;		//don't unnecessarily bias rho
	}
	else
	{
		dummy.SetEndOccStates(NStates);	//acceptors don't start with an electron... but they do start full of holes!
		dummy.carriertype = HOLE;
	}
	pt->impdefcharge += dummy.charge * NStates;

	dummy.uniqueID = sim->mdlData->uniqueIDctr++;
	if(dummy.carriertype == ELECTRON)
		node = pt->donorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the discrete(?) band tail in. Just being safe
	else
		node = pt->acceptorlike.insert(std::pair<MaxMin<double>, ELevel>(dummysort, dummy));	//add the discrete(?) band tail in. Just being safe

	//because of the back links, can't fix it up until after it is added because it would be a new location in memory
	CalcSRH(node->second, &imp, pt, sim->T);	//calculate all the SRH data for this particular node and allow the other
	//energy levels to do SRH back to it

	return;
}
double ProbDiscrete(void)	//beautiful... if I ever actually called this.
{
	return(1.0);
}
double ProbBanded(PtRange& range, Point* Pt, bool normalize)
{
	//need to check if it's within the band
	Basis* bas = new Basis();
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double mag = dif.Magnitude();
	bas->SetBasis(dif);
	//basis1 now points in direction of gaussian.
	//basis2 is parallel to it. Concern is how far along basis 1, or along the band.
	//does not matter how far across it is
	//the basis are normalized.
	XSpace BPt = bas->ReturnInBasis(Pt->position);
	XSpace BStart = bas->ReturnInBasis(range.startpos);

	double distance = BPt.x - BStart.x;

	delete bas;
	if(distance > 0 && distance < mag)
	{
		if(normalize)
			return(1.0/distance);
		else
			return(1.0);
	}
	return(0.0);
}

double ProbGaussian(PtRange& range, Point* Pt, bool normalize)
{
	Basis* bas = new Basis();
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	bas->SetBasis(dif);
	//basis1 now points in direction of gaussian.
	//basis2 is parallel to it. Concern is how far along basis 1, or along the gaussian.
	//does not matter how far across it is
	//the basis are normalized.
	XSpace BPt;
	XSpace BStart;
	BStart = bas->ReturnInBasis(range.startpos);
	BPt = bas->ReturnInBasis(Pt->position);
	double distance = fabs(BPt.x - BStart.x);
	double sigsq = FindSigmaSqGaussian(range.startpos, range.stoppos, 1);
	double prob;
	if(sigsq == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the gaussian start is within this point
			prob = 1.0;
		}
		else
			prob = 0.0;
	}
	else
	{
		if(normalize)
			prob = exp(-distance*distance/2.0/sigsq)/pow(2*PI*sigsq,0.5);
		else
			prob = exp(-distance*distance/2.0/sigsq);
	}
	delete bas;
	
	return(prob);
}

double ProbGaussian2(PtRange& range, Point* Pt, bool normalize)
{
	double sigsq = FindSigmaSqGaussian(range.startpos, range.stoppos, 2);
	double prob;
	if(sigsq == 0.0)	//sigma squared is 0, delta-dirac function.
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the gaussian start is within this point
			prob = 1.0;
		}
		else	//it will not exist
			prob = 0.0;
	}
	else
	{
		double dx = Pt->position.x - range.startpos.x;
		double dy = Pt->position.y - range.startpos.y;
		double exponent = dx*dx + dy*dy;
		exponent = exponent / (-2.0) / sigsq;
		if(normalize)
			prob = exp(exponent)/2*PI*sigsq;
		else
			prob = exp(exponent);
		/*
		double cprob = exp(exponent)/pow(2*PI*sigsq,0.5);

		dx = Pt->mxmy.x - range.startpos.x;
		dy = Pt->mxmy.y - range.startpos.y;
		exponent = dx*dx + dy*dy;
		exponent = exponent / (-2.0) / sigsq;
		double mxmyprob = exp(exponent)/pow(2*PI*sigsq,0.5);

		dx = Pt->pxpy.x - range.startpos.x;
		dy = Pt->mxmy.y - range.startpos.y;
		exponent = dx*dx + dy*dy;
		exponent = exponent / (-2.0) / sigsq;
		double pxmyprob = exp(exponent)/pow(2*PI*sigsq,0.5);

		dx = Pt->pxpy.x - range.startpos.x;
		dy = Pt->pxpy.y - range.startpos.y;
		exponent = dx*dx + dy*dy;
		exponent = exponent / (-2.0) / sigsq;
		double pxpyprob = exp(exponent)/pow(2*PI*sigsq,0.5);

		dx = Pt->pxpy.x - range.startpos.x;
		dy = Pt->pxpy.y - range.startpos.y;
		exponent = dx*dx + dy*dy;
		exponent = exponent / (-2.0) / sigsq;
		double mxpyprob = exp(exponent)/pow(2*PI*sigsq,0.5);

		double area = (Pt->pxpy.x - Pt->mxmy.x) * (Pt->position.y - Pt->mxmy.y) / 2.0;
			//(base) x (height)should be constant for all four triangles
		
		//first, focus on left triangle.
		double height = Smallest(mxmyprob, cprob, mxpyprob);
		double probL = height * area;	//prob L now holds the volume going up to the lowest point)
		//this leaves a trapezoidal pyramid with the base opposite to the said point

		*/

	}

	return(prob);
}
double Smallest(double a, double b, double c)
{
	if (a > b)
		a = b;
	if(a > c)
		a = c;
	return(a);
}
double ProbExponential(PtRange& range, Point* Pt, bool normalize)
{
	Basis* bas = new Basis();
	XSpace dif;
	double prob = 0.0;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double halfdist = dif.Magnitude();
	if(halfdist == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the exponential start is within this point, delta-dirac function
			prob = 1.0;
		}
		else	//otherwise it will not exist
			prob = 0.0;
	}
	else	//the points do separate...
	{
		double attenuate = log(2.0) / halfdist;
		bas->SetBasis(dif);
		//basis1 now points in direction of exponential.
		//basis2 is parallel to it. Concern is how far along basis 1, or along the exponential.
		//does not matter how far across it is
		//the basis are normalized.
		XSpace BPt;
		XSpace BStart;
		BStart = bas->ReturnInBasis(range.startpos);				
		BPt = bas->ReturnInBasis(Pt->position);
		double distance = BPt.x - BStart.x;	//along basis 1
		if(distance > 0)	//only care if it's in the positive direction of the exponential
		{	//the CDF is 1 - exp(-Ax) - need to know how much of the exponent is contained within the point

			if(normalize)
				prob = attenuate * exp(-attenuate * distance);
			else
				prob = exp(-attenuate * distance);		
			//normalized to 1.0
		}
		
	}
	delete bas;
	return(prob);
}

double ProbExponential2(PtRange& range, Point* Pt, bool normalize)
{
	Basis* bas = new Basis();
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double halfdist = dif.Magnitude();
	double prob = 0.0;
	if(halfdist == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the exponential start is within this point, delta-dirac function
			prob = 1.0;
		}
		else	//otherwise it will not exist
			prob = 0.0;
	}
	else	//the points do separate...
	{
		double attenuate = log(2.0) / halfdist;
		bas->SetBasis(dif);
		//basis1 now points in direction of exponential.
		//basis2 is parallel to it. Concern is how far along basis 1, or along the exponential.
		//does not matter how far across it is
		//the basis are normalized.
		XSpace BPt;
		XSpace BStart;
		BStart = bas->ReturnInBasis(range.startpos);				
		BPt = bas->ReturnInBasis(Pt->position);
		double distance = BPt.x - BStart.x;	//along basis 1
		if(distance > 0)	//only care if it's in the positive direction of the exponential
		{
			//now  use the actual distance to attenuate by
			distance = AbsDistance(BStart, BPt);	//these are orthonormal, so this is fine
			double exponent = -attenuate * distance;	//even though it's a different basis
			if(normalize)
				prob = attenuate/2.0/PI*exp(exponent);	//normalized distribution around a circle
			else
				prob = exp(exponent);
		}
	}

	delete bas;
	return (prob);
}

double ProbLinear(PtRange& range, Point* Pt, bool normalize)
{
	//nice and straightforward...
	Basis* bas = new Basis();
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double dist = dif.Magnitude();
	double prob = 0.0;
	if(dist == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the start is within this point, delta-dirac function
			prob = 1.0;
		}
		else	//otherwise it will not exist
			prob = 0.0;
	}
	else	//the points do separate...
	{
		bas->SetBasis(dif);
		XSpace BStart = bas->ReturnInBasis(range.startpos);
		XSpace BPt = bas->ReturnInBasis(Pt->position);
		prob = 1.0 - (BPt.x - BStart.x)/dist;
		if(prob > 1.0 || prob < 0.0)	//contain to within the points
			prob = 0.0;
		//normalize the probability. Follows a triangular distribution.
		//divide by area of triangle, h=1, b=dist /(bh/2) = 2/(bh)
		if(normalize)
			prob = prob*2/dist;
	}

	delete bas;
	return(prob);
}

double ProbParabolic(PtRange& range, Point* Pt, bool normalize)
{
	//nice and straightforward...
	Basis* bas = new Basis();
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double dist = dif.Magnitude();
	double prob = 0.0;
	if(dist == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the start is within this point, delta-dirac function
			prob = 1.0;
		}
		else	//otherwise it will not exist
			prob = 0.0;
	}
	else	//the points do separate...
	{
		bas->SetBasis(dif);
		XSpace BStart = bas->ReturnInBasis(range.startpos);
		XSpace BPt = bas->ReturnInBasis(Pt->position);
		double d = BPt.x - BStart.x;
		double a = 0.5/dist/dist;
		prob = 1.0 - a*d*d;
		if(prob > 1.0 || prob < 0.0)	//contain to within the points
			prob = 0.0;
		
		//normalize the probability
		if(normalize)
			prob = prob * pow(a,1.5) / (2.0*(a-1.0/3.0));
	}
	

	delete bas;
	return(prob);
}

double ProbParabolic2(PtRange& range, Point* Pt, bool normalize)
{
	//nice and straightforward...
	XSpace dif;
	dif.x = range.stoppos.x - range.startpos.x;
	dif.y = range.stoppos.y - range.startpos.y;
	double dist = dif.Magnitude();
	double prob = 0.0;
	if(dist == 0.0)
	{
		if(range.startpos.x <= Pt->pxpy.x
			&& range.startpos.x >= Pt->mxmy.x
			&& range.startpos.y >= Pt->mxmy.y
			&& range.startpos.y <= Pt->pxpy.y)
		{	//if the start is within this point, delta-dirac function
			prob = 1.0;
		}
		else	//otherwise it will not exist
			prob = 0.0;
	}
	else	//the points do separate...
	{
		double a = 0.5/dist/dist;	//used to get the distance to wht will be half way from the parabola
		dif.x = Pt->position.x - range.startpos.x;
		dif.y = Pt->position.y - range.startpos.y;
		double d = dif.Magnitude();	//distance to the high point
		prob = 1.0 - a*d*d;
		if(prob > 1.0 || prob < 0.0)	//contain to within the points
			return(0.0);	//no basis to delete, so just return
		//normalize the probability. Follows a triangular distribution.
		//divide by area of triangle, h=1, b=dist /(bh/2) = 2/(bh)
		if(normalize)
			prob = prob * pow(a,1.5) / (4.0*PI*(a-1.0/3.0));	//normalized around the circle
	}
	
	return(prob);
}

/*
F = m a = qE
m dv/dt = qE
m dv = q E dt
m del_v = q E del t
del momentum = q E del_t
del_t is change in time, so look at velocity and how long it takes to go across the point

v * del_t = width

0.5 m* v^2 = KE = E over band
sqrt(2E/m*) = v

del_t = width / sqrt(2E/m*)

because this is supposedly a constant energy, will approximate the particle to be a constant velocity.

this should also probably be stored in the eLevel and calculated initially, requiring only a multiplication by E.
would be an XSpace.

While this is not a constant energy within the point, it will be assumed that it is for now as the
charges will scatter, presumably to lower and lower energies and stay at roughly the same level in the band.
Absolutely horrible assumption, but it can be fixed later...

It needs to be fixed NOW.
*/
int CalcDelT(ELevel& elev, Point* pt)
{
	XSpace width;
	width.x = (pt->pxpy.x - pt->mxmy.x);
	width.y = (pt->pxpy.y - pt->mxmy.y);
	width.z = (pt->pxpy.z - pt->mxmy.z);
	double energy = (elev.max + elev.min)*QCHARGE;	//want 2E, so average E is (max + min) * 0.5, 2E is just max + min
	if(energy == 0.0)
		energy = 1e-5 * QCHARGE;

	double invsqrt = sqrt(energy / elev.efm);
	
	if(invsqrt > THERMALVELOCITY)
		invsqrt = THERMALVELOCITY;	//this is now the velocity

	//need to normalize qDelT for the momentum circles.
	//an electric field has a stronger effect on the slow moving particles, and all the momentum circles
	//are normalized so a particle contributes 1 for each direction as opposed to mv.  Therefore,
	//the electric field also needs to be divided by mv later.

	double normalize = invsqrt * elev.efm;		//velocity * mass
	normalize = 1.0/normalize;		// 1 / vm

	invsqrt = 1.0 / invsqrt;	//for divisions
	elev.time.x = width.x * invsqrt;	// x / v gives seconds in this time frame
	elev.time.y = width.y * invsqrt;
	elev.time.z = width.z * invsqrt;
	
	return(0);
}


int CalcScatter(ELevel& elev, double ts)
{
	//ts is the timestep for the entire step...
	if(elev.imp != NULL || elev.bandtail==true)	//electrons don't scatter in impurity or defect states. They're stuck...
	{
		elev.scatter.x = 0.0;
		elev.scatter.y = 0.0;
		elev.scatter.z = 0.0;
		return(0);
	}

	XSpace time = elev.time;	//pull out the times it takes an electron to travel in each direction
							//this has the max thermal velocity accounted for
	if(time.x > ts)			//if this exceeds the timestep, knock down to the timestep so it doesn't overscatter
		time.x = ts;
	if(time.y > ts)
		time.y = ts;
	if(time.z > ts)
		time.z = ts;

	double tauI = elev.taui;	//tau is previously set from the mobility of the device
	elev.scatter.x = 1.0 - exp(-time.x * tauI);
	elev.scatter.y = 1.0 - exp(-time.y * tauI);
	elev.scatter.z = 1.0 - exp(-time.z * tauI);

	return(0);	
}