#ifndef LOADSTATE_H
#define LOADSTATE_H

#include <vector>

class ModelDescribe;
class Model;
struct PtData;
class Point;
class Simulation;

class PointLinks
{
public:
	long int self;
	long int adjP;
	long int adjN;
	long int adjMX;
	long int adjMX2;
	long int adjPX;
	long int adjPX2;
	long int adjMY;
	long int adjMY2;
	long int adjPY;
	long int adjPY2;
	std::vector<long int> AngleData;
};

class SpecLinks
{
public:
	int specID;
	long int ptID;
};

class PsiCFLinks
{
public:
	long int ptID;
	std::vector<PtData> values;
};

class CLinkPostProcess
{
public:
	int srcID;
	int trgID;
	double resistance;
	double current;
};

class DataLinks
{
public:
	std::vector<PointLinks> PtLink;
	std::vector<SpecLinks> SpLink;
	std::vector<PsiCFLinks> PsiCF;
	std::vector<CLinkPostProcess> CLinks;
};



int LoadState(std::string fname, Model& mdl, ModelDescribe& mdesc);
int SetandCalcVars(ModelDescribe& mdesc, Model& mdl);
int LoadModelData(std::ifstream& file, ModelDescribe& mdesc, Model& mdl, std::stringstream& str, DataLinks& dLink);
int LoadPointData(std::ifstream& file, ModelDescribe& mdesc, Model& mdl, std::stringstream& str, DataLinks& PtrData);
int LoadBand(std::ifstream& file, Point* pt, ModelDescribe& mdesc, std::stringstream& str, bool band);
int LoadEState(std::ifstream& file, Point* pt, ModelDescribe& mdesc, std::stringstream& str, bool band);
int LoadMDescData(std::ifstream& file, ModelDescribe& mdesc, DataLinks& postProcess);
int LoadMaterialData(std::ifstream& file, ModelDescribe& mdesc);
int LoadLayerData(std::ifstream& file, ModelDescribe& mdesc);
int LoadImpurityData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str);
int LoadSimulationData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str, DataLinks& postProcess);
int LoadContactData(std::ifstream& file, Simulation* sim, std::stringstream& str, DataLinks& postProcess);
int LoadSpectrumData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str, DataLinks& postProcess);
int LinkPoints(Model& mdl, ModelDescribe& mdesc, DataLinks postP);
int LoadTransContactData(std::ifstream& file, Simulation* sim, std::stringstream& str, DataLinks& postProcess);	//there's more data on the stream...
/*
Need to relink:
contaqct->point when point is loaded
spectrum->incident points
calculate srh constants
*/

#endif