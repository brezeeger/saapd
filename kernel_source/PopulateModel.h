#ifndef POPULATEMODEL
#define POPULATEMODEL

#include <vector>
#include <map>

class ModelDescribe;
class Model;
class CustomDensStates;
class Contact;
class XSpace;
class ELevel;
class Point;
class Layer;
class Impurity;
class PtRange;
class Simulation;

int PopulateModel(ModelDescribe& mdesc, Model& mdl);
int CheckAdj(ELevel* src, Point* trgPt, int type);
CustomDensStates GetStateDensity(double startE, double endE, std::map<double, CustomDensStates>& band, double& retEnergy, double& densMin, double& densUse, double& densMax);
int SelectContactPoints(Model& mdl, ModelDescribe& mdesc);
int CreateExternalContactPoints(Contact* ctc, Model& mdl);
double FindSigmaSqGaussian(XSpace center, XSpace halfwidthPos, int dim);
int PointImpurities(ModelDescribe& mdesc, Point* pt, Layer& lyr);
int PointDefects(ModelDescribe& mdesc, Point* pt, Impurity* imp);
//all these probabilities return the probability at the point and NOT over a distribution of space.
double ProbBanded(PtRange& range, Point* Pt, bool normalize);
double ProbGaussian(PtRange& range, Point* Pt, bool normalize);
double ProbGaussian2(PtRange& range, Point* Pt, bool normalize);
double ProbExponential(PtRange& range, Point* Pt, bool normalize);
double ProbExponential2(PtRange& range, Point* Pt, bool normalize);
double ProbLinear(PtRange& range, Point* Pt, bool normalize);
double ProbParabolic(PtRange& range, Point* Pt, bool normalize);
double ProbParabolic2(PtRange& range, Point* Pt, bool normalize);
double Smallest(double a, double b, double c);
int CalcDelT(ELevel& elev, Point* pt);
int CalcScatter(ELevel& elev, double ts);

int CalcSRH(ELevel& defectState, Impurity* imp, Point* pt, double temp);
int LinkSRH(Point* pt);

void DiscreteImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);
void BandedImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);
void GaussianImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);
void ExponentialImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);
void LinearImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);
void ParabolicImp(ELevel& dummy, Impurity& imp, Point* pt, Simulation* sim);

int VacuumMaterial(ModelDescribe& mdesc);	//creates a default vacuum material if one does not exist



int SmartSaveState(ModelDescribe& mdesc);
int SpectrumIncidence(ModelDescribe& mdesc, Model& mdl);
bool FoundPoint(std::vector<Point*>& PtList, long int ID);

int BandTails(Simulation* sim, Point& pt);

#define CARTYPE_UNKNOWN 0
#define CARTYPE_CB 1
#define CARTYPE_VB 2
#define CARTYPE_DON 3
#define CARTYPE_ACP 4
#define CARTYPE_BTC 5
#define CARTYPE_BTV 6




#endif