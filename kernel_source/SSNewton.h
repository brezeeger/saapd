#ifndef SAAPD_SSNEWTON_H
#define SAAPD_SSNEWTON_H

#include <vector>

#define SSN_START_CHANGE 1e150
#define SSN_MAX_CHANGE 1.0e170	//when there's light pushing carriers to a brand new state, it will go from very empty to very full
#define SSN_EXIT_RATE_PERCENT_CHANGE_SEC 1.0e-6	//it will change by 0.1% / second, which means it is probably alternating back and forth around a numerical solution
#define SSN_CLAMP_ADJUST 1.0e-3


class Model;
class ModelDescribe;
class kernelThread;
class ELevel;
class SS_CurrentDerivFactors;
class Simulation;
class SteadyState_SAAPD;
class Point;

double SSNewtonPoint(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, Point* pt, SteadyState_SAAPD& data);
double SSNewton(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, bool fullMatrix=false);
int CalcAnalyticalJacobian(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrix, double* rhs, bool* useELevel, unsigned int sz, bool fullMatrix);
int ClearUnusedLevels(double* derivMatrix, double* rhs, bool* use, unsigned int sz);
int OptimizeMatrix(SteadyState_SAAPD& data, double* derivMatrix, unsigned int sz);
double DetermineClamp(SteadyState_SAAPD& data, double* delOccs, unsigned int sz, double& largestChange);
bool CalculateELevelRate(unsigned long int src, unsigned long int trg, std::vector<ELevel*> states, Simulation* sim, double& rate);
int CalculateELevelDeriv(unsigned long int src, unsigned long int trg, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, std::vector<ELevel*>& states, Simulation* sim, int whichsourceSrc, int whichsourceTrg);
int CalculateELevelDeriv(ELevel* src, ELevel* trg, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, Simulation* sim, int whichsourceSrc, int whichsourceTrg);

#endif