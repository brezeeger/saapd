#ifndef POISSONPREP_H
#define POISSONPREP_H



#define POISSON_PX 0	//used for the left hand side of poisson equation
#define POISSON_MX 1	//create a bit of order so contacts can access the correct
#define POISSON_PY 2	//psi on the LHS
#define POISSON_MY 3
#define POISSON_PZ 4
#define POISSON_MZ 5
#define POISSON_EXT 6
#define POISSON_INCREMENT_EXTRAS 7	//increment this after each element where an increase was used

class Model;
class SystemEq;
class ModelDescribe;
class Point;

int PoissonPrep(Model& mdl, SystemEq* System, ModelDescribe& mdesc);
int ModelEdge(Point& pt, int ndim);

#endif