#ifndef SAAPD_LOAD_H
#define SAAPD_LOAD_H

#include <vector>
#include <map>

class XSpace;
class Material;
class Optical;
class Impurity;
class Layer;
class ModelDescribe;
class Contact;
class Wavelength;
class Shape;
class Simulation;
struct CustomDensStates;
class LightSource;
class PtRange;
class Model;
class Environment;
class TransientSimData;
class TransContact;

template <class prop>
class ldescriptor;

int Load(std::string filename, ModelDescribe& mdl, Model& model, bool append);
size_t FindTag(std::string input, std::string& output, size_t start);
LightSource LoadSpectrum(std::string filename);
void LoadMaterialChildren(std::ifstream& file, Material* matl, ModelDescribe& mdl);
void LoadOpticalChildren(std::ifstream& file, Optical* optics, ModelDescribe& mdl);
//void LoadImpurityChildren(std::ifstream& file, Impurity* impur, ModelDescribe& mdl);
void LoadElectricalChildren(std::ifstream& file, Material* matl, ModelDescribe& mdl);
void LoadAlphaChildren(std::ifstream& file, Optical* optics, ModelDescribe& mdl, double factor);
void LoadLayerChildren(std::ifstream& file, Layer* lyr, ModelDescribe& mdl);
void LoadDeviceChildren(std::ifstream& file, ModelDescribe& mdl, bool forGUI=false);
void LoadVertex(std::ifstream& file, Shape* shape);
XSpace LoadXSpace(std::ifstream& file, std::string closetag="");
void LoadResolution(std::ifstream& file, ModelDescribe& mdl);
void LoadDimension(std::ifstream& file, ModelDescribe& mdl);
bool LoadSpectrumChildren(std::ifstream& file, ModelDescribe& mdl, LightSource* spectrum, bool preventConversion=false);
XSpace LoadGenericXSpace(std::ifstream& file, std::string closeTag);
void LoadWavelength(std::ifstream& file, std::vector<Wavelength>& WvVect);
void LoadTimeActive(std::ifstream& file, LightSource* spec);
void LoadFixedChildren(std::ifstream& file, Contact* ctc);
void LoadCosChildren(std::ifstream& file, Contact* ctc);
void LoadSimulationChildren(std::ifstream& file, ModelDescribe& mdl);
//void LoadDefectChildren(std::ifstream& file, Impurity* impur, ModelDescribe& mdl);
void LoadDefects(std::ifstream& file, Material* matl, ModelDescribe& mdl);
void LoadLDescMatChildren(std::ifstream& file, ldescriptor<Material*>* ldesc, ModelDescribe& mdl);
void LoadLDescImpChildren(std::ifstream& file, ldescriptor<Impurity*>* ldesc, ModelDescribe& mdl);
XSpace LoadStartPos(std::ifstream& file);
XSpace LoadStopPos(std::ifstream& file);
PtRange LoadPtRange(std::ifstream& file, ModelDescribe& mdl);
void LoadMaterialNKData(std::ifstream& file, Optical* optics);
void LoadShapeChildren(std::ifstream& file, Shape* shape);
void LoadCalcs(std::ifstream& file, Simulation* sim);

void LoadContactLink(std::ifstream& file, Simulation* sim);
void LoadSSContactChildren(std::ifstream& file, Simulation* sim, Environment& env);
void LoadSSEnvironmentChildren(std::ifstream& file, Simulation* sim);
void LoadSteadyStateChildren(std::ifstream& file, ModelDescribe& mdesc, bool merge=false);
void LoadSpectraTimeChildren(std::ifstream& file, TransientSimData* tSim);
void LoadTransientChildren(std::ifstream& file, ModelDescribe& mdesc, bool merge=false);

void LoadAddFlagsChildren(std::ifstream& file, TransContact *ctc);

void LoadCustomDOS(std::ifstream& file, std::map<double,CustomDensStates>& band);

void LoadValidOut(std::ifstream& file, Simulation* sim);
void LoadUrbachChildren(std::ifstream& file, Material* matl, bool band);


#endif