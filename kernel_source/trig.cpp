#include <cmath>
#include "BasicTypes.h"
#include "physics.h"


//cos(theta) = X/hypotenuse

double FindTheta(XSpace pos1, XSpace pos2)
{
	double theta = 0.0;
	XSpace delta;
	delta.x = pos2.x - pos1.x;
	delta.y = pos2.y - pos1.y;
	delta.z = pos2.z - pos1.z;
	double mag = delta.Magnitude();
	if(mag == 0.0)
		return(0.0);	//same points - no angle
	theta = acos(delta.x / mag);	//returns a value from 0 to PI.
	if(delta.y < 0)
		theta = -theta + PI2;	//reflect the angle across the X axis and put it back into 0...2*PI

	return(theta);
}

double FindPhi(XSpace pos1, XSpace pos2)
{
	XSpace delta;
	delta.x = pos2.x - pos1.x;
	delta.y = pos2.y - pos1.y;
	delta.z = pos2.z - pos1.z;
	double mag = delta.Magnitude();
	double phi = acos(delta.z / mag);	//returns a value from 0 to PI.

	return(phi);
}

//easiest way to do this is translate them so vect1 goes onto 0 point and vect2 leaves from 0 point
double FindThetaVectortailtoTail(XSpace inVect, XSpace outvect, bool retCosTheta)
{
	double theta = 0.0;
	XSpace v1 = inVect * -1.0;	//want the vector to be as if going out from the zero, zero coordinate
	XSpace v2 = outvect;

	double dotproduct = v1.dot(v2);
	double mag1 = inVect.Magnitude();
	double mag2 = outvect.Magnitude();
	if(retCosTheta)
	{
		if(mag1 > 0 && mag2 > 0)
			theta = dotproduct / (mag1 * mag2);	
	}
	else
	{
		if(mag1 > 0 && mag2 > 0)
			theta = acos(dotproduct / (mag1 * mag2));	//returns value from 0...PI, which is good.
	}

	return(theta);
}

//returns the point where a line segment intersects a plane in xspace ret, bool is success in finding it or not
bool LineIntersectPlane(XSpace& ret, XSpace linept0, XSpace linept1, XSpace PlaneNormal, XSpace ptPlane1)
{
/*
http://www.thepolygoners.com/tutorials/lineplane/lineplane.html
line equation: P = P_0 + t (P_1 - P_0)
Plane equation: N dot (P - P_2) = 0
let P be the point of intersection, n be the normal, and P_2 be a point on the plane
Solve for t by plugging in P and you get:

t = N dot (P_2 - P_0) / [N dot (P_1 - P_0)]
*/

	XSpace P2MP0 = ptPlane1 - linept0;
	XSpace P1MP0 = linept1 - linept0;
	double den = PlaneNormal.dot(P1MP0);
	if(den == 0.0 || den == -0.0)
	{
		ret.x = ret.y = ret.z = 0.0;
		return(false);
	}
	double num = PlaneNormal.dot(P2MP0);
	double t = num / den;
	ret = linept0 + P1MP0 * t;

	if(t>=0.0 && t<=1.0)
		return(true);

	return(false);
}