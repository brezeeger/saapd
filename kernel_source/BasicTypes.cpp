#include "BasicTypes.h"

#include <cmath>

#include "outputdebug.h"
#include "physics.h"
#include "helperfunctions.h"
#include "random.h"

extern MyRand SciRandom;


XSpace::XSpace()
{
	clear();
}
XSpace::XSpace(double x1, double y1, double z1)
{
	x = x1;
	y = y1;
	z = z1;
}
double XSpace::Magnitude(void)
{
	return(sqrt(x*x + y*y + z*z));
}
void XSpace::clear(void)
{
	x = y = z = 0.0;
	return;
}

void XSpace::keepMax(XSpace b) {
	if (b.x > x)
		x = b.x;
	if (b.y > y)
		y = b.y;
	if (b.z > z)
		z = b.z;
}
void XSpace::keepMin(XSpace b) {
	if (b.x < x)
		x = b.x;
	if (b.y < y)
		y = b.y;
	if (b.z < z)
		z = b.z;
}


bool XSpace::operator==(const XSpace &b) const
{
	if (x == b.x && y == b.y && z == b.z)
		return(true);
	return(false);
}

bool XSpace::operator>=(const XSpace &b) const
{
	if (x > b.x)
		return(true);
	else if (x < b.x)
		return(false);

	if (y > b.y)
		return(true);
	else if (y < b.y)
		return(false);

	if (z > b.z)
		return(true);
	else if (z < b.z)
		return(false);

	return(true);
}

bool XSpace::operator<=(const XSpace &b) const
{
	if (x < b.x)
		return(true);
	else if (x > b.x)
		return(false);

	if (y < b.y)
		return(true);
	else if (y > b.y)
		return(false);

	if (z < b.z)
		return(true);
	else if (z > b.z)
		return(false);

	return(true);
}

bool XSpace::operator!=(const XSpace &b) const
{
	if (x == b.x && y == b.y && z == b.z)
		return(false);
	return(true);
}

bool XSpace::operator>(const XSpace &b) const
{
	if (x > b.x)
		return(true);
	else if (x < b.x)
		return(false);
	//x's are ==
	if (y > b.y)
		return(true);
	else if (y < b.y)
		return(false);
	//y's ==
	if (z > b.z)
		return(true);
	else if (z < b.z)
		return(false);
	//z's ==
	return(false);	//they're ==
}

bool XSpace::operator<(const XSpace &b) const
{
	if (x < b.x)
		return(true);
	else if (x > b.x)
		return(false);
	//x's are ==
	if (y < b.y)
		return(true);
	else if (y > b.y)
		return(false);
	//y's ==
	if (z < b.z)
		return(true);
	else if (z > b.z)
		return(false);
	//z's ==
	return(false);	//they're ==
}
XSpace XSpace::operator-()
{
	XSpace tmp;
	tmp.x = -x;
	tmp.y = -y;
	tmp.z = -z;
	return(tmp);
}

XSpace& XSpace::operator +=(const XSpace &b)
{
	x += b.x;
	y += b.y;
	z += b.z;
	return *this;
}

XSpace XSpace::operator -(const XSpace &b) const
{
	XSpace ret;
	ret.x = x - b.x;
	ret.y = y - b.y;
	ret.z = z - b.z;
	return(ret);
}

XSpace XSpace::operator +(const XSpace &b) const
{
	XSpace ret;
	ret.x = x + b.x;
	ret.y = y + b.y;
	ret.z = z + b.z;
	return(ret);
}

XSpace XSpace::operator /(const double &b) const
{
	XSpace ret;
	double inv = 0.0;	//computer safe divide by zero (prevent things from blowing up, but send message)
	if (b == 0.0)
		ProgressCheck(44, false);	//call warning if /0
	else
		inv = 1.0 / b;

	ret.x = x*inv;
	ret.y = y*inv;
	ret.z = z*inv;

	return(ret);
}

XSpace XSpace::operator *(const double &b) const
{
	XSpace ret;

	ret.x = x*b;
	ret.y = y*b;
	ret.z = z*b;

	return(ret);
}

XSpace XSpace::operator *(const XSpace &b) const
{
	XSpace ret;

	ret.x = x*b.x;
	ret.y = y*b.y;
	ret.z = z*b.z;

	return(ret);
}

double XSpace::dot(XSpace b)
{
	double ret = 0.0;
	ret += x*b.x;
	ret += y*b.y;
	ret += z*b.z;
	return(ret);
}

int XSpace::Normalize(void)
{
	XSpace tmp = *this;
	double mag = tmp.Magnitude();
	if (mag == 0.0)	//then all of them already were zero, so do nothing
		return(1);
	tmp = tmp / mag;	//this will autoflag an error if magnitude is zero. Luckily, this will not invalidate the XSpace
	x = tmp.x;	//value of 0,0,0...
	y = tmp.y;
	z = tmp.z;
	return(1);
}


void Vertex::Remove()
{

	Vertex* pt = this;
	pt->prev->next = pt->next;
	pt->next->prev = pt->prev;	//remove point from loop

	Vertex* ptUid = pt->next;
	while (ptUid != pt->parent->start)	//lower the id of all the remaining pointers
	{
		ptUid->id--;
		ptUid = ptUid->next;
	}

	if (pt == pt->parent->start)	//special case
		pt->parent->start = pt->next;	//will be NULL if there are no other points

	//update center average position

	pt->parent->center.x = pt->parent->center.x * (double)(pt->parent->getNumVertices()) - pt->pt.x;
	pt->parent->center.y = pt->parent->center.y * (double)(pt->parent->getNumVertices()) - pt->pt.y;
	pt->parent->numpoints--;
	pt->parent->center.x = pt->parent->center.x / (double)(pt->parent->getNumVertices());
	pt->parent->center.y = pt->parent->center.y / (double)(pt->parent->getNumVertices());

	if (pt->pt.x == pt->parent->max.x || pt->pt.x == pt->parent->min.x ||
		pt->pt.y == pt->parent->max.y || pt->pt.y == pt->parent->min.y)	//if this point held one of the maxes
		parent->SetMaxMin();	//recalculate the maximum and minimums

	pt->id = 0;
	pt->next = NULL;
	pt->parent = NULL;
	pt->prev = NULL;
	pt->pt.x = 0;
	pt->pt.y = 0;

	delete pt;	//now actually get rid of it
	pt = NULL;
	return;
}


Shape::Shape()
{
	type = 0;
	numpoints = 0;
	start = NULL;
	center.x = 0;
	center.y = 0;	//just make sure they get set to zero...
	max.x = 0;
	max.y = 0;
	min.x = 0;
	min.y = 0;
}
Shape::~Shape()
{
	Vertex* ptRemove = start->prev;	//start on last point added
	Vertex* ptNext = ptRemove->prev;	//goes in reverse order. Starts with last point, and the next point
	//to be removed is the one added prior to it.
	while (ptNext != ptRemove)
	{
		ptRemove->next->prev = ptRemove->prev;	//make the next point point to the earlier point
		ptRemove->prev->next = ptRemove->next;	//make the prev point's next go to a point still there...

		ptRemove->next = NULL;
		ptRemove->parent = NULL;
		ptRemove->prev = NULL;
		ptRemove->pt.x = 0.0;
		ptRemove->pt.y = 0.0;
		ptRemove->id = 0;

		delete ptRemove;

		ptRemove = ptNext;
		ptNext = ptRemove->prev;
	}	//there is only one point left in the shape, and it was ptNext, which is also ptRemove.

	delete ptRemove;	//delete the last point

	start = NULL;	//clear it out
	center.x = 0;	//really only important for curvy objects, like circles or ellipses.
	center.y = 0;
	type = 0;
	max.x = 0;
	max.y = 0;
	min.x = 0;
	min.y = 0;

}

double Shape::GetXYArea()
{
	if (start == NULL)
		return(0.0);

	double sumpos = 0.0;
	double sumneg = 0.0;

	Vertex* CurPt = start;

	do
	{
		sumpos += CurPt->pt.x * CurPt->next->pt.y;
		sumneg += CurPt->pt.x * CurPt->prev->pt.y;
		CurPt = CurPt->next;
	} while (CurPt != start);

	return(0.5*(sumpos - sumneg));

}

double Shape::GetYZArea()
{
	if (start == NULL)
		return(0.0);

	double sumpos = 0.0;
	double sumneg = 0.0;

	Vertex* CurPt = start;

	do
	{
		sumpos += CurPt->pt.y * CurPt->next->pt.z;
		sumneg += CurPt->pt.y * CurPt->prev->pt.z;
		CurPt = CurPt->next;
	} while (CurPt != start);

	return(0.5*(sumpos - sumneg));

}
//insert new point at end of line.
void Shape::AddPoint(XSpace pos)
{
	Vertex* newpt = new Vertex;
	if (start == NULL)	//first point in a shape
	{
		newpt->id = 0;
		newpt->next = newpt;
		newpt->prev = newpt;
		newpt->parent = this;
		start = newpt;
		max = pos;
		min = pos;

	}
	else
	{
		newpt->next = start;	//setup the newpt connections
		newpt->prev = start->prev;
		newpt->id = newpt->prev->id + 1;
		newpt->parent = this;
		//now modify the existing point connections
		start->prev->next = newpt;
		start->prev = newpt;

		//update the max and mins of the shape
		if (pos.x > max.x)
			max.x = pos.x;
		if (pos.y > max.y)
			max.y = pos.y;
		if (pos.x < min.x)
			min.x = pos.x;
		if (pos.y < min.y)
			min.y = pos.y;
	}
	newpt->pt = pos;

	//update the center of the shape (average)
	center.x = (center.x * (double)numpoints + pos.x);
	center.y = center.y * (double)numpoints + pos.y;
	numpoints++;
	center.x = center.x / (double)numpoints;
	center.y = center.y / (double)numpoints;

	return;
}

void Shape::RemoveLastPoint()
{
	if (start && start->prev) {
		start->prev->Remove();
		SetMaxMin();
	}
	return;
}

//insert new point at end of line.
void Shape::RemovePoint(int id)	//not sure why you'd do this, but whatevs.
{
	if (start == NULL)
		return;

	Vertex* pt = start;
	do
	{
		if (pt->id == id)
		{
			pt->Remove();	//remove pointer, fix everything around it
			SetMaxMin();
			return;
		}

		pt = pt->next;

	} while (pt != start);	//only check through everything once

	return;
}

void Shape::SetMaxMin(void)
{
	if (start == NULL)
	{
		max.x = 0;
		max.y = 0;
		min.x = 0;
		min.y = 0;
		return;
	}
	//set default conditions
	max.x = start->pt.x;
	max.y = start->pt.y;
	min.x = max.x;
	min.y = max.y;

	Vertex* ptr = start->next;
	while (ptr != start && ptr != NULL)
	{
		if (ptr->pt.x > max.x)
			max.x = ptr->pt.x;
		if (ptr->pt.y > max.y)
			max.y = ptr->pt.y;
		if (ptr->pt.x < min.x)
			min.x = ptr->pt.x;
		if (ptr->pt.y < min.y)
			min.y = ptr->pt.y;

		ptr = ptr->next;	//next vertex
	}
	return;
}

XSpace Shape::getPoint(int index)
{
	if (index >= numpoints || index < 0)
		return(XSpace());
	Vertex* vert = start;
	if (start == nullptr)
		return(XSpace());

	for (int i = 0; i < index; ++i)
	{
		if (vert->next)
			vert = vert->next;
		else
			return(XSpace());
	}

	return(vert->pt);
}

bool Shape::modifyVertex(int index, XSpace pos)
{
	if (index >= numpoints || index < 0 || start == nullptr)
		return(false);

	Vertex* vert = start;
	for (int i = 0; i < index; ++i)
	{
		if (vert->next)
			vert = vert->next;
		else
			return(false);
	}
	if (vert)
	{
		vert->pt = pos;
		SetMaxMin();
		return(true);
	}
	return(false);
}

bool Shape::InShape(XSpace pos, double deviation)
{

	if (start == NULL)
		return(false);	//layer still doesn't have a defined area


	if (type == SHPPOINT)	//it will have the resolution of the device
	{
		XSpace ptCorner;
		ptCorner.x = start->pt.x + deviation;

		if (pos.x > ptCorner.x)
			return(false);

		ptCorner.x = start->pt.x - deviation;
		if (pos.x < ptCorner.x)
			return(false);

		ptCorner.y = start->pt.y + deviation;
		if (pos.y >= ptCorner.y)
			return(false);

		ptCorner.y = start->pt.y - deviation;
		if (pos.y <= ptCorner.y)
			return(false);

		return(true);
	}
	else if (type == SHPLINE)	//see if point lies on line...
	{
		Line shapeboundary(start->pt, start->next->pt);
		//now check if this point lies on the line
		if (shapeboundary.vertical == true)
		{
			if (fabs(pos.x - shapeboundary.pt1.x) <= deviation)	//make sure the vertical line has
			{		//the right x coordinate
				if (pos.y <= max.y + deviation &&	//and that it is within
					pos.y >= min.y - deviation)	//the y coordinates
					return(true);
			}
		}
		double y1 = shapeboundary.m * pos.x + shapeboundary.b;
		if (fabs(y1 - pos.y) <= deviation)	//given the X position of the point, it would have 
		{			//position y1 if it were on the line.  Is this within the spacing of the grid points?
			if ((pos.x >= shapeboundary.pt1.x - deviation &&	//need to include in case the line has slope
				pos.x <= shapeboundary.pt2.x + deviation) ||	//0, which cancels out x.
				(pos.x >= shapeboundary.pt2.x - deviation &&
				pos.x <= shapeboundary.pt1.x + deviation))
				return(true);
		}
		//else false

	}
	else if (type <= RIGID2D)	//unknown number of vertices
	{
		Vertex* ptr = start;

		Line PostoVertexPL;		//the angle to be added
		Line PostoVertexMI;		//angle to be subtracted
		PostoVertexPL.pt1 = pos;
		PostoVertexMI.pt1 = pos;
		double angle = 0;
		double delang = 0;
		do	//loop going around all the points for PostoVertex
		{
			PostoVertexPL.pt2 = ptr->pt;
			PostoVertexMI.pt2 = ptr->prev->pt;
			PostoVertexPL.CalcAngles();
			PostoVertexMI.CalcAngles();
			delang = PostoVertexPL.phi - PostoVertexMI.phi;
			//delang could be way off... if the distance is greter than pi, the next point swept all the way around
			while (delang > PI)	//therefore, it swept 2PI to far, so go back two pi
			{
				delang -= 2.0*PI;
			}
			while (delang <= -PI)	//same can be said for the other direction (vertices reverse order)
			{	//if the point lies on the line, (say shape is line...) 
				delang += 2.0*PI;
			}

			angle += delang;


			ptr = ptr->next;
		} while (ptr != start);

		if (fabs(angle) > 6)	//angle can only equal 0 or +/- 2PI. If it's 2PI, it's within.
			return(true);	//don't want to be off due to rounding issues...
	}
	else if (type == CIRCLE)
	{
		//		XSpace distancevect = center;	//originally center
		XSpace radiusvect = start->pt;	//radius of the circle
		double radius = AbsDistance(center, radiusvect);
		double distance = AbsDistance(center, pos);

		if (distance <= radius)
			return(true);
	}

	return(false);	//else, it is not in the point
}

/*
see if two lines intersect or not
y = m_1*x + b_1
y = m_2*x + b_2
have the y's equal each other
m_1*x + b_1 = m_2*x + b_2
x = (b_2 - b_1) / (m_1 - m_2)
x holds the location of intersection of the lines

because x found on assumption that y's are equal, just make sure
x is within the bounds of the two lines.

Problem:
.
|\
| \
|  \
|   \  /|
|    \/.|
|       |
|_______|

The theory says it must cross an even number of the segments.
If it is within the right peak, it must cross two boundaries, but it does it at
the point. Problem is it comes back inside at a vertex of two segments. One segment has
it entering into the system, and one segment has it leaving the system.

OPTION: Have the vertex line go to the center of each boundary segment. No overlap.

New Solution...
Measure angle around vertices.  It should be 2pi if inside.  If it's outside, everything will cancel out and be 0
*/
bool Line::Intersect(Line& lin)
{
	//first double check to see if they share a same points. If so, they obviously cross!
	if (pt1 == lin.pt1)
		return(true);
	if (pt2 == lin.pt1)
		return(true);
	if (pt1 == lin.pt2)
		return(true);
	if (pt2 == lin.pt2)
		return(true);

	if (vertical && lin.vertical)
	{
		if (pt1.x == lin.pt1.x)
			return(true);

		return(false);
	}
	else if (vertical)
	{
		double y = lin.m * pt1.x + lin.b;	//y now holds the expected cross value of lin on this
		//so check to make sure y is within this
		if (pt1.y > pt2.y)
		{
			if (y <= pt1.y && y >= pt2.y)
				return(true);
		}
		else	//pt2 is higher
		{
			if (y <= pt2.y && y >= pt1.y)
				return(true);
		}
		return(false);
	}
	else if (lin.vertical)
	{
		double y = m * lin.pt1.x + b;	//y now holds the expected cross value of this on lin
		//so check to make sure y is within this
		if (lin.pt1.y > lin.pt2.y)
		{
			if (y <= lin.pt1.y && y >= lin.pt2.y)
				return(true);
		}
		else	//pt2 is higher
		{
			if (y <= lin.pt2.y && y >= lin.pt1.y)
				return(true);
		}
		return(false);
	}
	else	//neither are vertical lines!
	{
		if (lin.m == m)	//avoid divide by zero
		{
			if (lin.b == b)	//same slopes, and same intercepts. They have varying X's
			{	//check to see if either of lin's points are between this's points
				if (lin.pt1.x <= pt1.x && lin.pt1.x >= pt2.x)
					return(true);
				else if (lin.pt1.x <= pt2.x && lin.pt1.x >= pt1.x)
					return(true);
				else if (lin.pt2.x <= pt2.x && lin.pt2.x >= pt1.x)
					return(true);
				else if (lin.pt2.x <= pt1.x && lin.pt2.x >= pt2.x)
					return(true);
			}
			else
				return(false);
		}

		double x = (lin.b - b) / (m - lin.m);	//X holds where they now intersect

		//see if they intersect, but keep in mind that the points may not always be from left to right or top to bottom...
		if (lin.pt1.x > lin.pt2.x)
		{
			if (x <= lin.pt1.x && x >= lin.pt2.x)
			{	//it's within lin, now check this
				if (pt1.x > pt2.x)	//check for which one needs to be tested first
				{
					if (x <= pt1.x && x >= pt2.x)	//within the limits
						return(true);
					return(false);	//it failed
				}
				else
				{
					if (x <= pt2.x && x >= pt1.x)	//within the limits
						return(true);
					return(false);	//failed
				}
			}
			//not within lin
			return(false);	//so fail
		}
		else
		{		//pt2 is larger(or =) pt1
			if (x <= lin.pt2.x && x >= lin.pt1.x)
			{	//it's within lin, now check this
				if (pt1.x > pt2.x)	//check for which one needs to be tested first
				{
					if (x <= pt1.x && x >= pt2.x)	//within the limits
						return(true);
					return(false);	//it failed
				}
				else
				{
					if (x <= pt2.x && x >= pt1.x)	//within the limits
						return(true);
					return(false);	//failed
				}
			}
			return(false);
		}
	}	//end else saying none are vertical lines
	//it should never get this far!
	return(false);	//if it didn't find success, assume failure...
}


bool Line::PointIn(XSpace pt)
{
	//first double check to see if they share a same points. If so, they obviously cross!
	if (pt1 == pt)
		return(true);
	if (pt2 == pt)
		return(true);

	if (vertical)
	{
		if (fabs(pt1.x - pt.x) < EQUIVZERO_POSITION)	//its on the line
		{
			if ((pt.y <= pt1.y && pt.y >= pt2.y)	//and its within the range
				|| (pt.y >= pt1.y && pt.y <= pt2.y))
				return(true);
		}

	}
	else
	{
		if (fabs(m * pt.x + b - pt.y) < EQUIVZERO_POSITION)	//if its on the line
		{
			if ((pt.x <= pt1.x && pt.x >= pt2.x)	//and its within the domain
				|| (pt.x >= pt1.x && pt.x <= pt2.x))
				return(true);	//it must be on the line
		}
	}
	return(false);	//if it didn't find success, assume failure...
}

Line::Line()
{
	m = 0;
	b = 0;
}
Line::Line(XSpace Pta, XSpace Ptb)
{
	pt1 = Pta;
	pt2 = Ptb;
	vertical = false;
	CalcMB();
	CalcAngles();
}

/*
m = deltaY / deltaX

*/
void Line::CalcMB(void)
{
	m = (pt2.x - pt1.x);
	b = (pt2.y - pt1.y);
	if (m == 0)	//avoid divide by zero
	{
		vertical = true;
		m = 0;
		b = 0;
	}
	else
	{
		m = b / m;
		b = pt1.y - m * pt1.x;
	}
	return;
}

XSpace Line::PtIn(double value, int coord)
{
	XSpace point;
	if (coord == PX || coord == NX)
	{
		point.x = value;
		if (!vertical)
			point.y = m * point.x + b;
		//else it's a vertical line with fixed X. y could be anything...
		else
			point.y = pt1.y;	//but just set it to one of the points

	}
	else if (coord == PY || coord == NY)
	{
		point.y = value;
		//y = mx+b; mx = y-b; x = (y-b)/m
		if (m != 0.0 && !vertical)
			point.x = (point.y - b) / m;
		else// if(vertical)
			point.x = pt1.x;
		//else	
		/*
		If m=0, the line is horizontal, x could be anything... so just assign to the first point
		If vertical, it must take on one of the points' x values.
		*/

	}
	return(point);
}
void Line::CalcAngles(void)
{
	theta = PID2;	//for now, everything is 2D on XY, so theta must be PI/2
	double y = pt2.y - pt1.y;
	double x = pt2.x - pt1.x;
	if (x == 0.0 && y == 0)
		phi = -100.0;	//flag that this as bad PHI
	else
	{
		phi = atan2(pt2.y - pt1.y, pt2.x - pt1.x);	//returns [-pi,pi]
		while (phi < 0)	//should only go through once, but be safe.
			phi += 2 * PI;	//normalize everything from 0...2PI
	}

}
double Line::Magnitude(void)
{
	double x = pt2.x - pt1.x;
	double y = pt2.y - pt1.y;
	double z = pt2.z - pt1.z;

	return(pow(x*x + y*y + z*z, 0.5));
}



PosNegSum::PosNegSum()
{

	Clear();
}

PosNegSum::~PosNegSum()
{
	Clear();
}

void PosNegSum::Clear(void)
{
	//	positive.clear();
	//	negative.clear();
	if (positive != NULL)
	{
		for (int i = 0; i<POSNEGDOUBLES; i++)
			positive[i] = 0.0;
	}
	else
	{
		//		positive = new double[POSNEGDOUBLES];
		for (int i = 0; i<POSNEGDOUBLES; i++)
			positive[i] = 0.0;
	}

	if (negative != NULL)
	{
		for (int i = 0; i<POSNEGDOUBLES; i++)
			negative[i] = 0.0;
	}
	else
	{
		//negative = new double[POSNEGDOUBLES];
		for (int i = 0; i<POSNEGDOUBLES; i++)
			negative[i] = 0.0;
	}

	possumcalc = true;	//they're all zero now, so it's all good!
	negsumcalc = true;
	netsumcalc = true;
	possum = negsum = netsum = factor = factorI = 0.0;
}
/*
void PosNegSum::ReduceMemoryFootprint(bool which)	//called every time a number is added to a particular side. Usually just picks a tree, and checks if it is too large or not
{
map<int,double>::iterator it;
map<int,double>* ptrSet = NULL;

if(which==true)
ptrSet = &positive;
else
ptrSet = &negative;

int sz = ptrSet->size();

if(sz >= 200)	//reduce it down to about twenty by just adding up the values by groups of 10 - this should result in 200 entires, 0-199
{
vector<double> tmpHolder;
tmpHolder.reserve(20);
int ctr = 0;
double val = 0.0;
for(it = ptrSet->begin(); it != ptrSet->end(); ++it)
{
val += it->second;
if(ctr == 9)
{
tmpHolder.push_back(val);
val = 0.0;
ctr = 0;
}
else
ctr++;
}
ptrSet->clear();	//clear out all the old values now that done iterating through
for(vector<double>::iterator itV = tmpHolder.begin(); itV != tmpHolder.end(); ++itV)
ptrSet->insert(*itV);	//and add the summed values back in
}
return;
}
*/
void PosNegSum::AddValue(double val)	//this should hopefully be a bit quicker... add similar numbers and move them up if they overstep their bounds.
{
	//note: Do NOT use frexp. This is a very very slow function.
	
	if (val > 0.0)
	{
		if(factor == 0.0)
		{
			factor = val;	//this will be the rough starting position
//			if (factor > 1e240)	//don't be rediculaous...
//				factor = 1e240;
//			else if (factor < 1e-240)
//				factor = 1e-240;
						
			factorI = 1.0 / factor;
		}
		
		val = val * factorI;
		//going to hard code this one... it should be significantly faster - not sure how AddELevelRate is being called so much.
		if (val < PNSUM0)
		{
			positive[0] += val;
			if (positive[0] > PNSUM1)
			{
				positive[1] += positive[0];
				positive[0] = 0.0;
			}
		}
		else if (val < PNSUM1)
		{
			positive[1] += val;
			if (positive[1] > PNSUM2)
			{
				positive[2] += positive[1];
				positive[1] = 0.0;
			}
		}
		else if (val < PNSUM2)
		{
			positive[2] += val;
			if (positive[2] > PNSUM3)
			{
				positive[3] += positive[2];
				positive[2] = 0.0;
			}
		}
		else if (val < PNSUM3)
		{
			positive[3] += val;
			if (positive[3] > PNSUM4)
			{
				positive[4] += positive[3];
				positive[3] = 0.0;
			}
		}
		else if (val < PNSUM4)
		{
			positive[4] += val;
			if (positive[4] > PNSUM5)
			{
				positive[5] += positive[4];
				positive[4] = 0.0;
			}
		}
		else if (val < PNSUM5)
		{
			positive[5] += val;
			if (positive[5] > PNSUM6)
			{
				positive[6] += positive[5];
				positive[5] = 0.0;
			}
		}
		else if (val < PNSUM6)
		{
			positive[6] += val;
			if (positive[6] > PNSUM7)
			{
				positive[7] += positive[6];
				positive[7] = 0.0;
			}
		}
		else if (val < PNSUM7)
		{
			positive[7] += val;
			if (positive[7] > PNSUM8)
			{
				positive[8] += positive[7];
				positive[7] = 0.0;
			}
		}
		else if (val < PNSUM8)
		{
			positive[8] += val;
			if (positive[8] > PNSUM9)
			{
				positive[9] += positive[8];
				positive[8] = 0.0;
			}
		}
		else if (val < PNSUM9)
		{
			positive[9] += val;
			if (positive[9] > PNSUM10)
			{
				positive[10] += positive[9];
				positive[9] = 0.0;
			}
		}
		else if (val < PNSUM10)
		{
			positive[10] += val;
			if (positive[10] > PNSUM11)
			{
				positive[11] += positive[10];
				positive[10] = 0.0;
			}
		}
		else if(val < PNSUM11)	//they're big values
		{
			positive[11] += val;
			if (positive[11] > PNSUM11/* && factor < 1e240*/)
			{
				//time to rescale
				double tmp = positive[11];
				positive[11] = 0.0;
				CalcFactorFromTerm(tmp);	//this function rebuilds the tree with a new factor, and adds in the factored value
			}
		}
		else if (factor < 1e240)
		{
			CalcFactorFromTerm(val);	//it's huge. time to shift things around.
		}


		possumcalc = false;
		netsumcalc = false;

	}
	else if (val < 0.0)
	{
		val = -val;	//make the number positive
		if(factor == 0.0)
		{
			factor = val;	//this will be the rough starting position
//			if (factor > 1e240)	//don't be rediculaous...
//				factor = 1e240;
//			else if (factor < 1e-240)
//				factor = 1e-240;

			factorI = 1.0 / factor;
		}

		val = factorI * val;

		if (val < PNSUM0)
		{
			negative[0] += val;
			if (negative[0] > PNSUM1)
			{
				negative[1] += negative[0];
				negative[0] = 0.0;
			}
		}
		else if (val < PNSUM1)
		{
			negative[1] += val;
			if (negative[1] > PNSUM2)
			{
				negative[2] += negative[1];
				negative[1] = 0.0;
			}
		}
		else if (val < PNSUM2)
		{
			negative[2] += val;
			if (negative[2] > PNSUM3)
			{
				negative[3] += negative[2];
				negative[2] = 0.0;
			}
		}
		else if (val < PNSUM3)
		{
			negative[3] += val;
			if (negative[3] > PNSUM4)
			{
				negative[4] += negative[3];
				negative[3] = 0.0;
			}
		}
		else if (val < PNSUM4)
		{
			negative[4] += val;
			if (negative[4] > PNSUM5)
			{
				negative[5] += negative[4];
				negative[4] = 0.0;
			}
		}
		else if (val < PNSUM5)
		{
			negative[5] += val;
			if (negative[5] > PNSUM6)
			{
				negative[6] += negative[5];
				negative[5] = 0.0;
			}
		}
		else if (val < PNSUM6)
		{
			negative[6] += val;
			if (negative[6] > PNSUM7)
			{
				negative[7] += negative[6];
				negative[6] = 0.0;
			}
		}
		else if (val < PNSUM7)
		{
			negative[7] += val;
			if (negative[7] > PNSUM8)
			{
				negative[8] += negative[7];
				negative[7] = 0.0;
			}
		}
		else if (val < PNSUM8)
		{
			negative[8] += val;
			if (negative[8] > PNSUM9)
			{
				negative[9] += negative[8];
				negative[8] = 0.0;
			}
		}
		else if (val < PNSUM9)
		{
			negative[9] += val;
			if (negative[9] > PNSUM10)
			{
				negative[10] += negative[9];
				negative[9] = 0.0;
			}
		}
		else if (val < PNSUM10)
		{
			negative[10] += val;
			if (negative[10] > PNSUM11)
			{
				negative[11] += negative[10];
				negative[10] = 0.0;
			}
		}
		else if(val < PNSUM11)	//they're big values
		{
			negative[11] += val;
			if (negative[11] > PNSUM11/* && factor<1e240*/)
			{
				//time to rescale
				double tmp = negative[11];
				negative[11] = 0.0;
				CalcFactorFromTerm(-tmp);	//this function rebuilds the tree with a new factor, and adds in the factored value
			}
		}
		else if (factor<1e240)
		{
			CalcFactorFromTerm(-val);	//it's huge. time to shift things around.
		}
		
		negsumcalc = false;
		netsumcalc = false;
	}

	return;
}

//index is where. Val is the value (not adjusted for array being added to), pos is if it should be added to the positive or negative side, force means do it no matter what
bool PosNegSum::AddTermCancel(int index, double val, bool pos, bool force)
{
	if(index < 0 || index >= POSNEGDOUBLES)
		return(false);

	if(force)
	{
		if(pos)
		{
			positive[index] = DoubleAdd(positive[index], val);
			if(positive[index] < 0.0)
			{
				negative[index] = DoubleSubtract(negative[index],positive[index]);	//negative stores the values as positive numbers, and positive was negative, so add to the negative number by subtracting
				positive[index] = 0.0;
			}
		}
		else
		{
			negative[index] = DoubleSubtract(negative[index], val);
			if(negative[index] < 0.0)
			{
				positive[index] = DoubleSubtract(positive[index], negative[index]);
				negative[index] = 0.0;
			}
		}
		return(true);
	}

	if(val <= 0.0)	//cancelling the terms
	{
		positive[index] = DoubleAdd(positive[index], val);
		if(positive[index] < 0.0)
		{
			negative[index] = DoubleSubtract(negative[index],positive[index]);	//negative stores the values as positive numbers, and positive was negative, so add to the negative number by subtracting
			positive[index] = 0.0;
		}
	}
	else
	{
		negative[index] = DoubleSubtract(negative[index], val);
		if(negative[index] < 0.0)
		{
			positive[index] = DoubleSubtract(positive[index], negative[index]);
			negative[index] = 0.0;
		}
		
	}
	return(true);

}


void PosNegSum::AddValueCancel(double val)	//this should hopefully be a bit quicker... add similar numbers and move them up if they overstep their bounds.
{
	//note: Do NOT use frexp. This is a very very slow function.
	
	if (val > 0.0)
	{
		if(factor == 0.0)
		{
			factor = val;	//this will be the rough starting position
			val = 1.0;
			factorI = 1.0 / factor;
		}
		else
			val = val * factorI;
		//going to hard code this one... it should be significantly faster - not sure how AddELevelRate is being called so much.
		if (val < PNSUM0)
		{
			AddTermCancel(0, val);
		}
		else if (val < PNSUM1)
		{
			AddTermCancel(1, val);
		}
		else if (val < PNSUM2)
		{
			AddTermCancel(2, val);
		}
		else if (val < PNSUM3)
		{
			AddTermCancel(3, val);
		}
		else if (val < PNSUM4)
		{
			AddTermCancel(4, val);
		}
		else if (val < PNSUM5)
		{
			AddTermCancel(5, val);
		}
		else if (val < PNSUM6)
		{
			AddTermCancel(6, val);
		}
		else if (val < PNSUM7)
		{
			AddTermCancel(7, val);
		}
		else if (val < PNSUM8)
		{
			AddTermCancel(8, val);
		}
		else if (val < PNSUM9)
		{
			AddTermCancel(9, val);
		}
		else if (val < PNSUM10)
		{
			AddTermCancel(10, val);
		}
		else 	//they're big values
			AddTermCancel(11, val);


		possumcalc = false;
		netsumcalc = false;

	}
	else if (val < 0.0)
	{
		if(factor == 0.0)
		{
			factor = -val;	//this will be the rough starting position
			val = -1.0;
			factorI = 1.0 / factor;
		}
		else
			val = val * factorI;
		

		if (val < PNSUM0)
		{
			AddTermCancel(0, val);
		}
		else if (val < PNSUM1)
		{
			AddTermCancel(1, val);
		}
		else if (val < PNSUM2)
		{
			AddTermCancel(2, val);
		}
		else if (val < PNSUM3)
		{
			AddTermCancel(3, val);
		}
		else if (val < PNSUM4)
		{
			AddTermCancel(4, val);
		}
		else if (val < PNSUM5)
		{
			AddTermCancel(5, val);
		}
		else if (val < PNSUM6)
		{
			AddTermCancel(6, val);
		}
		else if (val < PNSUM7)
		{
			AddTermCancel(7, val);
		}
		else if (val < PNSUM8)
		{
			AddTermCancel(8, val);
		}
		else if (val < PNSUM9)
		{
			AddTermCancel(9, val);
		}
		else if (val < PNSUM10)
		{
			AddTermCancel(10, val);
		}
		else 	//they're big values
			AddTermCancel(11, val);

		negsumcalc = false;
		netsumcalc = false;
	}

	return;
}

void PosNegSum::SubtractValueCancel(double val)
{
	AddValueCancel(-val);
	return;
}

void PosNegSum::SubtractValue(double val)
{
	AddValue(-val);
	return;
}

double PosNegSum::GetPosSum(void)
{
	if (!possumcalc)
	{
		possum = 0.0;
		for (int i = 0; i<POSNEGDOUBLES; ++i)
			possum += positive[i];
		possum = possum * factor;
		possumcalc = true;
		
	}
	return(possum);
}

double PosNegSum::GetNegSum(void)
{
	if (!negsumcalc)
	{
		negsum = 0.0;
		for (int i = 0; i<POSNEGDOUBLES; ++i)
			negsum -= negative[i];
		negsum = negsum * factor;
		negsumcalc = true;
	}
	return(negsum);
}

double PosNegSum::GetNetSumClear(void)
{
	double ret = GetNetSum();
	Clear();	//note, this doesn't delete data
	return(ret);
}

double PosNegSum::GetNetSum(void)
{
	if (netsumcalc)
		return(netsum);
	netsum = 0.0;
	possum = 0.0;
	negsum = 0.0;
	int pos = 0;
	int neg = 0;
	while (pos<POSNEGDOUBLES || neg<POSNEGDOUBLES)
	{
		if (pos < POSNEGDOUBLES && neg<POSNEGDOUBLES)
		{
			//always choose the smaller value
			netsum += DoubleSubtract(positive[pos], negative[neg]);
			possum += positive[pos];
			negsum -= negative[neg];
			++pos;
			++neg;
		}
		else if (pos < POSNEGDOUBLES)	//then the positive iterator is OK and the negative iterator is not OK
		{
			netsum += positive[pos];
			possum += positive[pos];
			++pos;
		}
		else if (neg < POSNEGDOUBLES)	//the negative iterator is OK, but the positive one isn't
		{
			netsum -= negative[neg];
			negsum -= negative[neg];
			++neg;
		}
		//else they're both bad and the while loop will end
	}
	possum = possum * factor;
	negsum = negsum * factor;
	netsum = netsum * factor;
	netsumcalc = negsumcalc = possumcalc = true;
	return(netsum);

	/*
	double net = 0.0;
	vector<double> PosVals;	//which ones should be removed
	vector<double> NegVals;	//which ones should be removed
	vector<double> NetVals;	//sums of those above removed that need to be added together
	NetVals.reserve(max(positive.size(), negative.size()));
	NegVals.reserve(negative.size());
	PosVals.reserve(positive.size());

	double firstGreaterEq, firstLessEq, ratioGreater, ratioLess;
	//	Tree<bool, double>* tmpPtr;

	multiset<double>::iterator tmpPtr;

	//positive.GenerateSortS(PosVals);
	//note, none of these values can ever be zero!
	for(multiset<double>::iterator itPos = positive.begin(); itPos != positive.end(); ++itPos)
	{
	//first try and find match
	firstGreaterEq = -1.0;
	firstLessEq = -1.0;
	tmpPtr = MSetFindFirstGreaterEqual(negative, *itPos);
	if(tmpPtr != negative.end())
	firstGreaterEq = *tmpPtr;

	tmpPtr = MSetFindFirstLessEqual(negative, *itPos);
	if(tmpPtr != negative.end())
	firstLessEq = *tmpPtr;

	ratioGreater = *itPos / firstGreaterEq;		//must be > 0.9 to cancel. If not assigned, negative and auto fail.
	ratioLess = firstLessEq / *itPos;			//must be > 0.9 to cancel. If not assigned, negative and auto fail.

	if(ratioLess > 0.9)	//note, ratioLess cannot exceed 1.0. default to get rid of the smaller number of the two first
	{
	ratioLess = 1.0 - ratioLess;	//choose the smaller of the two values
	ratioGreater = 1.0 - ratioGreater; //if this is bad, it will certainly be larger than ratioGreater

	if(ratioLess <= ratioGreater)
	{
	net = DoubleSubtract(*itPos, firstLessEq);	//should result in a positive value, with the net matching sign
	NetVals.push_back(net);
	PosVals.push_back(*itPos);
	NegVals.push_back(firstLessEq);

	//				positive.Remove(PosVals[i], true);
	//				negative.Remove(firstLessEq, true);
	//				if(net > 0.0)
	//					positive.Insert(false, net);
	//				else if(net < 0.0)	//this should never occur, but just be safe
	//					negative.Insert(false, -net);
	}
	else
	{
	net = DoubleSubtract(firstGreaterEq, *itPos);	//should result in a positive value, with the net being opposite sign
	NetVals.push_back(-net);
	PosVals.push_back(*itPos);
	NegVals.push_back(firstGreaterEq);

	//				positive.Remove(PosVals[i], true);
	//				negative.Remove(firstGreaterEq, true);
	//				if(net > 0.0)
	//					negative.Insert(false, net);
	//				else if(net < 0.0)	//this should never occur, but just be safe
	//					positive.Insert(false, -net);
	}
	}
	else if(ratioGreater > 0.9)	//cannot exceed 1.0 because the greater number is on bottom
	{
	ratioGreater = 1.0 - ratioGreater;
	ratioLess = 1.0 - ratioLess;

	if(ratioGreater < ratioLess)
	{
	net = DoubleSubtract(firstGreaterEq, *itPos);	//should result in a positive value, with the net being opposite sign
	NetVals.push_back(-net);
	PosVals.push_back(*itPos);
	NegVals.push_back(firstGreaterEq);

	//				positive.Remove(PosVals[i], true);
	//				negative.Remove(firstGreaterEq, true);
	//				if(net > 0.0)
	//					negative.Insert(false, net);
	//				else if(net < 0.0)	//this should never occur, but just be safe
	//					positive.Insert(false, -net);
	}
	else
	{
	net = DoubleSubtract(*itPos, firstLessEq);	//should result in a positive value, with the net matching sign
	NetVals.push_back(net);
	PosVals.push_back(*itPos);
	NegVals.push_back(firstLessEq);

	//				positive.Remove(PosVals[i], true);
	//				negative.Remove(firstLessEq, true);
	//				if(net > 0.0)
	//					positive.Insert(false, net);
	//				else if(net < 0.0)	//this should never occur, but just be safe
	//					negative.Insert(false, -net);
	}
	}
	}

	//now we should go through and remove all the values BEFORE adding in the net values
	for(vector<double>::iterator itVect = PosVals.begin(); itVect != PosVals.end(); ++itVect)
	{
	tmpPtr = positive.find(*itVect);
	if(tmpPtr != positive.end())	//should always be true, but be safe just in case
	positive.erase(tmpPtr);	//only erase 1. If multiple entries with same key, they'd all be erased
	}
	PosVals.clear();
	for(vector<double>::iterator itVect = NegVals.begin(); itVect != NegVals.end(); ++itVect)
	{
	tmpPtr = negative.find(*itVect);
	if(tmpPtr != negative.end())	//should always be true, but be safe just in case
	negative.erase(tmpPtr);	//only erase 1. If multiple entries with same key, they'd all be erased
	}
	NegVals.clear();
	//and insert the sums into the tree appropriately
	for(vector<double>::iterator itVect = NetVals.begin(); itVect != NetVals.end(); ++itVect)
	AddValue(*itVect);	//put the sums of the erased values back in the trees. Being that values were erased, this should not cause a sum to occur

	NetVals.clear();	//clear out the vector

	//this should have cancelled out everything with similar values
	multiset<double>::iterator positer = positive.begin();
	multiset<double>::iterator negiter = negative.begin();

	netsum = 0.0;
	while(positer != positive.end() && negiter != negative.end())
	{
	if(positer == positive.end())	//the positive one is all done, so only negative values are left (would not be in loop otherwise)
	{
	netsum = DoubleSubtract(netsum,*negiter);
	negiter++;
	}
	else if(negiter == negative.end())	//negative values are done, so only positive values are left
	{
	netsum = DoubleAdd(netsum, *positer);
	positer++;
	}
	else if(DoubleSubtract(*positer, *negiter) == 0.0)
	{
	++positer;	//just move on to the next value
	++negiter;
	}
	else if(netsum > 0.0)	//it is currently positive
	{
	ratioLess = netsum / *negiter;	//less is negative
	if(ratioLess < 1.1 && ratioLess > 0.9)
	{
	netsum = DoubleSubtract(netsum, *negiter);
	negiter++;
	}
	else if(*negiter < *positer)	//add the smaller of the two value
	{
	netsum = DoubleSubtract(netsum, *negiter);
	negiter++;
	}
	else
	{
	netsum += *positer;
	positer++;
	}
	}
	else if(netsum < 0.0)
	{
	ratioGreater = -netsum / *positer;
	if(ratioGreater < 1.1 && ratioGreater > 0.9)	//it will cancel out net
	{
	netsum = DoubleAdd(netsum, *positer);
	positer++;
	}
	else if(*positer < *negiter)	//choose the smaller of the two values
	{
	netsum = DoubleAdd(netsum, *positer);
	positer++;
	}
	else
	{
	netsum = DoubleSubtract(netsum, *negiter);
	negiter++;
	}
	}
	else //netsum = 0
	{
	if(*positer < *negiter)	//choose the smaller of the two values
	{
	netsum = *positer;
	positer++;
	}
	else
	{
	netsum = *negiter;
	negiter++;
	}
	}
	}	//end while loop, which is a glorified for loop over two vectors
	netsumcalc = true;

	return(netsum);
	*/
}

bool PosNegSum::CalcFactorFromTerm(double newTermNormalized)
{

	if(fabs(newTermNormalized) < PNSUM11 && fabs(newTermNormalized) >= PNSUM0 / PNSUMALTER)
		return(false);

	int curMin = POSNEGDOUBLES -1;
	int curMax = 0;
	double origValues[POSNEGDOUBLES*2];
	for(unsigned int i=0; i<POSNEGDOUBLES; ++i)
	{
		if(positive[i] != 0.0 || negative[i] != 0.0)	//find the highest index present (largest number)
			curMax = i;

		if(positive[POSNEGDOUBLES-1-i] != 0.0 || negative[POSNEGDOUBLES-1-i] != 0.0)	//find the lowest index present (smallest number)
			curMin = i;

		origValues[i] = positive[i] * factor;	//get absolute values of all the different numbers!
		origValues[i+POSNEGDOUBLES] = negative[i] * factor;
	}
	double origTerm = newTermNormalized * factor;
	for(unsigned int i=0; i<POSNEGDOUBLES; i++)
		positive[i] = negative[i] = 0.0;
	if(fabs(newTermNormalized) >= PNSUM11)
	{
		//then this is adding a really big number (comparatively)
		//we want to adjust this
		//big numbers matter more than small numbers. Anything really small is just cut out
		factor = origTerm / PNSUM10;	//make this term be at the very top!
//		if (factor > 1e240)	//don't be rediculaous...
//			factor = 1e240;

		factorI = 1.0 / factor;

		for(unsigned int i=0; i<POSNEGDOUBLES; i++)
		{
			AddValue(origValues[i]);
			SubtractValue(origValues[i+POSNEGDOUBLES]);
		}
		AddValue(origTerm);
	}
	else if(fabs(newTermNormalized) < PNSUM0 / PNSUMALTER)
	{
		//this is a really small number, and the factor should be reduced if possible. Large numbers will prevent this from occurring.
		if(curMax == POSNEGDOUBLES-1)
			return(false);	//it can't get any better. It would cut out important digits

		int range = 1 + curMax - curMin;	//how many array values does this sweep?
		int maxAdjust = POSNEGDOUBLES - curMax;	//how many indices can be shifted without data loss?
		double tmp = PNSUM0 / fabs(newTermNormalized);	//this should be greater than 1e10
		int adjust = 0;	//number of indices to shift
		double pows = 1.0;
		do
		{
			adjust++;
			pows = pows * PNSUMALTER;
		}while(pows < tmp);

		if(adjust > maxAdjust)
			return(false);	//this is useless. Even shifting it all the way will not help

		factor = factor / pows;	//by adding a small number, we shifted what was stored in say 1 to be stored in 1e20. Therefore, the factor needs to be divided by 1e20.
//		if (factor < 1e-240)
//			factor = 1e-240;

		factorI = 1.0 / factor;

		//because we simply shifted the factor by the constant difference in range, only the values need to be shifted in indices.
		for(int i=POSNEGDOUBLES-1; i>=0; --i)
		{
			if(i>=adjust)
			{
				positive[i] = positive[i-adjust];
				negative[i] = negative[i-adjust];
			}
			else
				positive[i] = negative[i] = 0.0;
		}

		AddValue(origTerm);	//must occur after factor is done

	}

	return(true);
}

PosNegSum& PosNegSum::operator+=(const PosNegSum &b)
{
	if(factor == b.factor)
	{
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			positive[i] += b.positive[i];
			negative[i] += b.negative[i];
		}
	}
	else
	{
		//damn it. Do it the long way
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			AddValue(b.positive[i] * b.factor);
			SubtractValue(b.negative[i] * b.factor);
		}

	}
	possumcalc = negsumcalc = netsumcalc = false;
	return(*this);
}

PosNegSum& PosNegSum::operator-=(const PosNegSum &b)
{
	if(factor == b.factor)
	{
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			positive[i] = DoubleSubtract(positive[i], b.positive[i]);	//want terms of same sign to cancel
			negative[i] = DoubleSubtract(negative[i], b.negative[i]);
			if(positive[i] < 0.0)
			{
				negative[i] -= positive[i];
				positive[i] = 0.0;
			}
			if(negative[i] < 0.0)
			{
				positive[i] -= negative[i];
				negative[i] = 0.0;
			}
		}
	}
	else
	{
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			AddValueCancel(b.negative[i] * b.factor);
			SubtractValueCancel(b.positive[i] * b.factor);
		}
	}
	possumcalc = negsumcalc = netsumcalc = false;
	return(*this);
}

PosNegSum PosNegSum::operator-(const PosNegSum &b)
{
	PosNegSum tmp;
	tmp.Clear();
	if(factor == b.factor)
	{
		tmp.factor = factor;
		tmp.factorI = factorI;
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			tmp.positive[i] = DoubleSubtract(positive[i], b.positive[i]);	//want terms of same sign to cancel
			tmp.negative[i] = DoubleSubtract(negative[i], b.negative[i]);
			if(tmp.positive[i] < 0.0)
			{
				tmp.negative[i] -= tmp.positive[i];
				tmp.positive[i] = 0.0;
			}
			if(tmp.negative[i] < 0.0)
			{
				tmp.positive[i] -= tmp.negative[i];
				tmp.negative[i] = 0.0;
			}
		}
	}
	else
	{
		tmp = *this;
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			tmp.AddValueCancel(b.negative[i] * b.factor);
			tmp.SubtractValueCancel(b.positive[i] * b.factor);
		}
	}
	tmp.possumcalc = tmp.negsumcalc = tmp.netsumcalc = false;

	return(tmp);
}

PosNegSum PosNegSum::operator-()
{
	PosNegSum tmp;
	for(int i=0; i<POSNEGDOUBLES; ++i)
	{
		tmp.positive[i] = negative[i];
		tmp.negative[i] = positive[i];
	}
	tmp.possumcalc = negsumcalc;
	tmp.negsumcalc = possumcalc;
	tmp.netsumcalc = netsumcalc;
	tmp.possum = negsum;
	tmp.negsum = possum;
	tmp.netsum = -netsum;
	tmp.factor = factor;
	tmp.factorI = factorI;
	
	return(tmp);
}

PosNegSum PosNegSum::operator+(const PosNegSum &b)
{
	PosNegSum tmp;
	tmp.Clear();
	if(factor == b.factor)
	{
		tmp.factor = factor;
		tmp.factorI = factorI;
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			tmp.positive[i] = positive[i]+b.positive[i];	//want terms of same sign to cancel
			tmp.negative[i] = negative[i]+b.negative[i];
		}
	}
	else
	{
		tmp = *this;
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			tmp.AddValueCancel(b.positive[i] * b.factor);
			tmp.SubtractValueCancel(b.negative[i] * b.factor);
		}
	}
	tmp.possumcalc = tmp.negsumcalc = tmp.netsumcalc = false;

	return(tmp);
}


PosNegSum& PosNegSum::operator=(const PosNegSum &b)
{
	for(int i=0; i<POSNEGDOUBLES; ++i)
	{
		positive[i] = b.positive[i];	//want terms of same sign to cancel
		negative[i] = b.negative[i];
	}
	possum = b.possum;
	negsum = b.negsum;
	netsum = b.netsum;
	possumcalc = b.possumcalc;
	negsumcalc = b.negsumcalc;
	netsumcalc = b.netsumcalc;
	factor = b.factor;
	factorI = b.factorI;

	return(*this);
}

bool PosNegSum::operator==(const PosNegSum &b)
{
	if(factor == b.factor)
	{
		for(int i=0; i<POSNEGDOUBLES; ++i)	//const requirement means I can't just call the net sum, but must check each individually
		{
			if(positive[i] != b.positive[i] || negative[i] != b.negative[i])
				return(false);
		}
	}
	else
	{
		double sumA = 0.0;
		double sumB = 0.0;
		for(int i=0; i<POSNEGDOUBLES; ++i)
		{
			sumA += (positive[i] - negative[i]);
			sumB += (b.positive[i] - b.negative[i]);
		}
		sumA = sumA * factor;
		sumB = sumB * b.factor;
		return(sumA == sumB);
	}
	return(true);
}


void VectorizedPFunc::Clear(void)
{
	Probabilities.clear();
	Indices.clear();
	NumberLine.clear();
	totProb = totProbi = 0.0;
	Normalized = ReadyforRandom = false;
	largestProb = 0.0;
	return;
}

VectorizedPFunc::VectorizedPFunc()
{
	Clear();
}

VectorizedPFunc::~VectorizedPFunc()
{
	Clear();
}

double VectorizedPFunc::ExtractProb(long int index)
{
	if (index < 0 || index >= long int(Probabilities.size()))
		return(0.0);	//this index does not exist yet

	Normalize();

	return(Probabilities[index].Normalized);
}

double VectorizedPFunc::ExtractRaw(long int index)
{
	if (index < 0 || index >= long int(Probabilities.size()))
		return(0.0);	//this index does not exist yet

	return(Probabilities[index].RawProb);
}

bool VectorizedPFunc::Choose(unsigned int& RetIndex)
{
	RetIndex = 0;
	SetNumberLine();	//assigns indices and fills in numberline if needs to be done

	if (Indices.size() == 0)	//there were no valid probabilities
		return(false);

	double random = SciRandom.Uniform01();	//chooses a random number from 0 to 1

	/*unsigned int index = 0;
	for (RetIndex = 0; RetIndex < Indices.size(); ++RetIndex)
	{
		if (NumberLine[RetIndex] >= random) // && NumberLine[RetIndex-1] < random
			break;
	}
	*/
	RetIndex = Indices.size() / 2;	//search quicker
	unsigned int adjust = RetIndex/2;
	do
	{
		if (adjust == 0)
			adjust = 1;

		if (NumberLine[RetIndex] >= random) //it might be too high
		{
			if (RetIndex == 0)
				break;
			if (NumberLine[RetIndex - 1] < random)	//found it
				break;

			RetIndex -= adjust;
		}
		else
			RetIndex += adjust;

		if (RetIndex >= Indices.size())	//FAIL
			return(false);
		adjust = adjust / 2;
	} while (1);

	RetIndex = Indices[RetIndex];
	if (Probabilities[RetIndex].Normalized == 0)
		return(false);

	return(true);
}

void VectorizedPFunc::Normalize(void)
{
	if (Normalized == true)
		return;

	largestProb = 0.0;
	totProb = 0.0;
	for (unsigned int i = 0; i < Probabilities.size(); ++i)
	{
		if (Probabilities[i].RawProb > 0.0)
			totProb += Probabilities[i].RawProb;
		
	}
	totProbi = totProb>0.0 ? 1.0 / totProb : 0.0;

	for (unsigned int i = 0; i < Probabilities.size(); ++i)
	{
		Probabilities[i].Normalized = Probabilities[i].RawProb * totProbi;
		if (Probabilities[i].Normalized > largestProb)
			largestProb = Probabilities[i].Normalized;
	}

	Normalized = true;
	return;
}

double VectorizedPFunc::GetTotalProb(void)
{
	Normalize();	//this assigns totProb if it hasn't been done yet
	return(totProb);
}

void VectorizedPFunc::SetNumberLine(void)
{
	Normalize();
	if (ReadyforRandom)
		return;

	Indices.clear();	//that's the first problem. It was getting WAY too large
	Indices.reserve(Probabilities.size());
	double ProbCutOff = largestProb * 1.0e-20;	//this doesn't  affect the numbers. If it's 20 orders of mag less probable than the max, it's going
	for (unsigned int i = 0; i < Probabilities.size(); ++i) //to have at best a <1e-20 chance, and the RNG only goes to like 1e-10.
	{
		if (Probabilities[i].Normalized > ProbCutOff)
			Indices.push_back(i);
	}
	//now need to sort the indices sothe values are in order from smallest to largest
	unsigned int indSize = Indices.size();

	if (indSize == 0)
	{
		NumberLine.clear();
		return;
	}
	else if (indSize == 1)
	{
		NumberLine.clear();
		NumberLine.push_back(1.0);
	}

	//find the smallest one and put it in the first. Then ignore that one and find the next smallest
	//of those remaining
	bool swaps = false;
	unsigned int timesthrough = 0;
	do
	{
		//test forward. Basically guarantees largest one ends up in furthest right index
		swaps = false;
		for (unsigned int i = timesthrough; i < indSize - 1 - timesthrough; ++i)
		{
			if (Probabilities[Indices[i]].Normalized > Probabilities[Indices[i + 1]].Normalized)
			{
				//swap 'em
				swaps = true;
				unsigned int tmp = Indices[i];
				Indices[i] = Indices[i + 1];
				Indices[i + 1] = tmp;
			}
		}
		if (!swaps)
			break;
		//guarantes smallest one ends up in furthest left index. -2 instead of -1 because furthest right is guaranteed correct
		for (unsigned int i = indSize - 2 - timesthrough; i > timesthrough && i<indSize; --i)	//now test in reverse direction
		{
			if (Probabilities[Indices[i]].Normalized < Probabilities[Indices[i - 1]].Normalized)
			{
				//swap 'em
				swaps = true;
				unsigned int tmp = Indices[i];
				Indices[i] = Indices[i - 1];
				Indices[i - 1] = tmp;
			}
		}

		timesthrough++;	//the end points are guaranteed to be correct, so don't iterate through them all
	} while (swaps);

	//now that the indices are sorted, time to put them on a number line.
	double curProb = 0.0;
	NumberLine.resize(Indices.size());
	for (unsigned int i = 0; i < indSize; ++i)
	{
		curProb += Probabilities[Indices[i]].Normalized;
		NumberLine[i] = curProb;
	}
	NumberLine.back() = 1.0;
	ReadyforRandom = true;
	return;
}

double VectorizedPFunc::AddProbability(double weight, long int index)
{
	if (weight == 0.0)
	{
		if (index < 0)
		{
			ProbPair tmp;
			tmp.RawProb = 0.0;
			tmp.Normalized = 0.0;
			Probabilities.push_back(tmp);
			return(0.0);
		}
		else if (index >= long int(Probabilities.size()))
		{
			ProbPair tmp;
			tmp.RawProb = 0.0;
			tmp.Normalized = 0.0;
			Probabilities.resize(index + 1, tmp);
			return(0.0);
		}
		else
			return(Probabilities[index].RawProb);
	}
	if (index < 0)	//new index to add onto the end
	{
		ProbPair tmp;
		tmp.RawProb = weight;
		tmp.Normalized = 0.0;
		Probabilities.push_back(tmp);
		if (weight > 0.0)
		{
			Normalized = false;
			ReadyforRandom = false;
		}
		return(weight);
	}
	else if (index >= long int(Probabilities.size()))
	{
		ProbPair tmp;
		tmp.RawProb = 0.0;
		tmp.Normalized = 0.0;
		Probabilities.resize(index + 1, tmp);
		Probabilities[index].RawProb = weight;
		if (weight > 0.0)
		{
			Normalized = false;
			ReadyforRandom = false;
		}
		return(weight);
	}
	else
	{
		bool update = Probabilities[index].RawProb > 0.0 ? true : false;
		Probabilities[index].RawProb += weight;
		if (Probabilities[index].RawProb > 0.0)
			update = true;
		if (update)
		{
			Normalized = ReadyforRandom = false;
		}
		return(Probabilities[index].RawProb);
	}
	return(weight);	//should never hit this, but just in case...?
}

int VectorizedPFunc::SetSize(unsigned int sz)
{
	ProbPair tmp;
	tmp.RawProb = tmp.Normalized = 0.0;	//make sure any entries added are set to zero
	Probabilities.resize(sz, tmp);	
	Indices.clear();
	NumberLine.clear();
	Indices.reserve(sz);
	NumberLine.reserve(sz);
	Normalized = ReadyforRandom = false;
	return(Probabilities.size());
}

int VectorizedPFunc::SetProbability(double weight, long int index)
{
	if (index < 0)	//new index to add onto the end
	{
		ProbPair tmp;
		tmp.RawProb = weight;
		tmp.Normalized = 0.0;
		Probabilities.push_back(tmp);
		if (weight > 0.0)
		{
			Normalized = false;
			ReadyforRandom = false;
		}
		return(0);
	}
	else if (index >= long int(Probabilities.size()))
	{
		ProbPair tmp;
		tmp.RawProb = 0.0;
		tmp.Normalized = 0.0;
		Probabilities.resize(index + 1, tmp);
		Probabilities[index].RawProb = weight;
		if (weight > 0.0)
		{
			Normalized = false;
			ReadyforRandom = false;
		}
		return(0);
	}
	else
	{
		if (weight != Probabilities[index].RawProb && (weight > 0.0 || Probabilities[index].RawProb > 0.0))
		{  //must change if different, but ignore if both are <= 0, because all those weights are considered 0.0
			Normalized = false;
			ReadyforRandom = false;
		}
		Probabilities[index].RawProb = weight;
		return(0);
	}
	return(0);
}
