#ifndef EQSOLVE_H
#define EQSOLVE_H

#include <vector>
#include <fstream>
#include <map>

#define UNDEF 0		//just a default state for the equation
#define UNSOLVED 1	//the system hasn't been solved yet
#define SOLVED 2	//every single equation has been reduced to one LHS coef of one
#define SINGULAR 4	//more variables than independent equations
#define CONFLICT 8	//more independent equations than variables, and they don't match
#define BADSYSTEM 16	//a bad system for some reason or another
#define POSX 0
#define NEGX 1
#define POSY 2
#define NEGY 3
#define POSZ 4
#define NEGZ 5
#define NO -1
#define DUBZERO 1e-13	//a double is good to about 14 digits (1.x(13x) E(+/-)##)
#define EQ_SOLVER_LHS true
#define EQ_SOLVER_RHS false
//therefore, adding values which are the "same" may not necessarily cancel out from values less than 14.
//this will cause additive calculations to be off by ~10^-15. Use this comparation to check if a value is zero, because
//the computer would not be able to check beyond this basis.

/*
class Var
{
public:
	vector<unsigned int> values;	//a vector of this class. Used to find equations.
	Var(void);	//constructor
};

*/

class kernelThread;

class EqVar
{
public:
	unsigned long int var;		//the correlating variable (or position in massive matrix from L->R)
	unsigned long int eq;	//what the coefficient actually is.
	EqVar(void);	//constructor
};


class Eq
{
public:
	std::map<unsigned long int, double> LHS;	//store all the left hand side info of the equation
	std::map<unsigned long int, double> RHS;	//store all the RHS terms
	bool Computed;	//A check to see if this equation has been used to eliminate variables.
	int Normalize(double norm);	//multiply the LHS and RHY by a constant value
	void clear(void);
	int DisplayEquation(std::fstream& file);
	Eq(void);	//constructor -- If it has, it shouldn't be modified - like leaving upper triangle
	~Eq(void);
};				//open in a matrix so then it can later be back-substituted.

class SystemEqReturns
{
public:
	unsigned long int which;
	double data;
};

class SystemEq
{
private:
	unsigned int flag;
	std::map<unsigned long int, Eq> Equation;	//first is the equation number, second is the equation...
	std::map<unsigned long int, std::map<unsigned long int, unsigned long int> > EquationsWithVariable;	//minimize search for equations containing the chosen variable.
	//now, first is the equation and second is just repeating the variable that it is. -- the first first is the appropriate variable -
	//note: EqWVar[variable].sort points to the equation - these all differ and want to sort by these
	//EqWVar[variable].data = variable  - this is just so the variable being found in the equations is also passed into the function

	int UncatalogEquation(std::map<unsigned long int, double>& lhs, unsigned int eq);
	int CatalogEquation(std::map<unsigned long int, double>& lhs, unsigned long int eq);
	int NormalizeEq(Eq& eq, unsigned long int var);
	int BackSubstitute(unsigned long int variable);
	int SelectGoodEq(std::map<unsigned long int, unsigned long int>& EqwVar, std::map<unsigned long int, unsigned long int>& Choices);
	int AddLorRHS(std::map<unsigned long int, double>& sideEq, double multiplier, std::map<unsigned long int, double>& ModifyEq);
	int AddEqWVar(std::map<unsigned long int, unsigned long int>& EqWVar, Eq& eq);
	std::map<unsigned long int, Eq>::iterator ChooseEquation(std::map<unsigned long int, unsigned long int>& EQwVar);
	unsigned long int CheckSystem(void);
public:
	SystemEq(void);						//first entry will be equation. 2nd entry will be variable
	~SystemEq(void);
	int SolveSystem(void);
	int AddTermEquation(unsigned long int eq, unsigned long int term, double value, bool side);
	int CreateClearEquation(unsigned long int eq);	//
	int RemoveEquation(unsigned long int eq);
	int RemoveTermEquation(unsigned long int eq, unsigned long int term, bool side);
	int TraceEq(void);
	int clear(void);
	int GetReturns(std::map<unsigned long int, std::vector<SystemEqReturns>>& ret);
};

class IntPair
{
public:
	long int start;
	long int stop;
	IntPair();
	IntPair(long int beg, long int end);
};
class ProcessedEquations
{
private:
	std::vector<IntPair> Available;
	std::vector<long int> EqProcessOrder;
	std::vector<long int> ZeroEffect;	//these variables have NO effect on the simulation whatsoever.
	unsigned int whichAvail;
	long int curAvail;
	unsigned int EqLoop;
public:
	int AddEqProcess(long int eq);
	void Initialze(long int startEq, long int stopEq);
	void Initialze(long int numEq);
	bool isAvail(long int eq);
	long int GetStartEq(void);
	long int GetNextEq(void);
	long int GetLastProccessedEq(void);
	long int StartLoopEqPOrder(void);
	long int LoopEqPOrder(void);
	int AddZeroEffect(long int var);
	bool hasZeroEffect(long int var);
	int ZeroOut(double rhs[]);
	ProcessedEquations();
	ProcessedEquations(long int startEq, long int stopEq);
	ProcessedEquations(long int numEq);
	~ProcessedEquations();
};

bool SolveDenseOneTime(kernelThread* kThread, double matrix[], double rhs[], long int sz);


#endif