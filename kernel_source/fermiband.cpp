#include <cmath>
#include "fermiband.h"
#include "model.h"
#include "physics.h"
#include "outputdebug.h"
/*
this file is used to find the absolute value of Ef relative to the valence band.
Because the bands and occupancy are discretized, the typical n=ni*exp((Ef-Ei)/kT) is
not going to work by a long shot. 10 carriers at the bottom of the band should have a
lower fermi energy than 10 carriers at the top of the band, for example.

Therefore, the least squares method is used to obtain Ef, namely minimizing the function
S = SUM[ (f(E)-occ/tot)^2 ]

therefore: solving
dS/dEf = 0 for Ef

An Ef is assumed, the dS/dEf is calculated.
if dS/dEf < 0, it is set to Ef_Min
if dS/dEf > 0, it is set to Ef_Max
Ef_New = (Ef_Min + Ef_Max)/2.0
Can NOT use newton's method as the function looks something similar to:

|  ___
| /   \
|/_____|__________________________
|      |		  ____/
|	    \      __/
|		 \____/

where X is exp(beta*(E-Ef)), which is always positive. Note: exponential decay approaching 0 as Ef-->infinity
As Ef-->-infinity, dS/dEf goes to 0, but Ef can never equal negative infinity.

The complete function is:

note: ExpFact = exp(Beta*(E-Ef))
where: Beta = 1/(kT)
Degen is the degeneracy of states from the fermi function.
Typically 1 for the bands, 0.5 for donors, and 4.0 for acceptors.

f(E) = 1/(1 + degen * ExpFact)

		   ____	
 dS		   \	   / Occupied States \		 /	Beta * Degen * ExpFact	\		 /	Beta * degen * ExpFact	\
----- = 2   >	- |  ---------------  |	 *	|	-----------------------	 |	+	|	-----------------------	 |
 dEf	   /___	   \ (Total States)	 /		 \	(1 + Degen * ExpFact)^2	/		 \	(1 + Degen * ExpFact)^3	/
		  All Ei

This came from the equation

S = Sum[ (f(E) - occ/tot)^2 ]

This method works for any band structure as it is a "best fit" value to the present data, and accounts for
variations within the band.

Some changes... want to weight the difference of the above by the number of states
t_i = total states at energy level i
s_i = occupied states at energy level i

 dE
-----	= 2(beta) * SUM_over_i{ t_i^2 * f(E_i)^3 * Degen_i * Expfact - t_i * s_i * f(E_i) }
 dEf

 wanting to minimize, so the 2beta really does nothing here. ignore it as it's just an overall constant and will not
 change the sign of anything and adjusts all magnitudes proportionally the same.


****************
Note, post modification. Each needs to be weighed by the total number of states in the summation, so multiply by
the total number of states.  Also, reverse the signs so if dS/dEf is >0, then Ef needs to increase. Also note, we are setting
dS/dEf to 0. We ONLY CARE if it is positive or negative. Both sides have Beta * Degen. The betas are redundant and will always
contribute the same amount in each sum. the degen will vary with each state.
*/

void FindFermiPt(Point& pt, double accuracy)
{
	if(pt.MatProps->type == VACUUM)
	{
		pt.fermi = pt.Psi;	//which should be psi anyway... this is a vacuum
		return;
	}
	//first, capture all the occupied states
	unsigned int size = pt.cb.energies.size() + pt.vb.energies.size() + pt.CBTails.size() + pt.VBTails.size() + pt.acceptorlike.size() + pt.donorlike.size();
	double EfMin = pt.Psi;		//Set the ranges at the very extreme, so load data can keep pushing it down
	double EfMax = pt.vb.bandmin - pt.MatProps->Phi;	//and the minimum Ef to be at the bottom of the valence band
	
	double expfact = 0.0;	//the exponent used everywhere in this sum: exp(Beta*(Ei-Ef))
	double Beta = 0.0;
	if(pt.temp != 0.0)
		Beta = 1.0/KB/pt.temp;
	double dSdEf = 0.0;	//the value of interest to calculate
	double *Energies = new double[size];
	float *degen = new float[size];
	double *occupancy = new double[size];
	double *totalstates = new double[size];
	bool *CarrierType = new bool[size];
//	double sum1 = 0.0;
	double sum2 = 0.0;
	double outfactor = 0.0;
	double dif;

	if(pt.MatProps->Eg < 1.0 && pt.MatProps->Eg > 0.0)
	{
		accuracy = accuracy * pt.MatProps->Eg * pt.MatProps->Eg;
	}
	else if(pt.MatProps->Eg <= 0.0 && accuracy > 1.0e-10)
		accuracy = 1.0e-10;	//make it REALLY accurate as charge calculations will be very off for a slight numerical error in Ef

	//first populate data that does not change: the three arrays above. Also get a range for what the fermi level could be
	
	unsigned int i = LoadData(degen, occupancy, totalstates, Energies, CarrierType, EfMax, EfMin, pt, FERMI_LOAD_CB | FERMI_LOAD_VB | FERMI_LOAD_DONOR | FERMI_LOAD_ACCEPTOR | FERMI_LOAD_CBTAILS | FERMI_LOAD_VBTAILS);
	//i will be the counter on the next entry to be loaded. This SHOULD be size

	if(EfMin == pt.Psi)	//very unlikely this will happen...
		EfMin = pt.vb.bandmin - pt.MatProps->Phi;
	if(EfMax == pt.vb.bandmin - pt.MatProps->Phi)
		EfMax = pt.Psi;

	double EfGuess = (EfMax + EfMin)*0.5;	//initial guess to start the ball rolling
	double OldEfGuess = EfMax + 1.0;	//ensure that the old EFGuess is different for the first iteration.

	do
	{
		dSdEf = 0.0;	//initialize the sum
		EfGuess = (EfMax + EfMin)*0.5;	//cut in by half...
	//	sum1 = 0.0;
		sum2 = 0.0;

		for(i = 0; i < size; i++)
		{
			if(pt.temp == 0.0)
			{
				/*
				df/dEf = expfact/(1+expfact)^2, or ~ 1/expfact
				if Ei>Ef, this is inf/(inf^2*0), or 0 as the infs are stronger functions
				if Ei<Ef, this is 0/(0+1)^2, or the total = 0
				Same can be said for the second term, except its expfact/(1+expfact)^3
				setting to 0 will cause the same output in the sum, 0...
				The total sum is then -(fraction)/inf + 1/inf^2

				However, at T=0, df/dEf is always 0, with a discontinuity at Ef.
				A new method is needed...
				based on the previousmethodl df/dEf is the same for all values, so make it not matter by "setting
				it to one."  therefore, this is equivalent to a least (singles?) method, in which
				the fermi function is simply compared to the occupied states, and the best case scenario
				is roughly equal errors on both sides. Theoretically, at 0K, everything is in the ground state anyway,
				so if Ef is too low, the sum will be negative from the -occ/tot, and if Ef is too high, there will be
				extra 1's added that don't get cancelled out by the states, pushing Ef down.
				*/
				if(CarrierType[i] == CONDUCTION)
				{
					if(EfGuess > Energies[i])
						sum2 = totalstates[i];	//here, sum 1 is the fermi energy
					else if(EfGuess < Energies[i])
						sum2 = 0.0;
					else
						sum2 = totalstates[i]/(1.0 + degen[i]);
					dif = sum2 - occupancy[i];	//essentially say if there are more occupied states than there should be from total states, increase the fermi function
				}
				else //get the number of holes present based on the energy
				{
					if(EfGuess < Energies[i])
						sum2 = totalstates[i];
					else if(EfGuess > Energies[i])
						sum2 = 0.0;
					else
						sum2 = totalstates[i] * degen[i] / (1.0 + degen[i]);

					dif = occupancy[i] - sum2;	//make it opposite as dSdEf should have dif subtracted
				}
				
				dSdEf += dif;

			}
			else	//T != 0, so this will work...
			{	//solve the equation as above
				if(CarrierType[i] == CONDUCTION)
					expfact = degen[i] * exp((Energies[i] - EfGuess)*Beta);
				else
					expfact = degen[i] * exp((EfGuess - Energies[i])*Beta);	//this is for the fermi function, 1-F(e) is just reversing the sign in the exponent - make applicable with holes

				double Fe = 1 + expfact;	//if expfact is infinite, then Fe is zero
				Fe = 1.0/Fe;
				if(Fe > 0.0)
					outfactor = totalstates[i] * (expfact * Fe) * Fe;	//which results in 0*0*inf, which is indeterminate
				else
					outfactor = 1.0;	//it's soooo far off that df(E)/dEf is zero, but we want to see an adjustment made
				//in this case, dif is likely INCREDIBLY small to begin with, that it won't really have much of an effect on things.

				dif = totalstates[i] * Fe - occupancy[i];	//this stays fine for the sum, which isn't actually used
				
				if(CarrierType[i] == ELECTRON)
					dSdEf += dif * outfactor;	//but this would become indeterminate since outfactor is indeterminate (expfact = infinity)
				else
					dSdEf -= dif * outfactor;	//this should actually push it in the opposite direction because it's holes
			}
		}

		if(dSdEf > 0)	//the slope is greater than 0, so the guess was too high
			EfMax = EfGuess;	//if there is generally more occupied states than there should be, raise the search on Ef
		else if(dSdEf < 0)
			EfMin = EfGuess;	//same as above excelt less, then lower the search for Ef
		else	//sum was 0, so this is the best fit to the data possible...
			break;

		if(EfGuess == OldEfGuess)	//if it's not changing, then there's probably a problem.
		{
			ProgressDouble(6, EfGuess);
			break;
		}
		OldEfGuess = EfGuess;
		
	}while(fabs(EfMax - EfMin) > accuracy);	//if you guess right, just quit early!
	
	EfGuess = (EfMax + EfMin)*0.5;	//if guessed right, this will just put in the same value as beforehand
	//if within bounds, it will put in a more accurate guess at the value

	pt.fermi = EfGuess;	//pt.fermi now holds the absolute value of where the fermi energy is in relation to the
	//entire diagram IF the valence and conduction band are at the appropriate positions.

//	if(pt.contactData)
//		ProgressDouble(43, EfGuess);

//	ProgressDouble(5, EfGuess);

	delete [] degen;
	delete [] occupancy;
	delete [] Energies;
	delete [] totalstates;
	delete [] CarrierType;

	return;
}



bool FindQuasiFermiPt(Point& pt, double accuracy, bool which)
{
	if(pt.MatProps->type == VACUUM)
	{
		pt.fermin = pt.Psi;
		pt.fermip = pt.Psi;
		return(true);
	}
	//first, capture all the occupied states
	unsigned int size;
	if(which==CONDUCTION)
		size = pt.cb.energies.size();
	else
		size = pt.vb.energies.size();

	if(size == 0)
		return(true);

	double EfMin = pt.Psi;		//set the maximum Ef to be at the top of the conduction band
	double EfMax = pt.vb.bandmin - pt.MatProps->Phi;	//and the minimum Ef to be at the bottom of the valence band
	
	double expfact = 0.0;	//the exponent used everywhere in this sum: exp(Beta*(Ei-Ef))
	double Beta = 0.0;
	if(pt.temp != 0.0)
		Beta = 1.0/KB/pt.temp;
	double dSdEf = 0.0;	//the value of interest to calculate
	double *Energies = new double[size];
	float *degen = new float[size];
	double *occupancy = new double[size];
	double *totalstates = new double[size];
	bool *CarrierType = new bool[size];
//	double sum1 = 0.0;
	double sum2 = 0.0;
	double outfactor = 0.0;
	double dif;
	bool fermilimit = false;

	if(pt.MatProps->Eg < 1.0 && pt.MatProps->Eg > 0.0)
	{
		accuracy = accuracy * pt.MatProps->Eg * pt.MatProps->Eg;
	}
	else if(pt.MatProps->Eg <= 0.0 && accuracy > 1.0e-10)
		accuracy = 1.0e-10;	//make it REALLY accurate as charge calculations will be very off for a slight numerical error in Ef

	if(accuracy < 1e-15)
		accuracy = 1e-15;	//make sure it's within a double limit

	//first populate data that does not change: the three arrays above.
	unsigned int i=0;
	if(which == CONDUCTION)	//find Efn
		i=LoadData(degen, occupancy, totalstates, Energies, CarrierType, EfMax, EfMin, pt, FERMI_LOAD_CB);
	else	//find Efp
		i=LoadData(degen, occupancy, totalstates, Energies, CarrierType, EfMax, EfMin, pt, FERMI_LOAD_VB);

	if(EfMin == pt.Psi)	//if it somehow didn't alter the min/max, then set them to defaults
		EfMin = pt.vb.bandmin - pt.MatProps->Phi;
	if(EfMax == pt.vb.bandmin - pt.MatProps->Phi)
		EfMax = pt.Psi;

	if(which == CONDUCTION)	//there will presumably be completely empty states in the CB. Allow it to go to the bottom of the band.
		EfMin = pt.vb.bandmin - pt.MatProps->Phi;
	else	//there are presumably empty states in the VB. Allow the fermi level to go to the top of the band.
		EfMax = pt.Psi;

	double EfGuess = (EfMax + EfMin)*0.5;	//initial guess to start the ball rolling
	double OldEfGuess = EfMax + 1.0;
	
	double totalocc = 0.0;	//the total occupancy for this particular position. Mostly want to check if it is just zero
	double totalst = 0.0;
//	bool allBad;
	//and then have the fermi min/max set which will allow it to be anything above/below said value to clear up the band diagram.
	for(i=0; i<size; i++)
	{
		totalocc += occupancy[i];
		totalst += totalstates[i];
	}

	if(totalocc == 0.0)	//can set the flag
	{
		fermilimit = true;	//allow anything above or below this value (depending on which band it is)
		if(size>0)
		{
			occupancy[0] = 1e-200;
		}
	}
	do
	{
		dSdEf = 0.0;	//initialize the sum
		EfGuess = (EfMax + EfMin)*0.5;	//cut in by half...
//		sum1 = 0.0;
		sum2 = 0.0;
//		allBad = true;

		for(i = 0; i < size; i++)
		{
			if(pt.temp == 0.0)
			{
				/*
				df/dEf = expfact/(1+expfact)^2, or ~ 1/expfact
				if Ei>Ef, this is inf/(inf^2*0), or 0 as the infs are stronger functions
				if Ei<Ef, this is 0/(0+1)^2, or the total = 0
				Same can be said for the second term, except its expfact/(1+expfact)^3
				setting to 0 will cause the same output in the sum, 0...
				The total sum is then -(fraction)/inf + 1/inf^2

				However, at T=0, df/dEf is always 0, with a discontinuity at Ef.
				A new method is needed...
				based on the previousmethodl df/dEf is the same for all values, so make it not matter by "setting
				it to one."  therefore, this is equivalent to a least (singles?) method, in which
				the fermi function is simply compared to the occupied states, and the best case scenario
				is roughly equal errors on both sides. Theoretically, at 0K, everything is in the ground state anyway,
				so if Ef is too low, the sum will be negative from the -occ/tot, and if Ef is too high, there will be
				extra 1's added that don't get cancelled out by the states, pushing Ef down.
				*/

				if(CarrierType[i] == CONDUCTION)
				{
					if(EfGuess > Energies[i])
						sum2 = totalstates[i];	//here, sum 1 is the fermi energy
					else if(EfGuess < Energies[i])
						sum2 = 0.0;
					else
						sum2 = totalstates[i]/(1.0 + degen[i]);

					dif = sum2 - occupancy[i];	//essentially say if there are more occupied states than there should be from total states, increase the fermi function
				}
				else //get the number of holes present based on the energy
				{
					if(EfGuess < Energies[i])
						sum2 = totalstates[i];
					else if(EfGuess > Energies[i])
						sum2 = 0.0;
					else
						sum2 = totalstates[i] * degen[i] / (1.0 + degen[i]);

					dif = occupancy[i] - sum2;	//normally would subtract dif in the next step, so just make it -dif
				}

				
				dSdEf += dif;

			}
			else	//T != 0, so this will work...
			{	//solve the equation as above
				if(CarrierType[i] == ELECTRON)
					expfact = degen[i] * exp((Energies[i] - EfGuess)*Beta);
				else
					expfact = degen[i] * exp((EfGuess - Energies[i])*Beta);
				double Fe = 1 + expfact;
				Fe = 1.0/Fe;
				
				if(Fe > 0)
				{
//					allBad = false;
					outfactor =  totalstates[i] * (expfact * Fe) * Fe;
				}
				else
					outfactor = 1.0;	//this would occur if expfact=+inf, so Fe=0, so the first two cancel leaving just the original Fe
				//make sure the cancelling terms occur roughly first. The difference is likely very negligible, and we don't want to
				//prematurely exit the loop
				dif = totalstates[i] * Fe - occupancy[i];

				double change = dif * outfactor;
				if(change == 0.0)	//we're beyond the 1e-300. Most likely 1e-200 * 1e-200 or something like that.
					change = dif;
				if(CarrierType[i] == ELECTRON)
					dSdEf += change;
				else
					dSdEf -= change;
			}
		}

		if(dSdEf > 0)	//slope is greater than 0 (think parabola), so the Ef used was too high
			EfMax = EfGuess;	//look for a larger Ef
		else if(dSdEf < 0)
			EfMin = EfGuess;	//look for a smaller Ef
		else	//sum was 0, so this is the best fit to the data possible...
			break;

		if(EfGuess == OldEfGuess)
		{
			ProgressDouble(6, EfGuess);
			break;
		}
		OldEfGuess = EfGuess;
	}while(fabs(EfMax-EfMin) > accuracy);	//if you guess right, just quit early!
	
	EfGuess = (EfMax + EfMin)*0.5;	//if guessed right, this will just put in the same value as beforehand
	//if within bounds, it will put in a more accurate guess at the value

	if(which == CONDUCTION)
		pt.fermin = EfGuess;
	else
		pt.fermip = EfGuess;

	//ProgressDouble(5, EfGuess);

	delete [] degen;
	delete [] occupancy;
	delete [] Energies;
	delete [] totalstates;
	delete [] CarrierType;

	return(fermilimit);
}

unsigned int LoadData(float* degen, double* occupancy, double* total, double* Energies, bool* type, double& EfMax, double& EfMin, Point& pt, int active)
{
	double cb = pt.cb.bandmin;
	double vb = pt.vb.bandmin;
	unsigned int i=0;
	if(active & FERMI_LOAD_CB)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.cb.energies.begin(); it != pt.cb.energies.end(); ++it)
		{
			degen[i] = 1.0;	//it's a band, so just one.
			Energies[i] = cb + it->second.eUse;
			type[i] = CONDUCTION;
			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	if(active & FERMI_LOAD_VB)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.vb.energies.begin(); it != pt.vb.energies.end(); ++it)
		{
			degen[i] = 1.0;	//it's a band, so just one.
			Energies[i] = vb - it->second.eUse;
			type[i] = VALENCE;
			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	if(active & FERMI_LOAD_DONOR)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.donorlike.begin(); it != pt.donorlike.end(); ++it)
		{
			degen[i] = it->second.imp->getDegeneracy();
			type[i] = CONDUCTION;	//it's treated as electrons
			
			Energies[i] = cb - it->second.eUse;
			
			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	if(active & FERMI_LOAD_ACCEPTOR)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.acceptorlike.begin(); it != pt.acceptorlike.end(); ++it)
		{
			degen[i] = it->second.imp->getDegeneracy();
			type[i] = VALENCE;	//it's treated as holes
			
			Energies[i] = vb + it->second.eUse;

			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	if(active & FERMI_LOAD_CBTAILS)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.CBTails.begin(); it != pt.CBTails.end(); ++it)
		{
			degen[i] = 1.0;
			type[i] = CONDUCTION;	//it's treated as electrons
			Energies[i] = cb - it->second.eUse;

			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	if(active & FERMI_LOAD_VBTAILS)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.VBTails.begin(); it != pt.VBTails.end(); ++it)
		{
			degen[i] = 1.0;
			type[i] = VALENCE;	//it's treated as electrons
			
			Energies[i] = vb + it->second.eUse;

			if(it->second.GetNumstates() != 0.0)
			{
				total[i] = it->second.GetNumstates();
				occupancy[i] = it->second.GetOccupancy();
			}
			else
			{
				total[i] = 0.0;
				occupancy[i] = 0.0;
			}
			if(it->second.AbsFermiEnergy > EfMax)
				EfMax = it->second.AbsFermiEnergy;
			if(it->second.AbsFermiEnergy < EfMin)
				EfMin = it->second.AbsFermiEnergy;

			i++;
		}
	}

	return(i);
}

//this function is for debugging purposes only
void CalcLeastSquaresGraphData(Point& pt, Simulation& simul)
{
	//first, capture all the occupied states
	unsigned int size = pt.cb.energies.size() + pt.vb.energies.size() + pt.CBTails.size() + pt.VBTails.size() + pt.acceptorlike.size() + pt.donorlike.size();
	double EfMax = pt.Psi;		//set the maximum Ef to be at the top of the conduction band
	double EfMin = pt.vb.bandmin - pt.MatProps->Phi;	//and the minimum Ef to be at the bottom of the valence band
	
	double Beta = 0.0;
	if(simul.T != 0.0)
		Beta = 1.0/KB/simul.T;
	double *Energies = new double[size];
	float *degen = new float[size];
	double *occupancy = new double[size];
	double *totalstates = new double[size];
	bool *type = new bool[size];

	double dif;


	//first populate data that does not change: the three arrays above.
	
	unsigned int i = LoadData(degen, occupancy, totalstates, Energies, type, EfMax, EfMin, pt, FERMI_LOAD_CB | FERMI_LOAD_VB | FERMI_LOAD_CBTAILS | FERMI_LOAD_VBTAILS | FERMI_LOAD_DONOR | FERMI_LOAD_ACCEPTOR);
	//i counts up in the loaddata/loaddataes. The following loop ensures all the extra
	//stuff is set to 0.0 and then has no effect on the calculations.


	double outputSum = 0.0;
	double outputDeriv = 0.0;

	WriteFermiInfo(0,0,0,true);	//start a new file
	for(double EfGuess = EfMin; EfGuess <= EfMax; EfGuess += 0.001)
	{
		outputSum = 0.0;
		outputDeriv = 0.0;
		for(i=0; i<size; i++)
		{
			double expFact = Energies[i] - EfGuess;
			if(type[i] == HOLE)
				expFact = -expFact;
			expFact = degen[i] * exp(expFact * Beta);
			double Fe = 1.0 + expFact;
			Fe = 1.0 / Fe;
			dif = Fe * totalstates[i] - occupancy[i];
			outputSum += dif * dif;
			if(Fe > 0.0)
			{
				if(type[i] == ELECTRON)
					outputDeriv += 2.0 * Beta * (expFact * Fe) * totalstates[i] * Fe * dif;
				else
					outputDeriv -= 2.0 * Beta * (expFact * Fe) * totalstates[i] * Fe * dif;
			}
			else
			{
				if(type[i] == ELECTRON)
					outputDeriv += 2.0 * Beta * dif;
				else
					outputDeriv -= 2.0 * Beta * dif;
			}

		}

		WriteFermiInfo(EfGuess, outputSum, outputDeriv, false);	//want to get a plot of the function
		
	}

	delete [] degen;
	delete [] occupancy;
	delete [] Energies;
	delete [] totalstates;
	delete [] type;
	return;
}
