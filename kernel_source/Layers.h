#ifndef SAAPD_LAYERS_H
#define SAAPD_LAYERS_H

#include "BasicTypes.h"
#include "../gui_source/XML.h"

class Shape;
class Impurity;
class Material;
class ModelDescribe;

template <class prop>
class ldescriptor
{
public:
	prop data;	//hold the specific information about the specified descriptor
	int flag;		//how to distribute various bits throughout device
	PtRange range;	//starting/highest intensity position to a half width, etc.
	int id;			//id to identify the impurity/matl that data should point to.
	double multiplier;
	void SaveToXML(XMLFileOut& file, std::string classTag)
	{
		if (file.isOpen() == false)
			return;
		if (id < 0)	//don't save bad descriptors
			return;

		file.CreateCategory(classTag);
		
		file.WriteTag("flag", flag);
		file.WriteTag("id", id);
		if (multiplier != 1.0)
			file.WriteTag("multiplier", multiplier, 6, true);
		if (flag != 0)	//the unity description.
			range.SaveToXML(file);

		file.CloseLastCategory();
	
	}

	ldescriptor()
	{
		data = NULL;
		flag = 0;
		range.startpos.x = 0;
		range.startpos.y = 0;
		range.stoppos.x = 0;
		range.stoppos.y = 0;
		id = 0;
		multiplier = 1.0;
	};
	~ldescriptor()
	{
		data = NULL;
		flag = 0;
		range.startpos.x = 0;
		range.startpos.y = 0;
		range.stoppos.x = 0;
		range.stoppos.y = 0;
		id = 0;
		multiplier = 0.0;
	};
	//stop = lowest intensity/direction/half intensity position(or end if flat)
};


class CustomLayerOutputs
{
public:
	bool OpticalPointEmissions;
	bool AllRates;
};


class Layer	//part of initial setup... vectors because want to cycle through each one
{
public:

	std::string name;				//name of the layer
	Shape* shape;				//shape of the layer - list of vector points.
	double spacingboundary;		//spacing between points at boundary of layer
	double spacingcenter;		//spacing between points at center of layer
	double sizeScale;	//scaling factor to reduce the overall size of the volume in the "free" dimensions: 1 to 0(nonexistent)
	XSpace deviation;			//used to figure out if a point is within error bounds of layer boundaries
	int id;					//used to distinguish the different layers. Note: negative layer = contact
	int contactid;			//used to see if this layer is supposed to be a contact
	bool exclusive;			//only this layer can be for this particular point
	bool AffectMesh;		//should this layer be used in determining the mesh?
	bool priority;			//this layer has priority in generating a mesh first
	CustomLayerOutputs out;	//custom layer outputs

	std::vector<ldescriptor<Impurity*>*> impurities;	//descriptions for all the impurities in this layer
	std::vector<ldescriptor<Material*>*> matls;	//descriptions for the material(s) in this layer

	//note, sort is wavelength. This is total output
	std::map<unsigned int, double> OutputLightIncident;	//total light photons incident on layer during simulation
	std::map<unsigned int, double> OutputLightCarrierGen;	//free carriers generated from light (ignore excitements within band)

	bool InLayer(XSpace pos);
	void SaveToXML(XMLFileOut& file);
	void Validate(ModelDescribe& mdesc);

	Layer();
	~Layer();
	Layer(const Layer &lyr);

};


#endif