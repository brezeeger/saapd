
#include "CalcBandStruct.h"

#include <cmath>

#include "EquationSolver.h"
#include "RateGeneric.h"
#include "model.h"
#include "contacts.h"
#include "physics.h"
#include "outputdebug.h"
#include "output.h"
#include "fermiband.h"
#include "PoissonPrep.h"
#include "Equilibrium.h"
#include "scatter.h"
#include "ss.h"
#include "transient.h"


/*
This function sets up the differential equations to solve poisson's equation in one dimension.
Creating numerous equations of (with x representing a point):
				(V_x+1 - V_x)		(V_x - V_x-1)
	 			---------------  -	---------------
d^2 V			pos_x+1 - pos_x		pos_x - pos_x-1				rho
-----	  =	   -------------------------------------	=	-	---
dx^2				(pos_x+1 - pos_x-1)/2						eps

That's all fine and dandy, except V is like -Psi.  And these values being solved are actually for Psi! So positive rho/eps

In the situations where x+1 or x-1 are outside of the system, these are added to the
right hand side of the equation, but must also be divided by a delta_x^2 depending on which one it is.
If a portion is removed, the pos_x's are invalid, so it is assumed that the width on one side is equal
to the width on the other side to keep things relatively in scale.

About the above paragraph.  Scratch that.  Just read the introduction to poissonprep.cpp for what it really does.
Still solves this equation, but its a tad more complicated.

First, the main coefficient is found, V_x and added to the equation as the first entry.
Then, as all the widths accurately represent the situation
*/


double CalcBandStruct(Model& mdl, ModelDescribe& mdesc, bool ThEq)
{
	if(!ThEq)
	{
		if(mdesc.simulation->simType == SIMTYPE_TRANSIENT && mdesc.simulation->TransientData)
		{
			if (mdesc.simulation->TransientData->DeterminePoissonValues() == CONTACT_BD_CHANGE)	//first figure out which variables are needed for poisson equation
				mdl.PoissonDependencyCalculated = false;	//unknowns changing between currents/efields will not trigger a recalculation - just the values plugged in change
		}
		else if(mdesc.simulation->simType == SIMTYPE_SS && mdesc.simulation->SteadyStateData)
		{
			if (mdesc.simulation->SteadyStateData->LastBDUpdated != mdesc.simulation->SteadyStateData->CurrentData)
			{
				mdesc.simulation->SteadyStateData->ExternalStates[mdesc.simulation->SteadyStateData->CurrentData]->ValidateData(mdl, mdesc.simulation);
				mdesc.simulation->SteadyStateData->LastBDUpdated = mdesc.simulation->SteadyStateData->CurrentData;
				mdl.PoissonDependencyCalculated = false;	//new iteration - new solution to band structure
			}
		}
	}

	if(mdl.PoissonDependencyCalculated == false)
	{
//		DisplayProgress(8, mdl.numpoints);
		SystemEq* System = new SystemEq();
		PoissonPrep(mdl, System, mdesc);	//prepare the equation to be solved
		System->SolveSystem();		//solve the system symbolically
		UpdatePsiCoeffs(mdl, System);	//store the symbolic solution for each point in the point class
		delete System;	//free up that portion of memory
		System = NULL;
		
		for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		{	//everything is presumably zero, but this should load in the boundary conditions as well
			
			if(!ThEq)
			{
				ptIt->second.ResetData();	//gets the proper number of carriers in the points, n, p, etc.
				mdesc.simulation->PrepPointBandStructure(mdl, ptIt->second);	//make sure values exist for the first time it is done
			}
			else
				mdl.poissonRho[ptIt->second.adj.self] = 0.0;	//clear everything out to start for ThEq
			//this is just updating rho...
		}
	//	mdl.poissonRhoBC[mdl.numpoints] = 0.0;	//this is for all the constants on the RHS. Make sure they are just a constant
		//and nothing more.	Cancel out this effect I added to see if that would solve the problem.
		//the real problem was I did not have a small enough resolution...
//		mdesc.simulation->changetimestep = 0.05 * mdesc.simulation->timestep;	//initialize this variable once
		
//		DisplayProgress(9,0);
	}

//	mdesc.simulation->changepsisafe = 1.0;	//allow a sq. change of 1eV before doing any throttling of the time step.
	//before solving, everything needs to be loaded for the first time
	
	double changePsi = 0.0;
	double maxchangePsi = 0.0;
	mdesc.simulation->PrepBoundaryConditions(mdl);	//loads contact values into boundary conditions (updates contact values if necessary)
	for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		changePsi = CalcPointBand(ptIt->second, mdl, *(mdesc.simulation));	//updates Psi, Ec, Ev, Ef, Efn, and Efp
		if(changePsi > maxchangePsi)
			maxchangePsi = changePsi;
	}
	
	//now update the external contact points as necessary
	for(std::vector<Contact*>::iterator itCtc = mdesc.simulation->contacts.begin(); itCtc!=mdesc.simulation->contacts.end(); ++itCtc)
	{
		(*itCtc)->DeterminePsiContactPts();	//update the band structure of the external points so those rates will be calculated correctly
	}
	
	mdl.PoissonDependencyCalculated = true;	//don't ever solve the equations again... At end instead of if to prevent message from occuring

	return(maxchangePsi);
}

/*
//updates the poissonDependency from solving a particular grid in poisson's equation
int UpdateEquation(std::map<unsigned long int, double>& eq, Point& pt, Model& mdl)
{
	
	unsigned long int index = 0;

	for(std::map<unsigned long int, double>::iterator term = eq.begin(); term != eq.end(); ++term)
	{
		pt.poissonDependency[index].data = term->second;	//the data holds how much of the particular rho is needed
		pt.poissonDependency[index].pt = term->first;	//sort holds the index in the array it should access to get that number
		index++;	//increment the index to prevent overwrites
		//poissonDependency is how all other points rho's affect this point
	
		//now do the whole master list thing for how it affects everything.
		//each point will store how it affects all the others -> NOT how it depends on all the others
		//EQ: V1 = A Rho1 + B Rho2 + C Rho3... node->Data is A/B/C. node->sort is 1/2/3 RHS
		//EQ: V2 = A Rho1 + B Rho2 + C Rho3... pt.self is LHS 1/2/3
		std::map<long int, Point>::iterator trgPt = mdl.gridp.find(term->first);
		if(trgPt != mdl.gridp.end())	//it exists
		{
			PtDataPtr temp;
			temp.pt = &pt;
			temp.data = term->second * trgPt->second.voli * QCHARGE;	//NOT pt.VOLI!
			trgPt->second.PsiCf.push_back(temp);	//how this point affects all other points. 
			if(fabs(temp.data) > trgPt->second.largestPsiCf)
				trgPt->second.largestPsiCf = fabs(temp.data);
//		mdl.gridp[node->Sort].SumPsiCf += temp.data;
		}
	}
	
	return(0);
}
*/

int UpdatePsiCoeffs(Model& mdl, SystemEq* system)	//puts the dependency of each variable in the equation
{
	for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		ptIt->second.PsiCf.clear();	//used in fast calculation of continuous update for fermi leveling.
		ptIt->second.largestPsiCf = 0.0;
//		mdl.gridp[pt].SumPsiCf = 0.0;	//used to avoid many, many sums in MC aspect of code
	}
	std::map<unsigned long int, std::vector<SystemEqReturns>> solution;
	if(system->GetReturns(solution) == -1)	//pull all the output data from the system of equations
	{
		ProgressString("Failed to load poisson equation solution");
		return(-1);
	}
	//now we need to apply said data
	PtDataPtr tmpPsiCf;
	for(std::map<unsigned long int, std::vector<SystemEqReturns>>::iterator variable = solution.begin(); variable != solution.end(); ++variable)
	{
		std::map<long int, Point>::iterator srcPt = mdl.gridp.find(variable->first);
		if(srcPt != mdl.gridp.end())
		{
			delete [] srcPt->second.poissonDependency;	//make sure no accidental memory leaks
			srcPt->second.pDsize = variable->second.size();
			srcPt->second.poissonDependency = new PtData[srcPt->second.pDsize];	//allocate data for the new dependency
			for(unsigned long i=0; i < srcPt->second.pDsize; ++i)
			{
				srcPt->second.poissonDependency[i].data = variable->second[i].data;	//how all the other rhos/BC affect this point (LHS in equation)
				srcPt->second.poissonDependency[i].pt = variable->second[i].which;

				//how this point affects all the other points
				std::map<long int, Point>::iterator trgPt = mdl.gridp.find(variable->second[i].which);
				if(trgPt != mdl.gridp.end())
				{
					tmpPsiCf.data = variable->second[i].data * trgPt->second.voli * QCHARGE;
					tmpPsiCf.pt = &(srcPt->second);
					trgPt->second.PsiCf.push_back(tmpPsiCf);
				
					if(fabs(tmpPsiCf.data) > trgPt->second.largestPsiCf)
						trgPt->second.largestPsiCf = fabs(tmpPsiCf.data);
				}

			}
		}
		variable->second.clear();	//clear out the memory in the vector after it's accessed
	}
	solution.clear();	//and clear out the memory of the map
	/*
	for(unsigned long int eq=0; eq < system->Equation.size(); eq++)
	{
		//every single equation should be 1 LHS and many RHS...
		//first, determine what the LHS is supposed to be.
		if(system->Equation[eq].LHS.size() == 1)	//make sure the equation has only one term in it...
		{
			unsigned long int pointID = system->Equation[eq].LHS.begin()->first;
			std::map<long int, Point>::iterator trgPt = mdl.gridp.find(pointID);

			if(trgPt != mdl.gridp.end())	//this is because fake points were created outside the model to act as boundary conditions
			{
				delete [] trgPt->second.poissonDependency;	//make sure no accidental memory leaks
				trgPt->second.pDsize = system->Equation[eq].RHS.size();
				trgPt->second.poissonDependency = new PtData[trgPt->second.pDsize];	//allocate data for the new dependency

			
				UpdateEquation(system->Equation[eq].RHS, trgPt->second, mdl);	//go through the solution and store it for later fast use...
			}
		}
	}
	*/
#ifndef NDEBUG
	CheckPoissonEQTransfer(mdl);	//outputdebug.cpp
#endif
	return(0);
}

//note, does not update fermi energy.
double CalcPointBand(Point& pt, Model& mdl, Simulation& sim)
{
	double prevPsi = pt.Psi;
	pt.Psi = 0.0;	//reset Psi for the calculation

	for(unsigned int i=0; i < pt.pDsize; i++)	//add up all the contributions to psi
	{
		if(pt.poissonDependency[i].pt < mdl.numpoints)
			pt.Psi += pt.poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl.poissonRho[pt.poissonDependency[i].pt];	//what that rho/BC is
		else	//this is a boundary condition. Just pull the particular boundary condition
		{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int,BoundaryCondition>::iterator BoundCond = mdl.poissonBC.find(pt.poissonDependency[i].pt);
			if(BoundCond != mdl.poissonBC.end())
				pt.Psi += pt.poissonDependency[i].data * BoundCond->second.value;

		}
	}
	//psi is now in the proper location

	if(pt.MatProps != NULL)
	{
		pt.cb.bandmin = pt.Psi - pt.MatProps->Phi;	//now put Ec
		pt.vb.bandmin = pt.cb.bandmin - pt.MatProps->Eg;	//and Ev in their places
		pt.ResetFermiEnergies(true, RATE_OCC_END, sim.accuracy, true);	//reclalculate all the energy states fermi levels (1st true) and skip the point value (will get set for scattering as an instantaneous value)
		//the fermi energies were reset with CalcPoint=true, which updated all the eUse levels.
		double Nbeta = 0.0;
		if(pt.temp > 0.0)
			Nbeta = -KBI / pt.temp;

		CalcScatterConsts(pt.scatterCBConst, pt.cb.energies, Nbeta);	//sums up all exponents
		CalcScatterConsts(pt.scatterVBConst, pt.vb.energies, Nbeta);
		
	}
	else
	{
		pt.cb.bandmin = pt.Psi;
		pt.vb.bandmin = pt.Psi;
		pt.fermi = pt.Psi;
		pt.fermin = pt.Psi;
		pt.fermip = pt.Psi;
	}

	double adjPsi = fabs(pt.Psi - prevPsi);

	return(adjPsi);
}

//updates all the points Ef, Efn, and Efp.
int UpdateAllFermiPt(Model& mdl, Simulation& simul)
{
	for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		UpdateFermiPt(ptIt->second, simul);
	return(0);
}

//updates the fermi energies based on location of Ec, Ev, and particle distributions for a 
int UpdateFermiPt(Point& pt, Simulation& sim)	//particular point
{
	FindFermiPt(pt, sim.accuracy);	//Load Ef

	//now see if we can outright avoid doing the Efn and Efp calculations once Ef is loaded
	bool limitEfn = FindQuasiFermiPt(pt, sim.accuracy, CONDUCTION);
	bool limitEfp = FindQuasiFermiPt(pt, sim.accuracy, VALENCE);

	if(limitEfn)	//the fermi level may be anything lower than the current value - a good guess is probably Ef.
	{	//this assumes that the carrier was in the bottom state for at least 1e-19 seconds of the simulation (first timestep)
		if(pt.fermi < pt.fermin)	//first try to set it to the point's fermi level
			pt.fermin = pt.fermi;
		else if(pt.fermip < pt.fermin && limitEfp == false)	//assuming Efp is a fixed value, try to use that value if possible
			pt.fermin = pt.fermip;
					//otherwise, just leave it at its current value.
	}

	if(limitEfp)	//same as above, except for Efp and it must be higher than the current value.
	{
		if(pt.fermi > pt.fermip)
			pt.fermip = pt.fermi;
		else if(pt.fermin > pt.fermip && limitEfn == false)
			pt.fermip = pt.fermin;
	}

	/*
	if(pt.cb.GetNumParticles() > 0.0)	//the conduction band is NOT empty
	{
		FindQuasiFermiPt(pt, sim.accuracy, CONDUCTION, sim.simtime);	//Load Efn as athe "best" answer
	}
	else	//there are no particles in the conduction band
	{
		if(pt.cb.energies.start)	//there is a CB...
		{
			Tree<ELevel,double>* bandmin = pt.cb.energies.start->SearchLeft();	//get the band minimum target
			pt.fermin = bandmin->Data.AbsFermiEnergy;
		}
		else	//it's a vacuum or something funny.
			pt.fermin = pt.fermi;

	}

	if(pt.vb.GetNumParticles() > 0.0)	//the valence band is NOT empty
	{
		FindQuasiFermiPt(pt, sim.accuracy, VALENCE, sim.simtime);	//Load Efp as athe "best" answer
	}
	else	//there are no particles in the valence band, so do a "fast" simplified calculation.
	{
		if(pt.vb.energies.start)	//there is a CB...
		{
			Tree<ELevel,double>* bandmin = pt.vb.energies.start->SearchLeft();	//get the band minimum target AbsFermi. This will be the highest Efp.
			pt.fermip = bandmin->Data.AbsFermiEnergy;	//note, energies are referenced to VB.
		}
		else	//it's a vacuum or something funny.
			pt.fermip = pt.fermi;

	}
	*/
	return(0);
}