#include "Tunneling.h"

#include "BasicTypes.h"
#include "model.h"	//
#include "physics.h"
#include "RateGeneric.h"
#include "outputdebug.h"
/*
The tunneling code uses corrections to the WKB approximation.

Tw = exp(-2/h_bar Integral [dx sqrt(2m* (V - E)) ]

Tw2 = Tw / (1 + 0.25 Tw)^2	This includes reflections, but does not have the sharp edge correction

T1 = 4 Tw E sqrt(Eb/E - 1) / Eb		This is a sharp edge correction, but does not account for reflections

T = T1 / (1 + 0.5 T1 + .25 Tw^2)
This has corrections to account for a sharp edge, and includes reflections in the tunneling.

For tunneling between bands:
Tw = exp(-2/h_bar Integral [ dx sqrt{m* / (2 Eg) * (Eg^2 - 4(E-V)^2) } ] - V must be the conduction band
Then unless there was a sharp edge in the conduction band, do NOT use anything in the VB as reference.
That model just doesn't fit in the framework.  That model may only be used with innerframework tunneling.


The integral is calculated as a sum, but instead of doing just a simple calculation as rectangular bars, the
potential is shifted into linear connecting lines.  this enables more accurate calculation of the integral as the
mesh does not have a constant delta.
*/


//search for a tunneling staten at a particular energy. Go iteratively.
//random number generator can only go to 1e-10, and it is multiplied by the number of carriers.

int TunnelingMain(int dir,	//the direction the carriers are tunneling in
	RefTrans& Output,	//the output sent back to be used
	Model& mdl,	//pass in the entire model simply
	ELevel* eLev,	//the energy level initiating the tunneling (the electrons start point)
	bool carrierType,	//what kind of carrier are we dealing with
	bool disableTunneling)	//should tunneling be disabled?
{

	//DISABLE TUNNELING
	if((carrierType == HOLE && eLev->carriertype == ELECTRON) || disableTunneling)
	{
		Output.transmission = 0.0;	//signal no tunneling and just get outta here right away. Trying to tunnel holes from the CB/IMP
		Output.tunnel = false;
		return(0);
	}

	WKBData* wkb = new WKBData;	//hold all the relevant data.
	//initialize the wkb data
	wkb->carrier = carrierType;
	wkb->curpoint = &eLev->getPoint();
	wkb->dir = dir;
	wkb->initBand = eLev->carriertype;	//if e-, will be CB. if hole, will be VB.
	if(eLev->imp == NULL && eLev->bandtail == false)	//it is in one of the bands
	{
		if(wkb->initBand == CONDUCTION)
			wkb->Energy = eLev->eUse + wkb->curpoint->cb.bandmin;
		else
			wkb->Energy = wkb->curpoint->vb.bandmin - eLev->eUse;		

		TunnelStart(mdl, wkb);	//determine a starting and ending position for the tunneling. The type of tunneling, etc.
	}
	else	//it is the impurity.  Right now, that means there is no tunneling. Why TunnelSTart is in the above calculation.
	{
		if(eLev->carriertype == CONDUCTION)
			wkb->Energy = wkb->curpoint->cb.bandmin - eLev->eUse;
		else
			wkb->Energy = wkb->curpoint->vb.bandmin + eLev->eUse;
	}
	
	//it is possible for electrons to be done in the valence band
	

	switch(wkb->type)
	{
		case TUNNEL_WKB:
			TunnelWKB(mdl, wkb);
			Output.ptrTargetState = FindELevel(wkb, mdl);
			break;
		case TUNNEL_WKB_SHARP:
			TunnelWKB(mdl, wkb);
			TunnelWKBSharp(wkb);
			Output.ptrTargetState = FindELevel(wkb, mdl);
			break;
		case TUNNEL_INTERBAND:
			Output.ptrTargetState = TunnelInterBand(wkb, mdl);
			break;
		case TUNNEL_BAND_IMPURITY:
//			break;	//this tunneling is not immplemented yet, so have it be equiv to tunnel_none.
		case TUNNEL_IMP_IMP:
//			break;	//not implemented yet. equiv to tunnel_none.
		case TUNNEL_NONE:	//just free the memory and quit the tunneling code
		default:
			wkb->type = TUNNEL_NONE;	//ensure that it gets the no tunneling when there isn't any code that has applied for the particular type yet.
			break;
	}
	
	if(Output.transmission <= 0.0)	//safety code just in case
	{
//		Output.transmission = 0.0;
		wkb->type = TUNNEL_NONE;
	}
	if(wkb->type != TUNNEL_NONE)
	{
		//now we need to find out what energy level it goes into
		
		Output.transmission = wkb->integral;
		if(Output.transmission > 1.0)
			Output.transmission = 1.0;
		Output.reflection = 1.0 - Output.transmission;
		Output.ptrTargetPoint = &Output.ptrTargetState->getPoint();
		Output.tunnel = true;
		Output.band = Output.ptrTargetState->carriertype;
		Output.ptrTargetFromState = eLev;
		Output.percentto = 1.0;	//only let it tunnel into one state, and don't concern yourself with the overlap
	}
	else	//make sure to set outputs to nothing
	{
		Output.transmission = 0.0;
		Output.reflection = 0.0;
		Output.ptrTargetPoint = NULL;
		Output.ptrTargetState = NULL;
		Output.tunnel = false;
		Output.ptrTargetFromState = NULL;
		Output.percentto = 0.0;

	}
		
	delete wkb;
	return(0);
}

//this will see the energy level that corresponds to the current point
ELevel* FindELevel(WKBData* wkb, Model& mdl)
{
	//get some local variables just to speed things up
	Point* pt = wkb->curpoint;
	double energy = wkb->Energy;
	ELevel* target = NULL;

	if(energy >= wkb->EcCur)
	{
		energy = energy - wkb->EcCur;	//get the energy to be relative to the current conduction band
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->cb.energies.begin(); cbit != pt->cb.energies.end(); ++cbit)
		{
			if(energy >= cbit->second.min && energy <= cbit->second.max)
			{
				target = &(cbit->second);
				break;
			}
		}
	}
	else if(energy <= wkb->EvCur)
	{
		energy = wkb->EvCur - energy;	//energy should be smaller than EvCur
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->vb.energies.begin(); vbit != pt->vb.energies.end(); ++vbit)
		{
			if(energy >= vbit->second.min && energy <= vbit->second.max)
			{
				target = &(vbit->second);
				break;
			}
		}
	}

	return(target);
}

/*
Tw = exp(-2/h_bar Integral [ dx sqrt{m* / (2 Eg) * (Eg^2 - 4(E-V)^2) } ]
then Tw2 it to include reflections
the nice thing about this is it is always electrons, and V(X) = (Ec + Ev)/2.  The smallest the sqrt can be is 0 (mathematically).
If it is less, then there is a rounding error.
When it gets to the edge, it ALWAYS fails in this model due to the negative square root!

*/
ELevel* TunnelInterBand(WKBData* wkb, Model& mdl)
{
	double potential1 = 0.0;	//integral value on previous side
	double potential2 = 0.0;	//integral value on forward side
	double delta = fabs(wkb->startpos - wkb->endpos);	//distance the portion is being tunneled through
	double efm = 1.0;	//effective mass
	double m = 0;	//slope of lines for linear vs stepwise approximation
	double energy = wkb->Energy;	//the absolute energy of the particle
	bool Finished = false;	//done tunneling
	Point* curPt = NULL;	//quick pointer to the current point
	Point* prevPt = NULL;	//quick pointer to the previous point
	ELevel* TargetState = NULL;	//return value -> the place tunneling into

	FirstInitIterTWKB(mdl, wkb);	//this will set everything up... including setting wkb->delta
	
	if(delta <= wkb->delta)	//then the electric field is SO strong that the tunneling takes place
	{	//entirely in a single point. This is essentially like a recombination term, and if it happens, the code may run into other
		//issues out of discretization.

		//in this case, the inside of the square root will start off with (E-V)^2 = (+/-Eg/2)^2, which will make the term 0.
		//at the very center of the trianglular shape, it will have E = V, so then it will just take on the form of Eg^2 ish.
		//the width is then the end positions.  The total area of the "barrier" then is the triangle (like capital delta) with height:
		//sqrt(m* Eg / 2)
		//however, this gets multiplied by -2 dx/h_bar, so move the two up in the sqrt term, divide by h, adjust for units
		//another however, this is a triangle.  The area is 0.5*b*h, and the 2 out front makes it just b*h, so don't move the two up!
		//the divide by 100 is then moved into the sqrtterm, so .01 -> 0.0001 -> 0.0001 * 0.5 = 0.00005
		curPt = wkb->curpoint;
		double sqrtterm = 0.00005 * MELECTRON * curPt->eg * QI;
		if(curPt->MatProps)
			sqrtterm = sqrtterm * curPt->MatProps->EFMeCond;
		sqrtterm = sqrt(sqrtterm);
		double exponent = -delta * sqrtterm / HBAR;
		exponent = exp(exponent);	//this is Tw, but want to include reflections, so Tw2 calculation follows
		double den = 1.0 + 0.5 * exponent + 0.0625 * exponent * exponent;
		wkb->integral = exponent / den;

		/*

		SI Units
		kg+0.5 J+0.5 m+1 J-1 s-1
		J = kg m^2 s^-2
		kg+0.5 J-0.5 m+1 s-1 = kg+0.5 m+1 s-1 kg-0.5 m-1 s+1 = 0

		My Units
		kg+0.5 eV0.5 cm+1 ev-1 s-1
		eV = J / Q = kg m+2 s-2 Q-1
		kg+0.5 eV-0.5 cm+1 s-1 = kg+0.5 kg-0.5 m-1 s+1 Q+0.5 cm+1 s-1
		= m-1 Q+0.5 cm+1 = cm/m Q^0.5

		Need to multiply by (m/cm) Q^-0.5 = 0.01/sqrt(q) to cancel out all the units properly
		*/

		//Regardless of the energy, it should be tunneling into the LOWEST energy state. Only do the lowest because it is the same point.  All non-material change bands tunnel into the lowest energy state...
		//or the bottom/top of the conduction/valence band

		if(wkb->initBand == CONDUCTION)	//the target state is then the lowest state in the valence band
		{
			if(curPt->vb.energies.size() > 0)
				TargetState = &(curPt->vb.energies.begin()->second);	//lowest energy
			else
				TargetState = NULL;
		}
		else
		{
			if(curPt->cb.energies.size() > 0)
				TargetState = &(curPt->cb.energies.begin()->second);
			else
				TargetState = NULL;
		}

		ProgressInt(41, curPt->adj.self);	//warn the user that their mesh may be inadequate for the given parameters.
		if(TargetState == NULL)
			wkb->type = TUNNEL_NONE;

		return(TargetState);
	}

	if(wkb->initBand == CONDUCTION)	//then it must be going into the valence band
	{
		do
		{
			InitIterTWKB(mdl, wkb);
			if(wkb->curpoint == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(NULL);	//then it is all failed.
			}
			curPt = wkb->curpoint;
			prevPt = wkb->prevpoint;

			if((wkb->prevpos <= wkb->startpos && wkb->betweenpos >= wkb->startpos && wkb->betweenpos <= wkb->endpos && wkb->curpos >= wkb->endpos) ||
			   (wkb->prevpos >= wkb->startpos && wkb->betweenpos <= wkb->startpos && wkb->betweenpos >= wkb->endpos && wkb->curpos <= wkb->endpos))	
			{	//the tunneling occurs in just these two points
				Finished = true;
				//this is similar to the problem above, except now there is a midpoint that has a known value.
				//the potential beforehand could be approximated as a triangle with a specific height.
				//now the potential is two triangles, which may have different heights. The other potentials on the endpoints will contribute 0,
				//but there may be different lengths.  this could only happen on the first iteration
//				........\............................../
//				.|.........|.............................|...................................|
				//the one thing for certain is that it is definitely tunneling through betweenpos. So I guess just do it?

				//the first area to be added: prev

				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->endpos - wkb->startpos);	//this is the triangular potential again.
					double sqrtterm = 0.00005 * MELECTRON * curPt->eg * QI;
					if(curPt->MatProps)
						sqrtterm = sqrtterm * curPt->MatProps->EFMeCond;
					sqrtterm = sqrt(sqrtterm);
					double exponent = -delta * sqrtterm / HBAR;
					exponent = exp(exponent);	//this is Tw, but want to include reflections, so Tw2 calculation follows
					double den = 1.0 + 0.5 * exponent + 0.0625 * exponent * exponent;
					wkb->integral = exponent / den;
					
					TargetState = FindELevel(wkb, mdl);
					
					if(TargetState == NULL)
						wkb->type = TUNNEL_NONE;
					
					return(TargetState);
				}
				else	//there can be a HUGE error here because the sampling size is just too small.  By doing linear bits
				{	//it may never actually see that it passes through the maximum where the Eg is. Therefore, do the above twice
					//but formatted towarsd the end.
					//don't want to miss it, so find it's location in both materials.

					delta = fabs(wkb->betweenpos - wkb->startpos);
					double distanceHalfPt=delta+1.0;	//the potential is maximized when E = (Ec + Ev)/2.
					//+1 is just to ensure the if statement doesn't fire if m=0.

					m = (curPt->Psi - prevPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					if(m != 0.0)
						distanceHalfPt = 0.5* prevPt->eg / m;	

					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm = 1.0;

					if(distanceHalfPt < delta)	//split this portion into two areas to sum over:
					{
						potential2 = wkb->EvPrev + m*(distanceHalfPt);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += distanceHalfPt * potential2;

						potential1 = potential2;	//setup for moving forward in this front half
						potential2 = wkb->EvPrev + m*(delta);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += (delta - distanceHalfPt) * (potential1 + potential2);	//dx, area of new trapezoid

					}
					else	//it doesn't reach the maximum contribution in the previous point, so just do this one normal
					{
						potential1 = 0.0;	//first one must always be 0.
						potential2 = wkb->EvPrev + m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += delta * potential2;
					}

					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm = 1.0;

					delta = fabs(wkb->endpos - wkb->betweenpos);

					//now check contributions from the other side
					if(m!=0)	//avoid divide by 0
						distanceHalfPt = 0.5* curPt->eg / m;	
					else
						distanceHalfPt = delta + 1.0;

					//it must move at least HALF an Eg to go through the maximum point in the space given, delta
					if(distanceHalfPt < delta)	//does it encounter the halfway point in the region of interest here?
					{
						
						potential1 = wkb->EvCur - m*wkb->delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

						//potential2 = energy;	//the theoretical max
						//potential2 = energy - potential2;
						//potential2 = 4.0 * potential2 * potential2;
						//potential2 = curPt->eg * curPt->eg - potential2;
						//if(potential2 < 0.0)	//fail safe in case of rounding errors
						//	potential2 = 0.0;
						//potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(efm * curPt->eg);

						wkb->integral += (wkb->delta - distanceHalfPt) * (potential1 + potential2);	//the dx should be within the proper range
						
						delta = delta - (wkb->delta - distanceHalfPt);	//change new delta to be what's left of the distance
						potential1 = 0.0;	//technically, pot1=pot2, pot2=0
					}
					else	//it does not move through the highest area point, so just do simple ones. Leave delta/efm as is
					{	//treat as linear/single triangle
						potential2 = 0.0;
						potential1 = wkb->EvCur - m*wkb->delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);
					}	//end it does not go through the maximum value of the function, so treat as simple function.
				}	//end different materials
			}	//end starts and ends on first two points
			else if((wkb->prevpos < wkb->startpos && (wkb->dir == PX || wkb->dir == PY)) ||	//is the previous point to the left of the start position and moving right?
			   (wkb->prevpos > wkb->startpos && (wkb->dir == NX || wkb->dir == NY)) )	//" " " " " " right " " " " " " left?
			{
				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->curpos - wkb->startpos);
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm = 1.0;
					potential1 = 0.0;	//At the start, E-V = Eg/2, so the sqrt term = 0
					potential2 = (wkb->EcCur + wkb->EvCur)*0.5;	//middle of band gap
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

					//sqrt{m* / (2 Eg) * (Eg^2 - 4(E-V)^2) }, ignore m0 and divide by 2. If Eg=0 as in a metal... there's no barrier to tunnel into!
				}
				else	//this is starting at a transition between different materials.
				{
					m = (curPt->Psi - prevPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					//only concerned that slope is positive in the direction of movement

					//must find where exactly the transition starts.
					if((wkb->startpos < wkb->betweenpos && (wkb->dir == PX || wkb->dir == PY)) ||
					   (wkb->startpos > wkb->betweenpos && (wkb->dir == NX || wkb->dir == NY)))	//then it starts before reaching the interface
					{	//must do multiple tunneling bits
						delta = fabs(wkb->betweenpos - wkb->startpos);
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						//I want the slope of the line... within a point, it is the same material, so just look at psi to first approx
						//this is CB start, so it should likely have a positive slope
							
						potential1 = 0.0;	//first one must always be 0.
						potential2 = wkb->EvPrev + m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += delta * potential2;	//once again, no 0.5 as tha will be handled later.

						//now prepwork for the integral occurring after the interface
						delta = wkb->delta;	//occurs through the entire interface
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						//m is still the same.
						potential1 = wkb->EvCur - m*delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

							//now do all the work again for potential 2. happy happy joy joy.
						potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = curPt->eg * curPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(potential2);

					}
					else	//it starts at the interface or in the second point. Regardless, only must concern with one point
					{
						delta = fabs(wkb->curpos - wkb->startpos);
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = wkb->EvCur - m*delta;	//set potential to VB and moving down properly to the starting point
						potential1 += curPt->eg * 0.5;	//bump it up to halfway between the CB and VB
						potential1 = energy - potential1;	//start calculation of square root term
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)	//be rounding error safe when close to 0
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

							//now do all the work again for potential 2. happy happy joy joy.
						potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = curPt->eg * curPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(potential2);

					}	//end starting within second point	
				}	//end starting transition between different materials
			}	//end it being the start point
			else if(energy <= wkb->EvCur)
			{	//then it is exiting the tunneling process
				Finished = true;
				if(curPt->MatProps == prevPt->MatProps)
				{
					//this is similar to the start... except the second potential ends up being 0.
					delta = fabs(wkb->prevpos - wkb->endpos);
					potential2 = 0.0;
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);
				}
				else	//ending in a different material
				{
					m = (curPt->Psi - prevPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					//only concerned that slope is positive in the direction of movement

					//must find where exactly the transition is.
					if((wkb->endpos > wkb->betweenpos && (wkb->dir == PX || wkb->dir == PY)) ||
					   (wkb->endpos < wkb->betweenpos && (wkb->dir == NX || wkb->dir == NY)))
					{	//the tunneling ends after going through the interface.
						//first, get the end part of the integral in curPt.
						delta = fabs(wkb->endpos - wkb->betweenpos);
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = wkb->EvCur - m * delta;
						potential1 += curPt->eg * 0.5;
						potential1 = energy - potential1;	//start calculation of square root term
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)	//be rounding error safe when close to 0
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

						wkb->integral += delta * potential1;	//potential2 would be 0.

						//now get the previous point values
						delta = wkb->deltaPrev;
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;	
						else
							efm = 1.0;
						potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential2 * potential1;
						potential1 = prevPt->eg * prevPt->eg - potential1;
						if(potential1 < 0.0)	//fail safe in case of rounding errors
							potential1 = 0.0;
						potential1 = efm / prevPt->eg * potential1;
						potential1 = sqrt(potential1);

						potential2 = wkb->EvPrev + m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

					}
					else	//it ends entirely in the previous point before the interface (or at the interface)
					{
						delta = fabs(wkb->endpos - wkb->prevpos);
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential2 * potential1;
						potential1 = prevPt->eg * prevPt->eg - potential1;
						if(potential1 < 0.0)	//fail safe in case of rounding errors
							potential1 = 0.0;
						potential1 = efm / prevPt->eg * potential1;
						potential1 = sqrt(potential1);

						potential2 = wkb->EvPrev + m*delta;	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors. This will likely be ~zero...
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);
					}

				}
			}	//end if it is done tunneling.
			else	//it continues to tunnel through the band as normal
			{
				if(curPt->MatProps == prevPt->MatProps)
				{
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);

					potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

					delta = wkb->delta + wkb->deltaPrev;

				}
				else
				{
					m = (curPt->Psi - prevPt->Psi) / (wkb->delta + wkb->deltaPrev);	
						//add m when working from prev, subtract m when working from cur
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm = 1.0;
					delta = wkb->deltaPrev;
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);

					potential2 = wkb->EvPrev + m*delta;	//potential 2 is now Ev at the interface from the left
					potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = prevPt->eg * prevPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors. This will likely be ~zero...
						potential2 = 0.0;
					potential2 = efm / prevPt->eg * potential2;
					potential2 = sqrt(potential2);

					wkb->integral += delta * (potential1 + potential2);
									
					//now setup the same on the other side
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;	
					else
						efm = 1.0;
					delta = wkb->delta;
					potential1 = wkb->EvCur - m * delta;
					potential1 += curPt->eg * 0.5;
					potential1 = energy - potential1;	//start calculation of square root term
					potential1 = 4.0 * potential1 * potential1;
					potential1 = curPt->eg * curPt->eg - potential1;
					if(potential1 < 0.0)	//be rounding error safe when close to 0
						potential1 = 0.0;
					potential1 = efm * potential1 / curPt->eg;
					potential1 = sqrt(potential1);

					potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

	
				}	//end midtunneling change of material
			}	//end midtunneling

			wkb->integral += delta * (potential1 + potential2);	//missing a 1/2 for the trapezoid. Will do all correction factors later...
			//This lack of 1/2 is the same as multiplying by 2, which is applied after the integral

		}while(!Finished);

		TargetState = FindELevel(wkb, mdl);


	}	//end started from the conduction band
	else	//started from the valence band, electrons exiting into the conduction band. Not holes going into CB.
	{
		do
		{
			InitIterTWKB(mdl, wkb);
			if(wkb->curpoint == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(NULL);	//then it is all failed.
			}
			curPt = wkb->curpoint;
			prevPt = wkb->prevpoint;

			if((wkb->prevpos <= wkb->startpos && wkb->betweenpos >= wkb->startpos && wkb->betweenpos <= wkb->endpos && wkb->curpos >= wkb->endpos) ||
			   (wkb->prevpos >= wkb->startpos && wkb->betweenpos <= wkb->startpos && wkb->betweenpos >= wkb->endpos && wkb->curpos <= wkb->endpos))	
			{	//the tunneling occurs in just these two points
				Finished = true;
				//this is similar to the problem above, except now there is a midpoint that has a known value.
				//the potential beforehand could be approximated as a triangle with a specific height.
				//now the potential is two triangles, which may have different heights. The other potentials on the endpoints will contribute 0,
				//but there may be different lengths.  this could only happen on the first iteration
//				........\............................../
//				.|.........|.............................|...................................|
				//the one thing for certain is that it is definitely tunneling through betweenpos. So I guess just do it?

				//the first area to be added: prev

				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->endpos - wkb->startpos);	//this is the triangular potential again.
					double sqrtterm = 0.00005 * MELECTRON * curPt->eg * QI;
					if(curPt->MatProps)
						sqrtterm = sqrtterm * curPt->MatProps->EFMeCond;
					sqrtterm = sqrt(sqrtterm);
					double exponent = -delta * sqrtterm / HBAR;
					exponent = exp(exponent);	//this is Tw, but want to include reflections, so Tw2 calculation follows
					double den = 1.0 + 0.5 * exponent + 0.0625 * exponent * exponent;
					wkb->integral = exponent / den;
					
					TargetState = FindELevel(wkb, mdl);
					if(TargetState == NULL)
						wkb->type = TUNNEL_NONE;
					
					return(TargetState);
				}
				else	//there can be a HUGE error here because the sampling size is just too small.  By doing linear bits
				{	//it may never actually see that it passes through the maximum where the Eg is. Therefore, do the above twice
					//but formatted towarsd the end.
					//don't want to miss it, so find it's location in both materials.

					delta = fabs(wkb->betweenpos - wkb->startpos);
					double distanceHalfPt=delta+1.0;	//the potential is maximized when E = (Ec + Ev)/2.
					//+1 is just to ensure the if statement doesn't fire if m=0.

					m = (prevPt->Psi - curPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					//note, this means all prevPt's get the -m*dx and curPt's get the +m*dx
					if(m != 0.0)
						distanceHalfPt = 0.5* prevPt->eg / m;	

					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm = 1.0;

					if(distanceHalfPt < delta)	//split this portion into two areas to sum over:
					{
						potential2 = wkb->EvPrev - m*(distanceHalfPt);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += distanceHalfPt * potential2;

						potential1 = potential2;	//setup for moving forward in this front half
						potential2 = wkb->EvPrev - m*(delta);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += (delta - distanceHalfPt) * (potential1 + potential2);	//dx, area of new trapezoid

					}
					else	//it doesn't reach the maximum contribution in the previous point, so just do this one normal
					{
						potential1 = 0.0;	//first one must always be 0.
						potential2 = wkb->EvPrev - m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += delta * potential2;
					}

					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;	
					else
						efm = 1.0;
					delta = fabs(wkb->endpos - wkb->betweenpos);

					//now check contributions from the other side
					if(m!=0)	//avoid divide by 0
						distanceHalfPt = 0.5* curPt->eg / m;	
					else
						distanceHalfPt = delta + 1.0;

					//it must move at least HALF an Eg to go through the maximum point in the space given, delta
					if(distanceHalfPt < delta)	//does it encounter the halfway point in the region of interest here?
					{
						
						potential1 = wkb->EvCur + m*wkb->delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

						//potential2 = energy;	//the theoretical max
						//potential2 = energy - potential2;
						//potential2 = 4.0 * potential2 * potential2;
						//potential2 = curPt->eg * curPt->eg - potential2;
						//if(potential2 < 0.0)	//fail safe in case of rounding errors
						//	potential2 = 0.0;
						//potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(efm * curPt->eg);

						wkb->integral += (wkb->delta - distanceHalfPt) * (potential1 + potential2);	//the dx should be within the proper range
						
						delta = delta - (wkb->delta - distanceHalfPt);	//change new delta to be what's left of the distance
						potential1 = 0.0;	//technically, pot1=pot2, pot2=0
					}
					else	//it does not move through the highest area point, so just do simple ones. Leave delta/efm as is
					{	//treat as linear/single triangle
						potential2 = 0.0;
						potential1 = wkb->EvCur + m*wkb->delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);
					}	//end it does not go through the maximum value of the function, so treat as simple function.
				}	//end different materials
			}	//end starts and ends on first two points
			else if((wkb->prevpos < wkb->startpos && (wkb->dir == PX || wkb->dir == PY)) ||	//is the previous point to the left of the start position and moving right?
			   (wkb->prevpos > wkb->startpos && (wkb->dir == NX || wkb->dir == NY)) )	//" " " " " " right " " " " " " left?
			{
				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->curpos - wkb->startpos);
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm = 1.0;
					potential1 = 0.0;	//At the start, E-V = Eg/2, so the sqrt term = 0
					potential2 = (wkb->EcCur + wkb->EvCur)*0.5;	//middle of band gap
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

					//sqrt{m* / (2 Eg) * (Eg^2 - 4(E-V)^2) }, ignore m0 and divide by 2. If Eg=0 as in a metal... there's no barrier to tunnel into!
				}
				else	//this is starting at a transition between different materials.
				{
					m = (prevPt->Psi - curPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					//only concerned that slope is positive in the direction of movement

					//must find where exactly the transition starts.
					if((wkb->startpos < wkb->betweenpos && (wkb->dir == PX || wkb->dir == PY)) ||
					   (wkb->startpos > wkb->betweenpos && (wkb->dir == NX || wkb->dir == NY)))	//then it starts before reaching the interface
					{	//must do multiple tunneling bits
						delta = fabs(wkb->betweenpos - wkb->startpos);
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						//I want the slope of the line... within a point, it is the same material, so just look at psi to first approx
						//this is CB start, so it should likely have a positive slope
							
						potential1 = 0.0;	//first one must always be 0.
						potential2 = wkb->EvPrev - m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

						wkb->integral += delta * potential2;	//once again, no 0.5 as tha will be handled later.

						//now prepwork for the integral occurring after the interface
						delta = wkb->delta;	//occurs through the entire interface
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						//m is still the same.
						potential1 = wkb->EvCur + m*delta;	//pot1 is now Ev at interface from the right
						potential1 += curPt->eg * 0.5;	//shift potential up to halfway up the band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

							//now do all the work again for potential 2. happy happy joy joy.
						potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = curPt->eg * curPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(potential2);

					}
					else	//it starts at the interface or in the second point. Regardless, only must concern with one point
					{
						delta = fabs(wkb->curpos - wkb->startpos);
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = wkb->EvCur + m*delta;	//set potential to VB and moving down properly to the starting point
						potential1 += curPt->eg * 0.5;	//bump it up to halfway between the CB and VB
						potential1 = energy - potential1;	//start calculation of square root term
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)	//be rounding error safe when close to 0
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

							//now do all the work again for potential 2. happy happy joy joy.
						potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = curPt->eg * curPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / curPt->eg * potential2;
						potential2 = sqrt(potential2);

					}	//end starting within second point	
				}	//end starting transition between different materials
			}	//end it being the start point
			else if(energy >= wkb->EcCur)
			{	//then it is exiting the tunneling process
				Finished = true;
				if(curPt->MatProps == prevPt->MatProps)
				{
					//this is similar to the start... except the second potential ends up being 0.
					delta = fabs(wkb->prevpos - wkb->endpos);
					potential2 = 0.0;
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);
				}
				else	//ending in a different material
				{
					m = (prevPt->Psi - curPt->Psi) / (wkb->delta + wkb->deltaPrev);	//should be positive whether going left or right
					//only concerned that slope is positive in the direction of movement

					//must find where exactly the transition is.
					if((wkb->endpos > wkb->betweenpos && (wkb->dir == PX || wkb->dir == PY)) ||
					   (wkb->endpos < wkb->betweenpos && (wkb->dir == NX || wkb->dir == NY)))
					{	//the tunneling ends after going through the interface.
						//first, get the end part of the integral in curPt.
						delta = fabs(wkb->endpos - wkb->betweenpos);
						if(curPt->MatProps)
							efm = curPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = wkb->EvCur + m * delta;
						potential1 += curPt->eg * 0.5;
						potential1 = energy - potential1;	//start calculation of square root term
						potential1 = 4.0 * potential1 * potential1;
						potential1 = curPt->eg * curPt->eg - potential1;
						if(potential1 < 0.0)	//be rounding error safe when close to 0
							potential1 = 0.0;
						potential1 = efm * potential1 / curPt->eg;
						potential1 = sqrt(potential1);

						wkb->integral += delta * potential1;	//potential2 would be 0.

						//now get the previous point values
						delta = wkb->deltaPrev;
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;	
						else
							efm = 1.0;
						potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential2 * potential1;
						potential1 = prevPt->eg * prevPt->eg - potential1;
						if(potential1 < 0.0)	//fail safe in case of rounding errors
							potential1 = 0.0;
						potential1 = efm / prevPt->eg * potential1;
						potential1 = sqrt(potential1);

						potential2 = wkb->EvPrev - m*(wkb->deltaPrev);	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);

					}
					else	//it ends entirely in the previous point before the interface (or at the interface)
					{
						delta = fabs(wkb->endpos - wkb->prevpos);
						if(prevPt->MatProps)
							efm = prevPt->MatProps->EFMeCond;
						else
							efm = 1.0;
						potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
						potential1 = energy - potential1;
						potential1 = 4.0 * potential2 * potential1;
						potential1 = prevPt->eg * prevPt->eg - potential1;
						if(potential1 < 0.0)	//fail safe in case of rounding errors
							potential1 = 0.0;
						potential1 = efm / prevPt->eg * potential1;
						potential1 = sqrt(potential1);

						potential2 = wkb->EvPrev - m*delta;	//potential 2 is now Ev at the interface from the left
						potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
						potential2 = energy - potential2;
						potential2 = 4.0 * potential2 * potential2;
						potential2 = prevPt->eg * prevPt->eg - potential2;
						if(potential2 < 0.0)	//fail safe in case of rounding errors. This will likely be ~zero...
							potential2 = 0.0;
						potential2 = efm / prevPt->eg * potential2;
						potential2 = sqrt(potential2);
					}

				}
			}	//end if it is done tunneling.
			else	//it continues to tunnel through the band as normal
			{
				if(curPt->MatProps == prevPt->MatProps)
				{
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);

					potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

					delta = wkb->delta + wkb->deltaPrev;

				}
				else
				{
					m = (prevPt->Psi - curPt->Psi) / (wkb->delta + wkb->deltaPrev);	
						//subtract m when working from prev, add m when working from cur
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm = 1.0;
					delta = wkb->deltaPrev;
					potential1 = (wkb->EcPrev + wkb->EvPrev)*0.5;	//middle of band gap
					potential1 = energy - potential1;
					potential1 = 4.0 * potential2 * potential1;
					potential1 = prevPt->eg * prevPt->eg - potential1;
					if(potential1 < 0.0)	//fail safe in case of rounding errors
						potential1 = 0.0;
					potential1 = efm / prevPt->eg * potential1;
					potential1 = sqrt(potential1);

					potential2 = wkb->EvPrev - m*delta;	//potential 2 is now Ev at the interface from the left
					potential2 += prevPt->eg * 0.5;	//and now it is between the band gap
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = prevPt->eg * prevPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors. This will likely be ~zero...
						potential2 = 0.0;
					potential2 = efm / prevPt->eg * potential2;
					potential2 = sqrt(potential2);

					wkb->integral += delta * (potential1 + potential2);
									
					//now setup the same on the other side
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;	
					else
						efm = 1.0;
					delta = wkb->delta;
					potential1 = wkb->EvCur + m * delta;
					potential1 += curPt->eg * 0.5;
					potential1 = energy - potential1;	//start calculation of square root term
					potential1 = 4.0 * potential1 * potential1;
					potential1 = curPt->eg * curPt->eg - potential1;
					if(potential1 < 0.0)	//be rounding error safe when close to 0
						potential1 = 0.0;
					potential1 = efm * potential1 / curPt->eg;
					potential1 = sqrt(potential1);

					potential2 = 0.5 *(wkb->EcCur + wkb->EvCur);
					potential2 = energy - potential2;
					potential2 = 4.0 * potential2 * potential2;
					potential2 = curPt->eg * curPt->eg - potential2;
					if(potential2 < 0.0)	//fail safe in case of rounding errors
						potential2 = 0.0;
					potential2 = efm / curPt->eg * potential2;
					potential2 = sqrt(potential2);

	
				}	//end midtunneling change of material
			}	//end midtunneling

			wkb->integral += delta * (potential1 + potential2);	//missing a 1/2 for the trapezoid. Will do all correction factors later...
			//This lack of 1/2 is the same as multiplying by 2, which is applied after the integral

		}while(!Finished);

		TargetState = FindELevel(wkb, mdl);

	}	//end started from valence band

	if(TargetState == NULL)
		wkb->type = TUNNEL_NONE;

	//now do corrections to the integral
	/*
	Tw = exp(-2/h_bar Integral [ dx sqrt{m* / (2 Eg) * (Eg^2 - 4(E-V)^2) } ]

	The x2 out front may be neglected because multiplied by two when getting the area of each slice
*	Need to divide by h_bar
*	multuply by sqrt(m0)
*	divide by sqrt(2) (x 0.5^0.5)
*	multiply by 0.01 to get cm->m from the dx
*	divide by q^0.5 to put Q / Q^0.5 from eV back in place
	*/
	double temp= MELECTRON * 0.00005 * QI;
	temp=sqrt(temp);
	temp = -temp/HBAR;	//temp is now the prefactor and unit corrections to the integral
	temp = exp(wkb->integral * temp);
	//now include the reflections (Tw -> Tw2)
	double den = 1.0 + 0.5 * temp + 0.0625 * temp * temp;
	wkb->integral = temp / den;

	return(TargetState);
}

int TunnelWKBSharp(WKBData* wkb)
{

	if(wkb->SharpEdge == false || wkb->EnergyBarrier <= wkb->relEnergy || wkb->EnergyBarrier <= 0.0)
	{	//use Tw2 instead of T1, T2. This model will fail if there is no barrier or if
		//the energy is close to the barrier.
		double temp = wkb->integral;
		double den = 1.0 + 0.5 * temp + 0.0625 * temp * temp;
		wkb->integral = temp / den;
		return(0);
	}

	//need to calculate T1 and T2
	//T1 = 4 Tw E sqrt(Eb/E - 1) / Eb	2 divides, 1 subtract, 3 multiply
	//T1 = 4 Tw sqrt(Eb*E - E^2) / Eb	1 divide, 1 subtract, 4 multiply	

	//T1 = 4 Tw sqrt(E(Eb - E)) / Eb	1 divide, 1 subtract, 3 multiply		WINNER

	//T1 = 4 Tw sqrt(Eb/E - 1) / (Eb/E)	2 divides, 1 subtract, 2 multiply
	//T1 = 4 Tw (E/Eb) sqrt(Eb/E - 1)
	//T1 = 4 Tw sqrt[E/Eb - (E/Eb)^2 ]	1 divide, 1 subtract, 3 multiply
	//T2 = T1 / (1 + 0.5 T1 + .25 Tw^2)
	double T1, T2, sqrtterm;
	double integral = wkb->integral;	//save on access via pointers

	sqrtterm = wkb->EnergyBarrier - wkb->relEnergy;
	sqrtterm = sqrtterm * wkb->relEnergy;
	sqrtterm = sqrt(sqrtterm);
	T1 = 4.0 * integral * sqrtterm / wkb->EnergyBarrier;
	T2 = 1.0 + 0.5 * T1 + 0.25 * integral * integral;
	wkb->integral = T1 / T2;

	if(wkb->integral > 1.0)
		wkb->integral = 1.0;

	if(wkb->integral < 0.0)
		wkb->integral = 0.0;

	return(0);
}

/*
For this set of tunneling, we know it is just CB->CB (e-) or VB->VB (h+), and there isn't any sharp edges to deal with.
For the most part, we should be able to integrate over a trapezoid between the two points UNLESS
1) Material changes.  Must split into two trapezoids.
2) Start or end points
*/
int TunnelWKB(Model& mdl, WKBData* wkb)
{
	Point* curPt = NULL;	//just some variables to make things a tad easier to handle.
	Point* prevPt = NULL;
	FirstInitIterTWKB(mdl, wkb);
	bool Finished = false;
	//avoid doing many if statements at the expense of more code...
	double delta = 0.0;
	double potential1 = 0.0;	//area of trapezoid height on start side
	double potential2 = 0.0;	//area of trapezoid height on end side
	double efm = 1.0;
	if(wkb->initBand == CONDUCTION)
	{
		do
		{
			InitIterTWKB(mdl, wkb);	//make sure it gets all the info in...
			if(wkb->curpoint == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			curPt = wkb->curpoint;
			prevPt = wkb->prevpoint;

			//first check if this is going to be a start point and adjustments need making
			if((wkb->prevpos < wkb->startpos && (wkb->dir == PX || wkb->dir == PY)) ||	//is the previous point to the left of the start position and moving right?
			   (wkb->prevpos > wkb->startpos && (wkb->dir == NX || wkb->dir == NY)) )	//" " " " " " right " " " " " " left?
			{
				if(curPt->MatProps == prevPt->MatProps)	//this should be simpler to integrate over
				{	//this will be a trapezoid with a height of zero on one side.
					delta = fabs(wkb->curpos - wkb->startpos);	//who knows which direction it is going.  This is likely faster than an if statement...
					potential1 = wkb->Energy;	//this will then cancel out the trapezoidal height subtraction
					potential2 = wkb->EcCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm = 1.0;
				}
				else	//these are different materials. rawr.
				{
					//time for some fancy business. First find the slope of the line.
					double difEc= wkb->EcCur - wkb->EcPrev;
					if(curPt->MatProps)
						difEc += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEc -= prevPt->MatProps->Phi;
					double m = difEc / (wkb->delta + wkb->deltaPrev);

					if(wkb->dir == PX || wkb->dir == PY)
					{
						if(wkb->startpos >= wkb->betweenpos)	//do everything as if the first point doesn't exist
						{
							delta = wkb->curpos - wkb->startpos;
							potential1 = wkb->EcCur - m * delta;
							potential2 = wkb->EcCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMeCond;
							else
								efm=1.0;
						}
						else	//it starts tunneling before the interface, so include the trapezoid before and after.
						{
							delta = wkb->startpos - wkb->prevpos;
							//for the initial integral sum. The width of the previous point - the distance to the start point
							//this is needed for getting the first potential, though it's not the actual delta
							potential1 = wkb->EcPrev + m * delta;
							potential2 = wkb->EcPrev + m * wkb->deltaPrev;
							delta = wkb->betweenpos - wkb->startpos;	//the actual width for the integral
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm = 1.0;

							wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * delta;

							//now setup the continuation of the integral

							delta = wkb->delta;
							potential1 = wkb->EcCur - m * delta;
							potential2 = wkb->EcCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMeCond;
							else
								efm=1.0;
						}
					}
					else if(wkb->dir == NX || wkb->dir == NY)
					{
						if(wkb->startpos <= wkb->betweenpos)	//do everything as if the first point doesn't exist
						{
							delta = wkb->startpos - wkb->curpos;
							potential1 = wkb->EcCur - m * delta;
							potential2 = wkb->EcCur;
						}
						else	//it starts tunneling before the interface, so include the trapezoid before and after.
						{
							delta = wkb->prevpos - wkb->startpos;
							//for the initial integral sum. The width of the previous point - the distance to the start point
							//this is needed for getting the first potential, though it's not the actual delta
							potential1 = wkb->EcPrev + m * delta;
							potential2 = wkb->EcPrev + m * wkb->deltaPrev;
							delta = wkb->startpos - wkb->betweenpos;	//the actual width for the integral
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm = 1.0;

							wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * delta;

							//now setup the continuation of the integral

							delta = wkb->delta;
							potential1 = wkb->EcCur - m * delta;
							potential2 = wkb->EcCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMeCond;
							else
								efm=1.0;
						}	//end tunneling included before interface
					}	//end looking at tunneling in the negative direction
				}	//end tunneling for different materials
			}	//end the initial tunneling point code
			else if(wkb->EcCur <= wkb->Energy)	//then it's done tunneling!
			{
				Finished = true;
				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->endpos - wkb->prevpos);
					potential1 = wkb->EcPrev;
					potential2 = wkb->Energy;
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm=1.0;
				}
				else	//different materials...
				{
					double difEc= wkb->EcCur - wkb->EcPrev;
					if(curPt->MatProps)
						difEc += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEc -= prevPt->MatProps->Phi;
					double m = difEc / (wkb->delta + wkb->deltaPrev);	//this should likely be negative unless e- affinities crazy

					if(wkb->dir == PX || wkb->dir == PY)
					{
						if(wkb->endpos <= wkb->betweenpos)	//only the first point
						{
							delta = wkb->endpos - wkb->prevpos;
							potential1 = wkb->EcPrev;
							potential2 = wkb->EcPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm = 1.0;
						}	//it also needs to include the current point in the calculations
						else
						{
							//get the complicated one first. Note: m is still likely negative
							potential1 = wkb->EcCur - m * wkb->delta;
							delta = wkb->curpos - wkb->endpos;
							potential2 = wkb->EcCur - m * delta;
							delta = wkb->endpos - wkb->betweenpos;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMeCond;
							else
								efm=1.0;
							
							wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * delta;

							//now setup for the usual integral sum done
							delta = wkb->deltaPrev;
							potential1 = wkb->EcPrev;
							potential2 = wkb->EcPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm=1.0;

						}	//end tunneling in positive direction and does starts before interface
					}	//end tunneling in positive direction
					else if(wkb->dir == NX || wkb->dir == NY)
					{
						if(wkb->endpos >= wkb->betweenpos)	//only the first point
						{
							delta = wkb->prevpos - wkb->endpos;
							potential1 = wkb->EcPrev;
							potential2 = wkb->EcPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm=1.0;
						}	//it also needs to include the current point in the calculations
						else
						{
							//get the complicated one first. Note: m is still likely negative
							potential1 = wkb->EcCur - m * wkb->delta;
							delta = wkb->endpos - wkb->curpos;
							potential2 = wkb->EcCur - m * delta;
							delta = wkb->betweenpos - wkb->endpos;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMeCond;
							else
								efm=1.0;
							
							wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * delta;

							//now setup for the usual integral sum done
							delta = wkb->deltaPrev;
							potential1 = wkb->EcPrev;
							potential2 = wkb->EcPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMeCond;
							else
								efm=1.0;

						}	//end tunneling in positive direction and does starts before interface
					}
				}	//end different materials
			}	//end it's on the last point to tunnel
			else	//all the tunneling inbetween
			{
				if(curPt->MatProps == prevPt->MatProps)	//the simplest case!
				{
					delta = wkb->delta;
					potential1 = wkb->EcPrev;
					potential2 = wkb->EcCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm=1.0;
				}	//look how beautiful and simple that was...
				else	//now the materials are different for tunneling over a material change that it never stops tunneling
				{
					//need to split this one into two integrals as well...
					double difEc = wkb->EcCur - wkb->EcPrev;
					if(curPt->MatProps)
						difEc += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEc -= prevPt->MatProps->Phi;
					double m = difEc / (wkb->delta + wkb->deltaPrev);

					//do the first integral portion
					potential1 = wkb->EcPrev;
					potential2 = wkb->EcPrev + m * wkb->deltaPrev;
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMeCond;
					else
						efm=1.0;
					wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * wkb->deltaPrev;
					
					//and the second integral portion
					potential1 = wkb->EcCur - m * wkb->delta;
					potential2 = wkb->EcCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMeCond;
					else
						efm=1.0;
					delta = wkb->delta;

				}	//end different materials
			}	//end normal tunneling code


			wkb->integral += sqrt( efm * (potential1 + potential2 - 2.0 * wkb->Energy) ) * delta;

		}while(!Finished);	//end loop which evalulates the wkb integral
	}
	else	//basically same as above (except VB), but removed an if statement done many times.
	{
		do
		{
			InitIterTWKB(mdl, wkb);	//make sure it gets all the info in...
			if(wkb->curpoint == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			curPt = wkb->curpoint;
			prevPt = wkb->prevpoint;

			//first check if this is going to be a start point and adjustments need making
			if((wkb->prevpos < wkb->startpos && (wkb->dir == PX || wkb->dir == PY)) ||	//is the previous point to the left of the start position and moving right?
			   (wkb->prevpos > wkb->startpos && (wkb->dir == NX || wkb->dir == NY)) )	//" " " " " " right " " " " " " left?
			{
				if(curPt->MatProps == prevPt->MatProps)	//this should be simpler to integrate over
				{	//this will be a trapezoid with a height of zero on one side.
					delta = fabs(wkb->curpos - wkb->startpos);	//who knows which direction it is going.  This is likely faster than an if statement...
					potential1 = wkb->Energy;	//this will then cancel out the trapezoidal height subtraction
					potential2 = wkb->EvCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMhCond;
					else
						efm=1.0;
				}
				else	//these are different materials. rawr.
				{
					//time for some fancy business. First find the slope of the line.
					double difEv= (wkb->EvCur + curPt->eg) - (wkb->EvPrev + prevPt->eg);
					if(curPt->MatProps)
						difEv += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEv -= prevPt->MatProps->Phi;
					//essentially, difference in Psi between the two points.
					double m = difEv / (wkb->delta + wkb->deltaPrev);	//the current point should be less, making this negative.

					if(wkb->dir == PX || wkb->dir == PY)
					{
						if(wkb->startpos >= wkb->betweenpos)	//do everything as if the first point doesn't exist
						{
							delta = wkb->curpos - wkb->startpos;
							potential1 = wkb->EvCur - m * delta;
							potential2 = wkb->EvCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMhCond;
							else
								efm=1.0;
						}
						else	//it starts tunneling before the interface, so include the trapezoid before and after.
						{
							delta = wkb->startpos - wkb->prevpos;
							//for the initial integral sum. The width of the previous point - the distance to the start point
							//this is needed for getting the first potential, though it's not the actual delta
							potential1 = wkb->EvPrev + m * delta;
							potential2 = wkb->EvPrev + m * wkb->deltaPrev;
							delta = wkb->betweenpos - wkb->startpos;	//the actual width for the integral
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;

							wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * delta;

							//now setup the continuation of the integral

							delta = wkb->delta;
							potential1 = wkb->EvCur - m * delta;
							potential2 = wkb->EvCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMhCond;
							else
								efm=1.0;
						}
					}
					else if(wkb->dir == NX || wkb->dir == NY)
					{
						if(wkb->startpos <= wkb->betweenpos)	//do everything as if the first point doesn't exist
						{
							delta = wkb->startpos - wkb->curpos;
							potential1 = wkb->EvCur - m * delta;
							potential2 = wkb->EvCur;
						}
						else	//it starts tunneling before the interface, so include the trapezoid before and after.
						{
							delta = wkb->prevpos - wkb->startpos;
							//for the initial integral sum. The width of the previous point - the distance to the start point
							//this is needed for getting the first potential, though it's not the actual delta
							potential1 = wkb->EvPrev + m * delta;
							potential2 = wkb->EvPrev + m * wkb->deltaPrev;
							delta = wkb->startpos - wkb->betweenpos;	//the actual width for the integral
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;

							wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * delta;

							//now setup the continuation of the integral

							delta = wkb->delta;
							potential1 = wkb->EvCur - m * delta;
							potential2 = wkb->EvCur;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMhCond;
							else
								efm=1.0;
						}	//end tunneling included before interface
					}	//end looking at tunneling in the negative direction
				}	//end tunneling for different materials
			}	//end the initial tunneling point code
			else if(wkb->EvCur >= wkb->Energy)	//then it's done tunneling!
			{
				Finished = true;
				if(curPt->MatProps == prevPt->MatProps)
				{
					delta = fabs(wkb->endpos - wkb->prevpos);
					potential1 = wkb->EvPrev;
					potential2 = wkb->Energy;
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMhCond;
					else
						efm=1.0;
				}
				else	//different materials...
				{
					double difEv = (wkb->EvCur + curPt->eg) - (wkb->EvPrev + prevPt->eg);
					if(curPt->MatProps)
						difEv += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEv -= prevPt->MatProps->Phi;
						
					double m = difEv / (wkb->delta + wkb->deltaPrev);	//this should be positive...

					if(wkb->dir == PX || wkb->dir == PY)
					{
						if(wkb->endpos <= wkb->betweenpos)	//only the first point
						{
							delta = wkb->endpos - wkb->prevpos;
							potential1 = wkb->EvPrev;
							potential2 = wkb->EvPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;
						}	//it also needs to include the current point in the calculations
						else
						{
							//get the complicated one first. Note: m is still likely positive
							delta = wkb->curpos - wkb->endpos;
							potential2 = wkb->EvCur - m * delta;
							potential1 = wkb->EvCur - m * wkb->delta;
							delta = wkb->endpos - wkb->betweenpos;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMhCond;
							else
								efm=1.0;
							
							wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * delta;

							//now setup for the usual integral sum done
							delta = wkb->deltaPrev;
							potential1 = wkb->EvPrev;
							potential2 = wkb->EvPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;

						}	//end tunneling in positive direction and does starts before interface
					}	//end tunneling in positive direction
					else if(wkb->dir == NX || wkb->dir == NY)
					{
						if(wkb->endpos >= wkb->betweenpos)	//only the first point
						{
							delta = wkb->prevpos - wkb->endpos;
							potential1 = wkb->EvPrev;
							potential2 = wkb->EvPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;
						}	//it also needs to include the current point in the calculations
						else
						{
							//get the complicated one first. Note: m is still likely negative
							delta = wkb->endpos - wkb->curpos;
							potential2 = wkb->EvCur - m * delta;
							potential1 = wkb->EvCur - m * wkb->delta;
							delta = wkb->betweenpos - wkb->endpos;
							if(curPt->MatProps)
								efm = curPt->MatProps->EFMhCond;
							else
								efm=1.0;
							
							wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * delta;

							//now setup for the usual integral sum done
							delta = wkb->deltaPrev;
							potential1 = wkb->EvPrev;
							potential2 = wkb->EvPrev + m * delta;
							if(prevPt->MatProps)
								efm = prevPt->MatProps->EFMhCond;
							else
								efm=1.0;

						}	//end tunneling in positive direction and does starts before interface
					}
				}	//end different materials
			}	//end it's on the last point to tunnel
			else	//all the tunneling inbetween
			{
				if(curPt->MatProps == prevPt->MatProps)	//the simplest case!
				{
					delta = wkb->delta;
					potential1 = wkb->EvPrev;
					potential2 = wkb->EvCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMhCond;
					else
						efm=1.0;
				}	//look how beautiful and simple that was...
				else	//now the materials are different for tunneling over a material change that it never stops tunneling
				{
					//need to split this one into two integrals as well...
					double difEv= (wkb->EvCur + curPt->eg) - (wkb->EvPrev + prevPt->eg);
					if(curPt->MatProps)
						difEv += curPt->MatProps->Phi;
					if(prevPt->MatProps)
						difEv -= prevPt->MatProps->Phi;
					double m = difEv / (wkb->delta + wkb->deltaPrev);	//could go either way, but we'll pretend positive for the sake of thinking things through
					//this means then that EvCur is greater or less energetic for holes
					//do the first integral portion
					potential1 = wkb->EvPrev;
					potential2 = wkb->EvPrev + m * wkb->deltaPrev;
					if(prevPt->MatProps)
						efm = prevPt->MatProps->EFMhCond;
					else
						efm=1.0;
					wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * wkb->deltaPrev;
					
					//and the second integral portion
					potential1 = wkb->EvCur - m * wkb->delta;
					potential2 = wkb->EvCur;
					if(curPt->MatProps)
						efm = curPt->MatProps->EFMhCond;
					else
						efm=1.0;
					delta = wkb->delta;

				}	//end different materials
			}	//end normal tunneling code


			wkb->integral += sqrt( efm * (2.0 * wkb->Energy - potential1 - potential2) ) * delta;	//flip energies because we're working with holes now...

		}while(!Finished);	//end loop which evalulates the wkb integral
	}
	
	
	/*
	now adjust the integral appropriately for all the constants which were taken out
	Tw = exp(-2/h_bar Integral [dx sqrt(2m* (V - E)) ]
	
	integral = SUM(m* * (V - E) * 2)
	the * 2 comes from doing the trapezoids, and NOT doing the proper area by multpiplying by 1/2.
	this leaves multiplying by -2 m0 / h_bar, and factors of q?

	J-1 S-1 KG0.5 J0.5 m1 = J-0.5 S-1 kg0.5 m1
	J1 = kg m2 s-2
	-0.5kg m-1 s1 s-1 kg0.5 m1 = 0

	1 ev = 1 J/Q
	my units...
	eV-1 s-1 cm eV0.5 kg0.5	=	eV-0.5 s-1 cm kg0.5	=	Q0.5 J-0.5 s-1 cm kg0.5 =	Q0.5 kg-0.5 m-1 s1 s-1 cm kg0.5
	= Q0.5 m-1 cm = q^0.5 cm/m
	
	Need to multiply by the following to adjust for constants:
	q^-0.5 * m / cm =	q^-0.5 * 0.01

	and then -2 sqrt(m0)/h_bar for the rest of the exponential in the integral

	*/
	double temp = -0.02 * sqrt(MELECTRON * QI);	//m / q. the mass used in all the above was relative.
	temp = temp * wkb->integral / HBAR;	//note, h_Bar is eV s
	wkb->integral = exp(temp);

	
	//Tw2 = Tw / (1 + 0.25 Tw)^2	This includes reflections, but does not have the sharp edge correction, but there are none here...
	//temp = Tw
	if(wkb->type == TUNNEL_WKB)	//do correction as final answer if Tw isn't used in corrections pertaining to particular scenarios.
	{
		temp = wkb->integral;
		double den = 1.0 + 0.5 * temp + 0.0625 * temp * temp;
		wkb->integral = temp / den;
	}

	if(wkb->integral > 1.0)
		wkb->integral = 1.0;

	if(wkb->integral < 0.0)
		wkb->integral = 0.0;

	return(0);
}

//initializes all the data for the start of the integration of WKB
int FirstInitIterTWKB(Model& mdl, WKBData* wkb)
{
	Point* curPt = wkb->curpoint;	//tunneling main intro sets curpoint to be the start point from the get go
	wkb->EcCur = curPt->cb.bandmin;
	wkb->EvCur = curPt->vb.bandmin;
	wkb->integral = 0.0;

	switch(wkb->dir)
	{
		case NX:
			wkb->delta = curPt->position.x - curPt->mxmy.x;
			wkb->curpos = curPt->position.x;
			break;
		case PX:
			wkb->delta = curPt->position.y - curPt->mxmy.y;
			wkb->curpos = curPt->position.x;
			break;
		case NY:
			wkb->delta = curPt->position.y - curPt->mxmy.y;
			wkb->curpos = curPt->position.y;
			break;
		case PY:
			wkb->delta = curPt->pxpy.y - curPt->position.y;
			wkb->curpos = curPt->position.y;
			break;
		default:
			wkb->delta = 0.0;
			break;
	}
	return(0);
}

//initializes all the data for each iteration of the integration of WKB.
int InitIterTWKB(Model& mdl, WKBData* wkb)
{
	//load all the upcoming data that may be needed. Really only need previous data.
	wkb->prevpoint = wkb->curpoint;
	wkb->deltaPrev = wkb->delta;
	wkb->EcPrev = wkb->EcCur;
	wkb->EvPrev = wkb->EvCur;
	wkb->prevpos = wkb->curpos;

	Point* curPt = NULL;
	switch(wkb->dir)
	{
		case NX:
			wkb->curpoint = wkb->curpoint->adj.adjMX;
			curPt = wkb->curpoint;	//note, this should not ever fail because the start/end positions were determined and it passed then

			wkb->delta = curPt->position.x - curPt->mxmy.x;
			wkb->EcCur = curPt->cb.bandmin;
			wkb->EvCur = curPt->vb.bandmin;
			wkb->curpos = curPt->position.x;
			wkb->betweenpos = curPt->pxpy.x;	//the previous point is in the positive direction.
			break;
		case PX:
			wkb->curpoint = wkb->curpoint->adj.adjPX;
			curPt = wkb->curpoint;

			wkb->delta = curPt->pxpy.x - curPt->position.x;
			wkb->EcCur = curPt->cb.bandmin;
			wkb->EvCur = curPt->vb.bandmin;
			wkb->curpos = curPt->position.x;
			wkb->betweenpos = curPt->mxmy.x;
			break;
		case NY:
			wkb->curpoint = wkb->curpoint->adj.adjMY;
			curPt = wkb->curpoint;

			wkb->delta = curPt->position.y - curPt->mxmy.y;
			wkb->EcCur = curPt->cb.bandmin;
			wkb->EvCur = curPt->vb.bandmin;
			wkb->curpos = curPt->position.y;
			wkb->betweenpos = curPt->pxpy.y;
			break;
		case PY:
			wkb->curpoint = wkb->curpoint->adj.adjPY;
			curPt = wkb->curpoint;

			wkb->delta = curPt->pxpy.y - curPt->position.y;
			wkb->EcCur = curPt->cb.bandmin;
			wkb->EvCur = curPt->vb.bandmin;
			wkb->curpos = curPt->position.y;
			wkb->betweenpos = curPt->mxmy.y;
			break;
		default:
			break;
	}
	return(0);
}

/*
Potential model bug.  It is possible that this may find a sharp barrier height that goes higher than the actual barrier
in the case of for example, a tunneling diode.  The slope is so steep that the valence band is reached before the "top" of the
barrier is reached.  Say it is reverse biased 5V...?
*/
int SharpBarrierSearchStartCB(WKBData* wkb, Model& mdl, double EnergyStart, Point* StartPoint)
{
	wkb->SharpEdge = true;	//make sure the flag is set as true
	double tempslope = SHARP_BARRIER;	//enforce it to go through the loop the first time
	Point* nextgridp;
	Point* curgp = StartPoint;
	Point* destpt = StartPoint;
	switch(wkb->dir)
	{
		case NX:
			nextgridp = curgp->adj.adjMX;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				destpt = curgp;
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(curgp->position.x - nextgridp->position.x);	//moving negatively, want positive slope moving in neg X direction
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMX;
			}
			break;
		case PX:
			nextgridp = curgp->adj.adjPX;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				destpt = curgp;
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(nextgridp->position.x - curgp->position.x);
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPX;
			}
			break;

		case NY:
			nextgridp = curgp->adj.adjMY;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				destpt = curgp;
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(curgp->position.y - nextgridp->position.y);	//moving negatively, want positive slope moving in neg X direction
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMY;
			}
			break;
		case PY:
			nextgridp = curgp->adj.adjPY;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				destpt = curgp;
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(nextgridp->position.y - curgp->position.y);
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPY;
			}
			break;
	}
	
	double temp = destpt->cb.bandmin - EnergyStart;	//just a temp variable to compare magnitudes. 
	if(temp > wkb->EnergyBarrier)	//this barrier is larger than any previously made barrier
	{
		wkb->EnergyBarrier = temp;
		wkb->relEnergy = wkb->Energy - EnergyStart;
	}
	return(0);
}

int SharpBarrierSearchEndCB(WKBData* wkb, Model& mdl, double EnergyEnd, Point* EndPoint)
{
	if(wkb->type == TUNNEL_INTERBAND)
	{
		wkb->SharpEdge = false;
		return(0);
	}
	double tempslope = SHARP_BARRIER;	//enforce it to go through the loop the first time
	Point* nextgridp;
	Point* curgp = EndPoint;
	wkb->SharpEdge = true;

	switch(wkb->dir)
	{
		case PX:
			nextgridp = curgp->adj.adjMX;	//we're searching backwards
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(curgp->position.x - nextgridp->position.x);	//moving negatively, want positive slope moving in neg X direction
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMX;
			}
			break;
		case NX:
			nextgridp = curgp->adj.adjPX;	//note not negative x, but moving backwards so positive x
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(nextgridp->position.x - curgp->position.x);
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPX;
			}
			break;

		case PY:
			nextgridp = curgp->adj.adjMY;	//tunnel in pos Y, but moving backwards so negative Y
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(curgp->position.y - nextgridp->position.y);	//moving negatively, want positive slope moving in neg X direction
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMY;
			}
			break;
		case NY:
			nextgridp = curgp->adj.adjPY;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, would also allow <= -, but the start is
			{	//always going to have an increasing potential
				tempslope = (nextgridp->cb.bandmin - curgp->cb.bandmin) /
							(nextgridp->position.y - curgp->position.y);
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPY;
			}
			break;
	}
	
	double temp = curgp->cb.bandmin - EnergyEnd;	//just a temp variable to compare magnitudes. 
	if(temp > wkb->EnergyBarrier)	//this barrier is larger than any previously made barrier
	{
		wkb->EnergyBarrier = temp;
		wkb->relEnergy = wkb->Energy - EnergyEnd;
	}
	return(0);
}

int SharpBarrierSearchStartVB(WKBData* wkb, Model& mdl, double EnergyStart, Point* StartPoint)
{
	wkb->SharpEdge = true;	//make sure the flag is set as true
	double tempslope = SHARP_BARRIER;	//enforce it to go through the loop the first time
	Point* nextgridp;
	Point* curgp = StartPoint;
	switch(wkb->dir)
	{
		case NX:
			nextgridp = curgp->adj.adjMX;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, these are all transformed to make the slope positive moving into the barrier
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// this should be negative
							(nextgridp->position.x - curgp->position.x);	// this also negative for appropriate tunneling barrier, making m positive
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMX;
			}
			break;
		case PX:
			nextgridp = curgp->adj.adjPX;
			while(nextgridp && tempslope >= SHARP_BARRIER)
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// (-)
							(curgp->position.x - nextgridp->position.x);	// (-) --> -/- = + for proper barrier
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPX;
			}
			break;

		case NY:
			nextgridp = curgp->adj.adjMY;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, these are all transformed to make the slope positive moving into the barrier
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// this should be negative
							(nextgridp->position.y - curgp->position.y);	// this also negative for appropriate tunneling barrier, making m positive
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMY;
			}
			break;
		case PY:
			nextgridp = curgp->adj.adjPY;
			while(nextgridp && tempslope >= SHARP_BARRIER)
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// (-)
							(curgp->position.y - nextgridp->position.y);	// (-) --> -/- = + for proper barrier
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPY;
			}
			break;
		default:	//curgp = start, so temp will just be for the one point
			break;
	}
	
	double temp = EnergyStart - curgp->vb.bandmin;	//just a temp variable to compare magnitudes. 
	if(temp > wkb->EnergyBarrier)	//this barrier is larger than any previously made barrier
	{
		wkb->EnergyBarrier = temp;
		wkb->relEnergy = wkb->Energy - EnergyStart;
	}
	return(0);
}

int SharpBarrierSearchEndVB(WKBData* wkb, Model& mdl, double EnergyEnd, Point* EndPoint)
{
	if(wkb->type == TUNNEL_INTERBAND)
	{
		wkb->SharpEdge = false;
		return(0);
	}
	wkb->SharpEdge = true;	//make sure the flag is set as true
	double tempslope = SHARP_BARRIER;	//enforce it to go through the loop the first time
	Point* nextgridp;
	Point* curgp = EndPoint;
	switch(wkb->dir)
	{
		case PX:
			nextgridp = curgp->adj.adjMX;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, these are all transformed to make the slope positive moving into the barrier
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// this should be negative
							(nextgridp->position.x - curgp->position.x);	// this also negative for appropriate tunneling barrier, making m positive
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMX;
			}
			break;
		case NX:
			nextgridp = curgp->adj.adjPX;
			while(nextgridp && tempslope >= SHARP_BARRIER)
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// (-)
							(curgp->position.x - nextgridp->position.x);	// (-) --> -/- = + for proper barrier
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPX;
			}
			break;

		case PY:
			nextgridp = curgp->adj.adjMY;
			while(nextgridp && tempslope >= SHARP_BARRIER)	//note, these are all transformed to make the slope positive moving into the barrier
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// this should be negative
							(nextgridp->position.y - curgp->position.y);	// this also negative for appropriate tunneling barrier, making m positive
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjMY;
			}
			break;
		case NY:
			nextgridp = curgp->adj.adjPY;
			while(nextgridp && tempslope >= SHARP_BARRIER)
			{	//always going to have an increasing potential
				tempslope = (nextgridp->vb.bandmin - curgp->vb.bandmin) /	// (-)
							(curgp->position.y - nextgridp->position.y);	// (-) --> -/- = + for proper barrier
					//setup for next iteration
				curgp = nextgridp;
				nextgridp = nextgridp->adj.adjPY;
			}
			break;
		default:	//curgp = start, so temp will just be for the one point
			break;
	}
	
	double temp = EnergyEnd - curgp->vb.bandmin;	//just a temp variable to compare magnitudes. 
	if(temp > wkb->EnergyBarrier)	//this barrier is larger than any previously made barrier
	{
		wkb->EnergyBarrier = temp;
		wkb->relEnergy = wkb->Energy - EnergyEnd;

	}
	return(0);
}

//determine the tunneling start position
int TunnelStart(Model& mdl, WKBData* wkb)
{
	Point* pt = wkb->curpoint;
	double energyUse = wkb->Energy;	//the energy that needs to be found.  Must search along the points.
	bool keepSearching = true;
	Point* startPt = wkb->curpoint;
	Point* searchPt = startPt;
	Point* adjPt = NULL;
	double EnergyAdj;
	double EnergyStart;
	double delta;
	double deltanext;
	double totaldistance;
	double difEc, difEv;
	double m;	//slope, y = mx + b... setup so b=0
	double deltapos;

	//first, find where it starts
	
	switch(wkb->dir)
	{
		case NX:
			searchPt = startPt->adj.adjMX;
			if(searchPt == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			adjPt = searchPt;
			delta = pt->position.x - pt->mxmy.x;
			deltanext = adjPt->pxpy.x - adjPt->position.x;
			totaldistance = delta + deltanext;
			if(wkb->initBand == CONDUCTION)
			{
				EnergyStart = pt->cb.bandmin;
				EnergyAdj = adjPt->cb.bandmin;
				//first, check to make sure this is actually going to tunnel...
				if(energyUse >= EnergyAdj && energyUse >= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				
				if(adjPt->MatProps == pt->MatProps) 
				{
					difEc = EnergyAdj - EnergyStart;	//this should be a positive number, delta E
					m = difEc / totaldistance;	//slope
					deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
					wkb->startpos = pt->position.x - deltapos;	//the starting position is... where it starts and then how far it must go
					if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
					}	//end test for a sharp barrier
				}
				else	//the materials are different
				{
					double difChi = 0.0;
					if(adjPt->MatProps)
						difChi += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difChi -= pt->MatProps->Phi;
					difEc = EnergyAdj - EnergyStart + difChi;	//this is with taking the difference in chi into account
					m = difEc / totaldistance;	//slope

					
					//note, in this case, the slope can be negative. Ec look like \|\ ish. This implies the barrier should not be the adjacent point, either
					if(m <= 0.0)
					{
						double bottomEnergyInterface = EnergyStart + delta*m;
						wkb->startpos = pt->mxmy.x;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = -difChi;
						wkb->relEnergy = energyUse - bottomEnergyInterface;
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
						if(deltapos > delta)	//then it has extended into the adjacent point
						{
							//need to see just how far beyond it goes beyond the point, or if it even does at all.
							//first, check to see if it goes beyond.
							double jumpend = EnergyAdj - m * deltanext;	//start from the other end to find the top of the barrier
							if(energyUse <= jumpend)
							{
								wkb->startpos = pt->mxmy.x;	//it starts directly at the interface
								wkb->SharpEdge =true;
								wkb->EnergyBarrier = EnergyAdj - EnergyStart;
								wkb->relEnergy = energyUse - EnergyStart;
							}
							else
							{
								deltapos = (EnergyAdj - energyUse) / m;	// +/+ = +
								wkb->startpos = adjPt->position.x + deltapos;	//dir is negative, so this will move it closer
							}
						}
						else	//then it starts in the same point
							wkb->startpos = pt->position.x - deltapos;	//the starting position is... where it starts and then how far it must go
						if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
						{
							SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
						}	//end test for a sharp barrier
					}	//end else (m>0)
				}	//end if different materials
				
			}	//end in the conduction band
			else	//working in the valence band...
			{
				EnergyStart = pt->vb.bandmin;	//looking for the point where the electron crosses into the valence band
				EnergyAdj = adjPt->vb.bandmin;
				if(energyUse <= EnergyAdj && energyUse <= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				
				if(adjPt->MatProps == pt->MatProps)
				{
					difEv = EnergyAdj - EnergyStart;	//it should be negative
					m = difEv / totaldistance;	//this should be negative as well.
					deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					wkb->startpos = pt->position.x - deltapos;
					if(m <= -SHARP_BARRIER)
					{
						SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}
				}
				else	//they are different materials
				{
					double difVB = adjPt->eg - pt->eg;
					if(adjPt->MatProps)
						difVB += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difVB -= pt->MatProps->Phi;
					difEv = EnergyAdj - EnergyStart + difVB;	//the difference in valence bands removing offsets from Eg & affinity
					m = difEv / totaldistance;	//this may be positive due to differences in Eg and electron affinity
					//when positive, it occurs at the interface
					
					if(m >= 0)
					{
						double VBBandMinInterface = EnergyStart + m*delta;
						wkb->startpos = pt->mxmy.x;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = difVB;
						wkb->relEnergy = VBBandMinInterface - energyUse;	//note, VBBand... > energyUse. Looking at hole energy, so positive is going down
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					
						if(deltapos > delta)
						{
							double jumpend = EnergyAdj - m * deltanext;
							if(energyUse >= jumpend)	//it moves over through the discontinuity in the band
							{
								wkb->startpos = pt->mxmy.x;	//it starts directly at the interface
								wkb->SharpEdge = true;
								//because this is electrons, they may only be going into the CB in this framework. The barrier is actually going
								//to be with the conduction band
								wkb->EnergyBarrier = EnergyStart - EnergyAdj;
								wkb->relEnergy = EnergyStart - energyUse;
							}
							else	//then it moves over in the adjacent point
							{	//find where it comes from in relation to the other point
								deltapos = (EnergyAdj - energyUse) / m;	//   -/- = +
								wkb->startpos = adjPt->position.x + deltapos;
							}
						}	//end tunneling starts beyond interface
						else	//tunneling starts before/at the interface
							wkb->startpos = pt->position.x - deltapos;	//going in the negative direction, so starting position
						
						if(m <= -SHARP_BARRIER)	//if the slope is large, treat it as a "vertical" wall ish, and find the end point of that
							SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}	//end else (m<0)					
				}	//end different materials
			}	//end tunneling starts form the valence band

			break;
		case PX:
			searchPt = startPt->adj.adjPX;
			if(searchPt == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			adjPt = searchPt;
			delta = pt->pxpy.x - pt->position.x;
			deltanext = adjPt->position.x - adjPt->mxmy.x;
			totaldistance = delta + deltanext;
			if(wkb->initBand == CONDUCTION)
			{
				EnergyStart = pt->cb.bandmin;
				EnergyAdj = adjPt->cb.bandmin;
				if(energyUse >= EnergyAdj && energyUse >= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				
				if(adjPt->MatProps == pt->MatProps) 
				{
					difEc = EnergyAdj - EnergyStart;	//this should be a positive number, delta E
					m = difEc / totaldistance;	//slope
					deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
					wkb->startpos = pt->position.x + deltapos;	//the starting position is... where it starts and then how far it must go
					if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
					}	//end test for a sharp barrier
				}
				else	//the materials are different
				{
					double difChi=0.0;
					if(adjPt->MatProps != NULL)
						difChi += adjPt->MatProps->Phi;
					if(pt->MatProps != NULL)
						difChi -= pt->MatProps->Phi;

					difEc = EnergyAdj - EnergyStart + difChi;	//this is with taking the difference in chi into account
					m = difEc / totaldistance;	//slope.  Note, this can now be negative. IT can slope downward in Psi, but the different e- affinities can make it go up
					
					if(m <= 0.0)
					{
						double bottomEnergyInterface = EnergyStart + delta*m;
						wkb->startpos = pt->pxpy.x;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = -difChi;
						wkb->relEnergy = energyUse - bottomEnergyInterface;
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
						if(deltapos > delta)	//then it has extended into the adjacent point
						{
							//need to see just how far beyond it goes beyond the point, or if it even does at all.
							//first, check to see if it goes beyond.
							double jumpend = EnergyAdj - m * deltanext;	//start from the other end to find the top of the barrier
							if(energyUse <= jumpend)
							{
								wkb->startpos = pt->pxpy.x;	//it starts directly at the interface
								wkb->SharpEdge =true;
								wkb->EnergyBarrier = EnergyAdj - EnergyStart;
								wkb->relEnergy = energyUse - EnergyStart;
							}
							else
							{
								deltapos = (EnergyAdj - energyUse) / m;	// +/+ = +
								wkb->startpos = adjPt->position.x - deltapos;	//dir is positive, so this will move it closer
							}

						}
						else	//then it starts in the same point
							wkb->startpos = pt->position.x + deltapos;	//the starting position is... where it starts and then how far it must go

						if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
						{
							SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
						}	//end test for a sharp barrier
					}	//end else (m > 0)
				}	//end if different materials
				
			}	//end in the conduction band
			else	//working in the valence band...
			{
				EnergyStart = pt->vb.bandmin;	//looking for the point where the electron crosses into the valence band
				EnergyAdj = adjPt->vb.bandmin;
				if(energyUse <= EnergyAdj && energyUse <= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				
				if(adjPt->MatProps == pt->MatProps)
				{
					difEv = EnergyAdj - EnergyStart;	//it should be negative
					m = difEv / totaldistance;	//this should be negative as well.
					deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					wkb->startpos = pt->position.x + deltapos;
					if(m <= -SHARP_BARRIER)
					{
						SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}
				}
				else	//they are different materials
				{
					double difVB = adjPt->eg - pt->eg;
					if(adjPt->MatProps)
						difVB += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difVB -= pt->MatProps->Phi;
					difEv = EnergyAdj - EnergyStart + difVB;	//the difference in valence bands removing offsets from Eg & affinity
					m = difEv / totaldistance;	//this should be negative as well, but can be positive if difVB is negative enough
					
					if(m >= 0)
					{
						double VBBandMinInterface = EnergyStart + m*delta;
						wkb->startpos = pt->pxpy.x;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = difVB;
						wkb->relEnergy = VBBandMinInterface - energyUse;	//note, VBBand... > energyUse. Looking at hole energy, so positive is going down
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					
						if(deltapos > delta)
						{
							double jumpend = EnergyAdj - m * deltanext;
							if(energyUse >= jumpend)	//it moves over through the discontinuity in the band
							{
								wkb->startpos = pt->pxpy.x;	//it starts directly at the interface
								wkb->SharpEdge = true;
								//because this is electrons, they may only be going into the CB in this framework. The barrier is actually going
								//to be with the conduction band
								wkb->EnergyBarrier = EnergyStart - EnergyAdj;
								wkb->relEnergy = EnergyStart - energyUse;
								
							}
							else	//then it moves over in the adjacent point
							{	//find where it comes from in relation to the other point
								deltapos = (EnergyAdj - energyUse) / m;	//   -/- = +
								wkb->startpos = adjPt->position.x - deltapos;
							}
						}	//end tunneling starts beyond interface
						else	//tunneling starts before/at the interface
							wkb->startpos = pt->position.x + deltapos;	//going in the positve direction, so starting position + change
						if(m <= -SHARP_BARRIER)	//if the slope is large, treat it as a "vertical" wall ish, and find the end point of that
							SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}	//end else (m<0)
				}	//end different materials

			}	//end tunneling starts form the valence band

			break;
		case NY:
			searchPt = startPt->adj.adjMY;
			if(searchPt == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			adjPt = searchPt;
			delta = pt->position.y - pt->mxmy.y;
			deltanext = adjPt->pxpy.y - adjPt->position.y;
			totaldistance = delta + deltanext;
			if(wkb->initBand == CONDUCTION)
			{
				EnergyStart = pt->cb.bandmin;
				EnergyAdj = adjPt->cb.bandmin;
				if(energyUse >= EnergyAdj && energyUse >= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				if(adjPt->MatProps == pt->MatProps) 
				{
					difEc = EnergyAdj - EnergyStart;	//this should be a positive number, delta E
					m = difEc / totaldistance;	//slope
					deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
					wkb->startpos = pt->position.x - deltapos;	//the starting position is... where it starts and then how far it must go
					if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
					}	//end test for a sharp barrier
				}
				else	//the materials are different
				{
					double difChi = 0.0;
					if(adjPt->MatProps)
						difChi += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difChi -= pt->MatProps->Phi;
					difEc = EnergyAdj - EnergyStart + difChi;	//this is with taking the difference in chi into account
					m = difEc / totaldistance;	//slope
					
					if(m <= 0.0)
					{
						double bottomEnergyInterface = EnergyStart + delta*m;
						wkb->startpos = pt->mxmy.y;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = -difChi;
						wkb->relEnergy = energyUse - bottomEnergyInterface;
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
						if(deltapos > delta)	//then it has extended into the adjacent point
						{
							//need to see just how far beyond it goes beyond the point, or if it even does at all.
							//first, check to see if it goes beyond.
							double jumpend = EnergyAdj - m * deltanext;	//start from the other end to find the top of the barrier
							if(energyUse <= jumpend)
							{
								wkb->startpos = pt->mxmy.y;	//it starts directly at the interface
								wkb->SharpEdge =true;
								wkb->EnergyBarrier = EnergyAdj - EnergyStart;
								wkb->relEnergy = energyUse - EnergyStart;
							}
							else
							{
								deltapos = (EnergyAdj - energyUse) / m;	// +/+ = +
								wkb->startpos = adjPt->position.y + deltapos;	//dir is negative, so this will move it closer
							}

						}
						else	//then it starts in the same point
							wkb->startpos = pt->position.y - deltapos;	//the starting position is... where it starts and then how far it must go

						if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
						{
							SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
						}	//end test for a sharp barrier
					}	//end else (m>0)
				}	//end if different materials
				
			}	//end in the conduction band
			else	//working in the valence band...
			{
				EnergyStart = pt->vb.bandmin;	//looking for the point where the electron crosses into the valence band
				EnergyAdj = adjPt->vb.bandmin;
				if(energyUse <= EnergyAdj && energyUse <= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				if(adjPt->MatProps == pt->MatProps)
				{
					difEv = EnergyAdj - EnergyStart;	//it should be negative
					m = difEv / totaldistance;	//this should be negative as well.
					deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					wkb->startpos = pt->position.y - deltapos;
					if(m <= -SHARP_BARRIER)
					{
						SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}
				}
				else	//they are different materials
				{
					double difVB = adjPt->eg - pt->eg;
					if(adjPt->MatProps)
						difVB += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difVB -= pt->MatProps->Phi;
					difEv = EnergyAdj - EnergyStart + difVB;	//the difference in valence bands removing offsets from Eg & affinity
					m = difEv / totaldistance;	//this should be negative as well.
					
					
					if(m >= 0)
					{
						double VBBandMinInterface = EnergyStart + m*delta;
						wkb->startpos = pt->mxmy.y;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = difVB;
						wkb->relEnergy = VBBandMinInterface - energyUse;	//note, VBBand... > energyUse. Looking at hole energy, so positive is going down
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					
						if(deltapos > delta)
						{
							double jumpend = EnergyAdj - m * deltanext;
							if(energyUse >= jumpend)	//it moves over through the discontinuity in the band
							{
								wkb->startpos = pt->mxmy.y;	//it starts directly at the interface
								wkb->SharpEdge = true;
								//because this is electrons, they may only be going into the CB in this framework. The barrier is actually going
								//to be with the conduction band
								wkb->EnergyBarrier = EnergyStart - EnergyAdj;
								wkb->relEnergy = EnergyStart - energyUse;
								
							}
							else	//then it moves over in the adjacent point
							{	//find where it comes from in relation to the other point
								deltapos = (EnergyAdj - energyUse) / m;	//   -/- = +
								wkb->startpos = adjPt->position.y + deltapos;
							}
						}	//end tunneling starts beyond interface
						else	//tunneling starts before/at the interface
							wkb->startpos = pt->position.y - deltapos;	//going in the negative direction, so starting position
						if(m <= -SHARP_BARRIER)	//if the slope is large, treat it as a "vertical" wall ish, and find the end point of that
							SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}	//end else (m<0)
				}	//end different materials

			}	//end tunneling starts form the valence band
			break;
		case PY:
			searchPt = startPt->adj.adjPY;
			if(searchPt == NULL)
			{
				wkb->type = TUNNEL_NONE;
				return(0);
			}
			adjPt = searchPt;
			delta = pt->pxpy.x - pt->position.y;
			deltanext = adjPt->position.y - adjPt->mxmy.y;
			totaldistance = delta + deltanext;
			if(wkb->initBand == CONDUCTION)
			{
				EnergyStart = pt->cb.bandmin;
				EnergyAdj = adjPt->cb.bandmin;
				if(energyUse >= EnergyAdj && energyUse >= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				if(adjPt->MatProps == pt->MatProps) 
				{
					difEc = EnergyAdj - EnergyStart;	//this should be a positive number, delta E
					m = difEc / totaldistance;	//slope
					deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
					wkb->startpos = pt->position.y + deltapos;	//the starting position is... where it starts and then how far it must go
					if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
					}	//end test for a sharp barrier
				}
				else	//the materials are different
				{
					double difChi = 0.0;
					if(adjPt->MatProps)
						difChi += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difChi -= pt->MatProps->Phi;
					difEc = EnergyAdj - EnergyStart + difChi;	//this is with taking the difference in chi into account
					m = difEc / totaldistance;	//slope
					if(m <= 0.0)
					{
						double bottomEnergyInterface = EnergyStart + delta*m;
						wkb->startpos = pt->pxpy.y;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = -difChi;
						wkb->relEnergy = energyUse - bottomEnergyInterface;
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	//difference from the starting position.
						if(deltapos > delta)	//then it has extended into the adjacent point
						{
							//need to see just how far beyond it goes beyond the point, or if it even does at all.
							//first, check to see if it goes beyond.
							double jumpend = EnergyAdj - m * deltanext;	//start from the other end to find the top of the barrier
							if(energyUse <= jumpend)
							{
								wkb->startpos = pt->pxpy.y;	//it starts directly at the interface
								wkb->SharpEdge =true;
								wkb->EnergyBarrier = EnergyAdj - EnergyStart;
								wkb->relEnergy = energyUse - EnergyStart;
							}
							else
							{
								deltapos = (EnergyAdj - energyUse) / m;	// +/+ = +
								wkb->startpos = adjPt->position.x - deltapos;	//dir is positive, so this will move it closer
							}
						}
						else	//then it starts in the same point
							wkb->startpos = pt->position.y + deltapos;	//the starting position is... where it starts and then how far it must go
						if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
						{
							SharpBarrierSearchStartCB(wkb, mdl, EnergyStart, startPt);
						}	//end test for a sharp barrier
					}	//end else(m>0)
				}	//end if different materials
				
			}	//end in the conduction band
			else	//working in the valence band...
			{
				EnergyStart = pt->vb.bandmin;	//looking for the point where the electron crosses into the valence band
				EnergyAdj = adjPt->vb.bandmin;
				if(energyUse <= EnergyAdj && energyUse <= EnergyStart)	//include energy start just in case in the future impurities are included
				{
					wkb->type = TUNNEL_NONE;
					return(0);
				}
				if(adjPt->MatProps == pt->MatProps)
				{
					difEv = EnergyAdj - EnergyStart;	//it should be negative
					m = difEv / totaldistance;	//this should be negative as well.
					deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					wkb->startpos = pt->position.x + deltapos;
					if(m <= -SHARP_BARRIER)
					{
						SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}
				}
				else	//they are different materials
				{
					double difVB = adjPt->eg - pt->eg;
					if(adjPt->MatProps)
						difVB += adjPt->MatProps->Phi;
					if(pt->MatProps)
						difVB -= pt->MatProps->Phi;
					difEv = EnergyAdj - EnergyStart + difVB;	//the difference in valence bands removing offsets from Eg & affinity
					m = difEv / totaldistance;	//this should be negative as well.
					
					if(m >= 0)
					{
						double VBBandMinInterface = EnergyStart + m*delta;
						wkb->startpos = pt->pxpy.x;
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = difVB;
						wkb->relEnergy = VBBandMinInterface - energyUse;	//note, VBBand... > energyUse. Looking at hole energy, so positive is going down
					}
					else
					{
						deltapos = (energyUse - EnergyStart) / m;	// -/- = +
					
						if(deltapos > delta)
						{
							double jumpend = EnergyAdj - m * deltanext;
							if(energyUse >= jumpend)	//it moves over through the discontinuity in the band
							{
								wkb->startpos = pt->pxpy.y;	//it starts directly at the interface
								wkb->SharpEdge = true;
								//because this is electrons, they may only be going into the CB in this framework. The barrier is actually going
								//to be with the conduction band
								wkb->EnergyBarrier = EnergyStart - EnergyAdj;
								wkb->relEnergy = EnergyStart - energyUse;
							}
							else	//then it moves over in the adjacent point
							{	//find where it comes from in relation to the other point
								deltapos = (EnergyAdj - energyUse) / m;	//   -/- = +
								wkb->startpos = adjPt->position.y - deltapos;
							}
						}	//end tunneling starts beyond interface
						else	//tunneling starts before/at the interface
							wkb->startpos = pt->position.y + deltapos;	//going in the positve direction, so starting position + change
						if(m <= -SHARP_BARRIER)	//if the slope is large, treat it as a "vertical" wall ish, and find the end point of that
							SharpBarrierSearchStartVB(wkb, mdl, EnergyStart, startPt);
					}	//end else(m<0)
				}	//end different materials

			}	//end tunneling starts form the valence band

			break;
		default:
			break;
	}
	//at this point, the starting position has been determined, and a flag is set with relevant data if it enters via a sharp interface.
	
	
	searchPt = startPt;
	Point* prevPt = searchPt;	//just to keep things simpler
	double EnergyEnd = 0.0;
	double deltaprev=0.0;
	do
	{
		switch(wkb->dir)
		{
			case NX:	//need to keep searching in the negative X direction
				searchPt = searchPt->adj.adjMX;
				break;
			case PX:
				searchPt = searchPt->adj.adjPX;
				break;
			case NY:
				searchPt = searchPt->adj.adjMY;
				break;
			case PY:
				searchPt = searchPt->adj.adjPY;
				break;
			default:
				keepSearching = false;
				break;
		}
		if(searchPt == NULL)
		{
			keepSearching = false;
			wkb->type = TUNNEL_NONE;
			return(0);	//avoid if statements from failing...Get out of the do loop
		}
		adjPt = pt;	//pointer to the previous point.
		pt = searchPt;	//pointer to the current point.

		if(energyUse >= searchPt->cb.bandmin && searchPt->MatProps->type != VACUUM)	//it is exiting into the conduction band
		{
			keepSearching = false;
			if(wkb->initBand == CONDUCTION)	//e-, CB->CB
			{
				if(wkb->SharpEdge == true)
					wkb->type = TUNNEL_WKB_SHARP;	//use T2 approximation
				else
					wkb->type = TUNNEL_WKB;	//use Tw2
			}
			else	//it started in the valence band
			{
				if(wkb->carrier == HOLE)
				{
					wkb->type = TUNNEL_NONE;	//don't allow holes to tunnel into the CB. Taken care of by e- tunnel into VB
					break;	//just exit the do/while loop.  This is not supposed to ever happen, but...
				}
				else	//electrons tunneling from the valence band into the CB
				{
					wkb->type = TUNNEL_INTERBAND;
				}
			}
			//now find where it exited. Also, cut the search short if it goes too far (I'll arbitrarily say 250nm). Realistically, find estimator for ~1e-10 for random number generator
			switch(wkb->dir)
			{
				case PX:
				case NX:
					if(fabs(wkb->startpos - searchPt->position.x) > 250e-7)
					{
						wkb->type = TUNNEL_NONE;
						return(0);
					}
					delta = searchPt->position.x - searchPt->mxmy.x;
					deltaprev = prevPt->position.x - prevPt->mxmy.x;
					break;
				case PY:
				case NY:
					if(fabs(wkb->startpos - searchPt->position.y) > 250e-7)
					{
						wkb->type = TUNNEL_NONE;
						return(0);
					}
					delta = searchPt->position.y - searchPt->mxmy.y;
					deltaprev = prevPt->position.y - prevPt->mxmy.y;
					
					break;
				default:
					break;
			}

			totaldistance= delta + deltaprev;
			EnergyEnd = searchPt->cb.bandmin;
			EnergyAdj = prevPt->cb.bandmin;	//this should be larger than EnergyEnd
			if(searchPt->MatProps == prevPt->MatProps)	//they are the same material
			{
				difEc = EnergyAdj - EnergyEnd;	//this should be a positive number, delta E
				m = difEc / totaldistance;	//slope - exiting into CB, so this is typically negative. As same material, it must be!
				deltapos = (energyUse - EnergyEnd) / m;	//difference from the starting position.
				if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
				{
					SharpBarrierSearchEndCB(wkb, mdl, EnergyEnd, searchPt);
				}	//end test for a sharp barrier
			}
			else	//they are at different materials
			{
				double difChi = 0.0;
				if(pt->MatProps)
					difChi += pt->MatProps->Phi;
				if(adjPt->MatProps)
					difChi -= adjPt->MatProps->Phi;

				difEc = EnergyEnd - EnergyAdj + difChi;	//this is with taking the difference in chi into account
				m = difEc / totaldistance;	//slope - this one is typically negative, but it may be positive or 0 due to differences in electron affinity
				
				if(m >= 0)	//tunneling in at interface
				{
					deltapos = delta;	//set it up so endpos goes to the interface
					if(wkb->SharpEdge == true)	//a sharp edge already exists. We want to use the larger of the edges
					{
						if(difChi > wkb->EnergyBarrier)	//this is a larger barrier, so use this one for the sharp edge stuff
						{
							wkb->EnergyBarrier = difChi;	//note, in this case, difChi should definitely be positive.
							wkb->relEnergy = energyUse - (EnergyEnd - m * delta);	//moving negatively to the interface, so -slope plus starting pos EnergyEnd
							//gives the bottom of te interface energy in ().
							wkb->type = TUNNEL_WKB_SHARP;
						}
					}
					else
					{
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = difChi;	//note, in this case, difChi should definitely be positive.
						wkb->relEnergy = energyUse - (EnergyEnd - m * delta);	//moving negatively to the interface, so -slope plus starting pos EnergyEnd
						//gives the bottom of te interface energy in ().
						wkb->type = TUNNEL_WKB_SHARP;
					}
				}
				else
				{
					deltapos = (EnergyEnd - energyUse) / m;	//difference from the ending position. Note, m<0... so deltapos is positive.
					if(deltapos > delta)	//then it has extended into the adjacent point
					{
					//need to see just how far beyond it goes beyond the point, or if it even does at all.
					//first, check to see if it goes beyond.
						double jumpend = EnergyAdj + m * deltaprev;	//start from the other end to find the top of the barrier
						if(energyUse <= jumpend)	//jumps in the interface
						{
							deltapos = delta;	//so it will know that it has to shift this much from the end point.
							if(EnergyAdj - EnergyEnd > wkb->EnergyBarrier)
							{
								wkb->SharpEdge =true;
								wkb->EnergyBarrier = EnergyAdj - EnergyEnd;
								wkb->relEnergy = energyUse - EnergyEnd;
							}
						}
						else	//jumps beyond the interface
						{
							deltapos = delta + (deltaprev - ((energyUse - EnergyAdj) / m) );	//how much it moves from the end point.
						//delta = length of first point, deltaprev = length of second point, delE/m = distance from 2nd point center to target
						}

					}	//end if deltapos goes into the adjacent point

					if(m <= -SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchEndCB(wkb, mdl, EnergyEnd, searchPt);
					}	//end test for a sharp barrier
				}	//end else m<0
			}	//end different material

			if(wkb->dir == PX || wkb->dir == PY)
				deltapos = -deltapos;	//the position will actually move negatively from the ending point as searching in the positive X direction

			if(wkb->dir == PX || wkb->dir == NX)
				wkb->endpos = searchPt->position.x + deltapos;
			else if(wkb->dir == PY || wkb->dir == NY)
				wkb->endpos = searchPt->position.y + deltapos;

		}	//end it exited into the conduction band


		if(energyUse <= searchPt->vb.bandmin && searchPt->MatProps->type != VACUUM)	//it is exiting into the valence band
		{
			keepSearching = false;
			if(wkb->initBand == CONDUCTION)	//it must be e-, CB->VB
			{
				wkb->type = TUNNEL_INTERBAND;
			}
			else
			{
				if(wkb->carrier == ELECTRON)	//don't do e- tunneling in VB to VB
				{
					wkb->type = TUNNEL_NONE;
					break;
				}
				else
				{
					if(wkb->SharpEdge == true)
						wkb->type = TUNNEL_WKB_SHARP;
					else
						wkb->type = TUNNEL_WKB;
				}
			}

			//now find where it exited - and cut short the search if it goes beyond 250 nm
			switch(wkb->dir)
			{
				case PX:
				case NX:
					if(fabs(wkb->startpos - searchPt->position.x) > 250e-7)
					{
						wkb->type = TUNNEL_NONE;
						return(0);
					}
					delta = searchPt->position.x - searchPt->mxmy.x;
					deltaprev = prevPt->position.x - prevPt->mxmy.x;
					break;
				case PY:
				case NY:
					if(fabs(wkb->startpos - searchPt->position.y) > 250e-7)
					{
						wkb->type = TUNNEL_NONE;
						return(0);
					}
					delta = searchPt->position.y - searchPt->mxmy.y;
					deltaprev = prevPt->position.y - prevPt->mxmy.y;
					break;
				default:
					break;
			}

			totaldistance= delta + deltaprev;
			EnergyEnd = searchPt->vb.bandmin;
			EnergyAdj = prevPt->vb.bandmin;	//this should be larger than EnergyEnd
			if(searchPt->MatProps == prevPt->MatProps)	//they are the same material
			{				
				difEv = EnergyEnd - EnergyAdj;	//the difference in valence bands 
				m = difEv / totaldistance;	//this should be positive as carriers are exiting into VB from same materials
				deltapos = (EnergyEnd - energyUse) / m;	// +/+ = +
				if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
				{
					SharpBarrierSearchEndVB(wkb, mdl, EnergyEnd, searchPt);
				}	//end test for a sharp barrier
			}
			else	//they are at different materials
			{
				double difVB = pt->eg - adjPt->eg;
				if(pt->MatProps)
					difVB += pt->MatProps->Phi;
				if(adjPt->MatProps)
					difVB -= adjPt->MatProps->Phi;
				
				difEv = EnergyEnd - EnergyAdj + difVB;	//the difference in valence bands removing offsets from Eg & affinity
				m = difEv / totaldistance;	//this should be positive, but may be negative as well.
				
				if(m <= 0)	//this is occurating at the interface
				{
					//note difVB is negative!
					//only want to override the sharp edge if it is larger
					if(wkb->SharpEdge == true)	//first make sure it exists
					{
						if(-difVB > wkb->EnergyBarrier)
						{
							wkb->EnergyBarrier = -difVB;
							wkb->type = TUNNEL_WKB_SHARP;
							wkb->relEnergy = (EnergyEnd - delta*m) - energyUse;
							deltapos = delta;
							//the part in () points to the top of the VB at the interface
						}
					}
					else	//it wasn't previously a sharp tunneling position
					{
						wkb->SharpEdge = true;
						wkb->EnergyBarrier = -difVB;
						wkb->type = TUNNEL_WKB_SHARP;
						wkb->relEnergy = (EnergyEnd - delta*m) - energyUse;
						deltapos = delta;
					}
				}
				else	//m>0 -- normal exiting out of a tunneling (maybe).
				{
					deltapos = (EnergyEnd - energyUse) / m;	// +/+ = +

					if(deltapos > delta)	//then it has extended into the adjacent point
					{
						//need to see just how far beyond it goes beyond the point, or if it even does at all.
						//first, check to see if it goes beyond.
						double jumpend = EnergyAdj + m * deltaprev;	//start from the other end to find the top of the barrier
						if(energyUse >= jumpend)	//jumps in the interface
						{
							deltapos = delta;	//so it will know that it has to shift this much from the end point.
							if(EnergyEnd - EnergyAdj > wkb->EnergyBarrier)	//call the entire barrier the differnce between the two points
							{	//the barrier may actually be larger if there is an incredibly fine mesh of points (but only do if the barrier beforehand was smaller)
								wkb->SharpEdge = true;
								wkb->EnergyBarrier = EnergyEnd - EnergyAdj;
								wkb->relEnergy = EnergyEnd - energyUse;
								wkb->type = TUNNEL_WKB_SHARP;
							}
						}
						else	//jumps beyond the interface
						{
							deltapos = delta + (deltaprev - ((energyUse - EnergyAdj) / m) );	//how much it moves from the end point.
							//delta = length of first point, deltaprev = length of second point, delE/m = distance from 2nd point center to target
						}

					}	//end if deltapos goes into the adjacent point

					if(m >= SHARP_BARRIER)	//now need to find just how large this barrier is.  It may extend multiple points in.
					{
						SharpBarrierSearchEndVB(wkb, mdl, EnergyEnd, searchPt);
					}	//end test for a sharp barrier
				}	//end else (M>0)
			}	//end different material

			if(wkb->dir == PX || wkb->dir == PY)
				deltapos = -deltapos;	//the position will actually move negatively from the ending point

			if(wkb->dir == PX || wkb->dir == NX)
				wkb->endpos = searchPt->position.x + deltapos;
			else if(wkb->dir == PY || wkb->dir == NY)
				wkb->endpos = searchPt->position.y + deltapos;


		}

		prevPt = searchPt;	//sort of like for part of while loop. Preparation for next loop.

	}while(keepSearching);
	//the end position should be found or there shouldn't be any tunneling occurring.

	//the problem should essentially be defined at this point

	return(0);
}

WKBData::WKBData()
{
	prevpoint=0;
	curpoint=0;
	delta=0;		//half width of the current point
	deltaPrev=0;	//half width of the previous point
	EcCur=0;	//conduction band minimum of current point
	EcPrev=0;	//" " " " previous "
	EvCur=0;	//valence " " " current "
	EvPrev=0;	//" " " " previous "
	startpos = 0;
	endpos = 0;
	curpos =0;
	prevpos = 0;
	betweenpos=0;
	initBand=CONDUCTION;	//did the band start in CONDUCTION or VALENCE
	carrier=ELECTRON;	//is the carrier ELECTRON or HOLE
	dir=0;		//what direction is the particle moving (M/P)(X/Y)
	Energy=0;	//what energy does the particle have
	integral=0;	//the summation of the integral, without the constants
	SharpEdge=false;	//was a high slope or change of material encountered (if so, use T2, else use Tw2)
	EnergyBarrier=0;	//if a sharp edge when entering/leaving, this is the barrier used to do the corrections
	relEnergy = 0.0;
	type = TUNNEL_NONE;
}