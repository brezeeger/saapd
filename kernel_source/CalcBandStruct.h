#ifndef POISSONEQUATION
#define POISSONEQUATION

#include <map>
#include "BasicTypes.h"

class ModelDescribe;
class Model;
class Point;
class Simulation;
class SystemEq;



double CalcBandStruct(Model& mdl, ModelDescribe& mdesc, bool ThEq=false);
//int CalcEField(Model& mdl);
double CalcPointBand(Point& pt, Model& mdl, Simulation& sim);
//int UpdateEquation(std::map<unsigned long int, double>& eq, Point& pt, Model& mdl);
int UpdatePsiCoeffs(Model& mdl, SystemEq* system);	//puts the dependency of each variable in the equation
int UpdateAllFermiPt(Model& mdl, Simulation& simul);
int UpdateFermiPt(Point& pt, Simulation& sim);


#endif