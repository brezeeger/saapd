#include "RateGeneric.h"

#include "model.h"
#include "ThGenRec.h"
#include "srh.h"
#include "scatter.h"
#include "DriftDiffusion.h"
#include "outputdebug.h"
#include "trig.h"
#include "contacts.h"
#include "transient.h"
#include "physics.h"

//returns true if this rate is actually calculated
bool CalcFastRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSourceSrc, int whichSourceTrg, double& rate, bool GenLight, PosNegSum* accuRate)
{
	rate = 0.0;
	int type = srcE->DetermineRateType(trgE);
	switch(type)
	{

		case RATE_GENERATION:
			if(sim->EnabledRates.ThermalGen)
			{
				rate = CalcGenRateFast(srcE, trgE, whichSourceSrc, whichSourceTrg, accuRate);
				return(true);
			}
			break;
		case RATE_RECOMBINATION:
			if(sim->EnabledRates.ThermalRec)
			{
				rate = CalcRecRateFast(srcE, trgE, whichSourceSrc, whichSourceTrg, sim, GenLight, accuRate);
				return(true);
			}
			break;
		case RATE_SRH1:
			if(sim->EnabledRates.SRH1)
			{
				rate = CalcSRH1Fast(srcE, trgE, whichSourceSrc, whichSourceTrg, sim, GenLight, accuRate);
				return(true);
			}
			break;
		case RATE_SRH2:
			if(sim->EnabledRates.SRH2)
			{
				rate = CalcSRH2Fast(srcE, trgE, whichSourceSrc, whichSourceTrg, accuRate);
				return(true);
			}
			break;
		case RATE_SRH3:
			if(sim->EnabledRates.SRH3)
			{
				rate = CalcSRH3Fast(srcE, trgE, whichSourceSrc, whichSourceTrg, sim, GenLight, accuRate);
				return(true);
			}
			break;
		case RATE_SRH4:
			if(sim->EnabledRates.SRH4)
			{
				rate = CalcSRH4Fast(srcE, trgE, whichSourceSrc, whichSourceTrg, accuRate);
				return(true);
			}
			break;
		case RATE_SCATTER:
			if(sim->EnabledRates.Scatter)
			{
				rate = CalcScatterFast(srcE, trgE, whichSourceSrc, whichSourceTrg, accuRate);
				return(true);
			}
			break;
		case RATE_DRIFTDIFFUSE:
			if(sim->EnabledRates.Current)
			{
				rate = CalcCurrentFast(srcE, trgE, whichSourceSrc, whichSourceTrg, false, accuRate);
				return(true);
			}
			break;
		case RATE_TUNNEL:
//			if(sim->EnabledRates.Tunnel)
//				;	//not implemented yet
			break;
		case RATE_LIGHTGEN:	//these rates are calculated in the light portion after all eLevel rates are done.
//			if(sim->EnabledRates.LightGen || sim->EnabledRates.LightRec)
//				;	//not implemented yet
			break;
		case RATE_LIGHTSCAT:	//these rates are calculated in the light portion after all eLevel rates are done.
//			if(sim->EnabledRates.LightScat)
//				;	//not implemented yet
			break;
		case RATE_UNDEFINED:
		default:
			break;
	}
	return(false);
}

int CalcFastDeriv(ELevel* srcE, ELevel* trgE, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, Simulation* sim, int whichsourceSrc, int whichsourceTrg)
{
	derivFact.factor = derivFact.factor_ci_srcband = derivFact.factor_ci_trgband = 0.0;
	derivSource = 0.0;
	derivTarget = 0.0;
	
	if(srcE == NULL || trgE == NULL || srcE == trgE)
		return(RATE_UNDEFINED);

	int type = srcE->DetermineRateType(trgE);
	switch(type)
	{

		case RATE_GENERATION:
			if(sim->EnabledRates.ThermalGen)
				CalcGenRecDerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_RECOMBINATION:
			if(sim->EnabledRates.ThermalRec)
				CalcGenRecDerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH1:
			if(sim->EnabledRates.SRH1)
				CalcSRH12DerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH2:
			if(sim->EnabledRates.SRH2)
				CalcSRH12DerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH3:
			if(sim->EnabledRates.SRH3)
				CalcSRH34DerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH4:
			if(sim->EnabledRates.SRH4)
				CalcSRH34DerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SCATTER:
			if(sim->EnabledRates.Scatter)
				CalcScatterDerivFast(srcE, trgE, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_DRIFTDIFFUSE:
			if(sim->EnabledRates.Current)
				CalculateCurrentDerivFast(srcE, trgE, derivFact, derivSource, derivTarget, whichsourceSrc, whichsourceTrg);
			break;
		case RATE_UNDEFINED:
			//then it is not going to have any effect aside from how the band diagram changes near it
		default:
			break;
	}
	return(type);
}

std::vector<ELevelRate>::iterator CalcRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{

	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	int type = srcE->DetermineRateType(trgE);

	switch(type)
	{

		case RATE_GENERATION:
			if(sim->EnabledRates.ThermalGen)
				ret = CalculateGenerationRate(srcE, trgE, sim, whichSource);
			break;
		case RATE_RECOMBINATION:
			if(sim->EnabledRates.ThermalRec)
				ret = CalculateRecombinationRate(srcE, trgE, sim, whichSource);
			break;
		case RATE_SRH1:
			if(sim->EnabledRates.SRH1)
				ret = CalculateSRH1Rate(srcE, trgE, sim, whichSource);
			break;
		case RATE_SRH2:
			if(sim->EnabledRates.SRH2)
				ret = CalculateSRH2Rate(srcE, trgE, sim, whichSource);
			break;
		case RATE_SRH3:
			if(sim->EnabledRates.SRH3)
				ret = CalculateSRH3Rate(srcE, trgE, sim, whichSource);
			break;
		case RATE_SRH4:
			if(sim->EnabledRates.SRH4)
				ret = CalculateSRH4Rate(srcE, trgE, sim, whichSource);
			break;
		case RATE_SCATTER:
			if(sim->EnabledRates.Scatter)
				ret = CalculateScatterRate(srcE, trgE, sim, whichSource);
			break;
		case RATE_DRIFTDIFFUSE:
			if(sim->EnabledRates.Current)
				ret = CalculateCurrentRate(srcE, trgE, sim, whichSource);
			break;
		case RATE_TUNNEL:
//			if(sim->EnabledRates.Tunnel)
//				;	//not implemented yet
			break;
		case RATE_LIGHTGEN:	//these rates are calculated in the light portion after all eLevel rates are done.
//			if(sim->EnabledRates.LightGen || sim->EnabledRates.LightRec)
//				;	//not implemented yet
			break;
		case RATE_LIGHTSCAT:	//these rates are calculated in the light portion after all eLevel rates are done.
//			if(sim->EnabledRates.LightScat)
//				;	//not implemented yet
			break;
		case RATE_UNDEFINED:
		default:
			break;
	}
	/*
	This applies to SteadyStateOld()

	if(sim->transient == false && ret != srcE->Rates.end())	//only for the monte carlo code, and the return has to exist
	{
		double NRate = DoubleSubtract(ret->RateIn, ret->RateOut);
		srcE->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
		trgE->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
		if(NRate > 0.0)
		{
			double tmpR = NRate * fabs(srcE->AbsFermiEnergy - trgE->AbsFermiEnergy);
			if(tmpR > srcE->maxREfIn)
				srcE->maxREfIn = tmpR;
			srcE->RateEfIn += NRate;
		}
		else if(NRate < 0.0)
		{
			double tmpR = -NRate * fabs(srcE->AbsFermiEnergy - trgE->AbsFermiEnergy);
			if(tmpR > srcE->maxREfOut)
				srcE->maxREfOut = tmpR;
			srcE->RateEfOut -= NRate;	//make sure it's positive
		}
	}
	*/
	return(ret);
	
}

double ELevelRate::GetStateNetRate() {
	return DoubleSubtract(RateIn, RateOut);
}

int RecordRate(Point* pt, ELevelRate& rate, ChargeTransfer& chgT, Simulation* sim)	//ts needed to get # photons for aggregate emission
{
	bool found=false;
	switch(rate.type)
	{
		case RATE_GENERATION:
			if(rate.RateType > 0.0)
				pt->genth += rate.RateType;
			break;
		case RATE_RECOMBINATION:
			if(rate.RateType > 0.0)
				pt->recth += rate.RateType;
			break;
		case RATE_SRH1:
			if(rate.RateType > 0.0)
				pt->srhR1 += rate.RateType;
			break;
		case RATE_SRH2:
			if(rate.RateType > 0.0)
				pt->srhR2 += rate.RateType;
			break;
		case RATE_SRH3:
			if(rate.RateType > 0.0)
				pt->srhR3 += rate.RateType;
			break;
		case RATE_SRH4:
			if(rate.RateType > 0.0)
				pt->srhR4 += rate.RateType;
			break;
		case RATE_SCATTER:
			if(rate.RateType > 0.0)
				pt->scatterR += rate.RateType;
			break;
		case RATE_DRIFTDIFFUSE:
			rate.RecordDDRate();	//the point in use might not actually be the point that is supposed to get the current applied to output, and direction must be applied
			break;
		case RATE_CONTACT_CURRENT_EXT:
			//only occurs if there is a contact in the source
			if(rate.carrier == ELECTRON)
				pt->contactData->nEnterOutput += rate.RateType;
			else
				pt->contactData->pEnterOutput += rate.RateType;
			break;
		case RATE_CONTACT_CURRENT_LINK:
			//do nothing! Don't know the time step yet...
			break;
		case RATE_LIGHTGEN:	
			if(rate.RateType > 0.0)
				pt->LightAbsorbGen += rate.RateType;
			break;
		case RATE_LIGHTSCAT:
			if(rate.RateType > 0.0)
				pt->LightAbsorbScat += rate.RateType;
			break;
		case RATE_LIGHTDEF:
			if(rate.RateType > 0.0)
				pt->LightAbsorbDef += rate.RateType;
			break;
		case RATE_LIGHTSRH:
			if(rate.RateType > 0.0)
				pt->LightAbsorbSRH += rate.RateType;
			break;
		case RATE_STIMEMISREC:
			if(rate.RateType > 0.0)
				pt->LightEmitRec += rate.RateType;
			break;
		case RATE_STIMEMISDEF:
			if(rate.RateType > 0.0)
				pt->LightEmitDef += rate.RateType;
			break;
		case RATE_STIMEMISSRH:
			if(rate.RateType > 0.0)
				pt->LightEmitSRH += rate.RateType;
			break;
		case RATE_STIMEMISSCAT:
			if(rate.RateType > 0.0)
				pt->LightEmitScat += rate.RateType;
			break;
		case RATE_TUNNEL:
			break;
		case RATE_UNDEFINED:
			break;
		default: //other data is hidden in lower numbers of rate.type
		if((rate.type & RATE_CONTACT_CURRENT_INT) || (rate.type & RATE_CONTACT_CURRENT_INTEXT))
		{
			found=true;
			rate.RecordDDRate();
		}

		if(!found)
			ProgressInt(52, rate.type);

			break;
	}
	if(rate.source->imp==NULL)
	{
		if(rate.carrier==ELECTRON)
			pt->CBNRin += rate.netrate;
		else
			pt->VBNRin += rate.netrate;
	}
	else
	{
		if(rate.carrier==ELECTRON)
			pt->DonNRin += rate.netrate;
		else
			pt->AcpNRin += rate.netrate;
	}
	return(0);
}

int ELevelRate::RecordRate()	//ts needed to get # photons for aggregate emission
{
	if (source == nullptr)
		return -1;
	Point& pt = source->getPoint();

	bool found=false;

	switch (type)
	{
	case RATE_GENERATION:
		if (RateType > 0.0)
			pt.genth += RateType;
		break;
	case RATE_RECOMBINATION:
		if (RateType > 0.0)
			pt.recth += RateType;
		break;
	case RATE_SRH1:
		if (RateType > 0.0)
			pt.srhR1 += RateType;
		break;
	case RATE_SRH2:
		if (RateType > 0.0)
			pt.srhR2 += RateType;
		break;
	case RATE_SRH3:
		if (RateType > 0.0)
			pt.srhR3 += RateType;
		break;
	case RATE_SRH4:
		if (RateType > 0.0)
			pt.srhR4 += RateType;
		break;
	case RATE_SCATTER:
		if (RateType > 0.0)
			pt.scatterR += RateType;
		break;
	case RATE_DRIFTDIFFUSE:
		RecordDDRate();	//the point in use might not actually be the point that is supposed to get the current applied to output, and direction must be applied
		break;
	case RATE_CONTACT_CURRENT_EXT:
		//only occurs if there is a contact in the source
		if(carrier == ELECTRON)
			pt.contactData->nEnterOutput += RateType;
		else
			pt.contactData->pEnterOutput += RateType;
		break;
	case RATE_CONTACT_CURRENT_LINK:
		//do nothing! Don't know the time step yet...
		break;
	case RATE_LIGHTGEN:
		if (RateType > 0.0)
			pt.LightAbsorbGen += RateType;
		pt.CBNRin += netrate;
		pt.VBNRin += netrate;
		break;
	case RATE_LIGHTSCAT:
		if (RateType > 0.0)
			pt.LightAbsorbScat += RateType;
		break;
	case RATE_LIGHTDEF:
		if (RateType > 0.0)
			pt.LightAbsorbDef += RateType;
		if(source->carriertype==ELECTRON)
			pt.DonNRin -= netrate;
		else
			pt.AcpNRin += netrate;

		if(target->carriertype==ELECTRON)
			pt.DonNRin += netrate;
		else
			pt.AcpNRin -= netrate;
		break;
	case RATE_LIGHTSRH:
		if (RateType > 0.0)
			pt.LightAbsorbSRH += RateType;

		if(source->imp)	//source is the lower bit, and net rate is absorption
		{
			if(source->imp->isCBReference())
				pt.DonNRin -= netrate;	//donor is losing carriers to CB, but pos net rate
			else
				pt.AcpNRin += netrate;
			pt.CBNRin += netrate;	//CB is gaining carriers
		}
		else
		{
			pt.VBNRin += netrate;	//electrons leaving the VB, because source is a band, which can only be VB
			if(target->imp)
			{
				if(target->imp->isCBReference())
					pt.DonNRin += netrate;
				else
					pt.AcpNRin -= netrate;
			}
			else
			{
				pt.CBNRin += netrate;
			}
		}
		break;
	case RATE_STIMEMISREC: //don't do pt.NRin for stim emis, because that would double count.
		if (RateType > 0.0)
			pt.LightEmitRec += RateType;
		break;
	case RATE_STIMEMISDEF:
		if (RateType > 0.0)
			pt.LightEmitDef += RateType;
		break;
	case RATE_STIMEMISSRH:
		if (RateType > 0.0)
			pt.LightEmitSRH += RateType;
		break;
	case RATE_STIMEMISSCAT:
		if (RateType > 0.0)
			pt.LightEmitScat += RateType;
		break;
	case RATE_TUNNEL:
		break;
	case RATE_UNDEFINED:	//want to see a message if this ever happens for debug purposes
	default: //other data is hidden in lower numbers of type
		if((type & RATE_CONTACT_CURRENT_INT) || (type & RATE_CONTACT_CURRENT_INTEXT))
		{
			found=true;
			RecordDDRate();	//these will get properly processed, and added into the contact data outputs
			//if other lower bits are set
		}

		if(!found)
			ProgressInt(52, type);
		break;
	}

	if(type < RATE_LIGHTMIN || (type > RATE_LIGHTMAX && type < RATE_CONTACT_MIN) || type > RATE_CONTACT_MAX)	//exclude optical rates
	{
		if(source->imp==NULL)
		{
			if(carrier==ELECTRON)
				pt.CBNRin += netrate;
			else
				pt.VBNRin += netrate;
		}
		else
		{
			if(carrier==ELECTRON)
				pt.DonNRin += netrate;
			else
				pt.AcpNRin += netrate;
		}
	}
	return(0);
}
/*
int RecordRate(Point* pt, std::vector<ELevelOpticalRate>::iterator& rate, ChargeTransfer& chgT, Simulation* sim)	//ts needed to get # photons for aggregate emission
{
	switch(rate->type)
	{
		
			break;
		case RATE_TUNNEL:
			break;
		case RATE_UNDEFINED:
		default:
			ProgressInt(52, rate->type);
			break;
	}
	return(0);
}
*/

bool ELevelRate::IncludeRate(int which) {
	double nRate = DoubleSubtract(RateIn, RateOut);	//safe subtraction. who knows about 'dem doubles

	bool goodSource = source!=nullptr ? source->AllowRate(nRate, which) : true;
	bool goodTarget = target != nullptr ? target->AllowRate(-nRate, which) : true;

	return(goodSource && goodTarget);
}

int ELevelRate::RecordDDRate()
{
	if(RateType == 0.0)	//cut out calculations on what would be roughly half
		return(0);

	if (source == nullptr || target == nullptr)
		return -1;

	Point& srcPt = source->getPoint();
	Point& trgPt = target->getPoint();
	double areai = srcPt.GetOverlapArea(&trgPt);
	if(areai != 0.0)
		areai = 1.0 / areai;
	
	XSpace direction = trgPt.position - srcPt.position;
	XSpace normalizeDir = direction;
	normalizeDir.Normalize();

	//the point that gets the rate will be the one where a straight line is drawn from center to center
	//where it passes through a px, py, or pz plane.

	XSpace normalPlane;	//let's just look at the source px/py/pz and if those don't intersect, then assume it's the target point

	normalPlane.y=normalPlane.z = 0.0;
	normalPlane.x=1.0;
	XSpace intersectPt;
	Point* usePt = NULL;
	//check the positive xyz directions
	if(LineIntersectPlane(intersectPt, srcPt.position, trgPt.position, normalPlane, srcPt.pxpy))
		usePt = &srcPt;
	else
	{
		//check y or z, but before if statement need to change normal plane
		normalPlane.x = 0.0;
		normalPlane.y = 1.0;
		if(LineIntersectPlane(intersectPt, srcPt.position, trgPt.position, normalPlane, srcPt.pxpy))
			usePt = &srcPt;
		else
		{
			normalPlane.y = 0.0;
			normalPlane.z = 1.0;
			if(LineIntersectPlane(intersectPt, srcPt.position, trgPt.position, normalPlane, srcPt.pxpy))
				usePt = &srcPt;
			else 				//it is not intersecting the x/y/z planes on the source point
				usePt = &trgPt;
		}
	}

	double therate = RateType;	//default rate value. Avoid double counting, which would occur with net rate. Note: RateType = Rate Out (sometimes zero)
	//crazy high rates that should cancel out not be able to cancel out...
	if(carrier == ELECTRON)	//current is going in the opposite direction as XSpace direction
	{
		therate = -therate;// * areai * QCHARGE;	//this rate should be in charge/cm^2/sec - was in carriers/sec
		usePt->Jn += normalizeDir * therate;
		if(type & RATE_CONTACT_SOURCE)
		{
			srcPt.contactData->JnOutput += normalizeDir * therate;
			srcPt.contactData->neteleciter += therate * normalizeDir.x;
			normalizeDir.x = fabs(normalizeDir.x);	//make it just the x/y/z component
			normalizeDir.y = fabs(normalizeDir.y);
			normalizeDir.z = fabs(normalizeDir.z);
			//therate is the rate OUT of the source, which is a contact. Want rate in, flip sign
			srcPt.contactData->JnOutputEnter += normalizeDir * -therate;	//there is no charge. This is just electrons entering the contact from a certain direction
		}
		if(type & RATE_CONTACT_TARGET)
		{
			trgPt.contactData->JnOutput += normalizeDir * therate;
			trgPt.contactData->neteleciter += therate * normalizeDir.x;
			normalizeDir.x = fabs(normalizeDir.x);	//make it just the x/y/z component
			normalizeDir.y = fabs(normalizeDir.y);	//most likely (1.0,0,0)
			normalizeDir.z = fabs(normalizeDir.z);
			//therate is rate OUT of the source, but this is a rate IN to the target.
			//leave as is
			trgPt.contactData->JnOutputEnter += normalizeDir * therate;	//there is no charge. This is just electrons entering the contact from a certain direction
		}


	}
	else	//this is holes, so do hole current.
	{
		//	rate = rate * areai * QCHARGE;	//this rate should be in charge/cm^2/sec - was in carriers/sec
		usePt->Jp += normalizeDir * therate;
		if(type & RATE_CONTACT_SOURCE)
		{
			srcPt.contactData->JpOutput += normalizeDir * therate;
			srcPt.contactData->netholeiter += therate * normalizeDir.x;	//just assuming X, though this probably doesn't matter
			normalizeDir.x = fabs(normalizeDir.x);	//make it just the x/y/z component
			normalizeDir.y = fabs(normalizeDir.y);
			normalizeDir.z = fabs(normalizeDir.z);
			//as before, therate is a rate q OUT of the source.
			//want rate q IN, flip sign
			srcPt.contactData->JpOutputEnter += normalizeDir * -therate;	//there is no charge. This is just electrons entering the contact from a certain direction
		}
		if(type & RATE_CONTACT_TARGET)
		{
			trgPt.contactData->JpOutput += normalizeDir * therate;
			trgPt.contactData->netholeiter += therate * normalizeDir.x;	//just assuming X, though this probably doesn't matter
			normalizeDir.x = fabs(normalizeDir.x);	//make it just the x/y/z component
			normalizeDir.y = fabs(normalizeDir.y);
			normalizeDir.z = fabs(normalizeDir.z);
			//therate is q rate out of source / IN to target.
			//leave the same
			trgPt.contactData->JpOutputEnter += normalizeDir * therate;	//there is no charge. This is just electrons entering the contact from a certain direction
		}

	}

	return(0);
}

//This is making the assumption that the entire difference in energy is attributed to light, and that phonons will not provide any energetic
//contribution but will cover the full momentum issue. It's crude, but you really need E-K diagrams and phonon dispersion curves to
//give this a proper treatment. Any approximations can be awful because phonons can acquire large momentums with minimal energies if you expand
//past the first brillouin zone, which is probably what's necessary to provide the massive changes in momentum needed. Considering optical phonons
//will only provide about 0.06eV, I think this is probably an OK (though not great) approximation.

/*
int RecordLightEmission(Point* pt, Tree<double, ELevelOpticalRate>* rate, Simulation* sim)
{
	if(sim->outputs.LightEmission == false)
		return(0);
	//assumes that this is a rate that will need to shed lots of energy...
	double ESrc = rate->Sort.source->CalcAbsEUseEnergy();
	double ETrg = rate->Sort.target->CalcAbsEUseEnergy();
	double dif = ESrc - ETrg;
	if(dif < ENERGY_CUTOFF_EMIT_PHOTON)	//could be reasonably done by phonons, optical phonons are about 60meV
		return(0);

	double ratecarpersec = rate->Data;

	float lambda = float(PHOTONENERGYCONST / dif);
	if(pt->MatProps)
	{
		OpSort op;
		op.LSpec = NULL;
		op.wavelength = lambda;
		pt->MatProps->OpticalFluxOut.Emissions.Add(ratecarpersec, lambda);
		sim->OpticalFluxOut.Emissions.Add(ratecarpersec, lambda);
	}
	
	return(0);
}
*/
//basically a new populatetarget function. Except a bit better tuned.
int FindAllTargets(ELevel& eLev, Point* pt, Model& mdl)
{
	eLev.TargetELev.clear();	//this data should pretty much always be constant! We'll cut this out and do once later.
	eLev.TargetELevDD.clear();

	if(eLev.imp == NULL && eLev.bandtail == false)
		eLev.TargetELev.reserve(pt->cb.energies.size() + pt->vb.energies.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->VBTails.size());
	else
		eLev.TargetELev.reserve(pt->cb.energies.size() + pt->vb.energies.size());
	
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->cb.energies.begin(); it != pt->cb.energies.end(); ++it)
	{
		if(it->second.uniqueID != eLev.uniqueID)
			eLev.TargetELev.push_back(&(it->second));
	}
	//these rates are all intrapoint, so that's how it should be.

	
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->vb.energies.begin(); it != pt->vb.energies.end(); ++it)
	{
		if(it->second.uniqueID != eLev.uniqueID)
			eLev.TargetELev.push_back(&(it->second));
	}

	if(eLev.imp == NULL || eLev.bandtail == false)	//if it is a defect (or band tail), it can't interact with the other defects/impurities yet!
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->acceptorlike.begin(); it != pt->acceptorlike.end(); ++it)
		{
			if(it->second.uniqueID != eLev.uniqueID)
				eLev.TargetELev.push_back(&(it->second));
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->donorlike.begin(); it != pt->donorlike.end(); ++it)
		{
			if(it->second.uniqueID != eLev.uniqueID)
				eLev.TargetELev.push_back(&(it->second));
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->CBTails.begin(); it != pt->CBTails.end(); ++it)
		{
			if(it->second.uniqueID != eLev.uniqueID)
				eLev.TargetELev.push_back(&(it->second));
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt->VBTails.begin(); it != pt->VBTails.end(); ++it)
		{
			if(it->second.uniqueID != eLev.uniqueID)
				eLev.TargetELev.push_back(&(it->second));
		}
	}

	//now find all adjacent target levels that match energy

	//negative x
	if(pt->adj.adjMX)
		FindELevelAdj(eLev, pt->adj.adjMX);
	if(pt->adj.adjMX2)
		FindELevelAdj(eLev, pt->adj.adjMX2);

	//positive x
	if(pt->adj.adjPX)
		FindELevelAdj(eLev, pt->adj.adjPX);
	if(pt->adj.adjPX2)
		FindELevelAdj(eLev, pt->adj.adjPX2);

	//negative y
	if(pt->adj.adjMY)
		FindELevelAdj(eLev, pt->adj.adjMY);
	if(pt->adj.adjMY2)
		FindELevelAdj(eLev, pt->adj.adjMY2);

	//positive y
	if(pt->adj.adjPY)
		FindELevelAdj(eLev, pt->adj.adjPY);
	if(pt->adj.adjPY2)
		FindELevelAdj(eLev, pt->adj.adjPY2);

	return(0);
}

int FindELevelAdj(ELevel& ESource, Point* ptT)
{
	if(ptT == NULL)
		return(0);

	double absMinESource;
	double absMaxESource;
	ESource.CalculateAbsoluteHighLowEnergy(absMaxESource, absMinESource);

	unsigned int size = ESource.TargetELevDD.size();

	if(ESource.carriertype == ELECTRON)
	{
		FindTargets(ESource, ptT->cb.energies);
		if(ESource.TargetELevDD.size() == size && absMinESource >= ptT->Psi)	//none were added and it should go to the top point
		{
			FindTargetOvershoot(ESource, ptT, CONDUCTION);
		}
		size = ESource.TargetELevDD.size();
	}

	if(ESource.carriertype == HOLE)
	{
		FindTargets(ESource, ptT->vb.energies);
		if(ESource.TargetELevDD.size() == size && absMaxESource <= ptT->vb.bandmin - ptT->MatProps->Phi)	//none were added.
		{
			FindTargetOvershoot(ESource, ptT, VALENCE);
		}
		size = ESource.TargetELevDD.size();
	}

	if(ptT->donorlike.size() > 0)
		FindTargets(ESource, ptT->donorlike);
	if(ptT->acceptorlike.size() > 0)
		FindTargets(ESource, ptT->acceptorlike);
	if(ptT->CBTails.size() > 0)
		FindTargets(ESource, ptT->CBTails);
	if(ptT->VBTails.size() > 0)
		FindTargets(ESource, ptT->VBTails);


	//if nothing was found, look for tunneling.

	//right now, skip tunneling...

	return(0);
}

/*
Originally, this calculated what the perceived occupancy would be at the overlap.  This is a very bad way to do things. IT makes the general
assumption that carriers automatically thermalize within an energy level.  This is not necessarily true - if you want that kind of accuracy,
then you should have finer divisions of the energy levels.  What would end up happening is that carriers will enter from the top, but would
automatically be assumed to be at the bottom.  Likewise, a stronger portion of carriers would be available to move in this manner.

In reality, carriers would have to scatter to a higher or lower energy and remain the same.  Therefore, this code should not take thermalization
into account, but should probably just do an outright average to get a more proper weighting in carriers going back and forth between energy states
*/

template <class KEY>
int FindTargets(ELevel& ESource, std::multimap<KEY,ELevel>& EnergyTargetStates)
{
	if(ESource.imp != NULL)	//this is an impurity
		return(0);	//just GET OUT...

	if(ESource.bandtail == true)	//it's a band tail - get out some more!
		return(0);

	if(EnergyTargetStates.size() == 0)
		return(0);

	RefTrans temp;
	TargetE tmpTarget;
	tmpTarget.percentOverlap = 0.0;
	tmpTarget.percentScatter = 0.0;
	tmpTarget.target = NULL;
	
//double t = ESource.ptrPoint->temp;
	double percentsuccess = 0.0;	//debugging variable
	Point* trgPt = NULL;
	for(std::map<KEY,ELevel>::iterator targets = EnergyTargetStates.begin(); targets != EnergyTargetStates.end(); ++targets)
	{
		if(targets->second.imp || targets->second.bandtail)
			continue;
		trgPt = &targets->second.getPoint();
				
		if(targets->second.carriertype == ELECTRON)
			tmpTarget.percentScatter = targets->second.boltzeUse * trgPt->scatterCBConst;
		else
			tmpTarget.percentScatter = targets->second.boltzeUse * trgPt->scatterVBConst;
		//I don't need to worry about changes in electron affinity because the number of carriers are reduced by that change already
		tmpTarget.target = &(targets->second);
		ESource.TargetELevDD.push_back(tmpTarget);

		percentsuccess += tmpTarget.percentScatter;
	}
	
	if(percentsuccess > 1.02)
	{
		ProgressDouble(29, percentsuccess);
	}

	/*
	Need to make a rehaul for a particular target destinations. See pg. 105, book 2.
	General idea: Build up list of all splits/divisions, then calculate what each portion of those
	get. Assign them to their appropriate state
	Tree<TargetData, double minEnergy> AllData

	*/
	/*
	TreeRoot<char, double> EnergySplits;	//applicable energy splits. Sort is absolute band min energy. Data is?

	double absMinESource;
	double absMaxESource;
	double absMinETarget;
	double absMaxETarget;
	double abseUseSource;
	ESource.CalculateAbsoluteHighLowEnergy(absMaxESource, absMinESource, abseUseSource);
	double Offset = -absMinESource;	//try to keep the exponent around 0.  If it gets too negative, then max/min get the smallest allowed numbers
	absMinESource += Offset;
	absMaxESource += Offset;	//get the correct values for all of them around zero.
	abseUseSource += Offset;
	std::vector<unsigned int> GoodTargetStates;	//holds which indices in EnergyTargetStates are valid targets
	std::vector<double> percentto;	//holds all the percent to values that align up with GoodTargetStates
	std::vector<double> MaxTarget;
	std::vector<double> MinTarget;
	MaxTarget.reserve(5);
	MinTarget.reserve(5);
	GoodTargetStates.reserve(5);	//typically, there will be 3, but allow possibility of overlap


	if(absMinESource == absMaxESource)	//give a finite width/avoid divide by 0 later
	{
		absMinESource -= 0.001;
		absMaxESource += 0.001;
	}

	EnergySplits.Insert(0, absMinESource);
	EnergySplits.Insert(0, absMaxESource);	//put the max and min energies in
	double sourceRelEf;
	double commonDen = CalcFermiSplitConc(ESource, true, sourceRelEf, absMaxESource, absMinESource, absMaxESource, absMinESource);
	if(commonDen != 0.0)
		commonDen = 1.0 / commonDen;

	bool addState = false;
	double absEUseTarget;	//unused, but needed to pass in
	for(unsigned int i=0; i<targetdests; i++)	//get all the divisions
	{
		EnergyTargetStates[i]->CalculateAbsoluteHighLowEnergy(absMaxETarget, absMinETarget, absEUseTarget);
		absMinETarget += Offset;	//adjust target for offset in making the states linear functions
		absMaxETarget += Offset;	//instead of just discretized functions.

		//make sure it has a definable width, so the integral doesn't end up being 0.
		if(absMinETarget == absMaxETarget)
		{
			absMinETarget -= .00001;
			absMaxETarget += .00001;
		}	//just give it a narrow width if it is defined as a discrete state

		if(absMinETarget >= absMinESource && absMinETarget <= absMaxESource)
		{
			addState = true;			//flag to add to good target states
			if(EnergySplits.Find(absMinETarget) == NULL)	//this division already exists?
				EnergySplits.Insert(0, absMinETarget);	//nope, add it
		}

		if(absMaxETarget >= absMinESource && absMaxETarget <= absMaxESource)	//same thing, but now checking the maxE
		{
			addState = true;
			if(EnergySplits.Find(absMaxETarget) == NULL)
				EnergySplits.Insert(0, absMaxETarget);
		}

		if(absMinETarget <= absMinESource && absMaxETarget >= absMaxESource)	//the state completely surrounds the other value.
			addState = true;

		if(addState)
		{
			addState = false;	//reset it
			GoodTargetStates.push_back(i);	//only want to cycle through active energy states in the future.
			//save processing time when there are a large number of states in the future.
			percentto.push_back(0.0);	//grow it as initialized. PReviously reserved.
			MaxTarget.push_back(absMaxETarget);
			MinTarget.push_back(absMinETarget);
		}
	}

	//at this point, there is a tree giving all the energy splits (in order), and all the active energies
	//have been cataloged.
	
	std::vector<double> percentsplit;	//holds all the percent to values that align up with GoodTargetStates
	targetdests = GoodTargetStates.size();	//now we'll only care about particular energy states.
	
	std::vector<double> splits;
	EnergySplits.GenerateSortS(splits);	//store all the double values in splits, which will be in order.
	percentsplit.reserve(EnergySplits.size-1);

	double partitionfunc = 0.0;
	std::vector<double> StateEVOverlap;
	StateEVOverlap.resize(targetdests);	//temporary holder of data

	double percentnumerator;
	double percent;
	for(unsigned int i=0; i < EnergySplits.size - 1; i++)	//look at each min(i)/max(i+1) pair
	{
		percentnumerator = CalcFermiSplitConc(ESource, false, sourceRelEf,splits[i+1], splits[i], absMaxESource, absMinESource);
		percent = percentnumerator * commonDen;

		percentsplit.push_back(percent);	//put the percentage in for this particular energy range
		partitionfunc = 0.0;

		if(percentsplit[i] == 0.0)	//then it managed to be discrete (insert into tree adds node if it already exists, which
			continue;	//it will always do for the bottom node
		//now need to calculate how this gets split up among the states
		for(unsigned int j=0; j<targetdests; j++)
		{
			double numstates = EnergyTargetStates[GoodTargetStates[j]]->GetNumstates();
			double EWidth = MaxTarget[j] - MinTarget[j];	//note this cannot be 0
			double maxOver = MaxTarget[j];	//see what the max/min are when overlapping with the state
			double minOver = MinTarget[j];
			
			if(maxOver > splits[i+1])	//make sure they don't exceed the bounds
				maxOver = splits[i+1];

			if(minOver < splits[i])
				minOver = splits[i];

			double evOverlap = maxOver - minOver;
			if(evOverlap <= 0)	//if it was entirely outside the state, because max>min at start, but min is moved > max or max is moved < min
			{
				StateEVOverlap[j] = 0.0;
				continue;	//no need for any more calculation, so just move to the next state to test for
			}
			StateEVOverlap[j] = numstates * (evOverlap / EWidth);	//building up partition function for states present
			//in this particular energy range.  Do not know if states are increasing or decreasing as you move up in the band
			//(typically, increases, but custom E-K in future that might not be the casE)
			partitionfunc += StateEVOverlap[j];	//essentially normalize by the number of states in this particular region of overlap
		}
		if(partitionfunc <= 0.0)
			continue;	//there were no overlaps found, so move on to the next energy split

		partitionfunc = 1.0 / partitionfunc;

		//now must go through all the target destinations again to add to the proper percentages for each division
		for(unsigned int j=0; j<targetdests; j++)
		{
			percentto[j] += StateEVOverlap[j] * partitionfunc * percentsplit[i];
			if(percentto[j] > 1.0)
				percentto[j]=1.0;	//failsafe. Ish.
			// add to the target - number states / total num states * percent of carriers getting this portion
		}
	}

	//now that they have all been properly added up, add them to the targets list.
	for(unsigned int j=0; j < targetdests; j++)	//and check them with all destination states...
	{
		percentsuccess += percentto[j];

		if(percentsuccess > 1.0000002)
		{
			percentto[j] = 1.0 - (percentsuccess - percentto[j]);	//get temp so percentsuccess is 1
			if(percentsuccess > 1.02)
				ProgressDouble(29, percentsuccess);
			percentsuccess = 1.0;
		}
		if(percentto[j] > 0.0)
		{		
			ESource.TargetELev.Insert(percentto[j], EnergyTargetStates[GoodTargetStates[j]]);

		}
	}	//end for loop through destinations


	EnergySplits.Clear();	//make sure the memory gets cleared out
	*/

	return(0);
}

//what happens if the carrier is under such a high field (somehow?) that the points are so far apart it cannot transfer carriers
//Essentially they are sent to vacuum, but should be falling down rapidly
int FindTargetOvershoot(ELevel& ESource, Point* ptT, bool band)
{
	TargetE tmpTarget;
	tmpTarget.percentOverlap = 0.0;
	tmpTarget.percentScatter = 1.0;	//just put it all in the top band
	tmpTarget.target = NULL;
	std::multimap<MaxMin<double>,ELevel>::reverse_iterator rIt;
	if(band == CONDUCTION)
	{
		if(ptT->cb.energies.empty() == false)
			tmpTarget.target = &(ptT->cb.energies.rbegin()->second);	//the last element is the first reverse element - largest energy level
	}
	else
	{
		if(ptT->vb.energies.empty() == false)
			tmpTarget.target = &(ptT->vb.energies.rbegin()->second);	//the last element is the first reverse element - largest energy level
	}
	
	if(tmpTarget.target)
	{
		double absMaxESource;
		double absMinESource;
		double absMaxTarget;
		double absMinTarget;
		ESource.CalculateAbsoluteHighLowEnergy(absMaxESource, absMinESource);
		tmpTarget.target->CalculateAbsoluteHighLowEnergy(absMaxTarget, absMinTarget);

		if((absMinESource >= absMaxTarget && band==CONDUCTION) || (absMaxESource <= absMinTarget && band == VALENCE))
			ESource.TargetELevDD.push_back(tmpTarget);
	}

	return(0);
}

double ELevel::CalcFermiSplitConc(bool setRelEF, double& relEf, double absEMax, double absEMin, double absESrcMax, double absESrcMin)
{
	double temp = pt.temp;
	double occ = GetBeginOccStates();
	double tot = GetNumstates();
	double val = 0.0;

	if(carriertype == HOLE)	//swap the values so it's like going up the conduction band with electrons
	{
		double tmp = absEMax;
		absEMax = -absEMin;
		absEMin = -tmp;

		tmp = absESrcMax;
		absESrcMax = -absESrcMin;
		absESrcMin = -tmp;
	}
	if(occ <= 0.0)
		occ = 1.0;	//just get a rough distrubution!

	if(occ >= tot)
		occ = tot;	//I doubt a state will have < 1 state, but be safe just in case!

	absEMax -= absESrcMin;	//shift it to keep numbers simple, small, and cut out calculations
	absEMin -= absESrcMin;
	absESrcMax -= absESrcMin;
	absESrcMin = 0.0;

	if(absEMin < 0.0)
		absEMin = 0.0;	//gotta keep it within overall boundaries

	if(absEMax > absESrcMax)
		absEMax = absESrcMax;

	if(temp == 0.0)
	{
		double cutoff = occ/tot * absESrcMax;

		if(absEMin >= cutoff)
			return(0.0);
		if(absEMax <= cutoff)
		{
			val = (absEMax - absEMin) / cutoff;
			return(val);
		}

		//now assume the cutoff is between the min and max, and cutoff is not zero, or it wouldn't returned zero on first if statement
		
		val = occ * (cutoff - absEMin) / cutoff;
		return(val);
	}

	if(occ >= tot)
	{
		val = absESrcMax - absESrcMin;
		val = occ * (absEMax - absEMin) / val;
		return(val);
	}

	//now the fun and hard part... This essentially looks at the occupancy of the level, and assumes an equal distribution
	//of states amongst the width. It then uses a boltzmann distribution to determine how many carriers are in each location.
	//this likely will not match up with the actual fermi level, so a new fermi level is calculated that will allow the distribution
	//to exist.  Using the proper fermi level, an appropriate number of carriers are then selected based on the location
	//of the interested slab of energy levels.
	double kT = KB * temp;
	double beta = 1.0 / kT;
	if(setRelEF)
	{
		double lnnum = occ * absESrcMax * beta;
		double lnden = -absESrcMax * beta;
		lnden = 1.0 - exp(lnden);
		lnden = tot * lnden;
		relEf = kT * log(lnnum / lnden);	//this is assuming Esrcmin is 0
	}
	
	double exp1 = relEf - absEMin;
	exp1 = exp1 * beta;
	exp1 = exp(exp1);
	double exp2 = relEf - absEMax;
	exp2 = exp2 * beta;
	exp2 = exp(exp2);

	val = tot * kT / absESrcMax * (exp1 - exp2);

	/*
	shift so EMax>EMin && EMin = 0

	integrate boltzmann: kT(exp[(Ef-EMin)Beta] - exp[(Ef-EMax)Beta])
	n = integral{ f(E) tot/energyWidth dE

	Solve for Ef:
	Ef = kT * ln(occ delE beta / tot) - kT * ln[exp(-EMin*Beta) - exp(-EMax*beta)]

	Then can use Ef to find what the n would be in that area:

	n = tot * kT / energyWidth * [exp(Beta*(Ef-E-)) - exp(Beta*(Ef-E+))]

	the fraction then sent is n with EMin/Emax bounds over n with energy level max/mins


	*/

	return(val);
}


int ELevel::AdjacentPoint(ELevel* trg)
{
	if (trg == nullptr)
		return RATE_NOTADJACENT;

	
	Point* trgPt = &trg->getPoint();

	if(pt.adj.adjMX == trgPt)
		return(RATE_MX);
	if(pt.adj.adjPX == trgPt)
		return(RATE_PX);
	if(pt.adj.adjMY == trgPt)
		return(RATE_MY);
	if(pt.adj.adjPY == trgPt)
		return(RATE_PY);
	if(pt.adj.adjMX2 == trgPt)
		return(RATE_MX2);
	if(pt.adj.adjPX2 == trgPt)
		return(RATE_PX2);
	if(pt.adj.adjMY2 == trgPt)
		return(RATE_MY2);
	if(pt.adj.adjPY2 == trgPt)
		return(RATE_PY2);

	return(RATE_NOTADJACENT);

}

int ELevel::DetermineRateType(ELevel* trgeLev)
{
	//first check if they are in the same point
	if(trgeLev == NULL)
		return(RATE_UNDEFINED);
	if(&pt == &trgeLev->getPoint())
	{
		//then it is gen/rec/srh/scatter
		//check for srh:
		if(imp || bandtail)	//then it is going to be an SRH of sorts...
		{
			if(trgeLev->imp || trgeLev->bandtail)
				return(RATE_UNDEFINED);	//there are no rates for carriers bouncing between impurities in a point yet

			//then the rarget must be either CB or VB

			if(trgeLev->carriertype == ELECTRON)	//IMP -> CB - SRH2
				return(RATE_SRH2);
			else
				return(RATE_SRH4);	//IMP hole -> VB, emission
		}

		if(trgeLev->imp || trgeLev->bandtail)
		{
			//no need to check if srceLev is an imp, because then it would have already returned rate undefined

			if(carriertype == ELECTRON)	//CB --> IMP - SRH1
				return(RATE_SRH1);
			else	//VB hole --> IMP capture
				return(RATE_SRH3);	//because it is rates, this one will need special care.
		}

		//so no impurities are involved anymore, and it is within the same point

		if(carriertype == trgeLev->carriertype)	//same point, same band, no impurities or band tails involved
		{
			//then it is scattering
			return(RATE_SCATTER);
		}

		//so if it has reached this far, then it is opposite types, and the carriers are in the same point, no efects involved
		//it must be generation/recombination
		if(carriertype == HOLE)	//electrons in the valence band
			return(RATE_GENERATION);
		else	//electrons in the CB
			return(RATE_RECOMBINATION);

	}
	else if(AdjacentPoint(trgeLev))
	{
		//they're next to one another, but not much else is known.

		//if either is an impurity it must be tunneling.
		if(imp || bandtail)
			return(RATE_UNDEFINED);	//this form of tunneling isn't supported yet
		if(trgeLev->imp || trgeLev->bandtail)
			return(RATE_UNDEFINED);

		//so they are adjacent to one another, and not impurities/defects
		if(carriertype != trgeLev->carriertype)
		{
			//then it must be tunneling - VB->CB or CB->VB
			if(PercentOverlap(trgeLev) > 0.0)	//only tunnel into points with energies that overlap
				return(RATE_TUNNEL);
		}
		else
		{
			return(RATE_DRIFTDIFFUSE);
		} //need to add possibility
	}
	else
	{
		return(RATE_TUNNEL);
	}

	return(RATE_UNDEFINED);
}

double Point::GetOverlapArea(Point* target)
{
	if (target == nullptr)
		return 0.0;

	XSpace max;
	XSpace min;
	max.x = max.y = max.z = min.x = min.y = min.z = 0.0;	//initialize
	
	if(adj.adjMX == target)	//it's in the negative x direction
	{
		max.x = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.x = 0.0;

		max.y = pxpy.y;
		max.z = pxpy.z;
		
		min.y = mxmy.y;
		min.z = mxmy.z;

		if(max.y > target->pxpy.y)	//likely the same
			max.y = target->pxpy.y;
		if(min.y < target->mxmy.y)
			min.y = target->mxmy.y;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjPX == target)
	{
		max.x = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.x = 0.0;

		max.y = pxpy.y;
		max.z = pxpy.z;
		
		min.y = mxmy.y;
		min.z = mxmy.z;

		if(max.y > target->pxpy.y)	//likely the same
			max.y = target->pxpy.y;
		if(min.y < target->mxmy.y)
			min.y = target->mxmy.y;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjMY == target)
	{
		max.y = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.y = 0.0;

		max.x = pxpy.x;
		max.z = pxpy.z;
		
		min.x = mxmy.x;
		min.z = mxmy.z;

		if(max.x > target->pxpy.x)	//likely the same
			max.x = target->pxpy.x;
		if(min.x < target->mxmy.x)
			min.x = target->mxmy.x;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjPY == target)
	{
		max.y = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.y = 0.0;

		max.x = pxpy.x;
		max.z = pxpy.z;
		
		min.x = mxmy.x;
		min.z = mxmy.z;

		if(max.x > target->pxpy.x)	//likely the same
			max.x = target->pxpy.x;
		if(min.x < target->mxmy.x)
			min.x = target->mxmy.x;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjMX2 == target)
	{
		max.x = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.x = 0.0;

		max.y = pxpy.y;
		max.z = pxpy.z;
		
		min.y = mxmy.y;
		min.z = mxmy.z;

		if(max.y > target->pxpy.y)	//likely the same
			max.y = target->pxpy.y;
		if(min.y < target->mxmy.y)
			min.y = target->mxmy.y;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjPX2 == target)
	{
		max.x = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.x = 0.0;

		max.y = pxpy.y;
		max.z = pxpy.z;
		
		min.y = mxmy.y;
		min.z = mxmy.z;

		if(max.y > target->pxpy.y)	//likely the same
			max.y = target->pxpy.y;
		if(min.y < target->mxmy.y)
			min.y = target->mxmy.y;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjMY2 == target)
	{
		max.y = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.y = 0.0;

		max.x = pxpy.x;
		max.z = pxpy.z;
		
		min.x = mxmy.x;
		min.z = mxmy.z;

		if(max.x > target->pxpy.x)	//likely the same
			max.x = target->pxpy.x;
		if(min.x < target->mxmy.x)
			min.x = target->mxmy.x;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}
	else if(adj.adjPY2 == target)
	{
		max.y = 1.0;	//make sure the difference will be 1.0 so won't affect area multiplication
		min.y = 0.0;

		max.x = pxpy.x;
		max.z = pxpy.z;
		
		min.x = mxmy.x;
		min.z = mxmy.z;

		if(max.x > target->pxpy.x)	//likely the same
			max.x = target->pxpy.x;
		if(min.x < target->mxmy.x)
			min.x = target->mxmy.x;

		if(max.z > target->pxpy.z)	//likely the same
			max.z = target->pxpy.z;
		if(min.z < target->mxmy.z)
			min.z = target->mxmy.z;
	}

	max = max - min;
	double area = (max.x * max.y * max.z);

	return(area);
}

int ContactCurrent(Point& pt, ModelDescribe& mdesc, Model& mdl)
{
	if(pt.contactData == NULL)
		return(0);

	if(pt.contactData->isolated == true)
		return(0);
	
	if(pt.contactData->GetCurrentActive() & CONTACT_SINK)
	{
		pt.contactData->netelectrontime = pt.contactData->neteleciter * mdesc.simulation->TransientData->timestep;
		pt.contactData->netholetime = pt.contactData->netholeiter * mdesc.simulation->TransientData->timestep;
		return(0);	//this is recorded elsewhere
	}

	//////////////////////////////////////////// Rates were introduced to keep the contacts from destroying the timestep, and output in general
	//ELevelOpticalRate SortRateMatch;
//	Tree<double, ELevelOpticalRate>* ptrConCurRate = NULL;
	
	
	//need to update the contact current rates
	

	for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = pt.cb.energies.begin(); cbIt != pt.cb.energies.end(); ++cbIt)
	{
		double netRate;
		for(std::vector<ELevelRate>::iterator rateIt = cbIt->second.Rates.begin(); rateIt != cbIt->second.Rates.end(); ++rateIt)
		{
			if(rateIt->type == RATE_CONTACT_CURRENT_INTEXT || rateIt->type == RATE_CONTACT_CURRENT_EXT)
			{
				netRate = rateIt->RateOut - rateIt->RateIn;	//rate out is what is entering the external part of the device
				netRate = netRate * mdesc.simulation->TransientData->timestep;
				MapAdd(pt.contactData->electrontime, pt.adj.self, netRate);
				pt.contactData->netelectrontime += netRate;
			}
			else if(rateIt->type == RATE_CONTACT_CURRENT_LINK)
			{
				netRate = rateIt->RateIn - rateIt->RateOut;	//this is entering the contact point from another contact
				netRate = netRate * mdesc.simulation->TransientData->timestep;
				MapAdd(pt.contactData->holetime,pt.adj.self,netRate);
				pt.contactData->netholetime += netRate;
			}
		}
	}

	
//	SortRateMatch.source = NULL;
//	SortRateMatch.carrier = HOLE;
	for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = pt.vb.energies.begin(); vbIt != pt.vb.energies.end(); ++vbIt)
	{
		double netRate;
		for(std::vector<ELevelRate>::iterator rateIt = vbIt->second.Rates.begin(); rateIt != vbIt->second.Rates.end(); ++rateIt)
		{
			if(rateIt->type == RATE_CONTACT_CURRENT_INTEXT || rateIt->type == RATE_CONTACT_CURRENT_EXT)
			{
				netRate = rateIt->RateOut - rateIt->RateIn;
				netRate = netRate * mdesc.simulation->TransientData->timestep;
				MapAdd(pt.contactData->holetime,pt.adj.self,netRate);
				pt.contactData->netholetime += netRate;
			}
			else if(rateIt->type == RATE_CONTACT_CURRENT_LINK)
			{
				netRate = rateIt->RateIn - rateIt->RateOut;	//this is entering the contact point from another contact
				netRate = netRate * mdesc.simulation->TransientData->timestep;
				MapAdd(pt.contactData->holetime,pt.adj.self,netRate);
				pt.contactData->netholetime += netRate;
			}
		}
	}
	
	/*

	This code was used to make sure the net charge on the contact was always zero. This means that if current is injected,
	it would become *bad*.  Essentially, this would cancel things out that we would expect to happen, and throw off
	any current injections that were set.
	/////////////////////////////////////
	double AmountCharge = pt->netCharge;	//note, this does not include volume or multiplication by Q at this point.
	if(AmountCharge == 0.0)	//it's all good at this point
		return(0);

	Simulation* sim = mdesc.simulation;	//make things a little simpler
	bool AddCarriersOneBand = false;

	double numelec = pt->cb.GetNumParticles();;
	double numhole = pt->vb.GetNumParticles();;
	double condN = numelec;
	double condP = numhole;

	if(pt->MatProps)
	{
		if(pt->MatProps->MuN > 0.0)
			condN = condN * pt->MatProps->MuN;
		if(pt->MatProps->MuP > 0.0)
			condP = condP * pt->MatProps->MuP;
	}

	double totCond = condN + condP;

	if(totCond == 0.0)
	{
		//then there must be nothing in the bands, but a charge is still present.
		AddCarriersOneBand = true;
	}

	double NumNTransferIn = 0.0;
	double NumPTransferIn = 0.0;
	if(AddCarriersOneBand == false)
	{
		NumNTransferIn = AmountCharge * condN / totCond;	//if too many holes, positive charge and transfer electrons in
		NumPTransferIn = -AmountCharge * condP / totCond;	//if too many holes, positive charge and transfer holes out
	}
	else
	{
		if(AmountCharge >  0.0)	//it is p type
			NumNTransferIn = AmountCharge;
		else
			NumPTransferIn = -AmountCharge;
	}

	if(NumNTransferIn < 0.0 && -NumNTransferIn > numelec)	//it wants to take out more electrons than there are.
	{
		AddCarriersOneBand = true;
		NumNTransferIn = 0.0;
		NumPTransferIn = -AmountCharge;	//if NumNTransferIn < 0, electrons are leaving meaning adding positive charge, which means amountcharge must have been negative.
	}

	if(NumPTransferIn < 0.0 && -NumPTransferIn > numhole)	//it wants to take out more electrons than there are.
	{
		AddCarriersOneBand = true;
		NumPTransferIn = 0.0;
		NumNTransferIn = AmountCharge;	//if NumPTransferIn < 0, holes are leaving meaning adding negative charge, which means amountcharge must have been positive.
	}
	//otherwise, allow currents to flow into the conduction and valence band to try and equalize the charge.

	//at this point, we know how many carriers are being added or taken away.
	//if carriers are being taken away, partition function is simply sum of carriers in band. If they all reduce proportionally, the distribution between them should be maintained
	//if carriers are being added, parititon function is unoccupied * exp(boltzEUse) for "easy" version - this should work *most* of the time
	//when it doesn't (it tries to put in too many carriers than there are states available), go 75%(arbitrary) of the way there, subtract out the # carriers and partition function

	std::vector<double> probs;

	if(NumNTransferIn > 0.0)
	{
		double partitionN = 0.0;
		unsigned int sz = pt->cb.energies.size;
		probs.resize(sz);
		double unocc;
		pt->cb.energies.GenerateSortPtr(eLevs);
		
		for(unsigned int i=0; i<sz; i++)
		{
			unocc = eLevs[i]->GetNumstates()-eLevs[i]->GetEndOccStates();
			probs[i] = unocc * exp(-KBI * eLevs[i]->eUse / pt->temp);
			partitionN += probs[i];
		}

		//now add the carriers back in
		double attemptMove, occ, numst;
		double partitionNi = 1.0 / partitionN;
		double summoved = 0.0;
		
		for(unsigned int i=0; i<sz; i++)
		{
			attemptMove = partitionNi * probs[i] * NumNTransferIn;
			occ = eLevs[i]->GetEndOccStates();
			numst = eLevs[i]->GetNumstates();
			if(occ + attemptMove > numst)
			{
				attemptMove = 0.75 * (numst - occ);	//move an amount that will work
				partitionN = 0.0;
				for(unsigned int j=i+1; j<sz; j++)
					partitionN += probs[j];

				partitionNi = 1.0 / partitionN;
				NumNTransferIn -= summoved;
				NumNTransferIn -= attemptMove;
				summoved = -attemptMove;	//need it to be zero after this iteration as it is not a part of the partition function
			}
			summoved += attemptMove;
			eLevs[i]->AddEndOccStates(attemptMove);
			if(attemptMove>0)
				pt->cb.AddNumParticles(attemptMove);
			else
				pt->cb.RemoveNumParticles(-attemptMove);
			pt->netCharge -= attemptMove;

		}
	}
	else if(NumNTransferIn < 0.0)
	{
		double partitionN = 0.0;
		unsigned int sz = pt->cb.energies.size;
		probs.resize(sz);
		pt->cb.energies.GenerateSortPtr(eLevs);
	
		for(unsigned int i=0; i<sz; i++)
		{
			probs[i] = eLevs[i]->GetEndOccStates();
			partitionN += probs[i];
		}

		//now add the carriers back in
		double attemptMove;
		double partitionNi = 1.0 / partitionN;
		
		for(unsigned int i=0; i<sz; i++)
		{
			attemptMove = partitionNi * probs[i] * NumNTransferIn;	//this will be negative!
			eLevs[i]->AddEndOccStates(attemptMove);
			if(attemptMove>0)
				pt->cb.AddNumParticles(attemptMove);
			else
				pt->cb.RemoveNumParticles(-attemptMove);
			pt->netCharge -= attemptMove;
		}
	}

	if(NumPTransferIn > 0.0)
	{
		double partitionP = 0.0;
		unsigned int sz = pt->vb.energies.size;
		probs.resize(sz);
		double unocc;
		pt->vb.energies.GenerateSortPtr(eLevs);
		
		for(unsigned int i=0; i<sz; i++)
		{
			unocc = eLevs[i]->GetNumstates()-eLevs[i]->GetEndOccStates();
			probs[i] = unocc * exp(-KBI * eLevs[i]->eUse / pt->temp);
			partitionP += probs[i];
		}

		//now add the carriers back in
		double attemptMove, occ, numst;
		double partitionPi = 1.0 / partitionP;
		double summoved = 0.0;
		
		for(unsigned int i=0; i<sz; i++)
		{
			attemptMove = partitionPi * probs[i] * NumPTransferIn;
			occ = eLevs[i]->GetEndOccStates();
			numst = eLevs[i]->GetNumstates();
			if(occ + attemptMove > numst)
			{
				attemptMove = 0.75 * (numst - occ);	//move an amount that will work
				partitionP = 0.0;	//must reset the partition function
				for(unsigned int j=i+1; j<sz; j++)
					partitionP += probs[j];

				partitionPi = 1.0 / partitionP;	//update the inverse
				NumPTransferIn -= summoved;	//remove all the parts that have already been done
				NumPTransferIn -= attemptMove;	//take the number of carriers moved for this portion out of it
				summoved = -attemptMove;	//make sure sommoved is zero for the next iteration
			}
			summoved += attemptMove;

			eLevs[i]->AddEndOccStates(attemptMove);
			if(attemptMove>0)
				pt->vb.AddNumParticles(attemptMove);
			else
				pt->vb.RemoveNumParticles(-attemptMove);
			pt->netCharge += attemptMove;

		}
	}

	if(NumPTransferIn < 0.0)
	{
		double partitionP = 0.0;
		unsigned int sz = pt->vb.energies.size;
		probs.resize(sz);
		pt->vb.energies.GenerateSortPtr(eLevs);
	
		for(unsigned int i=0; i<sz; i++)
		{
			probs[i] = eLevs[i]->GetEndOccStates();
			partitionP += probs[i];
		}

		//now add the carriers back in
		double attemptMove;
		double partitionPi = 1.0 / partitionP;
		
		for(unsigned int i=0; i<sz; i++)
		{
			attemptMove = partitionPi * probs[i] * NumPTransferIn;	//this will be negative!
			eLevs[i]->AddEndOccStates(attemptMove);
			if(attemptMove>0)
				pt->vb.AddNumParticles(attemptMove);
			else
				pt->vb.RemoveNumParticles(-attemptMove);
			pt->netCharge += attemptMove;
		}
	}

	pt->contactData->electrontime.Add(NumNTransferIn, pt->adj.self);
	pt->contactData->netelectrontime += NumNTransferIn;
	pt->contactData->holetime.Add(NumPTransferIn, pt->adj.self);
	pt->contactData->netholetime += NumPTransferIn;

//	pt->ResetData();
	CalculateRho(pt);	//update rho - this should be really flipping close to zero.
	mdl.poissonRho[pt->adj.self] = pt->rho;	//update the poisson equation

	*/

	return(0);
}

int ChargeTransfer::Init(Simulation* sim)
{
//	timestep = sim->timestep;	//just don't do timestep until the very end and multiply the totals?
	simtime = sim->TransientData->simtime;
	dd = 0.0;
	scat = 0.0;
	srh1 = 0.0;
	srh2 = 0.0;
	srh3 = 0.0;
	srh4 = 0.0;
	gen = 0.0;
	rec = 0.0;
	ddTrans = 0.0;
	scatTrans = 0.0;
	srh1Trans = 0.0;
	srh2Trans = 0.0;
	srh3Trans = 0.0;
	srh4Trans = 0.0;
	genTrans = 0.0;
	recTrans = 0.0;
	LTgen = EMrec = 0.0;
	LTgenTrans = EMrecTrans = 0.0;
	LTsrh = EMsrh = 0.0;
	LTsrhTrans = EMsrhTrans = 0.0;
	LTdef = EMdef = 0.0;
	LTdefTrans = EMdefTrans = 0.0;
	LTscat = EMscat = 0.0;
	LTscatTrans = EMscatTrans = 0.0;

	return(0);
}

void ChargeTransfer::MultiplyTimeStep(double tStep)
{
	dd = dd * tStep;
	scat = scat * tStep;
	srh1 = srh1 * tStep;
	srh2 = srh2 * tStep;
	srh3 = srh3 * tStep;
	srh4 = srh4 * tStep;
	gen = gen * tStep;
	rec = rec * tStep;
	LTsrh = LTsrh * tStep;
	LTgen = LTgen * tStep;
	LTdef = LTdef * tStep;
	LTscat = LTscat * tStep;
	EMsrh = EMsrh * tStep;
	EMrec = EMrec * tStep;
	EMdef = EMdef * tStep;
	EMscat = EMscat * tStep;

	ddTrans = ddTrans * tStep;
	scatTrans = scatTrans * tStep;
	srh1Trans = srh1Trans * tStep;
	srh2Trans = srh2Trans * tStep;
	srh3Trans = srh3Trans * tStep;
	srh4Trans = srh4Trans * tStep;
	genTrans = genTrans * tStep;
	recTrans = recTrans * tStep;
	LTsrhTrans = LTsrhTrans * tStep;
	LTgenTrans = LTgenTrans * tStep;
	LTdefTrans = LTdefTrans * tStep;
	LTscatTrans = LTscatTrans * tStep;
	EMsrhTrans = EMsrhTrans * tStep;
	EMrecTrans = EMrecTrans * tStep;
	EMdefTrans = EMdefTrans * tStep;
	EMscatTrans = EMscatTrans * tStep;

	return;
}



ELevelRate::ELevelRate(ELevel* src, ELevel* trg, int typei, bool carrieri, bool sourcecarrieri, bool targetcarrieri, double netratei, double rout, double rin, double rtype, double tstep, double dt, double dt2, double ds)
{
	source = src;
	target = trg;
	type = typei;
	carrier = carrieri;
	sourcecarrier = sourcecarrieri;
	targetcarrier = targetcarrieri;
	netrate = netratei;
	RateOut = rout;
	RateIn = rin;
	RateType = rtype;
	desiredTStep = tstep;
	deriv = dt;
	deriv2 = dt2;
	derivsourceonly = ds;
	CancellingRate = NULL;

}

ELevelRate::ELevelRate()
{
	source = NULL;
	target = NULL;
	CancellingRate = NULL;
}
ELevelRate::~ELevelRate()
{
	source = NULL;
	target = NULL;
	CancellingRate = NULL;
}

ELevelRate ELevelRate::operator+(const ELevelRate &b)
{
	ELevelRate tmp;
	tmp.source = source;
	tmp.target = target;
	tmp.type = type;
	tmp.carrier = carrier;
	tmp.netrate = netrate + b.netrate;
	tmp.RateOut = RateOut + b.RateOut;
	tmp.RateIn = RateIn + b.RateIn;
	tmp.RateType = RateType + b.RateType;
	tmp.deriv = deriv + b.deriv;
	tmp.deriv2 = deriv2 + b.deriv2;
	tmp.derivsourceonly = derivsourceonly + b.derivsourceonly;
	if (desiredTStep < 0.0 && b.desiredTStep < 0.0)
		tmp.desiredTStep = INFINITETSTEP;
	else if (desiredTStep < 0.0)
		tmp.desiredTStep = b.desiredTStep;
	else if (b.desiredTStep < 0.0)
		tmp.desiredTStep = desiredTStep;
	else //both had valid timesteps, so just scale it.
	{
		if (tmp.netrate == 0.0)
			tmp.desiredTStep = INFINITETSTEP;
		else if (netrate != 0.0)
			tmp.desiredTStep = (desiredTStep * netrate) / tmp.netrate;
		else
			tmp.desiredTStep = INFINITETSTEP;
	}

	return(tmp);
}


bool ELevelRate::DetermineNumCarrierTransfer(double& numCar, double NRate)
{
	if (source == NULL)
	{
		ProgressString("Determining number of carriers, but no source state defined");
		return(false);	//don't limit by availabilty. THough honestly, it doesn't matter
	}
	double srcElec, srcHole;
	double trgElec, trgHole;
	double srcNS = source->GetNumstates();

	bool backtoNull = false;
	if (target == NULL)	//it is going to a contact, which is a duplicate point
	{
		target = source;
		backtoNull = true;	//after this is done, make target null again. (flag)
	}
	double trgNS = target->GetNumstates();

	bool LimitByAvailability = false;
	srcElec = source->GetBeginElec();
	srcHole = source->GetBeginHole();
	trgElec = target->GetBeginElec();
	trgHole = target->GetBeginHole();
	numCar = source->GetBeginOccStates();

	if (source->imp)
	{
		if (source->imp->isCBReference())
		{
			numCar = srcElec;	//default value for donor as the source
			if (source->getPoint().largestOccES * LEVELINSIGNIFICANT > srcElec)	//this is an insignificant donor
			{
				if (target->imp)
				{
					if (target->imp->isCBReference()) //source/target both donors
					{
						if (target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)
							numCar = source->getPoint().largestOccES;
						else if (NRate > 0.0) //insig source, sig target, source gains carriers
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccES;
					}
					else //the target is an acceptor
					{
						if (target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//both insignificant
							numCar = source->getPoint().largestOccES;	//just extend timestep
						else if (NRate > 0.0) //insig source, sig target, source gains carriers. Limit by what it can accept
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccES;
					}	//end target being an acceptor
				} //end target being an impurity
				else //target is in band
				{
					if (target->carriertype == ELECTRON) //target is in CB
					{
						if (target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)
							numCar = source->getPoint().largestOccES;
						else if (NRate > 0.0)
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccES;
					}
					else //target carrier is VB
					{
						if (target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
							numCar = source->getPoint().largestOccES;	//insignificant VB & donor, just increase timestep
						else if (NRate > 0.0)
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccES;
					}	//end target being in VB
				} //end target being in band
			} //end source being insignificant
		} //end source being a donor
		else //the source impurity is an acceptor
		{
			numCar = srcHole;	//default value for acceptor as the source
			if (source->getPoint().largestUnOccES * LEVELINSIGNIFICANT > srcHole)	//this is an insignificant donor
			{
				if (target->imp)
				{
					if (target->imp->isCBReference()) //source/target both donors
					{
						if (target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)
							numCar = source->getPoint().largestUnOccES;
						else if (NRate > 0.0) //insig source, sig target, source gains carriers
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestUnOccES;
					}
					else //the target is an acceptor
					{
						if (target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//both insignificant
							numCar = source->getPoint().largestOccES;	//just extend timestep
						else if (NRate > 0.0) //insig source, sig target, source gains carriers. Limit by what it can accept
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestUnOccES;
					}	//end target being an acceptor
				} //end target being an impurity
				else //target is in band
				{
					if (target->carriertype == ELECTRON) //target is in CB
					{
						if (target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)
							numCar = source->getPoint().largestUnOccES;
						else if (NRate > 0.0)
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestUnOccES;
					}
					else //target carrier is VB
					{
						if (target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
							numCar = source->getPoint().largestUnOccES;	//insignificant VB & donor, just increase timestep
						else if (NRate > 0.0)
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestUnOccES;
					}	//end target being in VB
				} //end target being in band
			} //end source being insignificant
		} //end source being an acceptor
	} //end source being an impurity
	else //it is not an impurity or defect
	{
		if (source->carriertype == ELECTRON) //source is the conduction band
		{
			if (source->getPoint().largestOccCB * LEVELINSIGNIFICANT > srcElec)	//the source is insignificant
			{
				if (target->imp)
				{
					if (target->imp->isCBReference()) //target is donor
					{
						if (target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)	//the donor has a comparatively insignificant number of carriers
							numCar = source->getPoint().largestOccCB;
						else if (NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccCB;
					}
					else //target is acceptor
					{
						if (target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//the acceptor has a comparatively insignificant number of carriers (holes)
							numCar = source->getPoint().largestOccCB;
						else if (NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else //CB is insignificant, acceptor is significant, but carriers leaving the CB, so just maintain high time step and cap transfer in source
							numCar = source->getPoint().largestOccCB;
					}
				}
				else
				{
					if (target->carriertype == ELECTRON)
					{
						if (target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)	//both are insignificant, just increase the timestep and cap limits will keep things from blowing up
							numCar = source->getPoint().largestOccCB;
						else if (NRate > 0.0)	//electrons are entering the insignificant source from a significant target
						{
							numCar = srcHole;	//there are this many places that can accept electrons
							LimitByAvailability = true;
						}
						else //an insignificant source is sending carriers to a significant spot. There is a tiny timestep on the source(which doesn't matter), and a huge timestep on the target, so just increase the source timestep
							numCar = source->getPoint().largestOccCB;
					}
					else //transition is from CB to VB 
					{
						if (target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
							numCar = source->getPoint().largestOccCB;
						else if (NRate > 0.0) // the CB is gaining electrons
						{
							numCar = srcHole;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccCB;
					}	//end target not being an impurity
				}	//end target not being an impurity
			}	//end saying the source is insignificant
		}	//end saying the source is the in the conduction band
		else // source is the valence band
		{
			if (source->getPoint().largestOccVB * LEVELINSIGNIFICANT > srcHole)	//the source is an insignificant position
			{
				if (target->imp)
				{
					if (target->imp->isCBReference())	//target is donor
					{
						if (target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)	//the donor has a comparatively insignificant number of carriers
							numCar = source->getPoint().largestOccVB;
						else if (NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccVB;
					}
					else //target is acceptor
					{
						if (target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//the acceptor has a comparatively insignificant number of carriers (holes)
							numCar = source->getPoint().largestOccVB;
						else if (NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else //CB is insignificant, acceptor is significant, but carriers leaving the CB, so just maintain high time step and cap transfer in source
							numCar = source->getPoint().largestOccVB;
					}
				}
				else //the target is in the CB or VB
				{
					if (target->carriertype == ELECTRON) //target is CB
					{
						if (target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec) //target & source are insignificant
							numCar = source->getPoint().largestOccVB;
						else if (NRate > 0.0)	//the source of holes is gaining more holes, and source is insignificant while target is significant
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else //insignificant source of holes is sending holes out to a significant concentration - will not have large effect, just increase timestep
							numCar = source->getPoint().largestOccVB;
					}
					else //target is VB
					{
						if (target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
							numCar = source->getPoint().largestOccVB;
						else if (NRate > 0.0) //a strong hole is sending holes
						{
							numCar = srcElec;
							LimitByAvailability = true;
						}
						else
							numCar = source->getPoint().largestOccVB;
					}	//end saying target is valence band
				}	//end saying target is in a band
			}	//end saying the source is an insignificant point
		}	//end saying the source is the valence band
	}	//end saying the source is a band

	if (backtoNull)
		target = NULL;

	return(LimitByAvailability);
}
/*
ELevelOpticalRate::ELevelOpticalRate()
{
sourcecarrier = false;
targetcarrier = false;
netrate = 0.0;
RateOut = 0.0;
RateIn = 0.0;
desiredTStep = INFINITETSTEP;
deriv = 0.0;
deriv2 = 0.0;
source = target = NULL;
type = 0;
carrier = false;
}

ELevelOpticalRate::~ELevelOpticalRate()
{
sourcecarrier = false;
targetcarrier = false;
netrate = 0.0;
deriv = 0.0;
source = target = NULL;
type = 0;
carrier = false;
}

//these rates are assumed to have matching sort values for this to be done
ELevelOpticalRate ELevelOpticalRate::operator+(const ELevelOpticalRate &b)
{
ELevelOpticalRate tmp;

tmp.netrate = netrate + b.netrate;
tmp.RateOut = RateOut + b.RateOut;
tmp.RateIn = RateIn + b.RateIn;
tmp.RateType = RateType + b.RateType;
tmp.deriv = deriv + b.deriv;
tmp.deriv2 = deriv2 + b.deriv2;
tmp.derivsourceonly = derivsourceonly + b.derivsourceonly;
if(desiredTStep < 0.0 && b.desiredTStep < 0.0)
tmp.desiredTStep = INFINITETSTEP;
else if(desiredTStep < 0.0)
tmp.desiredTStep = b.desiredTStep;
else if(b.desiredTStep < 0.0)
tmp.desiredTStep = desiredTStep;
else //both had valid timesteps, so just scale it.
{
if(tmp.netrate == 0.0)
tmp.desiredTStep = INFINITETSTEP;
else if(netrate != 0.0)
tmp.desiredTStep = (desiredTStep * netrate) / tmp.netrate;
else
tmp.desiredTStep = INFINITETSTEP;
}

return(tmp);
}
*/
ELevelRate& ELevelRate::operator+=(const ELevelRate &b)
{
	netrate += b.netrate;
	RateOut += b.RateOut;
	RateIn += b.RateIn;
	RateType += b.RateType;
	deriv += b.deriv;
	deriv2 += b.deriv2;
	derivsourceonly += b.derivsourceonly;
	if (desiredTStep < 0.0 && b.desiredTStep < 0.0)
		desiredTStep = INFINITETSTEP;
	else if (desiredTStep < 0.0)
		desiredTStep = b.desiredTStep;
	else if (b.desiredTStep < 0.0)
		desiredTStep = desiredTStep;
	else //both had valid timesteps, so just scale it.
	{
		if (netrate == 0.0)
			desiredTStep = INFINITETSTEP;
		else if (netrate != 0.0)
			desiredTStep = (desiredTStep * netrate) / netrate;
		else
			desiredTStep = INFINITETSTEP;
	}

	return(*this);
}