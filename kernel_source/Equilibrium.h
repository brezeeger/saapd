#ifndef EQUIL_H
#define EQUIL_H

#define CALCTIME 0
#define CALCPX 1	//used for state variable
#define CALCMX 2
#define CALCPX2 3
#define CALCMX2 4
#define CALCPY 5
#define CALCMY 6
#define CALCPY2 7
#define CALCMY2 8
#define CALCSTART 9
#define POSITIVE true
#define NEGATIVE false

class ModelDescribe;
class Model;
class Point;
class Simulation;
class ELevel;
class kernelThread;

int ThermalEquilibrium(kernelThread* kThread, ModelDescribe& mdesc, Model& mdl);
int EFieldThermalEq(ModelDescribe& mdesc, Model& mdl);
int EquilibratePoint(Point* pt);

double GetPointRho(Point& pt, double Ef);
double GetPoissonDependency(Point* sourcePt, Point* targetPt);
double GetEfZero(Point& pt, Model& mdl, Simulation& sim);

//int ThermalEquilibrium2(ModelDescribe& mdesc, Model& mdl);
int UpdateBD(Point& pt, Model& mdl, double netChargeChange);
int InitializePoint(Point& pt);
double CalcOccByFermiBand(ELevel* eLev, double Fermi, double kbTi, double CBMin, double VBMin, bool& oppOcc);
double CalcPointBandFast(Point& pt, Model& mdl, bool IncludeFermi=false, double accuracy=1.0e-6);



#endif


