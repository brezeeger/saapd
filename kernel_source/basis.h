#ifndef BASIS
#define BASIS

#include "BasicTypes.h"	//must be here because XSpace is not pointers. Don't want them to be.

class Basis
{
	XSpace basis1;
	XSpace basis2;
	XSpace basis3;
	void SetOrthogonal();	//create orthonormal basis
	void Normalize();
public:
	void SetBasis(XSpace pos);
	Basis(XSpace dif);
	Basis();
	~Basis();
	XSpace ReturnInBasis(XSpace pos);
	XSpace ReturnBasis(int num);
};

#endif