#include "transient.h"

#include <ctime>
#include <iomanip>

#include "model.h"
#include "physics.h"
#include "RateGeneric.h"
#include "FindELevelSS.h"
#include "light.h"
#include "contacts.h"
#include "output.h"
#include "outputdebug.h"
#include "CalcBandStruct.h"
#include "SaveState.h"
#include "../gui_source/kernel.h"
#include "ss.h"
#include "Materials.h"

int TransientSimData::TransMain(kernelThread* kThread)
{
	int returnCode = 0;
	ModelDescribe& mdesc = *sim->mdesc;
	Model& mdl = *sim->mdlData;
	
//	CreateSSFromTransient(mdl, mdesc, kThread);	//now this will create everything needed to make the steady state data

	//before the transient starts, it needs to create steady state data
//	SS(mdesc, mdl, kThread);	//load up all the steady state values
	
	sim->simType = SIMTYPE_TRANSIENT;
	sim->curiter = 0;
	simtime = 0.0;
	sim->binctr = 0;
	sim->outputs.numinbin = 1;	//deprecate all that code - which probably applies more to steady state than the transient now
	outputCtr = 0;
	tolerance = sim->accuracy;
	//goal is to get things in steady state at short time frames, so there is no need to do all the time averaging

	time_t start = time(NULL);
	time_t end;
	time_t lastSaveState = start;

//	sim->curSSTarget = TargetTimes.begin()->second.SSTarget;
	SetStartingConditions(mdl, mdesc);	//for now, just the steady state for the initial conditions of the transient
//	UpdateAllTargetOccs(mdl, sim);	//relic from when steady state solution was used to figure out if things can move or not.
	//if loading a savestate, the simtime might not start out at zero
	tStepControl.FirstInit(this);
	kThread->shareData.ss = false;
	sim->groupNames.push_back("trans");
#ifndef NDEBUG
	RecordTimestep(true);
#endif
	for (; simtime < simendtime; sim->curiter++)	//no initialization. Done in case 0
	{
		if(sim->curiter > sim->maxiter && sim->maxiter > 0)
			break;	//get out of loop

		end = kThread->TransientKernelInteract(sim, start, lastSaveState);

		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return (-1);

		
		returnCode = 0;

		tStepControl.SetupIteration(this);	//this selects the timestep based on any flags that had been previously set
		InitIteration();	//basically just calculates the band structure with the newest values
		//this must be done everywhere before getting all the rates, unfortunately. Loops through all ELevels. Set End->Begin
		//do all the light rate calculations - these need to be separate, and occurs after ValidateTimeStep because the timestep can only
		//become less (and is therefore still valid)
		//ExecuteLight(mdl, mdesc);

		

		//set up all the timestep constants needed for projecting what the next timestep should be calculated to be
		tstepConsts.prev2timestep = tstepConsts.prevtimestep;
		tstepConsts.prevtimestep = timestep;
		tstepConsts.deltaTM2Inv = tstepConsts.deltaTM1Inv;
		tstepConsts.deltaTM1Inv = timestepi;
		tstepConsts.TM1PlusTM2 = tstepConsts.prev2timestep + tstepConsts.prevtimestep;
		if(tstepConsts.TM1PlusTM2 > 0.0)
			tstepConsts.TM1PlusTM2Inv = 1.0 / tstepConsts.TM1PlusTM2;
		else
			tstepConsts.TM1PlusTM2Inv = 0.0;

		//	if(sim->ForceAlterPercent)
		//		DisplayProgress(52,0);



//		ForceAlterPercent = simendtime;	//reset for the rates.
//		AlterPercentCarrier = true;	//this defaults to true, and gets set to false if there are ANY energy levels
		//that have unused rates.

		CalcAllRates(mdl, sim);	//sets ForceAlterPercent tstep and resets AlterPercentCarrier(passive). sim->nexttimestep set internally
		Light(mdl, mdesc);	//do the light now that the recombination rates (light sources) has been processed

		//now I can figure out what the timestep will be. Dammit. Light has to be after CalcAllRates. Light does not always exist.

		tStepControl.IterationUpdateInternal(sim);	//determine what the actual time step will be
		if (tStepControl.SafeOutput(simtime))
			OutputDataInternalPre(mdl, 1.0, 0);	//doing away with the whole binning thing

		MoveTime();	//this actually moves everything. updates point outputs/variables/stuff needed for poisson calculation

		if (tStepControl.ignoreRestIteration() && !tStepControl.SafeOutput(simtime))	//we don't really care about anything that happens here
			continue;
		
		timesteps.push_back(tStepControl.getUseTStep());	//save the timestep
		TestTargetTimes();	//save state and test if switching to a new environment. If so, update the light.
		

		OutputData(false);	//don't force it. If the time step conditions aren't write, it won't
//		ChargeTransfer chgT;
//		testMNRate = curmaxNetRate;

		//now that we have the timestep determined, just go through and update all the states for next iteration. Outputs
		//were put in when the calculation completed due to AddELevelRate.
		/*

		bool isGood = UpdateAllRates(mdl, mdesc.simulation, chgT);	//this will get the timestep figured out, and update all the eLevel rate in/out/net rate based on the time step control
		while (isGood == false)
		{
			//need to run through everything with a smaller timestep than before
			mdesc.simulation->TransientData->redos++;
			testMNRate = prevMNRate;	//it was bad
			returnCode = returnCode | 1;	//set the one bit
			isGood = TrySmallerTStep(chgT);
		}
		*/

		/*
		if(sim->AlterPercentCarrier || sim->ForceAlterPercent < sim->nexttimestep)	//make it have an effect on this time step
		{
		if(sim->TransientData->percentCarrierTransfer > MIN_PERCENT_TRANSFER)	//only matter if it can still go down
		{
		sim->TransientData->percentCarrierTransfer = sim->TransientData->percentCarrierTransfer * REDUCE_PERCENT_TRANSFERAGE;	//knock down for maxChange in CalcTransfer
		DisplayProgressDoub(13, sim->TransientData->percentCarrierTransfer);	//sends to screen
		ProgressDouble(40, sim->TransientData->percentCarrierTransfer);	//sends to trace file
		RecalculateAllTSteps(mdl, sim);
		sim->AlterPercentCarrier = true;	//make sure it's true so it resets all the rates!
		}
		}
		*/
//		SetupTimeStep();	//checks to see if nexttimestep is too large and shrinks it if necessary
		//verify timestep OK
		//chgT.MultiplyTimeStep(timestep);

//		RecordAll(mdl, mdesc, chgT);	//Adjust ELevel (net) Rate In/Out, and report desired timesteps down
		//	FixOverSteps(mdl, mdesc);
		//	sim->NoSS.Clear();	//DEBUG - clear out points/rate types that aren't in steady state (will limit timestep)
		//	RecordNonSteadyState(sim, false);	//debug to determine where the hold-up is in calculations.


		//output the data
		
		/*
		if (mdesc.simulation->TransientData->SSByTransient)
		{
			mdesc.simulation->curiter--;	//don't increment this
			mdesc.simulation->binctr = 0;	//it's ALWAYS zero
			//the time already is staying the same... unless it's SS.
			if (mdesc.simulation->TransientData->isSS)
			{
				mdesc.simulation->TransientData->iterEndtime = mdesc.simulation->TransientData->curTargettime;
				mdesc.simulation->SignificantMilestone=true;
			}
		}
		*/
		

#ifndef NDEBUG
		RecordTimestep(false);	//debugging purposes
#endif
//		tstepctr += 1.0;//used for sending info to kThread GUI
//		avgTStep += timestep;

		/*

		if (SSByTransient)
		{
			if (monTStep.isMaxedOut(SSbyTTime))	//let it go back to low values if it's gone through everything already
			{
				ResetAlterPercent = true;
			}
			if (isSS)	//finding ss by transient and it was found
			{
				sim->curiter++;	//undo the undo increment so it can start a new file for the next SS values
				DisplayProgress(65,0);
			}
		}

		*/

		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
		{
			DisplayProgressDoub(12, iterEndtime);
			ProgressInt(42, sim->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			
			return(0);
		}

		if(kThread->shareData.GUIsaveState)
		{
			DisplayProgressDoub(12, iterEndtime);
			ProgressInt(42, sim->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			kThread->shareData.GUIsaveState = false;	//reset boolean
			lastSaveState = end;	//end was the updated time Reset it.
		}

		
		if(returnCode & 1)
		{
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, REDO_CALCS + redos);
			redos = 0;
		}


		simtime = iterEndtime;	//move the clock up. Endtime is adjusted in DoRates		
	}	//end primary for loop

	InitIteration();	//this will clear out the sim optical values and whatnot
	CalcAllRates(mdl, sim);
//	OutputDataFinal(mdl, *(mdesc.simulation));	//just get updated outputs that are instantaneous
	Light(mdl, mdesc);
	
	
//	OutputDataToFileTransient(mdl, mdesc);

	OutputDataInternalPre(mdl, 1.0, 0);
	OutputData(true);	//nothing moved, so this is still up to date
	


	
	return(returnCode);
}

void TimeStepControl::FirstInit(TransientSimData* tsd, bool envChanged) {
	targetTStep = 1.0e-30;	//just start off small
	maxTStepUsed = targetTStep;
	state = RatesAdded;	//this should treat it as a maximum time step, all rates added, and nothing changed
	maxStepTolerance = tsd->tolerance * 1048576.0;
	if (maxStepTolerance > 0.01)
		maxStepTolerance = 0.01;
	activeTolerance = maxStepTolerance;
	numLevelsDown = 0;
	if (envChanged == false)
		startTimeData = 0.0;

	//don't care about the count, active, cutoffs, tstepmin, tstepmax, activemustbegreater, and numrepeatbeforenextstep
	//those are all deprecated from a previous algorithm
}

void TransientSimData::OutputData(bool force) {
	if (simtime < timeignore && !force)
		return;	//don't output any damn data. we're ignoring this bit

	if (!tStepControl.SafeOutput(simtime) && iterEndtime < simendtime && !sim->SignificantMilestone && !force)	//once again, not safe to actually output anything
		return;

	
	Model& mdl = *sim->mdlData;
	ModelDescribe &mdesc = *sim->mdesc;

	//transfer all the optical data assuming it exists
	sim->AggregateOpticalData(1.0);
	for (unsigned int i = 0; i<mdesc.materials.size(); i++)
		mdesc.materials[i]->AggregateOpticalData(1.0);

	//we're going to want subsequent numbers
	if (sim->outputs.OutputAllIndivRates)
	{
		std::stringstream fname;
		fname << sim->OutputFName << "_" << outputCtr;
		WriteEnergyStates(*sim->mdlData, fname.str());	//debug to write out states!
	}

	OutputDataInternalPost(mdl, *sim, 0);
	OutputDataToFile();	//this does the band diagrams


	sim->SignificantMilestone = false;	//always just reset.

	timesteps.clear();
	outputCtr++;
	/*
	if (sim->SignificantMilestone)	//determined via
	{
		if (sim->binctr != 0)	//output the data if it hasn't been output yet when hitting a target point
		{
			OutputDataToFileTransient(mdl, mdesc);	//send the data to file and clear from memory
			sim->binctr = 0;	//reset binctr
		}
		else if (isSS)
		{
			OutputDataInternalPost(mdl, *(mdesc.simulation), 0);
			sim->binctr = 1;
			OutputDataToFileTransient(mdl, mdesc);
			sim->binctr = 0;
			RecTimestepDistribution(mdesc.simulation, true);	//reset the file
		}

		sim->SignificantMilestone = false;
		AlterPercentCarrier = true;	//need to clear out all the old rate limitations
		ResetAlterPercent = true;

		//there is potentially a very drastic change in the environment, reset the timesteps - it checks for inverse timesteps being <= 0.0 (inverses are passed down each iteration)
		timestepi = 0.0;	//so this will be passed into sim->tstepConsts.deltaTM1Inv
		percentCarrierTransfer = INIT_PERCENT_TRANSFER;// / REDUCE_PERCENT_TRANSFERAGE;	//significant external environment change, get rough solution, but start with original
		//because alterpercentcarrier will knock it back to a particular value.
		//the divide is to make sure it goes to the initial when it will then multiply by RR%
	}
	else
	{
		ResetAlterPercent = false;	//this is definitely needed...
	}

	if(sim->curiter%2000==1999)
	{
	RecTimestepDistribution(mdesc.simulation, true);	//reset the file
	//			OutputCharge(mdesc.simulation, true);	//reset the .charge file to keep from taking up too much space
	//the charge file isn't so bad when it is only updated with other output files or screen refresh
	}
	*/
}

void TransientSimData::TestTargetTimes() {
	if (TargetTimes.size() > 0)	//this should always exist, but be safe
	{
		if (iterEndtime >= curTargettime)	//it hit the next target time step
		{
			std::map<double, TargetTimeData>::iterator ptrTT = TargetTimes.begin();
			bool save = ptrTT->second.SaveState;
			sim->SignificantMilestone = true;
			TargetTimes.erase(ptrTT);	//there should only be one instance, so remove it
			if (TargetTimes.size() > 0)	//there's more target times
			{
				curTargettime = TargetTimes.begin()->first;
				unsigned int oldTrg = sim->curSSTarget;
				sim->curSSTarget = TargetTimes.begin()->second.SSTarget;
				if (oldTrg != sim->curSSTarget)
				{

				}
			}

			if (save)	//is save state enabled for this particular target time
			{
				DisplayProgressDoub(12, iterEndtime);
				ProgressInt(42, sim->curiter);
				SaveState(*sim->mdlData , *sim->mdesc);
				DisplayProgress(40, 0);
			}

			UpdateLightEnable(*sim->mdesc);	//the spectrums could have changed, so check to see if any enable or disable
			tStepControl.FirstInit(this, true);	//make it rset as things could have abruptly changed.
		}
		//this should result in something...
	}
}

void TimeStepControl::UpdateActiveTolerance(double smallestT) {
	if (smallestT < 1.0e-12)
		smallestT = 1.0e-12;	//this is absurdly small. Prevent user from killing themselves on the simulation

	if (maxTStepUsed > 0.0 && targetTStep < maxTStepUsed)	{ //it's 0 on the first iterations! 
		activeTolerance = maxStepTolerance * targetTStep / maxTStepUsed;
		if (activeTolerance < smallestT)
			activeTolerance = smallestT;
	}
	else
		activeTolerance = maxStepTolerance;

	if (maxStepTolerance < smallestT)	//just for good measure on the reporting
		maxStepTolerance = smallestT;	//even though the used value is definitely safe
}

void TimeStepControl::SetupIteration(TransientSimData* tsd) {
	if (tsd == nullptr)
		return;

	double smallestAccuracy = tsd->tolerance;
	Simulation* sim = tsd->getSim();

	double tempfreq = 0.0;
	double maxfreq = 0.0;
	for (unsigned int j = 0; j<sim->contacts.size(); j++)	//update all the contact voltages as they have changed to new values...
	{
		tempfreq = sim->contacts[j]->GetFrequency();
		if (tempfreq > maxfreq)
			maxfreq = tempfreq;
	}

	

	TimeToNextMilestone = tsd->curTargettime - tsd->simtime;
	RemoveState(ClearMaxSignFlip | ClearFallSignFlip);	//only want these to exist through one iteration
	//and they're about to be set below if needed.


	/*
	This method works, but not as well as the other method
	
	if (isDroppingStep()) {
		timeFallDownLeft -= useTStep;
		if (isAllRatesIncluded()) {
			if (isAnyOccupancyChanges()) {
				if (isAllSmallChanges()) {
////					DecreaseTargetTStep();

				}
				
			}
			else {	//dropping, all rates included, nothing changed.
				//ready for another large time step
////				tsd->simtime -= useTStep;	//get rid of the non-event time step time wise from last iteration
				RemoveState(TStepFalling);
				targetTStep = maxTStepUsed;
			}
		}
		else { //it's dropping, not all rates are included
			if (isAnyOccupancyChanges()) { //same as before
				if (isAllSmallChanges());
////					DecreaseTargetTStep();
				//else maintain time step
			}
			else { //dropping, not all rates included, nothing changed
				//hasn't dropped enough to get to any active rates.
////				tsd->simtime -= useTStep;	//undo the time spent to change nothing
////				DecreaseTargetTStep();	//nothing changed, so just try moving forward with smaller iterations
				while (targetTStep < timeFallDownLeft)
					timeFallDownLeft -= useTStep;	//should be around 8 iterations or less. just skip doing a bunch of no action iterations
				//note, useTStep is <= targetTStep
			}
		}
		
		if (targetTStep >= timeFallDownLeft)	//this will act as a check on the falling to make sure it keeps
			DecreaseTargetTStep();	//going down. Basically prevents the thing from getting stuck.
		if (timeFallDownLeft < 0.0) {
			RemoveState(TStepFalling);
			targetTStep = maxTStepUsed;
		}

	}
	else if(isAnyRatesAdded()) {	//is at the maximum time step, and it tried to do something
		if (isAnyOccupancyChanges()) {
			if (isAllSmallChanges()) {
				//everything changed, but not very well.
				//either tighten the tolerance to get the resolution we want or it's time for increasing the time step again
				if (maxStepTolerance <= smallestAccuracy) {
					IncreaseTargetTStep();	//this induced a change on par with the restrictions set forth
	//				maxStepTolerance = smallestAccuracy * 1048576.0;	//reset the tolerance
				}
				else {
					DecreaseMaxTolerance();
				}
			}
			else {	//at current max time step, did something, and that something was significant.
				//rectify any numerical inconsistencies from taking the large time step
				SetFallingTimestep();
				//now mess with the tolerance
				DecreaseTargetTStep();
			}
		}
		else {	//it actually failedto change any occupancies when moving time forward
			if (targetTStep >= maxTStepAllowed) {
				if (isRatesExcluded()) { //at max time step, some rates were excluded, nothing chaged
					//therefore, this was a wasted iteration. Can't increase the timestep to probe slower rates, so just work
					//through the faster rates - force via the dropping flag
////					tsd->simtime -= useTStep;
					SetFallingTimestep();
					DecreaseTargetTStep();
					
				}
				else {	//nothing was excluded. At max step. Nothing changed. Just burn through many iterations of nothinginess, I guess
					SetState(AtSteadyState);
				}
			}
			else {	//the time step can still increase. It did nothing, so just move back the time and try again with a larger time step
////				tsd->simtime -= useTStep;
				IncreaseTargetTStep();
//				maxStepTolerance = smallestAccuracy * 1048576.0;	//basically start off with 6 digits less accuracy than desired.
			}
		}
		timeFallDownLeft = useTStep*0.5;
	}
	else {	//at the maximum time step. no rates were added so the time step is too large to do anything, so this was a nothing opp
////		tsd->simtime -= useTStep;
		//go back to working on a smaller timestep
		SetFallingTimestep();
		DecreaseTargetTStep();
		timeFallDownLeft = useTStep;
	}
	*/

	if (isDroppingStep()) {
		if (isAllRatesIncluded()) {
			if (isAnyOccupancyChanges()) {
				if (isAllSmallChanges()) {
					DecreaseTargetTStep();

				}
				
			}
			else {	//dropping, all rates included, nothing changed.
				//ready for another large time step
				tsd->simtime -= useTStep;	//get rid of the non-event time step time wise from last iteration
				RemoveState(TStepFalling);
				targetTStep = maxTStepUsed;
			}
		}
		else { //it's dropping, not all rates are included
			if (isAnyOccupancyChanges()) { //same as before
				if (isAllSmallChanges())
					DecreaseTargetTStep();
				//else maintain time step
			}
			else { //dropping, not all rates included, nothing changed
				//hasn't dropped enough to get to any active rates.
				tsd->simtime -= useTStep;	//undo the time spent to change nothing
				DecreaseTargetTStep();	//nothing changed, so just try moving forward with smaller iterations
				//note, useTStep is <= targetTStep
			}
		}
	}
	else if(isAnyRatesAdded()) {	//is at the maximum time step, and it tried to do something
		if (isAnyOccupancyChanges()) {
			if (isAllSmallChanges()) {
				//everything changed, but not very well.
				//either tighten the tolerance to get the resolution we want or it's time for increasing the time step again
				if (maxStepTolerance <= smallestAccuracy) {
					IncreaseTargetTStep();	//this induced a change on par with the restrictions set forth
	//				maxStepTolerance = smallestAccuracy * 1048576.0;	//reset the tolerance
				}
				else {
					DecreaseMaxTolerance();
				}
			}
			else {	//at current max time step, did something, and that something was significant.
				//rectify any numerical inconsistencies from taking the large time step
				SetFallingTimestep();
				//now mess with the tolerance
				DecreaseTargetTStep();
			}
		}
		else {	//it actually failedto change any occupancies when moving time forward
			if (targetTStep >= maxTStepAllowed) {
				if (isRatesExcluded()) { //at max time step, some rates were excluded, nothing chaged
					//therefore, this was a wasted iteration. Can't increase the timestep to probe slower rates, so just work
					//through the faster rates - force via the dropping flag
					tsd->simtime -= useTStep;
					SetFallingTimestep();
					DecreaseTargetTStep();
					
				}
				else {	//nothing was excluded. At max step. Nothing changed. Just burn through many iterations of nothinginess, I guess
					SetState(AtSteadyState);
				}
			}
			else {	//the time step can still increase. It did nothing, so just move back the time and try again with a larger time step
				tsd->simtime -= useTStep;
				IncreaseTargetTStep();
//				maxStepTolerance = smallestAccuracy * 1048576.0;	//basically start off with 6 digits less accuracy than desired.
			}
		}

	}
	else {	//at the maximum time step. no rates were added so the time step is too large to do anything, so this was a nothing opp
		tsd->simtime -= useTStep;
		//go back to working on a smaller timestep
		DecreaseTargetTStep();

	}

	if (maxStepTolerance > 0.01)
		maxStepTolerance = 0.01;	//but don't allow humungous changes. that could be DISASTEROUS!

	if (targetTStep > maxTStepAllowed)
		targetTStep = maxTStepAllowed;

	if (targetTStep > maxTStepUsed) 
		maxTStepUsed = targetTStep;
		

	if (targetTStep > TimeToNextMilestone && TimeToNextMilestone > 1.0e-12)	//basically make sure it converges in the final moments before a milestone
		targetTStep = TimeToNextMilestone * 0.125;
	else if (targetTStep > TimeToNextMilestone)	//but also make sure it can cross it...
		targetTStep = TimeToNextMilestone;


	if (maxfreq > 0.0)	//limit the timestep so there are 10 iterations going to max/0/min/0 each
	{
		maxfreq = 0.025 / maxfreq;	//equiv to 1 / (40 * maxfreq), so maxfreq now holds 1/40th of the period
		if (targetTStep > maxfreq)
			targetTStep = maxfreq;
	}

	UpdateActiveTolerance(smallestAccuracy);
	
}

void Point::ApplyRates() {

	n = p = elecindonor = holeinacceptor = elecTails = holeTails = netCharge = 0.0;
	for (auto es = cb.energies.begin(); es != cb.energies.end(); ++es)
		n += es->second.TransferCarriersTransient(netCharge);
	cb.SetNumParticles(n);

	for (auto es = vb.energies.begin(); es != vb.energies.end(); ++es)
		p += es->second.TransferCarriersTransient(netCharge);
	vb.SetNumParticles(p);

	for (auto es = acceptorlike.begin(); es != acceptorlike.end(); ++es)
		holeinacceptor += es->second.TransferCarriersTransient(netCharge);

	for (auto es = donorlike.begin(); es != donorlike.end(); ++es)
		elecindonor += es->second.TransferCarriersTransient(netCharge);

	for (auto es = CBTails.begin(); es != CBTails.end(); ++es)
		elecTails += es->second.TransferCarriersTransient(netCharge);

	for (auto es = VBTails.begin(); es != VBTails.end(); ++es)
		holeTails += es->second.TransferCarriersTransient(netCharge);

	netCharge = DoubleAdd(netCharge, DoubleSubtract(p + holeTails + holeinacceptor, n + elecTails + elecindonor));
	rho = netCharge * QCHARGE * voli;
	mdl.poissonRho[adj.self] = rho;

	srhR1 *= voli;	//get rates in normal units instead of just # of carriers
	srhR2 *= voli;	//that moved/sec. While useful for seeing where steps are,
	srhR3 *= voli;	//having actual rates makes life a bit easier...
	srhR4 *= voli;
	genth *= voli;
	recth *= voli;
	scatterR *= voli;
	LightAbsorbDef *= voli;
	LightAbsorbSRH *= voli;
	LightAbsorbGen *= voli;
	LightAbsorbScat *= voli;
	LightEmitDef *= voli;
	LightEmitRec *= voli;
	LightEmitScat *= voli;
	LightEmitSRH *=  voli;

	if (contactData)
		ContactCurrent(*this, mdl.getMDesc(), mdl);


}

void TransientSimData::MoveTime() {
	if (sim == nullptr)
		return;
	Model* mdl = sim->mdlData;
	if (mdl == nullptr)
		return;

	PosNegSum charge;
	for (auto it = mdl->gridp.begin(); it != mdl->gridp.end(); ++it) {
		it->second.ApplyRates();
		charge.AddValue(it->second.netCharge);
	}

	sim->negCharge = charge.GetNegSum() * QCHARGE;
	sim->posCharge = charge.GetPosSum() * QCHARGE;

	sim->AggregateOpticalData(timestep);
	for (unsigned int i = 0; i<sim->mdesc->materials.size(); i++)
		sim->mdesc->materials[i]->AggregateOpticalData(timestep);
	//	OutputChargeTransfer(chgT, false);	//these haven't been adjusted by the timestep yet
	
	charge.Clear();
	//	OutputCharge(mdesc.simulation, false);

	if (sim->outputs.OutputRatePlots)
		PlotDRate_C(*mdl, *sim->mdesc, NULL);

//	RecTimestepDistribution(sim, false);
}


int SetStartingConditions(Model& mdl, ModelDescribe& mdesc, int condition, unsigned int whichSS)
{
	if(condition == START_UNDEFINED)
	{
		if(mdesc.simulation->TransientData)
		{
			condition = mdesc.simulation->TransientData->StartingCondition;
			if(condition >= 0)
			{
				whichSS = condition;
				condition = START_SS;
			}
		}
		else
			condition = START_SS_TZERO;	//by default, do steady state for conditions at time zero.
	}
	if(condition == START_SS_TZERO)
	{
		whichSS = 0;
		condition = START_SS;
	}
	double occ, avail;
	if(condition == START_SS && mdesc.simulation->SteadyStateData->ExternalStates.size() == 0)
	{
		condition = START_THEQ;
		ProgressCheck(66,false);
	}

	if(condition == START_SS)
	{
		for(unsigned int i=0; i<mdesc.simulation->SteadyStateData->ExternalStates.size(); ++i)
		{
			if(mdesc.simulation->SteadyStateData->ExternalStates[i]->id == whichSS)
			{
				whichSS = i;
				break;
			}
		}

		if(whichSS >= mdesc.simulation->SteadyStateData->ExternalStates.size())
		{
			ProgressInt(73, whichSS);
			whichSS = 0;	//it failed to find the proper external state, so just juse the first one
			ProgressCheck(67,false);
		}
		if (mdesc.simulation->SteadyStateData->ExternalStates.size() == 0) {
			condition = START_THEQ;
			return(-1);
		}
		for(std::map<long int, Point>::iterator gridp = mdl.gridp.begin(); gridp!=mdl.gridp.end(); ++gridp)
		{
			Point* curPt = &gridp->second;
			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->cb.energies.begin(); it != curPt->cb.energies.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->vb.energies.begin(); it != curPt->vb.energies.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->acceptorlike.begin(); it != curPt->acceptorlike.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->donorlike.begin(); it != curPt->donorlike.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->CBTails.begin(); it != curPt->CBTails.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->VBTails.begin(); it != curPt->VBTails.end(); ++it)
			{
				it->second.SetOccFromRelEf(RATE_OCC_BEGIN, it->second.TargetEfs[whichSS]);
				it->second.TransferBeginToEnd();
				it->second.SSTargetCleared=1;
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}
		}
	}
	else if(condition == START_THEQ)
	{
		for(std::map<long int, Point>::iterator gridp = mdl.gridp.begin(); gridp!=mdl.gridp.end(); ++gridp)
		{
			Point* curPt = &gridp->second;
			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->cb.energies.begin(); it != curPt->cb.energies.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->vb.energies.begin(); it != curPt->vb.energies.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->acceptorlike.begin(); it != curPt->acceptorlike.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->donorlike.begin(); it != curPt->donorlike.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->CBTails.begin(); it != curPt->CBTails.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->VBTails.begin(); it != curPt->VBTails.end(); ++it)
			{
				it->second.GetOccupancyOcc(RATE_OCC_THEQ, occ, avail);
				if(occ<avail)
					it->second.SetBeginOccStates(occ);
				else
					it->second.SetBeginOccStatesOpposite(avail);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}
		}
	}
	else if(condition == START_ABSZERO)
	{
		for(std::map<long int, Point>::iterator gridp = mdl.gridp.begin(); gridp!=mdl.gridp.end(); ++gridp)
		{
			Point* curPt = &gridp->second;
			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->cb.energies.begin(); it != curPt->cb.energies.end(); ++it)
			{
				it->second.SetBeginOccStates(0.0);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->vb.energies.begin(); it != curPt->vb.energies.end(); ++it)
			{
				it->second.SetBeginOccStates(0.0);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->acceptorlike.begin(); it != curPt->acceptorlike.end(); ++it)
			{
				it->second.SetBeginOccStates(it->second.GetNumstates());
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->donorlike.begin(); it != curPt->donorlike.end(); ++it)
			{
				it->second.SetBeginOccStates(it->second.GetNumstates());
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->CBTails.begin(); it != curPt->CBTails.end(); ++it)
			{
				it->second.SetBeginOccStates(0.0);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}

			for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->VBTails.begin(); it != curPt->VBTails.end(); ++it)
			{
				it->second.SetBeginOccStates(0.0);
				it->second.TransferBeginToEnd();
				it->second.partialNRate = it->second.partialNRateM1 = 0.0;
			}
		}
	}
	else
		return(-1);
	return(0);
}

int CreateSSFromTransient(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	if(mdesc.simulation->TransientData==NULL)	//need data to generate from
		return(0);

	
	if(mdesc.simulation->SteadyStateData == NULL)
		mdesc.simulation->SteadyStateData = new SS_SimData(mdesc.simulation);	//create the data
//		delete mdesc.simulation->SteadyStateData;	don't erase the steady state data

	


	SS_SimData* SimSS = mdesc.simulation->SteadyStateData;

	std::stringstream txt;

	TransientSimData* SimT = mdesc.simulation->TransientData;
	
	SSContactData tmpCtcData;
	//note, there is no target time of 0.0! Might need to create a starting point, but it by default
	//should be thermal equilibrium. Won't worry about that quite yet.
	double curTime = 0.0;
	unsigned int txtSize = unsigned int(log10(SimT->TargetTimes.size()+SimSS->ExternalStates.size()))+1;
	mdesc.simulation->simType = SIMTYPE_TRANSIENT;	//make sure it reads the correct contact data stuff
	unsigned int i=SimSS->ExternalStates.size();	//all the previous targetEf's are steady state
	int maxID = (SimSS->ExternalStates.size() > 0) ? SimSS->ExternalStates[0]->id : -1;
	for(unsigned int j=0; j<SimSS->ExternalStates.size(); ++j)
	{
		if(SimSS->ExternalStates[j]->id > maxID)
			maxID = SimSS->ExternalStates[j]->id;
	}
	
	double prevTime=0.0;
	for(std::map<double,TargetTimeData>::iterator tt = SimT->TargetTimes.begin(); tt != SimT->TargetTimes.end(); ++tt)
	{
		Environment* tmpExtState = new Environment(mdesc.simulation->SteadyStateData);
		//at every single target time, something should change.
		tmpExtState->ContactEnvironment.clear();
		tmpExtState->ActiveLightEnvironment.clear();
		tmpExtState->reusePrevContactEnvironment = -1;
		tmpExtState->reusePrevLightEnvironment = -1;
		tmpExtState->flags = SS_ENV_NONE;
		++maxID;
		tmpExtState->id = maxID;	//want this to be unique!
		tt->second.SSTarget = i;	//this is the index for targetEf's that we are looking for! Essentially a vector from [0,ExtStates.size()]
		txt.clear();
		txt.str("SS");
		txt << std::setw(txtSize) << std::setfill('0') << std::right << i;
		tmpExtState->FileOut = txt.str();

		
		SimT->simtime = (tt->first + prevTime)*0.5;
		SimT->curTargettime = tt->first;
		for (unsigned int ctc = 0; ctc < SimT->ContactData.size(); ++ctc)
			SimT->ContactData[ctc]->UpdateActive(SimT->simtime, true);

		SimT->DeterminePoissonValues();	//this defaults to using transient stuff before steady state stuff
		mdesc.simulation->PrepBoundaryConditions(mdl);	//THIS is what matters

		for(unsigned int j=0; j<mdesc.simulation->contacts.size(); ++j)
		{
			Contact* ctc = mdesc.simulation->contacts[j];
			tmpCtcData.Clear();
			tmpCtcData.ctc = ctc;
			tmpCtcData.ctcID = ctc->id;
			tmpCtcData.voltage = ctc->GetVoltage();
			tmpCtcData.current = ctc->GetCurrent();
			tmpCtcData.currentDensity = ctc->GetCurDensity();
			tmpCtcData.eField = ctc->GetEField();
			tmpCtcData.current_ED = ctc->GetEDCurrent();
			tmpCtcData.eField_ED = ctc->GetEDEField();
			tmpCtcData.currentDensity_ED = ctc->GetCurEDDensity();
			tmpCtcData.eField_ED_Leak = ctc->GetEFieldLeak();
			tmpCtcData.current_ED_Leak = ctc->GetCurrentDensityEDLeak();
			tmpCtcData.currentD_ED_Leak = ctc->GetCurrentDensityEDLeak();
			tmpCtcData.currentActiveValue = ctc->GetCurrentActive();
			tmpCtcData.suppressVoltagePoisson = ctc->SuppressVoltage();
			tmpCtcData.suppressCurrentPoisson = ctc->SuppressCurrent();
			tmpCtcData.suppressEFieldPoisson = ctc->SuppressEField();

			if(tmpCtcData.currentActiveValue != CONTACT_INACTIVE)
			{
				tmpCtcData.currentActiveValue = CONTACT_SS | tmpCtcData.currentActiveValue;
				tmpExtState->ContactEnvironment.push_back(new SSContactData(tmpCtcData));
			}
		}

		for(unsigned int j=0; j<mdesc.spectrum.size(); ++j)
		{
			if(mdesc.spectrum[j]->enabled)
				tmpExtState->ActiveLightEnvironment.push_back(mdesc.spectrum[j]->ID);
		}

		//guarantee the time such that it is working towards the current target - make sure everything is A-OK

		//must create/extract spectra used
		//set an output name
		//get the contact data (voltage, electric field, current, current density
		//external efield/current/density/and leaks. Is it a thermal bath?
		//should there be a current out of it? ID

		//transient contact children: ID
		//linear/cos (voltage/efield/current/density leaks), thermal bath, current out
		SimSS->ExternalStates.push_back(tmpExtState);
		i++;
		prevTime = tt->first;
	}

	if(kThread)
		kThread->shareData.ssFromTransient = true;	//make sure it doesn't exit when the steady state comes to completion

	return(1);
}

bool TransientSimData::TrySmallerTStep(ChargeTransfer& chgT)
{
	double tStep = timestep * 0.25;
	timestep = tStep;
	Model& mdl = *sim->mdlData;
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		if (curPt->contactData)
		{
			if (curPt->contactData->GetCurrentActive() & CONTACT_SINK)	//this point acts as a thermal sink for everything! It will not fluctuate at all
				continue;	//as it doesn't fluctuate, make sure the occupancy of it cannot change
		}
		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->cb.energies.begin(); it != curPt->cb.energies.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->vb.energies.begin(); it != curPt->vb.energies.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->acceptorlike.begin(); it != curPt->acceptorlike.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->donorlike.begin(); it != curPt->donorlike.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->CBTails.begin(); it != curPt->CBTails.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->VBTails.begin(); it != curPt->VBTails.end(); ++it)
			SmallerTStepELevel(it->second, mdl, curPt, tStep);

		curPt->ResetData();	//update rho, etc.
	}
	InitIteration();	//update band structure
	ResetValues();	//copies end to begin - but also shifts netRate to netRateM1.
	CalcAllRates(mdl, sim);	//sets ForceAlterPercent tstep and resets AlterPercentCarrier(passive). sim->nexttimestep set internally
	Light(mdl, *sim->mdesc);	//do the light now that the recombination rates (light sources) has been processed
	
	bool isGood = UpdateAllRates(mdl, sim, chgT);
	return(isGood);
}

bool SmallerTStepELevel(ELevel& eLev, Model& mdl, Point* curPt, double TStep)
{
	eLev.netRate = eLev.netRateM1;	//the calculated rate is invalid, so put back the old one
	double adjust = eLev.netRate * TStep;
	eLev.AddOccPrevToEnd(adjust);
	
	return(true);
}

void Point::InitTransIteration() {
	CalcEField();

	/*
	this code is done at the END of the iteration in prep for the next or during adaptive mesh/populate point/equilibrium.
	double Nbeta = 0.0;
	if(temp > 0.0)
	Nbeta = -KBI / temp;

	scatterCBConst = 0.0;
	scatterVBConst = 0.0;
	CalcScatterConsts(scatterCBConst, cb.energies.start, Nbeta);	//sums up all exponents
	if(scatterCBConst > 0.0)
	scatterCBConst = 1.0 / scatterCBConst;

	CalcScatterConsts(scatterVBConst, vb.energies.start, Nbeta);
	if(scatterVBConst > 0.0)
	scatterVBConst = 1.0 / scatterVBConst;
	*/

	bool ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans;
	ResetAlterPercent = percentCarrierTransfer = IncreasePercentTrans = false;
	Simulation* sim = getSim();
	if (sim && sim->TransientData) {
		ResetAlterPercent = sim->TransientData->ResetAlterPercent;
		percentCarrierTransfer = false;// sim->TransientData->percentCarrierTransfer;
		IncreasePercentTrans = sim->TransientData->tStepControl.IncreasePercentTransfer;
	}

	Jn.x = Jn.y = Jn.z = Jp.x = Jp.y = Jp.z = 0.0;
	genth = recth = scatterR = 0.0;
	srhR1 = srhR2 = srhR3 = srhR4 = netCharge = 0.0;
	LightAbsorbDef = LightAbsorbGen = LightAbsorbScat = LightAbsorbSRH = 0.0;
	LightEmitDef = LightEmitRec = LightEmitScat = LightEmitSRH = 0.0;
	LightPowerEntry = LightEmission = 0.0;	//mW

	if (LightEmissions)
		LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!



	largestOccCB = largestOccES = largestOccVB = largestUnOccES = 0.0;

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	for (std::multimap<MaxMin<double>, ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
		it->second.InitTransIteration(ResetAlterPercent, percentCarrierTransfer, IncreasePercentTrans);

	if (sim) {
		if (sim->EnabledRates.Thermalize)	//must be after prepelevel (endocc --> beginocc)
			Thermalize(sim->curiter, RATE_OCC_BEGIN);	//and before the optical detailed init
	}

	OutputPointOpticalDetailedInit(sim, *this);
}

int TransientSimData::ResetValues()
{
	//clear out single iteration optical data
	sim->ResetOpticalFlux(false);
	
	for(unsigned int i=0; i<sim->mdesc->materials.size(); i++)
		sim->mdesc->materials[i]->ResetOpticalFlux(false);


	for(unsigned int i=0; i<sim->contacts.size(); ++i)
		sim->contacts[i]->neteleciter = sim->contacts[i]->netholeiter = 0.0;

//	isSS = true;	//it starts out as true, and then gets altered in the update/record rates.

	//clear out point data
	for (std::map<long int, Point>::iterator ptIt = sim->mdlData->gridp.begin(); ptIt != sim->mdlData->gridp.end(); ++ptIt)
	{
		ptIt->second.InitTransIteration();
//		Point* curPt = &(ptIt->second);
		//need to get the overall electric field of the point to adjust the momentum bit appropraitely
	}

	tStepControl.RemoveState(TimeStepControl::ClearIter);
	return(0);
}

int ELevel::InitTransIteration(bool ResetPercentTransfer, double percentTransfer, bool IncreaseTransfer)
{
	//double den = 1.0 / (eLev->efm * eLev->velocity); unused
	
	double occ, avail;
	GetOccupancyOcc(RATE_OCC_END,occ,avail);	//can't let things always re-zero out!
	if(occ<avail)
		SetBeginOccStates(occ);	//make sure that when carriers move, they start out correctly	Transfers.Clear();
	else
		SetBeginOccStatesOpposite(avail);

	if(carriertype==ELECTRON)
	{
		if(imp)
		{
			if(pt.largestOccES < occ)
				pt.largestOccES = occ;

			double unocc = GetEndAvailable();
			if(pt.largestUnOccES < unocc)
				pt.largestUnOccES = unocc;
			
		}
		else
		{
			if(pt.largestOccCB < occ)	//also goes for CBTails
				pt.largestOccCB = occ;
		}
	}
	else
	{
		if(imp)
		{
			if(pt.largestOccES < occ)
				pt.largestOccES = occ;

			double unocc = GetEndAvailable();
			if(pt.largestUnOccES < unocc)
				pt.largestUnOccES = unocc;
		}
		if(pt.largestOccVB < occ)	//this also goes for VBTails
			pt.largestOccVB = occ;
	}

//	FindAllTargets(eLev, pt, mdl);	//this populates all the targets that electrons may potentially go to.
	//need to also back calculate the dependencies, but it must do so completely

	if(ResetPercentTransfer)
	{
		percentTransfer = percentTransfer;
//		tStepCtrl->Reset();
	}
	else if(IncreaseTransfer)
	{
		percentTransfer = percentTransfer * INCREASE_PERCENT_TRANSFER;
		if(percentTransfer > INIT_PERCENT_TRANSFER)
			percentTransfer = INIT_PERCENT_TRANSFER;
		netRate = 0.0;	//cancel it out!
	}
	else
	{


		if((netRate < 0.0 && netRateM1 > 0.0) || 
		   (netRate > 0.0 && netRateM1 < 0.0) )
		{
//			netRate = 0.0;	allow it to keep reducing and hone in on the "answer" quickly
			percentTransfer = percentTransfer * REDUCE_PERCENT_TRANSFER;
			if(percentTransfer < MIN_PERCENT_TRANSFER)
				percentTransfer = MIN_PERCENT_TRANSFER;
		}
		else if((netRate > netRateM1 && netRateM1 > 0.0) ||	//the net rate is growing and the same sign.
			(netRate < netRateM1 && netRateM1 < 0.0))	// The only way it should ever grow and maintain sign is if there is an imbalance introduced externally that is propagating through
		{
			percentTransfer = percentTransfer * INCREASE_PERCENT_TRANSFER;	//double the percent transfer
			if(percentTransfer > MAX_PERCENT_TRANSFER)
				percentTransfer = MAX_PERCENT_TRANSFER;
		}
		else if(((netRate > 0.0 && netRateM1 > 0.0 && netRateM2 > 0.0) ||	//the net rate is always the same sign
				(netRate < 0.0 && netRateM1 < 0.0 && netRateM2 < 0.0))	//but it is not necessarily growing
				&& percentTransfer > MIN_PERCENT_TRANSFER)		//it's already hit bottom. Don't increase unless it's really increasing or it will grind the timestep to a halt
		{
			percentTransfer = percentTransfer * SOFT_INCREASE_PERCENT_TRANSFER;	//increase percent transfer by 1/8th
			if(percentTransfer > MAX_PERCENT_TRANSFER)
				percentTransfer = MAX_PERCENT_TRANSFER;
		}


	}

	LimitByAvailability = false;
//	allowTransferReduction = false;

	netRateM2 = netRateM1;	//needed for Determine Timestep to b eup to date.
	netRateM1 = netRate;	//netRate is calculated in UpdateAllRates (ProcessRateList)
	netRate = 0.0;
	if (getSim() && getSim()->TransientData) {
		if (getSim()->TransientData->tStepControl.isClearMaxSignFlip())
			partialFlippedSignMax = false;
		if (getSim()->TransientData->tStepControl.isClearFallSignFlip())
			partialFlippedSignFall = false;
	}
	else
		partialFlippedSignMax = partialFlippedSignFall = false;
//	partialNRate = 0.0;	//this is just going to be straight out assigned
//	nextRateLimit = 0.0;	//this will be found in calculate rates
//	maxAbsRate = 0.0;
	//then it's basically just look at the target energy level, figure out what kind of rate it is, and calculate the rate. Right?
	rateIn = 0.0;	//initialize the rates
	rateOut = 0.0;
//	unalteredROut = 0.0;
//	unalteredRIn = 0.0;
//	IntraPtRateIn = 0.0;
//	IntraPtRateOut = 0.0;
//	pEQRateIn = 0.0;
//	pEQRateOut = 0.0;
	deriv = deriv2 = derivsourceonly = /*desireChange =*/ adjustCarrier = 0.0;		
	Rates.clear();
	OpticalRates.clear();
	RateSums.Clear();	//just keep it from growing too large accidentally
	unusedRates.Clear();
	UpdateAbsFermiEnergy(RATE_OCC_BEGIN);	//needed to establish bounds on carrier motion
	maxEf = AbsFermiEnergy;
	minEf = AbsFermiEnergy;
	

	return(0);
}

double CalcAllRates(Model& mdl, Simulation* sim)
{
//	sim->nexttimestep = sim->simendtime;	//if negative, then it hasn't been set yet
	//must go through all points and all e-levels
//	Point* slowpoint = NULL;
	
	for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
	
		ptIt->second.desireTStep = sim->TransientData->simendtime;
		ptIt->second.limitsTStep = NULL;
	//first do the CB
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.cb.energies.begin(); eit != ptIt->second.cb.energies.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.vb.energies.begin(); eit != ptIt->second.vb.energies.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.acceptorlike.begin(); eit != ptIt->second.acceptorlike.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.donorlike.begin(); eit != ptIt->second.donorlike.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.CBTails.begin(); eit != ptIt->second.CBTails.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = ptIt->second.VBTails.begin(); eit != ptIt->second.VBTails.end(); ++eit)
		{
			eit->second.CalculateRates();
		}
	}
	
	for(unsigned int i=0; i<sim->contacts.size(); i++)
		ProcessContactTransient(mdl, sim->contacts[i], sim);

	/*
	//debug info
	if(sim->curiter % 2000 == 0)
		DebugTimestep(slowpoint, true);	//keep the file size from growing too high

	DebugTimestep(slowpoint, false);
	*/

	return(sim->TransientData->nexttimestep);
}

int ELevel::CalculateRates()
{
	double tempStep = -1.0;	//just a temporary variable for holding this value.
	std::vector<ELevelRate>::iterator rt;
	Rates.reserve(TargetELev.size());	//prevent any re-allocations
	Simulation* sim = getSim();	//this will be deprecated in Calcrate if new idea works.
	TimeStepControl* tsc = nullptr;
	if (sim && sim->TransientData)
		tsc = &sim->TransientData->tStepControl;
	PosNegSum unusedRates;
	for(std::vector<TargetE>::iterator it = TargetELevDD.begin(); it != TargetELevDD.end(); ++it)
	{
		rt = CalcRate(this, it->target, sim);
		if(rt != Rates.end())
		{
			if(rt->desiredTStep > 0.0 && (rt->desiredTStep < tempStep || tempStep < 0.0))	//DEPRECATE if new idea works
				tempStep = rt->desiredTStep;
			if(it->target->AbsFermiEnergy > maxEf)	//DEPRECATE IF NEW IDEA WORKS
				maxEf = it->target->AbsFermiEnergy;
			if(it->target->AbsFermiEnergy < minEf)	//DEPRECATE IF NEW IDEA WORKS
				minEf = it->target->AbsFermiEnergy;

		}
	}
	for(std::vector<ELevel*>::iterator it = TargetELev.begin(); it != TargetELev.end(); ++it)
	{
		rt = CalcRate(this, *it, sim);
		if(rt != Rates.end())
		{
			if(rt->desiredTStep > 0.0 && (rt->desiredTStep < tempStep || tempStep < 0.0))
				tempStep = rt->desiredTStep;
			if((*it)->AbsFermiEnergy > maxEf)
				maxEf = (*it)->AbsFermiEnergy;
			if((*it)->AbsFermiEnergy < minEf)
				minEf = (*it)->AbsFermiEnergy;
		}
	}



	//everything below will be deprecated soon enough... hopefully

	//add slight buffer so can just check if it's outside
	if(maxEf - minEf > 2.0 * sim->accuracy)
	{
		maxEf -= sim->accuracy;	//if the occupancy is too large, it is considered edge
		minEf += sim->accuracy;	//as it started from the source accuracy
	}
	
	//now calculate the maximum and minimum occupancy, type is equal to begoccstateiscarriertype
	double occ, avail;
	if(carriertype==ELECTRON)
	{
		OccFromAbsEf(minEf, occ, avail);
		minOcc = occ;
		minAvail = avail;
		OccFromAbsEf(maxEf, occ, avail);
		maxOcc = occ;
		maxAvail = avail;
	}
	else
	{
		//the max/min's must be swapped because max/minEf is in absolute eV, not relative
		OccFromAbsEf(maxEf, occ, avail);
		minOcc = occ;
		minAvail = avail;
		OccFromAbsEf(minEf, occ, avail);
		maxOcc = occ;
		maxAvail = avail;
	}
	
	
	//tempStep above is based on choosing the lowest timestep as desired by individual rates.
	//that way didn't quite work as intended, so this new way will look at the nonEQ net rates and the most it wants to change

	//tempStep = DetermineTimestep(srcElev, sim);
	//tempStep = CalcTransfer(srcElev, sim);
		
	if(tempStep < pt.desireTStep && tempStep > 0.0 && percentTransfer >= MIN_PERCENT_IGNORE_TIMESTEP)	//only want to look at this particular energy level, so look at tempstep and not desireTStep
	{
		pt.desireTStep = tempStep;	//this is where the timestep comes from...
		pt.limitsTStep = this;
	}

	return(0);	//always return the smallest timestep that is positive (now irrelevant)
}


//this adjusts the desired timestep to match what's needed for the point
int ThermalPtTStep(Point* pt, int Thermalize)
{
	if(Thermalize==THERMALIZE_NONE)
		return(0);

	double curTStep = pt->desireTStep;
	double curMinTStep = INFINITETSTEP;
	double nonThermalTStep = INFINITETSTEP;
	double curChange, curRate;
	double tmp1, tmp2, tmp3;
	tmp1=tmp2=tmp3=curChange=curRate=0.0;

	if(Thermalize & THERMALIZE_CB)
	{
		curChange = curRate = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->cb.energies.begin(); eLevIt != pt->cb.energies.end(); ++eLevIt)
		{
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			curChange += tmp1;
			curRate += eLevIt->second.netRate;
		}
		curChange = curRate!=0.0 ? curChange / curRate : INFINITETSTEP;	//now this is going to be TStep
		if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
			curMinTStep = curChange;
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->cb.energies.begin(); eLevIt != pt->cb.energies.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	if(Thermalize & THERMALIZE_CBT)
	{
		curChange = curRate = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->CBTails.begin(); eLevIt != pt->CBTails.end(); ++eLevIt)
		{
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			curChange += tmp1;
			curRate += eLevIt->second.netRate;
		}
		curChange = curRate!=0.0 ? curChange / curRate : INFINITETSTEP;	//now this is going to be TStep
		if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
			curMinTStep = curChange;
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->CBTails.begin(); eLevIt != pt->CBTails.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	if(Thermalize & THERMALIZE_VBT)
	{
		curChange = curRate = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->VBTails.begin(); eLevIt != pt->VBTails.end(); ++eLevIt)
		{
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			curChange += tmp1;
			curRate += eLevIt->second.netRate;
		}
		curChange = curRate!=0.0 ? curChange / curRate : INFINITETSTEP;	//now this is going to be TStep
		if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
			curMinTStep = curChange;
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->VBTails.begin(); eLevIt != pt->VBTails.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	if(Thermalize & THERMALIZE_VB)
	{
		curChange = curRate = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->vb.energies.begin(); eLevIt != pt->vb.energies.end(); ++eLevIt)
		{
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			curChange += tmp1;
			curRate += eLevIt->second.netRate;
		}
		curChange = curRate!=0.0 ? curChange / curRate : INFINITETSTEP;	//now this is going to be TStep
		if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
			curMinTStep = curChange;
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->vb.energies.begin(); eLevIt != pt->vb.energies.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	std::vector<Impurity*> impList;	//first build up an impurity list
	std::vector<double> change;
	std::vector<double> rate;
	bool impFound;
	unsigned int sz;

	if(Thermalize & THERMALIZE_ACP)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->acceptorlike.begin(); eLevIt != pt->acceptorlike.end(); ++eLevIt)
		{
			impFound = false;
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			sz = impList.size();	//this needs to be updated every time just in case
			for(unsigned int i=0; i<sz; i++)
			{
				if(eLevIt->second.imp == impList[i])
				{
					impFound=true;
					change[i] += tmp1;
					rate[i] += eLevIt->second.netRate;
					break;
				}
			}
			if(!impFound)
			{
				impList.push_back(eLevIt->second.imp);
				change.push_back(tmp1);
				rate.push_back(eLevIt->second.netRate);
			}
		}
		sz = impList.size();	//this needs to be updated every time just in case
		for(unsigned int i=0; i<sz; i++)
		{
			curChange = rate[i]!=0.0 ? change[i] / rate[i] : INFINITETSTEP;	//now this is going to be TStep
			if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
				curMinTStep = curChange;
		}

		impList.clear();
		change.clear();
		rate.clear();
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->acceptorlike.begin(); eLevIt != pt->acceptorlike.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	if(Thermalize & THERMALIZE_DON)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->donorlike.begin(); eLevIt != pt->donorlike.end(); ++eLevIt)
		{
			impFound = false;
//			eLevIt->second.tStepCtrl->GetRets(tmp1, tmp2, tmp3);
			sz = impList.size();	//this needs to be updated every time just in case
			for(unsigned int i=0; i<sz; i++)
			{
				if(eLevIt->second.imp == impList[i])
				{
					impFound=true;
					change[i] += tmp1;
					rate[i] += eLevIt->second.netRate;
					break;
				}
			}
			if(!impFound)
			{
				impList.push_back(eLevIt->second.imp);
				change.push_back(tmp1);
				rate.push_back(eLevIt->second.netRate);
			}
		}
		sz = impList.size();	//this needs to be updated every time just in case
		for(unsigned int i=0; i<sz; i++)
		{
			curChange = rate[i]!=0.0 ? change[i] / rate[i] : INFINITETSTEP;	//now this is going to be TStep
			if(curChange > 0.0 && (curChange < curMinTStep || curMinTStep == INFINITETSTEP))
				curMinTStep = curChange;
		}

		impList.clear();
		change.clear();
		rate.clear();
	}
	else
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLevIt = pt->donorlike.begin(); eLevIt != pt->donorlike.end(); ++eLevIt)
		{
//			curChange = eLevIt->second.tStepCtrl->GetTStep();
			if(nonThermalTStep == INFINITETSTEP || (curChange < nonThermalTStep && curChange>0.0))
				nonThermalTStep = curChange;
		}
	}

	//now grab the smaller of the two
	if(curMinTStep == INFINITETSTEP)
		pt->desireTStep = nonThermalTStep;
	else if(nonThermalTStep == INFINITETSTEP)
		pt->desireTStep = curMinTStep;
	else if(curMinTStep < nonThermalTStep && curMinTStep > 0.0)
		pt->desireTStep = curMinTStep;
	else if(nonThermalTStep > 0.0)
		pt->desireTStep = nonThermalTStep;
	else
		pt->desireTStep = INFINITETSTEP;	//I have no idea how this happened...

		//curTStep is the smallest number it can be (original value).
	if(pt->desireTStep != INFINITETSTEP && curTStep > pt->desireTStep)
		pt->desireTStep = curTStep;

	return(0);
}

bool UpdateAllRates(Model& mdl, Simulation* sim, ChargeTransfer& chgT)
{
	
	chgT.Init(sim);	//0 everything out and put the timestep/simulation time in

	sim->TransientData->nexttimestep = INFINITETSTEP;
	sim->TransientData->tmpMaxNRate = 0.0;
	Point* slowPt = NULL;
	PosNegSum nChargeRate;
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		curPt->desireTStep = INFINITETSTEP;//sim->simendtime;
		curPt->limitsTStep = NULL;
		nChargeRate.Clear();
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->cb.energies.begin(); eLevIt != curPt->cb.energies.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.SubtractValue(eLevIt->second.netRate);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->vb.energies.begin(); eLevIt != curPt->vb.energies.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.AddValue(eLevIt->second.netRate);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->acceptorlike.begin(); eLevIt != curPt->acceptorlike.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.AddValue(eLevIt->second.netRate);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->donorlike.begin(); eLevIt != curPt->donorlike.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.SubtractValue(eLevIt->second.netRate);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->CBTails.begin(); eLevIt != curPt->CBTails.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.SubtractValue(eLevIt->second.netRate);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->VBTails.begin(); eLevIt != curPt->VBTails.end(); ++eLevIt)
		{
			if (UpdateELevelRate(eLevIt->second, sim, chgT) == false)
				nChargeRate.AddValue(eLevIt->second.netRate);
		}
		
/*
		//update time step based on entire point rates if thermalizing
		if(sim->EnabledRates.Thermalize)
		{
			ThermalPtTStep(curPt, sim->EnabledRates.Thermalize);
		}
*/
//		if(curPt->contactData == NULL)
//		{
		//don't let a point have it's rho change by more than 0.1. A sign of massive divergence
		//this ignores any states that are at their equilibrium values!
		if(curPt->desireTStep != INFINITETSTEP && nChargeRate.GetNetSum() != 0.0)	//if it's infinite, it's at EQ!
		{
			//mag change of rho by 0.1.
			double rhoStep = fabs(curPt->vol * QI / nChargeRate.GetNetSum() * 0.1);
			if(rhoStep < curPt->desireTStep)
			{
				curPt->desireTStep = rhoStep;
				curPt->limitsTStep = NULL;	//it was all the states causing the slowdown
			}
		}
		if ((curPt->desireTStep < sim->TransientData->nexttimestep && curPt->desireTStep != INFINITETSTEP) || sim->TransientData->nexttimestep == INFINITETSTEP)
		{
			sim->TransientData->nexttimestep = curPt->desireTStep;
			slowPt = curPt;
		}
//		}
/*		else if(curPt->contactData->isolated == true)
		{
			if(curPt->desireTStep < sim->nexttimestep || sim->nexttimestep == INFINITETSTEP)
			{
				sim->nexttimestep = curPt->desireTStep;
				slowPt = curPt;
			}
		}*/
	}

	/*
	This forced the timestep to always increase. Not such a good idea (yet?)
	if (sim->TransientData->tstepConsts.prevtimestep > 0.0)
	{
		if (sim->TransientData->tmpMaxNRate >= 0.99*sim->TransientData->testMNRate)	//the max net Rate is not decreasing hardly at all
			sim->TransientData->nexttimestep = sim->TransientData->tstepConsts.prevtimestep*1.5;
		else
			sim->TransientData->nexttimestep = sim->TransientData->tstepConsts.prevtimestep*1.0625;
	}
	*/
	//if it gets here, then the max rate for this iteration should have been less than the previous
	sim->TransientData->prevMNRate = sim->TransientData->curmaxNetRate;
	sim->TransientData->curmaxNetRate = sim->TransientData->tmpMaxNRate;
#ifndef NDEBUG
	if(sim->curiter % 2000 == 0)
		DebugTimestep(slowPt, true);	//keep the file size from growing too high

	DebugTimestep(slowPt, false);
#endif
	return(true);
}

int CheckForSSLinkedRates(ELevel& state)
{
	//the optical rates are often added to, so these are added in post-calculation. These will often need some pairing, though it would tend to be
	//a conglomerate of other items.
	int sz = state.OpticalRates.size();
	for(int i=0; i<sz; ++i)
	{
		double nRate = state.OpticalRates[i].RateIn - state.OpticalRates[i].RateOut;
		//if the rates are the same, nRate will be close to zero, but if one of the rates is dominating,
		//then this will flag it to be processed a little differently for checking against the tStep on whether to include the rate or not.
		//this will prevent major accidental charge transfers that predominantly occur in drift diffusion.
		if(fabs(nRate) * 2.0 > state.OpticalRates[i].RateIn + state.OpticalRates[i].RateOut)
		{
			if(nRate > 0.0)
			{
				state.ExcessiveNetPRates.push_back(&(state.OpticalRates[i]));
				state.NetRateP.push_back(nRate);
			}
			else
			{
				state.ExcessiveNetNRates.push_back(&(state.OpticalRates[i]));
				state.NetRateN.push_back(nRate);
			}
		}
	}

	 sz = state.Rates.size();
	for(int i=0; i<sz; ++i)
	{
		double nRate = state.Rates[i].RateIn - state.Rates[i].RateOut;
		//if the rates are the same, nRate will be close to zero, but if one of the rates is dominating,
		//then this will flag it to be processed a little differently for checking against the tStep on whether to include the rate or not.
		//this will prevent major accidental charge transfers that predominantly occur in drift diffusion.
		if(fabs(nRate) * 2.0 > state.Rates[i].RateIn + state.Rates[i].RateOut)
		{
			if(nRate > 0.0)
			{
				state.ExcessiveNetPRates.push_back(&(state.Rates[i]));
				state.NetRateP.push_back(nRate);
			}
			else
			{
				state.ExcessiveNetNRates.push_back(&(state.Rates[i]));
				state.NetRateN.push_back(nRate);
			}
		}
	}

	//now do some links
	int szP = state.ExcessiveNetPRates.size();
	int szN = state.ExcessiveNetNRates.size();
	int NUsed = 0;	//get out of the loops if all the unbalanced negative rates are done
	double testRate;
	if(szP > 0 && szN > 0)
	{
		int indexP = 0;
		int indexN = 0;
		for(indexP = 0; indexP < szP && NUsed < szN; ++indexP)	//cycle through all the postive rates
		{
			for(indexN = 0; indexN < szN && NUsed < szN; ++indexN)
			{

				testRate = state.NetRateP[indexP] + state.NetRateN[indexN];	//note, the negative rates are negative
				//we want the rate to be also very close
				if(fabs(testRate) < 0.375*(state.NetRateP[indexP] - state.NetRateN[indexN]))	//if they are equal, this will end up being half the rate
				{  //if one is lopsided, say 10 & -1: test=9, 11*0.375=4.125, 9>4.125, and it won't go
					//or on the border: 5 & -10
					//TRate = -5, 5 < 0.375*(15)=5.625 - these two values are close enough to be considered cancelling
					//for exact within order of magnitude, need 0.375==1/3, but .375 is close and should be a faster multiply as it is mostly zeros
					state.ExcessiveNetNRates[indexN]->CancellingRate = state.ExcessiveNetPRates[indexP];
					state.ExcessiveNetPRates[indexP]->CancellingRate = state.ExcessiveNetNRates[indexN];
					//now let's trick it into always being one sided and thereby failing the test
					state.NetRateN[indexN] = 0.0;
					++NUsed;
					break;	//no more need to search thourgh this anymore
				}


			}
		}
	}

	state.ExcessiveNetPRates.clear();
	state.ExcessiveNetNRates.clear();
	state.NetRateN.clear();
	state.NetRateP.clear();

	//make an educated guess as to how much should be reserved
	state.ExcessiveNetNRates.reserve(szN + 5);
	state.ExcessiveNetPRates.reserve(szP + 5);
	state.NetRateN.reserve(szN + 5);	// This is crashing it for some crazy reason. Note, szN and szP were lik 11.
	state.NetRateP.reserve(szP + 5);
	return(0);
}

//returns true if it wants an infinite time step!
bool UpdateELevelRate(ELevel& state, Simulation* sim, ChargeTransfer& chgT)
{
// this will be added - but I believe i inadvertently fixed the one bug	FindELevelSS(state, sim);	//auto get refinement. This figures out what carrier concentration (roughly) is needed to have the
	//energy level be in steady state.

	double desiredTStep = INFINITETSTEP;	//time step actually used
	double TStepZero = INFINITETSTEP;	//time step needed to reach '0' net rate
	double tStepRFLip = INFINITETSTEP;	//time step needed to reach last known point where rate flips
	double tStepMax = INFINITETSTEP;
//	double tStepMin = INFINITETSTEP;	//time step minimum value. Either to matter, or it's steady state.
	
//	CheckForSSLinkedRates(state);	this just causes problems

	for(std::vector<ELevelRate>::iterator rt = state.OpticalRates.begin(); rt != state.OpticalRates.end(); ++rt)
		ProcessRateList(*rt, sim, &state, chgT);	//goes through and adds all the Rate In, Rate Out, and net Rates for the optical rates

	unsigned int sz = state.Rates.size();
	for(std::vector<ELevelRate>::iterator rt = state.Rates.begin(); rt!= state.Rates.end(); ++rt)	//now do the normal rates
		ProcessRateList(*rt, sim, &state, chgT);

	if(state.netRate == 0.0)
		return(true);	//infinite time step

	//put this before if state.netRAte > ... Don't want this type of contact to ever result in a diminishing timestep.
	//this type of contact is held constant
	Contact *ct = state.GetContact();
	if (ct) {
		if (ct->GetCurrentActive() & CONTACT_SINK)	//this point acts as a thermal sink for everything! It will not fluctuate at all.
		{
			//				state.tStepCtrl->SetRets(0.0);	//say no change
			sim->TransientData->monTStep.AddTStep(desiredTStep);
			//				state.tStepCtrl->SetTStep(desiredTStep);
			return(true);	//say infinite time step
		}
	}

	//first, see how far away it is from steady state value
	double occ, avail;
	state.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
	if(occ >= state.tOccMin && occ <= state.tOccMax
		&& avail >= state.tAvailMin && avail <= state.tAvailMax)
		return(true);	//say want an infinite time step. It is at it's steady state value!

	
	desiredTStep = state.CalcDesiredTStep(sim->curSSTarget);
	

	/*

	if (fabs(state.netRate) > sim->TransientData->testMNRate && sim->TransientData->testMNRate != 0.0)
	{
		return(false);
	}
	if (fabs(state.netRate) > sim->TransientData->tmpMaxNRate)
		sim->TransientData->tmpMaxNRate = fabs(state.netRate);

	

//	double occ = state.GetBeginOccStates();
//	double desirechange = state.targetOcc - occ;	//how much this state roughly wants to change


	//something like this will probably have to be re-enabled
	//double numCar = GetELevelNumCarrierTransfer(&state);	//if it's a small state, make it insignificant

	
	//	double percentChange = desirechange / numCar;	//what percentage is this? If it's a hardly occupised state, don't let this screw up
	//the timestep
/ *
	if(percentChange < MIN_PERCENT_IGNORE_TIMESTEP)
	{
		tempTStep = -1.0;	//it's close to the final answer,
		state.allowTransferReduction = true;	//this flag limits the change to whatever desirechange is above
		//this presumably will not result in hardly any charge creation/destruction because it is either:
		//an insignificant energy level (typcally well beyond the computational accuracy)
		//the energy states receiving/sending carriers should roughly balance out
	}
	else
		tempTStep = 0.5 * desirechange / state.netRate;	//only go halfway there...

* /
	
	state.tStepCtrl->AddROcc(state.GetBeginOccVariable(), DoubleSubtract(state.rateIn,state.rateOut), state.GetBeginOccMatch(), sim->accuracy);
	state.tStepCtrl->SetRets();

	double minDelta=-1;	//default disable that will be smaller than max-min if max=-1.
	double useAccuracy = sim->accuracy;
	if(useAccuracy > 0.25 || useAccuracy <1e-15)
		useAccuracy = 1e-10;
	
	if(state.tStepCtrl->maxOcc > 0.0)	//prefer to use maxOcc
		minDelta = state.tStepCtrl->maxOcc*useAccuracy;
	else if(state.tStepCtrl->minOcc > 0.0) //but ok to use minOcc if max isn't set yet
		minDelta = state.tStepCtrl->minOcc*useAccuracy;

	double occ, avail, tstepMin, useOcc, useMax, useMin, NStates;
	state.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
	NStates = state.GetNumstates();
	bool CalcAsOcc=true;

	double change1, change2, change3;
	int good=state.tStepCtrl->GetRets(change1, change2, change3);

	if(good>0 && minDelta > 0.0 && fabs(change1) < minDelta)	//the 'answer' is within the accuracy
	{
		if(change1>0.0) //if it has the answer, may as well make it work...
		{
			double newMax = state.tStepCtrl->minMatch ? occ + minDelta: avail+minDelta;
			if(state.tStepCtrl->maxOcc == -1.0 || newMax < state.tStepCtrl->maxOcc)
			{
				state.tStepCtrl->maxOcc = newMax;
				state.tStepCtrl->maxMatch = state.tStepCtrl->minMatch;
			}
		}
		else if(change1<0.0)	//a maximum is definitely set
		{
			double newMin = state.tStepCtrl->maxMatch ? occ-minDelta: avail-minDelta;
			if(newMin<0.0)
				newMin=0.0;
			if(state.tStepCtrl->minOcc == -1.0 || newMin > state.tStepCtrl->minOcc)
			{
				state.tStepCtrl->minOcc = newMin;
				state.tStepCtrl->minMatch = state.tStepCtrl->maxMatch;
			}
		}
		else
		{
			if(state.tStepCtrl->minMatch)
			{
				state.tStepCtrl->minOcc = occ - 0.5*minDelta;
				state.tStepCtrl->maxOcc = occ + 0.5*minDelta;
			}
			else
			{
				state.tStepCtrl->minOcc = avail - 0.5*minDelta;
				state.tStepCtrl->maxOcc = avail + 0.5*minDelta;
			}
			state.tStepCtrl->maxMatch = state.tStepCtrl->minMatch;
		}
//		change1 = (change1>0.0) ? minDelta:-minDelta;	//so adjust it by the accuracy of the solution so it likely sets other max/min
	}

	if(good>0 && ((change1>0.0&&state.netRate>0.0) || (change1<0.0&&state.netRate<0.0)))
		TStepZero = change1 / state.netRate;
	else if(good>0 && change1==0.0)
		TStepZero = INFINITETSTEP;	//it's already there.
	else if(good>0)
		TStepZero=INFINITETSTEP;	//it can't calculate a valid time for the timestep to go to zero, so just ignore TStepZero. It's behaving weirdly.
	else if(state.deriv != 0.0) //at this point, good=0, or change and netrate have opposite sign. Good = 0 is rare - basically just the first iteration or when it switches between occ and avail. 
		TStepZero = 1.0 / fabs(state.deriv); //else tempTStep = INFINITETSTEP (initialized)


	//figure out what the minimum time step for relevance is
	

	//first get everything on the same page
	if(occ < avail)
	{
		//we're using occupancy as the control method
		useOcc = occ;
		if(state.tStepCtrl->minMatch==true) //minOcc is storing occ
		{ //minMatch must equal maxMatch, or the data gets invalidated (-1).
			useMin = state.tStepCtrl->minOcc;
			useMax = state.tStepCtrl->maxOcc;
		}
		else
		{ //now the minOcc and maxOcc correspond to minAvail and maxAvail.
			useMin = state.tStepCtrl->maxOcc>0.0 ? NStates-state.tStepCtrl->maxOcc : -1.0;
			useMax = state.tStepCtrl->minOcc>0.0 ? NStates-state.tStepCtrl->minOcc : -1.0;
		}
	}
	else
	{
		useOcc = avail;
		CalcAsOcc=false;
		if(state.tStepCtrl->minMatch==false) //minOcc is storing avail
		{ //minMatch must equal maxMatch, or the data gets invalidated (-1).
			useMin = state.tStepCtrl->minOcc;
			useMax = state.tStepCtrl->maxOcc;
		}
		else //the min/max match the state occupancy, but it's best to look at avail
		{ //so it must be converted
			useMin = state.tStepCtrl->maxOcc>0.0 ? NStates-state.tStepCtrl->maxOcc : -1.0;
			useMax = state.tStepCtrl->minOcc>0.0 ? NStates-state.tStepCtrl->minOcc : -1.0;
		}
		
	}
	if(useOcc!=0.0 && state.netRate!= 0.0)
		tstepMin = fabs(useOcc * 1.0e-15 / state.netRate);	//smallest timestep required for the state to even change values
	else 
		tstepMin = INFINITETSTEP;
	//both maxOcc and minOcc exist. occ must be within both, and their delta must be 
	if(state.netRate > 0.0) //then avail must be > 0.0
		tStepMax = 0.75*avail / state.netRate;
	else if(state.netRate < 0.0) //as occ must be > 0.0
		tStepMax = 0.75*occ / -state.netRate;
	
	if(MY_IS_FINITE(tStepMax)==false)
		tStepMax = INFINITETSTEP;
	
	
	

	if(state.netRate == 0.0)
		tstepMin = INFINITETSTEP;	//this is always true in this situation
	else if(useMax >= 0.0 && useMin >= 0.0 && useOcc <= useMax && useOcc >= useMin && useMax-useMin <= minDelta*1.0009765625)	//multiplication is just to avoid slight rounding errors (fraction multiple of 0.5)
	{
		tstepMin = INFINITETSTEP;	//this state is in steady-state as far as the code is concerned
		//it cannot go beyond the boundaries of useMax or useMin. If it tries and gets set to them, and still tries to increase,
		//then the limit gets lifted.
	}
	else if(state.netRate > 0.0 && ((useMax >= 0.0 && CalcAsOcc) || (useMin >= 0.0 && !CalcAsOcc)))
	{
		//carriers are entering the state. There is a maximum occupancy or a minimum availability
		//when do I allow it to exceed the value?
		//if occ >=maxOcc and the rate > 0, maxOcc restriction was lifted.
		if(CalcAsOcc)
		{
			double delta = useMax - useOcc;
			tStepRFLip = delta / state.netRate;
		}
		else //the data we're looking at is as if it were available. useOcc=avail
		{ //usemin is minimum availability, which is the maximum occupancy
			double delta = useOcc - useMin;
			tStepRFLip = delta / state.netRate;
		}
	}
	else if(state.netRate < 0.0 && ((useMin >= 0.0 && CalcAsOcc) || (useMax >= 0.0 && !CalcAsOcc)))
	{
		//carriers are leaving the state. There is a minimum occupancy or maximum availability
		if(CalcAsOcc)
		{
			double delta = useOcc - useMin;
			tStepRFLip = delta / -state.netRate;
		}
		else //so useOcc is available, useMax is max available. This is netrate<0, so available is increasing
		{
			double delta = useMax - useOcc; //this should be positive
			tStepRFLip = delta / -state.netRate; //but netrate is negative
		}
	}
	
	//prevent time steps that would push it beyond/to full/empty occupancy
	if(tStepMax!=INFINITETSTEP)
	{
		if(tStepMax < tStepRFLip || tStepRFLip == INFINITETSTEP)
			tStepRFLip = tStepMax;
		if(tStepMax < TStepZero || TStepZero == INFINITETSTEP)
			TStepZero = tStepMax;
	}
	
	if(tstepMin == INFINITETSTEP)
		desiredTStep = INFINITETSTEP;
	else if(TStepZero > 0.0 && tStepRFLip > 0.0) //both are valid, but choose smaller
	{
		desiredTStep = (TStepZero < tStepRFLip) ? TStepZero : tStepRFLip;
	}
	else if(TStepZero > 0.0)
		desiredTStep = TStepZero;
	else if(tStepRFLip > 0.0)
		desiredTStep = tStepRFLip;
	//else desiredTStep = INFINITETSTEP, default


	

//	tempTStep = sim->TransientData->tStepControl.DetermineTStep(state.netRate, numCar,  state.percentTransfer);
		
	if(tstepMin > desiredTStep || tstepMin == INFINITETSTEP)
		desiredTStep = tstepMin;
		*/
	if((desiredTStep < state.getPoint().desireTStep && desiredTStep > 0.0) || state.getPoint().desireTStep < 0.0)
	{  	//update for non infinite values, and accept if point is still infinite
		state.getPoint().desireTStep = desiredTStep;	//this is where the timestep comes from...
		state.getPoint().limitsTStep = &state;
	}

	sim->TransientData->monTStep.AddTStep(desiredTStep);
//	state.tStepCtrl->SetTStep(desiredTStep);

//	if(desiredTStep != INFINITETSTEP)
//	{
//		state.tStepCtrl->SetRets(desiredTStep * state.netRate);
//	}
	return (desiredTStep < 0.0);	//true if infinite timestep
}

int ProcessRateList(ELevelRate& rate, Simulation* sim, ELevel* eLev, ChargeTransfer& chgT)
{
	if(eLev==NULL)
	{
		ProgressCheck(59, false);
		return(0);
	}
	RecordRate(&eLev->getPoint(), rate, chgT, sim);	//alwayss do this
	eLev->deriv += rate.derivsourceonly;

	if(rate.RateIn == rate.RateOut)	//this will be true whether they've been removed or not!
		return(0);

	double NetRate = DoubleSubtract(rate.RateIn, rate.RateOut);

	if(rate.source != eLev)
	{
		ProgressCheck(61, false);
		ProgressInt(57,rate.type);
	}

	if(rate.source == NULL || rate.target == NULL)	//make sure this is added for contact currents correctly. There is no "target" so this could trip up the stuff below
	{
		
		eLev->rateIn += rate.RateIn;
		eLev->rateOut += rate.RateOut;
		eLev->netRate += NetRate;
//		eLev->unalteredRIn += rate.RateIn;
//		eLev->unalteredROut += rate.RateOut;
		//want to add a link to the rates so it can find a corresponding rate to cancel with it
		return(0);
	}
/*
This may have to be undone

	//eLev->difEf += (rate->Sort.source->AbsFermiEnergy - rate->Sort.target->AbsFermiEnergy);	//these are updated in CalcBandStruct
	double sourceCar = GetELevelNumCarrierTransfer(rate.source);
	double targetCar = GetELevelNumCarrierTransfer(rate.target);
	if(rate.CancellingRate != NULL)	//there is a different rate that is cancelling this - most likely from drift-diffusion rates
	{
		rate.RateIn += rate.CancellingRate->RateIn;	//make sure these still get added from TransientData->tStepControl to eLev->rateIn/Out
		rate.RateOut += rate.CancellingRate->RateOut;

		NetRate = DoubleSubtract(rate.RateIn, rate.RateOut);	//update the NetRate appropriately
		rate.CancellingRate->RateIn = 0.0;	//now that they've been incorporated into this rate (for carrier changing purposes), clear them out
		rate.CancellingRate->RateOut = 0.0;	//to avoid double counting
		//this will cancel any effect of the rate on changing the carrier concentration

		//NOTE: rateType is still present, so it will be reported properly on the outputs, even though how it's added in may be different

		//now prevent it from going in and re-adding the original values when it gets back
		rate.CancellingRate->CancellingRate = NULL;
		rate.CancellingRate = NULL;
	}

	if(sim->TransientData->tStepControl.RateIsOK(NetRate, sourceCar, rate.source->percentTransfer) &&
		sim->TransientData->tStepControl.RateIsOK(NetRate, targetCar, rate.target->percentTransfer))
	{
*/	
		eLev->rateIn += rate.RateIn;
		eLev->rateOut += rate.RateOut;
		eLev->netRate += NetRate;
//	}

//	eLev->unalteredRIn += rate.RateIn;
//	eLev->unalteredROut += rate.RateOut;



	return(0);
}
/*
int ProcessRateList(std::vector<ELevelOpticalRate>::iterator& rate, Simulation* sim, ELevel* eLev, ChargeTransfer& chgT)
{
	double NetRate = DoubleSubtract(rate->RateIn, rate->RateOut);

	if(rate->type == RATE_CONTACT_CURRENT_EXT || rate->type == RATE_CONTACT_CURRENT_INTEXT)	//make sure this is added for contact currents correctly. There is no "target" so this could trip up the stuff below
	{
		eLev->rateIn += rate->RateIn;
		eLev->rateOut += rate->RateOut;
		eLev->netRate += NetRate;
		return(0);
	}

	//eLev->difEf += (rate->Sort.source->AbsFermiEnergy - rate->Sort.target->AbsFermiEnergy);	//these are updated in CalcBandStruct
	double sourceCar = GetELevelNumCarrierTransfer(rate->source);
	double targetCar = GetELevelNumCarrierTransfer(rate->target);

	if(sim->TransientData->tStepControl.RateIsOK(NetRate, sourceCar, rate->source->percentTransfer) &&
		sim->TransientData->tStepControl.RateIsOK(NetRate, targetCar, rate->target->percentTransfer))
	{
		eLev->rateIn += rate->RateIn;
		eLev->rateOut += rate->RateOut;
		eLev->netRate += NetRate;
	}

	eLev->unalteredRIn += rate->RateIn;
	eLev->unalteredROut += rate->RateOut;

	RecordRate(eLev->ptrPoint, rate, chgT, sim);


	return(0);
}
*/
double GetELevelNumCarrierTransfer(ELevel* eLev)
{
	if(eLev == NULL)
		return(0.0);
	double numCar = eLev->GetBeginOccStates();	//default value

	//find default values for insignificant points

	
	if(eLev->imp)
	{
		if(eLev->imp->isCBReference())
		{
			if(eLev->getPoint().largestOccES * LEVELINSIGNIFICANT > numCar)
				numCar = eLev->getPoint().largestOccES;
		}
		else
		{
			double unocc = eLev->GetNumstates() - numCar;
			if(eLev->getPoint().largestUnOccES * LEVELINSIGNIFICANT > unocc)
				numCar = eLev->getPoint().largestUnOccES;
		}
	}
	else //a band
	{
		//actually compare this to the most significant of them all...

		double maxCar = eLev->getPoint().largestOccCB > eLev->getPoint().largestOccVB ? eLev->getPoint().largestOccCB : eLev->getPoint().largestOccVB;
		if(maxCar * LEVELINSIGNIFICANT > numCar)
			numCar = maxCar;
		/*
		if(eLev->carriertype == ELECTRON) //the CB
		{
			if(eLev->getPoint().largestOccCB * LEVELINSIGNIFICANT > numCar)
				numCar = eLev->getPoint().largestOccCB;
		}
		else //VB
		{
			if(eLev->getPoint().largestOccVB * LEVELINSIGNIFICANT > numCar)
				numCar = eLev->getPoint().largestOccVB;
		}
		*/
	}

	if(eLev->netRate > 0.0)	//only going up in carriers
	{
		if(eLev->LimitByAvailability)	//it is insignificant, and potentially gaining a lot of carriers...
			numCar = eLev->GetNumstates() - numCar;
	}
	//if carriers are leaving, LimitByAvailability is GetBeginOccStates, so no need to test for that.

	return(numCar);
}

int RecordAll(Model& mdl, ModelDescribe& mdesc, ChargeTransfer& chgT)
{
	PosNegSum charge;

	if (mdesc.simulation->TransientData->simtime >= mdesc.simulation->TransientData->timeignore)
	{
		if(mdesc.simulation->binctr == 0)
		{
			mdesc.simulation->TransientData->storevalueStartTime = mdesc.simulation->TransientData->simtime;
		}
		OutputDataInternalPre(mdl, mdesc.simulation->TransientData->timestep, mdesc.simulation->binctr);	//add it to the tree of final results --- can't do this until you know what the timestep will be
	}	//the timestep weights the values recorded

	//this next loop actually moves the carriers in RecordPoint and then records the outputs
	for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		RecordPoint(mdl, ptIt->second, mdesc.simulation);
		if(ptIt->second.contactData)
			ContactCurrent(ptIt->second, mdesc, mdl);

		charge.AddValue(ptIt->second.netCharge);
		
		//this is preparation for the next iteration. It needs to be done for all the points
		//before any prepelevels fire because of drift diffusion finding the target
		//uses the scattering partition to get detailed balance.
/*
		double Nbeta = 0.0;
		if(curPt->temp > 0.0)
			Nbeta = -KBI / curPt->temp;

		curPt->scatterCBConst = 0.0;
		curPt->scatterVBConst = 0.0;
		CalcScatterConsts(curPt->scatterCBConst, curPt->cb.energies.start, Nbeta);	//sums up all exponents
		if(curPt->scatterCBConst > 0.0)
			curPt->scatterCBConst = 1.0 / curPt->scatterCBConst;

		CalcScatterConsts(curPt->scatterVBConst, curPt->vb.energies.start, Nbeta);
		if(curPt->scatterVBConst > 0.0)
			curPt->scatterVBConst = 1.0 / curPt->scatterVBConst;
*/
	}	//end for loop going through all the points in the mesh

	//now update the total number of photons for a given material output
	mdesc.simulation->AggregateOpticalData(mdesc.simulation->TransientData->timestep);

	for(unsigned int i=0; i<mdesc.materials.size(); i++)
		mdesc.materials[i]->AggregateOpticalData(mdesc.simulation->TransientData->timestep);

//	OutputChargeTransfer(chgT, false);	//these haven't been adjusted by the timestep yet

	mdesc.simulation->negCharge = charge.GetNegSum() * QCHARGE;
	mdesc.simulation->posCharge = charge.GetPosSum() * QCHARGE;
	charge.Clear();
//	OutputCharge(mdesc.simulation, false);

	if (mdesc.simulation->outputs.OutputRatePlots)
		PlotDRate_C(mdl, mdesc, NULL);

	RecTimestepDistribution(mdesc.simulation, false);

	return(0);
}

int RecordPoint(Model& mdl, Point& pt, Simulation* sim)
{
	

	double charge = 0.0;
		
	//first do the CB
	pt.n = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.cb.energies.begin(); it != pt.cb.energies.end(); ++it)
		RecordELevel(it->second, pt.n, charge, sim);	//charge is only updated for impurities
	pt.cb.numparticlesold = pt.cb.GetNumParticles();
	pt.cb.SetNumParticles(pt.n);	//apparently never store as divided by volume to remove rounding errors
	
	pt.p = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.vb.energies.begin(); it != pt.vb.energies.end(); ++it)
		RecordELevel(it->second, pt.p, charge, sim);	//charge is only updated for impurities
	pt.vb.numparticlesold = pt.vb.GetNumParticles();
	pt.vb.SetNumParticles(pt.p);
	
	pt.elecindonor = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.donorlike.begin(); it != pt.donorlike.end(); ++it)
		RecordELevel(it->second, pt.elecindonor, charge, sim);	//charge is only updated for impurities

	pt.holeinacceptor = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.acceptorlike.begin(); it != pt.acceptorlike.end(); ++it)
		RecordELevel(it->second, pt.holeinacceptor, charge, sim);	//charge is only updated for impurities

	pt.elecTails = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.CBTails.begin(); it != pt.CBTails.end(); ++it)
		RecordELevel(it->second, pt.elecTails, charge, sim);	//charge is only updated for impurities

	pt.holeTails = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.VBTails.begin(); it != pt.VBTails.end(); ++it)
		RecordELevel(it->second, pt.holeTails, charge, sim);	//charge is only updated for impurities

	double netPosCarriers = DoubleSubtract(pt.p + pt.holeTails + pt.holeinacceptor, pt.n + pt.elecTails + pt.elecindonor);	//p - n - probably very one sided...
	pt.netCharge = DoubleAdd(charge, netPosCarriers);	//subtract out number of electrons
	pt.rho = pt.netCharge * QCHARGE * pt.voli;	//update rho
	
	mdl.poissonRho[pt.adj.self] = pt.rho;	//and make sure it is updated for solving poisson's equation

	pt.srhR1 = pt.srhR1 * pt.voli;	//get rates in normal units instead of just # of carriers
	pt.srhR2 = pt.srhR2 * pt.voli;	//that moved/sec. While useful for seeing where steps are,
	pt.srhR3 = pt.srhR3 * pt.voli;	//having actual rates makes life a bit easier...
	pt.srhR4 = pt.srhR4 * pt.voli;
	pt.genth = pt.genth * pt.voli;
	pt.recth = pt.recth * pt.voli;
	pt.scatterR = pt.scatterR * pt.voli;
	pt.LightAbsorbDef = pt.LightAbsorbDef * pt.voli;
	pt.LightAbsorbSRH = pt.LightAbsorbSRH * pt.voli;
	pt.LightAbsorbGen = pt.LightAbsorbGen * pt.voli;
	pt.LightAbsorbScat = pt.LightAbsorbScat * pt.voli;
	pt.LightEmitDef = pt.LightEmitDef * pt.voli;
	pt.LightEmitRec = pt.LightEmitRec * pt.voli;
	pt.LightEmitScat = pt.LightEmitScat * pt.voli;
	pt.LightEmitSRH = pt.LightEmitSRH * pt.voli;
	//pt.LightPowerEntry does not change

	/*
	This is taken care of in OutputDataInternalPost (output.cpp)
	//the following is more complicated. It requires rate outs from the next point to be calculated as well, so this is WAY invalid.

	pt.Jn.x = pt.Jn.x * pt.areai.x * QCHARGE;	//fix all the current units. - they were in carriers/sec!
	pt.Jp.x = pt.Jp.x * pt.areai.x * QCHARGE;	//now it's charge/sec/cm^2 = A/cm^2
	pt.Jn.y = pt.Jn.y * pt.areai.y * QCHARGE;
	pt.Jp.y = pt.Jp.y * pt.areai.y * QCHARGE;
	pt.Jn.z = pt.Jn.z * pt.areai.z * QCHARGE;
	pt.Jp.z = pt.Jp.z * pt.areai.z * QCHARGE;

	*/
	return(0);
}

int RecordELevel(ELevel& srcElev, double& num, double& charge, Simulation* sim)
{

	if (srcElev.GetContact())
	{
		if (srcElev.GetContact()->GetCurrentActive() & CONTACT_SINK)	//this point acts as a thermal sink for everything! It will not fluctuate at all.
		{
			double curOcc = srcElev.GetEndOccStates();	//just grab the current value and don't change anything
			num += curOcc;	//this is for updating n, p, etc.
			charge += srcElev.GetNumstates()*double(srcElev.charge);
			return(0);	//and get outta here
		}
	}
	

	CalcTransfer(srcElev, sim);	//actually transfer the carriers appropriately by putting it in end occ
	//end is transferred to begin in prep. By getting here, it says that the iteration
	//before this one was valid, so the starting point was valid.

	double curOcc, curAvail;
	srcElev.GetOccupancyOcc(RATE_OCC_BEGIN, curOcc, curAvail);
	
	//if it made it to this point, then these values are OK!
	if (curAvail < curOcc)
		srcElev.SetPrevOccOpposite(curAvail);
	else
		srcElev.SetPrevOcc(curOcc);

	curOcc = srcElev.GetEndOccStates();	//just grab the current value and don't change anything
	num += curOcc;	//this is for updating n, p, etc.
	//we want instantaneous charge here!
	charge += srcElev.GetNumstates()*double(srcElev.charge);

	return(0);
}

int UpdateTargets(ELevel& eLev, Simulation* sim)
{
	if(sim==NULL)
		return(0);

	if(sim->curSSTarget >= eLev.TargetEfs.size())
		return(0);

	double relEf = eLev.TargetEfs[sim->curSSTarget];
	eLev.SetOccFromRelEf(RATE_OCC_TARGET, relEf);
	eLev.UpdateMaxMinTargets(relEf, sim->accuracy);

	return(1);
}

void TimeStepControl::UpdateUseTStep(double delta) {
	if (delta > activeTolerance) {	//activeT > 0, so delta > 0
		delta = activeTolerance / delta;	//the fraction (approximate. Assuming chang ein occ is linear with fermi function. False, but it's presumably a small change, so mostly true
		delta = targetTStep * delta;	//now this is the use time step
		if (delta < useTStep)
			useTStep = delta;
	}
}

double ELevel::TransferCarriersTransient(double& staticCharge) {
	Simulation* sim = getSim();
	if (sim == nullptr || sim->TransientData == nullptr)
		return endoccstates;

	if (GetContact() && (GetContact()->GetCurrentActive() & CONTACT_SINK) == CONTACT_SINK)
		return endoccstates;

	TransientSimData *tsd = sim->TransientData;	//save on typing...
	double tStep = tsd->timestep;
	double tolerance = sqrt(tsd->tStepControl.getTolerance() * sim->accuracy);	//geometric mean of the lowest accuracy, and what it's currently at

	//at this point, partialNRate and netRate should already have the proper values because of testing the total rate.

	double changeOcc = partialNRate * tStep;
	double occ, avail, occend, availend;

	
	AddEndOccStates(changeOcc);

	GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
	GetOccupancyOcc(RATE_OCC_END, occend, availend);
	
	double prevRelEf = CalcRelFermiEnergy(occ, avail);
	double postRelEf = CalcRelFermiEnergy(occend, availend);

	double deltaEf = abs(DoubleSubtract(postRelEf, prevRelEf));

	if (deltaEf > tolerance) {	//ruh roh. This can only happen if the |relative fermi level| is > 1.
		//in this case, the probabilty at 300K is 10^-17. AKA, the limits of a double, so occ or avail = nstates and the
		//other number is ridiculously small. When the other numbers for the point will be about 10+ orders of magnitude larger
		//this is basically a very insignificant point.
		//this might be able to happen in the future with more logic, but it will be to combat the above underlying assumption
		postRelEf = postRelEf > prevRelEf ? prevRelEf + tolerance : prevRelEf - tolerance;
		SetOccFromRelEf(RATE_OCC_END, postRelEf);
	}

	staticCharge += numstates * double(charge);

	if (postRelEf != prevRelEf)
		tsd->tStepControl.AppendState(TimeStepControl::ChangesMade);

	if (deltaEf > tolerance * 0.5) {	//was it a large change (indicating no steady state) and it hadn't hit steady state at this time step/tolerance yet?
		if ((tsd->tStepControl.IsHighestStep() && !partialFlippedSignMax) ||
			(tsd->tStepControl.isDroppingStep() && !partialFlippedSignFall))
			tsd->tStepControl.AppendState(TimeStepControl::LargeChanges);
	}
	return endoccstates;

}

int CalcTransfer(ELevel& eLev, Simulation* sim)	
{


	/*
	New idea, targetOcc is calculated and is roughly equal to the value that will result in
	steady state given everything else stays constant except this energy state.
	This will have set the timestep, so the change should pretty much ALWAYS be valid
	unless it is a really insignificant energy level in terms of occupancy compared to the
	rest of the band.
	*/

	//simplest first
	if(eLev.netRate == 0.0)
		return(0);

//	double numTransfer = GetELevelNumCarrierTransfer(&eLev);	// = GetBeginOccStates() unless tiny
	double occ, avail;//, occend, availend;
	eLev.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);

	double changeOcc = eLev.netRate * sim->TransientData->timestep;
	

	if(changeOcc > 0.0)
	{
		if(occ < avail)	//calculate from occ as there is more accuracy
		{
			if(occ < eLev.tOccMax && occ > eLev.tOccMin)	//is it in the target fermi level zone
			{  //note, this will NOT stop it from being at the max and trying to escape!

				//yes, make sure it will not go beyond the maximum target
				//will it go past the maximum occupancy
				if(changeOcc > eLev.tOccMax - occ)
				{
					eLev.SetEndOccStates(eLev.tOccMax);
					return(0);
				}
			}
		}
		else //same logic, but with avail
		{
			if(avail < eLev.tAvailMax && avail > eLev.tAvailMin)	//is it in the target fermi level zone
			{  //note, this will NOT stop it from being at the max and trying to escape!

				//yes, make sure it will not go beyond the maximum target
				//will it go past the maximum occupancy
				if(changeOcc > avail - eLev.tAvailMin)
				{
					eLev.SetEndOccStatesOpposite(eLev.tAvailMin);
					return(0);
				}
			}
		}
	}
	else if(changeOcc < 0.0)
	{
		if(occ < avail)	//calculate from occ as there is more accuracy
		{
			if(occ < eLev.tOccMax && occ > eLev.tOccMin)	//is it in the target fermi level zone
			{  //note, this will NOT stop it from being at the max and trying to escape!

				//yes, make sure it will not go beyond the maximum target
				//will it go past the maximum occupancy
				if(changeOcc < eLev.tOccMin - occ)
				{
					eLev.SetEndOccStates(eLev.tOccMin);
					return(0);
				}
			}
		}
		else //same logic, but with avail
		{
			if(avail < eLev.tAvailMax && avail > eLev.tAvailMin)	//is it in the target fermi level zone
			{  //note, this will NOT stop it from being at the max and trying to escape!

				//yes, make sure it will not go beyond the maximum target
				//will it go past the maximum occupancy
				if(changeOcc < avail - eLev.tAvailMax)
				{
					eLev.SetEndOccStatesOpposite(eLev.tAvailMax);
					return(0);
				}
			}
		}
	}

	eLev.AddEndOccStates(changeOcc);	//will choose properly based on whether it's occ or avail stored.
	
	/*
	if(sim->curSSTarget < eLev.TargetEfs.size())	//note, curSSTarget is unsigned int, so -1 is huge
	{
//		UpdateTargets(eLev, sim);	//just for now, do it every time. INEFFICIENT, but just get it to work
		if((eLev.netRate > 0.0 && (eLev.targetOcc > occ || eLev.targetAvail < avail)) || 
		   (eLev.netRate < 0.0 && (eLev.targetOcc < occ  || eLev.targetAvail > avail))) //these are the situations which make sense! It is proceeding towards the target
		{
			double dif, newOcc, newAvail;
			if(eLev.targetAvail >= eLev.targetOcc)	//we'll work with the occupancies, as those numbers have more accuracy
			{
				dif = occ - eLev.targetOcc;
				newOcc = dif * exp(eLev.netRate * sim->TransientData->timestep / dif) + eLev.targetOcc; //(-dif) = +c_t - c_i
				//if netrate>0, dif<=0. exp(-)<1, so occ<newOcc<target.
				//if netrate<0, dif>=0, exp(-)<1, so target<newOcc<occ
				dif = newOcc - occ;
				newAvail = eLev.GetNumstates() - newOcc;

				eLev.SetEndOccStates(newOcc);	//done.
				return(0);
			}
			else //we're going to work with availabilities to get better accuracy
			{ //this is essentially the same math as above, but we can flip the rate and treat occupancy as availability
				dif = avail - eLev.targetAvail;
				newAvail = dif * exp(-eLev.netRate * sim->TransientData->timestep / dif) + eLev.targetAvail;
				//if netrate>0, avail decreases, tarO>occ --> avail>tarA, dif>=0, exp(-) < 1, targetAvail < newAvail < avail.
				//if netrate<0, avail increases, occ>tarO --> avail<tarA, dif<=0, exp(-) < 1, avail < newAvail < targetAvail
				dif = newAvail - avail;
				newOcc = eLev.GetNumstates() - newAvail;
				eLev.SetEndOccStatesOpposite(newAvail);	//done
				return(0);
			}
		}
		/ *
		else if(occ != eLev.targetOcc && avail != eLev.targetAvail)
		{
			double origTO = eLev.targetOcc;
			double origTA = eLev.targetAvail;

			if(sim->curSSTarget-1 < eLev.TargetEfs.size())	//could it still be approaching a previous steady state value?
			{
				double relEf = eLev.CalcRelFermiEnergy(RATE_OCC_BEGIN);	//large occupancy = positive Ef
				
				//looking for the largest relative Ef
				double maxMagEf = relEf;	//start at the current relative Ef
				if(eLev.netRate > 0.0)	//look for the highest rel Ef
				{
					for(int testT = int(sim->curSSTarget)-1; testT > eLev.SSTargetCleared; --testT)
					{
						if(eLev.TargetEfs[testT] > maxMagEf)
							maxMagEf = eLev.TargetEfs[testT];
					}
				}
				else if(eLev.netRate < 0.0) //look for the lowest rel Ef.
				{
					for(int testT = int(sim->curSSTarget)-1; testT > eLev.SSTargetCleared; --testT)
					{
						if(eLev.TargetEfs[testT] < maxMagEf)
							maxMagEf = eLev.TargetEfs[testT];
					}
				}

				if(maxMagEf != relEf)	//it found a new possible target state that is consistent
				{
					eLev.SetOccFromRelEf(RATE_OCC_TARGET, relEf);
					double dif, newOcc, newAvail;
					if(eLev.targetAvail >= eLev.targetOcc)	//we'll work with the occupancies, as those numbers have more accuracy
					{
						dif = occ - eLev.targetOcc;
						newOcc = dif * exp(eLev.netRate * sim->TransientData->timestep / dif) + eLev.targetOcc; //(-dif) = +c_t - c_i
						//if netrate>0, dif<=0. exp(-)<1, so occ<newOcc<target.
						//if netrate<0, dif>=0, exp(-)<1, so target<newOcc<occ
						dif = newOcc - occ;
						newAvail = eLev.GetNumstates() - newOcc;

						eLev.SetEndOccStates(newOcc);	//done.

						eLev.targetAvail = origTA;	//restore the original targets
						eLev.targetOcc = origTO;
						return(0);
					}
					else //we're going to work with availabilities to get better accuracy
					{ //this is essentially the same math as above, but we can flip the rate and treat occupancy as availability
						dif = avail - eLev.targetAvail;
						newAvail = dif * exp(-eLev.netRate * sim->TransientData->timestep / dif) + eLev.targetAvail;
						//if netrate>0, avail decreases, tarO>occ --> avail>tarA, dif>=0, exp(-) < 1, targetAvail < newAvail < avail.
						//if netrate<0, avail increases, occ>tarO --> avail<tarA, dif<=0, exp(-) < 1, avail < newAvail < targetAvail
						dif = newAvail - avail;
						newOcc = eLev.GetNumstates() - newAvail;
						eLev.SetEndOccStatesOpposite(newAvail);	//done
						eLev.targetAvail = origTA;	//restore the original targets
						eLev.targetOcc = origTO;
						return(0);
					}
				}
			} //end moving away and there could be states it was previously approaching
		}	//end if/else is the system moving towards or away from steady state	
		* /
		//gotta do it an old fashioned kinda way. Just assume the R(c) = constant, and hope for a small timestep
	}	//end if making sure there is a steady state target



	double change;
	double Origchange = sim->TransientData->timestep * eLev.netRate;	//how much we want this to change
	if(Origchange > 0.0 && Origchange >= avail)	//don't let it go completely full or empty - just most of the way there.
		change = 0.96875 * avail;
	else if(Origchange < 0.0 && -Origchange >= occ)
		change = -0.96875 * occ;
	else
		change = Origchange;

	double error=0.0;

	eLev.AddEndOccStates(change);	//note, beginocc is set to endocc in prepElevel.

	eLev.GetOccupancyOcc(RATE_OCC_END, occend, availend);
	/ *
	if(change > 0.0) //occ is increasing
	{
		if(eLev.tStepCtrl->maxMatch==true && eLev.tStepCtrl->maxOcc > 0.0)
		{ //they line up
			if(occend > eLev.tStepCtrl->maxOcc) //it has moved beyond the limit
			{
//				if(occ < eLev.tStepCtrl->maxOcc) //it wasn't also previously at the maximum occupancy and trying to increase
//				{
					eLev.SetEndOccStates(eLev.tStepCtrl->maxOcc);
//				}
				change = eLev.tStepCtrl->maxOcc - occ;
			}
		}
		else if(eLev.tStepCtrl->minMatch==false && eLev.tStepCtrl->minOcc > 0.0)
		{
			//this means we're looking at avail for minOcc. Occ is increasing, so avail is decreasing
			if(availend < eLev.tStepCtrl->minOcc)
			{
//				if(avail > eLev.tStepCtrl->minOcc) //the availability used to be larger than minOcc
//				{ //so it wasn't at the limit and trying to get out
					eLev.SetEndOccStatesOpposite(eLev.tStepCtrl->minOcc);
//				}
				change = avail - eLev.tStepCtrl->minOcc;
			}
		}
	}
	else if(change < 0.0)	//occ is decreasing
	{
		if(eLev.tStepCtrl->minMatch==true && eLev.tStepCtrl->minOcc > 0.0)
		{ //they line up and there is a minimum
			if(occend < eLev.tStepCtrl->minOcc) //it took away too many carriers
			{
//				if(occ > eLev.tStepCtrl->minOcc) //it wasn't at the limit and still trying to go further (aka, bad limit)
//				{
					eLev.SetEndOccStates(eLev.tStepCtrl->minOcc);
//				}
				change = eLev.tStepCtrl->minOcc - occ;
			}
		}
		else if(eLev.tStepCtrl->maxMatch==false && eLev.tStepCtrl->maxOcc > 0.0)
		{ //the maxOcc is really maxAvail, so therefore minOcc. But we need to work as if avail
			if(availend > eLev.tStepCtrl->maxOcc) //avail went beyond the limit
			{
//				if(avail < eLev.tStepCtrl->maxOcc) //the limit is valid/not reset
//				{
					eLev.SetEndOccStatesOpposite(eLev.tStepCtrl->maxOcc);
//				}
				change = avail - eLev.tStepCtrl->maxOcc;
			}
		}
	}
	* /
	if(sim->outputs.chgError)
	{
		if(eLev.ptrPoint)
		{
			double difference = change - Origchange;	//so if it wanted to change 20 carriers, but only changed 5
			//then it is 15 less than it wanted to be. (-15).
			if(eLev.bandtail)
			{
				if(eLev.carriertype==ELECTRON)	//CB Band tail
				{
					if(difference>0.0)
						eLev.getPoint().cError.cbTP += difference;
					else if(difference<0.0) //it has 15 less electrons than it wanted, so 15 electrons destroyed
						eLev.getPoint().cError.cbTN -= difference;
				}
				else  //VB Band tail
				{ 
					if(difference>0.0)
						eLev.getPoint().cError.vbTN += difference;
					else if(difference<0.0) //it has 15 less holes than it wanted, so 15 holes destroyed
						eLev.getPoint().cError.vbTP -= difference;
				}
			}
			else if(eLev.imp)
			{
				if(eLev.carriertype==ELECTRON)	//donor
				{
					if(difference>0.0)
						eLev.getPoint().cError.donP += difference;
					else if(difference<0.0) //it has 15 less electrons than it wanted, so 15 electrons destroyed
						eLev.getPoint().cError.donN -= difference;
				}
				else  //acceptor
				{ 
					if(difference>0.0)
						eLev.getPoint().cError.acpN += difference;
					else if(difference<0.0) //it has 15 less holes than it wanted, so 15 holes destroyed
						eLev.getPoint().cError.acpP -= difference;
				}
			}
			else if(eLev.carriertype==ELECTRON)	//CB
			{
				if(difference>0.0)
					eLev.getPoint().cError.cbP += difference;
				else if(difference<0.0) //it has 15 less electrons than it wanted, so 15 electrons destroyed
					eLev.getPoint().cError.cbN -= difference;
			}
			else //VB
			{
				if(difference>0.0)
					eLev.getPoint().cError.vbN += difference;
				else if(difference<0.0) //it has 15 less holes than it wanted, so 15 holes destroyed
					eLev.getPoint().cError.vbP -= difference;
			}
		}
	}

	if(sim->TransientData->SSByTransient)
	{
		double tolerance = occ<avail&&occ!=0.0 ? occ*sim->accuracy : avail*sim->accuracy;
		change = fabs(eLev.netRate) * sim->TransientData->SSbyTTime;
		if(change > tolerance)
			sim->TransientData->isSS = false;
	}


/ *	if(numTransfer != occ || eLev.allowTransferReduction)	//flag that this is a really close value to staedy state
	{
		//this is a really insignificant point, and time timestep is likely overexaggerated
		double limit = eLev.targetOcc - occ;
		if((change > 0.0 && change > limit) ||
		   (change < 0.0 && change < limit))
		{
			change = limit;
		}
	}
	*/
	
/*	double testocc = occ + change;
	//go most of the way there
	if(testocc >= numst)
		change = 0.75*(numst - occ);
	else if(testocc <= 0.0)
		change = -0.75 * occ;
*/
//the premise here is to use the timestep to move carriers based on the net rate and net equilibrium rate.
	//the net equilibrium rate is constrained to half the value needed for the largest imbalance in that direction
	//that is within equilibrium.
	//if the equilibrium rate cancels out the net rate, it will do so.
	//if the net rate does not fully cancel out the equilibrium rate, then including the net rate is a folly.
	//this should ensure that the simulation always moves in the proper direction, though there may be a bit of charge
	//creation and destruction once the simulation approaches equilibrium
	/*
	if(change > 0.0)	//it is gaining carriers
	{
		double maxChange;
		if(eLev.LimitByAvailability)
			maxChange = eLev.percentTransfer * (numst - occ);
		else
		{
			maxChange = eLev.percentTransfer * occ;
			if(maxChange + occ > numst)
				maxChange = numst - occ;
		}

		if(DoubleSubtract(change, maxChange) > 0.0)
			change = maxChange;
	}
	else if(change < 0.0)	//it is losing carriers
	{
		double maxNChange = -eLev.percentTransfer * occ;
		//if limit by availability... this is the availability. If not limit by availability, well, this is still the amount
		//that we want done.
		//note, this change can not result in anything that will cause occupancy to be less than zero
		if(DoubleSubtract(change, maxNChange) < 0.0)
			change = maxNChange;
	
	}
	eLev.AddEndOccStates(change);	//note, beginocc is set to endocc in prepElevel.
	*/


	
	
	return(0);
}

//this is a rather complicated algorithm
int FixOverSteps(Model& mdl, ModelDescribe& mdesc)
{
	//first, determine which states need adjusting!
	//need to update the band diagram first! mdl.poissonrho was updated in record all
	CalcBandStruct(mdl, mdesc);

	//then we need to look at the rates of all the energy states, and if it has changed direction
	//indicating that it moved too many carriers, or it moved carriers in that should have been swept
	//away in that timestep
	return(0);
}

int FindOverSteps(Model& mdl, ModelDescribe& mdesc)
{

	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{

		for(std::map<MaxMin<double>, ELevel>::iterator cb = pt->second.cb.energies.begin(); cb != pt->second.cb.energies.end(); ++cb)
		{
			TestEnergyLevel(cb->second, mdesc.simulation);
		}
	}
	return(0);
}

int TestEnergyLevel(ELevel& eLev, Simulation* sim)
{
	double oldRate = eLev.netRate;

	eLev.UpdateAbsFermiEnergy(RATE_OCC_ENDADJUST);	//the fermi levels are going to be important...
	//we will be moving carriers all over the place, and the fermi level is going to provide
	//a fundamental limit

	PosNegSum nRate;
	
	//recalculate all rates!
	double tmpRate;	//assigned in CalcFastRate - init to zero within there
	for(std::vector<ELevel*>::iterator trg = eLev.TargetELev.begin(); trg != eLev.TargetELev.end(); ++trg)
	{
		CalcFastRate(&eLev, *trg, sim, RATE_OCC_ENDADJUST, RATE_OCC_ENDADJUST,tmpRate);	//calculate the rate, but tell it to use the test number of carriers in the source
		nRate.AddValue(tmpRate);
	}

	for(std::vector<TargetE>::iterator trg = eLev.TargetELevDD.begin(); trg != eLev.TargetELevDD.end(); ++trg)
	{
		CalcFastRate(&eLev, trg->target, sim, RATE_OCC_ENDADJUST, RATE_OCC_ENDADJUST,tmpRate);
		nRate.AddValue(tmpRate);
	}
	double netRate = nRate.GetNetSum();

	if(oldRate > 0.0 && netRate < 0.0)
	{
		//this is a point that gained too many carriers
	}
	else if(oldRate < 0.0 && netRate > 0.0)
	{
		//this is a point that lost too many carriers
	}
	//else the rate didn't change direction or miraculously went to zero. No correction needed.
	return(0);
}

int UpdateAllTargetOccs(Model& mdl, Simulation* sim)
{
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->cb.energies.begin(); it != curPt->cb.energies.end(); ++it)
			UpdateTargets(it->second, sim);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->vb.energies.begin(); it != curPt->vb.energies.end(); ++it)
			UpdateTargets(it->second, sim);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->acceptorlike.begin(); it != curPt->acceptorlike.end(); ++it)
			UpdateTargets(it->second, sim);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->donorlike.begin(); it != curPt->donorlike.end(); ++it)
			UpdateTargets(it->second, sim);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->CBTails.begin(); it != curPt->CBTails.end(); ++it)
			UpdateTargets(it->second, sim);

		for (std::map<MaxMin<double>, ELevel>::iterator it = curPt->VBTails.begin(); it != curPt->VBTails.end(); ++it)
			UpdateTargets(it->second, sim);
	}
	return(0);
}
///////////////////////////////////

TimeStepControl::TimeStepControl()
{
	//	Count.clear();
	//	Active.clear();
	for (int i = 0; i<TSTEPSIZE; ++i)
	{
		Count[i] = 0;
		Active[i] = false;
	}
	CutOffs[0] = 1e-60;	//if these change, change the values in GetIndex
	CutOffs[1] = 1e-40; //these are here just to speed up lookups of values a lot...
	CutOffs[2] = 1e-30;
	CutOffs[3] = 1e-28;
	CutOffs[4] = 1e-26;
	CutOffs[5] = 1e-25;
	CutOffs[6] = 1e-24;
	CutOffs[7] = 1e-23;
	CutOffs[8] = 1e-22;
	CutOffs[9] = 1e-21;
	CutOffs[10] = 1e-20;
	CutOffs[11] = 1e-19;
	CutOffs[12] = 1e-18;
	CutOffs[13] = 1e-17;
	CutOffs[14] = 1e-16;
	CutOffs[15] = 1e-15;
	CutOffs[16] = 1e-14;
	CutOffs[17] = 1e-13;
	CutOffs[18] = 1e-12;
	CutOffs[19] = 1e-11;
	CutOffs[20] = 1e-10;
	CutOffs[21] = 1e-9;
	CutOffs[22] = 1e-8;
	CutOffs[23] = 1e-7;
	CutOffs[24] = 1e-6;
	CutOffs[25] = 1e-5;
	CutOffs[26] = 1e-4;
	CutOffs[27] = 1e-4;
	CutOffs[28] = 1e-3;
	CutOffs[29] = 1e-2;
	CutOffs[30] = 0.1;
	CutOffs[31] = 1.0;

	TStepMin = 0.0;
	ActiveMustBeGreater = -1;	//I find a timestep under 10^-1000 to be highly unlikely
	NumRepeatBeforeNextStep = 100;	//log 10 scaling, but by 2's, so let's default to 100
	TimeToNextMilestone = 0.0;
	IncreasePercentTransfer = false;
	maxTStepAllowed = maxTStepUsed = targetTStep = useTStep = activeTolerance = maxStepTolerance = startTimeData = timeFallDownLeft = 0.0;
	state = 0;
	numLevelsDown = 0;
	lastOutTime = -1.0;
}

TimeStepControl::TimeStepControl(int numRepeat)
{
	for (int i = 0; i<TSTEPSIZE; ++i)
	{
		Count[i] = 0;
		Active[i] = false;
	}
	CutOffs[0] = 1e-60;	//if these change, change the values in GetIndex
	CutOffs[1] = 1e-40; //these are here just to speed up lookups of values a lot...
	CutOffs[2] = 1e-30;
	CutOffs[3] = 1e-28;
	CutOffs[4] = 1e-26;
	CutOffs[5] = 1e-25;
	CutOffs[6] = 1e-24;
	CutOffs[7] = 1e-23;
	CutOffs[8] = 1e-22;
	CutOffs[9] = 1e-21;
	CutOffs[10] = 1e-20;
	CutOffs[11] = 1e-19;
	CutOffs[12] = 1e-18;
	CutOffs[13] = 1e-17;
	CutOffs[14] = 1e-16;
	CutOffs[15] = 1e-15;
	CutOffs[16] = 1e-14;
	CutOffs[17] = 1e-13;
	CutOffs[18] = 1e-12;
	CutOffs[19] = 1e-11;
	CutOffs[20] = 1e-10;
	CutOffs[21] = 1e-9;
	CutOffs[22] = 1e-8;
	CutOffs[23] = 1e-7;
	CutOffs[24] = 1e-6;
	CutOffs[25] = 1e-5;
	CutOffs[26] = 1e-4;
	CutOffs[27] = 1e-4;
	CutOffs[28] = 1e-3;
	CutOffs[29] = 1e-2;
	CutOffs[30] = 0.1;
	CutOffs[31] = 1.0;

	TStepMin = 0.0;
	ActiveMustBeGreater = -1;	//I find a timestep under 10^-1000 to be highly unlikely
	NumRepeatBeforeNextStep = numRepeat;
	TimeToNextMilestone = 0.0;
	IncreasePercentTransfer = false;
	maxTStepAllowed = maxTStepUsed = targetTStep = useTStep = activeTolerance = maxStepTolerance = startTimeData = timeFallDownLeft = 0.0;
	state = 0;
	numLevelsDown = 0;
}

TimeStepControl::~TimeStepControl()
{
	for (int i = 0; i<TSTEPSIZE; ++i)
	{
		Count[i] = 0;
		Active[i] = false;
	}
}

double TimeStepControl::DetermineTStep(double netRate, double numCarriers, double percentMove)
{
	double maxMove = numCarriers * percentMove;
	if (IncreasePercentTransfer && percentMove < INIT_PERCENT_TRANSFER)	//have to get this before
		maxMove = maxMove * INCREASE_PERCENT_TRANSFER;

	if (netRate != 0.0 && percentMove >= MIN_PERCENT_IGNORE_TIMESTEP)
		return(fabs(maxMove / netRate));
	return(INFINITETSTEP);
}

bool TimeStepControl::RateIsOK(double netRate, double numCarriers, double percentMove)
{
	double tStep = DetermineTStep(netRate, numCarriers, percentMove);
	if (tStep == INFINITETSTEP || tStep >= TStepMin)	//if TStepMin set to INFINITE (-1), then all rates are OK
		return(true);
	return(false);
}

double TimeStepControl::UpdateActiveRate(double netRate, double numCarriers, double percentMove)
{
	double tStep = DetermineTStep(netRate, numCarriers, percentMove);
	if (tStep == INFINITETSTEP)
		return(0);

	int ind = GetIndex(tStep);
	if (ind != -1)
		Active[ind] = true;

	return(tStep);
}
/*
double TimeStepControl::UpdateActiveRate(ELevelOpticalRate& rate)
{
double numCarriers = 0.0;	//assigned in DetermineNumCarrierTransfer
double NRate = DoubleSubtract(rate.RateIn, rate.RateOut);
rate.source->LimitByAvailability = rate.DetermineNumCarrierTransfer(numCarriers, NRate);

double tStep = DetermineTStep(rate.netrate, numCarriers, rate.source->percentTransfer);
if(tStep == INFINITETSTEP)
return(INFINITETSTEP);

int ind = GetIndex(tStep);
if(ind != -1)
Active[ind] = true;

return(tStep);
}
*/

double TimeStepControl::UpdateActiveRate(ELevelRate& rate)
{
	double numCarriers = 0.0;	//assigned in DetermineNumCarrierTransfer
	double NRate = DoubleSubtract(rate.RateIn, rate.RateOut);
	rate.source->LimitByAvailability = rate.DetermineNumCarrierTransfer(numCarriers, NRate);

	double tStep = DetermineTStep(rate.netrate, numCarriers, rate.source->percentTransfer);
	if (tStep == INFINITETSTEP)
		return(INFINITETSTEP);

	//	if(rate.source->getPoint().contactData)	//don't let contacts influence the time step
	//		if(rate.source->getPoint().contactData->isolated == false)
	//			return(INFINITETSTEP);

	int ind = GetIndex(tStep);
	if (ind != -1)
		Active[ind] = true;

	return(tStep);
}

int TimeStepControl::GetIndex(double val)
{
	//basically have a fixed tree
	/*
	10^### as the cutoffs.
	31	0			>= 1.0
	30	-1			>= 0.1
	29	-2			>= 0.01
	28	-3			>= 1e-3
	27	-4			>= 1e-4
	26	-5			>= 1e-5
	25	-6			>= 1e-6
	24	-7			>= 1e-7
	23	-8
	22	-9
	21	-10
	20	-11
	19	-12
	18	-13
	17	-14
	16	-15
	15	-16
	14	-17
	13	-18
	12	-19
	11	-20
	10	-21
	9	-22
	8	-23
	7	-24
	6	-25
	5	-26
	4	-28
	3	-30
	2	-40
	1	-60
	0	<	really, really small - say there is no limit at all


	*/

	if (val >= 1e-15)
	{
		if (val >= 1e-7)
		{
			if (val >= 1e-3)
			{
				if (val >= 1e-1)
				{
					if (val >= 1.0e0)
						return(31);	//>= 1.0
					else
						return(30);	//>= 0.1
				}
				else
				{
					if (val >= 1e-2)
						return(29);	//>=1e-2
					else
						return(28);	//>=1e-3
				}
			}
			else
			{
				if (val >= 1.0e-5)
				{
					if (val >= 1.0e-4)
						return(27);	//>=1e-4
					else
						return(26);	//>=1e-5
				}
				else
				{
					if (val >= 1.0e-6)
						return(25); //>=1e-6
					else
						return(24);	//>=1e-7
				}
			}
		}
		else
		{
			if (val >= 1.0e-11)
			{
				if (val >= 1.0e-9)
				{
					if (val >= 1.0e-8)
						return(23);	//>=1e-8
					else
						return(22);	//>=1e-9
				}
				else
				{
					if (val >= 1.0e-10)
						return(21);	//>=1e-10
					else
						return(20);	//>=1e-11
				}
			}
			else
			{
				if (val >= 1.0e-13)
				{
					if (val >= 1.0e-12)
						return(19);	//>=1e-12
					else
						return(18);	//>=1e-13
				}
				else
				{
					if (val >= 1.0e-14)
						return(17);	//>=1e-14
					else
						return(16);	//>=1e-15
				}
			}
		}
	}
	else
	{
		if (val >= 1.0e-23)
		{
			if (val >= 1.0e-19)
			{
				if (val >= 1.0e-17)
				{
					if (val >= 1.0e-16)
						return(15);	//>=1e-16
					else
						return(14);	//>=1e-17
				}
				else
				{
					if (val >= 1.0e-18)
						return(13);	//>=1e-18
					else
						return(12);	//>=1e-19
				}
			}
			else
			{
				if (val >= 1.0e-21)
				{
					if (val >= 1.0e-20)
						return(11);	//>=1e-20
					else
						return(10);	//>=1e-21
				}
				else
				{
					if (val >= 1.0e-22)
						return(9); //>=1e-22
					else
						return(8);	//>=1e-23
				}
			}
		}
		else
		{
			if (val >= 1.0e-28)
			{
				if (val >= 1.0e-25)
				{
					if (val >= 1.0e-24)
						return(7);	//>=1e-24
					else
						return(6);	//>=1e-25
				}
				else
				{
					if (val >= 1.0e-26)
						return(5);	//>=1e-26
					else
						return(4);	//>=1e-28
				}
			}
			else
			{
				if (val >= 1.0e-40)
				{
					if (val >= 1.0e-30)
						return(3);	//>=1e-30
					else
						return(2);	//>=1e-40
				}
				else
				{
					if (val >= 1.0e-60)
						return(1);	//>=1.0e-60
					else
						return(0);	//no limit
				}
			}
		}
	}
	return(-1);
}


int TimeStepControl::ClearLowerCounts(int val)
{
	//must clear out count for all below - aka, delete or set to zero. Set to zero is probably cleanest
	for (int i = 0; i<val; ++i)
		Count[i] = 0;

	return(0);
}
void TimeStepControl::ClearArrays(void)
{
	for (int i = 0; i<TSTEPSIZE; ++i)
	{
		Count[i] = 0;
		Active[i] = false;
	}
	return;
}

int TimeStepControl::IterationUpdateInternal(Simulation* sim)
{
	UpdateTimeLimit(sim->TransientData->simtime, sim->TransientData->curTargettime);
	useTStep = targetTStep;	//reinitiailzie for the start!
	if (sim->mdlData) { //....this better be flippin' true. but maybe call this in a gui...?
		Model& mdl = *sim->mdlData;
		for (auto it = mdl.gridp.begin(); it != mdl.gridp.end(); ++it) {
			Point& pt = it->second;
			for (auto el = pt.cb.energies.begin(); el != pt.cb.energies.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);

			for (auto el = pt.vb.energies.begin(); el != pt.vb.energies.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);

			for (auto el = pt.donorlike.begin(); el != pt.donorlike.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);

			for (auto el = pt.acceptorlike.begin(); el != pt.acceptorlike.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);

			for (auto el = pt.CBTails.begin(); el != pt.CBTails.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);

			for (auto el = pt.VBTails.begin(); el != pt.VBTails.end(); ++el)
				el->second.TestTotalRate(RATE_OCC_BEGIN);
		}
	}

	if (useTStep > TimeToNextMilestone && TimeToNextMilestone > 0.0)
		useTStep = TimeToNextMilestone;

	sim->TransientData->timestep = useTStep;	//just make sure it's consistent if I miss using some of these values
	sim->TransientData->timestepi = 1.0 / useTStep;

//	if (IsHighestStep())	//only move time forward on the large time steps. Only relevant on the other transient methodology
		sim->TransientData->iterEndtime = sim->TransientData->simtime + useTStep;


	//any frequency checks will have limited the targetTStep in the first place.

	//everything below to be deprecated.

	IncreasePercentTransfer = false;	//default to off
	if (TimeToNextMilestone > 0.0)
	{
		int MaxActiveMustBeGreater = GetIndex(TimeToNextMilestone) - 2;	//give it some time to approach the timestep nicely
		if (MaxActiveMustBeGreater < 0)
			MaxActiveMustBeGreater = 0;
		//ensure rates 2 order of magnitude lower will be able to occur (because it must be greater...)
		if (ActiveMustBeGreater > MaxActiveMustBeGreater)
			ActiveMustBeGreater = MaxActiveMustBeGreater;
	}

	int ptrAct = ActiveMustBeGreater + 1;	//usually results in zero, at most, TSTEPSIZE
	for (; ptrAct < TSTEPSIZE; ++ptrAct)	//we are looking for the lowest order of magnitude of active rates
	{
		if (Active[ptrAct] == true)
			break;
	}

	if (ptrAct == TSTEPSIZE)	//time for a full reset - everything is infinite
	{
		for (int i = 1; i<TSTEPSIZE; ++i)
			Count[i] = 0; //full reset...

		Count[0] = 1;
		ActiveMustBeGreater = -1;	//can't let it exclude the lowest values! This should allow everything
		TStepMin = INFINITETSTEP;	//make sure it allows everything
	}
	else if (ptrAct >= 0)//the active was used
	{
		Count[ptrAct]++;
		if (Count[ptrAct] >= NumRepeatBeforeNextStep)
		{
			ActiveMustBeGreater = ptrAct;	//prep for next iteration
			TStepMin = (ptrAct>0) ? CutOffs[ptrAct - 1] : INFINITETSTEP;	//set the minimum timestep for individual net rates
			IncreasePercentTransfer = true;
		}
		else
		{
			ClearLowerCounts(ptrAct);
			ActiveMustBeGreater = -1;	//back to the fastest rate
			TStepMin = (ptrAct>0) ? CutOffs[ptrAct - 1] : INFINITETSTEP;
		}
	}
	else // not sure how this would occur
	{
		Count[0] = 1;
		ActiveMustBeGreater = -1;
		TStepMin = INFINITETSTEP;
	}

	for (int i = 0; i<TSTEPSIZE; ++i) 	//figured out everything for this particular set of data, now clear the data
		Active[i] = false;

	return(0);
}


double TimeStepControl::UpdateTimeLimit(double curTime, double targetTime)
{
	TimeToNextMilestone = targetTime - curTime;
	if (TimeToNextMilestone < 0.0)
		TimeToNextMilestone = INFINITETSTEP;

	return(TimeToNextMilestone);
}




MonitorTStep::MonitorTStep()
{
	ClearTStep();
	CutOffs[0] = 1e-200;	//everything less than 1e-190
	CutOffs[1] = 1e-190;	//1e-190 to 1e-180
	CutOffs[2] = 1e-180;
	CutOffs[3] = 1e-170;
	CutOffs[4] = 1e-160;
	CutOffs[5] = 1e-150;
	CutOffs[6] = 1e-140;
	CutOffs[7] = 1e-130;
	CutOffs[8] = 1e-120;
	CutOffs[9] = 1e-110;
	CutOffs[10] = 1e-100;
	CutOffs[11] = 1e-90;
	CutOffs[12] = 1e-80;
	CutOffs[13] = 1e-70;
	CutOffs[14] = 1e-60;
	CutOffs[15] = 1e-50;
	CutOffs[16] = 1e-45;
	CutOffs[17] = 1e-40;
	CutOffs[18] = 1e-35;
	CutOffs[19] = 1e-30;
	CutOffs[20] = 1e-25;
	CutOffs[21] = 1e-20;
	CutOffs[22] = 1e-18;
	CutOffs[23] = 1e-16;
	CutOffs[24] = 1e-14;
	CutOffs[25] = 1e-12;
	CutOffs[26] = 1e-10;
	CutOffs[27] = 1e-08;
	CutOffs[28] = 1e-06;
	CutOffs[29] = 1e-04;	//1e-4 to 1e-2
	CutOffs[30] = 1e-02; //1e-2 to 1
	CutOffs[31] = 1;	//TSTEPSIZE=32, > 1


}

int MonitorTStep::ClearTStep()
{
	for (unsigned int i = 0; i<TSTEPSIZE; ++i)
		Count[i] = 0;
	return(0);
}

int MonitorTStep::AddTStep(double step)
{
	if (step < 0.0)	//'infinite'
	{
		Count[TSTEPSIZE - 1]++;
		return(TSTEPSIZE - 1);
	}

	for (unsigned int i = TSTEPSIZE - 1; i<TSTEPSIZE; --i)
	{
		if (step > CutOffs[i])
		{
			Count[i]++;
			return(i);
		}
	}
	Count[0]++;
	return(0);
}


bool MonitorTStep::isMaxedOut(double SSTime)
{
	for (unsigned int i = 0; i<TSTEPSIZE - 1; ++i)	//don't include the last one
	{ //start with the shortest time frame
		if (Count[i] > 0)	//there was a bad state here
		{
			if (SSTime > CutOffs[i + 1]) //and the time step desired (next one) is less than the steady state time
				return(false);
		}
	}
	return(true);
}

//////////////////////////////
TStepELevel::TStepELevel()
{
	Reset();
}

void TStepELevel::Reset(void)
{
	occ0 = occ1 = occ2 = occ3 = r0 = r1 = r2 = r3 = del1 = del2 = del3 = 0.0;
	numGood = 0;
	istype0 = istype1 = istype2 = istype3 = minMatch = maxMatch = true;
	minOcc = maxOcc = -1.0;
	tStepDesire = INFINITETSTEP;
}

int TStepELevel::GetRets(double& ret1, double& ret2, double& ret3)
{
	ret1 = del1;
	ret2 = del2;
	ret3 = del3;
	return(numGood);
}

int TStepELevel::AddROcc(double occ, double nRate, bool matchocc, double accuracy)
{
	//first, see if any data needs to be cleared out
	//note, 3 will be cleared out anyway
	if (matchocc != istype0)
	{
		istype2 = istype1 = istype0 = !matchocc;
		r2 = r1 = r0 = occ2 = occ1 = occ0 = 0.0;	//make all this data invalid
	}
	else if (matchocc != istype1)
	{
		istype2 = istype1 = !matchocc;	//then 1 & 2 are invalidated
		r2 = r1 = occ2 = occ1 = 0.0;
	}
	else if (matchocc != istype2)
	{
		istype2 = !matchocc;
		r2 = occ2 = 0.0;
	}
	//now shift everything over provided it's new data (initialized to 0)
	if (occ != occ0 || occ0 == 0.0)	//gotta let it start assigning values as well!
	{
		r3 = r2;
		r2 = r1;
		r1 = r0;
		r0 = nRate;
		occ3 = occ2;
		occ2 = occ1;
		occ1 = occ0;
		occ0 = occ;
		istype3 = istype2;
		istype2 = istype1;
		istype1 = istype0;
		istype0 = matchocc;
	}
	else
	{
		//it really didn't change. Just update r0 to be the closest data point
		r0 = nRate;
	}

	if (minMatch != maxMatch)	//make sure we're comparing apples to apples!
	{
		minMatch = maxMatch = matchocc;
		minOcc = maxOcc = -1.0;
	}

	if ((nRate > 0.0 && matchocc) || (nRate < 0.0 && !matchocc))
	{
		//nRate>0 = carriers entering, not enough within
		if (occ > minOcc || minOcc < 0.0)
		{
			minOcc = occ;
			minMatch = matchocc;
		}
		//carriers are trying to enter but this supposedly was the maximum
		if (occ >= maxOcc && maxOcc>0.0)
		{
			//let it keep drifting in the correct direction - slowly. The occupancy will reset min/maxOcc
			//a number of times between iterations. The user can control this, and therefore can choose their timing/accuracy

			maxOcc = (accuracy<1.0 && accuracy > 1.0e-15) ? occ*(1.0 + accuracy) : occ*1.0000000001;	//1+1e-10 default

			/*
			old way
			if(occ == occ1)	//so if the rates are of roughly the same magnitude, but switching sign, it's probably still in the neighborhood of the right answer
			{
			if(fabs(nRate)+fabs(r1) < 10.0*fabs(r1))
			maxOcc = occ * (1.0+accuracy);
			else
			maxOcc=-1.0;
			}
			else if(occ == occ2)
			{
			if(fabs(nRate)+fabs(r2) < 10.0*fabs(r2))
			maxOcc = occ * (1.0+accuracy);
			else
			maxOcc=-1.0;
			}
			else if(occ == occ3)
			{
			if(fabs(nRate)+fabs(r3) < 10.0*fabs(r3))
			maxOcc = occ * (1.0+accuracy);
			else
			maxOcc=-1.0;
			}
			else
			maxOcc = -1.0;	//reset max Occ
			*/
		}
	}
	else if ((nRate < 0.0 && matchocc) || (nRate > 0.0 && !matchocc))
	{
		if (occ < maxOcc || maxOcc < 0.0)
		{
			maxOcc = occ;
			maxMatch = matchocc;
		}
		//carriers are trying to leave, but this is at or below the minimum
		if (occ <= minOcc)	//occ>0, if minOcc <0 and invalid, still won't pass test
		{
			minOcc = (accuracy<1.0 && accuracy > 1.0e-15) ? occ*(1.0 - accuracy) : occ*0.9999999999;	//1-1e-10 default
			/*
			old way
			if(occ == occ1)	//so if the rates are of roughly the same magnitude, but switching sign, it's probably still in the neighborhood of the right answer
			{
			if(fabs(nRate)+fabs(r1) < 10.0*fabs(r1))
			minOcc = occ * (1.0-accuracy);
			else
			minOcc=-1.0;
			}
			else if(occ == occ2)
			{
			if(fabs(nRate)+fabs(r2) < 10.0*fabs(r2))
			minOcc = occ * (1.0-accuracy);
			else
			minOcc=-1.0;
			}
			else if(occ == occ3)
			{
			if(fabs(nRate)+fabs(r3) < 10.0*fabs(r3))
			minOcc = occ * (1.0-accuracy);
			else
			minOcc=-1.0;
			}
			else
			minOcc = -1.0;	//reset max Occ
			*/
		}
	}
	else if (nRate == 0.0)	//it found the solution! it's basically done.
	{
		maxMatch = matchocc;
		minMatch = matchocc;
		double minDelta = (accuracy >= 1e-15 && accuracy<0.25) ? 0.5*occ*accuracy : occ*5e-11;
		maxOcc = occ + minDelta;
		minOcc = occ - minDelta;

	}

	//make sure max and min always have a spacing of at least max*accuracy, or decent sized buffer zone
	if (maxOcc < 1.0e-288 && maxOcc>0.0)
		maxOcc = 1.0e-288;	//basically allow a certain number of digits close to zero

	if (minOcc != -1.0 && maxOcc != -1.0)
	{
		double minDelta = (accuracy >= 1e-15 && accuracy<0.25) ? maxOcc*accuracy : maxOcc*1e-10;
		double del = maxOcc - minOcc;
		if (del < minDelta)
			minOcc = maxOcc - minDelta;	//equiv to (1-accuracy)*maxOcc, so >0 and valid
	}

	return(1);
}

int TStepELevel::SetRets(double val)
{
	numGood = 1;
	del1 = val;
	del2 = del3 = 0.0;
	return(1);
}
int TStepELevel::SetRets(double val1, double val2)
{
	numGood = 2;
	del1 = val1;
	del2 = val2;
	del3 = 0.0;
	return(2);
}
int TStepELevel::SetRets(double val1, double val2, double val3)
{
	numGood = 3;
	del1 = val1;
	del2 = val2;
	del3 = val3;
	return(3);
}

int TStepELevel::SetRets()
{
	int numUse = 0;
	numGood = 0;
	if (r0 != 0.0 && occ0 != 0.0)
	{
		numUse++;
		if (istype0 == istype1 && occ1 != 0.0)
		{
			numUse++;
			if (istype0 == istype2 && occ2 != 0.0)
			{
				numUse++;
				if (istype0 == istype3 && occ3 != 0.0)
					numUse++;
			}
		}
	}
	if (occ0 != 0.0 && r0 == 0.0)	//it's on the answer!
	{
		del1 = del2 = del3 = 0.0;	//just say it don't change!
		numGood = 1;
		return(1);
	}
	if (numUse <= 1)
	{
		del1 = del2 = del3 = 0.0;
		numGood = 0;
		return(0);
	}
	if (numUse == 2)
	{
		//solve linear
		if (occ0 == occ1 || r0 == r1)
		{
			//can't get an answer
			del1 = del2 = del3 = 0.0;
			numGood = 0;
			return(0);
		}
		double deriv = (r0 - r1) / (occ0 - occ1);
		del1 = -r0 / deriv;
		del2 = del3 = 0.0;
		numGood = 1;
		return(1);
	}
	else if (numUse == 3)
	{
		//solve linear, quadratic doesn't always result in a solution
		//the first one must not match either of the next two
		if (occ0 != occ1 && r0 != r1)
		{
			double deriv = (r0 - r1) / (occ0 - occ1);
			del1 = -r0 / deriv;
			del2 = del3 = 0.0;
			numGood = 1;
			return(1);
		}
		if (occ0 != occ2 && r0 != r2)
		{
			double deriv = (r0 - r2) / (occ0 - occ2);
			del1 = -r0 / deriv;
			del2 = del3 = 0.0;
			numGood = 1;
			return(1);
		}
		del1 = del2 = del3 = 0.0;
		numGood = 0;
		return(0);
	}
	else if (numUse == 4)
	{
		//first make sure all the c's are different
		if (occ0 == occ1 || occ0 == occ2 || occ0 == occ3 ||
			occ1 == occ2 || occ1 == occ3 || occ2 == occ3)
		{
			//one of them is the same, so now we have to do the linear bit.
			if (occ0 != occ1 && r0 != r1) //first try the most recent
			{
				double deriv = (r0 - r1) / (occ0 - occ1);
				del1 = -r0 / deriv;
				del2 = del3 = 0.0;
				numGood = 1;
				return(1);
			}
			if (occ0 != occ2 && r0 != r2) //then the second most recent
			{
				double deriv = (r0 - r2) / (occ0 - occ2);
				del1 = -r0 / deriv;
				del2 = del3 = 0.0;
				numGood = 1;
				return(1);
			}
			if (occ0 != occ3 && r0 != r3) //then the least most recent
			{
				double deriv = (r0 - r3) / (occ0 - occ3);
				del1 = -r0 / deriv;
				del2 = del3 = 0.0;
				numGood = 1;
				return(1);
			}
			//then this most recent one is bad with all of them and is no good
			del1 = del2 = del3 = 0.0;
			numGood = 0;
			return(0);
		}

		//now time to do some icky match to get the answers. All the occupancies are different.
		double o[4];
		double r[4];
		//the carriers/rates need to be organized such that c0 is furthest to the left when
		//getting higher order derivatives
		double deriv1, deriv2, deriv3;	//1st, 2nd, and 3rd derivative
		double a, b, c, d;	//four terms in third order taylor series


		//now to order for the third deriv... i don't feel like typing out 4! possibilities.
		o[0] = occ0;
		o[1] = occ1;
		o[2] = occ2;
		o[3] = occ3;
		r[0] = r0;
		r[1] = r1;
		r[2] = r2;
		r[3] = r3;
		//sequentially finds the smallest available number and puts it in the current (lowest) slot
		for (int i = 0; i<3; ++i)	//when i=3, j=4, nothing to test against
		{
			for (int j = i + 1; j<4; ++j)
			{
				if (o[i] > o[j])	//they need to be swapped
				{
					double tmp = o[j];
					o[j] = o[i];
					o[i] = tmp;
					tmp = r[j];
					r[j] = r[i];
					r[i] = tmp;
				}
			}
		}
		//now that they are all in order, we need to make sure that it constantly
		//increases or decreases, otherwise it's going to converge on a weird number.
		//this would mean we need just first order

		if (!(	//check the condition that they are all bigger or smaller than the next, and then say not that...
			(r[0]>r[1] && r[1]>r[2] && r[2]>r[3]) ||
			(r[0]<r[1] && r[1]<r[2] && r[2]<r[3])))
		{
			double deriv = (r0 - r1) / (occ0 - occ1);	//just use the most recent data to first order
			del1 = -r0 / deriv;
			del2 = del3 = 0.0;
			numGood = 1;
			return(1);
		}



		a = (r[3] - r[2]) / (o[3] - o[2]);
		deriv2 = 0.5*(o[3] - o[1]);
		deriv2 = (a - (r[2] - r[1]) / (o[2] - o[1])) / deriv2;	//2nd deriv of RHS

		a = (r[2] - r[1]) / (o[2] - o[1]);
		deriv3 = 0.5*(o[2] - o[0]);
		deriv3 = (a - (r[1] - r[0]) / (o[1] - o[0])) / deriv3;	//2nd deriv of LHS

		deriv3 = 3.0 * (deriv2 - deriv3) / (o[3] - o[0]);

		//now get 2nd deriv

		//they're in order, but need to remove occ3/r3 from the list
		if (occ3 == o[0] && r3 == r[0])
		{
			//move the others abov eup
			o[0] = o[1];
			o[1] = o[2];
			o[2] = o[3];
			r[0] = r[1];
			r[1] = r[2];
			r[2] = r[3];
		}
		else if (occ3 == o[1] && r3 == r[1])
		{
			o[1] = o[2];
			o[2] = o[3];
			r[1] = r[2];
			r[2] = r[3];
		}
		else if (occ3 == o[2] && r3 == r[2])
		{
			o[2] = o[3];
			r[2] = r[3];
		}
		//else it stayed at the end and can be ignored

		deriv1 = (r[2] - r[1]) / (o[2] - o[1]);
		deriv2 = 0.5*(o[2] - o[0]);
		deriv2 = (deriv1 - (r[1] - r[0]) / (o[1] - o[0])) / deriv2;

		deriv1 = (r0 - r1) / (occ0 - occ1);

		a = deriv3 / 6.0;
		b = 0.5 * deriv2;
		c = deriv1;
		d = r0;

		if (a == 0.0)
		{
			//lotta work for nothing...
			del1 = -r0 / deriv1;
			del2 = del3 = 0.0;
			numGood = 1;
			return(0);
		}

		std::complex<double> param, sol1, sol2, sol3;

		double d0 = b*b - 3 * a * c;
		double d1 = 2 * b*b*b - 9 * a*b*c + 27 * a*a*d;
		double sqrtterm = d1 * d1 - 4.0 * d0 * d0 * d0;

		if (MY_IS_FINITE(sqrtterm) == false) //check to see if numbers are going to work
		{
			//lotta work for nothing...
			del1 = -r0 / deriv1;
			del2 = del3 = 0.0;
			numGood = 1;
			return(0);
		}
		std::complex<double> sqrtt2 /*(sqrtterm, 0.0);*/(-18 * a*b*c*d + 4 * b*b*b*d - b*b*c*c + 4 * a*c*c*c + 27 * a*a*d*d, 0.0);
		param = CubeRootComplex(0.5*(d1 + 3 * fabs(a)*sqrt(3.0*sqrtt2)));
		if (param.real() == 0.0)	//then this is a special case. we want the remaining bit to be the negative square root of d1 in sqrt(d1^2-4d0^3). It perfectly cancelled
		{
			if (d1 == 0.0)
			{
				del1 = -b / (3.0*a);
				del2 = del3 = 0.0;
				numGood = 1;
				return(numGood);
			}
			param = CubeRootComplex(std::complex<double>(d1, 0.0));	//this means (d1 - (-d1))/2, or 2d1/2.
		}
		sol1 = 1.0;
		sol2 = std::complex<double>(-1.0, sqrt(3));
		sol2 = 0.5*sol2;
		sol3 = std::complex<double>(-0.5, -sol2.imag());

		sol1 = -1.0 / (3.0*a)*(b + param + d0 / param);
		sol2 = -1.0 / (3.0*a)*(b + sol2*param + d0 / (sol2*param));
		sol3 = -1.0 / (3.0*a)*(b + sol3*param + d0 / (sol3*param));

		numGood = 0;
		if (DoubleEqual(fabs(sol1.real()), abs(sol1)))	//there is essentially no imaginary portion
		{
			o[numGood] = sol1.real();	//temporary answers
			numGood++;
		}
		if (DoubleEqual(fabs(sol2.real()), abs(sol2)))	//there is essentially no imaginary portion
		{
			o[numGood] = sol2.real();	//temporary answers
			numGood++;
		}
		if (DoubleEqual(fabs(sol3.real()), abs(sol3)))	//there is essentially no imaginary portion
		{
			o[numGood] = sol3.real();	//temporary answers
			numGood++;
		}

		if (numGood == 0)
		{
			del1 = del2 = del3 = 0.0;
			return(0);
		}
		else if (numGood == 1)
		{
			del1 = o[0];
			del2 = del3 = 0.0;
			return(1);
		}
		else if (numGood == 2)
		{
			if (fabs(o[0]) < fabs(o[1]))
			{
				del1 = o[0];
				del2 = o[1];
				del3 = 0.0;
			}
			else
			{
				del1 = o[1];
				del2 = o[0];
				del3 = 0.0;
			}
		}
		else
		{
			if (fabs(o[1]) < fabs(o[0]))
			{
				d0 = o[0];	//tmp
				o[0] = o[1];
				o[1] = d0;
			}

			if (fabs(o[2]) < fabs(o[0]))
			{
				d0 = o[0];	//tmp
				o[0] = o[2];
				o[2] = d0;
			}

			if (fabs(o[2]) < fabs(o[1]))
			{
				d0 = o[1];	//tmp
				o[1] = o[2];
				o[2] = d0;
			}
			del1 = o[0];
			del2 = o[1];
			del3 = o[2];
		}
	}
	return(numGood);
}
///////////////////////////////////
int TransientSimData::SetupTimeStep()
{
	if (SSByTransient) //totally different rules
	{
		if (nexttimestep < 0.0)
			nexttimestep = SSbyTTime;
		if (nexttimestep > SSbyTTime)
			nexttimestep = SSbyTTime;

		timestep = nexttimestep;
		timestepi = 1.0 / timestep;
		iterEndtime = simtime;	//it doesn't actually progress forward in time! Just keep going at current 'time' until it hits SS
		return(0);
	}
	double tempfreq = 0.0;
	double maxfreq = 0.0;
	for (unsigned int j = 0; j<sim->contacts.size(); j++)	//update all the contact voltages as they have changed to new values...
	{
		tempfreq = sim->contacts[j]->GetFrequency();
		if (tempfreq > maxfreq)
			maxfreq = tempfreq;
	}

	if (nexttimestep <= 0.0 || nexttimestep == simendtime)	//if the next time step is simendtime, that means there are rates that are in steady state, and everything else is diverging and was ignored.
	{
		timestep = simendtime - simtime;	//just force it to hit curtargettime (later in code cuts down)
	}
	else
		timestep = nexttimestep;

	if (timestep > tstepConsts.prevtimestep*10.0 && tstepConsts.prevtimestep != 0.0)
		timestep = tstepConsts.prevtimestep*10.0;


	if (maxfreq > 0.0)	//limit the timestep so there are 10 iterations going to max/0/min/0 each
	{
		maxfreq = 0.025 / maxfreq;	//equiv to 1 / (40 * maxfreq), so maxfreq now holds 1/40th of the period
		if (timestep > maxfreq || timestep <= 0.0)
			timestep = maxfreq;
	}

	//finally, make sure the timestep does not go beyond the simulation time or the next target
	if (timestep + simtime > curTargettime)
	{
		timestep = curTargettime - simtime;

		iterEndtime = curTargettime;	//and mark the end of the iteration as the end of the simulation
		if (timestep > 0.0)
			timestepi = 1.0 / timestep;
		else
		{
			timestep = 1e-18;
			timestepi = 1e18;
			iterEndtime = simtime + timestep;
		}
	}
	else
	{
		iterEndtime = simtime + timestep;
		timestepi = 1.0 / timestep;
	}
	return(0);
}

double TransientSimData::InitIteration()	//set the time, set the band structure, clear out intermediate variables.
{
	//Calculate Rho, set boundary conditions, solve poisson's equation, put fermi positions in
	//proper place. Also reduces simulation timestep as needed.
	monTStep.ClearTStep();
	ResetValues();

	return(CalcBandStruct(*sim->mdlData, *sim->mdesc));	//returns the total of how much the band structure changed
}

int TransientSimData::Clear(void)
{
	tStepControl.ReInit();
	tStepControl.SetNumRepeat(100);
	tstepConsts.deltaTM1Inv = tstepConsts.deltaTM2Inv = tstepConsts.prev2timestep = tstepConsts.prevtimestep = tstepConsts.TM1PlusTM2 = tstepConsts.TM1PlusTM2Inv = 0.0;
	monTStep.ClearTStep();
	SSByTransient = false;
	SSbyTTime = 1.0;
	isSS = false;
	SmartSaveState = false;
	timeignore = simtime = timestep = nexttimestep = timestepi = 0.0;
	TargetTimes.clear();
	IntermediateSteps = 0;
	curTargettime = iterEndtime = simendtime = storevalueStartTime = 0.0;
	mints = maxts = avgts = 0.0;

	percentCarrierTransfer = ForceAlterPercent = 0.0;
	ResetAlterPercent = false;
	AlterPercentCarrier = false;

	curmaxNetRate = tmpMaxNRate = prevMNRate = testMNRate = 0.0;
	redos = 0;
	timesteps.clear();
//	if(timesteps)
//		delete [] timesteps;
	
//	timesteps = NULL;
	avgTStep = tstepctr = 0.0;

	return(0);
}

void TransContact::Validate(Simulation& sim) {
	ct = sim.getContact(ctcID);
	if (ct == nullptr)
		ctcID = -1;
}

void TransientSimData::Validate() {
	for (unsigned int i = 0; i < ContactData.size(); ++i)
		ContactData[i]->Validate(*sim);

	ModelDescribe *mdesc = sim->mdesc;
	for (unsigned int i = 0; i < SpecTime.size(); ++i) {
		if (mdesc->getSpectra(SpecTime[i].SpecID) == nullptr) {
			SpecTime.erase(SpecTime.begin() + i);
			--i;
		}
	}
}

TransContact* TransientSimData::GetTransContact(Contact *ct) {
	if (ct == nullptr)
		return nullptr;
	for (unsigned int j = 0; j < ContactData.size(); ++j) {
		if (ContactData[j]->GetContact() == ct) {
			return ContactData[j];
		}
	}
	return nullptr;
}


TransContact* TransientSimData::GetTransContact(int ctID) {
	if (ctID < 0)
		return nullptr;
	for (unsigned int j = 0; j < ContactData.size(); ++j) {
		if (ContactData[j]->GetContact()->id == ctID) {
			return ContactData[j];
		}
	}
	return nullptr;
}

int TransientSimData::CreateNewContacts(std::vector<Point*>& edges)	//if it's creating new contacts... then there are no contacts.
{
	unsigned int sz = edges.size();
	LinearTimeDependence tmp;
	tmp.SetValues(0.0, 0.0, 0.0, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_VOLT_LINEAR, false);

	for (unsigned int i = 0; i<sz; i++)
	{
		if (edges[i]->contactData == NULL)
		{
			edges[i]->contactData = new Contact;	//use the point contact pointer to create the contact
			sim->contacts.push_back(edges[i]->contactData);	//now take the main link to contacts and put it in here
			edges[i]->contactData->point.push_back(edges[i]);
		}
		edges[i]->contactData->id = i+1;
		TransContact *tCt = GetTransContact(edges[i]->contactData);
		if (tCt == nullptr) {
			tCt = new TransContact(edges[i]->contactData);
			ContactData.push_back(tCt);
		}
		//ok, now we need to set some stuff up for this particular contact
	
		tCt->AddLinearDependence(tmp, CTC_LIN_VOLT, simtime, simtime);
		
		tCt->UpdateActive(simtime);	//make sure anything created is also up to date!
	
	}
	return(0);
}

int TransientSimData::DeterminePoissonValues()
{
	int voltageCount = 0;
	int EFieldCount = 0;
	int CurrentCount = 0;
	int EFieldCt_ED = 0;
	int CurrentCt_ED = 0;
	int MinEEnergyCount = 0;
	int DOppCount = 0;
	int JNeutralCount = 0;
	int EFPois = 0;
	bool BandDiagramNeedsRecalc = false;

	Model* mdl = sim->mdlData;
	ModelDescribe *mdesc = sim->mdesc;
	
	LinearTimeDependence tmp;	//used for filling in data that doesn't exist and creates a problem not well defined enough

	//first step is figuring out what the current state is.
	//no suppression of any variables.
	unsigned int numContacts = ContactData.size();
	unsigned int curActive = CONTACT_INACTIVE;	//just default 0
	unsigned int targetNumberRestrictions = 2;	//default for a 1d problem
	for (unsigned int i = 0; i <numContacts; i++)
	{
		ContactData[i]->UpdateActive(sim->TransientData->simtime);

		curActive = ContactData[i]->GetCurrentActive();

		if (curActive & CONTACT_MASK_VOLT)
			voltageCount++;
		if (curActive & CONTACT_MASK_EFIELD_NONED)
			EFieldCount++;
		if (curActive & CONTACT_MASK_CURRENTS_NONED)
			CurrentCount++;
		if (curActive & CONTACT_MASK_EFIELD_ED)
			EFieldCt_ED++;
		if (curActive & CONTACT_MASK_CURRENTS_ED)
			CurrentCt_ED++;
		if (curActive & CONTACT_FLOAT_EFIELD_ESCAPE)	//this is allowing an electric field
			targetNumberRestrictions++;
		if (curActive & CONTACT_MINIMIZE_E_ENERGY)
			MinEEnergyCount++;
		if (curActive & CONTACT_OPP_DFIELD)
			DOppCount++;
		if (curActive & CONTACT_CONSERVE_CURRENT)
			JNeutralCount++;
		if (curActive & CONTACT_MASK_POISSON_EFIELD)	//general count as all these auto conflict with one another
			EFPois++;

		ContactData[i]->SetSuppressions(false, false, false, false, false, false);	//clear out everything
	}

	//there needs to be at least one voltage, and then one other fixed (non_ED)
	//if there are multiple voltages... you're screwed. Need to choose the "best" two and suppress all the others.
	//go with the newest voltages for contacts seems somewhat reasonable.

	//assuming 1D for now - more complicated in multiple dimensions - no simple test

	if (voltageCount == 0)
	{

		tmp.SetValues(0.0, 0.0, simtime, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_VOLT_LINEAR, false);
		//else if (sim->SteadyStateData)
		//we need to get a voltage at zero
		if (sim->contacts.size() == 0)	//well shit. This means that there are no contacts at all, and targetNumberRestrictions is 2.
		{
			//I guess we should make some contacts
			//first find the left-most and right-most points, which are presumably first and last.
			std::vector<Point*> edges;
			DetermineEdgePoints(*mdl, edges);
			//now if edges is one, screw this simulation. There's only one point so this simulation is positively useless.
			//every single edge should be a contact of some sort... but we are really only concerned about 1D.
			//so we're gonna assume this is 1D for this code!
			CreateNewContacts(edges);
			voltageCount += edges.size();	//update the voltages for future references
		}
		else if (ContactData.size() == 1) //we'll just make the contact have a reference voltage of zero
		{

			ContactData[0]->AddLinearDependence(tmp, CTC_LIN_VOLT, simtime, simtime);	//now it has a reference of zero volts within

			voltageCount++;
			if (curActive & CONTACT_FLOAT_EFIELD_ESCAPE)	//this contact has the flag set that would require knowing more knowledge than we have.
				curActive -= CONTACT_FLOAT_EFIELD_ESCAPE;	//get rid of this flag

		}
		else if (ContactData.size() == targetNumberRestrictions)
		{
			//try and determine which might be an exterior point. There's likely only two!
			//test the first contact
			bool found = false;
			short which = -1;
			for (unsigned int i = 0; i<ContactData[0]->GetContact()->point.size(); i++)
			{
				if (ContactData[0]->GetContact()->point[i]->adj.adjMX == NULL || ContactData[0]->GetContact()->point[i]->adj.adjPX)
				{
					found = true;
					which = 0;
					break;
				}
			}
			if (!found)
			{
				for (unsigned int i = 0; i<ContactData[1]->GetContact()->point.size(); i++)
				{
					if (ContactData[1]->GetContact()->point[i]->adj.adjMX == NULL || ContactData[1]->GetContact()->point[i]->adj.adjPX)
					{
						found = true;
						which = 1;
						break;
					}
				}
			}
			if (found)	//then use whatever which is and set the voltage to zero...
			{
				ContactData[which]->AddLinearDependence(tmp, CTC_LIN_VOLT, sim->TransientData->simtime, sim->TransientData->simtime);
				voltageCount++;
			}
			else
			{
				ContactData[0]->AddLinearDependence(tmp, CTC_LIN_VOLT, sim->TransientData->simtime, sim->TransientData->simtime);
				voltageCount++;	//otherwise default to the first one
			}
			BandDiagramNeedsRecalc = true;	//there were no voltages

		}
		else
		{
			//there are more than two contacts, just choose the first contact on the edge, otherwise choose the zeroth contact...
			int which = -1;
			unsigned int ctsize = sim->contacts.size();
			for (unsigned int i = 0; i<ctsize; i++)
			{
				for (unsigned int pt = 0; pt<ContactData[i]->GetContact()->point.size(); pt++)
				{
					if (ContactData[i]->GetContact()->point[pt]->adj.adjMX == NULL || ContactData[i]->GetContact()->point[pt]->adj.adjPX == NULL)
					{
						which = i;
						break;
					}
				}
				if (which != -1)	//get out of the loop
					break;
			}
			if (which < 0)
				which = 0;

			ContactData[which]->AddLinearDependence(tmp, CTC_LIN_VOLT, sim->TransientData->simtime, sim->TransientData->simtime);
			voltageCount++;
		}
	}	//end making sure there is at least one reference voltage in the simulation


	unsigned int totalCount = voltageCount + EFieldCount + CurrentCount + MinEEnergyCount + DOppCount + JNeutralCount;	//parts that could affect poisson's equation

	//do auto suppressions
	for (unsigned int i = 0; i < numContacts; i++)
	{
		int ctcActive = ContactData[i]->GetCurrentActive();
		if (ctcActive & CONTACT_MASK_EFIELD_NONED)	//suppress efield/current if it's efield (PRIORITY)
		{
			if (ctcActive & CONTACT_MINIMIZE_E_ENERGY)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EMIN);
				totalCount--;
				MinEEnergyCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_MASK_CURRENTS_NONED)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_CURRENT);
				totalCount--;
				CurrentCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_OPP_DFIELD)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_OPPD);
				totalCount--;
				DOppCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_CONSERVE_CURRENT)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_JNEUTRAL);
				totalCount--;
				JNeutralCount--;
				//Countmain does not change because CtEField still is there.
			}
		}
		else if (ctcActive & CONTACT_CONSERVE_CURRENT)
		{
			if (ctcActive & CONTACT_MINIMIZE_E_ENERGY)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EMIN);
				totalCount--;
				MinEEnergyCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_MASK_CURRENTS_NONED)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_CURRENT);
				totalCount--;
				CurrentCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_OPP_DFIELD)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_OPPD);
				totalCount--;
				DOppCount--;
				//Countmain does not change because CtEField still is there.
			}
		}
		else if (ctcActive & CONTACT_OPP_DFIELD)
		{
			if (ctcActive & CONTACT_MINIMIZE_E_ENERGY)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EMIN);
				totalCount--;
				MinEEnergyCount--;
				//Countmain does not change because CtEField still is there.
			}

			if (ctcActive & CONTACT_MASK_CURRENTS_NONED)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_CURRENT);
				totalCount--;
				CurrentCount--;
				//Countmain does not change because CtEField still is there.
			}
		}
		else if (ctcActive & CONTACT_MINIMIZE_E_ENERGY)
		{
			if (ctcActive & CONTACT_MASK_CURRENTS_NONED)
			{
				ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_CURRENT);
				totalCount--;
				CurrentCount--;
				//Countmain does not change because minimizeCt still is there.
			}
		}

	}


	if (totalCount < targetNumberRestrictions)	//basically, there are too many unknowns for the number of equations, so default some unknowns to zero, with voltages set preferentially
	{
		//we need to add another bit, and there is guaranteed to at least have one voltage at this point
		//if there were no contacts, it created two contacts (one on each end) with zero bias
		//so if totalCount < 2, that means they implemented just one contact and just assigned a voltage
		int numberAdditions = targetNumberRestrictions - totalCount;

		if (MinEEnergyCount == 0 && JNeutralCount == 0 && DOppCount == 0)	//default to add a OppDField
		{
			for (unsigned int i = 0; i < numContacts; i++)
			{
				int ctcActive = ContactData[i]->GetCurrentActive();
				if ((ctcActive & CONTACT_MASK_POISSON_EFIELD) == CONTACT_INACTIVE)	//none of the EField flags are present
				{
					ContactData[i]->AddAdditionalFlags(CONTACT_OPP_DFIELD, sim->TransientData->simtime);	//choose OPP_DFIELD as default transient value
					DOppCount++;	//update all the counts
					totalCount++;
					EFPois++;
					numberAdditions--;
				}	//end test that it doesn't already contain a voltage
			}
		}

		int AddVoltages = sim->contacts.size() - voltageCount;	//this is the number of voltages that may be added in the current framework
		if (AddVoltages > 0)
		{
			tmp.SetValues(0.0, 0.0, sim->TransientData->simtime, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_VOLT_LINEAR, false);	//these are new values where there were previously none...
			for (unsigned int i = 0; i<numContacts; i++)
			{
				int ctcActive = ContactData[i]->GetCurrentActive();

				if ((ctcActive & CONTACT_MASK_VOLT) == CONTACT_INACTIVE)	//neither of those flags aren't present
				{
					ContactData[i]->AddLinearDependence(tmp, CTC_LIN_VOLT, sim->TransientData->simtime, sim->TransientData->simtime);
					voltageCount++;	//update all the counts
					totalCount++;
					AddVoltages--;
					numberAdditions--;
				}	//end test that it doesn't already contain a voltage
				if (numberAdditions <= 0)
					break;
			}	//end loop through contacts for adding voltages
		}	//end there are voltages to add

		//now if the voltages are all set at zero, and this still is not good enough, go through the contacts and set electric fields to zero.
		int AddEFields = sim->contacts.size() - EFieldCount - CurrentCount;	//current & efield counts will have the same effect on the band structure
		if (AddEFields > 0 && numberAdditions > 0)
		{
			if (sim->simType == SIMTYPE_TRANSIENT && sim->TransientData)
				tmp.SetValues(0.0, 0.0, sim->TransientData->simtime, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_EFIELD_LINEAR, false);
			for (unsigned int i = 0; i<numContacts; i++)
			{
				int ctcActive = ContactData[i]->GetCurrentActive();
				if ((ctcActive & (CONTACT_EFIELD_COS | CONTACT_EFIELD_LINEAR | CONTACT_CURRENT_COS | CONTACT_CURRENT_LINEAR | CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_LINEAR)) == CONTACT_INACTIVE)	//none of these flags aren't present
				{
					ContactData[i]->AddLinearDependence(tmp, CTC_LIN_EFIELD, sim->TransientData->simtime, sim->TransientData->simtime);
					EFieldCount++;	//update all the counts
					totalCount++;
					AddEFields--;
					numberAdditions--;
				}	//end test that it doesn't already contain a voltage
				if (numberAdditions <= 0)
					break;
			}	//end loop through contacts for adding voltages
		}

		BandDiagramNeedsRecalc = true;

	}
	else if (totalCount > targetNumberRestrictions)	//now we need to be selective on the contacts because it is making an overly defined system
	{
		bool suppress;	//flag to do suppression for old stuff
		double tmpTime;
		std::multimap<double, int> WhichContacts;	//contact ID=data, previous alteration=time
		int numCtc = sim->contacts.size();
		int flags;
		int numSuppressions = totalCount - targetNumberRestrictions;

		//we want to first make sure voltages win in determining poisson's equation
		//after this, then electric field, then ConserveCurrent, then DField, then minimize E Energy, and finally current
		//when there are multiples, go with the contacts that have the most recent time updates.

		//so start off suppressing currents if there are any
		if (CurrentCount > 0 && numSuppressions > 0)
		{
			if (CurrentCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_CURRENTS_NONED) && ContactData[i]->SuppressCurrent() == false)
					{
						ContactData[i]->SetSuppressions(false, true, false, false, false, false, SUPPRESS_SET_CURRENT);
						numSuppressions--;
						CurrentCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_CURRENTS_NONED) && ContactData[i]->SuppressCurrent() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(false, true, false, false, false, false, SUPPRESS_SET_CURRENT);
						numSuppressions--;
						CurrentCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}


		if (MinEEnergyCount > 0 && numSuppressions > 0)
		{
			if (MinEEnergyCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MINIMIZE_E_ENERGY) && ContactData[i]->SuppressMinEField() == false)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EMIN);
						numSuppressions--;
						MinEEnergyCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MINIMIZE_E_ENERGY) && ContactData[i]->SuppressMinEField() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EMIN);
						numSuppressions--;
						MinEEnergyCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}


		if (JNeutralCount > 0 && numSuppressions > 0)
		{
			if (JNeutralCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_CONSERVE_CURRENT) && ContactData[i]->SuppressChargeNeutralCurrent() == false)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_JNEUTRAL);
						numSuppressions--;
						JNeutralCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_CONSERVE_CURRENT) && ContactData[i]->SuppressChargeNeutralCurrent() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_JNEUTRAL);
						numSuppressions--;
						JNeutralCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}

		if (DOppCount > 0 && numSuppressions > 0)
		{
			if (DOppCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_OPP_DFIELD) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_OPPD);
						numSuppressions--;
						DOppCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_OPP_DFIELD) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_OPPD);
						numSuppressions--;
						DOppCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}

		if (EFieldCount > 0 && numSuppressions > 0)
		{
			if (EFieldCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_EFIELD_NONED) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EFIELD);
						numSuppressions--;
						EFieldCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_EFIELD_NONED) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_EFIELD);
						numSuppressions--;
						EFieldCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}

		if (voltageCount > 0 && numSuppressions > 0)
		{
			if (voltageCount < numSuppressions)	//suppress all of them
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_VOLT) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_VOLT);
						numSuppressions--;
						voltageCount--;
						totalCount--;
					}
				}
			}
			else //more current contacts than suppressions, so suppress numsuppression times, starting with oldest currents
			{
				for (int i = 0; i < numCtc; i++)
				{
					flags = ContactData[i]->GetCurrentActive();
					if ((flags & CONTACT_MASK_VOLT) && ContactData[i]->SuppressDFieldOpp() == false)
					{
						tmpTime = ContactData[i]->GetPreviousAlteration(sim->TransientData->simtime);
						WhichContacts.insert(std::pair<double, int>(tmpTime, i));	//map, so puts in chronological order
					}
				}
				for (int i = 0; i < numSuppressions; i++)
					WhichContacts.erase(WhichContacts.begin());	//get rid of the ones that were set the longest time ago

				for (int i = 0; i < numCtc; i++)
				{
					suppress = true;
					for (std::multimap<double, int>::iterator which = WhichContacts.begin(); which != WhichContacts.end(); ++which)
					{
						if (which->second == i)  //it is found so it is a valid contact
						{
							suppress = false;
							break;	//get out of for loop, though it probably doesn't matter too much
						}
					}
					if (suppress)
					{
						ContactData[i]->SetSuppressions(true, true, true, true, true, true, SUPPRESS_SET_VOLT);
						numSuppressions--;
						voltageCount--;
						totalCount--;
					}
				}
				WhichContacts.clear();	//clear out for next time
			}
		}

	}

	//now it is time to compare the contacts with the previous iteration to see if anything has changed.
	numContacts = ContactData.size();
	int prevFlags, curFlags, id, mask;
	std::map<int, int>::iterator prevItems;
	for (unsigned int i = 0; i<numContacts; i++)
	{
		id = ContactData[i]->GetContactID();
		prevItems = sim->ContactPoisson.find(id);
		if (prevItems == sim->ContactPoisson.end())
			prevFlags = CONTACT_INACTIVE;
		else
			prevFlags = prevItems->second;

		curFlags = ContactData[i]->GetCurrentActive();
		//now kill the stuff that gets suppressed...

		mask = CONTACT_INACTIVE; // 0
		if (ContactData[i]->SuppressCurrent())
			mask = mask | CONTACT_MASK_CURRENTS_NONED;

		if (ContactData[i]->SuppressEField())
			mask = mask | CONTACT_MASK_EFIELD_NONED;

		if (ContactData[i]->SuppressVoltage())
			mask = mask | CONTACT_MASK_VOLT;

		if (ContactData[i]->SuppressMinEField())
			mask = mask | CONTACT_MINIMIZE_E_ENERGY;

		if (ContactData[i]->SuppressChargeNeutralCurrent())
			mask = mask | CONTACT_CONSERVE_CURRENT;

		if (ContactData[i]->SuppressDFieldOpp())
			mask = mask | CONTACT_OPP_DFIELD;



		//now everything is loaded in the mask as 1's that we want to force to be zeros.
		mask = ~mask;	//make that everything we want to be zeros is zero, and everything else is one
		curFlags = curFlags & mask;	//and now all those options have been cleared away

		//now a tricky part.
		//group together all the various flags that would not change the poisson equation symbolically
		if (curFlags & (CONTACT_MASK_VOLT))
			curFlags = curFlags | CONTACT_MASK_VOLT;

		//The EField and the current are treated the same way in the poisson equation.  One of them just involves
		//being transofrmed into the other as needed.
		if (curFlags & (CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_CURRENTS_NONED))
			curFlags = curFlags | CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_CURRENTS_NONED;

		if (curFlags & CONTACT_MASK_EFIELD_ED)
			curFlags = curFlags | CONTACT_MASK_EFIELD_ED;

		if (curFlags & CONTACT_MASK_COMPLEX_EFIELD)
			curFlags = curFlags | CONTACT_MASK_COMPLEX_EFIELD;

		//at this point, any ambiguities from switching between them should be done

		//now need to check for the existence of groups to see if poisson equation would change
		if (BandDiagramNeedsRecalc == false)	//don't bother doing all the checks if you know it needs to be done!
		{
			if (curFlags != prevFlags)
				BandDiagramNeedsRecalc = true;
		}

		sim->ContactPoisson[id] = curFlags;	//insert or overwrite. MAP

		ContactData[i]->TransferSuppressionsActiveToContact();
	}

	


	if (BandDiagramNeedsRecalc)
		return(CONTACT_BD_CHANGE);

	return(CONTACT_NO_BD_CHANGE);
}
//need code to check poisson's equation needs to be resolved.

bool TransContact::TransferSuppressionsActiveToContact() {
	if (ct == nullptr)
		return false;

	ct->SetSuppressions(suppressVoltagePoisson, suppressCurrentPoisson, suppressEFieldPoisson, suppressEFieldMinimize, suppressOppDField, suppressChargeNeutral);
	ct->SetCurrentActiveValue(currentActiveValue);
	return true;

}

TransientSimData::TransientSimData(Simulation* s) : sim(s)
{
	timesteps.clear();
	simendtime = 1.0e-9;	//just simulate a ns by default
	Clear();
}

TransientSimData::~TransientSimData()
{
	Clear();
}

///////////////////////////////
TransContact::TransContact(Contact *ctc, int id) {
	VoltageCos.clear();
	VoltageLin.clear();
	EFieldLin.clear();
	EFieldCos.clear();
	AdditionalFlags.clear();
	ActiveAdditional.clear();
	ActiveCos.clear();
	ActiveLinear.clear();
	ActiveValues.clear();
	nexttimechange = 0.0;
	lastTimeUpdate = -1.0;

	suppressVoltagePoisson = false;
	suppressCurrentPoisson = false;
	suppressEFieldPoisson = false;
	suppressChargeNeutral = suppressEFieldMinimize = suppressOppDField = false;

	ct = ctc;
	if (ct)
		ctcID = ct->id;
	else
		ctcID = id;
}

TransContact::~TransContact() {
	VoltageCos.clear();
	VoltageLin.clear();
	EFieldLin.clear();
	EFieldCos.clear();
	AdditionalFlags.clear();
	ActiveAdditional.clear();
	ActiveCos.clear();
	ActiveLinear.clear();
	ActiveValues.clear();
	ct = nullptr;	//NO DELETE. simulation owns the data. This is just a pointer

}

int TransContact::SetSuppressions(bool voltage, bool current, bool eField, bool eMin, bool eOpp, bool JNeutral, int which)
{
	if (which & SUPPRESS_SET_VOLT)
		suppressVoltagePoisson = voltage;
	if (which & SUPPRESS_SET_CURRENT)
		suppressCurrentPoisson = current;
	if (which & SUPPRESS_SET_EFIELD)
		suppressEFieldPoisson = eField;
	if (which & SUPPRESS_SET_EMIN)
		suppressEFieldMinimize = eMin;
	if (which & SUPPRESS_SET_JNEUTRAL)
		suppressChargeNeutral = JNeutral;
	if (which & SUPPRESS_SET_OPPD)
		suppressOppDField = eOpp;
	return(0);
}


int TransContact::RemoveAdditionalFlags(int flags, double time)
{
	std::map<double, TimeDependence>::iterator prevFlag = AdditionalFlags.find(time);
	if (prevFlag != AdditionalFlags.end())	//this is the same time... then just add the flags together regardless
	{
		prevFlag->second.RemoveFlags(flags);
		return(1);
	}
	return(0);
}

int TransContact::AddAdditionalFlags(int flags, double time)
{
	std::map<double, TimeDependence>::iterator prevFlag = AdditionalFlags.find(time);
	if (prevFlag != AdditionalFlags.end())	//this is the same time... then just add the flags together regardless
	{
		prevFlag->second.AddFlags(flags);	//flag already exists, so this is the resetting of it (multiple steps)
		prevFlag->second.RemoveFlags(CONTACT_RESET);	//make sure this goes away!
		//		int newFlags = prevFlag->second | flags;
		//		if (newFlags & CONTACT_RESET)
		//			newFlags -= CONTACT_RESET;	//if the flag already exists, there's no need to "reset" it
		//		prevFlag->second = newFlags;
		return(0);
	}
	prevFlag = MapFindFirstLess(AdditionalFlags, time);
	if (prevFlag != AdditionalFlags.end())
	{
		TimeDependence t(time, -1.0, flags, false, 0.0);
		if (flags & CONTACT_RESET)
		{
			t.RemoveFlags(CONTACT_RESET);
			AdditionalFlags[time] = t;
			return(0);
		}
		t.AddFlags(prevFlag->second.GetFlags());
		AdditionalFlags[time] = t;
		return(0);
	}
	else
	{
		TimeDependence t(time, -1.0, flags, false, 0.0);
		t.RemoveFlags(CONTACT_RESET);

		AdditionalFlags[time] = t;
		return(0);
	}

	return(0);
}


int TransContact::UpdateActive(double curtime, bool forceUpdate, int SSActive)
{
	if (ct == nullptr)
		return(CONTACT_NO_BD_CHANGE);

	if (!forceUpdate && lastTimeUpdate <= curtime && (curtime < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY))
	{ //the lastTimeUpdate is just to make sure that it isn't going backwards in time, which would make this irrelevant
		lastTimeUpdate = curtime;
		return(CONTACT_NO_BD_CHANGE);	//there's nothing to do.
	}

	ActiveCos.clear();
	ActiveLinear.clear();
	//this goes through and updates what functional values actually matter for determining currents, voltages, etc.
	double LastNoSumVolt = 0.0;
	double LastNoSumEField = 0.0;
	double LastNoSumCurrent = 0.0;
	double LastNoSumCurDensity = 0.0;
	double LastNoSumCurrentOut = 0.0;
	freqVoltage = freqEField = freqCurrent = 0.0;	//reset these voltages
	std::map<double, TimeDependence>::iterator Addflags = MapFindFirstLessEqual(AdditionalFlags, curtime);
	currentActiveValue = (Addflags != AdditionalFlags.end()) ? Addflags->second.GetFlags() : CONTACT_INACTIVE;

	//	nexttimechange = CONTACT_HOLD_INDEFINITELY;	//this will be updated in the UpdateActive_Loop(Cos/Lin)Time function
	double tmp;	//gotta choose the larger of the two
	LastNoSumVolt = UpdateActive_LoopCosTime(VoltageCos, freqVoltage, curtime);
	tmp = UpdateActive_LoopLinTime(VoltageLin, curtime);
	if (tmp > LastNoSumVolt)
		LastNoSumVolt = tmp;

	LastNoSumEField = UpdateActive_LoopCosTime(EFieldCos, freqEField, curtime);
	tmp = UpdateActive_LoopLinTime(EFieldLin, curtime);
	if (tmp > LastNoSumEField)
		LastNoSumEField = tmp;

	LastNoSumCurrent = UpdateActive_LoopCosTime(CurrentCos, freqCurrent, curtime);
	tmp = UpdateActive_LoopLinTime(CurrentLin, curtime);
	if (tmp > LastNoSumCurrent)
		LastNoSumCurrent = tmp;

	LastNoSumCurDensity = UpdateActive_LoopCosTime(CurrentDensityCos, freqCurrent, curtime);
	tmp = UpdateActive_LoopLinTime(CurrentDensityLin, curtime);
	if (tmp > LastNoSumCurDensity)
		LastNoSumCurDensity = tmp;

	//	LastNoSumCurrentOut = UpdateActive_LoopAdditionalTime(AdditionalFlags, curtime);


	// activeCos and ActiveLinear now contain everything that has an active time.
	// Now cut out everything starting before the start time. This will result in only the contact entries that matter.
	// if no sum, it clears everything out before it. Anything summable added afterwards will add on to this value

	// time to loop through the two lists
	int flags;
	for (std::list<std::multimap<double, CosTimeDependence>::iterator>::iterator it = ActiveCos.begin(); it != ActiveCos.end();) //nothing in the post process
	{
		//it is cleared out and added to with the index being the previous size, so it should maintain order with the vector
		flags = (*it)->second.GetFlags();
		if (flags & CONTACT_VOLT_COS)
		{
			if ((*it)->second.GetStartTime() < LastNoSumVolt && (*it)->second.IsSum())
				it = ActiveCos.erase(it);	//this causes it to point at the element in the list after the one that was deleted
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays the same (not erased), so increment the for loop here.
			}
		}
		else if (flags & (CONTACT_EFIELD_COS | CONTACT_EFIELD_COS_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumEField && (*it)->second.IsSum())
				it = ActiveCos.erase(it);
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}
		else if (flags & (CONTACT_CURRENT_COS | CONTACT_CURRENT_COS_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumCurrent && (*it)->second.IsSum())
				it = ActiveCos.erase(it);
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}

		}
		else if (flags & (CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_COS_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumCurDensity && (*it)->second.IsSum())
				it = ActiveCos.erase(it);
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}
	}

	for (std::list<std::multimap<double, LinearTimeDependence>::iterator>::iterator it = ActiveLinear.begin(); it != ActiveLinear.end();) //nothing in the post process
	{
		//it is cleared out and added to with the index being the previous size, so it should maintain order with the vector
		flags = (*it)->second.GetFlags();
		if (flags & CONTACT_VOLT_LINEAR)
		{
			if ((*it)->second.GetStartTime() < LastNoSumVolt && (*it)->second.IsSum())
				it = ActiveLinear.erase(it);	//this causes it to point at the element in the list after the one that was deleted
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}
		else if (flags & (CONTACT_EFIELD_LINEAR | CONTACT_EFIELD_LINEAR_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumEField && (*it)->second.IsSum())
				it = ActiveLinear.erase(it);	//this causes it to point at the element in the list after the one that was deleted
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}
		else if (flags & (CONTACT_CURRENT_LINEAR | CONTACT_CURRENT_LINEAR_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumCurrent && (*it)->second.IsSum())
				it = ActiveLinear.erase(it);	//this causes it to point at the element in the list after the one that was deleted
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}
		else if (flags & (CONTACT_CUR_DENSITY_LINEAR | CONTACT_CUR_DENSITY_LINEAR_ED))
		{
			if ((*it)->second.GetStartTime() < LastNoSumCurDensity && (*it)->second.IsSum())
				it = ActiveLinear.erase(it);	//this causes it to point at the element in the list after the one that was deleted
			else
			{
				currentActiveValue = currentActiveValue | flags;
				++it;	//it stays, so increment here.
			}
		}

	}

	/*
	for (std::list<std::map<double, ContactCurOut>::iterator>::iterator it = ActiveCurOut.begin(); it != ActiveCurOut.end();) //nothing in the post process
	{
	flags = (*it)->second.flags;
	if (flags & CONTACT_CONNECT_EXTERNAL)
	{
	if ((*it)->second.starttime < LastNoSumCurrentOut)
	it = ActiveCurOut.erase(it);
	else
	{
	currentActiveValue = currentActiveValue | flags;
	++it;
	}
	}
	}
	*/

	std::map<double, int>::iterator ptrActiveVals = MapFindFirstGreater(ActiveValues, curtime);
	if (ptrActiveVals != ActiveValues.end())
		nexttimechange = ptrActiveVals->first;
	else
		nexttimechange = CONTACT_HOLD_INDEFINITELY;

	lastTimeUpdate = curtime;

	//now update the actual contact data!
	ct->SetFreqCurrent(freqCurrent);
	ct->SetFreqVoltage(freqVoltage);
	ct->SetFreqEField(freqEField);
	ct->SetCurrentActiveValue(currentActiveValue);

	return(CONTACT_BD_CHANGE);	//potentially change! not necessarily true.
}


int TransContact::BuildActiveValues(void)	//this should be called once and once only (after contact has all relevant data)
{
	ActiveValues.clear();	//clear out the data
	//essentially need to add up a bunch of flags for each type there is.
	std::map<double, int>::iterator ActCheck;
	int flag;
	double stime, etime;


	for (std::multimap<double, CosTimeDependence>::iterator it = VoltageCos.begin(); it != VoltageCos.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, CosTimeDependence>::iterator it = EFieldCos.begin(); it != EFieldCos.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, CosTimeDependence>::iterator it = CurrentDensityCos.begin(); it != CurrentDensityCos.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, CosTimeDependence>::iterator it = CurrentCos.begin(); it != CurrentCos.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	//////////////////////linear
	for (std::multimap<double, LinearTimeDependence>::iterator it = VoltageLin.begin(); it != VoltageLin.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, LinearTimeDependence>::iterator it = EFieldLin.begin(); it != EFieldLin.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, LinearTimeDependence>::iterator it = CurrentDensityLin.begin(); it != CurrentDensityLin.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, LinearTimeDependence>::iterator it = CurrentLin.begin(); it != CurrentLin.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(stime);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}
	}

	for (std::multimap<double, TimeDependence>::iterator it = AdditionalFlags.begin(); it != AdditionalFlags.end(); ++it)
	{
		stime = it->second.GetStartTime();
		ActCheck = ActiveValues.find(it->first);
		if (ActCheck == ActiveValues.end())	//otherwise, it will have already gotten all the relevant data for this contact
		{
			UpdateActive(stime, true);
			flag = GetCurrentActive();
			ActiveValues[stime] = flag;
		}

		etime = it->second.GetEndTime();
		if (etime != CONTACT_HOLD_INDEFINITELY)
		{
			ActCheck = ActiveValues.find(etime);
			if (ActCheck == ActiveValues.end())
			{
				UpdateActive(etime, true);
				flag = GetCurrentActive();
				ActiveValues[etime] = flag;
			}
		}

	}


	return(0);
}

double TransContact::UpdateActive_LoopCosTime(std::multimap<double, CosTimeDependence>& data, double& maxFreq, double curtime)
{
	double NoSum = 0.0;
	double tmp = 0.0;

	for (std::multimap<double, CosTimeDependence>::iterator it = data.begin(); it != data.end(); ++it)
	{
		double stime = it->second.GetStartTime();
		double etime = it->second.GetEndTime();

		if (curtime >= stime && (curtime < etime || etime == CONTACT_HOLD_INDEFINITELY))	//is this an active voltage?
		{
			ActiveCos.push_back(it);
			if (it->second.IsSum() == false)
			{
				tmp = stime;
				if (tmp > NoSum)
					NoSum = tmp;
			}
			double freq = it->second.GetFreq();
			if (freq > maxFreq)
				maxFreq = freq;
		}
	}

	return(NoSum);
}

double TransContact::UpdateActive_LoopAdditionalTime(std::map<double, TimeDependence>& data, double curtime)
{
	double NoSum = 0.0;
	double tmp = 0.0;

	for (std::map<double, TimeDependence>::iterator it = data.begin(); it != data.end(); ++it)
	{

		double stime = it->second.GetStartTime();
		double etime = it->second.GetEndTime();
		if (curtime >= stime && (curtime < etime || etime == CONTACT_HOLD_INDEFINITELY))	//is this an active voltage?
		{
			ActiveAdditional.push_back(it);
			tmp = stime;
			if (tmp > NoSum)
				NoSum = tmp;
		}
	}

	return(NoSum);	//this should return the largest start time...
}

double TransContact::UpdateActive_LoopLinTime(std::multimap<double, LinearTimeDependence>& data, double curtime)
{
	double NoSum = 0.0;
	double tmp = 0.0;

	for (std::multimap<double, LinearTimeDependence>::iterator it = data.begin(); it != data.end(); ++it)
	{
		double stime = it->second.GetStartTime();
		double etime = it->second.GetEndTime();
		if (curtime >= stime && (curtime < etime || etime == CONTACT_HOLD_INDEFINITELY))	//is this an active voltage?
		{
			ActiveLinear.push_back(it);
			if (it->second.IsSum() == false)
			{
				tmp = stime;
				if (tmp > NoSum)
					NoSum = tmp;
			}
		}
	}

	return(NoSum);
}


int TransContact::UpdateInternalValsPreBD(double curtime)
{
	if (ct == nullptr)
		return(0);
	//update the electric field entering the contact externally
	//update the current entering the contact from an external source
	//update the current density entering the contact
	//update the voltage

	//first, we need to know what values ought to be set!
	UpdateActive(curtime);	//note, this won't cause a lot of stuff to be done if nothing has changed
	//now let's update them!

	//zero everything out!
	double voltage, current, eField, currentDensity, current_ED, eField_ED, currentDensity_ED, eField_ED_Leak, current_ED_Leak, currentD_ED_Leak;
	voltage = current = eField = currentDensity = current_ED = eField_ED = currentDensity_ED = eField_ED_Leak = current_ED_Leak = currentD_ED_Leak = 0.0;


	for (std::list<std::multimap<double, CosTimeDependence>::iterator>::iterator itC = ActiveCos.begin(); itC != ActiveCos.end(); ++itC)
	{
		double val = (*itC)->second.GetValue(curtime);
		int flags = (*itC)->second.GetFlags();
		if (flags & CONTACT_VOLT_COS)
			voltage += val;
		if (flags & CONTACT_EFIELD_COS)
			eField += val;
		if (flags & CONTACT_CURRENT_COS)
			current += val;
		if (flags & CONTACT_CUR_DENSITY_COS)
			currentDensity += val;
		if (flags & CONTACT_EFIELD_COS_ED)
		{
			eField_ED += val;
			if (flags & CONTACT_LEAK)
				eField_ED_Leak += val * (*itC)->second.GetLeak();
		}
		if (flags & CONTACT_CURRENT_COS_ED)
		{
			current_ED += val;
			if (flags & CONTACT_LEAK)
				current_ED_Leak += val * (*itC)->second.GetLeak();
		}
		if (flags & CONTACT_CUR_DENSITY_COS_ED)
		{
			currentDensity_ED += val;
			if (flags & CONTACT_LEAK)
				currentD_ED_Leak += val * (*itC)->second.GetLeak();
		}
	}

	for (std::list<std::multimap<double, LinearTimeDependence>::iterator>::iterator itL = ActiveLinear.begin(); itL != ActiveLinear.end(); ++itL)
	{
		double val = (*itL)->second.GetValue(curtime);
		int flags = (*itL)->second.GetFlags();
		if (flags & CONTACT_VOLT_LINEAR)
			voltage += val;
		if (flags & CONTACT_EFIELD_LINEAR)
			eField += val;
		if (flags & CONTACT_CURRENT_LINEAR)
			current += val;
		if (flags & CONTACT_CUR_DENSITY_LINEAR)
			currentDensity += val;
		if (flags & CONTACT_EFIELD_LINEAR_ED)
		{
			eField_ED += val;
			if (flags & CONTACT_LEAK)
				eField_ED_Leak += val * (*itL)->second.GetLeak();
		}
		if (flags & CONTACT_CURRENT_LINEAR_ED)
		{
			current_ED += val;
			if (flags & CONTACT_LEAK)
				current_ED_Leak += val * (*itL)->second.GetLeak();
		}
		if (flags & CONTACT_CUR_DENSITY_LINEAR_ED)
		{
			currentDensity_ED += val;
			if (flags & CONTACT_LEAK)
				currentD_ED_Leak += val * (*itL)->second.GetLeak();
		}
	}

	ct->SetVoltage(voltage);
	ct->SetCurrent(current);
	ct->SetEField(eField);
	ct->SetCurrentDensity(currentDensity);
	ct->SetCurrent_ED(current_ED);
	ct->SetEField_ED(eField_ED);
	ct->SetCurrentDensity_ED(currentDensity_ED);
	ct->SetCurrent_ED_leak(current_ED_Leak);
	ct->SetEField_ED_leak(eField_ED_Leak);
	ct->SetCurrentDensity_ED_leak(currentD_ED_Leak);

	ct->DeterminePsiContactPts();	//this sets the value of psi for any prepwork boundary conditions if they are needed

	return(0);
}

int TransContact::UpdateInternalValsPostBD(double curtime)
{
	//what is the voltage/current/eField/etc. of all this stuff now that the band diagram has been calculated.
	//I don't think this actually matters or is useful?
	return(0);
}


int TransContact::GetTimes(ctcIDS which, std::vector<double>& times)
{
	times.clear();
	switch (which)
	{
	case CTC_COS_VOLT:
		times.reserve(VoltageCos.size() * 2);	//prevent future allocations
		for (std::multimap<double, CosTimeDependence>::iterator it = VoltageCos.begin(); it != VoltageCos.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_COS_EFIELD:
	case CTC_COS_EFIELD:
		times.reserve(EFieldCos.size() * 2);	//prevent future allocations
		for (std::multimap<double, CosTimeDependence>::iterator it = EFieldCos.begin(); it != EFieldCos.end(); ++it) {
			times.push_back(it->first);
			if (it->second.GetEndTime() > 0.0)
				times.push_back(it->second.GetEndTime()*0.99999999999999);
			else
				times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_COS_CUR:
	case CTC_COS_CUR:
		times.reserve(CurrentCos.size() * 2);	//prevent future allocations
		for (std::multimap<double, CosTimeDependence>::iterator it = CurrentCos.begin(); it != CurrentCos.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_COS_CURD:
	case CTC_COS_CURD:
		times.reserve(CurrentDensityCos.size() * 2);	//prevent future allocations
		for (std::multimap<double, CosTimeDependence>::iterator it = CurrentDensityCos.begin(); it != CurrentDensityCos.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_LIN_VOLT:
		times.reserve(VoltageLin.size() * 2);	//prevent future allocations
		for (std::multimap<double, LinearTimeDependence>::iterator it = VoltageLin.begin(); it != VoltageLin.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_LIN_EFIELD:
	case CTC_LIN_EFIELD:
		times.reserve(EFieldLin.size() * 2);	//prevent future allocations
		for (std::multimap<double, LinearTimeDependence>::iterator it = EFieldLin.begin(); it != EFieldLin.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_LIN_CUR:
	case CTC_LIN_CUR:
		times.reserve(CurrentLin.size() * 2);	//prevent future allocations
		for (std::multimap<double, LinearTimeDependence>::iterator it = CurrentLin.begin(); it != CurrentLin.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ED_LIN_CURD:
	case CTC_LIN_CURD:
		times.reserve(CurrentDensityLin.size() * 2);	//prevent future allocations
		for (std::multimap<double, LinearTimeDependence>::iterator it = CurrentDensityLin.begin(); it != CurrentDensityLin.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ADDITIONAL:
		times.reserve(AdditionalFlags.size() * 2);	//prevent future allocations
		for (std::multimap<double, TimeDependence>::iterator it = AdditionalFlags.begin(); it != AdditionalFlags.end(); ++it) {
			times.push_back(it->first);
			times.push_back(it->second.GetEndTime());
		}
		break;
	case CTC_ACTIVE:
		times.reserve(ActiveValues.size());	//prevent future allocations
		for (std::multimap<double, int>::iterator it = ActiveValues.begin(); it != ActiveValues.end(); ++it) {
			times.push_back(it->first);
		}
		break;
	default:
		break;
	}

	return(0);
}




int TransContact::AddTimeDependence(TimeDependence& dat, double curtime)
{
	double starttime = dat.GetStartTime();
	int flgs = dat.GetFlags();
	bool added = false;
	if (dat.isLin())
	{
		if (ALL_SET_BITS_ARE_ONE(flgs, CONTACT_VOLT_LINEAR)) {
			added = true;
			VoltageLin.insert(std::pair<double, LinearTimeDependence>(starttime, (LinearTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_EFIELD_LINEAR | CONTACT_EFIELD_LINEAR_ED)) {
			added = true;
			EFieldLin.insert(std::pair<double, LinearTimeDependence>(starttime, (LinearTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_CURRENT_LINEAR | CONTACT_CURRENT_LINEAR_ED)) {
			added = true;
			CurrentLin.insert(std::pair<double, LinearTimeDependence>(starttime, (LinearTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_CUR_DENSITY_LINEAR | CONTACT_CUR_DENSITY_LINEAR_ED)) {
			added = true;
			CurrentDensityLin.insert(std::pair<double, LinearTimeDependence>(starttime, (LinearTimeDependence&)dat));
		}
	}
	else if (dat.isCos())
	{
		if (ALL_SET_BITS_ARE_ONE(flgs, CONTACT_VOLT_COS)) {
			added = true;
			VoltageCos.insert(std::pair<double, CosTimeDependence>(starttime, (CosTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_EFIELD_COS | CONTACT_EFIELD_COS_ED)) {
			added = true;
			EFieldCos.insert(std::pair<double, CosTimeDependence>(starttime, (CosTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_CURRENT_COS | CONTACT_CURRENT_COS_ED)) {
			added = true;
			CurrentCos.insert(std::pair<double, CosTimeDependence>(starttime, (CosTimeDependence&)dat));
		}
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_COS_ED)) {
			added = true;
			CurrentDensityCos.insert(std::pair<double, CosTimeDependence>(starttime, (CosTimeDependence&)dat));
		}

	}
	else {
		//it is neither.
		if (ANY_SET_BITS_ARE_ONE(flgs, CONTACT_CONNECT_EXTERNAL | CONTACT_SINK | CONTACT_CONSERVE_CURRENT | CONTACT_OPP_DFIELD | CONTACT_MINIMIZE_E_ENERGY)) {
			added = true;
			AdditionalFlags.insert(std::pair<double, TimeDependence>(starttime, dat));
		}
	}
	if (added) {
		if ((nexttimechange == CONTACT_HOLD_INDEFINITELY || starttime < nexttimechange) &&
			starttime > curtime)
			nexttimechange = starttime;
	}
	return 0;
}

bool TransContact::RemoveTimeDependence(double sTime, double eTime, int flags)
{

	bool removed = false;
	if (ALL_SET_BITS_ARE_ONE(flags, CONTACT_VOLT_LINEAR)) {
		auto it = MapFindFirstLessEqual(VoltageLin, sTime);
		while (it != VoltageLin.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != VoltageLin.end()) {
			VoltageLin.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_EFIELD_LINEAR | CONTACT_EFIELD_LINEAR_ED)) {
		auto it = MapFindFirstLessEqual(EFieldLin, sTime);
		while (it != EFieldLin.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != EFieldLin.end()) {
			EFieldLin.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_CURRENT_LINEAR | CONTACT_CURRENT_LINEAR_ED)) {
		auto it = MapFindFirstLessEqual(CurrentLin, sTime);
		while (it != CurrentLin.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != CurrentLin.end()) {
			CurrentLin.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_CUR_DENSITY_LINEAR | CONTACT_CUR_DENSITY_LINEAR_ED)) {
		auto it = MapFindFirstLessEqual(CurrentDensityLin, sTime);
		while (it != CurrentDensityLin.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != CurrentDensityLin.end()) {
			CurrentDensityLin.erase(it);
			removed = true;
		}
	}

	if (ALL_SET_BITS_ARE_ONE(flags, CONTACT_VOLT_COS)) {
		auto it = MapFindFirstLessEqual(VoltageCos, sTime);
		while (it != VoltageCos.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != VoltageCos.end()) {
			VoltageCos.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_EFIELD_COS | CONTACT_EFIELD_COS_ED)) {
		auto it = MapFindFirstLessEqual(EFieldCos, sTime);
		while (it != EFieldCos.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != EFieldCos.end()) {
			EFieldCos.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_CURRENT_COS | CONTACT_CURRENT_COS_ED)) {
		auto it = MapFindFirstLessEqual(CurrentCos, sTime);
		while (it != CurrentCos.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != CurrentCos.end()) {
			CurrentCos.erase(it);
			removed = true;
		}
	}
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_COS_ED)) {
		auto it = MapFindFirstLessEqual(CurrentDensityCos, sTime);
		while (it != CurrentDensityCos.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != CurrentDensityCos.end()) {
			CurrentDensityCos.erase(it);
			removed = true;
		}
	}



	//it is neither.
	if (ANY_SET_BITS_ARE_ONE(flags, CONTACT_CONNECT_EXTERNAL | CONTACT_SINK | CONTACT_CONSERVE_CURRENT | CONTACT_OPP_DFIELD | CONTACT_MINIMIZE_E_ENERGY)) {
		auto it = MapFindFirstLessEqual(AdditionalFlags, sTime);
		while (it != AdditionalFlags.end()) {	//add some additional protection. keep going through them
			if (!DoubleEqual(it->first, sTime))
				++it;
			else if (!DoubleEqual(it->second.GetEndTime(), eTime))	//until it finds the correct end time. Not enough known data to actually get at the rest
				++it;
			else
				break;
		}
		if (it != AdditionalFlags.end()) {
			AdditionalFlags.erase(it);
			removed = true;
		}
	}



	return removed;
}

int TransContact::AddCosTimeDependence(CosTimeDependence data, ctcIDS which, double starttime, double curtime)
{
	bool added = true;
	switch (which)
	{
	case CTC_COS_VOLT:
		VoltageCos.insert(std::pair<double, CosTimeDependence>(starttime, data));
		break;
	case CTC_COS_EFIELD:
		EFieldCos.insert(std::pair<double, CosTimeDependence>(starttime, data));
		break;
	case CTC_COS_CUR:
		CurrentCos.insert(std::pair<double, CosTimeDependence>(starttime, data));
		break;
	case CTC_COS_CURD:
		CurrentDensityCos.insert(std::pair<double, CosTimeDependence>(starttime, data));
		break;
	default:
		added = false;
		break;
	}

	if (added)
	{
		if ((nexttimechange == CONTACT_HOLD_INDEFINITELY || starttime < nexttimechange) &&
			starttime > curtime)
			nexttimechange = starttime;
	}

	return(0);
}

int TransContact::AddLinearDependence(LinearTimeDependence data, ctcIDS which, double starttime, double curtime)		//adds a fixed voltage to the contact
{
	bool added = true;
	switch (which)
	{
	case CTC_LIN_VOLT:
		VoltageLin.insert(std::pair<double, LinearTimeDependence>(starttime, data));
		break;
	case CTC_LIN_EFIELD:
		EFieldLin.insert(std::pair<double, LinearTimeDependence>(starttime, data));
		break;
	case CTC_LIN_CUR:
		CurrentLin.insert(std::pair<double, LinearTimeDependence>(starttime, data));
		break;
	case CTC_LIN_CURD:
		CurrentDensityLin.insert(std::pair<double, LinearTimeDependence>(starttime, data));
		break;
	default:
		added = false;
		break;
	}

	if (added)
	{
		if ((nexttimechange == CONTACT_HOLD_INDEFINITELY || starttime < nexttimechange) &&
			starttime > curtime)
			nexttimechange = starttime;
	}

	return(0);
}




void TransContact::UpdateNextTime(double time)
{

	nexttimechange = CONTACT_HOLD_INDEFINITELY;

	//look for the lowest starttime greater than time, and update the pointers as necessary
	std::multimap<double, CosTimeDependence>::iterator ptrCos = VoltageCos.upper_bound(time);	//find first greater. [,)
	std::multimap<double, LinearTimeDependence>::iterator ptrLinear = VoltageLin.upper_bound(time);
	std::multimap<double, TimeDependence>::iterator ptrCurOut = AdditionalFlags.upper_bound(time);
	if (ptrCos != VoltageCos.end())
	{
		if (ptrCos->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrCos->first;
	}
	if (ptrLinear != VoltageLin.end())
	{
		if (ptrLinear->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrLinear->first;
	}
	if (ptrCurOut != AdditionalFlags.end())
	{
		if (ptrCurOut->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrCurOut->first;
	}

	//now do the efields
	ptrCos = EFieldCos.upper_bound(time);
	ptrLinear = EFieldLin.upper_bound(time);
	if (ptrCos != EFieldCos.end())
	{
		if (ptrCos->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrCos->first;
	}
	if (ptrLinear != EFieldLin.end())
	{
		if (ptrLinear->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrLinear->first;
	}

	//now do the currents
	ptrCos = CurrentCos.upper_bound(time);
	ptrLinear = CurrentLin.upper_bound(time);
	if (ptrCos != CurrentCos.end())
	{
		if (ptrCos->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrCos->first;
	}
	if (ptrLinear != CurrentLin.end())
	{
		if (ptrLinear->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrLinear->first;
	}

	//and the currend densities
	ptrCos = CurrentDensityCos.upper_bound(time);
	ptrLinear = CurrentDensityLin.upper_bound(time);
	if (ptrCos != CurrentDensityCos.end())
	{
		if (ptrCos->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrCos->first;
	}
	if (ptrLinear != CurrentDensityLin.end())
	{
		if (ptrLinear->first < nexttimechange || nexttimechange == CONTACT_HOLD_INDEFINITELY)
			nexttimechange = ptrLinear->first;
	}



	return;
}


double TransContact::GetFrequency(void)
{
	double maxFreqActive = 0.0;
	if (freqVoltage > maxFreqActive)
		maxFreqActive = freqVoltage;
	if (freqCurrent > maxFreqActive)
		maxFreqActive = freqCurrent;
	if (freqEField > maxFreqActive)
		maxFreqActive = freqEField;

	return(maxFreqActive);
}


double TransContact::GetVoltageFrequency(void) { return freqVoltage; }
double TransContact::GetCurrentFrequency(void) { return freqCurrent; }
double TransContact::GetEfieldFrequency(void) { return freqEField; }


double TransContact::GetPreviousAlteration(double time)
{
	std::map<double, int>::iterator node = MapFindFirstLessEqual(ActiveValues, time);
	if (node != ActiveValues.end())
		return(node->first);
	else
		return(CONTACT_HOLD_INDEFINITELY);	//flag that this is a bad return
}


void TransContact::InitNewIteration(double time, bool forcecalc)
{
	UpdateInternalValsPreBD(time);	//updates all the voltages/currents/efields, etc.
	ct->InitNewIteration();	//clears out the contact variables
	UpdateNextTime(time);	//marks the next time the update needs to go through all the logic to change the current stuff

	return;
}

int TransientSimData::PrepBoundaryConditions(bool force) {
	for (unsigned int i = 0; i < ContactData.size(); ++i)
		ContactData[i]->InitNewIteration(simtime, force);
	return 0;
}