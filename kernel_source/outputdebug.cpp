
#include "outputdebug.h"

#include "model.h"
#include "light.h"
#include "RateGeneric.h"
#include "DriftDiffusion.h"
#include "transient.h"
#include "contacts.h"
#include "physics.h"
#include "srh.h"
#include "SSNewton.h"
#include "ss.h"
#include "EquationSolver.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>

int CompareMatrixOutputs(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	unsigned long int sz = data.EStates.size();
	
	unsigned long int matrixSz = sz*sz;
	double* derivMatrixAnal = NULL;
	double* delOccsAnal = NULL;
	bool* useELevelAnal = NULL;
	double* derivMatrixNum = NULL;
	double* delOccsNum = NULL;
	bool* useELevelNum = NULL;
	try
	{
		useELevelAnal = new bool[sz];
		delOccsAnal = new double[sz];

		useELevelNum = new bool[sz];
		delOccsNum = new double[sz];
	}
	catch (...)
	{
		ProgressCheck(63, false);

		return(-1);
	}
	try
	{
		derivMatrixAnal = new double[matrixSz];
		derivMatrixNum = new double[matrixSz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		delete [] useELevelAnal;
		delete [] delOccsAnal;
		delete [] useELevelNum;
		delete [] delOccsNum;
		return(-1);
	}
	std::ofstream file;
	std::string fName = "MatrixCompare_"+std::to_string(data.CalcStateCtr)+".dat";
	file.open(fName, std::fstream::out);

	if (!file.is_open())
		return(-1);


	CalcAnalyticalJacobian(data, mdl, mdesc, kThread, derivMatrixAnal, delOccsAnal,useELevelAnal,sz,true);

	CalcNumericalJacobian(data, mdl, mdesc, kThread, derivMatrixNum, delOccsNum,useELevelNum,sz,true, derivMatrixAnal);

	file << "Matrix size: " << sz << " x " << sz << std::endl;

	file << "Comparing useELevels: ";
	unsigned int bad = 0;
	std::vector<unsigned long int> badIndices;
	std::vector<double> pErr;
	for(unsigned long int i=0; i<sz; ++i)
	{
		if(useELevelNum[i] != useELevelAnal[i])
		{
			badIndices.push_back(i);
			bad++;
		}
	}

	if(bad == 0)
		file << "Good" << std::endl;
	else
	{
		file << bad << std::endl;
		for(unsigned int i=0; i<bad-1; i++)
		{
			file << badIndices[i] << ", ";
		}
		file << badIndices[bad-1] << std::endl;
	}

	badIndices.clear();
	bad=0;

	file << "Comparing Rates:";
	for(unsigned long int i=0; i<sz; ++i)
	{
		if(!DoubleEqual(delOccsAnal[i], delOccsNum[i]))//, 1.0e-13))
		{
			badIndices.push_back(i);
			pErr.push_back(PercentError(delOccsAnal[i], delOccsNum[i]));
			bad++;
		}
	}
#ifdef NDEBUG
	if(bad == 0)
		file << " Good to 13 digits" << std::endl;
	else
	{
		file << bad << " not accurate to 13 digits. Showing not accurate to 8 digits." << std::endl;
		double sum = 0.0;
		double sumSQ = 0.0;
		for (unsigned long int i = 0; i < bad; i++)

		{
			if (fabs(pErr[i]) > 1.0e-8)
				file << std::setw(7) << std::right << badIndices[i] << ", " << std::setw(12) << std::right << std::setprecision(3) << pErr[i] * 100.0 << "%, "
				<< std::setw(14) << std::left << std::setprecision(5) << badIndices[i] << " "
				<< std::setw(14) << std::left << std::setprecision(5) << badIndices[i] << std::endl;
			sum += pErr[i];
			sumSQ += (pErr[i] * pErr[i]);
		}
		sum = sum / double(bad);
		double stdDev = sum*sum;
		sumSQ = sumSQ / double(bad);
		stdDev = sqrt(sumSQ - stdDev);
		file << "Average: " << std::scientific << std::setprecision(6) << sum << std::endl;
		file << "Deviation: " << std::scientific << std::setprecision(6) << stdDev << std::endl;
	}
#else
	file << bad << " not accurate to 13 digits. Showing not accurate to 8 digits." << std::endl;
	double sum=0.0;
	double sumSQ = 0.0;
	for (unsigned long int i = 0; i<sz; i++)
	{
		if (delOccsAnal[i] != 0.0 || delOccsNum[i] != 0.0)
			file << std::setw(7) << std::right << i << ", "
				<< std::setw(14) << std::left << std::setprecision(5) << delOccsAnal[i] << " "
				<< std::setw(14) << std::left << std::setprecision(5) << delOccsNum[i] << std::endl;
		
	}
#endif
	bad=0;
	badIndices.clear();
	pErr.clear();

	file << "Comparing Derivatives:";
	for(unsigned long int i=0; i<matrixSz; ++i)
	{
		if(!DoubleEqual(derivMatrixAnal[i], derivMatrixNum[i], 1.0e-13))
		{
			badIndices.push_back(i);
			pErr.push_back(PercentError(derivMatrixAnal[i], derivMatrixNum[i]));
			bad++;
		}
	}
	if(bad == 0)
		file << " Good to 13 digits" << std::endl;
	else
	{
		file << bad << " not accurate to 13 digits. Showing not accurate to 8 digits." << std::endl;
		double sum=0.0;
		double sumSQ = 0.0;
		double max = 0.0;
		for(unsigned long int i=0; i<bad; i++)
		{
			if(fabs(pErr[i]) > 1.0e-8)
				file << std::setw(7) << std::right << badIndices[i] << ", " << std::setw(12) << std::right << std::setprecision(3) << pErr[i]*100.0 << "%, "
					<< std::setw(14) << std::left << std::setprecision(5) << derivMatrixAnal[badIndices[i]] << " "
					<< std::setw(14) << std::left << std::setprecision(5) << derivMatrixNum[badIndices[i]] << std::endl;
			sum += pErr[i];
			sumSQ += (pErr[i] * pErr[i]);
			if (fabs(pErr[i]) > max)
				max = fabs(pErr[i]);
		}
		sum = sum/double(bad);
		double stdDev=sum*sum;
		sumSQ = sumSQ/double(bad);
		stdDev = sqrt(sumSQ - stdDev);
		file << "Average: " << std::scientific << std::setprecision(6) << sum << std::endl;
		file << "Deviation: " << std::scientific << std::setprecision(6) << stdDev << std::endl;
		file << "Max: " << std::scientific << std::setprecision(6) << max << std::endl;
	}

	SolveDenseOneTime(kThread, derivMatrixAnal, delOccsAnal, sz);
	SolveDenseOneTime(kThread, derivMatrixNum, delOccsNum, sz);

	bad=0;
	badIndices.clear();
	pErr.clear();

	file << "Comparing delOccs:";
	for(unsigned long int i=0; i<sz; ++i)
	{
		if(!DoubleEqual(delOccsAnal[i], delOccsNum[i], 1.0e-13))
		{
			badIndices.push_back(i);
			pErr.push_back(PercentError(delOccsAnal[i], delOccsNum[i]));
			bad++;
		}
	}
	if(bad == 0)
		file << " Good to 13 digits" << std::endl;
	else
	{
		file << bad << " not accurate to 13 digits. Showing not accurate to 8 digits." << std::endl;
		double sum=0.0;
		double sumSQ = 0.0;
		for(unsigned long int i=0; i<bad; i++)
		{
			if(fabs(pErr[i]) > 1.0e-8)
				file << std::setw(7) << std::right << badIndices[i] << ", " << std::setw(12) << std::right << std::setprecision(3) << pErr[i]*100.0 << "%, "
					<< std::setw(14) << std::left << std::setprecision(5)<< delOccsAnal[badIndices[i]] << " " 
					<< std::setw(14) << std::left << std::setprecision(5)<< delOccsNum[badIndices[i]] << std::endl;
			sum += pErr[i];
			sumSQ += (pErr[i] * pErr[i]);
		}
		sum = sum/double(bad);
		double stdDev=sum*sum;
		sumSQ = sumSQ/double(bad);
		stdDev = sqrt(sumSQ - stdDev);
		file << "Average: " << std::scientific << std::setprecision(6) << sum << std::endl;
		file << "Deviation: " << std::scientific << std::setprecision(6) << stdDev << std::endl;
	}

	bad=0;
	badIndices.clear();
	pErr.clear();
	file.close();

	delete [] useELevelAnal;
	delete [] delOccsAnal;
	delete [] useELevelNum;
	delete [] delOccsNum;
	delete [] derivMatrixAnal;
	delete [] derivMatrixNum;

	return(0);
}

double PercentError(double ref, double comp)
{
	if(ref == 0.0)	//deal with all obnoxious divide by zeros
		return(comp==0 ? 0.0 : (comp<0 ? -1e100:1e100));
	return((comp-ref)/ref);
}

int MCStateProbDistribution(std::vector<unsigned int>& counts, std::vector<unsigned int>& bdcounts, SteadyState_SAAPD& data, int iter)
{
	std::ofstream file;
	std::string fName = "randomDistribution_" + std::to_string(iter) + ".dat";
	file.open(fName, std::fstream::out);

	if (!file.is_open())
		return(0);

//	int width1 = int(log10(double(counts.size()))) + 3;
//	if(width1 < 7)
//		width1 = 7;
	file << std::left << std::setw(7) << "Index";
	file << std::left << std::setw(9) << "UniqueID";
	file << std::left << std::setw(8) << "Total";
	file << std::left << std::setw(8) << "Bad";
	file << std::left << std::setw(8) << "Good" << std::endl;
	
	for (unsigned int i = 0; i < counts.size(); ++i)
	{
		file << std::setw(7) << std::left << std::fixed << i;
		file << std::setw(9) << std::left << std::fixed << data.EStates[i].ptr->uniqueID;
		file << std::left << std::setw(8) << counts[i];
		file << std::left << std::setw(8) << bdcounts[i];
		file << std::left << std::setw(8) << counts[i] - bdcounts[i] << std::endl;
	}

	file.close();
	return(1);
}

int RecordMCTransfer(bool newFile, ELevel* src, double ChangeLocal, unsigned long iter, double orig)
{
	std::ofstream file;
	
	if(newFile)
	{
		file.open("MCData.dat", std::fstream::out);	//open the file for writing blank
		file << "Iteration ";
		file << "Point    " << std::setw(5) << std::right << "Type" << std::setw(8) << std::right << "EV(eV)" << std::setw(16) << std::right << "Occupancy"
			<< std::setw(16) << std::right << "Local_Change" << std::endl;
		file.close();
		return(0);
	}

	file.open("MCData.dat", std::fstream::out | std::fstream::app);	//open the file for writing blank
	if(file.is_open() == false)
	{
		std::cout << "Failed to open MCData.dat" << std::endl;
		return(0);
	}

	file << std::setw(9) << std::right << iter << " ";
	file << std::setw(9) << std::left << src->getPoint().adj.self << " ";
	
	if(src->imp)
	{
		if(src->imp->isCBReference())
			file << "DON: " << std::setw(8) << std::setprecision(4) << std::fixed << std::right << src->eUse;
		else
			file << "ACC: " << std::setw(8) << std::setprecision(4) << std::fixed  << std::right << src->eUse;
	}
	else if(src->carriertype == ELECTRON)
		file << "CON: " << std::setw(8) << std::setprecision(4) << std::fixed  << std::right << src->eUse;
	else
		file << "VAL: " << std::setw(8) << std::setprecision(4) << std::fixed  << std::right << src->eUse;

	file << " " << std::setw(15) << std::setprecision(6) << std::scientific << std::right << orig;
	file << " " << std::setw(15) << std::setprecision(6) << std::scientific << std::right << ChangeLocal << std::endl;


	file.close();
	return(0);


}

int ChargeTransfer::OutputTransfer(bool clear)
{
	std::ofstream file;

	if(clear)
	{
		file.open("ChargeTransfer.dat", std::fstream::out);	//open the file for writing blank
		file << "How much charge was transferring according to each rate in the entire device" << std::endl;
		file << std::setw(17) << std::right << "Sim_Time" << " ";
		file << std::setw(17) << std::right << "Drift_Diffusion" << " ";
		file << std::setw(17) << std::right << "DD_Trans" << " ";
		file << std::setw(17) << std::right << "Scattering" << " ";
		file << std::setw(17) << std::right << "Scat_Trans" << " ";
		file << std::setw(17) << std::right << "SRH1" << " ";
		file << std::setw(17) << std::right << "SRH1_Trans" << " ";
		file << std::setw(17) << std::right << "SRH2" << " ";
		file << std::setw(17) << std::right << "SRH2_Trans" << " ";
		file << std::setw(17) << std::right << "SRH3" << " ";
		file << std::setw(17) << std::right << "SRH3_Trans" << " ";
		file << std::setw(17) << std::right << "SRH4" << " ";
		file << std::setw(17) << std::right << "SRH4_Trans" << " ";
		file << std::setw(17) << std::right << "Generation" << " ";
		file << std::setw(17) << std::right << "Gen_Trans" << " ";
		file << std::setw(17) << std::right << "Recombination" << " ";
		file << std::setw(17) << std::right << "Rec_Trans" << " ";
		file << std::setw(17) << std::right << "Light_Gen" << " ";
		file << std::setw(17) << std::right << "Light_Gen_Trans" << " ";
		file << std::setw(17) << std::right << "Stim_Emis_Rec" << " ";
		file << std::setw(17) << std::right << "SE_Rec_Trans" << " ";
		file << std::setw(17) << std::right << "Light_SRH" << " ";
		file << std::setw(17) << std::right << "Light_SRH_Trans" << " ";
		file << std::setw(17) << std::right << "SE_SRH" << " ";
		file << std::setw(17) << std::right << "SE_SRH_Trans" << " ";
		file << std::setw(17) << std::right << "Light_Def" << " ";
		file << std::setw(17) << std::right << "Light_Def_Trans" << " ";
		file << std::setw(17) << std::right << "SE_Def" << " ";
		file << std::setw(17) << std::right << "SE_Def_Trans" << " ";
		file << std::setw(17) << std::right << "Light_Scatter" << " ";
		file << std::setw(17) << std::right << "Light_Scat_Trans" << " ";
		file << std::setw(17) << std::right << "SE_Scatter" << " ";
		file << std::setw(17) << std::right << "SE_Scatter_Trans" << std::endl;
		
		file.close();
		return(0);
	}

	file.open("ChargeTransfer.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open ChargeTransfer.dat" << std::endl;
		return(0);
	}

	file << std::setw(17) << std::setprecision(9) << std::right << simtime << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << dd << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << ddTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << scat << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << scatTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh1 << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh1Trans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh2 << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh2Trans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh3 << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh3Trans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh4 << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << srh4Trans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << gen << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << genTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << rec << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << recTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTgen << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTgenTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMrec << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMrecTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTsrh << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTsrhTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMsrh << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMsrhTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTdef << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTdefTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMdef << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMdefTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTscat << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << LTscatTrans << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMscat << " ";
	file << std::setw(17) << std::setprecision(9) << std::right << EMscatTrans << std::endl;

	file.close();

	return(0);
}


int RecordNonSteadyState(Simulation* sim, bool clear)
{
	std::ofstream file;
	RateCount count;
	count.gen = count.rec = count.srh1 = count.srh2 = count.srh3 = count.srh4 = count.scat = count.dd = count.tun = count.LTgen = count.LTsrh = count.LTdef = count.LTscat = count.EMrec = count.EMsrh = count.EMdef = count.EMscat = 0;

	if(clear)
	{
		file.open("SS.dat", std::fstream::out);	//open the file for writing blank
		file << std::endl;
		file.close();
		return(0);
	}


	file.open("SS.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open SS.dat" << std::endl;
		return(0);
	}

	file << std::fixed << std::setw(5) << sim->curiter << ": ";
	

	for(std::map<long int,int>::iterator it = sim->NoSS.begin(); it != sim->NoSS.end(); ++it)
	{

		file << it->first << "(" << it->second << "), ";

		if(it->second & RATE_GENERATION)	//check for the flags on the bits to get a tally of what's actually occurring
			count.gen++;
		if(it->second & RATE_RECOMBINATION)	//check for the flags on the bits to get a tally of what's actually occurring
			count.rec++;
		if(it->second & RATE_SRH1)	//check for the flags on the bits to get a tally of what's actually occurring
			count.srh1++;
		if(it->second & RATE_SRH2)	//check for the flags on the bits to get a tally of what's actually occurring
			count.srh2++;
		if(it->second & RATE_SRH3)	//check for the flags on the bits to get a tally of what's actually occurring
			count.srh3++;
		if(it->second & RATE_SRH4)	//check for the flags on the bits to get a tally of what's actually occurring
			count.srh4++;
		if(it->second & RATE_SCATTER)	//check for the flags on the bits to get a tally of what's actually occurring
			count.scat++;
		if(it->second & RATE_DRIFTDIFFUSE)	//check for the flags on the bits to get a tally of what's actually occurring
			count.dd++;
		if(it->second & RATE_TUNNEL)	//check for the flags on the bits to get a tally of what's actually occurring
			count.tun++;
		if(it->second & RATE_LIGHTGEN)	//check for the flags on the bits to get a tally of what's actually occurring
			count.LTgen++;
		if(it->second & RATE_LIGHTSCAT)	//check for the flags on the bits to get a tally of what's actually occurring
			count.LTscat++;
		if(it->second & RATE_LIGHTDEF)	//check for the flags on the bits to get a tally of what's actually occurring
			count.LTdef++;
		if(it->second & RATE_LIGHTSRH)	//check for the flags on the bits to get a tally of what's actually occurring
			count.LTsrh++;
		if(it->second & RATE_STIMEMISREC)	//check for the flags on the bits to get a tally of what's actually occurring
			count.EMrec++;
		if(it->second & RATE_STIMEMISDEF)	//check for the flags on the bits to get a tally of what's actually occurring
			count.EMdef++;
		if(it->second & RATE_STIMEMISSRH)	//check for the flags on the bits to get a tally of what's actually occurring
			count.EMsrh++;
		if(it->second & RATE_STIMEMISSCAT)	//check for the flags on the bits to get a tally of what's actually occurring
			count.EMscat++;
	}

	file << std::endl;

	file << "Gen: " << count.gen << " | Rec: " << count.rec << " | SRH1: " << count.srh1 << " | SRH2: " << count.srh2 << " | SRH3: " << count.srh3 << " | SRH4: " << count.srh4 << " | Scat: " << count.scat << " | DD: " << count.dd << " | TUN: " << count.tun << 
		" | LGen: " << count.LTgen << " | LSRH: " << count.LTsrh  << " | LDef: " << count.LTdef  << " | LScat: " << count.LTscat  << " | SERec: " << count.EMrec  << " | SEsrh: " << count.EMsrh  << " | SEdef: " << count.EMdef  << " | SEscat: " << count.EMscat <<
		std::endl << std::endl;

	return(0);
}



int TransientSimData::RecordTimestep(bool clear)
{

	std::ofstream file;
	
	if(clear)
	{
		file.open("timestep.dat", std::fstream::out);	//open the file for writing blank
		file << std::endl;
		file.close();
		return(0);
	}


	file.open("timestep.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open timestep.dat" << std::endl;
		return(0);
	}

	file << std::fixed << std::setw(8) << sim->curiter << "\t\t" << std::scientific << std::setprecision(8) << timestep << "\t\t" << std::setprecision(8) << simtime << std::endl;

	file.close();
	
	return(0);
}


int RecordGenRecomb(bool newFile, double ECb, double EVb, double gen, double rec, bool isGeneration)
{
	std::ofstream file;
	
	if(newFile)
	{
		file.open("genrecDebug.dat", std::fstream::out);	//open the file for writing blank
		file << "TYPE" << std::setw(30) << std::right << "EC(eV)" << std::setw(30) << std::right << "EV(eV)" << std::setw(30) << std::right << "Generation" << std::setw(30) << std::right << "Recombination" << std::endl;
		file.close();
		return(0);
	}


	file.open("genrecDebug.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		file << "Failed to open genrecDebug.dat" << std::endl;
		return(0);
	}

	if(isGeneration)
		file << "GEN:";
	else
		file << "REC:";

	file << std::scientific << std::setprecision(17) << std::setw(30) << std::right << ECb;
	file << std::scientific << std::setprecision(17) << std::setw(30) << std::right << EVb;
	file << std::scientific << std::setprecision(17) << std::setw(30) << std::right << gen;
	file << std::scientific << std::setprecision(17) << std::setw(30) << std::right << rec << std::endl;

	file.close();
	
	return(0);
}


int DebugCarrierTransfer(double Old, double New, int point, bool band)
{
	std::fstream trace;
	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}
	
	trace << "Point " << point << " - ";
	if(band==CONDUCTION)
		trace << "CB - ";
	else
		trace << "VB - ";

	trace << std::setprecision(0) << std::fixed << Old << " --> " << std::setprecision(0) << New << std::endl;
	trace.close();

	return(0);
}

int DebugTimestep(Point* pt, bool clear)
{
	std::fstream tstep;

	if(clear)
	{
		tstep.open("tstepDebug.dat", std::fstream::out);	//open the file for writing blank
		tstep.close();
		return(0);
	}

	if(pt == NULL)
		return(0);

	tstep.open("tstepDebug.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(tstep.is_open() == false)
	{
		std::cout << "Failed to open tstepDebug.dat" << std::endl;
		return(0);

	}

	tstep << "Point " << std::setw(7) << std::left << pt->adj.self;
	tstep << "Desired Timestep: " << std::setw(15) << std::scientific << std::setprecision(7) << std::left << pt->desireTStep << "    ";
	if(pt->limitsTStep)
	{
		if(pt->limitsTStep->imp)
		{
			tstep << "Impurity     ";
			tstep << std::setw(8) << std::right << std::setprecision(5) << pt->limitsTStep->min << "eV    ";
			tstep << "Occupancy: " << std::setw(14) << std::left << pt->limitsTStep->GetBeginOccStates() << " < ";
			if(pt->limitsTStep->imp->isCBReference())
				tstep << std::setw(14) << std::left << pt->largestOccES << "   ";
			else
				tstep << std::setw(14) << std::left << pt->largestUnOccES << "   ";
		}
		else if(pt->limitsTStep->carriertype == ELECTRON)
		{
			tstep << "Conduction   ";
			tstep << std::setw(8) << std::right << std::setprecision(5) << pt->limitsTStep->min << "eV    ";
			tstep << "Occupancy: " << std::setw(14) << std::left << pt->limitsTStep->GetBeginOccStates() << " < " << std::setw(14) << std::left << pt->largestOccCB << "   ";
		}
		else
		{
			tstep << "Valence      ";
			tstep << std::setw(8) << std::right << std::setprecision(5) << pt->limitsTStep->min << "eV    ";
			tstep << "Occupancy: " << std::setw(14) << std::left << pt->limitsTStep->GetBeginOccStates() << " < " << std::setw(14) << std::left << pt->largestOccVB << "   ";
		}
		
	}
	else
	{
		tstep << "NONE";
		tstep.close();
		return(0);
	}
	tstep << "%Transfer: " << std::setw(11) << std::left << std::setprecision(3) << pt->limitsTStep->percentTransfer << "  ";
	

	
	
	std::vector<ELevelRate>::iterator offender = pt->limitsTStep->Rates.end();
	double minTStep = DBL_MAX;

	for(std::vector<ELevelRate>::iterator rt = pt->limitsTStep->Rates.begin(); rt != pt->limitsTStep->Rates.end(); ++rt)
	{ //there won't be that many rates for a single energy level. no need to do unsigned.
		if(rt->desiredTStep < minTStep && rt->desiredTStep > 0.0 && rt->type != RATE_CONTACT_CURRENT_EXT && rt->type != RATE_CONTACT_CURRENT_INTEXT)
		{
			minTStep = rt->desiredTStep;
			offender = rt;
		}
	}

	if(offender != pt->limitsTStep->Rates.end())
	{
		tstep << "Type: " << std::setw(8) << std::left << offender->type;
		tstep << "Worst Rate: " << std::setw(15) << std::setprecision(7) << std::left << offender->netrate << "  ";
		tstep << "Net Rate: " << std::setw(15) << std::scientific << std::setprecision(7) << std::left << (pt->limitsTStep->rateIn - pt->limitsTStep->rateOut) << "   ";
		tstep << "Occ: " << std::setw(17) << std::setprecision(9) << std::left << pt->limitsTStep->GetBeginOccStates() << " to ";
		tstep << std::setw(17) << std::setprecision(9) << std::left << offender->target->GetBeginOccStates() << std::endl;
	}
	else
		tstep << "Could not find offender to timestep" << std::endl;

	tstep.close();
	return(0);
}

int ProgressInt(int check, long long int num)
{
	std::fstream trace;
	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}

	switch(check)
	{
		case 0:
			trace << "Finished Prob Calc on point " << num << "\t";
			break;
		case 1:
			trace << "Must iterate through " << num << " probability points" << std::endl;
			break;
		case 2:
			trace << num << "), ";
			break;
		case 3:
			trace << "Working on particle " << num << std::endl;
			break;
		case 4:
			trace << "(" << num << ", ";
			break;
		case 5:
			trace << " - Chose entry " << num+1 << std::endl;
			break;
		case 6:
			trace << "Results in spot: " << num << std::endl;
			break;
		case 7:
			trace << "Current: " << num << std::endl;
			break;
		case 8:
			trace << "Loop Check point " << num << std::endl;
			break;
		case 9:
			trace << "Invalid direction passed in Equation Solver: " << num << std::endl;
			break;
		case 10:
			trace << "Solving Equation " << num << std::endl;
			break;
		case 11:
			trace << "Could not normalize equation by variable " << num << "because that variables was not present" << std::endl;
			break;
		case 12:
			trace << "Solved for variable " << num << std::endl;
			break;
		case 13:
			trace << "copying solution to " << num << std::endl;
			break;
		case 14:
			trace << "Failed to normalize equation as coefficient was zero(most likely) for variable " << num << " or equation did not exist." << std::endl;
			break;
		case 15:
			trace << "Passed an invalid direction to Exclusive Adjacent function: " << num << std::endl;
			break;
		case 16:
			trace << "Failed to generate mesh. Failed to allocate enough memory for grid points: " << num << " spots reserved" << std::endl;
			break;
		case 17:
			trace << "Point " << num << " is considered vacuum (typically bug or bad model setup)" << std::endl;
			break;
		case 18:
			trace << "Point " << num << " has a negative midn or midp after SRH on iteration ";
			break;
		case 19:
			trace << num << "." << std::endl;
			break;
		case 20:
			trace << "Point " << num << " has a negative n or p on iteration ";
			break;
		case 21:
			trace << num << "." << std::endl;
			break;
		case 22:
			trace << "Point " << num << " has a negative midn or midp after Thermal Gen/Rec on iteration ";
			break;
		case 23:
			trace << num << "." << std::endl;
			break;
		case 24:
			trace << "Point " << num << " has a negative midn or midp before vertical carriers took effect on iteration ";
			break;
		case 25:
			trace << num << "." << std::endl;
			break;
		case 26:
			trace << "Point: " << num << " ";
			break;
		case 27:
			trace << "Iteration: " << num << " ";
			break;
		case 28:
			trace << "Caution: One task was added to the contents of another task, " 
				<< "and the energy to/from states were not identical." << std::endl;
			break;
		case 29:
			trace << "Movement from " ;
			break;
		case 30:
			trace << " to " ;
			break;
		case 31:
			trace << " was added to ";
			break;
		case 32:
			trace << "Miscalculated absolute energies. High energy was less than low energy." << std::endl;
			break;
		case 33:
			trace << "Attempted executing task when there was no time std::left." << std::endl;
			break;
		case 34:
			trace << "Number of recombination rates does not correspond to the number of VB states it can recombine with." << std::endl;
			break;
		case 35:
			trace << "Invalid accept task: # " << num << std::endl;
			break;
		case 36:
			trace << "Could not calculate intersect point between two points. Direction passed: " << num << std::endl;
			break;
		case 37:
			trace << "Removing excess carriers gave an invalid case: " << num << std::endl;
			break;
		case 38:
			trace << "Unknown fermi calculation choice for occupied states: " << num << std::endl;
			break;
		case 39:
			trace << "Failed to determine direction as source point: " << num;
			break;
		case 40:
			trace << " was not adjacent to target point: " << num << std::endl;
			break;
		case 41:
			trace << "Consider a finer mesh. Large electric fields present. Point " << num << " zener tunnels into itself. " << std::endl;
			break;
		case 42:
			trace << "Saving entire device state after iteration " << num << std::endl;
			break;
		case 43:
			trace << "Point " << num;
			break;
		case 44:
			trace << "Probably stuck in an infinite loop with task type " << num << std::endl;
			break;
		case 45:
			trace << "There have been " << num << " tasks of this type." << std::endl;
			break;
		case 46:
			trace << "Unaccounted carrier transfer type: " << num << "(1/2/4/8/16 CB_src, CB_trg, Imp_Src, Imp_trg, electron)" << std::endl;
			break;
		case 47:
			trace << "There were " << num << " more absorptions created in light than were actually committed in tasks." << std::endl;
			break;
		case 48:
			trace << " could not find a target percent use in point " << num << std::endl;
			break;
		case 49:
			trace << " could not find a return target percent for the source point " << num << std::endl;
			break;
		case 50:
			trace << "A calculated net rate moves further away equilibrium!" << std::endl;
			break;
		case 51:
			trace << "Adaptive Mesh Refinement: Approximately " << num << " points are being merged and split." << std::endl;
			break;
		case 52:
			trace << "Invalid rate type: " << num << std::endl;
			break;
		case 53:
			trace << "Bad external dimension contact data. Not sure how this happened. Flags: " << num << std::endl;
			break;
		case 54:
			trace << "Unknown boundary condition flag: " << num << std::endl;
			break;
		case 55:
			trace << "Point " << num << " has zero electrons/holes in the bands. Current BC set to zero as result." << std::endl;
			break;
		case 56:
			trace << "Variable " << num << " thinks it's in equation ";
			break;
		case 57:
			trace << "The rate type was " << num << std::endl;
			break;
		case 58:
			trace << "Tried to access invalid index in energy state optical weights (out of range): " << num << std::endl;
			break;
		case 59:
			trace << "Non-finite value placed in matrix for solver. Source Equation: " << num;
			break;
		case 60:
			trace << "  -- Adjusted Equation: " << num;
			break;
		case 61:
			trace << "  -- Term: " << num << std::endl;
			break;
		case 62:
			trace << "State " << num << "has a rate in, and the code determined it should gain carriers!" << std::endl;
			break;
		case 63:
			trace << "State " << num << "has a rate out, and the code determined it should lose carriers!" << std::endl;
			break;
		case 64:
			trace << "Unable to set vacuum level for external contact point because point is not external" << std::endl;
			break;
		case 65:
			trace << "Did not set contact point because it was active, but not by electric field, current, or voltage (perhaps suppressed)" << std::endl;
			break;
		case 66:
			trace << "Setting boundary condition based on current cannot be computed due to index of end points being out of range: " << num << std::endl;
			break;
		case 67:
			trace << "Infinite rates calculated. Rate Type: " << num << std::endl;
			break;
		case 68:
			trace << "Checking rate for contact, and one of the states is null" << std::endl;
			break;
		case 69:
			trace << "SS Adjust type: " << num;	//no end line
			break;
		case 70:
			trace << "Reduced tolerance." << std::endl;
			break;
		case 71:
			trace << std::setw(14) << std::right <<  num << " / ";
			break;
		case 72:
			trace << std::setw(14) << std::right << num << " (complete/mostly/little/total) elements in Jacobian didn't change when solving." << std::endl;
			break;
		case 73:
			trace << "Could not find steady state solution with id " << num << std::endl;
			break;
		case 74:
			trace << "Missed scenario in calculating desired timestep." << std::endl;
			break;
		case 75:
			trace << "Incorrectly calculated timestep (negative)" << std::endl;
			break;
		case 76:
			trace << "State " << num;
			break;
		default:
			trace << "Invalid ProgressInt call: " << check << std::endl;
			break;
	}

	trace.close();
	return (1);
}

int ProgressString(std::string str)
{
	std::fstream trace;
	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}
	trace << str << std::endl;
	trace.close();

	return(0);
}

int ProgressDouble(int check, double num)
{
	std::fstream trace;
	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	bool rest = false;
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}
	switch(check)
	{
		case 0:
			trace << "Random: " << num << " -- ";
			break;
		case 1:
			trace << " " << num << " - ";
			break;
		case 2:
			trace << "EfieldX " << num;
			break;
		case 3:
			trace << " --- y: " << num << std::endl;
			break;
		case 4:
			trace << "Prefactor: " << num << std::endl;
			break;
		case 5:
			trace << "Fermi Energy: " << num << std::endl;
			break;
		case 6:
			trace << "Problem calculating fermi energy.  Two consecutive same guesses: " << num << "eV." << std::endl;
			break;
		case 7:
			trace << "midp calculated to be a number less than zero - modified to zero from: " << std::scientific << std::setprecision(16) << num << std::endl;
			break;
		case 8:
			trace << "midn calculated to be a number less than zero - modified to zero from: " << std::scientific << std::setprecision(16) << num << std::endl;
			break;
		case 9:
			trace << "Attempted executing task which had " << num << " carriers (aka, negative!)" << std::endl;
			break;
		case 10:
			trace << "Just attempted to create or modify a task to less than zero carriers: " << num << std::endl << "Task now has 0.0 carriers." << std::endl;
			break;
		case 11:
			trace << "Numstates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 12:
			trace << "EndOccstates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 13:
			trace << "BeginOccstates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 14:
			trace << "OccMaxstates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 15:
			trace << "OccTimestates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 16:
			trace << "MidOccTimestates just set to number less than zero and reset to zero: " << num << std::endl;
			break;
		case 17:
			trace << "There are more Occmaxstates (" << num << ") than the number of states. Reset to number of states." << std::endl;
			break;
		case 18:
			trace << "There are more EndOccstates (" << num << ") than the number of states. Reset to number of states." << std::endl;
			break;
		case 19:
			trace << "There are more Beginoccstates (" << num << ") than the number of states. Reset to number of states." << std::endl;
			break;
		case 20:
			trace << "Just attempted to decrease midn or mid p by " << num << std::endl;
			break;
		case 21:
			trace << "Band was somehow just given a negative total number of carriers: " << num << std::endl;
			break;
		case 22:
			trace << "Band just had a negative number added (should be positive) to its total number of carriers: " << num << std::endl;
			break;
		case 23:
			trace << "Invalid (Valid: [0.0-1.0]) proportion of carriers being sent in a movement task setup: " << num << std::endl;
			break;
		case 24:
			trace << "MidN and MidP are now different. Midn: " << num;
			break;
		case 25:
			trace << " Midp: " << num << std::endl;
			break;
		case 26:
			trace << "In calculating the number of carriers needed to equilibrate, the result needed to take the sqrt of a negative number: " << num << std::endl;
			break;
		case 27:
			trace << "Individually assigning " << num << " carriers. This may be getting excessive." << std::endl;
			break;
		case 28:
			trace << "Failed to do proper bookkeeping of carriers for rho. It was off by " << num << std::endl;
			break;
		case 29:
			trace << "Just assigned a percent success of: " << std::fixed << std::setprecision(8) << num << ". It should not go over 1.0. Ever." << std::endl;
			break;
		case 30:
			trace << " changed from " << num << "eV to ";
			break;
		case 31:
			trace << num << "eV" << std::endl;
			break;
		case 32:
			trace << "Failed to move up " << num << " electrons as a result of light." << std::endl;
			break;
		case 33:
			trace << "Trying to take negative of square root in calculating generation/recombination. Mathematically should be impossible: " << num << std::endl;
			break;
		case 34:
			trace << "Trying to take negative of square root in calculating SRH. Mathematically should be impossible: " << num << std::endl;
			break;
		case 35:
			trace << "Trying to take negative of square root in calculating scattering. Mathematically should be impossible: " << num << std::endl;
			break;
		case 36:
			trace << "Just set a carrier momentum outside of it's allowed bounds: " << num << std::endl;
			break;
		case 37:
			trace << "Calculated Current: " << num;
			break;
		case 38:
			trace << "     Expected Current: " << num;
			break;
		case 39:
			trace << "     Expected Error (Del_Efp=2.4e-6): " << num << std::endl;
			break;
		case 40:
			trace << "Altering desired percent change of carriers: " << std::scientific << std::setprecision(3) << num*100.0 << "%" << std::endl;
			break;
		case 41:
			trace << "Attempted to transfer carriers by " << num;
			break;
		case 42:
			trace << " when it should have been " << num << std::endl;
			break;
		case 43:
			trace << "Fermi level for contact point just set to " << num << std::endl;
			break;	
		case 44:
			trace << "Thermalizing band started with " << num << "carriers ";
			break;
		case 45:
			trace << "and ended with " << num << " carriers. Potential bug." << std::endl;
			break;
		case 46:
			trace << "Maximum Occupancy Percent Change: " << num << std::endl;
			break;
		case 47:
			trace << "Max Rate Percent Change: " << num << std::endl;
			break;
		case 48:
			//60 seconds = 1 minute
			//3600 seconds = 1 hour
			//86400 seconds = 1 day
			//604800 seconds = 1 week
			trace << "Simulation took " << std::fixed;
			if(num >= 604800.0)
			{
				double weeks = floor(num/604800.0);
				num -= weeks * 604800.0;
				trace << std::setprecision(0) << weeks << " weeks, ";
				rest = true;
			}
			if(num >= 86400 || rest)
			{
				double days = floor(num/86400.0);
				num -= days * 86400.0;
				trace << std::setprecision(0) << days << " days, ";
				rest = true;
			}
			if(num >= 3600 || rest)
			{
				double hours = floor(num/3600);
				num -= hours * 3600.0;
				trace << std::setprecision(0) << hours << " hours, ";
				rest = true;
			}
			if(num >= 60.0 || rest)
			{
				double minutes = floor(num/60.0);
				num -= minutes * 60.0;
				if(rest)
					trace << std::setprecision(0) << minutes << " minutes, and ";
				else
					trace << std::setprecision(0) << minutes << " minutes and ";


			}
			trace << std::setprecision(1) << num << " seconds to finish." << std::endl;
			break;
		case 49:
			trace << "The maximum current supported by: the thermal velocity and number of carriers in the thermally equilibrated contact is roughly " << std::setprecision(4) << std::fixed;
			if (num < 1.0e-12)
				trace << num*1.0e15 << " fA" << std::endl;
			else if (num < 1.0e-9)
				trace << num*1.0e12 << " pA" << std::endl;
			else if (num < 1.0e-6)
				trace << num*1.0e9 << " nA" << std::endl;
			else if (num < 1.0e-3)
				trace << num*1.0e6 << " uA" << std::endl;
			else if (num < 1.0e-0)
				trace << num*1.0e3 << " mA" << std::endl;
			else if (num > 1.0e3)
				trace << num*1.0e-3 << " kA" << std::endl;
			else
				trace << num << " A" << std::endl;
			break;
		case 50:
			trace << "Decrease the current or increase the number of carriers at the edge (while maintaining the same total current) by a factor of roughly " << std::setprecision(4) << std::scientific << num << std::endl;
			 trace << "This may be done by increasing the area of the device or the contact doping." << std::endl;
			 trace << "Simulation proceeding in numerically inconsistent manner. It may crash, seemingly freeze, or output weird results." << std::endl;
			break;
		case 51:
			trace << "\t\tTotal Charge in Device: " << std::setprecision(6) << std::scientific << num << std::endl;
			break;
		case 52:
			trace << "Monte Carlo assumption failed. The new rate did not decrease! " << num << std::endl;
			break;
		case 53:
			trace << "Difference in delta for setting current upon exit was: " << num << std::endl;
			break;
		case 54:
			trace << " changed from relative Ef of: " << num;
			break;
		case 55:
			trace << " eV to " << num << " eV." << std::endl;
			break;
		default:
			break;
	}

	trace.close();
	return (1);
}



int ProgressCheck(int check, bool clear)
{
	std::fstream trace;

	if(clear)
	{
		trace.open("trace.txt", std::fstream::out);	//open the file for writing blank
		trace.close();
	}


	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}
	switch(check)
	{
		case 0:
			trace << "Successfully moved carriers for iteration " << std::endl;
			break;
		case 1:
			trace << "Successfully updated the E-Field throughout the device." << std::endl;
			break;
		case 2:
			trace << "In MoveCarriers." << std::endl;
			break;
		case 3:
			trace << "Prepping to assign probabilities" << std::endl;
			break;
		case 4:
			trace << "Probabilities normalized. e- assigned to: ";
			break;
		case 5:
			trace << "Searching out new positions for probability" << std::endl;
			break;
		case 6:
			trace << "Calculating new probability for location" << std::endl;
			break;
		case 7:
			trace << "In Choose. ";
			break;
		case 8:
			trace << "std::left Choose." << std::endl;
			break;
		case 9:
			trace << "Populated EGrid with device gridpoint data" << std::endl;
			break;
		case 10:
			trace << "Populate EBlock with data" << std::endl;
			break;
		case 11:
			trace << "next EBlock" << std::endl;
			break;
		case 12:
			trace << "Box: ";
			break;
		case 13:
			trace << "Exact: ";
			break;
		case 14:
			trace << "In SolveSystem" << std::endl;
			break;
		case 15:
			trace << "The discovered solution" << std::endl;
			break;
		case 16:
			trace << "There are ZERO free electrons in the whole device. What kind of model is this?!?!" << std::endl;
			trace << "Program can't figure out the bands without this information! Involves ln(#/0 - 1)." << std::endl;
			trace << "Because the program might correct itself, the device is being treated as 1 electron is in the whole device." << std::endl;
			break;
		case 17:
			trace << "There is a higher particle density than the number of states... Problem." << std::endl;
			break;
		case 18:
			trace << "Invalid direction sent to Get_Width." << std::endl;
			break;
		case 19:
			trace << "Failed to load any solutions because none exist!" << std::endl;
			break;
		case 20:
			trace << "Task generated with negative carriers. Problem somewhere..." << std::endl;
			break;
		case 21:
			trace << "Attempting to commit an execute task." << std::endl;
			break;
		case 22:
			trace << "Failed to remove accept task in execute's link tasks" << std::endl;
			break;
		case 23:
			trace << "There is more than one linked task for an accept task!" << std::endl;
			break;
		case 24:
			trace << "A task was just pulled off the master task list that does not have 1 (accept) or 0 (execute) linked tasks!" << std::endl;
			break;
		case 25:
			trace << "A motion task was created that did not have defined code to follow. Supposedly every combination has code... debug it!" << std::endl;
			break;
		case 26:
			trace << "Invalid task made it on the master task list or the task has been corrupted." << std::endl;
			break;
		case 27:
			trace << "MasterTask tree returns no next tasks but still has tasks in its tree." << std::endl;
			break;
		case 28:
			trace << "Number of particles modified by something that's NaN!" << std::endl;
			break;
		case 29:
			trace << "Number of particles modified by NaN within reset contact!" << std::endl;
			break;
		case 30:
			trace << "The Apply task had NaN as the assigned carriers" << std::endl;
			break;
		case 31:
			trace << "Numfail in the MC section is NaN!" << std::endl;
			break;
		case 32:
			trace << "PercentUse is NaN or INF. Indicates OccMax = 0 when there is still a task to be done." << std::endl;
			break;
		case 33:
			trace << "Simulation does not currently support multidimensional meshes. Layer's must be points or lines." << std::endl;
			break;
		case 44:
			trace << "Dividing XSpace by zero!" << std::endl;
			break;
		case 45:
			trace << "Bad carrier motion type. Hole movement does not initiate in CB or the leading if statements are bad." << std::endl;
			break;
		case 46:
			trace << "Attempting invalid MC. Source and target point MUST be different." << std::endl;
			break;
		case 47:
			trace << "Mesh just generated a point outside the dimensions of the simulation zone." << std::endl;
			break;
		case 48:
			trace << "No SRH rate found between two states." << std::endl;
			break;
		case 49:
			trace << "Boundary condition does not have a point associated with it." << std::endl;
			break;
		case 50:
			trace << "Boundary condition refers to a point that is not associated with a contact" << std::endl;
			break;
		case 51:
			trace << "Invalid emission coefficients data sent. No valid source or target energy state" << std::endl;
			break;
		case 52:
			trace << "Invalid emission coefficients data sent. There is either no electrons or no place for the elctrons to go to" << std::endl;
			break;
		case 53:
			trace << "Non-finite value in binning the wave data" << std::endl;
			break;
		case 54:
			trace << "Failed to allocate memory when inserting into tree" << std::endl;
			break;
		case 55:
			trace << "Failed to allocate memory when adding into tree" << std::endl;
			break;
		case 56:
			trace << "Failed to allocate memory when inserting into tree root" << std::endl;
			break;
		case 57:
			trace << "Failed to allocate memory when adding into tree root" << std::endl;
			break;
		case 58:
			trace << "Did not delete all binned wave data" << std::endl;
			break;
		case 59:
			trace << "Null energy level passed into process rate list" << std::endl;
			break;
		case 60:
			trace << "An energy level exists with a null point pointer! How!?!" << std::endl;
			break;
		case 61:
			trace << "While processing a rate, the rate.source does not match the energy level passed into the list." << std::endl;
			break;
		case 62:
			trace << "While calculating the coefficient for light emissions, the algorithm became stuck in an infinite loop and exited prematurely with potentially incorrect values." << std::endl;
			break;
		case 63:
			trace << "Failed to allocate enough memory for steady state" << std::endl;
			break;
		case 64:
			trace << "Failed to solve system of equations for steady state" << std::endl;
			break;
		case 65:
			trace << "One environment for ";	//to be paired with ProgressDouble(48,time);
			break;
		case 66:
			trace << "Attempted to set starting conditions to a steady state answer, but that steady state answer does not exist" << std::endl;
			break;
		default:
			trace << std::endl;
			break;
	}


	trace.close();
	return(1);
}

int CheckPoissonEQTransfer(Model& mdl)
{
	std::fstream file;

	file.open("Poisson.dat", std::fstream::out);	//open the file for writing blank file
	
	if(file.is_open() == false)
	{
		std::cout << "Failed to open Poisson.dat" << std::endl;
		return(0);
	}

	//see what would happen for every single file if a carrier was transferred between each carrier

	double SrcAffectSrc;
	double TrgAffectSrc;
	double SrcAffectTrg;
	double TrgAffectTrg;
	double SrcWidth;
	double TrgWidth;
	double DeltaPsiSrc;
	double DeltaPsiTrg;
	double difW;
	double difPsi;

	file << "How much the involved points in the band diagram would change if a hole is transferred from the source point to the target point" << std::endl;
	file << std::setw(13) << std::right << "Direction";
	file << std::setw(15) << std::right << "Source_Point";
	file << std::setw(15) << std::right << "Src_Width(cm)";
	file << std::setw(15) << std::right << "Target_Point";
	file << std::setw(15) << std::right << "Trg_Width(cm)";
	file << std::setw(15) << std::right << "Trg-Src_Wdth";
	file << std::setw(15) << std::right << "Trg-Src_DPsi";
	file << std::setw(15) << std::right << "Delta_Psi_Src";
	file << std::setw(15) << std::right << "Delta_Psi_Trg";
	file << std::setw(15) << std::right << "SrcAffectSrc";
	file << std::setw(15) << std::right << "TrgAffectSrc";
	file << std::setw(15) << std::right << "SrcAffectTrg";
	file << std::setw(15) << std::right << "TrgAffectTrg" << std::endl;

	Point* TrgPt = NULL;
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		Point* SrcPt = &(pt->second);	
		SrcWidth = SrcPt->pxpy.x - SrcPt->mxmy.x;
/*
This is indeed a mirror copy of Px, except reversed, so unclutter the data.
		if(SrcPt->adj.coordMx != EXT)
		{
			TrgPt = &(mdl.gridp[SrcPt->adj.coordMx]);
			TrgWidth = TrgPt->pxpy.x - TrgPt->mxmy.x;
			difW = TrgWidth - SrcWidth;

			FindPoissonCoeff(SrcPt, TrgPt->adj.self, SrcAffectTrg);
			FindPoissonCoeff(SrcPt, SrcPt->adj.self, SrcAffectSrc);
			FindPoissonCoeff(TrgPt, TrgPt->adj.self, TrgAffectTrg);
			FindPoissonCoeff(TrgPt, SrcPt->adj.self, TrgAffectSrc);

			//this is the transfer of a hole... so SrcAffects is subtracted as it loses a positive charge
			//and TrgAffects is added as it gains a positive charge
			DeltaPsiSrc = TrgAffectSrc - SrcAffectSrc;
			DeltaPsiTrg = TrgAffectTrg - SrcAffectTrg;
			difPsi = DeltaPsiTrg - DeltaPsiSrc;

			file << std::setw(13) << std::right << "-X";
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << SrcPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << SrcWidth;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << TrgPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << TrgWidth;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difW;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difPsi;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectTrg << std::endl;
		}
*/		
		if(SrcPt->adj.adjPX)
		{
			TrgPt = SrcPt->adj.adjPX;
			TrgWidth = TrgPt->pxpy.x - TrgPt->mxmy.x;
			difW = TrgWidth - SrcWidth;

			FindPoissonCoeff(SrcPt, TrgPt->adj.self, SrcAffectTrg);
			FindPoissonCoeff(SrcPt, SrcPt->adj.self, SrcAffectSrc);
			FindPoissonCoeff(TrgPt, TrgPt->adj.self, TrgAffectTrg);
			FindPoissonCoeff(TrgPt, SrcPt->adj.self, TrgAffectSrc);

			//this is the transfer of a hole... so SrcAffects is subtracted as it loses a positive charge
			//and TrgAffects is added as it gains a positive charge
			DeltaPsiSrc = TrgAffectSrc - SrcAffectSrc;
			DeltaPsiTrg = TrgAffectTrg - SrcAffectTrg;
			difPsi = DeltaPsiTrg - DeltaPsiSrc;

			file << std::setw(13) << std::right << "+X";
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << SrcPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << SrcWidth;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << TrgPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << TrgWidth;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difW;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difPsi;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectTrg << std::endl;
		}


		////////// Y's
		SrcWidth = SrcPt->pxpy.y - SrcPt->mxmy.y;
		if(SrcPt->adj.adjMY)
		{
			TrgPt = SrcPt->adj.adjMY;
			TrgWidth = TrgPt->pxpy.y - TrgPt->mxmy.y;
			difW = TrgWidth - SrcWidth;

			FindPoissonCoeff(SrcPt, TrgPt->adj.self, SrcAffectTrg);
			FindPoissonCoeff(SrcPt, SrcPt->adj.self, SrcAffectSrc);
			FindPoissonCoeff(TrgPt, TrgPt->adj.self, TrgAffectTrg);
			FindPoissonCoeff(TrgPt, SrcPt->adj.self, TrgAffectSrc);

			//this is the transfer of a hole... so SrcAffects is subtracted as it loses a positive charge
			//and TrgAffects is added as it gains a positive charge
			DeltaPsiSrc = TrgAffectSrc - SrcAffectSrc;
			DeltaPsiTrg = TrgAffectTrg - SrcAffectTrg;
			difPsi = DeltaPsiTrg - DeltaPsiSrc;

			file << std::setw(13) << std::right << "-Y";
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << SrcPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << SrcWidth;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << TrgPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << TrgWidth;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difW;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difPsi;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectTrg << std::endl;
		}
		
		if(SrcPt->adj.adjPY)
		{
			TrgPt = SrcPt->adj.adjPY;
			TrgWidth = TrgPt->pxpy.y - TrgPt->mxmy.y;
			difW = TrgWidth - SrcWidth;

			FindPoissonCoeff(SrcPt, TrgPt->adj.self, SrcAffectTrg);
			FindPoissonCoeff(SrcPt, SrcPt->adj.self, SrcAffectSrc);
			FindPoissonCoeff(TrgPt, TrgPt->adj.self, TrgAffectTrg);
			FindPoissonCoeff(TrgPt, SrcPt->adj.self, TrgAffectSrc);

			//this is the transfer of a hole... so SrcAffects is subtracted as it loses a positive charge
			//and TrgAffects is added as it gains a positive charge
			DeltaPsiSrc = TrgAffectSrc - SrcAffectSrc;
			DeltaPsiTrg = TrgAffectTrg - SrcAffectTrg;
			difPsi = DeltaPsiTrg - DeltaPsiSrc;

			file << std::setw(13) << std::right << "+Y";
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << SrcPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << SrcWidth;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(0) << TrgPt->adj.self;
			file << std::setw(15) << std::fixed << std::right << std::setprecision(12) << TrgWidth;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difW;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << difPsi;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << DeltaPsiTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectSrc;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << SrcAffectTrg;
			file << std::setw(15) << std::scientific << std::right << std::setprecision(5) << TrgAffectTrg << std::endl;
		}

	}	//end for loop for X's




	file.close();
	return(0);
}


int DebugCLink(bool newfile, double value, bool newSrc, bool newIteration)
{
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << "CLinkDebug.dat"; 
	file.clear();
	if(newfile)
		file.open(filename.str().c_str(), std::fstream::out);
	else
		file.open(filename.str().c_str(), std::fstream::out | std::fstream::app);

	if(file.is_open() == false)
		return(-1);
	
	if(newIteration)
		file << "NEW ITERATION ";

	if(newSrc)
		file << "New Source Point: ";	//then this will be mobility_partition * mu * occ
	file << value << std::endl;

	file.close();

	return(0);
}

int CompareRho(Model& mdl, std::string fn)
{
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << fn << "_Charge.dat"; 
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	file << "Charge Density Data for: " << fn << std::endl;
	file << std::setw(18) << std::right << "Point" << " ";
	file << std::setw(18) << std::right << "Desired_Charge" << " ";
	file << std::setw(18) << std::right << "Actual_Charge" << " ";
	file << std::setw(18) << std::right << "Percent difference" << " ";
	file << std::setw(18) << std::right << "NumElec" << " ";
	file << std::setw(18) << std::right << "NumHoles" << " ";
	file << std::setw(18) << std::right << "ImpCharge" << " ";
	file << std::setw(18) << std::right << "ElecInDonor" << " ";
	file << std::setw(18) << std::right << "HoleInAccp" << " ";
	file << std::setw(18) << std::right << "ElecInCBtail" << " ";
	file << std::setw(18) << std::right << "HoleInVBTail" << " ";
	file << std::setw(18) << std::right << "Volume" << " ";
	file << std::setw(18) << std::right << "NetCarriers" << std::endl;

	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		CalculateRho(pt->second);	//make sure the rho is up to date.
		file << std::setw(18) << std::right << pt->second.adj.self << " ";
		file << std::setw(18) << std::right << mdl.poissonRho[pt->second.adj.self] << " ";
		file << std::setw(18) << std::right << pt->second.rho << " ";
		file << std::setw(17) << std::right << (mdl.poissonRho[pt->second.adj.self] - pt->second.rho) / mdl.poissonRho[pt->second.adj.self]*100.0 << "% ";
		file << std::setw(18) << std::right << pt->second.cb.GetNumParticles() << " ";
		file << std::setw(18) << std::right << pt->second.vb.GetNumParticles() << " ";
		file << std::setw(18) << std::right << pt->second.impdefcharge << " ";
		file << std::setw(18) << std::right << pt->second.elecindonor << " ";
		file << std::setw(18) << std::right << pt->second.holeinacceptor << " ";
		file << std::setw(18) << std::right << pt->second.elecTails << " ";
		file << std::setw(18) << std::right << pt->second.holeTails << " ";
		file << std::setw(18) << std::right << pt->second.vol << " ";
		file << std::setw(18) << std::right << mdl.poissonRho[pt->second.adj.self] * pt->second.vol * QI << std::endl;
	}

	file.close();

	return(0);
}

int WriteDeviceBand(Model& mdl, std::string fn)
{
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << fn << ".band"; 
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	file << "Band diagrm data for " << fn << std::endl;
	file << std::setw(15) << std::right << "PosX" << " ";
	file << std::setw(15) << std::right << "PosY" << " ";
	file << std::setw(15) << std::right << "Psi" << " ";
	file << std::setw(15) << std::right << "EcMin" << " ";
	file << std::setw(15) << std::right << "EvMin" << " ";
	file << std::setw(15) << std::right << "Ef" << " ";
	file << std::setw(15) << std::right << "Charge" << std::endl;

	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		file << std::setw(15) << std::right << pt->second.position.x << " ";
		file << std::setw(15) << std::right << pt->second.position.y << " ";
		file << std::setw(15) << std::right << pt->second.Psi << " ";
		file << std::setw(15) << std::right << pt->second.cb.bandmin << " ";
		file << std::setw(15) << std::right << pt->second.vb.bandmin << " ";
		file << std::setw(15) << std::right << pt->second.fermi << " ";
		file << std::setw(15) << std::right << mdl.poissonRho[pt->second.adj.self] << std::endl;
//		WritePtEStates(&pt->second, "DevEq");
	}

	file.close();

	return(0);
}

int PlotFermiEquil(std::string fn, double delCExpConstant, double expConstant, double negBeta,
	double expCoeff, double numHSource, double numETarget, double numESource, double numHTarget,
	double min, double max)
{
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << fn << "_FermiEquilib.dat"; 
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);
	file << std::setw(10) << std::right << "delC" << std::setw(18) << "exp" << std::setw(18) << "parab" << std::endl;

	double parab;
	double expvalue;
	for(double delC = min+0.01; delC < max; delC += 0.5)
	{
		expvalue = delC * delCExpConstant + expConstant;
		expvalue = expvalue * negBeta;
		expvalue = expCoeff * exp(expvalue);

		parab = (numHSource + delC) * (numETarget + delC);	//this will never be 0 due to max/min's and the catch for 0!
		parab = (numESource - delC) * (numHTarget - delC) / parab;
		file << std::setw(9) << std::right << std::fixed << delC << " ";
		file << std::setw(13) << std::right << std::scientific << std::setprecision(11) << expvalue << " ";
		file << std::setw(13) << std::right << std::scientific << std::setprecision(11) << parab << std::endl;

	}


	file.close();
	return(0);
}

int WriteBand(std::ofstream& file, Point* pt)
{
	
	double NStates = 0.0;
	double occ = 0.0;
	if(pt->cb.energies.size() > 0)
	{
		file << "CB" << std::endl;
		for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator cbS = pt->cb.energies.rbegin(); cbS != pt->cb.energies.rend(); ++cbS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin + cbS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin + cbS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin + cbS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << cbS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << cbS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << cbS->second.GetEndOccStates() << std::endl;
			NStates += cbS->second.GetNumstates();
			occ += cbS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	if(pt->CBTails.size() > 0)
	{
		file << std::endl << "CB Tails" << std::endl;
		//next we want the CB Tails, go forward in energy
		NStates = occ = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator esS = pt->CBTails.begin(); esS != pt->CBTails.end(); ++esS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetEndOccStates() << std::endl;
			NStates += esS->second.GetNumstates();
			occ += esS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	if(pt->donorlike.size() > 0)
	{
		file << std::endl << "Donors/Donorlike Defects" << std::endl;
		//and donors
		NStates = occ = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator esS = pt->donorlike.begin(); esS != pt->donorlike.end(); ++esS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->cb.bandmin - esS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetEndOccStates() << std::endl;
			NStates += esS->second.GetNumstates();
			occ += esS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	if(pt->acceptorlike.size() > 0)
	{
		file << std::endl << "Acceptors/Acceptorlike Defects" << std::endl;
		//next acceptors, start with large energy as that will be near the top of the file
		NStates = occ = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator esS = pt->acceptorlike.rbegin(); esS != pt->acceptorlike.rend(); ++esS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetEndOccStates() << std::endl;
			NStates += esS->second.GetNumstates();
			occ += esS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	if(pt->VBTails.size() > 0)
	{
		file << std::endl << "VBTails" << std::endl;
		NStates = occ = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator esS = pt->VBTails.rbegin(); esS != pt->VBTails.rend(); ++esS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin + esS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << esS->second.GetEndOccStates() << std::endl;
			NStates += esS->second.GetNumstates();
			occ += esS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;	
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	if(pt->vb.energies.size() > 0)
	{
		file << std::endl << "VB" << std::endl;
		NStates = occ = 0.0;
		//now write valence band, want to do min energies first as that is the higher energy ( top of band)
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbS = pt->vb.energies.begin(); vbS != pt->vb.energies.end(); ++vbS)
		{
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin - vbS->second.max;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin - vbS->second.min;
			file << std::setw(14) << std::right << std::setprecision(6) << std::fixed << pt->vb.bandmin - vbS->second.eUse;
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << vbS->second.GetNumstates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << vbS->second.GetBeginOccStates();
			file << std::setw(20) << std::right << std::setprecision(10) << std::scientific << vbS->second.GetEndOccStates() << std::endl;
			NStates += vbS->second.GetNumstates();
			occ += vbS->second.GetEndOccStates();
		}
		file << "Total States:    " << std::setw(20) << std::right << std::setprecision(10) << NStates << std::endl;
		file << "Occupied States: " << std::setw(20) << std::right << std::setprecision(10) << occ << std::endl;
	}
	return(0);
}

int WriteImpurityDist(Model& mdl, std::string fn)
{
	std::ofstream file;
	std::stringstream filename;
	double donors;
	double acceptors;

	filename.str("");
	filename.clear();
	filename << fn << "_Imp.dat"; 
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	file << "Impurity Distribution: " << filename.str() << std::endl;
	file << std::setw(10) << std::left << "X";
	file << std::setw(10) << std::left << "Y";
	file << std::setw(20) << std::left << "NumDonors";
	file << std::setw(20) << std::left << "NumAcceptors" << std::endl;

	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		donors = 0.0;
		acceptors = 0.0;	//reset for the tally
		
		for(std::map<MaxMin<double>, ELevel>::iterator es = pt->second.acceptorlike.begin(); es != pt->second.acceptorlike.end(); ++es)
			acceptors += es->second.GetNumstates();
		for(std::map<MaxMin<double>, ELevel>::iterator es = pt->second.donorlike.begin(); es != pt->second.donorlike.end(); ++es)
			donors += es->second.GetNumstates();

		//normalize donors and acceptors to cm^-3 to try and regain the function
		acceptors = acceptors * pt->second.voli;		//first, introduce a density
		donors = donors  * pt->second.voli;		//to the problem

		if(donors > 0.0 || acceptors > 0.0)
		{
			file << std::setw(10) << std::left << pt->second.position.x;
			file << std::setw(10) << std::left << pt->second.position.y;
			file << std::setw(20) << std::left << donors;
			file << std::setw(20) << std::left << acceptors << std::endl;
		}
	}

	file.close();
	return(0);
	
}

int WritePtEStates(Point* pt, std::string fn)
{
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << fn << "_Pt" << pt->adj.self << ".bs"; 
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	pt->ResetData();
	file << "Band Diagram Data: " << filename.str() << std::endl;
	///header points
	file << "CB Reference: " << pt->cb.bandmin << std::endl;
	file << "VB Reference: " << pt->vb.bandmin << std::endl;
	file << std::setw(14) << std::right << "ELevel-";
	file << std::setw(14) << std::right << "ELevel+";
	file << std::setw(14) << std::right << "E_Use";
	file << std::setw(20) << std::right << "NumStates";
	file << std::setw(20) << std::right << "Begin_OccStates";
	file << std::setw(20) << std::right << "End_OccStates" << std::endl;

	WriteBand(file, pt);


	file.close();

	return(0);
}

int WriteEnergyStates(Model& mdl, std::string fn)
{
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
		WritePtEStates(&(pt->second), fn);
	
	return(0);
}

int WriteFermiInfo(double Ef, double sum, double deriv, bool clear)
{
	std::ofstream debugF;

	if(!clear)
		debugF.open("debugFermi.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	else
		debugF.open("debugFermi.dat", std::fstream::out);

	if(debugF.is_open() == false)
			return(-1);

	if(clear)
	{
		debugF << std::setw(20) << "EF" << std::setw(30) << "Least Squares Sum" << std::setw(30) << "Calculated Derivative" << std::endl;
		debugF.close();
		return(0);
	}

	debugF << std::setw(20) << std::scientific << Ef << std::setw(30) << std::setprecision(20) << sum << std::setw(30) << std::setprecision(20) << deriv << std::endl;

	debugF.close();
	return(0);
}

int CheckMesh(Model& mdl, std::string filename)
{
	std::ofstream file;
	filename += ".mesh";

	file.open(filename.c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	file << "Test data for model: " << filename << std::endl;
	///header points
	file << std::setw(8) << std::left << "Point";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Min_X";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Pos_X";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Max_X";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Width_X";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Min_Y";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Pos_Y";
	file << std::setw(16) << std::scientific << std::setprecision(7) << std::left << "Max_Y";
	file << std::setw(8) << std::left << "AdjP";
	file << std::setw(8) << std::left << "AdjN";
	file << std::setw(8) << std::left << "Adj+X1";
	file << std::setw(8) << std::left << "Adj+X2";
	file << std::setw(8) << std::left << "Adj-X1";
	file << std::setw(8) << std::left << "Adj-X2";
	file << std::setw(8) << std::left << "Adj+Y1";
	file << std::setw(8) << std::left << "Adj+Y2";
	file << std::setw(8) << std::left << "Adj-Y1";
	file << std::setw(8) << std::left << "Adj-Y2";

	file << std::endl;
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		file << std::setw(8) << std::left << pt->second.adj.self;
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.mxmy.x << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.position.x << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.pxpy.x << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.pxpy.x - pt->second.mxmy.x << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.mxmy.y << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.position.y << " ";
		file << std::setw(14) << std::left << std::setprecision(8) << pt->second.pxpy.y << " ";

		if(pt->second.adj.adjP)
			file << std::setw(8) << std::left << pt->second.adj.adjP->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjN)
			file << std::setw(8) << std::left << pt->second.adj.adjN->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjPX)
			file << std::setw(8) << std::left << pt->second.adj.adjPX->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjPX2)
			file << std::setw(8) << std::left << pt->second.adj.adjPX2->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjMX)
			file << std::setw(8) << std::left << pt->second.adj.adjMX->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjMX2)
			file << std::setw(8) << std::left << pt->second.adj.adjMX2->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjPY)
			file << std::setw(8) << std::left << pt->second.adj.adjPY->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjPY2)
			file << std::setw(8) << std::left << pt->second.adj.adjPY2->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjMY)
			file << std::setw(8) << std::left << pt->second.adj.adjMY->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		if(pt->second.adj.adjMY2)
			file << std::setw(8) << std::left << pt->second.adj.adjMY2->adj.self;
		else
			file << std::setw(8) << std::left << "-1";

		file << std::endl;
	}

	file.close();
	return(0);
}




int OutputRatesPoint(Point& pt, Simulation* sim)	//note, this outputs a TON of data
{
	if (sim->outputs.OutputAllIndivRates == false)	//we don't want this much output!
		return(0);

	std::ofstream file;
	std::stringstream filename;
	std::vector<ELevel*> States;	//a std::vector of all the states
	
	int numstates = pt.cb.energies.size() + pt.vb.energies.size() + pt.acceptorlike.size() + pt.donorlike.size() + pt.VBTails.size() + pt.CBTails.size();
	if(numstates <= 0)
		return(0);	//there are no states to report any data on

	double cbmin = pt.cb.bandmin;
	double vbmin = pt.vb.bandmin;
	double voli = pt.voli;
	double ktI = KBI / pt.temp;

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_Rates_";

	if(sim->curiter < 1e7)
		filename << "0";
	if(sim->curiter < 1e6)
		filename << "0";
	if(sim->curiter < 1e5)
		filename << "0";
	if(sim->curiter < 1e4)
		filename << "0";
	if(sim->curiter < 1e3)
		filename << "0";
	if(sim->curiter < 1e2)
		filename << "0";
	if(sim->curiter < 1e1)
		filename << "0";

	filename << sim->curiter << "Pt" << pt.adj.self << ".pt";
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	file << "Volume: " << std::scientific << std::setprecision(5) << pt.vol << " cm^-3" << std::endl;
	if(pt.MatProps)
		file << "Material: " << pt.MatProps->name << std::endl;
	
	States.reserve(numstates);
	
	for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator it = pt.cb.energies.rbegin(); it != pt.cb.energies.rend(); ++it)
		States.push_back(&(it->second));
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.CBTails.begin(); it != pt.CBTails.end(); ++it)
		States.push_back(&(it->second));
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.donorlike.begin(); it != pt.donorlike.end(); ++it)
		States.push_back(&(it->second));
	for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator it = pt.acceptorlike.rbegin(); it != pt.acceptorlike.rend(); ++it)
		States.push_back(&(it->second));
	for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator it = pt.VBTails.rbegin(); it != pt.VBTails.rend(); ++it)
		States.push_back(&(it->second));
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = pt.vb.energies.begin(); it != pt.vb.energies.end(); ++it)
		States.push_back(&(it->second));
	
	file << "Ref: ";
	file << std::right << std::setw(11) << "Unique_ID";
	file << std::right << std::setw(9) << "Type";
	file << std::right << std::setw(14) << "Energy_Used";
	file << std::right << std::setw(12) << "Energy_Min";
	file << std::right << std::setw(12) << "Energy_Max";
	file << std::right << std::setw(15) << "Number_States";
	file << std::right << std::setw(15) << "Occup_States";
	file << std::right << std::setw(15) << "Absolute_Ef" << std::endl;

	double n=0.0;
	double p=0.0;
	double NcEquiv = 0.0;	//n ~ Density(Nc) * exp[(Ef-Ec)/kT] ~ so summing up all the deviations from Ec
	double NvEquiv = 0.0;
	double ND = 0.0;
	double NA = 0.0;

	for(int i=0; i<numstates; i++)
	{
		file << std::setw(3) << std::fixed << std::right << i << ": ";
		file << std::setw(11) << std::fixed << std::right << States[i]->uniqueID;
		if(States[i]->imp)
		{
			if(States[i]->imp->isVBReference())
			{
				file << std::setw(9) << std::right << "Acceptor";
				file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << States[i]->eUse + vbmin;
				file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << States[i]->min + vbmin;
				file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << States[i]->max + vbmin;
				
				
				NA += States[i]->GetNumstates();
			}
			else
			{
				file << std::setw(9) << std::right << "Donor";
				file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << cbmin - States[i]->eUse;
				file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin - States[i]->min;
				file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin - States[i]->max;
				ND += States[i]->GetNumstates();
			}
		}
		else if(States[i]->carriertype == ELECTRON && States[i]->bandtail == false)
		{
			file << std::setw(9) << std::right << "CBand";
			file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << cbmin + States[i]->eUse;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin + States[i]->min;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin + States[i]->max;
			n+= States[i]->GetBeginOccStates();
			NcEquiv += States[i]->GetNumstates() * exp(-States[i]->eUse * ktI);
		}
		else if(States[i]->carriertype == HOLE && States[i]->bandtail == false)
		{
			file << std::setw(9) << std::right << "VBand";
			file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << vbmin - States[i]->eUse;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << vbmin - States[i]->min;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << vbmin - States[i]->max;
			p += States[i]->GetBeginOccStates();
			NvEquiv += States[i]->GetNumstates() * exp(-States[i]->eUse * ktI);
		}
		else if(States[i]->carriertype == HOLE && States[i]->bandtail == true)
		{
			file << std::setw(9) << std::right << "VBTail";
			file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << States[i]->eUse + vbmin;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << States[i]->min + vbmin;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << States[i]->max + vbmin;
		}
		else
		{
			file << std::setw(9) << std::right << "CBTail";
			file << std::setw(14) << std::scientific << std::setprecision(5) << std::right << cbmin - States[i]->eUse;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin - States[i]->min;
			file << std::setw(12) << std::scientific << std::setprecision(3) << std::right << cbmin - States[i]->max;
		}
		file << std::setw(15) << std::scientific << std::setprecision(6) << std::right << States[i]->GetNumstates();
		file << std::setw(15) << std::scientific << std::setprecision(6) << std::right << States[i]->GetBeginOccStates();
		States[i]->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
		file << std::setw(15) << std::scientific << std::setprecision(6) << std::right << States[i]->AbsFermiEnergy << std::endl;
	}
	
	n=n*voli;
	p=p*voli;
	NA = NA * voli;
	ND = ND * voli;
	NcEquiv = NcEquiv * voli;
	NvEquiv = NvEquiv * voli;
	double ncActual = HBAR * HBAR * QCHARGE * PI;
	double nvActual = pow(2.0 * MELECTRON * pt.MatProps->EFMhDens * KB * pt.temp / ncActual, 1.5) * 0.25  * 1.0e-6;
	ncActual = pow(2.0 * MELECTRON * pt.MatProps->EFMeDens  * KB * pt.temp / ncActual, 1.5) * 0.25 * 1.0e-6;
	file << std::endl;
	file << "Nd: " << std::scientific << std::setprecision(4) << ND << std::endl;
	file << "Na: " << std::scientific << std::setprecision(4) << NA << std::endl;
	file << "N:  " << std::scientific << std::setprecision(4) << n << std::endl;
	file << "P:  " << std::scientific << std::setprecision(4) << p << std::endl;
	file << "NcEquiv: " << std::scientific << std::setprecision(4) << NcEquiv << " vs " << std::scientific << std::setprecision(4) << ncActual << std::endl;
	file << "NvEquiv: " << std::scientific << std::setprecision(4) << NvEquiv << " vs " << std::scientific << std::setprecision(4) << nvActual << std::endl;

	file << std::setw(20) << std::left << "Thermal Generation: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.genth << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Th. Recombination: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.recth << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "SRH 1: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.srhR1 << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "SRH 2: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.srhR2 << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "SRH 3: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.srhR3 << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "SRH 4: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.srhR4 << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Scattering: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.scatterR << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "JnX: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.Jn.x * pt.areai.x * QCHARGE << " C cm^-2 s^-1" << std::endl;
	file << std::setw(20) << std::left << "JpX: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.Jp.x * pt.areai.x * QCHARGE << " C cm^-2 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Absorb Gen: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightAbsorbGen << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Absorb SRH: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightAbsorbSRH << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Absorb Def: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightAbsorbDef << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Absorb Scat: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightAbsorbScat << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Emit Rec: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightEmitRec << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Emit SRH: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightEmitSRH << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Emit Def: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightEmitDef << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Emit Scat: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightEmitScat << " cm^-3 s^-1" << std::endl;
	file << std::setw(20) << std::left << "Power Entry: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.LightPowerEntry << " mW" << std::endl;
	if (pt.contactData)
	{
		file << std::setw(20) << std::left << "Contact JnX: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.contactData->JnOutput.x * pt.areai.x * QCHARGE << " C cm^-2 s^-1" << std::endl;
		file << std::setw(20) << std::left << "Contact JpX: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << pt.contactData->JpOutput.x * pt.areai.x * QCHARGE << " C cm^-2 s^-1" << std::endl;
	}

	

	
	
	file << std::endl;	//just a break before all the rate data
	file << "Source Energy State Number --- Energy Level" << std::endl;
	file << "Rate Type --- Target energy Level --- Rate Out (carriers/sec) --- Net Rate(car/sec) --- Net Rate(normal units)" << std::endl;
	file << "Total R --- Net Rate Sums (car/sec) --- Net Rate Sums (normal units)" << std::endl;


	for(int i=0; i<numstates; i++)
	{
		double goodUnits = 0.0;
		ELevelDebugOut outputs;
		outputs.genth[0] = 0.0;
		outputs.genlight[0] = 0.0;
		outputs.deflight[0] = 0.0;
		outputs.condlight[0] = 0.0;
		outputs.recth[0] = 0.0;
		outputs.R1[0] = 0.0;
		outputs.R2[0] = 0.0;
		outputs.R3[0] = 0.0;
		outputs.R4[0] =0.0;
		outputs.scatter[0] = 0.0;
		outputs.JnP[0].x = outputs.JnP[0].y = outputs.JnP[0].z = 0.0;
		outputs.JnN[0].x = outputs.JnN[0].y = outputs.JnN[0].z = 0.0;
		outputs.JpP[0].x = outputs.JpP[0].y = outputs.JpP[0].z = 0.0;
		outputs.JpN[0].x = outputs.JpN[0].y = outputs.JpN[0].z = 0.0;
		outputs.Lgen[0] = outputs.Lgen[1] = 0.0;
		outputs.Lscat[0] = outputs.Lscat[1] = 0.0;
		outputs.Lsrh[0] = outputs.Lsrh[1] = 0.0;
		outputs.Ldef[0] = outputs.Ldef[1] = 0.0;
		outputs.Erec[0] = outputs.Erec[1] = 0.0;
		outputs.Escat[0] = outputs.Escat[1] = 0.0;
		outputs.Esrh[0] = outputs.Esrh[1] = 0.0;
		outputs.Edef[0] = outputs.Edef[1] = 0.0;
		outputs.LightEmit[0] = outputs.LightEmit[1] = 0.0;
		outputs.contact_extN[0] = outputs.contact_extN[1] = 0.0;
		outputs.contact_intN[0] = outputs.contact_intN[1] = 0.0;
		outputs.contact_linkN[0] = outputs.contact_linkN[1] = 0.0;
		outputs.contact_intextN[0] = outputs.contact_intextN[1] = 0.0;
		outputs.contact_extP[0] = outputs.contact_extP[1] = 0.0;
		outputs.contact_intP[0] = outputs.contact_intP[1] = 0.0;
		outputs.contact_linkP[0] = outputs.contact_linkP[1] = 0.0;
		outputs.contact_intextP[0] = outputs.contact_intextP[1] = 0.0;

		outputs.tunnel[0] = 0.0;
		outputs.genth[1] = 0.0;
		outputs.genlight[1] = 0.0;
		outputs.deflight[1] = 0.0;
		outputs.condlight[1] = 0.0;
		outputs.recth[1] = 0.0;
		outputs.R1[1] = 0.0;
		outputs.R2[1] = 0.0;
		outputs.R3[1] = 0.0;
		outputs.R4[1] =0.0;
		outputs.scatter[1] = 0.0;
		outputs.JnP[1].x = outputs.JnP[1].y = outputs.JnP[1].z = 0.0;
		outputs.JnN[1].x = outputs.JnN[1].y = outputs.JnN[1].z = 0.0;
		outputs.JpP[1].x = outputs.JpP[1].y = outputs.JpP[1].z = 0.0;
		outputs.JpN[1].x = outputs.JpN[1].y = outputs.JpN[1].z = 0.0;
		outputs.tunnel[1] = 0.0;
		file << std::endl;
		file << "Energy level: " << i << "  " << States[i]->CalcAbsEUseEnergy() << "eV" << std::endl;
		XSpace tPt;	//target point position
		XSpace change;	//change to get general debug direction
		XSpace absChange;	//absolute change to know whether this should be x,y, or z.
		for(std::vector<ELevelRate>::iterator rt = States[i]->Rates.begin(); rt != States[i]->Rates.end(); ++rt)
		{
			switch(rt->type)
			{
				case RATE_GENERATION:
					file << std::left << std::setw(8) << "ThGen ";
					goodUnits = voli;
					outputs.genth[0] += rt->RateType;
					outputs.genth[1] += rt->netrate;
					break;
				case RATE_RECOMBINATION:
					file << std::left << std::setw(8) << "ThRec ";
					goodUnits = voli;
					outputs.recth[0] += rt->RateType;
					outputs.recth[1] += rt->netrate;
					break;
				case RATE_SRH1:
					file << std::left << std::setw(8) << "SRH1 ";
					goodUnits = voli;
					outputs.R1[0] += rt->RateType;
					outputs.R1[1] += rt->netrate;
					break;
				case RATE_SRH2:
					file << std::left << std::setw(8) << "SRH2 ";
					goodUnits = voli;
					outputs.R2[0] += rt->RateType;
					outputs.R2[1] += rt->netrate;
					break;
				case RATE_SRH3:
					file << std::left << std::setw(8) << "SRH3 ";
					goodUnits = voli;
					outputs.R3[0] += rt->RateType;
					outputs.R3[1] += rt->netrate;
					break;
				case RATE_SRH4:
					file << std::left << std::setw(8) << "SRH4 ";
					goodUnits = voli;
					outputs.R4[0] += rt->RateType;
					outputs.R4[1] += rt->netrate;
					break;
				case RATE_SCATTER:
					file << std::left << std::setw(8) << "Scat ";
					goodUnits = voli;
					outputs.scatter[0] += rt->RateType;
					outputs.scatter[1] += rt->netrate;
					break;
				case RATE_DRIFTDIFFUSE:
					tPt = rt->target->getPoint().position;
					change = tPt - pt.position;
					absChange.x = fabs(change.x);
					absChange.y = fabs(change.y);
					absChange.z = fabs(change.z);

					if(States[i]->carriertype == ELECTRON)
					{
						if(absChange.x >= absChange.y && absChange.x >= absChange.z)
						{
							if(change.x >= 0)
							{
								file << std::left << std::setw(8) << "JnPosX ";
								goodUnits = pt.areai.x * QCHARGE;
								outputs.JnP[0].x += rt->RateType;
								outputs.JnP[1].x += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JnNegX ";
								goodUnits = pt.areai.x * QCHARGE;
								outputs.JnN[0].x += rt->RateType;
								outputs.JnN[1].x += rt->netrate;
							}
						}
						else if(absChange.y >= absChange.x && absChange.y >= absChange.z)
						{
							if(change.y >= 0)
							{
								file << std::left << std::setw(8) << "JnPosY ";
								goodUnits = pt.areai.y * QCHARGE;
								outputs.JnP[0].y += rt->RateType;
								outputs.JnP[1].y += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JnNegY ";
								goodUnits = pt.areai.y * QCHARGE;
								outputs.JnN[0].y += rt->RateType;
								outputs.JnN[1].y += rt->netrate;
							}
						}
						else
						{
							if(change.z >= 0)
							{
								file << std::left << std::setw(8) << "JnPosZ ";
								goodUnits = pt.areai.z * QCHARGE;
								outputs.JnP[0].z += rt->RateType;
								outputs.JnP[1].z += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JnNegZ ";
								goodUnits = pt.areai.z * QCHARGE;
								outputs.JnN[0].z += rt->RateType;
								outputs.JnN[1].z += rt->netrate;
							}
						}
					}
					else
					{
						if(absChange.x >= absChange.y && absChange.x >= absChange.z)
						{
							if(change.x >= 0)
							{
								file << std::left << std::setw(8) << "JpPosX ";
								goodUnits = pt.areai.x * QCHARGE;
								outputs.JpP[0].x += rt->RateType;
								outputs.JpP[1].x += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JpNegX ";
								goodUnits = pt.areai.x * QCHARGE;
								outputs.JpN[0].x += rt->RateType;
								outputs.JpN[1].x += rt->netrate;
							}
						}
						else if(absChange.y >= absChange.x && absChange.y >= absChange.z)
						{
							if(change.y >= 0)
							{
								file << std::left << std::setw(8) << "JpPosY ";
								goodUnits = pt.areai.y * QCHARGE;
								outputs.JpP[0].y += rt->RateType;
								outputs.JpP[1].y += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JpNegY ";
								goodUnits = pt.areai.y * QCHARGE;
								outputs.JpN[0].y += rt->RateType;
								outputs.JpN[1].y += rt->netrate;
							}
						}
						else
						{
							if(change.z >= 0)
							{
								file << std::left << std::setw(8) << "JpPosZ ";
								goodUnits = pt.areai.z * QCHARGE;
								outputs.JpP[0].z += rt->RateType;
								outputs.JpP[1].z += rt->netrate;
							}
							else
							{
								file << std::left << std::setw(8) << "JpNegZ ";
								goodUnits = pt.areai.z * QCHARGE;
								outputs.JpN[0].z += rt->RateType;
								outputs.JpN[1].z += rt->netrate;
							}
						}
					}
					break;
				case RATE_TUNNEL:
					file << std::left << std::setw(8) << "Tunnel ";
					goodUnits = pt.areai.x * QCHARGE;
					outputs.tunnel[0] += rt->RateType;
					outputs.tunnel[1] += rt->netrate;
					break;
				case RATE_CONTACT_CURRENT_EXT:
					goodUnits = pt.areai.x * QCHARGE;
					if (States[i]->carriertype == ELECTRON)
					{
						file << std::left << std::setw(8) << "ctcNext ";
						outputs.contact_extN[0] += rt->RateType;
						outputs.contact_extN[1] += rt->netrate;
					}
					else
					{
						file << std::left << std::setw(8) << "ctcPext ";
						outputs.contact_extP[0] += rt->RateType;
						outputs.contact_extP[1] += rt->netrate;
					}
					break;
//				case RATE_CONTACT_CURRENT_INT:
//					break;
				case RATE_CONTACT_CURRENT_LINK:
					
					goodUnits = pt.areai.x * QCHARGE;
					if(States[i]->carriertype == ELECTRON)
					{
						file << std::left << std::setw(8) << "ctcNlnk ";
						outputs.contact_linkN[0] += rt->RateType;
						outputs.contact_linkN[1] += rt->netrate;
					}
					else
					{
						file << std::left << std::setw(8) << "ctcPlnk ";
						outputs.contact_linkP[0] += rt->RateType;
						outputs.contact_linkP[1] += rt->netrate;
					}
					break;
//				case RATE_CONTACT_CURRENT_INTEXT:
//					break;
				default: //the following don't equal the value exact because there are flags in lower bits
					if (rt->type & RATE_CONTACT_CURRENT_INT)
					{
						goodUnits = pt.areai.x * QCHARGE;
						if (States[i]->carriertype == ELECTRON)
						{
							file << std::left << std::setw(8) << "ctcNint ";
							outputs.contact_intN[0] += rt->RateType;
							outputs.contact_intN[1] += rt->netrate;
						}
						else
						{
							file << std::left << std::setw(8) << "ctcPint ";
							outputs.contact_intP[0] += rt->RateType;
							outputs.contact_intP[1] += rt->netrate;
						}
					}
					else if (rt->type & RATE_CONTACT_CURRENT_INTEXT)
					{
						goodUnits = pt.areai.x * QCHARGE;
						if (States[i]->carriertype == ELECTRON)
						{
							file << std::left << std::setw(8) << "ctcNie ";
							outputs.contact_intextN[0] += rt->RateType;
							outputs.contact_intextN[1] += rt->netrate;
						}
						else
						{
							file << std::left << std::setw(8) << "ctcPie ";
							outputs.contact_intextP[0] += rt->RateType;
							outputs.contact_intextP[1] += rt->netrate;
						}
					}
					else
					{
						file << "Invalid Rate Type" << std::endl;
						goodUnits = 0.0;
						continue;
					}
					break;
			}
			if(rt->target)
				file << std::scientific << std::setprecision(4) << std::setw(13) << std::right << rt->target->CalcAbsEUseEnergy() << " ";
			else if(rt->source)
				file << std::scientific << std::setprecision(4) << std::setw(13) << std::right << rt->source->CalcAbsEUseEnergy() << " ";
			else
				file << std::setw(13) << "Inval_State" << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->RateType << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->netrate << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->netrate * goodUnits << std::endl;
		}

		for(std::vector<ELevelRate>::iterator rt = States[i]->OpticalRates.begin(); rt != States[i]->OpticalRates.end(); ++rt)
		{
			switch(rt->type)
			{
				case RATE_LIGHTGEN:
					file << std::left << std::setw(8) << "LGen ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				case RATE_LIGHTSCAT:
					file << std::left << std::setw(8) << "LScat ";
					goodUnits = voli;
					outputs.condlight[0] += rt->RateType;
					outputs.condlight[1] += rt->netrate;
					break;
				case RATE_LIGHTDEF:
					file << std::left << std::setw(8) << "LDef ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				case RATE_LIGHTSRH:
					file << std::left << std::setw(8) << "Lsrh ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				case RATE_STIMEMISREC:
					file << std::left << std::setw(8) << "SEGen ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				case RATE_STIMEMISSCAT:
					file << std::left << std::setw(8) << "SEScat ";
					goodUnits = voli;
					outputs.condlight[0] += rt->RateType;
					outputs.condlight[1] += rt->netrate;
					break;
				case RATE_STIMEMISDEF:
					file << std::left << std::setw(8) << "SEDef ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				case RATE_STIMEMISSRH:
					file << std::left << std::setw(8) << "SEsrh ";
					goodUnits = voli;
					outputs.genlight[0] += rt->RateType;
					outputs.genlight[1] += rt->netrate;
					break;
				default:
					file << "Invalid Rate Type" << std::endl;
					goodUnits = 0.0;
					continue;
					break;
			}
			file << std::scientific << std::setprecision(4) << std::setw(13) << std::right << rt->target->CalcAbsEUseEnergy() << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->RateType << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->netrate << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << rt->netrate * goodUnits << std::endl;
		}
		file << "----" << std::endl;
		file << std::setw(20) << std::left << "Thermal Generation: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.genth[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.genth[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.genth[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Th. Recombination: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.recth[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.recth[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.recth[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SRH 1: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R1[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R1[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R1[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SRH 2: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R2[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R2[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R2[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SRH 3: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R3[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R3[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R3[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SRH 4: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R4[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R4[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.R4[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Scattering: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.scatter[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.scatter[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.scatter[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Opt. Generation: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lgen[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lgen[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lgen[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SRH Absorption: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lsrh[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lsrh[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lsrh[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Scat. Abosprtion: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lscat[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lscat[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Lscat[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Def/Imp Absorption: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Ldef[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Ldef[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Ldef[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Stim. Emis. Recomb: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Erec[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Erec[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Erec[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SE SRH: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Esrh[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Esrh[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Esrh[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SE Defect/Imp: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Edef[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Edef[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Edef[1] * voli << std::endl;

		file << std::setw(20) << std::left << "SE Scattering: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Escat[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Escat[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.Escat[1] * voli << std::endl;

		file << std::setw(20) << std::left << "Conact N Int: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intN[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intN[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intN[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact P Int: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intP[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intP[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intP[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact N Ext: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extN[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extN[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extN[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact P Ext: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extP[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extP[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_extP[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact N Link: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkN[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkN[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkN[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact P Link: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkP[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkP[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_linkP[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact N IntExt: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextN[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextN[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextN[1] * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "Conact P IntExt: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextP[0] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextP[1] << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.contact_intextP[1] * pt.areai.x * QCHARGE << std::endl;

		
		file << std::setw(20) << std::left << "JnxN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[0].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].x * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "JnxP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[0].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].x * pt.areai.x * QCHARGE << std::endl;

		if(sim->mdlData->NDim > 1)
		{
			file << std::setw(20) << std::left << "JnyN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[0].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].y * pt.areai.y * QCHARGE << std::endl;

			file << std::setw(20) << std::left << "JnyP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[0].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].y * pt.areai.y * QCHARGE << std::endl;
		}

		if(sim->mdlData->NDim > 2)
		{
			file << std::setw(20) << std::left << "JnzN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[0].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnN[1].z * pt.areai.x * QCHARGE << std::endl;

			file << std::setw(20) << std::left << "JnzP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[0].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JnP[1].z * pt.areai.z * QCHARGE << std::endl;
		}
		file << std::setw(20) << std::left << "JpxN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[0].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].x * pt.areai.x * QCHARGE << std::endl;

		file << std::setw(20) << std::left << "JpxP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[0].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].x << " ";
		file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].x * pt.areai.x * QCHARGE << std::endl;
		
		if(sim->mdlData->NDim > 1)
		{
			file << std::setw(20) << std::left << "JpyN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[0].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].y * pt.areai.y * QCHARGE << std::endl;

			file << std::setw(20) << std::left << "JpyP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[0].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].y << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].y * pt.areai.y * QCHARGE << std::endl;
		}
		if(sim->mdlData->NDim > 2)
		{
			file << std::setw(20) << std::left << "JpzN: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[0].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpN[1].z * pt.areai.z * QCHARGE << std::endl;

			file << std::setw(20) << std::left << "JpzP: " << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[0].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].z << " ";
			file << std::scientific << std::setprecision(3) << std::setw(12) << std::right << outputs.JpP[1].z * pt.areai.z * QCHARGE << std::endl;
		}
		file << "-------------" << std::endl;
	}




	file.close();
	return(0);
}


int DebugSSZeroEffect(bool clear, bool newline, long int var)
{
	std::fstream file;

	if(clear)
	{
		file.open("sszero.dat", std::fstream::out);	//open the file for writing blank
		file.close();
		return(0);
	}

	file.open("sszero.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open sszero.dat" << std::endl;
		return(0);
	}

	if(newline)
		file << std::endl;
	else
		file << var << ", ";

	file.close();
	return(0);

}

int DebugSteadyState(ELevel* eStateLimitRate, ELevel* eStateLimitOcc, double rate, double delocc, bool clear)
{
	std::fstream file;

	if(clear)
	{
		file.open("ssdebug.dat", std::fstream::out);	//open the file for writing blank
		file.close();
		return(0);
	}

	file.open("ssdebug.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open ssdebug.dat" << std::endl;
		return(0);
	}

	if(eStateLimitRate)
		file << "Rate percent state " << std::setw(7) << std::left << eStateLimitRate->uniqueID << 
		"with occupancy " << std::scientific << std::setw(15) << std::setprecision(4) << std::left << eStateLimitRate->GetBeginOccStates() <<
		"eUse " << std::setw(8) << std::fixed << std::setprecision(5) << std::left << eStateLimitRate->eUse << 
		"and a rate of " << std::setw(13) << std::setprecision(4) << std::scientific << std::left << rate <<
		" for a percent of " << std::setw(13) << std::setprecision(4) << std::scientific << std::left << rate/eStateLimitRate->GetBeginOccStates() << std::endl;

	if(eStateLimitOcc)
		file << "dOcc percent state " << std::setw(7) << std::left << eStateLimitOcc->uniqueID << 
		"with occupancy " << std::scientific << std::setw(15) << std::setprecision(4) << std::left << eStateLimitOcc->GetBeginOccStates() <<
		"eUse " << std::setw(8) << std::fixed << std::setprecision(5) << std::left << eStateLimitOcc->eUse << 
		"and change of " << std::setw(13) << std::setprecision(4) << std::scientific << std::left << delocc <<
		" for a percent of " << std::setw(13) << std::setprecision(4) << std::scientific << std::left << delocc / eStateLimitOcc->GetBeginOccStates() << std::endl;

	file << std::endl;

	file.close();
	return(0);
}

int DebugSteadyStatePlot(unsigned int iteration, double rates[], double deloccs[], SteadyState_SAAPD& data)
{
	//need to make a plot of energy use, occupancies, change in occupancy %, rate %
	std::fstream file;
	std::string fname = "ssdebug"+std::to_string(iteration)+".dat";
	file.open(fname, std::fstream::out);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open ssdebug.dat" << std::endl;
		return(0);
	}
	unsigned long int sz = data.EStates.size();
	file << std::setw(15) << std::scientific << std::setprecision(7) << std::left << "Energy_Use";
	file << std::setw(15) << std::scientific << std::setprecision(7) << std::left << "Occupancy";
	file << std::setw(15) << std::scientific << std::setprecision(7) << std::left << "dOcc/occ";
	file << std::setw(15) << std::scientific << std::setprecision(7) << std::left << "Rate/occ" << std::endl;
	for(unsigned long int i=0; i<sz; ++i)
	{
		double occ = data.EStates[i].ptr->GetBeginOccStates();
		file << std::setw(15) << std::setprecision(5) << std::left << std::fixed << data.EStates[i].ptr->eUse;
		file << std::setw(15) << std::setprecision(5) << std::left << std::scientific << occ;
		file << std::setw(15) << std::setprecision(5) << std::left << std::scientific << deloccs[i]/occ;
		file << std::setw(15) << std::setprecision(5) << std::left << std::scientific << rates[i]/occ << std::endl;
	}

	file.close();
	return(0);
}

int PlotDRate_C(Model& mdl, ModelDescribe& mdesc, ELevel* eUse)
{
	if(eUse == NULL)
	{
		//pick a random energy state
		int ptI = rand() % mdl.gridp.size();
		Point* pt = &mdl.gridp.find(ptI)->second;// &(mdl.gridp[ptI]);
		int numStates = pt->cb.energies.size() + pt->vb.energies.size() + pt->CBTails.size() + pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size();
		unsigned int stI = rand() % numStates;

		if(stI < pt->cb.energies.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->cb.energies.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		stI -= pt->cb.energies.size();	//these will eventually push it to a very large number
		//and I severely doubt a user will create a finite element with more than 100, let alone thousands/billions depending on comiler implementation
		if(stI < pt->vb.energies.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->vb.energies.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		stI -= pt->vb.energies.size();
		if(stI < pt->CBTails.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->CBTails.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		stI -= pt->CBTails.size();
		if(stI < pt->VBTails.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->VBTails.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		stI -= pt->VBTails.size();
		if(stI < pt->acceptorlike.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->acceptorlike.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		stI -= pt->acceptorlike.size();
		if(stI < pt->donorlike.size())
		{
			std::multimap<MaxMin<double>,ELevel>::iterator whichE = pt->donorlike.begin();
			for(unsigned int i=0; i<stI; ++i)
				whichE++;

			eUse = &(whichE->second);
		}
		if(eUse == NULL)	//this just royally failed
			return(0);
	}

	std::vector<double> nRateNeg;
	std::vector<double> derivNeg;
	std::vector<double> occupancyNeg;
	std::vector<double> nRatePos;
	std::vector<double> derivPos;
	std::vector<double> occupancyPos;
	

	double fixedOpticalRate = 0.0;
	for(std::vector<ELevelRate>::iterator opt = eUse->OpticalRates.begin(); opt != eUse->OpticalRates.end(); ++opt)
		fixedOpticalRate += (opt->RateIn - opt->RateOut);

	double nStates = eUse->GetNumstates();
	double netRate, targetOcc;
	double maxOcc = nStates;
	double minOcc = 0.0;

	PosNegSum nRate;
	PosNegSum nDeriv;

	//get outputs for eLevel derivative that for the most part don't matter
	double sourceDeriv, targetDeriv;
	SS_CurrentDerivFactors derivFactor;	//

	std::vector<ELevel*> EStates;	//needed to pass into to calcELevelDeriv
	EStates.push_back(eUse);
	EStates.push_back(eUse);

	//seed the values
	{ //first minOcc
		//recalculate all rates!
		netRate = 0.0;
		targetOcc=0.0;
		for(std::vector<ELevel*>::iterator trg = eUse->TargetELev.begin(); trg != eUse->TargetELev.end(); ++trg)
		{
			CalcFastRate(eUse, *trg, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);	//calculate the rate, but tell it to use the test number of carriers in the source
			nRate.AddValue(netRate);
			EStates[1] = *trg;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		for(std::vector<TargetE>::iterator trg = eUse->TargetELevDD.begin(); trg != eUse->TargetELevDD.end(); ++trg)
		{
			CalcFastRate(eUse, trg->target, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);
			nRate.AddValue(netRate);
			EStates[1] = trg->target;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		netRate = 0.0;	//initialze value
		nRate.AddValue(eUse->FastELevelContactProcess(RATE_OCC_TARGET, RATE_OCC_BEGIN));	//recalculate the contacts as quickly as possible
		//it will do nothing if it is not a contact point

		nRate.AddValue(fixedOpticalRate);
		netRate = nRate.GetNetSumClear();	//get the value and then clear the data

		nRatePos.push_back(netRate);
		derivPos.push_back(nDeriv.GetNetSumClear());
		occupancyPos.push_back(targetOcc);
	}
	{ //then maxOcc
		//recalculate all rates!
		netRate = 0.0;
		targetOcc=nStates;
		for(std::vector<ELevel*>::iterator trg = eUse->TargetELev.begin(); trg != eUse->TargetELev.end(); ++trg)
		{
			CalcFastRate(eUse, *trg, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);	//calculate the rate, but tell it to use the test number of carriers in the source
			nRate.AddValue(netRate);
			EStates[1] = *trg;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		for(std::vector<TargetE>::iterator trg = eUse->TargetELevDD.begin(); trg != eUse->TargetELevDD.end(); ++trg)
		{
			CalcFastRate(eUse, trg->target, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);
			nRate.AddValue(netRate);
			EStates[1] = trg->target;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		netRate = 0.0;	//initialze value
		nRate.AddValue(eUse->FastELevelContactProcess(RATE_OCC_TARGET, RATE_OCC_BEGIN));	//recalculate the contacts as quickly as possible
		//it will do nothing if it is not a contact point

		nRate.AddValue(fixedOpticalRate);
		netRate = nRate.GetNetSumClear();	//get the value and then clear the data

		nRateNeg.push_back(netRate);
		derivNeg.push_back(nDeriv.GetNetSumClear());
		occupancyNeg.push_back(targetOcc);
	}

	targetOcc = 0.5 * nStates;	//starting point
	bool occmatches = eUse->GetBeginOccMatch();

	while(maxOcc - minOcc > maxOcc * 1.0e-8)
	{
		
		netRate = 0.0;

		//recalculate all rates!
		for(std::vector<ELevel*>::iterator trg = eUse->TargetELev.begin(); trg != eUse->TargetELev.end(); ++trg)
		{
			CalcFastRate(eUse, *trg, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);	//calculate the rate, but tell it to use the test number of carriers in the source
			nRate.AddValue(netRate);
			if(MY_IS_FINITE(netRate)==false)
			{
				int i=0;
			}
			EStates[1] = *trg;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		for(std::vector<TargetE>::iterator trg = eUse->TargetELevDD.begin(); trg != eUse->TargetELevDD.end(); ++trg)
		{
			CalcFastRate(eUse, trg->target, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN, netRate);
			nRate.AddValue(netRate);
			if(MY_IS_FINITE(netRate)==false)
			{
				int i=0;
			}
			EStates[1] = trg->target;
			CalculateELevelDeriv(0, 1,derivFactor, sourceDeriv, targetDeriv, EStates, mdesc.simulation, RATE_OCC_TARGET, RATE_OCC_BEGIN);
			nDeriv.AddValue(sourceDeriv);
		}

		netRate = 0.0;	//initialze value
		nRate.AddValue(eUse->FastELevelContactProcess(RATE_OCC_TARGET, RATE_OCC_BEGIN));	//recalculate the contacts as quickly as possible
		//it will do nothing if it is not a contact point

		nRate.AddValue(fixedOpticalRate);
		netRate = nRate.GetNetSumClear();	//get the value and then clear the data

		if((netRate > 0.0 && occmatches) || (netRate < 0.0 && !occmatches))
		{
			nRatePos.push_back(netRate);
			derivPos.push_back(nDeriv.GetNetSumClear());
			occupancyPos.push_back(targetOcc);

			minOcc = targetOcc;
		}
		else if((netRate < 0.0 && occmatches) || (netRate > 0.0 && !occmatches))
		{
			nRateNeg.push_back(netRate);
			derivNeg.push_back(nDeriv.GetNetSumClear());
			occupancyNeg.push_back(targetOcc);

			maxOcc = targetOcc;

		}
		else //if(netRate > 0.0 && RateCalc == F_E_SS_NEGATIVE)
		{
			//need to determine which is closer
			double difAbove = occupancyNeg.back() - targetOcc;
			double difBelow = targetOcc - occupancyPos.back();

			if(difAbove > difBelow)
			{
				//the resolution on higher occupancies (negative rate) is worse
				nRateNeg.push_back(netRate);
				derivNeg.push_back(nDeriv.GetNetSumClear());
				occupancyNeg.push_back(targetOcc);

				maxOcc = targetOcc;
			}
			else
			{
				nRatePos.push_back(netRate);
				derivPos.push_back(nDeriv.GetNetSumClear());
				occupancyPos.push_back(targetOcc);

				minOcc = targetOcc;
			}
		}
		
		targetOcc = (maxOcc + minOcc)*0.5;		
		
		//not else if on purpose! First time through, it will redefine the values as the same
		
	}

	std::fstream file;
	std::string fname = "Rate_"+std::to_string(mdesc.simulation->curiter)+"_"+std::to_string(eUse->uniqueID)+".dat";
	file.open(fname, std::fstream::out);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open Rate_*.dat" << std::endl;
		return(0);
	}
	file << "Point " << eUse->getPoint().adj.self << std::endl;
	if(eUse->carriertype == ELECTRON)
	{
		if(eUse->bandtail)
			file << "CB Band Tail " << eUse->eUse << "eV" << std::endl;
		else if(eUse->imp)
			file << "Donor " << eUse->eUse << "eV" << std::endl;
		else
			file << "Conduction Band " << eUse->eUse << "eV" << std::endl;
	}
	else
	{
		if(eUse->bandtail)
			file << "VB Band Tail " << eUse->eUse << "eV" << std::endl;
		else if(eUse->imp)
			file << "Acceptor " << eUse->eUse << "eV" << std::endl;
		else
			file << "Valence Band " << eUse->eUse << "eV" << std::endl;
	}

	if(!occmatches)
		file << "Occupancy flipped: " << nStates << " - occ" << std::endl;

	file << std::setw(18) << std::left << "Occupancy";
	file << std::setw(16) << std::left << "Rate";
	file << std::setw(16) << std::left << "dRate/dOccupancy" << std::endl;

	//now output the data. Positive rates should be lowest occupancy

	unsigned int sz = occupancyPos.size();
	for(unsigned int i=0; i<sz; ++i)
	{
		file << std::setw(18) << std::left << std::scientific << std::setprecision(8) << occupancyPos[i];
		file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << nRatePos[i];
		file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << derivPos[i] << std::endl;
	}

	//negative rates start at full occupancy and go to zero...
	sz = occupancyNeg.size();
	for(unsigned int i = sz-1; i<sz; --i)	//when i=0, it should go to big number
	{
		file << std::setw(18) << std::left << std::scientific << std::setprecision(8) << occupancyNeg[i];
		file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << nRateNeg[i];
		file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << derivNeg[i] << std::endl;
	}

	file.close();


	return(0);
}

int RecTimestepDistribution(Simulation* sim, bool clear)
{
	if(sim->TransientData == NULL)
		return(0);

	std::ofstream file;
	
	if(clear)
	{
		file.open("timestepDist.dat", std::fstream::out);	//open the file for writing blank

		for(unsigned int i=0; i<TSTEPSIZE-1; ++i)
		{
			file << "<" << std::left << std::scientific << std::setprecision(1) << std::setw(9) << sim->TransientData->monTStep.CutOffs[i + 1];
			file << " | ";
		}
		file << ">=" << std::left << std::scientific << std::setprecision(1) << std::setw(9) << sim->TransientData->monTStep.CutOffs[TSTEPSIZE - 1];

		file << std::endl;
		file.close();
		return(0);
	}


	file.open("timestepDist.dat", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(file.is_open() == false)
	{
		std::cout << "Failed to open timestepDist.dat" << std::endl;
		return(0);
	}

	for(unsigned int i=0; i<TSTEPSIZE; ++i)
		file << std::left << std::setw(10) << std::fixed << sim->TransientData->monTStep.Count[i] << " | ";

	file << std::endl;

	file.close();
	
	return(0);
}