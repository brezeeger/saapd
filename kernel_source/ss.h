#ifndef SAAPD_SS
#define SAAPD_SS

#include <vector>
#include "BasicTypes.h"
#include "RateGeneric.h"

#define SS_INITIAL_TOLERANCE 0.01	//how much Ef can change to start
#define SS_REDUCE_TOLERANCE 0.5	//when it's converged enough, reduce tolerance by this much
#define SS_EF_ALTER 0.25//0.125	//tolerance multiplied by this as absolute max Ef can change
#define SS_SS_EF_ALTER 0.125//0.0078125	//if LRinEf and LRoutEf are within tolerance of one another, alter Ef by this amount
#define SS_INCREASE_TOLERANCE_CTR 12.0	//how long does it take to move greater distances if not going very quickly? Lower = faster, but potentially more divergent
#define SS_INCREASE_TOLERANCE 2.0
#define SS_ADJUST_MAXMIN 1.0e-3
#define SS_ADJUST_MAX 1.0e-6
#define SS_TRANSIENT_MIN 1.0e-6	//if it gets to this point, we'll say it's close enough. The occ/avail is in the correct magnitude and accurate to 6 digits. That should be plenty good for a rough guesstimate.
#define SS_TRANS_CUTOFF_ADJ 0.5//0.0625

#define SS_FLAG_NONE 0	//these don't seem to be used...
#define SS_FLAG_CAP 1	//calculate the capacitance
#define SS_FLAG_QE 2	//calculate the quantum efficiency
#define SS_FLAG_FINDVOC 4	//find the open circuit voltage
#define SS_FLAG_FINDJSC 8	//find the short circuit current

#define SS_NONE 0
#define SS_CB 1
#define SS_VB 2
#define SS_CBBT 4
#define SS_VBBT 8
#define SS_DON 16
#define SS_ACP 32
#define SS_ALL_STATES 63
#define SS_BANDS 3
#define SS_MIDGAP 60
#define SS_STATE_SWITCH_CUTOFF 15

#define SS_CALC_DONE 0
#define SS_NO_SRH 1		//all states, but don't include SRH calculations
#define SS_MATCH_IMPS 2	//only do SRH calculations, but affect the impurities only
#define SS_ALL 3	//all states, all calculations
#define SS_SLOWPTS 4	//just target the points which are deemed slow. Cycle quickly DD 10x, then SRH 1x, then all
#define SS_TRANSLIKE 5	//find the rate at which it goes to zero. Move halfway there. Use the netrate to set a timestep and actually take the carriers away from the other states (disaster)
#define SS_NEWTON_NOLIGHT 6	//the rates excluding light
#define SS_NEWTON_ALL 7		//all the rates including light
#define SS_NEWTON_NL_FULL 8	//don't simplify the matrix.  It seems to be struggling, so maybe the irrelevant terms matter a bit more
#define SS_NEWTON_L_FULL 9	//don't simplify the matrix.  It seems to be struggling, so maybe the irrelevant terms matter a bit more
#define SS_TRANS_ACTUAL 10	//this is pretty much like the transient solver, but slightly modified. Makes sure the charge is in the right domain, then does the net rates, finds a tstep, and adjusts all the rates. transfer net, not individual rates. Kinda sucked
#define SS_SMOOTH_BAD 11	//smoothen the bad data
#define SS_MONTECARLO 12		//straight up monte carlo until it is solved
#define SS_NEWTON_NUMERICAL 13	//calculate using the newton method, but get deriviative numerically instead of analytically
#define SS_TRANS_CUTOFF 14	//attempts to adjust states by tolerance & timestep, and ignores states after the rate changes sign. SSByTransCutoff

#define SS_CALCTYPE_MIN SS_NO_SRH
#define SS_CALCTYPE_MAX SS_TRANS_CUTOFF

#define IS_NEWTON(a) (a==SS_NEWTON_NOLIGHT || a==SS_NEWTON_ALL || a==SS_NEWTON_NL_FULL || a==SS_NEWTON_L_FULL)

#define SS_ENV_NONE 0
#define SS_ENV_CTC_VALIDATED 1

#define SS_NO_CHARGE_RESTRICTIONS 0
#define SS_CHARGE_INCREASE 1
#define SS_CHARGE_DECREASE 2

#define MC_EV_INIT 0.01  //how much to initially adjust the fermi levels by! Start large, there is a rejection scenario!
#define MC_CLEAR_NONE 0
#define MC_CLEAR_ABS 1
#define MC_CLEAR_POS 2
#define MC_CLEAR_NEG 4
#define MC_CLEAR_ALL 7
#define MC_FAILMULTIPLIER 4
#define MC_TOLERANCE_REDUCE 0.125
#define MC_PERCENTGOOD 0.7	//default value for percent good.

#define MC_NUMTRACK_MIN 5	//how many terms for averaging!
#define MC_NUMTRACK_MAX 100	//max number terms for averaging/converging. This will likely be really slow

#define MC_OCC_BEGIN 1
#define MC_OCC_END 2

#define MC_NOCHANGE 0
#define MC_MOVEON 1
#define MC_DONE 2

#define SS_TRANS_HAVENM1 -1


#define ENV_CONT_VALID_GOOD 0
#define ENV_CONT_VALID_NOTPOSSIBLE 1
#define ENV_CONT_VALID_NO_VOLT 2
#define ENV_CONT_VALID_NEED_MORE 4
#define ENV_CONT_VALID_TOO_MUCH 8
#define ENV_CONT_VALID_OVERLAP 16

class Contact;	//contact.h
class ELevel;
class Model;
class ModelDescribe;
class Point;
class Simulation;
class kernelThread;
class LightSource;
class Calculations;
class MonteCarlo;
class ELevelLocal;
class SteadyState_SAAPD;
class SS_SimData;

class MonteCarlo
{
public:
	
	MonteCarlo();	//constructor
	MonteCarlo(unsigned int sz, SS_SimData* dat, double adjAcc=1.0);	//constructor
	~MonteCarlo();	//destructor

	int SetRate(ELevelLocal& eLev, double nRate, unsigned long int eIndex);
	void ClearProbabilities(int which = MC_CLEAR_ALL);
	bool ChooseAbsRate(unsigned int& index) { return(AbsNetRate.Choose(index)); }
	bool ChooseWantPosCharge(unsigned int& index) { return(PosChargeRate.Choose(index)); }
	bool ChooseWantNegCharge(unsigned int& index) { return(NegChargeRate.Choose(index)); }
	double GetAdjAccuracy(void) { return(adjustedAccuracy); }
	unsigned long int ChooseRandomState(void);
	bool SetFail(unsigned int index, bool val = true);
	bool GetFail(unsigned int index);
	bool ResetFail(unsigned int index);
	void ClearFail(void);
	unsigned int GetFailCount(void) { return(numFail); }
	unsigned long int GetFailCtr(void) { return(failCtr); }
	bool HasAllFailed(void);
	int AdjustState(unsigned long int index, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
	int AdjustClusterState(unsigned long int index, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
	double ev_Adjust;	//how much do we try to adjust the fermi levels by?
	
	bool ReduceTolerance(void);
	int OutputRandomStatedistribution(int iter, SteadyState_SAAPD& data);
	int RecordEf(unsigned int stateIndex, double Ef);
	int ReadyMoveOn(double percentGood);
	int ClearTracking(void);
	unsigned int GetNumGoodStates(void) { return(NumGoodStates); }
	unsigned int GetReadyStates(void) { return(readyStates); }
	double GetStateEf(unsigned int index);
	int TransferData(SteadyState_SAAPD& data);
	bool GetProgBar(void) { return(ProgBar); };
private:
	unsigned long int failCtr;
	unsigned long int numFail;
	unsigned long int FailReduceTolerance;
	double adjustedAccuracy;	//how much can the accuracy change
	std::vector<bool> HasFailed;
	std::vector<Point*> BStructPts;
	std::vector<unsigned int> RndmCount;	//statistics output
	std::vector<unsigned int> BadCount;		//stats output
	VectorizedPFunc AbsNetRate;
	VectorizedPFunc PosChargeRate;
	VectorizedPFunc NegChargeRate;	//stored as positive numbers!
	double* EfTracker;	//once all the Ef's are good, we're good...
	double* avgEf;
	unsigned int* EfCtr;
	bool* okMove;
	unsigned int NumGoodStates;	//how many states
	unsigned int readyStates;	//number of states that have enough data to do stats on
	unsigned int maxGoodStates;	//the number of states in the monte carlo simulation
	SS_SimData* simdata;
	bool ProgBar;	//false: message 1, true message 2
	unsigned int RandomCtr;	// 0 - 1000 Random, 1001-1050 'Bad States'
};

class ELevelLocal
{
public:
	double LRinEf;	//most recent RELATIVE Ef when this point has a rate in. Not enough carriers, so increase Ef. Init -100 (really far bounds)
	double LRoutEf;	//most recent RELATIVE Ef when this point has a rate out. Too many carriers, so decrease Ef. Init 100 (really far bounds)
	double prevEf;	//the previous RELATIVE Ef. >0.0 means it has carriers of expected type
	double curEf;	//current RELATIVE Ef. >0.0 means it is very full of carrier type. >= 0.0 means occupancy holds carrier availability
	double ChangeCharge;	//how much charge (not multiplied by Q) changed on this iteration
	double occupancy;	//the occupancy used for this charge
	double NStates;		//number of states in the energy level
	double LastGoodEf;	//what was the last 'solution' for Ef that this can refer to as a starting point if things are going poorly
	double tmpGoodEf;	//used for partial jumps for bad convergence
	int BadCount;		//how large this is adjusts how big of a step it takes over the simulation accuracy
	int dataPt;	//which ptLocal is it in
	bool OccMatches;	//does the occupancy match
	bool carType;		//just avoid pointer references. Probably free in optimizations anyway
	std::vector<unsigned int> NeighborEquivs;	//the indices of the neighboring equivalent states

	double GetOcc(void);
	double GetAvail(void);
	double GetElec(void);
	double GetHole(void);
	double GetCharge(bool includeFixed=false);	//returns the net charge - NOT MULTIPLIED BY Q
	int SetOcc(double val);
	int SetAvail(double val);
	int SetElec(double val);
	int SetHole(double val);

	int AddOcc(double val);
	int AddAvail(double val);
	int AddElec(double val);
	int AddHole(double val);
	int UpdateOccVars(double Ef = 0.0, bool UseArgEf = false);

	int UpdateModelOccupancy(int which = MC_OCC_BEGIN | MC_OCC_END);
	int UpdateOccupancyFromModel(int which = RATE_OCC_BEGIN);
	ELevel* ptr;	//a pointer to the state
	PosNegSum NRates;	//the rates for the energy state
	PosNegSum DerivSOnly;
	~ELevelLocal();

private:
	double EfUpdatedOcc;	//the fermi level that corresponds with the current occupancy
	
};

class PtLocal
{
public:
	PosNegSum VariableCharge;	//the charges in the finite element from carriers
	double fixedCharge;		//charges in finite element from impurities/defects
	double maxChangeBStruct;	//coefficient defining the largest change as a result of adding positive charge
	double prevCharge;
	double curcharge;
	double QVoli;	//the number of+/- carriers must be multiplied by q/V to get charge density
	long int mdlRho;	//index for poisson equation and assigning rho
	long int self;	//what it is in mdl.gridp
	bool ctcSink;	//is this a contact sink and we don't to actually adjust this point?
	
};

class SSContactData
{
public:
	Contact* ctc;	//which contact is it
	int ctcID;	//needed to know which contact to link to
	double voltage;
	double current;
	double currentDensity;
	double eField;
	double current_ED;	//extra dimensional current - this will impact the band diagram (via efield), but it adds one equation and one known variable
	double eField_ED;	//extra dimensional electric field - this will impact the band diagram, but it adds one equation and one known variable
	double currentDensity_ED;	//extra dimensional current density - this will impact the band diagram (via efield), but it adds one equation and one known variable
	double eField_ED_Leak;	//value that the electric field will leak deeper out of device (continuing from ther end)
	double current_ED_Leak;	//value that the current causes the electric field to leek deeper into the device (continuing on other end).
	double currentD_ED_Leak;
	

	int currentActiveValue;	//this determines what values above are inputs and what are outputs
	bool suppressVoltagePoisson;	//let's say there are more than two voltages, should this voltage be excluded from solving poisson's equation
	bool suppressCurrentPoisson;	//prevent having such an overly defined system that there is no solution - which may come from currents
	bool suppressEFieldPoisson;		//same can be said for the electric field
	bool suppressEFMinPoisson;	//suppress minimizing the electric field
	bool suppressOppD;			//suppress making the displacement field cancel out as a boundary condition
	bool suppressConserveJ;		//suppress making the current in/out of device conserved
	

	bool MergeSame(SSContactData* other, bool replace=false);	//returns true if successfully merged other into this data, and other is safe to delete.
	void Clear(void);
	SSContactData();
	SSContactData(Contact* ct);
	~SSContactData() { }
	double GetValue(int flag);	//send in the currentActiveValue flag you want to get the value for.
	void Validate(Simulation& sim);
	void SaveToXML(XMLFileOut& file);
	
};

class ctcOutputs
{
public:
	int ctcID;	//contactID this applies to the contact index in environment
	XSpace JnOut, JpOut;	//outputs for the contact
	XSpace JnOutEnter, JpOutEnter;	//outputs where positive value is electrons or holes entering the contact - not positional or charge based!!
	double pExtEnter;	//are holes entering the device in a non-standard way
	double nExtEnter;	//are electrons entering the device in a non-standard way? (EBeam?)
	double voltageOut;		//output voltage for the contact (-Ef)
	double voltageNOut;
	double voltagePOut;
};

class CapOutput
{
public:
	int ctcID1;	//index in environment for contact voltage changed
	double voltageIncrease;
	double additionalPosCharge;
	double additionalNegCharge;
	double capacitance;
};

class EnvironmentOutputs
{
public:
	std::vector<ctcOutputs> contacts;
	std::vector<CapOutput> capacitance;
	double quantumEfficiencyInternal;	//this can only be calculated from a given set of contacts
	double quantumEfficiencyExternal;	//if there are multiples
	double efficiency;
	bool calculated;
	EnvironmentOutputs();
	~EnvironmentOutputs();

};

class Environment
{
	SS_SimData * const ss;	//constant pointer to the simulation data!
	int LocateEdgeContacts(int& indexLEdge, int& indexREdge);
	int GenerateEdgeContacts(Model& mdl, std::vector<Point*>& edges, Simulation* sim);
public:
	Environment(const Environment& env);
	Environment(SS_SimData* s);
	~Environment();

	std::vector<SSContactData*> ContactEnvironment;	//data for each individual contact
	std::vector<int> ActiveLightEnvironment;	//all the different light sources for each environment
	int reusePrevLightEnvironment;	//default to -1 to say this isn't happening
	//otherwise, use ExternalStates[reusePrevLightEnvironment].LightEnvironment[];
	int reusePrevContactEnvironment;
	int flags;	//SS_ENV_CTC_VALIDATED. basically used to make sure the data is correct.
	int id;	//Have this be what reusePrev***Environment looks for
	EnvironmentOutputs output;
	std::string FileOut; //what we want to call the output file for this particular condition

	SS_SimData* getSSSim() { return ss; }
	bool hasSpectra(LightSource* lt);
	bool hasSpectra(int id);
	bool hasContact(Contact* ct);
	bool hasContact(int id);
	SSContactData* getContact(int id);
	SSContactData* getContact(Contact *ct);
	void SaveToXML(XMLFileOut& file);
	

	void Validate(ModelDescribe& mdesc);	//updates all the pointers
	int ValidateData(Model& mdl, Simulation* sim);	//is all the data good enough.
	int SelfContainedValidate();	//not simulation prep, but GUI setup prep. Only changes suppression variables, and knocks out/repairs bad data! Ignores reusing data.
};

class SS_SimData
{
	unsigned int CalcIndex;		//what is the current index for calc order?
	std::vector<int> CalculationOrder;	//INPUT - what order should things be calculated by?
	std::vector<double> InitAccuracy;	//INPUT - value of zero means use default for simulation
	std::vector<double> EndAccuracy;	//INPUT - value of zero means use default for simulation
	Simulation* const sim;	//just a pointer back to the simulation data, so it doesn't need to be passed everywhere.
	void BreakEnvironmentLoops();
public:
	int CurrentData;		//guessing this was supposed to be the equivalent of sim->curiter.
	int LastBDUpdated;		//a flag for checking the band diagram update
	double steadyStateClampReduction;	//INPUT - how much will it try to reduce the steady state clamp value by. Lower numbers stabilize convergence, but take longer.
	std::vector<Environment*> ExternalStates;	//INPUT - this needs to be solved for each one and then outputted
	
	
	double VoltCurrentScaleFactor;	//failing to converge, slowly turn these bits on
	double LightRateFactor;
	double LightEmissionFactor;
	double TunnellingFactor;
	double PercentGood;	//INPUT - how much should pass for it to be good
	unsigned int NumTrack;	//INPUT - how many iterations should the state track to be considered OK
	
	void AddCalcOrder(int spot, double initAcc=0.0, double endAcc=0.0);	
	bool RemoveCalcOrder(unsigned int index);
	bool UpdateCalcOrder(int index, int how=-1, double initAcc=-1.0, double endAcc=-1.0);
	bool SwapCalcOrder(unsigned int index1, unsigned int index2);
	void ClearCalcOrder(void) { CalculationOrder.clear(); return; };
	int GetCalcType(bool next = false);
	void SetCalcIndex(unsigned int which = 0);
	double GetCalcInitAccuracy(void);
	double GetCalcEndAccuracy(void);
	double InitIteration(Model& mdl, ModelDescribe& mdesc);

	Simulation* getSim() { return(sim); }
	unsigned int getNumberCalcSteps() { return CalculationOrder.size(); }
	void UpdateUnknownIDs();
	void SaveToXML(XMLFileOut& file);
	void Validate();	//this is mostly for the GUI
	void Initialize();
	SS_SimData(Simulation * s);
	~SS_SimData();
};

class SteadyState_SAAPD
{
public:
	std::vector<ELevelLocal> EStates;	//a set of all local info for the states that can change
	std::vector<PtLocal> Pt;
	std::vector<Point*> BandPtsUpdated;	//minimize the number of times CalcPointBandFast must be called
	std::vector<double> UpdatedPtsInitPsi;	//what were the initial v
	std::vector<MaxMin<long int>> ELevelIndicesInPt;	//allows going through EStates by point in the for loop. -1 means no changes in point
	
	double tolerance;	//how much Ef or any point in the band structure is allowed to change
	double targetCharge;	//how much charge (not multiplied by Q) should be in the device (target is typically 0)
	double chargeTolerance;	//how much charge is considered zero? If within this range, let the device adjust everything. If not, only adjust the device towards the targetCharge. Not multiplied by Q. Set by the size of the device.
	int CalcStateType;		//what states should be calculated in the steady state
	int CalcStateCtr;		//counter incremented every iteration for rotating through the things
	int CalcStateIncCtr;	//counter to increment the counter max. Initially be long, then shrink down
	int CalcStateCtrMax;	//what the counter is going to
	PosNegSum varCharge;	//the actual charge (not multiplied by Q) in the device
	double fixedCharge;	//what the fixed charge in the device is
	double totCharge;	//A constant charge for a run through
	bool ReduceTolerance;	//flag to reduce the tolerance. Default true, but set to false if any state is bad
	VectorizedPFunc PosChargeChanges;
	VectorizedPFunc NegChargeChanges;
	
	int SetCtcSinkPts(Model& mdl);	//must be done after Environment ValidateData
	int TransferMdlPtToSS(Model& mdl, int SSPtIndex);	//takes the point in SS index and receives data from MDL
	int Clear();
	~SteadyState_SAAPD();
};

int SS(ModelDescribe& mdesc, Model& mdl, kernelThread* kThread);
int StateControl(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, Calculations& InitCalcs);
int TroubleConverge(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data, kernelThread* kThread);
int FinalOutputs(Model& mdl, ModelDescribe& mdesc);
int FillInEnvOutputs(Model& mdl, ModelDescribe& mdesc, Environment& thisEnv);
int InitStructure(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data);
double LoopAllStatesForward(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
double LoopAllStatesBackward(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
double SS_SlowPtLoop(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread);
double SS_Transtype(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread);
double SS_TransferCarriers(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, double tStep, long int el);
int AdjustStateEf(SteadyState_SAAPD& data);
double LoopAllStatesRateToZero(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread);
double GetStateRateZero(ELevelLocal& eLev, PtLocal& pt, SteadyState_SAAPD& data, Model& mdl, Simulation* sim, bool RetInitNetRate=false);
int AdjustStateEf(SteadyState_SAAPD& data);
double maxEfChange(ELevelLocal& eLev);
int CreateLightSourcesFast(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, bool force=false);
int CalculateRatesCharge(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
int SSCorrectCharge(SteadyState_SAAPD& data, Model& mdl);
int SSInitData(SteadyState_SAAPD& data, Model& mdl);
unsigned int FindPtIndex(SteadyState_SAAPD& data, Model& mdl, Point* pt, unsigned int hint=-1);
int AdjustCharge(SteadyState_SAAPD& data, Model& mdl);
int GenerateOutput(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data, Calculations& InitCalc, kernelThread* kThread=NULL);
int ClearELevelRates(ELevel& eLev);
int GenerateOutputELevelCalc(Point* pt, ELevel& eLev, Simulation* sim);
int GenerateOutputELevelRec(Point* pt, ELevel& state, Simulation* sim);
int UpdatePtVarCharge(ELevelLocal& state, PtLocal& pt, double occ=-1.0);
int FindStateOccAdjustCharge(ELevelLocal& state, double chargeAdjust, double& newocc, double& newavail);
bool UpdateStateFermi(ELevelLocal& EState, double NetRate, double smallAdjust, double largeAdjust, double tolerance, double overAdjusted, double maxEfDelta);
double UpdateStateOccupancy(ELevelLocal& EState, double kTI, bool preventChangeCharge=false);
double RelEfFromOcc(double occ, double avail, double eWidth, double kt, float degen=1.0);
double OccAvailFromRelEf(double relEf, double NStates, double ktI, double eWidth, float degen = 1.0);
double CalcChargeTolerance(Model& mdl, ModelDescribe& mdesc);
double SSByTransCutoff(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread = NULL);
double SSTransEqPoint(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, int point, bool ignoreDD=true);
double GetSmoothedCurEf(SteadyState_SAAPD& data, unsigned long int EState);
double SmoothELevelCurEf(SteadyState_SAAPD& data, unsigned long int EState, double factor=1.0);

double CalcStateRate(ELevelLocal* eLev, Model& mdl, ModelDescribe& mdesc, std::vector<Point*>& BStructPts, bool& hadRate, PosNegSum* accuRate=NULL, bool includeLight=true, bool includeContacts=true, bool preInitialized=false);
int CreateLightSourceModifiedELevel(Simulation* sim, SteadyState_SAAPD& data, unsigned long int eLevel);

int SS_MC(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread = NULL, double adjAcc=1.0);
int SS_MC_CalcAllRates(MonteCarlo* MCData, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc);
double SS_Newton_Numerical(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrixAnalytical, bool fullMatrix=true);	//run this at the end to try and converge the answer well!
int CalcNumericalJacobian(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrix, double* rhs, bool* useELevel, unsigned int sz, bool fullMatrix, double* derivMatrixAnalytical);

#endif