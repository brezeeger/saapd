#ifndef BINTREE
#define BINTREE

#include <iostream>
#include <vector>
#include <fstream>

#define LEFT 0
#define RIGHT 1

using namespace std;
extern int ProgressString(string str);		//outputdebug.cpp
extern int ProgressCheck(int check, bool clear);	//outputdebug.cpp

template <class D, class S>
class TreeRoot;

template <class D, class S>
class Tree {
public:
	Tree<D,S>* left;		//pointer to left subtree
	Tree<D,S>* right;	//pointer to right subtree
	Tree<D,S>* parent;	//pointer to parent subtree. Null if root.
	TreeRoot<D,S>* root;	//pointer to root of tree, which will contain various info and functions.
	D Data;			//the data for the tree
	S Sort;			//used to determine how the tree will be sorted.
//	unsigned int depth;	//the depth into the tree
	int maxdepth;	//the maximum depth going down from the tree
	

///////////////////////////////CLASS FUNCTIONS/////////////////

	Tree()
	{
//	depth = -1;
	maxdepth = -1;
	left = NULL;
	right = NULL;
	parent = NULL;
	root = NULL;
	};

//////////////////////////////////////////////////////////

	Tree(D Dat, S srt)		//constructor with data.
	{
	//depth = -1;
	maxdepth = -1;
	left = NULL;
	right = NULL;
	parent = NULL;
	root = NULL;
	Sort = srt;
	Data = Dat;
	};
/////////////////////////////////////////////////////////////////
	~Tree()
	{	//make sure all the memory gets set to zero.
		this->left=NULL;
		this->right=NULL;
		this->parent=NULL;
//		this->Sort=0;
//		this->depth=0;
		this->maxdepth=0;
//		this->Data=0;	//had to remove because data might not be an integer...
		this->root=NULL;
	};				//destructor for a tree
/////////////////////////////////////////////////////////////////

/*	int GetDepth(int depth, S srt)	//finds the depth of a particular tree
	{
		if(srt < this->Sort)
		{
			if(this->left != NULL)
			{
				depth = this->left->GetDepth(depth+1, srt);
			}
			else
				return(-1);	//this node doesn't exist
		}
		else if(srt > this->Sort)
		{
			if(this->right != NULL)
			{
				depth = this->right->GetDepth(depth+1, srt);
			}
			else
				return(-1);
		}
		//otherwise, it is equal, so just return the depth passed in.
		return(depth);

	};	//end GetDepth()
	*/
/////////////////////////////////////////////////////////////////

	void UpdateRoot(TreeRoot<D, S>* TRoot)
	{
		if(this->left != NULL)
			this->left->UpdateRoot(TRoot);
		if(this->right != NULL)
			this->right->UpdateRoot(TRoot);

		this->root = TRoot;
		return;
	}

//////////////////////////////////////////////////////////////////
	int UpdateMaxDepthDown(void)	//for inserts, propagates the new maximum depth
	{										//down the tree. Once it encounters a higher one
		int maxL = 0;
		int maxR = 0;
		int max;

		if(this->left == NULL && this->right == NULL)
			max = this->root->GetDepth(this);
		else
		{
			if(this->left != NULL)
				maxL = this->left->maxdepth;
			if(this->right != NULL)
				maxR = this->right->maxdepth;
		
			if(maxL > maxR)
				max = maxL;
			else
				max = maxR;
		}

		if(max != this->maxdepth)		//it needs to keep propagating downward
		{
			this->maxdepth = max;
			if(this->parent != NULL)
				this->parent->UpdateMaxDepthDown();
		}	//if they were the same, then the rest of the tree should be unchanged.
		return(0);
	};	//end UpdateMaxDepthDown
/////////////////////////////////////////////////////////////////
	int UpdateMaxDepthTreeUp(void)	//updates all the max depths above this node
	{							//useful for a delete operation or after a balance operation
		if(this == NULL)	//just in case this is inadvertantly called by accident
			return(0);
		if(this->left == NULL && this->right == NULL)
		{	//this depth is a maximum depth
			this->maxdepth = this->root->GetDepth(this);
			return(this->maxdepth);
		}
		int maxL = 0;
		int maxR = 0;

		if(this->left != NULL)
			maxL = this->left->UpdateMaxDepthTreeUp();
		if(this->right != NULL)
			maxR = this->right->UpdateMaxDepthTreeUp();

		if(maxL > maxR)
			this->maxdepth = maxL;
		else
			this->maxdepth = maxR;

		return(this->maxdepth);
	};	//end UpdateMaxDepthTreeUp
/////////////////////////////////////////////////////////////////

	int KillSelfandChildren()	//destroy node and all children. free memory.
	{
	Tree<D,S>* next;


	//kill everything to the left
	next = this->left;
	if(next != NULL)
		next->KillSelfandChildren();
	
	//kill everything to the right
	next = this->right;
	if(next != NULL)
		next->KillSelfandChildren();

	this->root->size--;

	next = this;
	//kill yourself. This is morbid.
	delete next;
	next = NULL;

	

	return(0);
	};	//end killselfandchildren function

/////////////////////////////////////////////////////////////////////////////////////

	int CopyToNewTree(TreeRoot<D,S>* NewRoot, bool includeChildren)	//copies the tree and children over to a new tree
	{
		if(this->root == NewRoot)	//prevent copying to self. Would likely cause an infinite loop and other odd behaviors
			return(0);

		NewRoot->Insert(this->Data, this->Sort);

		if(includeChildren)
		{
			if(this->left != NULL)
				this->left->CopyToNewTree(NewRoot, includeChildren);
			if(this->right != NULL)
				this->right->CopyToNewTree(NewRoot, includeChildren);
		}

		return(1);
	}

/////////////////////////////////////////////////////////////////////

/*	int MaxDepth(unsigned int presentmax)			//returns the max depth of a particular branch's children
	{
	Tree<D,S>* ptrTree;

	if(this->depth > presentmax)
		presentmax = this->depth;	//adjust for itself

	ptrTree = this->left;
	if(ptrTree != NULL)
		presentmax = ptrTree->MaxDepth(presentmax);	//replace with a new max depth if needed

	ptrTree = this->right;
	if(ptrTree != NULL)
		presentmax = ptrTree->MaxDepth(presentmax);	//replace with a new max depth if needed

	return(presentmax);
	};	//end function maxdepth
*/
//////////////////////////////////////////////////////////////////

	int BalanceAll(Tree<D,S>* node)
	{
		if(node==NULL)
			return(0);
		if(node->left != NULL)
			BalanceAll(node->left);
		if(node->right != NULL)
			BalanceAll(node->right);
		
		node->Balance(true);
		return(0);
	};	//end BalanceAll
//////////////////////////////////////////////////////////////////

	int Balance(bool prevparent)	//balances the binary tree starting from this node down
	{
	Tree<D,S>* ptrRoot = this;
	if(ptrRoot == NULL)
		return(0);	//nothing to balance. May happen if removes last node from tree.
	Tree<D,S>* ChildL = ptrRoot->left;
	Tree<D,S>* ChildR = ptrRoot->right;
	int balance = 0;
	bool rotate=false;

	if(ChildL != NULL)
		balance += ChildL->maxdepth;//MaxDepth(ChildL->depth);
	else
		balance += ptrRoot->root->GetDepth(this);
	if(ChildR != NULL)
		balance -= ChildR->maxdepth;//MaxDepth(ChildR->depth);
	else
		balance -= ptrRoot->root->GetDepth(this);

	if(balance > 1)	//ChildL has a larger maximum depth and must be rotated in
	{
		if(ChildL)
		{
			rotate=true;
			int bal = 0;
			Tree<D,S>* ChildLL = ChildL->left;
			Tree<D,S>* ChildLR = ChildL->right;
		
			if(ChildLL != NULL)
				bal += ChildLL->maxdepth;//MaxDepth(ChildLL->depth);
			else
				bal += ChildL->root->GetDepth(ChildL);
			if(ChildLR != NULL)
				bal -= ChildLR->maxdepth;//(ChildLR->depth);
			else
				bal -= ChildL->root->GetDepth(ChildL);

			if(bal < 0)	//	a double right rotation is needed. first a left rotation with childL as root
				ChildL->RotateLeft();
		//then a right rotation with respect to what was the root is needed.
			ptrRoot->RotateRight();	//this happens no matter what the balance is, but happens second if bal<0				
		}
	}
	else if(balance < -1)
	{
		if(ChildR)	//this should be redundant, but you never know...
		{
			rotate=true;
			int bal = 0;
			Tree<D,S>* ChildRL = ChildR->left;
			Tree<D,S>* ChildRR = ChildR->right;

			if(ChildRL != NULL)
				bal += ChildRL->maxdepth;//MaxDepth(ChildRL->depth);
			else
				bal += ChildR->root->GetDepth(ChildR);
			if(ChildRR != NULL)
				bal -= ChildRR->maxdepth;//MaxDepth(ChildRR->depth);
			else
				bal -= ChildR->root->GetDepth(ChildR);

			if(bal > 0)	//double left rotation needed. First, right rotation with childR as root
				ChildR->RotateRight();
		//then a left rotation with the root as the root.
			ptrRoot->RotateLeft();
		}
	}
	
	if(ptrRoot->parent != NULL)	//this has to be true IF any balancing occurred.
	{							//thie child being rotated in is not the parent of ptrRoot
		if(rotate)
		{	//everything shifted above this node, so fix the max depths above this node
			ptrRoot->parent->UpdateMaxDepthTreeUp();	//recalculates all above
			ptrRoot->parent->UpdateMaxDepthDown();		//recalculates below as needed
		}

		if(prevparent==false)
			ptrRoot->parent->Balance(false);
	}
	return(0);
	};	//end balance

////////////////////////////////////////////////////////////////
/*
	int GeneratePath(void)	//encodes in an integer which direction to go. not sure what useful for.
	{
	S PrevSort = Sort;
	Tree<D,S>* ptr;
	int path = 0;

	ptr = parent;
	while(ptr != NULL)
	{
		if(PrevSort < ptr->Sort)	//meant a left was done
		{
			path = path + (LEFT<<ptr->depth);	//left is 0... but for the sake of readability
			PrevSort = ptr->Sort;
			ptr = ptr->parent;
		}
		else	//in case there are multiple entries, allow =
		{
			path = path + (RIGHT << ptr->depth);	//first node depth is zero
			PrevSort = ptr->Sort;
			ptr = ptr->parent;
		}
	}
	//when the last entry is done, the depth will be 0. and the then a mod 1 can be used to read
	//off whether to move left or right.
	return(path);
	};	//end generate path function
*/

//////////////////////////////////////////////////////////////////

	void GenerateNodeList(vector<Tree<D,S>*>& nodes)
	{
		if(this->left)
			this->left->GenerateNodeList(nodes);

		nodes.push_back(this);

		if(this->right)
			this->right->GenerateNodeList(nodes);

		return;
	}

//////////////////////////////////////////////////////////////////

	void GenerateSort(vector<D>& srtD)	//creates vector of all the values
	{									//if simply need to cycle through all values
	Tree<D,S>* ptrTree = this->left;	//such as what equations need adding.
	if(ptrTree != NULL)
		ptrTree->GenerateSort(srtD);	//add the smaller values to the left

	srtD.push_back(this->Data);		//add this value

	ptrTree = this->right;
	if(ptrTree != NULL)					//add the larger values to the right if they exist
		ptrTree->GenerateSort(srtD);
	
	return;
	};	//end generatesort

/////////////////////////////////////////////////////////////////////////

	void GenerateSortPtr(vector<D*>& srtD)	//creates vector of all the values
	{									//if simply need to cycle through all values
	Tree<D,S>* ptrTree = this->left;	//such as what equations need adding.
	if(ptrTree != NULL)
		ptrTree->GenerateSortPtr(srtD);	//add the smaller values to the left

	srtD.push_back(&(this->Data));		//add this value

	ptrTree = this->right;
	if(ptrTree != NULL)					//add the larger values to the right if they exist
		ptrTree->GenerateSortPtr(srtD);
	
	return;
	};	//end generatesort

/////////////////////////////////////////////////////////////////////////

	void GenerateSortS(vector<S>& srtS)	//creates vector of all the values
	{									//if simply need to cycle through all values

		if(left != NULL)
			left->GenerateSortS(srtS);	//add the smaller values to the left

		srtS.push_back(Sort);		//add this value
	
		if(right != NULL)					//add the larger values to the right if they exist
			right->GenerateSortS(srtS);
	
		return;
	};	//end generatesortS

/////////////////////////////////////////////////////////////////////////

	void GenerateSortSPtr(vector<S*>& srtS)	//creates vector of all the values
	{									//if simply need to cycle through all values
		//such as what equations need adding.
		if(left != NULL)
			left->GenerateSortSPtr(srtS);	//add the smaller values to the left

		srtS.push_back(&Sort);		//add this value

		if(right != NULL)					//add the larger values to the right if they exist
			right->GenerateSortSPtr(srtS);
	
		return;
	};	//end generatesortS
/////////////////////////////////////////////////////////////////////////
/*
	void RecalcDepth(unsigned int depth)
	{
	Tree<D,S>* ptrTree;

	this->depth = depth;

	ptrTree = this->left;
	if(ptrTree != NULL)
		ptrTree->RecalcDepth(depth+1);

	ptrTree = this->right;
	if(ptrTree != NULL)
		ptrTree->RecalcDepth(depth+1);
	
	return;
	};	//end recalculate depth
*/
//////////////////////////////////////////////////////////////////

	void DisplayNodes(fstream& fileout)
	{
	Tree<D,S>* ptrTree = this->left;

	if(ptrTree != NULL)
		ptrTree->DisplayNodes(fileout);

	fileout << "(" << this->Sort << ", " << this->Data << ");   ";

	ptrTree = this->right;
	if(ptrTree != NULL)
		ptrTree->DisplayNodes(fileout);


	return;
	};

	void DisplayBuggedTree(ofstream& fileout)
	{
		if(left > reinterpret_cast<Tree<D,S>*>(0x00000060))
			left->DisplayBuggedTree(fileout);

		fileout << "Left: " << left << "     Right: " << right << "    Parent: " << parent;
//		fileout << "    (" << this->Sort << ", " << this->Data << ");" << endl;
		

		if(right > reinterpret_cast<Tree<D,S>*>(0x00000060))
			right->DisplayBuggedTree(fileout);

		return;

	}

////////////////////////////////////////////////////////

	Tree<D,S>* InsertExistingNode(Tree<D,S>* InsTree)
	{
		Tree<D,S>* ptrTree = this;
		Tree<D,S>* peek = this;
	
		while(peek != NULL)
		{
			ptrTree = peek;	//can ignore first time. If it successfully peeks(restarts loop),

			if(InsTree->Sort <= peek->Sort)	//want to know left side
				peek = ptrTree->left;
			else// if(srt > peek->Sort)	//want to know right side
				peek = ptrTree->right;
		}

		InsTree->root = this->root;	//set the root for the tree node.

		if(InsTree->Sort <= ptrTree->Sort)	//insert left
		{
			ptrTree->left = InsTree;	//link the two together
			InsTree->parent = ptrTree;

		}
		else if(InsTree->Sort > ptrTree->Sort)
		{
			ptrTree->right = InsTree;	//link the two together
			InsTree->parent = ptrTree;
		}
		InsTree->root->size++;	//increment size of root
		InsTree->maxdepth = InsTree->root->GetDepth(InsTree);	//it can't possibly have children,
		//so it's depth must be the maximum depth

		if(InsTree->parent != NULL)
		{
			InsTree->parent->UpdateMaxDepthDown();	//update all the depths below as needed
			InsTree->parent->Balance(false);	//balance this insertion and all below it
		}	
		return(InsTree);
	}


////////////////////////////////////////////////////////

	Tree<D,S>* Insert(D Dat, S srt)	//insert node into an appropriate open branch below this branch
	{							//if the node already exists, it replaces the data value.
	Tree<D,S>* ptrTree = this;
	Tree<D,S>* peek = this;
	
	while(peek != NULL)
	{
		ptrTree = peek;	//can ignore first time. If it successfully peeks(restarts loop),
		//know this is fine. If it doen't, then know to stop

		if(srt <= peek->Sort)	//want to know left side
			peek = ptrTree->left;
		else// if(srt > peek->Sort)	//want to know right side
			peek = ptrTree->right;
/*		else	//peek points to exactly on a matching sort value
		{
			if(Dat == peek->Data)
				return(peek);

			peek->Data = Dat;	//overwrite the data
			return(peek);
		}*/
	}
	Tree<D,S>* InsTree = NULL;
	try
	{
		InsTree = new Tree(Dat, srt);	//now that we know it's a new node, allocate space
	}
	catch (...)
	{
		ProgressCheck(54, false);
		return(NULL);
	}
	InsTree->root = this->root;	//set the root for the tree node.

	if(srt <= ptrTree->Sort)	//insert left
	{
		ptrTree->left = InsTree;	//link the two together
		InsTree->parent = ptrTree;
//		InsTree->depth = ptrTree->depth + 1;	//set the depth of the node
	}
	else if(srt > ptrTree->Sort)
	{
		ptrTree->right = InsTree;	//link the two together
		InsTree->parent = ptrTree;
//		InsTree->depth = ptrTree->depth + 1;	//set the depth of the node
	}
	InsTree->root->size++;	//increment size of root
	InsTree->maxdepth = InsTree->root->GetDepth(InsTree);	//it can't possibly have children,
				//so it's depth must be the maximum depth

	if(InsTree->parent != NULL)
	{
/*		if(InsTree->parent->Verify())
		{
			ProgressString("Data is not OK before balancing on insert.");
		}*/
		InsTree->parent->UpdateMaxDepthDown();	//update all the depths below as needed
		InsTree->parent->Balance(false);	//balance this insertion and all below it
	}

//	root->VerifyTree();

	return(InsTree);
	};	//end insert into tree
//////////////////////////////////////////////////////////
	Tree<D,S>* Replace(D Dat, S Srt)
	{
		Tree<D,S>* present = this;
		Tree<D,S>* RetVal = NULL;

		while(present != NULL)
		{
			if(Srt == present->Sort)
			{
				present->Data = Dat;
				RetVal = present;
				break;
			}
			else if(Srt < present->Sort)
				present = present->left;
			else
				present = present->right;
		}
		if(RetVal == NULL)
			RetVal = present->Insert(Dat, Srt);


		return(RetVal);	//returns null if not found
	}


//////////////////////////////////////////////////////////

	int Multiply(D Dat)		//multiples the data and all subtree data by the specified constant
	{
		if(this->left != NULL)
			this->left->Multiply(Dat);

		this->Data = this->Data * Dat;

		if(this->right != NULL)
			this->right->Multiply(Dat);
		return(0);
	}

/////////////////////////////////////////////////////////////////

	int MultiplySrt(S srt)		//multiples the sort and all subtree sort by the specified constant. This should only be initiated from root, or it will screw up the ordering!
	{
		if(this->left != NULL)
			this->left->MultiplySrt(srt);

		this->Sort = this->Sort * srt;

		if(this->right != NULL)
			this->right->MultiplySrt(srt);
		return(0);
	}


/////////////////////////////////////////////////////////////////

	int RemovePtr(S* srt, bool prevbalance)
	{
		S sort = *srt;
		Tree<D,S>* ptrRemove = FindPtr(srt);

		if(ptrRemove != NULL)
		{
			ptrRemove->Remove(sort, prevbalance);
			return(1);
		}
		return(0);

/*		while(ptrRemove != NULL)
		{
			if(&(ptrRemove->Sort) != srt)
			{	//this is not the correct sort one! So keep looking.
				if(ptrRemove->left != NULL)
					ptrRemove = ptrRemove->left->Find(sort);	//keep looking on the left side of the tree
				else	//it's not the correct one, AND there is no more possibilities!
					return(0);	//return failure...
			}
			else	//found the correct sort to get rid of it!
			{
				ptrRemove->Remove(sort, prevbalance);
				return(1);	//return success
			}
		}
		return(0);
		*/
	}
/////////////////////////////////////////////////////////////////
//repositions the data in the tree if its sort value were to change.
	//essentially, this is a Remove() and then an Insert(), except no deleting data.
	//very much a modified copy-paste.
	int Reposition(bool prevbalance)
	{
		if(this == NULL)
			return(0);
		Tree<D,S>* ptrMove = this;
		Tree<D,S>* ChildL = ptrMove->left;
		Tree<D,S>* ChildR = ptrMove->right;
		Tree<D,S>* Parent = ptrMove->parent;
		Tree<D,S>* ptrBalance = NULL;
		
		if(ChildL == NULL && ChildR == NULL)
		{
			if(Parent != NULL)
			{
				if(Parent->left == ptrMove)
					Parent->left = NULL;
				else
					Parent->right = NULL;
				ptrBalance = Parent;
			}
			else
			{
				//no parent, no children, this is the only one. Just get out of this call quickly.
				return(1);
			}

		}
		else if(ChildL == NULL && ChildR != NULL)
		{
			//ChildR is valid, so just move childR into the place of the present item
			ChildR->parent = Parent;	//left child doesn't mtter
			ptrBalance = ChildR;
			if(Parent != NULL)
			{
				if(Parent->right == ptrMove)	//the child was on the right, so add it
				{		//to the right side of the parent
					Parent->right = ChildR;
				}
				else
				{	//the deleted node was to the left
					Parent->left = ChildR;
				}
//				Parent->RecalcDepth(Parent->depth);
			}
			else	//this was the root entry being deleted
			{
				ptrMove->root->start = ChildR;	//set the starting point to this entry
//				ChildR->RecalcDepth(0);	//the depth of the starting entry is 0.
			}
		}
		else if(ChildL != NULL && ChildR == NULL)	//childR does not exist
		{
			//ChildL is valid. move childL into place of the present item.
			ChildL->parent = Parent;
			ptrBalance = ChildL;
			if(Parent != NULL)
			{
				if(Parent->right == ptrMove)
					Parent->right = ChildL;
				else
					Parent->left = ChildL;

//				Parent->RecalcDepth(Parent->depth);	//update the depths
			}
			else
			{
				ptrMove->root->start = ChildL;
//				ChildL->RecalcDepth(0);	
			}

		}															//of everything above it
		else if(ChildL != NULL && ChildR != NULL)
		{	//things start to get really ugly...
			//first try the easy ways.
			int balance = 0;
			balance += ChildL->maxdepth;//MaxDepth(ChildL->depth);
			balance -= ChildR->maxdepth;//MaxDepth(ChildR->depth);

			if(balance > 0)
			{	//the left side is deeper, so take one out of left side
				Tree<D,S>* Replace = ChildL->SearchRight();	//this spot will also be valid in what will be deleted

				if(ChildL == Replace)
				{
					Replace->right = ChildR;	//there was nothing to replace->left (Search Left
					ChildR->parent = Replace;	//returns furthest left spot
					Replace->parent = Parent;
					ptrBalance = Replace;
					//Replace->right does not change in any manner. Just shifts with Replace.
				}
				else	//ChildL is not REplace. Therefore, Replace is at least one unit to left
				{		//of ChildL
					
					//Replace has no right children, but may have a left child
					Tree<D,S>* RepL = Replace->left;
					ptrBalance = Replace->parent;

					if(RepL != NULL)
					{
						RepL->parent = Replace->parent;	//repL takes on replaces spot
						RepL->parent->right = RepL;	//replace was to the right of the parent
//						RepL->parent->RecalcDepth(RepL->parent->depth);	//update depths for balancing
						//Rep:'s right and left do not change (except depth)
					}
					else	//RepL doesn't exist.
					{
						Replace->parent->right = NULL;	//no more data for it to look at
					}
					ChildL->parent = Replace;	//the children of what was to be removed
					ChildR->parent = Replace;	//are now children of what is replacing the removed value
					Replace->right = ChildR;	//link back to them
					Replace->left = ChildL;
					Replace->parent = Parent;	//replace is fully linked
//					Replace->depth = ptrRemove->depth;
				}	//end else saying childL is not replace
				
				//now update the parent, which is the same regardless of the scenario
				if(Parent != NULL)
				{
					if(Parent->left == ptrMove)
						Parent->left = Replace;
					else
						Parent->right = Replace;

//					Parent->RecalcDepth(Parent->depth);	//update the depths
				}
				else	//deleted the root node
				{		//Replace is now the root node.
					Replace->root->start = Replace;
//					Replace->root->start->RecalcDepth(0);	//recalculate all depths
				}
			}
			else
			{	//the right side is deeper/default, so take one out of right side
				Tree<D,S>* Replace = ChildR->SearchLeft();	//this spot will also be valid in what will be deleted

				if(ChildR == Replace)
				{
					Replace->left = ChildL;	//there was nothing to replace->left (Search Left
					ChildL->parent = Replace;	//returns furthest left spot
					Replace->parent = Parent;
					ptrBalance = Replace;
					//Replace->right does not change in any manner. Just shifts with Replace.
				}
				else	//ChildR is not REplace. Therefore, Replace is at least one unit to left
				{		//of ChildR
					
					//Replace has no left children, but may have a right child
					Tree<D,S>* RepR = Replace->right;	
					ptrBalance = Replace->parent;
					if(RepR != NULL)
					{
						RepR->parent = Replace->parent;	//repR takes on replaces spot
						RepR->parent->left = RepR;	//replace was to the left of the parent
//						RepR->parent->RecalcDepth(RepR->parent->depth);	//update depths for balancing
						//RepR's right and left do not change (except depth)
					}
					else	//RepR doesn't exist.
					{
						Replace->parent->left = NULL;	//no more data for it to look at
					}
					ChildL->parent = Replace;	//the children of what was to be removed
					ChildR->parent = Replace;	//are now children of what is replacing the removed value
					Replace->left = ChildL;	//link back to them
					Replace->right = ChildR;
					Replace->parent = Parent;	//replace is fully linked
	//				Replace->depth = ptrRemove->depth;
				}	//end else saying childR is not replace
				
				//now update the parent, which is the same regardless of the scenario
				if(Parent != NULL)
				{
					if(Parent->left == ptrMove)
						Parent->left = Replace;
					else
						Parent->right = Replace;

	//				Parent->RecalcDepth(Parent->depth);	//update the depths
				}
				else	//deleted the root node
				{		//Replace is now the root node.
					Replace->root->start = Replace;
	//				Replace->root->start->RecalcDepth(0);	//recalculate all depths
				}
			}	//end if balance to determine which side to replace from
		}	//end else if it has two children...

		ptrMove->left = NULL;	//now ptrMove is completely isolated from the tree, and if it pops up
		ptrMove->right = NULL;	//it won't propagate things erroneously.
		ptrMove->parent = NULL;


		if(ptrBalance != NULL)	//this must always be done on edits
		{
			ptrBalance->UpdateMaxDepthTreeUp();	//fix all depths from above
			ptrBalance->UpdateMaxDepthDown();	//now fix below as needed
		}
		if(prevbalance==false)
		{
			if(ptrBalance != NULL)	//start balancing from the parent of what was deleted
				ptrBalance->Balance(false);
			else	//there was no parent, so it was the root
				ptrMove->root->start->Balance(false);	//balance the root of the tree
		}

		//ptrRemove has been completely disassociated from the tree and the tree rebuilt
		//now do code to put it back in properly
		
		Tree<D,S>* ptrTree = ptrMove->root->start;
		Tree<D,S>* peek = ptrTree;

		S srt = ptrMove->Sort;
	
		while(peek != NULL)
		{
			ptrTree = peek;	//can ignore first time. If it successfully peeks(restarts loop),
			//know this is fine. If it doen't, then know to stop

			if(srt <= peek->Sort)	//want to know left side
				peek = ptrTree->left;
			else// if(srt > peek->Sort)	//want to know right side
				peek = ptrTree->right;
		}

		if(srt <= ptrTree->Sort)	//insert left
		{
			ptrTree->left = ptrMove;	//link the two together
			ptrMove->parent = ptrTree;
	//		InsTree->depth = ptrTree->depth + 1;	//set the depth of the node
		}
		else if(srt > ptrTree->Sort)
		{
			ptrTree->right = ptrMove;	//link the two together
			ptrMove->parent = ptrTree;
	//		InsTree->depth = ptrTree->depth + 1;	//set the depth of the node
		}

		ptrMove->maxdepth = ptrMove->root->GetDepth(ptrMove);	//it can't possibly have children,
				//so it's depth must be the maximum depth

		if(ptrMove->parent != NULL)
		{
			ptrMove->parent->UpdateMaxDepthDown();	//update all the depths below as needed
			if(prevbalance == false)
				ptrMove->parent->Balance(false);	//balance this insertion and all below it
		}	

		return(1);

	}

/////////////////////////////////////////////////////////////////
	int Remove(S srt, bool prevbalance)			//remove a node from the tree.
	{
	Tree<D,S>* ptrRemove = Find(srt);	//start searching from a given node
	Tree<D,S>* ptrBalance = NULL;
	if(ptrRemove != NULL)	//this does exist...
	{
		ptrRemove->root->size--;
/*
	Save the children!
*/
		Tree<D,S>* ChildL = ptrRemove->left;
		Tree<D,S>* ChildR = ptrRemove->right;
		Tree<D,S>* Parent = ptrRemove->parent;
		
		if(ChildL == NULL && ChildR == NULL)
		{
			if(Parent != NULL)	//break the link from the parent to the removed pointer. There was nothing above it.
			{
				if(Parent->left == ptrRemove)
					Parent->left = NULL;
				else
					Parent->right = NULL;
				ptrBalance = Parent;	//point to update all depths from and start balancing the tree
			}
			else
			{
				ptrRemove->root->start = NULL;
				ptrRemove->root->size = 0;	//no children, and no parent. last entry.
			}
			//this one checks out OK!
		}
		else if(ChildL == NULL && ChildR != NULL)
		{
			//ChildR is valid, so just move childR into the place of the present item
			ChildR->parent = Parent;	//left child doesn't mtter
			ptrBalance = ChildR;
			if(Parent != NULL)
			{
				if(Parent->right == ptrRemove)	//the child was on the right, so add it
				{		//to the right side of the parent
					Parent->right = ChildR;
				}
				else
				{	//the deleted node was to the left
					Parent->left = ChildR;
				}
			}
			else	//this was the root entry being deleted
			{
				ptrRemove->root->start = ChildR;	//set the starting point to this entry
			}
		}
		else if(ChildL != NULL && ChildR == NULL)	//childR does not exist
		{
			//ChildL is valid. move childL into place of the present item.
			ChildL->parent = Parent;
			ptrBalance = ChildL;
			if(Parent != NULL)
			{
				if(Parent->right == ptrRemove)
					Parent->right = ChildL;
				else
					Parent->left = ChildL;

//				Parent->RecalcDepth(Parent->depth);	//update the depths
			}
			else
			{
				ptrRemove->root->start = ChildL;
//				ChildL->RecalcDepth(0);	
			}

		}															//of everything above it
		else if(ChildL != NULL && ChildR != NULL)
		{	//things start to get really ugly...
			//first try the easy ways.
			int balance = 0;
			balance += ChildL->maxdepth;//MaxDepth(ChildL->depth);
			balance -= ChildR->maxdepth;//MaxDepth(ChildR->depth);
			//is it possible that maxdepth values are incorrect, so various assumptions might be incorrect, (occasionally)

			if(balance > 0)
			{	//the left side is deeper, so take one out of left side
				Tree<D,S>* Replace = ChildL->SearchRight();	//this spot will also be valid in what will be deleted
				//replace cannot = NULL. SearchRight() will return the node, and ChildL != null
				if(ChildL == Replace)
				{
					Replace->right = ChildR;	//there was nothing to replace->left (Search Left
					ChildR->parent = Replace;	//returns furthest left spot
					Replace->parent = Parent;
					ptrBalance = Replace;
					//Replace->right does not change in any manner. Just shifts with Replace.
				}
				else	//ChildL is not REplace. Therefore, Replace is at least one unit to right
				{		//of ChildL
					
					//Replace has no right children, but may have a left child
					Tree<D,S>* RepL = Replace->left;
					ptrBalance = Replace->parent;	//must be ChildL or one of it's right children

					if(RepL != NULL)
					{
						RepL->parent = Replace->parent;	//repL takes on replaces spot
						RepL->parent->right = RepL;	//replace was to the right of the parent
//						RepL->parent->RecalcDepth(RepL->parent->depth);	//update depths for balancing
						//Rep:'s right and left do not change (except depth)
					}
					else	//RepL doesn't exist.
					{
						Replace->parent->right = NULL;	//no more data for it to look at
					}
					ChildL->parent = Replace;	//the children of what was to be removed
					ChildR->parent = Replace;	//are now children of what is replacing the removed value
					Replace->right = ChildR;	//link back to them
					Replace->left = ChildL;
					Replace->parent = Parent;	//replace is fully linked
//					Replace->depth = ptrRemove->depth;
				}	//end else saying childL is not replace
				
				//now update the parent, which is the same regardless of the scenario
				if(Parent != NULL)
				{
					if(Parent->left == ptrRemove)
						Parent->left = Replace;
					else
						Parent->right = Replace;

//					Parent->RecalcDepth(Parent->depth);	//update the depths
				}
				else	//deleted the root node
				{		//Replace is now the root node.
					Replace->root->start = Replace;
//					Replace->root->start->RecalcDepth(0);	//recalculate all depths
				}
			}
			else
			{	//the right side is deeper/default, so take one out of right side
				Tree<D,S>* Replace = ChildR->SearchLeft();	//this spot will also be valid in what will be deleted

				if(ChildR == Replace)
				{
					Replace->left = ChildL;	//there was nothing to replace->left (Search Left
					ChildL->parent = Replace;	//returns furthest left spot
					Replace->parent = Parent;
					ptrBalance = Replace;
					//Replace->right does not change in any manner. Just shifts with Replace.
				}
				else	//ChildR is not REplace. Therefore, Replace is at least one unit to left
				{		//of ChildR
					
					//Replace has no left children, but may have a right child
					Tree<D,S>* RepR = Replace->right;	
					ptrBalance = Replace->parent;
					if(RepR != NULL)
					{
						RepR->parent = Replace->parent;	//repR takes on replaces spot
						RepR->parent->left = RepR;	//replace was to the left of its original parent (searchLeft)
//						RepR->parent->RecalcDepth(RepR->parent->depth);	//update depths for balancing
						//RepR's right and left do not change (except depth)
					}
					else	//RepR doesn't exist.
					{
						Replace->parent->left = NULL;	//no more data for it to look at
					}
					ChildL->parent = Replace;	//the children of what was to be removed
					ChildR->parent = Replace;	//are now children of what is replacing the removed value
					Replace->left = ChildL;	//link back to them
					Replace->right = ChildR;
					Replace->parent = Parent;	//replace is fully linked
	//				Replace->depth = ptrRemove->depth;
				}	//end else saying childR is not replace
				
				//now update the parent, which is the same regardless of the scenario
				if(Parent != NULL)
				{
					if(Parent->left == ptrRemove)
						Parent->left = Replace;
					else
						Parent->right = Replace;

	//				Parent->RecalcDepth(Parent->depth);	//update the depths
				}
				else	//deleted the root node
				{		//Replace is now the root node.
					Replace->root->start = Replace;
	//				Replace->root->start->RecalcDepth(0);	//recalculate all depths
				}
			}	//end if balance to determine which side to replace from
		}	//end else if it has two children...

		//ptrRemove has been completely disassociated from the tree
		//now rebuild the tree
		if(ptrBalance != NULL)	//this must always be done on edits
		{
			ptrBalance->UpdateMaxDepthTreeUp();	//fix all depths from above
			ptrBalance->UpdateMaxDepthDown();	//now fix below as needed
		}
		if(prevbalance==false)
		{
			

			if(ptrBalance != NULL)	//start balancing from the parent of what was deleted
			{
/*				if(ptrBalance->Verify())
				{
					ProgressString("Data is not OK before balancing on remove.");
				}*/
				ptrBalance->Balance(false);
			}
			else if(ptrRemove->root->start)	//there was no parent, so it was the root
				ptrRemove->root->start->Balance(false);	//balance the root of the tree
			//else it was the only one in there, so there was no balancing to be done
		}

		//null out the pointers for good measure
		ptrRemove->parent = NULL;
		ptrRemove->root = NULL;
		ptrRemove->left = NULL;
		ptrRemove->right = NULL;
	}	//end if ptrRemove != NULL
	
	
	
	delete (ptrRemove);	//free the memory
	ptrRemove = NULL;
	return(0);
	};	//end Remove function
	
////////////////////////////////////////////////////////////////////////	

	Tree<D,S>* FindPtr(S* sort)
	{
		Tree<D,S>* present = this;
		Tree<D,S>* RetVal = NULL;
		S srt = *sort;
		while(present != NULL)
		{
			if(srt <= present->Sort)	//could it be to the left
			{
				if(sort == &(present->Sort))	//is this the correct one?
				{
					RetVal = present;	//yes, set return and get out
					break;
				}
				present = present->left;	//no, keep searching
			}
			else	//it's to the right
				present = present->right;
		}

		return(RetVal);	//returns null if not found
	}

///////////////////////////////////////////////////////////////

	Tree<D,S>* Find(S srt)		//returns a pointer to the node with the first given sort field
	{
	Tree<D,S>* present = this;
	Tree<D,S>* RetVal = NULL;
	while(present != NULL)
	{
		if(srt == present->Sort)
		{
			RetVal = present;
			break;
		}
		else if(srt < present->Sort)
			present = present->left;
		else
			present = present->right;
	}

	return(RetVal);	//returns null if not found
	};	//end find function

//////////////////////////////////////////////

	D* FindPtrData(S srt)		//returns a pointer to the node with the first given sort field
	{	//the search must go through the actual tree.
	
	D* ptr = NULL;
	
	if(srt == this->Sort)
	{
		ptr = &(this->Data);
		return(ptr);
	}
	else if(srt < this->Sort)
	{
		if(this->left != NULL)
			ptr = this->left->FindPtrData(srt);	//add the smaller values to the left
	}
	else
	{
		if(this->right != NULL)
			ptr = this->right->FindPtrData(srt);	//add the smaller values to the left
	}

	return(ptr);	//returns null if not found
	};	//end find function

///////////////////////////////////////////////////////////////

	Tree<D,S>* SearchLeft(void)	//returns the node furthest to the left from a given node
	{
	Tree<D,S>* ptrTree = this;
	while(ptrTree->left != NULL)
		ptrTree = ptrTree->left;

	return(ptrTree);
	};		//end search left code

//////////////////////////////////////////////////////////////////////

	Tree<D,S>* SearchRight(void)	//returns the node furthest to the right from a given node
	{
	Tree<D,S>* ptrTree = this;
	while(ptrTree->right != NULL)
		ptrTree = ptrTree->right;

	return(ptrTree);
	};	//end search right code

///////////////////////////////////////////////////////////////////////

	void RotateRight(void)	//rotates the binary tree with the present node as the root
	{
	Tree<D,S>* rt = this;
	Tree<D,S>* ChildL = left;
	Tree<D,S>* Parent = parent;
	
	if(ChildL == NULL)
		return;	//nothing to rotate

	Tree<D,S>* ChildLR = ChildL->right;

	//ChildL becomes root
	//root goes to right of childL
	//right child of childL goes to left child of root

	//update the parent link
	if(Parent != NULL)
	{
		if(Parent->right == rt)
			Parent->right = ChildL;
		else
			Parent->left = ChildL;
	}
	else
		rt->root->start = ChildL;
	ChildL->parent = rt->parent;
	ChildL->right = rt;
	//ChildR->right doesn't change
	rt->parent = ChildL;
	rt->left = ChildLR;
	//root->right doesn't change

	if(ChildLR != NULL)
		ChildLR->parent = rt;
	//ChildLR children don't change

//	ChildL->RecalcDepth(root->depth);
//depth removed. maxdepth refigured out after operations in insert, remove, and balance.
	return;
	
	};	//end rotate right code

//////////////////////////////////////////////////////////

	void RotateLeft(void)	//rotates the binary tree with the present node as the root
	{
	Tree<D,S>* rt = this;
	Tree<D,S>* ChildR = right;
	Tree<D,S>* Parent = parent;
	
	if(ChildR == NULL)
		return;	//nothing to rotate

	Tree<D,S>* ChildRL = ChildR->left;

	//ChildR becomes root
	//root goes to left of childR
	//left child of childR goes to right child of root

	//update the parent link
	if(Parent != NULL)
	{
		if(Parent->left == rt)
			Parent->left = ChildR;
		else
			Parent->right = ChildR;
	}
	else	//there was no parent as it was the root node
		rt->root->start = ChildR;	//root of this turn to root of tree to starting position of tree

	ChildR->parent = rt->parent;
	ChildR->left = rt;
	//ChildL->left doesn't change
	rt->parent = ChildR;
	rt->right = ChildRL;
	//root->right doesn't change
	if(ChildRL != NULL)
		ChildRL->parent = rt;
	//ChildLR children don't change

//	ChildR->RecalcDepth(root->depth);
//depth removed. maxdepth calculated at end of insert, remove, and balance as needed.
//rotations should be an internal function...
	return;
	};	//end rotate left code

//////////////////////////////////////////////////////////

	Tree<D,S>* Verify(void)	//try to find how tree is corrupt or not.
	{
		Tree<D,S>* ret = NULL;
		if(right > reinterpret_cast<Tree<D,S>*>(0x00000060))	//all the addresses that screw up seem to be relatively small 0x0000040 (64)
			ret = right->Verify();
		else if(right <=reinterpret_cast<Tree<D,S>*>(0x00000060) && right != NULL)
			ret = this;

		if(left > reinterpret_cast<Tree<D,S>*>(0x00000060))
			ret = left->Verify();
		else if(left <= reinterpret_cast<Tree<D,S>*>(0x00000060) && left != NULL)
			ret = this;

		if(parent != NULL)
		{
			if(parent->left != this && parent->right != this)
			{
				ProgressString("Child has incorrect parent pointer");
				ret = this;
			}
			if(parent->root != root)
			{
				ProgressString("Parent/Child have different roots");
			}
		}
		else
		{
			if(root->start != this)
			{
				ProgressString("First element in tree has non-matching root");
			}
		}



		return(ret);
	}

};	//end class Tree<D,S>

/******************************************************************
*
*
*
*
*******************************************************************/

template <class D, class S>
class TreeRoot
{
public:
	unsigned int size;	//keep a running record of the number of entries in the tree
	Tree<D,S>* start;		//pointer to first tree node
	
//////////////////////////FUNCTIONS///////////////////////////////////////

	TreeRoot()			//constructor for the tree root
	{
		size = 0;
		start = NULL;
		Clear();
	};

/////////////////////////////////////////////////////////////////

	~TreeRoot()
	{ 
		Clear();
		start = NULL;
		size = 0;
	};	//destructor for the tree root.

//////////////////////////////////////////////////////////////////////

	void UpdateRoot(void)
	{
		if(this->start != NULL)
			this->start->UpdateRoot(this);
		return;
	}
//////////////////////////////////////////////////////////////////////
	int CopyToNewTree(TreeRoot<D,S>* NewRoot)	//copies the entire tree and children over to a new tree
	{
		if(this == NewRoot)
			return(0);

		if(this->start)
			this->start->CopyToNewTree(NewRoot, true);

		return(0);
	}

////////////////////////////////////////////////////////////
	Tree<D,S>* Add(D Dat, S srt)		//used to add the data of the tree together. Then default to insert if not present
	{
	Tree<D,S>* ptrTree = FindParent(srt);
	if(ptrTree != NULL)
	{
		if(srt < ptrTree->Sort)
		{
			if(ptrTree->left == NULL)
			{
				ptrTree = ptrTree->Insert(Dat, srt);	//so add it
				return(ptrTree);
			}
		}
		else if(srt > ptrTree->Sort)
		{
			if(ptrTree->right == NULL)
			{
				ptrTree = ptrTree->Insert(Dat, srt);
				return(ptrTree);
			}
		}
		ptrTree = ptrTree->Find(srt);	//should only have to search 1 depth, and it should always work
	}
	else	//couldn't find parent because it was root
	{
		if(start == NULL)	//there's nothing to start from...
		{
			Tree<D,S>* InsTree = NULL;
			try {
			 InsTree = new Tree<D,S>(Dat, srt);	//add the root
			}
			catch (...)
			{
				ProgressCheck(57, false);
				return(NULL);
			}
			start = InsTree;
//			InsTree->depth = 0;
			InsTree->maxdepth = 0;
			InsTree->root = this;
			this->size=1;
			return(InsTree);
		}
		ptrTree = start;
	}
	//it found the proper spot, and now needs to be adjustd

	ptrTree->Data += Dat;

//	VerifyTree();

	return(ptrTree);
	};	//end add function

/////////////////////////////////////////////////////////////////

	int Multiply(D Dat)	//multiplies the entire tree by a given constant
	{
		this->start->Multiply(Dat);
		return(0);
	}

	int MultiplySrt(S srt)	//multiplies the entire tree by a given constant
	{
		this->start->MultiplySrt(srt);
		return(0);
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* InsertExistingNode(Tree<D,S>* InsTree)
	{
		
		if(start == NULL)	//there is a first entry to start from
		{
			InsTree->left = NULL;
			InsTree->right = NULL;
			InsTree->parent = NULL;
			InsTree->root = this;
			InsTree->maxdepth = 0;
			start = InsTree;
			size++;
			return(InsTree);
		}

		Tree<D,S>* ptrTree = NULL;
		Tree<D,S>* peek = start;

		while(peek != NULL)
		{
			ptrTree = peek;	//can ignore first time. If it successfully peeks(restarts loop),

			if(InsTree->Sort <= peek->Sort)	//want to know left side
				peek = ptrTree->left;
			else// if(srt > peek->Sort)	//want to know right side
				peek = ptrTree->right;
		}

		InsTree->root = this;	//set the root for the tree node.
		InsTree->left = NULL;
		InsTree->right = NULL;

		if(InsTree->Sort <= ptrTree->Sort)	//insert left
		{
			ptrTree->left = InsTree;	//link the two together
			InsTree->parent = ptrTree;

		}
		else if(InsTree->Sort > ptrTree->Sort)
		{
			ptrTree->right = InsTree;	//link the two together
			InsTree->parent = ptrTree;
		}
		size++;	//increment size of root
		InsTree->maxdepth = InsTree->root->GetDepth(InsTree);	//it can't possibly have children,
		//so it's depth must be the maximum depth

		if(InsTree->parent != NULL)
		{
			InsTree->parent->UpdateMaxDepthDown();	//update all the depths below as needed
			InsTree->parent->Balance(false);	//balance this insertion and all below it
		}	
		return(InsTree);
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* Insert(D Dat, S srt)	//insert node into an appropriate open branch below this branch
	{	//starter function for inserting node into tree. Also handles chance there is nothing present
		if(start != NULL)	//there is a first entry to start from
		{
			Tree<D,S>* ptr = start->Insert(Dat, srt);
//			VerifyTree();	this is now done at the tree node
			return(ptr);
		}
		Tree<D,S>* NewTree = NULL;
		try
		{
			NewTree = new Tree<D,S>(Dat, srt);
		}
		catch (...)
		{
			ProgressCheck(56, false);
			return(NULL);
		}
		start = NewTree;
	//	NewTree->depth = 0;
		NewTree->maxdepth = 0;
		NewTree->root = this;
		size++;	//refers to total size in the tree
//		VerifyTree();
		return(NewTree);
	};	//end function insert

/////////////////////////////////////////////////////////////////

	int RemoveRef(S* ptrSrt, bool prevbalance)
	{
		if(start != NULL)
			start->RemovePtr(ptrSrt, prevbalance);

//		VerifyTree();

		return(0);
	}

	int Remove(S srt, bool prevbalance)			//remove specified sort value from tree
	{
	if(start != NULL)
		start->Remove(srt, prevbalance);

//	VerifyTree();
	return(0);
	};	//end function remove

/////////////////////////////////////////////////////////////////

	void Clear()				//remove all entries from the tree
	{
	//kill everything. Do this recursively
	if(start!=NULL)
		start->KillSelfandChildren();
	start = NULL;
	size = 0;

	return;
	};	//end function clear

/////////////////////////////////////////////////////////////////

	Tree<D,S>* Replace(D Dat, S Srt)
	{
		Tree<D,S>* present = start;
		Tree<D,S>* RetVal = NULL;

		while(present != NULL)
		{
			if(Srt == present->Sort)
			{
				present->Data = Dat;
				RetVal = present;
				break;
			}
			else if(Srt < present->Sort)
				present = present->left;
			else
				present = present->right;
		}
		if(RetVal == NULL)
			RetVal = Insert(Dat, Srt);

		return(RetVal);	//returns null if not found
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindPtr(S* sort)
	{
		Tree<D,S>* present = start;
		Tree<D,S>* RetVal = NULL;
		S srt = *sort;
		while(present != NULL)
		{
			if(srt <= present->Sort)	//could it be to the left
			{
				if(sort == &(present->Sort))	//is this the correct one?
				{
					RetVal = present;	//yes, set return and get out
					break;
				}
				present = present->left;	//no, keep searching
			}
			else	//it's to the right
				present = present->right;
		}

		return(RetVal);	//returns null if not found
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* Find(S srt)		//returns a pointer to the node with the given sort field
	{
	Tree<D,S>* present = start;
	Tree<D,S>* RetVal = NULL;
	while(present != NULL)
	{
		if(srt == present->Sort)
		{
			RetVal = present;
			break;
		}
		else if(srt < present->Sort)
			present = present->left;
		else
			present = present->right;
	}

	return(RetVal);	//returns null if not found
	};	//end function find

	///////////////////////////////

	D* FindPtrData(S srt)		//returns a pointer to the node with the first given sort field
	{	//the search must go through the actual tree.
	
		D* ptr = NULL;
		if(this->start != NULL)
			ptr = this->start->FindPtrData(srt);


	return(ptr);	//returns null if not found
	};	//end find function

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindFirstLessEqual(S srt)
	{
		Tree<D,S>* top = Find(srt);	//traverse down the path it will be on
		if(top != NULL)	//found one equal to
			return(top);

		//there are none with the proper sort value, so find where it would go
		top = FindParent(srt);
		//top will be null if there are none, or if the first entry was correct.  But won't get here
		//if first entry was correct

		Tree<D,S>* below = NULL;	//the largest number below it

		while(top != NULL)
		{
			if(top->Sort <= srt)	//was it less than the target?
			{
				if(below != NULL)
				{
					if(top->Sort > below->Sort)
						below = top;	//move it closer to the value while being less than or equal to it
				}
				else	//below does not exist yet
					below = top;
			}
			top = top->parent;	//move down the list
		}
		
		return(below);
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindFirstGreater(S srt)
	{
		Tree<D,S>* top = Find(srt);	//traverse down the path it will be on
		if(top == NULL)	//this one does not exist
		{
			top = FindParent(srt);	//will only return null if there are no entries
		}
		else	//it found the particular entry
		{
			//want to look to the right of this particular entry, and then as far left as possible
			if(top->right != NULL)
				top = top->right->SearchLeft();
		}
		Tree<D,S>* above = NULL;	//the largest number above it
		while(top != NULL)
		{

			if(top->Sort > srt)	//is this above or below the target?
			{					//it's above
				if(above != NULL)	//there previously is an above
				{
					if(top->Sort < above->Sort)	//is this closer to the actual (smaller)
						above = top;	//then replace above, which is the "next" value
				}
				else
					above = top;	//there is no previous value, so just set it
			}

			top = top->parent;	//move down the list
		}
		
		return(above);
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindFirstGreaterEqual(S srt)
	{
		Tree<D,S>* top = Find(srt);	//traverse down the path it will be on
		if(top != NULL)	//this value exists
		{
			return(top);
		}
		top = FindParent(srt);	//will only return null if there are no entries or the top one is correct

		Tree<D,S>* above = NULL;	//the largest number above it

		while(top != NULL)
		{

			if(top->Sort >= srt)	//is this above or below the target?
			{					//it's above
				if(above != NULL)	//there previously is an above
				{
					if(top->Sort < above->Sort)	//is this closer to the actual (smaller)
						above = top;	//then replace above, which is the "next" value
				}
				else
					above = top;	//there is no previous value, so just set it
			}

			top = top->parent;	//move down the list
		}
		
		return(above);
	}

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindFirstLess(S srt)
	{
		//first find the node which is less than it but the right child is greater (if not exist, then done)
		//record that node, move over to the right, and then search down the left until either
		//1)the value is now less than the sort value, at which point record that node and start over
		//2)the furthest left node is still greater than sort
		//top will traverse down the path it will be on
		Tree<D,S>* top = Find(srt);	//traverse down the path it will be on
		if(top == NULL)	//this one does not exist
		{
			top = FindParent(srt);	//will only return null if there are no entries
		}
		else	//it found the particular entry
		{
			//want to look to the left of this particular entry, and then as far right as possible
			if(top->left != NULL)
				top = top->left->SearchRight();
		}
		Tree<D,S>* below = NULL;	//the largest number below it
//		Tree<D,s>* above = NULL;	//the largest number above it

		while(top != NULL)
		{
			if(top->Sort < srt)	//was it less than the target?
			{
				if(below != NULL)
				{
					if(top->Sort > below->Sort)
						below = top;	//move it closer to the value while being less than or equal to it
				}
				else	//below does not exist yet
					below = top;
			}
			top = top->parent;	//move down the list
		}
		
		return(below);
	}

	/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindParent(S srt)	//returns a pointer to the parent of the given node with sort value
	{
	Tree<D,S>* present = start;
	Tree<D,S>* RetVal = NULL;

	while(present != NULL)
	{
		if(srt == present->Sort)
			break;

		RetVal = present;
		
		if(srt < present->Sort)
			present = present->left;
		else
			present = present->right;
	}

	return(RetVal);	//returns null if not found
	};	//end function findparent

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindParentPtr(S* sort)	//returns a pointer to the parent of the given node with sort value
	{
	Tree<D,S>* present = start;
	Tree<D,S>* RetVal = NULL;
	S srt = *sort;

	while(present != NULL)
	{
		if(sort == &(present->Sort))
			break;

		RetVal = present;
		
		if(srt <= present->Sort)
			present = present->left;
		else
			present = present->right;
	}

	return(RetVal);	//returns null if not found
	};	//end function findparentptr

/////////////////////////////////////////////////////////////////

	Tree<D,S>* FindPath(int path, int depth)	//path is a bunch of 0's and 1's. shift, if 0, go left, if 1, go right.
	{
	Tree<D,S>* RetValue = start;

	while(RetValue != NULL && depth != 0)
	{
		if(path%2 == LEFT)	//check first bit for left or right
			RetValue = RetValue->left;	//it was left, so traverse left
		else	//check first bit for left or right
			RetValue = RetValue->right;	//it was left, so traverse left

		depth--;	//moved down another depth from the desired outcome
		path = path >> 1;	//divide by 2, or pop out the front bit to get the next command

	}
	return(RetValue);
	};	//end function find path

/////////////////////////////////////////////////////////////////

	void GenerateNodeList(vector<Tree<D,S>*>& nodes)
	{
		nodes.clear();
		nodes.reserve(size);

		if(start)
			start->GenerateNodeList(nodes);

		return;
	}

/////////////////////////////////////////////////////////////////

	void GenerateSort(vector<D>& srtD)	//starter function to creating a vector of data values in order
	{
	srtD.clear();
	srtD.reserve(size);	//allow enough space in the sorted vector

	if(start)
	{
		start->GenerateSort(srtD);	//calls the tree specific function that branches out recursively
	}
	return;
	};	//end function GenerateSort

/////////////////////////////////////////////////////////////////

	void GenerateSortPtr(vector<D*>& srtD)	//starter function to creating a vector of data values in order
	{
		Tree<D,S>* ptr = start;
		srtD.clear();
		srtD.reserve(this->size);	//allow enough space in the sorted vector

		if(ptr != NULL)
		{
			ptr->GenerateSortPtr(srtD);	//calls the tree specific function that branches out recursively
		}
		return;
	};	//end function GenerateSort

/////////////////////////////////////////////////////////////////

	void GenerateSortS(vector<S>& srtS)	//starter function to creating a vector of data values in order
	{
		srtS.clear();
		srtS.reserve(this->size);	//allow enough space in the sorted vector
		if(start != NULL)
			start->GenerateSortS(srtS);	//calls the tree specific function that branches out recursively

		return;
	};	//end function GenerateSort

/////////////////////////////////////////////////////////////////

	void GenerateSortSPtr(vector<S*>& srtS)	//starter function to creating a vector of data values in order
	{
		srtS.clear();
		srtS.reserve(size);	//allow enough space in the sorted vector

		if(start != NULL)
			start->GenerateSortSPtr(srtS);	//calls the tree specific function that branches out recursively
	
		return;
	};	//end function GenerateSort

/////////////////////////////////////////////////////////////////

	int GetDepth(Tree<D,S>* target)	//gets the depth of a particular node.
	{					//it's basically a find function that keeps track how far it must go in.
		int depth = 0;
		Tree<D,S>* present = target;

		while(present != NULL)
		{
			present = present->parent;
			depth++;
		}

		if(this->start == NULL)
		{
			depth = -1;
		}
		return(depth);
	};	//end GetDepth function

	/////////////////////////////////////////////////////////////////

	D SumData(void)
	{
		D sum = 0;
		vector<D*> items;
		GenerateSortPtr(items);

		for(unsigned int i=0; i<items.size(); i++)
			sum += *(items[i]);
		return(sum);
	}	//end sum all data function

	/////////////////////////////////////////////////////////////////

	S SumDataSrt(void)
	{
		S sum = 0;
		vector<S*> items;
		GenerateSortSPtr(items);

		for(unsigned int i=0; i<items.size(); i++)
			sum += *(items[i]);
		return(sum);
	}	//end sum all data function
/////////////////////////////////////////////////////////////////

	void DisplayTree(fstream& fileout)	//starter function for displaying all values in the tree
	{
		if(start != NULL)
			start->DisplayNodes(fileout);

	return;
	};	//end function display tree
/////////////////////////////////////////////////////////////////

	Tree<D,S>* TransferNode(Tree<D,S>* TrnsTree)	//Transfers a node from a tree over to this one
	{	
	D dat = TrnsTree->Data;	//make backup of copies
	S srt = TrnsTree->Sort;

	Tree<D,S>* ptr;

	if(TrnsTree->root != NULL)	//first disassociate it with the previous tree
	{
		TrnsTree->root->Remove(TrnsTree->Sort);
	}
	else
		TrnsTree->KillSelfandChildren();	//get rid of node. It's not associated with a tree.
											//also make sure children aren't sent over too
	
	ptr = Insert(dat, srt);	//insert the saved data

	return(ptr);
	};	//end function Transfer Node

//////////////////////////////////////////////////////////////

	bool VerifyTree()
	{
		if(start)
		{
			Tree<D,S>* node = start->Verify();
			if(node)
			{
				ofstream file;
				file.open("corrupt.dat", fstream::out);
				file << "Size: " << size << endl;
				start->DisplayBuggedTree(file);
				ProgressString("Invalid tree on add or remove");
				return(false);
			}
		}
		return(true);
	}

};	//end treeroot class

#endif