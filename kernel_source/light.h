#ifndef SAAPD_LIGHT_H
#define SAAPD_LIGHT_H

#include <map>
#include <vector>
#include <set>
#include <list>
#include <cstdint>
#include "BasicTypes.h"

#define SUCCESS 1
#define INVALID 0

#define ENERGY_CUTOFF_EMIT_PHOTON 0.1
#define INTENSITY_EMIT_CUTOFF 1e-4

#define ABSORB true
#define EMIT false
#define LIGHT_INTENSITY_CUTOFF 1e-6	//it will have to have absorbed 99.999% of the light

#define SPECTRUM_INTENSITY true
#define SPECTRUM_INTENS_DERIV false

#define SPECTRUM_DISTR_UNDEFINED 0
#define SPECTRUM_DISTR_STATE_EQUAL 1
#define SPECTRUM_DISTR_VBSTATE_SMALL 2
#define SPECTRUM_DISTR_VBSTATE_LARGE 3
#define SPECTRUM_DISTR_ZERO_SRCWIDTH 4
#define SPECTRUM_DISTR_ZERO_TRGWIDTH 5
#define SPECTRUM_DISTR_ZERO_SRCTRGWIDTH 6

#define EMIT_NONE 0	//which portions have been calculated already?
#define EMIT_PX 1
#define EMIT_NX 2
#define EMIT_PY 4
#define EMIT_NY 8
#define EMIT_PZ 16
#define EMIT_NZ 32
#define EMIT_PXPY 64
#define EMIT_NXPY 128
#define EMIT_PYPZ 256
#define EMIT_PYNZ 512
#define EMIT_PXNY 1024
#define EMIT_NXNY 2048
#define EMIT_NYPZ 4096
#define EMIT_NYNZ 8192
#define EMIT_PXPZ 16384
#define EMIT_NXPZ 32768
#define EMIT_PXNZ 65356
#define EMIT_NXNZ 131072
#define EMIT_PXPYPZ 262144
#define EMIT_NXPYPZ 524288
#define EMIT_PXNYPZ 1048576
#define EMIT_NXNYPZ 2097152
#define EMIT_PXPYNZ 4194304
#define EMIT_NXPYNZ 8388608
#define EMIT_PXNYNZ 16777216
#define EMIT_NXNYNZ 33554432


class ELevel;
class Shape;
class Point;
class Model;
class ModelDescribe;
class Simulation;
class ELevelRate;
class kernelThread;
class XMLFileOut;


class LightVariation
{
public:
	double boltzFitV;	//fitting of exponential term where the holes are
	double boltzFitC;	//fitting of exponential term where the electrons are
	ELevel* elecSrcC;	//energy state that electrons fell from
	ELevel* holeSrcV;	//energy state that electrons fell into
	LightVariation();
	~LightVariation();	//doesn't really do much
	//the above allows finding width of the energy levels, the gap between the levels, and the number of states
};

class Wavelength
{
public:
	float lambda;	//wavelength of light in nm
	double intensity;		//intensity of light as mW/cm^2
//Search for LIGHT_OLD_VARIABLES to add these back in.
//	double collimation;	//how much spread is there from the direction (radians, pi basicaloly means point source)
//	double coherence1;	//gaussian width for E-Field1
//	double coherence2;	//gaussian width for EField 2
	double energyphoton;	//energy of this light in eV per photon
	double cutOffPower;	//energy that the raytrace should stop existing
	uint32_t DirEmitted;	//which directions have been emitted so far - matters if direction=0 on light source. Make sure 32 bits (needs 26)
	bool IsCutOffPowerInit;	//flag to check if the init cut off power is done yet.
	
	void SaveToXML(XMLFileOut& file);
	Wavelength operator+(const Wavelength &rhs) const;
	Wavelength& operator+=(const Wavelength &rhs);
	Wavelength();
	~Wavelength();	//deletes the emission data when destroyed
};

class LightSource
{
public:
	std::map<double, Wavelength> light;	//what are the lights in this source. first is energy of photon, second is associated data
	std::map<double, bool> times;	//when does the spectrum turn on and off?
	double EnergyNextUse;	//adjust algorithm to do one energy wavelength at a time for good memory/calculation time management
	bool enabled;	//is this spectrum currently enabled
	bool inputType;	//do I take this as mW / cm^2 / nm or just mW / cm^2
	std::string name;	//name for the spectrum if more than one
	Shape* sourceShape;	//the shape made where the light comes from - NULL is infinite plane perp to direction containing point center
	XSpace center;	//this is the center position for the sourceShape
	XSpace direction;	//the direction the light is shining
	bool centerinit;	//has the center been initialized appropriately?
	int ID;	//generic ID to easily identify the light source
	std::vector<Point*> IncidentPoints;	//what points should light from this spectrum hit. Will often just be one point
	//but in 2D more.  If light comes from multiple directions, this might be beneficial...


	void SaveToXML(XMLFileOut& file);
	void AddWavelength(float wavelength, double intensity);
	LightSource();
	~LightSource();	//deletes all the wavelength data associated with this light source, and thereby emission data as well
};

class RayTrace
{
public:
	Wavelength* waveData;	//pointer to wavelenth data in LightSource - do not delete
	XSpace direction;	//direction of the light
	XSpace position;	//position of the light
	//	unsigned long long int num;		//number of photons in the light packet - if it exceeds this value, start a new one...
	double powerLeft;	//energy left in photon in mW

	Point* curPt;	//the current point the photon packet is in - do not delete
	XSpace EField1;	//important in future when adding diffraction, interference, polarized light
	XSpace EField2;	//important in future when adding diffraction, interference, polarized light
	bool enteredDevice;
	bool inNonexistentVolume;	//a flag used to say that in the current point, it is part of the region not occupied by material
	double cutoffPowerRedFactor;	//factor to reduce the cutoff power by in event most incoming light is reflected.
	int reflectCount;	//number of reflections that have occurred for ray tracing.
	RayTrace();
	RayTrace(LightSource* ltSrc, Wavelength& wv);
	RayTrace(RayTrace* copyRay);
	double GetAlpha();
	int Initialize(LightSource* ltSrc, Wavelength& wv);
	int Initialize(RayTrace* copyRay);
	int Initialize(void);
	~RayTrace();
};



class OpSort
{
public:
	float wavelength;
	LightVariation* LSpec;
	OpSort();
	~OpSort();	//does not delete data in LSPec

	bool operator ==(const OpSort &b) const;
	bool operator !=(const OpSort &b) const;
	bool operator <=(const OpSort &b) const;
	bool operator >=(const OpSort &b) const;
	bool operator <(const OpSort &b) const;
	bool operator >(const OpSort &b) const;
};



class SourceSpectrumDistribution
{
	LightVariation* ltVar;	//this is the primary input
	double Egap;
	double SrcWidth;
	double TrgWidth;
	double CoeffSrc;
	double CoeffTrg;
	double NumSrc;
	double NumTrg;
	// ^^ individual variables
	// vv avoid repeated calculations over and over again
	double LinCoeff;
	double ExpConst;	//what the exponential coefficient should be that is constant throughout.
	double CoeffDif;	//CoeffTrg - CoeffSrc. IF 0, resort to U - L
	double invCoeffDif;	//1.0 / CoeffDif
	double transition1;	//point where it goes from both partial to 1 (or both) being fully used
	double transition2;	//point where it goes back to both being partial
	double transitionend;	//EGap + SrcWidth + TrgWidth
	int which;	//0 undefeind, 1 equal, 2 VB smaller, 3 VB larger, 4 Source Width = 0.0, 5 TargetWidth = 0.0, 6 BothWidth = 0
	//double transitionStart = Egap;

	std::map<double, double> fnVals;	//data is f(del_E), sort is del_E. Every time GetIntensity is called, it stores the value in here!
	double IntegDelE;	//what the integral over del_E is, so now there's a total to divide all the values by for normalization
	double IntegDelEI;
	bool goodIntegral;

public:
	int Init(LightVariation* val);
	double GetIntensity(double energy);
	double GetStart() { return(Egap); };
	double GetEnd() { return(transitionend); };
	double GetTrans1() { return(transition1); };
	double GetTrans2() { return(transition2); };
	double GetIntegral();
	double GetIntegralI();
	void SetArbEnd(double val) { transitionend = val; return; };
	double GenerateFunction(int divisions, std::vector<double>& choices);	//divisions is how many times to bisect
	double GenerateFunction(double start, double width, std::vector<double>& choices);	//goes based off fitting an earlier distribution
	double AcquireValue(double energy);	//this gets the normalized value of the function
	int Clear();	//Does not delete data in LightVariation

};

class BulkMapData {
	std::vector<double> values;	//these two should always be lined up
	std::vector<unsigned char> types;
public:
	BulkMapData() {	}
	~BulkMapData() { Clear(); }

	unsigned int AddValue(double val, unsigned char type, unsigned int hint = -1) {
		if (hint < types.size() && types[hint] == type) {
			values[hint] += val;
			return hint;
		}
		for (unsigned int i = 0; i < types.size(); ++i) {
			if (types[i] == type) {
				values[i] += val;
				return i;
			}
		}
		types.push_back(type);
		values.push_back(val);
		return (types.size() - 1);
	}
	double GetValue(unsigned char type, unsigned int hint = -1) {
		if (hint < types.size() && types[hint] == type)
			return values[hint];

		for (unsigned int i = 0; i < types.size(); ++i) {
			if (types[i] == type)
				return values[i];
		}
		return 0.0;
	}
	bool ClearType(unsigned char type, unsigned int hint = -1) {
		if (hint < types.size() && types[hint] == type) {
			values.erase(values.begin() + hint);
			types.erase(types.begin() + hint);
			return true;
		}
		for (unsigned int i = 0; i < types.size(); ++i) {
			if (types[i] == type) {
				values.erase(values.begin() + i);
				types.erase(types.begin() + i);
				return true;
			}
		}
		return false;
	}
	unsigned int GetHint(unsigned char type, unsigned int prevHint = -1) {	//created to avoid needlessly searching through all the types repeatedly for a particular index
		if (prevHint < types.size() && types[prevHint] == type)
			return prevHint;
		for (unsigned int i = 0; i < types.size(); ++i) {
			if (types[i] == type)
				return i;
		}
		return -1;
	}
	void Clear() {
		values.clear();
		types.clear();
	}
	unsigned int getNumTypes() { return types.size(); }
	void Multiply(double d) {
		for (auto it = values.begin(); it != values.end(); ++it)
			(*it) *= d;
	}
	void AddValues(const BulkMapData& other, double factor) {
		for (unsigned int i = 0 ; i< other.values.size(); ++i) {
			AddValue(other.values[i] * factor, other.types[i]);
		}
	}

	const std::vector<unsigned char>& getTypes() const { return types; }
};

class BulkOpticalData
{
	std::map<double, BulkMapData> AllData;	//the indices always line up. Lots of random inserts and accesses by energy
	std::map<double, BulkMapData>::iterator lastAccessed;	//process things one energy at a time. Do a log_n search once per wavelength
public:
	//memory is an issue when processing fine optical resolutions. For a fully active photon,
	//this setup reduces the memory (ignoring iterators, links, and other internal variables, etc in map structures) from
	//224 bytes to 134 bytes.
	
	/*
	std::map<double, double> LightIncident;	//total photons incident during simulation, sorted by wavelength
	std::map<double, double> LightEnter;	//total photons that actually make it into the device
	std::map<double, double> LightLeavePos;	//total light photons leaving the device
	std::map<double, double> LightLeaveNeg;	//total light photons leaving the device
	std::map<double, double> LightCarrierGen;	//VB->CB induced by light
	std::map<double, double> LightSRH;	//carriers impurity/band transitions induced by light (e- increase energy)
	std::map<double, double> LightDef;	//carrier impurity/impurity transition induced by light (e- increase energy)
	std::map<double, double> LightScat;	//carrier scattering transition induced by light (e- increase energy)
	std::map<double, double> StimEmisRec;	//carrier CB->VB induced by light
	std::map<double, double> StimEmisSRH;	//carriers impurity/band induced by light (e- decreases energy)
	std::map<double, double> StimEmisDef;	//carriers impurity/impurity transition induced by light (e- decrease energy)
	std::map<double, double> StimEmisScat;	//carriers scattering in band from light (e- decreases energy)
	std::map<double, double> GeneratedEg;	//generated light band to band
	std::map<double, double> GeneratedSRH;	//generated light from srh
	*/
	int AddExistingBulkData(BulkOpticalData* otherTree, double factor);
	unsigned int AddData(double energy, unsigned char type, double val, unsigned int hint = -1);	//returns hint for type location. Assumes the exact value is being passed in

	void ClearAll(void);
	BulkOpticalData();
	~BulkOpticalData();

	static const unsigned char generatedEg = 0;
	static const unsigned char generatedSRH = 1;
	static const unsigned char lightIncident = 2;
	static const unsigned char lightEnter = 3;
	static const unsigned char absorbBands = 4;
	static const unsigned char stimEmisBands = 5;
	static const unsigned char absorbSRH = 6;
	static const unsigned char stimEmisSRH = 7;
	static const unsigned char absorbDef = 8;
	static const unsigned char stimEmisDef = 9;
	static const unsigned char absorbScat = 10;
	static const unsigned char stimEmisScat = 11;
	static const unsigned char lightLeavePos = 12;
	static const unsigned char lightLeaveNeg = 13;
	static const unsigned char lastType = 13;

private:
	unsigned int ctrs[lastType + 1];	//go based off the defined consts above
	unsigned int ctrIncident;	//in AllData, these sizes aren't actually obvious.
	unsigned int ctrEnter;
	unsigned int ctrLeavePos;
	unsigned int ctrLeaveNeg;
	unsigned int ctrAbsorbEg;
	unsigned int ctrAbsorbSRH;
	unsigned int ctrAbsorbDef;
	unsigned int ctrAbsorbScat;
	unsigned int ctrSEeg;
	unsigned int ctrSEsrh;
	unsigned int ctrSEdef;
	unsigned int ctrSEscat;
	unsigned int ctrGenEG;
	unsigned int ctrGenSRH;
	unsigned int ctrGenDef;
	void ResetCounters() {
		for (int i = 0; i < lastType; ++i)
			ctrs[i] = 0;

		for (auto it = AllData.begin(); it != AllData.end(); ++it) {
			for (auto it2 = it->second.getTypes().begin(); it2 != it->second.getTypes().end(); ++it2) {
				if (*it2 <= lastType)
					ctrs[*it2] += 1;
			}
		}
	}
};


#define OPTIC_WEIGHT_FE true
#define OPTIC_WEIGHT_1MFE false

class OpticalWeightELevel
{
private:
	double* OpticalWeight1MFE;	//what all the weights should be for this particular energy level and it's optics (1 - f(E))
	double* OpticalWeightFE;	//same, except just f(E). At some point, the accuracy is just going to be 1-1=0 or 1-tiny = 1, and I want the actual value for the other, so it must be explicitly calculated
	double* densStateFactor;	//what the density of state factor should be
	int sz;	//the size of the above array
	unsigned long long int iterUpdate;	//last iteration this was updated on.
public:
	unsigned int InitializeData(ELevel* eLev, unsigned long int curiter, double res);	//if -1, then it can't create the pointer, but that usually only matters once. Returns the size of the pointer, so 0 means failure. ELevel should be eLevel this data is in strcutre of
	double GetData(int index, bool which);
	int size(void) { return(sz); };
	OpticalWeightELevel(const OpticalWeightELevel& opt);
	OpticalWeightELevel();
	~OpticalWeightELevel();
};


class TransitionPair
{
public:
	ELevel* source;	//what is the potential source state
	ELevel* target;	//and the potential target state
	std::vector<ELevel*> origTarget;	//what was the original target (only applicable for thermalization of carriers)
	double weightAbsorb;	//weight likelyhood of transition by multiplying the number of states in each
	double weightEmit;
	TransitionPair();
	~TransitionPair();
};

class OpticalTransitionData	//stored in point by energy as key. Generated when a new index is done in optical storage
{
public:
	double energy;	//key for map in Point - but this will probably be useful to have in the data
	double weightAbsorb;	//weight absorb total
	double weightEmit;	//weight emit total
	unsigned long int lastIterUpdated;	//what iteration were transitions for this energy updated? if not this one, update all the weight absorbs/emits.
	std::vector<TransitionPair> TransitionData;	//hold all the data in a nice, clean manner
	bool forceFindTransitions;	//the sources and targets have changed - weighting may not stay the same
	OpticalTransitionData();
	~OpticalTransitionData();
};

class SubOptStorage	//array dependent on energy
{
public:
	double absorbRate;	//the total absorption rate for an energy level - not how split among states
	double emitRate;	//total stimulated emission rate for a particular energy - not how split among states
	double alphaxFactor;	//factor for calculating derivative
	SubOptStorage();	//make sure it gets initialized to zero! Who cares what happens when deleted
	~SubOptStorage();
};

class OpticalStorage	//stored in point
{
	SubOptStorage* data;	//these will be binned by energy level, so the index will determine the energy.	keep the two bits of data next to one another
	std::set<int> Indices;	//keep track of which data indices have rates
	int size;	//how large the arrays are - just be safe by keeping the "max" index
public:
	double GetAbsorb(int index);
	double GetEmit(int index);
	int UseRemoveNext(double& absorb, double& emit, double& ax);
	int SetAbsorbEmit(int index, double absorb, double emit);
	int AddAbsorbEmit(int index, double absorb, double emit, double ax);
	int SetAbsorb(int index, double absorb);
	int SetEmit(int index, double absorb);
	int Allocate(unsigned int num);
	int Clear();

	OpticalStorage();
	~OpticalStorage();
};


/*

class PhotonPacket
{
public:
	XSpace direction;	//direction of the light
	XSpace position;	//position of the light
	float lambda;	//wavelength of the light in nm
	unsigned long long int num;		//number of photons in the light packet - if it exceeds this value, start a new one...
	double energy;	//energy in eV
};

class Spectrum
{
public:
	float lambda;	//wavelength of light in nm
	double intensity;		//intensity of light as mW/cm^2
	XSpace direction;		//what direction is a bulk of this light coming from?
	unsigned short angleVar;	//0 = direct beam, 90 = completely diffuse (angle can vary 90 degrees from the specified direction)
};

class NK	//used for materials
{
public:
	double n;	//index of refraction
	double k;	//extinction coefficient
	double alpha;	//calculated from k: 4 pi k / lamda_0
};

class Optical	//used in a tree. This is data. Sort value is wavelength.
{				//will want to find based on value
public:
	double a,b;	//used to generate k/alpha values
	bool useAB;	//were the AB values specified or an NK generated. If so, constant n
	TreeRoot<NK, float> WavelengthData;	//sort value is the wavelength in nm
	~Optical();
	Optical();
};
*/



int Light(Model& mdl, ModelDescribe& mdesc, Point* singlePt=NULL, kernelThread* kThread=NULL);
int DoLight1D(Model& mdl, ModelDescribe& mdesc, Point* singlePt, kernelThread* kThread);
int RayTrace1D(Model& mdl, Simulation* sim, RayTrace& ray, std::list<RayTrace>& Raytree, Point* singlePt, bool allowMerge, bool calcRates);
int ProcessRays(std::list<RayTrace>& Raylist, Model& mdl, ModelDescribe& mdesc, Point* singlePt, bool allowMerge, bool calcRates);
int LightChangePoints(RayTrace& ray, Point* ptT, Simulation* sim, std::list<RayTrace>& Raytree);
int LightEnter(Simulation* sim, RayTrace& ray, Point* ptT, std::list<RayTrace>& Raytree);
int LightExit(Simulation* sim, RayTrace& ray);
int GenerateLight(Simulation* sim, ELevelRate& rData);
bool AcceptLight(Wavelength* wv, Simulation* sim, double intensityMultiplier=1.0);	//returns whether to keep the light or not. Intensity must be pre-calculated


template<typename VAL>
bool AddLightToMap(std::map<double, VAL>& output, double energy, VAL intensity, Simulation* sim);

int UpdateLightEnable(ModelDescribe& mdesc);
bool ValidELevel(Point* pt, ELevel* eLev, int which, double maxE, double minE);
int FindThermalizedUpperTransitions(int which, Point* pt, int ThermIndexLow, OpticalTransitionData* transitions, double maxE, double minE, double& weightA, double& weightE,
									TransitionPair& tmpTrans, Simulation* sim, double elecSource, double availSource);
OpticalTransitionData* FindTransitions(double energy, Point* pt, OpticalTransitionData* transitions, Simulation* sim);
int CalcTransitions(OpticalTransitionData* transitions, Point* pt, unsigned long iter, bool thermalize);
int AbsorbLight(RayTrace& ray, double distTravel, Simulation* sim, Point* singlePt, bool calcRates);
double FresnelEq(double nI, double costhI, double nT, double costhT, bool para);
bool SnellLaw(double nI, double nT, double thetaIn, double& thetaRefract);
double GetRelIndexRefraction(RayTrace& ray, Point* pt);	//ray needs to look at two points, and if going to vacuum, pt may be null. Can't rely on point methods as a result
int FindReflectionVector(XSpace inVect, XSpace plane, XSpace& ReflectVector);
bool FindRefractionVector(XSpace vectIn, XSpace plane, double thetaInc, double nI, double nR, XSpace& RefractVect);
double CalcNumberPhotonsSimple(RayTrace& ray, double power=0.0);
int PostAbsorbLightPt(Point* curPt, Simulation* sim);
int PostAbsorbLight(Model& mdl, Simulation* sim, Point* singlePt);

int DetermineEmissionCoefficients(Wavelength& wave, ELevel* eSrc, ELevel* eTrg);
double CalcBoltzCoeff(double eWidth, double numSt, double numCar);
double CalcApproxOcc(double coeff, double delE, double NumSt);


#endif