#ifndef SAAPD_GENERATE_MODEL
#define SAAPD_GENERATE_MODEL

#include <vector>
#include <map>
#include "BasicTypes.h"
#include "model.h"

#define DONEREDUCE 0	//when reducing the model, x was clear, and then y was clear
#define CHANGEX 1
#define YCLEAR 2	//change x when <=2
#define CHANGEY 3	//change y when >=3
#define XCLEAR 4
#define PRIORITY 5
#define PRIORITYX 6	//update all x's within said layers
#define PRIORITYY 7	//then update all y's within said layers
#define PRIORITYCLR 8	//during clear, update everything. If it changes, go to PRIX, if not, ChangeX

#define XDIR 0
#define YDIR 1
#define ZDIR 2

class Line;
class Shape;
class XSpace;
class Layer;
class Adjacent;


class Boundaries
{
public:
	std::vector<Line> lines;	//all the lines of the boundaries
	std::vector<Shape*> curves;	//the circle and ellipse boundaries
	Boundaries();
	~Boundaries();
};

class LayerMesh
{
public:
	Layer* layer;	//pointer to the layer it is describing
	std::map<XSpace, double> Points;	//a listing of where all the vertices will be. Data is the point's desired spacing
	bool exclusive;	//should this layer take priority over all other layers no matter what?	
	XSpace min;	//the minimum boundary on the mesh
	XSpace max;	//the maximum boundary on the mesh
	LayerMesh();
	LayerMesh(Layer* lyr);
	~LayerMesh();

};

class VoidSpace
{
public:
	XSpace min;
	XSpace max;
};

class MinPoint
{
public:
	Layer* layer;	//the layer the point is supposed to be associated with
	double spacing;	//what the point's spacing is
	bool remove;
	bool exterior;	//allows the point to have it's position anywhere in the point
};

class MiniPoint	//all points are rectangles. Just generate the mesh...
{
public:
	XSpace pxpy;	//right top corner of device (or right back)
	XSpace mxmy;	//bottom left corner of device (or left front)
	XSpace center;	//center of the mesh point
	XSpace TargetRes;	//the target resolution of the point
	Adjacent adj;	//the adjacent points in the matrix
	bool grow;		//flag to see if the point is capable of growing
	bool active;	//flag to see if this data point still matters... (or if it has been absorbed into another point)
	bool priority;	//give this point priority when doing mesh updates.
	bool last;		//make this point be updated after all other points are done
	XSpace bdist;	//distance to the nearest relevant boundary line
	MiniPoint();
};


int LinkID(ModelDescribe& mdl);
int CheckModel(ModelDescribe& mdl);
bool InLayer(XSpace pos, ModelDescribe& mdl);
double Magnitude(XSpace vect);
Boundaries GenerateBoundaryLines(ModelDescribe& mdl);
int GenerateMesh(ModelDescribe& mdl, Model& model);
XSpace MinFactoredDistancetoLine(XSpace pt1, XSpace mindist, Line itLine);
bool ExclusiveAdjacent(MiniPoint pt1, int pt1index, MiniPoint pt2, int pt2index, int dir);
bool MaxTwoPerp(MiniPoint pt1, MiniPoint pt2, int dir);
MiniPoint* EliminateUnused(long int& numpoints, long int newnumpoints, MiniPoint* allpoints);
int GenerateLastShapes(std::vector<Shape*>& lastShapes, Boundaries& bounds);
int GlobalizeDescriptors(ModelDescribe& mdl);

int GenerateMesh1D(ModelDescribe& mdesc, Model& mdl);
int FillBasicMeshInfo(std::map<XSpace, MinPoint>& mesh, ModelDescribe& mdesc, Model& mdl);
int CombineLayerMeshes(LayerMesh* lyrmeshes, unsigned int numlayers, ModelDescribe& mdesc, std::map<XSpace,MinPoint>& mesh);
int StitchMesh(std::map<XSpace, MinPoint>& mesh);
int VoidMesh(std::map<XSpace, MinPoint>& mesh, std::vector<VoidSpace>& voids);
XSpace GetClosestBoundary(Layer* lyr, XSpace pos);
int FindVoids(ModelDescribe& mdesc, std::vector<VoidSpace>& voids, unsigned int numlayers);
int AddPoints(std::map<XSpace, double>& data, ModelDescribe& mdesc, std::map<XSpace, MinPoint>& mesh, unsigned int numlayers, Layer* srcLayer);
int BuildLayerMesh(LayerMesh& mesh, Layer* lyr);
int ShiftPointsProportionally(std::map<XSpace, double>& data, double width, double normalize, XSpace OldPos, XSpace direction);
double GetDesiredSpacing(short direction, XSpace max, XSpace min, XSpace pos, double edge, double center);



#endif