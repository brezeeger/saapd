#include "ss.h"

#include "contacts.h"
#include "BasicTypes.h"
#include "model.h"
#include "physics.h"
#include "light.h"
#include "RateGeneric.h"
#include "CalcBandStruct.h"
#include "transient.h"
#include "outputdebug.h"
#include "output.h"
#include "PopulateModel.h"
#include "SaveState.h"
#include "srh.h"
#include "scatter.h"
#include "Equilibrium.h"
#include "DriftDiffusion.h"
#include "../gui_source/kernel.h"
#include "../gui_source/GUIHelper.h"
#include "SSNewton.h"
#include "SteadyState.h"
#include "EquationSolver.h"

int SS(ModelDescribe& mdesc, Model& mdl, kernelThread* kThread)
{

	//find steady state for the current conditions - don't change those
	Simulation* sim = mdesc.simulation;
	if(sim->SteadyStateData==NULL)
		return(-1);

	if(sim->SteadyStateData->ExternalStates.size() == 0)
		return(-1);

	sim->SteadyStateData->Initialize();

	mdesc.simulation->simType = SIMTYPE_SS;
	kThread->shareData.ss = true;
	SteadyState_SAAPD data;
	Calculations InitCalcs = mdesc.simulation->EnabledRates;
	data.Clear();	//amke sure it's empty and smei-decent going in
//	GenerateOutput(mdl, mdesc);
	SSInitData(data, mdl);
	data.chargeTolerance = CalcChargeTolerance(mdl ,mdesc);
	InitStructure(mdl, mdesc, data);	//make sure post charge adjust is put in model
	time_t start=time(NULL);
	time_t newSS;
	time_t timenow;

//	int IncreaseCtr = 0;
//	int outCtr = 0;
	unsigned int ptSize = data.Pt.size();	//should also be ELevelIndicesInPt size
	sim->OutFiles.clear();
	//keep doing this thing foreva
	kThread->shareData.KernelTargetTolerance = sim->accuracy;
	kThread->shareData.KernelssnumStates = sim->SteadyStateData->ExternalStates.size();

	
	
	data.CalcStateCtrMax = 2;
	data.CalcStateCtr = 0;
	data.CalcStateIncCtr = 0;

	int whichEnv = sim->SteadyStateData->ExternalStates[0]->reusePrevContactEnvironment;
	if(whichEnv == -1)
		whichEnv = 0;

	//this is only a good start if it starts with thermal equilibrium

	if(sim->TransientData)
	{ //then we will want to restore the begin/endOcc at the end
		double occ, avail;
		for(unsigned long int i=0; i<data.EStates.size(); ++i)
		{

			data.EStates[i].ptr->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			if(occ < avail)
				data.EStates[i].ptr->SetPrevOcc(occ);
			else
				data.EStates[i].ptr->SetPrevOccOpposite(avail);
		}
	}

	
	

	//the order will be Drift-Diffuse, SRH, All Combo (no light at all). Start off simple, then add complexity
	//Newton without any light until tolerance is reached.
	//Reset the tolerance, and include all the light. All future environments maintain newton
	//answer as that is probably the closest converged answer.
	int convergencectr = 0;
	for(unsigned long int i=0; i<data.EStates.size(); ++i)	//make sure there are enough target fermi levels set.
		data.EStates[i].ptr->TargetEfs.resize(sim->SteadyStateData->ExternalStates.size());
	
	for(unsigned int i=0; i < sim->SteadyStateData->ExternalStates.size(); ++i)
	{
		time(&newSS);
		
		kThread->shareData.KernelTimeLeft = 0;
		kThread->shareData.KernelMinTolerance = -1.0;
		kThread->shareData.KernelMaxTolerance = -1.0;
		kThread->shareData.KernelTolerance = data.tolerance;
		kThread->shareData.KernelsscurState = i;
		kThread->shareData.KernelMaxChange = 0.0;
		kThread->shareData.CurSim = sim->OutputFName+" - "+sim->SteadyStateData->ExternalStates[i]->FileOut;
		sim->SteadyStateData->CurrentData = i;
		sim->SteadyStateData->SetCalcIndex();	//reset the counter for calculating things (0 is default)
		data.CalcStateType = sim->SteadyStateData->GetCalcType();	//grabs the first user defined calculation method
		data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
		if (data.tolerance <= 0.0)
			data.tolerance = SS_INITIAL_TOLERANCE;
		data.CalcStateCtr = 0;
		data.targetCharge = 0.0;
		data.ReduceTolerance = false;	//just initialize to true for the first time around. It starts as init / reduce
		

		sim->SteadyStateData->ExternalStates[i]->ValidateData(mdl, sim);
		data.SetCtcSinkPts(mdl);	//uses contact data currently stored in contact to update Pt[pt] for flag saying to skip over modifying it
		UpdateLightEnable(mdesc);	//make sure the light is all properly setup for this particular iteration
//		IncreaseCtr = 0;	//but if it's far, allow it to take larger steps.
//		outCtr = 0;

		InitStructure(mdl, mdesc, data);
		kThread->shareData.KernelDiverge = false;
//		GenerateOutput(mdl, mdesc, data, InitCalcs);
		kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
		convergencectr=0;
		do
		{
#ifndef NDEBUG	//always output when in debug mode
//			GenerateOutput(mdl, mdesc, data, InitCalcs);
//			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
#else
			//in release mode, only output if the user requests it (save time!)
			if(kThread->shareData.GUIsaveState)	//this hopefully won't matter!
			{
				GenerateOutput(mdl, mdesc, data, InitCalcs);	//it's going to output the file when it reduces the tolerance anyway
				kThread->shareData.GUIsaveState = false;	//reset it so it can be clicked again
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
			}
			//statecontrol will clear out optical rates if in no light mode
#endif
			//
			if(StateControl(data, mdl, mdesc,kThread, InitCalcs)==SS_CALC_DONE)
				break;	//==1 if NEWTON_NO_LIGHT has accuracy to go to Newton_all, but there is no light in the final calculation


			/*  OLD METHOD

			//Calculate rates and charge
			CalculateRatesCharge(data, mdl, mdesc);
			
			//Go through all the states, and based on their net rate, either increase or decrease Ef(occupancy)
			//Also sets data.tolerance
			AdjustStateEf(data);

			//did the adjustment in Ef cause some really large changes to occur?
			//keep it neutral and minimize any changes to the band structure
			SSCorrectCharge(data, mdl);	//also updates mdl.poissonRho

			END OLD METHOD */


			double delta = LoopAllStatesRateToZero(data, mdl, mdesc, InitCalcs, kThread);
			if(IS_NEWTON(data.CalcStateType))
			{
				if(data.tolerance != 0.0) //this wasn't the first run through (no tolerance).
				{
					if(data.CalcStateCtr > 10)	//let it go ten times before it starts making tiny changes
					{
						double newToler = data.tolerance * 0.9;	//force it to reduce by at least 90%.
						data.tolerance = delta * 0.5;
						if(newToler < 2.0 * sim->accuracy)
							newToler = 2.0 * sim->accuracy;
						if(data.tolerance > newToler)
							data.tolerance = newToler;

						if(delta > data.tolerance*8.0)	//it's not changing very much, so try going into a full blown calculation
						{
							if(data.CalcStateType == SS_NEWTON_ALL)
								data.CalcStateType = SS_NEWTON_L_FULL;
							else if(data.CalcStateType == SS_NEWTON_NOLIGHT)
								data.CalcStateType = SS_NEWTON_NL_FULL;
						}
						else if(data.CalcStateType == SS_NEWTON_L_FULL)
							data.CalcStateType = SS_NEWTON_ALL;
						else if(data.CalcStateType == SS_NEWTON_NL_FULL)
							data.CalcStateType = SS_NEWTON_NOLIGHT;
						

						if(newToler == 2.0 * sim->accuracy && delta > 20.0 * newToler)
							convergencectr++;
						else
							convergencectr=0;	//keep counter reset
					}
					else
						data.tolerance = delta * 0.5;
				}
				else
					data.tolerance = delta*0.5;	//changes in occupancy scaled to only let Ef change by this much (or % if Ef large)
				kThread->shareData.KernelTolerance = data.tolerance;	//let the user know how it's improving
			}
			else if(delta < data.tolerance)	//could only be possible if not SS_NEWTON, so may as well else if
				data.ReduceTolerance = true;
			

			if(convergencectr > 5)	//it seems to be hopeless - try inching closer to it
			{
				TroubleConverge(mdl ,mdesc, data, kThread);	//this should inch it towards the next solution by slowly turning stuff on
				convergencectr =0;	//it is NOT fast, but it should work.
			}

			kThread->shareData.KernelMaxChange = delta;
			//now do all the checks to make sure the kernel responds adequately
			timenow = kThread->SSKernelInteract(sim, start);

			if(kThread->shareData.GUIabortSimulation)	//just get out if possible
				return (-1);



			if(kThread->shareData.GUIstopSimulation)
				break;	//get out of the loop and output the data as it stands

			if(kThread->shareData.GUINext)
			{
				kThread->shareData.GUINext = false;
				DisplayProgress(70, 0);
				break;	//get out of the do/while loop and just keep going
			}

			data.CalcStateCtr++;

		} while (data.CalcStateType != SS_CALC_DONE);	//just make sure it goes more than the first iteration

		if(kThread->shareData.GUIstopSimulation)
		{
			DisplayProgress(68, 0);
			kThread->shareData.KernelcurInfoMsg = MSG_STOPPED;
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
			GenerateOutput(mdl, mdesc, data, InitCalcs, kThread);
			kThread->shareData.GUIstopSimulation = false;
			FinalOutputs(mdl, mdesc);
			return(0);
		}
		//it has converged on an answer, now output the data proper

		if (kThread->shareData.GUIabortSimulation)	//just get out if possible
		{
			DisplayProgress(69, 0);
			return (-1);
		}
		GenerateOutput(mdl, mdesc, data, InitCalcs, kThread);
		time(&timenow);
		double delTime = difftime(timenow, newSS);
		ProgressCheck(65,false);
		ProgressDouble(48, delTime);
		DisplayProgress(66,i+1);
		DisplayProgress(67,sim->SteadyStateData->ExternalStates.size());
		DisplayProgressDoub(20, delTime);

		for(unsigned int j=0; j<data.EStates.size(); ++j)
		{
			data.EStates[j].LastGoodEf = data.EStates[j].curEf;
			data.EStates[j].ptr->TargetEfs[i] = data.EStates[j].curEf;	//make sure the steady state data is
			//ready for the transient stuff
		}

		//get the stuff in the ELevel vector


		data.tolerance = 0.0;	//reset tolerance for next iteration
		data.CalcStateCtr = 0;
		kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
		mdesc.simulation->curiter++;	//prevent any of this info from being overwritten!
		kThread->shareData.KernelsscurState++;
		
		
	}
	kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
	FinalOutputs(mdl, mdesc);

	if(sim->TransientData)
	{ //then we will want to restore the begin/endOcc at the end to original values
		double occ, avail;
		for(unsigned long int i=0; i<data.EStates.size(); ++i)
		{

			data.EStates[i].ptr->GetOccupancyOcc(RATE_OCC_PREV, occ, avail);
			if (occ < avail)
				data.EStates[i].ptr->SetBeginOccStates(occ);
			else
				data.EStates[i].ptr->SetBeginOccStatesOpposite(avail);
			data.EStates[i].ptr->TransferBeginToEnd();
		}
	}


	return(0);
}

int StateControl(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, Calculations& InitCalcs)
{
	Simulation* sim = mdesc.simulation;
	//	GenerateOutput(mdl, mdesc, data, InitCalcs);	//debug always output the file!

	//first bit is to select the next type of output
	//second half is to set all the data meant for the new output

	double targetTolerance = sim->SteadyStateData->GetCalcEndAccuracy();
	if (targetTolerance <= 0.0)
		targetTolerance = sim->accuracy;

	if (data.CalcStateType == SS_ALL)
	{
		if (data.ReduceTolerance)
		{
			data.tolerance = data.tolerance * SS_REDUCE_TOLERANCE;
			//					DisplayProgressDoub(21, data.tolerance);


			if (kThread->shareData.KernelMinTolerance < 0 || kThread->shareData.KernelMinTolerance > data.tolerance)
				kThread->shareData.KernelMinTolerance = data.tolerance;

			kThread->shareData.KernelTolerance = data.tolerance;

			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
			//					ProgressInt(70,0);
			data.ReduceTolerance = false;
			data.CalcStateCtr = 0;
			data.CalcStateCtrMax = 2;	//reset on size reduction
		}
		else if (data.CalcStateCtr >= data.CalcStateCtrMax)
		{
			data.CalcStateIncCtr++;

			if (data.CalcStateIncCtr > 20 / data.CalcStateCtrMax + 2)
			{
				data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one

				kThread->shareData.KernelshortProgBarPosition = 0;
				data.CalcStateIncCtr = 0;
				data.CalcStateCtrMax++;

				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
#ifndef NDEBUG
				GenerateOutput(mdl, mdesc, data, InitCalcs, kThread);
#endif
			}

			data.CalcStateCtr = 0;

		}
	}
	else if (data.CalcStateType == SS_TRANS_ACTUAL)
	{
		if (data.CalcStateCtr > 0)
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
		}

	}
	else if (data.CalcStateType == SS_TRANS_CUTOFF)
	{
		if (data.ReduceTolerance)
		{
			data.ReduceTolerance = false;
			data.tolerance = data.tolerance * 0.5;
			if (data.tolerance <= targetTolerance)
			{
				data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
				data.CalcStateCtr = 0;
				data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
				if (data.tolerance <= 0.0)
					data.tolerance = SS_INITIAL_TOLERANCE;
			}
		}

	}
	else if(data.CalcStateType == SS_NO_SRH)
	{
		if(data.ReduceTolerance || data.CalcStateCtr >= 2)	//always just do 2 sweeps back and forth
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}
	}
	else if(data.CalcStateType == SS_TRANSLIKE)
	{
		data.ReduceTolerance = false;
		if(data.CalcStateCtr > 0)	//do this 5x.
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}
	}
	else if(data.CalcStateType == SS_MATCH_IMPS)	//this always just runs through once - multiple times would have zero effect
	{
		if (data.CalcStateCtr > 0)	//do this 5x.
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}

	}
	else if(data.CalcStateType == SS_SLOWPTS)
	{
		if (data.CalcStateCtr > 0)	//do this 5x.
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}
	}
	else if(data.CalcStateType == SS_NEWTON_NOLIGHT || data.CalcStateType == SS_NEWTON_NL_FULL)
	{
		if (data.tolerance < targetTolerance)
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;

		}
	}
	else if(data.CalcStateType == SS_NEWTON_ALL || data.CalcStateType == SS_NEWTON_L_FULL)
	{
		if (data.tolerance < targetTolerance)
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}

	}
	else if (data.CalcStateType == SS_MONTECARLO)		//it only runs through the function once...
	{
		if (data.CalcStateCtr > 0)
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}
		
	}
	else if (data.CalcStateType == SS_NEWTON_NUMERICAL)
	{
		if (data.ReduceTolerance)
		{
			data.ReduceTolerance = false;
			data.tolerance = data.tolerance * 0.5;
			kThread->shareData.KernelTolerance = data.tolerance;
		}
		if (data.tolerance < targetTolerance)
		{
			data.CalcStateType = sim->SteadyStateData->GetCalcType(true);	//move on to next one
			data.CalcStateCtr = 0;
			data.tolerance = sim->SteadyStateData->GetCalcInitAccuracy();
			if (data.tolerance <= 0.0)
				data.tolerance = SS_INITIAL_TOLERANCE;
		}
	}
	

	////////////////////////////////////Get outta here if we're done!
	if (data.CalcStateType == SS_CALC_DONE)
		return(SS_CALC_DONE);

	/////////////////////////////////////////////////////////////////////////////////////////
	//now set the necessary variables for this!

	if (data.tolerance <= targetTolerance)	//it must have moved one!
	{
		targetTolerance = sim->SteadyStateData->GetCalcEndAccuracy();
		if (targetTolerance <= 0)
			targetTolerance = sim->accuracy;
		if (sim->SteadyStateData->GetCalcInitAccuracy() <= 0.0)
			data.tolerance = sim->accuracy * 1024;	//it's not the first one, and a initial tolerance wasn't set
	}

	//if it didn't load

	if (data.CalcStateType == SS_ALL)
	{
		sim->EnabledRates = InitCalcs;
	}
	else if (data.CalcStateType == SS_TRANS_ACTUAL)
	{
		sim->EnabledRates = InitCalcs;
	}
	else if (data.CalcStateType == SS_TRANS_CUTOFF)
	{
		sim->EnabledRates = InitCalcs;	//do everything
		kThread->shareData.KernelcurInfoMsg = MSG_SS_TRANS;

	}
	else if (data.CalcStateType == SS_NO_SRH)
	{
		sim->EnabledRates = InitCalcs;	//do everything
		sim->EnabledRates.SRH1 = sim->EnabledRates.SRH2 = sim->EnabledRates.SRH3 = sim->EnabledRates.SRH4 = false;
	}
	else if (data.CalcStateType == SS_TRANSLIKE)
	{
		sim->EnabledRates = InitCalcs;	//do everything
	}
	else if (data.CalcStateType == SS_MATCH_IMPS)
	{
		sim->EnabledRates.DisableAll();
		sim->EnabledRates.SRH1 = sim->EnabledRates.SRH2 = sim->EnabledRates.SRH3 = sim->EnabledRates.SRH4 = true;

	}
	else if (data.CalcStateType == SS_SLOWPTS)
	{
		sim->EnabledRates = InitCalcs;
	}
	else if (data.CalcStateType == SS_NEWTON_NOLIGHT || data.CalcStateType == SS_NEWTON_NL_FULL)
	{
		sim->EnabledRates = InitCalcs;
		sim->EnabledRates.LightEmissions = false;	//get it close for the first time through
		sim->EnabledRates.Lgen = false;
		sim->EnabledRates.Ldef = false;
		sim->EnabledRates.Lscat = false;
		sim->EnabledRates.Lsrh = false;
		sim->EnabledRates.Lpow = false;
		sim->EnabledRates.SEdef = false;
		sim->EnabledRates.SErec = false;
		sim->EnabledRates.SEscat = false;
		sim->EnabledRates.SEsrh = false;
	}
	else if (data.CalcStateType == SS_NEWTON_ALL || data.CalcStateType == SS_NEWTON_L_FULL)
	{
		sim->EnabledRates = InitCalcs;	//do everything
	}
	else if (data.CalcStateType == SS_MONTECARLO)
	{
		sim->EnabledRates = InitCalcs;	//do everything
	}
	else if (data.CalcStateType == SS_NEWTON_NUMERICAL)
	{
		sim->EnabledRates = InitCalcs;	//do everything
	}
	
	for (unsigned int i = 0; i<data.EStates.size(); ++i)
		ClearELevelRates(*data.EStates[i].ptr);

	if (kThread)
	{
		kThread->shareData.KernelTargetTolerance = targetTolerance;
	}

	return(data.CalcStateType);
}

int TroubleConverge(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data, kernelThread* kThread)
{
	//try to slowly morph from one state to the next.

	//potential issues, contacts going from being inactive to active and vice versa.
	kThread->shareData.KernelDiverge = true;
	//solution, look at answer from the previous for the newly active contact and work with that value as the starting point

	//there is no previous solution. Let's do that one first.
	SS_SimData* ss = mdesc.simulation->SteadyStateData;
	time_t start = time(NULL);
	time_t timenow = start;

	double testAccuracy = mdesc.simulation->accuracy * 10.0;
	double change, occavail, kTi;
	double scaleFactor = 0.1;
	for(unsigned int el=0; el<data.EStates.size(); ++el)
		data.EStates[el].tmpGoodEf = data.EStates[el].LastGoodEf;	//last good is from the end of the previous iteration
	//tmpGood is between each I converged a little bit step
	
	if(ss->CurrentData == 0) //having trouble with the first iteration - probably not thermal EQ
	{
		ss->VoltCurrentScaleFactor = 0.0;	//first thing to scale. This DOES NOT apply to electric field. That one just kinda has to remain the same
		if(mdesc.simulation->EnabledRates.Tunnel)
			ss->TunnellingFactor = 0.0;		//second thing to scale
		else
			ss->TunnellingFactor = 1.0;
		if(mdesc.simulation->EnabledRates.Ldef ||
			mdesc.simulation->EnabledRates.Lgen || 
			mdesc.simulation->EnabledRates.Lpow || 
			mdesc.simulation->EnabledRates.Lscat || 
			mdesc.simulation->EnabledRates.Lsrh || 
			mdesc.simulation->EnabledRates.SEdef || 
			mdesc.simulation->EnabledRates.SEscat || 
			mdesc.simulation->EnabledRates.SEsrh || 
			mdesc.simulation->EnabledRates.SErec)
			ss->LightRateFactor = 0.0;		//third thing to scale
		else
			ss->LightRateFactor = 1.0;
		if(mdesc.simulation->EnabledRates.LightEmissions == true)
			ss->LightEmissionFactor = 0.0;	//last thing to scale up to one.
		else
			ss->LightEmissionFactor = 1.0;	//doesn't matter, so don't confuse the system
		bool RestorePrevStructure = true;
		
		
		do //go through this loop until it is all scaled up
		{
			if(RestorePrevStructure)
			{
				//first, restore all the data to a known decent state
				for(unsigned int el=0; el<data.EStates.size(); ++el)
				{
					data.EStates[el].curEf = data.EStates[el].tmpGoodEf;
					kTi = KBI / data.EStates[el].ptr->getPoint().temp;

					if(data.EStates[el].ptr->imp)
						occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
						data.EStates[el].ptr->max - data.EStates[el].ptr->min,
						data.EStates[el].ptr->imp->getDegeneracy());
					else
						occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
						data.EStates[el].ptr->max - data.EStates[el].ptr->min);

					if (data.EStates[el].curEf >= 0.0) //it is mostly full of carriers, so occupancy holds the smaller value: avail
					{
						data.EStates[el].occupancy = occavail;
						data.EStates[el].OccMatches = false;
					}
					else //typically true - curEf < 0, so the occupancy is smaller and that's what gets stored in occupancy
					{
						data.EStates[el].occupancy = occavail;
						data.EStates[el].OccMatches = true;
					}
				}
				InitStructure(mdl, mdesc, data);
			}
			data.tolerance = 0.0;		//reset the tolerance.
			SSContactData tmpCtcData;	//this is will be used to update contact info. Don't modify the original

			int extSt = ss->ExternalStates[0]->reusePrevContactEnvironment<0?0:ss->ExternalStates[0]->reusePrevContactEnvironment;
			//first, update all the contacts appropriately
			int numCtc = ss->ExternalStates[extSt]->ContactEnvironment.size();
			for (int i = 0; i < numCtc; ++i)
			{
				if (ss->ExternalStates[extSt]->ContactEnvironment[i]->ctc) //be safe! don't dereference a null pointer!
				{
					tmpCtcData = *ss->ExternalStates[extSt]->ContactEnvironment[i];
					tmpCtcData.voltage = ss->VoltCurrentScaleFactor * tmpCtcData.voltage;
					tmpCtcData.current = ss->VoltCurrentScaleFactor * tmpCtcData.current;
					tmpCtcData.currentDensity = ss->VoltCurrentScaleFactor * tmpCtcData.currentDensity;
					tmpCtcData.currentDensity_ED = ss->VoltCurrentScaleFactor * tmpCtcData.currentDensity_ED;
					tmpCtcData.currentD_ED_Leak = ss->VoltCurrentScaleFactor * tmpCtcData.currentD_ED_Leak;
					tmpCtcData.current_ED = ss->VoltCurrentScaleFactor * tmpCtcData.current_ED;
					tmpCtcData.ctc->UpdateSS(tmpCtcData);
					tmpCtcData.ctc->UpdatePlaneRecPoints(mdesc.Ldimensions);
				}
			}

			int nonSigConverge = 0;
			do
			{
				if(ss->LightRateFactor != 0.0)	//no point in doing light if it's not going to affect anything
				{
					CreateLightSourcesFast(data, mdl, mdesc);
					Light(mdl, mdesc, NULL, kThread);	//fill in the optical rates in each state - which were just cleared when the light sources were made
				}
				change = SSNewton(data, mdl, mdesc, kThread);
				if(change > data.tolerance * 1.9 && data.tolerance > 0.0)
					nonSigConverge+=2;
				else
					nonSigConverge=nonSigConverge>0?nonSigConverge-1:0;
				
				data.tolerance = change * 0.5;

				kThread->shareData.KernelMaxChange = change;
				//now do all the checks to make sure the kernel responds adequately
				timenow = kThread->SSKernelInteract(mdesc.simulation, start);

				if(kThread->shareData.GUIabortSimulation || kThread->shareData.GUIstopSimulation || kThread->shareData.GUINext)	//just get out if possible
					return (0);	//get out

			}while(change >= testAccuracy && nonSigConverge < 10);

			//now do the scaling
			if(change < testAccuracy)	//then it converged to a nicer value
			{
				for(unsigned int el=0; el<data.EStates.size(); ++el)	//remember this good value
					data.EStates[el].tmpGoodEf = data.EStates[el].curEf;	//so it can go back to it if needed

				if(ss->VoltCurrentScaleFactor < 1.0)
				{
					ss->VoltCurrentScaleFactor += scaleFactor;
					if(ss->VoltCurrentScaleFactor > 1.0)
					{
						ss->VoltCurrentScaleFactor = 1.0;
						scaleFactor = 0.05;
					}
				}
				else if(ss->TunnellingFactor < 1.0)
				{
					ss->TunnellingFactor += scaleFactor;
					if(ss->TunnellingFactor >= 1.0)
					{
						ss->TunnellingFactor = 1.0;
						scaleFactor = 0.05;
					}
				}
				else if(ss->LightRateFactor < 1.0)
				{
					if(ss->LightRateFactor == 0.0)
						ss->LightRateFactor = 1.0e-10;
					else if(ss->LightRateFactor < 0.05)
						ss->LightRateFactor = ss->LightRateFactor * 10.0;
					else if(ss->LightRateFactor > 1.0)
					{
						ss->LightRateFactor = 1.0;
						scaleFactor = 0.05;
					}
					else
						ss->LightRateFactor += scaleFactor;
				}
				else if(ss->LightEmissionFactor < 1.0)
				{
					if(ss->LightEmissionFactor == 0.0)
						ss->LightEmissionFactor = 1.0e-10;
					else if(ss->LightEmissionFactor < 0.05)
						ss->LightEmissionFactor = ss->LightEmissionFactor * 10.0;
					else if(ss->LightEmissionFactor > 1.0)
						ss->LightEmissionFactor = 1.0;
					else
						ss->LightEmissionFactor += scaleFactor;
				}
			}
			else //restore stuff and try a smaller jump
			{
				RestorePrevStructure = true;
				if(ss->VoltCurrentScaleFactor < 1.0)
				{
					if(ss->VoltCurrentScaleFactor > 0.0)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->VoltCurrentScaleFactor -= scaleFactor;
						if(ss->VoltCurrentScaleFactor < 0.0)
							ss->VoltCurrentScaleFactor = 0.0;
					}
					else //it's just being bad - at thermal equilibrium. Hopeless
					{
						return(-1);
					}
				}
				else if(ss->TunnellingFactor < 1.0)
				{
					scaleFactor = scaleFactor * 0.5;
					ss->TunnellingFactor -= scaleFactor;
					if(ss->TunnellingFactor < 0.0)
					{
						ss->TunnellingFactor = 0.0;
					}
				}
				else if(ss->LightRateFactor < 1.0)
				{
					if(ss->LightRateFactor < 0.05)
						ss->LightRateFactor = ss->LightRateFactor * 0.2;
					else if(ss->LightRateFactor > 0.05)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->LightRateFactor -= scaleFactor;
					}
				}
				else if(ss->LightEmissionFactor < 1.0)
				{
					if(ss->LightEmissionFactor < 0.05)
						ss->LightEmissionFactor = ss->LightEmissionFactor * 0.2;
					else if(ss->LightEmissionFactor > 0.05)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->LightEmissionFactor -= scaleFactor;
					}
				}
			}
		}while(ss->LightEmissionFactor < 1.0 || ss->LightRateFactor < 1.0 || ss->TunnellingFactor < 1.0 || ss->VoltCurrentScaleFactor < 1.0);
	}	//end if it is the first environment
	else
	{
		//it had a previous environment it was working off of
		//this should also have the outputs loaded in
		
		ss->VoltCurrentScaleFactor = 0.0;	//first thing to scale. This DOES NOT apply to electric field. That one just kinda has to remain the same
		if(mdesc.simulation->EnabledRates.Tunnel)
			ss->TunnellingFactor = 0.0;		//second thing to scale
		else
			ss->TunnellingFactor = 1.0;
		if(mdesc.simulation->EnabledRates.Ldef ||
			mdesc.simulation->EnabledRates.Lgen || 
			mdesc.simulation->EnabledRates.Lpow || 
			mdesc.simulation->EnabledRates.Lscat || 
			mdesc.simulation->EnabledRates.Lsrh || 
			mdesc.simulation->EnabledRates.SEdef || 
			mdesc.simulation->EnabledRates.SEscat || 
			mdesc.simulation->EnabledRates.SEsrh || 
			mdesc.simulation->EnabledRates.SErec)
			ss->LightRateFactor = 0.0;		//third thing to scale
		else
			ss->LightRateFactor = 1.0;
		if(mdesc.simulation->EnabledRates.LightEmissions == true)
			ss->LightEmissionFactor = 0.0;	//last thing to scale up to one.
		else
			ss->LightEmissionFactor = 1.0;	//doesn't matter, so don't confuse the system
		bool RestorePrevStructure = true;
		
		int ExtIndex = ss->CurrentData;
		
		do //go through this loop until it is all scaled up
		{
			if(RestorePrevStructure)
			{
				//first, restore all the data to a known decent state
				for(unsigned int el=0; el<data.EStates.size(); ++el)
				{
					data.EStates[el].curEf = data.EStates[el].tmpGoodEf;
					kTi = KBI / data.EStates[el].ptr->getPoint().temp;

					if(data.EStates[el].ptr->imp)
						occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
						data.EStates[el].ptr->max - data.EStates[el].ptr->min,
						data.EStates[el].ptr->imp->getDegeneracy());
					else
						occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
						data.EStates[el].ptr->max - data.EStates[el].ptr->min);

					if (data.EStates[el].curEf >= 0.0) //it is mostly full of carriers, so occupancy holds the smaller value: avail
					{
						data.EStates[el].occupancy = occavail;
						data.EStates[el].OccMatches = false;
					}
					else //typically true - curEf < 0, so the occupancy is smaller and that's what gets stored in occupancy
					{
						data.EStates[el].occupancy = occavail;
						data.EStates[el].OccMatches = true;
					}
				}
				InitStructure(mdl, mdesc, data);
			}
			data.tolerance = 0.0;		//reset the tolerance.
			SSContactData tmpCtcData;	//this is will be used to update contact info. Don't modify the original
			int extCtInd = ss->ExternalStates[ExtIndex]->reusePrevContactEnvironment;
			int tmp = extCtInd;
			while (extCtInd >= 0)
			{
				tmp = extCtInd;
				extCtInd = ss->ExternalStates[extCtInd]->reusePrevContactEnvironment;
			}
			extCtInd = tmp;
			if (extCtInd < 0)	//if there was no reuse beforehand, it just goes straight to -1.
				extCtInd = ExtIndex;	//in this case, use the current value, which is > 0

			int prevInd = ExtIndex - 1;

			int numCtc = ss->ExternalStates[extCtInd]->ContactEnvironment.size();
			for (int i = 0; i < numCtc; ++i)
			{
				if (ss->ExternalStates[extCtInd]->ContactEnvironment[i]->ctc) //be safe! don't dereference a null pointer!
				{
					tmpCtcData = *ss->ExternalStates[extCtInd]->ContactEnvironment[i];
					double delta = 0.0;
					for(unsigned int j=0; j< ss->ExternalStates[prevInd]->output.contacts.size(); ++j)
					{
						if(ss->ExternalStates[prevInd]->output.contacts[j].ctcID == i)
						{
							delta = ss->ExternalStates[extCtInd]->ContactEnvironment[i]->voltage - ss->ExternalStates[prevInd]->output.contacts[j].voltageOut;
							tmpCtcData.voltage = ss->ExternalStates[prevInd]->output.contacts[j].voltageOut + delta * ss->VoltCurrentScaleFactor;

							delta = ss->ExternalStates[prevInd]->output.contacts[j].JpOutEnter.x * ss->ExternalStates[prevInd]->ContactEnvironment[j]->ctc->point[0]->area.x;
							delta -= ss->ExternalStates[prevInd]->output.contacts[j].JnOutEnter.x * ss->ExternalStates[prevInd]->ContactEnvironment[j]->ctc->point[0]->area.x;
							
							//convention is positive current in the contact means holes entering the contact
							//now delta holds prev, where prev = JpEnter - JnEnter
							tmpCtcData.current = delta + (ss->ExternalStates[extCtInd]->ContactEnvironment[i]->current-delta) * ss->VoltCurrentScaleFactor;
							if(tmpCtcData.currentActiveValue & CONTACT_CUR_DENSITY_LINEAR)
							{
								//need to make it just hte current
								tmpCtcData.currentActiveValue = tmpCtcData.currentActiveValue | CONTACT_CURRENT_LINEAR;	//set the input flag to include current
								tmpCtcData.currentActiveValue = tmpCtcData.currentActiveValue & ~(CONTACT_CUR_DENSITY_LINEAR);	//zero out the current density flag
								tmpCtcData.currentDensity = 0.0;
							}

							//the following will just scale inputs from the previous to the current as inputs. These are not potential outputs

							delta = ss->ExternalStates[extCtInd]->ContactEnvironment[i]->currentDensity_ED - ss->ExternalStates[prevInd]->ContactEnvironment[i]->currentDensity_ED;
							tmpCtcData.currentDensity_ED = delta * ss->VoltCurrentScaleFactor + ss->ExternalStates[prevInd]->ContactEnvironment[i]->currentDensity_ED;

							delta = ss->ExternalStates[extCtInd]->ContactEnvironment[i]->currentD_ED_Leak - ss->ExternalStates[prevInd]->ContactEnvironment[i]->currentD_ED_Leak;
							tmpCtcData.currentD_ED_Leak = delta * ss->VoltCurrentScaleFactor + ss->ExternalStates[prevInd]->ContactEnvironment[i]->currentD_ED_Leak;

							delta = ss->ExternalStates[extCtInd]->ContactEnvironment[i]->current_ED - ss->ExternalStates[prevInd]->ContactEnvironment[i]->current_ED;
							tmpCtcData.current_ED = delta * ss->VoltCurrentScaleFactor + ss->ExternalStates[prevInd]->ContactEnvironment[i]->current_ED;

							break;
						}
					}
						
					
					tmpCtcData.ctc->UpdateSS(tmpCtcData);
					tmpCtcData.ctc->UpdatePlaneRecPoints(mdesc.Ldimensions);
				}
			}

			int nonSigConverge = 0;
			do
			{
				if(ss->LightRateFactor != 0.0)	//no point in doing light if it's not going to affect anything
				{
					CreateLightSourcesFast(data, mdl, mdesc);
					Light(mdl, mdesc, NULL, kThread);	//fill in the optical rates in each state - which were just cleared when the light sources were made
				}
				change = SSNewton(data, mdl, mdesc, kThread);
				if(change > data.tolerance * 1.9 && data.tolerance > 0.0)
					nonSigConverge+=2;
				else
					nonSigConverge=nonSigConverge>0?nonSigConverge-1:0;
				
				data.tolerance = change * 0.5;

				kThread->shareData.KernelMaxChange = change;
				kThread->shareData.KernelTolerance = data.tolerance;
				//now do all the checks to make sure the kernel responds adequately
				timenow = kThread->SSKernelInteract(mdesc.simulation, start);

				if(kThread->shareData.GUIabortSimulation || kThread->shareData.GUIstopSimulation || kThread->shareData.GUINext)	//just get out if possible
					return (0);	//get out

			}while(change >= testAccuracy && nonSigConverge < 10);

			if(change < testAccuracy)	//then it converged to a nicer value
			{
				for(unsigned int el=0; el<data.EStates.size(); ++el)	//remember this good value
					data.EStates[el].tmpGoodEf = data.EStates[el].curEf;	//so it can go back to it if needed

				if(ss->VoltCurrentScaleFactor < 1.0)
				{
					ss->VoltCurrentScaleFactor += scaleFactor;
					if(ss->VoltCurrentScaleFactor > 1.0)
					{
						ss->VoltCurrentScaleFactor = 1.0;
						scaleFactor = 0.05;
					}
				}
				else if(ss->TunnellingFactor < 1.0)
				{
					ss->TunnellingFactor += scaleFactor;
					if(ss->TunnellingFactor >= 1.0)
					{
						ss->TunnellingFactor = 1.0;
						scaleFactor = 0.05;
					}
				}
				else if(ss->LightRateFactor < 1.0)
				{
					if(ss->LightRateFactor == 0.0)
						ss->LightRateFactor = 1.0e-10;
					else if(ss->LightRateFactor < 0.05)
						ss->LightRateFactor = ss->LightRateFactor * 10.0;
					else if(ss->LightRateFactor > 1.0)
					{
						ss->LightRateFactor = 1.0;
						scaleFactor = 0.05;
					}
					else
						ss->LightRateFactor += scaleFactor;
				}
				else if(ss->LightEmissionFactor < 1.0)
				{
					if(ss->LightEmissionFactor == 0.0)
						ss->LightEmissionFactor = 1.0e-10;
					else if(ss->LightEmissionFactor < 0.05)
						ss->LightEmissionFactor = ss->LightEmissionFactor * 10.0;
					else if(ss->LightEmissionFactor > 1.0)
						ss->LightEmissionFactor = 1.0;
					else
						ss->LightEmissionFactor += scaleFactor;
				}
			}
			else //restore stuff and try a smaller jump
			{
				RestorePrevStructure = true;
				if(ss->VoltCurrentScaleFactor < 1.0)
				{
					if(ss->VoltCurrentScaleFactor > 0.0)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->VoltCurrentScaleFactor -= scaleFactor;
						if(ss->VoltCurrentScaleFactor < 0.0)
							ss->VoltCurrentScaleFactor = 0.0;
					}
					else //it's just being bad - at thermal equilibrium. Hopeless
					{
						return(-1);
					}
				}
				else if(ss->TunnellingFactor < 1.0)
				{
					scaleFactor = scaleFactor * 0.5;
					ss->TunnellingFactor -= scaleFactor;
					if(ss->TunnellingFactor < 0.0)
					{
						ss->TunnellingFactor = 0.0;
					}
				}
				else if(ss->LightRateFactor < 1.0)
				{
					if(ss->LightRateFactor < 0.05)
						ss->LightRateFactor = ss->LightRateFactor * 0.2;
					else if(ss->LightRateFactor > 0.05)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->LightRateFactor -= scaleFactor;
					}
				}
				else if(ss->LightEmissionFactor < 1.0)
				{
					if(ss->LightEmissionFactor < 0.05)
						ss->LightEmissionFactor = ss->LightEmissionFactor * 0.2;
					else if(ss->LightEmissionFactor > 0.05)
					{
						scaleFactor = scaleFactor * 0.5;
						ss->LightEmissionFactor -= scaleFactor;
					}
				}
			}

		}while(ss->LightEmissionFactor < 1.0 || ss->LightRateFactor < 1.0 || ss->TunnellingFactor < 1.0 || ss->VoltCurrentScaleFactor < 1.0);
	}

	kThread->shareData.KernelDiverge = false;
	return(0);
}

int FinalOutputs(Model& mdl, ModelDescribe& mdesc)
{
	//need to output for each environment
	//environment QE is not really useful. That data gets output
	//I-V curves from the viewpoint of every contact with that of every other.
	SS_SimData* ss = mdesc.simulation->SteadyStateData;
	
	

	if(ss==NULL)
		return(0);
	OutputEnvironmentSummary(mdesc, mdesc.simulation->SteadyStateData);	//what were the inputs to the simulation
	OutputEnvironments(mdesc.simulation, ss);

	//output JV curves for each contact in relation to the other contacts
	
	bool unique;

	//if the environments have the same FileOut name, they should be grouped together
	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		unique = true;
		for(unsigned int j=0; j<mdesc.simulation->groupNames.size(); ++j)
		{
			if (ss->ExternalStates[i]->FileOut == mdesc.simulation->groupNames[j])
			{
				unique = false;
				break;
			}
		}
		if(unique)
			mdesc.simulation->groupNames.push_back(ss->ExternalStates[i]->FileOut);
	}

	for (unsigned int i = 0; i<mdesc.simulation->groupNames.size(); ++i)
	{
		for(unsigned int ct1 = 0; ct1 < mdesc.simulation->contacts.size(); ++ct1)
		{
			for(unsigned int ct2 = ct1+1; ct2 < mdesc.simulation->contacts.size(); ++ct2)
			{
				OutputContactJV(mdesc.simulation, ss, mdesc.simulation->groupNames[i], ct1, ct2);	//output every possible JV curve
				OutputContactJV(mdesc.simulation, ss, mdesc.simulation->groupNames[i], ct2, ct1);	//but allow user to choose whether viewing from normally biased or not
			}
		}
	}





	return(0);
}

int FillInEnvOutputs(Model& mdl, ModelDescribe& mdesc, Environment& thisEnv)
{
	//each contact needs to have a voltage, current, and currentDens
	Environment* useCtcEnv = &thisEnv;
	if(thisEnv.reusePrevContactEnvironment != -1)	//then this should refer to a different environment contact
		useCtcEnv = &(*mdesc.simulation->SteadyStateData->ExternalStates[thisEnv.reusePrevContactEnvironment]);

	ctcOutputs tmpCtcout;
	thisEnv.output.contacts.clear();	//make sure it doesn't keep loading up tons of data for each iteration
	for(unsigned int i=0; i<useCtcEnv->ContactEnvironment.size(); ++i)
	{
		if(useCtcEnv->ContactEnvironment[i]->currentActiveValue != CONTACT_INACTIVE && useCtcEnv->ContactEnvironment[i]->ctc != NULL)
		{
			tmpCtcout.ctcID = i;
			if(useCtcEnv->ContactEnvironment[i]->ctc->point.size() > 0)
			{
				tmpCtcout.voltageOut = -useCtcEnv->ContactEnvironment[i]->ctc->point[0]->fermi;
				tmpCtcout.voltageNOut = -useCtcEnv->ContactEnvironment[i]->ctc->point[0]->fermin;
				tmpCtcout.voltagePOut = -useCtcEnv->ContactEnvironment[i]->ctc->point[0]->fermip;
				tmpCtcout.JnOut.x = useCtcEnv->ContactEnvironment[i]->ctc->JnOutput.x * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.x;
				tmpCtcout.JnOut.y = useCtcEnv->ContactEnvironment[i]->ctc->JnOutput.y * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.y;
				tmpCtcout.JnOut.z = useCtcEnv->ContactEnvironment[i]->ctc->JnOutput.z * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.z;
				tmpCtcout.JpOut.x = useCtcEnv->ContactEnvironment[i]->ctc->JpOutput.x * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.x;
				tmpCtcout.JpOut.y = useCtcEnv->ContactEnvironment[i]->ctc->JpOutput.y * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.y;
				tmpCtcout.JpOut.z = useCtcEnv->ContactEnvironment[i]->ctc->JpOutput.z * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.z;

				tmpCtcout.JnOutEnter.x = useCtcEnv->ContactEnvironment[i]->ctc->JnOutputEnter.x * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.x;
				tmpCtcout.JnOutEnter.y = useCtcEnv->ContactEnvironment[i]->ctc->JnOutputEnter.y * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.y;
				tmpCtcout.JnOutEnter.z = useCtcEnv->ContactEnvironment[i]->ctc->JnOutputEnter.z * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.z;
				tmpCtcout.JpOutEnter.x = useCtcEnv->ContactEnvironment[i]->ctc->JpOutputEnter.x * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.x;
				tmpCtcout.JpOutEnter.y = useCtcEnv->ContactEnvironment[i]->ctc->JpOutputEnter.y * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.y;
				tmpCtcout.JpOutEnter.z = useCtcEnv->ContactEnvironment[i]->ctc->JpOutputEnter.z * QCHARGE * useCtcEnv->ContactEnvironment[i]->ctc->point[0]->areai.z;
				tmpCtcout.nExtEnter = useCtcEnv->ContactEnvironment[i]->ctc->nEnterOutput;
				tmpCtcout.pExtEnter = useCtcEnv->ContactEnvironment[i]->ctc->pEnterOutput;
				thisEnv.output.contacts.push_back(tmpCtcout);
			}

		}
	}

	thisEnv.output.calculated = true;
	thisEnv.output.capacitance.clear();	//must calculate this eventually
	thisEnv.output.quantumEfficiencyExternal = thisEnv.output.quantumEfficiencyInternal = 
		thisEnv.output.efficiency = 0.0;

	return(0);
}

int InitStructure(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data)
{
	//just make sure everything is definitely on the same page
	unsigned int ptSize = data.Pt.size();	//should also be ELevelIndicesInPt size
	

	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change
		for (long int el = start; el <= stop; ++el)
		{
			if(data.EStates[el].OccMatches)
			{
				data.EStates[el].ptr->SetBeginOccStates(data.EStates[el].occupancy);
				data.EStates[el].ptr->SetEndOccStates(data.EStates[el].occupancy);
			}
			else
			{
				data.EStates[el].ptr->SetBeginOccStatesOpposite(data.EStates[el].occupancy);
				data.EStates[el].ptr->SetEndOccStatesOpposite(data.EStates[el].occupancy);
			}
		}

		data.EStates[start].ptr->getPoint().ResetData();
		mdl.poissonRho[data.Pt[pt].mdlRho] = data.EStates[start].ptr->getPoint().rho;
	}

	CalcBandStruct(mdl, mdesc);	//make sure the band structure is up to date with the new values
	return(0);
}

double LoopAllStatesForward(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	double tmpChange;
	double largestChange = 0.0;
	unsigned int ptSize = data.Pt.size();
	data.totCharge = data.varCharge.GetNetSumClear() + data.fixedCharge;
	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
		{
			data.varCharge.AddValue(data.Pt[pt].VariableCharge.GetNetSum());
			continue; //this point's values do not change
		}

		if(data.Pt[pt].ctcSink)
		{
			data.varCharge.AddValue(data.Pt[pt].VariableCharge.GetNetSum());
			continue;	//this is a sink. It does not change. Period
		}

		for (long int el = start; el <= stop; ++el)
		{
			if((data.CalcStateType == SS_MATCH_IMPS && (data.EStates[el].ptr->imp || data.EStates[el].ptr->bandtail))
				|| (data.CalcStateType == SS_NO_SRH && data.EStates[el].ptr->imp==NULL && data.EStates[el].ptr->bandtail==false)
				|| (data.CalcStateType == SS_ALL))
			{  //if it is matching imps, only the impurities. Otherwise, everything
				tmpChange = GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data
				if(fabs(tmpChange) > largestChange)
					largestChange = fabs(tmpChange);
			}
			if(data.EStates[el].OccMatches)
			{
				if(data.EStates[el].carType==HOLE)
					data.varCharge.AddValue(data.EStates[el].occupancy);
				else
					data.varCharge.AddValue(-data.EStates[el].occupancy);
			}
			else
			{
				if(data.EStates[el].carType==HOLE)	//occupancy represents electrons in the VB
				{  //want to add (NStates-avail), occ=avail
					data.varCharge.AddValue(-data.EStates[el].occupancy);
					data.varCharge.AddValue(data.EStates[el].NStates);
				}
				else //occupancy reps holes in the CB
				{ //want to subtract (Nstates-avail), occ=avail
					data.varCharge.AddValue(data.EStates[el].occupancy);
					data.varCharge.AddValue(-data.EStates[el].NStates);
				}
			}
		}
	}
	data.totCharge = data.varCharge.GetNetSum() + data.fixedCharge;
	return(largestChange);
}

double LoopAllStatesBackward(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	double tmpChange;
	double largestChange = 0.0;
	unsigned int ptSize = data.Pt.size();
	data.totCharge = data.varCharge.GetNetSumClear() + data.fixedCharge;
	for (unsigned int pt = ptSize-1; pt < ptSize; --pt)	//go through in reverse order. At 0, it jumps to a humungo number
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
		{
			data.varCharge.AddValue(data.Pt[pt].VariableCharge.GetNetSum());
			continue; //this point's values do not change
		}

		if(data.Pt[pt].ctcSink)
		{
			data.varCharge.AddValue(data.Pt[pt].VariableCharge.GetNetSum());
			continue;	//this is a sink. It does not change. Period
		}

		for (long int el = stop; el >= start; --el)
		{
			tmpChange = GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);
			if(fabs(tmpChange) > largestChange)
				largestChange = fabs(tmpChange);

			if(data.EStates[el].OccMatches)
			{
				if(data.EStates[el].carType==HOLE)
					data.varCharge.AddValue(data.EStates[el].occupancy);
				else
					data.varCharge.AddValue(-data.EStates[el].occupancy);
			}
			else
			{
				if(data.EStates[el].carType==HOLE)	//occupancy represents electrons in the VB
				{  //want to add (NStates-avail), occ=avail
					data.varCharge.AddValue(-data.EStates[el].occupancy);
					data.varCharge.AddValue(data.EStates[el].NStates);
				}
				else //occupancy reps holes in the CB
				{ //want to subtract (Nstates-avail), occ=avail
					data.varCharge.AddValue(data.EStates[el].occupancy);
					data.varCharge.AddValue(-data.EStates[el].NStates);
				}
			}
		}
	}
	data.totCharge = data.varCharge.GetNetSum() + data.fixedCharge;
	return(largestChange);
}

double SS_SlowPtLoop(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread)
{
	std::vector<MaxMin<unsigned int>> SlowPtRanges;
	MaxMin<unsigned int> tmpRange;
	bool addPt;
	tmpRange.max = tmpRange.min = -1;	//start off saying invalid (really large number...)
	data.CalcStateType = SS_ALL;
	mdesc.simulation->EnabledRates = InitCalc;
	time_t start;
	start = time(NULL);
	unsigned int ptSize = data.Pt.size();
	double tmpChange;

	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change

		if(data.Pt[pt].ctcSink)
			continue;	//this is a sink. It does not change. Period

		addPt = false;
		for (long int el = start; el <= stop; ++el)
		{
			tmpChange = GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data
			if(fabs(tmpChange) > data.tolerance)
				addPt = true;
		}
		if(addPt)
		{
			if(tmpRange.min == -1)
			{
				tmpRange.min = tmpRange.max = pt;
			}
			else if(tmpRange.max == pt-1)	//was the previous one in range
			{
				tmpRange.max = pt;
			}
		}
		else if(tmpRange.min != -1)	//then it is not adding an additional point
		{
			SlowPtRanges.push_back(tmpRange);
			tmpRange.min = tmpRange.max = -1;
		}
	}
	if(tmpRange.min != -1)	//make sure to include the last one
	{
		SlowPtRanges.push_back(tmpRange);
		tmpRange.min = tmpRange.max = -1;
	}

	//now the fun part...
	unsigned int slowSz = SlowPtRanges.size();
	//now make sure each one is not a significant portion of the points. Otherwise try and split evenly
	unsigned int maxSz = 3 + ptSize/4;
	for(unsigned int i=0; i<slowSz; ++i)
	{
		unsigned int SlSz = SlowPtRanges[i].max - SlowPtRanges[i].min;
		if(SlSz > maxSz)
		{
			SlSz = SlSz/3;
			unsigned int tmpMax = SlowPtRanges[i].max;

			SlowPtRanges[i].max = SlowPtRanges[i].min + SlSz;	//reset the 


			tmpRange.min = SlowPtRanges[i].max;	//one point overlap
			tmpRange.max = tmpRange.min + SlSz;
			SlowPtRanges.push_back(tmpRange);
			tmpRange.min = tmpRange.max;
			tmpRange.max = tmpMax;
			SlowPtRanges.push_back(tmpRange);
			slowSz += 2;	//adjust the size for two more push backs
		}
	}

	bool SectionGood;


	for(unsigned int slow=0; slow<slowSz; ++slow)
	{
		unsigned int ptStart = SlowPtRanges[slow].min;
		unsigned int ptStop = SlowPtRanges[slow].max;
		if (ptStop < ptStart || ptStart == -1 || ptStop == -1)
			continue; //this point's values do not change
		unsigned int getOutCtr = 0;
		do
		{
			mdesc.simulation->EnabledRates = InitCalc;
			mdesc.simulation->EnabledRates.SRH1 = mdesc.simulation->EnabledRates.SRH2 = mdesc.simulation->EnabledRates.SRH3 = mdesc.simulation->EnabledRates.SRH4 = false;
			data.CalcStateType = SS_NO_SRH;
			//first do 10 back and forths on no SRH
			for(int i=0; i<10; ++i)
			{
				for(unsigned int pt = ptStart; pt <= ptStop; ++pt)
				{
					long int start = data.ELevelIndicesInPt[pt].min;
					long int stop = data.ELevelIndicesInPt[pt].max;
					if (start < 0 || stop < 0 || stop < start)
						continue; //this point's values do not change

					for (long int el = start; el <= stop; ++el)
						GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data
				}
				for(unsigned int pt = ptStop; pt >= ptStart && pt<=ptStop; --pt)
				{
					long int start = data.ELevelIndicesInPt[pt].min;
					long int stop = data.ELevelIndicesInPt[pt].max;
					if (start < 0 || stop < 0 || stop < start)
						continue; //this point's values do not change

					for (long int el = stop; el <= stop && el>=start; --el)
						GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data
				}
			}

			//then a single SRH iteration, which will get the impurities to match the bands
			data.CalcStateType = SS_MATCH_IMPS;
			mdesc.simulation->EnabledRates = InitCalc;	//basically set everything to false, but only include the SRH that were specified in the file
			mdesc.simulation->EnabledRates.Current = mdesc.simulation->EnabledRates.Ldef = mdesc.simulation->EnabledRates.Lgen = mdesc.simulation->EnabledRates.LightEmissions = 
				mdesc.simulation->EnabledRates.Lpow = mdesc.simulation->EnabledRates.Lscat = mdesc.simulation->EnabledRates.Lsrh = mdesc.simulation->EnabledRates.Scatter = 
				mdesc.simulation->EnabledRates.SEdef = mdesc.simulation->EnabledRates.SErec = mdesc.simulation->EnabledRates.SEscat = mdesc.simulation->EnabledRates.SEsrh = 
				mdesc.simulation->EnabledRates.ThermalGen = mdesc.simulation->EnabledRates.ThermalRec = mdesc.simulation->EnabledRates.Tunnel = false;

			for(unsigned int pt = ptStart; pt <= ptStop; ++pt)
			{
				long int start = data.ELevelIndicesInPt[pt].min;
				long int stop = data.ELevelIndicesInPt[pt].max;
				if (start < 0 || stop < 0 || stop < start)
					continue; //this point's values do not change

				for (long int el = start; el <= stop; ++el)
					GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data

			}
			data.CalcStateType = SS_ALL;
			mdesc.simulation->EnabledRates = InitCalc;
			for(unsigned int i=0; i<25; ++i)
			{
				SectionGood = true;	//want only the last one to let it go to false
				for(unsigned int pt = ptStart; pt <= ptStop; ++pt)
				{
					long int start = data.ELevelIndicesInPt[pt].min;
					long int stop = data.ELevelIndicesInPt[pt].max;
					if (start < 0 || stop < 0 || stop < start)
						continue; //this point's values do not change

					for (long int el = start; el <= stop; ++el)
					{
						tmpChange = GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation);	//this is constantly updated poissonRho and Resetting Point Data
						if(fabs(tmpChange) > data.tolerance)
							SectionGood = false;
					}
				}
				if(SectionGood)
					break;
			}
			kThread->SSKernelInteract(mdesc.simulation, start);

			if(kThread->shareData.GUIabortSimulation)	//just get out if possible
				return (0.0);



			if(kThread->shareData.GUIstopSimulation)
				return(0.00);	//get out of the loop and output the data as it stands

			if(kThread->shareData.GUINext)
				return(0.0);

			getOutCtr++;
		}while(SectionGood==false && getOutCtr < 30);	//do this a max of 30 times.
	}	//end for loop through all the different slow sections


	mdesc.simulation->EnabledRates = InitCalc;
	mdesc.simulation->EnabledRates.SRH1 = mdesc.simulation->EnabledRates.SRH2 = mdesc.simulation->EnabledRates.SRH3 = mdesc.simulation->EnabledRates.SRH4 = false;
	data.CalcStateType = SS_SLOWPTS;	//reset to original value so control can take care of it properly

	return(1.0);
}

double SS_Transtype(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread)
{
	//this function goes through all the states, finds the net rate. Moves it 50% of the way there. Adjusts all the other states it interacted with
	double NRate;
	double largestChange = 0.0;
	unsigned int ptSize = data.Pt.size();
	time_t sttime = time(NULL);
	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change

		if(data.Pt[pt].ctcSink)
			continue;	//this is a sink. It does not change. Period

		kThread->SSKernelInteract(mdesc.simulation, sttime);
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			break;

		if(kThread->shareData.GUIstopSimulation)
			break;	//get out of the loop and output the data as it stands

		if(kThread->shareData.GUINext)
			break;	//get out of the do/while loop and just keep going
		
		Point* ptrPt = &mdl.gridp.find(data.Pt[pt].self)->second;// &(mdl.getPoint(data.Pt[pt].self]);
		std::vector<Point*> Updated;
		
		bool in = false;
		Updated.push_back(ptrPt);
		for(unsigned int j=0; j<data.BandPtsUpdated.size(); ++j)
		{
			if(ptrPt == data.BandPtsUpdated[j])
			{
				in = true;
				break;
			}
		}
		if(!in)
			CalcPointBandFast(*ptrPt, mdl);	//get this reset before it is used
		
		if(ptrPt->adj.adjMX)
		{
			in = false;
			Updated.push_back(ptrPt->adj.adjMX);
			for(unsigned int j=0; j<data.BandPtsUpdated.size(); ++j)
			{
				if(ptrPt->adj.adjMX == data.BandPtsUpdated[j])
				{
					in = true;
					break;
				}
			}
			if(!in)
				CalcPointBandFast(*ptrPt->adj.adjMX, mdl);	//get this reset before it is used
		}
		if(ptrPt->adj.adjPX)
		{
			in = false;
			Updated.push_back(ptrPt->adj.adjPX);
			for(unsigned int j=0; j<data.BandPtsUpdated.size(); ++j)
			{
				if(ptrPt->adj.adjPX == data.BandPtsUpdated[j])
				{
					in = true;
					break;
				}
			}
			if(!in)
				CalcPointBandFast(*ptrPt->adj.adjPX, mdl);	//get this reset before it is used
		}
		data.BandPtsUpdated = Updated;





		for (long int el = start; el <= stop; ++el)
		{
			double initOcc = data.EStates[el].occupancy;
			double OrigPsi = ptrPt->Psi;
			double LPsi = ptrPt->adj.adjMX ? ptrPt->adj.adjMX->Psi : 0.0;
			double RPsi = ptrPt->adj.adjPX ? ptrPt->adj.adjPX->Psi : 0.0;	//this is not the correct value. It hasn't been updated yet!
			bool initMatch = data.EStates[el].OccMatches;
			double initEf = data.EStates[el].curEf;
			double initScatConst = data.EStates[el].carType==ELECTRON ? ptrPt->scatterCBConst : ptrPt->scatterVBConst;
			double initRho = ptrPt->rho;
			NRate = GetStateRateZero(data.EStates[el], data.Pt[pt], data, mdl, mdesc.simulation, true);	//this is constantly updated poissonRho and Resetting Point Data
			//the true flag causes it to return the initial rate
			if(NRate == 0.0)
				continue;	//no need to do any more adjustments on this point

			double newOcc = data.EStates[el].occupancy;
			bool newMatch = data.EStates[el].OccMatches;
			double delta, tStep;
			//reset the point to it's original values
			//need to get the original values back in for recalculating the rates
			


			if(initMatch == newMatch)
				delta = initOcc - newOcc;
			else 	//they're not the same so... make one of them the same
				delta = data.EStates[el].NStates - initOcc - newOcc;

			if(initMatch)
			{
				data.EStates[el].ptr->SetBeginOccStates(initOcc);
				data.EStates[el].ptr->SetEndOccStates(initOcc);
			}
			else
			{
				data.EStates[el].ptr->SetBeginOccStatesOpposite(initOcc);
				data.EStates[el].ptr->SetEndOccStatesOpposite(initOcc);
			}
			if(data.EStates[el].carType == ELECTRON)	//nothing would have changed if it were an impurity
				data.EStates[el].ptr->getPoint().scatterCBConst = initScatConst;
			else
				data.EStates[el].ptr->getPoint().scatterVBConst = initScatConst;

			data.EStates[el].occupancy = initOcc;
			data.EStates[el].OccMatches = initMatch;
			data.EStates[el].curEf = initEf;
			//only go half way there for convergence sake. The other states are changing too!
			tStep = fabs(delta / NRate) * 0.0675;
			
			ptrPt->SetLocalVacuum(OrigPsi);	//also update sthe cb/vb. Not fermi levels, though
			if(ptrPt->adj.adjMX)
				ptrPt->adj.adjMX->SetLocalVacuum(LPsi);	//also update sthe cb/vb. Not fermi levels, though
			if(ptrPt->adj.adjPX)
				ptrPt->adj.adjPX->SetLocalVacuum(RPsi);	//also update sthe cb/vb. Not fermi levels, though
			
			//now calculate all the rates and update the corresponding carriers with this time step
			SS_TransferCarriers(data, mdl, mdesc, tStep, el);


			

		}
	}


	return(0.0);
}

double SS_TransferCarriers(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, double tStep, long int el)
{
	ELevel* ptrE = data.EStates[el].ptr;
	Point* ptrPt = &ptrE->getPoint();
	Simulation* sim = mdesc.simulation;
	double NRateIn;
	std::vector<Point*> BStructPts;

	BStructPts.push_back(ptrPt);
	if(ptrPt->adj.adjMX)
		BStructPts.push_back(ptrPt->adj.adjMX);
	if(ptrPt->adj.adjPX)
		BStructPts.push_back(ptrPt->adj.adjPX);
	

	double totNRateIn = 0.0;
	double occ,avail;
	double kT = KB * ptrE->getPoint().temp;
	double degen=1.0;
	ptrE->Rates.clear();	//make sure this is empty first
	data.EStates[el].NRates.Clear();
	for(unsigned int pts=0; pts < BStructPts.size(); ++pts)
	{
		if(BStructPts[pts]->contactData)
			ProcessContactSteadyState(mdl,BStructPts[pts]->contactData, sim, ptrE);	//this will store all the original rates in ptrE
	}
	for (unsigned int trg = 0; trg < ptrE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
	{
		if (CalcFastRate(ptrE, ptrE->TargetELev[trg], sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
		{
			if(NRateIn==0.0)
				continue;
			if(ptrE->TargetELev[trg]->imp)
				degen = ptrE->TargetELev[trg]->imp->getDegeneracy();
			else
				degen = 1.0;
			//the net rate in was valid.
			data.EStates[el].NRates.AddValue(NRateIn);
			totNRateIn += NRateIn;
			double chrgTransfer;	//what is the charged rate out of the target
			// > 0 means positive charges are entering the target or negative charges leaving
			// < 0 means negative charges are entering the target or positive charges leaving
			if(ptrE->carriertype==ELECTRON) //the source state is an electron
			{
				//a positive NRate in means negative charges are entering the source
				//so negative charges are leaving the target, which means chrgTransfer > 0. No sign change
				chrgTransfer = NRateIn * tStep;
			}
			else
			{
				chrgTransfer = -NRateIn * tStep;
			}
			//if NRateIn positive, positive charges are entering the source
			//so positive charges are leaving the target, we want chrgTrans to be <0, but NRateIn > 0, so flip sign

			ptrE->TargetELev[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			if(ptrE->TargetELev[trg]->carriertype == ELECTRON)
			{
				// chrgTransfer is the rate OUT of the target.
				//if > 0, Pos charges entering/negative charges leaving.
				//for electron states, that means subtract chrgTransfer from occupancy
				if(chrgTransfer > 0.0 && chrgTransfer > occ)
					chrgTransfer = 0.5 * occ;
				else if(chrgTransfer < 0.0 && -chrgTransfer > avail)
					chrgTransfer = -0.5 * avail;

				ptrE->TargetELev[trg]->RemoveBeginOccStates(chrgTransfer);
				ptrE->TargetELev[trg]->RemoveEndOccStates(chrgTransfer);
			}
			else
			{
				// chrgTransfer is the rate Out of the target
				//if >0, Pos charges entering target or neg leaving.
				//these are holes, so add occ states

				if(chrgTransfer > 0.0 && chrgTransfer > avail)
					chrgTransfer = 0.5 * avail;
				else if(chrgTransfer < 0.0 && -chrgTransfer > occ)
					chrgTransfer = -0.5 * occ;

				ptrE->TargetELev[trg]->AddBeginOccStates(chrgTransfer);
				ptrE->TargetELev[trg]->AddEndOccStates(chrgTransfer);
			}
			ptrE->TargetELev[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			data.EStates[ptrE->TargetELev[trg]->ssLink].curEf = RelEfFromOcc(occ, avail, ptrE->TargetELev[trg]->max - ptrE->TargetELev[trg]->min, kT, degen);
			if(data.EStates[ptrE->TargetELev[trg]->ssLink].curEf <= 0.0)
			{
				data.EStates[ptrE->TargetELev[trg]->ssLink].occupancy = occ;
				data.EStates[ptrE->TargetELev[trg]->ssLink].OccMatches = true;
			}
			else
			{
				data.EStates[ptrE->TargetELev[trg]->ssLink].occupancy = avail;
				data.EStates[ptrE->TargetELev[trg]->ssLink].OccMatches = false;
			}
			

		}
	}

	//all the drift diffusion rates
	for (unsigned int trg = 0; trg < ptrE->TargetELevDD.size(); ++trg)
	{
		if (CalcFastRate(ptrE, ptrE->TargetELevDD[trg].target, sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
		{
			if(NRateIn==0.0)
				continue;
			data.EStates[el].NRates.AddValue(NRateIn);
			totNRateIn += NRateIn;
			//the net rate in was valid.
			double chrgTransfer;	//what is the charged rate out of the target
			// > 0 means positive charges are entering the target or negative charges leaving
			// < 0 means negative charges are entering the target or positive charges leaving
			if(ptrE->carriertype==ELECTRON) //the source state is an electron
			{
				//a positive NRate in means negative charges are entering the source
				//so negative charges are leaving the target, which means chrgTransfer > 0. No sign change
				chrgTransfer = NRateIn * tStep;
			}
			else
				chrgTransfer = -NRateIn * tStep;
			//if NRateIn positive, positive charges are entering the source
			//so positive charges are leaving the target, we want chrgTrans to be <0, but NRateIn > 0, so flip sign
			ptrE->TargetELevDD[trg].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);

			if(ptrE->TargetELevDD[trg].target->carriertype == ELECTRON)
			{
				// chrgTransfer is the rate OUT of the target.
				//if > 0, Pos charges entering/negative charges leaving.
				//for electron states, that means subtract chrgTransfer from occupancy
				if(chrgTransfer > 0.0 && chrgTransfer > occ)
					chrgTransfer = 0.5 * occ;
				else if(chrgTransfer < 0.0 && -chrgTransfer > avail)
					chrgTransfer = -0.5 * avail;

				ptrE->TargetELevDD[trg].target->RemoveBeginOccStates(chrgTransfer);
				ptrE->TargetELevDD[trg].target->RemoveEndOccStates(chrgTransfer);
			}
			else
			{
				// chrgTransfer is the rate Out of the target
				//if >0, Pos charges entering target or neg leaving.
				//these are holes, so add occ states
				if(chrgTransfer > 0.0 && chrgTransfer > avail)
					chrgTransfer = 0.5 * avail;
				else if(chrgTransfer < 0.0 && -chrgTransfer > occ)
					chrgTransfer = -0.5 * occ;
				
				ptrE->TargetELevDD[trg].target->AddBeginOccStates(chrgTransfer);
				ptrE->TargetELevDD[trg].target->AddEndOccStates(chrgTransfer);
			}

			ptrE->TargetELevDD[trg].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			kT = KB * ptrE->TargetELevDD[trg].target->getPoint().temp;
			data.EStates[ptrE->TargetELevDD[trg].target->ssLink].curEf = RelEfFromOcc(occ, avail, ptrE->TargetELevDD[trg].target->max - ptrE->TargetELevDD[trg].target->min, kT);
			if(data.EStates[ptrE->TargetELevDD[trg].target->ssLink].curEf <= 0.0)
			{
				data.EStates[ptrE->TargetELevDD[trg].target->ssLink].occupancy = occ;
				data.EStates[ptrE->TargetELevDD[trg].target->ssLink].OccMatches = true;
			}
			else
			{
				data.EStates[ptrE->TargetELevDD[trg].target->ssLink].occupancy = avail;
				data.EStates[ptrE->TargetELevDD[trg].target->ssLink].OccMatches = false;
			}
		}
	}

	for(unsigned int i=0; i<ptrE->Rates.size(); ++i)
	{
		if(ptrE->Rates[i].netrate==0.0)
			continue;

		double NRateSrcIn = DoubleSubtract(ptrE->Rates[i].RateIn, ptrE->Rates[i].RateOut);
		data.EStates[el].NRates.AddValue(NRateSrcIn);
		totNRateIn += NRateSrcIn;	//netRate could be Rout - Rin, though unlikely

		if(data.Pt[data.EStates[ptrE->Rates[i].target->ssLink].dataPt].ctcSink)
			continue;

		double chrgTransfer;

		if(ptrE->Rates[i].sourcecarrier == ptrE->Rates[i].targetcarrier)
			chrgTransfer = -NRateSrcIn * tStep;	//the target must lose the carriers the source gained
		else  //the target does the same thing as the source because opposite carrier types
			chrgTransfer = NRateSrcIn * tStep;
		
		ptrE->Rates[i].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
		//don't let it go beyond - if it does, it probably isn't having much an effect on the source anyway
		

		if(chrgTransfer < 0.0 && -chrgTransfer > occ)
			chrgTransfer = -0.5*occ;
		else if(chrgTransfer > 0.0 && chrgTransfer > avail)
			chrgTransfer = 0.5 * avail;
			// chrgTransfer is the rate Out of the target
			//if >0, Pos charges entering target or neg leaving.
			//these are holes, so add occ states
			

		ptrE->Rates[i].target->AddBeginOccStates(chrgTransfer);
		ptrE->Rates[i].target->AddEndOccStates(chrgTransfer);

		ptrE->Rates[i].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
		kT = KB * ptrE->Rates[i].target->getPoint().temp;
		data.EStates[ptrE->Rates[i].target->ssLink].curEf = RelEfFromOcc(occ, avail, ptrE->Rates[i].target->max - ptrE->Rates[i].target->min, kT);
		if(data.EStates[ptrE->Rates[i].target->ssLink].curEf <= 0.0)
		{
			data.EStates[ptrE->Rates[i].target->ssLink].occupancy = occ;
			data.EStates[ptrE->Rates[i].target->ssLink].OccMatches = true;
		}
		else
		{
			data.EStates[ptrE->Rates[i].target->ssLink].occupancy = avail;
			data.EStates[ptrE->Rates[i].target->ssLink].OccMatches = false;
		}
	}

	for(unsigned int i=0; i<ptrE->OpticalRates.size(); ++i)
	{
		if(ptrE->OpticalRates[i].netrate==0.0)
			continue;

		double NRateSrcIn = DoubleSubtract(ptrE->OpticalRates[i].RateIn, ptrE->OpticalRates[i].RateOut);
		data.EStates[el].NRates.AddValue(NRateSrcIn);
		totNRateIn += NRateSrcIn;	//netRate could be Rout - Rin, though unlikely

		if(data.Pt[data.EStates[ptrE->OpticalRates[i].target->ssLink].dataPt].ctcSink)
			continue;

		double chrgTransfer;

		if(ptrE->OpticalRates[i].sourcecarrier == ptrE->OpticalRates[i].targetcarrier)
			chrgTransfer = -NRateSrcIn * tStep;	//the target must lose the carriers the source gained
		else  //the target does the same thing as the source because opposite carrier types
			chrgTransfer = NRateSrcIn * tStep;
		
		ptrE->OpticalRates[i].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
		//don't let it go beyond - if it does, it probably isn't having much an effect on the source anyway
		

		if(chrgTransfer < 0.0 && -chrgTransfer > occ)
			chrgTransfer = -0.5*occ;
		else if(chrgTransfer > 0.0 && chrgTransfer > avail)
			chrgTransfer = 0.5 * avail;
			// chrgTransfer is the rate Out of the target
			//if >0, Pos charges entering target or neg leaving.
			//these are holes, so add occ states
			

		ptrE->OpticalRates[i].target->AddBeginOccStates(chrgTransfer);
		ptrE->OpticalRates[i].target->AddEndOccStates(chrgTransfer);

		ptrE->OpticalRates[i].target->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
		kT = KB * ptrE->OpticalRates[i].target->getPoint().temp;
		data.EStates[ptrE->OpticalRates[i].target->ssLink].curEf = RelEfFromOcc(occ, avail, ptrE->OpticalRates[i].target->max - ptrE->OpticalRates[i].target->min, kT);
		if(data.EStates[ptrE->OpticalRates[i].target->ssLink].curEf <= 0.0)
		{
			data.EStates[ptrE->OpticalRates[i].target->ssLink].occupancy = occ;
			data.EStates[ptrE->OpticalRates[i].target->ssLink].OccMatches = true;
		}
		else
		{
			data.EStates[ptrE->OpticalRates[i].target->ssLink].occupancy = avail;
			data.EStates[ptrE->OpticalRates[i].target->ssLink].OccMatches = false;
		}
	}


	totNRateIn = data.EStates[el].NRates.GetNetSum() * tStep;
	ptrE->AddBeginOccStates(totNRateIn);
	ptrE->AddEndOccStates(totNRateIn);
	kT = KB * ptrE->getPoint().temp;
	ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
	data.EStates[el].curEf = RelEfFromOcc(occ, avail, ptrE->max - ptrE->min, kT);
	if(data.EStates[el].curEf <= 0.0)
	{
		data.EStates[el].occupancy = occ;
		data.EStates[el].OccMatches = true;
	}
	else
	{
		data.EStates[el].occupancy = avail;
		data.EStates[el].OccMatches = false;
	}
	

	for(unsigned int i=0; i<BStructPts.size(); ++i)	//everything changed
	{
		BStructPts[i]->ResetData();
		mdl.poissonRho[BStructPts[i]->adj.self] = BStructPts[i]->rho;
		double Nbeta = 0.0;
		if(BStructPts[i]->temp > 0.0)
			Nbeta = -KBI / BStructPts[i]->temp;

		CalcScatterConsts(BStructPts[i]->scatterCBConst, BStructPts[i]->cb.energies, Nbeta);
		CalcScatterConsts(BStructPts[i]->scatterVBConst, BStructPts[i]->vb.energies, Nbeta);
	}
	data.BandPtsUpdated.clear();
	for(unsigned int i=0; i<BStructPts.size(); ++i)	//now that the rho's are updated, adjust that too
	{
		CalcPointBandFast(*BStructPts[i],mdl);
		data.BandPtsUpdated.push_back(BStructPts[i]);	//these are the only three which are up to date
	}


	return(0.0);
}

double LoopAllStatesRateToZero(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread)
{
	//this is modelled after the thermal equilibrium code
	//go through it forward and reverse

	double largestChange = 0.0;
	double tmpChange;

	unsigned int ptSize = data.Pt.size();

//	CalcBandStruct(mdl, mdesc);	//make sure everything starts out on the same page
	//CalcBandStrcut gets done in CreateLightSourcesFast. The band structure is locally updated in GetStateRateZero
	
	if(data.CalcStateType == SS_SLOWPTS)
	{
		CreateLightSourcesFast(data, mdl, mdesc);
		Light(mdl, mdesc, NULL, kThread);	//fill in the optical rates in each state - which were just cleared when the light sources were made
		//first populate slow points while doing a convergence calc
		largestChange = SS_SlowPtLoop(data, mdl, mdesc, InitCalc, kThread);
	}	//end it started out as a do the icky localized stuff first
	else if(data.CalcStateType == SS_TRANS_ACTUAL)
	{
		for(unsigned int i=0; i<data.Pt.size(); ++i)
		{
			tmpChange = SSTransEqPoint(data, mdl, mdesc, i, true);
			if(largestChange < tmpChange)
				largestChange = tmpChange;
		}
		CalcBandStruct(mdl, mdesc);
	}
	else if(data.CalcStateType == SS_NEWTON_ALL || data.CalcStateType == SS_NEWTON_NOLIGHT)
	{
//		CompareMatrixOutputs(data, mdl, mdesc, kThread);	//just see what the hell is going on why one converges and the other doesn't
		largestChange = SSNewton(data, mdl, mdesc, kThread);	//default false to full matrix
	}
	else if(data.CalcStateType == SS_NEWTON_NL_FULL || data.CalcStateType == SS_NEWTON_L_FULL)
	{
		largestChange = SSNewton(data, mdl, mdesc, kThread, true);	//does full matrix if it seems to be having trouble converging
	}
	else if (data.CalcStateType == SS_TRANS_CUTOFF)
	{
		largestChange = SSByTransCutoff(data, mdl, mdesc, InitCalc, kThread);
	}
	else if (data.CalcStateType == SS_MONTECARLO)
	{
		largestChange = SS_MC(data, mdl, mdesc, InitCalc, kThread, 0.0078125); // (1/128)
	}
	else if (data.CalcStateType == SS_NEWTON_NUMERICAL)
	{
		largestChange = SS_Newton_Numerical(data, mdl, mdesc, kThread, NULL);
	}
	else if(data.CalcStateType != SS_TRANSLIKE)
	{
		CreateLightSourcesFast(data, mdl, mdesc);
		Light(mdl, mdesc, NULL, kThread);	//fill in the optical rates in each state - which were just cleared when the light sources were made
		data.BandPtsUpdated.clear();
		tmpChange = LoopAllStatesForward(data, mdl, mdesc);
		if(fabs(tmpChange) > largestChange)
			largestChange = fabs(tmpChange);

		if(data.CalcStateType == SS_MATCH_IMPS)	//nothing will change on a second iteration
			return(largestChange);

		tmpChange = LoopAllStatesBackward(data, mdl, mdesc);
		if(fabs(tmpChange) > largestChange)
			largestChange = fabs(tmpChange);
	}
	else
	{
		CreateLightSourcesFast(data, mdl, mdesc);
		Light(mdl, mdesc, NULL, kThread);	//fill in the optical rates in each state - which were just cleared when the light sources were made
		largestChange = SS_Transtype(data, mdl, mdesc, InitCalc, kThread);
	}
	
	

	return(largestChange);
}


//finds the fermi level for the state to within accuracy digits that has a rate of zero (or rates on opposite sides of zero)
double GetStateRateZero(ELevelLocal& eLev, PtLocal& pt, SteadyState_SAAPD& data, Model& mdl, Simulation* sim, bool RetInitNetRate)
{
	double NetRate = 0.0;
	double initEf = eLev.curEf;
	eLev.NRates.Clear();

	//assume the light data is constant.
	ELevel* ptrE = eLev.ptr;
	Point* ptrPt = &ptrE->getPoint();
	if(data.CalcStateType == SS_MATCH_IMPS)	//don't work on bands for this section. Match the impurities to the bands
	{
		if(ptrE->imp == NULL && ptrE->bandtail==false)
			return(0.0);
	}


	std::vector<Point*> BStructPts;
	std::vector<double> PoissonCoeffs;

	
	double tmp=0.0;
	
	BStructPts.push_back(ptrPt);
	FindPoissonCoeff(ptrPt, ptrPt->adj.self, tmp);	//The proper PsiCf 
	PoissonCoeffs.push_back(tmp);	//pre multiplied by charge&volume, so only multiply by positive charges

	if(ptrPt->adj.adjMX)
	{
		BStructPts.push_back(ptrPt->adj.adjMX);
		FindPoissonCoeff(ptrPt, ptrPt->adj.adjMX->adj.self, tmp);	//The proper PsiCf 
		PoissonCoeffs.push_back(tmp);
	}
	if(ptrPt->adj.adjPX)
	{
		BStructPts.push_back(ptrPt->adj.adjPX);
		FindPoissonCoeff(ptrPt, ptrPt->adj.adjPX->adj.self, tmp);	//The proper PsiCf 
		PoissonCoeffs.push_back(tmp);
	}

	
	for(unsigned int pts=0; pts<BStructPts.size(); ++pts)
	{
		bool in=false;
		for(unsigned int j=0; j<data.BandPtsUpdated.size(); ++j)
		{
			if(BStructPts[pts] == data.BandPtsUpdated[j])
			{
				in = true;
				break;
			}
		}
		if(!in)
			CalcPointBandFast(*BStructPts[pts], mdl);	//get this reset before it is used
	}

	data.BandPtsUpdated = BStructPts;	//these are the ones that will be kept up to date in this function

	double Nbeta = -KBI / ptrPt->temp;
	//initialization
	double scatConst=0.0;
	if(ptrE->imp==NULL && ptrE->bandtail==false)
	{
		if(eLev.carType == ELECTRON)
			scatConst = 1.0 / ptrPt->scatterCBConst;	//uninvert it so it's backto a sum of avail * boltzeUse
		else
			scatConst = 1.0 / ptrPt->scatterVBConst;	//uninvert it so it's backto a sum of avail * boltzeUse
	}

	bool keepGoing = true;
	double accuracyBottom = 1.0 - data.tolerance;	//0.99999999 ish
	bool SetRout = false;
	bool SetRin = false;
	double stateWidth = ptrE->max - ptrE->min;
	double degen = 1.0;
	if(ptrE->imp)
		degen = ptrE->imp->getDegeneracy();

	double occ, RateCur, RatePrev, prevOcc, avail, prevAvail, initNRate;
	initNRate = 0.0;
	RateCur = RatePrev = 0.0;
	prevOcc = ptrE->GetBeginOccStates();
	double minInEf = eLev.curEf - data.tolerance * 100.0;	//data.tolerance <= 1.0e-5
	double maxOutEf = eLev.curEf + data.tolerance * 100.0;

	for(int ctr=0; keepGoing==true; ++ctr)
	{
		double NRateIn;
		//set the current EF based on whether it's the first iteration, the second iteration, or afterwards
		if(ctr == 0) //first time, try and cut it in half and get a better resolution
		{
			eLev.prevEf = eLev.curEf;
//			occ = prevOcc;
			//don't change ANYTHING. Want to start at it's original value
			//seeing as the trans_ss idea was bad, reimplement this.
			if(eLev.LRinEf != -100.0 && eLev.LRoutEf != -100.0)	//they are both valid from before
				eLev.curEf = 0.5 * (eLev.LRinEf + eLev.LRoutEf);
			else if(eLev.LRinEf != -100.0)
				eLev.curEf = eLev.LRinEf;
			else if(eLev.LRoutEf != 100.0)
				eLev.curEf = eLev.LRoutEf;
			//else don't change it and just recalculate the rEf
			occ = OccAvailFromRelEf(eLev.curEf, eLev.NStates, -Nbeta, stateWidth, degen);
		}
		else if(ctr == 1)	//see if the other max is still on the outside. This should just improve the answer
		{
			initNRate = eLev.NRates.GetNetSum();
			RatePrev = RateCur;
			eLev.prevEf = eLev.curEf;
			double oldOcc = OccAvailFromRelEf(eLev.prevEf, eLev.NStates, -Nbeta, stateWidth, degen);
			do
			{
				if(SetRin)
				{
					//now let's test the rate out one and see if that is gonna stay the same

					//this has no value. The Rate in was set, so we want to add more carriers,
					//or increase the curEf.
					eLev.curEf += fabs(eLev.curEf * data.tolerance * 32.0);

				}
				else //setRout must have been set, or the loop exitted
				{
					//trying to lose carriers, so decrease curEf
					eLev.curEf -= fabs(eLev.curEf * data.tolerance * 32.0);

				}
				occ = OccAvailFromRelEf(eLev.curEf, eLev.NStates, -Nbeta, stateWidth, degen);
			} while(occ == oldOcc);	//make sure it gets to the point where it can change if Ef is all crazy far and causing =0
		}
		else
		{
			//do the secant method
			double difRates = RateCur - RatePrev;	//(-) - (+) = (-)
			double difEf = eLev.curEf - eLev.prevEf; //(+)
			eLev.prevEf = eLev.curEf;
			if(difEf == 0.0 || difRates == 0.0)
			{
				if(difRates == 0.0)	//it's not going anywhere. Make it move!
				{
					if(SetRin && SetRout && eLev.LRinEf != eLev.LRoutEf)
						eLev.curEf = 0.5 * (eLev.LRinEf + eLev.LRoutEf);
					else if(RateCur > 0.0)
					{
						if(!SetRout)
							eLev.curEf += 0.1;
						else
							eLev.curEf += fabs(eLev.curEf * data.tolerance );
					}
					else
					{
						if(!SetRin)
							eLev.curEf -= 0.1;
						else
							eLev.curEf -= fabs(eLev.curEf * data.tolerance);
					}
					if(eLev.curEf == eLev.prevEf)	//it's not going anywhere
					{
						if(SetRout && SetRin)	//probably averaging 2 found numbers that are good
							break;	//it's found the answer. Stop looking
						if(SetRout)	//haven't found a rate in yet. bump things in that direction
						{
							eLev.LRinEf = eLev.LRoutEf - 0.1;
							eLev.curEf -= 0.05;
						}
						else
						{
							eLev.LRoutEf = eLev.LRinEf + 0.1;
							eLev.curEf += 0.05;
						}

					}
				}
				else if(RateCur > 0)	//gaining carriers, so increase Ef
				{
					if(eLev.LRoutEf == 100.0)
						eLev.curEf += 0.1;	//only do a 'tiny' adjustment - it should still be rather large
					else
					{
						difEf = eLev.LRoutEf - eLev.curEf;
						if(difEf > 0.1)	//just in case it does something stupid and diverges a shitton
							eLev.curEf += 0.1;
						else
							eLev.curEf += fabs(eLev.curEf * data.tolerance * 0.125);
					}
				}
				else
				{
					if(eLev.LRinEf == -100.0)
						eLev.curEf -= 0.1;
					else
					{
						if(eLev.curEf - eLev.LRinEf > 0.1)
							eLev.curEf -= 0.1;
						else
							eLev.curEf -= fabs(eLev.curEf * data.tolerance * 0.125);
					}
				}

			}
			else
			{
				double slope = difRates / difEf;	// (-) / (+) = (-)
				double distFromCur = -RateCur / slope; // -(-) / (-) = (-)
				//so when occ is practically zero, the rates can turn over
				//aka, removing carriers decreases the rate in. This is very counter intuitive.
				//regardless, it basically causes an infinite loop, so we need to restrict things a bit
				if(RateCur > 0.0)
				{
					//distFromCur better be positive
					if(distFromCur < 0.0)
						distFromCur = -distFromCur;
					//if it's changing a ton, that ought to be limited as well
					if(SetRout && distFromCur + eLev.curEf > eLev.LRoutEf)
						distFromCur = 0.25 * (eLev.LRoutEf - eLev.curEf);
					if(distFromCur > 0.25)
						distFromCur = 0.25;
				}
				else if(RateCur < 0.0)
				{
					//distFromCur better be negative...
					if(distFromCur > 0.0)
						distFromCur = -distFromCur;
					if(SetRin && distFromCur + eLev.curEf < eLev.LRinEf)
						distFromCur = 0.25 * (eLev.LRinEf - eLev.curEf);;
					if(distFromCur < -0.25)
						distFromCur = -0.25;
				}

				if(fabs(distFromCur) < (eLev.curEf * data.tolerance))	//it is basically on the solution
				{
					eLev.curEf += distFromCur;
					break;
				}
				double newEf = eLev.curEf + distFromCur;
				if(newEf == eLev.curEf)	//we're not really gonna get any closer to the answer
				{
					eLev.LRinEf = eLev.LRoutEf = eLev.curEf;
					break;
				}
				eLev.curEf = newEf;
			}
			//always converge on an answer if it tries to diverge cuz system is stupid
			if((SetRin && SetRout) && (eLev.curEf >= eLev.LRoutEf || eLev.curEf <= eLev.LRinEf))
				eLev.curEf = 0.5 * (eLev.LRinEf + eLev.LRoutEf);

			//now that the Ef has been set, we can shift the Ef/Rates

			RatePrev = RateCur;
			occ = OccAvailFromRelEf(eLev.curEf, eLev.NStates, -Nbeta, stateWidth, degen);
		}

		
		ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, prevOcc, prevAvail);	//just in case it had the opposite occupancy, now it's fine.
		if(eLev.curEf > 0.0)	//lots of carriers, stores availability
		{
			eLev.occupancy = occ;
			eLev.OccMatches = false;
			ptrE->SetBeginOccStatesOpposite(eLev.occupancy);
			ptrE->SetEndOccStatesOpposite(eLev.occupancy);	//needed for updating scatter consts
		}
		else
		{
			eLev.occupancy = occ;
			eLev.OccMatches = true;
			ptrE->SetBeginOccStates(eLev.occupancy);
			ptrE->SetEndOccStates(eLev.occupancy);	//needed for updating scatter conts
		}
		ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);	//just in case it had the opposite occupancy, now it's fine.

		//now do a "quick" reset data
		if(eLev.carType == ELECTRON)
		{
			double change = occ - prevOcc;
			if(change == 0.0 && (occ == 0.0 || occ==eLev.NStates) && ctr!=0)
				break;	//we're toast. Just move on. This state is hopeless
			if(ptrE->imp==NULL && ptrE->bandtail==false)
			{
				//it's the conduction band
				if(change>=0.0)
					ptrPt->cb.AddNumParticles(change);
				else
					ptrPt->cb.RemoveNumParticles(-change);

				scatConst += (avail - prevAvail) * ptrE->boltzeUse;	//just update the one term in the sum
				ptrPt->scatterCBConst = 1.0 / scatConst;
			}
			change = -change;	//this now represents the change in charge (no Q)
			ptrPt->rho += change * pt.QVoli;	//electron, so negative charge
			mdl.poissonRho[pt.mdlRho] = ptrPt->rho;
			for(unsigned int pts=0; pts< BStructPts.size(); ++pts)
			{
				double delta = change * PoissonCoeffs[pts];	//how much the band diagram shifts
				BStructPts[pts]->Psi += delta;
				BStructPts[pts]->cb.bandmin += delta;
				BStructPts[pts]->vb.bandmin += delta;
				BStructPts[pts]->fermi += delta;
				BStructPts[pts]->fermin += delta;
				BStructPts[pts]->fermip += delta;
			}
		}
		else
		{
			double change = occ - prevOcc;
			if(change == 0.0 && (occ == 0.0 || occ==eLev.NStates) && ctr!=0)
				break;	//we're toast. Just move on. This state is hopeless
			if(ptrE->imp==NULL && ptrE->bandtail==false)
			{
				//it's the conduction band
				if(change>=0.0)
					ptrPt->vb.AddNumParticles(change);
				else
					ptrPt->vb.RemoveNumParticles(-change);

				scatConst += (avail - prevAvail) * ptrE->boltzeUse;	//just update the one term in the sum
				ptrPt->scatterVBConst = 1.0 / scatConst;
			}
			ptrPt->rho += change * pt.QVoli;
			mdl.poissonRho[pt.mdlRho] = ptrPt->rho;
			for(unsigned int pts=0; pts< BStructPts.size(); ++pts)
			{
				double delta = change * PoissonCoeffs[pts];	//how much the band diagram shifts
				BStructPts[pts]->Psi += delta;
				BStructPts[pts]->cb.bandmin += delta;
				BStructPts[pts]->vb.bandmin += delta;
				BStructPts[pts]->fermi += delta;
				BStructPts[pts]->fermin += delta;
				BStructPts[pts]->fermip += delta;
			}
		}


		

		eLev.NRates.Clear();	//clear out the sum of rates

		for (unsigned int trg = 0; trg < ptrE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
		{
			if (CalcFastRate(ptrE, ptrE->TargetELev[trg], sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false) == true)	//returns true if calculated rate
				eLev.NRates.AddValue(NRateIn);
		}

		//all the drift diffusion rates
		for (unsigned int trg = 0; trg < ptrE->TargetELevDD.size(); ++trg)
		{
			if (CalcFastRate(ptrE, ptrE->TargetELevDD[trg].target, sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false) == true)	//returns true if calculated rate
				eLev.NRates.AddValue(NRateIn);
		}

		ptrE->Rates.clear();	//clear all rates stored in the main elevel pointer - does not include optical rates

		//if it's a contact, store any contact resulting rates in ptrE->Rates.
		for(unsigned int pts=0; pts < BStructPts.size(); ++pts)
		{
			if(BStructPts[pts]->contactData)
				ProcessContactSteadyState(mdl,BStructPts[pts]->contactData, sim, ptrE);
		}
		//that should add any rates into ptrE->Rates
		

		//go through all these added rates and add them to the NRates PosNegSum.
		for(unsigned int i=0; i<ptrE->Rates.size(); ++i)
			eLev.NRates.AddValue(ptrE->Rates[i].netrate);	//should just be contacts if there are any. Contact net rates are In-Out

		//now add any optical rates associated with this energy state
		for(unsigned int i=0; i<ptrE->OpticalRates.size(); ++i)	//keep the light rates constant throughout
		{
			eLev.NRates.AddValue(ptrE->OpticalRates[i].RateIn);	//optical.netRate is based on type. 
			eLev.NRates.SubtractValue(ptrE->OpticalRates[i].RateOut);	//NOT NECESSARILY R_in - R_out
		}

		RateCur = eLev.NRates.GetNetSum();	//get the sum in a cancelling manner.

		if(RateCur > 0.0)
		{
			//then it is a rate in. This will be a lower Ef.
			SetRin = true;
			eLev.LRinEf = eLev.curEf;
			if(!SetRout)
			{
				if(eLev.LRoutEf < eLev.LRinEf)
					eLev.LRoutEf = eLev.LRinEf + 0.25;
			}

/*			if(eLev.curEf <= minInEf)	//only allow small adjustments!
			{
				eLev.curEf = minInEf;
				break;
			}*/


		}
		else if(RateCur < 0.0)
		{
			//it is a rate out. Ef is too high (too many carriers)
			SetRout = true;
			eLev.LRoutEf = eLev.curEf;
			if(!SetRin)
			{
				if(eLev.LRoutEf < eLev.LRinEf)
					eLev.LRinEf = eLev.LRoutEf - 0.25;
			}
/*			if(eLev.curEf >= maxOutEf)
			{
				eLev.curEf = maxOutEf;	//only allow small adjustments!
				break;
			}*/

		}
		else //nailed it
		{
			//we're just done.  We found the awesome value.
			keepGoing = false;
			eLev.LRinEf = eLev.LRoutEf = eLev.curEf;
		}

		

		if(SetRin && SetRout)
		{
			double minTestEf = fabs(eLev.LRoutEf) * data.tolerance;	//the tolerance alotted
			minTestEf = eLev.LRoutEf - minTestEf;	//now actually get the real value
			if(eLev.LRinEf >= minTestEf)
			{
				keepGoing = false;
				if(fabs(RatePrev) < fabs(RateCur))	//choose the better of the two previous iterations
					eLev.curEf = eLev.prevEf;
			}
		}


	}

	//it seems to somehow be missing this and allowing jumps > 100 * data.tolerance
	/*
	if(eLev.curEf < minInEf)
		eLev.curEf = minInEf;
	else if(eLev.curEf > maxOutEf)
		eLev.curEf = maxOutEf;
	*/
	//the value has been set, make sure everything is completely up to date
	occ = OccAvailFromRelEf(eLev.curEf, eLev.NStates, -Nbeta, stateWidth, degen);
	ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, prevOcc, prevAvail);	//just in case it had the opposite occupancy, now it's fine.
	if(eLev.curEf > 0.0)	//lots of carriers, stores availability
	{
		eLev.occupancy = occ;
		eLev.OccMatches = false;
		ptrE->SetBeginOccStatesOpposite(eLev.occupancy);
		ptrE->SetEndOccStatesOpposite(eLev.occupancy);	//needed for updating scatter consts
	}
	else
	{
		eLev.occupancy = occ;
		eLev.OccMatches = true;
		ptrE->SetBeginOccStates(eLev.occupancy);
		ptrE->SetEndOccStates(eLev.occupancy);	//needed for updating scatter conts
	}
	ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);

	if(eLev.carType == ELECTRON)
	{
		double change = occ - prevOcc;
		if(ptrE->imp==NULL && ptrE->bandtail==false)
		{
			//it's the conduction band
			if(change>=0.0)
				ptrPt->cb.AddNumParticles(change);
			else
				ptrPt->cb.RemoveNumParticles(-change);
			scatConst += (avail - prevAvail) * ptrE->boltzeUse;	//just update the one term in the sum
			ptrPt->scatterCBConst = 1.0 / scatConst;
		}
		change = -change;	//this now represents the change in charge (no Q)
		ptrPt->rho += change * pt.QVoli;	//electron, so negative charge
		mdl.poissonRho[pt.mdlRho] = ptrPt->rho;
		for(unsigned int pts=0; pts< BStructPts.size(); ++pts)
		{
			double delta = change * PoissonCoeffs[pts];	//how much the band diagram shifts
			BStructPts[pts]->Psi += delta;
			BStructPts[pts]->cb.bandmin += delta;
			BStructPts[pts]->vb.bandmin += delta;
			BStructPts[pts]->fermi += delta;
			BStructPts[pts]->fermin += delta;
			BStructPts[pts]->fermip += delta;
		}
	}
	else
	{
		double change = occ - prevOcc;
		if(ptrE->imp==NULL && ptrE->bandtail==false)
		{
			if(change>=0.0)
				ptrPt->vb.AddNumParticles(change);
			else
				ptrPt->vb.RemoveNumParticles(-change);
			scatConst += (avail - prevAvail) * ptrE->boltzeUse;	//just update the one term in the sum
			ptrPt->scatterVBConst = 1.0 / scatConst;
		}
		ptrPt->rho += change * pt.QVoli;
		mdl.poissonRho[pt.mdlRho] = ptrPt->rho;
		for(unsigned int pts=0; pts< BStructPts.size(); ++pts)
		{
			double delta = change * PoissonCoeffs[pts];	//how much the band diagram shifts
			BStructPts[pts]->Psi += delta;
			BStructPts[pts]->cb.bandmin += delta;
			BStructPts[pts]->vb.bandmin += delta;
			BStructPts[pts]->fermi += delta;
			BStructPts[pts]->fermin += delta;
			BStructPts[pts]->fermip += delta;
		}
	}

	if(RetInitNetRate)
		return(initNRate);

	return(initEf - eLev.curEf);
}


int AdjustStateEf(SteadyState_SAAPD& data)
{
	//tolerance is how much you allow Ef to change, which starts at 0.1eV, so this means 0.01 eV
	unsigned int ptSize = data.Pt.size();	//should also be ELevelIndicesInPt size
	double adjustLarge = data.tolerance * SS_EF_ALTER;	//25%
	double adjustSmall = data.tolerance * SS_SS_EF_ALTER;		//~12.5%
	data.ReduceTolerance = true;	//default to say it's good

	int method;
	double totQ = data.varCharge.GetNetSumClear();
	if(totQ <= data.targetCharge + data.chargeTolerance && totQ >= data.targetCharge - data.chargeTolerance)
		method = SS_NO_CHARGE_RESTRICTIONS;
	else if(totQ > data.targetCharge + data.chargeTolerance)
		method = SS_CHARGE_DECREASE;
	else
		method = SS_CHARGE_INCREASE;

	method = SS_NO_CHARGE_RESTRICTIONS;	//just see if this cures that last bit
//	ProgressInt(69, method);
//	ProgressDouble(51, totQ);

	data.varCharge.Clear();
	data.PosChargeChanges.SetSize(ptSize);	//prevent a bunch of allocations
	data.NegChargeChanges.SetSize(ptSize);
	double furthestEf = 0.0;

	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change

		if(data.Pt[pt].ctcSink==false)	//update stuff because it is not constant
		{
			double kTI = data.EStates[start].ptr->getPoint().temp>0.0 ? KBI / data.EStates[start].ptr->getPoint().temp:0.0;
			data.Pt[pt].VariableCharge.Clear();	//make sure it's cleared out before it gets reset

			for (long int el = start; el <= stop; ++el)
			{
			
				//add in additional rates - optical and contacts
				data.EStates[el].NRates.AddValue(data.EStates[el].ptr->RateSums.GetNetSum());
				data.EStates[el].DerivSOnly.AddValue(data.EStates[el].ptr->derivsourceonly);
				double NetRate = data.EStates[el].NRates.GetNetSum();
				double MaxDeltaEf = maxEfChange(data.EStates[el]) * 10.0;	//give it a little wiggle room to allow convergence
				if(fabs(MaxDeltaEf) > furthestEf)
					furthestEf = fabs(MaxDeltaEf);
				//adjust relative Ef by rules
				//update LRin/outEf appropriately with what's currently in prevEf
				//this returns true if the Ef for a rate in and rate out are within the tolerance
				if(method == SS_NO_CHARGE_RESTRICTIONS)
				{
					if(UpdateStateFermi(data.EStates[el], NetRate, adjustSmall, adjustLarge, data.tolerance, 1.0, MaxDeltaEf) == false)
						data.ReduceTolerance = false;
				}
				else if(method == SS_CHARGE_DECREASE)
				{
					if((data.EStates[el].carType == ELECTRON && NetRate >= 0.0) ||
						(data.EStates[el].carType == HOLE && NetRate <= 0.0))
					{
						if(UpdateStateFermi(data.EStates[el], NetRate, adjustSmall, adjustLarge, data.tolerance, 1.0, MaxDeltaEf) == false)
							data.ReduceTolerance = false;
					}
					else
					{
						if(UpdateStateFermi(data.EStates[el], NetRate, adjustSmall*0.25, adjustLarge*0.25, data.tolerance, 1.0, MaxDeltaEf) == false)
							data.ReduceTolerance = false;
					}
				}
				else if(method == SS_CHARGE_INCREASE)
				{
					if((data.EStates[el].carType == HOLE && NetRate >= 0.0) ||
						(data.EStates[el].carType == ELECTRON && NetRate <= 0.0))
					{
						if(UpdateStateFermi(data.EStates[el], NetRate, adjustSmall, adjustLarge, data.tolerance, 1.0, MaxDeltaEf) == false)
							data.ReduceTolerance = false;
					}
					else
					{
						if(UpdateStateFermi(data.EStates[el], NetRate, adjustSmall*0.25, adjustLarge*0.25, data.tolerance, 1.0, MaxDeltaEf) == false)
							data.ReduceTolerance = false;
					}
				}

				//update occupancy and ChargeChange
				UpdateStateOccupancy(data.EStates[el], kTI);
				data.PosChargeChanges.SetProbability(data.EStates[el].ChangeCharge, el);
				data.NegChargeChanges.SetProbability(-data.EStates[el].ChangeCharge, el);
				
				UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);

			} 	//end for loop through all energy states
			//update pt variable charge
			



			data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();
			double changeCharge = data.Pt[pt].curcharge - data.Pt[pt].prevCharge;
			//if there is more positive charge now than there was beforehand, the change should be positive
			double bStructChange = changeCharge * data.Pt[pt].maxChangeBStruct;
			if (fabs(bStructChange) > data.tolerance) //the amount of charge changing here is too much as it is causing the band diagram to potentially diverge
			{
				//first need to find how much change is allowed (set by data.tolerance)
				double chargeAlterAllowed = data.tolerance / fabs(data.Pt[pt].maxChangeBStruct);
				//maxchangeBStruct is >0 or bStructChange=0 and always data.tolerance>0.0
				//chargeAlterAllowed needs to have it's sign set by changeCharge
				if (changeCharge < 0.0)
					chargeAlterAllowed = -chargeAlterAllowed;

				double chargeAdjustmentNecessary = chargeAlterAllowed - changeCharge;
				//if changed -25, but wanted to only change -5, we'd want to alter by +20. -5 - -25 = 20
				//likewise, changed 25, wanted only 5, want alter by -20. 5-25 = -20

				//now that we know how bad it was off, reduce the adjustments proportionally to
				//get to that change. This should cause curEf to land somewhere between the previous Ef
				//value and the new one that was just calculated
				double percentReduction = fabs(chargeAdjustmentNecessary / changeCharge);

				data.Pt[pt].VariableCharge.Clear();
				double kT = data.EStates[start].ptr->getPoint().temp * KB;

				for (long int el = start; el <= stop; ++el)
				{
					double ChargeAdjust = -percentReduction * data.EStates[el].ChangeCharge;
					//if added electrons: -25 electrons. Only wanted -5. --> ChargeAdjust is +20
					//if removed electrons: remove 25. Only wanted 5. --> ChargeAdjust is -20.
					//if added holes: 25 holes. Only wanted 5. --> ChargeAdjust is -20.


					if(method == SS_NO_CHARGE_RESTRICTIONS || //all states can be adjusted
						(method == SS_CHARGE_DECREASE && ChargeAdjust > 0.0) || //it was allowed to decrease charge, and now it's trying to raise it back up
						(method == SS_CHARGE_INCREASE && ChargeAdjust < 0.0)	//it was allowed to increase charge, and it went too much and is trying ewr it
						)
					{
						double newocc, newavail;
						FindStateOccAdjustCharge(data.EStates[el], ChargeAdjust, newocc, newavail);

						if (data.EStates[el].ptr->imp == NULL)
							data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT);
						else
							data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT, data.EStates[el].ptr->imp->getDegeneracy());

						//this actually doesn't work! ChargeRemove is going to compare to occupancy, which was updated
						//on the first attempt at change. So set true to prevent ChargeRemove from being updated, and then just go with mathematical method  to get it
						newocc = UpdateStateOccupancy(data.EStates[el], kTI, true);	//make sure all the numbers are consistent - true=don't update ChangeCharge
						data.EStates[el].ChangeCharge = (1.0 - percentReduction) * data.EStates[el].ChangeCharge;

						data.PosChargeChanges.SetProbability(data.EStates[el].ChangeCharge, el);	//reset the probabilities
						data.NegChargeChanges.SetProbability(-data.EStates[el].ChangeCharge, el);
					}
					//update pt variable charge
					UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);

				} // end loop through all the energy states in the point

				data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();

			} //end if - the band structure changed too much as a result of this point
		}	//end not contact sink code

		data.varCharge.AddValue(data.Pt[pt].curcharge);
	} //end loop through all the points
	
//	if(furthestEf < data.tolerance)	//it may have not found the Ef on either side, but it's really close on all the values
//		data.ReduceTolerance =true;

	return(0);
}

double maxEfChange(ELevelLocal& eLev)
{
	double NetRate = eLev.NRates.GetNetSum();
	double deriv = eLev.DerivSOnly.GetNetSum();	//this should be negative, always. Adding a carrier should always make the NetRate go down
	double MaxChangeOcc = -NetRate / deriv;
	double newEf=0.0;

	if(eLev.curEf > 0.0)	//there are a ton of carriers in here
	{
		double newAvail = eLev.occupancy - MaxChangeOcc;	//the occupancy is the number of available spots
		double newocc = eLev.NStates - newAvail;
		if(newocc < 0.0)
		{
			newocc = 0.0;
			newAvail = eLev.NStates;
		}
		else if(newocc > eLev.NStates)
		{
			newocc = eLev.NStates;
			newAvail = 0.0;
		}
		
		double kT = KB * eLev.ptr->getPoint().temp;
		if (eLev.ptr->imp == NULL)
			newEf = RelEfFromOcc(newocc, newAvail, eLev.ptr->max - eLev.ptr->min, kT);
		else
			newEf = RelEfFromOcc(newocc, newAvail, eLev.ptr->max - eLev.ptr->min, kT, eLev.ptr->imp->getDegeneracy());
	}
	else
	{
		double newocc = eLev.occupancy + MaxChangeOcc;
		double newAvail = eLev.NStates - newocc;
		if(newocc < 0.0)
		{
			newocc = 0.0;
			newAvail = eLev.NStates;
		}
		else if(newocc > eLev.NStates)
		{
			newocc = eLev.NStates;
			newAvail = 0.0;
		}
		
		double kT = KB * eLev.ptr->getPoint().temp;
		if (eLev.ptr->imp == NULL)
			newEf = RelEfFromOcc(newocc, newAvail, eLev.ptr->max - eLev.ptr->min, kT);
		else
			newEf = RelEfFromOcc(newocc, newAvail, eLev.ptr->max - eLev.ptr->min, kT, eLev.ptr->imp->getDegeneracy());
	}

	return(newEf - eLev.curEf);
}

int CreateLightSourcesFast(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, bool force)
{
	unsigned int ptSize = data.Pt.size();
	if(!force && (mdesc.simulation->EnabledRates.LightEmissions == false || mdesc.simulation->EnabledRates.RecyclePhotonsAffectRates == false))
		return(0);

	CalcBandStruct(mdl, mdesc);	//don't need to always update the entire band structure all the time!
	double tmpDouble;
	//if it was enabled, then light emissions should have been created.
	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		Point* tmpPt = mdl.getPoint(data.Pt[pt].self);
		if (tmpPt && tmpPt->LightEmission)
			tmpPt->LightEmissions->light.clear();

		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop<start)
			continue; //this point's values do not change

		for (long int el = start; el <= stop; ++el)
		{
			for (unsigned int trg = 0; trg < data.EStates[el].ptr->TargetELev.size(); ++trg)
				CalcFastRate(data.EStates[el].ptr, data.EStates[el].ptr->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, tmpDouble, true);

			data.EStates[el].ptr->OpticalRates.clear();	//new light sources requires new rates to be calculated
		}
	}
	return(0);
}

int CalculateRatesCharge(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	data.varCharge.Clear();
	unsigned int ptSize = data.Pt.size();	//should also be ELevelIndicesInPt size
	data.PosChargeChanges.SetSize(ptSize);	//prevent a bunch of allocations
	data.NegChargeChanges.SetSize(ptSize);
	SS_CurrentDerivFactors Placeholder;	//doesn't actually get used in this bit

	//clear out the external contact rates
	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); ++i)
	{
		mdesc.simulation->contacts[i]->neteleciter = mdesc.simulation->contacts[i]->netholeiter = 0.0;
		mdesc.simulation->contacts[i]->JnOutput.clear();
		mdesc.simulation->contacts[i]->JpOutput.clear();
		mdesc.simulation->contacts[i]->JnOutputEnter.clear();
		mdesc.simulation->contacts[i]->JpOutputEnter.clear();
		mdesc.simulation->contacts[i]->nEnterOutput = mdesc.simulation->contacts[i]->pEnterOutput = 0.0;
		//also need to clear out any rates in the external points that might get tallied!
		//well, not really, but this would be one heck of a memory leak otherwise
		for (unsigned int j = 0; j<mdesc.simulation->contacts[i]->endPtCalcs.size(); ++j)
		{
			Point* curPt = &(mdesc.simulation->contacts[i]->endPtCalcs[j]);
			curPt->Jn.x = curPt->Jn.y = curPt->Jn.z = curPt->Jp.x = curPt->Jp.y = curPt->Jp.z = 0.0;
			curPt->genth = curPt->recth = curPt->scatterR = 0.0;
			curPt->srhR1 = curPt->srhR2 = curPt->srhR3 = curPt->srhR4 = curPt->netCharge = 0.0;
			curPt->LightAbsorbDef = curPt->LightAbsorbGen = curPt->LightAbsorbScat = curPt->LightAbsorbSRH = 0.0;
			curPt->LightEmitDef = curPt->LightEmitRec = curPt->LightEmitScat = curPt->LightEmitSRH = 0.0;
			curPt->LightPowerEntry = curPt->LightEmission = 0.0;	//mW

			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->cb.energies.begin(); eit != curPt->cb.energies.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->vb.energies.begin(); eit != curPt->vb.energies.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->acceptorlike.begin(); eit != curPt->acceptorlike.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->donorlike.begin(); eit != curPt->donorlike.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->CBTails.begin(); eit != curPt->CBTails.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->VBTails.begin(); eit != curPt->VBTails.end(); ++eit)
				ClearELevelRates(eit->second);

		}
	}


	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		Point* tmpPt = mdl.getPoint(data.Pt[pt].self);
		if(tmpPt && tmpPt->LightEmissions)
			tmpPt->LightEmissions->light.clear();

		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop<start)
			continue; //this point's values do not change

		data.Pt[pt].VariableCharge.Clear();
		data.Pt[pt].prevCharge = data.Pt[pt].curcharge;
		data.Pt[pt].curcharge = 0.0;	//clear out the current charge
		for (long int el = start; el <= stop; ++el)
		{
			data.EStates[el].NRates.Clear();
			data.EStates[el].prevEf = data.EStates[el].curEf;
			data.EStates[el].ptr->OpticalRates.clear();	//these get assigned during light. Use of RateSums should make this clear irrelevant, but better safe than sorry
			data.EStates[el].ptr->Rates.clear();	//these get assigned during contacts. Use of RateSums should make this clear irrelevant, but better safe than sorry
			data.EStates[el].ptr->RateSums.Clear();	//this gets updated for every single rate added. Therefore, just check this one value
			double prevOcc = data.EStates[el].occupancy;
			bool prevMatch = data.EStates[el].OccMatches;
			if (data.ReduceTolerance) //reset the rates associated with the fermi level as honing in on the answer
			{
				data.EStates[el].LRinEf = -100.0;
				data.EStates[el].LRoutEf = 100.0;
			}

			UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);

			//calculate rates (fast)
			double NRateIn, derivS, derivT;	//this is a tmp output for CalcFastRate.
			for (unsigned int trg = 0; trg < data.EStates[el].ptr->TargetELev.size(); ++trg)
			{
				if (CalcFastRate(data.EStates[el].ptr, data.EStates[el].ptr->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
					data.EStates[el].NRates.AddValue(NRateIn);
				if(CalcFastDeriv(data.EStates[el].ptr, data.EStates[el].ptr->TargetELev[trg], Placeholder, derivS, derivT, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN) != RATE_UNDEFINED)
					data.EStates[el].DerivSOnly.AddValue(derivS);
			}

			for (unsigned int trg = 0; trg < data.EStates[el].ptr->TargetELevDD.size(); ++trg)
			{
				if (CalcFastRate(data.EStates[el].ptr, data.EStates[el].ptr->TargetELevDD[trg].target, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
					data.EStates[el].NRates.AddValue(NRateIn);
				if(CalcFastDeriv(data.EStates[el].ptr, data.EStates[el].ptr->TargetELevDD[trg].target, Placeholder, derivS, derivT, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN) != RATE_UNDEFINED)
					data.EStates[el].DerivSOnly.AddValue(derivS);
			}


		}

		data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();

		data.varCharge.AddValue(data.Pt[pt].curcharge);
	}

	//these are going to put the rates in the actual e level. There is no "fast" version for these
	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); i++)
		ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);

	Light(mdl, mdesc);	//this must unfortunately be the last thing done due to processing of light emissions. Can't get around looping through everything 3x
	//////////////////////////////
	
	return(0);
}

int SSCorrectCharge(SteadyState_SAAPD& data, Model& mdl)
{
	unsigned int ptSize = data.Pt.size();
//	double totalChargeAdjust = data.targetCharge - data.varCharge.GetNetSum();
	//assuming target is zero, and there's +5 charge, we'll want to remove 5 charge.

/*
	//fix adjustments to hit the target charge
	if (totalChargeAdjust < 0.0)
	{
		if (data.PosChargeChanges.GetTotalProb() < -totalChargeAdjust)
		{
			//this is a shitty situation. It started not close to the desired charge value
			//and not enough charge adjusted to move it into proper charge
			//Ex: There's 20 extra holes in the device, but no state gained positive charge. How
			//do you distribute getting rid of the 20 holes.

			//most practical way is to weight everything by most change in carriers for minimal adjustment in Ef.
			AdjustCharge(data, totalChargeAdjust);
			totalChargeAdjust = 0.0;	//prevent it from doing more modifications beyond this one
		}
	}
	else if (totalChargeAdjust > 0.0)
	{
		if (data.NegChargeChanges.GetTotalProb() < totalChargeAdjust)
		{
			//also a shitty situation. See above.
			AdjustCharge(data, totalChargeAdjust);
			totalChargeAdjust = 0.0;	//prevent it from doing more modifications beyond this one
		}
	}
	*/
	
	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop<start)
			continue; //this point's values do not change


/*
		//fix the charge on the point
		if (totalChargeAdjust > 0.0) //more code, but faster by not constantly checking this if statement
		{
			data.Pt[pt].VariableCharge.Clear();
			double kT = data.EStates[start].ptr->getPoint().temp * KB;
			for (long int el = start; el <= stop; ++el)
			{
				//fix the excess charge for each point by reducing it proportionally
				double newocc, newavail, ChargeAdjust;
				//it wants to add positive charge. That means it added too much negative charge
				//need to get probability on negative charge adjustment
				ChargeAdjust = data.NegChargeChanges.ExtractProb(el) * totalChargeAdjust;	//the one line that's different

				//DEBUG
				ChargeAdjust = 0.0;

				FindStateOccAdjustCharge(data.EStates[el], ChargeAdjust, newocc, newavail);
				if (data.EStates[el].ptr->imp == NULL)
					data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT);
				else
					data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT, data.EStates[el].ptr->imp->degeneracy);

				//store new value in EndOcc and BeginOcc.
				if (newocc <= newavail)
				{
					data.EStates[el].ptr->SetBeginOccStates(newocc);
					data.EStates[el].ptr->SetEndOccStates(newocc);
					data.EStates[el].occupancy = newocc;
					data.EStates[el].OccMatches = true;
				}
				else
				{
					data.EStates[el].ptr->SetBeginOccStatesOpposite(newavail);
					data.EStates[el].ptr->SetEndOccStatesOpposite(newavail);
					data.EStates[el].occupancy = newavail;
					data.EStates[el].OccMatches = false;
				}

				UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);
			}
		}
		else if (totalChargeAdjust < 0.0)
		{
			data.Pt[pt].VariableCharge.Clear();
			double kT = data.EStates[start].ptr->getPoint().temp * KB;

			for (long int el = start; el <= stop; ++el)
			{
				//fix the excess charge for each point by reducing it proportionally
				double newocc, newavail, ChargeAdjust;
				//it wants to add negative charge, so remove some of the positive charge added
				ChargeAdjust = data.PosChargeChanges.ExtractProb(el) * totalChargeAdjust;	//the one line that's different

				//DEBUG
				ChargeAdjust = 0.0;

				FindStateOccAdjustCharge(data.EStates[el], ChargeAdjust, newocc, newavail);
				if (data.EStates[el].ptr->imp == NULL)
					data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT);
				else
					data.EStates[el].curEf = RelEfFromOcc(newocc, newavail, data.EStates[el].ptr->max - data.EStates[el].ptr->min, kT, data.EStates[el].ptr->imp->degeneracy);

				//store new value in EndOcc and BeginOcc.
				if (newocc <= newavail)
				{
					data.EStates[el].ptr->SetBeginOccStates(newocc);
					data.EStates[el].ptr->SetEndOccStates(newocc);
					data.EStates[el].occupancy = newocc;
					data.EStates[el].OccMatches = true;
				}
				else
				{
					data.EStates[el].ptr->SetBeginOccStatesOpposite(newavail);
					data.EStates[el].ptr->SetEndOccStatesOpposite(newavail);
					data.EStates[el].occupancy = newavail;
					data.EStates[el].OccMatches = false;
				}

				UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);
			}
		}
		else
		{
		*/
			for (long int el = start; el <= stop; ++el)
			{
				if(data.EStates[el].OccMatches)
				{
					data.EStates[el].ptr->SetBeginOccStates(data.EStates[el].occupancy);
					data.EStates[el].ptr->SetEndOccStates(data.EStates[el].occupancy);
				}
				else
				{
					data.EStates[el].ptr->SetBeginOccStatesOpposite(data.EStates[el].occupancy);
					data.EStates[el].ptr->SetEndOccStatesOpposite(data.EStates[el].occupancy);
				}
			}
//		}
		
		data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();
		//update point charge
		mdl.poissonRho[data.Pt[pt].mdlRho] = data.Pt[pt].curcharge * data.Pt[pt].QVoli;
		//update mdl.poissonRho.
		data.EStates[start].ptr->getPoint().ResetData();
		//pt.ResetData() - set's rho, all the various output variables, and all the stuff used to calculate the rates
	} //end loop through all points
	return(0);
}

int SSInitData(SteadyState_SAAPD& data, Model& mdl)
{
	MaxMin<long int> tmpIndices;
	tmpIndices.min = tmpIndices.max = -1;
	ELevelLocal tmpInit;
	PtLocal tmpPtInit;
	long int ctr = 0;
	int ctrPt = 0;
	tmpInit.LRinEf = -100.0;
	tmpInit.LRoutEf = 100.0;
	tmpInit.prevEf = tmpInit.curEf = tmpInit.ChangeCharge = 0.0;
	tmpInit.NRates.Clear();
	tmpInit.BadCount = 0;

	data.Pt.reserve(mdl.gridp.size());	//make sure enough memory is allocated!
	double occ, avail;
	double tmpCharge;
	double kT;

	data.varCharge.Clear();
	data.fixedCharge = 0.0;
	PosNegSum tmpFixed;
	tmpFixed.Clear();

	//initialize the local copies of all the relevant data
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		kT = KB * curPt->temp;
		tmpIndices.min = ctr;
		tmpPtInit.VariableCharge.Clear();
		tmpPtInit.fixedCharge = tmpPtInit.maxChangeBStruct = tmpPtInit.prevCharge = tmpPtInit.curcharge = 0.0;
		tmpInit.dataPt = ctrPt;
		tmpPtInit.maxChangeBStruct = curPt->largestPsiCf;

		tmpIndices.min = ctr;
		tmpPtInit.mdlRho = curPt->adj.self;
		tmpPtInit.self = ptIt->first;	//this should likely match with mdl.rho, but just in case that changes in the future
		tmpPtInit.QVoli = QCHARGE * curPt->voli;
		tmpCharge = 0.0;

		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->cb.energies.begin(); eLevIt != curPt->cb.energies.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpCharge -= occ;
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT);
			tmpInit.LastGoodEf = tmpInit.curEf;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
				
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->vb.energies.begin(); eLevIt != curPt->vb.energies.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpCharge += occ;
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT);
			tmpInit.LastGoodEf = tmpInit.curEf;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->acceptorlike.begin(); eLevIt != curPt->acceptorlike.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT, tmpInit.ptr->imp->getDegeneracy());
			tmpInit.LastGoodEf = tmpInit.curEf;
			tmpCharge += occ;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			if (eLevIt->second.imp)
			{
				double charge = double(eLevIt->second.imp->getCharge()) * tmpInit.NStates;	//no Q or volume multiply
				tmpPtInit.fixedCharge += charge;
				tmpFixed.AddValue(charge);
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->donorlike.begin(); eLevIt != curPt->donorlike.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT, tmpInit.ptr->imp->getDegeneracy());
			tmpInit.LastGoodEf = tmpInit.curEf;
			tmpCharge -= occ;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			if (eLevIt->second.imp)
			{
				double charge = double(eLevIt->second.imp->getCharge()) * tmpInit.NStates;	//no Q or volume multiply
				tmpPtInit.fixedCharge += charge;
				tmpFixed.AddValue(charge);
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->CBTails.begin(); eLevIt != curPt->CBTails.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT);
			tmpInit.LastGoodEf = tmpInit.curEf;
			tmpCharge -= occ;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eLevIt = curPt->VBTails.begin(); eLevIt != curPt->VBTails.end(); ++eLevIt)
		{
			tmpInit.ptr = &(eLevIt->second);
			tmpInit.NStates = eLevIt->second.GetNumstates();
			tmpInit.carType = eLevIt->second.carriertype;
			eLevIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			tmpInit.curEf = RelEfFromOcc(occ, avail, eLevIt->second.max - eLevIt->second.min, kT);
			tmpInit.LastGoodEf = tmpInit.curEf;
			tmpCharge += occ;
			if (occ <= avail)
			{
				tmpInit.occupancy = occ;
				tmpInit.OccMatches = true;
			}
			else
			{
				tmpInit.occupancy = avail;
				tmpInit.OccMatches = false;
			}
			data.EStates.push_back(tmpInit);
			eLevIt->second.ssLink = ctr;
			ctr++;
		}
		tmpIndices.max = ctr - 1;
		tmpPtInit.curcharge = tmpPtInit.prevCharge = tmpCharge;
		if (tmpIndices.max >= tmpIndices.min)	//if less, there were no energy levels in this point
		{
			data.ELevelIndicesInPt.push_back(tmpIndices);	//store the indices for the point
			data.Pt.push_back(tmpPtInit);
			ctrPt++;
		}
		data.varCharge.AddValue(tmpCharge);

	}

	//now that all the EStates are added in the vector, it won't change memory locations
	data.fixedCharge = tmpFixed.GetNetSumClear();
	data.totCharge = data.fixedCharge + data.varCharge.GetNetSum();
	data.targetCharge = 0.0;
//	AdjustCharge(data, mdl);

	//now let's fill in the neighbor equivs in this data structure
	for (unsigned int pt = 0; pt < data.Pt.size(); ++pt)
	{

		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop<start)
			continue; //this point's values do not change

		Point* srcPt = mdl.getPoint(data.Pt[pt].self);
		
		for (long int st = start; st <= stop; ++st) //loop through all the states in the point
		{
			ELevel* src = data.EStates[st].ptr;
			data.EStates[st].NeighborEquivs.clear();
			data.EStates[st].NeighborEquivs.reserve(src->NeighborEquivs.size());
			for (unsigned int nbr = 0; nbr < src->NeighborEquivs.size(); ++nbr)
			{
				Point* trgPt = &src->NeighborEquivs[nbr]->getPoint();
				unsigned int trgPtIndex = FindPtIndex(data, mdl, trgPt, pt);
				if (trgPtIndex >= data.ELevelIndicesInPt.size())
					continue;	//can not find it, move on to next one

				long int nbrstart = data.ELevelIndicesInPt[trgPtIndex].min;
				long int nbrstop = data.ELevelIndicesInPt[trgPtIndex].max;
				if (nbrstart < 0 || nbrstop < 0 || nbrstop < nbrstart)
					continue; //this point's values do not change
					//loop through all
				//let's try to speed this up a bit...
				long int TryIndex = nbrstart + st - start;
				if (TryIndex <= nbrstop)
				{
					if (data.EStates[TryIndex].ptr == src->NeighborEquivs[nbr])
					{
						data.EStates[st].NeighborEquivs.push_back(TryIndex);
						continue;	//we found it by following patterns - don't do the loop below!
					}
				}
				for (long int i=nbrstart; i<=nbrstop; ++i)
				{
					if (data.EStates[i].ptr == src->NeighborEquivs[nbr])
					{
						data.EStates[st].NeighborEquivs.push_back(i);
						break;
					}
				}
			}
		}
	}

	return(0);
}

unsigned int FindPtIndex(SteadyState_SAAPD& data, Model& mdl, Point* pt, unsigned int hint)
{
	if (!pt)
		return(-1);

	if (hint < data.Pt.size())
	{
		if (mdl.getPoint(data.Pt[hint].self) == pt)
			return(hint);

		bool badlow = false;
		bool badhigh = false;
		for (unsigned int dif = 1; !badhigh || !badlow; ++dif)	//start at the hint and branch out from there!
		{
			if (!badhigh)
			{
				if (hint + dif < data.Pt.size())
				{
					if (mdl.getPoint(data.Pt[hint + dif].self) == pt)
						return(hint + dif);
				}
				else
					badhigh = true;
			}
			if (!badlow)
			{
				if (unsigned int(hint - dif) < data.Pt.size())
				{
					if (mdl.getPoint(data.Pt[hint - dif].self) == pt)
						return(hint - dif);
				}
				else
					badlow = true;
			}
		}
		return(-1);
	}


	unsigned int find;
	for (find = 0; find < data.Pt.size(); ++find)
	{
		if (mdl.getPoint(data.Pt[find].self) == pt)
			return(find);
	}
	return(-1);
}

//this function simply gets the system to the desired charge by minimally changing the fermi level
int AdjustCharge(SteadyState_SAAPD& data, Model& mdl)
{
	VectorizedPFunc ChargeProbabilities;
	unsigned long int sz = data.EStates.size();
	unsigned int ptSize = data.Pt.size();
	ChargeProbabilities.SetSize(ptSize);

	//build up the probabilities
	
	//adjust each state's fermi level by 0.001 eV, and the amount of change 
	//or just look at the derivative of the occupancy wrt to an increase in Ef
	data.SetCtcSinkPts(mdl);
	double maxChange = 0.0;
	double minChange = 1.0e100;	//something that must be way too large
	double fermiMax = SS_ADJUST_MAX;
	double impGainCharge = 0.0;
	double impLoseCharge = 0.0;
	double chrg;
	
	data.varCharge.Clear();
	for (unsigned int pt = 0; pt < ptSize; ++pt)
	{
		if(data.Pt[pt].ctcSink)	//skip contact sinks
		{
			data.varCharge.AddValue(data.Pt[pt].curcharge);
			chrg = data.Pt[pt].curcharge * data.Pt[pt].QVoli;
			ChargeProbabilities.SetProbability(chrg, pt);
			continue;
		}


		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change

		data.Pt[pt].VariableCharge.Clear();
		for (long int el = start; el <= stop; ++el)
		{
			UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);
			if(data.EStates[el].ptr->imp)
			{
				//this is an impurity
				if(data.EStates[el].carType == ELECTRON)
				{
					//its donor like
					if(data.EStates[el].OccMatches)
					{
						//how much more negative this state can get (number of holes)
						impLoseCharge += data.EStates[el].NStates - data.EStates[el].occupancy;
						//how much more positive it can get
						impGainCharge += data.EStates[el].occupancy;
					}
					else
					{
						//how much more positive this can get (number of electrons)
						impGainCharge += data.EStates[el].NStates - data.EStates[el].occupancy;
						//how much more negative this can get (number of holes)
						impLoseCharge += data.EStates[el].occupancy;
					}
				}
				else
				{
					//its acceptor like
					if(data.EStates[el].OccMatches)
					{
						//how much more negative this state can get (number of holes)
						impLoseCharge += data.EStates[el].occupancy;
						//how much more positive it can get (number of electrons)
						impGainCharge += data.EStates[el].NStates - data.EStates[el].occupancy;
					}
					else
					{
						//how much more positive this can get (number of electrons)
						impGainCharge += data.EStates[el].occupancy;
						//how much more negative this can get (number of holes)
						impLoseCharge += data.EStates[el].NStates - data.EStates[el].occupancy;
					}
				}
			}
		}

		data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();
		data.varCharge.AddValue(data.Pt[pt].curcharge);
		chrg = data.Pt[pt].curcharge * data.Pt[pt].QVoli;

		ChargeProbabilities.SetProbability(chrg, pt);
	}
	
	//now figure out the max change in rho and min change in rho
	
	
	double curDeviceCharge = data.varCharge.GetNetSum();
	//end initialization

	double targetChange = data.targetCharge - curDeviceCharge;

	bool AddPosCharge = (targetChange > 0.0) ? true : false;
	bool ModifyImpOnly = false;
/*	if(AddPosCharge)
	{
		//we want to add charge, let's see if we can do this with just the impurity
		ModifyImpOnly=(impGainCharge > targetChange) ? true : false;
	}
	else
	{
		//want to remove charge. See if we can do this with just the impurity
		ModifyImpOnly = (impLoseCharge > -targetChange) ? true : false;
	}
	*/
	do
	{
		double prevDeviceCharge = curDeviceCharge;

//		for(unsigned int pt=1; pt < ptSize-1; ++pt)
		for(unsigned int pt=0; pt < ptSize; ++pt)
		{
			double rho = fabs(ChargeProbabilities.ExtractRaw(pt));
			if(rho > maxChange)
				maxChange = rho;
			if(rho < minChange)
				minChange = rho;
			/*
			double rhoM1 = ChargeProbabilities.ExtractRaw(pt-1);
			double rhoP1 = ChargeProbabilities.ExtractRaw(pt+1);

			//first, ignore peaks in rho, these shouldn't change much - we'll allow change if it approaches
			//zero and that't the correct thing
			if(rhoM1 <= rho && rho <= rhoP1)
			{
				double dif = rhoP1 - rhoM1;
				if(dif > maxChange)
					maxChange = dif;
				if(dif < minChange && dif != 0.0)
					minChange = dif;
			}
			else if(rhoM1 >= rho && rho >= rhoP1)
			{
				double dif = rhoM1 - rhoP1;
				if(dif > maxChange)
					maxChange = dif;
				if(dif < minChange && dif != 0.0)
					minChange = dif;
			}
			else
			{
				double M1 = fabs(rho - rhoM1);
				double P1 = fabs(rho - rhoP1);
				double dif = M1<P1 ? M1 : P1;	//choose the smaller as the range
				
				if(dif > maxChange)
					maxChange = dif;
				if(dif < minChange && dif != 0.0)
					minChange = dif;
			}
			*/
		}
		
		double deni = maxChange - minChange;
		if(deni != 0.0)
			deni = 1.0/deni;

		double tmpMaxChange =0.0;
		minChange = 1.0e100;	//this isn't used anymore (stored in deni), so it can be reset

		data.varCharge.Clear();
		//now start applying these charges to adjust the entire model by this amount of charge
		for (unsigned int pt = 0; pt < ptSize; ++pt)
		{
			if(data.Pt[pt].ctcSink)	//skip contact sinks
			{
				data.varCharge.AddValue(data.Pt[pt].curcharge);
				continue;
			}

			long int start = data.ELevelIndicesInPt[pt].min;
			long int stop = data.ELevelIndicesInPt[pt].max;
			if (start < 0 || stop < 0 || stop < start)
				continue; //this point's values do not change

			double range = fabs(ChargeProbabilities.ExtractRaw(pt));
			/*
			if(pt == 0)
			{
				range = ChargeProbabilities.ExtractRaw(1) - ChargeProbabilities.ExtractRaw(0);
				if(range == 0.0)	//don't change this point
				{
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
				}
				else if(range < 0.0 && AddPosCharge)	//then rho can only move down to maintain
				{ //the shape of the plot
					//but we want it to go up. Ignore this point
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
				}
				else if(range > 0.0 && !AddPosCharge)
				{
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
					//same logic
				}
				else if(range < minChange)
				{
					//it can move in the correct direction... but we want it to only move a tiny bit
					range = minChange;
				}
				else if(range > maxChange)
				{
					range = maxChange;
				}
			}
			else if(pt == ptSize-1)
			{
				range = ChargeProbabilities.ExtractRaw(ptSize-2) - ChargeProbabilities.ExtractRaw(ptSize-1);
				if(range == 0.0)	//don't change this point
				{
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
				}
				else if(range < 0.0 && AddPosCharge)	//then rho can only move down to maintain
				{ //the shape of the plot
					//but we want it to go up. Ignore this point
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
				}
				else if(range > 0.0 && !AddPosCharge)
				{
					data.varCharge.AddValue(data.Pt[pt].curcharge);
					continue;
					//same logic
				}
				else if(range < minChange)
				{
					//it can move in the correct direction... but we want it to only move a tiny bit
					range = minChange;
				}
				else if(range > maxChange)
				{
					range = maxChange;
				}
			}
			else
			{
				double rhoM1 = ChargeProbabilities.ExtractRaw(pt-1);
				double rho = ChargeProbabilities.ExtractRaw(pt);
				double rhoP1 = ChargeProbabilities.ExtractRaw(pt+1);
				if(rhoM1 <= rho && rho <= rhoP1)
				{
					range = rhoP1 - rhoM1;
					if(range > tmpMaxChange)
						tmpMaxChange = range;
					if(range < minChange && range != 0.0)
						minChange = range;
				}
				else if(rhoM1 >= rho && rho >= rhoP1)
				{
					range = rhoM1 - rhoP1;
					if(range > tmpMaxChange)
						tmpMaxChange = range;
					if(range < minChange && range != 0.0)
						minChange = range;
				}
				else //rho is a peak
				{
					double M1 = fabs(rho - rhoM1);
					double P1 = fabs(rho - rhoP1);
					double dif = M1<P1 ? M1 : P1;	//choose the smaller as the range
					if(dif > tmpMaxChange)
						tmpMaxChange = dif;
					if(dif < minChange && dif != 0.0)
						minChange = dif;

					if(rho > rhoM1 && AddPosCharge)	//this is defining the peak. Don't let this rho value change
					{
						//max rho, don't want this point to grow, keep it fixed
						data.varCharge.AddValue(data.Pt[pt].curcharge);
						continue;
					}
					else if(rho < rhoM1 && !AddPosCharge)
					{
						data.varCharge.AddValue(data.Pt[pt].curcharge);
						continue;
					}
					range = M1<P1 ? M1 : P1;	//choose the smaller as the range
				}
				

			}
			*/
			//make sure the range stays within the limits
			if(range > maxChange)
				range = maxChange;
//			if(range < minChange)
//				range = minChange;

			double kT = data.EStates[start].ptr->getPoint().temp * KB;
			double incFermi = maxChange - range;
			incFermi = incFermi * deni;
			incFermi = fermiMax * pow(SS_ADJUST_MAXMIN, incFermi);	//incFermi ranges from 0 to 1
			double decFermi = 1.0 - incFermi;	//should be like 0.999999001023489
			incFermi += 1.0;		//should be like 1.00000001232190

			double kTi = KBI / mdl.getPoint(data.Pt[pt].self)->temp;

			double adjustFermi = fermiMax * (range / maxChange);

			data.Pt[pt].VariableCharge.Clear();
			for (long int el = start; el <= stop; ++el)
			{
				if(ModifyImpOnly)
				{
					if(data.EStates[el].ptr->imp == NULL)
					{
						UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);
						continue;
					}
				}
				if((AddPosCharge && data.EStates[el].carType == HOLE) ||
					(!AddPosCharge && data.EStates[el].carType == ELECTRON))
				{     //want the fermi level to move more positive
					if(data.EStates[el].curEf > 0.0)
						data.EStates[el].curEf = data.EStates[el].curEf + adjustFermi;// * incFermi;
					else
						data.EStates[el].curEf = data.EStates[el].curEf + adjustFermi;//* decFermi;
				}
				else
				{    //want the fermi level to move more negative
					if(data.EStates[el].curEf > 0.0)
						data.EStates[el].curEf = data.EStates[el].curEf - adjustFermi;//* decFermi;
					else
						data.EStates[el].curEf = data.EStates[el].curEf - adjustFermi;// incFermi;
				}
				double occavail;

				if(data.EStates[el].ptr->imp)
					occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
					data.EStates[el].ptr->max - data.EStates[el].ptr->min,
					data.EStates[el].ptr->imp->getDegeneracy());
				else
					occavail = OccAvailFromRelEf(data.EStates[el].curEf, data.EStates[el].NStates, kTi, 
					data.EStates[el].ptr->max - data.EStates[el].ptr->min);

				if (data.EStates[el].curEf >= 0.0) //it is mostly full of carriers, so occupancy holds the smaller value: avail
				{
					data.EStates[el].occupancy = occavail;
					data.EStates[el].OccMatches = false;
				}
				else //typically true - curEf < 0, so the occupancy is smaller and that's what gets stored in occupancy
				{
					data.EStates[el].occupancy = occavail;
					data.EStates[el].OccMatches = true;
				}

				UpdatePtVarCharge(data.EStates[el], data.Pt[pt]);
			}
			data.Pt[pt].curcharge = data.Pt[pt].fixedCharge + data.Pt[pt].VariableCharge.GetNetSum();
			data.varCharge.AddValue(data.Pt[pt].curcharge);

			chrg = fabs(data.Pt[pt].curcharge) * data.Pt[pt].QVoli;
			if(tmpMaxChange < fabs(chrg))
				tmpMaxChange = fabs(chrg);
			ChargeProbabilities.SetProbability(chrg, pt);
		}

		maxChange = tmpMaxChange;
		curDeviceCharge = data.varCharge.GetNetSum();

		double difCharge = curDeviceCharge - prevDeviceCharge;
		double stillToGo = data.targetCharge - curDeviceCharge;
		double adjustFermiMax = fabs(stillToGo / difCharge);
		if(adjustFermiMax > 1.5)
			adjustFermiMax = 1.5;
		if(adjustFermiMax < 0.01)
			adjustFermiMax = 0.01;

		fermiMax = fermiMax * adjustFermiMax;
		if(fermiMax > 0.01)
			fermiMax = 0.01;

	}while((curDeviceCharge < data.targetCharge && AddPosCharge) ||
		(curDeviceCharge > data.targetCharge && !AddPosCharge));
	ChargeProbabilities.Clear();

	return(0);
}

int GenerateOutput(Model& mdl, ModelDescribe& mdesc, SteadyState_SAAPD& data, Calculations& InitCalc, kernelThread* kThread)
{
	Simulation* sim = mdesc.simulation;

	if (kThread)
	{
		kThread->shareData.KernelcurInfoMsg = MSG_GENOUTPUT;
		kThread->shareData.KernelshortProgBarPosition = 0;
		kThread->shareData.KernelshortProgBarRange = mdl.numpoints;
		kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
	}

	Calculations oldCalc = sim->EnabledRates;
	sim->EnabledRates = InitCalc;
	//clear out optical data
	if(sim->OpticalFluxOut)
		sim->OpticalFluxOut->ClearAll();
	if(sim->OpticalAggregateOut)
		sim->OpticalAggregateOut->ClearAll();

	for (unsigned int i = 0; i < mdesc.materials.size(); i++)
	{
		if(mdesc.materials[i]->OpticalFluxOut)
			mdesc.materials[i]->OpticalFluxOut->ClearAll();
		if(mdesc.materials[i]->OpticalAggregateOut)
			mdesc.materials[i]->OpticalAggregateOut->ClearAll();
	}

	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); ++i)
	{
		mdesc.simulation->contacts[i]->neteleciter = mdesc.simulation->contacts[i]->netholeiter = 0.0;
		sim->contacts[i]->JnOutput.clear();
		sim->contacts[i]->JpOutput.clear();
		sim->contacts[i]->JnOutputEnter.clear();
		sim->contacts[i]->JpOutputEnter.clear();
		sim->contacts[i]->nEnterOutput = sim->contacts[i]->pEnterOutput = 0.0;
		//also need to clear out any rates in the external points that might get tallied!
		//well, not really, but this would be one heck of a memory leak otherwise
		for(unsigned int j=0; j<sim->contacts[i]->endPtCalcs.size(); ++j)
		{
			Point* curPt = &(sim->contacts[i]->endPtCalcs[j]);
			curPt->Jn.x = curPt->Jn.y = curPt->Jn.z = curPt->Jp.x = curPt->Jp.y = curPt->Jp.z = 0.0;
			curPt->genth = curPt->recth = curPt->scatterR = 0.0;
			curPt->srhR1 = curPt->srhR2 = curPt->srhR3 = curPt->srhR4 = curPt->netCharge = 0.0;
			curPt->LightAbsorbDef = curPt->LightAbsorbGen = curPt->LightAbsorbScat = curPt->LightAbsorbSRH = 0.0;
			curPt->LightEmitDef = curPt->LightEmitRec = curPt->LightEmitScat = curPt->LightEmitSRH = 0.0;
			curPt->LightPowerEntry = curPt->LightEmission = 0.0;	//mW

			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->cb.energies.begin(); eit != curPt->cb.energies.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->vb.energies.begin(); eit != curPt->vb.energies.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->acceptorlike.begin(); eit != curPt->acceptorlike.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->donorlike.begin(); eit != curPt->donorlike.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->CBTails.begin(); eit != curPt->CBTails.end(); ++eit)
				ClearELevelRates(eit->second);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->VBTails.begin(); eit != curPt->VBTails.end(); ++eit)
				ClearELevelRates(eit->second);

		}
	}

	InitStructure(mdl, mdesc, data);	//make sure everything stored is copied over, data points are reset, and band structure is calculated
	//constants for calculating the rates

	//calculate all the rates
	unsigned int ptCtr = 0;
	time_t tm = time(NULL);
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		curPt->Jn.x = curPt->Jn.y = curPt->Jn.z = curPt->Jp.x = curPt->Jp.y = curPt->Jp.z = 0.0;
		curPt->genth = curPt->recth = curPt->scatterR = 0.0;
		curPt->srhR1 = curPt->srhR2 = curPt->srhR3 = curPt->srhR4 = curPt->netCharge = 0.0;
		curPt->LightAbsorbDef = curPt->LightAbsorbGen = curPt->LightAbsorbScat = curPt->LightAbsorbSRH = 0.0;
		curPt->LightEmitDef = curPt->LightEmitRec = curPt->LightEmitScat = curPt->LightEmitSRH = 0.0;
		curPt->LightPowerEntry = curPt->LightEmission = 0.0;	//mW
		curPt->CBNRin = curPt->VBNRin = curPt->DonNRin = curPt->AcpNRin = 0.0;

		OutputPointOpticalDetailedInit(sim, *curPt);	//Prepare the optical output file - if one exists

		if(curPt->LightEmissions)
			curPt->LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!

		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.cb.energies.begin(); eit != ptIt->second.cb.energies.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.vb.energies.begin(); eit != ptIt->second.vb.energies.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.acceptorlike.begin(); eit != ptIt->second.acceptorlike.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.donorlike.begin(); eit != ptIt->second.donorlike.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.CBTails.begin(); eit != ptIt->second.CBTails.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.VBTails.begin(); eit != ptIt->second.VBTails.end(); ++eit)
			GenerateOutputELevelCalc(curPt, eit->second, sim);

		if (kThread)
		{
			ptCtr++;
			kThread->shareData.KernelshortProgBarPosition = ptCtr;
			kThread->SSKernelInteract(sim, tm);
//			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
		}

	}
	for (unsigned int i = 0; i<sim->contacts.size(); i++)
		ProcessContactSteadyState(mdl, sim->contacts[i], sim);

	Light(mdl, mdesc, NULL, kThread);

	//now output all the rates
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);

		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.cb.energies.begin(); eit != ptIt->second.cb.energies.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.vb.energies.begin(); eit != ptIt->second.vb.energies.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.acceptorlike.begin(); eit != ptIt->second.acceptorlike.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.donorlike.begin(); eit != ptIt->second.donorlike.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.CBTails.begin(); eit != ptIt->second.CBTails.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
		for (std::map<MaxMin<double>, ELevel>::iterator eit = ptIt->second.VBTails.begin(); eit != ptIt->second.VBTails.end(); ++eit)
			GenerateOutputELevelRec(curPt, eit->second, sim);
	}
	//to be consistent, rates from external contact points also need to be recorded. They might not show up
	//in the main data files, but they might influence the contacts
	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); ++i)
	{
		for(unsigned int j=0; j<sim->contacts[i]->endPtCalcs.size(); ++j)
		{
			Point* curPt = &(sim->contacts[i]->endPtCalcs[j]);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->cb.energies.begin(); eit != curPt->cb.energies.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->vb.energies.begin(); eit != curPt->vb.energies.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->acceptorlike.begin(); eit != curPt->acceptorlike.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->donorlike.begin(); eit != curPt->donorlike.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->CBTails.begin(); eit != curPt->CBTails.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
			for (std::map<MaxMin<double>, ELevel>::iterator eit = curPt->VBTails.begin(); eit != curPt->VBTails.end(); ++eit)
				GenerateOutputELevelRec(curPt, eit->second, sim);
		}
	}


	//can't do the current output until all the adjacent points have been done!
	for (std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
	{
		Point* curPt = &(ptIt->second);
		curPt->Output.Allocate(1);	//allocate stuff for the last entry

		curPt->Output.Psi[0] = curPt->Psi;
		curPt->Output.Ec[0] = curPt->cb.bandmin;
		curPt->Output.Ev[0] = curPt->vb.bandmin;
		curPt->Output.fermi[0] = curPt->fermi;
		curPt->Output.fermin[0] = curPt->fermin;
		curPt->Output.fermip[0] = curPt->fermip;
		curPt->Output.CarCB[0] = curPt->cb.GetNumParticles();
		curPt->Output.CarVB[0] = curPt->vb.GetNumParticles();
		curPt->Output.n[0] = curPt->n * curPt->voli;
		curPt->Output.p[0] = curPt->p * curPt->voli;
		curPt->Output.rho[0] = curPt->rho;
		curPt->Output.PointImpCharge[0] = (curPt->impdefcharge - curPt->elecindonor + curPt->holeinacceptor);
		curPt->Output.Jnx[0] = curPt->Jn.x * curPt->areai.x * QCHARGE;
		curPt->Output.Jpx[0] = curPt->Jp.x * curPt->areai.x * QCHARGE;
		curPt->Output.Jny[0] = curPt->Jn.y * curPt->areai.y * QCHARGE;
		curPt->Output.Jpy[0] = curPt->Jp.y * curPt->areai.y * QCHARGE;
		curPt->Output.Jnz[0] = curPt->Jn.z * curPt->areai.z * QCHARGE;
		curPt->Output.Jpz[0] = curPt->Jp.z * curPt->areai.z * QCHARGE;
		curPt->Output.CBNRIn[0] = curPt->CBNRin;
		curPt->Output.VBNRIn[0] = curPt->VBNRin;
		curPt->Output.DonNRin[0] = curPt->DonNRin;
		curPt->Output.AcpNRin[0] = curPt->AcpNRin;
		curPt->Output.genth[0] = curPt->genth * curPt->voli;
		curPt->Output.recth[0] = curPt->recth * curPt->voli;
		curPt->Output.R1[0] = curPt->srhR1 * curPt->voli;
		curPt->Output.R2[0] = curPt->srhR2 * curPt->voli;
		curPt->Output.R3[0] = curPt->srhR3 * curPt->voli;
		curPt->Output.R4[0] = curPt->srhR4 * curPt->voli;
		curPt->Output.scatter[0] = curPt->scatterR * curPt->voli;
		curPt->Output.Lgen[0] = curPt->LightAbsorbGen * curPt->voli;
		curPt->Output.Lsrh[0] = curPt->LightAbsorbSRH * curPt->voli;
		curPt->Output.Ldef[0] = curPt->LightAbsorbDef * curPt->voli;
		curPt->Output.Lscat[0] = curPt->LightAbsorbScat * curPt->voli;
		curPt->Output.Erec[0] = curPt->LightEmitRec * curPt->voli;
		curPt->Output.Esrh[0] = curPt->LightEmitSRH * curPt->voli;
		curPt->Output.Edef[0] = curPt->LightEmitDef * curPt->voli;
		curPt->Output.Escat[0] = curPt->LightEmitScat * curPt->voli;
		curPt->Output.LPowerEnter[0] = curPt->LightPowerEntry;
		curPt->Output.LightEmit[0] = curPt->LightEmission;

		OutputRatesPoint(*curPt, sim);
	}
	
	sim->binctr = 1;

	if(sim->OpticalFluxOut)
	{
		sim->OpticalAggregateOut->AddExistingBulkData(sim->OpticalFluxOut, 1.0);
		for (unsigned int i = 0; i<mdesc.materials.size(); i++)
			mdesc.materials[i]->OpticalAggregateOut->AddExistingBulkData(mdesc.materials[i]->OpticalFluxOut, 1.0);
	}

	FillInEnvOutputs(mdl, mdesc, *mdesc.simulation->SteadyStateData->ExternalStates[mdesc.simulation->SteadyStateData->CurrentData]);	//i is the environment who's 

	OutputDataToFileSS(mdl, mdesc);

	sim->EnabledRates = oldCalc;

	return(0);
}

int ClearELevelRates(ELevel& eLev)
{
	eLev.netRate = eLev.netRateM1 = eLev.netRateM2 = eLev.rateIn = eLev.rateOut = eLev.deriv = eLev.deriv2 = eLev.derivsourceonly = 0.0;
	eLev.Rates.clear();
	eLev.OpticalRates.clear();
	eLev.RateSums.Clear();
	eLev.Rates.reserve(eLev.TargetELev.size() + eLev.TargetELevDD.size());	//prevent any re-allocations
	return(0);
}

int GenerateOutputELevelCalc(Point* pt, ELevel& eLev, Simulation* sim)
{
	ClearELevelRates(eLev);

	for (std::vector<TargetE>::iterator it = eLev.TargetELevDD.begin(); it != eLev.TargetELevDD.end(); ++it)
		CalcRate(&eLev, it->target, sim);
	for (std::vector<ELevel*>::iterator it = eLev.TargetELev.begin(); it != eLev.TargetELev.end(); ++it)
		CalcRate(&eLev, *it, sim);

	return(0);
}

int GenerateOutputELevelRec(Point* pt, ELevel& state, Simulation* sim)
{
	for (std::vector<ELevelRate>::iterator rt = state.OpticalRates.begin(); rt != state.OpticalRates.end(); ++rt)
		rt->RecordRate();

	for (std::vector<ELevelRate>::iterator rt = state.Rates.begin(); rt != state.Rates.end(); ++rt)	//now do the normal rates
		rt->RecordRate();
		

	return(0);
}

int UpdatePtVarCharge(ELevelLocal& state, PtLocal& pt, double occ)
{
	if (occ < 0.0)
		occ = state.occupancy;

	//update pt variable charge
	if (state.OccMatches == true && state.carType == ELECTRON)
		pt.VariableCharge.SubtractValue(occ);
	else if (state.OccMatches == true && state.carType == HOLE)
		pt.VariableCharge.AddValue(occ);
	else if (state.OccMatches == false && state.carType == ELECTRON)
	{
		pt.VariableCharge.SubtractValue(state.NStates);
		pt.VariableCharge.AddValue(occ);
	}
	else// if (data.EStates[el].OccMatches == false && data.EStates[el].carType == HOLE)
	{
		pt.VariableCharge.AddValue(state.NStates);
		pt.VariableCharge.SubtractValue(occ);
	}
	return(0);
}

int FindStateOccAdjustCharge(ELevelLocal& state, double chargeAdjust, double& newocc, double& newavail)
{
	if (state.OccMatches == true)
	{
		if (state.carType == ELECTRON) //occupancy is in electrons (negative charge)
		{
			newocc = state.occupancy - chargeAdjust;
			newavail = state.NStates - newocc;
		}
		else //occupancy is holes. (positive charge)
		{
			newocc = state.occupancy + chargeAdjust;
			newavail = state.NStates - newocc;
		}
	}
	else //the occupancy is actually the availability
	{
		if (state.carType == ELECTRON) //occupancy is the holes(availability)
		{
			newavail = state.occupancy + chargeAdjust;
			newocc = state.NStates - newavail;
		}
		else
		{
			newavail = state.occupancy - chargeAdjust;
			newocc = state.NStates - newavail;
		}
	}
	if (newocc < 0.0)
	{
		newocc = 0.0;
		newavail = state.NStates;
	}
	if (newavail < 0.0)
	{
		newavail = 0.0;
		newocc = state.NStates;
	}

	return(0);
}

//returns whether it is in tolerance or not
bool UpdateStateFermi(ELevelLocal& EState, double NetRate, double smallAdjust, double largeAdjust, double tolerance, double overAdjusted, double maxEfDelta)
{
	bool inTolerance = false;
	bool ReducedAdjustL = false;
	bool ReducedAdjustS = false;
	double oldEf = EState.curEf;
	if(EState.BadCount < 0)
		EState.BadCount = 0;
	double SzAdjust = double(EState.BadCount) / SS_INCREASE_TOLERANCE_CTR;
/*	if(largeAdjust > fabs(maxEfDelta))
	{
		largeAdjust = fabs(maxEfDelta);
		ReducedAdjustL = true;
	}*/
	if(smallAdjust > fabs(maxEfDelta))
	{
		smallAdjust = fabs(maxEfDelta);
		ReducedAdjustS = true;
	}
	if(SzAdjust > 0.0)
		largeAdjust = largeAdjust * (1.0 + SzAdjust);//exp(SzAdjust);
	

	if (NetRate > 0.0) //carriers are trying to enter. Increase relative Ef
	{ //convention says more carriers has higher relative Ef - regardless of carrier type
		
		if (overAdjusted >= 1.0)
		{
			EState.LRinEf = EState.prevEf;
			if (EState.LRoutEf - EState.LRinEf < tolerance)
			{
				EState.curEf += smallAdjust;
				inTolerance = true;
				EState.BadCount = 0;
			}
			else
			{
				EState.curEf += largeAdjust;
				if (EState.curEf == oldEf && ReducedAdjustL==false)	//it can't get any more accurate at the current levels. Say it's A OK.
					inTolerance = true;
			}
		}
		else
			EState.curEf = EState.prevEf + overAdjusted;
	}
	else if (NetRate < 0.0)
	{
		if (overAdjusted >= 1.0)
		{
			EState.LRoutEf = EState.prevEf;
			if (EState.LRoutEf - EState.LRinEf < tolerance)
			{
				EState.curEf -= smallAdjust;
				inTolerance = true;
				EState.BadCount = 0;	//it made it, so we're done
			}
			else
			{
				EState.curEf -= largeAdjust;
				if (EState.curEf == oldEf && ReducedAdjustL==false)	//it can't get any more accurate at the current levels. Say it's A OK.
					inTolerance = true;
			}
		}
		else
			EState.curEf = EState.prevEf - overAdjusted;
	}
	else if(overAdjusted >= 1.0)//don't change much anything, but indicate it has hit steady state
	{
		EState.LRinEf = EState.LRoutEf = EState.prevEf;
		inTolerance = true;
	}

	if(EState.LRinEf == -100.0 || EState.LRoutEf == 100.0)
		EState.BadCount++;
	else
		EState.BadCount-=10;	//start reducing tolerance quickly

	return(inTolerance);
}

double UpdateStateOccupancy(ELevelLocal& EState, double kTI, bool preventChangeCharge)
{
	double newocc;
	if (EState.ptr->imp)
		newocc = OccAvailFromRelEf(EState.curEf, EState.NStates, kTI, EState.ptr->max - EState.ptr->min, EState.ptr->imp->getDegeneracy());
	else
		newocc = OccAvailFromRelEf(EState.curEf, EState.NStates, kTI, EState.ptr->max - EState.ptr->min, 1.0);

	if (EState.curEf <= 0.0)	//that means there are fewer carriers, so occ matches expected value
	{
		if (preventChangeCharge == false)
		{

			if (EState.OccMatches == true)	//it matched previously as well
			{
				if (EState.carType == ELECTRON)
					EState.ChangeCharge = EState.occupancy - newocc;
				//if there are more carriers than beforehand, there is more negative charge
				else
					EState.ChangeCharge = newocc - EState.occupancy;
				//if there are more carriers than before, there is more positive charge
			}
			else
			{
				if (EState.carType == ELECTRON)
				{
					//occupancy holds the number of holes in the previous iteration, but newocc holds the number of electrons in the current iteration
					EState.ChangeCharge = (EState.NStates - EState.occupancy) - newocc;
					//if the number of electrons increases, more negative charge.
				}
				else
				{
					//occupancy holds num of elec in previous iter, but newocc holds number of holes
					EState.ChangeCharge = newocc - (EState.NStates - EState.occupancy);
					//a larger number of holes in newocc means more positive charge
				}
			}
		}
		//this is now the case no matter what the previous iteration says because curEf < 0.0
		EState.occupancy = newocc;
		EState.OccMatches = true;
	}
	else// if (EState.curEf > 0.0) //that means there are tons of carriers, so previous occupancy does not match
	{
		if (preventChangeCharge == false)
		{
			if (EState.OccMatches == false)	//the previous one did not match either
			{
				if (EState.carType == ELECTRON) //both occuapncy and new occ are holes
					EState.ChangeCharge = newocc - EState.occupancy;
				//more holes now, so that implies more positive charge added
				else //both occ and newocc are electrons
					EState.ChangeCharge = EState.occupancy - newocc;
				//more electrons now implies more negative charge
			}
			else //the previous occupancy matches the carrier type but the current iteration uses available
			{
				if (EState.carType == ELECTRON) //prev=electron, cur=hole
					EState.ChangeCharge = EState.occupancy - (EState.NStates - newocc);
				//more elec now = more negative charge
				else //prev=hole, cur=electron
					EState.ChangeCharge = (EState.NStates - newocc) - EState.occupancy;
				//more holes now = more positive charge
			}
		}
		EState.occupancy = newocc;
		EState.OccMatches = false;
	}
	return(newocc);
}

double RelEfFromOcc(double occ, double avail, double eWidth, double kt, float degen)
{
	double ret = 0.0;
	
	if (kt == 0.0)	//temperature is zero. eUse is halfway up the state. Completely filled or completely empty
	{
		double percent = occ / (occ+avail);	//Ef is this percentage up from the bottom
		if (eWidth == 0.0)
			eWidth = 0.001;	//allow some variation...
		ret = eWidth * (percent - 0.5);
	}
	else
	{
		double lnVal = double(degen) * occ / avail;
		if (MY_IS_FINITE(lnVal) && lnVal!=0.0)
		{
			lnVal = log(lnVal);
			ret = kt * lnVal;
		}
		else if (lnVal == 0.0)
		{
			//occupancy is zero.
			ret = -100.0;	//back to default values...
		}
		else
			ret = 100.0;
	}
	

	return(ret);
}

double OccAvailFromRelEf(double relEf, double NStates, double ktI, double eWidth, float degen)
{
	double ret = 0.0;
	if (relEf == 0.0)	//easiest one to do! Assume matches occupancy
		ret = NStates / (1.0 + double(degen));
	else if (ktI == 0.0 || MY_IS_FINITE(ktI)==false) //this actually means T=0.0!
	{
		if (eWidth == 0.0)	//zero temp, completely discrete state, what the hell...
		{
			eWidth = 0.001;	//just give it something to work with...
		}

		ret = 0.5 + relEf / eWidth;
		if (ret > 1.0)
			ret = 1.0;
		if (ret < 0.0)
			ret = 0.0;

		ret = ret * NStates;
	}
	else if (relEf < 0.0)	//this means occ is lower than avail, and we should calculate occ
	{
		relEf = relEf * ktI;
		relEf = double(degen) * exp(-relEf) + 1.0;	//note negative relEf means less carriers
		ret = NStates / relEf;
	}
	else //relEf > 0.0, so that means occupancy is large, so we should calculate availability
	{
		relEf = relEf * ktI;
		relEf = exp(relEf)/double(degen) + 1.0;	//note positive relEf means more carriers, so less availability
		ret = NStates / relEf;
	}
	if(MY_IS_FINITE(ret)==false)
		int debug=0;
	return(ret);
}


/*
This assumes a mobility of 10,000, carrier concentration of 10^20, and sets the current
at 1 pA (10^-12) -- simulation accuracy in amps/cm^2.  This means the electric field must be 10^-36.  This assumes the charge is
constant throughout the entire device. It looks at the poisson equation coefficients to see
which two points change the most and the least, respectively.  It then determines the rho needed
to have the electric field to match the accuracy.
*/
double CalcChargeTolerance(Model& mdl, ModelDescribe& mdesc)
{
	double ret = 0.0;
	double EFieldTarget = mdesc.simulation->accuracy * 1.0e-24;

	XSpace posMax;
	XSpace posMin;
	double tmpSum;
	double SumMax = -1.0e200;	//this will be the largest number
	double SumMin = 1.0e200;
	unsigned long int ptSize = mdl.gridp.size();
	//now loop through all the points
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt!= mdl.gridp.end(); ++pt)
	{
		tmpSum = 0.0;
		for(unsigned long int i=0; i<pt->second.pDsize; ++i)
		{
			if(pt->second.poissonDependency[i].pt < ptSize)	//don't include the boundary conditions in this
			{
				tmpSum += pt->second.poissonDependency[i].data;
			}
		}
		if(tmpSum > SumMax)
		{
			SumMax = tmpSum;
			posMax = pt->second.position;
		}
		if(tmpSum < SumMin)
		{
			SumMin = tmpSum;
			posMin = pt->second.position;
		}
	}
	if(SumMin != SumMax)
		ret = EFieldTarget * (posMax-posMin).Magnitude() / (SumMax - SumMin);
	else
		ret = 1.0e-10;	//arbitrarily 10^-10 C cm^-3

	//now let's see how many carriers this actually is
	ret = ret * mdesc.Ldimensions.x * mdesc.Ldimensions.y * mdesc.Ldimensions.z * QI;

	return(ret);
}

/*
Going to use the transient to try and solve the entire thing.  I'm not going to explicitly conserve charge, though.
Choose target change in Ef. Calculate their rates. Set time step to adjust all rates so the one that would make the adjustment quickest is limiting factor.
If a rate changes sign, add it to the 'failed' list. These will have no account on the time step, and will be restored to their previous value as it likely causes serious divergence.
The failed list states will not be changed. Once all states have changed, look at the change in Ef for all of them from thir initial value, and return the last one.
*/

double SSByTransCutoff(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread)
{
	//first make sure everything is transferred to the model and it is ready to go!
	
	//just make it to satisfy calcstaterate. These don't actually modify other stuff now.
	std::vector<Point*> ptVect;
	bool used;
	double maxEfChange = 0.0;

	time_t start;
	start = time(NULL);
	if (kThread)
	{
		kThread->shareData.KernelTimeLeft = 0;
		kThread->shareData.KernelMinTolerance = -1.0;
		kThread->shareData.KernelMaxTolerance = -1.0;
		kThread->shareData.KernelTolerance = data.tolerance;
		kThread->shareData.KernelshortProgBarRange = data.EStates.size();// * MC_FAILMULTIPLIER;
		kThread->shareData.KernelshortProgBarPosition = 0;
		kThread->shareData.KernelcurInfoMsg = MSG_SS_TRANS;

	}

	for (unsigned int pt = 0; pt < data.ELevelIndicesInPt.size(); ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || start > stop || (unsigned long int)stop >= data.EStates.size())
			continue;	//invalid point

		long int mdlIndex = data.Pt[pt].self;
		double NBeta = (mdl.getPoint(mdlIndex)->temp!=0.0)? -KBI/mdl.getPoint(mdlIndex)->temp : 0.0;
		for (long int el = start; el <= stop; ++el)
		{
			data.EStates[el].UpdateModelOccupancy();	//looks at curEf in EStates, updates occupancy/occmatches, and sets the state occupation in mdl
			data.EStates[el].prevEf = data.EStates[el].curEf;
			data.EStates[el].BadCount = 0;
			
		}
		mdl.getPoint(mdlIndex)->ResetData();	//updates rho, but does not update scatter constants
		mdl.poissonRho[mdlIndex] = mdl.getPoint(mdlIndex)->rho;
		CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterCBConst, mdl.getPoint(mdlIndex)->cb.energies, NBeta);
		CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterVBConst, mdl.getPoint(mdlIndex)->vb.energies, NBeta);
	}
	


	

	double desiredTStep, occChange, tmpStep;
	double prevOcc, prevAvail, occ, avail;
	bool anyGood = false;
	bool invalidM1 = true;
	long int BigStateChange = -1;
	unsigned int badCount = 0;
	CreateLightSourcesFast(data, mdl, mdesc);

	do
	{ 

		if (kThread)
		{

			kThread->shareData.KernelshortProgBarPosition = badCount;
			kThread->SSKernelInteract(mdesc.simulation, start);

			if (kThread->shareData.GUIabortSimulation)	//just get out if possible
				break;

			if (kThread->shareData.GUIstopSimulation)
				break;	//get out of the loop and output the data as it stands

			if (kThread->shareData.GUINext)
				break;

			if (kThread->shareData.GUIsaveState)
			{
				GenerateOutput(mdl, mdesc, data, InitCalc);	//it's going to output the file when it reduces the tolerance anyway
				kThread->shareData.GUIsaveState = false;	//reset it so it can be clicked again
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
			}
		}

		desiredTStep = INFINITETSTEP;
		anyGood = false;
		
		//everything is going to need to be adjusted first!
		CalcBandStruct(mdl, mdesc);	//get the band diagram all correct!
		Light(mdl, mdesc);	//more efficient to do everything at once! 
		for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); i++)
			ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);	//these will put values in RateSums!

		//first, calculate current rates to get the desired timestep
		for (unsigned int pt = 0; pt < data.ELevelIndicesInPt.size(); ++pt)
		{
			long int start = data.ELevelIndicesInPt[pt].min;
			long int stop = data.ELevelIndicesInPt[pt].max;
			if (start < 0 || stop < 0 || start > stop || (unsigned long int)stop >= data.EStates.size())
				continue;	//invalid point

			long int mdlIndex = data.Pt[pt].self;
			double NBeta = (mdl.getPoint(mdlIndex)->temp != 0.0) ? -KBI / mdl.getPoint(mdlIndex)->temp : 0.0;

			
			for (long int el = start; el <= stop; ++el)
			{
				//find the desired time step, but only look at good cones

				if (invalidM1)	//the m1 values are invalid and must be recalulated.
				{
					data.EStates[el].ptr->netRateM1 = data.EStates[el].ptr->RateSums.GetNetSumClear();	//get light and contact values in netRateM1
					data.EStates[el].ptr->netRateM1 += CalcStateRate(&data.EStates[el], mdl, mdesc, ptVect, used, NULL, false, false, true);	//2 falses ignore light and contacts. Preinitialized data (true)
					//CalcStateRate will clear out RateSums when called, and Rates/Optical Rates at end of funtion
				}

				data.EStates[el].UpdateModelOccupancy();	//looks at curEf in EStates, updates occupancy/occmatches, and sets the state occupation in mdl (begin and end)
				prevOcc = data.EStates[el].GetOcc();
				prevAvail = data.EStates[el].GetAvail();
				if (data.EStates[el].BadCount == 0)	//only adjust if it hasn't been bad yet.
				{
					if (data.EStates[el].ptr->netRateM1 < 0.0)	//losing carriers
					{
						data.EStates[el].UpdateOccVars(data.EStates[el].curEf - data.tolerance*SS_TRANS_CUTOFF_ADJ, true);
						occ = data.EStates[el].GetOcc();
						avail = data.EStates[el].GetAvail();
						occChange = occ < avail ? fabs(occ - prevOcc) : fabs(avail - prevAvail);	//get the most accurate change values
						tmpStep = -occChange / data.EStates[el].ptr->netRateM1;
						if (desiredTStep == INFINITETSTEP || (tmpStep < desiredTStep && tmpStep > 0.0))
							desiredTStep = tmpStep;
					}
					else if (data.EStates[el].ptr->netRateM1 > 0.0)	//losing carriers
					{
						data.EStates[el].UpdateOccVars(data.EStates[el].curEf + data.tolerance*SS_TRANS_CUTOFF_ADJ, true);
						occ = data.EStates[el].GetOcc();
						avail = data.EStates[el].GetAvail();
						occChange = occ < avail ? fabs(occ - prevOcc) : fabs(avail - prevAvail);	//get the most accurate change values
						tmpStep = occChange / data.EStates[el].ptr->netRateM1;
						if (desiredTStep == INFINITETSTEP || (tmpStep < desiredTStep && tmpStep > 0.0))
							desiredTStep = tmpStep;
					}

					if ((prevOcc == occ && prevAvail == avail))	//the change is so small it is not making any noticable changes 
						data.EStates[el].BadCount = 1;	//so ignore this state from now on. For tmpStep to be zero, crazy stuff must be going on

					//restore to original values
					if (prevOcc < prevAvail)
						data.EStates[el].SetOcc(prevOcc);
					else
						data.EStates[el].SetAvail(prevAvail);
				}
			}

		}
	
		invalidM1 = false;	//the M1 values should be good!
		//next, actually adjust everything according to the time step
		//or make a really small adjustment based on the current tolerance
		if (desiredTStep > 0)	//it will actually make some changes and is not infinite(-1)
		{
			for (unsigned int pt = 0; pt < data.ELevelIndicesInPt.size(); ++pt)
			{
				long int start = data.ELevelIndicesInPt[pt].min;
				long int stop = data.ELevelIndicesInPt[pt].max;
				if (start < 0 || stop < 0 || start > stop || (unsigned long int)stop >= data.EStates.size())
					continue;	//invalid point

				long int mdlIndex = data.Pt[pt].self;
				double NBeta = (mdl.getPoint(mdlIndex)->temp != 0.0) ? -KBI / mdl.getPoint(mdlIndex)->temp : 0.0;
				for (long int el = start; el <= stop; ++el)
				{
					if (data.EStates[el].BadCount == 0)
					{
						data.EStates[el].AddOcc(data.EStates[el].ptr->netRateM1 * desiredTStep);	//updates occupancy/curEf
						data.EStates[el].UpdateModelOccupancy(MC_OCC_BEGIN);	//looks at curEf in EStates, updates occupancy/occmatches, and sets the state occupation in mdl (beginoccstates)
					}
					else //make itsy bitsy tiny adjustments. Always make sure every state is converging to try and avoid it jumping back and forth in an ugly manner
					{
						if (data.EStates[el].ptr->netRateM1 > 0)
						{
							double prevEf = data.EStates[el].curEf;	//save original ef for comparison
							data.EStates[el].AddOcc(data.EStates[el].ptr->netRateM1 * desiredTStep);	//first update by time step
							double eftStep = data.EStates[el].curEf;	//save this ef
							data.EStates[el].UpdateOccVars(prevEf + data.tolerance*SS_TRANS_CUTOFF_ADJ*0.125, true);	//now do big bulk update
							double efAdj = data.EStates[el].curEf;	//save efAdjusted
							
							if (fabs(efAdj - prevEf) > fabs(eftStep - prevEf))	//if the timestep adjustment was smaller, use that one
								data.EStates[el].UpdateOccVars(eftStep, true);

							data.EStates[el].UpdateModelOccupancy(MC_OCC_BEGIN);	//looks at curEf in EStates, updates occupancy/occmatches, and sets the state occupation in mdl (beginoccstates)
						}
						else if (data.EStates[el].ptr->netRateM1 < 0)
						{
							double prevEf = data.EStates[el].curEf;	//save original ef for comparison
							data.EStates[el].AddOcc(data.EStates[el].ptr->netRateM1 * desiredTStep);	//first update by time step
							double eftStep = data.EStates[el].curEf;	//save this ef
							data.EStates[el].UpdateOccVars(prevEf - data.tolerance*SS_TRANS_CUTOFF_ADJ*0.125, true);	//now do big bulk update
							double efAdj = data.EStates[el].curEf;	//save efAdjusted

							if (fabs(efAdj - prevEf) > fabs(eftStep - prevEf))	//if the timestep adjustment was smaller, use that one
								data.EStates[el].UpdateOccVars(eftStep, true);

							data.EStates[el].UpdateModelOccupancy(MC_OCC_BEGIN);	//looks at curEf in EStates, updates occupancy/occmatches, and sets the state occupation in mdl (beginoccstates)
						}
					}
					data.EStates[el].ptr->Rates.clear();	//prep for the next rate calculation, and make sure we don't have runaway data allocation
					data.EStates[el].ptr->OpticalRates.clear();
					data.EStates[el].ptr->RateSums.Clear();
				}
				mdl.getPoint(mdlIndex)->ResetData();	//updates rho, but does not update scatter constants
				mdl.poissonRho[mdlIndex] = mdl.getPoint(mdlIndex)->rho;
				CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterCBConst, mdl.getPoint(mdlIndex)->cb.energies, NBeta);
				CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterVBConst, mdl.getPoint(mdlIndex)->vb.energies, NBeta);

			}
			CalcBandStruct(mdl, mdesc);	//get the band diagram all correct now that everything is adjusted

			Light(mdl, mdesc);	//more efficient to do everything at once! 
			for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); i++)
				ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);	//these will put values in RateSums!

			//now check all the states and make sure the move was good (did the rate change sign).
			for (unsigned int pt = 0; pt < data.ELevelIndicesInPt.size(); ++pt)
			{
				long int start = data.ELevelIndicesInPt[pt].min;
				long int stop = data.ELevelIndicesInPt[pt].max;
				if (start < 0 || stop < 0 || start > stop || (unsigned long int)stop >= data.EStates.size())
					continue;	//invalid point

				long int mdlIndex = data.Pt[pt].self;
				double NBeta = (mdl.getPoint(mdlIndex)->temp != 0.0) ? -KBI / mdl.getPoint(mdlIndex)->temp : 0.0;
				bool badChanges = false;	//
				for (long int el = start; el <= stop; ++el)
				{

					data.EStates[el].ptr->netRate = data.EStates[el].ptr->RateSums.GetNetSumClear();	//get light and contact values in netRateM1
					data.EStates[el].ptr->netRate += CalcStateRate(&data.EStates[el], mdl, mdesc, ptVect, used, NULL, false, false, true);	//2 falses ignore light and contacts. Preinitialized data (true)
					if (data.EStates[el].BadCount == 0)	//only adjust if it hasn't been bad yet.
					{
						if ((data.EStates[el].ptr->netRate < 0 && data.EStates[el].ptr->netRateM1 < 0.0) ||
							(data.EStates[el].ptr->netRate > 0 && data.EStates[el].ptr->netRateM1 > 0.0))
						{
							anyGood = true;
							//it already has the good data in both mdl and data.
							//							data.EStates[el].BadCount = SS_TRANS_HAVENM1;	//-1 -- avoid redoing this calculation every time - redo actually depends on all other states not changing too!
							data.EStates[el].ptr->netRateM1 = data.EStates[el].ptr->netRate;

							if (fabs(data.EStates[el].curEf - data.EStates[el].prevEf) > maxEfChange)
							{
								BigStateChange = el;
								maxEfChange = fabs(data.EStates[el].curEf - data.EStates[el].prevEf);
							}
						}
						else //the rate changed sign! It went to far. Stop processing this state!
						{
							data.EStates[el].BadCount=1;
							data.EStates[el].UpdateOccupancyFromModel(RATE_OCC_END);	//restore old data into data.EStates.
							data.EStates[el].ptr->TransferEndToBegin();	//make sure the mdl begin is updated
							badChanges = true;	//this means rho and the scatter constants are invalid for any other rates calculated.
							invalidM1 = true;
							badCount++;
						}
					}
					else //it was previously a state that has gone too far
					{
						if ((data.EStates[el].ptr->netRate < 0 && data.EStates[el].ptr->netRateM1 < 0.0) ||
							(data.EStates[el].ptr->netRate > 0 && data.EStates[el].ptr->netRateM1 > 0.0))
						{
							//it already has the good data in both mdl and data.
							data.EStates[el].ptr->netRateM1 = data.EStates[el].ptr->netRate;
						}
						else //the rate changed sign! It went to far. Again. Stop processing this state!
						{
							data.EStates[el].UpdateOccupancyFromModel(RATE_OCC_END);	//restore old data into data.EStates.
							data.EStates[el].ptr->TransferEndToBegin();	//make sure the mdl begin is updated
							badChanges = true;	//this means rho and the scatter constants are invalid for any other rates calculated.
							invalidM1 = true;
							//no increment on the badCount because it is already marked as bad
						}
					}
					data.EStates[el].ptr->Rates.clear();	//prep for the next rate calculation, and make sure we don't have runaway data allocation
					data.EStates[el].ptr->OpticalRates.clear();
					data.EStates[el].ptr->RateSums.Clear();
				}
				if (badChanges)
				{
					mdl.getPoint(mdlIndex)->ResetData();	//updates rho, but does not update scatter constants
					mdl.poissonRho[mdlIndex] = mdl.getPoint(mdlIndex)->rho;	//this needs to be done in case 
					CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterCBConst, mdl.getPoint(mdlIndex)->cb.energies, NBeta);
					CalcScatterConsts(mdl.getPoint(mdlIndex)->scatterVBConst, mdl.getPoint(mdlIndex)->vb.energies, NBeta);
				}
			}
		}
	} while (anyGood);

/*
	if (BigStateChange >= 0)
	{
		ProgressInt(76, BigStateChange);
		ProgressDouble(54, data.EStates[BigStateChange].prevEf);
		ProgressDouble(55, data.EStates[BigStateChange].curEf);
	}
*/
	return(maxEfChange);
}

/*
This will look at a single point, and solve it's values by transient.
The transient method is REALLY good if it is far away from equilibrium, which
also is when solving by a newton raphson method is awful. Typically, ways to prevent
divergence involve restricting the change in fermi level. When far away, that has very
little effect. When close to the state, it can cause large changes in charge. The transient
method looks more at carriers, so large changes are made without creating massive
charge imbalances. (think transfer between light and no light conditions.
The carrier change is equal and the charge is balanced, but the fermi levels
differ a lot. Restricting the fermi level to maintain stability actually causes
massive divergence)
*/
double SSTransEqPoint(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, int point, bool ignoreDD)
{
	if(unsigned int(point) >= data.Pt.size())
		return(0.0);	//fail

	if(unsigned int(point) >= data.ELevelIndicesInPt.size())	//this should be straightforward...
		return(0.0);
	//we do not care about updating the band structure.
	std::vector<ELevel*> mdlEStates;	//this will just make it easier to reference
	std::vector<double> VBGainHoles;	//DEBUG
	Point* pt = mdl.getPoint(data.Pt[point].self);
	unsigned int firstELevel = data.ELevelIndicesInPt[point].min;
	unsigned int lastELevel = data.ELevelIndicesInPt[point].max;
	unsigned int numStates = lastELevel - firstELevel + 1;
	mdlEStates.reserve(numStates);
	double TypicalMaxCharge = 0;	//if the charge goes WAAAY beyond this typical value, the point needs to be reset because it is likely wrong!
	double ActualCharge = 0.0;
	for(unsigned int i = firstELevel; i<=lastELevel; ++i)
	{
		mdlEStates.push_back(data.EStates[i].ptr);
		TypicalMaxCharge += fabs(data.EStates[i].NStates * mdlEStates.back()->charge);
		ActualCharge += data.EStates[i].GetCharge(false);
		mdlEStates.back()->netRate = 0.0;	//quick init so netRateM1 is OK in later init
	}
	ActualCharge += data.Pt[point].fixedCharge;
	//just try and get it in the ball park for charge. The transient rates will
	//sort everything out afterwards. This is just to make sure things are at least somewhat in the right ballpark charge wise
	
	if(TypicalMaxCharge > 0.0 && ActualCharge > TypicalMaxCharge * 1.01)
	{
		//correct the situation - there is too much positive charge
		//need to add in negative charge.
		double ElecAdded = ActualCharge - TypicalMaxCharge;
		std::vector<double> probs;
		double tot = 0.0;
		probs.resize(numStates);
		for(unsigned int i = 0, j = firstELevel; i<numStates; ++i, ++j)
		{
			probs[i] = data.EStates[j].GetHole();
			tot += probs[i];
		}
		tot = 1.0 / tot;
		for(unsigned int i = 0, j = firstELevel; i<numStates; ++i, ++j)
			data.EStates[j].AddElec(tot * probs[i] * ElecAdded);
		probs.clear();
	}
	else if(TypicalMaxCharge > 0.0 && -ActualCharge > TypicalMaxCharge * 1.01)
	{
		//correct the situation - there is too much negative charge
		double HoleAdded = -ActualCharge - TypicalMaxCharge;
		std::vector<double> probs;
		double tot = 0.0;
		probs.resize(numStates);
		for(unsigned int i = 0, j = firstELevel; i<numStates; ++i, ++j)
		{
			probs[i] = data.EStates[j].GetElec();
			tot += probs[i];
		}
		tot = 1.0 / tot;
		for(unsigned int i = 0, j = firstELevel; i<numStates; ++i, ++j)
			data.EStates[j].AddHole(tot * probs[i] * HoleAdded);
		probs.clear();
	}
	
	//copy all this over to the mdl framework
	for(unsigned int i=firstELevel; i<= lastELevel; ++i)
	{
		data.EStates[i].ptr->SetBeginOccStates(data.EStates[i].GetOcc());
		data.EStates[i].ptr->SetEndOccStates(data.EStates[i].GetOcc());
		if(i==firstELevel+10)
			VBGainHoles.push_back(data.EStates[i].GetOcc());
	}

	double Nbeta = 0.0;
	if(pt->temp > 0.0)
		Nbeta = -KBI / pt->temp;

	Outputs saveOutput = mdesc.simulation->outputs;
	mdesc.simulation->outputs.Edef = mdesc.simulation->outputs.Erec = mdesc.simulation->outputs.Escat = mdesc.simulation->outputs.Esrh =
		mdesc.simulation->outputs.Ldef = mdesc.simulation->outputs.Lgen = mdesc.simulation->outputs.Lscat = mdesc.simulation->outputs.Lsrh = 
		mdesc.simulation->outputs.LightEmission = mdesc.simulation->outputs.Lpow = false;	//prevent a bunch of map processing

	std::vector<bool> AllowDecrease;
	AllowDecrease.resize(numStates,false);
	int decCtr = 0;
	double MaxPercentTransferToEnd=0.125;
	int limitIndex;
	int ctr = 0;
	std::vector<double> timestep;
	std::vector<int> LimitList;
	while(MaxPercentTransferToEnd > SS_TRANSIENT_MIN)
	{
		//now do a bunch of iterations! This is entirely going to be in the mdl framework.
		if(!ignoreDD)
		{
			//the band diagram for the point only matters if carriers are being transferred spatially
			CalcPointBandFast(*pt, mdl);
			if(pt->adj.adjMX)
			{
				CalcPointBandFast(*pt->adj.adjMX, mdl);	//get this reset before it is used
				CalcScatterConsts(pt->adj.adjMX->scatterCBConst, pt->adj.adjMX->cb.energies, Nbeta);
				CalcScatterConsts(pt->adj.adjMX->scatterVBConst, pt->adj.adjMX->vb.energies, Nbeta);
			}

			if(pt->adj.adjPX)
			{
				CalcPointBandFast(*pt->adj.adjPX, mdl);	//get this reset before it is used
				CalcScatterConsts(pt->adj.adjPX->scatterCBConst, pt->adj.adjPX->cb.energies, Nbeta);
				CalcScatterConsts(pt->adj.adjPX->scatterVBConst, pt->adj.adjPX->vb.energies, Nbeta);
			}
		}

		double totNRateIn = 0.0;
		double occ,avail;
		double kT = KB * pt->temp;
		float degen=1.0;
		
		pt->largestOccCB = pt->largestOccES = pt->largestOccVB = pt->largestUnOccES = 0.0;	//keep irrelevant states from screwing everything up!
		for(unsigned int i=0; i<numStates; ++i)
		{
			mdlEStates[i]->NetRateN.clear();
			mdlEStates[i]->NetRateP.clear();
			mdlEStates[i]->RateSums.Clear();
			mdlEStates[i]->netRateM1 = mdlEStates[i]->netRate;
			mdlEStates[i]->netRate = 0.0;
			mdlEStates[i]->rateIn = 0.0;
			mdlEStates[i]->rateOut = 0.0;
			mdlEStates[i]->Rates.clear();
			mdlEStates[i]->OpticalRates.clear();
			mdlEStates[i]->TransferEndToBegin();
			mdlEStates[i]->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			if (mdlEStates[i]->imp || mdlEStates[i]->bandtail)	//it's a defect/bandtail - not CB/VB
			{
				if (occ > pt->largestOccES)
					pt->largestOccES = occ;
				if (avail > pt->largestUnOccES)
					pt->largestUnOccES = avail;
			}
			else if (mdlEStates[i]->carriertype == ELECTRON)
			{
				if (occ > pt->largestOccCB)
					pt->largestOccCB = occ;
			}
			else if (occ > pt->largestOccVB)
				pt->largestOccVB = occ;
		}
	
		pt->ResetData();

		mdl.poissonRho[data.Pt[point].mdlRho] = pt->rho;
		
		CalcScatterConsts(pt->scatterCBConst, pt->cb.energies, Nbeta);
		CalcScatterConsts(pt->scatterVBConst, pt->vb.energies, Nbeta);
		
		Light(mdl, mdesc, pt);	//puts all the light rates into RateSums.
		if(pt->contactData && !ignoreDD)
			ProcessContactSteadyState(mdl,pt->contactData, mdesc.simulation);
		//puts all the contact rates into Rate Sums

		double tStep;
		double desiredTstep = -1;;
		limitIndex = -1;
		for(unsigned int i=0; i<numStates; ++i)
		{
			ELevel* ptrE = mdlEStates[i];	//because i'm copying code and lazy
			double NRateIn = 0.0;
			for (unsigned int trg = 0; trg < ptrE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
			{
				if (CalcFastRate(ptrE, ptrE->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
				{
					if(NRateIn==0.0)
						continue;

					ptrE->RateSums.AddValue(NRateIn);
				}
			}
			if(!ignoreDD)
			{
				for (unsigned int trg = 0; trg < ptrE->TargetELevDD.size(); ++trg)	//all the scattering/recomb/srh rates
				{
					if (CalcFastRate(ptrE, ptrE->TargetELevDD[trg].target, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
					{
						if(NRateIn==0.0)
							continue;

						ptrE->RateSums.AddValue(NRateIn);
					}
				}
			}
			NRateIn = ptrE->RateSums.GetNetSum();
			ptrE->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			
			double useOccupancy = 0.0;	//if it's an insignificant one, we're going to inflate the desired timestep by giving it a large value.
			//this will cause over steps, but the overall change on the rest of the device should be negligible. A newton-raphson should for the most
			//part just eliminate any excess charge that built up, which won't even appear in the sums. 1e-50 + 1 = 1 as far as the computer is concerned
			if (NRateIn > 0.0)
			{
				if (ptrE->imp || ptrE->bandtail)	//it's a defect/bandtail - not CB/VB
				{
					//nRateIn means it has a few carriers, but more carriers are being added. We want to be on the same magnitude of what's currently in there
					useOccupancy = (occ > pt->largestOccES * LEVELINSIGNIFICANT) ? occ : pt->largestOccES;
				}
				else if (ptrE->carriertype == ELECTRON)
				{
					useOccupancy = (occ > pt->largestOccCB * LEVELINSIGNIFICANT) ? occ : pt->largestOccCB;
				}
				else// if (ptrE->carriertype == HOLE)
				{
					useOccupancy = (occ > pt->largestOccVB * LEVELINSIGNIFICANT) ? occ : pt->largestOccVB;
				}
			}
			else if (NRateIn < 0.0) //this means carriers are leaving
			{
				if (ptrE->imp || ptrE->bandtail)	//it's a defect/bandtail - not CB/VB
				{
					//nRateIn means it has too many carriers, We want to be on the same magnitude of what it's going to
					useOccupancy = (occ > pt->largestUnOccES * LEVELINSIGNIFICANT) ? occ : pt->largestOccES;
				}
				else if (ptrE->carriertype == ELECTRON)
				{
					useOccupancy = (occ > pt->largestOccCB * LEVELINSIGNIFICANT) ? occ : pt->largestOccCB;
				}
				else// if (ptrE->carriertype == HOLE)
				{
					useOccupancy = (occ > pt->largestOccVB * LEVELINSIGNIFICANT) ? occ : pt->largestOccVB;
				}
			}
			else
				useOccupancy = occ;	//just avoid 0.0/0.0

			if (AllowDecrease[i] == false) //only let the ones that didn't get close influence the time step
			{
				if (NRateIn != 0.0)
				{
					tStep = fabs(useOccupancy / NRateIn);
					if (tStep < desiredTstep || desiredTstep <= 0.0)	//choose the smallest time step
					{
						desiredTstep = tStep;
						limitIndex = i;
					}
				}
			}
			/*
			if(occ > avail)
			{
				avail = MaxPercentTransferToEnd * avail;	//this is how much it is allowed to send in
				if(avail > 0.0 && NRateIn != 0.0)
				{
					tStep = fabs(avail / NRateIn);
					if(tStep < desiredTstep || desiredTstep <= 0.0)	//choose the smallest time step
					{
						desiredTstep = tStep;
						limitIndex = i;
					}
				}
			}
			else// if(NRateIn < 0.0)
			{
				occ = MaxPercentTransferToEnd * occ;	//this is how much it is allowed to send in
				if(occ > 0.0 && NRateIn != 0.0)
				{
					tStep = fabs(occ / NRateIn);
					if(tStep < desiredTstep || desiredTstep <= 0.0)	//choose the smallest time step
					{
						desiredTstep = tStep;
						limitIndex = i;
					}
				}
			}
			*/
		}	//end loop cycling through all states and calculating their rates
		//now see how it needs to be adjusted
		

		//any are not converged enough
		for(unsigned int i=0; i<numStates; ++i)
		{
			//check to keep Going

			double NRate = mdlEStates[i]->RateSums.GetNetSum();
			mdlEStates[i]->netRate = NRate;
			if(NRate == 0.0)
			{
				if(!AllowDecrease[i])
				{
					AllowDecrease[i] = true;
					decCtr++;
				}
				continue;	//that was easy
			}

			mdlEStates[i]->GetOccupancyOcc(RATE_OCC_BEGIN,occ,avail);
			if(NRate > 0.0)
			{
				double transfer = desiredTstep * NRate;
				if (AllowDecrease[i]) //it is near what it thought was it's solution. Don't let it transfer very much
				{
					double smaller = occ < avail ? occ : avail;
					if (smaller == 0.0)
						smaller = 1.0e-280;	//just a small number... arbitrary

					smaller = smaller * MaxPercentTransferToEnd * 0.125;	//allow 1/8th the change it was beforehand
					if (smaller < transfer)	//if transfer<0, this can't happen. smaller > 0 and transfer > 0
						transfer = smaller;
					else if (smaller < -transfer)	//if transfer was >0, it is now negative and can't happen. However, if neg, now it compares
						transfer = -smaller;
				}
				if(transfer > avail) //now make sure there is actually enough space
				{
					transfer = avail * 0.75;
					if(!AllowDecrease[i])
					{
						AllowDecrease[i] = true;
						decCtr++;
					}
				}
				
				mdlEStates[i]->AddEndOccStates(transfer);
				if(i==10)
					VBGainHoles.push_back(transfer);
			}
			else
			{
				double transfer = desiredTstep * -NRate;	//this will be positive...
				if (AllowDecrease[i]) //it is near what it thought was it's solution. Don't let it transfer very much
				{
					double smaller = occ < avail ? occ : avail;
					if (smaller == 0.0)
						smaller = 1.0e-280;	//just a small number... arbitrary

					smaller = smaller * MaxPercentTransferToEnd * 0.125;	//allow 1/8th the change it was beforehand
					if (smaller < transfer)	//if transfer<0, this can't happen. smaller > 0 and transfer > 0
						transfer = smaller;
					else if (smaller < -transfer)	//if transfer was >0, it is now negative and can't happen. However, if neg, now it compares
						transfer = -smaller;
				}
				if(transfer > occ)
				{
					transfer = occ * 0.75;
					if(!AllowDecrease[i])
					{
						AllowDecrease[i] = true;
						decCtr++;
					}
				}
				
				mdlEStates[i]->RemoveEndOccStates(transfer);
				if(i==10)
					VBGainHoles.push_back(-transfer);
			}

			if((mdlEStates[i]->netRateM1 < 0.0 && mdlEStates[i]->netRate > 0.0) || 
				(mdlEStates[i]->netRateM1 > 0.0 && mdlEStates[i]->netRate < 0.0))	//it is now starting to hone in on the solution
			{
				if(!AllowDecrease[i])
				{
					AllowDecrease[i] = true;
					decCtr++;
				}
			}


			
		}

		if(decCtr == numStates)
		{
			AllowDecrease.clear();
			AllowDecrease.resize(numStates, false);	//reset everything
			MaxPercentTransferToEnd = MaxPercentTransferToEnd * 0.5;
			decCtr = 0;
		}

		//my debug stuff
		ctr++;
		timestep.push_back(desiredTstep);
		LimitList.push_back(limitIndex);
	} //end while keepGoing
	
	//update the ss_data structure
	for(unsigned int i = firstELevel,j=0; i<=lastELevel; ++i,++j)
	{
		double occ, avail;
		mdlEStates[j]->GetOccupancyOcc(RATE_OCC_END, occ, avail);
		if(occ < avail)
		{
			data.EStates[i].SetOcc(occ);
			mdlEStates[j]->SetBeginOccStates(occ);
		}
		else
		{
			data.EStates[i].SetAvail(avail);
			mdlEStates[j]->SetBeginOccStatesOpposite(avail);
		}
	}
	pt->ResetData();
	mdl.poissonRho[data.Pt[point].mdlRho] = pt->rho;

	mdlEStates.clear();

	mdesc.simulation->outputs = saveOutput;	//restore the outputs

	return(0.0);
}

double GetSmoothedCurEf(SteadyState_SAAPD& data, unsigned long int EState)
{
	if (EState >= data.EStates.size())
		return(0.0);	//nothing was changed

	unsigned int numEquiv = data.EStates[EState].NeighborEquivs.size();

	if (numEquiv == 0)
		return(0.0);

	//must determine percent weights...
	//two percentages: one for each adjacent point by distance - and the other based on all the points in that point distribution
	Point* srcPt = &data.EStates[EState].ptr->getPoint();
	std::vector<Point*> Pts;
	std::vector<Point*> NbrPt;	//what it is in data
	std::vector<unsigned int> WhichPt;	//which point index does it correspond with
	std::vector<unsigned int> NbrElevs;
	std::vector<double> NumInPoint;	//used for averaging among the point - and then
	std::vector<double> PtDistances;
	std::vector<double> Probabilities;
	NbrElevs.insert(NbrElevs.begin(), data.EStates[EState].NeighborEquivs.begin(), data.EStates[EState].NeighborEquivs.end());	//copy the vector in
	double PtProb = 0.0;

	for (unsigned int i = 0; i < numEquiv; ++i)
	{
		bool found = false;
		NbrPt.push_back(&data.EStates[NbrElevs[i]].ptr->getPoint());
		for (unsigned int j = 0; j < Pts.size(); ++j)
		{
			if (NbrPt.back() == Pts[j])
			{
				found = true;
				NumInPoint[j] += 1.0;
				WhichPt.push_back(j);
				break;
			}
		}
		if (!found)
		{
			PtDistances.push_back((srcPt->position - NbrPt.back()->position).Magnitude());
			PtProb += PtDistances.back();

			WhichPt.push_back(Pts.size());
			Pts.push_back(NbrPt.back());
			NumInPoint.push_back(1.0);
		}
	}

	//each is going to be like an expanded lever rule. Given 3 distances, A, B, and C
	/*
	A: (B+C)/(A+B+C)
	B: (A+C)/(A+B+C)
	C: (A+B)/(A+B+C)
	Total Weight: (B+C+A+C+A+B)/(A+B+C)=2*(A+B+C)/(A+B+C)= 2 = (Number distances - 1)
	Therefore, the prob for the A distance = (B+C)/(A+B+C)/(N-1) - in most cases, N=2. If N=1, you can't smoothen it out anyway - if N=1, ignore distance aspect

	PtDistances has just the thing stored in it, so we can basically change it too (tot - self)/tot

	We want to invert NumInPoint to allow simple multiply, as that will be the averaging over all the different energy states
	*/


	if (Pts.size() > 1)
	{
		Probabilities.resize(NbrElevs.size());
		double numDist = double(Pts.size() - 1);

		for (unsigned int i = 0; i < Probabilities.size(); ++i)
			Probabilities[i] = (PtProb - PtDistances[WhichPt[i]]) / (PtProb * NumInPoint[WhichPt[i]] * numDist);
	}
	else
	{
		double tot = 1.0 / double(Probabilities.size());
		Probabilities.resize(NbrElevs.size(), tot);
	}

	//now PtDistances

	//These are equivalent positions - they share the same state type (band/tail/don/acp), and are the same distance away from the band.
	//this is JUST a weighted averaging problem, then.

	double tmpCurEf = 0.0;
	for (unsigned int i = 0; i < Probabilities.size(); ++i)
		tmpCurEf += Probabilities[i] * data.EStates[NbrElevs[i]].curEf;

	
	
	Pts.clear();
	NbrPt.clear();	//what it is in data
	WhichPt.clear();	//which point index does it correspond with
	NbrElevs.clear();
	NumInPoint.clear();	//used for averaging among the point - and then
	PtDistances.clear();
	Probabilities.clear();

	return(tmpCurEf);

}

/*

This will smoothen out the particular energy states fermi level according to it's neighbor!
*/

double SmoothELevelCurEf(SteadyState_SAAPD& data, unsigned long int EState, double factor)
{
	if (EState >= data.EStates.size())
		return(0.0);	//nothing was changed

	unsigned int numEquiv = data.EStates[EState].NeighborEquivs.size();

	if (numEquiv == 0)
		return(0.0);

	double newEf = GetSmoothedCurEf(data, EState);

	double delta = newEf - data.EStates[EState].curEf;
	if (factor < 1.0 && factor > 0.0)
	{
		delta = delta * (1.0 - factor);
		data.EStates[EState].UpdateOccVars(newEf - delta, true);
	}
	else
		data.EStates[EState].UpdateOccVars(newEf, true);

	return(delta);
}


int SS_Cluster_MC(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread, double adjAcc)
{
	MonteCarlo* MCData = new MonteCarlo(data.EStates.size(), mdesc.simulation->SteadyStateData, adjAcc);

	unsigned int mainIndex;
	bool reCalcRates = true;
	mdesc.simulation->EnabledRates = InitCalc;
	UpdateLightEnable(mdesc);
	time_t start;
	start = time(NULL);
	if (kThread)
	{
		kThread->shareData.KernelTimeLeft = 0;
		kThread->shareData.KernelMinTolerance = -1.0;
		kThread->shareData.KernelMaxTolerance = -1.0;
		data.tolerance = MCData->ev_Adjust;
		kThread->shareData.KernelTolerance = data.tolerance;
		kThread->shareData.KernelshortProgBarRange = data.EStates.size();// * MC_FAILMULTIPLIER;
		kThread->shareData.KernelshortProgBarPosition = 0;
		kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO;

	}
	CreateLightSourcesFast(data, mdl, mdesc);	//get this done at least once before it all starts!
	do
	{
		int val = MCData->ReadyMoveOn(mdesc.simulation->SteadyStateData->PercentGood);
		if (val == MC_DONE)
		{
			MCData->ev_Adjust = MCData->ev_Adjust * MC_TOLERANCE_REDUCE;
			data.tolerance = MCData->ev_Adjust;
			MCData->ClearFail();	//mark them all as good again!
			MCData->ClearTracking();
			GenerateOutput(mdl, mdesc, data, InitCalc, kThread);	//update output to see progression of data
			reCalcRates = true;
			if (kThread)
			{
				kThread->shareData.KernelTolerance = data.tolerance;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
				kThread->shareData.KernelshortProgBarRange = data.EStates.size();// * MC_FAILMULTIPLIER;
				kThread->shareData.KernelshortProgBarPosition = 0;
				kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO;
			}
		}
		else if (val == MC_MOVEON)
		{
			
			if (kThread)
			{
				kThread->shareData.KernelTolerance = data.tolerance;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
				kThread->shareData.KernelshortProgBarRange = data.EStates.size() * mdesc.simulation->SteadyStateData->PercentGood;
				kThread->shareData.KernelshortProgBarPosition = MCData->GetNumGoodStates();
				kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO2;
			}
		}
		if (kThread)
		{
			unsigned int pos;
			if (MCData->GetProgBar())
				pos = MCData->GetNumGoodStates();
			else
			{
				pos = MCData->GetReadyStates();
				if (pos == 0)
					pos = 1;	//make it not seem like it's frozen...
			}
			kThread->shareData.KernelshortProgBarPosition = pos;
			kThread->SSKernelInteract(mdesc.simulation, start);

			if (kThread->shareData.GUIabortSimulation)	//just get out if possible
				break;

			if (kThread->shareData.GUIstopSimulation)
				break;	//get out of the loop and output the data as it stands

			if (kThread->shareData.GUINext)
				break;

			if (kThread->shareData.GUIsaveState)
			{
				GenerateOutput(mdl, mdesc, data, InitCalc);	//it's going to output the file when it reduces the tolerance anyway
				kThread->shareData.GUIsaveState = false;	//reset it so it can be clicked again
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
			}
		}


		//		if (reCalcRates)
		//			SS_MC_CalcAllRates(MCData, data, mdl, mdesc);	//contact sinks are automatically set to zero!
		mainIndex = MCData->ChooseRandomState();

		//now we need to choose

		//		if (MCData->ChooseAbsRate(mainIndex))
		//		{
		if (MCData->AdjustState(mainIndex, data, mdl, mdesc) == 1)	//it changed occupation!
		{
			CreateLightSourceModifiedELevel(mdesc.simulation, data, mainIndex);	//update the light output
		}
		//
		MCData->RecordEf(mainIndex, data.EStates[mainIndex].curEf);	//if it keeps failing, the same number will keep getting put in, which is OK
		//			{
		//				reCalcRates = false;
		//				continue;	//try again
		//			}
		//			else
		//				reCalcRates = true;
		//		}


	} while (MCData->ev_Adjust * MCData->GetAdjAccuracy() >= mdesc.simulation->accuracy);

	MCData->OutputRandomStatedistribution(mdesc.simulation->SteadyStateData->CurrentData, data);

	MCData->TransferData(data);	//get all the average Ef's in the model value
	double change = MCData->ev_Adjust;
	delete MCData;
	MCData = NULL;

	//	SS_MC_Newton(data, mdl, mdesc, kThread);	//this will do a numerical approximation of the jacobian by calculating all the rates
	/*	double oldT = data.tolerance;
	data.tolerance = 0.01;
	SSNewton(data, mdl, mdesc, kThread, true);	//try to hone in at the end via a normal newton calculation
	data.tolerance = oldT;
	*/
	//as is, shifting an energy level occupancy by 0.1%, and recalculating all the rates. Then it solves the matrix (hopefully)
	//As is, this seemed to ROYALLY screw everything up, and it was supposed to be close!
	//Correction: once you put the matrix terms in the right location, it only mildly screws everything up. THere is some very awkward carrier depletion that occurs,
	//but at least the band diagram doesn't vary by 6 orders of magnitude.
	return(change);

}


/*
This will pick a state at random (weight by |Net Rate|).
Adjust the fermi level in the proper direction by a specified eV. Make it continually get to be smaller and smaller.
Check #1: If the magnitude of the rate decreases, accept the move. If not, reject/restore.
If the magnitude decreases, this is a move in the correct direction.

Maintain device charge.
Pick states at random that want the charge needed to restore the previous device state charge.
Continue adding charge to state - up to specified fermi level eV difference or until all charge is gathered.
Accept putting the charge in state if it decreases magnitude of net rate - attempt 3x - if always fail, just move on to a new state.
first is eV tolerance change. if no good, second is leftover charge (false, this will never work). If no good, second uses the two to extrapolate to a 'good rate' (0)

Reset the device, rinse, repeat.

Keep track of what is rejected. Store in char array. Exclude rejected. Note whether want positive charge or want negative charge. (REJECT, WANTPOS, WANTNEG)

If it cannot move enough charge (such as if not enough states can accept the change charge), then the original passed gets reduced to a smaller charge step in the
correct direction
*/
int SS_MC(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, Calculations& InitCalc, kernelThread* kThread, double adjAcc)
{
	MonteCarlo* MCData = new MonteCarlo(data.EStates.size(), mdesc.simulation->SteadyStateData, adjAcc);
	double finalAccuracy = mdesc.simulation->SteadyStateData->GetCalcEndAccuracy();
	if (finalAccuracy <= 0)
		finalAccuracy = mdesc.simulation->accuracy;
	unsigned int mainIndex;
	bool reCalcRates = true;
	mdesc.simulation->EnabledRates = InitCalc;
	UpdateLightEnable(mdesc);
	time_t start;
	start = time(NULL);
	if (kThread)
	{
		kThread->shareData.KernelTimeLeft = 0;
		kThread->shareData.KernelMinTolerance = -1.0;
		kThread->shareData.KernelMaxTolerance = -1.0;
		data.tolerance = MCData->ev_Adjust;
		kThread->shareData.KernelTolerance = data.tolerance;
		kThread->shareData.KernelshortProgBarRange = data.EStates.size();// * MC_FAILMULTIPLIER;
		kThread->shareData.KernelshortProgBarPosition = 0;
		kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO;
		kThread->shareData.KernelMaxChange = data.tolerance;	//it is changing by the tolerance amount
		
	}
	CreateLightSourcesFast(data, mdl, mdesc);	//get this done at least once before it all starts (if it will affect rates)
	unsigned int counter = 0;
	do
	{
		int val = MCData->ReadyMoveOn(mdesc.simulation->SteadyStateData->PercentGood);
		if (val == MC_DONE)
		{
			MCData->ReduceTolerance();
			data.tolerance = MCData->ev_Adjust;
			GenerateOutput(mdl, mdesc, data, InitCalc, kThread);	//update output to see progression of data
			reCalcRates = true;
			if (kThread)
			{
				kThread->shareData.KernelTolerance = data.tolerance;
				kThread->shareData.KernelMaxChange = data.tolerance;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
				kThread->shareData.KernelshortProgBarRange = data.EStates.size();
				kThread->shareData.KernelshortProgBarPosition = 0;
				kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO;
			}
		}
		else if (val == MC_MOVEON)	//done acquiring statistics, now want the stats to converge!
		{
			MCData->ClearFail();	//make them all have failed and require at least one more entry at this point
			if (kThread)
			{
				kThread->shareData.KernelcurInfoMsg = MSG_MONTECARLO2;
				kThread->shareData.KernelshortProgBarRange = data.EStates.size() * mdesc.simulation->SteadyStateData->PercentGood;
			}
		}
		if (kThread)
		{
			unsigned int pos;
			if (MCData->GetProgBar())
				pos = MCData->GetNumGoodStates();
			else
			{
				pos = MCData->GetReadyStates();
				if (pos == 0)
					pos = 1;	//make it not seem like it's frozen...
			}
			kThread->shareData.KernelshortProgBarPosition = pos;
			kThread->SSKernelInteract(mdesc.simulation, start);

			if (kThread->shareData.GUIabortSimulation)	//just get out if possible
				break;

			if (kThread->shareData.GUIstopSimulation)
				break;	//get out of the loop and output the data as it stands

			if (kThread->shareData.GUINext)
				break;

			if (kThread->shareData.GUIsaveState)
			{
				GenerateOutput(mdl, mdesc, data, InitCalc);	//it's going to output the file when it reduces the tolerance anyway
				kThread->shareData.GUIsaveState = false;	//reset it so it can be clicked again
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, UPDATED_FILE);
			}
		}


//		if (reCalcRates)
//			SS_MC_CalcAllRates(MCData, data, mdl, mdesc);	//contact sinks are automatically set to zero!
		mainIndex = MCData->ChooseRandomState();

//		if (MCData->ChooseAbsRate(mainIndex))
//		{
		if(MCData->AdjustState(mainIndex, data, mdl, mdesc) == 1)	//it changed occupation!
		{
			CreateLightSourceModifiedELevel(mdesc.simulation, data, mainIndex);	//update the light output
		}
		//
		MCData->RecordEf(mainIndex, data.EStates[mainIndex].curEf);	//if it keeps failing, the same number will keep getting put in, which is OK
//			{
//				reCalcRates = false;
//				continue;	//try again
//			}
//			else
//				reCalcRates = true;
//		}

		counter++;
		if (counter == data.EStates.size())
		{
			CalcBandStruct(mdl, mdesc);	//periodically reset everything and make sure the boundary conditions are reset
			counter = 0;
		}
	} while (MCData->ev_Adjust * MCData->GetAdjAccuracy() >= finalAccuracy);

	MCData->OutputRandomStatedistribution(mdesc.simulation->SteadyStateData->CurrentData, data);

	MCData->TransferData(data);	//get all the average Ef's in the model value
	double change = MCData->ev_Adjust;
	delete MCData;
	MCData = NULL;

//	SS_MC_Newton(data, mdl, mdesc, kThread);	//this will do a numerical approximation of the jacobian by calculating all the rates
/*	double oldT = data.tolerance;
	data.tolerance = 0.01;
	SSNewton(data, mdl, mdesc, kThread, true);	//try to hone in at the end via a normal newton calculation
	data.tolerance = oldT;
*/
	//as is, shifting an energy level occupancy by 0.1%, and recalculating all the rates. Then it solves the matrix (hopefully)
	//As is, this seemed to ROYALLY screw everything up, and it was supposed to be close!
	//Correction: once you put the matrix terms in the right location, it only mildly screws everything up. THere is some very awkward carrier depletion that occurs,
	//but at least the band diagram doesn't vary by 6 orders of magnitude.
	return(change);

}

int CalcNumericalJacobian(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrix, double* rhs, bool* useELevel, unsigned int sz, bool fullMatrix, double* derivMatrixAnalytical)
{
	if(derivMatrix == NULL || rhs == NULL || sz == 0)
		return(1);

	time_t TotStart = time(NULL);
	time_t predictTime = TotStart;
	time_t startRotate = TotStart;
	time_t now;
	unsigned long int matrixSz = sz*sz;
	double timeelapse = 0.0;

	for(unsigned long int el=0;el<sz;++el)
	{
		if(data.EStates[el].OccMatches)
		{
			data.EStates[el].ptr->SetBeginOccStates(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStates(data.EStates[el].occupancy);
		}
		else
		{
			data.EStates[el].ptr->SetBeginOccStatesOpposite(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStatesOpposite(data.EStates[el].occupancy);
		}
	}

	PrepAllSFirst(mdl, mdesc);

	kThread->shareData.KernelcurInfoMsg = MSG_CALCJACOBIAN;
	kThread->shareData.KernelshortProgBarPosition = 0;
	kThread->shareData.KernelshortProgBarRange = sz;
	kThread->shareData.KernelTimeLeft = 0.0;
	kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);


	PosNegSum* InitRates = new PosNegSum[sz];
	PosNegSum* AdjRate = new PosNegSum[sz*2];
	

	for (unsigned int pt = 0; pt < data.Pt.size(); ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change
		Point* ptrPt = mdl.getPoint(data.Pt[pt].self);
		double Nbeta = -KBI / ptrPt->temp;
		CalcScatterConsts(ptrPt->scatterCBConst, ptrPt->cb.energies, Nbeta);
		CalcScatterConsts(ptrPt->scatterVBConst, ptrPt->vb.energies, Nbeta);
	}
	for (unsigned long int st = 0; st < sz; ++st) //loop through all the states in the point
	{
		useELevel[st] = false;
		ELevel* ptrE = data.EStates[st].ptr;	//because i'm copying code and lazy
		double NRateIn = 0.0;
		ptrE->RateSums.Clear();
		InitRates[st].Clear();
		if(ptrE->IsContactSink())
			continue;	//don't include these rates in contact sinks?
		for (unsigned int trg = 0; trg < ptrE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
		{
			if (CalcFastRate(ptrE, ptrE->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false, &InitRates[st]) == true)	//returns true if calculated rate
			{
//				ptrE->RateSums.AddValue(NRateIn);
				useELevel[st] = true;
			}
		}
		for (unsigned int trg = 0; trg < ptrE->TargetELevDD.size(); ++trg)	//all the drift diffusion rates
		{
			if (CalcFastRate(ptrE, ptrE->TargetELevDD[trg].target, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false, &InitRates[st]) == true)	//returns true if calculated rate
			{
//				ptrE->RateSums.AddValue(NRateIn);
				useELevel[st] = true;
			}
		}

	}

	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); i++)
		ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);	//this also puts in ELevel* RateSums

	Light(mdl, mdesc);	//puts all the light rates into RateSums. for everything - now that they've all been quickly generated!
	
	

	//now put all this into the ptr Rate!
	for (unsigned long int st = 0; st < sz; ++st)
	{
		InitRates[st] += data.EStates[st].ptr->RateSums;	//add in light and contact rates
		data.EStates[st].ptr->RateSums.Clear();	//clear out light and contact rates
//		data.EStates[st].ptr->netRateM1 = data.EStates[st].ptr->RateSums.GetNetSumClear();	//this will store the original rates for everything
	}
	
	unsigned long int prevSrc=0;
	
	for(unsigned long int src=0; src<sz; ++src)	//cycle through all the energy levels
	{
		ELevel* srcEptr = data.EStates[src].ptr;
		Point* curPt = &srcEptr->getPoint();
		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
			break;

		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);

		now = time(NULL);	//load the end time for dif
		timeelapse = difftime(now, predictTime);
		kThread->shareData.KernelshortProgBarPosition = src;
		if(timeelapse > 1.5)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//update the progress bar every second or every iteration, whichever is longer
			
			long int moved = src-prevSrc;
			kThread->shareData.KernelTimeLeft = timeelapse/double(moved) * double(sz-src);
			prevSrc = src;
				//					DisplayProgress(60, src*sz);
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
			predictTime = now;
		}

		//initialization
		for(unsigned long int trg=0; trg<sz; ++trg)
			derivMatrix[trg*sz+src]=0.0;	//make sure it gets cleared out! This state means nothing!

//		rhs[src] = -data.EStates[src].ptr->netRateM1;	//store -Rates (Newton's equation)

		if(data.EStates[src].occupancy == 0.0)	//this state clearly means jack shit. Ignore it all together.
			continue;	//move on to the next state.

		if(curPt->IsContactSink())
			continue;
		
		if(useELevel[src]==false)
			continue;	//it doesn't calculate any rates! This state should have no effect on the system

		//now we're going to update the occupancy by adding a small amount of carriers
		double oldOcc = data.EStates[src].occupancy;
		bool oldMatch = data.EStates[src].OccMatches;
		//variable should always be < 90%. Typically < 50%.
		double difOcc = data.EStates[src].occupancy * (1.01 - 1.0/1.01);
		if (!oldMatch)
			difOcc = -difOcc;	//kinda went the intended wrong way, but meh. This will still work.
		
		double Nbeta = -KBI / curPt->temp;

		for (int direction = 0; direction < 2; direction++)
		{
			data.EStates[src].occupancy = (direction == 1) ? oldOcc * 1.01 : oldOcc / 1.01;	//this can not possibly go over num states, because this

			//otherwise... recalculate all the rates with this adjustment
//			double difOcc = data.EStates[src].occupancy - oldOcc;	//this can only be zero if occ = 0. So all good!
			

			if (data.EStates[src].OccMatches)
			{
				data.EStates[src].ptr->SetBeginOccStates(data.EStates[src].occupancy);
				data.EStates[src].ptr->SetEndOccStates(data.EStates[src].occupancy);
			}
			else
			{
				data.EStates[src].ptr->SetBeginOccStatesOpposite(data.EStates[src].occupancy);
				data.EStates[src].ptr->SetEndOccStatesOpposite(data.EStates[src].occupancy);
			}
			curPt->ResetData();
			mdl.poissonRho[curPt->adj.self] = curPt->rho;
			CalcBandStruct(mdl, mdesc);
			
			CalcScatterConsts(curPt->scatterCBConst, curPt->cb.energies, Nbeta);
			CalcScatterConsts(curPt->scatterVBConst, curPt->vb.energies, Nbeta);
			CreateLightSourceModifiedELevel(mdesc.simulation, data, src);




			//at this point, the light sources should all be changed.
			std::vector<Point*> ptVect;	//just make it to satisfy calcstaterate. These don't actually modify other stuff now.
			bool used;
			if (direction == 0)
			{
//				AdjRate[trg*2].Clear();
				for (unsigned long int trg = 0; trg < sz; ++trg)
				{
					double nRate = CalcStateRate(&data.EStates[trg], mdl, mdesc, ptVect, used, &AdjRate[trg*2]);	//calculate the total rate
				}
			}
			else
			{
//				AdjRate[trg*2+1].Clear();
				for (unsigned long int trg = 0; trg < sz; ++trg)
				{
					//			AdjRate.Clear();	Cleared befire assugbed in CalcStateRate
					double nRate = CalcStateRate(&data.EStates[trg], mdl, mdesc, ptVect, used, &AdjRate[trg*2+1]);	//calculate the total rate
					//this includes any contacts or light rates
//					AdjRate[direction] -= InitRates[trg];	//get the difference. New - Old.
					//			double dif = DoubleSubtract(nRate, data.EStates[trg].ptr->netRateM1);
					derivMatrix[trg*sz + src] = (AdjRate[trg*2+1]-AdjRate[trg*2]/*InitRates[trg]*/).GetNetSum() / difOcc;	//dR_trg/dc_src, and no crazy stuff to do!
				}
			}
		}
		data.EStates[src].occupancy = oldOcc;
		data.EStates[src].OccMatches = oldMatch;
		//restore to original value
		if(data.EStates[src].OccMatches)
		{
			data.EStates[src].ptr->SetBeginOccStates(data.EStates[src].occupancy);
			data.EStates[src].ptr->SetEndOccStates(data.EStates[src].occupancy);
		}
		else
		{
			data.EStates[src].ptr->SetBeginOccStatesOpposite(data.EStates[src].occupancy);
			data.EStates[src].ptr->SetEndOccStatesOpposite(data.EStates[src].occupancy);
		}
		curPt->ResetData();
		mdl.poissonRho[curPt->adj.self] = curPt->rho;
		
		CalcScatterConsts(curPt->scatterCBConst, curPt->cb.energies, Nbeta);
		CalcScatterConsts(curPt->scatterVBConst, curPt->vb.energies, Nbeta);
		CreateLightSourceModifiedELevel(mdesc.simulation, data, src);	//restore the light outputs on the src point
	}
	

	//refine the matrix by zeroing out the effect of states that will not be changing
	ClearUnusedLevels(derivMatrix, rhs, useELevel, sz);

	for (unsigned long int i = 0; i < sz; ++i)
		rhs[i] = -InitRates[i].GetNetSumClear();// -data.EStates[i].ptr->netRateM1;	//for contact sinks, this is EVERYTHING

	if(!fullMatrix)
		OptimizeMatrix(data, derivMatrix, sz);

	delete[] InitRates;
	delete[] AdjRate;

	return(0);

}

double SS_Newton_Numerical(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrixAnalytical, bool fullMatrix)	//end it with one giant as Jacobian calculation
{
	time_t TotStart = time(NULL);
	time_t predictTime = TotStart;
	time_t startRotate = TotStart;
	
	double timeelapse = 0.0;
	double largestChange = 0.0;
	//going to solve this jacobian numerically instead of doing the actual derivatives. We'll see if this bites us in the rear or not.
	//basically, MC should have already gotten us really close to the answer.  This should get the final convergence all good!
	unsigned long int sz = data.EStates.size();
	
	unsigned long int matrixSz = sz*sz;
#ifndef NDEBUG
	double* Rates = NULL;
	DisplayProgress(59, sz);
#endif
	double* derivMatrix = NULL;
	double* delOccs = NULL;
	bool* useELevel = NULL;
	try
	{
#ifndef NDEBUG
		Rates = new double[sz];
#endif
		useELevel = new bool[sz];
		delOccs = new double[sz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
		return(-1);
	}
	try
	{
		derivMatrix = new double[matrixSz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
#ifndef NDEBUG
		delete [] Rates;
#endif
		delete [] useELevel;
		delete [] delOccs;
		return(-1);
	}

	CalcNumericalJacobian(data, mdl, mdesc, kThread, derivMatrix, delOccs, useELevel, sz, fullMatrix, derivMatrixAnalytical);

#ifndef NDEBUG
	for(unsigned long int src=0; src<sz; ++src)
		Rates[src] = -delOccs[src];
#endif


	

	kThread->shareData.KernelcurInfoMsg = (fullMatrix) ? MSG_PROCESSJACOBIANFULL : MSG_PROCESSJACOBIAN;

	//restore original occupancy in case it failed
	
	if (SolveDenseOneTime(kThread, derivMatrix, delOccs, sz) == false)
	{
		ProgressCheck(64, false);	//send notification to trace file of failed output
		DisplayProgress(58, 0);		//tell user in dialogue box
		delete [] derivMatrix;
		delete [] useELevel;
		delete [] delOccs;
#ifndef NDEBUG
		delete [] Rates;
#endif
		return(-1.0);
	}

	
	//prevent it from going too far
	//clamp based on deltaEf.
	double clamp = DetermineClamp(data, delOccs, sz, largestChange);	//largestChange is return value

	//update everything that really matters
	for(unsigned int src=0; src<sz; ++src)
	{
		data.EStates[src].prevEf = data.EStates[src].curEf;
		data.EStates[src].AddOcc(delOccs[src] * clamp);	//updates occupancy and curEf
		data.EStates[src].UpdateModelOccupancy();	//updates mdl.EStates based on curEf.
	}
#ifndef NDEBUG
	delete [] Rates;
#endif
	delete [] derivMatrix;
	delete [] useELevel;
	delete [] delOccs;
	
	
	return(largestChange);
}

int CreateLightSourceModifiedELevel(Simulation* sim, SteadyState_SAAPD& data, unsigned long int eLevel)
{
	if(!sim->EnabledRates.LightEmissions)	//don't bother calculating if the rates are disabled!
		return(0);

	if (!sim->EnabledRates.RecyclePhotonsAffectRates)
		return(0);

	if(eLevel >= data.EStates.size())
		return(0);
	bool saveOut = sim->outputs.LightEmission;
	sim->outputs.LightEmission = false;
	
	Point* pt = &data.EStates[eLevel].ptr->getPoint();
	unsigned int ptID = data.EStates[eLevel].dataPt;
	if(ptID >= data.Pt.size())
		return(0);
	long int start = data.ELevelIndicesInPt[ptID].min;
	long int stop = data.ELevelIndicesInPt[ptID].max;

	if (start < 0 || stop < 0 || stop < start)
			return(0);

	pt->LightEmissions->light.clear();	//first clear it out.
	double tmpDouble;
	for (long int el = start; el <= stop; ++el)
	{
		for (unsigned int trg = 0; trg < data.EStates[el].ptr->TargetELev.size(); ++trg)
			CalcFastRate(data.EStates[el].ptr, data.EStates[el].ptr->TargetELev[trg], sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN, tmpDouble, true);

		data.EStates[el].ptr->OpticalRates.clear();	//new light sources requires new rates to be calculated. Actually, don't think the rate gets saved, but better safe than sorry!
	}
	sim->outputs.LightEmission = saveOut;
	return(0);
}

int SS_MC_CalcAllRates(MonteCarlo* MCData, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	//initialize all rates, and make sure the data is up to date
	double occ;
	for (unsigned int pt = 0; pt < data.Pt.size(); ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change
		for (long int i = start; i <= stop; ++i) //loop through all the states in the point
		{
			data.EStates[i].ptr->RateSums.Clear();	//this might be the only thing that matters...
			data.EStates[i].ptr->Rates.clear();
			data.EStates[i].ptr->OpticalRates.clear();
			occ = data.EStates[i].occupancy;
			if (data.EStates[i].OccMatches)
				data.EStates[i].ptr->SetBeginOccStates(occ);
			else
				data.EStates[i].ptr->SetBeginOccStatesOpposite(occ);

			data.EStates[i].ptr->TransferBeginToEnd();	//make sure it's the same
		}
		mdl.getPoint(data.Pt[pt].self)->ResetData();
		mdl.poissonRho[data.Pt[pt].mdlRho] = mdl.getPoint(data.Pt[pt].self)->rho;
	}

	CalcBandStruct(mdl, mdesc);	//get the entire band structure correct
	
	for (unsigned int i = 0; i<mdesc.simulation->contacts.size(); i++)
		ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);	//this also puts in ELevel* RateSums

	for (unsigned int pt = 0; pt < data.Pt.size(); ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change
		Point* ptrPt = mdl.getPoint(data.Pt[pt].self);
		double Nbeta = -KBI / ptrPt->temp;
		CalcScatterConsts(ptrPt->scatterCBConst, ptrPt->cb.energies, Nbeta);
		CalcScatterConsts(ptrPt->scatterVBConst, ptrPt->vb.energies, Nbeta);
		
		for (long int st = start; st <= stop; ++st) //loop through all the states in the point
		{
			ELevel* ptrE = data.EStates[st].ptr;	//because i'm copying code and lazy
			double NRateIn = 0.0;
			for (unsigned int trg = 0; trg < ptrE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
			{
				if (CalcFastRate(ptrE, ptrE->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
					ptrE->RateSums.AddValue(NRateIn);
			}
			for (unsigned int trg = 0; trg < ptrE->TargetELevDD.size(); ++trg)	//all the scattering/recomb/srh rates
			{
				if (CalcFastRate(ptrE, ptrE->TargetELevDD[trg].target, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, true) == true)	//returns true if calculated rate
					ptrE->RateSums.AddValue(NRateIn);
			}
			
		}
	}
	Light(mdl, mdesc);	//puts all the light rates into RateSums. for everything - now that they've all been quickly generated!
	
	//now put all this into the probabilities!
	for (unsigned int pt = 0; pt < data.Pt.size(); ++pt)
	{
		long int start = data.ELevelIndicesInPt[pt].min;
		long int stop = data.ELevelIndicesInPt[pt].max;
		if (start < 0 || stop < 0 || stop < start)
			continue; //this point's values do not change
		for (long int st = start; st <= stop; ++st)
		{
			double nRate = data.EStates[st].ptr->RateSums.GetNetSum();
			if (data.Pt[pt].ctcSink || MCData->GetFail(st) || nRate==0.0)	//contact sinks are in 'thermal equilibrium' so we don't want this point to ever be randomly chosen.
			{  //if it's failed, this is not going to go over well
				MCData->SetRate(data.EStates[st], 0.0, st);
				MCData->SetFail(st);
			}
			else
				MCData->SetRate(data.EStates[st], nRate, st);
		}
		
	}


	return(0);
}
int MonteCarlo::OutputRandomStatedistribution(int iter, SteadyState_SAAPD& data)
{
	return(MCStateProbDistribution(RndmCount, BadCount, data, iter));
}

int MonteCarlo::TransferData(SteadyState_SAAPD& data)
{
	for(unsigned int i=0; i<maxGoodStates; ++i)
	{
		if(okMove[i])
			data.EStates[i].UpdateOccVars(avgEf[i], true);	//get the right Ef loaded
		data.EStates[i].UpdateModelOccupancy();
	}
	return(0);
}

//this just 'zeroes' out everything
int MonteCarlo::ClearTracking(void)
{
	for(unsigned int i=0; i<maxGoodStates; ++i)
	{
		EfCtr[i] = 0;
		for(unsigned int j=0; j<simdata->NumTrack; ++j)
		{
			EfTracker[i*simdata->NumTrack + j] = 0.0;	//guarantee that it will fail the initial tests!
		}
		avgEf[i] = 0.0;
		okMove[i] = false;
	}
	NumGoodStates = 0;
	readyStates = 0;
	return(0);
}

bool MonteCarlo::ReduceTolerance(void)
{
	ev_Adjust = ev_Adjust * MC_TOLERANCE_REDUCE;
	ClearFail();	//mark them all as good again!
	ClearTracking();
	ProgBar = false;
	return(false);
}
int MonteCarlo::ReadyMoveOn(double percentGood)
{
	if(percentGood > 1.0 || percentGood <= 0.0)
		percentGood = MC_PERCENTGOOD;
	if ((readyStates == maxGoodStates && NumGoodStates >= (maxGoodStates*percentGood)))
		return (MC_DONE);
	if (readyStates == maxGoodStates && ProgBar == false)
	{
		ProgBar = true;
		return(MC_MOVEON);
	}
	return(MC_NOCHANGE); 
}
unsigned long int MonteCarlo::ChooseRandomState(void)
{
	if (RandomCtr > 1050)
		RandomCtr = 0;

	unsigned long int Num;
	if (RandomCtr <= 1000 || numFail*8 > maxGoodStates)	//haven't narrowed it down to just a few bad states yet
	{
		Num = unsigned long int(floor(SciRandom.UniformMinMax(0.0, maxGoodStates)));	//this is still quicker than searching through all those other states!
		RndmCount[Num]++;
	}
	else
	{
		Num = unsigned long int(floor(SciRandom.UniformMinMax(0.0, numFail)));	//if 17 failed, will return 0-16
		for (unsigned long int i = 0; i < maxGoodStates; ++i)	//loop through all the states
		{
			if (HasFailed[i])	//found a potential state
			{
				if (Num > 0)	//not the bad state we're looking for
					--Num;	//so decrement the counter
				else //found it!
				{
					Num = i;	//this is the index
					break;	//now get out of the loop
				}
			}
		}
	}
	RandomCtr++;	//want 1000 pure random, and 50 targeted to failed places. Potentially improve convergence speed.
	return(Num);
}

bool MonteCarlo::SetFail(unsigned int index, bool val)
{
	if (index < HasFailed.size())
	{
		if (!HasFailed[index] && val)
			numFail++;
		else if (HasFailed[index] && !val)
			numFail--;
		HasFailed[index] = val;
	}
	else
		return(false);	//failed to set it!
	return(true);
}
bool MonteCarlo::ResetFail(unsigned int index)
{
	if (index < HasFailed.size())
	{
		if (HasFailed[index])
			numFail--;
		HasFailed[index] = false;
	}
	else
		return(false);	//failed to set it!
	return(true);
}
void MonteCarlo::ClearFail(void)
{
	unsigned int sz = HasFailed.size();
	HasFailed.clear();	//this shouldn't reallocate it - just change the size
	HasFailed.resize(sz, true);
	numFail = sz;
	return;
}
bool MonteCarlo::GetFail(unsigned int index)
{
	if (index < HasFailed.size())
		return(HasFailed[index]);

	return(true);	//if true, usually a flag that this shouldn't be done
}
bool MonteCarlo::HasAllFailed(void)
{
	return(numFail == HasFailed.size());
}

/*
In this method, it needs to adjust the primary state, but the other states around it need to also adjust for it to be effective. The surrounding environment
holds back a significant amount of change. To adjust a state accurately, it needs to look at the other states involved as if they also changed. THe maximum change
in the other states needs to be limited simultaneously by fermi level and charge transferred (think turning light on, same charge amount, but totally different fermi
level changes).

Can either look at all target states, and temporarily adjust each of their states appropriately based on what their individual net rates want.
Or you can adjust each state based on the net rate with that individual state. This would be like partial transient. Doesn't really work.
Finally, can adjust on a point basis. Target one point, adjust it and it's targeted neighbor points (make way for tunneling in the future).

This is not implemented. Going to see if I can adjust the transient intelligently first.
*/

int MonteCarlo::AdjustClusterState(unsigned long int index, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	ELevelLocal* eLev = &data.EStates[index];
	if (data.Pt[eLev->dataPt].ctcSink)	//don't let the contacts also change!
		return(0);

	double occ, avail;
	double origOcc = eLev->GetOcc();		//in case it fails!
	double origAvail = eLev->GetAvail();
	double origEf = eLev->curEf;
	bool tmp;	//just a dummy variable needed for CalcStateRate (did it find any rates to calculate?)
	double origRate = CalcStateRate(eLev, mdl, mdesc, BStructPts, tmp);;
	if (origRate == 0.0)
		return(0);	//0 means 'failed,' but we just don't want to recalculate everything!

	double tmpAdjust = SciRandom.GaussianAvg(ev_Adjust, ev_Adjust*0.5);	//modify it by an amount in the viciinity of the current scale
	//	do
	//	{
	if (origRate > 0.0)
	{
		eLev->UpdateOccVars(eLev->curEf + tmpAdjust, true);
		occ = eLev->GetOcc();
		avail = eLev->GetAvail();
	}
	else if (origRate < 0.0)
	{
		eLev->UpdateOccVars(eLev->curEf - tmpAdjust, true);
		occ = eLev->GetOcc();
		avail = eLev->GetAvail();
	}
	//		tmpAdjust = tmpAdjust * 2.0;	//when it gets really small adjustments, it might take FOREVER to get out of this loop unless the adjustments increase in size!
	//	} while (origOcc == occ && avail == origAvail);	//make sure it actually causes a change

	double newRate = CalcStateRate(eLev, mdl, mdesc, BStructPts, tmp);	//the calculation of the rate updates all the necessary variables!
	if (newRate <= 0.0 && origRate < 0.0 && newRate >= origRate)	//it stayed the same or got closer to 0, but did not extend past zero
		return(1);	//success
	if (newRate >= 0.0 && origRate > 0.0 && newRate <= origRate)
		return(1);

	//now to see if it succeeds! Determine NRAte=0 curEf (approximate)
	double difRate = newRate - origRate;	//guaranteed not to be zero
	double difEf = eLev->curEf - origEf;	//also guaranteed not to be zero
	//can actually just do ratios. We're looking for distance beyond curEf=0, which is guaranteed to have happened at this point

	/*
	|R_o / dR| = |Ef_o-Ef0| / |dEf|
	|R_n / dR| = |Ef_n-Ef0| / |dEf|
	dR = |R_o-R_n|
	dEf=|EF_o-Ef_n|
	Want |Ef_n-Ef0| = |R_n / (dR) * (Ef_o-Ef_n)|

	This is a bad acceptance criteria!

	double probCutoff = fabs(newRate / difRate * difEf);	//the energy excess
	probCutoff = exp(-probCutoff * KBI / eLev->ptr->getPoint().temp);
	double accept = SciRandom.Uniform01();	//random numnber

	//if the new rate were zero, always accept. ==> probCutOff=0 --> =1.0
	if(accept <= probCutoff)
	return(1);
	*/
	if (fabs(newRate) <= fabs(origRate))
		return(1);

	//FAILURE. Go back to original state
	eLev->UpdateOccVars(origEf, true);	//get the values back to original in data.Estates[]
	ELevel* srcE = eLev->ptr;
	Point* srcPt = &srcE->getPoint();
	if (eLev->OccMatches)
		srcE->SetBeginOccStates(eLev->occupancy);
	else
		srcE->SetBeginOccStatesOpposite(eLev->occupancy);
	srcE->TransferBeginToEnd();
	srcPt->ResetData();
	mdl.poissonRho[srcPt->adj.self] = srcPt->rho;
	double NBeta = -KBI / srcPt->temp;

	CalcScatterConsts(srcPt->scatterCBConst, srcPt->cb.energies, NBeta);
	CalcScatterConsts(srcPt->scatterVBConst, srcPt->vb.energies, NBeta);

	CalcPointBandFast(*srcPt, mdl);
	for (unsigned int i = 0; i < BStructPts.size(); ++i)
		CalcPointBandFast(*BStructPts[i], mdl);

	srcE->RateSums.Clear();	//it got a new rate in CalcStateRate
	//		srcE->RateSums.AddValue(NRate);

	failCtr++;
	BadCount[index]++;
	//		SetFail(index);
	//		SetRate(data.EStates[index], 0.0, index);	//make sure it can't randomly choose this failing state again
	return(0);	//indicate failure
}
int MonteCarlo::AdjustState(unsigned long int index, SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc)
{
	ELevelLocal* eLev = &data.EStates[index];
	if (data.Pt[eLev->dataPt].ctcSink)	//don't let the contacts also change!
		return(0);

	double occ, avail;
	double origOcc = eLev->GetOcc();		//in case it fails!
	double origAvail = eLev->GetAvail();
	double origEf = eLev->curEf;
	bool tmp;	//just a dummy variable needed for CalcStateRate (did it find any rates to calculate?)
	double origRate = CalcStateRate(eLev, mdl, mdesc, BStructPts,tmp);;
	if (origRate == 0.0)
		return(0);	//0 means 'failed,' but we just don't want to recalculate everything!

	double tmpAdjust = SciRandom.GaussianAvg(ev_Adjust*0.5, ev_Adjust*0.25);	//modify it by an amount in the viciinity of the current scale
	//need it to on average be less than ev_Adjust or it will never get good statistics!
//	do
//	{
		if (origRate > 0.0)
		{
			eLev->UpdateOccVars(eLev->curEf + tmpAdjust, true);
			occ = eLev->GetOcc();
			avail = eLev->GetAvail();
		}
		else if (origRate < 0.0)
		{
			eLev->UpdateOccVars(eLev->curEf - tmpAdjust, true);
			occ = eLev->GetOcc();
			avail = eLev->GetAvail();
		}
//		tmpAdjust = tmpAdjust * 2.0;	//when it gets really small adjustments, it might take FOREVER to get out of this loop unless the adjustments increase in size!
//	} while (origOcc == occ && avail == origAvail);	//make sure it actually causes a change

	double newRate = CalcStateRate(eLev, mdl, mdesc, BStructPts,tmp);	//the calculation of the rate updates all the necessary variables!
	if(newRate <= 0.0 && origRate < 0.0 && newRate >= origRate)	//it stayed the same or got closer to 0, but did not extend past zero
		return(1);	//success
	if(newRate >= 0.0 && origRate > 0.0 && newRate <= origRate)
		return(1);

	//now to see if it succeeds! Determine NRAte=0 curEf (approximate)
	double difRate = newRate - origRate;	//guaranteed not to be zero
	double difEf = eLev->curEf - origEf;	//also guaranteed not to be zero
	//can actually just do ratios. We're looking for distance beyond curEf=0, which is guaranteed to have happened at this point

	/*
	|R_o / dR| = |Ef_o-Ef0| / |dEf|
	|R_n / dR| = |Ef_n-Ef0| / |dEf|
	dR = |R_o-R_n|
	dEf=|EF_o-Ef_n|
	Want |Ef_n-Ef0| = |R_n / (dR) * (Ef_o-Ef_n)|

	This is a bad acceptance criteria!
	
	double probCutoff = fabs(newRate / difRate * difEf);	//the energy excess
	probCutoff = exp(-probCutoff * KBI / eLev->ptr->getPoint().temp);
	double accept = SciRandom.Uniform01();	//random numnber

	//if the new rate were zero, always accept. ==> probCutOff=0 --> =1.0
	if(accept <= probCutoff)
		return(1);
	*/
	if(fabs(newRate) <= fabs(origRate))
		return(1);

	//FAILURE. Go back to original state
	eLev->UpdateOccVars(origEf, true);	//get the values back to original in data.Estates[]
	ELevel* srcE = eLev->ptr;
	Point* srcPt = &srcE->getPoint();
	if (eLev->OccMatches)
		srcE->SetBeginOccStates(eLev->occupancy);
	else
		srcE->SetBeginOccStatesOpposite(eLev->occupancy);
	srcE->TransferBeginToEnd();
	srcPt->ResetData();
	mdl.poissonRho[srcPt->adj.self] = srcPt->rho;
	double NBeta = -KBI / srcPt->temp;

	CalcScatterConsts(srcPt->scatterCBConst, srcPt->cb.energies, NBeta);
	CalcScatterConsts(srcPt->scatterVBConst, srcPt->vb.energies, NBeta);

	CalcPointBandFast(*srcPt, mdl);
	for (unsigned int i = 0; i < BStructPts.size(); ++i)
		CalcPointBandFast(*BStructPts[i], mdl);

	srcE->RateSums.Clear();	//it got a new rate in CalcStateRate
	//		srcE->RateSums.AddValue(NRate);

	failCtr++;
	BadCount[index]++;
	//		SetFail(index);
	//		SetRate(data.EStates[index], 0.0, index);	//make sure it can't randomly choose this failing state again
	return(0);	//indicate failure
	
	//old code below
	/*  Nothing seems to ever change!

	double deltacharge;	//how much charge needs to be added into the system to correct it
	if (occ < avail)
		deltacharge = (eLev->carType == ELECTRON) ? occ - origOcc : origOcc - occ;
	else
		deltacharge = (eLev->carType == ELECTRON) ? origAvail - avail : avail - origAvail;

	if (deltacharge == 0.0)
		return(1);	//guess that's it!

	


	//the loop to try and get as much charge neutralized
	unsigned int attemptCtr = 0;
	while (deltacharge != 0.0 && attemptCtr <= data.EStates.size())	//most the states are irrelevant, so any changes might go unnoticed, resulting in the progress being undone
	{
		attemptCtr++;

		unsigned long int adjState;
		if (deltacharge > 0.0)
		{
			do
			{
				adjState = ChooseRandomState();
			} while (adjState != index);
			
	//		if (PosChargeRate.Choose(adjState) == false)
	//		{
	//			PosChargeRate.SetProbability(0.0, adjState);
	//			failCtr++;
	//			continue;
	//		}
			
			double trgOocc, trgOavail, trgOef;	//the original values
			trgOocc = data.EStates[adjState].GetOcc();
			trgOavail = data.EStates[adjState].GetAvail();
			trgOef = data.EStates[adjState].curEf;

			bool updateEvGood = true;	//default to say it's good
			if (data.EStates[adjState].carType == ELECTRON)	//we want to add holes... so decrease electrons
				data.EStates[adjState].UpdateOccVars(trgOef - ev_Adjust, true);
			else
				data.EStates[adjState].UpdateOccVars(trgOef + ev_Adjust, true);
			double oldTrgRate = data.EStates[adjState].ptr->RateSums.GetNetSum();
			double eVAdjRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);
			double evAdjOcc, evAdjAvail, evAdjEf;	//the  data for the eV adjustment stuff!
			evAdjOcc = data.EStates[adjState].GetOcc();
			evAdjAvail = data.EStates[adjState].GetAvail();
			evAdjEf = data.EStates[adjState].curEf;
			double evChargeChng;	//how much the charge changed from original to adjusted (orig + this) = adjusted
			if (data.EStates[adjState].carType == ELECTRON) //-(5 - 20) = +15 charge
				evChargeChng = (trgOocc < trgOavail) ? -(evAdjOcc - trgOocc) : evAdjAvail - trgOavail;
			else
				evChargeChng = (trgOocc < trgOavail) ? (evAdjOcc - trgOocc) : -(evAdjAvail - trgOavail);

			//evChargeChng > 0 && deltacharge > 0
			if (evChargeChng <= deltacharge && fabs(oldTrgRate) > fabs(eVAdjRate))
			{ 
				SetRate(data.EStates[adjState], eVAdjRate, adjState);
				deltacharge -= evChargeChng;	//delta is how much needs to be added. Chng was what was, so subtract it!
			}
			else //if it failed
			{
				if (evChargeChng > deltacharge)	//in this scenario, it passed. But it moved way too much charge
				{
					//this should automatically succeed. So let's just do it! It is moving less, and going in the right direction when it didn't go 'far enough'
					if (trgOocc < trgOavail)
					{
						if (data.EStates[adjState].carType == ELECTRON)	//trying to add positive charge
							data.EStates[adjState].SetOcc(trgOocc - deltacharge);
						else
							data.EStates[adjState].SetOcc(trgOocc + deltacharge);
					}
					else
					{
						if (data.EStates[adjState].carType == HOLE)	//trying to add positive charge
							data.EStates[adjState].SetAvail(trgOavail - deltacharge);	//so get rid of electrons
						else
							data.EStates[adjState].SetAvail(trgOavail + deltacharge);	//so add holes
					}
					double finishRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);	//this should also update all the stuff around it
//					if (fabs(finishRate) > fabs(origRate))
//						ProgressDouble(52, finishRate);

					SetRate(data.EStates[adjState], finishRate, adjState);
					deltacharge = 0.0;	//loop exit!

				}
				else
				{

					//so we're going to use the occupancies to try and find the "zero" rate
					double difRate = eVAdjRate - oldTrgRate;
					double delOcc = (trgOocc < trgOavail) ? evAdjOcc - trgOocc : trgOavail - evAdjAvail;	//Aocc - Oocc = (N-AA) - (N-OA) = OA-AA
					if (delOcc != 0.0 && difRate != 0.0)
					{
						double m = delOcc / difRate;	//inverse of slope of line between two
						double distOrig = -oldTrgRate*m;
						if (trgOocc < trgOavail)
							data.EStates[adjState].SetOcc(trgOocc + distOrig);
						else
							data.EStates[adjState].SetAvail(trgOavail - distOrig);
					}
					else if (data.EStates[adjState].carType == ELECTRON)	//the zeroing idea won't work, so just try a much smaller change
						data.EStates[adjState].UpdateOccVars(trgOef - ev_Adjust*0.01, true);
					else
						data.EStates[adjState].UpdateOccVars(trgOef + ev_Adjust*0.01, true);

					double RZeroRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);
					double RZeroOcc, RZeroAvail, RZeroEf;	//the  data for the eV adjustment stuff!
					RZeroOcc = data.EStates[adjState].GetOcc();
					RZeroAvail = data.EStates[adjState].GetAvail();
					RZeroEf = data.EStates[adjState].curEf;
					double RZeroChargeChng;
					if (data.EStates[adjState].carType == ELECTRON)
						RZeroChargeChng = (trgOocc < trgOavail) ? -(RZeroOcc - trgOocc) : RZeroAvail - trgOavail;
					else
						RZeroChargeChng = (trgOocc < trgOavail) ? (RZeroOcc - trgOocc) : -(RZeroAvail - trgOavail);


					if (RZeroChargeChng < deltacharge && fabs(oldTrgRate) > fabs(RZeroRate) && RZeroChargeChng > 0.0)
					{ //don't need to do this one again!
						SetRate(data.EStates[adjState], RZeroRate, adjState);
						deltacharge -= RZeroChargeChng;
					}
					else
					{
						//it completely failed. Reset the state to the original values
						data.EStates[adjState].UpdateOccVars(trgOef, true);
						ELevel* trgE = data.EStates[adjState].ptr;
						Point* trgPt = trgE->getPoint();
						if (data.EStates[adjState].OccMatches)
							trgE->SetBeginOccStates(data.EStates[adjState].occupancy);
						else
							trgE->SetBeginOccStatesOpposite(data.EStates[adjState].occupancy);
						trgE->TransferBeginToEnd();
						trgPt->ResetData();
						mdl.poissonRho[trgPt->adj.self] = trgPt->rho;
						double NBeta = -KBI / trgPt->temp;

						CalcScatterConsts(trgPt->scatterCBConst, trgPt->cb.energies, NBeta);
						CalcScatterConsts(trgPt->scatterVBConst, trgPt->vb.energies, NBeta);

						CalcPointBandFast(*trgPt, mdl);
						for (unsigned int i = 0; i < BStructPts.size(); ++i)
							CalcPointBandFast(*BStructPts[i], mdl);

						trgE->RateSums.Clear();	//it got a new rate in CalcStateRate
						trgE->RateSums.AddValue(oldTrgRate);

						//don't let it try this state again!
						SetFail(adjState);
						SetRate(data.EStates[adjState], 0.0, adjState);
					}	//end bad round 3
				}	//end bad round 2
			}	//end bad round 1
		}	//end positive charge entering
		else if (deltacharge < 0.0)
		{
			do
			{
				adjState = ChooseRandomState();
			} while (adjState != index);
			
	//		if (NegChargeRate.Choose(adjState) == false)
	//		{
	//			NegChargeRate.SetProbability(0.0, adjState);
	//			failCtr++;
	//			continue;
	//		}
			
			double trgOocc, trgOavail, trgOef;	//the original values
			trgOocc = data.EStates[adjState].GetOcc();
			trgOavail = data.EStates[adjState].GetAvail();
			trgOef = data.EStates[adjState].curEf;

			bool updateEvGood = true;	//default to say it's good
			if (data.EStates[adjState].carType == ELECTRON)	//we want to add electrons... so increase electrons
				data.EStates[adjState].UpdateOccVars(trgOef + ev_Adjust, true);
			else
				data.EStates[adjState].UpdateOccVars(trgOef - ev_Adjust, true);
			double oldTrgRate = data.EStates[adjState].ptr->RateSums.GetNetSum();
			double eVAdjRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);
			double evAdjOcc, evAdjAvail, evAdjEf;	//the  data for the eV adjustment stuff!
			evAdjOcc = data.EStates[adjState].GetOcc();
			evAdjAvail = data.EStates[adjState].GetAvail();
			evAdjEf = data.EStates[adjState].curEf;
			double evChargeChng;	//how much the actual charge changed?
			if (data.EStates[adjState].carType == ELECTRON)	// -(20 - 5) = -15
				evChargeChng = (trgOocc < trgOavail) ? -(evAdjOcc - trgOocc) : evAdjAvail - trgOavail;
			else
				evChargeChng = (trgOocc < trgOavail) ? (evAdjOcc - trgOocc) : -(evAdjAvail - trgOavail);

			//evChargeChng < 0 and deltacharge < 0, so magnitude is less!
			if (evChargeChng > deltacharge && fabs(oldTrgRate) > fabs(eVAdjRate))
			{ 
				SetRate(data.EStates[adjState], eVAdjRate, adjState);
				deltacharge -= evChargeChng;
			}
			else //if it failed
			{
				if (evChargeChng < deltacharge)	//in this scenario, it passed. But it moved way too much charge
				{
					//this should automatically succeed. So let's just do it! It is moving less, and going in the right direction when it didn't go 'far enough'
					if (trgOocc < trgOavail)
					{
						if (data.EStates[adjState].carType == ELECTRON)	//trying to add negative charge
							data.EStates[adjState].SetOcc(trgOocc - deltacharge);	//delta charge is negative
						else
							data.EStates[adjState].SetOcc(trgOocc + deltacharge);
					}
					else
					{
						if (data.EStates[adjState].carType == HOLE)	//trying to add negative charge
							data.EStates[adjState].SetAvail(trgOavail - deltacharge);	//so add electrons (dcharge < 0)
						else
							data.EStates[adjState].SetAvail(trgOavail + deltacharge);	//so remove holes
					}
					double finishRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);	//this should also update all the stuff around it
//					if (fabs(finishRate) > fabs(origRate))
//						ProgressDouble(52, finishRate);

					SetRate(data.EStates[adjState], finishRate, adjState);
					deltacharge = 0.0;	//loop exit!

				}
				else
				{

					//so we're going to use the occupancies to try and find the "zero" rate
					double difRate = eVAdjRate - oldTrgRate;
					double delOcc = (trgOocc < trgOavail) ? evAdjOcc - trgOocc : trgOavail - evAdjAvail;	//Aocc - Oocc = (N-AA) - (N-OA) = OA-AA
					if (delOcc != 0.0 && difRate != 0.0)
					{
						double m = delOcc / difRate;	//inverse of slope of line between two
						double distOrig = -oldTrgRate*m;
						if (trgOocc < trgOavail)
							data.EStates[adjState].SetOcc(trgOocc + distOrig);
						else
							data.EStates[adjState].SetAvail(trgOavail - distOrig);
					}
					else if (data.EStates[adjState].carType == ELECTRON)	//the zeroing idea won't work, so just try a much smaller change. adding e-
						data.EStates[adjState].UpdateOccVars(trgOef + ev_Adjust*0.01, true);
					else
						data.EStates[adjState].UpdateOccVars(trgOef - ev_Adjust*0.01, true);

					double RZeroRate = CalcStateRate(&data.EStates[adjState], mdl, mdesc);
					double RZeroOcc, RZeroAvail, RZeroEf;	//the  data for the eV adjustment stuff!
					RZeroOcc = data.EStates[adjState].GetOcc();
					RZeroAvail = data.EStates[adjState].GetAvail();
					RZeroEf = data.EStates[adjState].curEf;
					double RZeroChargeChng;
					if (data.EStates[adjState].carType == ELECTRON)
						RZeroChargeChng = (trgOocc < trgOavail) ? -(RZeroOcc - trgOocc) : RZeroAvail - trgOavail;
					else
						RZeroChargeChng = (trgOocc < trgOavail) ? (RZeroOcc - trgOocc) : -(RZeroAvail - trgOavail);

					//dcharge < 0, RZeroCharge < 0 hopefully
					if (RZeroChargeChng > deltacharge && fabs(oldTrgRate) > fabs(RZeroRate) && RZeroChargeChng < 0.0)
					{ //don't need to do this one again!
						SetRate(data.EStates[adjState], RZeroRate, adjState);
						deltacharge -= RZeroChargeChng;
					}
					else
					{
						//it completely failed. Reset the state to the original values
						data.EStates[adjState].UpdateOccVars(trgOef, true);
						ELevel* trgE = data.EStates[adjState].ptr;
						Point* trgPt = trgE->getPoint();
						if (data.EStates[adjState].OccMatches)
							trgE->SetBeginOccStates(data.EStates[adjState].occupancy);
						else
							trgE->SetBeginOccStatesOpposite(data.EStates[adjState].occupancy);
						trgE->TransferBeginToEnd();
						trgPt->ResetData();
						mdl.poissonRho[trgPt->adj.self] = trgPt->rho;
						double NBeta = -KBI / trgPt->temp;

						CalcScatterConsts(trgPt->scatterCBConst, trgPt->cb.energies, NBeta);
						CalcScatterConsts(trgPt->scatterVBConst, trgPt->vb.energies, NBeta);

						CalcPointBandFast(*trgPt, mdl);
						for (unsigned int i = 0; i < BStructPts.size(); ++i)
							CalcPointBandFast(*BStructPts[i], mdl);

						trgE->RateSums.Clear();	//it got a new rate in CalcStateRate
						trgE->RateSums.AddValue(oldTrgRate);

						//don't let it try this state again!
						SetFail(adjState);
						SetRate(data.EStates[adjState], 0.0, adjState);
					}	//end bad round 3
				}	//end bad round 2
			}	//end bad round 1
		}	//end deltaCharge < 0
	} //end while deltacharge != 0.0 or it just kep failing

	//then we need to make it less of a move to avoid charge creation/destruciton causing the thing to go bonkers if it failed too many times
	//it wants to add more holes to keep the charge neutral
	
//time to see if this is the line killing everything...	
//	data.EStates[index].AddHole(deltacharge);	//if zero, this will have no change. If negative, it will remove holes, so same as adding electrons.
	//AddHole is essentially the same as AddCharge.

	*/
}

double CalcStateRate(ELevelLocal* eLev, Model& mdl, ModelDescribe& mdesc, std::vector<Point*>& BStructPts, bool& hadRate, PosNegSum* accuRate, bool includeLight, bool includeContacts, bool preInitialized)
{

	//general assumptions - light stays the same! This is a state that changed, and everything corresponding to it needs to be updated
	ELevel* srcE = eLev->ptr;
	Point* srcPt = &srcE->getPoint();
	double NRateIn = 0.0;
	srcE->RateSums.Clear();
	if (accuRate)
		accuRate->Clear();

//	eLev->UpdateModelOccupancy(MC_OCC_BEGIN);	//doesn't necessarily have curEf updated
	if (!preInitialized)
	{
		if (eLev->OccMatches)
			srcE->SetBeginOccStates(eLev->occupancy);
		else
			srcE->SetBeginOccStatesOpposite(eLev->occupancy);
		srcE->TransferBeginToEnd();
		srcPt->ResetData();
		mdl.poissonRho[srcPt->adj.self] = srcPt->rho;
		double NBeta = -KBI / srcPt->temp;

		CalcScatterConsts(srcPt->scatterCBConst, srcPt->cb.energies, NBeta);
		CalcScatterConsts(srcPt->scatterVBConst, srcPt->vb.energies, NBeta);
		//update band structure of self & neighbors
		CalcPointBandFast(*srcPt, mdl);
	}
	
	hadRate=false;
	
	for (unsigned int trg = 0; trg < srcE->TargetELev.size(); ++trg)	//all the scattering/recomb/srh rates
	{
		if (CalcFastRate(srcE, srcE->TargetELev[trg], mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false, accuRate) == true)	//returns true if calculated rate
		{
			if(accuRate == NULL && fabs(NRateIn) > 1.0e-250)	//just ignore all the tiny stuff...
				srcE->RateSums.AddValue(NRateIn);
			hadRate=true;	//if it adds a rate of 0, RateSums won't adjust. Should still say it calculated a rate though!
		}
	}

	BStructPts.clear();
	for (unsigned int trg = 0; trg < srcE->TargetELevDD.size(); ++trg)	//all the Drift-diffusion rates
	{
		if (includeContacts)
		{
			bool update = true;
			for (unsigned int pt = 0; pt < BStructPts.size(); pt++)
			{
				if (&srcE->TargetELevDD[trg].target->getPoint() == BStructPts[pt])
				{
					update = false;
					break;
				}
			}
			if (update)
			{
				BStructPts.push_back(&srcE->TargetELevDD[trg].target->getPoint());
				if (!preInitialized)
					CalcPointBandFast(*BStructPts.back(), mdl);
				if (srcE->TargetELevDD[trg].target->getPoint().contactData)	//only want to do this once per point!
				{
					NRateIn = srcE->RateSums.GetNetSum();
					ProcessContactSteadyState(mdl, srcE->TargetELevDD[trg].target->getPoint().contactData, mdesc.simulation, srcE);

					if (srcE->RateSums.ChangedSinceNetSum())
						hadRate = true;
				}
			}
		}
		if (CalcFastRate(srcE, srcE->TargetELevDD[trg].target, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, NRateIn, false, accuRate) == true)	//returns true if calculated rate
		{
			if (!accuRate  && fabs(NRateIn) > 1.0e-250)
				srcE->RateSums.AddValue(NRateIn);
			hadRate=true;
		}

		
	}
	NRateIn = srcE->RateSums.GetNetSum();

	if (includeLight)
		Light(mdl, mdesc, srcPt);
	if (includeContacts && srcPt->contactData)
		ProcessContactSteadyState(mdl, srcPt->contactData, mdesc.simulation, srcE);
	if(srcE->RateSums.ChangedSinceNetSum())
	{
		NRateIn = srcE->RateSums.GetNetSum();
		hadRate = true;
	}

	if(accuRate)
	{
		*accuRate += srcE->RateSums;
		NRateIn = accuRate->GetNetSum();
	}

	srcE->Rates.clear();
	srcE->OpticalRates.clear();

	return(NRateIn);
}

ELevelLocal::~ELevelLocal()
{
	NeighborEquivs.clear();
	ptr = NULL;
	NRates.Clear();
	DerivSOnly.Clear();
}

int MonteCarlo::SetRate(ELevelLocal& eLev, double nRate, unsigned long int eIndex)
{
	if (nRate == 0.0)
	{
		AbsNetRate.SetProbability(0.0, eIndex);	//this will remove it from the absnetrate
		PosChargeRate.SetProbability(0.0, eIndex);	//this will remove it from the pFn
		NegChargeRate.SetProbability(0.0, eIndex);	//this will remove it from the pFn
	}
	else if (nRate > 0.0)
	{
		if (eLev.carType == ELECTRON)	//net rate in of electrons
		{
			NegChargeRate.SetProbability(nRate, eIndex);
			PosChargeRate.SetProbability(0.0, eIndex);	//clear from the positive list
		}
		else //net rate in of holes
		{
			PosChargeRate.SetProbability(nRate, eIndex);
			NegChargeRate.SetProbability(0.0, eIndex);
		}
		AbsNetRate.SetProbability(nRate, eIndex);
	}
	else //nrate carriers leaving
	{
		if (eLev.carType == ELECTRON)	//net rate out of electrons
		{
			PosChargeRate.SetProbability(-nRate, eIndex);	//keep all the rate probabilities positive
			NegChargeRate.SetProbability(0.0, eIndex);	//clear from the positive list
		}
		else //net rate out of holes
		{
			NegChargeRate.SetProbability(-nRate, eIndex);
			PosChargeRate.SetProbability(0.0, eIndex);
		}
		AbsNetRate.SetProbability(-nRate, eIndex);
	}
	return(0);
}

MonteCarlo::MonteCarlo()
{
	ev_Adjust = MC_EV_INIT;
	EfCtr = NULL;
	EfTracker = NULL;
	NumGoodStates = 0;
	maxGoodStates = 0;
	adjustedAccuracy = 1.0;
	ClearProbabilities();
	simdata = NULL;
	avgEf = NULL;
	okMove = NULL;
	ProgBar = false;
}

MonteCarlo::MonteCarlo(unsigned int sz, SS_SimData* dat, double adjAcc)
{

	ev_Adjust = dat->GetCalcInitAccuracy();
	ClearProbabilities();
	AbsNetRate.SetSize(sz);
	PosChargeRate.SetSize(sz);
	NegChargeRate.SetSize(sz);
	HasFailed.resize(sz, false);
	numFail = 0;
	BStructPts.clear();
	FailReduceTolerance = sz * MC_FAILMULTIPLIER;	//it should surely hit most at some point by this many iterations!
	RndmCount.resize(sz, 0);
	BadCount.resize(sz, 0);
	EfCtr = new unsigned int[sz];
	avgEf = new double[sz];
	EfTracker = new double[sz * dat->NumTrack];
	okMove = new bool[sz];
	maxGoodStates = sz;
	NumGoodStates = 0;
	simdata = dat;
	readyStates = 0;
	ProgBar = false;
	if (dat->GetCalcEndAccuracy() <= 0)
		adjustedAccuracy = (adjAcc > 0.0 && adjAcc <= 1.0) ? adjAcc : 1.0;
	else
		adjustedAccuracy = 1.0;
	for(unsigned int i=0; i<sz; ++i)
	{
		EfCtr[i] = 0;
		for(unsigned int j=0; j<simdata->NumTrack; ++j)
		{
			EfTracker[i*simdata->NumTrack + j] = 0.0;	//guarantee that it will fail the initial tests!
		}
		okMove[i] = false;
		avgEf[i] = 0.0;
	}

}


MonteCarlo::~MonteCarlo()
{
	ClearProbabilities();
	BStructPts.clear();
	HasFailed.clear();
	RndmCount.clear();
	BadCount.clear();
	delete [] EfCtr;
	delete [] EfTracker;
	delete [] avgEf;
	delete [] okMove;
	NumGoodStates = 0;
	maxGoodStates = 0;
	simdata = NULL;	//data is actually a part of Simulation class
}

int MonteCarlo::RecordEf(unsigned int stateIndex, double Ef)
{
	if(stateIndex >= maxGoodStates)	//not in array
		return(0);
	
	//determine if the value was previously in a state to pass!
	double min = EfTracker[stateIndex*simdata->NumTrack];
	double max = min;
	for(unsigned int i=stateIndex*simdata->NumTrack, j=0; j<simdata->NumTrack; ++j, ++i)
	{
		if(EfTracker[i] < min)
			min = EfTracker[i];
		if(EfTracker[i] > max)
			max = EfTracker[i];
	}
	bool good = true;
	if(max - avgEf[stateIndex] > ev_Adjust)
		good = false;
	if(avgEf[stateIndex] - min > ev_Adjust)
		good = false;

	//update the current Ef and the average
	avgEf[stateIndex] = (avgEf[stateIndex] * double(simdata->NumTrack) - EfTracker[stateIndex*simdata->NumTrack + EfCtr[stateIndex]] + Ef) / double(simdata->NumTrack);
	EfTracker[stateIndex*simdata->NumTrack + EfCtr[stateIndex]] = Ef;
	EfCtr[stateIndex] += 1;
	if(EfCtr[stateIndex] >= simdata->NumTrack)	//has this state at least been sampled X times?
	{
		EfCtr[stateIndex] = 0;
		if(okMove[stateIndex]==false)
			readyStates++;
		okMove[stateIndex] = true;
	}

	min = EfTracker[stateIndex*simdata->NumTrack];
	max = min;
	for(unsigned int i=stateIndex*simdata->NumTrack, j=0; j<simdata->NumTrack; ++j, ++i)
	{
		if(EfTracker[i] < min)
			min = EfTracker[i];
		if(EfTracker[i] > max)
			max = EfTracker[i];
	}
	bool good2 = true;
	if(max - avgEf[stateIndex] > ev_Adjust)
		good2 = false;
	if(avgEf[stateIndex] - min > ev_Adjust)
		good2 = false;

	if(okMove[stateIndex])	//only do if there are eonugh statistics.
	{
		if(good2 && !good)	//good2: currently a good state. good: was a good state beforehand.
			NumGoodStates++;	//so only increase if it is good now, but wasn't beforehand.
		else if(!good2 && good)
			NumGoodStates--;

		if (ProgBar==false)	//just trying to get full on okMove
			SetFail(stateIndex, false);	//if it's in okMove, it hasn't failed
		else
			SetFail(stateIndex, !good2);
	}
	else
	{
//		if (ProgBar == false)	//trying to just get enough statistics
		//if(ProgBar==true) will never happen, because for ProgBar=true, all of okMove must be true.
		SetFail(stateIndex, !okMove[stateIndex]);	//pretty much everything will have failed early on
	}

	return(1);
}

double MonteCarlo::GetStateEf(unsigned int index)
{
	if(index >= maxGoodStates)
		return(0.0);

	return(avgEf[index]);
}

void MonteCarlo::ClearProbabilities(int which)
{
	if (which & MC_CLEAR_ABS)
		AbsNetRate.Clear();
	if (which & MC_CLEAR_POS)
		PosChargeRate.Clear();
	if (which & MC_CLEAR_NEG)
		NegChargeRate.Clear();
	return;
}


int ELevelLocal::UpdateModelOccupancy(int which)
{
	if (!ptr)
		return(0);

	UpdateOccVars();	//make sure the data is good with the current Ef

	if (OccMatches)
	{
		if (which & MC_OCC_BEGIN)
			ptr->SetBeginOccStates(occupancy);
		if (which & MC_OCC_END)
			ptr->SetEndOccStates(occupancy);
	}
	else
	{
		if (which & MC_OCC_BEGIN)
			ptr->SetBeginOccStatesOpposite(occupancy);
		if (which & MC_OCC_END)
			ptr->SetEndOccStatesOpposite(occupancy);
	}

	return(1);
}


int ELevelLocal::UpdateOccupancyFromModel(int which)
{
	if (!ptr)
		return(0);

	double occ, avail;
	ptr->GetOccupancyOcc(which, occ, avail);

	if (occ < avail)
		SetOcc(occ);
	else
		SetAvail(avail);

	return(1);
}

int ELevelLocal::UpdateOccVars(double Ef, bool UseArgEf)
{
	if(UseArgEf)
		curEf = Ef;
	if (EfUpdatedOcc != curEf)	//only update occupancy if it is not at the past value.
	{
		double temp = ptr->getPoint().temp;
		double kTi = temp > 0.0 ? KBI / temp : 0.0;
		if (ptr->imp)
			occupancy = OccAvailFromRelEf(curEf, NStates, kTi, ptr->max - ptr->min, ptr->imp->getDegeneracy());
		else
			occupancy = OccAvailFromRelEf(curEf, NStates, kTi, ptr->max - ptr->min);

		if (curEf >= 0.0) //it is mostly full of carriers, so occupancy holds the smaller value: avail
			OccMatches = false;
		else //typically true - curEf < 0, so the occupancy is smaller and that's what gets stored in occupancy
			OccMatches = true;

		EfUpdatedOcc = curEf;
	}
	
	return(0);
}
double ELevelLocal::GetOcc(void)
{
	if(EfUpdatedOcc != curEf)
		UpdateOccVars();

	if(OccMatches)
		return(occupancy);
	return(NStates-occupancy);
}

double ELevelLocal::GetAvail(void)
{
	if(EfUpdatedOcc != curEf)
		UpdateOccVars();

	if(!OccMatches)
		return(occupancy);
	return(NStates-occupancy);
}

double ELevelLocal::GetElec(void)
{
	if(EfUpdatedOcc != curEf)
		UpdateOccVars();

	if(OccMatches)
	{
		if(carType == ELECTRON)
			return(occupancy);
		else
			return(NStates - occupancy);
	}
	else //the opposite
	{
		if(carType == HOLE)
			return(occupancy);
		else
			return(NStates-occupancy);
	}
}
double ELevelLocal::GetHole(void)
{
	if(EfUpdatedOcc != curEf)
		UpdateOccVars();

	if(OccMatches)
	{
		if(carType == HOLE)
			return(occupancy);
		else
			return(NStates - occupancy);
	}
	else //iit is the opposite
	{
		if(carType == ELECTRON)
			return(occupancy);
		else
			return(NStates-occupancy);
	}
}
int ELevelLocal::SetOcc(double val)
{
	double occ = val;
	double avail = NStates - val;

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}
int ELevelLocal::SetAvail(double val)
{
	double avail = val;
	double occ = NStates - val;

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
	return(0);
}
int ELevelLocal::SetElec(double val)
{
	double occ;
	double avail;
	if(carType == ELECTRON)
	{
		occ = val;
		avail = NStates - val;
	}
	else
	{
		avail = val;
		occ = NStates - val;
	}

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}
int ELevelLocal::SetHole(double val)
{
	double occ;
	double avail;
	if(carType == HOLE)
	{
		occ = val;
		avail = NStates - val;
	}
	else
	{
		avail = val;
		occ = NStates - val;
	}

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}

int ELevelLocal::AddElec(double val)
{
	double occ;
	double avail;
	if(carType == ELECTRON)
	{
		if(OccMatches)
		{
			occ = occupancy + val;
			avail = NStates - occ;
		}
		else //occupancy is holes, and holes are being taken away
		{
			avail = occupancy - val;
			occ = NStates - avail;
		}
		
	}
	else //the type is holes
	{
		if(OccMatches)
		{ //and occupancy is holes
			occ = occupancy - val;
			avail = NStates - occ;
		}
		else //occupancy is electrons, and e- are being added
		{
			avail = occupancy + val;
			occ = NStates - avail;
		}
	}

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}

int ELevelLocal::AddHole(double val)
{
	double occ;
	double avail;
	if(carType == HOLE)
	{
		if(OccMatches)
		{
			occ = occupancy + val;
			avail = NStates - occ;
		}
		else //occupancy is e-, and e- are being taken away
		{
			avail = occupancy - val;
			occ = NStates - avail;
		}
		
	}
	else //the type is e-
	{
		if(OccMatches)
		{ //and occupancy is e-
			occ = occupancy - val;
			avail = NStates - occ;
		}
		else //occupancy is h+, and h+ are being added
		{
			avail = occupancy + val;
			occ = NStates - avail;
		}
	}

	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}

int ELevelLocal::AddOcc(double val)
{
	double occ;
	double avail;
	if (val == 0.0)
		return(0);

	if(OccMatches)
	{
		occ = occupancy + val;
		if(occ > NStates)
			occ = NStates;
		if(occ < 0)
			occ = 0;
		avail = NStates - occ;
	}
	else
	{
		avail = occupancy - val;
		if(avail > NStates)
			avail = NStates;
		if(avail < 0)
			avail = 0;
		occ = NStates - avail;
	}
	
	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}

int ELevelLocal::AddAvail(double val)
{
	double occ;
	double avail;
	
	if(OccMatches)
	{
		occ = occupancy - val;
		if(occ > NStates)
			occ = NStates;
		if(occ < 0)
			occ = 0;
		avail = NStates - occ;
	}
	else //occupancy is holes, and holes are being taken away
	{
		avail = occupancy + val;
		if(avail > NStates)
			avail = NStates;
		if(avail < 0)
			avail = 0;
		occ = NStates - avail;
	}
	
	double kT = ptr->getPoint().temp * KB;
	float degen = (ptr->imp) ? ptr->imp->getDegeneracy() : 1.0;
	curEf = RelEfFromOcc(occ, avail, ptr->max - ptr->min, kT, degen);
	EfUpdatedOcc = curEf;
	if(curEf >= 0.0)
	{
		OccMatches=false;
		occupancy = avail;
	}
	else
	{
		OccMatches = true;
		occupancy = occ;
	}
	return(0);
}

double ELevelLocal::GetCharge(bool includeFixed)
{
	double charge = 0.0;
	if(EfUpdatedOcc != curEf)
		UpdateOccVars();

	if(includeFixed)
	{
		if(ptr->imp)
			charge += NStates * double(ptr->charge);
	}
	if(carType == ELECTRON)
		charge -= GetElec();
	else
		charge += GetHole();

	return(charge);
}

SSContactData::SSContactData()
{
	Clear();
}

SSContactData::SSContactData(Contact* ct) : SSContactData()
{
	ctc = ct;
	if (ctc)
		ctcID = ctc->id;
}

bool SSContactData::MergeSame(SSContactData* other, bool replace)
{
	if (other == nullptr)
		return false;
	if (other == this)
		return false;	//can't merge with self

	if (other->ctcID != ctcID)	//can't merge if they go to different contacts!
		return false;

	if (TEST_BIT(other->currentActiveValue, CONTACT_VOLT_LINEAR)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_VOLT_LINEAR) == false) { //is the slot available or wanting to replace?
			voltage = other->voltage;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_EFIELD_LINEAR)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_EFIELD_LINEAR) == false) { //is the slot available or wanting to replace?
			eField = other->eField;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_CURRENT_LINEAR)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_CURRENT_LINEAR) == false) { //is the slot available or wanting to replace?
			current = other->current;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_CUR_DENSITY_LINEAR) == false) { //is the slot available or wanting to replace?
			currentDensity = other->currentDensity;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_EFIELD_LINEAR_ED)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_EFIELD_LINEAR_ED) == false) { //is the slot available or wanting to replace?
			eField_ED = other->eField_ED;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_CURRENT_LINEAR_ED)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_CURRENT_LINEAR_ED) == false) { //is the slot available or wanting to replace?
			current_ED = other->current_ED;
		}
	}
	if (TEST_BIT(other->currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED)) {	//does it want to put in the voltage?
		if (replace || TEST_BIT(currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED) == false) { //is the slot available or wanting to replace?
			currentDensity_ED = other->currentDensity_ED;
		}
	}

	currentActiveValue |= other->currentActiveValue;

	if (TEST_BIT(currentActiveValue, CONTACT_SINK) && TEST_BIT(currentActiveValue, CONTACT_CONNECT_EXTERNAL))
	{
		//both these are set!
		if (replace) //preference to the other
		{
			if (TEST_BIT(other->currentActiveValue, CONTACT_SINK))
				currentActiveValue &= (~CONTACT_CONNECT_EXTERNAL);
			else //the other must have put in connect external
				currentActiveValue &= (~CONTACT_SINK);
		}
		else //try to keep what was originally there
		{
			if (TEST_BIT(other->currentActiveValue, CONTACT_SINK))
				currentActiveValue &= (~CONTACT_SINK);
			else //the other must have put in connect external
				currentActiveValue &= (~CONTACT_CONNECT_EXTERNAL);
		}
	}

	return true;
}

double SSContactData::GetValue(int flag)
{
	if (TEST_BIT(flag, CONTACT_VOLT_LINEAR))
		return voltage;
	if (TEST_BIT(flag, CONTACT_CURRENT_LINEAR))
		return current;
	if (TEST_BIT(flag, CONTACT_CUR_DENSITY_LINEAR))
		return currentDensity;
	if (TEST_BIT(flag, CONTACT_EFIELD_LINEAR))
		return eField;
	if (TEST_BIT(flag, CONTACT_CURRENT_LINEAR_ED))
		return current_ED;
	if (TEST_BIT(flag, CONTACT_EFIELD_LINEAR_ED))
		return eField_ED;
	if (TEST_BIT(flag, CONTACT_CUR_DENSITY_LINEAR_ED))
		return currentDensity_ED;
	if (TEST_BIT(flag, CONTACT_FLOAT_EFIELD_ESCAPE))
		return eField_ED_Leak;
	if (TEST_BIT(flag, CONTACT_FLOAT_EFIELD_ESCAPE))
		return current_ED_Leak;
	if (TEST_BIT(flag, CONTACT_FLOAT_EFIELD_ESCAPE))
		return currentD_ED_Leak;
	return(0.0);
}

void SSContactData::Clear(void)
{
	ctc = NULL;
	ctcID = -1;
	voltage = current = currentDensity = eField = current_ED = eField_ED = currentDensity_ED = eField_ED_Leak = current_ED_Leak = currentD_ED_Leak = 0.0;
	suppressCurrentPoisson = suppressEFieldPoisson = suppressVoltagePoisson = suppressEFMinPoisson = suppressOppD = suppressConserveJ = false;
	currentActiveValue = CONTACT_INACTIVE;
	return;
}

void SSContactData::Validate(Simulation& sim)
{
	ctc = sim.getContact(ctcID);
}

double SS_SimData::InitIteration(Model& mdl, ModelDescribe& mdesc)	//set the time, set the band structure, clear out intermediate variables.
{
		
	return(CalcBandStruct(mdl, mdesc));	//returns the total of how much the band structure changed
}

SS_SimData::SS_SimData(Simulation *s) : sim(s)
{
	CurrentData = 0;	//don't want it to be zero!  THat way it won't match curiter on first calculation
	LastBDUpdated = -1;	//don't want it same as currentData by default when comparing it to CurrentData
	steadyStateClampReduction = SS_EF_ALTER;
	VoltCurrentScaleFactor = LightRateFactor = LightEmissionFactor = TunnellingFactor = 1.0;
	ExternalStates.clear();
//	OutFiles.clear();
	NumTrack = MC_NUMTRACK_MIN;
	PercentGood = MC_PERCENTGOOD;
	CalculationOrder.clear();

}

void SS_SimData::BreakEnvironmentLoops() {
	std::vector<int> GoodEnvironments;
	std::vector<int> LinkedEnvironment;
	int ssSz = ExternalStates.size();
	for (unsigned int i = 0; i<ssSz; ++i)	{
		LinkedEnvironment.clear();
		if (ExternalStates[i]->reusePrevContactEnvironment < 0 || ExternalStates[i]->reusePrevContactEnvironment >= ssSz) {
			GoodEnvironments.push_back(i);
			ExternalStates[i]->reusePrevContactEnvironment = -1;	//just a default bad value so it's consistent
		}
		else {
			int curLink = i;
			bool keepGoing = true;
			do {
				curLink = ExternalStates[curLink]->reusePrevContactEnvironment;
				if (curLink < 0 || curLink >= ssSz) { //then it ends well
					GoodEnvironments.push_back(i);
					keepGoing = false;
				}
				else { //we don't want this to form an infinite loop of links
					for (unsigned int link = 0; link<LinkedEnvironment.size(); ++link) {
						if (curLink == LinkedEnvironment[link]) { //we have an infinite loop
							//so choose an environment that at least has data, preferably
							for (unsigned int l2 = 0; l2<LinkedEnvironment.size(); ++l2) {
								if (ExternalStates[LinkedEnvironment[l2]]->ContactEnvironment.size() > 0) {
									ExternalStates[LinkedEnvironment[l2]]->reusePrevContactEnvironment = -1;
									keepGoing = false;
								}
							}
							//that validate data will fill in with 0's
							if (keepGoing) //there was no 'good' data set to choose from
								ExternalStates[curLink]->reusePrevContactEnvironment = -1;

							GoodEnvironments.push_back(i);
							//the main contact has now been labelled as OK because it eventually
							//gets to one that will not reference others
							keepGoing = false;
						}
					}
					LinkedEnvironment.push_back(curLink);
				}
			} while (keepGoing);
		}
	}
	GoodEnvironments.clear();

	//an exact copy of the above code, except now it's replace reusePrevContact with reusePrevLight
	for (unsigned int i = 0; i<ExternalStates.size(); ++i)
	{
		LinkedEnvironment.clear();
		if (ExternalStates[i]->reusePrevLightEnvironment < 0 || ExternalStates[i]->reusePrevLightEnvironment >= ssSz) {
			GoodEnvironments.push_back(i);
			ExternalStates[i]->reusePrevLightEnvironment = -1;	//just a default bad value so it's consistent
		}
		else {
			int curLink = i;
			bool keepGoing = true;
			do {
				curLink = ExternalStates[curLink]->reusePrevLightEnvironment;
				if (curLink < 0 || curLink >= ssSz) {
					//then it ends well
					GoodEnvironments.push_back(i);
					keepGoing = false;
				}
				else {
					//we don't want this to form an infinite loop of links
					for (unsigned int link = 0; link<LinkedEnvironment.size(); ++link) {
						if (curLink == LinkedEnvironment[link]) {
							for (unsigned int l2 = 0; l2<LinkedEnvironment.size(); ++l2) {
								if (ExternalStates[LinkedEnvironment[l2]]->ActiveLightEnvironment.size() > 0) {
									ExternalStates[LinkedEnvironment[l2]]->reusePrevLightEnvironment = -1;
									keepGoing = false;
								}
							}
							//we have an infinite loop
							//so just make this one an 'empty' environment
							//that validate data will fill in with 0's
							if (keepGoing)
								ExternalStates[curLink]->reusePrevLightEnvironment = -1;
							GoodEnvironments.push_back(i);
							//the main contact has now been labelled as OK because it eventually
							//gets to one that will not reference others
							keepGoing = false;
						}
					}
					LinkedEnvironment.push_back(curLink);
				}
			} while (keepGoing);
		}
	}
	GoodEnvironments.clear();
}

void SS_SimData::Initialize() {

	if (steadyStateClampReduction <= 0.0 || steadyStateClampReduction >= 1.0)
		steadyStateClampReduction = SS_EF_ALTER;

	BreakEnvironmentLoops();
}

void SS_SimData::Validate() {
	if (sim == nullptr)
		return;

	for (unsigned int i = 0; i < ExternalStates.size(); ++i)
		ExternalStates[i]->Validate(*sim->mdesc);
}

void SS_SimData::AddCalcOrder(int spot, double initAcc, double endAcc)
{
	if (spot >= SS_CALCTYPE_MIN && spot <= SS_CALCTYPE_MAX)
	{
		CalculationOrder.push_back(spot);
		InitAccuracy.push_back(initAcc);
		EndAccuracy.push_back(endAcc);
	}
	return;
}

bool SS_SimData::SwapCalcOrder(unsigned int index1, unsigned int index2)
{
	if (index1 >= CalculationOrder.size() || index2 >= CalculationOrder.size())
		return(false);
	if (index1 == index2)
		return(true);	//swapped with itself...

	int tmp;
	tmp = CalculationOrder[index1];
	CalculationOrder[index1] = CalculationOrder[index2];
	CalculationOrder[index2] = tmp;

	double t = InitAccuracy[index1];
	InitAccuracy[index1] = InitAccuracy[index2];
	InitAccuracy[index2] = t;

	t = EndAccuracy[index1];
	EndAccuracy[index1] = EndAccuracy[index2];
	EndAccuracy[index2] = t;

	return true;
}

bool SS_SimData::UpdateCalcOrder(int index, int how, double initAcc, double endAcc)
{
	if ((unsigned int)index < CalculationOrder.size())
	{
		if (how >= SS_CALCTYPE_MIN && how <= SS_CALCTYPE_MAX)
			CalculationOrder[index] = how;
		if (initAcc > 0.0)
			InitAccuracy[index] = initAcc;
		if (endAcc > 0.0)
			EndAccuracy[index] = endAcc;

		if (EndAccuracy[index] > InitAccuracy[index])
			EndAccuracy[index] = InitAccuracy[index];

		return true;
	}
	return false;
}

bool SS_SimData::RemoveCalcOrder(unsigned int index)
{
	if (index < CalculationOrder.size())
	{
		CalculationOrder.erase(CalculationOrder.begin() + index);
		InitAccuracy.erase(InitAccuracy.begin() + index);
		EndAccuracy.erase(EndAccuracy.begin() + index);
		return true;
	}
	return false;
}


int SS_SimData::GetCalcType(bool next)
{
	if (next)
		CalcIndex++;
	if (CalculationOrder.size() == 0)
	{
		CalcIndex = 0;
		CalculationOrder.push_back(SS_TRANS_CUTOFF);	//it was never created, so put one in. This is quickest/most universal that should converge.
		InitAccuracy.push_back(0.0);
		EndAccuracy.push_back(0.0);
	}

	if (CalcIndex >= CalculationOrder.size())
		return(SS_CALC_DONE);
	return(CalculationOrder[CalcIndex]);
}

double SS_SimData::GetCalcInitAccuracy(void)
{
	if (CalcIndex >= InitAccuracy.size())
		return(0.0);
	return(InitAccuracy[CalcIndex]);
}

double SS_SimData::GetCalcEndAccuracy(void)
{
	if (CalcIndex >= EndAccuracy.size())
		return(0.0);
	return(EndAccuracy[CalcIndex]);
}

void SS_SimData::SetCalcIndex(unsigned int which)
{
	if (which >= CalculationOrder.size())
		CalcIndex = CalculationOrder.size();	//signify done
	else
		CalcIndex = which;
	return;
}

SS_SimData::~SS_SimData()
{
	unsigned int sz = ExternalStates.size();
	for (unsigned int i = 0; i < sz; ++i)
	{
		delete ExternalStates[i];
		
	}
	ExternalStates.clear();
	CurrentData = 0;
	steadyStateClampReduction = 0.0;
//	OutFiles.clear();
	CalculationOrder.clear();
	InitAccuracy.clear();
	EndAccuracy.clear();

}

void SS_SimData::UpdateUnknownIDs()
{
	int largestID = -1;
	std::vector<int*> needsChanging;

	for (unsigned int i = 0; i < ExternalStates.size(); ++i)
	{
		if (ExternalStates[i]->id < 0)
			needsChanging.push_back(&ExternalStates[i]->id);
		else if (ExternalStates[i]->id > largestID)
			largestID = ExternalStates[i]->id;
	}
	largestID++;
	for (unsigned int i = 0; i < needsChanging.size(); ++i)
	{
		*needsChanging[i] = largestID;
		largestID++;
	}
}

int Simulation::AddOutFile(std::string fname)
{
	bool add = true;
	for(unsigned int i=0; i<OutFiles.size(); ++i)
	{
		if(OutFiles[i] == fname)
		{
			add=false;
			break;
		}
	}
	if(add)
		OutFiles.push_back(fname);
	return(0);
}

Environment::Environment(SS_SimData* s) : ss(s)
{
	reusePrevContactEnvironment = reusePrevLightEnvironment = -1;
	flags = SS_ENV_NONE;
	id = -1;
	FileOut = "";
}

Environment::Environment(const Environment& env) : ss(env.ss)
{
	ActiveLightEnvironment = env.ActiveLightEnvironment;
	reusePrevContactEnvironment = env.reusePrevContactEnvironment;
	reusePrevLightEnvironment = env.reusePrevLightEnvironment;
	flags = env.flags;
	id = env.id;	//making a copy after all...
	output = env.output;
	FileOut = env.FileOut;

	ContactEnvironment.reserve(env.ContactEnvironment.size());
	for (unsigned int i = 0; i < env.ContactEnvironment.size(); ++i)
	{
		if (env.ContactEnvironment[i] == nullptr)
			ContactEnvironment.push_back(nullptr);
		else
			ContactEnvironment.push_back(new SSContactData(*env.ContactEnvironment[i]));	//no pointers owning memory here to worry about!
	}
}

Environment::~Environment()
{
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
		delete ContactEnvironment[i];
	ContactEnvironment.clear();
	ActiveLightEnvironment.clear();
	output.contacts.clear();
	output.capacitance.clear();
}

bool Environment::hasSpectra(LightSource* lt)
{
	if (lt == nullptr)
		return false;

	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i) {
		if (ActiveLightEnvironment[i] == lt->ID)
			return true;
	}
	return false;
}

bool Environment::hasSpectra(int id)
{
	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i) {
		if (ActiveLightEnvironment[i] == id)
			return true;
	}
	return false;
}

bool Environment::hasContact(Contact* ct)
{
	if (ct == nullptr)
		return false;
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		if (ContactEnvironment[i]->ctc == ct)
			return true;
	}
	return false;
}

SSContactData* Environment::getContact(Contact* ct)
{
	if (ct == nullptr)
		return nullptr;
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		if (ContactEnvironment[i]->ctc == ct)
			return ContactEnvironment[i];
	}
	return nullptr;
}

SSContactData* Environment::getContact(int id)
{
	if (id < 0)
		return nullptr;
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		if (ContactEnvironment[i]->ctcID == id)
			return ContactEnvironment[i];
	}
	return nullptr;
}

bool Environment::hasContact(int id)
{
	if (id < 0)
		return false;
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		if (ContactEnvironment[i]->ctcID == id)
			return true;
	}
	return false;
}

void Environment::Validate(ModelDescribe& mdesc) {
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		ContactEnvironment[i]->Validate(*mdesc.simulation);
	}
	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i) {
		if (mdesc.getSpectra(ActiveLightEnvironment[i]) == nullptr) {
			ActiveLightEnvironment.erase(ActiveLightEnvironment.begin() + i);
			--i;
		}
	}
}

int Environment::SelfContainedValidate()
{
	if (ss == nullptr)	//can't validate...
		return ENV_CONT_VALID_NOTPOSSIBLE;
	Simulation *sim = ss->getSim();
	if (sim == nullptr)
		return ENV_CONT_VALID_NOTPOSSIBLE;
	ModelDescribe *mdesc = sim->mdesc;
	if (mdesc == nullptr)
		return ENV_CONT_VALID_NOTPOSSIBLE;

	//first things first. Make sure every contact data is good.
	//the data in the GUI is highly volatile and may be corrupted between uses, while the data in a running simulation is established, tested, and then run
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
	{
		//cannot trust that the contact links are still valid!
		int ctID = ContactEnvironment[i]->ctcID;
		//so make the link up to date. Or kill it if the link is gone!
		//a user could have very well unchecked a contact for a layer, deallocating the contact. Then rechecked it.
		//this creates a new contact with the same ID, but they wouldn't be pointing to one another!
		bool good = false;
		for (unsigned int j = 0; j < sim->contacts.size(); ++j)
		{
			if (sim->contacts[j]->id == ctID)
			{
				ContactEnvironment[i]->ctc = sim->contacts[j];
				good = true;
				break;
			}
		}
		if (!good) {
			//get rid of it. It is also linking to broken memory that is already deleted. It does not own the contact though.
			delete ContactEnvironment[i];
			ContactEnvironment.erase(ContactEnvironment.begin() + i);
			--i;	//make sure the counter still checks the next one.
			//note, this will ONLY delete if it needs to access this data, and it's bad. Deleting the contact and making a new one
			//should never reach this!
		}
	}

	//next double check the light - make sure all the spectra still exist
	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i) {
		bool found = false;
		for (unsigned int j = 0; j < mdesc->spectrum.size(); ++j) {
			if (mdesc->spectrum[j]->ID == ActiveLightEnvironment[i]) {
				found = true;
				break;
			}
		}

		if (!found)
		{
			ActiveLightEnvironment.erase(ActiveLightEnvironment.begin() + i);
			--i;
		}
	}

	//now make sure there are no duplicates of what is left... and delete any duplicates there might be after merging the data
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
	{
		for (unsigned int j = i + 1; j < ContactEnvironment.size(); ++j)
		{
			//first, double check duplicate pointers
			if (ContactEnvironment[i] == ContactEnvironment[j])
			{
				ContactEnvironment.erase(ContactEnvironment.begin() + j);	//don't kill the data. need it still for i!
				--j;
				continue;
			}

			if (ContactEnvironment[i]->ctcID == ContactEnvironment[j]->ctcID)
			{
				//it wasn't a duplicate, but it was going to the same contact. This is also bad...
				//can't have multiple things trying to control the same contact. The contact will only link back to one of them
				//so attempt to merge j into i.
				ContactEnvironment[i]->MergeSame(ContactEnvironment[j]); // this should be guaranteed to return true as all the other conditions have been guaranteed earlier in this function
				//and just get rid of j now that all applicable data is taken from it!
				delete ContactEnvironment[j];
				ContactEnvironment.erase(ContactEnvironment.begin() + j);
				--j;
				continue;
			}
		}
	}

	//and check the lights for duplicates, removing any found
	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i) {
		for (unsigned int j = i + 1; j < ActiveLightEnvironment.size(); ++j) {
			if (ActiveLightEnvironment[i] == ActiveLightEnvironment[j]) {
				ActiveLightEnvironment.erase(ActiveLightEnvironment.begin() + j);
				--j;
			}
		}
	}
	
	//at this point, everything we are working with is valid and no duplicates.
	//we are not trying to rectify user mistakes. Only point them out so the user can fix 'em or understand what's going on.

	unsigned int VoltageCount, EFieldCount, currentCount, minimizeCt, DOppCount, JNeutralCt, EFPoisCt, EDCurrentCount, EDEfieldCt, targetRestrictions, inactiveCount;
	VoltageCount = EFieldCount = currentCount = minimizeCt = EDCurrentCount = EDEfieldCt = inactiveCount = EFPoisCt = DOppCount = JNeutralCt = 0;
	targetRestrictions = 2;	//default for 1D problem

	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
	{
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_VOLT_LINEAR)
			VoltageCount++;
		if (ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_CURRENTS_NONED | CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_COMPLEX_EFIELD))
		{
			EFPoisCt++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				currentCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_NONED)
				EFieldCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
				minimizeCt++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD)
				DOppCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT)
				JNeutralCt++;

		}

		//note the else prevents current and efield from both simultaneously incrementing for the same contact
		//they have the same effect on the poisson equation. 
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_ED)
		{
			EDCurrentCount++;
			targetRestrictions++;
		}
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_ED)
		{
			EDEfieldCt++;
			targetRestrictions++;
		}
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_FLOAT_EFIELD_ESCAPE)
			targetRestrictions++;
		if (ContactEnvironment[i]->currentActiveValue == CONTACT_INACTIVE)
			inactiveCount++;
	}

	//now based on this...
	unsigned int totalCountMain = VoltageCount + EFPoisCt;	//number of boundary conditions in the equation
	unsigned int totalCountED = targetRestrictions - 2;	//targetRestrictions initialized to 2 for 1D problem
	unsigned int totalCount = VoltageCount + currentCount + EFieldCount + minimizeCt + DOppCount + JNeutralCt + totalCountED;

	unsigned int retVal = ENV_CONT_VALID_GOOD;

	if (VoltageCount == 0)
		retVal |= ENV_CONT_VALID_NO_VOLT | ENV_CONT_VALID_NEED_MORE;

	//Do a set of auto suppressions - Can't minimize EField in device if the EField is set!
	for (unsigned int i = 0; i < ContactEnvironment.size(); i++)
	{
		//but first reset the suppressions so start with a clean slate.
		ContactEnvironment[i]->suppressConserveJ = false;
		ContactEnvironment[i]->suppressCurrentPoisson = false;
		ContactEnvironment[i]->suppressEFieldPoisson = false;
		ContactEnvironment[i]->suppressEFMinPoisson = false;
		ContactEnvironment[i]->suppressOppD = false;
		ContactEnvironment[i]->suppressVoltagePoisson = false;

		//suppress efield/current if it's efield (PRIORITY)
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_NONED) {
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY) {

					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED) {
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD) {
					ContactEnvironment[i]->suppressOppD = true;
					totalCount--;
					DOppCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT) {
					ContactEnvironment[i]->suppressConserveJ = true;
					totalCount--;
					JNeutralCt--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT) {
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY) {

					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED) {
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD) {
					ContactEnvironment[i]->suppressOppD = true;
					totalCount--;
					DOppCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD) {
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY) {

					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED) {
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY) {
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					retVal |= ENV_CONT_VALID_OVERLAP;
					//Countmain does not change because minimizeCt still is there.
				}
			}
		
	}

	if (totalCount == targetRestrictions)
	{
		return(retVal);	//once taking care of auto suppressions, we have a winner!
	}

	if (totalCount < targetRestrictions) //once taking care of auto suppressions, we're still low.
	{
		retVal |= ENV_CONT_VALID_NEED_MORE; //also will result in no suppressions, but definitely need more data!
		for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
		{
			ContactEnvironment[i]->suppressConserveJ = false;
			ContactEnvironment[i]->suppressCurrentPoisson = false;
			ContactEnvironment[i]->suppressEFieldPoisson = false;
			ContactEnvironment[i]->suppressEFMinPoisson = false;
			ContactEnvironment[i]->suppressOppD = false;
			ContactEnvironment[i]->suppressVoltagePoisson = false;
		}
		return(retVal);
	}

	//at this point, even with auto suppressions, still too much!
	int numSuppressions = totalCount - targetRestrictions;
	unsigned int numCtc = ContactEnvironment.size();
	retVal |= ENV_CONT_VALID_TOO_MUCH;
	if (currentCount > 0)
	{
		//No particular order to suppression.
		for (unsigned int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_CURRENTS_NONED)) &&
					ContactEnvironment[i]->suppressCurrentPoisson == false)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
	}
	//after suppressing currents (their could be some injection ignoring drift-diffusion... suppress minimizing efields
	if (minimizeCt > 0 && numSuppressions > 0)
	{
		//No particular order to suppression.
		for (unsigned int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MINIMIZE_E_ENERGY)) &&
					ContactEnvironment[i]->suppressEFMinPoisson == false)
				{
					ContactEnvironment[i]->suppressEFMinPoisson = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
	}

	//after suppressing EField minimization, suppress enforcing current conservation
	if (JNeutralCt > 0 && numSuppressions > 0)
	{
		//No particular order to suppression.
		for (unsigned int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_CONSERVE_CURRENT)) &&
					ContactEnvironment[i]->suppressConserveJ == false)
				{
					ContactEnvironment[i]->suppressConserveJ = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
	}
	//after suppressing current conservation, suppress auto DField neutralization BC

	if (DOppCount > 0 && numSuppressions > 0)
	{
		//No particular order to suppression.
		for (unsigned int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_OPP_DFIELD)) &&
					ContactEnvironment[i]->suppressOppD == false)
				{
					ContactEnvironment[i]->suppressOppD = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
	}

	if (EFieldCount > 0 && numSuppressions > 0)
	{
		//No particular order to suppression.
		for (unsigned int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_EFIELD_LINEAR)) &&
					ContactEnvironment[i]->suppressEFieldPoisson == false)
				{
					ContactEnvironment[i]->suppressEFieldPoisson = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
	}

	if (VoltageCount > 0 && numSuppressions > 0)
	{
		//first remove non edge voltages
		int SSFirst, SSSecond;
		LocateEdgeContacts(SSFirst, SSSecond);	//this will fail to find anything because the points aren't assigned in the model. So it'll just pick a contact at random instead
		//of a 'better' contact.
		for (int i = 0; i < (int)ContactEnvironment.size(); ++i)
		{
			if (i == SSFirst || i == SSSecond)
				continue;	//skip these two possibilities

			if (ContactEnvironment[i]->ctc) //it's a validly linked contact
			{
				if (ContactEnvironment[i]->currentActiveValue & (CONTACT_VOLT_LINEAR))
				{
					ContactEnvironment[i]->suppressVoltagePoisson = true;
					numSuppressions--;
				}
			}
			if (numSuppressions <= 0)
				break;
		}
		if (numSuppressions > 0 && SSSecond != -1)
		{
			if (ContactEnvironment[SSSecond]->currentActiveValue & (CONTACT_VOLT_LINEAR))
			{
				ContactEnvironment[SSSecond]->suppressVoltagePoisson = true;
				numSuppressions--;
			}
		}
		if (numSuppressions > 0 && SSFirst != -1)
		{
			if (ContactEnvironment[SSFirst]->currentActiveValue & (CONTACT_VOLT_LINEAR))
			{
				ContactEnvironment[SSFirst]->suppressVoltagePoisson = true;
				numSuppressions--;
			}
		}

	}

	return retVal;
}



int Environment::ValidateData(Model& mdl, Simulation* sim)
{
	int numCtc = ContactEnvironment.size();

	if (reusePrevContactEnvironment >= 0 && reusePrevContactEnvironment < sim->SteadyStateData->CurrentData)	//this is using a different environment for contacts
	{
		//this data set hasn't been validated yet. So validate them - this also assigns the contacts
		sim->SteadyStateData->ExternalStates[reusePrevContactEnvironment]->ValidateData(mdl, sim);
		flags = flags | SS_ENV_CTC_VALIDATED;	//the other one was validated, so this one is as well
		return(0);
	}

	if(flags & SS_ENV_CTC_VALIDATED)
	{
		for (int i = 0; i < numCtc; ++i)
		{
			if (ContactEnvironment[i]->ctc) //be safe! don't dereference a null pointer!
			{
				ContactEnvironment[i]->ctc->UpdateSS(*ContactEnvironment[i]);
				ContactEnvironment[i]->ctc->UpdatePlaneRecPoints(sim->mdesc->Ldimensions);
			}
		}
		return(0);
	}

	//first need to see what we have
	unsigned int VoltageCount, EFieldCount, currentCount, minimizeCt, DOppCount, JNeutralCt, EFPoisCt, EDCurrentCount, EDEfieldCt, targetRestrictions, inactiveCount;
	VoltageCount = EFieldCount = currentCount = minimizeCt = EDCurrentCount = EDEfieldCt = inactiveCount = EFPoisCt = DOppCount = JNeutralCt = 0;
	targetRestrictions = 2;	//default for 1D problem

	
	int numSimCtc = sim->contacts.size();

	if (numSimCtc == 0) //there are no contacts defined. This is no good!
	{
		//well, ain't this a hoot. No defined, contacts, so create some - presumably two
		std::vector<Point*> edges;
		DetermineEdgePoints(mdl, edges);
		int adj = GenerateEdgeContacts(mdl, edges, sim);
		numCtc += adj;
		numSimCtc += adj;
	}

	if (numCtc > numSimCtc)	//we have a big problem...
	{
		//need to get the contacts added into the simulation test
		//this should be presumably be a small list, so most large scale
		//optimizations for speed would probably take too much setup...
		std::vector<int> UsedIndices;	//what SSData's have been used
		std::vector<int> UnusedIndices;	//which contacts have bad links
		//any added contacts default to zero
		ContactEnvironment.resize(numCtc);	//make sure each one has a spot
		
		//make sure the new additions are nulled out
		for (int i = numSimCtc; i < numCtc; ++i)
			ContactEnvironment[i]->ctc = NULL;

		//each contact can only link to one SS index
			//make sure multiple contacts don't link to the same ss
		for (int i = 0; i < numCtc; ++i)
		{
			if (sim->contacts[i]->ContactEnvironmentIndex >= numSimCtc)
			{
				sim->contacts[i]->ContactEnvironmentIndex = -1;
				UnusedIndices.push_back(i);
				continue;
			}
			else if (sim->contacts[i]->ContactEnvironmentIndex < 0)
			{
				UnusedIndices.push_back(i);
				continue;
			}
			else
			{
				bool found = false;
				int useSz = UsedIndices.size();
				for (int testUse = 0; testUse < useSz; ++testUse)
				{
					if (UsedIndices[testUse] == sim->contacts[i]->ContactEnvironmentIndex)
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					UsedIndices.push_back(sim->contacts[i]->ContactEnvironmentIndex);
					ContactEnvironment[sim->contacts[i]->ContactEnvironmentIndex]->ctc = sim->contacts[i];
				}
				else //this SS already exists, sodon't have this contact associate with it
				{
					//the contact link will already have been set
					sim->contacts[i]->ContactEnvironmentIndex = -1;
					UnusedIndices.push_back(i);
				}
			}
		}
		//now associate the remaining contacts appropriately
		unsigned int unusedSz = UnusedIndices.size();
		for (unsigned int i = 0; i < unusedSz; ++i)
		{
			unsigned int usedSSSz = UsedIndices.size();
			bool found = false;
			for (int j = 0; j < int(numCtc); ++j) //numCtc is the number of SS data groups
			{
				//we're looking to assign a contact to a SS value.
				//try to make this contact not do jack unless absolutely necessary
				if (ContactEnvironment[j]->ctc == NULL)
				{
					ContactEnvironment[j]->ctc = sim->contacts[UnusedIndices[i]];
					ContactEnvironment[j]->ctc->ContactEnvironmentIndex = j;
					ContactEnvironment[j]->currentActiveValue = CONTACT_INACTIVE;
				}
			}
		}
	}
	else if (numCtc < numSimCtc) //you can't infer where these contacts ought to be
	{
		//go through the contacts, and flag the numSimCtcs to save.
		//also mark overlapping contacts as issues.
		std::vector<unsigned int> GoodSSContacts;
		std::vector<unsigned int> unusedCtc;
		std::vector<unsigned int> usedCtc;
		for (int i = 0; i < numCtc; ++i)
		{
			if (sim->contacts[i]->ContactEnvironmentIndex >= 0)
			{
				bool found = false;
				for (unsigned int j = 0; j < GoodSSContacts.size(); ++j)
				{
					if (GoodSSContacts[j] == sim->contacts[i]->ContactEnvironmentIndex)
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					GoodSSContacts.push_back(sim->contacts[i]->ContactEnvironmentIndex);
					ContactEnvironment[sim->contacts[i]->ContactEnvironmentIndex]->ctc = sim->contacts[i];
					//make sure the link is 2 way.
					usedCtc.push_back(i);
				}
				else
				{
					unusedCtc.push_back(i);	//don't replace the index. Pull that data when creating a new one.
				}
			}
			else
				unusedCtc.push_back(i); //index is negative, so it gets set to a null one
		}
		std::vector<SSContactData*> tmpSwap;
		//get rid of the bad SS contact data.
		//this might include CONTACT_INACTIVE stuff
		tmpSwap.resize(unusedCtc.size() + usedCtc.size());
		for (unsigned int i = 0; i < usedCtc.size(); ++i)
		{
			//we know this data to be good. Reorganize it cleanly
			tmpSwap[i] = ContactEnvironment[sim->contacts[usedCtc[i]]->ContactEnvironmentIndex];
			tmpSwap[i]->ctc = sim->contacts[usedCtc[i]];
			tmpSwap[i]->ctc->ContactEnvironmentIndex = i;
		}
		int j = 0;
		for (unsigned int i = usedCtc.size(); i < tmpSwap.size(); ++i)
		{
			if (sim->contacts[unusedCtc[j]]->ContactEnvironmentIndex >= 0)
			{
				tmpSwap[i] = ContactEnvironment[sim->contacts[unusedCtc[j]]->ContactEnvironmentIndex];
				tmpSwap[i]->ctc = sim->contacts[unusedCtc[j]];
				tmpSwap[i]->ctc->ContactEnvironmentIndex = i;
			}
			else
			{
				tmpSwap[i]->ctc = sim->contacts[unusedCtc[j]];
				tmpSwap[i]->ctc->ContactEnvironmentIndex = i;
				ContactEnvironment[i]->currentActiveValue = CONTACT_INACTIVE;
			}
			++j;	//just the second increment.
		}
		ContactEnvironment.swap(tmpSwap);	//put the tmp buffer in the new ContactEnvironment
		tmpSwap.clear();	//clear out the temporary buffer
	}
	else
	{
		//need to verify that there are no repeats on any of it and that the contacts are all
		//appropriately linked
		//if all are linked correctly, then all is well
		
		std::vector<int> ProblemContacts;
		std::vector<int> ProblemSS;
		std::vector<int> ProblemContactsEmpty;
		std::vector<int> ProblemSSEmpty;
		for (int i = 0; i < numCtc; ++i)
		{
			//bad contact index
			if (sim->contacts[i]->ContactEnvironmentIndex < 0)
			{
				ProblemContactsEmpty.push_back(i);
			}
			else if (ContactEnvironment[sim->contacts[i]->ContactEnvironmentIndex]->ctc != sim->contacts[i])
			{ //the SS link does not go back to this contact
				ProblemContacts.push_back(i);
			}

			if (ContactEnvironment[i]->ctc == NULL) //the SS link goes nowhere
			{
				ProblemSSEmpty.push_back(i);
			}
			else if (ContactEnvironment[i]->ctc->ContactEnvironmentIndex != i)
			{ //the link goes to a contact that doesn't think it goes here...
				ProblemSS.push_back(i);
			}
		}

		//if the links were only one way as opposed to the wrong destination, try and fix those
		std::vector<int> tmp;
		for (unsigned int i = 0; i < ProblemSS.size(); ++i)
		{
			//look for matching ProblemContactsEmpty
			bool match = false;
			for (std::vector<int>::iterator PCE = ProblemContactsEmpty.begin(); PCE < ProblemContactsEmpty.end(); ++PCE)
			{
				if (ContactEnvironment[ProblemSS[i]]->ctc == sim->contacts[*PCE])
				{
					sim->contacts[*PCE]->ContactEnvironmentIndex = ProblemSS[i];
					match = true;
					ProblemContactsEmpty.erase(PCE);
					break;
				}
			}
			if (!match)
				tmp.push_back(ProblemSS[i]);	//these indices are still bad
		}
		ProblemSS.swap(tmp);
		tmp.clear();

		//now sim->contacts[]->index appropriate, and we must check if that index goes to a null contact
		for (unsigned int i = 0; i < ProblemContacts.size(); ++i)
		{
			//look for matching ProblemSSEmpty
			bool match = false;
			for (std::vector<int>::iterator PCE = ProblemSSEmpty.begin(); PCE < ProblemSSEmpty.end(); ++PCE)
			{
				if (sim->contacts[ProblemContacts[i]]->ContactEnvironmentIndex == *PCE)
				{
					match = true;
					ContactEnvironment[*PCE]->ctc = sim->contacts[ProblemContacts[i]];
					ProblemSSEmpty.erase(PCE);
					break;
				}
			}
			if (!match)
				tmp.push_back(ProblemContacts[i]);	//these indices are still bad
		}
		ProblemContacts.swap(tmp);
		tmp.clear();

		//at this point, whatever is left does not have any matches at all.
		//there must be an equal number of issues on either side.
		//just assign semi-randomly, and make the contacts irrelevant as they don't have valid data anyway
		ProblemContacts.insert(ProblemContacts.end(), ProblemContactsEmpty.begin(), ProblemContactsEmpty.end());
		ProblemSSEmpty.insert(ProblemSSEmpty.end(), ProblemSS.begin(), ProblemSS.end());
		for (unsigned int i = 0; i < ProblemContacts.size(); ++i)
		{
			sim->contacts[ProblemContacts[i]]->ContactEnvironmentIndex = ProblemSSEmpty[i];
			ContactEnvironment[ProblemSSEmpty[i]]->ctc = sim->contacts[ProblemContacts[i]];
			ContactEnvironment[ProblemSSEmpty[i]]->currentActiveValue = CONTACT_INACTIVE;
		}
		ProblemContacts.clear();
		ProblemContactsEmpty.clear();
		ProblemSS.clear();
		ProblemSSEmpty.clear();
	}

	for (int i = 0; i < numCtc; ++i)
	{
		ContactEnvironment[i]->suppressConserveJ = false;	//make sure it starts off clear!
		ContactEnvironment[i]->suppressCurrentPoisson = false;
		ContactEnvironment[i]->suppressEFieldPoisson = false;
		ContactEnvironment[i]->suppressEFMinPoisson = false;
		ContactEnvironment[i]->suppressOppD = false;
		ContactEnvironment[i]->suppressVoltagePoisson = false;

		if (ContactEnvironment[i]->currentActiveValue & CONTACT_VOLT_LINEAR)
			VoltageCount++;
		if (ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_CURRENTS_NONED | CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_COMPLEX_EFIELD))
		{
			EFPoisCt++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				currentCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_NONED)
				EFieldCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
				minimizeCt++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD)
				DOppCount++;
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT)
				JNeutralCt++;

		}

		//note the else prevents current and efield from both simultaneously incrementing for the same contact
		//they have the same effect on the poisson equation. 
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_ED)
		{
			EDCurrentCount++;
			targetRestrictions++;
		}
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_ED)
		{
			EDEfieldCt++;
			targetRestrictions++;
		}
		if (ContactEnvironment[i]->currentActiveValue & CONTACT_FLOAT_EFIELD_ESCAPE)
			targetRestrictions++;
		if (ContactEnvironment[i]->currentActiveValue == CONTACT_INACTIVE)
			inactiveCount++;
	}

	

	unsigned int totalCountMain = VoltageCount + EFPoisCt;	//number of boundary conditions in the equation
	unsigned int totalCountED = targetRestrictions-2;	//targetRestrictions initialized to 2 for 1D problem
	unsigned int totalCount = VoltageCount + currentCount + EFieldCount + minimizeCt + DOppCount + JNeutralCt + totalCountED;
	
	//just assume 1D presently
	if (totalCount == 0) //implies everything is inactive
	{
		//there isn't anything going on... let's just do thermal equilibrium.
		//set voltages to 0 on edge contacts and force 0 current.
		int SSFirst, SSSecond;
		LocateEdgeContacts(SSFirst, SSSecond);	//assign SSFirst/SSSecond. -1 means not valid

		if (SSFirst != -1 && SSSecond != -1)
		{
			//easiest solution
			ContactEnvironment[SSFirst]->voltage = ContactEnvironment[SSSecond]->voltage = 0.0;
			ContactEnvironment[SSFirst]->suppressCurrentPoisson = false;	//setting current zero for rates
			ContactEnvironment[SSSecond]->suppressCurrentPoisson = false;	//but not letting it affect poisson equation
			ContactEnvironment[SSFirst]->suppressVoltagePoisson = false;
			ContactEnvironment[SSSecond]->suppressVoltagePoisson = false;
			int ActValue = CONTACT_SS | CONTACT_VOLT_LINEAR | CONTACT_CONNECT_EXTERNAL;
			ContactEnvironment[SSFirst]->currentActiveValue = ActValue;
			ContactEnvironment[SSSecond]->currentActiveValue = ActValue;
			inactiveCount -= 2;
			VoltageCount += 2;
			totalCountMain += 2;
			totalCount += 2;
		}
		else if (SSFirst != -1)
		{
			//then SSSecond is invalid. So we'll just set the voltage to zero and try to make sure the displacement fields would roughly cancel (maintain balanced band diagram)
			ContactEnvironment[SSFirst]->voltage = ContactEnvironment[SSFirst]->current = 0.0;
			int ActValue = CONTACT_SS | CONTACT_VOLT_LINEAR | CONTACT_OPP_DFIELD | CONTACT_CONNECT_EXTERNAL;
			ContactEnvironment[SSFirst]->currentActiveValue = ActValue;
			ContactEnvironment[SSFirst]->suppressEFMinPoisson = false;
			ContactEnvironment[SSFirst]->suppressVoltagePoisson = false;
			inactiveCount -= 2;
			VoltageCount += 1;
			currentCount += 1;
			totalCountMain += 2;
			totalCount += 2;
		}
		else if (SSSecond != -1)
		{
			//SSFirst was invalid
			ContactEnvironment[SSSecond]->voltage = ContactEnvironment[SSSecond]->current = 0.0;
			int ActValue = CONTACT_SS | CONTACT_VOLT_LINEAR | CONTACT_OPP_DFIELD | CONTACT_CONNECT_EXTERNAL;
			ContactEnvironment[SSSecond]->currentActiveValue = ActValue;
			ContactEnvironment[SSSecond]->suppressEFMinPoisson = false;
			ContactEnvironment[SSSecond]->suppressVoltagePoisson = false;
			inactiveCount -= 2;
			VoltageCount += 1;
			currentCount += 1;
			totalCountMain += 2;
			totalCount += 2;
		}
		else //SSFirst = -1, SSSecond = -1
		{
			//both are invalid. This is what I call a wut? simulation.
			//nothing has been specified, so we'll just pick the first two valid contacts...
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc)	//be safe
				{
					if (SSFirst == -1)
					{
						SSFirst = i;
					}
					else //separating just to be safe. Don't think could get Array[-1], but this makes sure
					{
						if (ContactEnvironment[SSFirst]->ctc != ContactEnvironment[i]->ctc)
						{
							SSSecond = i;
							break;
						}
					}
				}
			}
			//these aren't on the outside, and are potentially anywhere, so can't set current to zero
			ContactEnvironment[SSFirst]->voltage = 0.0;	//arbitrarily set the votlages to 0
			ContactEnvironment[SSSecond]->voltage = 0.0;
			int ActValue = CONTACT_SS | CONTACT_VOLT_LINEAR | CONTACT_CONNECT_EXTERNAL;
			ContactEnvironment[SSFirst]->currentActiveValue = ActValue;
			ContactEnvironment[SSSecond]->currentActiveValue = ActValue;
			ContactEnvironment[SSFirst]->suppressVoltagePoisson = false;
			ContactEnvironment[SSSecond]->suppressVoltagePoisson = false;
			inactiveCount -= 2;
			VoltageCount += 2;
			totalCountMain += 2;
			totalCount += 2;
		}

	}
	
	if (VoltageCount == 0)
	{
		//need at least one voltage for the active stuff
		//preferentially a side contact. This can only happen if other contacts had been defined
		int SSFirst, SSSecond;
		bool AddedVoltage = false;
		LocateEdgeContacts(SSFirst, SSSecond);	//assign SSFirst/SSSecond. -1 means not valid
		if (SSFirst != -1)
		{
			if (ContactEnvironment[SSFirst]->currentActiveValue != CONTACT_INACTIVE)
			{
				ContactEnvironment[SSFirst]->voltage = 0.0;
				ContactEnvironment[SSFirst]->currentActiveValue = CONTACT_VOLT_LINEAR | ContactEnvironment[SSFirst]->currentActiveValue;
				ContactEnvironment[SSFirst]->suppressVoltagePoisson = false;
				AddedVoltage = true;
				VoltageCount++;
				totalCountMain++;
				totalCount++;
			}
		}
		if (!AddedVoltage && SSSecond != -1)
		{
			if (ContactEnvironment[SSSecond]->currentActiveValue != CONTACT_INACTIVE)
			{
				ContactEnvironment[SSSecond]->voltage = 0.0;
				ContactEnvironment[SSSecond]->currentActiveValue = CONTACT_VOLT_LINEAR | ContactEnvironment[SSSecond]->currentActiveValue;
				ContactEnvironment[SSSecond]->suppressVoltagePoisson = false;
				AddedVoltage = true;
				VoltageCount++;
				totalCountMain++;
				totalCount++;
			}
		}
		if (!AddedVoltage)
		{
			//just find an active contact and set the voltage arbitrarily to zero
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->currentActiveValue != CONTACT_INACTIVE)	//be safe
				{
					ContactEnvironment[SSSecond]->voltage = 0.0;
					ContactEnvironment[SSSecond]->currentActiveValue = CONTACT_VOLT_LINEAR | ContactEnvironment[SSSecond]->currentActiveValue;
					ContactEnvironment[SSSecond]->suppressVoltagePoisson = false;
					AddedVoltage = true;
					VoltageCount++;
					totalCountMain++;
					totalCount++;
				}
			}
		}
	}

	//Do a set of auto suppressions - Can't minimize EField in device if the EField is set!
	for (int i = 0; i < numCtc; i++)
	{
		if (ContactEnvironment[i]->ctc) //it's a validly linked contact
		{
			if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_EFIELD_NONED)	//suppress efield/current if it's efield (PRIORITY)
			{
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
				{
				
					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD)
				{
					ContactEnvironment[i]->suppressOppD = true;
					totalCount--;
					DOppCount--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT)
				{
					ContactEnvironment[i]->suppressConserveJ = true;
					totalCount--;
					JNeutralCt--;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_CONSERVE_CURRENT)
			{
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
				{

					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD)
				{
					ContactEnvironment[i]->suppressOppD = true;
					totalCount--;
					DOppCount--;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_OPP_DFIELD)
			{
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
				{

					ContactEnvironment[i]->suppressEFMinPoisson = true;
					totalCount--;
					minimizeCt--;
					//Countmain does not change because CtEField still is there.
				}

				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					//Countmain does not change because CtEField still is there.
				}
			}
			else if (ContactEnvironment[i]->currentActiveValue & CONTACT_MINIMIZE_E_ENERGY)
			{
				if (ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_CURRENTS_NONED)
				{
					ContactEnvironment[i]->suppressCurrentPoisson = true;
					totalCount--;
					currentCount--;
					//Countmain does not change because minimizeCt still is there.
				}
			}
		}
	}
	//at this point there is at least ONE voltage, and more
	if (totalCount < targetRestrictions)
	{
		int numberAdditions = targetRestrictions - totalCount;
		//how many items need to be added for this to work?
		//default to try and make it so there is the smallest amount of electric field possible in the device
		if (numberAdditions > 0 && DOppCount == 0 && minimizeCt==0 && JNeutralCt == 0)	//you can only have one contact for manipulating the electric field in the device
		{
			for (int i = 0; i < numCtc; i++)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					//make sure it does not already have an electric field set from something else
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_CURRENTS_NONED | CONTACT_OPP_DFIELD)) == CONTACT_INACTIVE)
					{
						ContactEnvironment[i]->currentActiveValue = CONTACT_OPP_DFIELD | ContactEnvironment[i]->currentActiveValue;	//probably the safest value
						ContactEnvironment[i]->suppressEFMinPoisson = false;
						numberAdditions--;
						DOppCount++;
						totalCount++;
						totalCountMain++;
						EFPoisCt++;
						break;
					}
				}
			}
		}

		int AddVoltages = numCtc - VoltageCount;
		if (AddVoltages > 0 && numberAdditions > 0)
		{
			//just start adding zero voltages
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					//it currently does not have a voltage input
					if ((ContactEnvironment[i]->currentActiveValue & CONTACT_MASK_POISSON_VOLT) == CONTACT_INACTIVE)
					{
						//add a voltage...
						ContactEnvironment[i]->voltage = 0.0;
						ContactEnvironment[i]->currentActiveValue = CONTACT_VOLT_LINEAR | ContactEnvironment[i]->currentActiveValue;
						ContactEnvironment[i]->suppressVoltagePoisson = false;
						//add the voltage info

						//now add the counts
						AddVoltages--;
						numberAdditions--;
						totalCount++;
						VoltageCount++;
						totalCountMain++;
					}
				}
				if (numberAdditions <= 0 || AddVoltages <= 0)
					break;
			}
		}

		int AddEFields = numCtc - EFPoisCt;
		if (AddEFields > 0 && numberAdditions > 0)
		{
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_CURRENTS_NONED | CONTACT_EFIELD_LINEAR | CONTACT_MASK_COMPLEX_EFIELD)) == CONTACT_INACTIVE)
					{
						//default to make the efield zero
						ContactEnvironment[i]->eField = 0.0;
						ContactEnvironment[i]->currentActiveValue = CONTACT_EFIELD_LINEAR | ContactEnvironment[i]->currentActiveValue;
						ContactEnvironment[i]->suppressEFieldPoisson = false;
						EFieldCount++;
						AddEFields--;
						numberAdditions--;
						EFPoisCt++;
						totalCount++;
						totalCountMain++;
					}
				}
				if (numberAdditions <= 0 || AddEFields <= 0)
					break;
			}
		}
	}
	else if (totalCount > targetRestrictions)
	{
		//we need to suppress some stuff!
		//first thing to try and suppress are currents, then EFields, and then voltages
		int numSuppressions = totalCount - targetRestrictions;
		
		if (currentCount > 0)
		{
			//No particular order to suppression.
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MASK_CURRENTS_NONED)) &&
						ContactEnvironment[i]->suppressCurrentPoisson == false)
					{
						ContactEnvironment[i]->suppressCurrentPoisson = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
		}
		//after suppressing currents (their could be some injection ignoring drift-diffusion... suppress minimizing efields
		if (minimizeCt > 0 && numSuppressions > 0)
		{
			//No particular order to suppression.
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_MINIMIZE_E_ENERGY)) &&
						ContactEnvironment[i]->suppressEFMinPoisson == false)
					{
						ContactEnvironment[i]->suppressEFMinPoisson = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
		}

		//after suppressing EField minimization, suppress enforcing current conservation
		if (JNeutralCt > 0 && numSuppressions > 0)
		{
			//No particular order to suppression.
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_CONSERVE_CURRENT)) &&
						ContactEnvironment[i]->suppressConserveJ == false)
					{
						ContactEnvironment[i]->suppressConserveJ = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
		}
		//after suppressing current conservation, suppress auto DField neutralization BC

		if (DOppCount > 0 && numSuppressions > 0)
		{
			//No particular order to suppression.
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_OPP_DFIELD)) &&
						ContactEnvironment[i]->suppressOppD == false)
					{
						ContactEnvironment[i]->suppressOppD = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
		}

		if (EFieldCount > 0 && numSuppressions > 0)
		{
			//No particular order to suppression.
			for (int i = 0; i < numCtc; ++i)
			{
				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if ((ContactEnvironment[i]->currentActiveValue & (CONTACT_EFIELD_LINEAR)) &&
						ContactEnvironment[i]->suppressEFieldPoisson == false)
					{
						ContactEnvironment[i]->suppressEFieldPoisson = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
		}

		if (VoltageCount > 0 && numSuppressions > 0)
		{
			//first remove non edge voltages
			int SSFirst, SSSecond;
			LocateEdgeContacts(SSFirst, SSSecond);
			for (int i = 0; i < numCtc; ++i)
			{
				if (i == SSFirst || i == SSSecond)
					continue;	//skip these two possibilities

				if (ContactEnvironment[i]->ctc) //it's a validly linked contact
				{
					if (ContactEnvironment[i]->currentActiveValue & (CONTACT_VOLT_LINEAR))
					{
						ContactEnvironment[i]->suppressVoltagePoisson = true;
						numSuppressions--;
					}
				}
				if (numSuppressions <= 0)
					break;
			}
			if (numSuppressions > 0 && SSSecond != -1)
			{
				if (ContactEnvironment[SSSecond]->currentActiveValue & (CONTACT_VOLT_LINEAR))
				{
					ContactEnvironment[SSSecond]->suppressVoltagePoisson = true;
					numSuppressions--;
				}
			}
			if (numSuppressions > 0 && SSFirst != -1)
			{
				if (ContactEnvironment[SSFirst]->currentActiveValue & (CONTACT_VOLT_LINEAR))
				{
					ContactEnvironment[SSFirst]->suppressVoltagePoisson = true;
					numSuppressions--;
				}
			}
			
		}
	}




	//now update all the contacts with the appropriate SSContactData and select the points used
	//for each contact to calculate the current output
	for (int i = 0; i < numCtc; ++i)
	{
		if (ContactEnvironment[i]->ctc) //be safe! don't dereference a null pointer!
		{
			ContactEnvironment[i]->ctc->UpdateSS(*ContactEnvironment[i]);
			ContactEnvironment[i]->ctc->UpdatePlaneRecPoints(sim->mdesc->Ldimensions);
		}
	}

	flags = flags | SS_ENV_CTC_VALIDATED;
	//and put the light data in what's being used.

	return(CONTACT_BD_CHANGE);
}

int Environment::GenerateEdgeContacts(Model& mdl, std::vector<Point*>& edges, Simulation* sim)
{
	int CIndex = sim->contacts.size();
	int SSIndex = ContactEnvironment.size();
	int TotCreated = 0;
	unsigned int sz = edges.size();
	Contact* tmpC;
	SSContactData* SSdata = new SSContactData();
	SSdata->Clear();
	for (unsigned int i = 0; i < sz; ++i)
	{
		//this is only happening for steady state
		if (edges[i]->contactData == NULL)
		{
			tmpC = new Contact;
			edges[i]->contactData = tmpC;	//make sure the point links to it
			sim->contacts.push_back(tmpC);	//store contact in overall simulation bit
			tmpC->point.push_back(edges[i]);	//make sure contact links to point
			tmpC->id = CIndex;	//give contact a unique index

			tmpC->ContactEnvironmentIndex = SSIndex;	//make sure it get sassociated with the proper SS data
			SSdata->ctc = tmpC;	//make sure SS data points back to this newly created contact

			ContactEnvironment.push_back(SSdata);	//put the SS data in the environment - though it is inactive
			
			CreateExternalContactPoints(tmpC, mdl);	//this is needed for all the contact rates to actually work!
			SSIndex++;	//update indices appropriately
			TotCreated++;
			CIndex++;
		}
	}
	return(TotCreated);
}

int Environment::LocateEdgeContacts(int& indexLEdge, int& indexREdge)
{
	indexLEdge = indexREdge = -1;	//default not found

	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i)
	{
		if (ContactEnvironment[i]->ctc)	//be safe
		{
			for (unsigned int pt = 0; pt < ContactEnvironment[i]->ctc->point.size(); ++pt)
			{
				if (ContactEnvironment[i]->ctc->point[pt]->adj.adjMX == NULL)
					indexLEdge = i;	//the left contact is chosen
				if (ContactEnvironment[i]->ctc->point[pt]->adj.adjPX == NULL)
					indexREdge = i;	//the right contact is chosen
			}
		}
		if (indexLEdge != -1 && indexREdge != -1)	//found both. Be done.
			break;
	}
	return(0);
}

SteadyState_SAAPD::~SteadyState_SAAPD()
{
	Clear();
}

int SteadyState_SAAPD::Clear()
{
	//class PosNegSum is straight up data. Can jsut clear it out
	EStates.clear();
	Pt.clear();
	ELevelIndicesInPt.clear();
	varCharge.Clear();
	PosChargeChanges.Clear();
	NegChargeChanges.Clear();
	BandPtsUpdated.clear();
	UpdatedPtsInitPsi.clear();
	ReduceTolerance = true;
	tolerance = SS_INITIAL_TOLERANCE;
	targetCharge = 0.0;
	chargeTolerance = 1.0e-10;	//just a random value that might? be reasonable
	return(0);
}

int SteadyState_SAAPD::SetCtcSinkPts(Model& mdl)
{
	
	for(unsigned int i=0; i< Pt.size(); ++i)
	{
		if(mdl.getPoint(Pt[i].self)->contactData)
		{
			if(mdl.getPoint(Pt[i].self)->contactData->GetCurrentActive() & CONTACT_SINK)
			{
				Pt[i].ctcSink = true;
				mdl.getPoint(Pt[i].self)->Thermalize(0);	//make sure it is thermalized and not 'bad'
				TransferMdlPtToSS(mdl, i);
			}
			else
				Pt[i].ctcSink = false;
		}
		else
			Pt[i].ctcSink = false;
	}

	return(0);
}

int SteadyState_SAAPD::TransferMdlPtToSS(Model& mdl, int SSPtIndex)
{
	int ptSz = Pt.size();
	if(SSPtIndex < 0 || SSPtIndex >= ptSz)
		return(-1);	//fail

	long int start = ELevelIndicesInPt[SSPtIndex].min;
	long int stop =  ELevelIndicesInPt[SSPtIndex].max;

	if(stop < start || start < 0 || stop < 0)
		return(-1);
	double occ, avail;
	ELevel* el;
	double kT = mdl.getPoint(Pt[SSPtIndex].self)->temp * KB;
	double ptFixedCharge = 0.0;
	Pt[SSPtIndex].VariableCharge.Clear();
	for(long int i = start; i<=stop; ++i)
	{
		el = EStates[i].ptr;
		el->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
		if(occ <= avail)
		{
			EStates[i].occupancy = occ;
			EStates[i].OccMatches = true;
		}
		else
		{
			EStates[i].occupancy = avail;
			EStates[i].OccMatches = false;
		}
		
		EStates[i].curEf = RelEfFromOcc(occ, avail, el->max - el->min, kT, (el->imp) ? el->imp->getDegeneracy() : 1.0);	//didn't know I could do this. Going to save so many lines in the future!

		if (el->imp)
		{
			double charge = double(el->imp->getCharge()) * EStates[i].NStates;	//no Q or volume multiply
			ptFixedCharge += charge;
		}
			

		if (EStates[i].carType == ELECTRON)
			Pt[SSPtIndex].VariableCharge.AddValue(-occ);
		else
			Pt[SSPtIndex].VariableCharge.AddValue(occ);

		PosChargeChanges.SetProbability(0.0, i);	//reset the probabilities
		NegChargeChanges.SetProbability(0.0, i);
	}

	Pt[SSPtIndex].fixedCharge = ptFixedCharge;
	Pt[SSPtIndex].curcharge = ptFixedCharge + Pt[SSPtIndex].VariableCharge.GetNetSum();
	Pt[SSPtIndex].prevCharge = Pt[SSPtIndex].curcharge;
	mdl.poissonRho[Pt[SSPtIndex].self] = Pt[SSPtIndex].curcharge * QCHARGE * mdl.getPoint(Pt[SSPtIndex].self)->voli;
	
	return(0);
}

EnvironmentOutputs::EnvironmentOutputs()
{
	quantumEfficiencyExternal = quantumEfficiencyInternal = 0.0;
	calculated = false;
	contacts.clear();
	capacitance.clear();
}

EnvironmentOutputs::~EnvironmentOutputs()
{
	quantumEfficiencyExternal = quantumEfficiencyInternal = 0.0;
	calculated = false;
	contacts.clear();
	capacitance.clear();
}

