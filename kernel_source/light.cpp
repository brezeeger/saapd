#include "light.h"

#include <ctime>
#include "physics.h"
#include "model.h"
#include "basis.h"
#include "transient.h"
#include "helperfunctions.h"
#include "output.h"
#include "outputdebug.h"
#include "trig.h"
#include "ss.h"
#include "RateGeneric.h"
#include "../gui_source/kernel.h"
#include "Materials.h"

/*
Functions needed

Snell's law to find angle for material and packet.
Fresnel's law for determining amount of reflection and transmitted
Generating photons to move
Ray-Tracing photons through the materials.
Calculate Light absorbed in a given point
Finding which electrons to kick up in energy (weight by num electrons and choose)

Methodology for kicking up impurities in absorption that doesn't necessarily have nk data - applied later. Need actual physics.
--add all absorption coefficients applicable to band minimum energies (all possibilties)
--multiplying the number of CB and VB states for appropriate alphas and add - this is roughly the number of absorption possibilities to get
    the particular absorption coefficient
-- look at energies for a particular impurity and multiply number of states in two energy levels.
-- absorption coefficient used should be the summed alpha * ratio of imp state combos to EG combos

Extracting values for n,k outside of range - might be a better option than above...

*/


//currently, everything is 1D. That means all the planes are the YZ. simplifies things for now, but will need to change when 2/3D.
//will have to look at the layers - returns in radians. This splits the beam of light, which has been apparently taken care of electronically
//into the next two

int Light(Model& mdl, ModelDescribe& mdesc, Point* singlePt, kernelThread* kThread)
{
	if(mdesc.simulation->EnabledRates.Lgen == false
		&& mdesc.simulation->EnabledRates.Ldef == false
		&& mdesc.simulation->EnabledRates.Lscat == false
		&& mdesc.simulation->EnabledRates.Lsrh == false
		&& mdesc.simulation->EnabledRates.Lpow == false
		&& mdesc.simulation->EnabledRates.SEdef == false
		&& mdesc.simulation->EnabledRates.SErec == false
		&& mdesc.simulation->EnabledRates.SEscat == false
		&& mdesc.simulation->EnabledRates.SEsrh == false
		&& mdesc.simulation->EnabledRates.LightEmissions == false)
		return(0);	//check if this calculation should be done

	if(mdl.gridp.size() == 0)	//seriously doubt this will EVER happen, but one check here saves many begin() checks for valid iterators
		return(0);

	if(mdl.NDim == 1)
		DoLight1D(mdl, mdesc, singlePt, kThread);

	return(0);
}

int DoLight1D(Model& mdl, ModelDescribe& mdesc, Point* singlePt, kernelThread* kThread)	//I can now do many simplifications
{
	/*
	1D assumptions, there is no shape to the source. there is no collimation to the soruce. Cell extends infinitely in y & z
	EFields do not matter because they can not directly interfere with one another for a given wavelength.
	There is no coherence involved
	All variation is in X
	*/
	LightSource* curLSrc = NULL;
	//list<RayTrace*> RayProcessList;
	std::list<RayTrace> RayList;
	RayTrace tmpRay;
	double area = mdesc.Ldimensions.y * mdesc.Ldimensions.z;
	time_t start = std::time(NULL);
	time_t end;
	double timeelapse = 0.0;
	long int ctr = 0;
	if(kThread)
	{
		kThread->shareData.KernelcurInfoMsg = MSG_LIGHTSPECTRUM;
		for(unsigned int i=0; i<mdesc.spectrum.size(); ++i)
			ctr += mdesc.spectrum[i]->light.size();
		kThread->shareData.KernelshortProgBarRange = ctr;
		kThread->shareData.KernelshortProgBarPosition = 0;
		kThread->shareData.KernelTimeLeft = 0.0;	//don't do a time estimation
		ctr = 0;
	}
	//the official spectrum - this may not be appropriately binned.

	//need to get the lowest energies, and then we'll step up based on
	double currentEnergy = 1.0e6;	//we are not going to have a 1MeV photon for PV simulations
	double nextEnergy = currentEnergy;	//the next approximate energy to look for
	double eTolerance = mdesc.simulation->EnabledRates.OpticalEResolution * 0.25;	//a delta because doubles aren't exact

	//first go through all the spectra
	for (unsigned int i = 0; i < mdesc.spectrum.size(); i++)
	{
		curLSrc = mdesc.spectrum[i];
		if (curLSrc->enabled == false)	//if it is currently disabled, don't add the light in
			continue;

		if (curLSrc->direction.x == 0)
			continue;

		//some pre-initialization code
		if (curLSrc->centerinit == false)
		{
			curLSrc->centerinit = true;
			if (curLSrc->sourceShape != NULL)
			{
				curLSrc->center = curLSrc->sourceShape->center;
			}
			else
			{
				if (curLSrc->direction.x > 0)
				{
					//get furthest left point. This presumably should be the first one entered into the mesh

					Point* lPt = &(mdl.gridp.begin()->second);
					while (lPt->adj.adjMX)
						lPt = lPt->adj.adjMX;

					curLSrc->center.x = lPt->mxmy.x - 1e-4;	//just put it a micron further over
					curLSrc->center.y = lPt->position.y;
					curLSrc->center.z = lPt->position.z;

				}
				else
				{
					Point* rPt = &(mdl.gridp.rbegin()->second);
					while (rPt->adj.adjPX)
						rPt = rPt->adj.adjPX;

					curLSrc->center.x = rPt->pxpy.x + 1e-4;	//just put it a micron further over
					curLSrc->center.y = rPt->position.y;
					curLSrc->center.z = rPt->position.z;
				}
			}
		}

		if (curLSrc->light.size() > 0)	//make sure there are light sources
		{
			double fEnergy = curLSrc->light.begin()->first;
			double sEnergy = (curLSrc->light.size() >= 1) ? (curLSrc->light.begin()++)->first : -1.0;
			curLSrc->EnergyNextUse = fEnergy;	//signal that it wants to find this particular energy when doing the next run.
			if (fEnergy < currentEnergy - eTolerance)	//
			{
				currentEnergy = fEnergy;
				if (sEnergy > 0.0 && sEnergy < nextEnergy - eTolerance)
					nextEnergy = sEnergy;
			}
			else if (fEnergy < nextEnergy - eTolerance)	//it is not the lowest, but it is the second lowest
				nextEnergy = fEnergy;
		}
	}
	//now go through all the points if there are any emissions
	if (mdesc.simulation->EnabledRates.LightEmissions == true)	//no need to do the following points
	{
		//the spectrum from all the recombination occuring in the points
		for (std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
		{
			if (curPt->second.LightEmissions == NULL)	//there are none
				continue;
			curLSrc = curPt->second.LightEmissions;
			ctr++;
			if (curLSrc->enabled == false)	//if it is currently disabled, don't add the light in
				continue;

			if (curLSrc->light.size() > 0)	//make sure there are light sources
			{
				double fEnergy = curLSrc->light.begin()->first;
				double sEnergy = (curLSrc->light.size() >= 1) ? (curLSrc->light.begin()++)->first : -1.0;
				curLSrc->EnergyNextUse = fEnergy;	//signal that it wants to find this particular energy when doing the next run.
				if (fEnergy < currentEnergy - eTolerance)	//
				{
					currentEnergy = fEnergy;
					if (sEnergy > 0.0 && sEnergy < nextEnergy - eTolerance)
						nextEnergy = sEnergy;
				}
				else if (fEnergy < nextEnergy - eTolerance)	//it is not the lowest, but it is the second lowest
					nextEnergy = fEnergy;
			}
			else
				curLSrc->EnergyNextUse = -1.0;	//disabled
		}
	}
	if (currentEnergy == 1.0e6)	//there was no active light
		currentEnergy = -1.0;

	while (currentEnergy != -1.0)
	{
		nextEnergy = -1.0;
		for (unsigned int i = 0; i < mdesc.spectrum.size(); i++)
		{
			curLSrc = mdesc.spectrum[i];
			if (curLSrc->EnergyNextUse == -1.0)	//this light source is done! Most likely thing to happen
				continue;

			if (curLSrc->enabled == false)	//if it is currently disabled, don't add the light in
				continue;

			if (curLSrc->direction.x == 0)
				continue;

			if (curLSrc->EnergyNextUse < currentEnergy + eTolerance && curLSrc->EnergyNextUse > currentEnergy - eTolerance)
			{
				std::map<double, Wavelength>::iterator ray = curLSrc->light.find(curLSrc->EnergyNextUse);
				if (ray != curLSrc->light.end())
				{
					std::map<double, Wavelength>::iterator nray = ray;
					nray++;	//get the next light source here
					if (nray != curLSrc->light.end())	//did it exist
					{
						curLSrc->EnergyNextUse = nray->first;	//this will be the next thing it tries to do
						if (nextEnergy == -1.0 || (nray->first < nextEnergy-eTolerance))
							nextEnergy = nray->first;
					}
					else
						curLSrc->EnergyNextUse = -1.0;
				}
				else
				{
					curLSrc->EnergyNextUse = -1.0;
					continue;	//this ray is invalid, so don't try and process it!
				}

	//		}
	//		for (std::map<double, Wavelength>::iterator ray = curLSrc->light.begin(); ray != curLSrc->light.end(); ++ray)
	//		{
				if (ray->second.IsCutOffPowerInit == false)
					ray->second.cutOffPower = ray->second.intensity * area * LIGHT_INTENSITY_CUTOFF;

				tmpRay.Initialize(curLSrc, ray->second);	//creates a new pointer for ray tracing
				tmpRay.powerLeft = ray->second.intensity * area;
				if (AcceptLight(tmpRay.waveData, mdesc.simulation))	//convert the light to a binned energy and process if it's high/low enough energy
				{
					RayList.push_back(tmpRay);
					ProcessRays(RayList, mdl, mdesc, singlePt, mdesc.simulation->EnabledRates.RecyclePhotonsAffectRates, true);	//the ray and all reflections are immediately taken care of. Any sources are added in
				}
				ctr++;
				std::time(&end);	//load the end time for dif
				timeelapse = difftime(end, start);
				if (timeelapse > 2.0)	//if tasks are taking a LONG time, notify the user that it's still running every
				{	//10 seconds
					if (kThread)
					{
						start = end;
						kThread->shareData.KernelshortProgBarPosition = ctr;
						kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
					}
					else if (timeelapse > 10.0)
					{
						DisplayProgress(54, ctr);
						DisplayProgress(29, curLSrc->light.size());
						start = end;
					}

				}
			} //end if this was a source needing processing this time
			else if (curLSrc->EnergyNextUse > 0.0 && (nextEnergy == -1.0 || curLSrc->EnergyNextUse < nextEnergy - eTolerance))	//nextEnergy hasn't been assigned
			{ //valid next energy, and it is less than what there currently is or it hasn't been assigned yet
				nextEnergy = curLSrc->EnergyNextUse;
			}
		}

		ctr = 0;

		if (mdesc.simulation->EnabledRates.LightEmissions == true)	//no need to do the following points
		{
	/*		if (kThread)
			{
				kThread->shareData.KernelshortProgBarPosition = 0;
				kThread->shareData.KernelshortProgBarRange = mdl.numpoints;
				kThread->shareData.KernelcurInfoMsg = MSG_LIGHTEMISSIONS;
			}
	*/
			//the spectrum from all the recombination occuring in the points
			for (std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
			{
				if (curPt->second.LightEmissions == NULL)
					continue;
				curLSrc = curPt->second.LightEmissions;
				ctr++;
				if (curLSrc->enabled == false)	//if it is currently disabled, don't add the light in
					continue;

				if (curLSrc->EnergyNextUse == -1.0)	//this light source is done! Most likely thing to happen
					continue;

				if (curLSrc->EnergyNextUse < currentEnergy + eTolerance && curLSrc->EnergyNextUse > currentEnergy - eTolerance)
				{
					std::map<double, Wavelength>::iterator ray = curLSrc->light.find(curLSrc->EnergyNextUse);
					if (ray != curLSrc->light.end())
					{
						std::map<double, Wavelength>::iterator nray = ray;
						nray++;	//get the next light source here
						if (nray != curLSrc->light.end())	//did it exist
						{
							curLSrc->EnergyNextUse = nray->first;	//this will be the next thing it tries to do
							if (nextEnergy == -1.0 || (nray->first < nextEnergy - eTolerance))
								nextEnergy = nray->first;
						}
						else
							curLSrc->EnergyNextUse = -1.0;
					}
					else
					{
						curLSrc->EnergyNextUse = -1.0;
						continue;	//this ray is invalid, so don't try and process it!
					}
//				for (std::map<double, Wavelength>::iterator ray = curLSrc->light.begin(); ray != curLSrc->light.end(); ++ray)
//				{
					if (ray->second.IsCutOffPowerInit == false)
						ray->second.cutOffPower = ray->second.intensity * area * LIGHT_INTENSITY_CUTOFF;

					if ((ray->second.DirEmitted & (EMIT_PX | EMIT_NX)) != (EMIT_PX | EMIT_NX))	//it can still emit light - hasn't been added in yet
					{
						if (AcceptLight(&ray->second, mdesc.simulation))	//convert it if necessary
						{
							tmpRay.Initialize();	//create a ray going in the positive x direction with half the power
							tmpRay.waveData = &(ray->second);
							tmpRay.curPt = &(curPt->second);
							tmpRay.enteredDevice = true;
							tmpRay.powerLeft = ray->second.intensity * area;	//look at total power
							tmpRay.powerLeft = tmpRay.powerLeft * 0.5;	//half goe sin each direction
							if ((ray->second.DirEmitted & EMIT_PX) == EMIT_NONE)
							{
								tmpRay.direction.x = 1.0;
								tmpRay.position = curPt->second.position;
								tmpRay.position.x = curPt->second.mxmy.x;	//it is moving in the positive direction!
								ray->second.DirEmitted = ray->second.DirEmitted | EMIT_PX;	//flag that this has been done now
								RayList.push_back(tmpRay);
							}
							if ((ray->second.DirEmitted & EMIT_NX) == EMIT_NONE)
							{
								//and do the point in the other direction
								tmpRay.direction.x = -1.0;
								tmpRay.position = curPt->second.position;
								tmpRay.position.x = curPt->second.pxpy.x;	//it is moving in the positive direction!
								ray->second.DirEmitted = ray->second.DirEmitted | EMIT_NX;	//flag that this has been done now
								RayList.push_back(tmpRay);
							}
							ProcessRays(RayList, mdl, mdesc, singlePt, true, mdesc.simulation->EnabledRates.RecyclePhotonsAffectRates);	//they are immediately done
						}
					}
				} //end if the current energy was good
				else if (curLSrc->EnergyNextUse > 0.0 && (nextEnergy == -1.0 || curLSrc->EnergyNextUse < nextEnergy - eTolerance))	//nextEnergy hasn't been assigned
				{ //valid next energy, and it is less than what there currently is or it hasn't been assigned yet
					nextEnergy = curLSrc->EnergyNextUse;
				}

				std::time(&end);	//load the end time for dif
				timeelapse = difftime(end, start);
				if (timeelapse > 2.0)	//if tasks are taking a LONG time, notify the user that it's still running every
				{	//10 seconds
					if (kThread)
					{
						start = end;
						kThread->shareData.KernelshortProgBarPosition = ctr;
						kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE, 0);
					}
					else if (timeelapse > 10.0)
					{
						DisplayProgress(55, ctr);
						DisplayProgress(29, mdl.numpoints);
						start = end;
					}
				}
			}
		}
		PostAbsorbLight(mdl, mdesc.simulation, singlePt);	//the data needs to be added in for each wavelength individually because the data gets cleared!
		currentEnergy = nextEnergy;	
	}	//end while(currentEnergy != -1.0)
	
	
	return(0);
}

int ProcessRays(std::list<RayTrace>& Raylist, Model& mdl, ModelDescribe& mdesc, Point* singlePt, bool allowMerge, bool calcRates)
{
	while(Raylist.size() > 0)
	{
		RayTrace1D(mdl, mdesc.simulation, Raylist.front(), Raylist, singlePt, allowMerge, calcRates);
		Raylist.pop_front();	//clear the memory of the ray, and move on to the next one. Prevent balancing and just grab the first one on the tree
	}
	return(0);
}

//must have list to add reflection traces in
int RayTrace1D(Model& mdl, Simulation* sim, RayTrace& ray, std::list<RayTrace>& Raytree, Point* singlePt, bool allowMerge, bool calcRates)
{
	
//	double velocity = SPEEDLIGHT * 100 / GetRelIndexRefraction(ray, ray.curPt);
//	double posX = ray.position.x; unused
//	double time;	//just a temp variable used for the appropriate velocity bit
	double boundaryX;

	if(ray.curPt == NULL)	//this needs to enter or exit the device.
	{
		Point* FirstPt = NULL;		
		if(ray.direction.x > 0)
		{
			Point* tmpPt = NULL;
			//find boundary that it will hit
			for(unsigned int i=0; i<mdl.boundaries.size(); i++)
			{
				tmpPt = &(mdl.gridp.find(mdl.boundaries[i])->second);	//should always return a valid value
				if(tmpPt)
				{
					if(FirstPt == NULL && tmpPt->pxpy.x > ray.position.x)	//the point is to the right
						FirstPt = tmpPt;
					else if(tmpPt->pxpy.x > ray.position.x && tmpPt->position.x < FirstPt->position.x)	//if the first condition is true, then FirstPt must exist and you can check against it's position
						FirstPt = tmpPt;
				}
			}
			if(FirstPt == NULL)	//the ray is moving right, not currently in a point, 
			{
				LightExit(sim, ray);
				return(0);	//nothing else to do with this particular ray
			}
			boundaryX = FirstPt->mxmy.x;
		}
		else if(ray.direction.x < 0)
		{
			Point* tmpPt = NULL;
			//find boundary that it will hit
			for(unsigned int i=0; i<mdl.boundaries.size(); i++)
			{
				tmpPt = &(mdl.gridp.find(mdl.boundaries[i])->second);	//should always return a valid value
				if(tmpPt)
				{
					if(FirstPt == NULL && tmpPt->pxpy.x < ray.position.x)	//the point is to the left
						FirstPt = tmpPt;
					else if(FirstPt)
					{
						if(tmpPt->position.x > FirstPt->position.x && tmpPt->pxpy.x < ray.position.x)
							FirstPt = tmpPt;
					}
						
				}
			}
			if(FirstPt == NULL)	//the ray is moving right, not currently in a point, 
			{
				LightExit(sim, ray);
				return(0);	//nothing else to do with this particular ray
			}
			boundaryX = FirstPt->pxpy.x;
		}
		else
		{
			boundaryX = 0.0;
			ray.powerLeft = 0.0;	//it has no direction defined, so just get out
		}
		ray.position.x = boundaryX;
		//at this point, a First Point exists and it is at the proper boundary.
		LightEnter(sim, ray, FirstPt, Raytree);	//ray is set to be in first point now.
		
	}

	//at this point, the ray is now inside of a point. We now continually refract the ray until it leaves and generate new rays for reflections
	//with a +1 added.
	Point* nextPt = NULL;
	XSpace TravelDist;
	double absorbLength;
	double ratio;
	while(ray.curPt != NULL && ray.powerLeft > ray.cutoffPowerRedFactor * ray.waveData->cutOffPower && ray.reflectCount <= sim->maxLightReflections)	//there must be enough power left to justify continuiing the calculation, and it must be in the device!
	{
		if(ray.direction.x > 0)
		{
			nextPt = ray.curPt->adj.adjPX;
			if(allowMerge && ray.curPt->LightEmissions)
			{
				std::map<double,Wavelength>::iterator fndSrc = ray.curPt->LightEmissions->light.find(ray.waveData->energyphoton);
				if(fndSrc != ray.curPt->LightEmissions->light.end())
				{
					if(ray.curPt->LightEmissions->direction.x >= 0.0)	//include (0,0,0), but this is 1D right now
					{
						if((fndSrc->second.DirEmitted & EMIT_PX) == EMIT_NONE)	//it hasn't emitted in the positive direction yet
						{
							if(ray.curPt->LightEmissions->direction.x == 0.0)
								ray.powerLeft += fndSrc->second.intensity * ray.curPt->area.x * 0.5;	//area.x = y * z
							else
								ray.powerLeft += fndSrc->second.intensity * ray.curPt->area.x;	//area.x = y * z
							fndSrc->second.DirEmitted = fndSrc->second.DirEmitted | EMIT_PX;	//make sure this doesn't get emitted again
							if(fndSrc->second.IsCutOffPowerInit == false)
								fndSrc->second.cutOffPower = fndSrc->second.intensity * ray.curPt->area.x * LIGHT_INTENSITY_CUTOFF;
							if(fndSrc->second.cutOffPower < ray.waveData->cutOffPower)
								ray.waveData->cutOffPower = fndSrc->second.cutOffPower;	//grab the smallest power that matters
						}
					}
				}
			}
		}
		else if(ray.direction.x < 0)
		{
			nextPt = ray.curPt->adj.adjMX;
			if(allowMerge && ray.curPt->LightEmissions)
			{
				std::map<double,Wavelength>::iterator fndSrc = ray.curPt->LightEmissions->light.find(ray.waveData->energyphoton);
				if(fndSrc != ray.curPt->LightEmissions->light.end())
				{
					if(ray.curPt->LightEmissions->direction.x <= 0.0)	//include (0,0,0), but this is 1D right now
					{
						if((fndSrc->second.DirEmitted & EMIT_NX) == EMIT_NONE)	//it hasn't emitted in the negative direction yet
						{
							if(ray.curPt->LightEmissions->direction.x == 0.0)
								ray.powerLeft += fndSrc->second.intensity * ray.curPt->area.x * 0.5;	//area.x = y * z
							else
								ray.powerLeft += fndSrc->second.intensity * ray.curPt->area.x;	//area.x = y * z

							fndSrc->second.DirEmitted = fndSrc->second.DirEmitted | EMIT_NX;	//make sure this doesn't get emitted again
							if(fndSrc->second.IsCutOffPowerInit == false)
								fndSrc->second.cutOffPower = fndSrc->second.intensity * ray.curPt->area.x * LIGHT_INTENSITY_CUTOFF;
							if(fndSrc->second.cutOffPower < ray.waveData->cutOffPower)
								ray.waveData->cutOffPower = fndSrc->second.cutOffPower;	//grab the smallest power that matters
						}
					}
				}
			}
		}

//		velocity = SPEEDLIGHT * 100.0 / GetRelIndexRefraction(ray, ray.curPt);	//get it in cm/s - this is the total speed of the light, independent of angle.
		TravelDist.x = ray.curPt->width.x;
		/*time*/ratio = fabs(TravelDist.x / (/*velocity * */ ray.direction.x));	//note the ray direction is normalized to magnitude one - just get the x component velocity
		TravelDist = ray.direction * ratio;//velocity * time;
/*		TravelDist.x = velocity * ray.direction.x * time;	//make sure it's the correct sign
		TravelDist.y = velocity * ray.direction.y * time;
		TravelDist.z = velocity * ray.direction.z * time;*/
		absorbLength = TravelDist.Magnitude();
		
		ray.position += TravelDist;	//move it along it's path

		AbsorbLight(ray, absorbLength, sim, singlePt, calcRates);

		if(nextPt != NULL)
		{
			LightChangePoints(ray, nextPt, sim, Raytree);
			ray.curPt = nextPt;	//do not want this in Light Exit, because LightExit causes a reflection of the ray (but not by creating a new ray)
			//so it would shift in position, but stay in the same point.
		}
		else //it will be exiting the device after this point
		{
			LightExit(sim, ray);	//any refracted rays are recorded as leaving the device, and the ray is reflected back through if appropriate
		}
		
	}

	return(0);
}

int LightChangePoints(RayTrace& ray, Point* ptT, Simulation* sim, std::list<RayTrace>& Raytree)
{
	if(ptT == NULL)
		return(0);

	Point* ptI = ray.curPt;	//initial point - this can also not be null or it wouldn't be in the while loop that called this

	double reduceInit = ptI->sizeScaling;
	double reduceTar = ptT->sizeScaling;
	RayTrace newRay = ray;	//initialize the data


	if(ray.inNonexistentVolume==false && reduceInit <= reduceTar)	//they take up the same amount of volume, so just check for normal relection/refraction stuff
	{ //or it is in the material and going into a position that will only interact with material in the other one
		if(ptI->MatProps != ptT->MatProps)
		{
			XSpace LightDirection = ray.direction;	//this should be normalized to distance one.
	
			XSpace PerpPlane;
			if(ray.direction.x > 0.0)
				PerpPlane.x = -1.0;
			else
				PerpPlane.x = 1.0;

			PerpPlane.y = 0.0;
			PerpPlane.z = 0.0;
	
			double nI = GetRelIndexRefraction(ray, ptI);	//safe and valid answers if no point
			double nT = GetRelIndexRefraction(ray, ptT);	//or no material properties defined

			double thetaI = 0.0;
			double thetaT = 0.0;
			double Reflection = 1.0;
			double Transmission = 0.0;

			thetaI = FindThetaVectortailtoTail(LightDirection, PerpPlane, false);
			//determine amount of reflection...
			bool Refract = SnellLaw(nI, nT, thetaI, thetaT);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely

			XSpace ReflectVector, RefractVector;
			FindReflectionVector(LightDirection, PerpPlane, ReflectVector);
			if(Refract)	//not total internal reflection
			{
				FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nT, RefractVector);
				double costhI = cos(thetaI);
				double costhT = cos(thetaT);
				double para = FresnelEq(nI, costhI, nT, costhT, true);
				double perp = FresnelEq(nI, costhI, nT, costhT, false);
				para = para*para;	//needs it squared to be for intensity and not E-Field
				perp = perp*perp;
				Reflection = 0.5 * (para + perp);
				Transmission = 1.0 - Reflection;
			}

			double flux = ray.powerLeft * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
			if(ptT->MatProps && sim->outputs.QE)
				ptT->MatProps->AddOpticalData(BulkOpticalData::lightIncident, ray.waveData->energyphoton, flux);


			//generate the new ray appropriately

			
			newRay.reflectCount++;
			newRay.direction = ReflectVector;
			newRay.powerLeft = ray.powerLeft * Reflection;

			if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
				newRay.cutoffPowerRedFactor = newRay.cutoffPowerRedFactor * Reflection;
			//the reflected ray stays in the same point area.


			if(newRay.reflectCount <= sim->maxLightReflections && newRay.powerLeft >= newRay.waveData->cutOffPower * newRay.cutoffPowerRedFactor)
				Raytree.push_back(newRay);		//don't do this if it will be too many reflections or if it's absorbed enough light

			//and update the old ray values
			ray.direction = RefractVector;
			ray.powerLeft = ray.powerLeft * Transmission;
			if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
				ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * Transmission;


			flux = ray.powerLeft * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
			if(ptT->MatProps && sim->outputs.QE)
				ptT->MatProps->AddOpticalData(BulkOpticalData::lightEnter, ray.waveData->energyphoton, flux);
				
			if(ptI->MatProps && sim->outputs.QE)
			{
				if(ray.direction.x > 0)
					ptT->MatProps->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux);
				else
					ptT->MatProps->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux);
			}
		}
	}
	else if(ray.inNonexistentVolume && reduceInit <= reduceTar)
	{
		//in this situation, it is in "vacuum" and it is going into a volume that has material and potentially a volume that is vacuum.
		//there  is a refract ray, a reflect ray, and a no change ray.

		XSpace LightDirection = ray.direction;	//this should be normalized to distance one.
	
		XSpace PerpPlane;
		if(ray.direction.x > 0.0)
			PerpPlane.x = -1.0;
		else
			PerpPlane.x = 1.0;

		PerpPlane.y = 0.0;
		PerpPlane.z = 0.0;
	
		double nI = 1.0;	//safe and valid answers if no point
		double nT = GetRelIndexRefraction(ray, ptT);	//or no material properties defined

		double thetaI = 0.0;
		double thetaT = 0.0;
		double Reflection = 1.0;
		double Transmission = 0.0;

		thetaI = FindThetaVectortailtoTail(LightDirection, PerpPlane, false);
		//determine amount of reflection...
		bool Refract = SnellLaw(nI, nT, thetaI, thetaT);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely

		XSpace ReflectVector, RefractVector;
		FindReflectionVector(LightDirection, PerpPlane, ReflectVector);
		if(Refract)	//not total internal reflection
		{
			FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nT, RefractVector);
			double costhI = cos(thetaI);
			double costhT = cos(thetaT);
			double para = FresnelEq(nI, costhI, nT, costhT, true);
			double perp = FresnelEq(nI, costhI, nT, costhT, false);
			para = para*para;	//needs it squared to be for intensity and not E-Field
			perp = perp*perp;
			Reflection = 0.5 * (para + perp);
			Transmission = 1.0 - Reflection;
		}

		//generate the new rays appropriately

		//first get the proportions of each
		double PropStayVac = 1.0/(1.0 - reduceInit);	//note, temp variable initially
		double PropHitMaterial = (reduceTar - reduceInit) * PropStayVac;	//note, if same, then the vacuum light will not hit vacuum light. Good.
		PropStayVac = (1.0 - reduceTar) * PropStayVac;	//here, if they are the same, then it is (1-x)/(1-x) = 1, which also matches

		double flux = ray.powerLeft * PropHitMaterial * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s

		if(ptT->MatProps && sim->outputs.QE)
			ptT->MatProps->AddOpticalData(BulkOpticalData::lightIncident, ray.waveData->energyphoton, flux);

		
		newRay.direction = RefractVector;
		newRay.powerLeft = ray.powerLeft * PropHitMaterial * Transmission;
		if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
			newRay.cutoffPowerRedFactor = newRay.cutoffPowerRedFactor * PropHitMaterial * Transmission;
		newRay.inNonexistentVolume = false;
		newRay.curPt = ptT;

		flux = newRay.powerLeft * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
		if(ptT->MatProps && sim->outputs.QE)
			ptT->MatProps->AddOpticalData(BulkOpticalData::lightEnter, ray.waveData->energyphoton, flux);
		//in a nonexistent volume, so there should not have been a light enter in the initial material, so no light exit either
		
		if(newRay.powerLeft >= newRay.waveData->cutOffPower * newRay.cutoffPowerRedFactor && newRay.powerLeft > 0.0)
			Raytree.push_back(newRay);		//don't do this if it will be too many reflections or if it's absorbed enough light

		//and the reflection ray
		newRay.direction = ReflectVector;
		newRay.powerLeft = ray.powerLeft * PropHitMaterial * Reflection;
		if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
			newRay.cutoffPowerRedFactor = newRay.cutoffPowerRedFactor * PropHitMaterial * Reflection;
		newRay.inNonexistentVolume = true;	//should have been set to true, but just making a point
		newRay.reflectCount++;
		newRay.curPt = ptI;

		if(newRay.powerLeft >= newRay.waveData->cutOffPower * newRay.cutoffPowerRedFactor && newRay.powerLeft > 0.0)
			Raytree.push_back(newRay);	//don't do this if it will be too many reflections or if it's absorbed enough light (or if the ray shouldn't go...

		//and update the old ray values
		ray.powerLeft = ray.powerLeft * PropStayVac;	//this much didn't reflect/refract off the material
		ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * PropStayVac;
	}
	else if(ray.inNonexistentVolume == false && reduceInit > reduceTar)	//it's in material, and entering a narrow region
	{
		XSpace LightDirection = ray.direction;	//this should be normalized to distance one.
	
		XSpace PerpPlane;
		if(ray.direction.x > 0.0)
			PerpPlane.x = -1.0;
		else
			PerpPlane.x = 1.0;

		PerpPlane.y = 0.0;
		PerpPlane.z = 0.0;
	
		double nI = GetRelIndexRefraction(ray, ptI);	//safe and valid answers if no point
		double nT = GetRelIndexRefraction(ray, ptT);	//or no material properties defined
		double nTVac = 1.0;

		double thetaI = 0.0;
		double thetaTV = 0.0;
		double thetaTM = 0.0;
		double ReflectionV = 1.0;
		double TransmissionV = 0.0;
		double ReflectionM = 1.0;
		double TransmissionM = 0.0;

		thetaI = FindThetaVectortailtoTail(LightDirection, PerpPlane, false);
		XSpace ReflectVector;
		FindReflectionVector(LightDirection, PerpPlane, ReflectVector);

		XSpace RefractVectorVac;
		//determine amount of reflection...
		bool RefractVac = SnellLaw(nI, nTVac, thetaI, thetaTV);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely
		if(RefractVac)	//not total internal reflection
		{
			FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nTVac, RefractVectorVac);
			double costhI = cos(thetaI);
			double costhT = cos(thetaTV);
			double para = FresnelEq(nI, costhI, nTVac, costhT, true);
			double perp = FresnelEq(nI, costhI, nTVac, costhT, false);
			para = para*para;	//needs it squared to be for intensity and not E-Field
			perp = perp*perp;
			ReflectionV = 0.5 * (para + perp);
			TransmissionV = 1.0 - ReflectionV;
		}

		XSpace RefractVectorM;
		bool RefractM = SnellLaw(nI, nT, thetaI, thetaTM);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely
		if(RefractM)	//not total internal reflection
		{
			FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nT, RefractVectorM);
			double costhI = cos(thetaI);
			double costhT = cos(thetaTM);
			double para = FresnelEq(nI, costhI, nT, costhT, true);
			double perp = FresnelEq(nI, costhI, nT, costhT, false);
			para = para*para;	//needs it squared to be for intensity and not E-Field
			perp = perp*perp;
			ReflectionM = 0.5 * (para + perp);
			TransmissionM = 1.0 - ReflectionM;
		}

		//generate the new rays appropriately. Reflect for material, reflect for vac, refract for material (same ray), refract for vac
		double PropStayMatl = reduceTar / reduceInit;
		double PropToVac = 1.0 - PropStayMatl;


		if(ptT->MatProps != ptI->MatProps && sim->outputs.QE)	//record all the proper light incident(T)/enter(T)/leaving(S) materials for the
		{
			double flux = ray.powerLeft * PropStayMatl * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
			if(ptT->MatProps)
			{
				ptT->MatProps->AddOpticalData(BulkOpticalData::lightIncident, ray.waveData->energyphoton, flux);
				ptT->MatProps->AddOpticalData(BulkOpticalData::lightEnter, ray.waveData->energyphoton, flux * TransmissionM);
			}

			//the flux leaving the material is the flux transmitted into the material and transmitted into the vacuum
			double flux2 = ray.powerLeft * PropToVac * TransmissionV * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
			if(ptI->MatProps)
			{
				if(ray.direction.x > 0)
					ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux * TransmissionM + flux2);
				else
					ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux * TransmissionM + flux2);
			}
		}
		else if(sim->outputs.QE)//they are the same material, but you still have to deal with the vacuum/material interface
		{
			double flux = ray.powerLeft * PropToVac * TransmissionV * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
			if(ptI->MatProps)
			{
				if(ray.direction.x > 0)
					ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux);
				else
					ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux);
			}
		}

		//first, refraction vacuum
		newRay.direction = RefractVectorVac;
		newRay.powerLeft = ray.powerLeft * PropToVac * TransmissionV;
		if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
			newRay.cutoffPowerRedFactor = newRay.cutoffPowerRedFactor * PropToVac * TransmissionV;
		newRay.inNonexistentVolume = true;
		newRay.curPt = ptT;
		
		if(newRay.powerLeft >= newRay.waveData->cutOffPower * newRay.cutoffPowerRedFactor && newRay.powerLeft > 0.0)
			Raytree.push_back(newRay);	//don't do this if it will be too many reflections or if it's absorbed enough light (or if the ray shouldn't go...

		//and reflection from the vacuum AND material portion
		newRay.direction = ReflectVector;
		newRay.powerLeft = ray.powerLeft * (PropToVac * ReflectionV + PropStayMatl * ReflectionM);
		if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
			newRay.cutoffPowerRedFactor = newRay.cutoffPowerRedFactor * (PropToVac * ReflectionV + PropStayMatl * ReflectionM);
		newRay.inNonexistentVolume = false;
		newRay.curPt = ptI;
		newRay.reflectCount++;
		
		if(newRay.powerLeft >= newRay.waveData->cutOffPower * newRay.cutoffPowerRedFactor && newRay.powerLeft > 0.0)
			Raytree.push_back(newRay);	//don't do this if it will be too many reflections or if it's absorbed enough light (or if the ray shouldn't go...

		//and refraction into the new material - this is the one that keeps going
		ray.direction = RefractVectorM;
		ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * PropStayMatl * TransmissionM;
		ray.powerLeft = ray.powerLeft * PropStayMatl * TransmissionM;

	}
	/*
	else if(ray.inNonexistentVolume && reduceInit > reduceTar)	//vacuum area, going into a smaller region
	{
		;	//everything should pass through with no changes whatsoever
	}
	*/

	return(0);
}

int LightEnter(Simulation* sim, RayTrace& ray, Point* ptT, std::list<RayTrace>& Raytree)
{
	if(ptT == NULL)
		return(0);

	XSpace LightDirection = ray.direction;	//this should be normalized to distance one.
	RayTrace newRays = ray;	//initialize data
	
	XSpace PerpPlane;
	if(ray.direction.x > 0.0)
		PerpPlane.x = -1.0;
	else
		PerpPlane.x = 1.0;
	PerpPlane.y = 0.0;
	PerpPlane.z = 0.0;

	double nI = 1.0;
	double nT = GetRelIndexRefraction(ray, ptT);
	//the angle at which the photon packet is striking the plane --- 0.0 = head on

	//get angle of refraction - need angle of incidence for sin(theta's).
	double thetaI = 0.0;
	double thetaT = 0.0;
	double Reflection = 1.0;
	double Transmission = 0.0;

	if(ptT->sizeScaling > 0.0 && ptT->sizeScaling < 1.0)
	{
		newRays.powerLeft = newRays.powerLeft * (1.0 - ptT->sizeScaling);
		newRays.inNonexistentVolume = true;
		newRays.curPt = ptT;
		newRays.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * (1.0 - ptT->sizeScaling);

		ray.powerLeft -= newRays.powerLeft;	//get rid of the power in the old ray.
		ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * ptT->sizeScaling;

		Raytree.push_back(newRays);
	}
	

	thetaI = FindThetaVectortailtoTail(LightDirection, PerpPlane, false);
	//determine amount of reflection
	bool Refract = SnellLaw(nI, nT, thetaI, thetaT);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely

	XSpace ReflectVector, RefractVector;
	
	FindReflectionVector(LightDirection, PerpPlane, ReflectVector);
	if(Refract)	//not total reflection
	{
		FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nT, RefractVector);
		double costhI = cos(thetaI);
		double costhT = cos(thetaT);
		double para = FresnelEq(nI, costhI, nT, costhT, true);
		double perp = FresnelEq(nI, costhI, nT, costhT, false);
		para = para*para;	//needs it squared to be for intensity and not E-Field
		perp = perp*perp;
		Reflection = 0.5 * (para + perp);	//this is the reflection based on no guaranteed transmittance
		Transmission = 1.0 - Reflection;
		RefractVector.Normalize();
		ray.direction = RefractVector;
		ray.curPt = ptT;
	}

	double flux = ray.powerLeft * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
	if(sim->outputs.QE)
	{
		sim->AddOpticalData(BulkOpticalData::lightIncident, ray.waveData->energyphoton, flux);
		ptT->MatProps->AddOpticalData(BulkOpticalData::lightIncident, ray.waveData->energyphoton, flux); //also say incident on material
	}

	double transmit = Transmission * ray.powerLeft;	//this light stays in and enters device
//	double reflect = ray.powerLeft - transmit; nothing is really done with this light	//this light is removed
	ray.powerLeft = transmit;	//change the return variable!
	ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * Transmission;	//reduce the cutoff in the event most is reflected

	if(sim->outputs.QE)
	{
		flux = transmit * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
		sim->AddOpticalData(BulkOpticalData::lightEnter, ray.waveData->energyphoton, flux);	//transmitted photons enter
		ptT->MatProps->AddOpticalData(BulkOpticalData::lightEnter, ray.waveData->energyphoton, flux); 	//also say enters material

	}

	return(SUCCESS);
	
}
int LightExit(Simulation* sim, RayTrace& ray)	//this is a ray that is exiting the device
{
	if(ray.inNonexistentVolume)	//the easy one...
	{
		double flux = ray.powerLeft * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
		if(ray.direction.x > 0)
			sim->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux);
		else
			sim->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux);
		return(0);
	}

	XSpace LightDirection = ray.direction;	//this should be normalized to distance one.
	
	XSpace PerpPlane;
	if(ray.direction.x > 0.0)
		PerpPlane.x = -1.0;
	else
		PerpPlane.x = 1.0;
	PerpPlane.y = 0.0;
	PerpPlane.z = 0.0;
	Point* ptI = ray.curPt;

	double nI = GetRelIndexRefraction(ray, ptI);
	double nT = 1.0;	//vacuum...
	//the angle at which the photon packet is striking the plane --- 0.0 = head on

	//get angle of refraction - need angle of incidence for sin(theta's).
	double thetaI = 0.0;
	double thetaT = 0.0;
	double Reflection = 1.0;
	double Transmission = 0.0;

	thetaI = FindThetaVectortailtoTail(LightDirection, PerpPlane, false);
	//determine amount of reflection
	bool Refract = SnellLaw(nI, nT, thetaI, thetaT);	//calculates angle of refracted beam. Apparently not needed. Can use vectors entirely

	XSpace ReflectVector, RefractVector;
	
	FindReflectionVector(LightDirection, PerpPlane, ReflectVector);
	if(Refract)	//not total reflection
	{
		FindRefractionVector(LightDirection, PerpPlane, thetaI, nI, nT, RefractVector);
		double costhI = cos(thetaI);
		double costhT = cos(thetaT);
		double para = FresnelEq(nI, costhI, nT, costhT, true);
		double perp = FresnelEq(nI, costhI, nT, costhT, false);
		para = para*para;	//needs it squared to be for intensity and not E-Field
		perp = perp*perp;
		Reflection = 0.5 * (para + perp);	//this is the reflection based on no guaranteed transmittance
		Transmission = 1.0 - Reflection;
	}
	
	double transmit = Transmission * ray.powerLeft;	//find the amount of light that leaves the device.
	if(sim->outputs.QE)
	{

		double flux = transmit * 0.001 * QI / ray.waveData->energyphoton;	//mW --> W = J/s --> eV/s --> photons(carriers)/s
		if(ray.direction.x > 0)
		{
			sim->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux);
			
			if(ptI)	//it might not have made it into a material
				ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeavePos, ray.waveData->energyphoton, flux);
		}
		else
		{
			sim->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux);
			if(ptI)	//it might not have made it into a material
				ptI->MatProps->AddOpticalData(BulkOpticalData::lightLeaveNeg, ray.waveData->energyphoton, flux);
		}
	}

	//now just bounce the ray back as opposed to creating a new ray.
	ray.powerLeft = ray.powerLeft - transmit;	//this light is removed
	ray.direction = ReflectVector;
	ray.reflectCount++;
	if(ray.reflectCount < 2)	//only knock it down if it has already been reflected a few times
		ray.cutoffPowerRedFactor = ray.cutoffPowerRedFactor * Reflection;

	return(SUCCESS);
	
	//now the harder version
}

int UpdateLightEnable(ModelDescribe& mdesc)
{
	if(mdesc.simulation->TransientData && mdesc.simulation->simType == SIMTYPE_TRANSIENT)
	{
		double time = mdesc.simulation->TransientData->iterEndtime;
		std::map<double,bool>::iterator ptrNode;
		for(unsigned int i=0; i<mdesc.spectrum.size(); i++)
		{
			ptrNode = MapFindFirstLessEqual(mdesc.spectrum[i]->times, time);
			if(ptrNode != mdesc.spectrum[i]->times.end())
				mdesc.spectrum[i]->enabled = ptrNode->second;
		}
	}
	else if(mdesc.simulation->SteadyStateData && mdesc.simulation->simType==SIMTYPE_SS)
	{
		for(unsigned int i=0; i<mdesc.spectrum.size(); i++)
			mdesc.spectrum[i]->enabled = false;	//just disable all the light

		//now enable the active ones
		int reuseLight = mdesc.simulation->SteadyStateData->ExternalStates[mdesc.simulation->SteadyStateData->CurrentData]->reusePrevLightEnvironment;
		//see if it needs to use a previous light setting. If not, make sure it is the current iteration
		int env = mdesc.simulation->SteadyStateData->CurrentData;
		int extSz = mdesc.simulation->SteadyStateData->ExternalStates.size();
		while(reuseLight >= 0 && reuseLight < extSz)
		{
			env = reuseLight;
			reuseLight = mdesc.simulation->SteadyStateData->ExternalStates[reuseLight]->reusePrevLightEnvironment;
		}
		
		for(unsigned int j=0; j<mdesc.spectrum.size(); ++j)
		{
			//for each spectra listed, check if it is within the active data
			for(unsigned int i=0; i<mdesc.simulation->SteadyStateData->ExternalStates[env]->ActiveLightEnvironment.size(); ++i)
			{
				if(mdesc.spectrum[j]->ID == mdesc.simulation->SteadyStateData->ExternalStates[env]->ActiveLightEnvironment[i])
				{
					mdesc.spectrum[j]->enabled = true;	//found it, enable it
					break;	//now get out and mnove on to the next one
				}
			}
			
		}
		
	}
	return(0);
}

int CalcTransitions(OpticalTransitionData* transitions, Point* pt, unsigned long iter, int thermalize)
{
	//this assumes that all the transitions are present in the data - they just need new numbers.
	unsigned int sz = transitions->TransitionData.size();
	double elecSrc, availSrc, elecTrg, availTrg;
	double WeightA = 0.0;
	double WeightE = 0.0;
	unsigned int ThermIndex = -1;
	unsigned int ELevIndex = -1;
	if(thermalize)
	{
		for(unsigned int i=0; i<pt->ThermData.size(); ++i)
			pt->ThermData[i].SetNormalize(iter);	//make sure all the data is calculated for all the energy states
	}

	for(unsigned int i=0; i<sz; ++i)
	{
		if(transitions->TransitionData[i].source == NULL || transitions->TransitionData[i].target == NULL)
			ProgressString("Invalid transition data found");
		else
		{
			//first let's see if the source is thermalized
			if(transitions->TransitionData[i].source->GetThermalType() & thermalize)	//check to see if the bits match
			{
				//they do, so now we need to find the appropriate indices
				pt->FindThermalizedELevel(ThermIndex, ELevIndex, transitions->TransitionData[i].source);
				if(ThermIndex != -1 && ELevIndex != -1) //both indices are good
				{
					elecSrc = pt->ThermData[ThermIndex].elec[ELevIndex];
					availSrc = pt->ThermData[ThermIndex].hole[ELevIndex];
				}
			}
			else
			{
				elecSrc = transitions->TransitionData[i].source->GetBeginElec();
				availSrc = transitions->TransitionData[i].source->GetBeginHole();
			}

			//now to check if the target has been thermalized or not

			if(transitions->TransitionData[i].target->GetThermalType() & thermalize)
			{
				//we need to see the weight on all the original targets, and then distribute
				//that amongst all the other states
				double origElecTrg = 0.0;
				double origAvailTrg = 0.0;
				for(unsigned int orig = 0; orig < transitions->TransitionData[i].origTarget.size(); ++orig)
				{
					pt->FindThermalizedELevel(ThermIndex, ELevIndex, transitions->TransitionData[i].origTarget[orig]);
					if(ThermIndex != -1 && ELevIndex != -1)	//it found it!
					{
						origElecTrg += pt->ThermData[ThermIndex].elec[ELevIndex];
						origAvailTrg += pt->ThermData[ThermIndex].hole[ELevIndex];
					}
					else
					{
						origElecTrg += transitions->TransitionData[i].origTarget[orig]->GetBeginElec();
						origAvailTrg += transitions->TransitionData[i].origTarget[orig]->GetBeginHole();
					}
				}
				if(origElecTrg == 0.0 && origAvailTrg == 0.0)
				{
					origElecTrg = 1.0;	//make it have no effect because there were no original targets
					origAvailTrg = 1.0;
				}

				//now it's time to actually use the target equilibrium values. The ThermIndex should be correct, and now it's a matter of finding the actually elevel
				pt->FindThermalizedELevel(ThermIndex, ELevIndex, transitions->TransitionData[i].target);
				if(ThermIndex != -1 && ELevIndex != -1)
				{
					if(pt->ThermData[ThermIndex].UseElecPart()==true)
					{
						elecTrg = pt->ThermData[ThermIndex].elec[ELevIndex];
//						availTrg = pt->ThermData[ThermIndex].hole[ELevIndex];
						double ElecSumI = 1.0/pt->ThermData[ThermIndex].GetESum();
//						double AvailSumI = 1.0/pt->ThermData[ThermIndex].GetHSum();

						//now get the adjusted electron target concentration
						elecTrg = origElecTrg * elecTrg * ElecSumI;
						availTrg = origAvailTrg * elecTrg * ElecSumI;
					//elecTrg/elecSum is going to give the proportion necessary to follow fermi distribution

					//the original is the actual weights of what's going in
					//the elecTrg * ElecSumI gets is the fermi fraction occupancy compared throughout the entire band
					}
					else
					{
						elecTrg = pt->ThermData[ThermIndex].EfLowElec[ELevIndex];
//						availTrg = pt->ThermData[ThermIndex].hole[ELevIndex];
						double ElecSumI = pt->ThermData[ThermIndex].GetEfLowSum();
//						double AvailSumI = 1.0/pt->ThermData[ThermIndex].GetHSum();

						//now get the adjusted electron target concentration
						elecTrg = origElecTrg * elecTrg * ElecSumI;
						availTrg = origAvailTrg * elecTrg * ElecSumI;
					}
				}
				else //it didn't find the target energy state in the thermalized stuff, so this is all wrong
				{
					//technically, this should never happen
					elecTrg = transitions->TransitionData[i].target->GetBeginElec();
					availTrg = transitions->TransitionData[i].target->GetBeginHole();
				}
			}
			else
			{
				elecTrg = transitions->TransitionData[i].target->GetBeginElec();
				availTrg = transitions->TransitionData[i].target->GetBeginHole();
			}
			transitions->TransitionData[i].weightAbsorb = elecSrc * availTrg;
			transitions->TransitionData[i].weightEmit = elecTrg * availSrc;
			WeightA += transitions->TransitionData[i].weightAbsorb;
			WeightE += transitions->TransitionData[i].weightEmit;
		}
	}
	transitions->weightAbsorb = WeightA;
	transitions->weightEmit = WeightE;
	transitions->lastIterUpdated = iter;


	return(0);
}

bool ValidELevel(Point* pt, ELevel* eLev, int which, double maxE, double minE)
{
	if(eLev == NULL)
		return(false);
	if(which == THERMALIZE_ACP || which ==THERMALIZE_VBT)
	{
		//then it's just going up from zero
		if(eLev->max >= minE && eLev->min <= maxE)
			return(true);
	}
	else if(which == THERMALIZE_DON || which == THERMALIZE_CBT)
	{
		if(pt==NULL)
			return(false);
		if(pt->eg-eLev->max <= maxE && pt->eg-eLev->min >= minE)
			return(true);
	}
	else if(which == THERMALIZE_VB)
	{
		if(-eLev->max <= maxE && -eLev->min >= minE)
			return(true);
	}
	else if(which == THERMALIZE_CB)
	{
		if(pt == NULL)
			return(false);
		if(pt->eg+eLev->min <= maxE && pt->eg+eLev->max >= minE)
			return(true);
	}
	return(false);

}

int FindThermalizedUpperTransitions(int which, Point* pt, int ThermIndexLow, OpticalTransitionData* transitions, double maxE, double minE, double& weightA, double& weightE,
									TransitionPair& tmpTrans, Simulation* sim, double elecSource, double availSource)
{
	if(sim->EnabledRates.Thermalize && which)
	{
		int ThermIndexTrg = -1;
		//this implies there will be thermalized data in elec/hole
		for(unsigned int i=0; i<pt->ThermData.size(); ++i)
		{
			if(pt->ThermData[i].CheckType(which))
			{
				ThermIndexTrg = i;
			}
		}

		if(ThermIndexTrg != -1)	//then there are thermalized states to find
		{
			//if there weren't, guess not doing anything
			//make sure this is gonna actually result in an energy within the tails
			double /*availTarget,*/ targetElec;
			if(pt->ThermData[ThermIndexTrg].Valid(minE, maxE))
			{
				double tmpWeightA = 0.0;	//the absorption weight going into valid energy states
				double tmpWeightE = 0.0;
				tmpTrans.origTarget.clear();	//make sure it starts out clear
				
				if(pt->ThermData[ThermIndexTrg].UseElecPart()==true)
				{
					for(unsigned int i=0; i<pt->ThermData[ThermIndexTrg].elec.size(); ++i)	//need to build up a temporary partition
					{
						if(ValidELevel(pt, pt->ThermData[ThermIndexTrg].GetELev(i), which, maxE, minE))
						{
							tmpWeightA += elecSource * pt->ThermData[ThermIndexTrg].hole[i];
							tmpWeightE += availSource * pt->ThermData[ThermIndexTrg].elec[i];
							tmpTrans.origTarget.push_back(pt->ThermData[ThermIndexTrg].GetELev(i));
						}
					}
					double totA = pt->ThermData[ThermIndexTrg].GetESum();
					if(totA != 0.0)
						totA = 1.0 / totA;
						
					//we need to look at how much weighting would have gone to the original states
					//and then distribute them across the entire band
					for(unsigned int i=0; i<pt->ThermData[ThermIndexTrg].elec.size(); ++i)
					{
//						availTarget = pt->ThermData[ThermIndexTrg].hole[i];
						targetElec = pt->ThermData[ThermIndexTrg].elec[i];
						tmpTrans.target = pt->ThermData[ThermIndexTrg].GetELev(i);
						tmpTrans.weightAbsorb = tmpWeightA * targetElec * totA;
						tmpTrans.weightEmit = tmpWeightE * targetElec * totA;
						weightA += tmpTrans.weightAbsorb;
						weightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
				else
				{
					for(unsigned int i=0; i<pt->ThermData[ThermIndexTrg].EfLowElec.size(); ++i)	//need to build up a temporary partition
					{
						if(ValidELevel(pt, pt->ThermData[ThermIndexTrg].GetELev(i), which, maxE, minE))
						{
							tmpWeightA += elecSource * pt->ThermData[ThermIndexTrg].hole[i];	//want this proportion entering into absorption for thermalized value
							tmpWeightE += availSource * pt->ThermData[ThermIndexTrg].elec[i];	//would assume there's this much in there to do stimulated emission.
							tmpTrans.origTarget.push_back(pt->ThermData[ThermIndexTrg].GetELev(i));
						}
					}
					double totA = pt->ThermData[ThermIndexTrg].GetEfLowSum();
					double totAE = pt->ThermData[ThermIndexTrg].GetESum();
					if(totAE != 0.0)
						totAE = 1.0 / totAE;

					//we need to look at how much weighting would have gone to the original states
					//and then distribute them across the entire band
					for(unsigned int i=0; i<pt->ThermData[ThermIndexTrg].EfLowElec.size(); ++i)
					{
//						availTarget = pt->ThermData[ThermIndexTrg].hole[i];
						targetElec = pt->ThermData[ThermIndexTrg].EfLowElec[i];
						tmpTrans.target = pt->ThermData[ThermIndexTrg].GetELev(i);
						tmpTrans.weightAbsorb = tmpWeightA * targetElec * totA;
						tmpTrans.weightEmit = tmpWeightE * pt->ThermData[ThermIndexTrg].EfLowElec[i] * totA;	//there has to be electrons there to have stimulated emission to begin with!
						weightA += tmpTrans.weightAbsorb;
						weightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}

				tmpTrans.origTarget.clear();
			}
		}
	}
	return(0);
}

OpticalTransitionData* FindTransitions(double energy, Point* pt, OpticalTransitionData* transitions, Simulation* sim)
{
	if(pt == NULL)
		return(NULL);

	if(transitions == NULL)	//need to create a new one.
	{
		//then it doesn't previously exist
		if(energy < sim->EnabledRates.OpticalEMin || (energy > sim->EnabledRates.OpticalEMax && sim->EnabledRates.OpticalEMax > 0.0))
		{
			//this is not a valid energy for doing any transitions. It should not have gotten this far, but let's save some calculations
			return(NULL);
		}
		if (pt->OpticTransitions == NULL)
		{
			pt->OpticTransitions = new OpticalTransitionData;
			transitions = pt->OpticTransitions;
		}
		else
		{
			transitions = pt->OpticTransitions;
		}

	}

	TransitionPair tmpTrans;	//buffer to copy into vector
	energy = sim->GetBinnedEnergy(energy);
	transitions->energy = energy;
	transitions->lastIterUpdated = sim->curiter;
	transitions->forceFindTransitions = false;	//it's doing it now, so turn off the flag
	transitions->TransitionData.clear();	//we need to recalculate everything
	transitions->TransitionData.reserve(pt->vb.energies.size() + pt->cb.energies.size() + pt->CBTails.size() + pt->VBTails.size() + pt->donorlike.size() + pt->acceptorlike.size());	//should be a relatively safe number of allowed transitions
	double cbbandmin = pt->eg;	//pt->cb.bandmin;
	double vbbandmin = 0.0;//pt->vb.bandmin;	//this is going to be treated as zero
	
	double minEnergy;	//the minimum energy in the search range
	double maxEnergy;	//the maximum energy in the search range
	double relminE;
	double relmaxE;
	double elecSource;	//number of electrons in the source that may be pushed up
	double availTarget;	//number of spots for electrons to be pushed into for the target
	double targetElec;	//number of electrons in the "target" for stimulated emission
	double availSource;	//number of places for electrons in the source for stimulated emission
	MaxMin<double> fndValue;
	double WeightA = 0.0;	//avoid dereferencing pointers constantly
	double WeightE = 0.0;
	double beta = pt->temp>0 ? KBI / pt->temp : 0.0;

	//easiest way is to make sure all the data is properly adjusted beforehand - we need to use elec and hole vectors, though
	for(unsigned int i=0; i<pt->ThermData.size(); ++i)
		pt->ThermData[i].SetNormalize(sim->curiter);
	
	int ThermSrcEH = 0;
	int ThermIndexSrc = -1;
	int ThermIndexTrg = -1;
	//adjust the transitions to have VBMin at zero!
	for(std::multimap<MaxMin<double>,ELevel>::iterator vbSit = pt->vb.energies.begin(); vbSit != pt->vb.energies.end(); ++vbSit)
	{
		
		if(sim->EnabledRates.Thermalize & THERMALIZE_VB)	//want the weights coming from each to be thermalized to improve speed.
		{ //this is NOT physical, but the thermalization step will cause this output to occur. This means the rates try to
			if(ThermIndexSrc == -1)	//only do this once for the VB
			{
				for(unsigned int i=0; i<pt->ThermData.size(); ++i)
				{
					if(pt->ThermData[i].CheckType(THERMALIZE_VB))
					{
						ThermIndexSrc = i;
						ThermSrcEH = pt->ThermData[i].elec.size()-1;	//first one most likely
					}
				}
			}
			ThermSrcEH = pt->ThermData[ThermIndexSrc].FindUniqueID(vbSit->second.uniqueID, ThermSrcEH);
			if(ThermSrcEH == -1)
			{
				double totOcc = pt->vb.GetNumParticles();	//push it into a state it can't get into which leads to SMALL timesteps
				double fermi = -log(totOcc/pt->vb.EffectiveDensity)/beta;
				double nStates = vbSit->second.GetNumstates();
				fermi = beta*(vbSit->second.eUse+fermi);
				fermi = 1.0/(1.0 + exp(fermi));
				availSource = nStates * fermi;
				elecSource = nStates - availSource;
			}
			else
			{
				elecSource = pt->ThermData[ThermIndexSrc].elec[ThermSrcEH];
				availSource = pt->ThermData[ThermIndexSrc].hole[ThermSrcEH];
				ThermSrcEH--;
			}
			
		}
		else
		{
			elecSource = vbSit->second.GetBeginElec();
			availSource = vbSit->second.GetBeginHole();
		}

		maxEnergy = -vbSit->second.min + energy;
		minEnergy = -vbSit->second.max + energy;
		tmpTrans.source = &(vbSit->second);
		//now search through the impurities and band tails - first, check the band tails
		if(maxEnergy > 0.0 && pt->VBTails.size() > 0)	//vb Band min is 0.0 eV
		{
			if(sim->EnabledRates.Thermalize && THERMALIZE_VBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_VBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
				//this needs to be in an if statement solely so the else beneath it happens
			}
			else
			{
			//the band tails are valid
				relminE = minEnergy;	//was - vbbandmin before, but vbbandmin=0 in new setup
				relmaxE = maxEnergy;
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::multimap<MaxMin<double>,ELevel>::iterator btailsL = pt->VBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->VBTails.begin())
					--btailsL;
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::multimap<MaxMin<double>,ELevel>::iterator btailsU = pt->VBTails.upper_bound(fndValue);	//first one > to the energy
				for(std::multimap<MaxMin<double>,ELevel>::iterator it = btailsL; it!=btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->acceptorlike.size() > 0)
		{
			//there are acceptors
			relminE = minEnergy;
			relmaxE = maxEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)
			{
				FindThermalizedUpperTransitions(THERMALIZE_ACP, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				if(pt->acceptorlike.begin()->second.min <= relmaxE && pt->acceptorlike.rbegin()->second.max >= relminE)	//check if there are any in range to justify doing the searches
				{
					fndValue.max = relminE;
					fndValue.min = fndValue.max;
					std::multimap<MaxMin<double>,ELevel>::iterator btailsL = pt->acceptorlike.lower_bound(fndValue);	//first one >= to the energy,
					if(btailsL != pt->acceptorlike.begin())
					{
						--btailsL;
						if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
							++btailsL;
					}
					fndValue.max = relmaxE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->acceptorlike.upper_bound(fndValue);	//first one > to the energy
					for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
					{ //all these will be valid
						availTarget = it->second.GetBeginHole();
						targetElec = it->second.GetBeginElec();
						tmpTrans.target = &(it->second);
						tmpTrans.weightAbsorb = elecSource * availTarget;
						tmpTrans.weightEmit = targetElec * availSource;
						WeightA += tmpTrans.weightAbsorb;
						WeightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
			}
		}

		if(pt->CBTails.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->CBTails.begin()->second.min <= relmaxE && pt->CBTails.rbegin()->second.max >= relminE)
			{
				fndValue.max = cbbandmin - maxEnergy;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->CBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->CBTails.begin())
				{
					--btailsL;	//this is guaranteed to be adjacent
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->CBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->donorlike.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_DON)
			{
				FindThermalizedUpperTransitions(THERMALIZE_DON, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->donorlike.begin()->second.min <= relmaxE && pt->donorlike.rbegin()->second.max >= relminE)
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->donorlike.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->donorlike.begin())
				{
					--btailsL;
					if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
						++btailsL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->donorlike.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(maxEnergy > cbbandmin)
		{
			//the CB is valid
			relmaxE = maxEnergy - cbbandmin;
			relminE = minEnergy - cbbandmin;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CB)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CB, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbL = pt->cb.energies.lower_bound(fndValue);	//first one >= to the energy,
				if(cbL != pt->cb.energies.begin())
				{
					--cbL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbU = pt->cb.energies.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = cbL; it != cbU; ++it)
				{ //all these will be valid
					targetElec = it->second.GetBeginElec();
					availTarget = it->second.GetBeginHole();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}	
			}
		}
		tmpTrans.source->OpticalRates.reserve(pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->cb.energies.size());
	}	//end going through all the valence band states

	ThermIndexSrc = -1;	//reset
	//now do the valence band tails
	for (std::map<MaxMin<double>, ELevel>::iterator vbSit = pt->VBTails.begin(); vbSit != pt->VBTails.end(); ++vbSit)
	{

		if(sim->EnabledRates.Thermalize & THERMALIZE_VBT)	//want the weights coming from each to be thermalized to improve speed.
		{ //this is NOT physical, but the thermalization step will cause this output to occur. This means the rates try to
			if(ThermIndexSrc == -1)	//only do this once for the VB
			{
				for(unsigned int i=0; i<pt->ThermData.size(); ++i)
				{
					if(pt->ThermData[i].CheckType(THERMALIZE_VBT))
					{
						ThermIndexSrc = i;
						ThermSrcEH = 0;	//first one most likely
					}
				}
			}
			ThermSrcEH = pt->ThermData[ThermIndexSrc].FindUniqueID(vbSit->second.uniqueID, ThermSrcEH);
			if(ThermSrcEH == -1)
			{
				elecSource = vbSit->second.GetBeginElec();	//couldn't find thermalized data, so just use current data
				availSource = vbSit->second.GetBeginHole();
			}
			else
			{
				elecSource = pt->ThermData[ThermIndexSrc].elec[ThermSrcEH];
				availSource = pt->ThermData[ThermIndexSrc].hole[ThermSrcEH];
				ThermSrcEH++;
			}
			
		}
		else
		{
			elecSource = vbSit->second.GetBeginElec();
			availSource = vbSit->second.GetBeginHole();
		}

		
		minEnergy = vbbandmin + vbSit->second.min + energy;
		maxEnergy = vbbandmin + vbSit->second.max + energy;
		//now search through the impurities and band tails - first, check the band tails
		tmpTrans.source = &(vbSit->second);

		if(maxEnergy > 0.0 && pt->VBTails.size() > 0)	//vb Band min is 0.0 eV
		{
			if(sim->EnabledRates.Thermalize && THERMALIZE_VBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_VBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
				//this needs to be in an if statement solely so the else beneath it happens
			}
			else
			{
			//the band tails are valid
				relminE = minEnergy;	//was - vbbandmin before, but vbbandmin=0 in new setup
				relmaxE = maxEnergy;
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->VBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->VBTails.begin())
					--btailsL;
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->VBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->acceptorlike.size() > 0)
		{
			//there are acceptors
			relminE = minEnergy;
			relmaxE = maxEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)
			{
				FindThermalizedUpperTransitions(THERMALIZE_ACP, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				if(pt->acceptorlike.begin()->second.min <= relmaxE && pt->acceptorlike.rbegin()->second.max >= relminE)	//check if there are any in range to justify doing the searches
				{
					fndValue.max = relminE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->acceptorlike.lower_bound(fndValue);	//first one >= to the energy,
					if(btailsL != pt->acceptorlike.begin())
					{
						--btailsL;
						if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
							++btailsL;
					}
					fndValue.max = relmaxE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->acceptorlike.upper_bound(fndValue);	//first one > to the energy
					for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
					{ //all these will be valid
						availTarget = it->second.GetBeginHole();
						targetElec = it->second.GetBeginElec();
						tmpTrans.target = &(it->second);
						tmpTrans.weightAbsorb = elecSource * availTarget;
						tmpTrans.weightEmit = targetElec * availSource;
						WeightA += tmpTrans.weightAbsorb;
						WeightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
			}
		}

		if(pt->CBTails.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->CBTails.begin()->second.min <= relmaxE && pt->CBTails.rbegin()->second.max >= relminE)
			{
				fndValue.max = cbbandmin - maxEnergy;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->CBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->CBTails.begin())
				{
					--btailsL;	//this is guaranteed to be adjacent
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->CBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->donorlike.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_DON)
			{
				FindThermalizedUpperTransitions(THERMALIZE_DON, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->donorlike.begin()->second.min <= relmaxE && pt->donorlike.rbegin()->second.max >= relminE)
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->donorlike.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->donorlike.begin())
				{
					--btailsL;
					if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
						++btailsL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->donorlike.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(maxEnergy > cbbandmin)
		{
			//the CB is valid
			relmaxE = maxEnergy - cbbandmin;
			relminE = minEnergy - cbbandmin;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CB)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CB, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbL = pt->cb.energies.lower_bound(fndValue);	//first one >= to the energy,
				if(cbL != pt->cb.energies.begin())
				{
					--cbL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbU = pt->cb.energies.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = cbL; it != cbU; ++it)
				{ //all these will be valid
					targetElec = it->second.GetBeginElec();
					availTarget = it->second.GetBeginHole();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}	
			}
		}
		tmpTrans.source->OpticalRates.reserve(pt->vb.energies.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->cb.energies.size());
	}	//end going through all the valence band tail states

	ThermIndexSrc = -1;
	//and the acceptor states
	for (std::map<MaxMin<double>, ELevel>::iterator vbSit = pt->acceptorlike.begin(); vbSit != pt->acceptorlike.end(); ++vbSit)
	{
		if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)	//want the weights coming from each to be thermalized to improve speed.
		{ //this is NOT physical, but the thermalization step will cause this output to occur. This means the rates try to

			//because there are multiple acceptors, you must check this every time
			for(unsigned int i=0; i<pt->ThermData.size(); ++i)
			{
				if(pt->ThermData[i].CheckType(THERMALIZE_ACP))
				{
					ThermSrcEH = pt->ThermData[i].FindUniqueID(vbSit->second.uniqueID, ThermSrcEH);
					if(ThermSrcEH != -1)	//is this energy level within this particular thermalization group for acceptors?
					{
						ThermIndexSrc = i;
						break;	//stop looking for the data
					}
					else
						ThermIndexSrc = -1;	//it hasn't been found
				}
				else
					ThermIndexSrc = -1;	//it hasn't been found
			}

			
			if(ThermSrcEH == -1)
			{
				elecSource = vbSit->second.GetBeginElec();	//couldn't find thermalized data, so just use current data
				availSource = vbSit->second.GetBeginHole();
			}
			else
			{
				elecSource = pt->ThermData[ThermIndexSrc].elec[ThermSrcEH];
				availSource = pt->ThermData[ThermIndexSrc].hole[ThermSrcEH];
				ThermSrcEH++;	//update the next most likely guess
			}
			
		}
		else
		{
			elecSource = vbSit->second.GetBeginElec();
			availSource = vbSit->second.GetBeginHole();
		}
		
		minEnergy = vbbandmin + vbSit->second.min + energy;
		maxEnergy = vbbandmin + vbSit->second.max + energy;
		tmpTrans.source = &(vbSit->second);
		
		if(maxEnergy > 0.0 && pt->VBTails.size() > 0)	//vb Band min is 0.0 eV
		{
			if(sim->EnabledRates.Thermalize && THERMALIZE_VBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_VBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
				//this needs to be in an if statement solely so the else beneath it happens
			}
			else
			{
			//the band tails are valid
				relminE = minEnergy;	//was - vbbandmin before, but vbbandmin=0 in new setup
				relmaxE = maxEnergy;
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->VBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->VBTails.begin())
					--btailsL;
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->VBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->acceptorlike.size() > 0)
		{
			//there are acceptors
			relminE = minEnergy;
			relmaxE = maxEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)
			{
				FindThermalizedUpperTransitions(THERMALIZE_ACP, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				if(pt->acceptorlike.begin()->second.min <= relmaxE && pt->acceptorlike.rbegin()->second.max >= relminE)	//check if there are any in range to justify doing the searches
				{
					fndValue.max = relminE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->acceptorlike.lower_bound(fndValue);	//first one >= to the energy,
					if(btailsL != pt->acceptorlike.begin())
					{
						--btailsL;
						if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
							++btailsL;
					}
					fndValue.max = relmaxE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->acceptorlike.upper_bound(fndValue);	//first one > to the energy
					for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
					{ //all these will be valid
						availTarget = it->second.GetBeginHole();
						targetElec = it->second.GetBeginElec();
						tmpTrans.target = &(it->second);
						tmpTrans.weightAbsorb = elecSource * availTarget;
						tmpTrans.weightEmit = targetElec * availSource;
						WeightA += tmpTrans.weightAbsorb;
						WeightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
			}
		}

		if(pt->CBTails.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->CBTails.begin()->second.min <= relmaxE && pt->CBTails.rbegin()->second.max >= relminE)
			{
				fndValue.max = cbbandmin - maxEnergy;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->CBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->CBTails.begin())
				{
					--btailsL;	//this is guaranteed to be adjacent
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->CBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->donorlike.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_DON)
			{
				FindThermalizedUpperTransitions(THERMALIZE_DON, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->donorlike.begin()->second.min <= relmaxE && pt->donorlike.rbegin()->second.max >= relminE)
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->donorlike.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->donorlike.begin())
				{
					--btailsL;
					if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
						++btailsL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->donorlike.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(maxEnergy > cbbandmin)
		{
			//the CB is valid
			relmaxE = maxEnergy - cbbandmin;
			relminE = minEnergy - cbbandmin;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CB)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CB, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbL = pt->cb.energies.lower_bound(fndValue);	//first one >= to the energy,
				if(cbL != pt->cb.energies.begin())
				{
					--cbL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbU = pt->cb.energies.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = cbL; it != cbU; ++it)
				{ //all these will be valid
					targetElec = it->second.GetBeginElec();
					availTarget = it->second.GetBeginHole();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}	
			}
		}
		tmpTrans.source->OpticalRates.reserve(pt->vb.energies.size() + pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->cb.energies.size() - 1);
	}	//end going through all the acceptor states

	//and the donors
	for (std::map<MaxMin<double>, ELevel>::iterator don = pt->donorlike.begin(); don != pt->donorlike.end(); ++don)
	{
		if(sim->EnabledRates.Thermalize & THERMALIZE_DON)	//want the weights coming from each to be thermalized to improve speed.
		{ //this is NOT physical, but the thermalization step will cause this output to occur. This means the rates try to
			//because there are multiple acceptors, you must check this every time
			for(unsigned int i=0; i<pt->ThermData.size(); ++i)
			{
				
				if(pt->ThermData[i].CheckType(THERMALIZE_DON))
				{
					ThermSrcEH = pt->ThermData[i].FindUniqueID(don->second.uniqueID, ThermSrcEH);	//this will be inefficient on the first go through
					if(ThermSrcEH != -1)	//is this energy level within this particular thermalization group for acceptors?
					{
						ThermIndexSrc = i;
						break;	//stop looking for the data
					}
					else
						ThermIndexSrc = -1;	//it hasn't been found
				}
				else
					ThermIndexSrc = -1;	//it hasn't been found
			}

			
			
			if(ThermSrcEH == -1)
			{
				elecSource = don->second.GetBeginElec();	//couldn't find thermalized data, so just use current data
				availSource = don->second.GetBeginHole();
			}
			else
			{
				elecSource = pt->ThermData[ThermIndexSrc].elec[ThermSrcEH];
				availSource = pt->ThermData[ThermIndexSrc].hole[ThermSrcEH];
				ThermSrcEH--;
			}
			
		}
		else
		{
			elecSource = don->second.GetBeginElec();
			availSource = don->second.GetBeginHole();
		}
		
		maxEnergy = cbbandmin - don->second.min + energy;
		minEnergy = cbbandmin - don->second.max + energy;
		//now search through the impurities and band tails - first, check the band tails
		tmpTrans.source = &(don->second);

		if(maxEnergy > 0.0 && pt->VBTails.size() > 0)	//vb Band min is 0.0 eV
		{
			if(sim->EnabledRates.Thermalize && THERMALIZE_VBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_VBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
				//this needs to be in an if statement solely so the else beneath it happens
			}
			else
			{
			//the band tails are valid
				relminE = minEnergy;	//was - vbbandmin before, but vbbandmin=0 in new setup
				relmaxE = maxEnergy;
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->VBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->VBTails.begin())
					--btailsL;
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->VBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->acceptorlike.size() > 0)
		{
			//there are acceptors
			relminE = minEnergy;
			relmaxE = maxEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)
			{
				FindThermalizedUpperTransitions(THERMALIZE_ACP, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				if(pt->acceptorlike.begin()->second.min <= relmaxE && pt->acceptorlike.rbegin()->second.max >= relminE)	//check if there are any in range to justify doing the searches
				{
					fndValue.max = relminE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->acceptorlike.lower_bound(fndValue);	//first one >= to the energy,
					if(btailsL != pt->acceptorlike.begin())
					{
						--btailsL;
						if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
							++btailsL;
					}
					fndValue.max = relmaxE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->acceptorlike.upper_bound(fndValue);	//first one > to the energy
					for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
					{ //all these will be valid
						availTarget = it->second.GetBeginHole();
						targetElec = it->second.GetBeginElec();
						tmpTrans.target = &(it->second);
						tmpTrans.weightAbsorb = elecSource * availTarget;
						tmpTrans.weightEmit = targetElec * availSource;
						WeightA += tmpTrans.weightAbsorb;
						WeightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
			}
		}

		if(pt->CBTails.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->CBTails.begin()->second.min <= relmaxE && pt->CBTails.rbegin()->second.max >= relminE)
			{
				fndValue.max = cbbandmin - maxEnergy;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->CBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->CBTails.begin())
				{
					--btailsL;	//this is guaranteed to be adjacent
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->CBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->donorlike.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_DON)
			{
				FindThermalizedUpperTransitions(THERMALIZE_DON, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->donorlike.begin()->second.min <= relmaxE && pt->donorlike.rbegin()->second.max >= relminE)
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->donorlike.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->donorlike.begin())
				{
					--btailsL;
					if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
						++btailsL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->donorlike.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(maxEnergy > cbbandmin)
		{
			//the CB is valid
			relmaxE = maxEnergy - cbbandmin;
			relminE = minEnergy - cbbandmin;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CB)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CB, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbL = pt->cb.energies.lower_bound(fndValue);	//first one >= to the energy,
				if(cbL != pt->cb.energies.begin())
				{
					--cbL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbU = pt->cb.energies.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = cbL; it != cbU; ++it)
				{ //all these will be valid
					targetElec = it->second.GetBeginElec();
					availTarget = it->second.GetBeginHole();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}	
			}
		}
		tmpTrans.source->OpticalRates.reserve(pt->vb.energies.size() + pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->cb.energies.size() - 1);
	}	//end going through all the donor states

	//and the CBTails
	for (std::map<MaxMin<double>, ELevel>::iterator cbt = pt->CBTails.begin(); cbt != pt->CBTails.end(); ++cbt)
	{
		if(sim->EnabledRates.Thermalize & THERMALIZE_DON)	//want the weights coming from each to be thermalized to improve speed.
		{ //this is NOT physical, but the thermalization step will cause this output to occur. This means the rates try to
			if(ThermIndexSrc == -1)	//only do this once for the VB
			{
				for(unsigned int i=0; i<pt->ThermData.size(); ++i)
				{
					if(pt->ThermData[i].CheckType(THERMALIZE_DON))
					{
						ThermIndexSrc = i;
						ThermSrcEH = pt->ThermData[i].elec.size() - 1;	//first one most likely
					}
				}
			}
			ThermSrcEH = pt->ThermData[ThermIndexSrc].FindUniqueID(cbt->second.uniqueID, ThermSrcEH);
			if(ThermSrcEH == -1)
			{
				elecSource = cbt->second.GetBeginElec();	//couldn't find thermalized data, so just use current data
				availSource = cbt->second.GetBeginHole();
			}
			else
			{
				elecSource = pt->ThermData[ThermIndexSrc].elec[ThermSrcEH];
				availSource = pt->ThermData[ThermIndexSrc].hole[ThermSrcEH];
				ThermSrcEH--;
			}
			
		}
		else
		{
			elecSource = cbt->second.GetBeginElec();
			availSource = cbt->second.GetBeginHole();
		}

		maxEnergy = cbbandmin - cbt->second.min + energy;
		minEnergy = cbbandmin - cbt->second.max + energy;
		//now search through the impurities and band tails - first, check the band tails
		if(maxEnergy > 0.0 && pt->VBTails.size() > 0)	//vb Band min is 0.0 eV
		{
			if(sim->EnabledRates.Thermalize && THERMALIZE_VBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_VBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
				//this needs to be in an if statement solely so the else beneath it happens
			}
			else
			{
			//the band tails are valid
				relminE = minEnergy;	//was - vbbandmin before, but vbbandmin=0 in new setup
				relmaxE = maxEnergy;
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->VBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->VBTails.begin())
					--btailsL;
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->VBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->acceptorlike.size() > 0)
		{
			//there are acceptors
			relminE = minEnergy;
			relmaxE = maxEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_ACP)
			{
				FindThermalizedUpperTransitions(THERMALIZE_ACP, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				if(pt->acceptorlike.begin()->second.min <= relmaxE && pt->acceptorlike.rbegin()->second.max >= relminE)	//check if there are any in range to justify doing the searches
				{
					fndValue.max = relminE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->acceptorlike.lower_bound(fndValue);	//first one >= to the energy,
					if(btailsL != pt->acceptorlike.begin())
					{
						--btailsL;
						if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
							++btailsL;
					}
					fndValue.max = relmaxE;
					fndValue.min = fndValue.max;
					std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->acceptorlike.upper_bound(fndValue);	//first one > to the energy
					for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
					{ //all these will be valid
						availTarget = it->second.GetBeginHole();
						targetElec = it->second.GetBeginElec();
						tmpTrans.target = &(it->second);
						tmpTrans.weightAbsorb = elecSource * availTarget;
						tmpTrans.weightEmit = targetElec * availSource;
						WeightA += tmpTrans.weightAbsorb;
						WeightE += tmpTrans.weightEmit;
						transitions->TransitionData.push_back(tmpTrans);
					}
				}
			}
		}

		if(pt->CBTails.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CBT)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CBT, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->CBTails.begin()->second.min <= relmaxE && pt->CBTails.rbegin()->second.max >= relminE)
			{
				fndValue.max = cbbandmin - maxEnergy;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->CBTails.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->CBTails.begin())
				{
					--btailsL;	//this is guaranteed to be adjacent
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->CBTails.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(pt->donorlike.size() > 0)
		{
			//the band tails are valid
			relminE = cbbandmin - maxEnergy;
			relmaxE = cbbandmin - minEnergy;
			if(sim->EnabledRates.Thermalize & THERMALIZE_DON)
			{
				FindThermalizedUpperTransitions(THERMALIZE_DON, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else if(pt->donorlike.begin()->second.min <= relmaxE && pt->donorlike.rbegin()->second.max >= relminE)
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsL = pt->donorlike.lower_bound(fndValue);	//first one >= to the energy,
				if(btailsL != pt->donorlike.begin())
				{
					--btailsL;
					if(relminE > btailsL->first.max || relminE < btailsL->first.min)	//it wasn't good one below, so go back up
						++btailsL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator btailsU = pt->donorlike.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = btailsL; it != btailsU; ++it)
				{ //all these will be valid
					availTarget = it->second.GetBeginHole();
					targetElec = it->second.GetBeginElec();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}
			}
		}

		if(maxEnergy > cbbandmin)
		{
			//the CB is valid
			relmaxE = maxEnergy - cbbandmin;
			relminE = minEnergy - cbbandmin;
			if(sim->EnabledRates.Thermalize & THERMALIZE_CB)
			{
				FindThermalizedUpperTransitions(THERMALIZE_CB, pt, ThermIndexSrc, transitions, maxEnergy, minEnergy, WeightA, WeightE, tmpTrans, sim, elecSource, availSource);
			}
			else
			{
				fndValue.max = relminE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbL = pt->cb.energies.lower_bound(fndValue);	//first one >= to the energy,
				if(cbL != pt->cb.energies.begin())
				{
					--cbL;
				}
				fndValue.max = relmaxE;
				fndValue.min = fndValue.max;
				std::map<MaxMin<double>, ELevel>::iterator cbU = pt->cb.energies.upper_bound(fndValue);	//first one > to the energy
				for (std::map<MaxMin<double>, ELevel>::iterator it = cbL; it != cbU; ++it)
				{ //all these will be valid
					targetElec = it->second.GetBeginElec();
					availTarget = it->second.GetBeginHole();
					tmpTrans.target = &(it->second);
					tmpTrans.weightAbsorb = elecSource * availTarget;
					tmpTrans.weightEmit = targetElec * availSource;
					WeightA += tmpTrans.weightAbsorb;
					WeightE += tmpTrans.weightEmit;
					transitions->TransitionData.push_back(tmpTrans);
				}	
			}
		}
		tmpTrans.source->OpticalRates.reserve(pt->vb.energies.size() + pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size() + pt->cb.energies.size() - 1);
	}	//end going through all the CB Tails states band states

	//avoid reallocations of lots of data...
	for (std::map<MaxMin<double>, ELevel>::iterator cbt = pt->cb.energies.begin(); cbt != pt->cb.energies.end(); ++cbt)
		cbt->second.OpticalRates.reserve(pt->vb.energies.size() + pt->VBTails.size() + pt->acceptorlike.size() + pt->donorlike.size() + pt->CBTails.size());

	transitions->weightAbsorb = WeightA;
	transitions->weightEmit = WeightE;
	
	return(transitions);
}

//this needs to determine how many photons to absorb, and where it can generate carriers to do so. - also does stimulated emission
int AbsorbLight(RayTrace& ray, double distTravel, Simulation* sim, Point* singlePt, bool calcRates)
{
	if(ray.inNonexistentVolume || distTravel <= 0.0)	//there is no absorption for this light.
		return(0);

	double alpha = ray.GetAlpha();
	Point* pt = ray.curPt;
	pt->LightPowerEntry += ray.powerLeft;	//note that this many mW are entering the device

	if(alpha <= 0.0)
		return(0);	//don't absorb any light...

	

	//now need to find the states possible for these absorptions (or emissions)

	//see if an appropriate transition dataset has been created.
//	std::map<double, OpticalTransitionData>::iterator opData = MapFindFirstLessEqual(pt->OpticTransitions, ray.waveData->energyphoton);
	double overallWeightAbsorb;
	double overallWeightEmit;
	OpticalTransitionData* ptrOptics = NULL;
	bool calcTransitions = true;
	bool calccoeffs = true;
	if(pt->OpticTransitions != NULL)
	{
		if(ray.waveData->energyphoton - pt->OpticTransitions->energy < sim->EnabledRates.OpticalEResolution)
		{
			//then this is matching data!
			ptrOptics = pt->OpticTransitions;
			calcTransitions = ptrOptics->forceFindTransitions;	//because it runs out of memory, it will always have to calcTransitions
			if (ptrOptics->lastIterUpdated == sim->curiter)
				calccoeffs = false;
		}
	}
	if(calcTransitions)	//it doesn't exist yet or needs to be redone
		ptrOptics = FindTransitions(ray.waveData->energyphoton, pt, ptrOptics, sim);	//this also does the transition calculation at the same time
	else if(calccoeffs)
		CalcTransitions(ptrOptics, pt, sim->curiter, sim->EnabledRates.Thermalize);	//this is the "light" version of the above
	//fir ptrOptics to be NULL, opData can't be end, or it can't be in the correct resolution
	//in either case, calcTransitions is true, so NULL is passed into find transitions, where it is used.
	//if calcTransitions is false, because the default to forceFindTransitions is going to be false.
	
	overallWeightAbsorb = ptrOptics->weightAbsorb;	//get in local registers
	overallWeightEmit = ptrOptics->weightEmit;

	//always added energy to find the "target"
	if(ptrOptics->weightAbsorb <= 0.0 && ptrOptics->weightEmit <= 0.0)	//there were no possible transitions, so the photons should continue w/out issue
		return(0);

	double totalWeighti = 1.0/(overallWeightAbsorb + overallWeightEmit);

	//power intensity from absorption: P = P_0 * exp(-alpha * w)
	//power intensity from emission: P = P_0 * exp(alpha * w)
	//they use the same coefficient to describe their rate, but the dominating one
	//depends on the relative carrier concentration between the two.
	//however, I'm not taking that to be fullly accurate and am going to go by
	//the product of the states available and the nubmer of carriers
	//therefore, if equal weights, the light ought to not change, which means multiplying 
	//by the product.  Being a product, the absorption coefficient in the exponent
	//of what remains should be the part that is a partition function.

	//this is a similar setup to % scattered, where the rate can be found to be the exponent without the t (distance = velocity * time)
	//you can find dP/dt from P=P_0 * exp(alpha v t SUM{Nc-Nv}/SUM{Nc+Nv} and del_P = Energy/photon * photons/sec --- where photons/sec = 0.5 * (dNv/dt - dNc/dt) as dNv=-dNc.
	//then realizing dP/dt * del_T = del_P, you can get the final equation for the net rate between two states
	// R = photon/energy * Power_Init * alpha * distance * SUM{Nc-Nv}/SUM{Nc+Nv}
	//note, this results in the simple power loss of P=P_0 exp(-alpha x) if only absorption and splits it appropriately between the states
	//simple stimulated emission of P= P_0 exp (alpha x) if no absorption
	//if equal, P = P_0 exp(alpha x * 0) = P_0 with no change and absorb rate = stimemit rate.

	//there are places for this particular photon to be absorbed, so it is definitely absorbed...
	double absorbRate, StimEmitRate;
	double expConst = distTravel * alpha * totalWeighti;
	double expVal;
	double PowerConst = ray.powerLeft * 0.001 * QI / ray.waveData->energyphoton ;	//mW --> W = J/s --> eV/s --> eV/s photon / eV cm^-1 cm = photon/s
	double PowerChange = overallWeightEmit - overallWeightAbsorb;
	if(PowerChange != 0.0)	//if it's zero, then you can get away with the simple approximation
	{
		double PCi = 1.0 / PowerChange;
		expVal = exp(PowerChange * totalWeighti * alpha * distTravel);
		PowerChange = expVal - 1.0;	//we're integrating Pdx over the distance travelled
		absorbRate = PowerConst  * PowerChange * PCi;
		StimEmitRate = absorbRate * ptrOptics->weightEmit;
		absorbRate = absorbRate * ptrOptics->weightAbsorb;
	}
	else //Ws = Wa, taylor expand, which avoids divide by zero, then left with A/(A+S) or S/(A+S), A=S, so 0.5
	{
		expVal = 1.0;
		absorbRate = PowerConst * alpha * distTravel * 0.5;
		StimEmitRate = absorbRate;
	}
	
	//overallWeightAbsorb = absorb / (overallWeightAbsorb * ray.waveData->energyphoton);	//for getting probability of what should be absorbed... one division as * is faster
	//also save on multiplying each bit by the number absorbed. So we have eV/s * (indiv / overall) / (eV/photon) = photon/sec

	
	int index = sim->GetOpticalIndex(ray.waveData->energyphoton);	//index in the point data
	if((singlePt==NULL || singlePt == pt) && calcRates)	//only add absorption rates to a particular point, so only calculate derivatives for those points
		pt->OpticalOut->AddAbsorbEmit(index, absorbRate, StimEmitRate, PowerConst * alpha*distTravel*expVal);	//the total absorb/emission rate
	ray.powerLeft = ray.powerLeft * exp(expConst * (overallWeightEmit - overallWeightAbsorb));

	return(0);
}

int CalcDerivLight(Point* pt, OpticalTransitionData& opData, double derivFactor, double alphax)
{
	if(derivFactor == 0.0)	//there was no light transition data here, so skip all these obnoxious calculations
		return(0);

	double deriv = 0.0;
	
	ELevel *src, *trg, *wrt;
	src = trg = wrt = NULL;

	double dwai_dc, dwsi_dc, dwa_dc, dws_dc;

	TransitionPair* transition;
	double Wa, Ws, Wai, Wsi;
	double eleclow, holelow, elechigh, holehigh;

	unsigned int szOp = opData.TransitionData.size();
	//first, get all the energy states used inside the vector
	Wa = opData.weightAbsorb;
	Ws = opData.weightEmit;
	double den = Ws - Wa;
	if(den != 0.0)
		den = 1.0/den;
	double denindiv = Ws * Ws - Wa * Wa;
	if(denindiv != 0.0)
		denindiv = 1.0 / denindiv;
	double factorend = (Wa - Ws) / (Wa + Ws);

	//loop through every transition for this particular energy level
	for(unsigned int i=0; i<szOp; ++i)
	{
		transition = &(opData.TransitionData[i]);
		src = transition->source;
		trg = transition->target;
		Wai = transition->weightAbsorb;
		Wsi = transition->weightEmit;

		//every transition has derivatives wrt to every other transition
		for(unsigned int cmp=0; cmp<szOp; ++cmp)
		{
			//now we know that the source is going to be the lower energy
			
			
			wrt = opData.TransitionData[cmp].source;
			//first we'll calculate values with respect to the source in this particular group
			dwa_dc = dws_dc = 0.0;	//reset the values
			//we're calculating wrt to this particular state. This state could correspond with many transitions, so gotta check them all!
			for(unsigned int cmp2=0; cmp2<szOp; ++cmp2)
			{
				if(wrt == opData.TransitionData[cmp2].source)
				{
					if(opData.TransitionData[cmp2].source->carriertype==ELECTRON)
					{
					//need to know what sign to make it as wrt to carrier
						dwa_dc += opData.TransitionData[cmp2].target->GetBeginHole();	//source is low and matches, positive sign
						dws_dc -= opData.TransitionData[cmp2].target->GetBeginElec();	//source is low and doesn't match, negative sign
					}
					else
					{
						dwa_dc -= opData.TransitionData[cmp2].target->GetBeginHole();	//source is low, doesn't match, negative sign
						dws_dc += opData.TransitionData[cmp2].target->GetBeginElec();	//source is low, matches, positive sign
					}
				}
				else if(wrt == opData.TransitionData[cmp2].target)	//now we want to be pulling the carriers from the source
				{
					if(opData.TransitionData[cmp2].target->carriertype==ELECTRON)
					{
					//need to know what sign to make it as wrt to carrier
						dwa_dc -= opData.TransitionData[cmp2].source->GetBeginElec();	//e_low * h_high, deriv wrt high, carrier type differ, -
						dws_dc += opData.TransitionData[cmp2].source->GetBeginHole();	//e_high * h_low, deriv wrt high, carrier type matches, +
					}
					else
					{
						dwa_dc += opData.TransitionData[cmp2].source->GetBeginElec();	//e_low * h_high, deriv wrt high, carrier type matches, +
						dws_dc -= opData.TransitionData[cmp2].source->GetBeginHole();	//e_high * h_low, deriv wrt high, carrier type differ, -
					}
				}
			}

			opData.TransitionData[cmp].source->GetOccupancyEH(RATE_OCC_BEGIN, eleclow, holelow);
			opData.TransitionData[cmp].target->GetOccupancyEH(RATE_OCC_BEGIN, elechigh, holehigh);

			if(opData.TransitionData[cmp].source == opData.TransitionData[i].source || opData.TransitionData[cmp].source == opData.TransitionData[i].target)
			{
				if(opData.TransitionData[cmp].source->carriertype==ELECTRON)
				{
					//need to know what sign to make it as wrt to carrier
					dwai_dc = holehigh;	//source is low and matches, positive sign
					dwsi_dc = -elechigh;	//source is low and doesn't match, negative sign
				}
				else
				{
					dwai_dc = -holehigh;	//source is low, doesn't match, negative sign
					dwsi_dc = elechigh;	//source is low, matches, positive sign
				}
			}
			else
			{
				dwai_dc = 0.0;
				dwsi_dc = 0.0;
			}

			double firstterm = derivFactor * (dwai_dc - dwsi_dc);
			double secondterm = derivFactor * den * (Wsi - Wai) * (dws_dc - dwa_dc);
			double thirdterm = alphax * denindiv * (Wai - Wsi);
			double fourthterm = thirdterm * factorend * (dws_dc + dwa_dc);
			thirdterm = thirdterm * (dws_dc - dwa_dc);

			//this is the derivative for the optical absorption minus stimulated emission
			//if the source is a hole, that means the rate was for gaining carriers
			//but if the source was an electron, this is the derivative of the rate out, and we want
			//the rate in.
			if(src->carriertype==ELECTRON)
				deriv = -(firstterm + secondterm + thirdterm + fourthterm);
			else
				deriv = firstterm + secondterm + thirdterm + fourthterm;

			pt->AddDeriv(src, wrt, deriv);


			//when comparing to the target state, we're still investigating this one particular transition
			//we are still calculating the derivative wrt to the same state.




			firstterm = derivFactor * (dwai_dc - dwsi_dc);
			secondterm = derivFactor * den * (Wsi - Wai) * (dws_dc - dwa_dc);
			thirdterm = alphax * denindiv * (Wai - Wsi);
			fourthterm = thirdterm * factorend * (dws_dc + dwa_dc);
			thirdterm = thirdterm * (dws_dc - dwa_dc);


			//now we need to look at the target state.
			//The target will have the |same derivative| for this particular transition 
			//but we need to get the correct sign. If target is electron, the rate is pushing electrons
			//into the target, so positive.
			if(trg->carriertype==HOLE)
				deriv = -(firstterm + secondterm + thirdterm + fourthterm);
			else
				deriv = firstterm + secondterm + thirdterm + fourthterm;

			pt->AddDeriv(trg, wrt, deriv);

			//now do all this wrt to the other one!
			wrt = opData.TransitionData[cmp].target;

			dwa_dc = dws_dc = 0.0;	//reset the values
			//we're calculating wrt to this particular state. This state could correspond with many transitions, so gotta check them all!
			for(unsigned int cmp2=0; cmp2<szOp; ++cmp2)
			{
				if(wrt == opData.TransitionData[cmp2].source)
				{
					if(opData.TransitionData[cmp2].source->carriertype==ELECTRON)
					{
					//need to know what sign to make it as wrt to carrier
						dwa_dc += opData.TransitionData[cmp2].target->GetBeginHole();	//source is low and matches, positive sign
						dws_dc -= opData.TransitionData[cmp2].target->GetBeginElec();	//source is low and doesn't match, negative sign
					}
					else
					{
						dwa_dc -= opData.TransitionData[cmp2].target->GetBeginHole();	//source is low, doesn't match, negative sign
						dws_dc += opData.TransitionData[cmp2].target->GetBeginElec();	//source is low, matches, positive sign
					}
				}
				else if(wrt == opData.TransitionData[cmp2].target)	//now we want to be pulling the carriers from the source
				{
					if(opData.TransitionData[cmp2].target->carriertype==ELECTRON)
					{
					//need to know what sign to make it as wrt to carrier
						dwa_dc -= opData.TransitionData[cmp2].source->GetBeginElec();	//e_low * h_high, deriv wrt high, carrier type differ, -
						dws_dc += opData.TransitionData[cmp2].source->GetBeginHole();	//e_high * h_low, deriv wrt high, carrier type matches, +
					}
					else
					{
						dwa_dc += opData.TransitionData[cmp2].source->GetBeginElec();	//e_low * h_high, deriv wrt high, carrier type matches, +
						dws_dc -= opData.TransitionData[cmp2].source->GetBeginHole();	//e_high * h_low, deriv wrt high, carrier type differ, -
					}
				}
			}

			if(opData.TransitionData[cmp].source == opData.TransitionData[i].source || opData.TransitionData[cmp].source == opData.TransitionData[i].target)
			{
				if(opData.TransitionData[cmp].target->carriertype==ELECTRON)
				{
					//need to know what sign to make it as wrt to carrier
					dwai_dc = -eleclow;	//source is low and matches, positive sign
					dwsi_dc = holelow;	//source is low and doesn't match, negative sign
				}
				else
				{
					dwai_dc = eleclow;	//source is low, doesn't match, negative sign
					dwsi_dc = -holelow;	//source is low, matches, positive sign
				}
			}
			else
			{
				dwai_dc = 0.0;
				dwsi_dc = 0.0;
			}



			firstterm = derivFactor * (dwai_dc - dwsi_dc);
			secondterm = derivFactor * den * (Wsi - Wai) * (dws_dc - dwa_dc);
			thirdterm = alphax * denindiv * (Wai - Wsi);
			fourthterm = thirdterm * factorend * (dws_dc + dwa_dc);
			thirdterm = thirdterm * (dws_dc - dwa_dc);

			//this is the derivative for the optical absorption minus stimulated emission
			//if the source is a hole, that means the rate was for gaining carriers
			//but if the source was an electron, this is the derivative of the rate out, and we want
			//the rate in.
			if(src->carriertype==ELECTRON)
				deriv = -(firstterm + secondterm + thirdterm + fourthterm);
			else
				deriv = firstterm + secondterm + thirdterm + fourthterm;

			pt->AddDeriv(src, wrt, deriv);


			//when comparing to the target state, we're still investigating this one particular transition
			//we are still calculating the derivative wrt to the same state.


			firstterm = derivFactor * (dwai_dc - dwsi_dc);
			secondterm = derivFactor * den * (Wsi - Wai) * (dws_dc - dwa_dc);
			thirdterm = alphax * denindiv * (Wai - Wsi);
			fourthterm = thirdterm * factorend * (dws_dc + dwa_dc);
			thirdterm = thirdterm * (dws_dc - dwa_dc);


			//now we need to look at the target state.
			//The target will have the |same derivative| for this particular transition 
			//but we need to get the correct sign. If target is electron, the rate is pushing electrons
			//into the target, so positive.
			if(trg->carriertype==HOLE)
				deriv = -(firstterm + secondterm + thirdterm + fourthterm);
			else
				deriv = firstterm + secondterm + thirdterm + fourthterm;

			pt->AddDeriv(trg, wrt, deriv);

		}
	}

	return(0);
}

int PostAbsorbLightPt(Point* curPt, Simulation* sim)
{
	//it should have data no matter what because it will have been created in the absorb Light section
	if(sim->SteadyStateData)
	{
		if(sim->SteadyStateData->LightRateFactor == 0.0)
		{
			curPt->OpticalOut->Clear();
			return(0);
		}
	}
	ELevelRate rateDataAbsorb;
	ELevelRate rateDataStimEmis;

	rateDataAbsorb.deriv=rateDataAbsorb.deriv2=rateDataAbsorb.derivsourceonly=0.0;
	rateDataStimEmis.deriv=rateDataStimEmis.deriv2=rateDataStimEmis.derivsourceonly=0.0;

	rateDataAbsorb.carrier = ELECTRON;
	rateDataStimEmis.carrier = ELECTRON;
	double absorbRateT, emitRateT, energy, weightAi, weightEi, absorbRate, StimEmitRate, rate;
	double alphax;
	int index, previndex = -1;

	index = curPt->OpticalOut->UseRemoveNext(absorbRateT, emitRateT, alphax);
	while(index != -1)	//flag to end
	{
		energy = sim->GetEnergyIndex(index);
		if(energy == 0.0)
		{
			index = curPt->OpticalOut->UseRemoveNext(absorbRateT, emitRateT, alphax);	//reset for next time through the loop
			continue;
		}
//		std::map<double,OpticalTransitionData>::iterator opData = MapFindFirstLessEqual(curPt->OpticTransitions, energy);	//THIS MUST EXIST!
		//but be safe anyway
		if (curPt->OpticTransitions->energy != energy)
//		if(opData == curPt->OpticTransitions.end())
		{
			//then it probably shouldn't have existed in the first place.
			index = curPt->OpticalOut->UseRemoveNext(absorbRateT, emitRateT, alphax);	//reset for next time through the loop
			continue;	//restart the loop with the next bit of data
		}
			//the energy is guaranteed to be within the bounds, provided it was fine.
			//now lets adjust the rates appropriately
		weightAi = MY_IS_DEN(curPt->OpticTransitions->weightAbsorb) ? 1.0 / curPt->OpticTransitions->weightAbsorb : 0.0;
		weightEi = MY_IS_DEN(curPt->OpticTransitions->weightEmit) ? 1.0 / curPt->OpticTransitions->weightEmit : 0.0;
		


//		double debug = opData->second.weightAbsorb>0.0?1.0/opData->second.weightAbsorb:0.0;
//		if(MY_IS_FINITE(debug)==false)
//			debug=1.0;

		double derivFactor = absorbRateT * weightAi;	//P_0 / (Ws-Wa) * [exp(a x (Ws-Wa) / (Ws+Wa)) - 1]

/*
This is not used in the Newton-Raphson method. This literally goes through and calculates the derivative for all the states for a change in occupancy of each state based on the energy of the light input, which varies widely.
A GIANT WASTE OF TIME (now)


		if(sim->transient==false)	//avoid tons of unneeded calculations.
			CalcDerivLight(curPt, opData->second, derivFactor, alphax);

*/	
		
		//need to extract data for creating second derivative. This rate may be a result of multiple waves
		//entering at different angles and intensities.
		//all these derivatives are stored within point as opposed to in the actual rate. It would otherwise take a ton more effort
		//to recalculate a bunch of values for each transition within this particular energy level.


		for (std::vector<TransitionPair>::iterator TPair = curPt->OpticTransitions->TransitionData.begin(); TPair != curPt->OpticTransitions->TransitionData.end(); ++TPair)
		{
			if(TPair->weightAbsorb==0.0 && TPair->weightEmit==0.0)
				continue;	//save a bunch of calculations if ain't nuttin' happening
			absorbRate = absorbRateT * weightAi * TPair->weightAbsorb;
			StimEmitRate = emitRateT * weightEi * TPair->weightEmit;
			rate = absorbRate - StimEmitRate;

			rateDataAbsorb.source = TPair->source;	//should be lower band, absorb light, carrier moves up
			rateDataAbsorb.target = TPair->target;
			rateDataAbsorb.netrate = rate;	//want to make sure the rate is positive for positive absorption
		
			rateDataStimEmis.source = TPair->target;	//the rateData Source should always have electrons leaving it
			rateDataStimEmis.target = TPair->source;	//the transition targets are always higher than the transition sources
			rateDataStimEmis.netrate = -rate;
		
			if(TPair->target->imp && TPair->source->imp)	//both the target and source state are impurities
			{
				rateDataStimEmis.type = RATE_STIMEMISDEF;
				rateDataAbsorb.type = RATE_LIGHTDEF;
				if(!curPt->MatProps->isOpticalOutputsSuppressed())
				{
					if(sim->outputs.Ldef)
					{
						sim->AddOpticalData(BulkOpticalData::absorbDef, energy, absorbRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::absorbDef, energy, absorbRate);
					}
					if(sim->outputs.Edef)
					{
						sim->AddOpticalData(BulkOpticalData::stimEmisDef, energy, StimEmitRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::stimEmisDef, energy, StimEmitRate);
					}
				}
			}
			else if(TPair->target->imp || TPair->source->imp)	//both are not due to previous if, but one of them is
			{
				rateDataStimEmis.type = RATE_STIMEMISSRH;
				rateDataAbsorb.type = RATE_LIGHTSRH;
				if (!curPt->MatProps->isOpticalOutputsSuppressed())
				{
					if(sim->outputs.Lsrh)
					{
						sim->AddOpticalData(BulkOpticalData::absorbSRH, energy, absorbRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::absorbSRH, energy, absorbRate);
					}

					if(sim->outputs.Esrh)
					{
						sim->AddOpticalData(BulkOpticalData::stimEmisSRH, energy, StimEmitRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::stimEmisSRH, energy, StimEmitRate);
					}
				}
			}
			else if(TPair->target->carriertype != TPair->source->carriertype)	//no impurities, opposite carrier type: VB/CB (includes tails)
			{	//typical generation from light
				rateDataStimEmis.type = RATE_STIMEMISREC;
				rateDataAbsorb.type = RATE_LIGHTGEN;
				if (!curPt->MatProps->isOpticalOutputsSuppressed())
				{
					if(sim->outputs.Lgen)
					{
						sim->AddOpticalData(BulkOpticalData::absorbBands, energy, absorbRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::absorbBands, energy, absorbRate);
					}
					if(sim->outputs.Erec)
					{
						sim->AddOpticalData(BulkOpticalData::stimEmisBands, energy, StimEmitRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::stimEmisBands, energy, StimEmitRate);
					}
				}
			}
			else if(TPair->target->carriertype == TPair->source->carriertype)
			{
				rateDataStimEmis.type = RATE_STIMEMISSCAT;
				rateDataAbsorb.type = RATE_LIGHTSCAT;
				if (!curPt->MatProps->isOpticalOutputsSuppressed())
				{
					if(sim->outputs.Lscat)
					{
						sim->AddOpticalData(BulkOpticalData::absorbScat, energy, absorbRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::absorbScat, energy, absorbRate);
					}
					if(sim->outputs.Escat)
					{
						sim->AddOpticalData(BulkOpticalData::stimEmisScat, energy, StimEmitRate);
						curPt->MatProps->AddOpticalData(BulkOpticalData::stimEmisScat, energy, StimEmitRate);
					}
				}
			}
			else
			{
				rateDataAbsorb.type = RATE_UNDEFINED;	//warning flag that something is going wrong!
				rateDataStimEmis.type = RATE_UNDEFINED;	//warning flag that something is going wrong!
			}

			if(sim->SteadyStateData)	//don't scale the optical outputs, but scale the net effects
			{
				absorbRate = absorbRate * sim->SteadyStateData->LightRateFactor;	//throw in the constant that limits light rates from
				StimEmitRate = StimEmitRate * sim->SteadyStateData->LightRateFactor;	//affecting all the rates with the smallest number of additional multiplies
			}
		
		//		else it is carriers staying within the band and light is absorbed as heat in exciting carriers higher into band.
			if(rateDataAbsorb.source->carriertype == ELECTRON)
			{ //likewise, rateDataStimEmis.target == ELECTRON as the source/target are flipped, but we only care about what is happening in the absorb source!
			//the absorb source means electrons in the lower level (source) are moving up, and this is an electron, so absorb=RateOut

				rateDataAbsorb.RateOut = absorbRate;
				rateDataAbsorb.RateIn = StimEmitRate;
				rateDataAbsorb.sourcecarrier = ELECTRON;
				rateDataStimEmis.targetcarrier = ELECTRON;

			}
			else //now rateDataStimEmis.target == HOLE as well. Should be same as above except RIN/ROUT flipped
			{
				rateDataAbsorb.RateOut = StimEmitRate;	//now stim emit means electrons in the target (upper) are entering the source, but the source is type hole, so this is the same as holes leaving
				rateDataAbsorb.RateIn = absorbRate;
		
				rateDataAbsorb.sourcecarrier = HOLE;
				rateDataStimEmis.targetcarrier = HOLE;

			}

			if(rateDataStimEmis.source->carriertype == ELECTRON) //and rateDataStimEmit.source == ELECTRON
			{
		//		rateDataAbsorb.target->rateIn += absorbRate;	//electrons are entering the absorbing target, so this should be a rate in
		//		rateDataAbsorb.target->unalteredRIn += absorbRate;
		//		rateDataAbsorb.target->rateOut += StimEmitRate;	//if electrons leave the absorption target, they are leaving via stimulated emission
		//		rateDataAbsorb.target->unalteredROut += StimEmitRate;

				rateDataStimEmis.RateOut = StimEmitRate;	//in stimulated emission, the source is the higher energy level and carriers are falling from it(leaving)
				//so rate out is stimemitrate
				rateDataStimEmis.RateIn = absorbRate;

				rateDataAbsorb.targetcarrier = ELECTRON;
				rateDataStimEmis.sourcecarrier = ELECTRON;
			}
			else //should be the opposite in/out as above because the carrier type changed.
			{
	//			rateDataAbsorb.target->rateOut += absorbRate;	//the absorb target is receiving electrons via absorption, which is the same as holes leaving
	//			rateDataAbsorb.target->unalteredROut += absorbRate;
	//			rateDataAbsorb.target->rateIn += StimEmitRate;	//the absorb target is sending electrons away via simtulated emission, which is the same as holes entering
	//			rateDataAbsorb.target->unalteredRIn += StimEmitRate;

				rateDataStimEmis.RateOut = absorbRate;
				rateDataStimEmis.RateIn = StimEmitRate;
				rateDataAbsorb.targetcarrier = HOLE;
				rateDataStimEmis.sourcecarrier = HOLE;
			}

			rateDataAbsorb.RateType = absorbRate;
			rateDataStimEmis.RateType = StimEmitRate;
			if(sim->TransientData && sim->simType == SIMTYPE_TRANSIENT)
			{
				rateDataAbsorb.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateDataAbsorb);
				rateDataStimEmis.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateDataStimEmis);
			}
			else
			{
				rateDataAbsorb.desiredTStep = rateDataStimEmis.desiredTStep = 0.0;
			}
			rateDataAbsorb.source->AddELevelRate(rateDataAbsorb, true);	//add the rate to the rate tree & update the rate sums
			rateDataStimEmis.source->AddELevelRate(rateDataStimEmis, true);	//add the rate to the rate tree & update the rate sums

			double NRate = DoubleSubtract(rateDataAbsorb.RateIn, rateDataAbsorb.RateOut);
				
			rateDataAbsorb.source->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
			rateDataAbsorb.target->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
			double tmpR = fabs(NRate * (rateDataAbsorb.source->AbsFermiEnergy - rateDataAbsorb.target->AbsFermiEnergy));
			/*
			if(NRate > 0.0)
			{
				if(tmpR > rateDataAbsorb.source->maxREfIn)
					rateDataAbsorb.source->maxREfIn = tmpR;
				rateDataAbsorb.source->RateEfIn += NRate;
			}
			else if(NRate < 0.0)
			{
				if(tmpR > rateDataAbsorb.source->maxREfOut)
					rateDataAbsorb.source->maxREfOut = tmpR;
				rateDataAbsorb.source->RateEfOut -= NRate;
			}
			*/
			NRate = DoubleSubtract(rateDataStimEmis.RateIn, rateDataStimEmis.RateOut);

		//cannot combine because carrier types might be different. It could identical or the opposite
		//so just repeat code - but tmpR is at least the same between both.
			/*
			if(NRate > 0.0)
			{
				if(tmpR > rateDataStimEmis.source->maxREfIn)
					rateDataStimEmis.source->maxREfIn = tmpR;
				rateDataStimEmis.source->RateEfIn += NRate;
			}
			else if(NRate < 0.0)
			{
				if(tmpR > rateDataStimEmis.source->maxREfOut)
					rateDataStimEmis.source->maxREfOut = tmpR;
				rateDataStimEmis.source->RateEfOut -= NRate;
			}
			*/
		}	//end for loop going through all the transition pairs for this particular energy level

		index = curPt->OpticalOut->UseRemoveNext(absorbRateT, emitRateT, alphax);	//reset for next time through the loop
	}	//end while loop going through all the processed energies for this point
	return(0);
}

int PostAbsorbLight(Model& mdl, Simulation* sim, Point* singlePt)
{
	if(singlePt==NULL)
	{
		for(std::map<long int,Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
		{
			PostAbsorbLightPt(&(curPt->second), sim);
		}	//end for loop through all points
	}
	else
		PostAbsorbLightPt(singlePt, sim);

	return(0);
}

double FresnelEq(double nI, double costhI, double nT, double costhT, bool para)
{
	double ret = 0.0;
	double num = 0.0;
	double den = 1.0;
	if(para)
	{
		double I = nI * costhI;	//note, theta's are (-PI/2, PI/2), so this is (0,1], and the n's != 0, >1 so speed does not exceed light
		double T = nT * costhT;
		num = T - I;
		den = T + I;
	}
	else
	{
		double I = nI * costhT;
		double T = nT * costhI;
		num = T - I;
		den = I + T;
	}
	
	if(den == 0.0) 	//index of refraction can't be equal to zero, so this means both costhT/I are zero
	{  //which means we have 0/0
		return(1.0);
	}
	ret = num / den;

	return(ret);
}

//assumes neither nI or nT = 0. GetRelIndexRefraction prevents that from happening
bool SnellLaw(double nI, double nT, double thetaIn, double& thetaRefract)
{
	//if(thetaIn > PID2)	//incident angle can only be 0...pi/2.
		//int debug=0;
	if(nI == nT)
	{
		thetaRefract = thetaIn;
		return(true);
	}
	//n1 sin1 = n2 sin2
	double nratio = nI / nT;
	thetaIn = sin(thetaIn);	//must be 0< sin < 1
	double LHS = nratio * thetaIn;
	if(LHS > 1.0 || LHS < -1.0)
	{
		thetaRefract = 0.0;
		//total intenral reflection
		return(false);
	}
	thetaRefract = asin(LHS);	//returns (-pi.2 to pi/2 - should be OK value to use as is. funky stuff happens if negative
	return(true);
}

double GetRelIndexRefraction(RayTrace& ray, Point* pt)
{
	if (pt == nullptr || pt->MatProps == nullptr)
		return(1.0);
	
	float wavelength = ray.waveData->lambda;
	double n = pt->MatProps->getIndexRefractionWavelength(wavelength);
	return ((n!=0.0) ? n : 1.0);
}


double RayTrace::GetAlpha() {
	if (curPt == nullptr || curPt->MatProps == nullptr || curPt->MatProps->hasOpticalData()==false)
		return 0.0;
	return curPt->MatProps->getOpticalData()->getAlpha(waveData->lambda, curPt->eg);
}


//find the projection of the incoming vector on the plane
//continue the vector as it was
//subtract out the projection on the normal plane vector twice. Actually pretty simple...
int FindReflectionVector(XSpace inVect, XSpace plane, XSpace& ReflectVector)
{
	plane.Normalize();	//make sure this vector is normalized...

	XSpace projection;	//dot product in direction of plane
	double mag = inVect.dot(plane);
	projection = plane * mag;
	ReflectVector = inVect - projection*2.0;	//XSpace - XSpace(2)

	return(0);
}

bool FindRefractionVector(XSpace vectIn, XSpace plane, double thetaInc, double nI, double nR, XSpace& RefractVect)
{
	//simply calculate the parallel and perpendicular portions in relation to the plane - which also happen to be parallel
	//to the reflection || and perp parts. Because the magnitudes are all 1, can get away with sin and cos = opp and adj.

	double nRatio = nI / nR;
	XSpace RefractPara;	//vector parallel to plane
	XSpace RefractPerp;	//vector perpendicular to plane

	//make sure things are normalized
	vectIn.Normalize();
	plane.Normalize();

	RefractPerp = plane * plane.dot(vectIn);	//this isn't the perpendicular refraction vector yet (refletion perpendicular)
	RefractPara = (vectIn - RefractPerp) * nRatio;	//parallel refraction vector
	RefractPerp.Normalize();	//still not it, but it is pointing in the correct direction
	double sqrtterm = 1 - RefractPara.dot(RefractPara);	//this is the magnitude squared.
	
	if(sqrtterm > 0.0)
		RefractPerp = RefractPerp * sqrt(sqrtterm);	// from pythagorean's theorem. ish.
	else
	{
		RefractVect.x = RefractVect.y = RefractVect.z = 0.0;
		return(false);
	}
	// Para^2 + Perp^2 = 1, plane is unit and facing opposite direction. RefractPara dot itself is ||Para||^2 - didn't feel like
	//doing magnitude having sqrt, and then squaring that again
	RefractVect = RefractPara + RefractPerp;
	return(true);
}

//calculates the number of photons per second in the ray, provided a custom power is not provided
double CalcNumberPhotonsSimple(RayTrace& ray, double power)
{
	double numPhotons = 0.0;
	if(power == 0.0 && ray.waveData->energyphoton > 0.0)
	{
		numPhotons = ray.powerLeft / ray.waveData->energyphoton;
	}
	else if(ray.waveData->energyphoton > 0.0)
	{
		numPhotons = power / ray.waveData->energyphoton;
	}
	
	return(numPhotons);
}

/*
This functon looks at two energy levels, and makes some assumptions about the states:
1) The density of states is constant/eV for this particular grouping across the energy width
2) The electrons are distributed exponentially as expected from a boltzmann distribution,
but the factor that it energetically reduces by is unknown. This solves that by solving:
occ = integral[Emin, Emax, g(e) exp(-E*B)]
*/
int DetermineEmissionCoefficients(LightVariation& lVar, ELevel* eSrc, ELevel* eTrg)
{
	lVar.elecSrcC = NULL;	//make sure it is cleared out in case the values input are bad
	lVar.holeSrcV = NULL;
	lVar.boltzFitC = 0.0;
	lVar.boltzFitV = 0.0;
	if(eSrc == NULL || eTrg == NULL)	//gotta have a state existing for the electrons
	{
		ProgressCheck(51, false);
		return(0);
	}

	double srcElec = eSrc->GetBeginElec();;
	double srcNumSt = eSrc->GetNumstates();
//	if(eSrc->carriertype == ELECTRON)
//		srcElec = eSrc->GetBeginOccStates();
//	else
//		srcElec = srcNumSt - eSrc->GetBeginOccStates();

	//and the hole occupancy
	double trgHole = eTrg->GetBeginHole();
	double trgNumSt = eTrg->GetNumstates();
//	if(eTrg->carriertype == HOLE)
//		trgHole = eTrg->GetBeginOccStates();
//	else
//		trgHole = trgNumSt - eTrg->GetBeginOccStates();

	if(srcElec <= 0.0 || trgHole <= 0.0)	//there's no carriers to fall or no places for carriers to go
	{
		ProgressCheck(52, false);
		return(0);
	}
	
	lVar.elecSrcC = eSrc;
	lVar.holeSrcV = eTrg;

	//first, let's figure out what the electron occupancy of the source is:
	double width = eSrc->max - eSrc->min;
	lVar.boltzFitC = CalcBoltzCoeff(width, srcNumSt, srcElec);
	width = eTrg->max - eTrg->min;
	lVar.boltzFitV = CalcBoltzCoeff(width, trgNumSt, trgHole);

	return(0);
}

double CalcBoltzCoeff(double eWidth, double numSt, double numCar)
{
	if(eWidth <= 0)
	{
		if(numCar >= numSt)
			return(0.0);
		else if(numCar <= 0.0)
			return(ABSMAXPOS);	//this should guarantee it goes to zero...

		return(log(numSt/numCar));	//this should result in:
		//numSt * exp(-coeff) = numCar
	}
	if(numCar <= 0.0)
		return(ABSMAXPOS);

	double coeff;	//do I use the small or large approximation as a first guess
	double coeffNeg = -1.0;	//negative is flag that this hasn't been set yet.
	double coeffPos = 0.0;	//final value	-- this is equiv to 1/kT which means infinite temp
	double coeffPrev = 0.0;	//this will go along with coeffPos for the first around
	double valNeg = -1.0;	//has not been set, though the lowest number it could be is zero
	double valPos = numSt;	//final value
	double valPrev = numSt;	//have this correlate with valPos for the first iteration
	double minAcceptable = 0.99 * numCar;	//just get the value within 1%. This is not a massively critical calculation.
	double maxAcceptable = 1.01 * numCar;
	double val = 0.0;
	
	//assume that n << N for a first guess, and use the approx that coeff is small
	if(numCar / numSt < 1e-15)  //assuming that the coefficient is incredibly large to cancel out the exponenent term, and get ret to be very small
	{
		//we're going to assume then that the coefficient is huge, so the exponent is essentially zero
		//this says val(numCar) = NumSt / [coeff * eWidth]
		//we know eWidth > 0.0
		coeff = numSt / (eWidth * numCar);
		val = CalcApproxOcc(coeff, eWidth, numSt);
	}
	else  //assuming that coeff is a small value for a first guess
	{
		coeff = numSt * eWidth;	//must be >0
		coeff = 2.0 * (numSt - numCar) / coeff; //so this should be OK
		val = CalcApproxOcc(coeff, eWidth, numSt);
	}
	//coeff(Prev) and val(prev) have been seeded with values
	while(val > maxAcceptable || val < minAcceptable)	//it is not close enough for government work...
	{
		//it should always move a bit closer to the correct value. Make sure the two data points work well together!
		if(val < minAcceptable)
		{
			coeffNeg = coeff;
			valNeg = val;
		}
		else if(val > maxAcceptable)
		{
			coeffPos = coeff;
			valPos = val;
		}

		if(coeffNeg <= 0.0)	//I need to go with the secant method first.
		{
			//it will be between the positive and the previous one.
			double difCoeff = coeff - coeffPrev;	//presumably positive
			double difVal = val - valPrev;	//presumably negative
			double slope = difVal / difCoeff;	//presumably negative.
			double changeVal = numCar - val;	//presumably negative

			valPrev = val;	//update the values for prev before changes are made.
			coeffPrev = coeff;	//but don't interfere with the calculations

			coeff += changeVal / slope;	//it needs to move more positive
		}
		else //go with a secant min/max method
		{
			double difCoeff = coeffPos - coeffNeg;	//better be negative. Larger coeff = smaller number, so the coefficient for the big number < coefficient for the small number
			double difVal = valPos - valNeg;	//presumably positive... better be.
			double slope = difVal / difCoeff;	//presumably negative.
			double changeVal = numCar - valNeg;	//better be positive.
			if(val == valPrev)
			{
				coeff = coeffNeg + changeVal / slope;	//just update the coeff, though I doubt it changed
				ProgressCheck(62,false);	//send an output note that something bad happened in the code
				break;
			}
			valPrev = val;	//update the values for prev before changes are made.
			coeffPrev = coeff;	//but don't interfere with the calculations

			coeff = coeffNeg + changeVal / slope;	//it should move more negative.
		}
		val = CalcApproxOcc(coeff, eWidth, numSt);
	}	//end while test 
	return(coeff);
}

double CalcApproxOcc(double coeff, double delE, double NumSt)
{
	if(coeff == 0.0)
		return(NumSt);
	if(delE <= 0.0)
		return(-1.0);	//this is a function that should NEVER return a negative number.

	double ret = -1.0;
	double exponent = coeff * delE;
	if(exponent < 0.015)	//then use an approximation to the equation. It should be slightly less than actual. 
	{	//Taylor series exponent to cancel the -1 in the equation.
		//Might actually maintain some more computational accuracy
		//the approximation should at most be 0.011% off from doing an exp (assuming computer infinite accuracy)
		//but for really small numbers, doing 0.9999999995 - 1 vs just -x = 5e-10 will be more accurate.
		ret = 1.0 - 0.5 * exponent;
		ret = ret * NumSt;	//basically just says if it's low, the low approximation is good enough.
	}
	else
	{
		ret = NumSt * (1.0 - exp(-exponent)) / exponent;
	}
	return(ret);
}

int GenerateLight(Simulation* sim, ELevelRate& rData)
{
	if(sim->EnabledRates.LightEmissions == false && sim->outputs.LightEmission == false)	//not supposed to create to be absorbed nor output light
		return(0);

	if(sim->SteadyStateData && sim->simType == SIMTYPE_SS)
	{
		if(sim->SteadyStateData->LightEmissionFactor <= 0.0)	//prevent light creation if it is auto disabled
			return(0.0);
	}

	if(rData.RateType <= 0.0)	//there was not light produced
		return(0);

	if(rData.source == NULL || rData.target == NULL)	//make sure these exist...
		return(0);

	if(rData.source->getPoint().MatProps && rData.source->getPoint().MatProps->isOpticalOutputsSuppressed())
		return(0);

	
	double TransitionEfficiency=0.0;
	if(rData.source->imp && rData.target->imp)	//just assume it goes 100%. This transition doesn't actually exist yet
		TransitionEfficiency=1.0;
	else if(rData.source->imp)	//therefore the target is not an imp
	{ //it must be a band tail or in the bands
		if(rData.targetcarrier == ELECTRON)
			TransitionEfficiency = rData.source->imp->getOpticalEfficiencyCB();
		else
			TransitionEfficiency = rData.source->imp->getOpticalEfficiencyVB();
	}
	else if(rData.target->imp)	//target is imp, but source is not
	{
		if(rData.sourcecarrier == ELECTRON)
			TransitionEfficiency = rData.target->imp->getOpticalEfficiencyCB();
		else
			TransitionEfficiency = rData.target->imp->getOpticalEfficiencyVB();
	}
	else if (rData.source->bandtail || rData.target->bandtail)	//no impurities are involved
	{
		if (rData.sourcecarrier == rData.targetcarrier)	//it's close enough to say done by phonons
			TransitionEfficiency = 0.0;
		else  //make an optical transition
			TransitionEfficiency = 1.0;
	}
	else if(rData.source->getPoint().MatProps) //just go with the default material optic efficiency
		TransitionEfficiency = rData.source->getPoint().MatProps->getOpticalData(true)->getOpticEfficiency();


	if(TransitionEfficiency == 0.0)	//this doesn't generate any light->optics whatsoever!
		return(0);

	if(rData.source->opWeights==NULL)
		rData.source->opWeights = new OpticalWeightELevel;
	if(rData.target->opWeights==NULL)
		rData.target->opWeights = new OpticalWeightELevel;

	

	int szSrc = rData.source->opWeights->InitializeData(rData.source, sim->curiter, sim->EnabledRates.OpticalEResolution);	//these values would typically be calculated many times
	int szTrg = rData.target->opWeights->InitializeData(rData.target, sim->curiter, sim->EnabledRates.OpticalEResolution);
	if(szSrc == 0 || szTrg == 0)	//it for some reason cannot generate light as there is no data
		return(0);

	double minTop, maxBottom, minBottom, maxTop;	//minTop holds the lower end of energy level of top state
	//maxbottom holds the top of the lower state, so the two closest points.

	//convert all this to be relative to VB Bandmin
	double eg = rData.source->getPoint().eg;
	if (rData.carrier == ELECTRON)
	{
		if (rData.source->imp == NULL && rData.source->bandtail == false)	//it's a band
		{
			if (rData.source->carriertype == ELECTRON)
			{
				minTop = eg + rData.source->min;
				maxTop = eg + rData.source->max;
			}
			else
			{
				minTop = -rData.source->max;
				maxTop = -rData.source->min;
			}
		}
		else
		{
			if (rData.source->carriertype == ELECTRON)
			{
				minTop = eg - rData.source->max;
				maxTop = eg - rData.source->min;
			}
			else
			{
				minTop = rData.source->min;
				maxTop = rData.source->max;
			}
		}
		if (rData.target->imp == NULL && rData.target->bandtail == false)	//it's a band
		{
			if (rData.target->carriertype == ELECTRON)
			{
				maxBottom = eg + rData.target->max;
				minBottom = eg + rData.target->min;
			}
			else
			{
				maxBottom = -rData.target->min;
				minBottom = -rData.target->max;
			}
		}
		else
		{
			if (rData.target->carriertype == ELECTRON)
			{
				maxBottom = eg - rData.target->min;
				minBottom = eg - rData.target->max;
			}
			else
			{
				maxBottom = rData.target->max;
				minBottom = rData.target->min;
			}
		}
	}
	else  //the carrier type was a hole, so the source is actually the lower energy. Swap source and target in the above code
	{
		if (rData.target->imp == NULL && rData.target->bandtail == false)	//it's a band
		{
			if (rData.target->carriertype == ELECTRON)
			{
				minTop = eg + rData.target->min;
				maxTop = eg + rData.target->max;
			}
			else
			{
				minTop = -rData.target->max;
				maxTop = -rData.target->min;
			}
		}
		else
		{
			if (rData.target->carriertype == ELECTRON)
			{
				minTop = eg - rData.target->max;
				maxTop = eg - rData.target->min;
			}
			else
			{
				minTop = rData.target->min;
				maxTop = rData.target->max;
			}
		}
		if (rData.source->imp == NULL && rData.source->bandtail == false)	//it's a band
		{
			if (rData.source->carriertype == ELECTRON)
			{
				maxBottom = eg + rData.source->max;
				minBottom = eg + rData.source->min;
			}
			else
			{
				maxBottom = -rData.source->min;
				minBottom = -rData.source->max;
			}
		}
		else
		{
			if (rData.source->carriertype == ELECTRON)
			{
				maxBottom = eg - rData.source->min;
				minBottom = eg - rData.source->max;
			}
			else
			{
				maxBottom = rData.source->max;
				minBottom = rData.source->min;
			}
		}
	}
	double minenergywidth = minTop - maxBottom;
	double maxenergywidth = maxTop - minBottom;


	//make sure this light is in the appropriate range to even do the calculation
	if(sim->EnabledRates.OpticalEMax > 0.0 &&	//these energies aren't going to be in the processing range
	  (minenergywidth > sim->EnabledRates.OpticalEMax || maxenergywidth < sim->EnabledRates.OpticalEMin))
	  return(0);
	
	double totExpectIntensity = minenergywidth * rData.RateType * QCHARGE * 1000 * rData.source->getPoint().areai.x * TransitionEfficiency;	//mw/cm^2
	//eV/photon * carriers(photons)/sec * J/eV = J/s = W * mW/W * cm^-2 = mW/cm^2
	if(totExpectIntensity < sim->EnabledRates.OpticalIntensityCutoff)		//not a high enough intensity to justify processing
		return(0);

	bool outputElevHeader = true;
	
	
	Wavelength tmpWave;
	//LIGHT_OLD_VARIABLES
	/*tmpWave.coherence1 = tmpWave.coherence2 =*/ tmpWave.cutOffPower = tmpWave.intensity = 0.0;
	tmpWave.IsCutOffPowerInit = false;
	tmpWave.lambda = 0.0;
	//LIGHT_OLD_VARIABLES
//	tmpWave.collimation = PI;
	tmpWave.DirEmitted = EMIT_NONE;

	int szBig = (szSrc > szTrg) ? szSrc : szTrg;	//the larger of the two
	int szSmall = (szSrc < szTrg) ? szSrc : szTrg;	//the smaller of the two
	
	Point* pt = &rData.source->getPoint();
	long int ptID = -1;	//default disable (<0) - for detailed point optical outputs
	if(pt)	//safe null pointer
	{
		if(pt->Output.DetailedOptical)	//actually want full outputs
			ptID = rData.source->getPoint().adj.self;	//so set the proper ptID
	}

	//we need to build the partition function for the difference in delta E. This needs to be full whether part of the processed or not
	double curOffSet = minTop - minBottom;
	int szOffset = szSrc + szTrg - 1;	//if 10 on bottom and 4 on top, there are the ten transition energies, plus the three more of the small ones going to the bottom
	double* PFnDelE = new double[szOffset];
	double totalPFnDelE = 0.0;
	//these indices are ugly to go through... but it works out correctly. This likely only happens once per transition between two states (based on the rate type).
	for(int indexOffset = 0; indexOffset < szOffset; ++indexOffset)
	{
		PFnDelE[indexOffset] = 0.0;	//reset the data. We want the PFnDelE[0] to be the minimum energy

		for(int src = (indexOffset < szTrg)?0:indexOffset-szTrg+1; src < szSrc && src-indexOffset <= 0 ; ++src)
		{
			PFnDelE[indexOffset] += rData.source->opWeights->GetData(src, OPTIC_WEIGHT_FE)
				* rData.target->opWeights->GetData(szTrg - 1 + src - indexOffset, OPTIC_WEIGHT_1MFE);
			//-1 to get to last element, -indexOffset to go down the energies (larger gap), +src to move the target back up and make the energy smaller
		}
		totalPFnDelE += PFnDelE[indexOffset];
	}
	if(totalPFnDelE > 0.0)	//better be...
		totalPFnDelE = 1.0 / totalPFnDelE;

	//now process the data!
	
	int topOffset = int(floor(minTop / sim->EnabledRates.OpticalEResolution));	//the floor is probably unnecessary...
	int bottomOffset = int(ceil(maxBottom / sim->EnabledRates.OpticalEResolution));	//the floor is probably unnecessary...
	int ArrayOffset = topOffset - bottomOffset;	//how many points away they are on the grid of optical data.
	for(int i=0; i<szOffset; ++i)
	{
		double photonflux = rData.RateType * PFnDelE[i] * totalPFnDelE * TransitionEfficiency;
		if(MY_IS_FINITE(photonflux) == false)
			ProgressCheck(53, false);

		tmpWave.energyphoton = /*minenergywidth + */double(i+ArrayOffset) * sim->EnabledRates.OpticalEResolution;
		tmpWave.intensity = photonflux * tmpWave.energyphoton * QCHARGE * 1000 * rData.source->getPoint().areai.x;
		if(AcceptLight(&tmpWave, sim, 1e-4) == false)	//this puts the energy of the photon into the proper bin and sets the lambda (wavelength)
			continue;	//make sure it gets about 99.99% of the light from the output

		if(sim->SteadyStateData)	//if it's having trouble converging, slowly turn on the emissions
		{
			tmpWave.intensity = tmpWave.intensity * sim->SteadyStateData->LightEmissionFactor;
		}

		if(outputElevHeader)
		{
			OutputPointOpticalDetailedELev(sim, rData);
			outputElevHeader = false;
		}

		OutputPointOpticalDetailedSplit(sim, ptID, tmpWave.energyphoton, photonflux);

		if(sim->EnabledRates.LightEmissions)	//allow the light to be reabsorbed and truly "exit" the device (new source of light)
		{
			if(rData.source->getPoint().LightEmissions == NULL)
				rData.source->getPoint().LightEmissions = new LightSource;
			AddLightToMap(rData.source->getPoint().LightEmissions->light, tmpWave.energyphoton, tmpWave, sim);
		}

		if(sim->outputs.LightEmission)
		{
			pt->LightEmission += tmpWave.intensity;
			if (rData.source->imp == NULL && rData.target->imp == NULL)
			{
				sim->AddOpticalData(BulkOpticalData::generatedEg, tmpWave.energyphoton, photonflux);
				if (rData.source->getPoint().MatProps)
					rData.source->getPoint().MatProps->AddOpticalData(BulkOpticalData::generatedEg, tmpWave.energyphoton, photonflux);
					
			}
			else
			{
				sim->AddOpticalData(BulkOpticalData::generatedSRH, tmpWave.energyphoton, photonflux);
				if (rData.source->getPoint().MatProps)
					rData.source->getPoint().MatProps->AddOpticalData(BulkOpticalData::generatedSRH, tmpWave.energyphoton, photonflux);
			}
			
		}
	}
	

	delete [] PFnDelE;	//cleanup memory
	PFnDelE = NULL;


/*

Old method for generating light

	double numCarSrc = rData.source->GetBeginElec();
	double numCarTrg = rData.target->GetBeginHole();
//	if(rData.source->carriertype == HOLE)  used to be GetBeginOcc()
//		numCarSrc = rData.source->GetNumstates() - numCarSrc;	//want the number of electrons at the bottom
//	if(rData.target->carriertype == ELECTRON)
//		numCarTrg = rData.target->GetNumstates() - numCarTrg;	//want the number of holes near the top!
	
	
	//attempt to cut out a lot of run time! If the carrier amounts are small, the value is going to be butted up against the minimum energy, and there's no need to do all this processing.
	if(numCarSrc <= rData.source->carrierspectrumcutoff && numCarTrg <= rData.target->carrierspectrumcutoff)
	{
		//just do the minimum's and get outta here!
		double minTop, minBottom;
		if(rData.source->imp == NULL && rData.source->bandtail == false)	//it's a band
		{
			if(rData.source->carriertype == ELECTRON)
				minTop = rData.source->getPoint().cb.bandmin + rData.source->min;
			else
				minTop = rData.source->getPoint().vb.bandmin - rData.source->max;
		}
		else
		{
			if(rData.source->carriertype == ELECTRON)
				minTop = rData.source->getPoint().cb.bandmin - rData.source->max;
			else
				minTop = rData.source->getPoint().vb.bandmin + rData.source->min;

		}

		if(rData.target->imp == NULL && rData.source->bandtail == false)	//it's a band
		{
			if(rData.target->carriertype == ELECTRON)
				minBottom = rData.target->getPoint().cb.bandmin + rData.target->max;
			else
				minBottom = rData.target->getPoint().vb.bandmin - rData.target->min;
		}
		else
		{
			if(rData.target->carriertype == ELECTRON)
				minBottom = rData.target->getPoint().cb.bandmin - rData.target->min;
			else
				minBottom = rData.target->getPoint().vb.bandmin + rData.target->max;
		}
		tmpWave.energyphoton = minTop - minBottom;
		tmpWave.intensity = tmpWave.energyphoton * rData.netrate * QCHARGE * 1000.0 * rData.source->getPoint().areai.x;

		if(AcceptLight(&tmpWave, sim) == false)
			return(false);
		
		tmpWave.coherence1 = tmpWave.coherence2 = tmpWave.cutOffPower = 0.0;
		tmpWave.IsCutOffPowerInit = false;
		tmpWave.lambda = float(PHOTONENERGYCONST / tmpWave.energyphoton);
		tmpWave.collimation = PI;

		if(sim->EnabledRates.LightEmissions)	//allow the light to be reabsorbed and truly "exit" the device
			AddLightToMap(rData.source->getPoint().LightEmissions.light, tmpWave.energyphoton, tmpWave, sim);

		if(sim->outputs.LightEmission)
		{
			if(rData.source->getPoint().MatProps->optics->suppressOpticalOutputs==false)
			{
				AddLightToMap(sim->OpticalFluxOut->Emissions, tmpWave.energyphoton, rData.netrate, sim);
				AddLightToMap(rData.source->getPoint().MatProps->OpticalFluxOut->Emissions, tmpWave.energyphoton, rData.netrate, sim);
			}
		}
		return(true);
	}
	

	LightVariation process;
	DetermineEmissionCoefficients(process, rData.source, rData.target);	//this function takes up about 65% of the processessing time, and the source spectrum dist adds a couple more percent. Avoiding these calculations would be awesome.
	SourceSpectrumDistribution ssd;
	ssd.Clear();
	ssd.Init(&process);
	double minenergywidth = ssd.GetStart();
	double maxenergywidth = ssd.GetEnd();

	if(sim->EnabledRates.OpticalEMax > 0.0 &&	//these energies aren't going to be in the processing range
	  (minenergywidth > sim->EnabledRates.OpticalEMax || maxenergywidth < sim->EnabledRates.OpticalEMin))
	  return(0);
	
	double totExpectIntensity = minenergywidth * rData.netrate * QCHARGE * 1000 * rData.source->getPoint().areai.x;	//mw/cm^2
	//eV/photon * carriers(photons)/sec * J/eV = J/s = W * mW/W * cm^-2 = mW/cm^2
	if(totExpectIntensity < sim->EnabledRates.OpticalIntensityCutoff)		//not a high enough intensity to justify processing
		return(0);


	//the stop should still generate the full partition function, though we can't output all the data.
	vector<double> energies;	//stores the energies that the function has values for
	ssd.GenerateFunction(sim->EnabledRates.OpticalEMin, sim->EnabledRates.OpticalEResolution, energies);
	
	tmpWave.coherence1 = tmpWave.coherence2 = tmpWave.cutOffPower = tmpWave.intensity = 0.0;
	tmpWave.IsCutOffPowerInit = false;
	tmpWave.lambda = 0.0;
	tmpWave.collimation = PI;

	
	for(unsigned int c=0; c<energies.size(); c++)
	{
		double photonflux = rData.netrate * ssd.AcquireValue(energies[c]);	//total rate * (prob/toal prob)
		tmpWave.energyphoton = energies[c];
		tmpWave.lambda = float(PHOTONENERGYCONST / energies[c]);	//eV*nm / eV = nm
		tmpWave.intensity = photonflux * tmpWave.energyphoton * QCHARGE * 1000 * rData.source->getPoint().areai.x;
		//carriers(photons)/sec * eV/photon * J/eV = J/s = W * mW/W * cm^-2 = mW/cm^2

		if(MY_IS_FINITE(photonflux) == false)
			ProgressCheck(53, false);

		if(sim->EnabledRates.LightEmissions)	//allow the light to be reabsorbed and truly "exit" the device
			AddLightToMap(rData.source->getPoint().LightEmissions.light, tmpWave.energyphoton, tmpWave, sim);

		if(sim->outputs.LightEmission)
		{
			AddLightToMap(sim->OpticalFluxOut->Emissions, energies[c], photonflux, sim);
			if(rData.source->getPoint().MatProps)
				AddLightToMap(rData.source->getPoint().MatProps->OpticalFluxOut->Emissions, energies[c], photonflux, sim);
		}
	}
	
	energies.clear();

	//we can assume that everything is A-OK if it gets past that point
	*/

	return(1);
}

bool AcceptLight(Wavelength* wv, Simulation* sim, double intensityMultiplier)	//returns whether to keep the light or not. Intensity must be pre-calculated
{
	if(wv->intensity < sim->EnabledRates.OpticalIntensityCutoff * intensityMultiplier)
		return(false);

	if(wv->energyphoton < sim->EnabledRates.OpticalEMin || (wv->energyphoton > sim->EnabledRates.OpticalEMax && sim->EnabledRates.OpticalEMax > 0.0))
		return(false);

	double energy = DoubleSubtract(wv->energyphoton, sim->EnabledRates.OpticalEMin);
	if(energy == 0.0)
	{
		wv->energyphoton = sim->EnabledRates.OpticalEMin;	//just make it equal
		if(wv->energyphoton == 0.0)
			return(false);
		return(true);
	}
	energy += sim->EnabledRates.OpticalEResolution * 1.0e-7;	//remove some rounding errors from floating point inaccuracies (fixed in floor)
	double numSteps = floor(energy / sim->EnabledRates.OpticalEResolution);
	wv->energyphoton = sim->EnabledRates.OpticalEMin + numSteps * sim->EnabledRates.OpticalEResolution;
	if(wv->energyphoton==0.0)
		return(false);
	wv->lambda = float(PHOTONENERGYCONST / wv->energyphoton);

	//now adjust the photon energy and lambda to be a binned value
	return(true);

}

template<typename VAL>
bool AddLightToMap(std::map<double, VAL>& output, double energy, VAL intensity, Simulation* sim)
{
	double min = sim->EnabledRates.OpticalEMin;
	double max = sim->EnabledRates.OpticalEMax;

	if(energy < min || (energy > max && max > 0.0))	//can this be added
		return(false);

	//first, figure out if an approximately appropriate key is available
	std::map<double,VAL>::iterator less = MapFindFirstLessEqual(output, energy);
	double binwidth = sim->EnabledRates.OpticalEResolution;
	double distless = 2.0 * binwidth;	//initial guess that will fail
	double distmore = distless;
	
	if(less != output.end())
	{
		distless = energy - less->first + binwidth * 1.0e-7;	//must be >= 0, because less->first <= energy
		if(distless < binwidth)	//the *1e-7 is getting rid of rounding issues with binwidth.
		{
			less->second += intensity;
			return(true);
		}
	}
	//a new one must be created because it is too far away or non-existent
	double eUse =floor((energy - min)/binwidth + binwidth * 1e-7);	//the number of things to go over the minimum
	//the second binwidth is because for example at 1.275 is stored as 1.274999999999999999
	//.005 is stored as 0.0050000000000000001
	//and dividing the two give 154.999999999999999769, which when floored is 154.
	eUse = min + eUse * binwidth;	//make it be added to the lower value
	
	output[eUse] = intensity;	//add it in

	return(true);
}




Wavelength::Wavelength()
{
	lambda = 0.0;
	intensity = 0.0;
	energyphoton = 0.0;
	IsCutOffPowerInit = false;
	//LIGHT_OLD_VARIABLES
//	coherence1 = coherence2 = 0.0;
//	collimation = PI;
	DirEmitted = EMIT_NONE;
}

Wavelength::~Wavelength()
{
	//	delete emitdata;	this heap data is now linked in simulation, because we want this data to persist for awhile until it is sent as output
	
}

Wavelength Wavelength::operator+(const Wavelength &rhs) const	//add the intensity of rhs and keep values of lhs - should have same energy to begin with
{
	Wavelength tmp;
	tmp.intensity = intensity + rhs.intensity;
//LIGHT_OLD_VARIABLES
//	tmp.coherence1 = 0.5 * (coherence1 + rhs.coherence1);
//	tmp.coherence2 = 0.5 * (coherence2 + rhs.coherence2);
//	tmp.collimation = 0.5 * (collimation + rhs.collimation);
	tmp.DirEmitted = EMIT_NONE;
	if (IsCutOffPowerInit && rhs.IsCutOffPowerInit)
	{
		tmp.IsCutOffPowerInit = true;
		tmp.cutOffPower = cutOffPower + rhs.cutOffPower;
	}
	else
	{
		tmp.IsCutOffPowerInit = false;
		tmp.cutOffPower = 0.0;
	}
	if (energyphoton == rhs.energyphoton)
	{
		tmp.energyphoton = energyphoton;
		tmp.lambda = lambda;
	}
	else
	{
		tmp.energyphoton = 0.5 * (energyphoton + rhs.energyphoton);
		tmp.lambda = float(PHOTONENERGYCONST / tmp.energyphoton);
	}
	return(tmp);
}

Wavelength& Wavelength::operator+=(const Wavelength &rhs)	//add the intensity of rhs and keep values of lhs - should have same energy to begin with
{

	intensity += rhs.intensity;
	//LIGHT_OLD_VARIABLES
//	coherence1 = 0.5 * (coherence1 + rhs.coherence1);
//	coherence2 = 0.5 * (coherence2 + rhs.coherence2);
//	collimation = 0.5 * (collimation + rhs.collimation);
	DirEmitted = EMIT_NONE;	//this will usually reset it to NULL
	if (IsCutOffPowerInit && rhs.IsCutOffPowerInit)
	{
		IsCutOffPowerInit = true;
		cutOffPower = cutOffPower + rhs.cutOffPower;
	}
	else
	{
		IsCutOffPowerInit = false;
		cutOffPower = 0.0;
	}
	if (energyphoton != rhs.energyphoton)
	{
		energyphoton = 0.5 * (energyphoton + rhs.energyphoton);
		lambda = float(PHOTONENERGYCONST / energyphoton);
	}
	return(*this);
}

LightSource::LightSource()
{

	light.clear();
	times.clear();
	enabled = false;
	name = "No Assigned Name";
	sourceShape = NULL;
	center.x = center.y = center.z = 0.0;
	direction.x = 1.0;
	direction.y = direction.z = 0.0;
	ID = -1;
	IncidentPoints.clear();
	centerinit = false;
}

LightSource::~LightSource()
{
	light.clear(); 	// no need to set all those pointers to null...
	IncidentPoints.clear();
	times.clear();
	enabled = false;
	delete sourceShape;
	sourceShape = NULL;
	name = "";
}

void LightSource::AddWavelength(float lambda, double intensity)
{
	if (lambda <= 0.0 || intensity < 0.0)
		return;
	
	Wavelength tmp;
	tmp.lambda = lambda;
	tmp.DirEmitted = 0;
	tmp.energyphoton = PHOTONENERGYCONST / lambda;
	tmp.intensity = intensity;
	tmp.IsCutOffPowerInit = false;
	
	MapAdd(light, tmp.energyphoton, tmp);
	
	return;
}

RayTrace::RayTrace()
{
	waveData = NULL;
	direction.x = direction.y = direction.z = 0.0;
	position.x = position.y = position.z = 0;
	powerLeft = 0;
	curPt = NULL;
	EField1.clear();
	EField2.clear();
	enteredDevice = false;
	reflectCount = 0;
	inNonexistentVolume = false;
	cutoffPowerRedFactor = 1.0;


}
int RayTrace::Initialize(void)
{
	waveData = NULL;
	direction.x = direction.y = direction.z = 0.0;
	position.x = position.y = position.z = 0;
	powerLeft = 0;
	curPt = NULL;
	EField1.clear();
	EField2.clear();
	enteredDevice = false;
	reflectCount = 0;
	inNonexistentVolume = false;
	cutoffPowerRedFactor = 1.0;

	return(0);
}
int RayTrace::Initialize(RayTrace* copyRay)
{
	waveData = copyRay->waveData;
	direction = copyRay->direction;
	position = copyRay->position;
	powerLeft = copyRay->powerLeft;
	curPt = copyRay->curPt;
	EField1 = copyRay->EField1;
	EField2 = copyRay->EField2;
	enteredDevice = copyRay->enteredDevice;
	cutoffPowerRedFactor = copyRay->cutoffPowerRedFactor;
	reflectCount = copyRay->reflectCount;
	inNonexistentVolume = copyRay->inNonexistentVolume;

	return(0);
}

int RayTrace::Initialize(LightSource* ltSrc, Wavelength& wv)
{
	direction = ltSrc->direction;
	direction.Normalize();
	position = ltSrc->center;
	powerLeft = wv.intensity;
	curPt = NULL;
	EField1.clear();
	EField2.clear();
	enteredDevice = false;
	reflectCount = 0;
	inNonexistentVolume = false;
	waveData = &wv;
	cutoffPowerRedFactor = 1.0;
	return(0);
}

RayTrace::RayTrace(RayTrace* copyRay)
{
	Initialize(copyRay);
}

RayTrace::RayTrace(LightSource* ltSrc, Wavelength& wv)
{
	Initialize(ltSrc, wv);
}

RayTrace::~RayTrace()
{
	waveData = NULL;
	direction.x = direction.y = direction.z = 0.0;
	position.x = position.y = position.z = 0;
	powerLeft = 0;
	curPt = NULL;
	EField1.clear();
	EField2.clear();

}

LightVariation::LightVariation()
{
	elecSrcC = NULL;
	holeSrcV = NULL;
	boltzFitV = 0.0;
	boltzFitC = 0.0;
}

LightVariation::~LightVariation()
{
	elecSrcC = NULL;	//DO NOT DELETE!! !! !!
	holeSrcV = NULL;
	boltzFitV = 0.0;
	boltzFitC = 0.0;
}

OpSort::OpSort()
{
	wavelength = 0.0;
	LSpec = NULL;
}
OpSort::~OpSort()
{
	wavelength = 0.0;
	LSpec = NULL;	//don't delete, yo! This pointer doesn't "own" this data, so it's not being deleted here.

}

bool OpSort::operator==(const OpSort& b) const
{
	if (wavelength != b.wavelength)
		return(false);

	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return true.
		return(true);

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC != b.LSpec->boltzFitC)
			return(false);

		if (LSpec->boltzFitV != b.LSpec->boltzFitV)
			return(false);

		if (LSpec->elecSrcC != b.LSpec->elecSrcC)
			return(false);

		if (LSpec->holeSrcV != b.LSpec->holeSrcV)
			return(false);
	}
	else  //one of them exists, but not both. If both existed, the above test would have happened. If neither, then they would have already been set equal
		return(false);

	return(true);
}


bool OpSort::operator!=(const OpSort& b) const
{
	if (wavelength != b.wavelength)
		return(true);

	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return true.
		return(false);

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC != b.LSpec->boltzFitC)
			return(true);

		if (LSpec->boltzFitV != b.LSpec->boltzFitV)
			return(true);

		if (LSpec->elecSrcC != b.LSpec->elecSrcC)
			return(true);

		if (LSpec->holeSrcV != b.LSpec->holeSrcV)
			return(true);

		return(false);
	}

	//if it gets to this point, then only one of the LSpec's had been set. If both, the previous if would have fired (assuming they existed).
	return(true);
}


bool OpSort::operator<(const OpSort& b) const
{
	if (wavelength < b.wavelength)	//priority goes to wavelength first
		return(true);
	else if (wavelength > b.wavelength)
		return(false);

	//the existence of LSpec means higher value
	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return false.
		return(false);	//they are actually ==, which is not <

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC < b.LSpec->boltzFitC)
			return(true);
		else if (LSpec->boltzFitC > b.LSpec->boltzFitC)
			return(false);


		if (LSpec->boltzFitV < b.LSpec->boltzFitV)
			return(true);
		else if (LSpec->boltzFitV > b.LSpec->boltzFitV)
			return(false);

		if (LSpec->elecSrcC && b.LSpec->elecSrcC)	//they both exist, so compare
		{
			if (LSpec->elecSrcC->uniqueID < b.LSpec->elecSrcC->uniqueID)
				return(true);
			else if (LSpec->elecSrcC->uniqueID > b.LSpec->elecSrcC->uniqueID)
				return(false);
		}
		else if (LSpec->elecSrcC)	//the source exists, so it's value is higher because it exists and the other doesn't
			return(false);
		else if (b.LSpec->elecSrcC)	//the target exists, so it's value is higher.
			return(true);
		//they both don't exist, so move on

		if (LSpec->holeSrcV && b.LSpec->holeSrcV)
		{
			if (LSpec->holeSrcV->uniqueID < b.LSpec->holeSrcV->uniqueID)
				return(true);
			else if (LSpec->holeSrcV->uniqueID > b.LSpec->holeSrcV->uniqueID)
				return(false);
		}
		else if (LSpec->holeSrcV)	//source exists, but target doesn't
			return(false);
		else if (b.LSpec->holeSrcV)
			return(true);
	}
	else if (LSpec)	//source exists, target does not
		return(false);
	else if (b.LSpec)
		return(true);

	//they are the same, so not equal.
	return(false);
}


bool OpSort::operator>(const OpSort& b) const
{
	if (wavelength > b.wavelength)	//priority goes to wavelength first
		return(true);
	else if (wavelength < b.wavelength)
		return(false);

	//the existence of LSpec means higher value
	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return false.
		return(false);	//they are actually ==, which is not <

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC > b.LSpec->boltzFitC)
			return(true);
		else if (LSpec->boltzFitC < b.LSpec->boltzFitC)
			return(false);


		if (LSpec->boltzFitV > b.LSpec->boltzFitV)
			return(true);
		else if (LSpec->boltzFitV < b.LSpec->boltzFitV)
			return(false);

		if (LSpec->elecSrcC && b.LSpec->elecSrcC)	//they both exist, so compare
		{
			if (LSpec->elecSrcC->uniqueID > b.LSpec->elecSrcC->uniqueID)
				return(true);
			else if (LSpec->elecSrcC->uniqueID < b.LSpec->elecSrcC->uniqueID)
				return(false);
		}
		else if (LSpec->elecSrcC)	//the source exists, so it's value is higher because it exists and the other doesn't
			return(true);
		else if (b.LSpec->elecSrcC)	//the target exists, so it's value is higher.
			return(false);
		//they both don't exist, so move on

		if (LSpec->holeSrcV && b.LSpec->holeSrcV)
		{
			if (LSpec->holeSrcV->uniqueID > b.LSpec->holeSrcV->uniqueID)
				return(true);
			else if (LSpec->holeSrcV->uniqueID < b.LSpec->holeSrcV->uniqueID)
				return(false);
		}
		else if (LSpec->holeSrcV)	//source exists, but target doesn't
			return(true);
		else if (b.LSpec->holeSrcV)
			return(false);

	}
	else if (LSpec)	//source exists, target does not
		return(true);
	else if (b.LSpec)
		return(false);

	//they are the same, so not equal.
	return(false);
}

bool OpSort::operator<=(const OpSort& b) const
{
	if (wavelength < b.wavelength)	//priority goes to wavelength first
		return(true);
	else if (wavelength > b.wavelength)
		return(false);

	//the existence of LSpec means higher value
	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return false.
		return(true);	//they are actually ==, which is not <

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC < b.LSpec->boltzFitC)
			return(true);
		else if (LSpec->boltzFitC > b.LSpec->boltzFitC)
			return(false);


		if (LSpec->boltzFitV < b.LSpec->boltzFitV)
			return(true);
		else if (LSpec->boltzFitV > b.LSpec->boltzFitV)
			return(false);

		if (LSpec->elecSrcC && b.LSpec->elecSrcC)	//they both exist, so compare
		{
			if (LSpec->elecSrcC->uniqueID < b.LSpec->elecSrcC->uniqueID)
				return(true);
			else if (LSpec->elecSrcC->uniqueID > b.LSpec->elecSrcC->uniqueID)
				return(false);
		}
		else if (LSpec->elecSrcC)	//the source exists, so it's value is higher because it exists and the other doesn't
			return(false);
		else if (b.LSpec->elecSrcC)	//the target exists, so it's value is higher.
			return(true);
		//they both don't exist, so move on, =

		if (LSpec->holeSrcV && b.LSpec->holeSrcV)
		{
			if (LSpec->holeSrcV->uniqueID < b.LSpec->holeSrcV->uniqueID)
				return(true);
			else if (LSpec->holeSrcV->uniqueID > b.LSpec->holeSrcV->uniqueID)
				return(false);
		}
		else if (LSpec->holeSrcV)	//source exists, but target doesn't
			return(false);
		else if (b.LSpec->holeSrcV)
			return(true);

	}
	else if (LSpec)	//source exists, target does not
		return(false);
	else if (b.LSpec)
		return(true);

	//they are the same, so equal
	return(true);
}

bool OpSort::operator>=(const OpSort& b) const
{
	if (wavelength > b.wavelength)	//priority goes to wavelength first
		return(true);
	else if (wavelength < b.wavelength)
		return(false);

	//the existence of LSpec means higher value
	if (LSpec == b.LSpec)	//if this is true, then everything else below is equal and it would return false.
		return(true);	//they are actually ==, which is not <

	if (LSpec != NULL && b.LSpec != NULL)  //make sure they both exist to actually compare.
	{
		//are they functionally the same thing?
		if (LSpec->boltzFitC > b.LSpec->boltzFitC)
			return(true);
		else if (LSpec->boltzFitC < b.LSpec->boltzFitC)
			return(false);


		if (LSpec->boltzFitV > b.LSpec->boltzFitV)
			return(true);
		else if (LSpec->boltzFitV < b.LSpec->boltzFitV)
			return(false);

		if (LSpec->elecSrcC && b.LSpec->elecSrcC)	//they both exist, so compare
		{
			if (LSpec->elecSrcC->uniqueID > b.LSpec->elecSrcC->uniqueID)
				return(true);
			else if (LSpec->elecSrcC->uniqueID < b.LSpec->elecSrcC->uniqueID)
				return(false);
		}
		else if (LSpec->elecSrcC)	//the source exists, so it's value is higher because it exists and the other doesn't
			return(true);
		else if (b.LSpec->elecSrcC)	//the target exists, so it's value is higher.
			return(false);
		//they both don't exist, so move on, =

		if (LSpec->holeSrcV && b.LSpec->holeSrcV)
		{
			if (LSpec->holeSrcV->uniqueID > b.LSpec->holeSrcV->uniqueID)
				return(true);
			else if (LSpec->holeSrcV->uniqueID < b.LSpec->holeSrcV->uniqueID)
				return(false);
		}
		else if (LSpec->holeSrcV)	//source exists, but target doesn't
			return(true);
		else if (b.LSpec->holeSrcV)
			return(false);

	}
	else if (LSpec)	//source exists, target does not
		return(true);
	else if (b.LSpec)
		return(false);

	//they are the same, so equal
	return(true);
}

BulkOpticalData::BulkOpticalData()
{
	ClearAll();
}

BulkOpticalData::~BulkOpticalData()
{
	ClearAll();
}

void BulkOpticalData::ClearAll()
{
/*	LightIncident.clear();
	LightEnter.clear();
	LightLeavePos.clear();
	LightLeaveNeg.clear();
	LightCarrierGen.clear();
	LightSRH.clear();
	LightDef.clear();
	LightScat.clear();
	StimEmisRec.clear();
	StimEmisSRH.clear();
	StimEmisDef.clear();
	StimEmisScat.clear();
	GeneratedEg.clear();
	GeneratedSRH.clear();
	*/
	AllData.clear();
	lastAccessed = AllData.end();
	for (unsigned char i = 0; i <= lastType; ++i)
		ctrs[i] = 0;
}

unsigned int BulkOpticalData::AddData(double energy, unsigned char type, double val, unsigned int hint) {

	if (lastAccessed == AllData.end() || lastAccessed->first != energy) { //either points to nothing or points to wrong thing
		lastAccessed = AllData.find(energy);
		if (lastAccessed == AllData.end()) {
			//not found.
			std::pair<std::map<double, BulkMapData>::iterator, bool> ret = AllData.insert(std::pair<double, BulkMapData>(energy, BulkMapData()));
			lastAccessed = ret.first;
		}
	}
	unsigned int prevTypeCount = lastAccessed->second.getNumTypes();
	hint = lastAccessed->second.AddValue(val, type, hint);	//update the hint if it was wrong (or keep the same)
	
	if (prevTypeCount < lastAccessed->second.getNumTypes() && type <= lastType)	//it added another data point
		ctrs[type] += 1;

	return hint;
}

int BulkOpticalData::AddExistingBulkData(BulkOpticalData* otherTree, double factor)
{
	if (factor == 0.0)
		return(0);

	for (std::map<double, BulkMapData>::iterator it = otherTree->AllData.begin(); it != otherTree->AllData.end(); ++it) {

		lastAccessed = AllData.find(it->first);
		if (lastAccessed == AllData.end())  //didn't previously exist.
			lastAccessed = AllData.insert(std::pair<double, BulkMapData>(it->first, BulkMapData())).first;	//so create it
		lastAccessed->second.AddValues(it->second, factor);

	}
	//this way screws up the counters...
	ResetCounters();	//so fix them

	return(0);

}

int SourceSpectrumDistribution::Init(LightVariation* val)
{
	ltVar = val;
	if (ltVar == NULL)
	{
		Egap = 0.0;
		SrcWidth = 0.0;
		TrgWidth = 0.0;
		CoeffSrc = 0.0;
		CoeffTrg = 0.0;
		NumSrc = 0.0;
		NumTrg = 0.0;
		LinCoeff = 0.0;
		CoeffDif = 0.0;
		invCoeffDif = 0.0;
		transition1 = 0.0;
		transition2 = 0.0;
		transitionend = 0.0;
		ExpConst = 0.0;
		which = SPECTRUM_DISTR_UNDEFINED;
		return(0);
	}

	if (ltVar->elecSrcC == NULL || ltVar->holeSrcV == NULL)
	{
		Egap = 0.0;
		SrcWidth = 0.0;
		TrgWidth = 0.0;
		CoeffSrc = 0.0;
		CoeffTrg = 0.0;
		NumSrc = 0.0;
		NumTrg = 0.0;
		LinCoeff = 0.0;
		CoeffDif = 0.0;
		invCoeffDif = 0.0;
		transition1 = 0.0;
		transition2 = 0.0;
		transitionend = 0.0;
		ExpConst = 0.0;
		which = SPECTRUM_DISTR_UNDEFINED;
		return(0);
	}

	double absUseC = ltVar->elecSrcC->CalcAbsEUseEnergy();
	double difminC = ltVar->elecSrcC->eUse - ltVar->elecSrcC->min;
	double difmaxC = ltVar->elecSrcC->max - ltVar->elecSrcC->eUse;
	double absminC;// = absUseC - difminC;
	double absmaxC;// = absUseC + difmaxC;
	if (ltVar->elecSrcC->imp == NULL && ltVar->elecSrcC->bandtail == false)	//this is not a defect/impurity
	{
		if (ltVar->elecSrcC->carriertype == CONDUCTION)	//it is in the conduction band
		{
			absminC = absUseC - difminC;
			absmaxC = absUseC + difmaxC;
		}
		else //else the upper hand is actually in the valence band
		{  //which means that difMax is actually going down and difmin is going up in energy
			absminC = absUseC - difmaxC;	//this would be the lowest energy level
			absmaxC = absUseC + difminC;
		}
	}
	else //it is an impurity/band tail
	{
		if (ltVar->elecSrcC->carriertype == ELECTRON)	//the minimum energy is near the CB, which would be the absolute max
		{
			absmaxC = absUseC - difminC;
			absminC = absUseC + difmaxC;
		}
		else //the state is in reference to the valence band, so the absolute minimum lines up with the local minimum
		{
			absmaxC = absUseC + difmaxC;
			absminC = absUseC - difminC;
		}
	}
	//	if(absminC > absmaxC)
	//	{
	//		absminC = absmaxC;
	//		absmaxC = absUseC - difminC;	//absmin
	//	}

	double absUseV = ltVar->holeSrcV->CalcAbsEUseEnergy();
	double difminV = ltVar->holeSrcV->eUse - ltVar->holeSrcV->min;
	double difmaxV = ltVar->holeSrcV->max - ltVar->holeSrcV->eUse;
	double absminV;// = absUseV - difminV;
	double absmaxV;// = absUseV + difmaxV;

	if (ltVar->holeSrcV->imp == NULL && ltVar->holeSrcV->bandtail == false)
	{
		if (ltVar->holeSrcV->carriertype == ELECTRON) //the lower energy is somehow in the CB
		{
			absminV = absUseV - difminV; //but that means the min/max line up with what we'd expect
			absmaxV = absUseV + difmaxV;
		}
		else //more likely result
		{
			absminV = absUseV - difmaxV; //the VB max would be furthest down
			absmaxV = absUseV + difminV;
		}
	}
	else //it is a defect or impurity
	{
		if (ltVar->holeSrcV->carriertype == ELECTRON)
		{ //this is referencing a donor(like) state, so the min is near the CB and max far away. This is reverse to absolute values
			absmaxV = absUseV - difminV;
			absminV = absUseV + difmaxV;
		}
		else //this is referencing an aceptor-like state, so the min/max line up with absolute value
		{
			absmaxV = absUseV + difmaxV;
			absminV = absUseV - difminV;
		}
	}

	//	if(absminV > absmaxV)
	//	{
	//		absminV = absmaxV;
	//		absmaxV = absUseV - difminV;	//absmin
	//	}

	Egap = absminC - absmaxV;
	SrcWidth = ltVar->elecSrcC->max - ltVar->elecSrcC->min;
	TrgWidth = ltVar->holeSrcV->max - ltVar->holeSrcV->min;
	if (SrcWidth < 0.0)
		SrcWidth = -SrcWidth;
	if (TrgWidth < 0.0)
		TrgWidth = -TrgWidth;
	CoeffSrc = ltVar->boltzFitC;
	CoeffTrg = ltVar->boltzFitV;
	NumSrc = ltVar->elecSrcC->GetNumstates();
	NumTrg = ltVar->holeSrcV->GetNumstates();

	CoeffDif = CoeffTrg - CoeffSrc;
	if (CoeffDif == 0.0)	//this is a flag to just do U - L
	{
		invCoeffDif = 1.0;	//and make sure this factor has no effect on the equation
	}
	else
		invCoeffDif = 1.0 / CoeffDif;

	if (SrcWidth == 0.0 && TrgWidth == 0.0)
	{
		which = SPECTRUM_DISTR_ZERO_SRCTRGWIDTH;
		transition1 = transition2 = transitionend = Egap;
		LinCoeff = NumSrc * NumTrg;
		ExpConst = 0.0;	//this is just easier to do the whole calculation in these special cases
		//it otherwise gets to confusing
	}
	else if (SrcWidth == 0.0)
	{
		which = SPECTRUM_DISTR_ZERO_SRCWIDTH;
		transition1 = Egap;
		transition2 = Egap + TrgWidth;
		transitionend = transition2;
		LinCoeff = NumSrc * NumTrg / TrgWidth;
		ExpConst = 0.0;	//this is just easier to do the whole calculation in these special cases
		//it otherwise gets to confusing
		//the source width is zero, so Ec is zero
	}
	else if (TrgWidth == 0.0)
	{
		which = SPECTRUM_DISTR_ZERO_TRGWIDTH;
		transition1 = Egap;
		transition2 = Egap + SrcWidth;
		transitionend = transition2;
		LinCoeff = NumSrc * NumTrg / SrcWidth;
		ExpConst = 0.0;	//this is just easier to do the whole calculation in these special cases
		//it otherwise gets to confusing
	}
	else if (SrcWidth == TrgWidth)
	{
		which = SPECTRUM_DISTR_STATE_EQUAL;
		transition1 = Egap + SrcWidth;
		transition2 = Egap + TrgWidth;
		transitionend = Egap + SrcWidth + TrgWidth;
		LinCoeff = TrgWidth * SrcWidth;
		LinCoeff = NumSrc * NumTrg * invCoeffDif / LinCoeff;
		ExpConst = -CoeffTrg * TrgWidth + CoeffSrc * (TrgWidth + Egap);
	}
	else if (SrcWidth < TrgWidth)
	{
		which = SPECTRUM_DISTR_VBSTATE_LARGE;
		transition1 = Egap + SrcWidth;
		transition2 = Egap + TrgWidth;
		transitionend = Egap + SrcWidth + TrgWidth;
		LinCoeff = TrgWidth * SrcWidth;
		LinCoeff = NumSrc * NumTrg * invCoeffDif / LinCoeff;
		ExpConst = -CoeffTrg * TrgWidth + CoeffSrc * (TrgWidth + Egap);
	}
	else
	{
		which = SPECTRUM_DISTR_VBSTATE_SMALL;
		transition1 = Egap + TrgWidth;
		transition2 = Egap + SrcWidth;
		transitionend = Egap + SrcWidth + TrgWidth;
		LinCoeff = TrgWidth * SrcWidth;
		LinCoeff = NumSrc * NumTrg * invCoeffDif / LinCoeff;
		ExpConst = -CoeffTrg * TrgWidth + CoeffSrc * (TrgWidth + Egap);
	}





	fnVals.clear();
	fnVals.insert(std::pair<double, double>(Egap, 0.0));	//seed the end points to make sure when calculated, it does in fact go to zero!
	fnVals.insert(std::pair<double, double>(transitionend, 0.0));

	IntegDelE = 0.0;
	IntegDelEI = 0.0;
	goodIntegral = true;

	return(0);
}

double SourceSpectrumDistribution::GetIntensity(double energy)
{
	if (ltVar == NULL)
		return(0.0);

	std::map<double, double>::iterator node = fnVals.find(energy);
	if (node != fnVals.end())
		return(node->second);	//that was easy.

	//this is not.
	double integral = LinCoeff;
	if (which == SPECTRUM_DISTR_UNDEFINED)
	{
		return(0.0);	//this doesn't really apply...
	}

	if (energy < Egap || energy > transitionend)	//don't do superfluous calculations that won't matter
		return(0.0);
	if (which == SPECTRUM_DISTR_ZERO_SRCTRGWIDTH)	//these are both delta functions in density of states
	{
		//the linear coefficient is flat out WRONG
		if (energy == Egap)	//all the Egap/transitions are the same
		{
			integral = 1.0;	//I could do a complicated function... but what's the point?
			//this is the only valid value, and it will do an integral, and this will be the only
			//non-zero value, so when it's normalized, it will go back to one
		}
		else
			integral = 0.0;
	}
	else if (which == SPECTRUM_DISTR_ZERO_SRCWIDTH)
	{

		if (energy >= Egap && energy <= transitionend)	//there's only one way this works...
		{

			//the question is E=? - the source width is zero, so the integral MUST be over a delta function
			//so Ec1 = Ec2, and therefore E = Ec1 - delE
			//double E = TrgWidth + Egap - energy;	//the source width is zero...
			//double expval = CoeffTrg * (E - TrgWidth) - CoeffSrc;
			//* (E + energy - TrgWidth - Egap); Typically, this would be added on after CoeffSrc, but this would result in ZERO.
			//CoeffSrc needs to be in there alone to provide the exponential to multiply by numStates to get the occupied states constant.
			double expval = CoeffTrg * (Egap - energy) - CoeffSrc;
			integral = LinCoeff * exp(expval);

		}
		else
			integral = 0.0;
	}
	else if (which == SPECTRUM_DISTR_ZERO_TRGWIDTH)
	{

		if (energy >= Egap && energy <= transitionend)	//there's only one way this works...
		{
			//what is E? the trgWidth=0... so E = 0!  below should be  -trgWidth, but that's zero
			double expval = -CoeffTrg - CoeffSrc * (energy - Egap);
			integral = LinCoeff * exp(expval);

		}
		else
			integral = 0.0;
	}
	if (which == SPECTRUM_DISTR_STATE_EQUAL)
	{
		double exponent;
		integral = LinCoeff;

		if (Egap <= energy && energy < transition1)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = TrgWidth - (TrgWidth + Egap - energy);
				exponent = (energy - Egap) * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + TrgWidth * CoeffDif;
				double exp1 = exp(expCoeff);	//upper boundary from integral
				expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap - energy);
				double exp2 = exp(expCoeff);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition1 <= energy && energy <= transition2)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = TrgWidth - 0; or from the Ec-E perspective
				// or (TrgWidth + Egap + SrcWidth - energy) - (TrgWidth + Egap - energy) 
				//but energy = Egap + SrcWidth && SRcWidth = TrgWidth
				//so TrgWidth + Egap + SrcWidth - Egap - SrcWidth - (TrgWidth + Egap - TrgWidth - Egap)...
				exponent = TrgWidth * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + TrgWidth * CoeffDif;
				double exp1 = exp(expCoeff);	//upper boundary from integral
				double exp2 = exp(ExpConst - energy * CoeffSrc);	//exp((0) * CoeffDif);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition2 < energy && energy < transitionend)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = (TrgWidth + Egap + SrcWidth - Energy) - (0);
				exponent = (TrgWidth + Egap + SrcWidth - energy)  * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap + SrcWidth - energy);
				double exp1 = exp(expCoeff);	//upper boundary from integral
				double exp2 = exp(ExpConst - energy * CoeffSrc);	//	exp((0) * CoeffDif);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else
			integral = 0.0;
	}
	else if (which == SPECTRUM_DISTR_VBSTATE_SMALL)
	{
		//now there are three integrals in this particular value
		integral = LinCoeff;
		double exponent;
		if (Egap <= energy && energy < transition1)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = TrgWidth - (TrgWidth + Egap - energy);
				exponent = (Egap - energy) * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + TrgWidth * CoeffDif;
				double exp1 = exp(expCoeff);	//upper boundary from integral
				expCoeff = ExpConst - energy * CoeffSrc + CoeffDif *(TrgWidth + Egap - energy);
				double exp2 = exp(expCoeff);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition1 <= energy && energy <= transition2)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = (TrgWidth) - (0);
				exponent = TrgWidth * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + TrgWidth * CoeffDif;
				double exp1 = exp(expCoeff);	//upper boundary from integral
				double exp2 = exp(ExpConst - energy * CoeffSrc);//exp(() * CoeffDif);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition2 < energy && energy < transitionend)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = (TrgWidth + Egap + SrcWidth - energy) - (0);
				exponent = (TrgWidth + Egap + SrcWidth - energy) * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap + SrcWidth - energy);
				double exp1 = exp(expCoeff);	//upper boundary from integral
				double exp2 = exp(ExpConst - energy * CoeffSrc);	//	exp((0) * CoeffDif);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else
			integral = 0.0;
	}
	else if (which == SPECTRUM_DISTR_VBSTATE_LARGE)
	{
		//now there are three integrals in this particular value
		integral = LinCoeff;
		double exponent;
		if (Egap <= energy && energy < transition1)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = TrgWidth - (TrgWidth + Egap - energy);
				exponent = (Egap - energy) * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + TrgWidth * CoeffDif;
				double exp1 = exp(expCoeff);	//upper boundary from integral
				expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap - energy);
				double exp2 = exp(expCoeff);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition1 <= energy && energy <= transition2)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = (TrgWidth + Egap + SrcWidth - energy) - (TrgWidth + Egap - energy)
				exponent = SrcWidth * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap + SrcWidth - energy);
				double exp1 = exp(expCoeff);	//upper boundary from integral
				expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap - energy);
				double exp2 = exp(expCoeff);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else if (transition2 < energy && energy < transitionend)
		{
			if (CoeffDif == 0.0)
			{
				//upper - lower
				//exponent = (TrgWidth + Egap + SrcWidth - energy) - (0);
				exponent = (TrgWidth + Egap + SrcWidth - energy) * exp(-energy * CoeffSrc);
			}
			else
			{
				double expCoeff = ExpConst - energy * CoeffSrc + CoeffDif * (TrgWidth + Egap + SrcWidth - energy);
				double exp1 = exp(expCoeff);	//upper boundary from integral
				double exp2 = exp(ExpConst - energy * CoeffSrc);	//	exp((0) * CoeffDif);	//lower boundary from itnegral
				exponent = exp1 - exp2;
			}
			integral = integral * exponent;
		}
		else
			integral = 0.0;
	}
	else //this is an undefined node
		return(0.0);


	if (Egap < energy && energy < transitionend)
	{
		if (MY_IS_DEN(integral) == false)
			integral = 0.0;
		fnVals.insert(std::pair<double, double>(energy, integral));	//build up the function distribution
		goodIntegral = false;
	}


	return(integral);
}

double SourceSpectrumDistribution::GenerateFunction(int divisions, std::vector<double>& choices)
{
	if (ltVar == NULL)
		return(0.0);


	if (divisions <= 0)
		divisions = 1;
	double inner = (Egap + transition1) * 0.5;
	double outer = inner;


	//do the first section
	GetIntensity(inner);
	for (int i = 1; i<divisions; i++)
	{
		inner = 0.5 * (inner + Egap);	//have it go halfway to the next bit
		outer = 0.5 * (outer + transition1);
		GetIntensity(inner);
		GetIntensity(outer);
	}

	//and the second section (if it exists)
	GetIntensity(transition1);
	if (transition1 != transition2)
	{
		inner = 0.5 * (transition1 + transition2);
		outer = inner;
		GetIntensity(inner);
		for (int i = 1; i<divisions; i++)
		{
			inner = 0.5 * (inner + transition1);
			outer = 0.5 * (outer + transition2);
			GetIntensity(inner);
			GetIntensity(outer);
		}
	}

	//and the final section
	GetIntensity(transition2);
	inner = 0.5 * (transition2 + transitionend);
	outer = inner;
	GetIntensity(inner);
	for (int i = 1; i<divisions; i++)
	{
		inner = 0.5 * (inner + transition2);
		outer = 0.5 * (outer + transitionend);
		GetIntensity(inner);
		GetIntensity(outer);
	}
	GetIntegral();

	for (std::map<double, double>::iterator it = fnVals.begin(); it != fnVals.end(); ++it)
		choices.push_back(it->first);	//store all the energies in choices

	return(0.0);
}

double SourceSpectrumDistribution::GenerateFunction(double start, double width, std::vector<double>& choices)
{
	if (ltVar == NULL)
		return(0.0);

	if (which != 0 && which != 6)	//0 is undefefined and 6 is discrete
	{
		double lvl;
		if (start < Egap)
		{
			lvl = (Egap - start) / width;	//this holds how many widths are needed to get to the start of the function
			lvl = start + ceil(lvl) * width;
		}
		else if (start > Egap)	//still need to generate the entire function, but make sure it stays in line.
		{
			lvl = (start - Egap) / width;
			lvl = start - ceil(lvl) * width;
			GetIntensity(lvl);	//this should put a 0 point just below
			lvl += width;
		}
		else
			lvl = start;

		double intensity = 1.0;
		while (lvl < transitionend && (intensity != 0.0 || lvl < start))
		{
			intensity = GetIntensity(lvl);
			lvl += width;
		}
	}
	else if (which == 6)
	{
		//this is a discrete function...
		GetIntensity(Egap);	//make sure there's the non-zero value
		double lvl = 0.0;
		if (start < Egap)
		{
			lvl = (Egap - start) / width;	//this holds how many widths are needed to get to the start of the function
			lvl = start + floor(lvl) * width;
		}
		GetIntensity(lvl);	//and go to the item one below it
		lvl += width;
		GetIntensity(lvl);	//and one above it
		//closest thing to a discrete peak that we will get

	}
	GetIntegral();
	for (std::map<double, double>::iterator it = fnVals.begin(); it != fnVals.end(); ++it)
	{
		if (it->first >= start)
			choices.push_back(it->first);	//store all the energies in choices
	}
	return(0.0);
}

double SourceSpectrumDistribution::AcquireValue(double energy)
{
	if (ltVar == NULL)
		return(0.0);

	if (goodIntegral == false)
	{
		if (fnVals.size() < 2)
		{
			std::vector<double> tmp;
			GenerateFunction(3, tmp);	//this will create the function
		}
		else
			GetIntegral();
	}
	//it should now be a good function for sure...

	std::map<double, double>::iterator lower = MapFindFirstLessEqual(fnVals, energy);
	std::map<double, double>::iterator higher = MapFindFirstGreater(fnVals, energy);

	if (lower == fnVals.end() || higher == fnVals.end())	//it's outside the range of the function
		return(0.0);

	double width = higher->first - lower->first;
	double height = 0.5 * (lower->second + higher->second);
	double value = (width * height) * IntegDelEI;	//integdelEI is inverse sum, and needs to appropriately cancel out the product of all the others

	return(value);

}

double SourceSpectrumDistribution::GetIntegral()
{
	if (ltVar == NULL)
		return(0.0);

	if (goodIntegral)
		return(IntegDelE);

	if (fnVals.size() < 2)	//there are two by default, so this *should* always be valid, but just programming safety in (it might not be intitialized
		return(0.0);

	double width;
	IntegDelE = 0.0;
	double val;
	double EnergyZero = 0.0;
	double EnergyOne = 0.0;

	std::map<double, double>::iterator prev = fnVals.begin();
	for (std::map<double, double>::iterator it = fnVals.begin(); it != fnVals.end(); ++it)
	{
		if (it == fnVals.begin())	//skip the first one, which will set it = index 1 and prev at 0.
		{
			EnergyZero = it->first;
			continue;
		}
		if (prev == fnVals.begin())
		{
			EnergyOne = it->first;
		}
		width = it->first - prev->first;
		val = 0.5 * width * (it->second + prev->second);	//trapezoidal sum integration
		if (MY_IS_DEN(val) == true)
		{
			//			double den = DENORMALPOS;
			//			double absmin = ABSMIN;
			IntegDelE += val;
		}

		++prev;
	}
	if (IntegDelE == 0.0)
	{
		//this means that ALL the energies were zero, which they should NOT be.
		//Ensure non-zero by saying the lowest value is the only one to output anything.
		width = EnergyOne - EnergyZero;
		IntegDelE = 0.5 * width;
		fnVals.clear();	//clear out the function of generated values
		fnVals.insert(std::pair<double, double>(EnergyZero, 1.0));	//and put a good start
		fnVals.insert(std::pair<double, double>(EnergyOne, 0.0));	//with a quick finish
		//note, it is guaranteed to have at least 2 values from the get-go!
	}
	goodIntegral = true;
	if (IntegDelE > 0.0)
		IntegDelEI = 1.0 / IntegDelE;
	else
		IntegDelEI = 0.0;

	return(IntegDelE);
}

double SourceSpectrumDistribution::GetIntegralI()
{
	if (ltVar == NULL)
		return(0.0);

	if (goodIntegral == false)
		GetIntegral();	//then this will solve it

	return(IntegDelEI);
}

int SourceSpectrumDistribution::Clear()
{
	fnVals.clear();
	ltVar = NULL;
	which = SPECTRUM_DISTR_UNDEFINED;
	IntegDelE = 0.0;
	IntegDelEI = 0.0;
	goodIntegral = false;
	//the other variables shouldn't really matter...
	return(0);
}


OpticalTransitionData::OpticalTransitionData()
{
	lastIterUpdated = -1;
	weightAbsorb = 0.0;
	weightEmit = 0.0;
	energy = 0.0;
	TransitionData.clear();
	forceFindTransitions = false;
}

OpticalTransitionData::~OpticalTransitionData()
{
	lastIterUpdated = -1;
	weightAbsorb = 0.0;
	weightEmit = 0.0;
	energy = 0.0;
	TransitionData.clear();
	forceFindTransitions = false;
}

TransitionPair::TransitionPair()
{
	source = NULL;
	target = NULL;
	weightAbsorb = weightEmit = 0.0;
	origTarget.clear();
}

TransitionPair::~TransitionPair()
{
	source = NULL;
	target = NULL;
	origTarget.clear();
}

SubOptStorage::SubOptStorage()
{
	absorbRate = emitRate = alphaxFactor = 0.0;

}

SubOptStorage::~SubOptStorage()
{
	absorbRate = emitRate = alphaxFactor = 0.0;
}

OpticalStorage::OpticalStorage()
{
	size = 0;
	data = NULL;
	Indices.clear();
}

OpticalStorage::~OpticalStorage()
{
	size = 0;
	delete[] data;
	data = NULL;
	Indices.clear();
}

double OpticalStorage::GetAbsorb(int index)
{
	if (index < 0 || index >= size)
		return(0.0);
	return(data[index].absorbRate);
}

double OpticalStorage::GetEmit(int index)
{
	if (index < 0 || index >= size)
		return(0.0);
	return(data[index].emitRate);
}

int OpticalStorage::UseRemoveNext(double& absorb, double& emit, double& ax)
{
	if (Indices.size() > 0)
	{
		//get the index
		std::set<int>::iterator it = Indices.begin();
		int index = *it;
		//return the data
		absorb = data[index].absorbRate;
		emit = data[index].emitRate;
		ax = data[index].alphaxFactor;
		//clear out the data

		data[index].absorbRate = 0.0;
		data[index].emitRate = 0.0;
		data[index].alphaxFactor = 0.0;
		//remove references to this index
		Indices.erase(it);
		return(index);
	}
	else
	{
		absorb = 0.0;
		emit = 0.0;
		ax = 0.0;
		return(-1);
	}
}

int OpticalStorage::SetAbsorbEmit(int index, double absorb, double emit)
{
	if (index < 0 || index >= size)
		return(-1);
	data[index].absorbRate = absorb;
	data[index].emitRate = emit;
	Indices.insert(index);	//this should pretty much always be a relatively small tree
	return(0);
}

int OpticalStorage::AddAbsorbEmit(int index, double absorb, double emit, double ax)
{
	if (index < 0 || index >= size)
		return(-1);
	data[index].absorbRate += absorb;
	data[index].emitRate += emit;
	data[index].alphaxFactor += ax;
	Indices.insert(index);
	return(0);
}

int OpticalStorage::SetAbsorb(int index, double absorb)
{
	if (index < 0 || index >= size)
		return(-1);
	data[index].absorbRate = absorb;
	Indices.insert(index);
	return(0);
}

int OpticalStorage::SetEmit(int index, double emit)
{
	if (index < 0 || index >= size)
		return(-1);
	data[index].emitRate = emit;
	Indices.insert(index);
	return(0);
}

int OpticalStorage::Allocate(unsigned int num)
{
	size = num;
	delete[] data;	//if data = null, oh well
	data = new SubOptStorage[num];
	Indices.clear();
	return(0);
}

int OpticalStorage::Clear()
{
	delete[] data;
	data = NULL;
	size = 0;
	Indices.clear();
	return(0);
}


OpticalWeightELevel::OpticalWeightELevel()
{
	OpticalWeightFE = NULL;
	OpticalWeight1MFE = NULL;
	densStateFactor = NULL;
	sz = 0;
	iterUpdate = 0;
}

OpticalWeightELevel::OpticalWeightELevel(const OpticalWeightELevel& opt) {
	sz = opt.sz;
	iterUpdate = opt.iterUpdate;
	if (opt.OpticalWeight1MFE && sz > 0) {
		OpticalWeight1MFE = new double[sz];
		for (int i = 0; i < sz; ++i)
			OpticalWeight1MFE[i] = opt.OpticalWeight1MFE[i];
	}
	else
		OpticalWeight1MFE = nullptr;

	if (opt.OpticalWeightFE && sz > 0) {
		OpticalWeightFE = new double[sz];
		for (int i = 0; i < sz; ++i)
			OpticalWeightFE[i] = opt.OpticalWeightFE[i];
	}
	else
		OpticalWeightFE = nullptr;

	if (opt.densStateFactor && sz > 0) {
		densStateFactor = new double[sz];
		for (int i = 0; i < sz; ++i)
			densStateFactor[i] = opt.densStateFactor[i];
	}
	else
		densStateFactor = nullptr;
}

OpticalWeightELevel::~OpticalWeightELevel()
{
	delete[] OpticalWeightFE;
	OpticalWeightFE = NULL;
	delete[] OpticalWeight1MFE;
	OpticalWeight1MFE = NULL;
	delete[] densStateFactor;
	densStateFactor = NULL;
	sz = 0;
	iterUpdate = 0;
}

double OpticalWeightELevel::GetData(int index, bool which)
{
	if (index < sz)	//sz can only be the size of the array. if sz = 0, then index will always fail being unsigned
	{
		if (which == OPTIC_WEIGHT_FE)
			return(OpticalWeightFE[index]);
		else //if which == OPTIC_WEIGHT_1MFE
			return(OpticalWeight1MFE[index]);
	}
	ProgressInt(58, index);
	return(0.0);
}


unsigned int OpticalWeightELevel::InitializeData(ELevel* eLev, unsigned long int curiter, double res)
{
	if (eLev == NULL)	//bad data. Somehow.
		return(0);

	if (res <= 0.0 && sz == 0)	//can't initialize it at all - bad data
		return(0);



	if (curiter == iterUpdate && sz > 0)	//this is already up to date
		return(sz);
	bool calcDState = false;
	if (res > 0.0 && sz == 0)
	{
		double range = eLev->max - eLev->min;
		sz = unsigned int(floor(range / res)) + 2;	//the obvious range, plus one at either end
		OpticalWeight1MFE = new double[sz];	//sz was zero, so then this hasn't been initialized
		OpticalWeightFE = new double[sz];	//sz was zero, so then this hasn't been initialized
		densStateFactor = new double[sz];	//this is constant through the entire simulation - only calculate it once. fair amount of exp/log etc. Don't want to do this for every state every time!
		calcDState = true;
		for (int i = 0; i < sz; ++i)
			OpticalWeight1MFE[i] = OpticalWeightFE[i] = densStateFactor[i] = 0.0;	//make sure they are all zero!
	}

	eLev->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);	//E-Ef for electrons, Ef-E for holes
	double max, min, fermi, curEnergy, nextEnergy;	//these will store the absolute min/max/eUse energies for this state
//	eLev->CalculateAbsoluteHighLowEnergy(max, min);
	eLev->CalculateRelativeHighLowEnergy(max, min);	//relative to the VB edge - make optical spacing consistent even for between states of different gaps
	//for f(e), care if Ef
	fermi = eLev->AbsFermiEnergy - eLev->getPoint().vb.bandmin;	//now fermi is also relative to the VB edge
	bool safeExpFe = (fermi < min) ? true : false;	//this is usually the case - trying to multiply by constant in exponent so values stay near one and don't become indeterminately small
	bool safeExp1MFe = (fermi > max) ? true : false;	//also usually the case, if one exp goes tiny, it's addedto one & irrelevant, but the numerator maintains more accuracy at different spread
	double degen = (eLev->imp == NULL) ? 1.0 : eLev->imp->getDegeneracy();

	
	curEnergy = floor(min/res)*res;	//the starting energy is now an even optical spacing from the VB everywhere!
	nextEnergy = curEnergy + res;
	double totalIFE = 0.0;
	double totalIMFE = 0.0;
	double useCurEnergy = curEnergy;	//what to use for the fermi level distributions. CurEnergy might not actually be in the range of the state

	//for the bands, assume square root. Impurities/defects, use the function defined. Band tails, use the exponential. 
	//for the impurities - it DEFINITELY matters and is complicated. If exponential or linear, it doesn't matter. If parabolic or gaussian, the position of the state does matter because the function cannot be multiplied by just a constant to maintain the same shape

	if (eLev->getPoint().temp == 0.0)	//this is the easy one...
	{
		for (int i = 0; i<sz; ++i)
		{
			if (calcDState)
				densStateFactor[i] = eLev->CalculateDensStateFactor(curEnergy, nextEnergy);

			double tmpMax = (nextEnergy > max) ? max : nextEnergy;	//tmp variable for max energy. Smaller of maxes
			double tmpMin = (curEnergy < min) ? min : curEnergy;	//tmp variable for min energy. Largerer of mins
			useCurEnergy = 0.5 * (tmpMax + tmpMin);

			if (useCurEnergy < fermi)
			{
				OpticalWeight1MFE[i] = 0.0;
				OpticalWeightFE[i] = 1.0;
			}
			else if (useCurEnergy > fermi)
			{
				OpticalWeight1MFE[i] = 1.0;
				OpticalWeightFE[i] = 0.0;
			}
			else
			{
				OpticalWeightFE[i] = densStateFactor[i] / (1.0 + degen);
				OpticalWeight1MFE[i] = densStateFactor[i] * degen / (1.0 + degen);
			}
			totalIFE += OpticalWeightFE[i];
			totalIMFE += OpticalWeight1MFE[i];

			curEnergy = nextEnergy;
			nextEnergy += res;
		}
	}
	else
	{
		double beta = KBI / eLev->getPoint().temp;
		double sExFeConst = (safeExpFe == true) ? exp(beta * (fermi - min)) : 1.0;
		//essentially multiply the fermi function by exp(-B*(Ef-Emin)) to cancel out a large portion of the exponential. It's a partition function, so who cares?
		double denFe, denMFe, numMFe;
		for (int i = 0; i<sz; ++i)
		{
			if (calcDState)
				densStateFactor[i] = eLev->CalculateDensStateFactor(curEnergy, nextEnergy);

			denFe = (nextEnergy > max) ? max : nextEnergy;	//tmp variable for max energy. Smaller of maxes
			denMFe = (curEnergy < min) ? min : curEnergy;	//tmp variable for min energy. Largerer of mins
			useCurEnergy = 0.5 * (denFe + denMFe);

			//now actually assign denFe/denMFe as intended
			if (safeExpFe)
				denFe = sExFeConst + degen * exp(beta * (useCurEnergy - min));
			else
				denFe = 1.0 + degen * exp(beta * (useCurEnergy - fermi));

			if (safeExp1MFe) //here, we're multiplying by the inverse of the exponent in 1-f(E): exp(B*(fermi-Emax))
			{
				denMFe = 1.0 + degen * exp(beta * (useCurEnergy - fermi));
				numMFe = degen * exp(beta * (useCurEnergy - max));
			}
			else
			{
				numMFe = degen * exp(beta * (useCurEnergy - fermi));
				denMFe = 1.0 + numMFe;
			}

			OpticalWeightFE[i] = densStateFactor[i] / denFe;
			OpticalWeight1MFE[i] = densStateFactor[i] * numMFe / denMFe;
			totalIFE += OpticalWeightFE[i];	//update the totals
			totalIMFE += OpticalWeight1MFE[i];

			curEnergy = nextEnergy;
			nextEnergy += res;
		}
	}
	//inverse the totals, division is slow, so only do it once
	if (totalIFE > 0.0)
		totalIFE = 1.0 / totalIFE;
	if (totalIMFE > 0.0)
		totalIMFE = 1.0 / totalIMFE;

	//now normalize values
	for (int i = 0; i<sz; ++i)
	{
		OpticalWeight1MFE[i] = OpticalWeight1MFE[i] * totalIMFE;
		OpticalWeightFE[i] = OpticalWeightFE[i] * totalIFE;
	}

	iterUpdate = curiter;	//flag to not do this calculation anymore
	return(sz);
}





/*
bool ELevelOpticalRate::DetermineNumCarrierTransfer(double& numCar, double NRate)
{
if(source == NULL)
{
ProgressString("Determining number of carriers, but no source state defined");
return(false);	//don't limit by availabilty. THough honestly, it doesn't matter
}
double srcElec, srcHole;
double trgElec, trgHole;
double srcNS = source->GetNumstates();

bool backtoNull = false;
if(target == NULL)	//it is going to a contact, which is a duplicate point
{
target = source;
backtoNull = true;	//after this is done, make target null again. (flag)
}
double trgNS = target->GetNumstates();
srcElec = source->GetBeginElec();
srcHole = source->GetBeginHole();
trgElec = target->GetBeginElec();
trgHole = target->GetBeginHole();
numCar = source->GetBeginOccStates();

bool LimitByAvailability = false;


if(source->imp)
{
if(source->carriertype == ELECTRON)
{
numCar = srcElec;	//default value for donor as the source
if(source->getPoint().largestOccES * LEVELINSIGNIFICANT > srcElec)	//this is an insignificant donor
{
if(target->imp)
{
if(target->carriertype == ELECTRON) //source/target both donors
{
if(target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)
numCar = source->getPoint().largestOccES;
else if(NRate > 0.0) //insig source, sig target, source gains carriers
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccES;
}
else //the target is an acceptor
{
if(target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//both insignificant
numCar = source->getPoint().largestOccES;	//just extend timestep
else if(NRate > 0.0) //insig source, sig target, source gains carriers. Limit by what it can accept
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccES;
}	//end target being an acceptor
} //end target being an impurity
else //target is in band
{
if(target->carriertype == ELECTRON) //target is in CB
{
if(target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)
numCar = source->getPoint().largestOccES;
else if(NRate > 0.0)
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccES;
}
else //target carrier is VB
{
if(target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
numCar = source->getPoint().largestOccES;	//insignificant VB & donor, just increase timestep
else if(NRate > 0.0)
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccES;
}	//end target being in VB
} //end target being in band
} //end source being insignificant
} //end source being a donor
else //the source impurity is an acceptor
{
numCar = srcHole;	//default value for acceptor as the source
if(source->getPoint().largestUnOccES * LEVELINSIGNIFICANT > srcHole)	//this is an insignificant donor
{
if(target->imp)
{
if(target->imp->reference == CONDUCTION) //source/target both donors
{
if(target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)
numCar = source->getPoint().largestUnOccES;
else if(NRate > 0.0) //insig source, sig target, source gains carriers
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestUnOccES;
}
else //the target is an acceptor
{
if(target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//both insignificant
numCar = source->getPoint().largestOccES;	//just extend timestep
else if(NRate > 0.0) //insig source, sig target, source gains carriers. Limit by what it can accept
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestUnOccES;
}	//end target being an acceptor
} //end target being an impurity
else //target is in band
{
if(target->carriertype == ELECTRON) //target is in CB
{
if(target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)
numCar = source->getPoint().largestUnOccES;
else if(NRate > 0.0)
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestUnOccES;
}
else //target carrier is VB
{
if(target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
numCar = source->getPoint().largestUnOccES;	//insignificant VB & donor, just increase timestep
else if(NRate > 0.0)
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestUnOccES;
}	//end target being in VB
} //end target being in band
} //end source being insignificant
} //end source being an acceptor
} //end source being an impurity
else //it is not an impurity or defect
{
if(source->carriertype == ELECTRON) //source is the conduction band
{
if(source->getPoint().largestOccCB * LEVELINSIGNIFICANT > srcElec)	//the source is insignificant
{
if(target->imp)
{
if(target->imp->reference == CONDUCTION) //target is donor
{
if(target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)	//the donor has a comparatively insignificant number of carriers
numCar = source->getPoint().largestOccCB;
else if(NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccCB;
}
else //target is acceptor
{
if(target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//the acceptor has a comparatively insignificant number of carriers (holes)
numCar = source->getPoint().largestOccCB;
else if(NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
{
numCar = srcHole;
LimitByAvailability = true;
}
else //CB is insignificant, acceptor is significant, but carriers leaving the CB, so just maintain high time step and cap transfer in source
numCar = source->getPoint().largestOccCB;
}
}
else
{
if(target->carriertype == ELECTRON)
{
if(target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec)	//both are insignificant, just increase the timestep and cap limits will keep things from blowing up
numCar = source->getPoint().largestOccCB;
else if(NRate > 0.0)	//electrons are entering the insignificant source from a significant target
{
numCar = srcHole;	//there are this many places that can accept electrons
LimitByAvailability = true;
}
else //an insignificant source is sending carriers to a significant spot. There is a tiny timestep on the source(which doesn't matter), and a huge timestep on the target, so just increase the source timestep
numCar = source->getPoint().largestOccCB;
}
else //transition is from CB to VB
{
if(target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
numCar = source->getPoint().largestOccCB;
else if(NRate > 0.0) // the CB is gaining electrons
{
numCar = srcHole;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccCB;
}	//end target not being an impurity
}	//end target not being an impurity
}	//end saying the source is insignificant
}	//end saying the source is the in the conduction band
else // source is the valence band
{
if(source->getPoint().largestOccVB * LEVELINSIGNIFICANT > srcHole)	//the source is an insignificant position
{
if(target->imp)
{
if(target->imp->reference == CONDUCTION)	//target is donor
{
if(target->getPoint().largestOccES * LEVELINSIGNIFICANT > trgElec)	//the donor has a comparatively insignificant number of carriers
numCar = source->getPoint().largestOccVB;
else if(NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccVB;
}
else //target is acceptor
{
if(target->getPoint().largestUnOccES * LEVELINSIGNIFICANT > trgHole)	//the acceptor has a comparatively insignificant number of carriers (holes)
numCar = source->getPoint().largestOccVB;
else if(NRate > 0.0)	//the CB point is not significant & gaining carriers from a significant donor
{
numCar = srcElec;
LimitByAvailability = true;
}
else //CB is insignificant, acceptor is significant, but carriers leaving the CB, so just maintain high time step and cap transfer in source
numCar = source->getPoint().largestOccVB;
}
}
else //the target is in the CB or VB
{
if(target->carriertype == ELECTRON) //target is CB
{
if(target->getPoint().largestOccCB * LEVELINSIGNIFICANT > trgElec) //target & source are insignificant
numCar = source->getPoint().largestOccVB;
else if(NRate > 0.0)	//the source of holes is gaining more holes, and source is insignificant while target is significant
{
numCar = srcElec;
LimitByAvailability = true;
}
else //insignificant source of holes is sending holes out to a significant concentration - will not have large effect, just increase timestep
numCar = source->getPoint().largestOccVB;
}
else //target is VB
{
if(target->getPoint().largestOccVB * LEVELINSIGNIFICANT > trgHole)
numCar = source->getPoint().largestOccVB;
else if(NRate > 0.0) //a strong hole is sending holes
{
numCar = srcElec;
LimitByAvailability = true;
}
else
numCar = source->getPoint().largestOccVB;
}	//end saying target is valence band
}	//end saying target is in a band
}	//end saying the source is an insignificant point
}	//end saying the source is the valence band
}	//end saying the source is a band

if(backtoNull)
target = NULL;

return(LimitByAvailability);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator ==(const ELevelOpticalRate &b) const
{
if(target != b.target)
return(false);
if(type != b.type)
return(false);
if(carrier != b.carrier)
return(false);
if(source != b.source)	//the uniqueID's should match no matter what
return(false);

return(true);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator !=(const ELevelOpticalRate &b) const
{
if(target != b.target)
return(true);
if(type != b.type)
return(true);
if(carrier != b.carrier)
return(true);
if(source != b.source)
return(true);

return(false);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator <(const ELevelOpticalRate &b) const
{
//null is always less
if(target && b.target)
{
if(target->uniqueID < b.target->uniqueID)
return(true);
else if(target->uniqueID > b.target->uniqueID)
return(false);
}
else if(target && b.target == NULL)
return(false);
else if(target==NULL && b.target)
return(true);
//else if both are null: look at next variable

if(type < b.type)
return(true);
else if(type > b.type)
return(false);

if(carrier == false && b.carrier == true)	//assume true > false
return(true);
else if(carrier != b.carrier)	//this=true , b=false ">" = true
return(false);

if(source && b.source)
{
if(source->uniqueID < b.source->uniqueID)
return(true);
else if(source->uniqueID > b.source->uniqueID)
return(false);
}
else if(source && b.source == NULL)	//this should be the same when comparing ELevelRates in binary trees. This should be the only one that needs to be fixed to work with STD::MAPS
return(false);
else if(source == NULL && b.source)
return(true);

//then these rates are fundamentally the same
return(false);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator >(const ELevelOpticalRate &b) const
{
if(target && b.target)
{
if(target->uniqueID > b.target->uniqueID)
return(true);
else if(target->uniqueID < b.target->uniqueID)
return(false);
}
else if(target && b.target == NULL)
return(true);
else if(target == NULL && b.target)
return(false);

if(type > b.type)
return(true);
else if(type < b.type)
return(false);

if(carrier == true && b.carrier == false)
return(true);
else if(carrier != b.carrier)
return(false);

if(source && b.source)
{
if(source->uniqueID > b.source->uniqueID)
return(true);
else if(source->uniqueID < b.source->uniqueID)
return(false);
}
else if(source && b.source == NULL)
return(true);
else if(source == NULL && b.source)
return(false);

return(false);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator <=(const ELevelOpticalRate &b) const
{
//null is always less
if(target && b.target)
{
if(target->uniqueID < b.target->uniqueID)
return(true);
else if(target->uniqueID > b.target->uniqueID)
return(false);
}
else if(target && b.target == NULL)
return(false);
else if(target==NULL && b.target)
return(true);
//else if both are null: look at next variable

if(type < b.type)
return(true);
else if(type > b.type)
return(false);

if(carrier == false && b.carrier == true)	//assume true > false
return(true);
else if(carrier != b.carrier)	//this=true , b=false ">" = true
return(false);

if(source && b.source)
{
if(source->uniqueID < b.source->uniqueID)
return(true);
else if(source->uniqueID > b.source->uniqueID)
return(false);
}
else if(source && b.source == NULL)	//this should be the same when comparing ELevelRates in binary trees. This should be the only one that needs to be fixed to work with STD::MAPS
return(false);
else if(source == NULL && b.source)
return(true);

//then these rates are fundamentally the same
return(true);
}

//note, these do NOT include equilibrium values - those are actually data
bool ELevelOpticalRate::operator >=(const ELevelOpticalRate &b) const
{
if(target && b.target)
{
if(target->uniqueID > b.target->uniqueID)
return(true);
else if(target->uniqueID < b.target->uniqueID)
return(false);
}
else if(target && b.target == NULL)
return(true);
else if(target == NULL && b.target)
return(false);

if(type > b.type)
return(true);
else if(type < b.type)
return(false);

if(carrier == true && b.carrier == false)
return(true);
else if(carrier != b.carrier)
return(false);

if(source && b.source)
{
if(source->uniqueID > b.source->uniqueID)
return(true);
else if(source->uniqueID < b.source->uniqueID)
return(false);
}
else if(source && b.source == NULL)
return(true);
else if(source == NULL && b.source)
return(false);

return(true);	//they were equal
}

*/
