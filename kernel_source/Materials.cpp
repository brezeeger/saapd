#include "Materials.h"
#include "light.h"
#include "physics.h"

#include "model.h"

Material::Material(ModelDescribe& md, int id, std::string name, std::string describe,
	double eg, double mun, double mup, double phi, float epsR, float muR,
	short type, double ni, double nc, double nv,
	double efmec, double efmhc, double efmed, double efmhd,
	double tahThGenRec, bool direct, Optical* optic,
	double urbvbe, double urbcbe, double urbVbCapVb, double urbVbCapCb,
	double urbCbCapVb, double urbCbCapCb, double spHeatCap, double Thcond) : mdesc(md)
{
	_name = name;
	_describe = describe;
	_type = type;
	_epsR = epsR;
	_muR = muR;
	_ni = ni;
	_Nc = nc;
	_Nv = nv;
	_EFMeCond = efmec;
	_EFMhCond = efmhc;
	_EFMeDens = efmed;
	_EFMhDens = efmhd;
	_MuN = mun;
	_MuP = mup;
	_Eg = eg;	//signal that it hasn't been initialized (metals set to Eg 0.0)
	_Phi = phi;
	_SpHeatCap = spHeatCap;	//we'll leave this at 0, but value is 0.7 J /G /K
	_ThCond = Thcond;	//and this is 1.3 W /cm / K
	/*	_urbachVB.energy = urbvbe;	//say they don't exist
	_urbachCB.energy = urbcbe;
	_urbachCB.band = CONDUCTION;
	_urbachVB.band = VALENCE;
	_urbachCB.captureCB = urbachCB.captureVB = urbachVB.captureCB = urbachVB.captureVB = 0.0;
	_tauThGenRec = 4.545e-4;	//alpha = 1.1e-14 cm^3/s for n=p=1e17 and lower. W. Gerlach et all, phys. stat. sol. a 13, 277 (1972). Math leads to this value.
	_optics = NULL;

	_id=-1;
	_defects.clear();
	_OpticalFluxOut = NULL;
	_OpticalAggregateOut = NULL;
	_direct = false;
	*/
}
Material::~Material()
{
	/*
	name="";
	describe="";
	type=UNDEF;
	epsR=0.0;
	ni = 0.0;
	Nc = 0.0;
	Nv = 0.0;
	EFMeCond = 0.0;
	EFMhCond = 0.0;
	EFMeDens = 0.0;
	EFMhDens = 0.0;
	MuN = 0.0;
	MuP = 0.0;
	Eg = 0.0;
	Phi = 0.0;
	SpHeatCap = 0.0;
	ThCond = 0.0;
	urbachCB.energy = 0.0;
	urbachVB.energy = 0.0;
	
	optics = NULL;
	defects.clear();	//just clear the pointers.  This will not clear the impurity from the model's memory.
	//that will be cleared when mdl is cleared out. Can't erase without otherwise screwing up pointers
	//in mdl
	*/
	delete _OpticalFluxOut;
	delete _OpticalAggregateOut;
}

Impurity* Material::getDefectByID(int id) const {
	for (auto it = _defects.begin(); it != _defects.end(); ++it) {	//most models will only have a few defects per material
		if ((*it)->isImpurity(id))
			return *it;
	}
	return nullptr;
}

Impurity* Material::getDefectByName(std::string name) const {
	for (auto it = _defects.begin(); it != _defects.end(); ++it) {	//most models will only have a few defects per material
		if ((*it)->isImpurity(name))
			return *it;
	}
	return nullptr;
}

void Material::pairWithDefect(Impurity* defect) {
	if (defect == nullptr)
		return;
	
	Material *otherMat = defect->getDefectMaterial();
	//unlink the previous material
	if (otherMat && otherMat != this)
		otherMat->unpairWithDefect(defect);

	defect->setDefectMaterial(this);	//make sure it properly is linked back from the defect/impurity

	//check if this is already present. Compare pointer values for same memory.
	for (auto it = _defects.begin(); it != _defects.end(); ++it) {	//most models will only have a few defects per material
		if ((*it) == defect) return;	//nothing to add
	}
	_defects.push_back(defect);
	mdesc.addUniqueImpurity(defect);	//ensure it is included in mdesc impurities!

	return;
}

bool Material::unpairWithDefect(Impurity* defect) {
	if (defect == nullptr)
		return false;
	
	if (defect->getDefectMaterial() == this) //be safe in case it was already set to something else
		defect->setDefectMaterial();	//clear out the pointer if it goes here.
	
	//check if this is already present. Compare pointer values for same memory.
	for (auto it = _defects.begin(); it != _defects.end(); ++it) {	//most models will only have a few defects per material
		if ((*it) == defect) { //the defect was indeed paired
			_defects.erase(it);	//n ops, but I expect n to be small and this not to be called often
			return true;	//nothing to add
		}
	}
	
	return false;
}

bool Material::SetCustomState(bool band, double energy, double efm, double density) {
	if (band == conduction)
		return SetCBCustomState(energy, efm, density);
	return SetVBCustomState(energy, efm, density);
}

bool Material::SetCBCustomState(double energy, double efm, double density) {
	if (density < 0.0)
		density = 0.0;
	if (efm < 0.0)
		efm = 0.0;
	CustomDensStates st;
	st.density = density;
	st.EFMCond = efm;

	SortedVectorInsertReplace(_CBCustom, std::pair<double, CustomDensStates>(energy, st));
	return true;
}

bool Material::SetVBCustomState(double energy, double efm, double density) {
	if (density < 0.0)
		density = 0.0;
	if (efm < 0.0)
		efm = 0.0;
	CustomDensStates st;
	st.density = density;
	st.EFMCond = efm;

	SortedVectorInsertReplace(_VBCustom, std::pair<double, CustomDensStates>(energy, st));
	return true;
}

Corrections::Corrections(void)
{
	ni = 1.0;
}
Corrections::~Corrections(void)
{
	ni = 0.0;
}

void Material::CalcCorrections(double temp)
{
	//first calculate correction factor to the ni's based on temperature.
	//it's Temp dependence is T^1/5 exp(-Eg/2kT)
	//to correct the first part, multiply by (T/300)^1.5
	//the second part is to multiply by exp(-Eg/2kT) / exp(-Eg/600k))
	// = exp(-Eg/2kT + Eg/2k300) = exp(-300Eg/2kT300 + TEg/2kT300)
	// = exp[Eg(T-300)/(600kT)]
	if (temp == 300 || temp <= 0)
		_cfactor.ni = 1.0;
	else //do the correction calculation
	{
		double t1 = temp / 300.0;	//get the first temp dependence
		t1 = pow(t1, 1.5);	//finish it off...

		double t2 = temp - 300.0;
		t2 = t2 * _Eg;
		double den = 600.0 * KB * temp;
		t2 = t2 / den;
		t2 = exp(t2);
		_cfactor.ni = t1 * t2;
	}

	return;
}

bool Material::AddOpticalData(unsigned char which, double Energy, double photons) {
	Energy = mdesc.getSimulation()->GetBinnedEnergy(Energy);
	if (Energy <= 0.0 || which > BulkOpticalData::lastType || photons < 0.0)	//it wasn't in the resolution specified or was just bad.
		return false;

	if (_OpticalFluxOut == nullptr)	//nothing to store the data in yet.
		_OpticalFluxOut = new BulkOpticalData();

	_OpticalFluxOut->AddData(Energy, which, photons);
	return true;
}

bool Material::AggregateOpticalData(double factor) {
	if (_OpticalFluxOut) {
		if (_OpticalAggregateOut == nullptr)
			_OpticalAggregateOut = new BulkOpticalData();
		_OpticalAggregateOut->AddExistingBulkData(_OpticalFluxOut, factor);
		return true;
	}
	return false;
}

void Material::ResetOpticalFlux(bool includeAggregate) {
	if (_OpticalFluxOut)
		_OpticalFluxOut->ClearAll();
	if (includeAggregate && _OpticalAggregateOut)
		_OpticalAggregateOut->ClearAll();
}

void Material::Validate(ModelDescribe& mdesc) {
	for (unsigned int i = 0; i < _defects.size(); ++i) {
		if (mdesc.HasImpurity(_defects[i]) == false) {
			_defects.erase(_defects.begin() + i);
			--i;
		}
	}
}




void NK::UpdateK(double newk)
{
	double ratio = _k != 0.0 ? newk / _k : 0.0;
	_k = newk;
	_alpha *= ratio;
}

void NK::UpdateAlpha(double newa)
{
	double ratio = _alpha != 0.0 ? newa / _alpha : 0.0;
	_alpha = newa;
	_k *= ratio;
}

Optical::~Optical()
{
	WavelengthData.clear();
}
Optical::Optical()
{
	WavelengthData.clear();
	_n = 1.0;
	_a = 0;
	_b = 0;
	
	opticEfficiency = -1.0;	//default to not initialized
}

NK::NK(float lambda, double n, double k, double a, bool prefMaintainAlpha) {
	_n = n;
	if (lambda > 0.0 && k > 0.0 && a < 0.0)
	{
		_k = k;
		_alpha = 4.0e7 * PI * _k / double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
	}
	else if (lambda > 0.0 && k < 0.0 && a > 0.0)
	{
		_alpha = a;
		k = _alpha * 2.5e-8 * PII * double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
	}
	else if (lambda <= 0.0 && k > 0.0 && a > 0.0)
	{
		_k = k;
		_alpha = a;
//		lambda = float(4.0e7 * PI * k / alpha);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
	}
	else if (lambda > 0.0 && k > 0.0 && a > 0.0)
	{
		_k = k;
		_alpha = a;
		if (!prefMaintainAlpha)
		{
			double tmpAlpha = 4.0e7 * PI * k / double(lambda);	//4 pi k / lambda, lambda in nm, want cm^-1; nm^-1 = 1e7 cm^-1
			if (fabs((tmpAlpha - _alpha) / tmpAlpha) > 0.01)	//replace alpha if it's just WAAAY off
				_alpha = tmpAlpha;
		}
		else
		{
			double tmpK = a * 2.5e-8 * PII * double(lambda);;
			if (fabs((tmpK - k) / tmpK) > 0.01)	//replace alpha if it's just WAAAY off
				k = tmpK;
		}
	}
	else { //invalid. mark as such
		_n = -1.0;
		_k = -1.0;
		_alpha = -1.0;
	}

}

bool Optical::RemoveNearestWavelengthLambda(float lambda, float maxDelta) {
	if (WavelengthData.size() == 0)
		return false;
	if (isWavelengthNeedSort())
		SortWavelengths();
	
	unsigned int index = SortedVectorFindClosest(WavelengthData, lambda);
	//returns -1 if size is 0. logically, already know that won't be the case
	//and the found index must be valid.
	if (maxDelta < 0.0f) {
		WavelengthData.erase(WavelengthData.begin() + index);
		return true;
	}
	else {
		float delta = abs(WavelengthData[index].first - lambda);
		if (delta <= maxDelta) {
			WavelengthData.erase(WavelengthData.begin() + index);
			return true;
		}
	}
	return false;
}

bool Optical::RemoveWavelengthIndex(unsigned int index) {
	if (isWavelengthNeedSort())
		SortWavelengths();
	
	if (index < WavelengthData.size()) {
		WavelengthData.erase(WavelengthData.begin() + index);
		return true;
	}
	return false;
}

bool Optical::modifyWaveDataIndex(unsigned int index, double value, unsigned char which) {
	if (index < WavelengthData.size())
		return false;

	if (isWavelengthNeedSort())
		SortWavelengths();

	if (which == NK::absorption_coefficient && value >= 0.0) {
		WavelengthData[index].second.UpdateAlpha(value);
	}
	else if (which == NK::extinction_coefficient && value>=0.0) {
		WavelengthData[index].second.UpdateK(value);
	}
	else if (which == NK::index_refraction && value!=0.0) {
		WavelengthData[index].second.setN(value);
	}
	else if (which == NK::wavelength && value>0.0) {
		WavelengthData[index].first = value;
		VectorDoubleShrinkingBubbleSort(WavelengthData);	//since one element moved, this algorithm
		//will shift everything necessary by one in either the first pass up or the second pass down
	}
	else
		return false;
	return true;
}

bool Optical::AddWavelength(float lambda, double n, double k, double a, bool overWrite, bool prefMaintainAlpha, bool checkExists)
{
	NK tmpNK(lambda, n, k, a, prefMaintainAlpha);
	if (tmpNK.isInvalid())
		return(false); //it was an invalid think to try!
	//either update the wavelength data or put a new one in!

	if (checkExists) {
		if (isWavelengthNeedSort())
			SortWavelengths();
		return SortedVectorInsertReplace(WavelengthData, std::pair<float, NK>(lambda, tmpNK), !overWrite);
	}
	else if (overWrite) {	//tag to end so it gets priority. Basically a fast add/push_back
		WavelengthData.push_back(std::pair<float, NK>(lambda, tmpNK));
		setWavelengthUnsorted();
		return true;
	}
	else { //tag to beginning. really should never do this. inefficient.
		WavelengthData.insert(WavelengthData.begin(), std::pair<float, NK>(lambda, tmpNK));
		setWavelengthUnsorted();
		return true;	//no idea if it will keep when duplicate keys removed, cuz don't know if exists
	}
}

void Optical::GenerateWavelengthsFromAB(double startEnergy, double endEnergy, double deltaEnergy, double bandGap) {
	if (startEnergy < 0.0)
		startEnergy = 0.0;
	endEnergy = abs(endEnergy);
	Swap(startEnergy, endEnergy);	//make sure the start energy is smaller
	if (deltaEnergy <= 0.0)
		deltaEnergy = 0.01;
	if (bandGap < 0.0)
		bandGap = 0.0;

	int counter = 0;	//at higher energies, can afford to lose some of the accuracy...
	for (double energy = endEnergy; energy >= startEnergy; energy -= deltaEnergy) {
		if (energy <= 0.0)	//can't have a zero energy photon...
			break;
		double wvLen = PHOTONENERGYCONST / energy;	//start at high energy so wavelength starts low.
		double alpha = calcAlphaAB((float)wvLen, bandGap);

		AddWavelength((float)wvLen, _n, -1.0, alpha, true, true, false);	//equivalent to push_back. And it should be pushing back in order
		if (counter == 25)
		{
			deltaEnergy *= 2.0;
			counter = 0;
		}
		counter++;
	}
	SortWavelengths();
}

double Optical::calcAlphaAB(float wavelength, double bandGap)
{
	if (wavelength <= 0.0f)	//can't calculate it...
		return(0.0);
	double alpha = 0.0;
	//(A + B/hv) sqrt(hv - Eg)
	//a is inv cm - B is a constant
	//wv->lambda = float(PHOTONENERGYCONST / wv->energyphoton);
	double hvEnergy = PHOTONENERGYCONST / wavelength;	//aka 1240 nm * ev / nm...

	if (hvEnergy <= bandGap)
		return(0.0);

	double sqTerm = sqrt(hvEnergy - bandGap);
	double factor = _b / hvEnergy;	//hvEnergy can't be 0...

	//a is in cm^-1, but the calculation needs m^-1. 1/cm * 100cm/1m
	factor += _a * 100.0;
	alpha = sqTerm * factor;	//but this alpha then is in m^-1
	alpha /= 100.0;	// 1/1m * 1m / 100cm

	if (alpha < 0.0)	//don't think this should happen, but perhaps weird stuff with -B's or -A's
		alpha = 0.0;

	return(alpha);
}

double Optical::getAlpha(float wavelength, double bandGap)
{
	if (WavelengthData.size() == 0)
	{
		if (isUseAB() == false)
			return(0.0);

		return(calcAlphaAB(wavelength, bandGap));
	}

	if (isWavelengthNeedSort())
		SortWavelengths();

	return getWavelengthData(wavelength, NK::absorption_coefficient);

}

double Optical::getIndexRefractionEnergy(double energy) {
	if (energy <= 0.0)
		return 1.0;

	float lambda = float(PHOTONENERGYCONST / energy);
	double n = getWavelengthData(lambda, NK::index_refraction);
	return n != 0.0 ? n : 1.0;
}

double Optical::getIndexRefractionWavelength(float lambda) {
	if (lambda <= 0.0f)
		return 1.0;

	double n = getWavelengthData(lambda, NK::index_refraction);

	return n != 0.0 ? n : 1.0;	//negative indices of refraction are possible. Just do weird shit
}

double Optical::getExtinctionCoefficientEnergy(double energy) {
	if (energy <= 0.0)
		return 0.0;

	float lambda = float(PHOTONENERGYCONST / energy);
	return getWavelengthData(lambda, NK::extinction_coefficient);
}

double Optical::getExtinctionCoefficientWavelength(float lambda) {
	if (lambda <= 0.0f)
		return 0.0;

	return getWavelengthData(lambda, NK::extinction_coefficient);
}

double Optical::getWavelengthData(float wavelength, unsigned char which) {
	if (WavelengthData.size() == 0)
		return 0.0;

	if (isWavelengthNeedSort())
		SortWavelengths();

	unsigned int indexLessEqual = SortedVectorFindFirstLessEqual(WavelengthData, wavelength);
	if (indexLessEqual >= WavelengthData.size()) {	//there were no wavelengths that were less or equal
		if (WavelengthData.size() == 1)
			return WavelengthData[0].second.getValue(which);
		if (WavelengthData.size() == 2) {
			std::pair<float, double> v1(WavelengthData[0].first, WavelengthData[0].second.getValue(which));
			std::pair<float, double> v2(WavelengthData[1].first, WavelengthData[1].second.getValue(which));
			double ret = InterpolateValuesPositive(v1, v2, wavelength);
			return (ret < 0.0 ? 0.0 : ret);
		}
		std::pair<float, double> v1(WavelengthData[0].first, WavelengthData[0].second.getValue(which));
		std::pair<float, double> v2(WavelengthData[1].first, WavelengthData[1].second.getValue(which));
		std::pair<float, double> v3(WavelengthData[2].first, WavelengthData[2].second.getValue(which));
		double ret = InterpolateValuesPositive(v1, v2, v3, wavelength);
		return (ret < 0.0 ? 0.0 : ret);
	}

	//it found some matching values.
	std::pair<float, double> v[3];
	v[1].first = WavelengthData[indexLessEqual].first;
	v[1].second = WavelengthData[indexLessEqual].second.getValue(which);
	//edge cases
	if (indexLessEqual > 0) { //it's already known to be within the vector, so just not first element
		v[0].first = WavelengthData[indexLessEqual - 1].first;	//meaning the one before it is safe
		v[0].second = WavelengthData[indexLessEqual-1].second.getValue(which);
	}
	else //so here the matching element found was the first one
	{
		if (WavelengthData.size() == 1)
			return v[1].second;	//there was only one element, which was el 0, so return it.
		else if (WavelengthData.size() == 2) { //2 elements, found 0 index (which was supposedly middle)
			v[0].first = WavelengthData[1].first;
			v[0].second = WavelengthData[1].second.getValue(which);
			double ret = InterpolateValuesPositive(v[1], v[0], wavelength);
			return (ret < 0.0 ? 0.0 : ret);
		}
		v[0].first = WavelengthData[1].first;	//just do the first three
		v[0].second = WavelengthData[1].second.getValue(which);
		v[2].first = WavelengthData[2].first;
		v[2].second = WavelengthData[2].second.getValue(which);
		double ret = InterpolateValuesPositive(v[1], v[0], v[2], wavelength);
		return (ret < 0.0 ? 0.0 : ret);
	}
	//at this point, know 0 and 1 are safely assigned.
	if (indexLessEqual < WavelengthData.size() - 1) {	//there's room for the next one
		v[2].first = WavelengthData[indexLessEqual+1].first;
		v[2].second = WavelengthData[indexLessEqual+1].second.getValue(which);
	}
	else { //the element found was actually the last element
		if (WavelengthData.size() == 2) {	//there were 2 and it chose the last one
			double ret = InterpolateValuesPositive(v[1], v[0], wavelength);
			return (ret < 0.0 ? 0.0 : ret);
		}
		v[2].first = WavelengthData[indexLessEqual-2].first;
		v[2].second = WavelengthData[indexLessEqual-2].second.getValue(which);
	}

	double ret = InterpolateValuesPositive(v[0], v[1], v[2], wavelength);
	return (ret < 0.0 ? 0.0 : ret);

}