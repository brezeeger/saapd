#ifndef OUTPUT_DEBUG_H
#define OUTPUT_DEBUG_H

#include <vector>
#include <cstring>



//forward declarations
class ELevel;
class ChargeTransfer;
class Simulation;
class Point;
class Model;
class ModelDescribe;
class SS_CurrentDerivFactors;
class SteadyState_SAAPD;
class kernelThread;

int CompareMatrixOutputs(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
double PercentError(double ref, double comp);
int MCStateProbDistribution(std::vector<unsigned int>& counts, std::vector<unsigned int>& bdcounts, SteadyState_SAAPD& data, int iter);
int RecordMCTransfer(bool newFile, ELevel* src, double ChangeLocal, unsigned long iter, double orig);
//int OutputChargeTransfer(ChargeTransfer& chgT, bool clear);
int RecordNonSteadyState(Simulation* sim, bool clear);

int RecordGenRecomb(bool newFile, double ECb, double EVb, double gen, double rec, bool isGeneration);
int DebugCarrierTransfer(double Old, double New, int point, bool band);
int DebugTimestep(Point* pt, bool clear);
int ProgressInt(int check, long long int num);
int ProgressString(std::string str);
int ProgressDouble(int check, double num);
int ProgressCheck(int check, bool clear);
int CheckPoissonEQTransfer(Model& mdl);
int DebugCLink(bool newfile, double value, bool newSrc, bool newIteration);
int CompareRho(Model& mdl, std::string fn);
int WriteDeviceBand(Model& mdl, std::string fn);
int PlotFermiEquil(std::string fn, double delCExpConstant, double expConstant, double negBeta,
	double expCoeff, double numHSource, double numETarget, double numESource, double numHTarget,
	double min, double max);
int WriteBand(std::ofstream& file, Point* pt);
int WriteImpurityDist(Model& mdl, std::string fn);
int WritePtEStates(Point* pt, std::string fn);
int WriteEnergyStates(Model& mdl, std::string fn);
int WriteFermiInfo(double Ef, double sum, double deriv, bool clear);
int CheckMesh(Model& mdl, std::string filename);
int OutputRatesPoint(Point& pt, Simulation* sim);
int DebugSSZeroEffect(bool clear, bool newline, long int var);
int DebugSteadyState(ELevel* eStateLimitRate, ELevel* eStateLimitOcc, double rate, double delocc, bool clear);
int DebugSteadyStatePlot(unsigned int iteration, double rates[], double deloccs[], SteadyState_SAAPD& data);
int PlotDRate_C(Model& mdl, ModelDescribe& mdesc, ELevel* eUse);
int RecTimestepDistribution(Simulation* sim, bool clear);

#endif