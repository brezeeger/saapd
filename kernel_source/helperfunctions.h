#ifndef HELPERFNS_H
#define HELPERFNS_H

#include <cmath>
#include <complex>
#include <limits>
#include <vector>
#include <map>
#include <set>

#define BADRET false
#define GOODRET true
#define DENORMALPOS std::numeric_limits<double>::denorm_min()	//this is the ABSOLUTE smallest number
#define DENORMALNEG -std::numeric_limits<double>::denorm_min()
#define ABSMINPOS std::numeric_limits<double>::min()	//transition where dividing by this number results in infinity
#define ABSMINNEG -std::numeric_limits<double>::min()
#define ABSMAXPOS std::numeric_limits<double>::max()

#define MY_IS_FINITE(a) (a-a==0)
#define MY_IS_DEN(a) (a > ABSMINPOS || a < ABSMINNEG)
#define DOUBLEACCURACY 1.0e-13
#define ALL_SET_BITS_ARE_ZERO(test,key) ((((test) ^ (key)) & (key)) == (key))
#define ALL_SET_BITS_ARE_ONE(test,key) (((test) & (key)) == (key))
#define ANY_SET_BITS_ARE_ONE(test,key) (((test) & (key)) || ((key)==0))
#define ANY_SET_BITS_ARE_ZERO(test,key) ((((test)^(key)) & (key)) || ((key)==0))
#define RESET_SET_BITS_NOASSIGN(source,key) ((~(key)) & (source))
#define IS_OPPOSITE_SIGN(a,b) (((a)>0 && (b)<0) || ((a)<0 && (b)>0))

//forward declarations
class XSpace;
class Point;
class ELevel;

double ProbFraction(double num);
double Round(double num);
void SetDoubleNearestInt(double& num);
double FermiFn(double Ef, double E, double kTi, bool inverse, bool AbsZero, double degen);
XSpace DetermineDirection(Point* PrevPt, Point* CurPt);
int DistancePtsMainCoord(Point* src, Point* trg, double& ret);
int CalcPascalTriangle(std::vector<double>& ret, unsigned int power, bool inverse);
double DoubleSubtract(double A, double B);
bool IsSignificant(double src, double adjust, double accuracy = DOUBLEACCURACY);
bool DoubleEqual(double A, double B, double accuracy=DOUBLEACCURACY);
bool XSpaceEqual(XSpace A, XSpace B);
double DoubleAdd(double A, double B);
bool SolveQuadratic(double A, double B, double C, double& retAdd, double& retSubtract);
double CubeRoot(double num);
int SetabsEUseFromFermi(ELevel* eLev, double absFermi);
bool TaylorSecondOrder(double x1, double x2, double x3, double f1, double f2, double f3, double& ret1, double& ret2);
bool TaylorSecondOrder(double val, double f, double df, double d2f, double& ret1, double& ret2);
int CubeRootComplex(std::complex<double> input, std::complex<double>& out1, std::complex<double>& out2, std::complex<double>& out3);
std::complex<double> CubeRootComplex(std::complex<double> input);
double dot(XSpace a, XSpace b);
double AbsDistance(XSpace a, XSpace b);

template <typename S>
bool Swap(S& smaller, S& larger, bool force = false)
{
	if (smaller > larger || force)
	{
		S tmp = larger;
		larger = smaller;
		smaller = tmp;
		return(true);
	}
	return(false);
}
void ToLower(std::string& str);

double getTrapezoidalArea(double startPos, double endPos, double startTrapPos, double endTrapPos, double startTrapValue, double endTrapValue);

unsigned int getPrecision(double val, bool scientific=true);

template <typename KEY, typename T>
void AddMapValsToVector(std::map<KEY, T>& mp, std::vector<T>& vec)
{
	vec.reserve(vec.size() + mp.size());
	for(std::map<KEY,T>::iterator it = mp.begin(); it!= mp.end(); ++it)
		vec.push_back( it->second);
	return;
}

template <typename KEY, typename T>
void AddMapKeysToVector(std::map<KEY, T>& mp, std::vector<KEY>& vec)
{
	vec.reserve(vec.size() + mp.size());
	for(std::map<KEY,T>::iterator it = mp.begin(); it!= mp.end(); ++it)
		vec.push_back( it->first);
	return;
}

template <typename KEY, typename T>
typename std::map<KEY, T>::iterator MapAdd(std::map<KEY, T>& mp, KEY key, T val)
{
	std::pair<std::map<KEY, T>::iterator, bool> ret = mp.insert(std::pair<KEY, T>(key, val));
	if (ret.second == false)	//it did not insert
		ret.first->second += val;

	return(ret.first);
}

template <typename KEY, typename T>
typename std::map<KEY, T>::iterator MapInsertReplace(std::map<KEY, T>& mp, KEY key, T val)
{
	std::pair<std::map<KEY, T>::iterator, bool> ret = mp.insert(std::pair<KEY, T>(key, val));
	if (ret.second == false)	//it did not insert
		ret.first->second = val;

	return(ret.first);
}

template <typename KEY, typename T>
typename T MapSum(std::map<KEY, T>& mp)
{
	T sum = 0;
	for (std::map<KEY, T>::iterator it = mp.begin(); it != mp.end(); ++it)
		sum = sum + it->second;

	return(sum);
}

template <typename KEY, typename T>
typename std::map<KEY, T>::iterator MapFindFirstGreater(std::map<KEY, T>& mp, KEY key)
{
	return(mp.upper_bound(key));	//returns iterator one past the present key value - which is the first >
}

template <typename KEY, typename T>
typename std::multimap<KEY, T>::iterator MapFindFirstLessEqual(std::multimap<KEY, T>& mp, KEY key)
{
	std::multimap<KEY, T>::iterator it;

	it = mp.lower_bound(key);	//finds the lowest value ABOVE the key
	//the actual key does not exist, so the next lowest key ought to be correct
	if (it != mp.end())	//there is a value that is the lowest
	{
		if (it->first == key)
			return(it);
		if (it != mp.begin())	//if it's not the first value, there is another value
			--it;	//so go one before.
		else //it is the first value, so just say that it was not found
			it = mp.end();
	}
	else
	{
		//there are no values that are above the specified value
		it = mp.end();	//so default to none
		if (mp.size() > 0)	//but if there is a value, the largest one must be correct
			--it;
	}
	return(it);
}

template <typename KEY, typename T>
typename std::map<KEY, T>::iterator MapFindFirstLessEqual(std::map<KEY, T>& mp, KEY key)
{
	std::map<KEY, T>::iterator it;

	it = mp.lower_bound(key);	//finds the lowest value ABOVE the key
	//the actual key does not exist, so the next lowest key ought to be correct
	if (it != mp.end())	//there is a value that is the lowest
	{
		if (it->first == key)
			return(it);
		if (it != mp.begin())	//if it's not the first value, there is another value
			--it;	//so go one before.
		else //it is the first value, so just say that it was not found
			it = mp.end();
	}
	else
	{
		//there are no values that are above the specified value
		it = mp.end();	//so default to none
		if (mp.size() > 0)	//but if there is a value, the largest one must be correct
			--it;
	}
	return(it);
}

template <typename KEY, typename T>
typename std::map<KEY, T>::iterator MapFindFirstLess(std::map<KEY, T>& mp, KEY key)
{
	std::map<KEY, T>::iterator it = mp.lower_bound(key);	//find the first iterator >= key (upper_bound finds first iterator > key)
	if (it == mp.begin())	//the first entry is greater than the key, so there are none less
		return(mp.end());	//if there is no entry, it will be end, which is the same as begin

	//at this point, there MUSt be an entry less than the key
	//it is either it--

	if (it == mp.end())	//there are no results greater than the key, but the last element should be
	{  //assuming that the last element exists. Otherwise, just back away from end
		if (mp.size() == 0)
			return(mp.end());
	}


	it--;
	return(it);
}


template <typename T>
typename std::multiset<T>::iterator MSetFindFirstGreaterEqual(std::multiset<T>& mset, T key)
{
	return(mset.lower_bound(key));
	/*
	multiset<T>::iterator it = mset.find(key);
	if(it != mset.end())
	return(it);	//it found it, which satisfies first >=

	//it hasn't found it
	it = mset.upper_bound(key);	//this returns the first value OVER the specified value
	return(it);	//will return multiset::end() if no values are above it
	*/
}

template <typename T>
typename std::multiset<T>::iterator MSetFindFirstLessEqual(std::multiset<T>& mset, T key)
{
	std::multiset<T>::iterator it = mset.lower_bound(key);	//finds the lowest value ABOVE the key
	//the actual key does not exist, so the next lowest key ought to be correct
	if (it != mset.end())	//there is a value that is the lowest
	{
		if (it->first == key)
			return(it);
		if (it != mset.begin())	//if it's not the first value, there is another value
			--it;	//so go one before.
		else //it is the first value, so just say that it was not found
			it = mset.end();
	}
	else
	{
		//there are no values that are above the specified value
		it = mset.end();	//so default to none
		if (mset.size() > 0)	//but if there is a value, the largest one must be correct
			--it;
	}
	return(it);
}

template <typename vtype>
unsigned long int FindValInVector(std::vector<vtype>& v, vtype val, unsigned long int hint=-1)
{
	if (hint < v.size())
	{
		if (v[hint] == val)
			return(hint);

		bool badlow = false;
		bool badhigh = false;
		for (unsigned int dif = 1; !badhigh || !badlow; ++dif)	//start at the hint and branch out from there!
		{
			if (!badhigh)
			{
				if (hint + dif < v.size())
				{
					if (v[hint + dif] == val)
						return(hint + dif);
				}
				else
					badhigh = true;
			}
			if (!badlow)
			{
				if (unsigned int(hint - dif) < v.size())
				{
					if (v[hint - dif] == val)
						return(hint - dif);
				}
				else
					badlow = true;
			}
		}
		return(-1);
	}


	unsigned long int find;
	for (find = 0; find < v.size(); ++find)
	{
		if (v[find] == val)
			return(find);
	}
	return(-1);
}

template <typename T>
bool VectorUniqueAdd(std::vector<T>& vect, T v)
{
	for (unsigned int i = 0; i < vect.size(); ++i)
	{
		if (vect[i] == v)
			return(false);
	}
	vect.push_back(v);
	return(true);
}


template<typename T>
bool VectorDoubleShrinkingBubbleSort(std::vector<T>& srt)
{
	//go back and forth on the sort
	unsigned int sz = srt.size();
	if (sz == 0)
		return(false);	//that was easy...
	T tmpVal;

	bool changeMade;

	unsigned int loopCtr = 0;
	do
	{
		changeMade = false;
		//this loop guarantees putting the next largest element to the back
		for (unsigned int i = loopCtr; i < sz - 1 - loopCtr; ++i)
		{
			if (srt[i + 1] < srt[i])
			{
				tmpVal = srt[i];
				srt[i] = srt[i + 1];
				srt[i + 1] = tmpVal;
				changeMade = true;
			}
		}
		if (!changeMade)
			break;
		//this loop guarantees putting the next smallest element to the front
		for (unsigned int i = sz - 2 - loopCtr; i > loopCtr && i<sz; --i)
		{
			if (srt[i] < srt[i - 1])
			{
				tmpVal = srt[i];
				srt[i] = srt[i - 1];
				srt[i - 1] = tmpVal;
				changeMade = true;
			}
		}
		loopCtr++;
	} while (changeMade);
	return(loopCtr != 0);	//returns true if something was modified
}

template<typename S, typename T>
bool VectorDoubleShrinkingBubbleSort(std::vector<std::pair<S,T>>& srt)
{
	//go back and forth on the sort
	unsigned int sz = srt.size();
	if (sz == 0)
		return(false);	//that was easy...
	std::pair<S,T> tmpVal;

	bool changeMade;

	unsigned int loopCtr = 0;
	do
	{
		changeMade = false;
		//this loop guarantees putting the next largest element to the back
		for (unsigned int i = loopCtr; i < sz - 1 - loopCtr; ++i)
		{
			if (srt[i + 1].first < srt[i].first)
			{
				tmpVal = srt[i];
				srt[i] = srt[i + 1];
				srt[i + 1] = tmpVal;
				changeMade = true;
			}
		}
		if (!changeMade)
			break;
		//this loop guarantees putting the next smallest element to the front
		for (unsigned int i = sz - 2 - loopCtr; i > loopCtr && i<sz; --i)
		{
			if (srt[i].first < srt[i - 1].first)
			{
				tmpVal = srt[i];
				srt[i] = srt[i - 1];
				srt[i - 1] = tmpVal;
				changeMade = true;
			}
		}
		loopCtr++;
	} while (changeMade);
	return(loopCtr != 0);	//returns true if something was modified
}



//requires the '<' to be defined for the type
template <typename T>
int RemoveVectorDuplicates(std::vector<T>& dat) {
	//first sort it so anything that is the same will be adjacent to one another
	VectorDoubleShrinkingBubbleSort(dat);
	int removed = 0;
	for (unsigned int i = 1; i < dat.size(); ++i) {
		if (dat[i] == dat[i - 1]) {
			dat.erase(dat.begin() + i);	//erase the current index
			--i;	//that screwed with the indices. make sure stay in the same spot
			removed++;
		}
	}
	return removed;
}

template <typename T>
int RemoveVectorValue(std::vector<T>& v, T dat) {
	//first sort it so anything that is the same will be adjacent to one another
	int removed = 0;
	for (unsigned int i = 0; i < v.size(); ++i) {
		if (v[i] == dat) {
			v.erase(v.begin() + i);	//erase the current index
			--i;	//that screwed with the indices. make sure stay in the same spot
			removed++;
		}
	}
	return removed;
}

template <typename S, typename T>
void VectorSmartSort(std::vector<std::pair<int, double>>& data) {
	if (data.size() < 2)	//no sorting necessary
		return;

	bool sortedMinToMax = true;
	bool sortedMaxToMin = true;
	//just do a quick run through for data analysis

	for (unsigned int i = 1; i < data.size() && (sortedMinToMax || sortedMaxToMin); ++i) {
		if (data[i - 1].first > data[i].first)
			sortedMinToMax = false;
		else if (data[i - 1].first < data[i].first)
			sortedMaxToMin = false;
	}
	//for many items, they will be inserted in order. Question is forward or reverse.
	if (sortedMinToMax)
		return;	//no sorting necessary
	if (sortedMaxToMin) { //sorted in opposite order. Do minimal swaps
		int halfSize = data.size() / 2;
		for (unsigned int i = 0; i < halfSize; ++i) {
			std::pair<int, double> swap = data[i];
			data[i] = data[data.size() - 1 - i];
			data[data.size() - 1 - i] = swap;
		}
		return;
	}

	//it was not in order, so just implement a quick sort on the keyed value
	VectorQuickSort(&data[0], data.size())
}

template<typename S, typename T>
void VectorQuickSort(std::pair<S,T> vals[], unsigned int sz) {
	if (sz <= 1)
		return;
	if (sz == 2) {
		if (vals[1].first < vals[0].first) {
			std::pair<S, T> tmp = vals[0];
			vals[0] = vals[1];
			vals[1] = tmp;
		}
	}
	unsigned int frontCtr = 0;
	unsigned int backCtr = sz - 1;
	unsigned int testMid = sz / 2;

	//going to put the testVal in the back!
	std::pair<S, T> tmpVal;
	//first check the front one for being in the middle
	if ((vals[frontCtr].first >= vals[testMid].first && vals[frontCtr].first <= vals[backCtr].first) ||
		(vals[frontCtr].first <= vals[testMid].first && vals[frontCtr].first >= vals[backCtr].first)) {
		tmpVal = vals[backCtr];
		vals[backCtr] = vals[frontCtr];
		vals[frontCtr] = tmpVal;
	}
	else if ((vals[testMid].first >= vals[frontCtr].first && vals[testMid].first <= vals[backCtr].first) ||
		(vals[testMid].first <= vals[frontCtr].first && vals[testMid].first >= vals[backCtr].first)) {
		tmpVal = vals[backCtr];
		vals[backCtr] = vals[testMid];
		vals[testMid] = tmpVal;
	}

	for (unsigned int i = 0; i < backCtr; ++i) {	//don't check the back swap index
		if (vals[i].first < vals[backCtr].first) { //guarantee it's in the front
			if (i != frontCtr) { //don't swap with self...
				tmpVal = vals[i];
				vals[i] = vals[frontCtr];
				vals[frontCtr] = tmpVal;
			}
			frontCtr++;
		}
	}

	//now just do a quick swap of the back, because frontCtr is set as something >= testVal
	tmpVal = vals[backCtr];
	vals[backCtr] = vals[frontCtr];
	vals[frontCtr] = tmpVal;

	//pass in the same array, but the size of said array goes to the front counter index (the pivot)
	VectorQuickSort(vals, frontCtr);
	//start one beyond the front counter index, and go to the end of the array size.
	//so if frontCtr=5, and sz=8, we want to pass in vals[6], 2 (6,7)
	if (frontCtr + 2 < sz)	//at least 2 elements. STart is +1, and next is +2.
		VectorQuickSort(&vals[frontCtr + 1], sz - frontCtr - 1);

}

template<typename S, typename T>	//sorts by first value via merge. Strict ordering
void VectorFastMergeSort(std::vector<std::pair<S, T>>& vals) {
	if (vals.size() < 2)
		return;

	unsigned int firstMergeStart, secondMergeStart, secondMergeEnd;
	
	std::vector<std::pair<S, T>> tmp;	//buffer to work with
	std::vector<unsigned int> SortStarts;
	
	SortStarts.push_back(0);
	for (unsigned int search = 1; search < vals.size(); ++search) {
		if (vals[search - 1] <= vals[search])	//this range is currently in order
			continue;
		SortStarts.push_back(search);
	}
	SortStarts.push_back(vals.size());

	if (SortStarts.size() == 2)
		return;	//already sorted

	tmp.resize(vals.size());
	
	do {
		for (unsigned int whichSort = 0; whichSort < SortStarts.size()-2; whichSort += 2) {
			firstMergeStart = SortStarts[whichSort];
			secondMergeStart = SortStarts[whichSort+1];
			secondMergeEnd = SortStarts[whichSort+2];	//which will be the next firstMergeStart if applicable
			unsigned int index1 = firstMergeStart;
			unsigned int index2 = secondMergeStart;
			for (unsigned int index = firstMergeStart; index < secondMergeEnd; ++index) {
				if (vals[index1].first <= vals[index2].first && index1 < secondMergeStart) {
					tmp[index] = vals[index1];
					index1++;
				}
				else {
					tmp[index] = vals[index2];
					index2++;
				}
			}
		}
		if (SortStarts.size() % 2 == 0) {	//if even, there was a last group that needs to be copied over.
			for (unsigned int index = SortStarts[SortStarts.size() - 2]; index < tmp.size(); ++index)
				tmp[index] = vals[index];
		}
		std::vector<unsigned int> tmpSorts;
		tmpSorts.reserve(SortStarts.size()/2+1);
		for (unsigned wSort = 0; wSort < SortStarts.size()-1; wSort += 2)
			tmpSorts.push_back(SortStarts[wSort]);
		tmpSorts.push_back(SortStarts.back());

		SortStarts.swap(tmpSorts);
		vals.swap(tmp);	//make vals hold the right values. Also gets rid of extra reservation in vector
		tmpSorts.clear();	//shouldn't matter, but be safe. compiler would probably get rid of it anyway, or this is taking
		//care of garbage collection in a bit.
		
	} while (SortStarts.size() > 2);
	
	return;
}

template<typename S, typename T>
unsigned int SortedVectorFindFirstLessEqual(const std::vector<std::pair<S, T>>& vec, const S key) {
	if (vec.size() == 0)
		return -1;
	//edge cases
	if (vec[0].first > key)
		return -1;	//there is none less than or equal!

	unsigned int max = vec.size() - 1;
	if (vec[max].first <= key)
		return max;
	unsigned int min = 0;
	unsigned int test;

	do {
		test = (max + min) >> 1;
		((vec[test].first <= key) ? min : max) = test;	//min=test if the spot is less than key
	} while (max - min > 1); //and max=test if the spot is > key
	return min;
}

template<typename S, typename T>
unsigned int SortedVectorFindClosest(const std::vector<std::pair<S, T>>& vec, const S key) {
	if (vec.size() == 0)
		return -1;
	//edge cases
	if (vec.size() == 1)
		return 0;

	unsigned int max = vec.size() - 1;
	unsigned int min = 0;
	unsigned int test;
	S deltaMax = abs(vec[max].first - key);
	S deltaMin = abs(vec[0].first - key);
	S deltaTest;

	do {
		test = (max + min) >> 1;
		deltaTest = abs(vec[test].first - key);
		if (deltaMin > deltaMax) {	//the minimum was further away
			min = test;
			deltaMin = deltaTest;
		}
		else {
			max = test;
			deltaMax = deltaTest;
		}
	} while (max - min > 1); //and max=test if the spot is > key
	
	return (deltaMin < deltaMax ? min : max);
}

template<typename S, typename T>
unsigned int SortedVectorFindFirstLess(const std::vector<std::pair<S, T>>& vec, const S key) {
	if (vec.size() == 0)
		return -1;
	//edge cases
	if (vec[0].first >= key)
		return -1;	//there is none less than

	unsigned int max = vec.size() - 1;
	if (vec[max].first < key)
		return max;
	unsigned int min = 0;
	unsigned int test;

	do {
		test = (max + min) >> 1;
		((vec[test].first < key) ? min : max) = test;	//min=test if the spot is less than key
	} while (max - min > 1); //and max=test if the spot is > key
	return min;
}

template<typename S, typename T>
unsigned int SortedVectorFind(const std::vector<std::pair<S, T>>& vec, const S key) {
	if (vec.size() == 0)
		return -1;
	//edge cases
	if (vec[0].first == key)
		return 0;	//there is none less than

	unsigned int max = vec.size() - 1;
	if (vec[max].first == key)
		return max;
	unsigned int min = 0;
	unsigned int test;

	do {
		test = (max + min) >> 1;
		if (vec[test] == key)
			return test;
		((vec[test].first < key) ? min : max) = test;	//min=test if the spot is less than key
	} while (max - min > 1); //both max and min have always been tested, so if adjacent, not found
	return -1;
}

//returns size of vector if nothing found was greater
template <typename KEY, typename T>
unsigned int SortedVectorFindFirstGreater(const std::vector<std::pair<KEY, T>>& vec, const KEY key) {

	//edge cases
	if (vec.size() == 0)
		return 0;

	if (vec[0].first > key)
		return 0;	//the first is greater

	unsigned int max = vec.size() - 1;
	if (vec[max].first <= key)
		return vec.size();	//none are greater. insert would be before this, which when added
	//to begin, would be vector.end()

	unsigned int min = 0;
	unsigned int test;

	do {
		test = (max + min) >> 1;
		((vec[test].first <= key) ? min : max) = test;	//min=test if the spot is less than key
	} while (max - min > 1); //and max=test if the spot is > key
	return max;

}
//pass by reference in case the data is large and complicated. avoid copy
template<typename S, typename T>
void SortedVectorAdd(std::vector<std::pair<S, T>>& vec, const std::pair<S, T>& val) {
	unsigned int index = SortedVectorFindFirstLessEqual(vec, val.first);
	//inserts occur before the selected index. An insert at begin goes to element 0.
	//an insert at end tags along.
	if (index >= vec.size()) { //it didn't find anything less
		//means it needs to get added to the front of the vector. Ew.
		vec.insert(vec.begin(), val);
		return;
	}
	//next check if it found the value, or if it found the previous value.
	if (vec[index].first == val.first) { //found it
		vec[index].second += val.second;
		return;
	}
	//it found the previous value, but we need to insert before the next element
	vec.insert(vec.begin() + index + 1, val);	//if last element, this will be equiv to vec.end()
	return;
}

template<typename S, typename T>
bool SortedVectorInsertReplace(std::vector<std::pair<S, T>>& vec, const std::pair<S, T>& val, bool ignoreIfExists=false) {
	unsigned int index = SortedVectorFindFirstLessEqual(vec, val.first);
	//inserts occur before the selected index. An insert at begin goes to element 0.
	//an insert at end tags along.
	if (index >= vec.size()) { //it didn't find anything less
		//means it needs to get added to the front of the vector. Ew.
		vec.insert(vec.begin(), val);
		return true;
	}
	//next check if it found the value, or if it found the previous value.
	if (vec[index].first == val.first) { //found it
		if (!ignoreIfExists) {
			vec[index].second = val.second;
			return true;
		}
		return false;
	}
	//it found the previous value, but we need to insert before the next element
	vec.insert(vec.begin() + index + 1, val);	//if last element, this will be equiv to vec.end()
	return true;
}

template<typename S, typename T>
void SortedVectorRemoveEarlyDuplicateKeys(std::vector<std::pair<S, T>>& vec) {
	unsigned int ctr = 0;	//if this happens a lot, only rewrite the thing once.
	//most cases, this will not do anything
	for (unsigned int i = 0; i < vec.size()-1; ++i) {
		if (vec[i].first == vec[i + 1].first)
			ctr++;
	}
	if (ctr == 0)
		return;
	std::vector<std::pair<S,T>> v;
	v.reserve(vec.size() - ctr);
	for (unsigned int i = 0; i < vec.size(); ++i) {
		if (i == vec.size() - 1) {
			v.push_back(vec[i]);
		}
		else if (vec[i].first != vec[i + 1].first)
			v.push_back(vec[i]);
	}

	vec.swap(v);
}

template<typename S, typename T>
void SortedVectorInsert(std::vector<std::pair<S, T>>& vec, const std::pair<S, T>& val) {
	unsigned int index = SortedVectorFindGreater(vec, val.first);
	vec.insert(vec.begin() + index, val);	//if last element, this will be equiv to vec.end()
	return;
}

template<typename S, typename T>
void VectorsSortMergeIntoFirst(std::vector<std::pair<S, T>>& vec1, std::vector<std::pair<S, T>>& vec2) {
	vec1.insert(vec1.end(), vec2.begin(), vec2.end());	//if last element, this will be equiv to vec.end()
	VectorFastMergeSort(vec1);	//technically, these don't have to be sorted initially. It would just run faster
	return;
}

//this assumes two values will not be provided with the same sort value/key. that just wouldn't work... optimized towards absorption calculations
template<typename S, typename T>
T InterpolateValuesPositive(const std::pair<S, T>& v1, const std::pair<S, T>& v2, const std::pair<S, T>& v3, S location) {
	//check easy ones first
	if (location == v1.first)
		return v1.second;
	else if (location == v2.first)
		return v2.second;
	else if (location == v3.first)
		return v3.second;
	
	if (Swap(v1.first, v2.first))
		Swap(v1.second, v2.second, true);
	if (Swap(v1.first, v3.first))
		Swap(v1.second, v3.second, true);
	//now smallest is definitely in v1
	if (Swap(v2.first, v3.first))
		Swap(v2.second, v3.second, true);
	//and now it's in order.
	
	
	//now let's taylor series this shit - but only if the value is actually between v1 and v3
	if (location < v3.first && location > v1.first) {
		T d1a = (v2.second - v1.second) / (v2.first - v1.first);
		T d1b = (v3.second - v2.second) / (v3.first - v2.first);
		T d2 = (d1b - d1a);	//most accurate representation I can think of for getting 2nd deriv if integers
		d2 += d2;
		d2 /= (v3.first - v1.first);
		//if dx is to be the same, multiply denominator by 1/2. multiply expression by 2.
		//just add again as not sure what the template types will be (need 2.0f or 2.0, or integers, etc.)

		S delta = location - v1.first;
		return v1.second + delta * d1a + d2 * delta * delta / 2;
	}
	//if it is outside, this is most likely used in optical data. Likely exponentially decaying to zero and out
	//of bounds. As it turns out, exponential may be the right function, but calculating it proper is likely to generate lots of error
	//and waste many computational cycles.
	//This is why a Taylor series would be really awful extrapolation in this regard.

	//it should only call these if it's out of the bounds of all the data.
	if (location < v1.first && v1.second == 0.0)
		return 0.0;
	if (location > v3.first && v3.second == 0.0)
		return 0.0;

	//the best approximation is probably just linear...
	if (location < v1.first)
	{
		T d1a = (v2.second - v1.second) / (v2.first - v1.first);
		S delta = location - v1.first;
		return v1.second + delta * d1a;
	}
	else if (location > v3.first) {
		T d1b = (v3.second - v2.second) / (v3.first - v2.first);
		S delta = location - v2.first;
		return v2.second + delta * d1b;
	}
	return 0.0;	//should never happen
}

template<typename S, typename T>
T InterpolateValuesPositive(const std::pair<S, T>& v1, const std::pair<S, T>& v2, S location) {
	//check easy ones first
	if (location == v1.first)
		return v1.second;
	else if (location == v2.first)
		return v2.second;

	if (Swap(v1.first, v2.first))
		Swap(v1.second, v2.second, true);

	T d1a = (v2.second - v1.second) / (v2.first - v1.first);
	S delta = location - v1.first;
	return v1.second + delta * d1a;
}

template<typename S, typename T>
T InterpolateValuesPositive(const std::pair<S, T>& v1, S location) {
	return v1.second;
}



/*
initially 0, 10, 25, 40, 100
Then 0, 25, 100...0, 25

*/
#endif