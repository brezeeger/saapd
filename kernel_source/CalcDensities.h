#ifndef CALCDENSITIES
#define CALCDENSITIES

class Point;

int CalculateRho(Point& pt);
double CalculateN(Point* pt);
double CalculateP(Point* pt);



#endif