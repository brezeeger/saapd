#include "PoissonPrep.h"

#include <vector>
#include "EquationSolver.h"
#include "model.h"
#include "physics.h"
#include "contacts.h"

//need to get all the fixed values for the points... aka the boundary conditions

/*
This is going to be extremely tricky...
A voltage needs to be found for each point directly across in one of the x,y,z coords
The voltage used will be determined by a linear relationship between two known voltages.
These voltages will be determined based on one of the two points and that ones perpendicularly adjacent point
such that the centers are on either end of the main point of interest (perpendicularly to the direction of interest)

The system of equations is solved relatively normally.
If a contact is defined, add it to the solved tree and still use the equation.  If the equation is not
used, then it can serve as a discontinuity position, which is bad. Additionally, two contact points
won't define everything as they should, as the equation will define a neighboring point.
On these edges, it will influence all points in other directions.  It does not need the neuman boundary condition.
If it is on an edge not defined by a contact, it does some trickery...

The following only applies to 2D and 3D.

It uses the Neumann boundary condition, and it sets the boundary slope equal to the slope of the incoming flux
in that direction, slightly modified by the charge density.  The proportion of charge density removed
is chosen by the ratio of surface areas in all degrees of freedom (being that it flows through X,
it's the same as adding both X components as the contribution is equal in both).  This essentially sets the second
derivative in a particular direction to be some proportion of the charge density.  It cancels out
all dependencies within the equation.  Therefore, this method only works provided there is at least one
dependence left.  Therefore, this method does not work for 1D.  In 2D and 3D, it works for all points that are not
a corner, in which the equation reduces to 0=0.
Therefore, to succeed for all points, it only needs one direction to be periodic.  This will also
allow the edges to be undefined at a particular voltage, but then an embedded contact may be used.

In the event that a corner is come across which is not a contact, this is REALLY ugly and hopefully
never happens by an intelligent modeler.  In this case, split up the charge density via each direction again.
The flux can be considered the electric field at each point.  The change in flux will have half the change apply
to fluxes leaving the device by the physical symmetry of the rectangular prism "point."  Therefore, take that half,
and subtract it from the charge density.  This will leave one term in each direction, consisting of a known and unknown,
for the corner equation. However, the unknown is the same for each term, giving one equation with one unknown and many knowns.
Of course, the knowns must be determined elsewhere, but they may be determined by a contact voltage propagating through
the device.  This can also apply to a 1D edge if needed.

Next difficulty...
If the contact is entirely contained within by other contacts, they essentially fix the voltage throughout the rest of the
device, and it will result in competing solutions.  Therefore, don't treat it as if it were a contact for solving poisson's
equation. Even though metals have a "constant" voltage across it, that's not actually true.  Electrons just rapidly respond
to electric fields and cancel out the voltage and the electric field within. Otherwise, the slope of the electric field
at the edge could not be adjusted, and the ending voltage would be essentially fixed.

In order to be added to the solved portion, the contact must be next to a non-contact point (so exclude if all points
are either external or part of the contact).  Afterwards, set the contacts to the constant voltage.

New solution...
Always add the contacts to solved.  Only add it to the list of equations if there are at least two adjacent good points.
This will allow embedded contacts to have an influence, but not fix any conflicting solutions if they are multiple points.
If they are multiple points, then it will essentially create multiple sets of poisson's equation to solve.  YES!

So use New Solution and some of the other stuff mentioned above.

... This all simplifies down to the same simple(!) process that doesn't turn into the giant tangle of if statements
the above stuff would require.  Seems to be more by luck than anything...
1) Find the total surface area of point
2) Determine how much surface area is exposed in the direction of the exterior.
3) Take that ratio out of the total charge for the change in flux in the right hand side
4) Zero out the coefficients apply to that term



New version...
If it comes across a point to the external world, it will add one "fake" point outside of the device which all the flux
can enter into.


Newest version... the contact update.
There can be any number of contacts.
It is automatically determined what contacts should be used for solving the system of equations
If there is not enough contacts, contacts are created to form a starting point and get the necessary data
If there are too many contacts, then their information is suppressed.

If adjacent points are contacts, the system needs to be broken.
Actually, it makes sense that if every time an additional contact has behaviors specified beyond the original two,
it has an EField out to act as an outlet, with an optional current.

*/

int PoissonPrep(Model& mdl, SystemEq* System, ModelDescribe& mdesc)
{
	std::vector<Contact>::iterator itCon;
	EqVar eqvarpair;
	mdl.poissonBC.clear();	//make sure it starts out fresh.
	System->clear();	//make sure the system starts out empty!	


	XSpace widthp;	//width on the positive side
	XSpace widthn;	//width on the negative side
	long int pt1p, pt2p, pt1n, pt2n;
	double Vpt1p, Vpt2p, Vpt1n, Vpt2n, delta, Vpt, Vptn, Vptp;	//how much of the voltage is present
	
	//need to figure out the points in order to know the width of the second derivative
	
	bool mx;	//these flags indicate whether a point
	bool px;	//in this direction exists
	bool my;
	bool py;


	double areai;	//inverse of the total surface "area" of the point
	unsigned long int LHSAdditional = mdl.numpoints;	//used to extend the equations out so the final point can have a poisson
	bool incrementLHSAdd;
	unsigned long int RHSAdditional = mdl.numpoints;	//used to extend the equations on the right side (typically boundary conditions)
	unsigned long int eq = 0;	//just a counter to go through all the equations
	unsigned long int ptID=0;
	//equation as well. This will simply increment each time.

	//will allow mdl.numpoints t be the variable that keeps all the constants aded together. It's answer should always =1.
	for(std::map<long int, Point>::iterator itPt = mdl.gridp.begin(); itPt != mdl.gridp.end(); ++itPt)
	{
		Point* curPt = &(itPt->second);
		ptID = curPt->adj.self;
		incrementLHSAdd = false;
		//initialize flags to determine where it is out of bounds
		//only need to do one because the model is always defined within a rectangular prism
		if(curPt->adj.adjMX || curPt->adj.adjMX2)	//boundary in -x direction...
			mx = true;
		else
			mx = false;

		if(curPt->adj.adjPX || curPt->adj.adjPX2)	//boundary in +x direction
			px = true;
		else
			px = false;
/////////////////////////////////////////////////////////////////
		if(curPt->adj.adjMY || curPt->adj.adjMY2)
			my = true;
		else
			my = false;
		if(curPt->adj.adjPY || curPt->adj.adjPY2)
			py = true;
		else
			py = false;
/////////////////////////////////////////////////////////////////

		areai = 0.0;	//inverse of the area of the point. Used in boundary conditions if needed

		if(mdesc.NDim == 2)	//line area for each side
		{
			areai += (curPt->pxpy.x - curPt->mxmy.x);
			areai += (curPt->pxpy.y - curPt->mxmy.y);
			areai = 0.5 / areai;	// 1.0 / (area * 2)
	
		}
		if(mdesc.NDim == 3)	//surface area for each point side
		{
			areai += (curPt->pxpy.x - curPt->mxmy.x) * (curPt->pxpy.y - curPt->mxmy.y);
			areai += (curPt->pxpy.z - curPt->mxmy.z) * (curPt->pxpy.y - curPt->mxmy.y);
			areai += (curPt->pxpy.x - curPt->mxmy.x) * (curPt->pxpy.z - curPt->mxmy.z);
			areai = 0.5 / areai;

		}

		

		//sets this point as a "boundary" condition whether its on the boundary or not, as it is a contact,
		//or a known term.
		if(curPt->contactData != NULL)	//this is a contact point
		{	//this simply says that the RHS (rho/BC) will equal the full variable. Like 1.0 Psi_j = 1.0 Rho_j, except here Rho is a boundary condition
			BoundaryCondition tmpBC;
			tmpBC.pt = curPt;
			tmpBC.value = 0.0;	//just initialize it - it doesn't really matter
/*			if(eq >= System->Equation.size())	//ensure that there will be an equation
				System->Equation.resize(eq + 1);

			System->Equation[eq].RHS.Insert(1.0, ptID+mdl.numpoints);	//0 to mdl.numpoints-1 is coeffs for rho's
			//mdl.numpoints -> 2*mdl.numpoints-1 is for additional solutions based on boundary conditions
			System->Equation[eq].LHS.Insert(1.0, ptID);	//it needs an LHS side (simple one...)
			CatalogEquation(System->Equation[eq].LHS.start, eq, System->EquationsWithVariable);
			eq++;	
*/			
			unsigned int extPt = 0;	//this will be the boundary condition point
			for(unsigned int i=0; i<curPt->contactData->point.size(); ++i)
			{
				if(curPt->contactData->point[i] == curPt)
					break;
				//			if(pt->contactData->point[i]==NULL)
				//				continue;	this is a one to one match now
				extPt++;	//this will match the index of the point of interest that sets where the current goes
			}
			//most likely, the above code will just hit the first if statement, break, and be done.

			//add as many equations as appropriate
			int contactActivity = curPt->contactData->GetCurrentActive();
			bool suppressVoltage = curPt->contactData->SuppressVoltage();
			bool suppressCurrent = curPt->contactData->SuppressCurrent();
			bool suppressEField = curPt->contactData->SuppressEField();
			bool suppressEMin = curPt->contactData->SuppressMinEField();
			bool suppressEOpp = curPt->contactData->SuppressDFieldOpp();
			bool suppressNeutralJ = curPt->contactData->SuppressChargeNeutralCurrent();

			bool steadystate = ALL_SET_BITS_ARE_ONE(contactActivity, CONTACT_SS);	//is it steady state?
			bool cSink = ALL_SET_BITS_ARE_ONE(contactActivity, CONTACT_SINK);

			//do the voltages
			if (!suppressVoltage && (contactActivity & CONTACT_MASK_POISSON_VOLT) )
			{
				//this is simply a Psi = value equation for the primary point
				System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS); //Add BC value
				System->AddTermEquation(eq, ptID, 1.0, EQ_SOLVER_LHS);
				//now log in the boundary condition
				tmpBC.ID = RHSAdditional;
				tmpBC.type = BC_VOLT;
				mdl.poissonBC.insert(std::pair<unsigned long int,BoundaryCondition>(tmpBC.ID,tmpBC));

				eq++; //move on to the next equation in the system. This is just a quick inserted equation for boundary conditions
				RHSAdditional++;	//make sure this value isn't used again
			}


			if ((!suppressEField && (contactActivity & CONTACT_MASK_EFIELD_NONED))
				||
				(!suppressCurrent && (contactActivity & CONTACT_MASK_CURRENTS_NONED))
				||
				(!suppressEMin && (contactActivity & CONTACT_MINIMIZE_E_ENERGY))
				||
				(!suppressEOpp && (contactActivity & CONTACT_OPP_DFIELD))
				||
				(!suppressNeutralJ && (contactActivity & CONTACT_CONSERVE_CURRENT))
			  )
			{
				//this one is slightly more difficult. Order is chosen to match contacts.cpp
				Point* ptExt = NULL;
				if (contactActivity & CONTACT_USE_PLANEREC)	//if it's a sink, go with the point that it interacts with.
				{	//the sink is the end point - always
					ptExt = curPt->contactData->PlaneRecPoints[extPt];
				}
				else// if (contactActivity & CONTACT_CONNECT_EXTERNAL)
				{  //but if it connects to the outside world, we want to use endPtCalcs (which should be PlaneRecPoints[extPt])
					ptExt = &curPt->contactData->endPtCalcs[extPt];
				}
				/*else //it is not a sink. It does not connect to the outside world
				{ //if there was a current, it would connect to the external world. This means it is an E-Field
					ptExt = &curPt->contactData->endPtCalcs[extPt];	//go 'external'
				}
				*/
				double delta = ptExt->Psi - curPt->Psi;
				double dist;
				int dir = DistancePtsMainCoord(curPt, ptExt, dist);	//dist>0 equiv to adjPX==NULL: ptExt.pos - curPt.pos
				double disti = 1.0/dist;
				unsigned long otherIndex;
				if(ptExt->adj.self == -1)
				{
					if(dir==PX)
						otherIndex = LHSAdditional + POISSON_PX;
					else if(dir==NX)
						otherIndex = LHSAdditional + POISSON_MX;
					else if(dir==PY)
						otherIndex = LHSAdditional + POISSON_PY;
					else if(dir==NY)
						otherIndex = LHSAdditional + POISSON_MY;
					else if(dir==PZ)
						otherIndex = LHSAdditional + POISSON_PZ;
					else if(dir==NZ)
						otherIndex = LHSAdditional + POISSON_MZ;
					else
						otherIndex = LHSAdditional + POISSON_EXT;
					
					incrementLHSAdd = true;
				}
				else
					otherIndex = ptExt->adj.self;

				//(Psi_R - Psi_L)/|dist| = E_Field (given)
				//if dist>0, Psi_L = ctcPt (ptID). Matches. Psi_L multiplied by neg, and Psi_R is not.
				//if dist<0, Psi_R = ctcPt (ptID). Matches. Psi_R multiplied by double neg, and Psi_L just one.
				if (!suppressEOpp && (contactActivity & CONTACT_OPP_DFIELD)) //better to do this one as storing displacement field than elect
				{
					disti = disti * curPt->eps;
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, otherIndex, disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
				}
				else
				{
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, otherIndex, disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
				}
				tmpBC.ID = RHSAdditional;

				if (!suppressEField && (contactActivity & CONTACT_MASK_EFIELD_NONED))
					tmpBC.type = BC_SIMPLE_EFIELD;
				else if (!suppressCurrent && (contactActivity & CONTACT_MASK_CURRENTS_NONED))
					tmpBC.type = BC_SIMPLE_CURRENT;
				else if (!suppressEMin && (contactActivity & CONTACT_MINIMIZE_E_ENERGY))
					tmpBC.type = BC_MIN_EFIELD_ENERGY;
				else if (!suppressEOpp && (contactActivity & CONTACT_OPP_DFIELD))
					tmpBC.type = BC_OPP_EFIELD;
				else if (!suppressNeutralJ && (contactActivity & CONTACT_CONSERVE_CURRENT))
					tmpBC.type = BC_CURRENT_NEUTRAL;


				eq++;
				RHSAdditional++;
				
				mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));
/*
				if(!mx) //there is not a point adjacent to the left
				{
					//we are introducing the function (Psi_pt - Psi_L)/dist = E_Field (given)
					//a positive electric field points uphill in Psi, and despite all the V's, poisson eq is in terms of psi
					double disti = 2.0*(curPt->position.x - curPt->mxmy.x);	//essentially mirror the point across the boundary
					disti = 1.0 / disti;
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, LHSAdditional + POISSON_MX, -disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, disti, EQ_SOLVER_LHS);
					incrementLHSAdd = true;
					tmpBC.ID = RHSAdditional;
					tmpBC.type = 0;
					if(!suppressEField &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_EFIELD)) ||
						ALL_SET_BITS_ARE_ONE(contactActivity, (CONTACT_SS | CONTACT_EFIELD_LINEAR))))
						tmpBC.type += CONTACT_MASK_POISSON_EFIELD;
					if(!suppressCurrent &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_E_CURRENT)) ||
						(ALL_SET_BITS_ARE_ONE(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_CURRENTS_NONED))))
						tmpBC.type += CONTACT_MASK_POISSON_E_CURRENT;
					
					mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));

					eq++;
					RHSAdditional++;
				}
				else if(!px)
				{
					//we are introducing the function (Psi_R - Psi_pt)/dist = E_Field (given)
					//a positive electric field points uphill in Psi, and despite all the V's, poisson eq is in terms of psi
					double disti = 2.0*(curPt->pxpy.x - curPt->position.x);	//essentially mirror the point across the boundary
					disti = 1.0 / disti;
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, LHSAdditional + POISSON_PX, disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
					incrementLHSAdd = true;

					tmpBC.ID = RHSAdditional;
					tmpBC.type = CONTACT_INACTIVE;		//0
					if (!suppressEField &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_EFIELD)) ||
						ALL_SET_BITS_ARE_ONE(contactActivity, (CONTACT_SS | CONTACT_EFIELD_LINEAR))))
						tmpBC.type += CONTACT_MASK_POISSON_EFIELD;
					if (!suppressCurrent &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_E_CURRENT)) ||
						(ALL_SET_BITS_ARE_ONE(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_CURRENTS_NONED))))
						tmpBC.type += CONTACT_MASK_POISSON_E_CURRENT;
					
					mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));

					eq++;
					RHSAdditional++;
				}
				else
				{
					//now we're doing (Psi_R - Psi_pt)/dist = EField
					double disti = curPt->adj.adjPX->position.x - curPt->position.x;	//essentially mirror the point across the boundary
					disti = 1.0 / disti;
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, curPt->adj.adjPX->adj.self, disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
					tmpBC.ID = RHSAdditional;
					tmpBC.type = CONTACT_INACTIVE;
					if (!suppressEField &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_EFIELD)) ||
						ALL_SET_BITS_ARE_ONE(contactActivity, (CONTACT_SS | CONTACT_EFIELD_LINEAR))))
						tmpBC.type += CONTACT_MASK_POISSON_EFIELD;
					if (!suppressCurrent &&
						((ALL_SET_BITS_ARE_ZERO(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_POISSON_E_CURRENT)) ||
						(ALL_SET_BITS_ARE_ONE(contactActivity, CONTACT_SS) && (contactActivity & CONTACT_MASK_CURRENTS_NONED))))
						tmpBC.type += CONTACT_MASK_POISSON_E_CURRENT;
					
					mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));

					eq++;
					RHSAdditional++;
				}
				*/
			}	//end e-field or current in dimension of device

			if (contactActivity & CONTACT_MASK_POISSON_EFIELD_ED)
			{
				//this is an external dimension and must fit into the poisson equation setup
				//therefore this is fairly complicated to add on, and it MUST be a part of the following equation

				double disti;
				int addN = 0;
				int addP = 0;
				//a positive electric field/current means positive charge enters the contact
				if(mdesc.NDim == 1)	//the point is taken to be in y - arbitrarily from above
				{
					disti = 1.0/curPt->width.y;
					addN = POISSON_MY;
					addP = POISSON_PY;
				}
				else //if 2d or 3d, just use the z values
				{
					disti = 1.0/curPt->width.z;
					addN = POISSON_MZ;
					addP = POISSON_PZ;
				}
				System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
				System->AddTermEquation(eq, ptID, disti, EQ_SOLVER_LHS);
				System->AddTermEquation(eq, LHSAdditional+addP, -disti, EQ_SOLVER_LHS);
				incrementLHSAdd = true;
				tmpBC.ID = RHSAdditional;
				tmpBC.type = BC_EFIELD_ED;
				mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));
				RHSAdditional++;
				eq++;
				
				if (contactActivity & CONTACT_FLOAT_EFIELD_ESCAPE)
				{
					//if try to do an exact proportion, that proportion isn't allowed to change! (which is no good!)
					System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, LHSAdditional + addN, disti, EQ_SOLVER_LHS);
					tmpBC.ID = RHSAdditional;
					tmpBC.type = CONTACT_LEAK;
					mdl.poissonBC.insert(std::pair<unsigned long int, BoundaryCondition>(tmpBC.ID, tmpBC));
					RHSAdditional++;
					eq++;
				}
				//the above specifies the additional electric field values for the boundary conditions

				//NOW add the terms in for the poisson equation

				//this inserts an additional term into the current poisson equation that makes 
				//the added electric field (if one was indeed added - another variable may have been set)
				//integrate OK with the current equations

				
				disti = disti*disti;	//only worrying about LHS parts... second derivative
				//because this is "escaping" that means the device isn't being modeled outside of this
				//this means a lot of the "complicated" aspects used below will not matter.
				
				
				System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
				System->AddTermEquation(eq, LHSAdditional+addP, disti, EQ_SOLVER_LHS);
				//if this isn't just a leakage term, but tied with an electric field entering, the second
				//part of poisson's equation must be added because you can't assume that the Psi's
				//were equal and thus going to cancel one another out. This may still be the case.
				if (contactActivity & CONTACT_FLOAT_EFIELD_ESCAPE)	//includes float and  no float for SS
				{
					System->AddTermEquation(eq, LHSAdditional+addN, disti, EQ_SOLVER_LHS);
					System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
				}
				incrementLHSAdd = true;
				//note, NO CATALOG EQUATION ON PURPOSE
				//additionally, there is no boundary conditions to actually insert - they already have been
				//this is just adding degrees of freedom for a contact that pushes it beyond the limit

			}
		}	//end contact treatment

		//if it's made it this far, it is deserving of an equation.
		//do special code if it is next to as many boundaries as there are dimenions (the equivalent of a corner)
		//do more special code if it is along a boundary for a particular dimension.
		
		double epsi = 1.0 / curPt->eps / EPSNOT;	//otherwise, tsar the original weighting in of the RHS as the
		//epsilon, or have it automatically absorbed into the coefficients preceeding the multiplication by rho
		
		System->AddTermEquation(eq,ptID, epsi, EQ_SOLVER_RHS);
//		Phi = curPt->MatProps->Phi;


		if(curPt->adj.adjPX)
			pt1p = curPt->adj.adjPX->adj.self;
		else
			pt1p = EXT;

		if(curPt->adj.adjPX2)
			pt2p = curPt->adj.adjPX2->adj.self;
		else
			pt2p = EXT;

		if(px)	//this direction is legitimate
		{
			Vptp = 1.0;

			if(curPt->adj.adjPX && curPt->adj.adjPX2)
			{
				//things get ugly. Take the two voltages, and get a weighted average based on the position
				//between the two points.  Use that position as a particular voltage, and put the weighted voltages on top
				Line* lin = new Line(curPt->adj.adjPX->position, curPt->adj.adjPX2->position);
				XSpace intercept = lin->PtIn(curPt->position.y, PY);	//find the point on the line
				delete lin;
				//with value y = main point's y.
				delta = AbsDistance(curPt->adj.adjPX->position, curPt->adj.adjPX2->position);	//total distance
				Vpt1p = AbsDistance(intercept, curPt->adj.adjPX2->position) / delta;	//V+ = V1(1-d1/d) + d1/d V2
				Vpt2p = AbsDistance(intercept, curPt->adj.adjPX->position) / delta;	//d2/d = 1-d1/d because d=d1+d2
				//note, pt1p != pt2p ever except in special cirumstances, but in that case
				//j.pos.y = adj.pos.y, and this code doesn't execute.
//				Phi1p = curPt->adj.adjPX->MatProps->Phi;
//				Phi2p = curPt->adj.adjPX2->MatProps->Phi;

				widthp.x = intercept.x - curPt->position.x;	//they have the same y, so just subtract
			}
			else if(curPt->adj.adjPX)	//simple case, only one adjacent
			{	//this can only happen if Py == Py2, and they have the same X dimensions...
				widthp.x = curPt->pxpy.x - curPt->position.x;	//split this up in case j == pt1p
				if(curPt->MatProps != curPt->adj.adjPX->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjPX->eps;
					widthp.x += epsfactor * (curPt->adj.adjPX->position.x - curPt->adj.adjPX->mxmy.x);
				}
				else
					widthp.x += curPt->adj.adjPX->position.x - curPt->adj.adjPX->mxmy.x;
				
				Vpt1p = 1.0;
				Vpt2p = 0.0;

			//it is guaranteed to have a point on the left and the right...
			//note, this always occurs for 1D models...
			}
			else //now the other one is the only correct one
			{	//this can only happen if Py == Py2, and they have the same X dimensions...
				widthp.x = curPt->pxpy.x - curPt->position.x;	//split this up in case j == pt1p
				if(curPt->MatProps != curPt->adj.adjPX2->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjPX2->eps;
					widthp.x += epsfactor * (curPt->adj.adjPX2->position.x - curPt->adj.adjPX2->mxmy.x);
				}
				else
					widthp.x += curPt->adj.adjPX2->position.x - curPt->adj.adjPX2->mxmy.x;
				
				Vpt1p = 0.0;
				Vpt2p = 1.0;

			//it is guaranteed to have a point on the left and the right...
			//note, this always occurs for 1D models...
			}
			

		}
		else	//bad point in positive x direction. Implement Neumann boundary condition that the electric field
		{		//in X is adjusted proportionally by the surface area of the surface of the point in that direction
			//new method is to give it a "fake" point to calculate the value for - this variable only exists to give
			//freedom to the other variables

			widthp.x = 2.0 * (curPt->pxpy.x - curPt->position.x);	//split this up in case j == pt1p
			Vpt1p = 1.0;
			Vpt2p = 0.0;
			Vptp = 1.0;
			pt1p = LHSAdditional+POISSON_PX;	//say the variable is a dummy variable
			incrementLHSAdd = true;
			

		}
		
			//now do the mx coordinates:
		
		if(curPt->adj.adjMX)
			pt1n = curPt->adj.adjMX->adj.self;
		else
			pt1n = EXT;

		if(curPt->adj.adjMX2)
			pt2n = curPt->adj.adjMX2->adj.self;
		else
			pt2n = EXT;

		if(mx)	//make sure its not external
		{	
			Vptn = 1.0;

			if(curPt->adj.adjMX && curPt->adj.adjMX2)
			{
				Line* lin = new Line(curPt->adj.adjMX->position, curPt->adj.adjMX2->position);
				XSpace intercept = lin->PtIn(curPt->position.y, PY);	//find the point on the line
				delete lin;
				//with value y = main point's y.
				delta = AbsDistance(curPt->adj.adjMX->position, curPt->adj.adjMX2->position);	//total distance
				Vpt1n = AbsDistance(intercept, curPt->adj.adjMX2->position) / delta;
				Vpt2n = AbsDistance(intercept, curPt->adj.adjMX->position) / delta;				
				widthn.x = curPt->position.x - intercept.x;	//they have the same y, so just subtract
			}
			else if(curPt->adj.adjMX)
			{
				widthn.x = curPt->position.x - curPt->mxmy.x;	//split up in case j = pt1n
				if(curPt->MatProps != curPt->adj.adjMX->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjMX->eps;
					widthn.x += epsfactor * (curPt->adj.adjMX->pxpy.x - curPt->adj.adjMX->position.x);
				}
				else
					widthn.x += curPt->adj.adjMX->pxpy.x - curPt->adj.adjMX->position.x;
				Vpt1n = 1.0;
				Vpt2n = 0.0;
				//it is guaranteed to have a point on the left and the right...
				//note, this always occurs for 1D models
			}
			else
			{
				widthn.x = curPt->position.x - curPt->mxmy.x;	//split up in case j = pt1n
				if(curPt->MatProps != curPt->adj.adjMX2->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjMX2->eps;
					widthn.x += epsfactor * (curPt->adj.adjMX2->pxpy.x - curPt->adj.adjMX2->position.x);
				}
				else
					widthn.x += curPt->adj.adjMX2->pxpy.x - curPt->adj.adjMX2->position.x;
				Vpt1n = 0.0;
				Vpt2n = 1.0;
				//it is guaranteed to have a point on the left and the right...
				//note, this always occurs for 1D models
			}
			
		}
		else	//bad point in negative x direction.
		{
			widthn.x = 2.0 * (curPt->position.x - curPt->mxmy.x);	//make sure the proper distance is set
			Vptn = 1.0;	//say the coefficient on this variable is 1.
			Vpt1n = 1.0;	//there are no coefficients over here because they get cancelled out
			Vpt2n = 0.0;	//by rho
			pt1n = LHSAdditional+POISSON_MX;	//this is being assigned to a dummy variable...
			incrementLHSAdd = true;
		}

		delta = curPt->pxpy.x - curPt->mxmy.x;	//the space on either side of the boundary is the same, so the average width is the point's width!
		Vpt1p = Vpt1p/widthp.x/delta;	//turn the ratio of how much voltage for the points is added
		Vpt2p = Vpt2p/widthp.x/delta;	//into the coefficient for the equation
		Vpt1n = Vpt1n/widthn.x/delta;
		Vpt2n = Vpt2n/widthn.x/delta;
		//good news is that all the chi's introduced have the SAME exact coefficients as the voltages, except are multipled
		//by chi instead.  We know chi, so just add them together.  This will cause them to cancel out if they are all the same.
		//otherwise, there will be a difference.
		if(Vpt1p != 0.0)
			System->AddTermEquation(eq,pt1p,Vpt1p,EQ_SOLVER_LHS);
		if(Vpt2p != 0.0)
			System->AddTermEquation(eq,pt2p,Vpt2p,EQ_SOLVER_LHS);
		if(Vpt1n != 0.0)
			System->AddTermEquation(eq,pt1n,Vpt1n,EQ_SOLVER_LHS);
		if(Vpt2n != 0.0)
			System->AddTermEquation(eq,pt2n,Vpt2n,EQ_SOLVER_LHS);

		//and the main part:
		Vpt = -Vptp/widthp.x/delta - Vptn/widthn.x/delta;
		if(Vpt != 0.0)
			System->AddTermEquation(eq,ptID,Vpt,EQ_SOLVER_LHS);
		if(mdesc.NDim == 1)
		{
			if(incrementLHSAdd)
				LHSAdditional += POISSON_INCREMENT_EXTRAS;
			eq++;
			continue;	//move on to the next equation
		}


		if(curPt->adj.adjPY)
			pt1p = curPt->adj.adjPY->adj.self;
		else
			pt1p = EXT;

		if(curPt->adj.adjPY2)
			pt2p = curPt->adj.adjPY2->adj.self;
		else
			pt2p = EXT;

		//now start prepping the y system...
		if(py)
		{
			Vptp = 1.0;

			if(curPt->adj.adjPY && curPt->adj.adjPY2)
			{
				Line* lin = new Line(curPt->adj.adjPY->position, curPt->adj.adjPY2->position);
				XSpace intercept = lin->PtIn(curPt->position.x, PX);	//find the point on the line
				delete lin;
				//with value x = main point's x.
				delta = AbsDistance(curPt->adj.adjPY->position, curPt->adj.adjPY2->position);	//total distance
				Vpt1p = AbsDistance(intercept, curPt->adj.adjPY2->position) / delta;	//V+ = V1(1-d1/d) + d1/d V2
				Vpt2p = AbsDistance(intercept, curPt->adj.adjPY->position) / delta;	//d2/d = 1-d1/d because d=d1+d2
				widthp.y = intercept.y - curPt->position.y;	//they have the same x, so just subtract
			}
			else if(curPt->adj.adjPY)	//simplest case, they line up!
			{	//this can only happen if Py == Py2, and they have the same X dimensions...
				widthp.y = curPt->pxpy.y - curPt->position.y;
				if(curPt->MatProps != curPt->adj.adjPY->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjPY->eps;
					widthp.y += epsfactor * (curPt->adj.adjPY->position.y - curPt->adj.adjPY->mxmy.y);
				}
				else
					widthp.y += curPt->adj.adjPY->position.y - curPt->adj.adjPY->mxmy.y;

				Vpt1p = 1.0;
				Vpt2p = 0.0;
			}
			else
			{
				widthp.y = curPt->pxpy.y - curPt->position.y;
				if(curPt->MatProps != curPt->adj.adjPY2->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjPY2->eps;
					widthp.y += epsfactor * (curPt->adj.adjPY2->position.y - curPt->adj.adjPY2->mxmy.y);
				}
				else
					widthp.y += curPt->adj.adjPY2->position.y - curPt->adj.adjPY2->mxmy.y;
				Vpt1p = 0.0;
				Vpt2p = 1.0;
			}

		}
		else	//it's a positve y boundary
		{
			//only introduce one variable for outgoing flux! If it's already been done in X, no need to have two
			//variables to represent a fixed flux out. Will just cause the solution to mostly solve with a linear answer
			//between the two.
			widthp.y = 2.0 * (curPt->pxpy.y - curPt->position.y);	//make sure the proper distance is set
			if(mx && px)	//if the X's were valid, then create a dummy variable for outgoing flux
			{
				Vptp = 1.0;	//say the coefficient on this variable is 1.
				Vpt1p = 1.0;	//there are no coefficients over here because they get cancelled out
				Vpt2p = 0.0;	//by rho
				pt1p = LHSAdditional + POISSON_PY;	//this is being assigned to a dummy variable...
				incrementLHSAdd = true;		//increment the dummy variable
			}
			else	//there was an X that wasn't valid.  This point is essentially merged with the dummy X variable for outgoing flux
			{	
				Vpt1p = 0.0;	//there are no coefficients over here because they get cancelled out
				Vpt2p = 0.0;	//by rho
				Vptp = 0.0;		//this points coefficient has no contribution
			}
			
		}




		if(my)
		{
			Vptn = 1.0;

			if(curPt->adj.adjMX && curPt->adj.adjMX2)
			{
				Line* lin = new Line(curPt->adj.adjMY->position, curPt->adj.adjMY2->position);
				XSpace intercept = lin->PtIn(curPt->position.x, PX);	//find the point on the line
				delete lin;
				//with value y = main point's y.
				delta = AbsDistance(curPt->adj.adjMY->position, curPt->adj.adjMY2->position);	//total distance
				Vpt1n = AbsDistance(intercept, curPt->adj.adjMY2->position) / delta;
				Vpt2n = AbsDistance(intercept, curPt->adj.adjMY->position) / delta;
				widthn.y = curPt->position.y - intercept.y;	//they have the same y, so just subtract

			}
			else if(curPt->adj.adjMY)	//simplest case, they line up!
			{	//this can only happen if Px == Px2, and they have the same Y dimensions...
				widthn.y = curPt->position.y - curPt->mxmy.y;
				if(curPt->MatProps != curPt->adj.adjMY->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjMY->eps;
					widthn.y += epsfactor * (curPt->adj.adjMY->pxpy.y - curPt->adj.adjMY->position.y);
				}
				else
					widthn.y += curPt->adj.adjMY->pxpy.y - curPt->adj.adjMY->position.y;

				Vpt1n = 1.0;
				Vpt2n = 0.0;
			}
			else
			{	//this can only happen if Px == Px2, and they have the same Y dimensions...
				widthn.y = curPt->position.y - curPt->mxmy.y;
				if(curPt->MatProps != curPt->adj.adjMY2->MatProps)
				{
					double epsfactor = curPt->eps / curPt->adj.adjMY2->eps;
					widthn.y += epsfactor * (curPt->adj.adjMY2->pxpy.y - curPt->adj.adjMY2->position.y);
				}
				else
					widthn.y += curPt->adj.adjMY2->pxpy.y - curPt->adj.adjMY2->position.y;

				Vpt1n = 0.0;
				Vpt2n = 1.0;
			}
			
		}
		else	//it's a negative y boundary
		{
			widthn.y = 2.0 * (curPt->position.y - curPt->mxmy.y);	//make sure the proper distance is set
			if(mx && px)	//if the X's were valid, then create a dummy variable for outgoing flux
			{
				Vptn = 1.0;	//say the coefficient on this variable is 1.
				Vpt1n = 1.0;	//there are no coefficients over here because they get cancelled out
				Vpt2n = 0.0;	//by rho
				pt1n = LHSAdditional+POISSON_MY;	//this is being assigned to a dummy variable...
				incrementLHSAdd = true;		//increment the dummy variable
			}
			else	//there was an Y that wasn't valid.  This point is essentially merged with the dummy X variable for outgoing flux
			{	
				Vpt1n = 0.0;	//there are no coefficients over here because they get cancelled out
				Vpt2n = 0.0;	//by rho
				Vptn = 0.0;		//this points coefficient has no contribution
			}
		}

		delta = (widthn.y + widthp.y)*0.5;	//delta is now the average width
		Vpt1p = Vpt1p/widthp.y/delta;	//turn the ratio of how much voltage for the points is added
		Vpt2p = Vpt2p/widthp.y/delta;	//into the coefficient for the equation
		Vpt1n = Vpt1n/widthn.y/delta;
		Vpt2n = Vpt2n/widthn.y/delta;

		if(Vpt1p != 0.0)
			System->AddTermEquation(eq,pt1p,Vpt1p,EQ_SOLVER_LHS);
		if(Vpt2p != 0.0)
			System->AddTermEquation(eq,pt2p,Vpt2p,EQ_SOLVER_LHS);
		if(Vpt1n != 0.0)
			System->AddTermEquation(eq,pt1n,Vpt1n,EQ_SOLVER_LHS);
		if(Vpt2n != 0.0)
			System->AddTermEquation(eq,pt2n,Vpt2n,EQ_SOLVER_LHS);

		//and the main part:
		Vpt = -Vptp/widthp.y/delta - Vptn/widthn.y/delta;
		if(Vpt != 0.0)
			System->AddTermEquation(eq,ptID,Vpt,EQ_SOLVER_LHS);


		if(mdesc.NDim == 2)
		{
			if(incrementLHSAdd)
				LHSAdditional += POISSON_INCREMENT_EXTRAS;
			eq++;
			continue;
		}

		//theoretically, if there was a third dimension, more code would follow for the z coordinate

		if(mdesc.NDim == 3)	//just a prep for 3D world...  :/
		{
			if(incrementLHSAdd)
				LHSAdditional += POISSON_INCREMENT_EXTRAS;
			eq++;
			continue;
		}


	}

	//make a better order setting routine.
	

	return(0);
}

int ModelEdge(Point& pt, int ndim)
{
	int numedge = 0;
	//check for x edge
	if(pt.adj.adjPX == NULL && pt.adj.adjPX2 == NULL)
		numedge++;
	if(pt.adj.adjMX == NULL && pt.adj.adjMX2 == NULL)
		numedge++;

	if(ndim>1)
	{
		if(pt.adj.adjPY == NULL && pt.adj.adjPY2 == NULL)
			numedge++;
		if(pt.adj.adjMY == NULL && pt.adj.adjMY2 == NULL)
			numedge++;
	}

	return(numedge);
}
