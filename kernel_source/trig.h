#ifndef SAAPD_TRIG_H
#define SAAPD_TRIG_H

class XSpace;

double FindTheta(XSpace pos1, XSpace pos2);
double FindPhi(XSpace pos1, XSpace pos2);
double FindThetaVectortailtoTail(XSpace inVect, XSpace outvect, bool retCosTheta);
bool LineIntersectPlane(XSpace& ret, XSpace linept0, XSpace linept1, XSpace PlaneNormal, XSpace ptPlane1);

#endif