#include "contacts.h"

#include "physics.h"
#include "transient.h"
#include "outputdebug.h"
#include "DriftDiffusion.h"
#include "model.h"
#include "ss.h"
#include "light.h"
#include "output.h"

Contact* ELevel::GetContact()
{
	return(pt.contactData);
}

int CalcRateViaContact(ELevel* src, ELevel* trg, XSpace dim)
{
	return(CalcRateViaContact(&src->getPoint(), &trg->getPoint(), dim));
}

int CalcRateViaContact(Point* src, Point* trg, XSpace dim)
{
	int ctcActive = IsRateContactOutput(src, trg, dim);

	if(ctcActive == CONTACT_NONE)
		return(ctcActive);

	//only return the true possibilities (just need one). 
	if(ctcActive & CONTACT_SRC)
	{
		//this means, that the source state is indeed a contact
		int active = src->contactData->GetCurrentActive();

		//only calculate currents if it says there's a current, there is a sink, or it connects to the external world
		if(active & CONTACT_MASK_CURRENTS_NONED)
			ctcActive = ctcActive | CONTACT_CALCULATE;
		else if(active & CONTACT_CONNECT_EXTERNAL)
			ctcActive = ctcActive | CONTACT_CALCULATE;
		else if (active & CONTACT_SINK)	//if it's a sink, we want this to go towards the currents, even though the current may not be known
			ctcActive = ctcActive | CONTACT_CALCULATE;	//the else because why not? It's the same line on the if
	}

	if(ctcActive & CONTACT_TRG)
	{
		int active = trg->contactData->GetCurrentActive();
		if(active & CONTACT_MASK_CURRENTS_NONED)	//not SS
			ctcActive = ctcActive | CONTACT_CALCULATE;
		else if(active & CONTACT_CONNECT_EXTERNAL)
			ctcActive = ctcActive | CONTACT_CALCULATE;
		else if (active & CONTACT_SINK)
			ctcActive = ctcActive | CONTACT_CALCULATE;
	}

	//if t's gotten here, neither of the contacts had defined currents
	//so the current may be calculated normally
	return (ctcActive);
}
int IsRateContactOutput(ELevel* src, ELevel* trg, XSpace dim)
{
	if (src && trg)
		return(IsRateContactOutput(&src->getPoint(), &trg->getPoint(), dim));
	return(CONTACT_NONE);
}

int IsRateContactOutput(Point* srcPt, Point* trgPt, XSpace dim)
{
	if(srcPt==NULL || trgPt==NULL)
	{
//		ProgressInt(68, 0);	//this function gets called with invalid trgPt pointers purposefully, and auto returns contact_none
		return(CONTACT_NONE);
	}

	int activeSrc, activeTrg;

	if(srcPt->contactData)
	{
		activeSrc = srcPt->contactData->GetCurrentActive();
		
		if (ALL_SET_BITS_ARE_ZERO(activeSrc, (CONTACT_CONNECT_EXTERNAL | CONTACT_SINK)))	//it does not have any rate outputs
			activeSrc = CONTACT_INACTIVE;	//so flag this as inactive
	}
	else
		activeSrc = CONTACT_INACTIVE;

	if(trgPt->contactData)
	{
		activeTrg = trgPt->contactData->GetCurrentActive();
		if (ALL_SET_BITS_ARE_ZERO(activeTrg, (CONTACT_CONNECT_EXTERNAL | CONTACT_SINK)))	//it does not have any rate outputs
			activeTrg = CONTACT_INACTIVE;	//so flag this as inactive
	}
	else
		activeTrg = CONTACT_INACTIVE;

	if(activeSrc == CONTACT_INACTIVE && activeTrg == CONTACT_INACTIVE)	//note, contact inactive is 0
		return(CONTACT_NONE);

	XSpace direction = trgPt->position - srcPt->position;	//this gets flipped if the target is actually the contact
	direction.Normalize();	//just keep the numbers simpler
	double dir;
	double distPosSrc, distPosTrg, distNegSrc, distNegTrg;
	bool adjSrcPos, adjSrcNeg, adjTrgPos, adjTrgNeg;

	if(fabs(direction.x) >= fabs(direction.y) && fabs(direction.x) >= fabs(direction.z))
	{
		distPosSrc = dim.x - srcPt->pxpy.x;
		distNegSrc = srcPt->mxmy.x;
		distPosTrg = dim.x - trgPt->pxpy.x;
		distNegTrg = trgPt->mxmy.x;

		if(activeTrg && activeSrc == CONTACT_INACTIVE)	
			direction = -direction;	//want to reference from the contact point
		
		//if distPos > distNeg, that means it is closer to the left side
		//that means the outside should be viewed from the left side
		dir = direction.x;
		
		adjSrcNeg = srcPt->adj.adjMX==NULL ? false:true;
		adjSrcPos = srcPt->adj.adjPX==NULL ? false:true;
		adjTrgNeg = trgPt->adj.adjMX==NULL ? false:true;
		adjTrgPos = trgPt->adj.adjPX==NULL ? false:true;


	}
	else if(fabs(direction.y) >= fabs(direction.z))
	{
		distPosSrc = dim.y - srcPt->pxpy.y;
		distNegSrc = srcPt->mxmy.y;
		distPosTrg = dim.y - trgPt->pxpy.y;
		distNegTrg = trgPt->mxmy.y;
		if(activeTrg && activeSrc == CONTACT_INACTIVE)
			direction=-direction;
		
		dir = direction.y;

		adjSrcNeg = srcPt->adj.adjMY==NULL ? false:true;
		adjSrcPos = srcPt->adj.adjPY==NULL ? false:true;
		adjTrgNeg = trgPt->adj.adjMY==NULL ? false:true;
		adjTrgPos = trgPt->adj.adjPY==NULL ? false:true;
	}
	else
	{
		distPosSrc = dim.z - srcPt->pxpy.z;
		distNegSrc = srcPt->mxmy.z;
		distPosTrg = dim.z - trgPt->pxpy.z;
		distNegTrg = trgPt->mxmy.z;
		if(activeTrg && activeSrc == CONTACT_INACTIVE)
			direction=-direction;
		
		dir = direction.z;

		adjSrcNeg = srcPt->adj.adjMZ==NULL ? false:true;
		adjSrcPos = srcPt->adj.adjPZ==NULL ? false:true;
		adjTrgNeg = trgPt->adj.adjMZ==NULL ? false:true;
		adjTrgPos = trgPt->adj.adjPZ==NULL ? false:true;
	}

	if(activeSrc && activeTrg == CONTACT_INACTIVE)
	{
		if(activeSrc & CONTACT_SINK)	//src Contact Sink
		{
			if(distPosSrc > distNegSrc)
			{
				//the outside is the left side
				if(dir > 0.0) //the target is on the right side of the src contact
				{
					//but this is a contact sink.
					if(adjSrcNeg)	//there is another point to contact's left
						return(CONTACT_NONE); //that point would be the outside, therefore this is irrelevant
					//there is no other point to the contact's left. It is the outside point.
					//Because it is a contact sink, there is no rate possible in the negative direction
					//therefore the positive direction is the most outside, and it must be true
					return(CONTACT_SRC);

				}
				else if(dir < 0.0)
				{  //the target is on the left side of the src contact
					//the outside is to the left.
					//there is a non-contact point to the left
					//what's to the right doesn't matter.
					//this is considered the contact rate no matter what
					return(CONTACT_SRC);
				}
				else //there is no direction, which means it is the same position. This really shouldn't occur, but we'll say false. (non DD rate?
					return(CONTACT_NONE);
			}
			else if(distNegSrc > distPosSrc)
			{
				//the outside is the right side
				if(dir > 0.0)
				{
					//the target is to the right of the contact. This must count as the contact
					return(CONTACT_SRC);
				}
				else if(dir < 0.0)
				{
					//the target is to the left, but the outside is right.
					//this is a contact sink, so it stays constant.
					if(adjSrcPos) //there is a point to the right (outside) of the sink, so that one would be the rate
						return(CONTACT_NONE);	//which means this one is not the contact rate
					return(CONTACT_SRC);
				}
				else
					return(CONTACT_NONE);
			}
			else
			{
				//the source is precisely in the center and it is a thermal sink
				//gotta just choose one. Arbitrarily say the plane on the contact's negative side
				//is the one that counts as the contact data
				if(dir<0.0) //the target is to the left
					return(CONTACT_SRC);
				else if(dir>0.0)
					return(CONTACT_NONE);
				return(CONTACT_NONE);	//this makes no sense (target/source same point?)
			}
		}
		else //it is NOT a contact sink (external connection)
		{
			//this means that it will have created points on the outside if it is an edge point
			//to send rates into. If the other points already exist, no different!
			if(distPosSrc > distNegSrc)
			{
				//outside is to the left
				if(dir < 0.0) //the target is on the left (outside)
					return(CONTACT_SRC);
				//otherwise, it is on the right(inside, false) or the same point? (false)
				return(CONTACT_NONE);
			}
			else if(distNegSrc > distPosSrc) //outside to the right
			{
				if(dir>0.0) //the target is to the right (outside)
					return(CONTACT_SRC);
				//otherwise, false
				return(CONTACT_NONE);
			}
			else
			{
				//in the very center. Stick with same convention that the left of the contact
				//is the contact rate data
				if(dir<0.0)
					return(CONTACT_SRC);
				return(CONTACT_NONE);
			}	//end equal spacing
		}	//end not contact sink
	}	//end src is contact
	else if(activeTrg && activeSrc == CONTACT_INACTIVE)
	{
		if(activeTrg & CONTACT_SINK)	//target contact sink
		{
			if(distPosTrg > distNegTrg)
			{
				//the outside is the left side
				if(dir > 0.0) //the non-contact is on the right side of the contact
				{
					//but this is a contact sink.
					if(adjTrgNeg)	//there is another point to contact's left
						return(CONTACT_NONE); //that point would be the outside, therefore this is irrelevant

					//there is no other point to the contact's left. It is the outside point.
					//Because it is a contact sink, there is no rate possible in the negative direction
					//therefore the positive direction is the most outside, and it must be true
					return(CONTACT_TRG);

				}
				else if(dir < 0.0)
				{  //the non-contact is on the left side of the contact
					//the outside is to the left.
					//there is a non-contact point to the left: true
					//what's to the right doesn't matter.
					//this is considered the contact rate no matter what
					return(CONTACT_TRG);
				}
				else //there is no direction, which means it is the same position. This really shouldn't occur, but we'll say false. (non DD rate?
					return(CONTACT_NONE);
			}
			else if(distNegTrg > distPosTrg)
			{
				//the outside is the right side
				if(dir > 0.0)
				{
					//the non-contact is to the right of the contact. This must count as the contact
					return(CONTACT_TRG);
				}
				else if(dir < 0.0)
				{
					//the non-contact is to the left, but the outside is right.
					//this is a contact sink, so it stays constant.
					if(adjTrgPos) //there is a point to the right (outside) of the sink, so that one would be the rate
						return(CONTACT_NONE);	//which means this one is not the contact rate
					//there is no other point, so it is an edge point, making this one the proper rate
					return(CONTACT_TRG);
				}
				else //same point?
					return(CONTACT_NONE);
			}
			else
			{
				//the contact is precisely in the center and it is a thermal sink
				//gotta just choose one. Arbitrarily say the plane on the contact's negative side
				//is the one that counts as the contact data
				if(dir<0.0) //the non-contact is to the left
					return(CONTACT_TRG);
				//else if(dir>0.0)
				//	return(CONTACT_NONE);
				return(CONTACT_NONE);	//this makes no sense (target/source same point?)
			}
		}
		else //it is NOT a contact sink. 
		{
			//this means that it will have created points on the outside if it is an edge point
			//to send rates into. If the other points already exist, no different!
			if(distPosTrg > distNegTrg)
			{
				//outside is to the left
				if(dir < 0.0) //the non contact is on the left (outside)
					return(CONTACT_TRG);
				//otherwise, contact is on the right(inside, false) or the same point? (false)
				return(CONTACT_NONE);
			}
			else if(distNegTrg > distPosTrg) //outside to the right
			{
				if(dir>0.0) //the non-contact is to the right (outside)
					return(CONTACT_TRG);
				//otherwise, false
				return(CONTACT_NONE);
			}
			else
			{
				//in the very center. Stick with same convention that the left of the contact
				//is the contact rate data
				if(dir<0.0)
					return(CONTACT_TRG);
				return(CONTACT_NONE);
			}	//end equal spacing
		}	//end not contact sink
	}	//end target is contact and source is not
	else
	{
		//the source and target are both contacts.
		//this is a very screwy simulation. Two active contacts next to one another
		//first do the easy test
		if(srcPt->contactData == trgPt->contactData)
			return (CONTACT_NONE);	//this rate doesn't count.  Both points are part of the same contact

		if(distPosTrg > distNegTrg && distPosSrc > distNegSrc)
		{
			//they are both to the left of the center
			if(distPosTrg > distPosSrc) //the target is more to the left, so the source definitely gets it
			{
				//but what if the target is a sink and has nothing to the left?
				if(activeTrg & CONTACT_SINK)	//steady state contact sink
				{
					if(adjTrgNeg==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_SRC);
			}
			else //the source is more to the left, so the target gets the honors
			{
				//the source does as well if it's a sink and on the left edge
				if(activeSrc & CONTACT_SINK)	//steady state contact sink
				{
					if(adjSrcNeg==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_TRG);
			}
		}
		else if(distPosTrg < distNegTrg && distPosSrc < distNegSrc)
		{
			//they are both to the right of center, so the outside is the right
			if(distNegTrg > distNegSrc) //the target is more right (outside), so the source definitely gets it
			{
				//but what if the target is a sink and has nothing to the right?
				if(activeTrg & CONTACT_SINK)	//contact sink
				{
					if(adjTrgPos==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_SRC);
			}
			else //the source is more to the right, so the target gets the honors
			{
				//the source does as well if it's a sink and on the right edge
				if(activeSrc & CONTACT_SINK)	//contact sink
				{
					if(adjSrcPos==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_TRG);
			}
		}
		else if(distPosTrg == distNegTrg)
		{
			//the target is the center, and we by default prefer the negative side for the center contact
			if(distNegSrc < distNegTrg)
			{
				//the other contact is to the left
				//make sure the other contact is not an edge sink
				if(activeSrc & CONTACT_SINK)	//contact sink
				{
					if(adjSrcNeg==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_TRG);
			}
			else //the other contact is to the right
			{
				//that means this rate does not apply to the target. It may only apply if the target is
				//an edge sink.  If the target is in the center, that means there must be a point on the
				//other side if a contact exists on the other end.
				//it can't possibly be the target.
				//It can only be the source if that is an edge source
				if(activeSrc & CONTACT_SINK)	//contact sink
				{
					if(adjSrcNeg==false)	//it's an edge sink
						return(CONTACT_SRC);
				}
				return(CONTACT_NONE);
			}
		}
		else if(distPosSrc == distNegSrc)
		{
			//the source is the center, and we by default prefer the negative side for the center contact
			if(distNegTrg < distNegSrc)
			{
				//the other contact is to the left
				//make sure the other contact is not an edge sink
				if(activeTrg & CONTACT_SINK)	//contact sink
				{
					if(adjTrgNeg==false)	//it's an edge sink
						return(CONTACT_BOTH);
				}
				return(CONTACT_SRC);
			}
			else //the other contact(trg) is to the right
			{
				//thesource cannot possibly be an edge being centered and a point existing to the right
				//so must check the target to make sure it's not an edge contact
				
				if(activeTrg & CONTACT_SINK)	//contact sink
				{
					if(adjTrgPos==false)	//it's an edge sink
						return(CONTACT_TRG);
				}
				return(CONTACT_NONE);
			}
		}
		else
		{
			//the contacts must split the center. It cannot physically be the outside.
			int ret = CONTACT_NONE;
			//the only way the middle can be valid is if they both happen to be edge sinks
			if(activeTrg & CONTACT_SINK)	//contact sink
			{
					//they're adjacent, so one of the adj's will be true by default
				if(!adjTrgNeg || !adjTrgPos)	//it's an edge sink
					ret = ret | CONTACT_TRG;
			}
			if(activeSrc & CONTACT_SINK)	//contact sink
			{
				if(!adjSrcNeg || !adjSrcPos)	//it's an edge sink
					ret = ret | CONTACT_SRC;
			}
			return(ret);
		}
		
		//there is no current entering/leaving the contact
	} //end there is 2 active contacts
	return(CONTACT_NONE);	//shouldn't ever get here
}



//this figures out whether we shuold be using voltages, electric fields, or currents
//to determine the band structure and form which contacts they should be.
//while this is setup for SS or transient, this is only called by the transient section of the code

//a copy of this has been moved as a transientSim method. So this is likely never called now.
int CreateNewContacts(Model& mdl, Simulation* sim, std::vector<Point*> edges)
{
	unsigned int sz = edges.size();
	LinearTimeDependence tmp;
	tmp.SetValues(0.0, 0.0, 0.0, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_VOLT_LINEAR, false);
	
	for(unsigned int i=0; i<sz; i++)
	{
		if(edges[i]->contactData == NULL)
		{
			edges[i]->contactData = new Contact;	//use the point contact pointer to create the contact
			sim->contacts.push_back(edges[i]->contactData);	//now take the main link to contacts and put it in here
			edges[i]->contactData->point.push_back(edges[i]);
		}
		//ok, now we need to set some stuff up for this particular contact
		if (sim->SteadyStateData && sim->simType == SIMTYPE_SS)
		{
			edges[i]->contactData->id = i;
			sim->SteadyStateData->ExternalStates[sim->curiter]->ContactEnvironment[i]->ctc = edges[i]->contactData;
			edges[i]->contactData->ContactEnvironmentIndex = i;
		}
	}
	return(0);
}

int DetermineEdgePoints(Model& mdl, std::vector<Point*>& edges)
{
	edges.clear();
	if(mdl.NDim == 1)	//do a bit more code, but more efficient as doing the IF once.
	{
		edges.reserve(2);
		for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		{ //loop through all the points
			if(ptIt->second.adj.adjMX == NULL || ptIt->second.adj.adjPX == NULL)
				edges.push_back(&(ptIt->second));
		}
	}
	else if(mdl.NDim == 2)
	{
		edges.reserve(int(sqrt(double(mdl.numpoints)*4+2)));
		for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		{ //loop through all the points
			if(ptIt->second.adj.adjMX == NULL || ptIt->second.adj.adjPX == NULL
				|| ptIt->second.adj.adjMY == NULL || ptIt->second.adj.adjPY == NULL)
				edges.push_back(&(ptIt->second));
		}
	}
	else
	{
		edges.reserve(int(pow(double(mdl.numpoints), 1.0/3.0))+2);
		for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		{ //loop through all the points
			if(ptIt->second.adj.adjMX == NULL || ptIt->second.adj.adjPX == NULL
				|| ptIt->second.adj.adjMY == NULL || ptIt->second.adj.adjPY == NULL
				|| ptIt->second.adj.adjMZ == NULL || ptIt->second.adj.adjPZ == NULL)
				edges.push_back(&(ptIt->second));
		
		}
	}
	return(0);
}

//builds the partition function and returns the index of the vector that is where the source data is
int Fast_Build_Contact_Current_PFn(std::multimap<MaxMin<double>,ELevel>& band, std::vector<double>& weight, double& tot, Point* pt, ELevel* src, int whichSource, int whichElse)
{
	double kti = KBI / pt->temp;	//just save on a bunch of divisions
	int ret = -1;
	for(std::multimap<MaxMin<double>,ELevel>::iterator elIt = band.begin(); elIt != band.end(); ++elIt)
	{
		ELevel* eLev = &(elIt->second);

		double indivWeightPos = 0.0;
		double indivWeightNeg = 0.0;
		double indivWeight = 0.0;
		
			//it is an electron, positive represents the point becoming more positive
			//this means electrons are leaving the point, so we work with what electrons are there
		if(eLev == src)
		{
			eLev->GetOccupancyOcc(whichSource, indivWeightPos, indivWeightNeg);
			ret = weight.size();	//this will be the index of what's added once this is pushed on the end
		}
		else
			eLev->GetOccupancyOcc(whichElse, indivWeightPos, indivWeightNeg);

			//indivWeightPos = eLev->GetBeginOccStates();
			//it is an electron, negative means the point becoming more negative.
			//this means electrons are entering the point, so work with what is available
			//indivWeightNeg = eLev->GetNumstates() - eLev->GetBeginOccStates();	//availability

			//it is a hole, negative represents the point becoming more negative
			//this means holes are leaving the point, so work with what holes are there
		indivWeightNeg = indivWeightNeg * exp(-eLev->eUse * kti); 	//and exponential distribution

	       //this is messed up. Me thinking I was being smart, but not actually being smart.  The contact current is coming
	       //from an *identical* point.  Therefore, it's current ought to match the carriers there, which will match the carriers
	       //in the present point.  The limitation is going to be the smaller of what's available or the number of carriers.
	       //essentially make Pos=Neg and remove any difference between the two
		indivWeight = indivWeightPos < indivWeightNeg ? indivWeightPos : indivWeightNeg;
	     
		if(pt->MatProps)	//just be safe for null pointers
			indivWeight = (eLev->carriertype==ELECTRON) ? indivWeight * pt->MatProps->MuN : indivWeight * pt->MatProps->MuP;

		tot += indivWeightPos;
		weight.push_back(indivWeight);
//		state.push_back(eLev);
	}	//end for loop going through the entire band

	
	return(ret);
}

//don't add any rates into the source, and allow the carrier concentration to vary from begin and adjust the "band" locally
double ELevel::FastELevelContactProcess(int whichcarrierSrc, int whichcarrierOther)
{
	Contact* contact = GetContact();
	if(contact == NULL)	//there is no contact, there is no rate
		return(0.0);

	double nRateIn = 0.0;
	
	int active = contact->GetCurrentActive();	//what are the flags needed
	int calcRates = active & (CONTACT_CONNECT_EXTERNAL | CONTACT_MASK_CURRENTS_ED);	//get it to be just the flags corresponding to interactions

	if (calcRates == CONTACT_INACTIVE)
		return(0);

	int activeED = active & CONTACT_MASK_ED;	//only contains ED flags
	int activeNonED = active & CONTACT_MASK_NONED;	//only contains non-ED flags

	std::vector<double> PFnweights;

	double totalWeight = 0.0;
	double totalArea = 0.0;
	bool UpdateNonContactRates = false;
	int index = -1;
	for(unsigned int i=0; i<contact->point.size(); i++)	//build the partition function necessary for the contact first
	{
		int tmp;
		tmp = Fast_Build_Contact_Current_PFn(contact->point[i]->cb.energies, PFnweights, totalWeight, contact->point[i], this, whichcarrierSrc, whichcarrierOther);
		if(tmp != -1)	//this should only be true ONCE!
			index = tmp;
		tmp = Fast_Build_Contact_Current_PFn(contact->point[i]->vb.energies, PFnweights, totalWeight, contact->point[i], this, whichcarrierSrc, whichcarrierOther);
		if(tmp != -1)	//this should only be true ONCE!
			index = tmp;
		
		totalArea += contact->point[i]->area.y;	//stay consistent with all the others...
	}
	if(totalWeight != 0.0)
		totalWeight = 1.0 / totalWeight;

	if(index == -1)	//not sure how this happened...
		return(0.0);

	double occupancy, availability;
	GetOccupancyOcc(whichcarrierSrc, occupancy, availability);	//store the correct occupancies in
	double current = 0.0;
	if(activeED)
	{
	//	bool ChargeEnteringPoint = DetermineEDCurrentFlow(contact, activeED);
		bool AllowEField = false;// true;	an ED EField should not be injecting any current!
		if(active & (CONTACT_CUR_DENSITY_COS_ED | CONTACT_CUR_DENSITY_LINEAR_ED))
		{
			//we need to convert this to a particular current from the current density
			//this is just a simple area calculation
			double curDensity = contact->GetCurEDDensity();
			current += curDensity * totalArea;	//length x * length z, this just has to be going somewhere outside. In 1D, this is going to be x * (y or z)
			AllowEField = false;
		}
		if(active & CONTACT_MASK_CURRENTS_ED)
		{
			current += contact->GetEDCurrent();
			AllowEField = false;
		}
		if(current != 0.0)
		{
			//now we need to loop through all the partition functions and store the current * weight
//			rateData.type = RATE_CONTACT_CURRENT_EXT;
			
			double rate=0.0;
			if(current > 0.0)	//positive charge is entering the point
			{
				if(carriertype == HOLE)
					rate = QI * current * PFnweights[index] * totalWeight;	//totalPos is inversed
				else
					rate = -QI * current * PFnweights[index] * totalWeight;
			}
			else if(current < 0.0) //then negative charge is entering the point
			{
				if(carriertype == HOLE)
					rate = QI * current * PFnweights[index] * totalWeight;	//totalNeg is inversed
				else
					rate = -QI * current * PFnweights[index] * totalWeight;
			}
			
			nRateIn += rate;
			
		}
		else if(active & CONTACT_MASK_EFIELD_ED && AllowEField)
		{

			double EField = contact->GetEDEField();
			//the electric field only affects actual carriers, so the weighting system just won't work as before
			//there is no set current, so going by availability makes absolutely no sense.

			double rate=0.0;
			//a positive electric field means positive charge entering the contact
			//so if the electric field is positive, that means electrons should be leaving
			//or a rate out
			if(carriertype == ELECTRON)
				rate = -EField * occupancy * pt.MatProps->MuN * pt.widthi.y;
			else
				rate = EField * occupancy * pt.MatProps->MuP * pt.widthi.y;
			//the normal current density has a volume in the concentration, with an electric field component entering.
			//those lead to an area plane with that has a normal in the direction of travel of the current
			//in this case, that means to get there, we should divide by the volume and multiply by the area of that plane
			//otherwise known as dividing by the width
			
			nRateIn += rate;				

		}
	}

	current = 0.0;
	if(activeNonED != CONTACT_INACTIVE)	//there are electric fields/currents involved
	{
		//but this should ONLY apply if it is on the edge of the device. This will create additional currents/rates
		//but if it's not on the edge, the electric field should be set to induce these rates
		bool AllowEField = true;
		if(active & (CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_LINEAR))
		{
			totalArea = 0.0;
			for(unsigned int i=0; i<contact->point.size(); i++)	//build the partition function necessary for the contact first
				totalArea += contact->point[i]->area.x;	//stay consistent with all the others...

			//we need to convert this to a particular current from the current density
			//this is just a simple area calculation
			double curDensity = contact->GetCurDensity();
			current += curDensity * totalArea;	//length y * length z, this just has to be going somewhere outside. In 1D, this is going to be x * (y or z)
			AllowEField = false;
		}
		if(active & CONTACT_MASK_CURRENTS_NONED)
		{
			current += contact->GetCurrent();
			AllowEField = false;
		}
		if(current != 0.0)	//this does not go if the above code goes/
		{
			//now we need to loop through all the partition functions and store the current * weight

			double rate = 0.0;
			if(pt.adj.adjMX == NULL)	//assume the current interacts with the left. If statement order processed to match poisson prep
			{
				//a positive current means holes moving in the positive direction. The target is
				//in the more negative direction. That means holes will enter and e- will leave the source
				if(current > 0.0) //positive charge is entering
				{
					if(carriertype == HOLE)
						rate = QI * current * PFnweights[index] * totalWeight;	//totalPos is inversed
					else
						rate = -QI * current * PFnweights[index] * totalWeight;
				}
				else if(current < 0.0)
				{
					//a negative current means holes moving in the negative direction.
					//the target point is in the negative direction.
					//This means e- will enter and holes will leave the source
					if(carriertype == HOLE)
						rate = QI * current * PFnweights[index] * totalWeight;	//this < 0...
					else
						rate = -QI * current * PFnweights[index] * totalWeight;
				}
			}
			else if(pt.adj.adjPX == NULL) //assume the current interacts with the right
			{
				//here, the target is in the more positive direction.
				if(current > 0.0)
				{
					//Holes are moving in the positive direction from the source
					//so holes are leaving and e- are entering, and it is becoming more negative
					if(carriertype == HOLE)
						rate = -QI * current * PFnweights[index] * totalWeight;	//this < 0...
					else
						rate = QI * current * PFnweights[index] * totalWeight;
				}
				else if(current < 0.0)
				{
					//holes moving left, so holes are entering the source and e- are leaving
					//it is becoming more positive
					if(carriertype == HOLE)
						rate = -QI * current * PFnweights[index] * totalWeight;	//this > 0...
					else
						rate = QI * current * PFnweights[index] * totalWeight;
				}
			}
			else
			{
//				rateData.type = RATE_CONTACT_CURRENT_INT;
				//assume the same as adjPX being nonexistent and the "duplicate" point,
				//but now the point does exist. Adjust rates approprioately
				if(current > 0.0)
				{
					//Holes are moving in the positive direction from the source
					//so holes are leaving and e- are entering, and it is becoming more negative
					if(carriertype == HOLE)
						rate = -QI * current * PFnweights[index] * totalWeight;	//this < 0...
					else
						rate = QI * current * PFnweights[index] * totalWeight;
					UpdateNonContactRates = true;	//flag that there's going to be some obnoxious calculations done
				}
				else if(current < 0.0)
				{
					//holes moving left, so holes are entering the source and e- are leaving
					//it is becoming more positive
					if(carriertype == HOLE)
						rate = -QI * current * PFnweights[index] * totalWeight;	//this > 0...
					else
						rate = QI * current * PFnweights[index] * totalWeight;
					
					UpdateNonContactRates = true;	//flag that there's going to be some obnoxious calculations done
				}
			}
			nRateIn += rate;

		}
		else if (active & (CONTACT_MASK_EFIELD_NONED | CONTACT_CONNECT_EXTERNAL) && AllowEField)
		{
			double EField = contact->GetEField();
			
			//the electric field only affects actual carriers, so the weighting system just won't work as before
			//there is no set current, so going by availability makes absolutely no sense.
			double rate;
			//a positive electric field means positive field lines moving in the positive direction
			//this is not an external flow, so it doesn't necessarily mean entering the contact
				
			if(pt.adj.adjMX == NULL)	//assume the current interacts with the left
			{
//					rateData.type = RATE_CONTACT_CURRENT_INTEXT;
				 //if we assume a positive E-Field, and the source is to the left/negative
				//this implies that electrons should be leaving and that holes should be entering
				//we are assuming an identical point is adjacent to send the carriers, and that no scattering occurs
				if(carriertype == ELECTRON)
				{ //so then this would be negative and
					rate = -EField * occupancy * pt.MatProps->MuN * pt.widthi.y;
				}
				else
				{ //this will be positive.
					rate = EField * occupancy * pt.MatProps->MuP * pt.widthi.y;
				}
			}
			else if(pt.adj.adjPX == NULL) //assume the current interacts with the right
			{

				//if we assume a positve E-field, no the interaction is going to the right, or positive.
				//this means the point is the source, so holes should be leaving and electrons entering,
				//because it's coming from the opposite direction.
				if(carriertype == ELECTRON)
				{ //so then this would be positive and
					rate = EField * occupancy * pt.MatProps->MuN * pt.widthi.y;
				}
				else
				{ //this will be negative.
					rate = -EField * occupancy * pt.MatProps->MuP * pt.widthi.y;
				}
			}
			else //just assume it interacts with the point to the right as the previous else if, but different flag
			{
				if(carriertype == ELECTRON)
				{ //so then this would be positive and
					rate = EField * occupancy * pt.MatProps->MuN * pt.widthi.y;
				}
				else
				{ //this will be negative.
					rate = -EField * occupancy * pt.MatProps->MuP * pt.widthi.y;
				}
			}
			//the normal current density has a volume in the concentration, with an electric field component entering.
			//those lead to an area plane with that has a normal in the direction of travel of the current
			//in this case, that means to get there, we should divide by the volume and multiply by the area of that plane
			//otherwise known as dividing by the width
			//no Q Charge because this is done separate...
				
			nRateIn += rate;
		}
		

	}
	else if(active & CONTACT_CONNECT_EXTERNAL)
	{
		//in this case, it interacts with the outside world.
		//the voltages may or may not be set, but more importantly, there are no EFields/currents
		//It may interact with the outside world if it is on the outside edge of the device.

		//work backwards from poisson's equation.
		//rho should already be calculated

		//in the future, this should be reduced down to Gauss' law and measure the net flux in, figure the change
		//required from charge within, and translate that into an electric field.
		//for now, I'm just going to assume 1D to keep this simple and get the code running


		double EField = 0.0;
		double rate=0.0;
		double beginOcc = GetBeginOccStates();	//needed for adjusting rho/psi appropriately
		double SourceAffectSource, SourceAffectTarget;	//adding charge will directly change the electric field
		//in the adjacent point, and we can't skimp on doing this adjustment or it diverges massively
		FindPoissonCoeff(&pt, pt.adj.self, SourceAffectSource);

		if(pt.adj.adjMX == NULL && pt.adj.adjPX!=NULL)
		{
			FindPoissonCoeff(&pt, pt.adj.adjPX->adj.self, SourceAffectTarget);
			//the left is non existent, so we need to find the proper electric field
			//and get the appropriate rate from that.
			double differenceInChargedCarriers = (carriertype==HOLE) ? occupancy - beginOcc : beginOcc - occupancy;
			//this can be multiplied by SAS or SAT to see how much psi changes if it's viewed as positive charge
			//so if it's a hole, and there are more carriers, the difference in charge should be positive as shown...
			double rho = pt.rho + QCHARGE * (differenceInChargedCarriers) * pt.voli;	//adjust the charge appropriately
			double eps = (pt.MatProps) ? EPSNOT * double(pt.MatProps->epsR) : EPSNOT;
			double RHS = rho / eps;	//positive because V prop to -Psi
			double distP = fabs(pt.adj.adjPX->position.x - pt.position.x);
			double distM = 2.0 * (pt.position.x - pt.mxmy.x);
			RHS = RHS * 0.5 * (distP + distM);
			double PsiCur = pt.Psi + SourceAffectSource * differenceInChargedCarriers;
			double PsiAdj = pt.adj.adjPX->Psi + SourceAffectTarget * differenceInChargedCarriers;
			double LHS = (PsiAdj - PsiCur) / distP;
			RHS -= LHS;
			//now -(Psi_source - Psi_external)/dist_external = RHS
			//we're mirroring the point, so...
			RHS = RHS * distM;
			double PsiExt = PsiCur + RHS;
			EField = (PsiCur - PsiExt) / distM;	//the Efield is viewed as entering the point from the left
			//so if PsiCur is greater, it should result in an electric field putting positive
			//charge into the contact (or removing negative charge)
			

			if(carriertype == ELECTRON)
				rate = -EField * occupancy * pt.MatProps->MuN * pt.widthi.x;
			else
				rate = EField * occupancy * pt.MatProps->MuP *pt.widthi.x;
		}
		else if(pt.adj.adjPX == NULL && pt.adj.adjMX!=NULL)
		{
			FindPoissonCoeff(&pt, pt.adj.adjMX->adj.self, SourceAffectTarget);
			double differenceInChargedCarriers = (carriertype==HOLE) ? occupancy - beginOcc : beginOcc - occupancy;
			double rho = pt.rho + QCHARGE * (differenceInChargedCarriers) * pt.voli;	//adjust the charge appropriately
			double eps = (pt.MatProps) ? EPSNOT * double(pt.MatProps->epsR) : EPSNOT;
			double RHS = rho / eps;	//positive because V prop to -Psi
			double distM = fabs(pt.position.x - pt.adj.adjMX->position.x);
			double distP = 2.0 * (pt.pxpy.x - pt.position.x);
			RHS = RHS * 0.5 * (distP + distM);
			double PsiCur = pt.Psi + SourceAffectSource * differenceInChargedCarriers;
			double PsiAdj = pt.adj.adjMX->Psi + SourceAffectTarget * differenceInChargedCarriers;
			double LHS = (PsiCur - PsiAdj) / distM;
			RHS += LHS;
			//now (Psi_source - Psi_external)/dist_external = RHS... shit. It's (Psi_external - Psi_source) / dist_external
			//we're mirroring the point, so...
			RHS = RHS * distP;
			double PsiExt = PsiCur + RHS;
			EField = (PsiExt - PsiCur) / distP;	//the Efield is viewed as leaving the point
			//so if PsiCur is greater, it should result in an electric field putting positive
			//charge into the contact (or removing negative charge)
			//note this is the opposite of what the true efield is - the coordinates are reversed

			if(carriertype == ELECTRON) //a positive e- field is taking e- from the right and gaining e-
				rate = EField * occupancy * pt.MatProps->MuN * pt.widthi.x;
			else //a positive efield means holes are leaving
				rate = -EField * occupancy * pt.MatProps->MuP * pt.widthi.x;
		}

		nRateIn += rate;
		
		//a positive electric field means positive charge entering the contact
		//so if the electric field is positive, that means electrons should be leaving
		//or a rate out
			
		//the normal current density has a volume in the concentration, with an electric field component entering.
		//those lead to an area plane with that has a normal in the direction of travel of the current
		//in this case, that means to get there, we should divide by the volume and multiply by the area of that plane
		//otherwise known as dividing by the width
			
		

	}
	
	
	//process any contact links this may have.
	std::vector<ContactLink*> CLinks;
	if(contact->GetCLinks(CLinks))	//loads any links it has. If they exist, puts them in variable and returns true
	{
		unsigned int sz = CLinks.size();
		for(unsigned int i=0; i<sz; i++)
		{
			nRateIn += FastELevelContactLink(CLinks[i], whichcarrierSrc, whichcarrierOther);
		}
	}

//	if(UpdateNonContactRates)
//		UpdateContactRate(contact, sim);	//update the rates if needed due to their being currents of the INT sort.


	return(nRateIn);
}

double ELevel::FastELevelContactLink(ContactLink* link, int whichCarrierSrc, int whichcarrierOther)
{
	Contact* curCtc = GetContact();
	if (link == nullptr || curCtc == nullptr || link->resistance <= 0.0 && link->current == 0.0)	//then there really is no link between the two
		return(0.0);

	//this needs to take all the energy levels from one contact and calculate rates going to the other contact
	//if there is zero resistance, the fermi levels need to equalize INSTANTLY.
	//if the resistance is negative, then go with a current. As beforehand, a positive current 
	//means that the contact is receiving charge.
	//for resistances: just do I = V/R, and then partition based on availability and carriers present/mobility
	//the V is going to be the difference between the energy levels

	std::vector<ELevel*> SourceStates;	//the source states that are linked
	std::vector<double> PFnPosEnterSrc;	//holes enter src, e- leave source
	std::vector<double> PFnNegEnterSrc;	//e- enter src, holes leave
	double PfnPosSrcI = 0.0;
	double PfnNegSrcI = 0.0;

	std::vector<ELevel*> TargetStates;	//the target states of what are linked
	std::vector<double> PFnPosEnterTrg;
	std::vector<double> PFnNegEnterTrg;
	double PfnPosTrgI = 0.0;
	double PfnNegTrgI = 0.0;
	double totcarCBsrc, totcarVBsrc, totcarCBtrg, totcarVBtrg;
	totcarCBsrc= totcarVBsrc= totcarCBtrg= totcarVBtrg=0.0;
	double mobility_partition = 0.0;

	//now need to make a bunch of partition functions
	//and it needs to be done via all the points.
	unsigned int whichSrc = -1;	//largest number possible

	//loop through all source points
	for(unsigned int ptctr=0; ptctr<curCtc->point.size(); ptctr++)
	{
		Point* pt = curCtc->point[ptctr];
		double mu = (pt->MatProps != NULL) ? pt->MatProps->MuN : 1.0;
		double avail, occ;
		double boltz = KBI / pt->temp;
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = pt->cb.energies.begin(); cbIt != pt->cb.energies.end(); ++cbIt)
		{
			SourceStates.push_back(&(cbIt->second));
			cbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * cbIt->second.boltzeUse;
			PFnPosEnterSrc.push_back(occ);	//positive charge entering, so e- leaving. Look at what exists
			PFnNegEnterSrc.push_back(avail);	//negative charge entering, so e- entering. Look at what's available
			PfnPosSrcI += occ;
			PfnNegSrcI += avail;
			totcarCBsrc += occ;
			if(&(cbIt->second) == this)
				whichSrc = SourceStates.size() - 1;

		}

		
		mu = (pt->MatProps != NULL) ? pt->MatProps->MuP : 1.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = pt->vb.energies.begin(); vbIt != pt->vb.energies.end(); ++vbIt)
		{
			SourceStates.push_back(&(vbIt->second));
			vbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * vbIt->second.boltzeUse;
			PFnNegEnterSrc.push_back(occ);	//negative charge entering, so holes leaving. Look at what already is there
			PFnPosEnterSrc.push_back(avail);	//positive charge entering, so holes entering. Look where holes can go
			PfnNegSrcI += occ;
			PfnPosSrcI += avail;
			totcarVBsrc += occ;
			if(&(vbIt->second) == this)
				whichSrc = SourceStates.size() - 1;
		}
	}

	//now do the same thing for the target points
	for(unsigned int ptctr=0; ptctr<link->targetContact->point.size(); ptctr++)
	{
		Point* pt = link->targetContact->point[ptctr];
		double mu = (pt->MatProps != NULL) ? pt->MatProps->MuN : 1.0;
		double avail, occ;
		double boltz = KBI / pt->temp;
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = pt->cb.energies.begin(); cbIt != pt->cb.energies.end(); ++cbIt)
		{
			TargetStates.push_back(&(cbIt->second));
			cbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * cbIt->second.boltzeUse;
			PFnPosEnterTrg.push_back(occ);	//positive charge entering, so e- leaving. Look at what exists
			PFnNegEnterTrg.push_back(avail);	//negative charge entering, so e- entering. Look at what's available
			PfnPosTrgI += occ;
			PfnNegTrgI += avail;
			totcarCBtrg += occ;
		}

		
		mu = (pt->MatProps != NULL) ? pt->MatProps->MuP : 1.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = pt->vb.energies.begin(); vbIt != pt->vb.energies.end(); ++vbIt)
		{
			TargetStates.push_back(&(vbIt->second));
			vbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * vbIt->second.boltzeUse;
			PFnNegEnterTrg.push_back(occ);	//negative charge entering, so holes leaving. Look at what already is there
			PFnPosEnterTrg.push_back(avail);	//positive charge entering, so holes entering. Look where holes can go
			PfnNegTrgI += occ;
			PfnPosTrgI += avail;
			totcarVBtrg += occ;
		}
	}

	PfnNegSrcI = (PfnNegSrcI > 0.0) ? 1.0 / PfnNegSrcI : 0.0;
	PfnPosSrcI = (PfnPosSrcI > 0.0) ? 1.0 / PfnPosSrcI : 0.0;
	PfnNegTrgI = (PfnNegTrgI > 0.0) ? 1.0 / PfnNegTrgI : 0.0;
	PfnPosTrgI = (PfnPosTrgI > 0.0) ? 1.0 / PfnPosTrgI : 0.0;
	double carrierTrgI = (totcarCBtrg > 0.0 || totcarVBtrg > 0.0) ? 1.0 / (totcarCBtrg + totcarVBtrg) : 0.0;
	double carrierSrcI = (totcarCBsrc > 0.0 || totcarVBsrc > 0.0) ? 1.0 / (totcarCBsrc + totcarVBsrc) : 0.0;
	mobility_partition = (mobility_partition > 0.0) ? 1.0 / mobility_partition : 0.0;


	double nRateIn = 0.0;

	if(link->resistance <= 0.0)
	{
		unsigned int szSrc= SourceStates.size();
		unsigned int szTrg = TargetStates.size();
		double current = link->current * QI;
		double occSrc, availSrc, occTrg, availTrg, muSrc, muTrg, rIn, rOut;
		if(whichSrc >= szSrc)
			return(0.0);
		
		//current is rate of POSITIVE charge ENTERING source
		//current is rate of NEGATIVE charge LEAVING source
		if(carriertype==ELECTRON)
			muSrc = getPoint().MatProps != NULL ? getPoint().MatProps->MuN : 1.0;
		else
			muSrc = getPoint().MatProps != NULL ? getPoint().MatProps->MuP : 1.0;

		GetOccupancyOcc(RATE_OCC_BEGIN, occSrc, availSrc);

		for(unsigned int trg = 0; trg < szTrg; trg++)
		{
			if(TargetStates[trg]->carriertype==ELECTRON)
				muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuN : 1.0;
			else
				muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuP : 1.0;
			
			TargetStates[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occTrg, availTrg);

			//But this should be a total sum, so need to partition between
			//all carriers and all availabilities appropriately
			//we know the flow of current direction, so now this is just
			//a game of figuring out signs
			if(carriertype == ELECTRON && TargetStates[trg]->carriertype==ELECTRON)
			{
				//electrons are flowing out of the source into the target
				//assume POSITIVE current, so that would be electrons leaving the source, so ROut positive (negative net rate)
				rOut = current * muSrc * occSrc * (occTrg * carrierTrgI) * PFnNegEnterTrg[trg] * PfnNegTrgI;
				//positive current, electrons leaving the target/entering source, so RIn is negative
				rIn = -current * muTrg * occTrg * (occSrc * carrierSrcI) * PFnNegEnterSrc[whichSrc] * PfnNegSrcI;
				//these will both  result in a net flow of carriers in the same direction
			}
			else if(carriertype == HOLE && TargetStates[trg]->carriertype==HOLE)
			{
				//holes are flowing out of the source into the target
				//Corresponds with PFnNegEnterSrc & PFnPosEnterTrg
				//assume POSITIVE current, want contribution from ROut to be positive for charge entering
				rOut = -current * muSrc * occSrc * (occTrg * carrierTrgI) * PFnPosEnterTrg[trg] * PfnPosTrgI;
				//this is told from the source state, which is a hole, so if there is a positive current, there is an influx
				//of positive charge. This is an increase in holes, so rate in is positive
				rIn = current * muTrg * occTrg * (occSrc * carrierSrcI) * PFnPosEnterSrc[whichSrc] * PfnPosSrcI;
			}
			else if(SourceStates[whichSrc]->carriertype == ELECTRON && TargetStates[trg]->carriertype==HOLE)
			{
				//this is describing electrons leaving the source, so the rate out is appropriate difEf
				rOut = current * muSrc * occSrc * (occTrg * carrierTrgI) * PFnNegEnterTrg[trg] * PfnNegTrgI;
				//this is describing holes entering the source (elec), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
				//however, this rate out of holes into the source causes the source to lose carriers, so still need negative
				rIn = -current * muTrg * occTrg * (occSrc * carrierSrcI) * PFnPosEnterSrc[whichSrc] * PfnPosSrcI;
			}
			else //holes in src going to electrons in trg
			{
				rOut = -current * muSrc * occSrc * (occTrg * carrierTrgI) * PFnPosEnterTrg[trg] * PfnPosTrgI;
				//this is describing electrons entering the source (p), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
				//however, this rate out of e- into the source causes the source to lose carriers, so still need negative
				rIn = current * muTrg * occTrg * (occSrc * carrierSrcI) * PFnNegEnterSrc[whichSrc] * PfnNegSrcI;
			}
			
			nRateIn += (rIn - rOut);
		}	//end for loop through all target states
	} //end using current
	else //then use the resistance to figure out the current instead of some partition functions
	{
		//go through all the potential sources
		unsigned int szSrc= SourceStates.size();
		unsigned int szTrg = TargetStates.size();
		
		double invResist = QI * mobility_partition / link->resistance;
		double occSrc, availSrc, occTrg, availTrg, muSrc, muTrg, rIn, rOut;
		
		if(SourceStates[whichSrc]->carriertype==ELECTRON)
			muSrc = SourceStates[whichSrc]->getPoint().MatProps != NULL ? SourceStates[whichSrc]->getPoint().MatProps->MuN : 1.0;
		else
			muSrc = SourceStates[whichSrc]->getPoint().MatProps != NULL ? SourceStates[whichSrc]->getPoint().MatProps->MuP : 1.0;
		SourceStates[whichSrc]->GetOccupancyOcc(RATE_OCC_BEGIN, occSrc, availSrc);
		for(unsigned int trg = 0; trg < szTrg; trg++)
		{
		
			if(TargetStates[trg]->carriertype==ELECTRON)
				muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuN : 1.0;
			else
				muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuP : 1.0;

				TargetStates[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occTrg, availTrg);
			double difEf = CalculateDifEf(this, TargetStates[trg]);
				//this is returning positive difEf such that the carrier leaves the source.
				
				//I = V/(Rq), removes coulomb unit so carrier/sec
				//But this should be a total sum, so need to partition between
				//all carriers and all availabilities appropriately
			double rate = 0.0;	//net rate in to the source state

			if(carriertype == ELECTRON && TargetStates[trg]->carriertype==ELECTRON)
			{
				//electrons are flowing out of the source into the target
				rOut = difEf * invResist * muSrc * occSrc * (occTrg * carrierTrgI) * PFnNegEnterTrg[trg] * PfnNegTrgI;
				rIn = -difEf * invResist * muTrg * occTrg * (occSrc * carrierSrcI) * PFnNegEnterSrc[whichSrc] * PfnNegSrcI;
				//these will both  result in a net flow of carriers in the same direction
			}
			else if(carriertype == HOLE && TargetStates[trg]->carriertype==HOLE)
			{
				//holes are flowing out of the source into the target
				//Corresponds with PFnNegEnterSrc & PFnPosEnterTrg
				rOut = difEf * invResist * muSrc * occSrc * (occTrg * carrierTrgI) * PFnPosEnterTrg[trg] * PfnPosTrgI;
				rIn = -difEf * invResist * muTrg * occTrg * (occSrc * carrierSrcI) * PFnPosEnterSrc[whichSrc] * PfnPosSrcI;
				//negative because rate should be Net Rate In, but difEf is in terms of net rate out
			}
			else if(carriertype == ELECTRON && TargetStates[trg]->carriertype==HOLE)
			{
				//this is describing electrons leaving the source, so the rate out is appropriate difEf
				rOut = difEf * invResist * muSrc * occSrc * (occTrg * carrierTrgI) * PFnNegEnterTrg[trg] * PfnNegTrgI;
				//this is describing holes entering the source (elec), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
				//however, this rate out of holes into the source causes the source to lose carriers, so still need negative
				rIn = -difEf * invResist * muTrg * occTrg * (occSrc * carrierSrcI) * PFnPosEnterSrc[whichSrc] * PfnPosSrcI;
			}
			else //holes in src going to electrons in trg
			{
				rOut = difEf * invResist * muSrc * occSrc * (occTrg * carrierTrgI) * PFnPosEnterTrg[trg] * PfnPosTrgI;

				//this is describing electrons entering the source (p), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
				//however, this rate out of e- into the source causes the source to lose carriers, so still need negative
				rIn = -difEf * invResist * muTrg * occTrg * (occSrc * carrierSrcI) * PFnNegEnterSrc[whichSrc] * PfnNegSrcI;
			}

			
			nRateIn += (rIn - rOut);
					

		}	//end for loop through all target states
	}	//end else (resistance to figure out current)

	return(nRateIn);
}

int ProcessContactTransient(Model& mdl, Contact* contact, Simulation* sim, ELevel* oneonly)
{
	//assume that DeterminePoissonValues has already been called and everything has been initialized

	//process any contact links this may have.
	std::vector<ContactLink*> CLinks;
	//	DebugCLink(false, 0.0, false, true);
	if (contact->GetCLinks(CLinks))	//loads any links it has. If they exist, puts them in variable and returns true
	{
		unsigned int sz = CLinks.size();
		for (unsigned int i = 0; i<sz; i++)
		{
			ProcessContactLink(contact, CLinks[i], sim, oneonly);
		}
	}

	//TEST FOR BEING ACTIVE (interaction with outside world)
	int active = contact->GetCurrentActive();	//what are the flags needed
	int activeVEC = active & (CONTACT_MASK_SIMPLE_VEC | CONTACT_CONNECT_EXTERNAL);	//get it to be just the flags corresponding to interactions

	if (activeVEC == CONTACT_INACTIVE)
		return(0);

	int activeED = active & CONTACT_MASK_ED;	//only contains ED flags
	int activeNonED = active & CONTACT_MASK_NONED;	//only contains non-ED flags

	std::vector<ELevel*> PFnStates;
	std::vector<double> PFnweights;

	double totalWeight = 0.0;
	XSpace totalArea;
	bool UpdateNonContactRates = false;

	ELevelRate rateData;
	//ELevelOpticalRate rateData;
	//ELevelOpticalRateSort rateSort;
	rateData.target = NULL;
	rateData.deriv = 0.0;
	rateData.deriv2 = 0.0;
	rateData.derivsourceonly = 0.0;

	for (unsigned int i = 0; i<contact->point.size(); i++)	//build the partition function necessary for the contact first
	{
		Build_Contact_Current_PFn(contact->point[i]->cb.energies, PFnStates, PFnweights, totalWeight, contact->point[i]);
		Build_Contact_Current_PFn(contact->point[i]->vb.energies, PFnStates, PFnweights, totalWeight, contact->point[i]);

		totalArea.y += contact->point[i]->area.y;	//stay consistent with all the others...
		totalArea.x = contact->point[i]->area.x;
	}
	if (totalWeight != 0.0)
		totalWeight = 1.0 / totalWeight;


	double current = 0.0;
	if (activeED)
	{
		//	bool ChargeEnteringPoint = DetermineEDCurrentFlow(contact, activeED);
		bool AllowEField = true;
		if (active & (CONTACT_CUR_DENSITY_COS_ED | CONTACT_CUR_DENSITY_LINEAR_ED))
		{
			//we need to convert this to a particular current from the current density
			//this is just a simple area calculation
			double curDensity = contact->GetCurEDDensity();
			current += curDensity * totalArea.y;	//length x * length z, this just has to be going somewhere outside. In 1D, this is going to be x * (y or z)
			AllowEField = false;
		}
		if (active & CONTACT_MASK_CURRENTS_ED)
		{
			current += contact->GetEDCurrent();
			AllowEField = false;
		}
		if (current != 0.0)
		{
			//now we need to loop through all the partition functions and store the current * weight
			rateData.type = RATE_CONTACT_CURRENT_EXT;
			for (unsigned int i = 0; i<PFnStates.size(); i++)
			{
				double rate = 0.0;
				if (current > 0.0)	//positive charge is entering the point
				{
					if (PFnStates[i]->carriertype == HOLE)
						rate = QI * current * PFnweights[i] * totalWeight;	//totalPos is inversed
					else
						rate = -QI * current * PFnweights[i] * totalWeight;
				}
				else if (current < 0.0) //then negative charge is entering the point
				{
					if (PFnStates[i]->carriertype == HOLE)
						rate = QI * current * PFnweights[i] * totalWeight;	//totalNeg is inversed
					else
						rate = -QI * current * PFnweights[i] * totalWeight;
				}
				rateData.source = PFnStates[i];
				rateData.sourcecarrier = rateData.source->carriertype;
				rateData.netrate = rate;
				if (rate > 0.0)
				{
					rateData.RateIn = rate;
					rateData.RateOut = 0.0;
				}
				else
				{
					rateData.RateIn = 0.0;
					rateData.RateOut = -rate;
				}
				if (rate != 0.0)
				{
					rateData.RateType = rate;
					rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);
					PFnStates[i]->AddELevelRate(rateData, false);
				}
			}
		}
	}

	current = 0.0;
	if (activeNonED != CONTACT_INACTIVE)	//there are electric fields/currents involved
	{
		//in all situations, we should calculate the current based off the external point with the
		//contact.

		//if it's NOT an external contact, then no rates should need to be calculated because
		//psi would have been set when solving poisson's equation to do so
		unsigned int extPtID = 0;
		for (unsigned int ptID = 0; ptID<contact->point.size(); ++ptID)	//build the partition function necessary for the contact first
		{
			if (contact->point[ptID] == NULL)
				continue;

			if (contact->SuppressCurrent() == false)	//then this should be fine.
			{
				Point* srcPt = contact->point[ptID];
				Point* trgPt = &(contact->endPtCalcs[extPtID]);

				//need to then calculate the rates between all of these as drift diffusion
				std::vector<ELevelRate>::iterator rt;
				for (std::map<MaxMin<double>, ELevel>::iterator cbSrc = srcPt->cb.energies.begin(); cbSrc != srcPt->cb.energies.end(); ++cbSrc)
				{
					if (oneonly != NULL && &(cbSrc->second) != oneonly)
						continue;	//only want to get rates out of one particular state
					for (std::map<MaxMin<double>, ELevel>::iterator cbTrg = trgPt->cb.energies.begin(); cbTrg != trgPt->cb.energies.end(); ++cbTrg)
					{
						rt = CalculateCurrentRate(&(cbSrc->second), &(cbTrg->second), sim, RATE_OCC_BEGIN);
						if (rt != cbSrc->second.Rates.end())
							rt->type = RATE_CONTACT_CURRENT_EXT;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
					}
				}

				for (std::map<MaxMin<double>, ELevel>::iterator vbSrc = srcPt->vb.energies.begin(); vbSrc != srcPt->vb.energies.end(); ++vbSrc)
				{
					if (oneonly != NULL && &(vbSrc->second) != oneonly)
						continue;	//only want to get rates out of one particular state
					for (std::map<MaxMin<double>, ELevel>::iterator vbTrg = trgPt->vb.energies.begin(); vbTrg != trgPt->vb.energies.end(); ++vbTrg)
					{
						rt = CalculateCurrentRate(&(vbSrc->second), &(vbTrg->second), sim, RATE_OCC_BEGIN);
						if (rt != vbSrc->second.Rates.end())
							rt->type = RATE_CONTACT_CURRENT_EXT;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
					}
				}
			}
			else //the current was suppressed, so it's not going to match what you'd expect from the band diagram
			{
				//if the current is suppressed, then do the same as extra-dimensional current.
				//a current was specified (or not left open to be calculated), but it did not affect
				//the band structure.  This means that the current does not match reality
				current = 0.0;
				if (active & CONTACT_MASK_CURRENTS_NONED)
				{
					//we need to convert this to a particular current from the current density
					//this is just a simple area calculation
					double curDensity = contact->GetCurDensity();
					current += curDensity * totalArea.x;
					current += contact->GetCurrent();
				}

				if (current != 0.0)	//the answer is not do nothing :/
				{
					//this is essentially the same as doing an extra-dimensional current.
					rateData.type = RATE_CONTACT_CURRENT_EXT;
					for (unsigned int i = 0; i<PFnStates.size(); i++)
					{
						double rate = 0.0;
						if (current > 0.0)	//positive charge is entering the point
						{
							if (PFnStates[i]->carriertype == HOLE)
								rate = QI * current * PFnweights[i] * totalWeight;	//totalPos is inversed
							else
								rate = -QI * current * PFnweights[i] * totalWeight;
						}
						else if (current < 0.0) //then negative charge is entering the point
						{
							if (PFnStates[i]->carriertype == HOLE)
								rate = QI * current * PFnweights[i] * totalWeight;	//totalNeg is inversed
							else
								rate = -QI * current * PFnweights[i] * totalWeight;
						}
						rateData.source = PFnStates[i];
						rateData.sourcecarrier = rateData.source->carriertype;
						rateData.netrate = rate;
						if (rate > 0.0)
						{
							rateData.RateIn = rate;
							rateData.RateOut = 0.0;
						}
						else
						{
							rateData.RateIn = 0.0;
							rateData.RateOut = -rate;
						}
						if (rate != 0.0)
						{
							rateData.RateType = rate;
							rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);
							PFnStates[i]->AddELevelRate(rateData, false);
						}
					}
				}
			}
		}	//end loop going through all the points for the contact
	}




	if (UpdateNonContactRates)
		UpdateContactRate(contact, sim);	//update the rates if needed due to their being currents of the INT sort.

	return(0);
}


int ProcessContactSteadyState(Model& mdl, Contact* contact, Simulation* sim, ELevel* oneonly)
{
	//assume that DeterminePoissonValues has already been called and everything has been initialized

	//process any contact links this may have.
	std::vector<ContactLink*> CLinks;
	
	//	DebugCLink(false, 0.0, false, true);
	if (contact->GetCLinks(CLinks))	//loads any links it has. If they exist, puts them in variable and returns true
	{
		unsigned int sz = CLinks.size();
		for (unsigned int i = 0; i<sz; i++)
		{
			ProcessContactLink(contact, CLinks[i], sim, oneonly);
		}
	}

	//TEST FOR BEING ACTIVE (interaction with outside world)
	int active = contact->GetCurrentActive();	//what are the flags needed
	if ((active & CONTACT_SS) == CONTACT_INACTIVE)
		return(0);	//this isn't a steady state contact, so do nothing!

	int activeVEC = active & CONTACT_MASK_SIMPLE_VEC;	//get it to be just the flags corresponding to interactions
	int activeED = active & CONTACT_MASK_ED;	//only contains ED flags

	if (activeVEC == CONTACT_INACTIVE && activeED == CONTACT_INACTIVE)
		return(0);	//it's not doing anything

	std::vector<ELevel*> PFnStates;
	std::vector<double> PFnweights;

	std::vector<ELevel*> trgPFnStatesCB;
	std::vector<double> trgPFnWeightsCB;
	std::vector<ELevel*> trgPFnStatesVB;
	std::vector<double> trgPFnWeightsVB;
	std::vector<int> outputContactsCB;
	std::vector<int> outputContactsVB;
	int tmpOutContact;

	double totalWeight = 0.0;
	double trgTotalWeightCB = 0.0;
	double trgTotalWeightVB = 0.0;
	XSpace totalArea;
	totalArea.clear();
	bool UpdateNonContactRates = false;

	ELevelRate rateData;
	//ELevelOpticalRate rateData;
	//ELevelOpticalRateSort rateSort;
	rateData.target = NULL;
	rateData.deriv = 0.0;
	rateData.deriv2 = 0.0;
	rateData.derivsourceonly = 0.0;
	rateData.desiredTStep = 0.0;

	//used for activeEd, current is suppressed(current not set BD), or suppressed current is zero
	bool doPFn = (activeED != 0);
	double current = 0.0;
	if(!doPFn && activeVEC != CONTACT_INACTIVE)
	{
		if(contact->SuppressCurrent() == true)
		{
			current = 0.0;
			if (active & CONTACT_CUR_DENSITY_LINEAR)
			{
				//we need to convert this to a particular current from the current density
				//this is just a simple area calculation
				current = contact->GetCurDensity();
			}
			if(active & CONTACT_CURRENT_LINEAR)
				current += contact->GetCurrent();

			if(current != 0.0)
				doPFn = true;
		}
	}
	if(doPFn)
	{
		for (unsigned int i = 0; i<contact->point.size(); i++)	//build the partition function necessary for the contact first
		{
			Build_Contact_Current_PFn(contact->point[i]->cb.energies, PFnStates, PFnweights, totalWeight, contact->point[i]);
			Build_Contact_Current_PFn(contact->point[i]->vb.energies, PFnStates, PFnweights, totalWeight, contact->point[i]);

			totalArea.y += contact->point[i]->area.y;	//stay consistent with all the others...
			totalArea.x = contact->point[i]->area.x;

			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjMX, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMX->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjMX, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMX->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjMX, &outputContactsVB, tmpOutContact);
			}
			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjPX, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPX->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjPX, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPX->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjPX, &outputContactsVB, tmpOutContact);
			}
			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjMY, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMY->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjMY, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMY->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjMY, &outputContactsVB, tmpOutContact);
			}
			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjPY, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPY->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjPY, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPY->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjPY, &outputContactsVB, tmpOutContact);
			}
			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjMZ, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMZ->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjMZ, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjMZ->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjMZ, &outputContactsVB, tmpOutContact);
			}
			tmpOutContact = CalcRateViaContact(contact->point[i], contact->point[i]->adj.adjPZ, sim->mdesc->Ldimensions);
			if(tmpOutContact & CONTACT_CALCULATE)
			{
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPZ->cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, contact->point[i]->adj.adjPZ, &outputContactsCB, tmpOutContact);
				Build_Contact_Current_PFn(contact->point[i]->adj.adjPZ->vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, contact->point[i]->adj.adjPZ, &outputContactsVB, tmpOutContact);
			}
			if((active & CONTACT_SINK) == CONTACT_INACTIVE)
			{
				//if it's a contact sink, there is no external point to choose from
				//if not, there might be, and the code still stands
				if(active & CONTACT_CONNECT_EXTERNAL)
				{
					if(sim->mdesc->NDim==1)
					{
						//in 1D, check if it's the exterior. Make sure to include the external point
						if(contact->point[i]->adj.adjMX==NULL || contact->point[i]->adj.adjPX==NULL)
						{
							Build_Contact_Current_PFn(contact->endPtCalcs[i].cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, &(contact->endPtCalcs[i]), &outputContactsCB,CONTACT_SRC);
							Build_Contact_Current_PFn(contact->endPtCalcs[i].vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, &(contact->endPtCalcs[i]), &outputContactsVB,CONTACT_SRC);
						}
					}
					else if(sim->mdesc->NDim == 2)
					{
						if(contact->point[i]->adj.adjMX==NULL || contact->point[i]->adj.adjPX==NULL ||
							contact->point[i]->adj.adjMY==NULL || contact->point[i]->adj.adjPY==NULL)
						{
							Build_Contact_Current_PFn(contact->endPtCalcs[i].cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, &(contact->endPtCalcs[i]), &outputContactsCB,CONTACT_SRC);
							Build_Contact_Current_PFn(contact->endPtCalcs[i].vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, &(contact->endPtCalcs[i]), &outputContactsVB,CONTACT_SRC);
						}
					}
					else
					{
						if(contact->point[i]->adj.adjMX==NULL || contact->point[i]->adj.adjPX==NULL ||
							contact->point[i]->adj.adjMY==NULL || contact->point[i]->adj.adjPY==NULL || 
							contact->point[i]->adj.adjMZ==NULL || contact->point[i]->adj.adjPZ==NULL)
						{
							Build_Contact_Current_PFn(contact->endPtCalcs[i].cb.energies, trgPFnStatesCB, trgPFnWeightsCB, trgTotalWeightCB, &(contact->endPtCalcs[i]), &outputContactsCB,CONTACT_SRC);
							Build_Contact_Current_PFn(contact->endPtCalcs[i].vb.energies, trgPFnStatesVB, trgPFnWeightsVB, trgTotalWeightVB, &(contact->endPtCalcs[i]), &outputContactsVB,CONTACT_SRC);
						}
					}
				}
			}

		}
		if (totalWeight != 0.0)
			totalWeight = 1.0 / totalWeight;

		if(trgTotalWeightCB != 0.0)
			trgTotalWeightCB = 1.0 / trgTotalWeightCB;

		if(trgTotalWeightVB != 0.0)
			trgTotalWeightVB = 1.0 / trgTotalWeightVB;
	}
	
	current = 0.0;
	if (activeED) //this is current entering the contact from an external source
	{
		//	bool ChargeEnteringPoint = DetermineEDCurrentFlow(contact, activeED);
		bool AllowEField = true;
		if (active & CONTACT_CUR_DENSITY_LINEAR_ED)
		{
			//we need to convert this to a particular current from the current density
			//this is just a simple area calculation
			double curDensity = contact->GetCurEDDensity();
			current += curDensity * totalArea.y;	//length x * length z, this just has to be going somewhere outside. In 1D, this is going to be x * (y or z)
			AllowEField = false;
		}
		if (active & CONTACT_CURRENT_LINEAR_ED)
		{
			current += contact->GetEDCurrent();
			AllowEField = false;
		}
		if (current != 0.0)
		{
			//now we need to loop through all the partition functions and store the current * weight
			if(active & CONTACT_SINK)
			{
				//this is a sink. That means there is a current entering a constantly thermalized state
				//from an external source
				for (unsigned int i = 0; i<PFnStates.size(); i++)
				{
					rateData.type = RATE_CONTACT_CURRENT_EXT;
					rateData.target = NULL;
					double rate = 0.0;
					if (current > 0.0)	//positive charge is entering the point
					{
						if (PFnStates[i]->carriertype == HOLE)
							rate = QI * current * PFnweights[i] * totalWeight;	//totalPos is inversed
						else
							rate = -QI * current * PFnweights[i] * totalWeight;
					}
					else if (current < 0.0) //then negative charge is entering the point
					{
						if (PFnStates[i]->carriertype == HOLE)
							rate = QI * current * PFnweights[i] * totalWeight;	//totalNeg is inversed
						else
							rate = -QI * current * PFnweights[i] * totalWeight;
					}
					rateData.source = PFnStates[i];
					rateData.sourcecarrier = rateData.source->carriertype;
					rateData.netrate = rate;
					if (rate > 0.0)
					{
						rateData.RateIn = rate;
						rateData.RateOut = 0.0;
					}
					else
					{
						rateData.RateIn = 0.0;
						rateData.RateOut = -rate;
					}
					if (rate != 0.0)
					{
						rateData.RateType = rate;
						rateData.desiredTStep = 0.0;
						PFnStates[i]->AddELevelRate(rateData, false);
					}

					//This rate does NOTHING except generate an output for EXT.
					//it will not influence the band structure at all.
					//therefore, it must branch out to the other rates in an equal sense
					double expandedRate;
					if(PFnStates[i]->carriertype == HOLE)
					{
						for(unsigned int j=0; j<trgPFnStatesVB.size(); ++j)
						{
							if(trgPFnStatesVB[j]->getPoint().adj.self == EXT)	//this is an created point in line
								rateData.type = RATE_CONTACT_CURRENT_INTEXT;
							else //it is in the model, so an internal current
								rateData.type = RATE_CONTACT_CURRENT_INT;

							rateData.type = rateData.type | outputContactsVB[j];	//store in the flags
							//tellling whether the source or contact should have it's data recorded

							//this rate must now leave the source, so it's the opposite sign
							expandedRate = -rate * trgPFnWeightsVB[j] * trgTotalWeightVB;
							rateData.target = trgPFnStatesVB[j];
							rateData.targetcarrier = HOLE;

							if (expandedRate > 0.0)
							{
								rateData.RateIn = expandedRate;
								rateData.RateOut = 0.0;
							}
							else
							{
								rateData.RateIn = 0.0;
								rateData.RateOut = -expandedRate;
							}
							if (expandedRate != 0.0)
							{
								rateData.RateType = rateData.RateOut;
								rateData.desiredTStep = 0.0;
								if(oneonly == NULL || oneonly == PFnStates[i])
									PFnStates[i]->AddELevelRate(rateData, false);


								//now we need to reverse it in the other state for reporting purposes
								double tmp = rateData.RateIn;
								rateData.RateIn = rateData.RateOut;
								rateData.RateOut = tmp;
								rateData.RateType = rateData.RateOut;	//avoid double counting. One will go to zero
								rateData.netrate = -rateData.netrate;
								//the source/target carriers are the same
								rateData.source = trgPFnStatesVB[j];
								rateData.target = PFnStates[i];
								int newRType = RESET_SET_BITS_NOASSIGN(rateData.type, (CONTACT_BOTH));	//the calculate bit doens't really matter
								if(rateData.type & CONTACT_SRC)
									newRType = newRType | CONTACT_TRG;
								if(rateData.type & CONTACT_TRG)
									newRType = newRType | CONTACT_SRC;
								rateData.type = newRType;

								if(oneonly == NULL || oneonly == trgPFnStatesVB[j])
									trgPFnStatesVB[j]->AddELevelRate(rateData, false);
							}
						}
					}
					else
					{
						for(unsigned int j=0; j<trgPFnStatesCB.size(); ++j)
						{
							if(trgPFnStatesCB[j]->getPoint().adj.self == EXT)	//this is an created point in line
								rateData.type = RATE_CONTACT_CURRENT_INTEXT;
							else //it is in the model, so an internal current
								rateData.type = RATE_CONTACT_CURRENT_INT;

							rateData.type = rateData.type | outputContactsCB[j];	//store in the flags

							//this rate must now leave the source, so it's the opposite sign
							expandedRate = -rate * trgPFnWeightsCB[j] * trgTotalWeightCB;
							rateData.target = trgPFnStatesCB[j];
							rateData.targetcarrier = ELECTRON;

							if (expandedRate > 0.0)
							{
								rateData.RateIn = expandedRate;
								rateData.RateOut = 0.0;
							}
							else
							{
								rateData.RateIn = 0.0;
								rateData.RateOut = -expandedRate;
							}
							if (expandedRate != 0.0)
							{
								rateData.RateType = rateData.RateOut;
								rateData.desiredTStep = 0.0;
								if(oneonly == NULL || oneonly == PFnStates[i])
									PFnStates[i]->AddELevelRate(rateData, false);


								//now we need to reverse it in the other state for reporting purposes
								double tmp = rateData.RateIn;
								rateData.RateIn = rateData.RateOut;
								rateData.RateOut = tmp;
								rateData.RateType = rateData.RateOut;	//avoid double counting. One will go to zero
								rateData.netrate = -rateData.netrate;
								//the source/target carriers are the same
								rateData.source = trgPFnStatesVB[j];
								rateData.target = PFnStates[i];
								int newRType = RESET_SET_BITS_NOASSIGN(rateData.type, (CONTACT_BOTH));	//the calculate bit doens't really matter
								if(rateData.type & CONTACT_SRC)
									newRType = newRType | CONTACT_TRG;
								if(rateData.type & CONTACT_TRG)
									newRType = newRType | CONTACT_SRC;
								rateData.type = newRType;
								
								if(oneonly == NULL || oneonly == trgPFnStatesVB[j])
									trgPFnStatesVB[j]->AddELevelRate(rateData, false);
							}
						}
					}
				}
			}
			else //it is not a sink. The current can just go into the modifiable contact, and the other stuff will adjust accordingly
			{
				rateData.type = RATE_CONTACT_CURRENT_EXT;
				for (unsigned int i = 0; i<PFnStates.size(); i++)
				{
					double rate = 0.0;
					if (current > 0.0)	//positive charge is entering the point
					{
						if (PFnStates[i]->carriertype == HOLE)
							rate = QI * current * PFnweights[i] * totalWeight;	//totalPos is inversed
						else
							rate = -QI * current * PFnweights[i] * totalWeight;
					}
					else if (current < 0.0) //then negative charge is entering the point
					{
						if (PFnStates[i]->carriertype == HOLE)
							rate = QI * current * PFnweights[i] * totalWeight;	//totalNeg is inversed
						else
							rate = -QI * current * PFnweights[i] * totalWeight;
					}
					rateData.source = PFnStates[i];
					rateData.sourcecarrier = rateData.source->carriertype;
					rateData.netrate = rate;
					if (rate > 0.0)
					{
						rateData.RateIn = rate;
						rateData.RateOut = 0.0;
					}
					else
					{
						rateData.RateIn = 0.0;
						rateData.RateOut = -rate;
					}
					if (rate != 0.0)
					{
						rateData.RateType = rate;
						rateData.desiredTStep = 0.0;
						if(oneonly == NULL || oneonly == PFnStates[i])
							PFnStates[i]->AddELevelRate(rateData, false);
					}
				}
			}
		}
	}

	current = 0.0;
	if (activeVEC != CONTACT_INACTIVE)	//there are electric fields/currents involved
	{
		//in all situations, we should calculate the current based off the external point with the
		//contact.

		//if it's NOT an external contact, then no rates should need to be calculated because
		//psi would have been set when solving poisson's equation to do so
		unsigned int extPtID = 0;


		if (contact->SuppressCurrent() == false)	//then this should be fine.
		{
			std::vector<Point*> trgPt;
			std::vector<int> ctcOut;
			for (unsigned int ptID = 0; ptID<contact->point.size(); ++ptID)	//build the partition function necessary for the contact first
			{
				if (contact->point[ptID] == NULL)
					continue;

				Point* srcPt = contact->point[ptID];
				trgPt.clear();	//just make sure they are empty to start
				ctcOut.clear();	//make sure empty to start!
				int rType = RATE_CONTACT_CURRENT_INT;
				//CalcRateViaContact should return true for only one plane per side of a contact
				//make sure to get all the rates that were excluded because they came would go
				//to a place that's a contact rate
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjMX, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjMX);
					ctcOut.push_back(tmpOutContact);
				}
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjPX, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjPX);
					ctcOut.push_back(tmpOutContact);
				}
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjMY, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjMY);
					ctcOut.push_back(tmpOutContact);
				}
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjPY, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjPY);
					ctcOut.push_back(tmpOutContact);
				}
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjMZ, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjMZ);
					ctcOut.push_back(tmpOutContact);
				}
				tmpOutContact = CalcRateViaContact(srcPt, srcPt->adj.adjPZ, sim->mdesc->Ldimensions);
				if(tmpOutContact & CONTACT_CALCULATE)
				{
					trgPt.push_back(srcPt->adj.adjPZ);
					ctcOut.push_back(tmpOutContact);
				}

				if((active & CONTACT_SINK) == CONTACT_INACTIVE)
				{
					//if it's a contact sink, there is no external point to choose from
					//if not, there might be, and the code still stands
					if(active & CONTACT_CONNECT_EXTERNAL)
					{
						if(sim->mdesc->NDim==1)
						{
							//in 1D, check if it's the exterior. Make sure to include the external point
							if(srcPt->adj.adjMX==NULL || srcPt->adj.adjPX==NULL)
							{
								trgPt.push_back(&(contact->endPtCalcs[ptID]));
								ctcOut.push_back(CONTACT_SRC);	//the edge point should be marked as the contact output
								//there is no link from the target point
							}
						}
						else if(sim->mdesc->NDim == 2)
						{
							if(srcPt->adj.adjMX==NULL || srcPt->adj.adjPX==NULL ||
								srcPt->adj.adjMY==NULL || srcPt->adj.adjPY==NULL)
							{
								trgPt.push_back(&(contact->endPtCalcs[ptID]));
								ctcOut.push_back(CONTACT_SRC);
							}
						}
						else
						{
							if(srcPt->adj.adjMX==NULL || srcPt->adj.adjPX==NULL ||
								srcPt->adj.adjMY==NULL || srcPt->adj.adjPY==NULL || 
								srcPt->adj.adjMZ==NULL || srcPt->adj.adjPZ==NULL)
							{
								trgPt.push_back(&(contact->endPtCalcs[ptID]));
								ctcOut.push_back(CONTACT_SRC);
							}
						}
					}
				}
				for(unsigned int trgId=0; trgId < trgPt.size(); ++trgId)
				{
					if(trgPt[trgId] == &(contact->endPtCalcs[ptID])) //it's the external created point, so INTEXT
						rType = RATE_CONTACT_CURRENT_INTEXT;
					else
						rType = RATE_CONTACT_CURRENT_INT;

					int rTypeSrc = rType | ctcOut[trgId];	//flag to indicate whether source or target contact should have output
					int rTypeTrg = rType;
					if(ctcOut[trgId] & CONTACT_SRC)
						rTypeTrg = rTypeTrg | CONTACT_TRG;
					if(ctcOut[trgId] & CONTACT_TRG)
						rTypeTrg = rTypeTrg | CONTACT_SRC;

					//need to then calculate the rates between all of these as drift diffusion
					std::vector<ELevelRate>::iterator rt;
					if(oneonly == NULL)
					{
						for (std::map<MaxMin<double>, ELevel>::iterator cbSrc = srcPt->cb.energies.begin(); cbSrc != srcPt->cb.energies.end(); ++cbSrc)
						{
							for (std::map<MaxMin<double>, ELevel>::iterator cbTrg = trgPt[trgId]->cb.energies.begin(); cbTrg != trgPt[trgId]->cb.energies.end(); ++cbTrg)
							{
								//calculate the rate in one direction, forced (it would have otherwise cancelled it)
								rt = CalculateCurrentRate(&(cbSrc->second), &(cbTrg->second), sim, RATE_OCC_BEGIN, true);
								if (rt != cbSrc->second.Rates.end())
									rt->type = rTypeSrc;	//signify it is actually a current leaving the device. There is no rate back in, so to speak

								//now the target and source are flipped


								//now do it again from the opposite direction, forced.
								rt = CalculateCurrentRate(&(cbTrg->second), &(cbSrc->second), sim, RATE_OCC_BEGIN, true);
								if (rt != cbTrg->second.Rates.end())
									rt->type = rTypeTrg;	//signify it is actually a current leaving the device. There is no rate back in, so to speak


							}
						}

						for (std::map<MaxMin<double>, ELevel>::iterator vbSrc = srcPt->vb.energies.begin(); vbSrc != srcPt->vb.energies.end(); ++vbSrc)
						{
							for (std::map<MaxMin<double>, ELevel>::iterator vbTrg = trgPt[trgId]->vb.energies.begin(); vbTrg != trgPt[trgId]->vb.energies.end(); ++vbTrg)
							{
								rt = CalculateCurrentRate(&(vbSrc->second), &(vbTrg->second), sim, RATE_OCC_BEGIN, true);
								if (rt != vbSrc->second.Rates.end())
									rt->type = rTypeSrc;	//signify it is actually a current leaving the device. There is no rate back in, so to speak

								//and forced in the opposite direction to get the other currents, but capable of marking Jn/Jp.
								rt = CalculateCurrentRate(&(vbTrg->second), &(vbSrc->second), sim, RATE_OCC_BEGIN, true);
								if (rt != vbTrg->second.Rates.end())
									rt->type = rTypeTrg;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
							}
						}
					}
					else //just one state of interest
					{
						if(&oneonly->getPoint() == srcPt)
						{
							if(oneonly->imp==NULL && oneonly->bandtail==false)
							{
								if(oneonly->carriertype == ELECTRON)
								{
									for (std::map<MaxMin<double>, ELevel>::iterator cbTrg = trgPt[trgId]->cb.energies.begin(); cbTrg != trgPt[trgId]->cb.energies.end(); ++cbTrg)
									{
										//calculate the rate in one direction, forced (it would have otherwise cancelled it)
										rt = CalculateCurrentRate(oneonly, &(cbTrg->second), sim, RATE_OCC_BEGIN, true);
										if (rt != oneonly->Rates.end())
											rt->type = rTypeSrc;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
									}
								}
								else //it is a state in the source valence band
								{

									for (std::map<MaxMin<double>, ELevel>::iterator vbTrg = trgPt[trgId]->vb.energies.begin(); vbTrg != trgPt[trgId]->vb.energies.end(); ++vbTrg)
									{
										rt = CalculateCurrentRate(oneonly, &(vbTrg->second), sim, RATE_OCC_BEGIN, true);
										if (rt != oneonly->Rates.end())
											rt->type = rTypeSrc;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
									}
								}
							}
						}
						else if(&oneonly->getPoint() == trgPt[trgId])
						{
							if(oneonly->imp==NULL && oneonly->bandtail==false)
							{
								if(oneonly->carriertype == ELECTRON)
								{
									for (std::map<MaxMin<double>, ELevel>::iterator cbSrc = srcPt->cb.energies.begin(); cbSrc != srcPt->cb.energies.end(); ++cbSrc)
									{
										//now the target and source are flipped
										//now do it again from the opposite direction, forced.
										rt = CalculateCurrentRate(oneonly, &(cbSrc->second), sim, RATE_OCC_BEGIN, true);
										if (rt != oneonly->Rates.end())
											rt->type = rTypeTrg;	//signify it is actually a current leaving the device. There is no rate back in, so to speak
									}
								}
								else //it is in the valence band
								{
									for (std::map<MaxMin<double>, ELevel>::iterator vbSrc = srcPt->vb.energies.begin(); vbSrc != srcPt->vb.energies.end(); ++vbSrc)
									{
										//and forced in the opposite direction to get the other currents, but capable of marking Jn/Jp.
										rt = CalculateCurrentRate(oneonly, &(vbSrc->second), sim, RATE_OCC_BEGIN, true);
										if (rt != oneonly->Rates.end())
											rt->type = rTypeTrg;	//signify it is actually a current leaving the device. There is no rate back in, so to speak

									}
								}
							}
						}
					}
				}	//end for loop through all potential targets
			}	//end for loop going through all contact source points
			trgPt.clear();	//just make sure they are empty to start
			ctcOut.clear();	//make sure empty to start!
		}
		else //the current was suppressed, so it's not going to match what you'd expect from the band diagram
		{
			//if the current is suppressed, then do the same as extra-dimensional current.
			//a current was specified (or not left open to be calculated), but it did not affect
			//the band structure.  This means that the current does not match reality
			current = 0.0;
			if (active & CONTACT_CUR_DENSITY_LINEAR)
			{
				//we need to convert this to a particular current from the current density
				//this is just a simple area calculation
				double curDensity = contact->GetCurDensity();
				current += curDensity * totalArea.x;
			}
			if(active & CONTACT_CURRENT_LINEAR)
				current += contact->GetCurrent();

			if (current != 0.0)	//the answer is not do nothing :/
			{
				rateData.type = RATE_CONTACT_CURRENT_INT;

				for (unsigned int i = 0; i<PFnStates.size(); i++)
				{
					if(PFnStates[i]->carriertype == HOLE)
					{
						for(unsigned int j=0; j<trgPFnStatesVB.size(); ++j)
						{
							double rate = 0.0;
							if (current > 0.0)	//positive charge is entering the point
								rate = QI * current * PFnweights[i] * totalWeight * trgPFnWeightsVB[j] * trgTotalWeightVB;	//totalPos is inversed
							else if (current < 0.0) //then negative charge is entering the point
								rate = QI * current * PFnweights[i] * totalWeight * trgPFnWeightsVB[j] * trgTotalWeightVB;	//totalNeg is inversed
							rateData.source = PFnStates[i];
							rateData.sourcecarrier = HOLE;
							rateData.target = trgPFnStatesVB[j];
							rateData.targetcarrier = HOLE;
							rateData.netrate = rate;
							if (rate > 0.0)
							{
								rateData.RateIn = rate;
								rateData.RateOut = 0.0;
							}
							else
							{
								rateData.RateIn = 0.0;
								rateData.RateOut = -rate;
							}
							if (rate != 0.0)
							{
								rateData.RateType = rateData.RateOut;
								PFnStates[i]->AddELevelRate(rateData, false);

								//now add a corresponding rate as if it were from the other direction
								//being that only rate type's are tallied, which are RateOut, this will avoid double counting
								double tmp = rateData.RateIn;
								rateData.RateIn = rateData.RateOut;
								rateData.RateOut = tmp;
								rateData.RateType = rateData.RateOut;	//avoid double counting. One will go to zero
								rateData.netrate = -rateData.netrate;
								//the source/target carriers are the same
								rateData.source = trgPFnStatesVB[j];
								rateData.target = PFnStates[i];
								int newRType = RESET_SET_BITS_NOASSIGN(rateData.type, (CONTACT_BOTH));	//the calculate bit doens't really matter
								if(rateData.type & CONTACT_SRC)
									newRType = newRType | CONTACT_TRG;
								if(rateData.type & CONTACT_TRG)
									newRType = newRType | CONTACT_SRC;
								rateData.type = newRType;
								trgPFnStatesVB[j]->AddELevelRate(rateData, false);
							}
						}
					}
					else //the carriertype is an electron
					{
						for(unsigned int j=0; j<trgPFnStatesCB.size(); ++j)
						{

							double rate = 0.0;
							if (current > 0.0)	//positive charge is entering the point
								rate = -QI * current * PFnweights[i] * totalWeight * trgPFnWeightsCB[j] * trgTotalWeightCB;
							else if (current < 0.0) //then negative charge is entering the point
								rate = -QI * current * PFnweights[i] * totalWeight * trgPFnWeightsCB[j] * trgTotalWeightCB;

							rateData.source = PFnStates[i];
							rateData.sourcecarrier = ELECTRON;
							rateData.target = trgPFnStatesCB[j];
							rateData.targetcarrier = ELECTRON;
							rateData.netrate = rate;
							if (rate > 0.0)
							{
								rateData.RateIn = rate;
								rateData.RateOut = 0.0;
							}
							else
							{
								rateData.RateIn = 0.0;
								rateData.RateOut = -rate;
							}
							if (rate != 0.0)
							{
								rateData.RateType = rateData.RateOut;
								PFnStates[i]->AddELevelRate(rateData, false);

								double tmp = rateData.RateIn;
								rateData.RateIn = rateData.RateOut;
								rateData.RateOut = tmp;
								rateData.RateType = rateData.RateOut;	//avoid double counting. One will go to zero
								rateData.netrate = -rateData.netrate;
								//the source/target carriers are the same
								rateData.source = trgPFnStatesCB[j];
								rateData.target = PFnStates[i];
								int newRType = RESET_SET_BITS_NOASSIGN(rateData.type, (CONTACT_BOTH));	//the calculate bit doens't really matter
								if(rateData.type & CONTACT_SRC)
									newRType = newRType | CONTACT_TRG;
								if(rateData.type & CONTACT_TRG)
									newRType = newRType | CONTACT_SRC;
								rateData.type = newRType;
								trgPFnStatesCB[j]->AddELevelRate(rateData, false);
							}	//end if non zero rate
						}	//end for loop through all target CB states
					}	//end if the carrier type is an electron
				}	//end loop through all contact source states
			}	//end that there is a non zero current
		}	//end else indicating the current was suppressed
	}	//end checking if the contact is inactive


	trgPFnStatesCB.clear();
	trgPFnWeightsCB.clear();
	trgPFnStatesVB.clear();
	trgPFnWeightsVB.clear();
	PFnStates.clear();
	PFnweights.clear();
	outputContactsCB.clear();
	outputContactsVB.clear();

	if (UpdateNonContactRates)
		UpdateContactRate(contact, sim);	//update the rates if needed due to their being currents of the INT sort.

	return(0);
}



int ProcessContactLink(Contact* curCtc, ContactLink* link, Simulation* sim, ELevel* oneonly)
{

	if(link->resistance <= 0.0 && link->current == 0.0)	//then there really is no link between the two
		return(0);

	//this needs to take all the energy levels from one contact and calculate rates going to the other contact
	//if there is zero resistance, the fermi levels need to equalize INSTANTLY.
	//if the resistance is negative, then go with a current. As beforehand, a positive current 
	//means that the contact is receiving charge.
	//for resistances: just do I = V/R, and then partition based on availability and carriers present/mobility
	//the V is going to be the difference between the energy levels
	
	std::vector<ELevel*> SourceStates;	//the source states that are linked
	std::vector<double> PFnPosEnterSrcCB;	//holes enter src, e- leave source
	std::vector<double> PFnNegEnterSrcCB;	//e- enter src, holes leave
	std::vector<double> PFnPosEnterSrcVB;	//holes enter src, e- leave source
	std::vector<double> PFnNegEnterSrcVB;	//e- enter src, holes leave
	double PfnPosSrcCBI = 0.0;
	double PfnNegSrcCBI = 0.0;
	double PfnPosSrcVBI = 0.0;
	double PfnNegSrcVBI = 0.0;


	std::vector<ELevel*> TargetStates;	//the target states of what are linked
	std::vector<double> PFnPosEnterTrgCB;
	std::vector<double> PFnNegEnterTrgCB;
	std::vector<double> PFnPosEnterTrgVB;
	std::vector<double> PFnNegEnterTrgVB;
	double PfnPosTrgCBI = 0.0;
	double PfnNegTrgCBI = 0.0;
	double PfnPosTrgVBI = 0.0;
	double PfnNegTrgVBI = 0.0;
	double totcarCBsrc, totcarVBsrc, totcarCBtrg, totcarVBtrg;
	totcarCBsrc= totcarVBsrc= totcarCBtrg= totcarVBtrg=0.0;
	double mobility_partition = 0.0;

	//now need to make a bunch of partition functions
	//and it needs to be done via all the points.

	//loop through all source points
	for(unsigned int ptctr=0; ptctr<curCtc->point.size(); ptctr++)
	{
		Point* pt = curCtc->point[ptctr];
		double mu = (pt->MatProps != NULL) ? pt->MatProps->MuN : 1.0;
		double avail, occ;
		double boltz = KBI / pt->temp;
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = pt->cb.energies.begin(); cbIt != pt->cb.energies.end(); ++cbIt)
		{
			SourceStates.push_back(&(cbIt->second));
			cbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * cbIt->second.boltzeUse;
			PFnPosEnterSrcCB.push_back(occ);	//positive charge entering, so e- leaving. Look at what exists
			PFnNegEnterSrcCB.push_back(avail);	//negative charge entering, so e- entering. Look at what's available
			PFnNegEnterSrcVB.push_back(0.0);	//maintain indices to match state
			PFnPosEnterSrcVB.push_back(0.0);
			PfnPosSrcCBI += occ;
			PfnNegSrcCBI += avail;
			totcarCBsrc += occ;
		}

		
		mu = (pt->MatProps != NULL) ? pt->MatProps->MuP : 1.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = pt->vb.energies.begin(); vbIt != pt->vb.energies.end(); ++vbIt)
		{
			SourceStates.push_back(&(vbIt->second));
			vbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * vbIt->second.boltzeUse;
			PFnNegEnterSrcVB.push_back(occ);	//negative charge entering, so holes leaving. Look at what already is there
			PFnPosEnterSrcVB.push_back(avail);	//positive charge entering, so holes entering. Look where holes can go
			PFnPosEnterSrcCB.push_back(0.0);
			PFnNegEnterSrcCB.push_back(0.0);
			PfnNegSrcVBI += occ;
			PfnPosSrcVBI += avail;
			totcarVBsrc += occ;
		}
	}

	//now do the same thing for the target points
	for(unsigned int ptctr=0; ptctr<link->targetContact->point.size(); ptctr++)
	{
		Point* pt = link->targetContact->point[ptctr];
		double mu = (pt->MatProps != NULL) ? pt->MatProps->MuN : 1.0;
		double avail, occ;
		double boltz = KBI / pt->temp;
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = pt->cb.energies.begin(); cbIt != pt->cb.energies.end(); ++cbIt)
		{
			TargetStates.push_back(&(cbIt->second));
			cbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * cbIt->second.boltzeUse;
			PFnPosEnterTrgCB.push_back(occ);	//positive charge entering, so e- leaving. Look at what exists
			PFnNegEnterTrgCB.push_back(avail);	//negative charge entering, so e- entering. Look at what's available
			PFnNegEnterTrgVB.push_back(0.0);	//maintain indices to match target state
			PFnPosEnterTrgVB.push_back(0.0);
			PfnPosTrgCBI += occ;
			PfnNegTrgCBI += avail;
			totcarCBtrg += occ;
		}

		
		mu = (pt->MatProps != NULL) ? pt->MatProps->MuP : 1.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = pt->vb.energies.begin(); vbIt != pt->vb.energies.end(); ++vbIt)
		{
			TargetStates.push_back(&(vbIt->second));
			vbIt->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			mobility_partition += occ * mu;
			avail = avail * vbIt->second.boltzeUse;
			PFnNegEnterTrgVB.push_back(occ);	//negative charge entering, so holes leaving. Look at what already is there
			PFnPosEnterTrgVB.push_back(avail);	//positive charge entering, so holes entering. Look where holes can go
			PFnPosEnterTrgCB.push_back(0.0);
			PFnNegEnterTrgCB.push_back(0.0);
			PfnNegTrgVBI += occ;
			PfnPosTrgVBI += avail;
			totcarVBtrg += occ;
		}
	}

	PfnNegSrcCBI = (PfnNegSrcCBI > 0.0) ? 1.0 / PfnNegSrcCBI : 0.0;
	PfnPosSrcCBI = (PfnPosSrcCBI > 0.0) ? 1.0 / PfnPosSrcCBI : 0.0;
	PfnNegTrgVBI = (PfnNegTrgVBI > 0.0) ? 1.0 / PfnNegTrgVBI : 0.0;
	PfnPosTrgVBI = (PfnPosTrgVBI > 0.0) ? 1.0 / PfnPosTrgVBI : 0.0;

	PfnNegSrcVBI = (PfnNegSrcVBI > 0.0) ? 1.0 / PfnNegSrcVBI : 0.0;
	PfnPosSrcVBI = (PfnPosSrcVBI > 0.0) ? 1.0 / PfnPosSrcVBI : 0.0;
	PfnNegTrgCBI = (PfnNegTrgCBI > 0.0) ? 1.0 / PfnNegTrgCBI : 0.0;
	PfnPosTrgCBI = (PfnPosTrgCBI > 0.0) ? 1.0 / PfnPosTrgCBI : 0.0;

	double carrierTrgI = (totcarCBtrg > 0.0 || totcarVBtrg > 0.0) ? 1.0 / (totcarCBtrg + totcarVBtrg) : 0.0;
	double carrierSrcI = (totcarCBsrc > 0.0 || totcarVBsrc > 0.0) ? 1.0 / (totcarCBsrc + totcarVBsrc) : 0.0;
	mobility_partition = (mobility_partition > 0.0) ? 1.0 / mobility_partition : 0.0;


	ELevelRate rateData;
	rateData.target = NULL;
	rateData.type = RATE_CONTACT_CURRENT_LINK;
	rateData.deriv = 0.0;
	rateData.deriv2 = 0.0;
	rateData.derivsourceonly = 0.0;

	if(link->resistance <= 0.0)
	{
		unsigned int szSrc= SourceStates.size();
		unsigned int szTrg = TargetStates.size();
		double current = link->current * QI;
		double occSrc, availSrc, occTrg, availTrg, muSrc, muTrg;
		
		//current is rate of POSITIVE charge ENTERING source
		//current is rate of NEGATIVE charge LEAVING source

		for(unsigned int src = 0; src < szSrc; src++)
		{
			if(oneonly != NULL && SourceStates[src] != oneonly)	//the flag has to be set, and it needs to match
				continue;	//to put in the rate

			rateData.source = SourceStates[src];
			rateData.sourcecarrier = SourceStates[src]->carriertype;
			rateData.carrier = rateData.sourcecarrier;

			if(SourceStates[src]->carriertype==ELECTRON)
				muSrc = SourceStates[src]->getPoint().MatProps != NULL ? SourceStates[src]->getPoint().MatProps->MuN : 1.0;
			else
				muSrc = SourceStates[src]->getPoint().MatProps != NULL ? SourceStates[src]->getPoint().MatProps->MuP : 1.0;

			SourceStates[src]->GetOccupancyOcc(RATE_OCC_BEGIN, occSrc, availSrc);

//			DebugCLink(false,  mobility_partition * muSrc * occSrc, true, false);

			for(unsigned int trg = 0; trg < szTrg; trg++)
			{
				if(TargetStates[trg]->carriertype==ELECTRON)
					muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuN : 1.0;
				else
					muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuP : 1.0;
				
				TargetStates[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occTrg, availTrg);

				rateData.target = TargetStates[trg];
				rateData.targetcarrier = TargetStates[trg]->carriertype;

				//But this should be a total sum, so need to partition between
				//all carriers and all availabilities appropriately
				//we know the flow of current direction, so now this is just
				//a game of figuring out signs
				if(SourceStates[src]->carriertype == ELECTRON && TargetStates[trg]->carriertype==ELECTRON)
				{
					//electrons are flowing out of the source into the target
					//assume POSITIVE current, so that would be electrons leaving the source, so ROut positive (negative net rate)
					rateData.RateOut = current * mobility_partition * muSrc * occSrc * (totcarCBtrg * carrierTrgI) * PFnNegEnterTrgCB[trg] * PfnNegTrgCBI;
					//positive current, electrons leaving the target/entering source, so RIn is negative
					rateData.RateIn = -current * mobility_partition * muTrg * occTrg * (totcarCBsrc * carrierSrcI) * PFnNegEnterSrcCB[src] * PfnNegSrcCBI;
					//these will both  result in a net flow of carriers in the same direction
//					DebugCLink(false, PFnNegEnterTrgCB[trg] * PfnNegTrgCBI, false, false);
				}
				else if(SourceStates[src]->carriertype == HOLE && TargetStates[trg]->carriertype==HOLE)
				{
					//holes are flowing out of the source into the target
					//Corresponds with PFnNegEnterSrc & PFnPosEnterTrg
					//assume POSITIVE current, want contribution from ROut to be positive for charge entering
					rateData.RateOut = -current * mobility_partition * muSrc * occSrc * (totcarVBtrg * carrierTrgI) * PFnPosEnterTrgVB[trg] * PfnPosTrgVBI;
					//this is told from the source state, which is a hole, so if there is a positive current, there is an influx
					//of positive charge. This is an increase in holes, so rate in is positive
					rateData.RateIn = current * mobility_partition * muTrg * occTrg * (totcarVBsrc * carrierSrcI) * PFnPosEnterSrcVB[src] * PfnPosSrcVBI;
//					DebugCLink(false, PFnPosEnterTrgVB[trg] * PfnPosTrgVBI, false, false);
					
				}
				else if(SourceStates[src]->carriertype == ELECTRON && TargetStates[trg]->carriertype==HOLE)
				{
					//this is describing electrons leaving the source, so the rate out is appropriate difEf
					rateData.RateOut = current * mobility_partition * muSrc * occSrc * (totcarVBtrg * carrierTrgI) * PFnNegEnterTrgVB[trg] * PfnNegTrgVBI;

					//this is describing holes entering the source (elec), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
					//however, this rate out of holes into the source causes the source to lose carriers, so still need negative
					rateData.RateIn = -current * mobility_partition * muTrg * occTrg * (totcarCBsrc * carrierSrcI) * PFnPosEnterSrcCB[src] * PfnPosSrcCBI;
	//				DebugCLink(false, PFnNegEnterTrgVB[trg] * PfnNegTrgVBI, false, false);
					
				}
				else //holes in src going to electrons in trg
				{
					rateData.RateOut = -current * mobility_partition * muSrc * occSrc * (totcarCBtrg * carrierTrgI) * PFnPosEnterTrgCB[trg] * PfnPosTrgCBI;

					//this is describing electrons entering the source (p), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
					//however, this rate out of e- into the source causes the source to lose carriers, so still need negative
					rateData.RateIn = current * mobility_partition * muTrg * occTrg * (totcarVBsrc * carrierSrcI) * PFnNegEnterSrcVB[src] * PfnNegSrcVBI;
//					DebugCLink(false, PFnPosEnterTrgCB[trg] * PfnPosTrgCBI, false, false);
				}

				rateData.netrate = rateData.RateIn - rateData.RateOut;

				if(rateData.netrate > 0.0)
				{
					rateData.RateIn = rateData.netrate;
					rateData.RateOut = 0.0;
				}
				else
				{
					rateData.RateIn = 0.0;
					rateData.RateOut = -rateData.netrate;
				}

				rateData.RateType = rateData.netrate;
				rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);
				SourceStates[src]->AddELevelRate(rateData, false);

			}	//end for loop through all target states
		}	//end for loop through all source states
	}
	else //then use the resistance to figure out the current instead of some partition functions
	{
		//go through all the potential sources
		unsigned int szSrc= SourceStates.size();
		unsigned int szTrg = TargetStates.size();
		
		double invResist = QI * mobility_partition / link->resistance;
		double occSrc, availSrc, occTrg, availTrg, muSrc, muTrg;
		
		for(unsigned int src = 0; src < szSrc; src++)
		{
			if(oneonly != NULL && SourceStates[src] != oneonly)	//the flag has to be set, and it needs to match
				continue;	//to put in the rate

			if(SourceStates[src]->carriertype==ELECTRON)
				muSrc = SourceStates[src]->getPoint().MatProps != NULL ? SourceStates[src]->getPoint().MatProps->MuN : 1.0;
			else
				muSrc = SourceStates[src]->getPoint().MatProps != NULL ? SourceStates[src]->getPoint().MatProps->MuP : 1.0;
			SourceStates[src]->GetOccupancyOcc(RATE_OCC_BEGIN, occSrc, availSrc);

			rateData.source = SourceStates[src];
			rateData.sourcecarrier = SourceStates[src]->carriertype;
			rateData.carrier = rateData.sourcecarrier;

//			DebugCLink(false,  mobility_partition * muSrc * occSrc, true, false);

			for(unsigned int trg = 0; trg < szTrg; trg++)
			{
				rateData.target = TargetStates[trg];
				rateData.targetcarrier = TargetStates[trg]->carriertype;
				if(TargetStates[trg]->carriertype==ELECTRON)
					muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuN : 1.0;
				else
					muTrg = TargetStates[trg]->getPoint().MatProps != NULL ? TargetStates[trg]->getPoint().MatProps->MuP : 1.0;

				TargetStates[trg]->GetOccupancyOcc(RATE_OCC_BEGIN, occTrg, availTrg);
				double difEf = CalculateDifEf(SourceStates[src], TargetStates[trg]);
				//this is returning positive difEf such that the carrier leaves the source.
				
				//I = V/(Rq), removes coulomb unit so carrier/sec
				//But this should be a total sum, so need to partition between
				//all carriers and all availabilities appropriately
				double rate = 0.0;	//net rate in to the source state

				if(SourceStates[src]->carriertype == ELECTRON && TargetStates[trg]->carriertype==ELECTRON)
				{
					//electrons are flowing out of the source into the target
					rateData.RateOut = difEf * invResist * muSrc * occSrc * (totcarCBtrg * carrierTrgI) * PFnNegEnterTrgCB[trg] * PfnNegTrgCBI;
					rateData.RateIn = -difEf * invResist * muTrg * occTrg * (totcarCBsrc * carrierSrcI) * PFnNegEnterSrcCB[src] * PfnNegSrcCBI;
					//these will both  result in a net flow of carriers in the same direction
//					DebugCLink(false, (totcarCBtrg * carrierTrgI) * PFnNegEnterTrgCB[trg] * PfnNegTrgCBI, false, false);
				}
				else if(SourceStates[src]->carriertype == HOLE && TargetStates[trg]->carriertype==HOLE)
				{
					//holes are flowing out of the source into the target
					//Corresponds with PFnNegEnterSrc & PFnPosEnterTrg
					rateData.RateOut = difEf * invResist * muSrc * occSrc * (totcarVBtrg * carrierTrgI) * PFnPosEnterTrgVB[trg] * PfnPosTrgVBI;
					rateData.RateIn = -difEf * invResist * muTrg * occTrg * (totcarVBsrc * carrierSrcI) * PFnPosEnterSrcVB[src] * PfnPosSrcVBI;
					//negative because rate should be Net Rate In, but difEf is in terms of net rate out
//					DebugCLink(false,  (totcarVBtrg * carrierTrgI) * PFnPosEnterTrgVB[trg] * PfnPosTrgVBI, false, false);
				}
				else if(SourceStates[src]->carriertype == ELECTRON && TargetStates[trg]->carriertype==HOLE)
				{
					//this is describing electrons leaving the source, so the rate out is appropriate difEf
					rateData.RateOut = difEf * invResist * muSrc * occSrc * (totcarVBtrg * carrierTrgI) * PFnNegEnterTrgVB[trg] * PfnNegTrgVBI;

					//this is describing holes entering the source (elec), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
					//however, this rate out of holes into the source causes the source to lose carriers, so still need negative
					rateData.RateIn = -difEf * invResist * muTrg * occTrg * (totcarCBsrc * carrierSrcI) * PFnPosEnterSrcCB[src] * PfnPosSrcCBI;
//					DebugCLink(false, (totcarVBtrg * carrierTrgI) * PFnNegEnterTrgVB[trg] * PfnNegTrgVBI, false, false);
					
				}
				else //holes in src going to electrons in trg
				{
					rateData.RateOut = difEf * invResist * muSrc * occSrc * (totcarCBtrg * carrierTrgI) * PFnPosEnterTrgCB[trg] * PfnPosTrgCBI;

					//this is describing electrons entering the source (p), so the difEf does not actually need to change sign due to opposite difference mattering for rate out
					//however, this rate out of e- into the source causes the source to lose carriers, so still need negative
					rateData.RateIn = -difEf * invResist * muTrg * occTrg * (totcarVBsrc * carrierSrcI) * PFnNegEnterSrcVB[src] * PfnNegSrcVBI;
//					DebugCLink(false, (totcarCBtrg * carrierTrgI) * PFnPosEnterTrgCB[trg] * PfnPosTrgCBI, false, false);
				}

				rateData.netrate = rateData.RateIn - rateData.RateOut;

				//now put the entireity of the rate into either rate in or rate out, as they should
				//be complimentary
				if(rateData.netrate > 0.0)
				{
					rateData.RateIn = rateData.netrate;
					rateData.RateOut = 0.0;
				}
				else
				{
					rateData.RateIn = 0.0;
					rateData.RateOut = -rateData.netrate;
				}

				rateData.RateType = rateData.netrate;
				rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);
				SourceStates[src]->AddELevelRate(rateData, false);
					

			}	//end for loop through all target states
		}	//end for loop through all source states
	}	//end else (resistance to figure out current)

	return(0);
}

bool DetermineEDCurrentFlow(Contact* ctc, int ActiveED)
{
	//returns holes if net positive charge is flowing into the contact and returns electrons
	if(ActiveED == 0)	//there were no extra dimensional currents/efields that matter
		return(false);	//it doesn't really matter, there is no current thaht actually matters

	bool positiveCurrent;
	bool positiveDensity;
	bool positiveEField;
	bool activeCurrent = false;
	bool activeDensity = false;
	bool activeEField = false;

	if(ActiveED & (CONTACT_CUR_DENSITY_COS_ED | CONTACT_CUR_DENSITY_LINEAR_ED))
	{
		activeDensity = true;
		if(ctc->GetCurEDDensity() > 0.0)
			positiveDensity = true;
		else
			positiveDensity = false;
	}
	if(ActiveED & (CONTACT_CURRENT_COS_ED | CONTACT_CURRENT_LINEAR_ED))
	{
		activeCurrent = true;
		if(ctc->GetEDCurrent() > 0.0)
			positiveCurrent = true;
		else
			positiveCurrent = false;
	}
	if(ActiveED & (CONTACT_EFIELD_COS_ED | CONTACT_EFIELD_LINEAR_ED))
	{
		activeEField = true;
		if(ctc->GetEDEField() > 0.0)
			positiveEField = true;
		else
			positiveEField = false;
	}

	//now I just need to go through all the different combinations to test...
	//there are faster ways to do this with if statements that reduce code... but just doing the simple easy way
	if(!activeCurrent && !activeDensity && !activeEField)
	{
		//doesn't matter... should never occur anyway
		ProgressInt(53, ActiveED);
		return(false);
	}
	else if(activeCurrent && !activeDensity && !activeEField)
	{
		if(positiveCurrent)	//positive charge (holes) are flowing in
			return(HOLE);
		else
			return(ELECTRON);
	}
	else if(!activeCurrent && activeDensity && !activeEField)
	{
		if(positiveDensity)	//positive charge (holes) are flowing in
			return(HOLE);
		else
			return(ELECTRON);
	}
	else if(activeCurrent && activeDensity && !activeEField)
	{
		if(positiveCurrent == positiveDensity)	//indicate same charge flowing in
		{
			if(positiveCurrent)	//and one is positive, so they must both be positive
				return(HOLE);
			else
				return(ELECTRON);
		}
		else
		{
			double cDensity = ctc->GetCurEDDensity();
			double area = 0.0;

			for(unsigned int i=0; i<ctc->point.size(); i++)	//build the partition function necessary for the contact first
				area += ctc->point[i]->area.y;	//this will be the xz plane - just keep it simple for now...

			double currentD = cDensity / area;
			double current = ctc->GetEDCurrent();
			double totalCurrent = currentD + current;
			if(totalCurrent > 0.0)	//net positive current, net positive charges entering, so return holes
				return(HOLE);
			else
				return(ELECTRON);

		}
	}
	else if(!activeCurrent && !activeDensity && activeEField)
	{
		if(positiveEField)
			return(HOLE);
		else
			return(ELECTRON);
	}
	else if(activeCurrent && !activeDensity && activeEField)
	{
		//the electric field doesn't matter if the current is specified
		if(positiveCurrent)
			return(HOLE);
		else
			return(ELECTRON);
	}
	else if(!activeCurrent && activeDensity && activeEField)
	{
		//the electric field still doesn't matter because the current desnity overrides it
		if(positiveDensity)
			return(HOLE);
		else
			return(ELECTRON);
	}
	else// if(activeCurrent && activeDensity && activeEField)
	{
		//similar to above...
		if(positiveCurrent == positiveDensity)
		{
			if(positiveCurrent)
				return(HOLE);
			else
				return(ELECTRON);
		}
		else
		{
			double cDensity = ctc->GetCurEDDensity();
			double area = 0.0;

			for(unsigned int i=0; i<ctc->point.size(); i++)	//build the partition function necessary for the contact first
				area += ctc->point[i]->area.y;	//this will be the xz plane - just keep it simple for now...

			double currentD = cDensity / area;
			double current = ctc->GetEDCurrent();
			double totalCurrent = currentD + current;
			if(totalCurrent > 0.0)	//net positive current, net positive charges entering, so return holes
				return(HOLE);
			else
				return(ELECTRON);
		}
	}
	

	return(HOLE);
}

//positive represents the point becoming more positive in charge
int Build_Contact_Current_PFn(std::multimap<MaxMin<double>, ELevel>& band, std::vector<ELevel*>& state, std::vector<double>& weight, double& tot, Point* pt, std::vector<int>* contactOut, int COut)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator elIt = band.begin(); elIt != band.end(); ++elIt)
	{
		ELevel* eLev = &(elIt->second);

		double indivWeightPos = 0.0;
		double indivWeightNeg = 0.0;
		double indivWeight = 0.0;
		double occ, avail;
		if(eLev->carriertype == ELECTRON)
		{
			eLev->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			indivWeight = occ * avail * eLev->boltzeUse * eLev->getPoint().scatterCBConst;
			//occupancy basically pulls in the fermi level, avail keeps from trying to add
			//too much into a full state.
			if(pt->MatProps)	//just be safe for null pointers
				indivWeight = indivWeight * pt->MatProps->MuN;

		}
		else //the carrier type is a hole
		{
			eLev->GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
			indivWeight = occ * avail * eLev->boltzeUse * eLev->getPoint().scatterVBConst;
		
			if(pt->MatProps)	//just be safe for null pointers
				indivWeight = indivWeight * pt->MatProps->MuP;

		}
		tot += indivWeight;
		weight.push_back(indivWeight);
		state.push_back(eLev);
		if(COut != -1 || contactOut != NULL)
			contactOut->push_back(COut);
	}	//end for loop going through the entire band

	
	return(0);
}

int UpdateContactRate(Contact* ctc, Simulation* sim)
{
	//need to come up with a list of rates that must be removed and have the corresponding rate
	//replaced with the opposite values from the contact
	//there is no actual target energy level specified, but convention chosen states that it will
	//choose the point in the positive direction, which would be the contact adjPX point.
	//this point *must* exist, otherwise the appropriate rate would never have been created.
	//so sum up the net electron change and the net hole change, change the signs for the target
	//point
	
	//remove all drift-diffusion rates between the two points on both sides. Going to result in a cancelling rate!
	//then partition the target point appropriately by mobility, availability/number of carriers
	//for e- and holes, then add in the appropriate contact rates that are forced.

	Point* trgPt;
	Point* srcPt;
	std::vector<ELevel*> eLevs;
	std::vector<ELevelRate*> Rates;
	ELevelRate tmpRate;
//	ELevelOpticalRateSort tmpRSort;
	std::vector<std::vector<ELevelRate>::iterator> CancelRates;
	std::vector<std::vector<ELevelRate>::iterator> tmpCancelRates;
	for(unsigned int pt=0; pt<ctc->point.size(); pt++)
	{
		srcPt = ctc->point[pt];
		if(srcPt == NULL)
			continue;	//be safe...
		trgPt = srcPt->adj.adjPX;
		if(trgPt == NULL)
			trgPt = ctc->point[pt]->adj.adjPY;
		if(trgPt == NULL)
			trgPt = ctc->point[pt]->adj.adjPZ;
		if(trgPt == NULL)
			continue;	//continue being safe

		double netHoleEnterSrc = 0.0;
		double netElectronEnterSrc = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = srcPt->cb.energies.begin(); cbIt != srcPt->cb.energies.end(); ++cbIt)
		{
			bool cancelRates = false;
			tmpCancelRates.clear();
			tmpCancelRates.reserve(cbIt->second.Rates.size());	//prevent any memory allocations
			for (std::vector<ELevelRate>::iterator rtIt = cbIt->second.Rates.begin(); rtIt != cbIt->second.Rates.end(); ++rtIt)
			{
				//remove appropriate drift-diffusion rates
				//add appropriate net rate to electrons
				if(rtIt->type == RATE_CONTACT_CURRENT_INT)
				{
					netElectronEnterSrc += rtIt->netrate;
					cancelRates = true;
				}
				else if(rtIt->type == RATE_DRIFTDIFFUSE)
				{ //make sure it is with the correct point
					if(&rtIt->target->getPoint() == trgPt)
					{
						tmpCancelRates.push_back(rtIt);
					}	//end if the rate is going to the correct target point to cancel
				}	//end else if rate drift-diffuse
			}	//end loop for going through all the rates

			if(cancelRates)
				CancelRates.insert(CancelRates.end(), tmpCancelRates.begin(), tmpCancelRates.end());

		}	//end loop through all the conduction band energy levels

		
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = srcPt->vb.energies.begin(); vbIt != srcPt->vb.energies.end(); ++vbIt)
		{
			bool cancelRates = false;
			tmpCancelRates.clear();
			tmpCancelRates.reserve(vbIt->second.Rates.size());	//prevent any memory allocations
			for (std::vector<ELevelRate>::iterator rtIt = vbIt->second.Rates.begin(); rtIt != vbIt->second.Rates.end(); ++rtIt)
			{
				//remove appropriate drift-diffusion rates
				//add appropriate net rate to electrons
				if(rtIt->type == RATE_CONTACT_CURRENT_INT)
				{
					netHoleEnterSrc += rtIt->netrate;
					cancelRates = true;
				}
				else if(rtIt->type == RATE_DRIFTDIFFUSE)
				{ //make sure it is with the correct point
					if(&rtIt->target->getPoint() == trgPt)
					{
						tmpCancelRates.push_back(rtIt);
					}	//end if the rate is going to the correct target point to cancel
				}	//end else if rate drift-diffuse
			}	//end loop for going through all the rates

			if(cancelRates)
				CancelRates.insert(CancelRates.end(), tmpCancelRates.begin(), tmpCancelRates.end());

		}	//end loop through all the valence band energy levels


		if(CancelRates.size() > 0.0)	//we have some work to do...
		{
			std::vector<double> holeProbs;
			std::vector<double> elecProbs;
//			vector<ELevel*> cbStates;
//			vector<ELevel*> vbStates;
			double totalHoleI = 0.0;
			double totalElecI = 0.0;	//total probability
			unsigned int cbSize = trgPt->cb.energies.size();
			unsigned int vbSize = trgPt->vb.energies.size();
//			trgPt->cb.energies.GenerateSortPtr(cbStates);
//			trgPt->vb.energies.GenerateSortPtr(vbStates);
			elecProbs.resize(cbSize);
			holeProbs.resize(vbSize);
			//build the partition function. First loop through all the electron bits.
			double ktI = KBI / trgPt->temp;

			unsigned int ctr = 0;
			//also clear out the rates in the target point
			for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = trgPt->cb.energies.begin(); cbIt != trgPt->cb.energies.end(); ++cbIt)
			{
				if(netElectronEnterSrc > 0.0)	//electrons are entering the source, we are looking at the target
				{   //therefore, electrons are leaving the target and we should look at the number of electrons in the target
					elecProbs[ctr] = cbIt->second.GetBeginOccStates() * trgPt->MatProps->MuN;
					totalElecI += elecProbs[ctr];
				}
				else if(netElectronEnterSrc < 0.0) //electrons are leaving the source, and therefore entering the target.
				{ //therefore, look at the partition of what is available for the electrons to enter as the probability
					elecProbs[ctr] = (cbIt->second.GetNumstates() - cbIt->second.GetBeginOccStates()) * trgPt->MatProps->MuN * exp(-cbIt->second.eUse * ktI);
					totalElecI += elecProbs[ctr];
				}
				else
					elecProbs[ctr] = 0.0;

				for (std::vector<ELevelRate>::iterator rtIt = cbIt->second.Rates.begin(); rtIt != cbIt->second.Rates.end(); ++rtIt)
				{
					if(rtIt->type == RATE_DRIFTDIFFUSE &&
						&rtIt->target->getPoint() == srcPt)	//does this target have a rate going into the contact point
						CancelRates.push_back(rtIt);
				}	//end for loop going through all target energy states (non-contact point)
				ctr++;
			}

			ctr = 0;	//reset the counter for vb states
			for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = trgPt->vb.energies.begin(); vbIt != trgPt->vb.energies.end(); ++vbIt)
			{
				if(netHoleEnterSrc > 0.0)	//holes are entering the source, we are looking at the target
				{   //therefore, holes are leaving the target and we should look at the number of holes in the target
					holeProbs[ctr] = vbIt->second.GetBeginOccStates() * trgPt->MatProps->MuP;
					totalHoleI += holeProbs[ctr];
				}
				else if(netHoleEnterSrc < 0.0) //holes are leaving the source, and therefore entering the target.
				{ //therefore, look at the partition of what is available for the holes to enter as the probability
					holeProbs[ctr] = (vbIt->second.GetNumstates() - vbIt->second.GetBeginOccStates()) * trgPt->MatProps->MuP * exp(-vbIt->second.eUse * ktI);
					totalHoleI += holeProbs[ctr];
				}
				else
					holeProbs[ctr] = 0.0;

				for (std::vector<ELevelRate>::iterator rtIt = vbIt->second.Rates.begin(); rtIt != vbIt->second.Rates.end(); ++rtIt)
				{
					if(rtIt->type == RATE_DRIFTDIFFUSE &&
						&rtIt->target->getPoint() == srcPt)	//does this target have a rate going into the contact point
						CancelRates.push_back(rtIt);
				}	//end for loop going through all target energy states (non-contact point)
				ctr++;
			}
			
			if(totalHoleI > 0.0)	//safe divide by zero and if not, it is automatically put to zero
				totalHoleI = -netElectronEnterSrc / totalHoleI;	//the amount of carriers needed is prob/totalprob * -elecEnterSrc
			if(totalElecI > 0.0)
				totalElecI = -netHoleEnterSrc / totalElecI;


			//now the stage is set to actually cancel all these various rates appropriately.
			//I initially thought I could do things inone sweeping blow, but contact currents are just one rate
			//and the DD is one rate for each target state. There is not a one to once cancellation, and that would be
			//too obnoxious to calculate. So they do have to all be done separately, and then readded in separately.
			unsigned int csize = CancelRates.size();
			//first let's cancel out the drift diffusion rates in the source
			ctr = 0;
			for(unsigned int i=0; i<csize; i++)
			{
				tmpRate.deriv = -CancelRates[i]->deriv;	//need to cancel out all the important stuff
				tmpRate.deriv2 = -CancelRates[i]->deriv2;
				tmpRate.derivsourceonly = -CancelRates[i]->derivsourceonly;
				tmpRate.netrate = -CancelRates[i]->netrate;
				tmpRate.RateIn = -CancelRates[i]->RateIn;
				tmpRate.RateOut = -CancelRates[i]->RateOut;

				tmpRate.carrier = CancelRates[i]->carrier;
				tmpRate.desiredTStep = CancelRates[i]->desiredTStep;
				
				tmpRate.source = CancelRates[i]->source;
				tmpRate.sourcecarrier = CancelRates[i]->sourcecarrier;
				tmpRate.target = CancelRates[i]->target;
				tmpRate.targetcarrier = CancelRates[i]->targetcarrier;
				tmpRate.type = CancelRates[i]->type;
				tmpRate.RateType = -CancelRates[i]->RateType;
				tmpRate.source->AddELevelRate(tmpRate, false);
				//this should have cancelled the DD rate in the source/target and all subsequent calculations

			}	//end for loop through all cancel rates

			//now we need to put the appropriate rates into the target rates

			tmpRate.type = RATE_CONTACT_CURRENT_INT;
			tmpRate.carrier = ELECTRON;
			tmpRate.sourcecarrier = ELECTRON;
			tmpRate.target = NULL;
			tmpRate.deriv = 0.0;
			tmpRate.deriv2 = 0.0;
			tmpRate.derivsourceonly = 0.0;

			ctr = 0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator cbIt = trgPt->cb.energies.begin(); cbIt != trgPt->cb.energies.end(); ++cbIt)
			{
				tmpRate.source = &(cbIt->second);
				tmpRate.netrate = totalElecI * elecProbs[ctr];
				if(tmpRate.netrate > 0.0)
				{
					tmpRate.RateIn = tmpRate.netrate;
					tmpRate.RateOut = 0.0;
				}
				else //netrate is negative...
				{
					tmpRate.RateOut = -tmpRate.netrate;
					tmpRate.RateIn = 0.0;
				}
				tmpRate.RateType = tmpRate.netrate;
				tmpRate.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(tmpRate);
				cbIt->second.AddELevelRate(tmpRate, false);
				ctr++;
			}

			tmpRate.carrier = HOLE;
			tmpRate.sourcecarrier = HOLE;
			ctr = 0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator vbIt = trgPt->vb.energies.begin(); vbIt != trgPt->vb.energies.end(); ++vbIt)
			{
				tmpRate.source = &(vbIt->second);
				tmpRate.netrate = totalHoleI * holeProbs[ctr];
				if(tmpRate.netrate > 0.0)
				{
					tmpRate.RateIn = tmpRate.netrate;
					tmpRate.RateOut = 0.0;
				}
				else //netrate is negative...
				{
					tmpRate.RateOut = -tmpRate.netrate;
					tmpRate.RateIn = 0.0;
				}
				tmpRate.RateType= tmpRate.netrate;
				tmpRate.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(tmpRate);
				vbIt->second.AddELevelRate(tmpRate, false);
				ctr++;
			}	//end loop through all vb states to add in contact rates

		}	//end if cancel rates exist
	}	//end loop through all the additional points
	return(0);
}



Contact::Contact()
{
	electrontime.clear();
	holetime.clear();
	netelectrontime = 0.0;
	netholetime = 0.0;
	netholeiter = neteleciter = 0.0;
	point.clear();
	CLinks.clear();
	PlaneRecPoints.clear();

	voltage = 0.0;
	
	id = 0;
	name = "";
	isolated = false;
	ExternalPoint = NULL;
	freqVoltage = 0.0;
	freqCurrent = 0.0;
	freqEField = 0.0;
	current = 0.0;
	eField = 0.0;
	currentDensity = 0.0;
	current_ED = 0.0;
	currentDensity_ED = 0.0;
	eField_ED = 0.0;
	currentActiveValue = 0;
	
	suppressVoltagePoisson = false;
	suppressCurrentPoisson = false;
	suppressEFieldPoisson = false;
	suppressChargeNeutral = suppressEFieldMinimize = suppressOppDField = false;
	allowOutputMessages = true;
	eField_ED_Leak = 0.0;
	current_ED_Leak = 0.0;
	currentD_ED_Leak = 0.0;
	endPtCalcs.clear();
	JnOutput.clear();
	JpOutput.clear();
	JnOutput.clear();
	JpOutputEnter.clear();
	nEnterOutput = pEnterOutput = 0.0;
	ContactEnvironmentIndex = -1;



}
Contact::~Contact()
{
	electrontime.clear();	//delete all output data concerning current in and out of the contact
	holetime.clear();		//delete all output data concerning current in and out of the contact
	point.clear();		//clear the list of points this contact affects
	
	delete ExternalPoint;
	ExternalPoint = NULL;
	CLinks.clear();
	
	PlaneRecPoints.clear();
	endPtCalcs.clear();	//note, this calls the destructors on each of the internal points, so the memory does get properly cleared


	netelectrontime = 0.0;
	netholetime = 0.0;

	voltage = 0.0;
	id = 0;
}

int Contact::SetThEq(void)
{
	voltage = 0.0;
	freqVoltage = 0.0;
	freqCurrent = 0.0;
	freqEField = 0.0;
	current = 0.0;
	eField = 0.0;
	currentDensity = 0.0;
	current_ED = 0.0;
	currentDensity_ED = 0.0;
	eField_ED = 0.0;
	currentActiveValue = CONTACT_VOLT_LINEAR | CONTACT_SS | CONTACT_SINK;
	suppressVoltagePoisson = false;
	suppressCurrentPoisson = false;
	suppressEFieldPoisson = false;
	allowOutputMessages = true;
	eField_ED_Leak = 0.0;
	current_ED_Leak = 0.0;
	currentD_ED_Leak = 0.0;
	
	nEnterOutput = pEnterOutput = 0.0;


	return(0);
}

int Contact::SetInactive(void)
{
	netelectrontime = 0.0;
	netholetime = 0.0;
	netholeiter = neteleciter = 0.0;

	voltage = 0.0;
	freqVoltage = 0.0;
	freqCurrent = 0.0;
	freqEField = 0.0;
	current = 0.0;
	eField = 0.0;
	currentDensity = 0.0;
	current_ED = 0.0;
	currentDensity_ED = 0.0;
	eField_ED = 0.0;
	currentActiveValue = CONTACT_INACTIVE;
	suppressVoltagePoisson = false;
	suppressCurrentPoisson = false;
	suppressEFieldPoisson = false;
	allowOutputMessages = false;
	eField_ED_Leak = 0.0;
	current_ED_Leak = 0.0;
	currentD_ED_Leak = 0.0;
	nEnterOutput = pEnterOutput = 0.0;

	return(0);
}

int Contact::SetSuppressions(bool voltage, bool current, bool eField, bool eMin, bool eOpp, bool JNeutral, int which)
{
	if (which & SUPPRESS_SET_VOLT)
		suppressVoltagePoisson = voltage;
	if (which & SUPPRESS_SET_CURRENT)
		suppressCurrentPoisson = current;
	if (which & SUPPRESS_SET_EFIELD)
		suppressEFieldPoisson = eField;
	if (which & SUPPRESS_SET_EMIN)
		suppressEFieldMinimize = eMin;
	if (which & SUPPRESS_SET_JNEUTRAL)
		suppressChargeNeutral = JNeutral;
	if (which & SUPPRESS_SET_OPPD)
		suppressOppDField = eOpp;
	return(0);
}

int Contact::AddCLink(ContactLink data)
{
	if (data.targetContact != NULL)
		CLinks[data.targetContact->id] = data;
	else
		return(1);	//signify that is was somehow bad
	return(0);
}

bool Contact::isVoltageActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_VOLT_COS | CONTACT_VOLT_LINEAR));
}

bool Contact::isEFieldActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_EFIELD_COS | CONTACT_EFIELD_LINEAR));
}

bool Contact::isCurrentActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_CURRENT_COS | CONTACT_CURRENT_LINEAR));
}

bool Contact::isCurrentDensityActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_CUR_DENSITY_COS | CONTACT_CUR_DENSITY_LINEAR));
}

bool Contact::isInjectionEFieldActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_EFIELD_COS_ED | CONTACT_EFIELD_LINEAR_ED));
}

bool Contact::isInjectionCurrentActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_CURRENT_COS_ED | CONTACT_CURRENT_LINEAR_ED));
}

bool Contact::isInjectionCurrentDensityActive() {
	return(ANY_SET_BITS_ARE_ONE(currentActiveValue, CONTACT_CUR_DENSITY_COS_ED | CONTACT_CUR_DENSITY_LINEAR_ED));
}



double Contact::GetVoltage(void)
{
	return(voltage);
}



bool Contact::GetCLinks(std::vector<ContactLink*>& CLinkVect)
{
	CLinkVect.clear();
	int sz = CLinks.size();
	if (sz == 0)
		return(false);
	CLinkVect.reserve(sz);	//minimize reallocations

	for (std::map<int, ContactLink>::iterator it = CLinks.begin(); it != CLinks.end(); ++it)
		CLinkVect.push_back(&(it->second));

	return(true);
}


double Contact::GetFrequency(void)
{
	double maxFreqActive = 0.0;
	if (freqVoltage > maxFreqActive)
		maxFreqActive = freqVoltage;
	if (freqCurrent > maxFreqActive)
		maxFreqActive = freqCurrent;
	if (freqEField > maxFreqActive)
		maxFreqActive = freqEField;

	return(maxFreqActive);
}

double Contact::GetVoltageFrequency(void) { return freqVoltage; }
double Contact::GetCurrentFrequency(void) { return freqCurrent; }
double Contact::GetEfieldFrequency(void) { return freqEField; }

void Contact::InitNewIteration(SSContactData* data)
{
	if (data == NULL)
	{
		voltage = current = eField = currentDensity = current_ED = eField_ED = currentDensity_ED = eField_ED_Leak = current_ED_Leak = currentD_ED_Leak = 0.0;
		currentActiveValue = CONTACT_INACTIVE;
	}
	else if (data->ctc != this)
	{
		voltage = current = eField = currentDensity = current_ED = eField_ED = currentDensity_ED = eField_ED_Leak = current_ED_Leak = currentD_ED_Leak = 0.0;
		currentActiveValue = CONTACT_INACTIVE;
		//message of corruption?
	}
	else
	{
		currentActiveValue = data->currentActiveValue;
		voltage = (currentActiveValue & CONTACT_VOLT_LINEAR) ? data->voltage : 0.0;
		current = (currentActiveValue & CONTACT_CURRENT_LINEAR) ? data->current : 0.0;
		eField = (currentActiveValue & CONTACT_EFIELD_LINEAR) ? data->eField : 0.0;
		currentDensity = (currentActiveValue & CONTACT_CUR_DENSITY_LINEAR) ? data->currentDensity : 0.0;
		current_ED = (currentActiveValue & CONTACT_CURRENT_LINEAR_ED) ? data->current_ED : 0.0;
		currentDensity_ED = (currentActiveValue & CONTACT_CUR_DENSITY_LINEAR_ED) ? data->currentDensity_ED : 0.0;
		eField_ED = (currentActiveValue & CONTACT_EFIELD_LINEAR_ED) ? data->eField_ED : 0.0;
		
		SetSuppressions(data->suppressVoltagePoisson, data->suppressCurrentPoisson, data->suppressEFieldPoisson, data->suppressEFMinPoisson, data->suppressOppD, data->suppressConserveJ);
		eField_ED_Leak = (currentActiveValue & CONTACT_EFIELD_LINEAR_ED) ? data->eField_ED_Leak : 0.0;;
		current_ED_Leak = (currentActiveValue & CONTACT_CURRENT_LINEAR_ED) ? data->current_ED_Leak : 0.0;;
		currentD_ED_Leak = (currentActiveValue & CONTACT_CUR_DENSITY_LINEAR_ED) ? data->currentD_ED_Leak : 0.0;;
	}
	
	nEnterOutput = pEnterOutput = 0.0;
	JnOutput.clear();
	JpOutput.clear();
	JnOutput.clear();
	JpOutputEnter.clear();
	DeterminePsiContactPts();	//this sets the value of psi for any prepwork boundary conditions if they are needed

	return;
}

void Contact::InitNewIteration()
{
	
	nEnterOutput = pEnterOutput = 0.0;	//clear the simple output variables
	JnOutput.clear();
	JpOutput.clear();
	JnOutput.clear();
	JpOutputEnter.clear();

	

	return;
}

//this determines psi for the contact points, and then pushes down structure appropriately
int Contact::DeterminePsiContactPts(bool forceValue, double value)
{

	unsigned int szEnd = endPtCalcs.size();
	unsigned int szPt = point.size();
	unsigned int iEnd = 0;
	Point* ctcPt = NULL;
	Point* extPt = NULL;
	int ctcActive = GetCurrentActive();
	XSpace dim;

	if ((ctcActive & CONTACT_ALLBC) == CONTACT_INACTIVE)	//not ED, nor other flags
		return(0);

	for (unsigned int pt = 0; pt < szPt && iEnd < szEnd; ++pt)
	{
		if (point[pt] == NULL)
			continue;

		if (ctcActive & CONTACT_USE_PLANEREC)	//if it's a sink, always go with the point that it interacts with.
		{	//the sink is the end point - always
			extPt = PlaneRecPoints[pt];
		}
		else// if (ctcActive & CONTACT_CONNECT_EXTERNAL)
		{  //but if it connects to the outside world, we want to use endPtCalcs (which should be PlaneRecPoints[extPt])
			extPt = &endPtCalcs[iEnd];
		}
		/*
		else //it is not a sink. It does not connect to the outside world
		{ //if there was a current, it would connect to the external world. This means it is an E-Field
		extPt = &endPtCalcs[iEnd];	//go 'external'
		}
		*/
		/*
		extPt = PlaneRecPoints[pt];	//the point the contact calculates outputs with (this may be endPtCalcs[iEnd])
		if (extPt == NULL)	//if it doesn't want to calculate an output current and it is not a thermal sink, this will be null
		{
		//this is a fabricated point to act as an external contact - it should be the thermally equilibrated equiv of point[pt]
		extPt = &endPtCalcs[iEnd];	//so force it to an existing point - this should line up with pt
		}
		*/
		if (extPt == NULL)	//not really sure what's going on here! It should be impossible to get in this if statement
		{
			++iEnd;
			continue;
		}
		//determine which point to use as a boundary condition
		ctcPt = point[pt];	//the point in the mesh

		double oldPsi = extPt->Psi;

		if ((ctcActive & CONTACT_EFIELD_LINEAR) && SuppressEField() == false)	//this should get priority for band structure
			extPt->Psi = DeterminePsiByEField(extPt, ctcPt, forceValue, value);
		else if ((ctcActive & (CONTACT_CUR_DENSITY_LINEAR | CONTACT_CURRENT_LINEAR)) && SuppressCurrent() == false)	//secondary priority, basically calc EField to get current desired
			extPt->Psi = DeterminePsiByCurrent(extPt, ctcPt, forceValue, value);
		else if ((ctcActive & CONTACT_VOLT_LINEAR) && SuppressVoltage() == false) //there's only a voltage set here, so continue the band diagram
			extPt->Psi = DeterminePsiByVoltage(extPt, ctcPt);	//no change in extPt if contact is sink
		else if ((ctcActive & CONTACT_MINIMIZE_E_ENERGY) && SuppressMinEField() == false)
		{
			double dist;
			int dir = DistancePtsMainCoord(ctcPt, extPt, dist);	//dist>0 equiv to adjPX==NULL: ptExt.pos - curPt.pos
			extPt->Psi = eField * dist + ctcPt->Psi;
			/*
			PoissonPrep Code
			System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
			System->AddTermEquation(eq, otherIndex, disti, EQ_SOLVER_LHS);
			System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
			(Psi_EXT - Psi_CUR) / dist = EFIELD
			*/
		}
		else if ((ctcActive & CONTACT_OPP_DFIELD) && SuppressDFieldOpp() == false)
		{
			double dist;
			int dir = DistancePtsMainCoord(ctcPt, extPt, dist);	//dist>0 equiv to adjPX==NULL: ptExt.pos - curPt.pos
			extPt->Psi = eField * dist + ctcPt->Psi;
			/*
			PoissonPrep Code
			System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
			System->AddTermEquation(eq, otherIndex, disti, EQ_SOLVER_LHS);
			System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
			(Psi_EXT - Psi_CUR) / dist = EFIELD
			*/
		}
		else if ((ctcActive & CONTACT_CONSERVE_CURRENT) && SuppressChargeNeutralCurrent() == false)
		{
			double dist;
			int dir = DistancePtsMainCoord(ctcPt, extPt, dist);	//dist>0 equiv to adjPX==NULL: ptExt.pos - curPt.pos
			extPt->Psi = eField * dist + ctcPt->Psi;
			/*
			PoissonPrep Code
			System->AddTermEquation(eq, RHSAdditional, 1.0, EQ_SOLVER_RHS);
			System->AddTermEquation(eq, otherIndex, disti, EQ_SOLVER_LHS);
			System->AddTermEquation(eq, ptID, -disti, EQ_SOLVER_LHS);
			(Psi_EXT - Psi_CUR) / dist = EFIELD
			*/
		}
		else
		{
			ProgressInt(65, 0);
			++iEnd;
			continue;
		}
		double change = extPt->Psi - oldPsi;
		extPt->cb.bandmin = extPt->Psi - extPt->MatProps->Phi;
		extPt->vb.bandmin = extPt->cb.bandmin - extPt->eg;
		//		extPt->ResetFermiEnergies(false, RATE_OCC_BEGIN,,true);
		extPt->fermi += change;	//I don't think these are even used!
		extPt->fermin += change;
		extPt->fermip += change;

		++iEnd;	//update iteration for the end points in the calculation
	}
	
	return(0);
}

//this is the hard pain in the @$$ one to calculate
double Contact::DeterminePsiByCurrent(Point* extPt, Point* curPt, bool forceValue, double value)
{
	double retPsi = 0.0;
	//the best thing to do here is probably calculate the current between the two points
	//get a minimum deltaEF for changes in velocity that doesn't max out,
	//look at the linear coefficient between them all

	//first, initialize the point to be relatively close to the other
	double dif = curPt->Psi - extPt->Psi;
	double oldPsi = extPt->Psi;
	double ocb = extPt->cb.bandmin;
	double ovb = extPt->vb.bandmin;

	extPt->Psi = curPt->Psi;
	extPt->cb.bandmin = extPt->Psi-extPt->MatProps->Phi;	//just in case not same material type...
	extPt->vb.bandmin = extPt->cb.bandmin - extPt->eg;		//yeah, this is a pain with contact sinks!

	double current;
	if (forceValue)
		current = value;
	else
	{
		current = GetCurDensity();
		current = current * extPt->area.x;
		current += GetCurrent();
	}
	double targetRate;	//what is our net Rate desired of holes entering the external contact point
	//Positive refers to holes flowing in the positive direction
	double dist, disti, widthiEXT, widthiPT;
	int dir = DistancePtsMainCoord(curPt, extPt, dist);
//	XSpace direction = extPt->position - curPt->position;
//	dist = direction.Magnitude();
	dist=fabs(dist);	//dist is +/- for any of the coordinates
	disti = 1.0/dist;
	
	if(dir==PX)
	{
		//so holes flowing left->right, means holes entering the external contact point
		targetRate = current * QI;	//this is our net charge transfer
		widthiEXT = extPt->widthi.x;
		widthiPT = curPt->widthi.x;
//		dist = 2.0 * (curPt->pxpy.x - curPt->position.x);
	}
	else if (dir==NX)	//care about negative direction
	{
		//holes flowing right->left does not match current definition, so negative to get
		//holes flowing into the external contact point
		targetRate = -current * QI;
		widthiEXT = extPt->widthi.x;
		widthiPT = curPt->widthi.x;
//		dist = 2.0 * (curPt->position.x - curPt->mxmy.x);
	}
	else if(dir==PY)
	{
		//so holes flowing left->right, means holes entering the external contact point
		targetRate = current * QI;	//this is our net charge transfer
		widthiEXT = extPt->widthi.y;
		widthiPT = curPt->widthi.y;
//		dist = 2.0 * (curPt->pxpy.x - curPt->position.x);
	}
	else if (dir==NY)	//care about negative direction
	{
		//holes flowing right->left does not match current definition, so negative to get
		//holes flowing into the external contact point
		targetRate = -current * QI;
		widthiEXT = extPt->widthi.y;
		widthiPT = curPt->widthi.y;
//		dist = 2.0 * (curPt->position.x - curPt->mxmy.x);
	}
	else if(dir==PZ)
	{
		//so holes flowing left->right, means holes entering the external contact point
		targetRate = current * QI;	//this is our net charge transfer
		widthiEXT = extPt->widthi.z;
		widthiPT = curPt->widthi.z;
//		dist = 2.0 * (curPt->pxpy.x - curPt->position.x);
	}
	else if (dir==NZ)	//care about negative direction
	{
		//holes flowing right->left does not match current definition, so negative to get
		//holes flowing into the external contact point
		targetRate = -current * QI;
		widthiEXT = extPt->widthi.z;
		widthiPT = curPt->widthi.z;
//		dist = 2.0 * (curPt->position.x - curPt->mxmy.x);
	}
	else
	{
		ProgressInt(64, 0);	//send message to trace file
		return(0.0);
	}
	double RateInExtElectrons = 0.0;
	double RateInExtHoles = 0.0;
	double NRateInExtElec = 0.0;
	double NRateInExtHole = 0.0;
	double RateOutExtElectrons = 0.0;
	double RateOutExtHoles = 0.0;
	double ConstRIEE = 0.0;	//the constant value of rate in external point electrons
	double ConstROEE = 0.0;	//constant value of rate out external point electrons
	double ConstRIEH = 0.0;	//" " " " in " " holes
	double ConstROEH = 0.0;	//" " " " out " " holes
	double LinCoeffRIEE = 0.0;	//the linear value of rate in external point electrons
	double LinCoeffROEE = 0.0;	//constant value of rate out external point electrons
	double LinCoeffRIEH = 0.0;	//" " " " in " " holes
	double LinCoeffROEH = 0.0;	//" " " " out " " holes
//	double maxDifferencePeakVelocityPos = 1.0e20;	//these should be sufficiently high...
//	double maxDifferencePeakVelocityNeg = -1.0e20;
	double prevMDifPVPos = 0.0;
	double prevMDifPVNeg = 0.0;
	double barrierSrcElec = GetBarrierReduction(curPt, extPt, ELECTRON);
	double barrierTrgElec = GetBarrierReduction(extPt, curPt, ELECTRON);
	double barrierSrcHole = GetBarrierReduction(curPt, extPt, HOLE);
	double barrierTrgHole = GetBarrierReduction(extPt, curPt, HOLE);
	//load up al the constants that are gonna be used
	double muNC = curPt->MatProps->MuN;	//these should be the same material! It is just a duplicate
	double muPC = curPt->MatProps->MuP;	//that is thermally equilibrated. Not necessarily with contact sinks!
	double muNEXT = extPt->MatProps->MuN;
	double muPEXT = extPt->MatProps->MuP;
	double scatterNEXT = extPt->scatterCBConst;
	double scatterPEXT = extPt->scatterVBConst;
	double scatterNPT = curPt->scatterCBConst;
	double scatterPPT = curPt->scatterVBConst;
	double vSource, vTarget;	//these get changed for every single calculation
	double EXTOcc, EXTAvail, PTOcc, PTAvail;	//occupancies and availabilities - change with each calculation
	ELevel* EXTeLev;
	ELevel* PTeLev;
	double tmpVal;	//just a temporary double to do some calcs with
	double difEf;
//	double vTestN = dist / muN;	//this seems to be inverse velocity units!
//	double vTestP = dist / muP;	//seems to be inverse velocity units!
	bool constTVelocity = false;
	bool constSVelocity = false;

	//determine the maximum current possible
	//assume the difference is so great all carriers are at their maximum thermal velocity in a single direction
	double maxCurrent = 0.0;
	double deriv = 0.0;
	for (std::map<MaxMin<double>, ELevel>::iterator extIt = extPt->cb.energies.begin(); extIt != extPt->cb.energies.end(); ++extIt)
	{
		EXTeLev = &(extIt->second);
		EXTeLev->GetOccupancyOcc(RATE_OCC_END, EXTOcc, EXTAvail);	//use rate_occ_end because the new values haven't been put into begin yet!
		double occUse = EXTOcc < EXTAvail ? EXTOcc : EXTAvail;
		double vel = EXTeLev->velocity;
		maxCurrent += vel * occUse * widthiEXT;
		deriv += occUse * muNEXT * disti * widthiEXT;
	}
	for (std::map<MaxMin<double>, ELevel>::iterator extIt = extPt->vb.energies.begin(); extIt != extPt->vb.energies.end(); ++extIt)
	{
		EXTeLev = &(extIt->second);
		EXTeLev->GetOccupancyOcc(RATE_OCC_END, EXTOcc, EXTAvail);	//use rate_occ_end because the new values haven't been put into begin yet!
		double occUse = EXTOcc < EXTAvail ? EXTOcc : EXTAvail;
		double vel = EXTeLev->velocity;
		maxCurrent += vel * occUse * widthiEXT;
		deriv += occUse * muPEXT * disti * widthiEXT;
	}
	for (std::map<MaxMin<double>, ELevel>::iterator extIt = curPt->cb.energies.begin(); extIt != curPt->cb.energies.end(); ++extIt)
	{
		PTeLev = &(extIt->second);
		PTeLev->GetOccupancyOcc(RATE_OCC_END, PTOcc, PTAvail);	//use rate_occ_end because the new values haven't been put into begin yet!
		double occUse = PTOcc < PTAvail ? PTOcc : PTAvail;
		double vel = PTeLev->velocity;
		maxCurrent += vel * occUse * widthiEXT;
	}
	for (std::map<MaxMin<double>, ELevel>::iterator extIt = curPt->vb.energies.begin(); extIt != curPt->vb.energies.end(); ++extIt)
	{
		PTeLev = &(extIt->second);
		PTeLev->GetOccupancyOcc(RATE_OCC_END, PTOcc, PTAvail);	//use rate_occ_end because the new values haven't been put into begin yet!
		double occUse = PTOcc < PTAvail ? PTOcc : PTAvail;
		double vel = PTeLev->velocity;
		maxCurrent += vel * occUse * widthiEXT;
	}
	maxCurrent = maxCurrent * QCHARGE;
	if (fabs(current) > maxCurrent)
	{
		if (allowOutputMessages)
		{
			double ratio = fabs(current) / maxCurrent;
			DisplayProgressDoub(22, maxCurrent);	//send message to user that he can see!
			DisplayProgressDoub(23, ratio);
			ProgressDouble(49, maxCurrent);	//and to the trace file to review later
			ProgressDouble(50, ratio);
			allowOutputMessages = false;
		}
		double delEf = targetRate / deriv;	//if the current is positive, holes are entering the external contact.
		extPt->Psi = curPt->Psi + delEf;	//just a rough guideline, but it probably won't work.
		return(extPt->Psi);

	}

	bool repeat = false;
	double totDelta;
	do
	{
		LinCoeffRIEE = 0.0;	//the linear value of rate in external point electrons
		LinCoeffROEE = 0.0;	//constant value of rate out external point electrons
		LinCoeffRIEH = 0.0;	//" " " " in " " holes
		LinCoeffROEH = 0.0;	//" " " " out " " holes
//		maxDifferencePeakVelocityPos = 1.0e20;	//these should be sufficiently high...
//		maxDifferencePeakVelocityNeg = -1.0e20;
		NRateInExtElec = NRateInExtHole = 0.0;
		for (std::map<MaxMin<double>, ELevel>::iterator extIt = extPt->cb.energies.begin(); extIt != extPt->cb.energies.end(); ++extIt)
		{
			EXTeLev = &(extIt->second);
			EXTeLev->GetOccupancyOcc(RATE_OCC_END, EXTOcc, EXTAvail);	//use rate_occ_end because the new values haven't been put into begin yet!
			//this is otherwise calculating with the wrong carriers!
			for (std::map<MaxMin<double>, ELevel>::iterator ptIt = curPt->cb.energies.begin(); ptIt != curPt->cb.energies.end(); ++ptIt)
			{
				PTeLev = &(ptIt->second);
				PTeLev->GetOccupancyOcc(RATE_OCC_END, PTOcc, PTAvail);
				//the source is ext, and the target is pt
				difEf = CalculateDifEf(EXTeLev, PTeLev, RATE_OCC_END, RATE_OCC_END);	//positive for carriers flowing out of source (external pt)


				/*
				Took out max velocity for stability

				tmpVal = PTeLev->velocity * vTestN;	//should be the absolute +/- limit on what the difference in fermi levels can be
				vTarget = tmpVal - difEf;	//this sets the moving upwards bound (using vTarget as temp variable)
				vSource = -tmpVal - difEf;	//and this sets the moving downwards bound (using vSource as temp variable)

				if (vTarget < maxDifferencePeakVelocityPos && vTarget > prevMDifPVPos)	//it's not already a constant
					maxDifferencePeakVelocityPos = vTarget;
				if (vSource > maxDifferencePeakVelocityNeg && vSource < prevMDifPVNeg)	//it is closer to zero
					maxDifferencePeakVelocityNeg = vSource;

				tmpVal = EXTeLev->velocity * vTestN;
				vTarget = tmpVal - difEf;	//this sets the moving upwards bound (using vTarget as temp variable)
				vSource = -tmpVal - difEf;	//and this sets the moving downwards bound (using vSource as temp variable)
				if (vTarget < maxDifferencePeakVelocityPos && vTarget > prevMDifPVPos)	//it's not already a constant
					maxDifferencePeakVelocityPos = vTarget;
				if (vSource > maxDifferencePeakVelocityNeg && vSource < prevMDifPVNeg)	//it is closer to zero
					maxDifferencePeakVelocityNeg = vSource;
				*/

				//positive velocities correspond to a rate out
				//difEf > 0.0 if carriers leave source (external point)
				vTarget = -muNEXT * disti * difEf;	//if difEf>0, means the drift velocity of target is entering, so rate out should be negative
				vSource = muNC * disti * difEf;	//difEf>0, the drift velocity of source is leaving so positive
				constTVelocity = false;
				constSVelocity = false;
				//see how far

				/*  IGNORE MAX VELOCITY!
				if (vTarget > PTeLev->velocity)	//there's a max velocity - doubtful this will ever be reached too often, but current
				{
					vTarget = PTeLev->velocity;	//does saturate when the voltage is over loaded
					constTVelocity = true;
				}
				if (vTarget < -PTeLev->velocity)
				{
					vTarget = -PTeLev->velocity;
					constTVelocity = true;
				}

				if (vSource > EXTeLev->velocity)
				{
					vSource = EXTeLev->velocity;
					constSVelocity = true;
				}
				if (vSource < -EXTeLev->velocity)
				{
					vSource = -EXTeLev->velocity;
					constSVelocity = true;
				}
				*/
				//ok, now time to start adding in some terms
				tmpVal = 0.5*EXTOcc * widthiEXT * PTAvail * PTeLev->boltzeUse * scatterNPT * barrierTrgElec;
				if (!constTVelocity)
					LinCoeffRIEE += tmpVal * muNEXT * disti;	//vdrift looks at Eft-Efs, Efs changing. Two negatives (charge entering is negative)
				tmpVal = tmpVal * vTarget;
				NRateInExtElec += (tmpVal);	//vtarget is for carriers leaving target, therefore positive for carriers entering source (what we care about)
				//				RateOutExtElectrons += tmpVal * vTarget;

				tmpVal = 0.5*PTOcc * widthiPT * EXTAvail * EXTeLev->boltzeUse * scatterNEXT * barrierSrcElec;
				if (!constSVelocity)
					LinCoeffROEE += tmpVal * muNC * disti;	//see above description
				tmpVal = -tmpVal * vSource;
				NRateInExtElec += (tmpVal);	//vSource is for carriers leaving, so negative
				//				RateInExtElectrons += tmpVal * vSource;


			}
		}

		for (std::map<MaxMin<double>, ELevel>::iterator extIt = extPt->vb.energies.begin(); extIt != extPt->vb.energies.end(); ++extIt)
		{
			EXTeLev = &(extIt->second);
			EXTeLev->GetOccupancyOcc(RATE_OCC_END, EXTOcc, EXTAvail);
			for (std::map<MaxMin<double>, ELevel>::iterator ptIt = curPt->vb.energies.begin(); ptIt != curPt->vb.energies.end(); ++ptIt)
			{
				PTeLev = &(ptIt->second);
				PTeLev->GetOccupancyOcc(RATE_OCC_END, PTOcc, PTAvail);
				//the source is ext, and the target is pt
				//				velocity = 0.5*(PTeLev->velocity + EXTeLev->velocity);	//this is the maximum velocity
				difEf = CalculateDifEf(EXTeLev, PTeLev, RATE_OCC_END, RATE_OCC_END);
				/*
				Took out max velocity for stability
				tmpVal = PTeLev->velocity * vTestP;	//should be the absolute +/- limit on what the difference in fermi levels can be
				vTarget = tmpVal - difEf;	//this sets the moving upwards bound (using vTarget as temp variable)
				vSource = -tmpVal - difEf;	//and this sets the moving downwards bound (using vSource as temp variable)

				if (vTarget < maxDifferencePeakVelocityPos && vTarget > 0.0)	//it's not already a constant
					maxDifferencePeakVelocityPos = vTarget;
				if (vSource > maxDifferencePeakVelocityNeg && vSource < 0.0)	//it is closer to zero
					maxDifferencePeakVelocityNeg = vSource;

				tmpVal = EXTeLev->velocity * vTestP;
				vTarget = tmpVal - difEf;	//this sets the moving upwards bound (using vTarget as temp variable)
				vSource = -tmpVal - difEf;	//and this sets the moving downwards bound (using vSource as temp variable)
				if (vTarget < maxDifferencePeakVelocityPos && vTarget > 0.0)	//it's not already a constant
					maxDifferencePeakVelocityPos = vTarget;
				if (vSource > maxDifferencePeakVelocityNeg && vSource < 0.0)	//it is closer to zero
					maxDifferencePeakVelocityNeg = vSource;

*/
				vTarget = -muPEXT * disti * difEf;
				vSource = muPC * disti * difEf;
				constTVelocity = false;
				constSVelocity = false;
				//see how far
				/*
				Having a max velocity seems to make things go bad. Ignore this for now!

				if (vTarget > PTeLev->velocity)	//there's a max velocity - doubtful this will ever be reached too often, but current
				{
					vTarget = PTeLev->velocity;	//does saturate when the voltage is over loaded
					constTVelocity = true;
				}
				if (vTarget < -PTeLev->velocity)
				{
					vTarget = -PTeLev->velocity;
					constTVelocity = true;
				}

				if (vSource > EXTeLev->velocity)
				{
					vSource = EXTeLev->velocity;
					constSVelocity = true;
				}
				if (vSource < -EXTeLev->velocity)
				{
					vSource = -EXTeLev->velocity;
					constSVelocity = true;
				}
				*/
				//ok, now time to start adding in some terms

				tmpVal = 0.5*EXTOcc * widthiEXT * PTAvail * PTeLev->boltzeUse * scatterPPT * barrierTrgHole;
				if (!constTVelocity)
					LinCoeffRIEH += tmpVal * muPEXT * disti;	//now Efs-Eft, where Efs changes,

				//				RateOutExtHoles += tmpVal * vTarget;
				tmpVal = tmpVal * vTarget;
				NRateInExtHole += (tmpVal);

				tmpVal = 0.5*PTOcc * widthiPT * EXTAvail * EXTeLev->boltzeUse * scatterPEXT * barrierSrcHole;
				if (!constSVelocity)
					LinCoeffROEH += tmpVal * muPC * disti;
				tmpVal = -tmpVal * vSource;
				NRateInExtHole += (tmpVal);
				//				RateInExtHoles += tmpVal * vSource;


			}
		}

		//so at this point, the rate in/out of electrons/holes to the external point is calculated,
		//along with how adjusting the band will alter the rates linearly
		//along with where the first discontinuity occurs where it enters into a new linear section

		//our target rate is the total number of holes flowing into the external point
		double totCurRate = DoubleSubtract(NRateInExtHole, NRateInExtElec);
		totDelta = DoubleSubtract(targetRate, totCurRate);	//so if we wanted 100 holes/sec, but were getting 10 h/sec
		//then we would want an additional 90 h/sec.
		double totCoeff = LinCoeffRIEH + LinCoeffROEH + LinCoeffROEE + LinCoeffRIEE;
		//this should result in a non-zero value becuase RIE* and ROE* are the opposite sign, and they are subtracted
		//additionally the two differences should have the same sign because the factors flip with Efs-Eft to Eft-Efs
		if (totDelta == 0.0)	//nailed it
		{
			extPt->cb.bandmin = ocb;
			extPt->vb.bandmin = ovb;
			return(extPt->Psi);
		}
		repeat = false;
		if (totCoeff == 0.0) //if it starts WAY OFF when setting ctc point psi's the same. Shift by large amounts
		{
			ProgressString("Somehow calculated a coefficient of zero for contact current!");
//			return(extPt->Psi);
			if(totDelta < 0.0)	//too many holes are flowing into the external point
			{
				//to decrease the number of holes flowing into the ext point,
				retPsi = -0.1;
				repeat = true;
			}
			else //not enough holes are flowing into the external point
			{
				retPsi = 0.1;
				repeat = true;
			}
		}
		else
			retPsi = totDelta / totCoeff;
		//totDelta was calculated for holes entering.
		//totCoeff was calculated as if Eft-Efs (holes)
		//the source is the external point.
		//if the delta increases, that means Efs went down
		

		if (retPsi > 0.0 /* && retPsi > maxDifferencePeakVelocityPos*/)	//it went beyond a linear regime
		{
//			retPsi = maxDifferencePeakVelocityPos;
			repeat = true;
			extPt->cb.bandmin += retPsi;	//prep for more calculations
			extPt->vb.bandmin += retPsi;
//			prevMDifPVPos = maxDifferencePeakVelocityPos;
		}
		else if (retPsi < 0.0 /*&& retPsi < maxDifferencePeakVelocityNeg*/)
		{
//			retPsi = maxDifferencePeakVelocityNeg;
			repeat = true;
			extPt->cb.bandmin += retPsi;	//prep to repeat calculation
			extPt->vb.bandmin += retPsi;
//			prevMDifPVNeg = maxDifferencePeakVelocityNeg;
		}
		
		if (IsSignificant(extPt->Psi, retPsi))
			extPt->Psi += retPsi;
		else
			repeat=false;	//if it's close enough that no change occurs, well gtfo of this loop

	} while (repeat);

	//so an increased retPsi means knock down the extPtPsi by that amount

	//restore old values so it gets adjusted correctly in the calling function
	extPt->cb.bandmin = ocb;
	extPt->vb.bandmin = ovb;
//	ProgressDouble(53, totDelta);
	return(extPt->Psi);
}

double Contact::DeterminePsiByEField(Point* extPt, Point* ctcPt, bool forceValue, double value)
{
	//if it's a sink, extPt will be inside and the actual external point doesn't matter. Calculating for
	//point on the other side to make sure the EField from the sink into the device is accurate
	//if it's not a sink, this will be needed to determine the current leaving the point via currentout
	if(ctcPt == NULL)
	{
		ProgressInt(64, 0);	//send message to trace file
		return(0.0);
	}
	if(extPt == NULL)
	{
		ProgressInt(64, 0);	//send message to trace file
		return(0.0);
	}
	double retPsi = 0.0;
	double dist;
	int dir = DistancePtsMainCoord(ctcPt, extPt, dist);	//returns the largest magnitude single coordinate distance
	//between two points, where the first point is source and positive means positive direction in x, y, or z
	double psiCur = ctcPt->Psi;
	double EField = forceValue ? value : GetEField();	//note, positive electric field is uphill when going right
	//so (Psi_R - Psi_L)/dist = EFIELD
	//if the external point is to the right, dist is positive and we get the correct value
	retPsi = psiCur + EField * dist;
	
	//if external point to left, dist is negative, and the correct outcome still occures
	
	/*

	if(extPt->adj.self == EXT)	//this is an external point, and is therefore the value of interest
	{
		if (ctcPt->adj.adjMX && ctcPt->adj.adjPX == NULL)	//care about positive direction
		{
			retPsi = EField * dist + psiCur;
			//Psi_R is goal, Psi_L is curPt

		}
		else if (ctcPt->adj.adjPX && ctcPt->adj.adjMX == NULL)	//care about negative direction
		{
			//Psi_L is goal, Psi_R s curPt
			retPsi = -EField * dist + psiCur;
		}
	}
	else
	{
		ProgressInt(64, 0);	//send message to trace file
		return(0.0);
	}
	*/

	return(retPsi);
}

double Contact::DeterminePsiByVoltage(Point* extPt, Point* ctcPt)
{
	//this still matters based on calculating currents - but only when it's not a sink
	//if it is a sink, we don't want the internal point to be modified in any way
	if(currentActiveValue & CONTACT_SS)
	{
		if(currentActiveValue & CONTACT_SINK)
		{
			return(extPt->Psi);	//don't let there be any change
		}
	}
	else if(currentActiveValue & CONTACT_SINK)
		return(extPt->Psi);

	double retPsi = 0.0;
	double rho = ctcPt->rho;
	double eps = (ctcPt->MatProps) ? EPSNOT * double(ctcPt->MatProps->epsR) : EPSNOT;
	double RHS = rho / eps;	//positive because V prop to -Psi
	double distM = 2.0 * (ctcPt->position.x - ctcPt->mxmy.x);	//distance to point on left
	double distP = 2.0 * (ctcPt->pxpy.x - ctcPt->position.x);	//distance to point on right
	double psiAdj;
	double psiCur = ctcPt->Psi;
	bool useLeft = false;
	if (ctcPt->adj.adjMX && ctcPt->adj.adjPX == NULL)
	{
		useLeft = true;
		psiAdj = ctcPt->adj.adjMX->Psi;
	}
	else if (ctcPt->adj.adjPX && ctcPt->adj.adjMX == NULL)
	{
		psiAdj = ctcPt->adj.adjPX->Psi;
	}
	else
	{
		ProgressInt(64, 0);	//send message to trace file
		return(0.0);
	}

	RHS = RHS * 0.5 * (distM + distP);
	RHS = RHS + psiCur / distM + psiCur / distP;
	RHS = useLeft ? RHS - psiAdj / distM : RHS - psiAdj / distP;
	retPsi = useLeft ? RHS*distP : RHS*distM;



	return(retPsi);
}

int LinearTimeDependence::SetValues(double init, double m, double start, double end, double leak, int setflags, bool sumin)
{
	initialValue = init;
	slope = m;
	starttime = start;
	endTime = end;
	percentLeak = leak;
	flags = setflags;
	sum = sumin;
	return(0);
}


bool CosTimeDependence::operator == (const CosTimeDependence& b) const
{
	if (b.A != A)
		return(false);
	if (b.delta != delta)
		return(false);
	if (b.freq != freq)
		return(false);

	return(true);
}

TimeDependence::TimeDependence(double st, double en, int flg, bool sm, double pLeak) :
sum(sm), starttime(st), endTime(en), percentLeak(pLeak), flags(flg)
{ }


CosTimeDependence::CosTimeDependence() : TimeDependence()
{
	A = 0;
	delta = 0;
	freq = 0;
	DC = 0.0;
	
}

CosTimeDependence::CosTimeDependence(double amplitude, double frequency, double del, double direct, double stime, double etime, double leak, bool add, int flg)
{
	initialize(amplitude, frequency, del, direct, stime, etime, leak, add, flg);
}

void CosTimeDependence::initialize(double amplitude, double frequency, double del, double direct, double stime, double etime, double leak, bool add, int flg)
{
	A = amplitude;
	freq = frequency;
	delta = del;
	DC = direct;
	sum = add;
	starttime = stime;
	endTime = etime;
	percentLeak = leak;
	flags = flg;

	return;
}

double CosTimeDependence::GetAmp(void)
{
	return(A);
}

double CosTimeDependence::GetFreq(void)
{
	return(freq);
}

double CosTimeDependence::GetDelta(void)
{
	return(delta);
}

double CosTimeDependence::GetDC(void)
{
	return(DC);
}

double CosTimeDependence::GetValue(double time)
{
	//A cos(2 * PI * frequency * time + delta);
	if (A == 0.0)
		return(DC);
	else if ((freq == 0.0 || time == 0.0) && delta == 0.0)
		return(A + DC);
	else if (freq == 0.0 || time == 0.0)
		return(DC + A * cos(delta));

	//try and cut out some calculations above

	double arg = PI2 * freq * (time - starttime);
	arg += delta;
	arg = cos(arg);

	return(DC + A * arg);
}

int Contact::UpdateSS(SSContactData& data)
{
	if (data.ctc != this)
		return(-1);	//invalid contact link

	currentActiveValue = data.currentActiveValue;
	if (currentActiveValue == CONTACT_INACTIVE)
		return(0);	//nothing else matters. Just get out

	voltage = data.voltage;
	freqVoltage = 0.0;
	freqCurrent = 0.0;
	freqEField = 0.0;
	current = data.current;
	eField = data.eField;
	currentDensity = data.currentDensity;
	current_ED = data.current_ED;
	eField_ED = data.eField_ED;
	currentDensity_ED = data.currentDensity_ED;
	suppressVoltagePoisson = data.suppressVoltagePoisson;
	suppressCurrentPoisson = data.suppressCurrentPoisson;
	suppressEFieldPoisson = data.suppressEFieldPoisson;
	suppressEFieldMinimize = data.suppressEFMinPoisson;
	suppressChargeNeutral = data.suppressConserveJ;
	suppressOppDField = data.suppressOppD;

	eField_ED_Leak = data.eField_ED_Leak;
	current_ED_Leak = data.current_ED_Leak;
	currentD_ED_Leak = data.currentD_ED_Leak;
	allowOutputMessages = true;

	
	return(0);

}

bool Contact::TestRecCurrent(Point* pta, Point* ptb)
{
	int szPt = point.size();	//this should be the same as PlaneRecPoints
	//PlaneRecPoints will only be NULL if no connect_external or contact sink, so no further logic is needed to test this
	for(int i=0; i<szPt; ++i)
	{
		if(pta == point[i] && ptb == PlaneRecPoints[i])
			return(true);
		if(ptb == point[i] && pta == PlaneRecPoints[i])
			return(true);
	}
	return(false);
}

int Contact::UpdatePlaneRecPoints(XSpace dim)
{
	int tmp;
	PlaneRecPoints.clear();
	for(unsigned int j=0; j<point.size(); ++j)
	{
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMX, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMX);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPX, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPX);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMY, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMY);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPY, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPY);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMZ, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMZ);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPZ, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPZ);
			continue;
		}

		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMX2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMX2);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPX2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPX2);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMY2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMY2);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPY2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPY2);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjMZ2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjMZ2);
			continue;
		}
		tmp = CalcRateViaContact(point[j], point[j]->adj.adjPZ2, dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(point[j]->adj.adjPZ2);
			continue;
		}

		tmp = CalcRateViaContact(point[j], &(endPtCalcs[j]), dim);
		if(tmp & CONTACT_CALCULATE)
		{
			PlaneRecPoints.push_back(&(endPtCalcs[j]));
			continue;
		}
		PlaneRecPoints.push_back(NULL);	//there is no point for this one
	}
	return(PlaneRecPoints.size());
}

int Contact::TransferDataToNewContact(Contact* newctc)
{
	if(newctc==NULL)
		return(0);


	for(std::multimap<int, ContactLink>::iterator it = CLinks.begin(); it!= CLinks.end(); ++it)
		newctc->AddCLink(it->second);

	newctc->id = id;
	newctc->ContactEnvironmentIndex = ContactEnvironmentIndex;
	newctc->isolated = isolated;
	newctc->netholetime = netholetime;
	newctc->netelectrontime = netelectrontime;
	newctc->netholeiter = netholeiter;
	newctc->neteleciter = neteleciter;
	newctc->JnOutput = JnOutput;
	newctc->JpOutput = JpOutput;
	newctc->JnOutputEnter = JnOutputEnter;
	newctc->JpOutputEnter = JpOutputEnter;
	newctc->nEnterOutput = nEnterOutput;
	newctc->pEnterOutput = pEnterOutput;
	newctc->point.insert(newctc->point.begin(), point.begin(), point.end());
	newctc->PlaneRecPoints.insert(newctc->PlaneRecPoints.begin(), PlaneRecPoints.begin(), PlaneRecPoints.end());
	for (unsigned int i = 0; i < endPtCalcs.size(); ++i)
		newctc->endPtCalcs.push_back(endPtCalcs[i]);
//	newctc->endPtCalcs.insert(newctc->endPtCalcs.begin(), endPtCalcs.begin(), endPtCalcs.end());
	if (name != "")
		newctc->name = name;

	return(1);
}

bool Contact::IsContactSink(void)
{
	if(currentActiveValue & CONTACT_SS)
	{
		if(currentActiveValue & CONTACT_SINK)
			return(true);	//this does not change ANYTHING
	}
	else if(currentActiveValue & CONTACT_SINK)
		return(true);
	return(false);
}