#include "AdaptiveMesh.h"

#include "light.h"
#include "model.h"
#include "physics.h" //just a bunch of defines - including KB
#include "helperfunctions.h"
#include "PopulateModel.h"
#include "outputdebug.h"
#include "CalcBandStruct.h"
#include "Equilibrium.h"
#include "RateGeneric.h"
#include "contacts.h"
#include "outputdebug.h"
#include "output.h"
#include "srh.h"

/*
This isn't exactly the most efficient code due to all the nested for loops, etc.
However, not very many points should ever be splitting up at a given time.
*/

	/*
	To do:
X	separate the points needing to be split in blocks.
X	For each range, determine the number of points needed - sum the absolute value of the differences between adjacent points
X	Create a set of unpopulated points - the outermost positions can not be changed, but they should still be an acceptable distance away from the adjacent point. - make the sort value the same as the start one to
	help maintain order.
X	Use similar setup to populate model initialization to get all the proper densities, etc within the point
X	For each point, identify the original points it overlaps and add the number of carriers in to each state according to % of old point occupied by new point and carriers in old point

X	Check the contacts and remove bad contact points and select the new contact points. Use populatemodel code.
	
X	Populating the outputs.
X	Currents should all be the same.
X	Point rates are in cm^3/sec, they may the same.
X	Band diagram constants should be copied over the same
X	concentration n/p/rho copy the same
X	num electrons, num holes, impurity charge multiply take % of old point occupied by new point and add it.

X	Actually remove the old points from the model
	
	*/

int AdaptiveMesh(Model& mdl, ModelDescribe& mdesc)
{
	CheckAdjustMesh(mdl, mdesc);	//populate MergePoints and SplitPoints
	bool changed = false;
	std::vector<Point*> AllReplacedPoints;
	AllReplacedPoints.clear();
	
	if(mdl.MergeSplitPoints.size() > 0)
	{
		if(ReplacePoints(mdl, mdesc, mdl.MergeSplitPoints, AllReplacedPoints, true) == POINTSREPLACED)	//true is a flag for merged points
			changed = true;	//need to know if merge because 4 consecutive points are needed in a single layer for the points to merge
	}
/*	
	if(mdl.SplitPoints.size > 0)
	{
		if(ReplacePoints(mdl, mdesc, mdl.SplitPoints, AllReplacedPoints, false) == POINTSREPLACED)	//false is a flag saying not merging points
			changed = true;
	}
*/
	if(changed)
	{
		RenumberPoints(mdl, mdesc, AllReplacedPoints);	//rearrange the mdl.gridp order, delete the old points, update contacts
		DisplayProgress(50, mdl.gridp.size());	//inform user of change
		CalcBandStruct(mdl, mdesc); //recalculate poisson's equation.
		WriteDeviceBand(mdl, mdesc.simulation->OutputFName);
		CheckMesh(mdl, mdesc.simulation->OutputFName);
//		ThermalEquilibrium(mdesc, mdl);	//just to see if it's a screwed up repopulating of points
	}
	return(0);
}

int CheckAdjustMesh(Model& mdl, ModelDescribe& mdesc)
{
	mdl.MergeSplitPoints.clear();

	double SplitCutoff;
	double MergeCutoff;
	bool notifyChange = false;
	
	for(std::map<long int, Point>::iterator testPt = mdl.gridp.begin(); testPt != mdl.gridp.end(); ++testPt)
	{
		//for now, we'll assume it is 1D only.
		SplitCutoff = SPLITPSI * testPt->second.temp;
		MergeCutoff = MERGEPSI * testPt->second.temp;
		

		if(testPt->second.adj.adjMX)
			notifyChange = notifyChange | ComparePsi(mdesc, SplitCutoff, MergeCutoff, &(testPt->second), testPt->second.adj.adjMX, mdl.MergeSplitPoints);
		if(testPt->second.adj.adjPX)
			notifyChange = notifyChange | ComparePsi(mdesc, SplitCutoff, MergeCutoff, &(testPt->second), testPt->second.adj.adjPX, mdl.MergeSplitPoints);
		if(testPt->second.adj.adjMX2)
			notifyChange = notifyChange | ComparePsi(mdesc, SplitCutoff, MergeCutoff, &(testPt->second), testPt->second.adj.adjMX, mdl.MergeSplitPoints);
		if(testPt->second.adj.adjPX2)
			notifyChange = notifyChange | ComparePsi(mdesc, SplitCutoff, MergeCutoff, &(testPt->second), testPt->second.adj.adjPX, mdl.MergeSplitPoints);
	}

	if(notifyChange)
	{
		ProgressInt(51, mdl.MergeSplitPoints.size());
		DisplayProgress(51, mdl.MergeSplitPoints.size());
	}

	return(0);
}

bool CompareLayers(Point* testPt, Point* trgPt, ModelDescribe& mdesc)
{
	unsigned int sz = mdesc.layers.size();

	for(unsigned int i=0; i<sz; i++)	//cycle through the layers and see if the points occupy all the same layers
	{  //if a layer is come across where one point is in it and the other is not, these two points can not be merged
		if(mdesc.layers[i]->InLayer(testPt->position) != mdesc.layers[i]->InLayer(trgPt->position))
			return(false);
	}

	return(true);
}

bool ComparePsi(ModelDescribe& mdesc, double SplitCutoff, double MergeCutoff, Point* testPt, Point* trgPt,  std::map<long int, int>&  MergeSplit)
{
	if(trgPt->contactData || testPt->contactData)	//exempt contacts from being merged or split
		return(false);

	double distance = (testPt->position - trgPt->position).Magnitude();
	double difPsi = fabs(testPt->Psi - trgPt->Psi);

	 std::map<long int, int>::iterator SearchExistTest;
	 std::map<long int, int>::iterator SearchExistTrg;
	 std::map<long int, int>::iterator endMS = MergeSplit.end();

	int useID;
	bool returnchange = false;

	if(difPsi > SplitCutoff && distance > MINPOSITIONCHANGE)	//these points need to be split up
	{
		SearchExistTest = MergeSplit.find(testPt->adj.self);
		SearchExistTrg = MergeSplit.find(trgPt->adj.self);

		if(SearchExistTest==endMS && SearchExistTrg==endMS)	//neither exist, 
		{
			useID = -int(MergeSplit.size());	//make sure it is unique - it's just a number. - negative indicates split
			MergeSplit[testPt->adj.self] = useID;	//create or overwrite
			MergeSplit[trgPt->adj.self] = useID;
			returnchange = true;	//would be redundant, but signify intentional faqlse.
		}
		else if(SearchExistTest!=endMS && SearchExistTrg==endMS)
		{
			useID = SearchExistTest->second;	//add it on to the group of apoints that are already added
			MergeSplit[trgPt->adj.self] = useID;
			returnchange = true;
		}
		else if(SearchExistTest==endMS && SearchExistTrg!=endMS)
		{
			useID = SearchExistTrg->second;
			MergeSplit[testPt->adj.self] = useID;
			returnchange = true;
		}
		else  //both previously exist - probably shouldn't ever happen - but it may once ordering jumps around from adaptive mesh
		{
			if(SearchExistTest->second != SearchExistTrg->second)	//need to combine the two groups
			{
				ReplaceID(SearchExistTest->second, SearchExistTrg->second, MergeSplit);
			}
			returnchange = true;
		}
	}

	if(difPsi < MergeCutoff)	//these points should be merged together
	{
		//fist make sure they are not  contacts... That would wreak havoc. Contacts cannot merge, but they can be split!
		
		if(testPt->contactData || trgPt->contactData)	//either points is a contacts
			return(false);

		if(testPt->MatProps != trgPt->MatProps)	//don't allow merging of points that are different materials...
			return(false);

		SearchExistTest = MergeSplit.find(testPt->adj.self);	//check to make sure it isn't a split!
		SearchExistTrg = MergeSplit.find(trgPt->adj.self);
		if(SearchExistTest!=endMS)
		{
			if(SearchExistTest->second < 0.0)	//then it is a split point, so don't merge
				return(false);
		}
		if(SearchExistTrg!=endMS)	//one is already a part of a split, so don't merge
		{
			if(SearchExistTrg->second < 0.0)	//this one is a split, so don't merge
				return(false);
		}

		if(CompareLayers(testPt, trgPt, mdesc) == false)	//they are not a part of the same layers, so don't allow them to merge
			return(false);

		//no need for a second bit as they are now the same tree, and if they are done, they must merge

		if(SearchExistTest==endMS && SearchExistTrg==endMS)	//neither exist, 
		{
			useID = MergeSplit.size();	//make sure it is unique - it's just a number.
			MergeSplit[testPt->adj.self] = useID;
			MergeSplit[trgPt->adj.self] = useID;
			returnchange = false;	//you need three points to do a merge. This only gives you two.
		}
		else if(SearchExistTest!=endMS && SearchExistTrg==endMS)
		{
			useID = SearchExistTest->second;	//add it on to the group of apoints that are already added
			MergeSplit[trgPt->adj.self] = useID;
			returnchange = true;	//you need three points to do a merge, and this gives you one extra on to an additional
			//but if it starts, it must start in a pair, so that would be three. The exception being if a merge point
			//becomes a split point, at which case it no longer matters because the end result is true anyway
		}
		else if(SearchExistTest==endMS && SearchExistTrg!=endMS)
		{
			useID = SearchExistTrg->second;
			MergeSplit[testPt->adj.self]=useID;
			returnchange = true;
		}
		else  //both previously exist - probably shouldn't ever happen - but it may once ordering jumps around from adaptive mesh
		{
			if(SearchExistTest->second != SearchExistTrg->second)	//need to combine the two groups
			{
				ReplaceID(SearchExistTest->second, SearchExistTrg->second, MergeSplit);
			}
			returnchange = false;	//two previously formed groups merge together, or two that were just checked are now together...
		}
	}

	return(returnchange);
}

int ReplaceID(int FindID, int ReplaceWith, std::map<long int, int>&  MergeSplit)
{
	for(std::map<long int, int>::iterator it = MergeSplit.begin(); it != MergeSplit.end(); ++it)
	{
		if(it->second == FindID)
			it->second = ReplaceWith;
	}
	return(0);
}

int RemovedInvalidReplacedPoints(std::vector<Point*>& AllReplacedPoints, Point* Search)
{
	for (std::vector<Point*>::iterator iter = AllReplacedPoints.begin(); iter < AllReplacedPoints.end(); iter++)
	{
		if(*iter == Search)
		{
			AllReplacedPoints.erase(iter);
			break;
		}
	}
	return(0);
}

int ReplacePoints(Model& mdl, ModelDescribe& mdesc, std::map<long int, int>& BadPoints, std::vector<Point*>& AllReplacedPoints, bool merge)
{
	std::vector<Point*> CurrentRange;
	CurrentRange.reserve(BadPoints.size());
	int UseID;
	XSpace NegBoundary;
	XSpace PosBoundary;
	double totalChange;
	Point* First = NULL;	//first point of all the bad points being replaced (position)
	Point* Last = NULL;		//last point of all the bad points being replaced (position)
	Point* prevPt = NULL;
	Point* CurPt = NULL;
	Point tmpPt(mdl);	//used for adding in the new range of points
	tmpPt.poissonDependency = NULL;	//don't accidentally delete bad data if pointer is initialized to non-NULL.
	tmpPt.Clear();	//make sure it is empty
	double avgTemp;
	double count;
	std::vector<Point*> AddedPoints;
	std::map<long int,Point>::iterator tmpPtrGridp;
	std::vector<std::map<long int, int>::iterator> BPtsVect;	//this is to make sure for loop in populatecurrentrange occurs despite the constant removal of bad points
	std::map<double, int> PointPositions;	//first is sort in position, second is data/flag for type of point
	std::map<double,int> Boundarypositions;	//want them in order
	std::map<double,int>::iterator ptrPtPos;
	std::vector<double> Boundaries;
	std::vector<double> PointPos;
	int retValue = POINTSNOTREPLACED;

	PopulateCurrentRange(mdl, AllReplacedPoints, 0, BadPoints, true);	//builds up list of all points (ptrs) that are being replaced

	while(BadPoints.size() > 0)
	{
		UseID = BadPoints.begin()->second;
		CurrentRange.clear();	//make sure it's empty for this particular range
		PopulateCurrentRange(mdl, CurrentRange, UseID, BadPoints, false);
		if(CurrentRange.size() == 0)	//there are no more left...
			break;

		if(merge && CurrentRange.size() < 5)	//you need at least three points for a merge to work - actually five for two very close together boundaries - points on either side and one in the middle to be removed
		{
			for(unsigned int i=0; i<CurrentRange.size(); i++)
				RemovedInvalidReplacedPoints(AllReplacedPoints, CurrentRange[i]);
			continue;
		}

		retValue = POINTSREPLACED;	//if it gets this far, then points will be replaced
		
		NegBoundary = CurrentRange[0]->position;	//initialization
		PosBoundary = NegBoundary;
		First = CurrentRange[0];
		Last = First;

		unsigned int size = CurrentRange.size();

		for(unsigned int i=1; i<size; i++)	//don't need to go through that first point again!
		{
			if(CurrentRange[i]->position < NegBoundary)	//defaults to x first, then y if x's =, and finally z if x's and y's =
			{
				First = CurrentRange[i];
				NegBoundary = First->position;
			}
			if(CurrentRange[i]->position > PosBoundary)
			{
				Last = CurrentRange[i];
				PosBoundary = Last->position;
			}
		}
		//first should be the furthest over, and all these points should be adjacent to one another!

		//find where the boundaries must be located for a clean split of points
		unsigned int sz = mdesc.layers.size();
		Boundarypositions.clear();
		for(unsigned int i=0; i<sz; i++)
		{
			if(mdesc.layers[i]->shape->min.x < PosBoundary.x && mdesc.layers[i]->shape->min.x > NegBoundary.x)
				Boundarypositions.insert(std::pair<double, int>(mdesc.layers[i]->shape->min.x, 0));

			if(mdesc.layers[i]->shape->max.x < PosBoundary.x && mdesc.layers[i]->shape->max.x > NegBoundary.x)
				Boundarypositions.insert(std::pair<double, int>(mdesc.layers[i]->shape->max.x, 0));
		}

		//make sure there are no duplicates in the list, which would unnecessarily complicate things.
		//tehcnically, there shouldn't be because of keys, but there might be some that are essentially the same due to double accuracy
		{
			std::map<double,int>::iterator prev = Boundarypositions.begin();
			std::map<double,int>::iterator bndit = Boundarypositions.begin();
			if(bndit != Boundarypositions.end())
				bndit++;
			while(bndit != Boundarypositions.end())
			{
				if(DoubleEqual(prev->first, bndit->first))
				{
					Boundarypositions.erase(prev);	//prev is invalidated, and bndit is updated
				}
				prev = bndit;	//so reassign prev in this manner.
				bndit++;
			}
		}	//i just want those variables to go out of scope. This is a  glorified for loop starting at the second entry

		//Note, this assumes 1D in X !!!
		totalChange = 0.0;
		CurPt = First;
		count = 1.0;	//counter for number in avgTemp
		avgTemp = First->temp;
		do
		{
			prevPt = CurPt;
			CurPt = CurPt->adj.adjPX;	//move over one point in space.
			if(CurPt)	//this should ALWAYS be true... but ya never know.
			{
				totalChange += fabs(CurPt->Psi - prevPt->Psi);
				avgTemp += CurPt->temp;
				count += 1.0;
			}
		}while(CurPt != Last && CurPt != NULL);	//still want it to sum on last...
		avgTemp = avgTemp / count;

		double desiredChangePoints = TARGETPSI * avgTemp;	//this is set at roughly 0.2 kT. Deltailed balance impossible if the difference between two points is > kT.
		double distance = (PosBoundary-NegBoundary).Magnitude();

		double newnumpoints = totalChange / desiredChangePoints;
		if(merge)
			newnumpoints = Round(newnumpoints);	//allow to be zero so three points may merge if desired.
		else
			newnumpoints = ceil(newnumpoints);

//		unsigned int intnumpts = (unsigned int)(newnumpoints);
		double distanceBetweenPoints = distance / (newnumpoints + 1.0);	//+1 because can't forget the two halves of the edge points
		PointPositions.clear();
		PointPositions.insert(std::pair<double, int>(NegBoundary.x, EDGEPOINT));	//1 signifies that it can not be deleted
		PointPositions.insert(std::pair<double, int>(PosBoundary.x, EDGEPOINT));
		
		for(double xpos = NegBoundary.x + distanceBetweenPoints; xpos < PosBoundary.x; xpos += distanceBetweenPoints)
		{
			if(DoubleEqual(xpos, PosBoundary.x) == false)	//don't allow xpos to be just barely under PosBoundary due to double inaccuracy
				PointPositions.insert(std::pair<double, int>(xpos, TEMPPOINT));
		}
		
		//now go through the boundaries and remove 1 point on either side of the boundary provided it 1) can be removed,
		
		bool remove = true;
		{
			std::map<double,int>::iterator it = Boundarypositions.begin();
			std::map<double,int>::iterator itprev = it;
			std::map<double,int>::iterator itnext = it;
			if(it != Boundarypositions.end())
				itnext++;
			while(it != Boundarypositions.end())
			{
				remove = false;
				ptrPtPos = MapFindFirstLess(PointPositions, it->first);
				if(ptrPtPos != PointPositions.end())
				{
					if(ptrPtPos->second == TEMPPOINT)
					{
						remove = true;
						if(itprev != Boundarypositions.begin())	//not the first iteration
						{
							if(ptrPtPos->first < itprev->first)	//don't remove points that are beyond the previous boundary
							{
								remove = false;
							}	//if it is beyond that boundary, prevent removal
						}	//test for a previously existing boundary
					}	//end if it is a point classification that can be removed
				}	//end if a point was found

				if(remove)
					PointPositions.erase(ptrPtPos);

				remove = false;	//reset for checking inthe positive x directoin
				ptrPtPos = PointPositions.lower_bound(it->first);	// >= the position
				if(ptrPtPos != PointPositions.end())
				{
					if(ptrPtPos->second == TEMPPOINT)
					{
						remove = true;
						if(itnext != Boundarypositions.begin())	//not the last iteration
						{
							if(ptrPtPos->first > itnext->first)
							{
								remove = false;
							}	//if it is beyond that boundary, prevent removal
						}	//test for a previously existing boundary
					}	//end if it is a point classification that can be removed
				}	//end if a point was found

				if(remove)
					PointPositions.erase(ptrPtPos);

				if(it != Boundarypositions.begin())	//it's not the first time through the loop
					itprev = it;
				if(itnext != Boundarypositions.end())	//the lead may get to the end early
					itnext++;
				it++;	//and do the normal increment
			}	//end while loop
		}	//end iterators staying local
		/*
		for(unsigned int i=0; i<sz; i++)
		{
			remove=false;
			ptrPtPos = PointPositions.FindFirstLess(Boundaries[i]);
			if(ptrPtPos)	//a point exists... check if it should be removed
			{
				if(ptrPtPos->Data == TEMPPOINT)	//it must be a valid point to be taken out, which is only temppoints
				{
					remove = true;
					if(i>0)
					{
						if(ptrPtPos->Sort < Boundaries[i-1])	//don't remove points that are beyond the previous boundary
						{
							remove = false;
						}	//if it is beyond that boundary, prevent removal
					}	//test for a previously existing boundary
				}	//end if it is a point classification that can be removed
			}	//end if a point was found

			if(remove)
				ptrPtPos->Remove(ptrPtPos->Sort, true);

				
			//now check in the positive X direction
			remove = false;
			ptrPtPos = PointPositions.FindFirstGreaterEqual(Boundaries[i]);
			if(ptrPtPos)	//a point exists... check if it should be removed
			{
				if(ptrPtPos->Data == TEMPPOINT)	//it must be a valid point to be taken out, which is only temppoints
				{
					remove = true;
					if(i<sz-1)
					{
						if(ptrPtPos->Sort > Boundaries[i+1])	//don't remove points that are beyond the next boundary
						{
							remove = false;
						}	//if it is beyond that boundary, prevent removal
					}	//test for a previously existing boundary
				}	//end if it is a point classification that can be removed
			}	//end if a point was found

			if(remove)
				ptrPtPos->Remove(ptrPtPos->Sort, true);
		}
		*/


		//now add points around each boundary
		double widthneg;	//how much space is available in the negative direction
		double widthpos;	//how much space is available in the positive direction
		double boundarydesiredefault = 0.5 * distanceBetweenPoints;
		double boundarydesired;

		{
			std::map<double,int>::iterator it = Boundarypositions.begin();
			std::map<double,int>::iterator itprev = it;
			std::map<double,int>::iterator itnext = it;
			if(it != Boundarypositions.end())
				itnext++;
			while(it != Boundarypositions.end())
			{
				if(itprev == it)	//it's the first iteration
					widthneg = it->first - NegBoundary.x;
				else
					widthneg = it->first - itprev->first;

				if(itnext == Boundarypositions.end())
					widthpos = PosBoundary.x - it->first;
				else
					widthpos = itnext->first - it->first;

				if(widthneg < widthpos)	//allows for two points in a given layer selection when divide by 4 that are guaranteed not to overlap
					boundarydesired = 0.25 * widthneg;	//go with the smaller width
				else
					boundarydesired = 0.25 * widthpos;

				if(boundarydesiredefault < boundarydesired)	//the mesh calls for a width of distance between points - go with half as the NegBoundaries are actually points and not boundaries
					boundarydesired = boundarydesiredefault;	//otherwise it would potentially put an overlapping point on.

				PointPositions.insert(std::pair<double, int>(it->first - boundarydesired, BOUNDARYPOINT));
				PointPositions.insert(std::pair<double, int>(it->first + boundarydesired, BOUNDARYPOINT));
				

				//increment on the "for" loop
				if(it != Boundarypositions.begin())	//it's not the first time through the loop
					itprev = it;
				if(itnext != Boundarypositions.end())	//the lead may get to the end early
					itnext++;
				it++;	//and do the normal increment
			}
		}
		/*
		for(unsigned int i=0; i<sz; i++)
		{
			if(i==0)
				widthneg = Boundaries[i] - NegBoundary.x;
			else
				widthneg = Boundaries[i] - Boundaries[i-1];

			if(i==sz-1)
				widthpos = PosBoundary.x - Boundaries[i];
			else
				widthpos = Boundaries[i+1] - Boundaries[i];

			if(widthneg < widthpos)	//allows for two points in a given layer selection when divide by 4 that are guaranteed not to overlap
				boundarydesired = 0.25 * widthneg;	//go with the smaller width
			else
				boundarydesired = 0.25 * widthpos;

			if(boundarydesiredefault < boundarydesired)	//the mesh calls for a width of distance between points - go with half as the NegBoundaries are actually points and not boundaries
				boundarydesired = boundarydesiredefault;	//otherwise it would potentially put an overlapping point on.

			//now insert the points appropriately - this should create a proper mxmy and pxpy
			PointPositions.Insert(BOUNDARYPOINT, Boundaries[i] - boundarydesired);
			PointPositions.Insert(BOUNDARYPOINT, Boundaries[i] + boundarydesired);
		}
		*/
		//and now it's time to actually add the points in
		
		
		AddedPoints.clear();
		AddedPoints.reserve(PointPositions.size());
		
		//first let's get some default values in...
		long int ptID = First->adj.self;	//first recreate the initial point and then use that as a template to push forward
		tmpPt.adj.self = ptID;
		tmpPt.mxmy = Last->mxmy;	//some initializations, mostly for the y/z data
		tmpPt.position = Last->position;
		tmpPt.pxpy = Last->pxpy;
		{
			std::map<double,int>::reverse_iterator it = PointPositions.rbegin();
			std::map<double,int>::reverse_iterator itprev = it;
			std::map<double,int>::reverse_iterator itnext = it;
			std::map<double,int>::reverse_iterator itlast = PointPositions.rend();
			if(it != PointPositions.rend())	//there is at least one entry in the device
			{
				itnext++;
				itlast--;	//it now points to the first element, but as a reverse iterator
			}

			while(it != PointPositions.rend())
			{

				if(itnext == PointPositions.rend())
				{
					tmpPt.mxmy.x = First->mxmy.x;
					tmpPt.position.x = it->first;	//it will be i==0
					tmpPt.pxpy.x = 0.5 * (it->first + itprev->first);	//i==0 + i==1
				}
				else if(it == PointPositions.rbegin())
				{
					tmpPt.mxmy.x = 0.5 * (itnext->first + it->first);
					tmpPt.position.x = it->first;
					tmpPt.pxpy.x = Last->pxpy.x;
				}
				else
				{
					tmpPt.mxmy.x = 0.5 * (itnext->first + it->first);
					tmpPt.position.x = it->first;
					tmpPt.pxpy.x = 0.5 * (itprev->first + it->first);
				}

				tmpPtrGridp = mdl.gridp.insert(std::pair<long int, Point>(ptID, tmpPt)).first;
				AddedPoints.push_back(&(tmpPtrGridp->second));

				if(it != PointPositions.rbegin())
					itprev = it;
				if(itnext != PointPositions.rend())
					itnext++;
				it++;
			}
		}	//end brackets to contain iterators

		/*
		for(unsigned int i=PointPositions.size-1; i<PointPositions.size; i--) //unsigned, so it will go from 0 to large.
		{  //doing it backwards because they are all inserted with the same ID to keep ordering roughly equal
			//but the tree on insert goes left(less) if equal. Add them in reverse order, and the furthest left will be the smallest
			if(i==0)	//pointpositions.size > 1 as there are always the two edge points, so i+1 (1) certainly exists
			{
				tmpPt.mxmy.x = First->mxmy.x;	
				tmpPt.position.x = PointPos[0];
				tmpPt.pxpy.x = 0.5 * (PointPos[0] + PointPos[1]);	//i, i+1
				
			}
			else if(i == PointPositions.size-1)	//same logic as above (this says last one), so i-1 certainly exists
			{
				tmpPt.mxmy.x = 0.5 * (PointPos[i-1] + PointPos[i]);
				tmpPt.position.x = PointPos[i];
				tmpPt.pxpy.x = Last->pxpy.x; //this is redundant, but the code will be clearer to read
			}
			else
			{
				tmpPt.mxmy.x = 0.5 * (PointPos[i-1] + PointPos[i]);
				tmpPt.position.x = PointPos[i];
				tmpPt.pxpy.x = 0.5 * (PointPos[i] + PointPos[i+1]);
				//can't really link points because they don't all exist. It will require a second loop,
				//so just do all the internal links later
			}
		
			tmpPtrGridp = mdl.gridp.insert(pair<long int, Point>(ptID, tmpPt)).first;
			AddedPoints.push_back(&(tmpPtrGridp->second));
			
		}
		*/

		//and link this in with the MX direction, provided it's not the last one linked
		//now fix up the adjacentness of these fellows...
	
		for(unsigned int i=0; i<AddedPoints.size(); i++)
		{
			//do the negative links
			if(i < AddedPoints.size()-1)	//this is not the furthest to the left
			{
				AddedPoints[i]->adj.adjN = AddedPoints[i+1];	//remember, AddedPoints was made in REVERSE order
				AddedPoints[i]->adj.adjMX = AddedPoints[i+1];	//so +1 means to the left
			}
			else  //it is the furthest to the left
			{
				AddedPoints[i]->adj.adjN = First->adj.adjN;
				AddedPoints[i]->adj.adjMX = First->adj.adjMX;
				if(First->adj.adjMX)	//there was a point before this first point, and that point needs to be linked to this one
					First->adj.adjMX->adj.adjPX = AddedPoints[i];	//i=0
				if(First->adj.adjN)
					First->adj.adjN->adj.adjP = AddedPoints[i];
			}

			//do the positive links
			if(i > 0)	//this is NOT the furthest to the right
			{
				AddedPoints[i]->adj.adjP = AddedPoints[i-1];	//remember, addedpoints was made in REVERSE order
				AddedPoints[i]->adj.adjPX = AddedPoints[i-1];	//so -1 means to the right
			}
			else //it is the furthest to the right
			{
				AddedPoints[0]->adj.adjP = Last->adj.adjP;
				AddedPoints[0]->adj.adjPX = Last->adj.adjPX;
				if(Last->adj.adjPX)	//there is a point beyond that needs to be linked in to here
					Last->adj.adjPX->adj.adjMX = AddedPoints[0];
				if(Last->adj.adjP)
					Last->adj.adjP->adj.adjN = AddedPoints[0];
			}
		}

		InitializeNewPoints(mdesc, mdl, AddedPoints, AllReplacedPoints, avgTemp);

	}	//end while bad points are left to process - each iteration is a new range

	return(retValue);
}
	
	

	




int CopyPointData(Point* NewPt, Point* OldPt, Simulation* sim, double pointOverlap)
{
	// this is done initially because of point overlap! NewPt->Output.Allocate(sim->numinbin);
	for(unsigned int i=0; i<sim->binctr; i++)
	{
		NewPt->Output.Psi[i] = OldPt->Output.Psi[i];
		NewPt->Output.Ec[i] = OldPt->Output.Ec[i];
		NewPt->Output.Ev[i] = OldPt->Output.Ev[i];
		NewPt->Output.fermi[i] = OldPt->Output.fermi[i];
		NewPt->Output.fermin[i] = OldPt->Output.fermin[i];
		NewPt->Output.fermip[i] = OldPt->Output.fermip[i];
		NewPt->Output.CarCB[i] = OldPt->Output.CarCB[i] * pointOverlap;
		NewPt->Output.CarVB[i] = OldPt->Output.CarVB[i] * pointOverlap;
		NewPt->Output.n[i] = OldPt->Output.n[i];
		NewPt->Output.p[i] = OldPt->Output.p[i];
		NewPt->Output.rho[i] = OldPt->Output.rho[i];
		NewPt->Output.PointImpCharge[i] = OldPt->Output.PointImpCharge[i] * pointOverlap;
		NewPt->Output.Jnx[i] = OldPt->Output.Jnx[i];
		NewPt->Output.Jpx[i] = OldPt->Output.Jpx[i];
		NewPt->Output.Jny[i] = OldPt->Output.Jny[i];
		NewPt->Output.Jpy[i] = OldPt->Output.Jpy[i];
		NewPt->Output.Jnz[i] = OldPt->Output.Jnz[i];
		NewPt->Output.Jpz[i] = OldPt->Output.Jpz[i];
		NewPt->Output.genth[i] = OldPt->Output.genth[i];
		NewPt->Output.recth[i] = OldPt->Output.recth[i];
		NewPt->Output.R1[i] = OldPt->Output.R1[i];
		NewPt->Output.R2[i] = OldPt->Output.R2[i];
		NewPt->Output.R3[i] = OldPt->Output.R3[i];
		NewPt->Output.R4[i] = OldPt->Output.R4[i];
		NewPt->Output.scatter[i] = OldPt->Output.scatter[i];	
		NewPt->Output.LPowerEnter[i] = OldPt->Output.LPowerEnter[i];	
		NewPt->Output.Ldef[i] = OldPt->Output.Ldef[i];	
		NewPt->Output.Lgen[i] = OldPt->Output.Lgen[i];	
		NewPt->Output.Ldef[i] = OldPt->Output.Ldef[i];	
		NewPt->Output.Lscat[i] = OldPt->Output.Lscat[i];
		NewPt->Output.Edef[i] = OldPt->Output.Edef[i];	
		NewPt->Output.Erec[i] = OldPt->Output.Erec[i];	
		NewPt->Output.Edef[i] = OldPt->Output.Edef[i];	
		NewPt->Output.Escat[i] = OldPt->Output.Escat[i];
		NewPt->Output.LightEmit[i] = OldPt->Output.LightEmit[i];
	}
	NewPt->heat = OldPt->heat;
	NewPt->temp = OldPt->temp;

	return(0);
}

int ZeroOutOccupancy(Point* pt)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->cb.energies.begin(); cbit != pt->cb.energies.end(); ++cbit)
	{
		cbit->second.SetEndOccStates(0.0);
		cbit->second.SetBeginOccStates(0.0);
	}

	for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->vb.energies.begin(); vbit != pt->vb.energies.end(); ++vbit)
	{
		vbit->second.SetEndOccStates(0.0);
		vbit->second.SetBeginOccStates(0.0);
	}

	for(std::multimap<MaxMin<double>,ELevel>::iterator impit = pt->acceptorlike.begin(); impit != pt->acceptorlike.end(); ++impit)
	{
		impit->second.SetEndOccStates(0.0);
		impit->second.SetBeginOccStates(0.0);
	}

	for(std::multimap<MaxMin<double>,ELevel>::iterator impit = pt->donorlike.begin(); impit != pt->donorlike.end(); ++impit)
	{
		impit->second.SetEndOccStates(0.0);
		impit->second.SetBeginOccStates(0.0);
	}

	for(std::multimap<MaxMin<double>,ELevel>::iterator impit = pt->CBTails.begin(); impit != pt->CBTails.end(); ++impit)
	{
		impit->second.SetEndOccStates(0.0);
		impit->second.SetBeginOccStates(0.0);
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator impit = pt->VBTails.begin(); impit != pt->VBTails.end(); ++impit)
	{
		impit->second.SetEndOccStates(0.0);
		impit->second.SetBeginOccStates(0.0);
	}


	return(0);
}

double CalcPointPercentageOverlap(Point* NewPt, Point* OldPt)
{
	XSpace OverlapMin;
	XSpace OverlapMax;
	
	double percentage = 0.0;

	//and do some quick checks to cut out the below calculations if they obviously don't interact.
	if( NewPt->pxpy.x < OldPt->mxmy.x || NewPt->mxmy.x > OldPt->pxpy.x || 
		NewPt->pxpy.y < OldPt->mxmy.y || NewPt->mxmy.y > OldPt->pxpy.y || 
		NewPt->pxpy.z < OldPt->mxmy.z || NewPt->mxmy.z > OldPt->pxpy.z || NewPt->MatProps != OldPt->MatProps)
		return(0.0);

	//take the largest of the minimums and the smallest of the maximums.
	//if there is no overlap, the minimum will be larger than the maximum and the max-min will be negative.
	//therefore, just check for negatives on the delta to see if there is any overlap.

	//first do X
	if(OldPt->mxmy.x > NewPt->mxmy.x)
		OverlapMin.x = OldPt->mxmy.x;
	else
		OverlapMin.x = NewPt->mxmy.x;

	if(OldPt->pxpy.x < NewPt->pxpy.x)
		OverlapMax.x = OldPt->pxpy.x;
	else
		OverlapMax.x = NewPt->pxpy.x;


	//then Y
	if(OldPt->mxmy.y > NewPt->mxmy.y)
		OverlapMin.y = OldPt->mxmy.y;
	else
		OverlapMin.y = NewPt->mxmy.y;

	if(OldPt->pxpy.y < NewPt->pxpy.y)
		OverlapMax.y = OldPt->pxpy.y;
	else
		OverlapMax.y = NewPt->pxpy.y;



	//and now Z
	if(OldPt->mxmy.z > NewPt->mxmy.z)
		OverlapMin.z = OldPt->mxmy.z;
	else
		OverlapMin.z = NewPt->mxmy.z;

	if(OldPt->pxpy.z < NewPt->pxpy.z)
		OverlapMax.z = OldPt->pxpy.z;
	else
		OverlapMax.z = NewPt->pxpy.z;


	XSpace delta;
	delta.x = DoubleSubtract(OverlapMax.x, OverlapMin.x);
	delta.y = DoubleSubtract(OverlapMax.y, OverlapMin.y);
	delta.z = DoubleSubtract(OverlapMax.z, OverlapMin.z);
	if(delta.x <= 0.0 || delta.y <= 0.0 || delta.z <= 0.0)
		return(0.0);
	double Volume = delta.x * delta.y * delta.z;
	percentage = Volume * OldPt->voli;

	if(percentage > 1.0)
		percentage = 1.0;
	if(percentage < 0.0)
		percentage = 0.0;

	return(percentage);
}

//presumably, these are going to be the same material, so they should have very similar carrier outputs, but they might
//not quite line up...

//they must be the same band/impurity/energies/material
double CheckELevOverlap(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt)
{
	if(NewELev->carriertype != OldELev->carriertype)
		return(0.0);

	if(NewELev->imp != OldELev->imp)
		return(0.0);

	if(NewPt->MatProps != OldPt->MatProps)
		return(0.0);

	double percentOverlap = 0.0;
	double min,max;
	double widthOldi = OldELev->max - OldELev->min;
	if(widthOldi > 0.0)
		widthOldi = 1.0 / widthOldi;
	else
		widthOldi = 0.0;

	//grab the lower max
	if(OldELev->max < NewELev->max)
		max = OldELev->max;
	else
		max = NewELev->max;

	//and the higher min
	if(OldELev->min > NewELev->min)
		min = OldELev->min;
	else
		min = NewELev->min;

	if(max == min && min == OldELev->max && OldELev->max == OldELev->min)	//they're all equal
		percentOverlap = 1.0;
	else
		percentOverlap = DoubleSubtract(max,min) * widthOldi;

	if(percentOverlap > 1.0)
		percentOverlap = 1.0;
	else if(percentOverlap < 0.0)
		percentOverlap = 0.0;

	return(percentOverlap);
}

int CopyELevel(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt, double pointOverlap)
{
	double EOverlap = CheckELevOverlap(NewELev, OldELev, NewPt, OldPt);
	if(EOverlap > 0.0 && EOverlap <= 1.0)	//accept valid ranges
	{
		double AmountTransfer = OldELev->GetEndOccStates() * pointOverlap * EOverlap;
		NewELev->AddEndOccStates(AmountTransfer);	//this will prevent adding over the number of states allowed

		AmountTransfer = OldELev->GetBeginOccStates() * pointOverlap * EOverlap;
		NewELev->AddBeginOccStates(AmountTransfer);	//this will prevent adding over the number of states allowed
	}

	return(0);
}


int InitializeNewPoints(ModelDescribe& mdesc, Model& mdl, std::vector<Point*> CurrentPoints, std::vector<Point*> ReplacedPoints, double temp)
{
	int vacID = 0;	//index of the vacuum material
	double kT = KB * temp;
	double beta = kT;
	if(beta != 0)
		beta = 1.0 / beta;
	else
		beta = 1e20;	//just a big number!

	unsigned int numinbin = mdesc.simulation->outputs.numinbin;	//used in for loop for points when creating output data.

	if(numinbin <= 0)	//needs at least one
		numinbin = 1;

	mdesc.simulation->Vol = 0.0;
	vacID = VacuumMaterial(mdesc);	//create a vacuum material if one does not exist and add it to mdesc with ID -1
	unsigned int size = CurrentPoints.size();
	unsigned int rsize = ReplacedPoints.size();
	std::vector<ELevel*> CurELevl;
	std::vector<ELevel*> RepELevl;
	std::vector<ELevel*> tmpELevel;
	std::vector<double> TotOverlap;
	std::vector<double> AbsFermi;
	std::vector<double> Rhos;
	unsigned int iter;
	double pointOverlap;
	unsigned int ecurSize;
	unsigned int erepSize;
	int numlevels;
	double TotPtOverlap;
	double avgTemp;
	double dist;	//for figuring out the closest point
	double tmp;	//a temporary double
	XSpace distancePoints;
	Point* Closest;
	Point* Left;
	Point* Middle;
	Point* Right;
	bool linear = false;
	double avgCharge;	//probably easier to add up the charge and volume than anything else
	double totVol;
	double totalCarriers;
	double chargeChange, difRho, percentDiffer;

	for(unsigned int cur=0; cur<size; cur++)	//loop through all the points
	{
		CurrentPoints[cur]->Populate(vacID, mdesc.simulation->outputs.numinbin, beta);	//now the current point has all it's proper impurities, defects, materials, etc. loaded into it.
		//contact points are even loaded... :/
		ZeroOutOccupancy(CurrentPoints[cur]);
		avgCharge = 0.0;
		totVol = 0.0;
		Rhos.clear();
		

		CurrentPoints[cur]->Output.Allocate(mdesc.simulation->outputs.numinbin);	//make sure the outputs are set appropriately
		numlevels = CurrentPoints[cur]->cb.energies.size() + CurrentPoints[cur]->vb.energies.size() + CurrentPoints[cur]->CBTails.size()
			+ CurrentPoints[cur]->VBTails.size() + CurrentPoints[cur]->acceptorlike.size() + CurrentPoints[cur]->donorlike.size();
		CurELevl.resize(numlevels);
		TotOverlap.resize(numlevels);
		AbsFermi.resize(numlevels);
		
		//this creates entries for all energy levels.
		iter=0;	//reset the iteration counter for assigning them
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = CurrentPoints[cur]->cb.energies.begin(); cbit != CurrentPoints[cur]->cb.energies.end(); ++cbit)
		{
			CurELevl[iter] = &(cbit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}

		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = CurrentPoints[cur]->vb.energies.begin(); vbit != CurrentPoints[cur]->vb.energies.end(); ++vbit)
		{
			CurELevl[iter] = &(vbit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->acceptorlike.begin(); impit != CurrentPoints[cur]->acceptorlike.end(); ++impit)
		{
			CurELevl[iter] = &(impit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->donorlike.begin(); impit != CurrentPoints[cur]->donorlike.end(); ++impit)
		{
			CurELevl[iter] = &(impit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->CBTails.begin(); impit != CurrentPoints[cur]->CBTails.end(); ++impit)
		{
			CurELevl[iter] = &(impit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->VBTails.begin(); impit != CurrentPoints[cur]->VBTails.end(); ++impit)
		{
			CurELevl[iter] = &(impit->second);
			AbsFermi[iter] = 0.0;
			TotOverlap[iter] = 0.0;
			iter++;
		}

		ecurSize = CurELevl.size();
		TotPtOverlap = 0.0;
		avgTemp = 0.0;
		dist = mdesc.Ldimensions.x;
		Closest = NULL;

		for(unsigned int rep=0; rep<rsize; rep++)
		{
			distancePoints = CurrentPoints[cur]->position - ReplacedPoints[rep]->position;
			tmp = distancePoints.Magnitude();
			if(tmp < dist)
			{
				dist = tmp;
				Closest = ReplacedPoints[rep];
			}
			pointOverlap = CalcPointPercentageOverlap(CurrentPoints[cur], ReplacedPoints[rep]);
			if(pointOverlap > 0.0)
			{
				//now create a vector of all the energies in the old point
				Rhos.push_back(ReplacedPoints[rep]->rho);
				iter=0;
				RepELevl.resize(ReplacedPoints[rep]->cb.energies.size() + ReplacedPoints[rep]->vb.energies.size() + ReplacedPoints[rep]->CBTails.size() + ReplacedPoints[rep]->VBTails.size() + ReplacedPoints[rep]->acceptorlike.size() + ReplacedPoints[rep]->donorlike.size());
				//this creates entries for them all.
				
				for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = ReplacedPoints[rep]->cb.energies.begin(); cbit != ReplacedPoints[rep]->cb.energies.end(); ++cbit)
				{
					RepELevl[iter] = &(cbit->second);
					iter++;
				}

				
				for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = ReplacedPoints[rep]->vb.energies.begin(); vbit != ReplacedPoints[rep]->vb.energies.end(); ++vbit)
				{
					RepELevl[iter] = &(vbit->second);
					iter++;
				}

				for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->acceptorlike.begin(); impit != CurrentPoints[cur]->acceptorlike.end(); ++impit)
				{
					RepELevl[iter] = &(impit->second);
					iter++;
				}

				for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->donorlike.begin(); impit != CurrentPoints[cur]->donorlike.end(); ++impit)
				{
					RepELevl[iter] = &(impit->second);
					iter++;
				}

				for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->CBTails.begin(); impit != CurrentPoints[cur]->CBTails.end(); ++impit)
				{
					RepELevl[iter] = &(impit->second);
					iter++;
				}

				for(std::multimap<MaxMin<double>,ELevel>::iterator impit = CurrentPoints[cur]->VBTails.begin(); impit != CurrentPoints[cur]->VBTails.end(); ++impit)
				{
					RepELevl[iter] = &(impit->second);
					iter++;
				}
				
				
				
				erepSize = RepELevl.size();

				for(unsigned int ecur = 0; ecur < ecurSize; ecur++)	//cycle through all energy levels in the current point
				{
					for(unsigned int erep = 0; erep < erepSize; erep++)	//and all energy levels in the previous point
					{
						CopyELevel(CurELevl[ecur], RepELevl[erep], CurrentPoints[cur], ReplacedPoints[rep], pointOverlap);	//safeguard in case avg. fermi doesn't work
						GetAvgFermi(CurELevl[ecur], RepELevl[erep], CurrentPoints[cur], ReplacedPoints[rep], pointOverlap, AbsFermi[ecur], TotOverlap[ecur]);
					}
				}	//end for loop to copy all the energy level data over

				CopyPointData(CurrentPoints[cur], ReplacedPoints[rep], mdesc.simulation, pointOverlap);
				avgTemp += ReplacedPoints[rep]->temp * pointOverlap;
				TotPtOverlap += pointOverlap;
				avgCharge += ReplacedPoints[rep]->rho * ReplacedPoints[rep]->vol * pointOverlap;	//used to make sure the end result
				totVol += ReplacedPoints[rep]->vol * pointOverlap;	//is close to the proper rho - that way the band diagram doesn't go to hell
			}	//end copy point data over
		}

		if(TotPtOverlap > 0.0)
			avgTemp = avgTemp / TotPtOverlap;
		else
			avgTemp = mdesc.simulation->T;

		double kbTi = KBI / avgTemp;

		//now need to get the best data I can for figuring out what Psi should be and shift the bands!
		linear = false;
		if(Closest->adj.adjMX && Closest->adj.adjPX)
		{
			Left = Closest->adj.adjMX;
			Right = Closest->adj.adjPX;
			Middle = Closest;
		}
		else if(Closest->adj.adjMX)
		{
			if(Closest->adj.adjMX->adj.adjMX)
			{
				Right = Closest;
				Middle = Right->adj.adjMX;
				Left = Middle->adj.adjMX;
			}
			else
			{
				linear = true;
				Middle = Closest;
				Left= Closest->adj.adjMX;
				Right = Left;
			}
		}
		else if(Closest->adj.adjPX)
		{
			if(Closest->adj.adjPX->adj.adjPX)
			{
				Left = Closest;
				Middle = Closest->adj.adjPX;
				Right = Middle->adj.adjPX;
			}
			else
			{
				linear = true;
				Middle = Closest;
				Left = Closest->adj.adjPX;
				Right = Left;
			}
		}
		else //there are no other points that are good
		{
			linear = true;
			Middle = Closest;
			Left = Closest;
			Right = Closest;
		}

		if(linear)
		{
			if(Left == Middle) //there is only one point. This is awful. Just go with that point's data as best as possible
			{
				double dif = Middle->Psi - CurrentPoints[cur]->Psi;	//want currentPonts[cur]->Psi to move to Middle->Psi
				CurrentPoints[cur]->Psi += dif;	//cur = cur-cur + middle
				if(CurrentPoints[cur]->MatProps)
				{
					CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi - CurrentPoints[cur]->MatProps->Phi;
					CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->cb.bandmin - CurrentPoints[cur]->eg;
				}
				else
				{
					CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi;
					CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->Psi;
				}
	//			CurrentPoints[cur]->fermi += dif;
	//			CurrentPoints[cur]->fermin += dif;
	//			CurrentPoints[cur]->fermip += dif;
			}
			else
			{
				double width = (Middle->position - Left->position).Magnitude();
				double difPsi = Left->Psi - Middle->Psi;
				double distMid = (CurrentPoints[cur]->position - Middle->position).Magnitude();
				double dif = difPsi * distMid / width;
				CurrentPoints[cur]->Psi = Middle->Psi + dif;
				if(CurrentPoints[cur]->MatProps)
				{
					CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi - CurrentPoints[cur]->MatProps->Phi;
					CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->cb.bandmin - CurrentPoints[cur]->eg;
				}
				else
				{
					CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi;
					CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->Psi;
				}
	//			CurrentPoints[cur]->fermi += dif;
	//			CurrentPoints[cur]->fermin += dif;
	//			CurrentPoints[cur]->fermip += dif;
			}
		}
		else //taylor series, 3 points. 2nd derivative, f(middle)
		{
			double widthL = (Middle->position - Left->position).Magnitude();
			double widthR = (Middle->position - Right->position).Magnitude();
			double width = 0.5 * (widthL + widthR);
			double difL = Middle->Psi - Left->Psi;
			double difR = Right->Psi - Middle->Psi;
			double dist = CurrentPoints[cur]->position.x - Middle->position.x;	//assuming 1D in X, though the above code doesn't
			double firstDeriv = difL / widthL;
			double secondDeriv = (difR/widthR - firstDeriv)/width;
			CurrentPoints[cur]->Psi = Middle->Psi + firstDeriv * dist + 0.5 * secondDeriv * dist * dist;
//			double dif = CurrentPoints[cur]->Psi -  Middle->Psi;	//now dif is the change from middle to current
			if(CurrentPoints[cur]->MatProps)
			{
				CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi - CurrentPoints[cur]->MatProps->Phi;
				CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->cb.bandmin - CurrentPoints[cur]->eg;
			}
			else
			{
				CurrentPoints[cur]->cb.bandmin = CurrentPoints[cur]->Psi;
				CurrentPoints[cur]->vb.bandmin = CurrentPoints[cur]->Psi;
			}
	//		CurrentPoints[cur]->fermi += dif;
	//		CurrentPoints[cur]->fermin += dif;
	//		CurrentPoints[cur]->fermip += dif;
		
		}

		//for the current point, it has now obtained an average fermi for each
		totalCarriers = 0.0;
		for(unsigned int ecur = 0; ecur < ecurSize; ecur++)
		{
			if(TotOverlap[ecur] > 0.0)
			{
				bool oppOcc;
				CurELevl[ecur]->AbsFermiEnergy = AbsFermi[ecur] / TotOverlap[ecur];
				double occ = CalcOccByFermiBand(CurELevl[ecur], CurELevl[ecur]->AbsFermiEnergy, kbTi, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin, oppOcc);
				if(oppOcc==false)
				{
					CurELevl[ecur]->SetBeginOccStates(occ);
					CurELevl[ecur]->SetEndOccStates(occ);
					if(CurELevl[ecur]->imp == NULL)	//impurities are too likely to be close to the limits and would skew the charge, potentially significantly.
						totalCarriers += occ;
				}
				else
				{
					CurELevl[ecur]->SetBeginOccStatesOpposite(occ);
					CurELevl[ecur]->SetEndOccStatesOpposite(occ);
					if(CurELevl[ecur]->imp == NULL)	//impurities are too likely to be close to the limits and would skew the charge, potentially significantly.
						totalCarriers += (CurELevl[ecur]->GetNumstates()-occ);
				}
			}
		}

		CurrentPoints[cur]->ResetData();	//put things in rho, num particles, etc. appropriately.
		CalculateRho(*(CurrentPoints[cur]));
		//now verify everything...
		avgCharge = avgCharge / totVol;	//it was a weighted average, so remove the weighting. This is the rho we want to maintain
		difRho = avgCharge - CurrentPoints[cur]->rho;
		chargeChange = difRho * CurrentPoints[cur]->vol * QI;
		percentDiffer = fabs(chargeChange / totalCarriers);

		if(percentDiffer < 0.2)	//it is close to the correct value, so just adjust it to get the proper rho
		{
			if(chargeChange > 0.0)	//we need to add carriers to the valence band and take them away from the conduction band
			{
				for(unsigned int ecur = 0; ecur < ecurSize; ecur++)
				{
					if(TotOverlap[ecur] > 0.0 && CurELevl[ecur]->imp == NULL)
					{
						double change = CurELevl[ecur]->GetBeginOccStates() * percentDiffer;
						if(CurELevl[ecur]->carriertype == HOLE)
						{
							CurELevl[ecur]->AddBeginOccStates(change);
							CurELevl[ecur]->AddEndOccStates(change);
						}	//end check if holes should be added
						else
						{
							CurELevl[ecur]->RemoveBeginOccStates(change);
							CurELevl[ecur]->RemoveEndOccStates(change);
						}	//end check for if electrons should be removed
					}	//end test to make sure this elevel matters? It was needed above, so I presume it's needed here
				}
			}
			else if(chargeChange < 0.0)	//we need to add carriers to the CB and take them away from the VB
			{
				for(unsigned int ecur = 0; ecur < ecurSize; ecur++)
				{
					if(TotOverlap[ecur] > 0.0 && CurELevl[ecur]->imp == NULL)
					{
						double change = CurELevl[ecur]->GetBeginOccStates() * percentDiffer;
						if(CurELevl[ecur]->carriertype == ELECTRON)
						{
							CurELevl[ecur]->AddBeginOccStates(change);
							CurELevl[ecur]->AddEndOccStates(change);
						}	//end check if holes should be added
						else
						{
							CurELevl[ecur]->RemoveBeginOccStates(change);
							CurELevl[ecur]->RemoveEndOccStates(change);
						}	//end check for if electrons should be removed
					}	//end test to make sure this elevel matters? It was needed above, so I presume it's needed here
				}
			}
		}
		else
		{
		//	double occ = CalcOccByFermiBand(CurELevl[ecur], CurELevl[ecur]->AbsFermiEnergy, kbTi, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin);
			CurrentPoints[cur]->ResetFermiEnergies(false, RATE_OCC_END, mdesc.simulation->accuracy, true);	//make sure the fermi levels are appropriate

			double Ef = CurrentPoints[cur]->fermi;
			double difEfnEf = CurrentPoints[cur]->fermin - Ef;	//maintain same distances between all the different fermi levels
			double difEfpEf = CurrentPoints[cur]->fermip - Ef;
			double maxEf = CurrentPoints[cur]->Psi;
			double minEf= CurrentPoints[cur]->vb.bandmin - CurrentPoints[cur]->MatProps->Phi;
//			double currentCharge = CurrentPoints[cur]->rho;
			double chargeImp, chargeN, chargeP, charge;
			
			double kbTI = KBI / CurrentPoints[cur]->temp;
			double QVoli = QCHARGE * CurrentPoints[cur]->voli;
			bool oppOcc;	//set in CalcOccByFermiBand
						
			do
			{
				Ef = 0.5 * (maxEf + minEf);
				chargeImp = chargeN = chargeP = 0.0;
					//get the amount of charge resulting from all the electrons and holes in the band diagram.

				for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = CurrentPoints[cur]->cb.energies.begin(); cbit != CurrentPoints[cur]->cb.energies.end(); ++cbit)
				{
					tmp = CalcOccByFermiBand(&(cbit->second), Ef + difEfnEf, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeN += tmp;
					else
						chargeN += (cbit->second.GetNumstates()-tmp);
				}
				for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = CurrentPoints[cur]->vb.energies.begin(); vbit != CurrentPoints[cur]->vb.energies.end(); ++vbit)
				{
					tmp = CalcOccByFermiBand(&(vbit->second), Ef + difEfpEf, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeP += tmp;
					else
						chargeP += (vbit->second.GetNumstates()-tmp);
				}
				for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->acceptorlike.begin(); esit != CurrentPoints[cur]->acceptorlike.end(); ++esit)
				{
					tmp = CalcOccByFermiBand(&(esit->second), Ef, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeImp += tmp;
					else
						chargeImp += (esit->second.GetNumstates()-tmp);
				}
				for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->donorlike.begin(); esit != CurrentPoints[cur]->donorlike.end(); ++esit)
				{
					tmp = CalcOccByFermiBand(&(esit->second), Ef, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeImp -= tmp;
					else
						chargeImp -= (esit->second.GetNumstates()-tmp);
				}
				for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->CBTails.begin(); esit != CurrentPoints[cur]->CBTails.end(); ++esit)
				{
					tmp = CalcOccByFermiBand(&(esit->second), Ef, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeImp -= tmp;
					else
						chargeImp -= (esit->second.GetNumstates()-tmp);
				}
				for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->VBTails.begin(); esit != CurrentPoints[cur]->VBTails.end(); ++esit)
				{
					tmp = CalcOccByFermiBand(&(esit->second), Ef, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
					if(oppOcc==false)
						chargeImp += tmp;
					else
						chargeImp += (esit->second.GetNumstates()-tmp);
				}

				charge = chargeP - chargeN + (CurrentPoints[cur]->impdefcharge - chargeImp);
				charge = charge * QVoli;	//this converts it into rho. Note: avgCharge is actually a rho. I was stupid in my naming...

				if(charge == avgCharge)
				{
					break;
				}
				else if(charge > avgCharge)	//there is too much positive charge, we need more negative charge, which means we need a higher fermi level
				{
					minEf = Ef;
				}
				else
				{
					maxEf = Ef;
				}
				
			}while(!DoubleEqual(maxEf,minEf));
			
			CurrentPoints[cur]->fermi = Ef;
			CurrentPoints[cur]->fermin = Ef + difEfnEf;
			CurrentPoints[cur]->fermip = Ef + difEfpEf;

			double occupancy;
			CurrentPoints[cur]->cb.SetNumParticles(0.0);
			for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = CurrentPoints[cur]->cb.energies.begin(); cbit != CurrentPoints[cur]->cb.energies.end(); ++cbit)
			{
				occupancy = CalcOccByFermiBand(&(cbit->second), CurrentPoints[cur]->fermin, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					cbit->second.SetEndOccStates(occupancy);
					cbit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					cbit->second.SetEndOccStatesOpposite(occupancy);
					cbit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy	=cbit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->cb.AddNumParticles(occupancy);
				SetabsEUseFromFermi(&(cbit->second), CurrentPoints[cur]->fermin);
			}
			CurrentPoints[cur]->n = CurrentPoints[cur]->cb.GetNumParticles();

			CurrentPoints[cur]->vb.SetNumParticles(0.0);
			for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = CurrentPoints[cur]->vb.energies.begin(); vbit != CurrentPoints[cur]->vb.energies.end(); ++vbit)
			{
				occupancy = CalcOccByFermiBand(&(vbit->second), CurrentPoints[cur]->fermip, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					vbit->second.SetEndOccStates(occupancy);
					vbit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					vbit->second.SetEndOccStatesOpposite(occupancy);
					vbit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy=vbit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->vb.AddNumParticles(occupancy);
				SetabsEUseFromFermi(&(vbit->second), CurrentPoints[cur]->fermip);
			}
			CurrentPoints[cur]->p = CurrentPoints[cur]->vb.GetNumParticles();

			CurrentPoints[cur]->holeinacceptor = 0.0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->acceptorlike.begin(); esit != CurrentPoints[cur]->acceptorlike.end(); ++esit)
			{
				occupancy = CalcOccByFermiBand(&(esit->second), CurrentPoints[cur]->fermi, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					esit->second.SetEndOccStates(occupancy);
					esit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					esit->second.SetEndOccStatesOpposite(occupancy);
					esit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy=esit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->holeinacceptor += occupancy;
				SetabsEUseFromFermi(&(esit->second), CurrentPoints[cur]->fermi);
			}
			CurrentPoints[cur]->elecindonor = 0.0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->donorlike.begin(); esit != CurrentPoints[cur]->donorlike.end(); ++esit)
			{
				occupancy = CalcOccByFermiBand(&(esit->second), CurrentPoints[cur]->fermi, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					esit->second.SetEndOccStates(occupancy);
					esit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					esit->second.SetEndOccStatesOpposite(occupancy);
					esit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy=esit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->elecindonor += occupancy;
				SetabsEUseFromFermi(&(esit->second), CurrentPoints[cur]->fermi);
			}
			CurrentPoints[cur]->elecTails = 0.0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->CBTails.begin(); esit != CurrentPoints[cur]->CBTails.end(); ++esit)
			{
				occupancy = CalcOccByFermiBand(&(esit->second), CurrentPoints[cur]->fermi, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					esit->second.SetEndOccStates(occupancy);
					esit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					esit->second.SetEndOccStatesOpposite(occupancy);
					esit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy=esit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->elecTails += occupancy;
				SetabsEUseFromFermi(&(esit->second), CurrentPoints[cur]->fermi);
			}
			CurrentPoints[cur]->holeTails = 0.0;
			for(std::multimap<MaxMin<double>,ELevel>::iterator esit = CurrentPoints[cur]->VBTails.begin(); esit != CurrentPoints[cur]->VBTails.end(); ++esit)
			{
				occupancy = CalcOccByFermiBand(&(esit->second), CurrentPoints[cur]->fermi, kbTI, CurrentPoints[cur]->cb.bandmin, CurrentPoints[cur]->vb.bandmin,oppOcc);
				if(oppOcc==false)
				{
					esit->second.SetEndOccStates(occupancy);
					esit->second.SetBeginOccStates(occupancy);
				}
				else
				{
					esit->second.SetEndOccStatesOpposite(occupancy);
					esit->second.SetBeginOccStatesOpposite(occupancy);
					occupancy=esit->second.GetBeginOccStates();
				}
				CurrentPoints[cur]->holeTails += occupancy;
				SetabsEUseFromFermi(&(esit->second), CurrentPoints[cur]->fermi);
			}
		}

		CurrentPoints[cur]->ResetData();	//now that it is all correct
		
	}

	return(0);
}

int GetAvgFermi(ELevel* NewELev, ELevel* OldELev, Point* NewPt, Point* OldPt, double pointOverlap, double& TotFermi, double& TotOverlap)
{
	double EOverlap = CheckELevOverlap(NewELev, OldELev, NewPt, OldPt);
	if(EOverlap > 0.0 && EOverlap <= 1.0)	//accept valid ranges
	{
		TotOverlap += EOverlap * pointOverlap;
		TotFermi += OldELev->AbsFermiEnergy * EOverlap * pointOverlap;
	}
	return(0);
}

int PopulateCurrentRange(Model& mdl, std::vector<Point*>& currentPts, int ID, std::map<long int, int>& BadPoints, bool ignoreID)	//data of node is which group the point(sort long) belongs to
{
	//NO VECTOR CLEAR OF CURRENTPTS INTENTIONAL!!
	for(std::map<long int, int>::iterator Nodes = BadPoints.begin(); Nodes != BadPoints.end(); ++Nodes)
	{
		if(Nodes->second == ID && ignoreID == false)
		{
			std::map<long int,Point>::iterator tmpPtr = mdl.gridp.find(Nodes->first);
			if(tmpPtr != mdl.gridp.end())
			{
				currentPts.push_back(&(tmpPtr->second));
				BadPoints.erase(Nodes);	//remove the node from the bad list because it is being processed
			}
		}
		else if(ignoreID)	//don't delete them off the list and ignore the id to build a master list to reference in the future!
		{
			std::map<long int,Point>::iterator tmpPtr = mdl.gridp.find(Nodes->first);
			if(tmpPtr != mdl.gridp.end())
			{
				currentPts.push_back(&(tmpPtr->second));
			}
		}
	}
	return(0);
}


int RenumberPoints(Model& mdl, ModelDescribe& mdesc, std::vector<Point*> AllReplacedPoints)	//it is essential that numbers never go beyond mdl.numpoints for the rest of the code to work with the arrays for poisson's equation. 0 to mdl.numpoints-1 is for rho and mdl.numpoints to 2*mdl.numpoints is fo boundary condition data.
{
/*
	ClearContacts(AllReplacedPoints);	//remove references in contacts to the old points. Must be done before the points are removed
	
	
	//to do this, renumbering will screw it up. Can not do all in one giant sweep - must do each individually, but doing so will screw up the for loop iteration.
	long int num = 0;
	vector<Tree<Point, long int>*> NodeList;
	mdl.gridp.GenerateNodeList(NodeList);

	long int sz = mdl.gridp.size();
	//this is a very dangerous move. I want to rebuild the tree, but the tree is going to be completely corrupt when I modify the points.
	//I do not want to actually delete any data, so here goes nothing...


	//reset the binary tree without actually deleting any data (cheating...)
	mdl.gridp.size = 0;

	Point* PrevPt = NULL;
	bool add;
	mdl.boundaries.clear();
	for(long int iter=0; iter<sz; iter++)
	{
		add = true;
		for(unsigned int i=0; i< AllReplacedPoints.size(); i++)
		{
			if(&(NodeList[iter]->Data) == AllReplacedPoints[i])
			{
				add = false;
				break;
			}
		}
		if(add)
		{
			if((NodeList[iter]->Data.adj.adjMX == NULL && NodeList[iter]->Data.adj.adjMX2 == NULL) ||
				(NodeList[iter]->Data.adj.adjPX2 == NULL && NodeList[iter]->Data.adj.adjPX == NULL))
				mdl.boundaries.push_back(num);

			NodeList[iter]->Sort = num;
			NodeList[iter]->Data.adj.self = num;
			NodeList[iter]->Data.adj.adjN = PrevPt;
			NodeList[iter]->Data.adj.adjP = NULL;
					
			mdl.gridp.InsertExistingNode(NodeList[iter]);	//Simply inserts the existing node into the tree. As far as the root is concerned, it has been reset.
			if(PrevPt)
			{
				PrevPt->adj.adjP = &(NodeList[iter]->Data);
			}
			

			PrevPt = &(NodeList[iter]->Data);
			num++;

			
		}
		else
		{
			
			delete NodeList[iter];
		}
		
	}
	mdl.numpoints = mdl.gridp.size;
	delete [] mdl.poissonRho;
	mdl.poissonRho = new double[mdl.numpoints];
	if(mdl.gridp.start)
		mdl.ptZero = &(mdl.gridp.start->SearchLeft()->Data);
	else
		mdl.ptZero = NULL;

	mdl.PoissonDependencyCalculated = false;	//it's a new mesh, so poisson's equation is no longer valid
	
	SelectContactPoints(mdl, mdesc);	//the points have been updated, so now new contact points must be selected
	//you must do this because the points added might be contacts themselves.
	for(unsigned int i=0;i<mdesc.simulation->contacts.size(); i++)
	{
		if(mdesc.simulation->contacts[i]->isolated == false)	//the contact is connected to the outer world and acts as a sink
		{
			for(unsigned int j=0; j<mdesc.simulation->contacts[i]->point.size(); j++)
			{
				EquilibratePoint(mdesc.simulation->contacts[i]->point[j]);	//make sure the contact point stays at rho ~0
			}
		}
	}
	
	*/
	return(0);
}

int ClearContacts(std::vector<Point*> AllReplacedPoints)
{
	//now that all the points are added in appropriately and populated, adjust the contacts.
	//first, remove all contact references of points that were deleted.
	for(unsigned int i=0; i<AllReplacedPoints.size(); i++)	//go through all the points which are being replaced
	{
		if(AllReplacedPoints[i]->contactData)	//see if it was a contact - populatePoint would have added the new contacts in as needed
		{
			for(std::vector<Point*>::iterator iter = AllReplacedPoints[i]->contactData->point.begin(); iter != AllReplacedPoints[i]->contactData->point.end(); iter++)
			{ //	loop through all the points listed in the contact
				if((*iter) == AllReplacedPoints[i]) //check if it matches the old back contact point
				{
					AllReplacedPoints[i]->contactData->point.erase(iter);	//get rid of that link
					break;	//get out of the contact point loop and move on to the next replaced point
				}
			}
		}
	}

	return(0);
}