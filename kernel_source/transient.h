#ifndef TRANSIENT_H
#define TRANSIENT_H

#include <map>
#include <fstream>
#include <vector>
#include <list>
#include <stdint.h>

#include "contacts.h"

#define INFINITETSTEP -1.0	//flag to say the timestep would be infinite

#define INIT_PERCENT_TRANSFER 0.125	//start by attempting to transfer 12.5% of the carriers (simple double value <-- subtract exponent when multiply)
#define REDUCE_PERCENT_TRANSFER 0.5	//how much it will try and reduce the net rate on this particular iteration
#define INCREASE_PERCENT_TRANSFER 2.0 //how much to increase the percent transfer
#define SOFT_INCREASE_PERCENT_TRANSFER 1.125	//it is moving towards a value consistently, but not converging fast enough
#define MIN_PERCENT_TRANSFER 1.0e-15	//the minimum percentage of carriers it will try to adjust, prevent infinite loop
#define MAX_PERCENT_TRANSFER 0.5	//allow it to change by up to half... which is kinda huge
#define MIN_PERCENT_IGNORE_TIMESTEP 1.0e-10	//if the rate is below this value, it cannot be allowed to have an effect on the time step. It's already really close to the correct answer.
#define LEVELINSIGNIFICANT 1e-8	//if a energy level occupancy is this much below the largest energy level occupancy in the point, it's not very important...
#define CLAMP_TIMESTEP 0.125
#define TSTEPSIZE 32
#define PERCENT_TRANSFER_MIN 0.0001220703125	//perhaps outdated?
#define TRANSFER_ABSOLUTE_MIN 0.01		//perhaps outdated?

#define START_SS_TZERO -1	//need to be negative so it can otherwise store the steady state index
#define START_SS -2			//need to be negative so it can otherwise store the steady state index
#define START_THEQ -3		//need to be negative so it can otherwise store the steady state index
#define START_ABSZERO -4	//need to be negative so it can otherwise store the steady state index
#define START_UNDEFINED -5	//need to be negative so it can otherwise store the steady state index

#define SAAPD_KEEP_GOING 0
#define SAAPD_SAVE_STATE 1
#define SAAPD_ABORT 2

//the aboved basically gives two regions - one being close, and it will hone in on the answer independent of time, and the other saying the timestep should definitely matter

class ChargeTransfer;
class Model;
class ModelDescribe;
class Simulation;
class Point;
class ELevelRate;
class ELevel;
class kernelThread;
class Contact;
class TransientSimData;
extern enum ctcIDS;


class TStepELevel
{
private:
	double occ0, occ1, occ2, occ3;	//the occupancies
	double r0, r1, r2, r3;	//the net rates
	double del1, del2, del3;	//how much does it want to change
	int numGood;
	bool istype0, istype1, istype2, istype3;	//do the occupancies match the carrier types. Resets info if they're not all the same!
	double tStepDesire;	//what we want the time step to be
public:
	int AddROcc(double occ, double nRate, bool matchocc, double accuracy);
	int GetRets(double& ret1, double& ret2, double& ret3);	//returns how many of these values are actually good
	int SetRets();	//use occ & r to set del123
	int SetRets(double val);	//just set the first one
	int SetRets(double val1, double val2);
	int SetRets(double val1, double val2, double val3);
	int SetTStep(double val) { tStepDesire = val; return(0); };
	int WriteDataToFile(std::ofstream& file);
	double GetTStep(void) { return(tStepDesire); };
	void Reset(void);

	double maxOcc, minOcc;
	bool maxMatch, minMatch;

	TStepELevel();
};

class TimeStepConsts
{
public:
	double prevtimestep;	//the previous timestep
	double prev2timestep;	//the timestep two iterations ago
	double deltaTM2Inv;	//the timestep two iterations ago
	double deltaTM1Inv;	//the timestep the previous iteration
	double TM1PlusTM2;	//the sum of the two previous iteration timesteps (reference time that new time must be over - assume time at tM2=0 to simplify math)
	double TM1PlusTM2Inv;	//inverse of abmove
};

class MonitorTStep
{
public:
	unsigned int Count[TSTEPSIZE];
	double CutOffs[TSTEPSIZE];	//i'm not going to try and be quick...

	int AddTStep(double step);
	int ClearTStep(void);
	bool isMaxedOut(double SSTime);
	MonitorTStep();
};


class TimeDependence
{
protected:
	bool sum;		//should this add on to other values? A sum may be added to an existing one that does not share, but one that does not share will be
	double starttime;	//what should be the start time of this
	double endTime;	//what the end time should be: any value <= 0 has no specified end time and ends when the next non-sum value is used
	double percentLeak;	//only valid for currents/EField on external dimensions.  [0,1.0]. Basically says this much of the E-field will continue in the non-calculated dimension. (fudge factor...)
	int flags;		//CONTACT_VOLT_COS, CONTACT_EFIELD_COS, CONTACT_CURRENT_COS
public:
	TimeDependence(double st = 0.0, double en = 0.0, int flg = 0, bool sm = false, double pLeak = 0.0);
	virtual ~TimeDependence() { }
	double GetStartTime(void) { return(starttime); };
	double GetEndTime(void) { return(endTime); };
	double GetLeak(void) { return(percentLeak); };
	int GetFlags(void) { return(flags); };
	bool IsSum(void) { return(sum); };
	void RemoveFlags(int flg) { flags &= (~flg); }
	void AddFlags(int flg) { flags |= flg; }
	void ClearFlags() { flags = 0; }
	void SetFlags(int flg) { flags = flg; }
	virtual bool isCos(void) { return false; }
	virtual bool isLin(void) { return false; }
	virtual double GetValue(double time) { return 0.0; }
};

class CosTimeDependence : public TimeDependence
{
	double A;			//Acos(2*pi*freq*t + delta);
	double freq;		//Acos(2*pi*freq*t + delta);
	double delta;		//Acos(2*pi*freq*t + delta);
	double DC;		//DC + Acos(...)
public:
	CosTimeDependence();
	CosTimeDependence(double amplitude, double frequency, double del, double direct, double stime, double etime, double leak, bool add, int flg);
	bool operator == (const CosTimeDependence& b) const;		//used for binary tree insert
	void initialize(double amplitude, double frequency, double del, double direct, double stime, double etime, double leak, bool add, int flg);
	virtual double GetValue(double time);		//calculate the value for this voltage
	double GetAmp(void);
	double GetFreq(void);
	double GetDelta(void);
	double GetDC(void);
	virtual bool isCos(void) { return true; }
};

class LinearTimeDependence : public TimeDependence
{
	double initialValue;	// F = init + slope *(time - starttime)
	double slope;
public:
	LinearTimeDependence(int flg = 0, double st = 0.0, double end = 0.0, double init = 0.0, double slp = 0.0, bool sm = false, double pLeak = 0.0) :
		TimeDependence(st, end, flg, sm, pLeak), initialValue(init), slope(slp) { }
	virtual double GetValue(double time) { return(initialValue + slope * (time - starttime)); };
	double GetInitVal(void) { return(initialValue); };
	double GetSlope(void) { return(slope); };
	int SetValues(double init, double slope, double start, double end, double leak, int flags, bool sum);
	virtual bool isLin(void) { return true; }
};

class TransContact{
	std::multimap<double, CosTimeDependence> VoltageCos;	//holds data for a cosine dependence of this contact, sort is starttime
	std::multimap<double, LinearTimeDependence> VoltageLin;	//holds data for a fixed time dependence, sort is starttime, data is voltage
	std::multimap<double, LinearTimeDependence> EFieldLin;	//what is the electric field entering(left)/leaving(right) the contact (fixed)
	std::multimap<double, CosTimeDependence> EFieldCos;	//what is the electric field entering the contact (cos)
	std::multimap<double, LinearTimeDependence> CurrentDensityLin;	//current A/cm^2
	std::multimap<double, CosTimeDependence> CurrentDensityCos;		//current A/cm^2
	std::multimap<double, LinearTimeDependence> CurrentLin;	//current entering the contact (amps)
	std::multimap<double, CosTimeDependence> CurrentCos; //current entering contact (amps)
	//	std::multimap<double, ContactCurOut> CurrentOut;	//just specify having a current out and that's it
	std::map<double, TimeDependence> AdditionalFlags;	//any additional flags that should be included (Minimize E Field energy)
	//note, band diagram will first and foremost be calculated by voltages that are set
	//next, it will look to electric fields (within the device dimensions)
	//finally, it will look to currents. If a value is not assigned, it will be calculated. This MAY cause significant discrepancies.
	//it should output errors if any of these discrepancies are found.

	std::map<double, int> ActiveValues;	//what data should be actively used for currents or electric fields. If the flag is not set, then assume the value can change.
	//ActiveValues should have a value assigned for every start/endtime there is. This should be used in determining currents, EField flux, etc.
	std::map<double, int> ActiveBandDiagram;	//data is what values should be used to calculate the band diagram, and sort is the starting time
	
	std::list<std::multimap<double, CosTimeDependence>::iterator> ActiveCos;	//what ones in the tree are actively being used for the sum
	std::list<std::multimap<double, LinearTimeDependence>::iterator> ActiveLinear;
	std::list<std::multimap<double, TimeDependence>::iterator> ActiveAdditional;

	double lastTimeUpdate;	//what was the last time updated as?
	double nexttimechange;	//set to -1 if there are no more time changes

	int currentActiveValue;	//this should always match up with ActiveValues though calculated independently
	double freqVoltage;	//the fastest frequency of the voltage that are active
	double freqCurrent;	//the fastest frequency of the currents that are active
	double freqEField;	//the fastest frequency of the electric field that are active

	
	int ctcID;	//the contact id
	Contact* ct;

	bool suppressVoltagePoisson;	//let's say there are more than two voltages, should this voltage be excluded from solving poisson's equation
	bool suppressCurrentPoisson;	//prevent having such an overly defined system that there is no solution - which may come from currents
	bool suppressEFieldPoisson;		//same can be said for the electric field
	bool suppressEFieldMinimize;	//does this contact want to minimize the electric field? suppress if e field set, or enough stuff is already set
	bool suppressOppDField;		//boundary condition to basically try and get the exiting efields balanced and opposite (which should shoot for ~0, esp. if charge neutral)
	bool suppressChargeNeutral;	//boundary condition to make contacts charge neutral
	bool allowOutputMessages;	//don't spam the trace file or user with error messages resulting from this contact

	void XMLCos(XMLFileOut& file, std::multimap<double, CosTimeDependence>& dat);
	void XMLLin(XMLFileOut& file, std::multimap<double, LinearTimeDependence>& dat);
	void XMLTim(XMLFileOut& file, std::map<double, TimeDependence>& dat);
	int PrivateCosSave(std::ofstream& file, std::multimap<double, CosTimeDependence>& data);
	int PrivateLinSave(std::ofstream& file, std::multimap<double, LinearTimeDependence>& data);
	int PrivateTimeDepSave(std::ofstream& file, std::map<double, TimeDependence>& data);

public:
	TransContact(Contact *ctc = nullptr, int id = -1);
	~TransContact();

	int SavePrivateData(std::ofstream& file);	//for save state

	int RemoveAdditionalFlags(int flags, double time);
	int AddAdditionalFlags(int flags, double time);
	double GetPreviousAlteration(double time);
	int GetContactID() { return ctcID; }

	int GetCurrentActive(void) { return(currentActiveValue); };
	int BuildActiveValues(void);
	int UpdateActive(double curtime = 0.0, bool forceUpdate = false, int SSActive = -1);
	double UpdateActive_LoopCosTime(std::multimap<double, CosTimeDependence>& data, double& maxFreq, double curtime = 0.0);
	double UpdateActive_LoopLinTime(std::multimap<double, LinearTimeDependence>& data, double curtime = 0.0);
	double UpdateActive_LoopAdditionalTime(std::map<double, TimeDependence>& data, double curtime = 0.0);

	int UpdateInternalValsPreBD(double curtime = 0.0);	//some stuff has to be set before the band diagram can be calculated (ActiveValues)
	int UpdateInternalValsPostBD(double curtime = 0.0);	//and some stuff has to be set after the band diagram can be calculated (Non-active values)
	
	void UpdateNextTime(double time);
	int GetTimes(ctcIDS which, std::vector<double>& times);
	double GetNextTimeChange(void) { return nexttimechange; }

	int AddTimeDependence(TimeDependence& dat, double curtime = 0.0);	//basically AddCosTimeDependence, AddLinDependence, and AddCurOutDependence - with less inputs. why the hell did i not do it this way to begin with
	bool RemoveTimeDependence(double sTime, double eTime, int flag);
	int AddCosTimeDependence(CosTimeDependence data, ctcIDS which, double startime, double curtime = 0.0);	//adds a cos dependence to the contact
	int AddLinearDependence(LinearTimeDependence data, ctcIDS which, double starttime, double curtime = 0.0);		//adds a fixed voltage to the contact

	double GetFrequency(void);
	double GetVoltageFrequency(void);
	double GetCurrentFrequency(void);
	double GetEfieldFrequency(void);

	int SetSuppressions(bool voltage, bool current, bool eField, bool eMin, bool eOpp, bool JNeutral, int which = SUPPRESS_SET_ALL);
	bool SuppressVoltage(void) { return(suppressVoltagePoisson); };
	bool SuppressCurrent(void) { return(suppressCurrentPoisson); };
	bool SuppressEField(void) { return(suppressEFieldPoisson); };
	bool SuppressMinEField(void) { return(suppressEFieldMinimize); };
	bool SuppressDFieldOpp(void) { return(suppressOppDField); };
	bool SuppressChargeNeutralCurrent(void) { return(suppressChargeNeutral); };

	Contact* GetContact() { return ct; }
	void SetContactID(int id) { ctcID = id; if (ct && ct->id != id) ct = nullptr; }
	void SetContact(Contact* ctc) { ct = ctc; ctcID = ct ? ct->id:-1; }

	bool TransferSuppressionsActiveToContact();
	void UpdateGridDisplay(std::vector<TransDataDisplay>& disp);	//located in gui_source/SimTransient.cpp. Only place it's used, but need to access lots of protected data

	void InitNewIteration(double time, bool forcecalc);	//run once each iteration.  Updates the contact voltage if needed
	void SaveToXML(XMLFileOut& file);

	void Validate(Simulation& sim);
};

class TimeStepControl
{
private:
	//	map<int, int> Count;
	short Count[TSTEPSIZE];
	bool Active[TSTEPSIZE];
	double CutOffs[TSTEPSIZE];
	//	map<int,bool> Active;
	double TStepMin;
	double TStepMax;

	double maxTStepUsed;	//how high has the time step gotten?
	double maxTStepAllowed;	//user chosen maximum time step

	double targetTStep;	//what is the timestep used for comparing rates
	double useTStep;	//when actually transferring carriers, this is the time step used
	double activeTolerance;	//what is the current tolerance. always > 0
	double maxStepTolerance;	//this one kinda varies

	double startTimeData;	//at what time did the data start collecting for an output set
	double lastOutTime;	//what was the start time of the previous one?

	double timeFallDownLeft;	//it's always gotta fall down within the amount of time provided
	
	uint8_t state;
	unsigned int numLevelsDown;	//how many times have we decreased from the maximum step? 5*this for the number of times the timestep may be repeated

	int ActiveMustBeGreater;	//the active band must be greater than this
	int NumRepeatBeforeNextStep;	//how many repeats you can do before moving on to the next step
	int ClearLowerCounts(int val);
	void ClearArrays(void);
	double UpdateTimeLimit(double curTime, double targetTime);
	double TimeToNextMilestone;
	int GetIndex(double val);
	static const uint8_t AtSteadyState = 0x80;	//flag to say it's done all the rates, can't increase the time step, and nothing changed. Equivocally at steady state


public:
	static const uint8_t TStepFalling = 0x01;	//repairing damage done from an overstep
	static const uint8_t RatesOmitted = 0x02;	//flag to let us know if any rates were omitted during the calculations
	static const uint8_t RatesAdded = 0x04;	//flag to let us know a rate was added
	static const uint8_t ChangesMade = 0x08;	//flag to help know when the time step is so small that nothing actually changed.
	static const uint8_t LargeChanges = 0x10;	//flag to let us know we're not really ready to increase the time step
	static const uint8_t ClearFallSignFlip = 0x20;	//flag set so energy states know they should clear the flag for sign flipping
	static const uint8_t ClearMaxSignFlip = 0x40;	//flag set so energy states know they should clear the flag for sign flipping
	static const uint8_t ClearIter = 0x1E;	//everything but the first bit for falling or max step and that it's done

	bool IncreasePercentTransfer;	//flag whether to increase the percent transfer or not
	double DetermineTStep(double netRate, double numCarriers, double percentMove);
	void SetNumRepeat(int val) { NumRepeatBeforeNextStep = val; }
	void SetMaxTStep(double val) { TStepMax = val; }
	void ReInit(void) { ClearArrays(); ActiveMustBeGreater = -1; }	//call if external environment changes
	int IterationUpdateInternal(Simulation* sim);	//uses count and active to set TStepMin, updates Count appropriately. Do ONCE per iteration
	bool RateIsOK(double netRate, double numCarriers, double percentMove);	//checks the net rate to see if it should be allowed
	double UpdateActiveRate(double netRate, double numCarriers, double percentMove);	//returns timestep
	//	double UpdateActiveRate(ELevelOpticalRate& rate);	//returns timestep
	double UpdateActiveRate(ELevelRate& rate);	//returns timestep

	bool SafeOutput(double curTime) { return (!TestState(ChangesMade) && !TestState(RatesOmitted) && curTime >= lastOutTime + 1.0e-15); }	//worked way down to bottom. Any numerical problems should have been washed out
	void ClearState() { state = 0; }
	void SetState(uint8_t st) { state = st; }
	void AppendState(uint8_t st) { state |= st; }
	void RemoveState(uint8_t st) { state &= (~st); }
	bool TestState(uint8_t st) { return ((state & st) == st); }
	bool IsHighestStep() { return (TestState(TStepFalling) == false); }
	bool isDroppingStep() { return TestState(TStepFalling); }
	bool isAllRatesIncluded() { return !TestState(RatesOmitted); }
	bool isAnyOccupancyChanges() { return TestState(ChangesMade); }
	bool isLargeOccupancyChanges() { return TestState(LargeChanges); }
	bool isAllSmallChanges() { return !TestState(LargeChanges); }
	bool isRatesExcluded() { return TestState(RatesOmitted); }
	bool isAnyRatesAdded() { return TestState(RatesAdded); }
	bool isAtSteadyState() { return TestState(AtSteadyState); }
	bool ignoreRestIteration() { return (!TestState(ChangesMade) && targetTStep != maxTStepAllowed); }	//it's basically churning away at nothing
	bool isClearMaxSignFlip() { return TestState(ClearMaxSignFlip); }
	bool isClearFallSignFlip() { return TestState(ClearFallSignFlip); }
	double getTargetTStep() { return targetTStep; }
	double getUseTStep() { return useTStep; }
	uint8_t getState() { return state; }
	double getTolerance() { return activeTolerance; }	//> 0
	void UpdateUseTStep(double delta);
	void SetFallingTimestep() { AppendState(TStepFalling); numLevelsDown = 0; }
	void DecreaseTargetTStep() { targetTStep *= 0.125; numLevelsDown++; AppendState(ClearFallSignFlip);  }	//cut by 1/8
	void IncreaseTargetTStep() { targetTStep *= 4.0; AppendState(ClearMaxSignFlip); }	//quadruple.
	void DecreaseMaxTolerance() { maxStepTolerance *= 0.125; AppendState(ClearMaxSignFlip); }
	void SetStartTimeData(double d) { startTimeData = d; }
	double GetStartTimeData() { return startTimeData; }
	double GetUserMaxStep() { return maxTStepAllowed; }
	double GetMaxTStepUsed() { return maxTStepUsed; }
	void UpdateLastOutTime() { lastOutTime = startTimeData; }
	double GetMaxTolerance() { return maxStepTolerance; }

	void UpdateActiveTolerance(double smallestT = 1.0e-12);

	void SetUserMaxTStep(double step) { maxTStepAllowed = step > 0.0 ? step : 1.0e-3; }
	void FirstInit(TransientSimData* tsd, bool envChanged=false);
	
	void SetupIteration(TransientSimData* tsd);	//sets the target time step

	TimeStepControl();	//default constructor
	TimeStepControl(int numRepeat);	//set constructor
	~TimeStepControl();

};

class TargetTimeData
{
public:
	bool SaveState;
	unsigned int SSTarget;
};

class SpectraTimes
{
public:
	bool enable;
	int SpecID;
	double time;
};

class TransientSimData
{
	Simulation* const sim;
public:
	TransientSimData(Simulation* s);
	~TransientSimData();
	int SetupTimeStep();
	double InitIteration();
	int Clear(void);
	int DeterminePoissonValues();
	Simulation* getSim() { return sim; }
	int CreateNewContacts(std::vector<Point*>& edges);
	TransContact* GetTransContact(Contact *ct);
	TransContact* GetTransContact(int ctID);
	int PrepBoundaryConditions(bool force = false);
	void SaveToXML(XMLFileOut& file);
	void Validate();
	void MoveTime();
	int RecordTimestep(bool clear);
	void OutputData(bool force=false);

	int TransMain(kernelThread *kThread);
	int ResetValues();
	bool TrySmallerTStep(ChargeTransfer &chgT);
	void TestTargetTimes();
	
	void OutputTStepsToFile(std::string fname);
	int OutputDataToFile();

	TimeStepConsts tstepConsts;	//constants needed for the timestep
	TimeStepControl tStepControl;	//used to determine what rates can be added for calculations and doing the timestep
	MonitorTStep monTStep;

	bool SSByTransient;	//flag that tells it we only care about SS, and we're gonna get there via the transient. Don't care about intermittent data.
	double SSbyTTime;	//the amount of time to multiply by the net rate to see if the % change in occupancy is less than tolerance.
	bool isSS;		//is the simulation in steady state for the transient?
	bool SmartSaveState;	//should the program automatically determine when to save the state?

	double timeignore;	//time to ignore on recording data
	double simtime;		//current iteration's start time
	double timestep;	//current time difference in this particular iteration
	double nexttimestep;	//what is the timestep for the next iteration...?
	double timestepi;	//inverse of the itme step because it is divided very often...
	std::vector<TransContact*> ContactData;
	std::map<double, TargetTimeData> TargetTimes;	//these times are times the simulation should target, bool indicates whether to save the device state at that time
	std::vector<SpectraTimes> SpecTime;

	int IntermediateSteps;	//how many intermediate steps will it take? default to 99. This looks at the target times, and adds this many targets between each pre-existing time. sorta forces updates. Don't care to use in GUI.
	int StartingCondition;	//START_(SS_TZERO, SS, THEQ, ABSZERO). START_SS is assumed for values >= 0
	double curTargettime;	//what the current target time is for what's above...
	double iterEndtime;		//end time for each particular iteration (absolute time)
	double simendtime;	//when the simulation should be considered complete.
	double storevalueStartTime;	//when binning values, this will be the starting time used.

	double mints;	//minimum timestep for this particular bin of data
	double maxts;	//maximum timestep for this particular bin
	double avgts;	//the average timestep in this bin of data

	double tolerance;	//the tolerance for the given time step. At large time step, it varies. at small time step, it is sim->accuracy
	double percentCarrierTransfer;	//how many of the carriers is it trying to transfer?
	bool ResetAlterPercent;
	bool AlterPercentCarrier;	//a (passive) flag for whether percent carrier needs to be reduced - all rates are beyond the limit
	double ForceAlterPercent;	//if the timestep is above this value, force altering the percent carriers
	
	double curmaxNetRate;	//what was the maximum net rate of this particular iteration
	double tmpMaxNRate;	//what is the max rate for this calculation?
	double prevMNRate;	//what the max rate was on the previous iteration - important for when it fails. Don't compare iteration to itself!
	double testMNRate;	//the rate that gets tested against

	unsigned int redos;	//how many times does it redo the calculation because of too large a step

	std::vector<double> timesteps;	//the timesteps for all the iterations

	double avgTStep;//used for sending data back to the kernel
	double tstepctr;

	unsigned int outputCtr;	//just so all the files outputted are sequential. That makes life easier.
	
};

//int Transient(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
int SetStartingConditions(Model& mdl, ModelDescribe& mdesc, int condition=START_UNDEFINED, unsigned int whichSS=-1);
int CreateSSFromTransient(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread=NULL);
//int PrepAll(Model& mdl, ModelDescribe& mdesc);
int PrepELevel(ELevel& eLev, Model& mdl, Point* pt, bool ResetPercentTransfer, double percentTransfer, bool IncreaseTransfer);
double CalcAllRates(Model& mdl, Simulation* sim);
//bool TrySmallerTStep(Model& mdl, ModelDescribe& mdesc, ChargeTransfer& chgT);
bool SmallerTStepELevel(ELevel& eLev, Model& mdl, Point* curPt, double TStep);
//int CalcELevelRates(Model& mdl, Point& pt, ELevel& srcElev, Simulation* sim);
//template <class ELevel, class S>
//double CalcELevelRates(Model& mdl, Point* pt, Tree<ELevel, S>* eNode, Simulation* sim);
int ThermalPtTStep(Point* pt);
bool UpdateAllRates(Model& mdl, Simulation* sim, ChargeTransfer& chgT);
bool UpdateELevelRate(ELevel& state, Simulation* sim, ChargeTransfer& chgT);
int ProcessRateList(ELevelRate& rate, Simulation* sim, ELevel* eLev, ChargeTransfer& chgT);
//int ProcessRateList(vector<ELevelOpticalRate>::iterator& rate, Simulation* sim, ELevel* eLev, ChargeTransfer& chgT);
double GetELevelNumCarrierTransfer(ELevel* eLev);
int RecordAll(Model& mdl, ModelDescribe& mdesc, ChargeTransfer& chgT);
int RecordPoint(Model& mdl, Point& pt, Simulation* sim);
int RecordELevel(ELevel& srcElev, double& num, double& charge, Simulation* sim);
int CalcTransfer(ELevel& eLev, Simulation* sim);
int FixOverSteps(Model& mdl, ModelDescribe& mdesc);
int FindOverSteps(Model& mdl, ModelDescribe& mdesc);
int TestEnergyLevel(ELevel& eLev, Simulation* sim);
int UpdateAllTargetOccs(Model& mdl, Simulation* sim);

#endif