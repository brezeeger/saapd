#include "SteadyState.h"

#include <ctime>

#include "model.h"
#include "RateGeneric.h"
#include "helperfunctions.h"
#include "FindELevelSS.h"
#include "output.h"
#include "outputdebug.h"
#include "SaveState.h"
#include "contacts.h"
#include "light.h"
#include "transient.h"
#include "physics.h"

#include "../gui_source/kernel.h"

int SteadyState(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	Simulation* sim = mdesc.simulation;
	sim->outputs.numinbin = 1;	//obviously... but just make sure

	if(sim->accuracy < 1e-14)	//keep within double accuracy!
		sim->accuracy = 1.0e-14;
	if(sim->accuracy > 1.0)
		sim->accuracy = 1.0e-12;

	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	sim->TransientData->timestep = 1.0;	//make sure data is not thrown off here

	double adjustPercentage = 1.0e-8;	//how much to move towards the steady state rate
	double maxPercentChange = 1.0;	//how much the occupancy changed by percentage. Want this to be really small, so the answer is close
	double LargestCarrierTransfer = 0.0;
	double LargestBandDiagramChange = 0.0;	//check
	double LargestRate=0.0;	//check

	do
	{
		

		LargestBandDiagramChange = PrepAllSFirst(mdl, mdesc);
		CalcFirstSSRates(mdl, mdesc);	//now all the basic rates are in
		LargestRate = 0.0;
		LargestCarrierTransfer = FindSSOccupancyAll(mdl, mdesc, LargestRate, maxPercentChange);

		time(&end);	//load the end time for dif
		timeelapse = difftime(end, start);
		if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
			DisplayProgressDoub(14, LargestRate);
			DisplayProgressDoub(15, LargestCarrierTransfer);
			DisplayProgressDoub(17, maxPercentChange);
			DisplayProgressDoub(16, LargestBandDiagramChange);
			
			OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately
			start = end;
		}
		
		if(maxPercentChange > 1.0e16)
			adjustPercentage = 1.0e-8;
		else if(maxPercentChange > 1.0e14)
			adjustPercentage = 1e-7;
		else if(maxPercentChange > 1.0e12)
			adjustPercentage = 1e-6;
		else if(maxPercentChange > 1.0e10)
			adjustPercentage = 1.0e-5;
		else if(maxPercentChange > 1.0e5)
			adjustPercentage = 1e-5;
		else if(maxPercentChange > 1.0e3)
			adjustPercentage = 1e-4;
		else if(maxPercentChange > 3.0e2)
			adjustPercentage = 5e-4;
		else if(maxPercentChange > 1.0e2)
			adjustPercentage = 0.001;
		else if(maxPercentChange > 3.0e1)
			adjustPercentage = 5e-3;
		else if(maxPercentChange > 1.0e1)
			adjustPercentage = 0.001;
		else if(maxPercentChange > 1.0e0)
			adjustPercentage = 0.005;
		else if(maxPercentChange > 1.0e-1)
			adjustPercentage = 0.01;
		else if(maxPercentChange > 1.0e-2)
			adjustPercentage = 0.02;
		else if(maxPercentChange > 1.0e-3)
			adjustPercentage = 0.05;
		else if(maxPercentChange > 1.0e-4)
			adjustPercentage = 0.1;
		else if(maxPercentChange > 1.0e-5)
			adjustPercentage = 0.2;
		else if(maxPercentChange > 1.0e-6)
			adjustPercentage = 0.5;
		else
			adjustPercentage = 1.0;

			
			
			//		if(LargestBandDiagramChange < 0.001)	//move the adjustment in the proper direction
//			adjustPercentage = adjustPercentage * 1.5;
//		else
//			adjustPercentage = adjustPercentage * 0.0009765625;

		//don't let it exceed the limits
//		if(adjustPercentage > 1.0)
//			adjustPercentage = 1.0;
		if(adjustPercentage < 1.0e-10)
			adjustPercentage = 1.0e-10;

		UpdateAllOccupancy(mdl, adjustPercentage);
		
		

		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
			break;
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);
		if(kThread->shareData.GUIsaveState)
		{
			DisplayProgressDoub(12, mdesc.simulation->TransientData->iterEndtime);
			ProgressInt(42, mdesc.simulation->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			kThread->shareData.GUIsaveState = false;	//reset boolean
		}
//		(sim->curiter)++;
	} while(adjustPercentage != 1.0 || maxPercentChange > sim->accuracy);

	PrepAllSFirst(mdl,mdesc);	//now get the data properly setup for the current state
	CalcFirstSSRates(mdl, mdesc);	//get the updated rates
	OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately
	(sim->curiter)++;
	

	return(0);
}

int OutputSSRates(Model& mdl, ModelDescribe& mdesc)
{
	PosNegSum charge;
//	mdl.ptZero->ResetData();
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->second.cb.energies.begin(); cbit != pt->second.cb.energies.end(); ++cbit)
		{
			RecordELevelSSRates(cbit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(cbit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->second.vb.energies.begin(); vbit != pt->second.vb.energies.end(); ++vbit)
		{
			RecordELevelSSRates(vbit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(vbit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.acceptorlike.begin(); eit != pt->second.acceptorlike.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eit = pt->second.donorlike.begin(); eit != pt->second.donorlike.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eit = pt->second.CBTails.begin(); eit != pt->second.CBTails.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for (std::map<MaxMin<double>, ELevel>::iterator eit = pt->second.VBTails.begin(); eit != pt->second.VBTails.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}

		pt->second.srhR1 = pt->second.srhR1 * pt->second.voli;	//get rates in normal units instead of just # of carriers
		pt->second.srhR2 = pt->second.srhR2 * pt->second.voli;	//that moved/sec. While useful for seeing where steps are,
		pt->second.srhR3 = pt->second.srhR3 * pt->second.voli;	//having actual rates makes life a bit easier...
		pt->second.srhR4 = pt->second.srhR4 * pt->second.voli;
		pt->second.genth = pt->second.genth * pt->second.voli;
		pt->second.recth = pt->second.recth * pt->second.voli;
		pt->second.scatterR = pt->second.scatterR * pt->second.voli;
		pt->second.LightAbsorbDef = pt->second.LightAbsorbDef * pt->second.voli;
		pt->second.LightAbsorbSRH = pt->second.LightAbsorbSRH * pt->second.voli;
		pt->second.LightAbsorbGen = pt->second.LightAbsorbGen * pt->second.voli;
		pt->second.LightAbsorbScat = pt->second.LightAbsorbScat * pt->second.voli;
		pt->second.LightEmitDef = pt->second.LightEmitDef * pt->second.voli;
		pt->second.LightEmitRec = pt->second.LightEmitRec * pt->second.voli;
		pt->second.LightEmitScat = pt->second.LightEmitScat * pt->second.voli;
		pt->second.LightEmitSRH = pt->second.LightEmitSRH * pt->second.voli;
		pt->second.ResetData();	//update all internal variables for outputdatafinal
		charge.AddValue(pt->second.netCharge);	//this will now just be net charges
		//so if 2 e- and a hole are in, it will just do 1e-.
		OutputRatesPoint(pt->second, mdesc.simulation);	//debug thorough output. tests flag to do output within function
	}

	mdesc.simulation->negCharge = charge.GetNegSum() * QCHARGE;
	mdesc.simulation->posCharge = charge.GetPosSum() * QCHARGE;
	
	OutputCharge(mdesc.simulation, false);

	OutputDataFinal(mdl, *(mdesc.simulation));
	
	//want store value start time to result in a simple average which is 1.0/(iterendtime - storevaluestarttime)
	//mdesc.simulation->storevalueStartTime = mdesc.simulation->iterEndtime - double(mdesc.simulation->binctr);	//make sure the data doesn't get messed up
	
	OutputDataToFileSS(mdl, mdesc);	//and get the data out to file
	mdesc.simulation->binctr = 0;
	
	
	return(0);
}

int UpdateOccupancy(ELevel& eLev)
{
	double occ = eLev.GetBeginOccStates();
	double tmp = eLev.targetOcc - occ;	//the difference between the two
	double maxPercentChange = fabs(tmp / occ);
	double adjustPercentage = 1.0;

	if(maxPercentChange > 1.0e16)
		adjustPercentage = 1.0e-8;
	else if(maxPercentChange > 1.0e14)
		adjustPercentage = 1e-7;
	else if(maxPercentChange > 1.0e12)
		adjustPercentage = 1e-6;
	else if(maxPercentChange > 1.0e10)
		adjustPercentage = 1.0e-5;
	else if(maxPercentChange > 1.0e5)
		adjustPercentage = 1e-5;
	else if(maxPercentChange > 1.0e3)
		adjustPercentage = 1e-4;
	else if(maxPercentChange > 3.0e2)
		adjustPercentage = 5e-4;
	else if(maxPercentChange > 1.0e2)
		adjustPercentage = 0.001;
	else if(maxPercentChange > 3.0e1)
		adjustPercentage = 5e-3;
	else if(maxPercentChange > 1.0e1)
		adjustPercentage = 0.001;
	else if(maxPercentChange > 1.0e0)
		adjustPercentage = 0.005;
	else if(maxPercentChange > 1.0e-1)
		adjustPercentage = 0.01;
	else if(maxPercentChange > 1.0e-2)
		adjustPercentage = 0.02;
	else if(maxPercentChange > 1.0e-3)
		adjustPercentage = 0.05;
	else if(maxPercentChange > 1.0e-4)
		adjustPercentage = 0.1;
	else if(maxPercentChange > 1.0e-5)
		adjustPercentage = 0.2;
	else if(maxPercentChange > 1.0e-6)
		adjustPercentage = 0.5;
	else
		adjustPercentage = 1.0;

	double change = tmp * adjustPercentage;	//but only want to move a fraction of them
	eLev.AddEndOccStates(change);

	return(0);
}

int UpdateAllOccupancy(Model& mdl, double movePercent)
{
//	double change, tmp, occ;
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.cb.energies.begin(); eLev != pt->second.cb.energies.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
		//	occ = eLev->second.GetBeginOccStates();
		//	tmp = eLev->second.targetOcc - occ;	//the difference between the two
		//	change = tmp * movePercent;	//but only want to move a fraction of them
		//	eLev->second.AddEndOccStates(change);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.vb.energies.begin(); eLev != pt->second.vb.energies.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
//			occ = eLev->second.GetBeginOccStates();
//			tmp = eLev->second.targetOcc - occ;	//the difference between the two
//			change = tmp * movePercent;	//but only want to move a fraction of them

//			eLev->second.AddEndOccStates(change);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.CBTails.begin(); eLev != pt->second.CBTails.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
//			occ = eLev->second.GetBeginOccStates();
//			tmp = eLev->second.targetOcc - occ;	//the difference between the two
//			change = tmp * movePercent;	//but only want to move a fraction of them

//			eLev->second.AddEndOccStates(change);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.VBTails.begin(); eLev != pt->second.VBTails.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
//			occ = eLev->second.GetBeginOccStates();
//			tmp = eLev->second.targetOcc - occ;	//the difference between the two
//			change = tmp * movePercent;	//but only want to move a fraction of them

//			eLev->second.AddEndOccStates(change);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.acceptorlike.begin(); eLev != pt->second.acceptorlike.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
//			occ = eLev->second.GetBeginOccStates();
//			tmp = eLev->second.targetOcc - occ;	//the difference between the two
//			change = tmp * movePercent;	//but only want to move a fraction of them

//			eLev->second.AddEndOccStates(change);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.donorlike.begin(); eLev != pt->second.donorlike.end(); ++eLev)
		{
			UpdateOccupancy(eLev->second);
//			occ = eLev->second.GetBeginOccStates();
//			tmp = eLev->second.targetOcc - occ;	//the difference between the two
//			change = tmp * movePercent;	//but only want to move a fraction of them

//			eLev->second.AddEndOccStates(change);
		}
	}
	return(0);
}

double FindSSOccupancyAll(Model& mdl, ModelDescribe& mdesc, double& largestNRate, double& maxPercentChange)
{
	double LCarrierTransfer = 0.0;
	largestNRate = 0.0;
	maxPercentChange = 0.0;
	double tmp, occ;
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.cb.energies.begin(); eLev != pt->second.cb.energies.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;


		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.vb.energies.begin(); eLev != pt->second.vb.energies.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.CBTails.begin(); eLev != pt->second.CBTails.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.VBTails.begin(); eLev != pt->second.VBTails.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.acceptorlike.begin(); eLev != pt->second.acceptorlike.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->second.donorlike.begin(); eLev != pt->second.donorlike.end(); ++eLev)
		{
			tmp = fabs(eLev->second.RateSums.GetNetSum());
			if(tmp > largestNRate)
				largestNRate = tmp;
			FindELevelSS(eLev->second, mdesc.simulation, mdl);
			occ = eLev->second.GetBeginOccStates();
			tmp = fabs(eLev->second.targetOcc - occ);
			if(tmp > LCarrierTransfer)
				LCarrierTransfer = tmp;
			tmp = tmp / occ;
			if(tmp > maxPercentChange)
				maxPercentChange = tmp;
		}
	}


	return(LCarrierTransfer);
}

int CalcFirstSSERates(Model& mdl, std::multimap<MaxMin<double>,ELevel>& states, Simulation* sim)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator st = states.begin(); st != states.end(); ++st)
	{
		for(std::vector<TargetE>::iterator trg = st->second.TargetELevDD.begin(); trg != st->second.TargetELevDD.end(); ++trg)
			CalcRate(&(st->second), trg->target, sim);	//calculate the rates to all target energy levels
		for(std::vector<ELevel*>::iterator trg = st->second.TargetELev.begin(); trg != st->second.TargetELev.end(); ++trg)
			CalcRate(&(st->second), *trg, sim);	//calculate the rates to all target energy levels
	}
	return(0);
}

int CalcFirstSSRates(Model& mdl, ModelDescribe& mdesc)
{
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		
		CalcFirstSSERates(mdl, pt->second.cb.energies, mdesc.simulation);
		CalcFirstSSERates(mdl, pt->second.vb.energies, mdesc.simulation);
		CalcFirstSSERates(mdl, pt->second.donorlike, mdesc.simulation);
		CalcFirstSSERates(mdl, pt->second.acceptorlike, mdesc.simulation);
		CalcFirstSSERates(mdl, pt->second.CBTails, mdesc.simulation);
		CalcFirstSSERates(mdl, pt->second.VBTails, mdesc.simulation);
		
	}
	for(unsigned int i=0; i<mdesc.simulation->contacts.size(); i++)
		ProcessContactSteadyState(mdl, mdesc.simulation->contacts[i], mdesc.simulation);
	
	Light(mdl, mdesc, NULL);	//now update all the light rates

	return(0);
}

int PrepELevelSFirst(std::multimap<MaxMin<double>,ELevel>& states, Model& mdl, Point& pt, Simulation* sim)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = states.begin(); eLev != states.end(); ++eLev)
	{
		eLev->second.TransferEndToBegin();	//approx ~setBegin(GetEnd);
		eLev->second.Rates.clear();
		eLev->second.OpticalRates.clear();
		eLev->second.RateSums.Clear();
	}

	return(0);
}

double PrepAllSFirst(Model& mdl, ModelDescribe& mdesc)
{
	//clear out all light data
	if(mdesc.simulation->OpticalFluxOut)
		mdesc.simulation->OpticalFluxOut->ClearAll();
	for(unsigned int i=0; i<mdesc.materials.size(); i++)
	{
		if(mdesc.materials[i]->OpticalFluxOut)
			mdesc.materials[i]->OpticalFluxOut->ClearAll();
	}

	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		//not sure if this is actually necessary...
		pt->second.Jn.x = pt->second.Jn.y = pt->second.Jn.z = pt->second.Jp.x = pt->second.Jp.y = pt->second.Jp.z = 0.0;
		pt->second.genth = pt->second.recth = pt->second.scatterR = 0.0;
		pt->second.srhR1 = pt->second.srhR2 = pt->second.srhR3 = pt->second.srhR4 = pt->second.netCharge = 0.0;
		pt->second.LightAbsorbDef = pt->second.LightAbsorbGen = pt->second.LightAbsorbScat = pt->second.LightAbsorbSRH = 0.0;
		pt->second. LightEmitDef = pt->second.LightEmitRec = pt->second.LightEmitScat = pt->second.LightEmitSRH = 0.0;
		pt->second.LightPowerEntry = pt->second.LightEmission = 0.0;	//mW
		if(pt->second.LightEmissions)
			pt->second.LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!
		pt->second.largestOccCB = pt->second.largestOccES = pt->second.largestOccVB = pt->second.largestUnOccES = 0.0;

		PrepELevelSFirst(pt->second.cb.energies, mdl, pt->second, mdesc.simulation);
		PrepELevelSFirst(pt->second.vb.energies, mdl, pt->second, mdesc.simulation);
		PrepELevelSFirst(pt->second.acceptorlike, mdl, pt->second, mdesc.simulation);
		PrepELevelSFirst(pt->second.donorlike, mdl, pt->second, mdesc.simulation);
		PrepELevelSFirst(pt->second.CBTails, mdl, pt->second, mdesc.simulation);
		PrepELevelSFirst(pt->second.VBTails, mdl, pt->second, mdesc.simulation);
		
		pt->second.ResetData();	//get the data correct throughout the point
		mdesc.simulation->PrepPointBandStructure(mdl, pt->second);	//and update mdl.poisson rho with the correct charge

		OutputPointOpticalDetailedInit(mdesc.simulation, pt->second);
	}

	return(mdesc.simulation->InitIteration(mdl, mdesc));	//this will update the band structure and return the max change

	return(0);
}

/*
This code is entirely made too just find steady state and not care about transients. Completely numerical algorithm
*/
int SteadyStateOld(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	Simulation* sim = mdesc.simulation;
	double change;
	bool first = true;
	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	sim->TransientData->timestep = 1.0;	//make sure data is not thrown off here
	GetStartCharge(mdl, mdesc);
	std::string tmpOutF = sim->OutputFName;	//hold it to replace
	std::ostringstream newOutName;
	newOutName << sim->OutputFName << "_" << sim->TransientData->curTargettime;
	sim->OutputFName = newOutName.str();

	for(unsigned int i=0; i<sim->maxiter; i++)
	{
		time(&end);	//load the end time for dif
		timeelapse = difftime(end, start);
		if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
			DisplayProgress(53, i);
			DisplayProgress(29, sim->maxiter);

//			guiShare.KernelsimTime = mdesc.simulation->simtime;
//			guiShare.KernelnextTimeTarget = mdesc.simulation->curTargettime;


//			wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, KERNEL_UPDATE);
//			event.SetInt(i);
//			parent->GetEventHandler()->AddPendingEvent(event);

			start = end;
		}

		PrepAllS(mdl, mdesc, first);
		CalcAllSSRates(mdl, mdesc);	//now all the rates & derivates are in
		OutputDataInternalPre(mdl, 1.0, sim->binctr);
		change = ProcessAllSSRates(mdl, mdesc);
		if(change <= SS_MINPERCENT * SS_MINPERCENT)	//the EASIEST way to get outta here!
			break;
		first = false;
		
//		OutputDataInternalPost(mdl, *(mdesc.simulation), 0);
//		sim->binctr++;
//		OutputDataToFile(mdl, mdesc);
		//debug - I need to see how the code changes
		RecordAllSSRates(mdl, mdesc);	
		

		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
			break;
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);
		if(kThread->shareData.GUIsaveState)
		{
			DisplayProgressDoub(12, mdesc.simulation->TransientData->iterEndtime);
			ProgressInt(42, mdesc.simulation->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			kThread->shareData.GUIsaveState = false;	//reset boolean
		}

		mdesc.simulation->curiter++;
	}

	sim->curiter = 0;	//set it at the lowest file
	PrepAllS(mdl,mdesc, false);	//now get the data properly setup for the current state
	CalcAllSSRates(mdl, mdesc);	//get the updated rates
	RecordAllSSRates(mdl, mdesc);	//now record the rates instead of processing them

	sim->OutputFName = tmpOutF;
	

	return(0);
}

int GetStartCharge(Model& mdl, ModelDescribe& mdesc)
{
	PosNegSum charge;
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->second.cb.energies.begin(); cbit != pt->second.cb.energies.end(); ++cbit)
			charge.SubtractValue(cbit->second.GetBeginOccStates());

		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->second.vb.energies.begin(); vbit != pt->second.vb.energies.end(); ++vbit)
			charge.AddValue(vbit->second.GetBeginOccStates());

		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.acceptorlike.begin(); eit != pt->second.acceptorlike.end(); ++eit)
		{
			charge.AddValue(eit->second.GetNumstates() * eit->second.charge);	//if charge is zero, this does nothing
			charge.AddValue(eit->second.GetBeginOccStates());
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.donorlike.begin(); eit != pt->second.donorlike.end(); ++eit)
		{
			charge.AddValue(eit->second.GetNumstates() * eit->second.charge);	//if charge is zero, this does nothing
			charge.SubtractValue(eit->second.GetBeginOccStates());
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.CBTails.begin(); eit != pt->second.CBTails.end(); ++eit)
			charge.SubtractValue(eit->second.GetBeginOccStates());

		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.VBTails.begin(); eit != pt->second.VBTails.end(); ++eit)
			charge.AddValue(eit->second.GetBeginOccStates());

		pt->second.Output.Allocate(mdesc.simulation->outputs.numinbin);
	}

	mdesc.simulation->negCharge = charge.GetNegSum() * QCHARGE;
	mdesc.simulation->posCharge = charge.GetPosSum() * QCHARGE;
	mdesc.simulation->TransientData->iterEndtime = mdesc.simulation->TransientData->curTargettime;	//move things up to the next target to process the next bit. Say this iteration took care of this section, which it did
	OutputCharge(mdesc.simulation, false);
	return(0);
}

double PrepAllS(Model& mdl, ModelDescribe& mdesc, bool first)
{
	//clear out all light data
	mdesc.simulation->OpticalFluxOut->ClearAll();
	for(unsigned int i=0; i<mdesc.materials.size(); i++)
		mdesc.materials[i]->OpticalFluxOut->ClearAll();

//	AdaptiveMesh(mdl, mdesc);
	

	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		//not sure if this is actually necessary...
		pt->second.Jn.x = pt->second.Jn.y = pt->second.Jn.z = pt->second.Jp.x = pt->second.Jp.y = pt->second.Jp.z = 0.0;
		pt->second.genth = pt->second.recth = pt->second.scatterR = 0.0;
		pt->second.srhR1 = pt->second.srhR2 = pt->second.srhR3 = pt->second.srhR4 = pt->second.netCharge = 0.0;
		pt->second.LightAbsorbDef = pt->second.LightAbsorbGen = pt->second.LightAbsorbScat = pt->second.LightAbsorbSRH = 0.0;
		pt->second. LightEmitDef = pt->second.LightEmitRec = pt->second.LightEmitScat = pt->second.LightEmitSRH = 0.0;
		pt->second.LightPowerEntry = pt->second.LightEmission = 0.0;	//mW
		if(pt->second.LightEmissions)
			pt->second.LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!
		pt->second.largestOccCB = pt->second.largestOccES = pt->second.largestOccVB = pt->second.largestUnOccES = 0.0;

		PrepELevelS(pt->second.cb.energies, mdl, pt->second, mdesc.simulation, first);
		PrepELevelS(pt->second.vb.energies, mdl, pt->second, mdesc.simulation, first);
		PrepELevelS(pt->second.acceptorlike, mdl, pt->second, mdesc.simulation, first);
		PrepELevelS(pt->second.donorlike, mdl, pt->second, mdesc.simulation, first);
		PrepELevelS(pt->second.CBTails, mdl, pt->second, mdesc.simulation, first);
		PrepELevelS(pt->second.VBTails, mdl, pt->second, mdesc.simulation, first);

		pt->second.ResetData();	//get the data correct throughout the point
		mdesc.simulation->PrepPointBandStructure(mdl, pt->second);	//and update mdl.poisson rho with the correct charge
	}

		//this will update the band structure

	return(mdesc.simulation->InitIteration(mdl, mdesc));
}


int PrepELevelS(std::multimap<MaxMin<double>,ELevel>& states, Model& mdl, Point& pt, Simulation* sim, bool first)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = states.begin(); eLev != states.end(); ++eLev)
	{
		if(first)
			eLev->second.percentTransfer = SS_MINPERCENT;	//start off safely

//		FindAllTargets(eLev->second, &pt, mdl);

		if(eLev->second.prevAdjust == true)	//it was prevented from being adjusted before, so don't screw up data used for taylor series expansion
		{
			eLev->second.netRateM2 = eLev->second.netRateM1;
			eLev->second.netRateM1 = eLev->second.netRate;
//			eLev->second.occM2 = eLev->second.occM1;
//			eLev->second.occM1 = eLev->second.GetBeginOccStates();
//			eLev->second.EminEfM2 = eLev->second.EminEfM1;
//			eLev->second.EminEfM1 = eLev->second.EminEf;
		}

		eLev->second.SetBeginOccStates(eLev->second.GetEndOccStates());
	
//		eLev->second.RateEfOut = 0.0;
//		eLev->second.RateEfIn = 0.0;
//		eLev->second.maxREfIn = 0.0;
//		eLev->second.maxREfOut = 0.0;


		eLev->second.Rates.clear();
		eLev->second.OpticalRates.clear();
		eLev->second.RateSums.Clear();
	}

	return(0);
}

int CalcAllSSRates(Model& mdl, ModelDescribe& mdesc)
{
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		pt->second.chargeRate = 0.0;
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.cb.energies, mdesc.simulation);
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.vb.energies, mdesc.simulation);
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.donorlike, mdesc.simulation);
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.acceptorlike, mdesc.simulation);
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.CBTails, mdesc.simulation);
		pt->second.chargeRate += CalcSSERates(mdl, pt->second, pt->second.VBTails, mdesc.simulation);
		pt->second.chargeRate = fabs(pt->second.chargeRate);
	}
	for(unsigned int i=0; i<mdesc.simulation->contacts.size(); i++)
		ProcessContactTransient(mdl, mdesc.simulation->contacts[i], mdesc.simulation);
	
	Light(mdl, mdesc, NULL);	//now update all the light rates

	mdesc.simulation->TransientData->tStepControl.ReInit();	//make sure it doesn't keep having lots of data in here from the rate calculations

	return(0);
}

double CalcSSERates(Model& mdl, Point& pt, std::multimap<MaxMin<double>,ELevel>& states, Simulation* sim)
{
	double chargeRate = 0.0;
	PosNegSum derivs;
	PosNegSum derivSOnly;
	PosNegSum derivs2;
	for(std::multimap<MaxMin<double>,ELevel>::iterator st = states.begin(); st != states.end(); ++st)
	{
		for(std::vector<TargetE>::iterator trg = st->second.TargetELevDD.begin(); trg != st->second.TargetELevDD.end(); ++trg)
		{
			std::vector<ELevelRate>::iterator ret = CalcRate(&(st->second), trg->target, sim);	//calculate the rates to all target energy levels
			if(ret != st->second.Rates.end())	//add in the derivatives in a low precision loss manner
			{
				derivs.AddValue(ret->deriv);
				derivs2.AddValue(ret->deriv2);
				derivSOnly.AddValue(ret->derivsourceonly);
			}
		}
		for(std::vector<ELevel*>::iterator trg = st->second.TargetELev.begin(); trg != st->second.TargetELev.end(); ++trg)
		{
			std::vector<ELevelRate>::iterator ret = CalcRate(&(st->second), *trg, sim);	//calculate the rates to all target energy levels
			if(ret != st->second.Rates.end())	//add in the derivatives in a low precision loss manner
			{
				derivs.AddValue(ret->deriv);
				derivs2.AddValue(ret->deriv2);
				derivSOnly.AddValue(ret->derivsourceonly);
			}
		}
		st->second.derivsourceonly = derivSOnly.GetNetSumClear();
		st->second.deriv = derivs.GetNetSumClear();
		st->second.deriv2 = derivs2.GetNetSumClear();

		double chargeRateChange = st->second.RateSums.GetNetSum();	//this will also add it for later that it can just recal
		if(st->second.carriertype == ELECTRON)	//rate in (positive) is negative charge.
			chargeRate -= chargeRateChange;
		else
			chargeRate += chargeRateChange;
	}
	return(chargeRate);
}

double ProcessAllSSRates(Model& mdl, ModelDescribe& mdesc)
{
	double totChange = 0.0;

	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->second.cb.energies.begin(); cbit != pt->second.cb.energies.end(); ++cbit)
			totChange += fabs(ProcessELevel(&(cbit->second), mdesc.simulation));
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->second.vb.energies.begin(); vbit != pt->second.vb.energies.end(); ++vbit)
			totChange += fabs(ProcessELevel(&(vbit->second), mdesc.simulation));
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.acceptorlike.begin(); eit != pt->second.acceptorlike.end(); ++eit)
			totChange += fabs(ProcessELevel(&(eit->second), mdesc.simulation));
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.donorlike.begin(); eit != pt->second.donorlike.end(); ++eit)
			totChange += fabs(ProcessELevel(&(eit->second), mdesc.simulation));
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.CBTails.begin(); eit != pt->second.CBTails.end(); ++eit)
			totChange += fabs(ProcessELevel(&(eit->second), mdesc.simulation));
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.VBTails.begin(); eit != pt->second.VBTails.end(); ++eit)
			totChange += fabs(ProcessELevel(&(eit->second), mdesc.simulation));


		pt->second.ResetData();
		ContactCurrent(pt->second, mdesc, mdl);	//this will adjust carriers and rho appropriately if it is actually a contact
			
		mdl.poissonRho[pt->second.adj.self] = pt->second.rho;	//update rho used in band diagram calculations
//		mdl.ptZero->ResetData();
	}
	return(totChange);
}



double ProcessELevel(ELevel* eLev, Simulation* sim)
{
//	double nRate1, nRate2, nRate3, occ1, occ2, occ3;
	eLev->netRate = eLev->RateSums.GetNetSumClear();
	
	if(eLev->netRate == 0.0)	//it's steady state, DO NOTHING!
		return(0);

	//this is needed to maintain charge. Badly. Otherwise it diverges very quickly
	if((eLev->netRate > 0.0 && eLev->carriertype == ELECTRON && -sim->negCharge > sim->posCharge) ||	//there is more negative charge than there should be, and this is trying to add negative charge
	   (eLev->netRate < 0.0 && eLev->carriertype == ELECTRON && -sim->negCharge < sim->posCharge) ||	//there is not enough negative charge and this is trying to take away negative charge
	   (eLev->netRate > 0.0 && eLev->carriertype == HOLE && -sim->negCharge < sim->posCharge) ||	//there is not enough negative charge, and this is trying to add postive charge
	   (eLev->netRate < 0.0 && eLev->carriertype == HOLE && -sim->negCharge > sim->posCharge))	//there is not enough positive charge, and this is trying to take away positive charge
	{
		eLev->prevAdjust = false;	//set flag so it doesn't update all the net rate, difEf previous iteration stuff
		return(0.0);
	}
	
	eLev->prevAdjust = true;
	double occ = eLev->GetBeginOccStates();
	double numst = eLev->GetNumstates();
	double change = 0.0;	//how much the fermi level is going to change
	double ret1, ret2;	//these are set in TaylorSecondOrder
	/*
	eLev->CalcRelFermiEnergy(BEGINOCC, true);	//update EminEf via BeginOccStates - which should match endoccstates
	
	if(TaylorSecondOrder(eLev->EminEf, eLev->EminEfM1, eLev->EminEfM2, eLev->netRate, eLev->netRateM1, eLev->netRateM2, ret1, ret2) == GOODRET)
	{
		//now check to make sure this actually is a good return conceptually
		if((eLev->netRate > 0.0 && eLev->carriertype == ELECTRON) ||
			(eLev->netRate < 0.0 && eLev->carriertype == HOLE))	// talking about a gain of negative charge, so Ef increases
		{
			if(ret1 > 0 && ret2 > 0.0)
				change = (ret1 < ret2) ? ret1 : ret2;	//choose the smaller change to hit zero
			else if(ret1 > 0)
				change = ret1;
			else if(ret2 > 0)
				change = ret2;
			//else the returns were bad, don't update change
		}
		else
		{
			if(ret1 < 0 && ret2 < 0)
				change = (ret1 > ret2) ? ret1 : ret2;	//grab the smaller magnitude
			else if(ret1 < 0)
				change = ret1;
			else if(ret2 < 0)
				change = ret2;
		}
	}
	*/
	UpdatePercentTransfer(eLev);	//look at the net rates and see if we should be trying to converge or prevent divergence
	/*
	if(change == 0.0)	//we have not found a solution yet
	{
		if((eLev->netRate >  0.0 && eLev->carriertype == ELECTRON) ||
			(eLev->netRate <  0.0 && eLev->carriertype == HOLE))
		{
			change = eLev->percentTransfer;	//it wants to add electrons in, so Ef should be higher, but only slightly
		}
		else
		{
			change = -eLev->percentTransfer;
		}
	}

	
	double maxChange = (occ > 0.5 * numst) ? numst - occ : occ;	//get the shorter of the distances to full or empty
	if(maxChange <= 0.0)
		maxChange = numst * eLev->percentTransfer;
	
	if(flag != SS_DONE)
	{
		change = (eLev->netRate > 0.0) ? eLev->percentTransfer * (numst - occ) : -eLev->percentTransfer * occ;	//just move it in the right direction
		maxChange = maxChange * eLev->percentTransfer;
	}
	change = change * eLev->percentTransfer;
	*/

	double maxChange;	//the maximum amount Ef can change by.
	/*
	if(eLev->netRate > 0.0)
		maxChange = eLev->maxREfIn / eLev->RateEfIn * eLev->percentTransfer;
	else
		maxChange = eLev->maxREfOut / eLev->RateEfOut * eLev->percentTransfer;
		
	//maxChange = 0.01 * eLev->percentTransfer;

	if(change > maxChange)
		change = maxChange;
	else if(change < -maxChange)
		change = -maxChange;
		
	//now the change in Ef needs to be translated into a number of carriers
	eLev->UpdateAbsFermiEnergy(BEGINOCC);	//make sure this is accurate
	double newocc = numst * eLev->CalcProbFromFermi(eLev->AbsFermiEnergy + change);
	change = DoubleSubtract(newocc, occ);

	
	//now let's restrict changes based on rho and the band diagram
	if(eLev->ptrPoint->largestPsiCf > 0.0 && eLev->ptrPoint->chargeRate > 0.0)	//both these should be absolute  value, and above zero - avoid divide by zero and zero as answer
	{
		maxChange = fabs(maxChange * eLev->netRate  / (eLev->ptrPoint->chargeRate * eLev->ptrPoint->largestPsiCf));
		//maxChange initially represents how much we're allowing the fermi level to move. We're going to allow the bands to shift that much
		//as well. The netRate/PointchargeRate simply gives how much leeway is allowed as other energy levels will gain and lose carriers
		//the largestPsiCf describes the maximum change that may result in the band for the transfer of one carrier into this particular state
		
	}
	else  //unless the chargeRate is zero, this should NEVER occur.
	{
		maxChange = 0.01 * maxChange / eLev->ptrPoint->largestPsiCf;	//allowed change in band structure / how much the band structure changes per carrier - and just make it small
	}

	if(change > maxChange)
		change = maxChange;
	else if(change < -maxChange)
		change = -maxChange;
	*/
	
	//the second derivative does not seem to be helping it converge at all
	//previous method of first deriv, no SS_Max that is reset on rate direction chage worked best
	
	if(TaylorSecondOrder(0.0, eLev->netRate, eLev->deriv, 0.0, ret1, ret2))	//does linear and quadratic solution
	{  //set 2nd derivative to 0. Made return very bad...
		//check for a good return concept wise
		if(eLev->netRate > 0.0)	// talking about a gain of negative charge, so Ef increases
		{
			if(ret1 > 0 && ret2 > 0.0)
				change = (ret1 < ret2) ? ret1 : ret2;	//choose the smaller change to hit zero
			else if(ret1 > 0)
				change = ret1;
			else if(ret2 > 0)
				change = ret2;
			//else the returns were bad, don't update change
		}
		else if(eLev->netRate < 0.0)
		{
			if(ret1 < 0 && ret2 < 0)
				change = (ret1 > ret2) ? ret1 : ret2;	//grab the smaller magnitude
			else if(ret1 < 0)
				change = ret1;
			else if(ret2 < 0)
				change = ret2;
		}
	}
//	eLev->desireChange = change;
	change = change * eLev->percentTransfer;
	/*
	if(eLev->prevChange > 0.0)
	{
		maxChange = eLev->prevChange * SS_MAXCHANGEPREV;	//when removing max percent, this will act as a safe limit.
	
		if(change > maxChange)
			change = maxChange;
		else if(change < -maxChange)
			change = -maxChange;
	}
	*/
	//and one last check to make sure it doesn't go beyond the bounds of the state
	if(eLev->netRate > 0.0)
		maxChange = 0.5*(numst - occ);	//there's this much space available
	else
		maxChange = 0.5*occ;	//only let half the carriers be taken away at most!

	if(change > maxChange)
		change = maxChange;
	else if(change < -maxChange)
		change = -maxChange;

//	eLev->prevChange = fabs(change);

//	change = (occ > 0.5 * numst) ? (numst - occ) : occ;	//grab the shortest distance
//	change = (eLev->netRate > 0.0) ? change * eLev->percentTransfer : -change * eLev->percentTransfer;
//	eLev->AddBeginOccStates(change);
	eLev->AddEndOccStates(change);
		
	return(change);


}

int UpdatePercentTransfer(ELevel* elev)
{
	
	if((elev->netRate > 0.0 && elev->netRateM1 < 0.0) || (elev->netRate < 0.0 && elev->netRateM1 > 0.0)) //changed signs
	{
//		if(elev->percentTransfer > SS_MAXPERCENT)
//			elev->percentTransfer = SS_MAXPERCENT * SS_DECREASEPERCENT;
//		elev->prevChange = 0.0;

		elev->percentTransfer = elev->percentTransfer * SS_DECREASEPERCENT;
	}
	else if((elev->netRate > 0.0 && elev->netRate < elev->netRateM1 && elev->netRateM1 <= elev->netRateM2) ||
		(elev->netRate < 0 && elev->netRate > elev->netRateM1 && elev->netRateM1 >= elev->netRateM2))
		elev->percentTransfer = elev->percentTransfer * SS_INCREASEPERCENT;
	else if(fabs(elev->netRate) <= fabs(elev->netRateM1))	//they are the same sign and getting smaller
	{
		elev->percentTransfer = elev->percentTransfer * SS_SOFTINCREASEPERCENT;
	}
//	else //the rate is getting higher with each iteration. It shouldn't unless the neighbor changes drastically
//		elev->percentTransfer = elev->percentTransfer * SS_DECREASEPERCENT;
//	else //same sign and getting smaller
//	{
//		elev->percentTransfer = elev->percentTransfer * SS_SOFTINCREASEPERCENT;	//it stayed the same rate, so try to step in the correct direction a little more
//	}
	/*
	if(fabs(elev->netRate) < fabs(elev->netRateM1))	//the net rate is shrinking! change should be converging on the correct answer
		elev->percentTransfer = elev->percentTransfer * SS_INCREASEPERCENT;	//it's going in the right direction, so use more of the desired change to get rate = 0
	else if(elev->netRate == elev->netRateM1)
		elev->percentTransfer = elev->percentTransfer * SS_SOFTINCREASEPERCENT;	//it stayed the same rate, so try to step in the correct direction a little more
	else
		elev->percentTransfer = elev->percentTransfer * SS_DECREASEPERCENT;	//the net rate is increasing, so don't adjust it quite as much
		*/

	//the derivative determing how much to transfer has a very strong second derivative.
	//going to see if not having a max percent transfer speeds up convergence (though I suspect it will initially speed up divergence)
	//make sure it stays within the limits

	//the max might be more critical when including second derivative...
	if(elev->percentTransfer > SS_MAXPERCENT)
		elev->percentTransfer = SS_MAXPERCENT;

	if(elev->percentTransfer < SS_MINPERCENT)
		elev->percentTransfer = SS_MINPERCENT;

	return(0);
}

int RecordAllSSRates(Model& mdl, ModelDescribe& mdesc)
{
	PosNegSum charge;
//	mdl.ptZero->ResetData();
	for(std::map<long int,Point>::iterator pt = mdl.gridp.begin(); pt!=mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator cbit = pt->second.cb.energies.begin(); cbit != pt->second.cb.energies.end(); ++cbit)
		{
			RecordELevelSSRates(cbit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(cbit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator vbit = pt->second.vb.energies.begin(); vbit != pt->second.vb.energies.end(); ++vbit)
		{
			RecordELevelSSRates(vbit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(vbit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.acceptorlike.begin(); eit != pt->second.acceptorlike.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.donorlike.begin(); eit != pt->second.donorlike.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.CBTails.begin(); eit != pt->second.CBTails.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eit = pt->second.VBTails.begin(); eit != pt->second.VBTails.end(); ++eit)
		{
			RecordELevelSSRates(eit->second.Rates, pt->second, mdesc.simulation);
			RecordELevelSSRates(eit->second.OpticalRates, pt->second, mdesc.simulation);
		}

		pt->second.srhR1 = pt->second.srhR1 * pt->second.voli;	//get rates in normal units instead of just # of carriers
		pt->second.srhR2 = pt->second.srhR2 * pt->second.voli;	//that moved/sec. While useful for seeing where steps are,
		pt->second.srhR3 = pt->second.srhR3 * pt->second.voli;	//having actual rates makes life a bit easier...
		pt->second.srhR4 = pt->second.srhR4 * pt->second.voli;
		pt->second.genth = pt->second.genth * pt->second.voli;
		pt->second.recth = pt->second.recth * pt->second.voli;
		pt->second.scatterR = pt->second.scatterR * pt->second.voli;
		pt->second.LightAbsorbDef = pt->second.LightAbsorbDef * pt->second.voli;
		pt->second.LightAbsorbSRH = pt->second.LightAbsorbSRH * pt->second.voli;
		pt->second.LightAbsorbGen = pt->second.LightAbsorbGen * pt->second.voli;
		pt->second.LightAbsorbScat = pt->second.LightAbsorbScat * pt->second.voli;
		pt->second.LightEmitDef = pt->second.LightEmitDef * pt->second.voli;
		pt->second.LightEmitRec = pt->second.LightEmitRec * pt->second.voli;
		pt->second.LightEmitScat = pt->second.LightEmitScat * pt->second.voli;
		pt->second.LightEmitSRH = pt->second.LightEmitSRH * pt->second.voli;
		pt->second.ResetData();	//update all internal variables for outputdatafinal
		charge.AddValue(pt->second.netCharge);	//this will now just be net charges
		//so if 2 e- and a hole are in, it will just do 1e-.
	}

	mdesc.simulation->negCharge = charge.GetNegSum() * QCHARGE;
	mdesc.simulation->posCharge = charge.GetPosSum() * QCHARGE;
	mdesc.simulation->TransientData->iterEndtime = mdesc.simulation->TransientData->curTargettime;	//move things up to the next target to process the next bit. Say this iteration took care of this section, which it did
	OutputCharge(mdesc.simulation, false);

	OutputDataInternalPost(mdl, *(mdesc.simulation), mdesc.simulation->binctr);	//prep the data for output
	mdesc.simulation->binctr++;
	//want store value start time to result in a simple average which is 1.0/(iterendtime - storevaluestarttime)
	mdesc.simulation->TransientData->storevalueStartTime = mdesc.simulation->TransientData->iterEndtime - double(mdesc.simulation->binctr);	//make sure the data doesn't get messed up
	if (mdesc.simulation->binctr == mdesc.simulation->outputs.numinbin)
	{
		OutputDataToFileSS(mdl, mdesc);	//and get the data out to file
		mdesc.simulation->binctr = 0;
	}
	
	return(0);
}
/*
int RecordELevelSSRates(std::vector<ELevelOpticalRate>& rates, Point& pt, Simulation* sim)
{

	for(std::vector<ELevelOpticalRate>::iterator rt = rates.begin(); rt != rates.end(); ++rt)
	{
		switch(rt->type)
		{
			case RATE_LIGHTGEN:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbGen += rt->RateType;
				break;
			case RATE_LIGHTSCAT:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbScat += rt->RateType;
				break;
			case RATE_LIGHTDEF:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbDef += rt->RateType;
				break;
			case RATE_LIGHTSRH:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbSRH += rt->RateType;
				break;
			case RATE_STIMEMISREC:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitRec += rt->RateType;
				break;
			case RATE_STIMEMISDEF:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitDef += rt->RateType;
				break;
			case RATE_STIMEMISSRH:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitSRH += rt->RateType;
				break;
			case RATE_STIMEMISSCAT:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitScat += rt->RateType;
				break;
			case RATE_UNDEFINED:
			default:
				ProgressInt(52, rt->type);
				break;
		}	//end switch statement
	}	//end for loop through all rates



	return(0);
}
*/
int RecordELevelSSRates(std::vector<ELevelRate>& rates, Point& pt, Simulation* sim)
{

	for(std::vector<ELevelRate>::iterator rt = rates.begin(); rt != rates.end(); ++rt)
	{
		switch(rt->type)
		{
			case RATE_GENERATION:
				pt.genth += rt->RateType;
				break;
			case RATE_RECOMBINATION:
				pt.recth += rt->RateType;
				break;
			case RATE_SRH1:
				pt.srhR1 += rt->RateType;
				break;
			case RATE_SRH2:
				pt.srhR2 += rt->RateType;
				break;
			case RATE_SRH3:
				pt.srhR3 += rt->RateType;
				break;
			case RATE_SRH4:
				pt.srhR4 += rt->RateType;
				break;
			case RATE_SCATTER:
				pt.scatterR += rt->RateType;
				break;
			case RATE_DRIFTDIFFUSE:
				rt->RecordDDRate();	//the point in use might not actually be the point that is supposed to get the current applied to output, and direction must be applied
				break;
			case RATE_CONTACT_CURRENT_EXT:
			case RATE_CONTACT_CURRENT_INT:
			case RATE_CONTACT_CURRENT_LINK:
			case RATE_CONTACT_CURRENT_INTEXT:
				//do nothing! Don't know the time step yet...
				break;
			case RATE_LIGHTGEN:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbGen += rt->RateType;
				break;
			case RATE_LIGHTSCAT:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbScat += rt->RateType;
				break;
			case RATE_LIGHTDEF:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbDef += rt->RateType;
				break;
			case RATE_LIGHTSRH:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightAbsorbSRH += rt->RateType;
				break;
			case RATE_STIMEMISREC:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitRec += rt->RateType;
				break;
			case RATE_STIMEMISDEF:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitDef += rt->RateType;
				break;
			case RATE_STIMEMISSRH:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitSRH += rt->RateType;
				break;
			case RATE_STIMEMISSCAT:	//there is no equilibrium value applied here, but keep code consistent in case I do calculate that someday, which would be mildly difficult to do exactly
				pt.LightEmitScat += rt->RateType;
				break;
			case RATE_TUNNEL:
				break;
			case RATE_UNDEFINED:
			default:
				ProgressInt(52, rt->type);
				break;
		}	//end switch statement
	}	//end for loop through all rates



	return(0);
}