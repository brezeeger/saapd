#ifndef THERMAL_GEN_REC_H
#define THERMAL_GEN_REC_H

#include <vector>

class ELevel;
class Simulation;
class ELevelRate;
class PosNegSum;

std::vector<ELevelRate>::iterator CalculateGenerationRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
std::vector<ELevelRate>::iterator CalculateRecombinationRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
double CalcRecRateFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim=NULL, bool GenLight=false, PosNegSum* accuRate=NULL);
double CalcGenRateFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* accuRate=NULL);
int CalcGenRecDerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget);

#endif