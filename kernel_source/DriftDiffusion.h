#ifndef DRIFTDIFFUSION_H
#define DRIFTDIFFUSION_H

#define DD_SAFEOCC_DIVIDE_ZERO 1.0e-280

#include <map>
#include <vector>

#include "RateGeneric.h"


class ELevel;
class Point;
class Simulation;
class PosNegSum;

class SS_CurrentDerivFactors
{
public:
//	double RST;	//rate in as source to target
//	double RTS;	//rate in as target to source
	double factor_ci_srcband;	//what if the other state is in the source band, but not the actual source or target
	double factor_ci_trgband;	//what if the other state is in the target band, but not the actual source or target
	double factor;	//the state is not the source or target. This is factor resulting from shift in fermi levels due to band bending
};

double CalcCurrentFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, bool forceCalc = false, PosNegSum* AccuRate=NULL);
int CalculateCurrentDerivFast(ELevel* srcE, ELevel* trgE, SS_CurrentDerivFactors& derivFactor, double& derivSource, double& derivTarget, int whichSourceSrc, int whichSourceTrg);
std::vector<ELevelRate>::iterator CalculateCurrentRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource = RATE_OCC_BEGIN, bool forceCalc = false);

int CalculateThermalEmissionDerivFast(ELevel* srcESent, ELevel* trgESent, SS_CurrentDerivFactors& derivFactor, double& derivSource, double& derivTarget, int whichSourceSrc, int whichSourceTrg);
double CalculateThermalEmissionRateFast(ELevel* srcESent, ELevel* trgESent, int whichSourceSrc, int whichSourceTrg);
std::vector<ELevelRate>::iterator CalculateThermalEmissionRate(ELevel* srcESent, ELevel* trgESent, Simulation* sim, int whichSource);

double CalculateDifEf(ELevel* srcE, ELevel* trgE, int whichSourceSrc = RATE_OCC_BEGIN, int whichSourceTrg = RATE_OCC_BEGIN);
std::vector<ELevelRate>::iterator CalculateExtremeDD(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
double GetBarrierReduction(ELevel* srcE, Point* trgPt, bool carrierType);
double GetBarrierReduction(Point* srcPt, Point* trgPt, bool carrierType);
double CalcCurrentEQTransfer(ELevel* srcE, ELevel* trgE);
double CalcDistanceEf(ELevel* eLev, int whichSource = RATE_OCC_BEGIN);

int FindPoissonCoeff(Point* pt, long int targetindex, double& ret);


#endif