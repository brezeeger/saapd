#ifndef SAAPD_MATERIALS_H
#define SAAPD_MATERIALS_H

#include<vector>
#include<map>
#include <string>


//external classes:
class Optical;
class BulkOpticalData;
class Impurity;
class XMLFileOut;
class ModelDescribe;

class Corrections	//factors that need multiplying by for various corrections to certain values
{			//for example, ni has a temperature dependence, and everything is reported at 300 kelvin.
public:
	double ni;
	Corrections();
	~Corrections();
};

class NK	//used for materials
{
	double _k;	//extinction coefficient
	double _alpha;	//calculated from k: 4 pi k / lamda_0 -- in cm^-1 to match distance travelled and units in rest of simulation
	float _n;	//index of refraction
public:
	NK(float lambda, double n, double k, double a, bool prefMaintainAlpha);
	~NK() { }

	static const unsigned char absorption_coefficient = 0x00;
	static const unsigned char extinction_coefficient = 0x01;
	static const unsigned char index_refraction = 0x02;
	static const unsigned char wavelength = 0x04;	//not used here technically, but it is the sort

	void UpdateK(double newk);
	void UpdateAlpha(double newa);
	float getN() const { return _n; }
	bool setN(float n) { if (n != 0.0) { _n = n; return true; } return false; }
	double getK() const { return _k; }
	double getAlpha() const { return _alpha; }
	void getNKA(double& n, double& k, double &a) { n = _n; k = _k; a = _alpha; }
	double getValue(unsigned char which) const {
		if (which == extinction_coefficient)
			return _k;
		else if (which == index_refraction)
			return (double)_n;
		return _alpha;	//
	}

	bool isInvalid() const {
		return (_n == -1.0 && _k == -1.0 && _alpha == -1.0);
	}
};

class Optical	//used in a tree. This is data. Sort value is wavelength.
{				//will want to find based on value
	double _a, _b, _n;	//used to generate k/alpha values. N for material wide n if ab is given.
	double opticEfficiency;	//how well recombination produces light (some recombination is by phonons)
	std::vector<std::pair<float, NK>> WavelengthData;	//first value (sort) is the wavelength in nm. This is for a material. Once initialized, it becomes a constant
	unsigned char flags;
//	bool WavelengthUnsorted;	//minimalize the amount of sorting done.
//	bool useAB;	//were the AB values specified or an NK generated. If so, constant n
//	bool suppressOpticalOutputs;	//let it process the light emissions, etc., but don't output the data
//	bool disableOpticalEmissions;	//this material will not do any light emissions whatsoever.

	void setWavelengthUnsorted() { flags |= flg_unsorted; }
	void setWavelengthSorted() { flags &= (~flg_unsorted); }
	bool isWavelengthNeedSort() { return ((flags & flg_unsorted) != 0); }

	double getWavelengthData(float lambda, unsigned char which = NK::absorption_coefficient);
	
public:
	~Optical();
	Optical();

	//values for the flag. minimize the data. Doing everything through interface, so no need to deal make things simple with public bools in code
	static const unsigned char flg_unsorted = 0x01;	//unsorted means there may be duplicates
	static const unsigned char flg_useAB = 0x02;
	static const unsigned char flg_suppressOpticalOutputs = 0x04;
	static const unsigned char flg_disableOpticalEmissions = 0x08;

	bool AddWavelength(float lambda, double n, double k = -1.0, double a = -1.0, bool overWrite = true, bool prefMaintainAlpha = false, bool checkExists = true);	//note lambda in nm and alpha in m. if not check for existence, major performance hit if not overwriting.
	bool RemoveNearestWavelengthLambda(float lambda, float maxDelta=-1.0f);	//defaults to say no limit
	bool RemoveWavelengthIndex(unsigned int index);
	void ClearWavelengths() { WavelengthData.clear(); setWavelengthSorted(); }
	void SaveToXML(XMLFileOut& file);
	
	double calcAlphaAB(float wavelength, double bandGap);
	void setOpticEfficiency(double eff) {
		if (eff > 1.0)
			eff = 1.0;
		else if (eff < 0.0)
			eff = 0.0;
		opticEfficiency = eff;
	}
	double getOpticEfficiency() { return opticEfficiency; }

	double getGlobalN() const { return _n; }
	double getGlobalA() const { return _a; }
	double getGlobalB() const { return _b; }

	double getAlpha(float wavelength, double bandGap = 0.0);	//bandgap only needed if optic data hasn't been populated from AB data.
	double getIndexRefractionWavelength(float lambda);
	double getIndexRefractionEnergy(double energy);
	double getExtinctionCoefficientWavelength(float lambda);
	double getExtinctionCoefficientEnergy(double energy);
	float getWavelength(unsigned int index) { return index < WavelengthData.size() ? WavelengthData[index].first : -1.0; }
	std::pair<float, NK> getWaveData(unsigned int index) { return index < WavelengthData.size() ? WavelengthData[index] : std::pair<float, NK>(-1.0, NK(0.0, 0.0, 0.0, 0.0, false)); }

	void setUseAB() { flags |= flg_useAB; }
	void clearUseAB() { flags &= (~flg_useAB); }
	bool isUseAB() { return ((flags & flg_useAB) != 0); }	//only for when generating the nk data on initialization
	void GenerateWavelengthsFromAB(double startEnergy, double endEnergy, double deltaEnergy, double bandGap);

	void setABN(double a, double b, double n) { _n = (n == 0.0) ? 1.0 : n; _a = a; _b = b;	}
	
	void hideOutputs() { flags |= flg_suppressOpticalOutputs; }
	void showOutputs() { flags &= (~flg_suppressOpticalOutputs); }
	bool isOutputsShown() const { return ((flags & flg_suppressOpticalOutputs) == 0); }

	void enableEmissions() { flags &= (~flg_disableOpticalEmissions); }
	void disableEmissions() { flags |= flg_disableOpticalEmissions; }
	bool isEmissionsEnabled() const { return ((flags & flg_disableOpticalEmissions) == 0); }

	unsigned int getNumWavelengthData() const { return WavelengthData.size(); }
	bool hasWavelengths() const { return !WavelengthData.empty(); }

	bool modifyWaveDataIndex(unsigned int index, double value, unsigned char which);

	void SortWavelengths() {
		VectorFastMergeSort(WavelengthData);
		SortedVectorRemoveEarlyDuplicateKeys(WavelengthData);
		setWavelengthSorted();
	}
};

struct CustomDensStates
{
	double density;	//cm^-3, eV^-1
	//	double energy;	//eV - sort value, but it's easier to also have it in the data section.
	double EFMCond;	//what the partiuclar energy level's effective mass is
};

class Urbach
{
	double energy;	//must be >0 to be considered valid data
	double captureCB;	//capture cross section for carriers from the CB
	double captureVB;	//capture cross section for carriers from the VB
	bool band;	//which band does this apply to?
public:
	static const bool conduction = true;
	static const bool valence = false;

	bool isActive() const { return (energy > 0.0); }

	bool isConductionBand() const { return (band == conduction); }
	void setConductionBand() { band = conduction; }

	bool isValenceBand() const { return (band == valence); }
	void setValenceBand() { band = valence; }

	double getUrbachEnergy() const { return energy; }
	void setUrbachEnergy(double e) { energy = (e >= 0.0) ? e : 0.0; }

	double getCBCaptureCrossSection() const { return captureCB; }
	void setCBCaptureCrossSection(double c) { captureCB = (c >= 0.0) ? c : 0.0; }

	double getVBCaptureCrossSection() const { return captureVB; }
	void setVBCaptureCrossSection(double c) { captureVB = (c >= 0.0) ? c : 0.0; }


};

class Material
{
private:
	ModelDescribe& mdesc;	//pointer to parent!
protected:
	std::string _name;		//the name of the material
	std::string _describe;	//a description of the material
	double _tauThGenRec;	//the "scattering" time constant for generation and recombination
	double _ni;			//intrinsic carrier concentration at instrinsic 300K
	double _Nc;			//effective density of states at conduction band (edge) at 300k. Not really used. Based on EFMeDens
	double _Nv;			//effective DOS at valence band (edge) at 300k. Not really used. Based on EFMhDens
	double _EFMeDens;		//Relative effective mass of electron & hole at intrinsic 300K for density of states
	double _EFMhDens;		//Relative efm of holes for density of states
	double _EFMeCond;		//Relative effective mass of electron & hole at intrinsic 300K for conductivity
	double _EFMhCond;		//Relative efm of holes for conductivity
	double _MuN;			//electron mobility from lattice contributions
	double _MuP;			//hole mobility from lattice contributions (1/mu = 1/mu_lat + 1/mu_other, such as impurities)
	double _Eg;			//band gap at intrinsic 300K.

	//	double _EcEfi;		//Ec - Efi (how far away is the intrinsic fermi energy from Ec?). This isn't used in any calculations
	double _Phi;			//electron affinity at instrinsic 300K. Work function of metal.
	double _SpHeatCap;	//specific heat capacity
	double _ThCond;		//thermal conductivity

	float _epsR;			//relative permittivity.
	float _muR;			//relative permeability
	int _id;				//used to identify the material for layers during load
	short _type;			//metal, semiconductor, insulator, etc.

	//given a material, it will have a particular conduction/valence band. This only needs to be calculated once per material
	//and only once per simulation!
	Corrections _cfactor;	//corrections to the properties above, based on temperature, etc.
	std::vector<Impurity*> _defects;	//the defects in the material (treat as impurities)
	std::vector<std::pair<double, CustomDensStates>> _CBCustom;	//the first is sort, energy. second is data.
	std::vector<std::pair<double, CustomDensStates>> _VBCustom; //the first is sort/energy. second is data.
	BulkOpticalData* _OpticalFluxOut;	//stores the iteration data for carrier/sec for just this material (also for layers, device, etc.)
	BulkOpticalData* _OpticalAggregateOut;	//adds up all the iterations based on carriers for just this material
	Optical _optics;	//optical properties of the material

	Urbach _urbachVB;		//urbach energy of the material. <= 0 is nonexistent
	Urbach _urbachCB;		//urbach energy of the material. <= 0 is nonexistent

	bool _direct;		//is this a direct band gap material? (No means no emission of light)

public:
	const static short undefined = 0;
	const static short semiconductor = 1;
	const static short metal = 2;
	const static short dielectric = 3;
	const static short vacuum = 4;

	const static bool conduction = true;
	const static bool valence = false;

	void CalcCorrections(double temp);
	void SaveToXML(XMLFileOut& file);
	void Validate(ModelDescribe& mdesc);

	Material(ModelDescribe& md, int id = -1, std::string name = "Default(Si)", std::string describe = "Default Silicon Values",
		double eg = 1.12, double mun = 1400.0, double mup = 450.0, double phi = 4.05, float epsR = 11.68f, float muR = 1.0f,
		short type = semiconductor, double ni = 9.2248e9, double nc = 2.812e19, double nv = 1.83e19,
		double efmec = 0.26, double efmhc = 0.386, double efmed = 1.07885, double efmhd = 0.810187,
		double tahThGenRec = 4.545e-4, bool direct = false, Optical* optic = nullptr,
		double urbvbe = 0.0, double urbcbe = 0.0, double urbVbCapVb = 0.0, double urbVbCapCb = 0.0,
		double urbCbCapVb = 0.0, double urbCbCapCb = 0.0, double spHeatCap = 0.0, double Thcond = 0.0);
	~Material();
	//optical holds the data. int is the wavelength in nm.
	//getters and setters
	std::string getName() const { return _name; }		//the name of the material
	std::string getUserDescription() const { return _describe; }	//a description of the material
	double getBandGenRecTau() const { return _tauThGenRec; }	//the "scattering" time constant for generation and recombination
	double getIntrinsicCarrierConcentration() const { return _ni * _cfactor.ni; }		//intrinsic carrier concentration, with temperature correction from 300k
	double getCBEffectiveDOS() const { return _Nc; }			//effective density of states at conduction band (edge) at 300k. Not really used. Based on EFMeDens
	double getVBEffectiveDOS() const { return _Nv; }			//effective DOS at valence band (edge) at 300k. Not really used. Based on EFMhDens
	double getEFM_E_Density() const { return _EFMeDens; }		//Relative effective mass of electron & hole at intrinsic 300K for density of states
	double getEFM_H_Density() const { return _EFMhDens; }		//Relative efm of holes for density of states
	double getEFM_E_Conductance() const { return _EFMeCond; }	//Relative effective mass of electron & hole at intrinsic 300K for conductivity
	double getEFM_H_Conductance() const { return _EFMhCond; }		//Relative efm of holes for conductivity
	double getElectronMobility() const { return _MuN; }			//electron mobility from lattice contributions
	double getHoleMobility() const { return _MuP; }			//hole mobility from lattice contributions (1/mu = 1/mu_lat + 1/mu_other, such as impurities)
	double getBandGap() const { return _Eg; }			//band gap at intrinsic 300K.

	double getAffinity() const { return _Phi; }			//electron affinity at instrinsic 300K. Work function if metal.
	double getIntrinsicWorkFunction() const {	//it might change if doped, so specify intrinsic
		if (_type == metal)
			return _Phi;
		if (_type == semiconductor || _type == dielectric)
			return _Phi + 0.5*_Eg;
		return 0.0;
	}
	double getSpecificHeatCapacity() const { return _SpHeatCap; } //specific heat capacity
	double getThermalConductivity() const { return _ThCond; }		//thermal conductivity

	float getRelativePermittivity() const { return _epsR; }			//relative permittivity.
	float getRelativePermeability() const { return _muR; }			//relative permeability
	int getID() const { return _id; }				//used to identify the material for layers during load
	bool needsNewID() const { return _id < 0; }
	bool isMaterial(int id) const { return (_id >= 0 && id == _id); }

	bool isSemiconductor() const { return (_type == semiconductor); }
	bool isMetal() const { return (_type == metal); }
	bool isDielectric() const { return (_type == dielectric); }
	bool isVacuum() const { return (_type == vacuum); }
	short getType() const { return _type; }			//metal, semiconductor, insulator, etc.

	bool isDirect() const { return _direct; }
	bool isIndirect() const { return !_direct; }

	bool isOpticalOutputsSuppressed() const { return !_optics.isOutputsShown(); }
	bool isOpticalEmissionsEnabled() const { return _optics.isEmissionsEnabled(); }

	Optical& getOpticalData() { return _optics; }
	double getIndexRefractionWavelength(float lambda) const {
		return _optics.getIndexRefractionWavelength(lambda);
	}

	unsigned int getNumDefects() const { return _defects.size(); }
	bool hasDefects() const { return !_defects.empty(); }
	Impurity* getDefectByIndex(unsigned int i) const { return ((i < _defects.size()) ? _defects[i] : nullptr); }
	Impurity* getDefectByID(int id) const;
	Impurity* getDefectByName(std::string name) const;
	void pairWithDefect(Impurity* defect);
	bool unpairWithDefect(Impurity* defect);
	void clearDefects() { _defects.clear(); }	//note, material does not own impurity data

	Urbach& getUrbachCB() { return _urbachCB; }	//return reference. That data is already protected
	Urbach& getUrbachVB() { return _urbachVB; }	//return reference. That data is already protected
	
	bool SetCustomState(bool band, double energy, double efm, double density);	//if done in order/adding to back
	bool SetCBCustomState(double energy, double efm, double density); //the insert time is amortized
	bool SetVBCustomState(double energy, double efm, double density); //basically bit of logic + insert at end=push_back

	bool hasCustomCBStates() const { return !_CBCustom.empty(); }
	bool hasCustomVBStates() const { return !_VBCustom.empty(); }

	bool AddOpticalData(unsigned char which, double Energy, double photons);
	double GetOpticalIntensity(unsigned char which, double Energy);
	bool AggregateOpticalData(double factor = 1.0);
	void ResetOpticalFlux(bool includeAggregate = false);
	
	void SetID(int id) { _id = id; }

	void MarkNeedNewID() { _id = -1; }

	void SetName(std::string name) {
		if (name.size() > 19)
			name.resize(19);
		_name = name;
	}
	void SetDescription(std::string desc) {
		if (desc.size() > 400)
			desc.resize(400);
		_describe = desc;
	}

	void SetGenRecTimeConstant(double tau) { //zero uses average scattering tau's for energy state
		_tauThGenRec = (tau < 0.0) ? 0.0 : tau;
	}

	void SetStandardIntrinsicCarrierConcentration(double ni) {
		_ni = (ni < 0.0) ? 0.0 : ni;
	}

	void SetCBEffectiveDOS(double nc) {
		_Nc = (nc < 0.0) ? 0.0 : nc;
	}

	void SetVBEffectiveDOS(double nv) {
		_Nv = (nv < 0.0) ? 0.0 : nv;
	}

	void SetElectronEFMDensity(double efm) {
		_EFMeDens = (efm > 0.0) ? efm : 1.0;
	}

	void SetElectronEFMConduction(double efm) {
		_EFMeCond = (efm > 0.0) ? efm : 1.0;
	}

	void SetHoleEFMDensity(double efm) {
		_EFMhDens = (efm > 0.0) ? efm : 1.0;
	}

	void SetHoleEFMConduction(double efm) {
		_EFMhCond = (efm > 0.0) ? efm : 1.0;
	}

	void SetElectronMobility(double mun) {		//no mobility means pretty much infinite scattering and no e- current
		_MuN = (mun > 0.0) ? mun : 0.0;
	}

	void SetHoleMobility(double mup) { //no mobility means pretty much infinite scattering and no e- current
		_MuP = (mup > 0.0) ? mup : 0.0;
	}

	void SetBandGap(double eg, bool autoType=true) {
		_Eg = eg;	//negative/zero band gaps are possible. commonly known as a metal.
		if (autoType) {
			if (eg < 0.05)
				_type = metal;
			else if (eg < 5.0)
				_type = semiconductor;
			else if (eg < 20.0)
				_type = dielectric;
			else
				_type = vacuum;
		}
	}

	void SetElectronAffinity(double phi) {
		_Phi = (phi > 0.0) ? phi : 0.0;
	}

	void SetSpecificHeatCapacity(double s) {
		_SpHeatCap = (s > 0.0) ? s : 0.0;
	}

	void SetThermalConductivity(double t) {
		_ThCond = (t > 0.0) ? t : 0.0;
	}

	void SetRelativePermittivity(double eps) {
		_epsR = (eps > 0.0) ? eps : 1.0;
	}

	void SetRelativePermeability(double mu) {
		_muR = (mu > 0.0) ? mu : 1.0;
	}

	void SetType(short type) {
		if (type < semiconductor || type > vacuum) { //invalid value
			if (_type < semiconductor || _type > vacuum) //it was previously invalid
				_type = undefined; //make sure it's the proper invalid
			return;
		}
		_type = type;
	}

	void UpdateTemperatureCorrections(double temp);

	void SetDirectBandgap(bool tryUpdateOpticEfficiency=false) {
		_direct = true;
		if (tryUpdateOpticEfficiency) {
			if (_optics.getOpticEfficiency() < 0.2)
				_optics.setOpticEfficiency(1.0);
		}
	}

	void SetIndirectBandgap(bool tryUpdateOpticEfficiency = false) {
		_direct = false;
		if (tryUpdateOpticEfficiency) {
			if (_optics.getOpticEfficiency() > 0.6)
				_optics.setOpticEfficiency(0.1);
		}
	}

	
};

#endif