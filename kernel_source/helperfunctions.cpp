#include "helperfunctions.h"


#include "BasicTypes.h"
#include "model.h"
#include "physics.h"
#include "random.h"


extern MyRand SciRandom;

double ProbFraction(double num)
{
	double answer = floor(num);	//just take the fraction
	double random = SciRandom.Uniform01();	//random # from 0 to 1

//	if(num > 0)
	if(random < num-answer)	//if the random number is less than the fractional part of the number
		answer = ceil(num);	//then round up

//	if(num < 0)
//		if(random < num-answer)
//			answer = ceil(num);
		//this function is identical for both positive and negative numbers. Which I guess makes sense, it is essentially shifted
		//to the 0-1 frame no matter what.

	return(answer);
}

double Round(double num)
{
	return(floor(num+0.5));
}

void SetDoubleNearestInt(double& num)	//often, values become something like 4.00000000000167. That 167 at the end really screws things up!
{
	double num2 = floor(num + 0.5);
	num = num2;
	return;
}

void ToLower(std::string& str)
{
	for (unsigned int i = 0; i<str.size(); i++)
	{
		str[i] = char(tolower(str[i]));	//just remove the warning by forcing the type-cast
		//int = tolower(int)
		//we have char = tolower(char). There will be no loss of data, because the conversion will just pad 0's
	}
	return;
}

double FermiFn(double Ef, double E, double kTi, bool inverse, bool AbsZero, double degen)
{
	double fermi = E-Ef;

	if(inverse)	//1 - f(E)
	{
		fermi = -fermi;
		degen = 1.0 / degen;	//the degeneracy gets divided in the occupational probability
	}
	if(AbsZero == false)
	{
		fermi = fermi * kTi;	// (E-Ef)/kT
		fermi = degen * exp(fermi) + 1.0;	// exp((E-Ef)/kT)+1
		fermi = 1.0/fermi;	// 1.0 / [ e^((E-Ef)/kT) + 1]
	}
	else	//temperature is zero...
	{
		if(fermi > 0.0)	//positive exponent in denominator
			fermi = 0.0;
		else if (fermi < 0.0)
			fermi = 1.0;	//1.0 / (0+1)
		else
			fermi = 1.0/(1.0+degen);	//by definition, the probability is one half at E=Ef.
	}
	return(fermi);
}

XSpace DetermineDirection(Point* PrevPt, Point* CurPt)
{
	XSpace direction;
	XSpace vect;
	vect = CurPt->position - PrevPt->position;
	double magi = vect.Magnitude();
	if(magi <= 0.0)
		return(direction);	//will return (0,0,0). Was same point, so it didn't go anywhere. Also prevent divide by zero. :/
	magi = 1.0 / magi;
	direction.x = vect.x * magi;
	direction.y = vect.y * magi;
	direction.z = vect.z * magi;
	return(direction);
}

//returns single coordinate largest magnitude with positive Left->Right distance starting from src, and an int (P/N)(X/Y/Z) detailing direction
int DistancePtsMainCoord(Point* src, Point* trg, double& ret)
{
	ret=0.0;
	int dir=-1;
	if(src==NULL || trg==NULL)
		return(dir);
	XSpace dist = trg->position - src->position;
	if(fabs(dist.x) >= fabs(dist.y) && fabs(dist.x) >= fabs(dist.z))
	{
		ret= dist.x;
		dir=dist.x>0 ? PX: NX;
	}
	else if(fabs(dist.y) >= fabs(dist.z))
	{
		ret = dist.y;
		dir = dist.y>0 ? PY: NY;
	}
	else
	{
		ret = dist.z;
		dir = dist.z>0 ? PZ : NZ;
	}
	return(dir);
}

int CalcPascalTriangle(std::vector<double>& ret, unsigned int power, bool inverse)
{
	ret.clear();
	ret.resize(power + 1);
	ret[0] = 1.0;
	double row = (double)power + 1.0;

	for(unsigned int i=1; i<=power; i++)
	{
		double value = ret[i-1] * (row - double(i)) / double(i);
		SetDoubleNearestInt(value);
		ret[i] = value;
	}
	if(inverse)
	{
		for(unsigned int i=1; i<= power; i+=2)
		{
			ret[i] = -ret[i];
		}
	}
	return(0);
}

//the following functions are used to try and avoid noise corruptiung various values. A double is accurate to 15.9 digits, roughly.
//therefore, only compare the accuracies to 14 digits.
//will probably eventually need to make a class of my own values
double DoubleSubtract(double A, double B)
{
	double ret = A - B;
//	double logA = 1.0;
	double fA = fabs(A);
//	if(A != 0.0)
//		logA = log10(fA);
	if(ret != 0.0)
		if(fabs(ret) < fA * 1e-13)	//cut out rounding issues during subtractions.
			ret = 0.0;
	return(ret);
}
bool IsSignificant(double src, double adjust, double accuracy)
{
	if (adjust == 0.0)
		return(false);
	return(fabs(adjust) >= fabs(src*accuracy));
}
bool DoubleEqual(double A, double B, double accuracy)
{
	double val = A-B;
//	double logA = 1.0;
	double fA = fabs(A);
//	if(A != 0.0)
//		logA = log10(fA);
	if(val == 0.0)
		return(true);

	if(fabs(val) < fA * accuracy)
			return(true);

	return(false);
}

bool XSpaceEqual(XSpace A, XSpace B)
{
	if(DoubleEqual(A.x, B.x) == false)
		return(false);

	if(DoubleEqual(A.y, B.y) == false)
		return(false);

	return(DoubleEqual(A.z, B.z));
}

double DoubleAdd(double A, double B)
{
	double ret = A + B;
	double compare = fabs(ret);
	double fA = fabs(A);
//	double loga = 1.0;

//	if(A != 0.0)
//		loga = log10(fA);
	if(ret != 0.0)
		if(compare < fA * 1e-13)	//cut out rounding issues during subtractions.
			ret = 0.0;
	return(ret);
}

bool SolveQuadratic(double A, double B, double C, double& retAdd, double& retSubtract)
{

	if(A == 0.0)	//not a quadratic equation
	{
		if(B == 0.0)
		{
			retAdd = 0.0;	//make sure the variables are just seroed out
			retSubtract = 0.0;
			if(C == 0.0)	//the equation is 0.0 = 0.0. Everything is good. Default to 0 and say legitimate answer
				return(GOODRET);
			else
				return(BADRET);	//this quadratic equation is not solvable
		}
		else	//linear equation
		{
			retAdd = -C / B;
			retSubtract=retAdd;
			return(GOODRET);
		}
	}
	double sqrtterm = DoubleSubtract(B*B, 4.0 * A * C);

	if(sqrtterm < 0.0)	//there is no real solution
	{
		retAdd = 0.0;
		retSubtract = 0.0;
		return(BADRET);
	}

	sqrtterm = sqrt(sqrtterm);
	//if it's gotten this far, then A != 0.0
	double den = 0.5 / A;
	
	retAdd = DoubleSubtract(sqrtterm,B) * den;
	retSubtract = -DoubleAdd(B, sqrtterm) * den;

	return(GOODRET);
}

double CubeRoot(double num)
{
	double ret = 0.0;
	if(num > 0.0)
	{
		ret = pow(num, ONETHIRD);
	}
	if(num < 0.0)
	{
		ret = -pow(-num, ONETHIRD);
	}
	return(ret);
}


int SetabsEUseFromFermi(ELevel* eLev, double absFermi)
{
	return(0);	//this is a BAD idea.
	/*
	double fractionOcc = eLev->GetEndOccStates() / eLev->GetNumstates();

	double temp = eLev->ptrPoint->temp;
	if(temp <= 0.0)	//invalid ish...
	{
		if(fractionOcc == 0.0)
			eLev->eUse = eLev->min;
		else if(fractionOcc == 1.0)
			eLev->eUse = eLev->max;
		else
			eLev->eUse = eLev->min + fractionOcc * (eLev->max - eLev->min);
		return(0);
	}

	if(fractionOcc == 0.0 || fractionOcc >= 1.0)	//don't change if not gonna work...
		return(0);

//	if(eLev->carriertype == HOLE)	//pretend it's an electron to start with
//		fractionOcc = 1.0 - fractionOcc;

	fractionOcc = 1.0 / fractionOcc;	

	double lnterm = fractionOcc - 1.0;
	if(eLev->imp && eLev->carriertype==ELECTRON)
		lnterm = lnterm / eLev->imp->degeneracy;
	else if(eLev->imp)
		lnterm = lnterm * eLev->imp->degeneracy;

	if(lnterm > 0.0)
		lnterm = log(lnterm);
	else
	{
		eLev->eUse = 0.5*(eLev->min + eLev->max);
		return(0);
	}

	//everything is a valid number at this point...
	double difEf= lnterm * KB * temp;	//if electrons, this is E-Ef, if holes, this is Ef-E. (arises from 1-f(delE) = f(-delE)

	if(eLev->imp == NULL)
	{
		if(eLev->carriertype == HOLE && eLev->bandtail == false)	//difEf negative, and subtract out min to band and band to Ef
			eLev->eUse = difEf - (absFermi - eLev->ptrPoint->vb.bandmin);
		else if(eLev->carriertype == HOLE && eLev->bandtail == true)
			eLev->eUse = (absFermi - eLev->ptrPoint->vb.bandmin) + difEf;
		else if(eLev->bandtail == false) //here difEf likely positive
			eLev->eUse = difEf - (eLev->ptrPoint->cb.bandmin - absFermi);
		else
			eLev->eUse = (eLev->ptrPoint->cb.bandmin - absFermi) - difEf;

		eLev->AbsFermiEnergy = absFermi;
	}
	else
	{
		//definitely electrons...
		if(eLev->imp->reference == VALENCE)	//E-Ef is probably negative, but it works either way
			eLev->eUse = (absFermi - eLev->ptrPoint->vb.bandmin) + difEf;
		else //here difEf is probably positive, but as before, works either way
			eLev->eUse = (eLev->ptrPoint->cb.bandmin - absFermi) - difEf;

		eLev->AbsFermiEnergy = absFermi;
	}
	
	//now check to make sure this isn't some hot electrons or something. The fermi level
	//given cannot be used to make a valid point at this energy level.
	if(eLev->eUse > eLev->max)
	{
		eLev->eUse = eLev->max;

		if(eLev->imp == NULL)
		{
			if(eLev->carriertype == ELECTRON && eLev->bandtail == false)
				eLev->AbsFermiEnergy = (eLev->eUse + eLev->ptrPoint->cb.bandmin) - difEf;
			else if(eLev->carriertype == ELECTRON && eLev->bandtail == true)
				eLev->AbsFermiEnergy = (eLev->ptrPoint->cb.bandmin - eLev->eUse) - difEf;
			else if(eLev->bandtail == true) //valence band tails
				eLev->AbsFermiEnergy = (eLev->ptrPoint->vb.bandmin + eLev->eUse) - difEf;
			else //valence band
				eLev->AbsFermiEnergy = difEf + (eLev->ptrPoint->vb.bandmin - eLev->eUse);
		}
		else
		{
			if(eLev->imp->reference == CONDUCTION)
				eLev->AbsFermiEnergy = (eLev->ptrPoint->cb.bandmin - eLev->eUse) - difEf;
			else
				eLev->AbsFermiEnergy = (eLev->ptrPoint->vb.bandmin + eLev->eUse) - difEf;
		}

	}
	else if(eLev->eUse < eLev->min)
	{
		eLev->eUse = eLev->min;
		if(eLev->imp == NULL)
		{
			if(eLev->carriertype == ELECTRON && eLev->bandtail == false)
				eLev->AbsFermiEnergy = (eLev->eUse + eLev->ptrPoint->cb.bandmin) - difEf;
			else if(eLev->carriertype == ELECTRON && eLev->bandtail == true)
				eLev->AbsFermiEnergy = (eLev->ptrPoint->cb.bandmin - eLev->eUse) - difEf;
			else if(eLev->bandtail == true) //VBTails
				eLev->AbsFermiEnergy = (eLev->ptrPoint->vb.bandmin + eLev->eUse) - difEf;
			else //VB
				eLev->AbsFermiEnergy = difEf + (eLev->ptrPoint->vb.bandmin - eLev->eUse);
		}
		else
		{
			if(eLev->imp->reference == CONDUCTION)
				eLev->AbsFermiEnergy = (eLev->ptrPoint->cb.bandmin - eLev->eUse) - difEf;
			else
				eLev->AbsFermiEnergy = (eLev->ptrPoint->vb.bandmin + eLev->eUse) - difEf;
		}
	}
	//now given a fermi level, multiplying the nbumber of states by the fermi probability will give the current occupied status
	//so this is hopefully a lot more accurate in getting some values put in - this is pretty important when calculating the
	//fermi levels so there is minimal shift in the data, as the fermi function varies wildly.
	

	return(0);
	*/
}

//returns if the ret1/2 values are valid
bool TaylorSecondOrder(double x1, double x2, double x3, double f1, double f2, double f3, double& ret1, double& ret2)
{
	ret1 = 0.0;
	ret2 = 0.0;
	if(f1 == 0.0)	//it is right on the value, so don't change anything
		return(GOODRET);

	double x_min, x_mid, x_max;
	double f_min, f_mid, f_max;

	if(x1 <= x2 && x1 <= x3)	//x1 is lowest
	{
		x_min = x1;
		f_min = f1;
		if(x2 <= x3)	//x2 is mid
		{
			x_mid = x2;
			f_mid = f2;
			x_max = x3;
			f_max = f3;
		}
		else  //x3 is mid
		{
			x_mid = x3;
			f_mid = f3;
			x_max = x2;
			f_max = f2;
		}
	}
	else if(x2 <= x1 && x2 <= x3)	//x2 is lowest
	{
		x_min = x2;
		f_min = f2;
		if(x1 <= x3)	//x1 is mid
		{
			x_mid = x1;
			f_mid = f1;
			x_max = x3;
			f_max = f3;
		}
		else //x3 is mid
		{
			x_mid = x3;
			f_mid = f3;
			x_max = x1;
			f_max = f1;
		}
	}
	else  //x3 is lowest
	{
		x_min = x3;
		f_min = f3;
		if(x1 <= x2) //x1 is mid
		{
			x_mid = x1;
			f_mid = f1;
			x_max = x2;
			f_max = f2;
		}
		else //x2 is mid
		{
			x_mid = x2;
			f_mid = f2;
			x_max = x1;
			f_max = f1;
		}
	}

	//let's test for derivs

	double dx1a = DoubleSubtract(x_mid, x_min);	//one or both of these is the appropriate
	double dx1b = DoubleSubtract(x_max, x_mid);	//first derivative
	//these are in numberical order, so if either of these are zero... we need to do a first derivative

	if(dx1a == 0.0 || dx1b == 0.0)	//can't do a second order because insufficient/conflicting
	{
		//attempt to do linear using just the current and previous value.
		//f(x+dx) = 0 = f(x) + f'(x) * dx
		//dx = -f(x) / f'(x) = -f_n / ((f_n - f_prev)/(x_n - x_prev))
		//dx = -f_n * (x_n - x_prev) / (f_n - f_prev)
		dx1a = DoubleSubtract(f1, f2);	//how y values changed
		if(dx1a == 0.0)		//they did not change y values, so you can't extrapolate to zero
			return(BADRET);	//so say it can't be done
		ret1 = f1 * DoubleSubtract(x1, x2) / dx1a;
		if(ret1 == 0.0)	//the first derivative was actually invalid as f(x) changed, but x = const
			return(BADRET);
		ret2 = ret1;	//there is only one good value
		return(GOODRET);	//got a good return value
	}

	//now the first derivatives are both good, which means the second derivative will be good
	double dx2 = 0.5 * DoubleSubtract(x_max, x_min);
	
	double f1a = DoubleSubtract(f_mid, f_min);	//deriv for x_min/x_mid
	double f1b = DoubleSubtract(f_max, f_mid);	//deriv for x_mid/x_max

	f1a = f1a / dx1a;
	f1b = f1b / dx1b;
	double dd2 = (f1b - f1a) / dx2;	//want to do later slope - earlier slope, so mid/max - min/mid

	//now I need to choose the appropriate first derivative, and I'll put it in f1a
	//the appropriate f(x) is f1.
	
	if(x1 == x_max)	//want the first derivative on the far side
		f1a = f1b;
	else if(x1 == x_mid)	//it could go either way, choose first derivative based on where the 2nd point came from
	{
		if(x2 == x_max)
			f1a = f1b;
		//else, no change
	}
/*	else //x1 == x_min
	{
		f1a = f1a;	//do nothing
	}
*/

	//f(x+dx) = 0 = f(x) + f'(x) * dx + f''(x)/2 * dx^2
	return(SolveQuadratic(0.5 * dd2, f1a, f1, ret1, ret2));
}

unsigned int getPrecision(double val, bool scientific) {
	std::stringstream buffer;
	int precision = 13;	//far enough that it should not catch number noise. It should also induce some rounding
	//to get the values to zero.
	if (scientific)
		buffer << std::scientific;
	else
		buffer << std::fixed;
	buffer << std::setprecision(precision) << val;
	std::string str = buffer.str();
	//get rid of everything on the e and beyond
	size_t pos = str.find('e');
	if (pos == std::string::npos)
		pos = str.find('E');

	if (pos != std::string::npos)
		str.erase(pos);	//erase everything from this character and beyond - aka the E+/-006
	
	
	while (str.size() > 0) {
		if (str.back() == '0') {
			precision--;
			str.pop_back();
		}
		else
			break;
	}
	return precision;
}

bool TaylorSecondOrder(double val, double f, double df, double d2f, double& ret1, double& ret2)
{
	ret1 = ret2 = 0.0;
	//solve equation: val = f(x) + (df(x)/dx) * del_x + 0.5 (d^2f(x)/dx^2) * del_x^2
	double A = 0.5 * d2f;
	double B = df;
	double C = f - val;

	if(SolveQuadratic(A,B,C,ret1,ret2))
		return(true);

	if(df == 0.0)
		return(val==f);	//it's already on the value, so return true. otherwise, return false.

	ret1 = (val - f) / df;
	ret2 = ret1;
	return(true);
}

int CubeRootComplex(std::complex<double> input, std::complex<double>& out1, std::complex<double>& out2, std::complex<double>& out3)
{
	double angle = arg(input) / 3.0;
	double mag = abs(input);
	double angle2 = angle + 2.0943951023931954923084289221863;	// + 2pi/3
	double angle3 = angle - 2.0943951023931954923084289221863;	// - 2pi/3
	mag = pow(mag, 1.0/3.0);
	out1 = std::polar(mag, angle);
	out2 = std::polar(mag, angle2);
	out3 = std::polar(mag, angle3);
	return(0);
}

//returns the most real/non-imaginary answer
std::complex<double> CubeRootComplex(std::complex<double> input)
{
	bool Noimag = (input.imag()==0.0);
	double angle = arg(input) / 3.0;	//[-pi/3, pi/3]	--- at most pi/3 away from zero
	double mag = abs(input);
	double angle2 = angle + 2.0943951023931954923084289221863;	// + 2pi/3 ..... [pi/3,pi]
	double angle3 = angle - 2.0943951023931954923084289221863;	// - 2pi/3 ..... [-pi,-pi/3]
	mag = pow(mag, 1.0/3.0);	//cube root

	//want to find the closest angle to 0, pi, or -pi, and then return the most 'real' answer
	double angleUse = angle;
	double minDifAngle = fabs(angle);

	double difAngle = -(angle2-PI);
	if(difAngle < minDifAngle)
	{
		minDifAngle = difAngle;
		angleUse = angle2;
	}

	difAngle = angle3+PI;
	if(difAngle < minDifAngle)
	{
		minDifAngle = difAngle;
		angleUse = angle3;
	}
	if(Noimag)
		return(input.real()<0.0?std::complex<double>(-mag,0.0):std::complex<double>(mag,0.0));

	return(std::polar(mag, angleUse));	//returns the most 'real' answer
}

double dot(XSpace a, XSpace b)
{
	XSpace temp;
	temp.x = a.x * b.x;
	temp.y = a.y * b.y;
	temp.z = a.z * b.z;
	return(temp.x + temp.y + temp.z);
}

double AbsDistance(XSpace a, XSpace b)
{
	double x = a.x - b.x;
	double y = a.y - b.y;
	double z = a.z - b.z;

	double distance = pow(x*x + y*y + z*z, 0.5);
	return(distance);
}



double getTrapezoidalArea(double startPos, double endPos, double startTrapPos, double endTrapPos, double startTrapValue, double endTrapValue) {
	//make sure the values are ordered properly
	Swap(startPos, endPos);
	Swap(startTrapPos, endTrapPos);
	
	if (startPos >= endTrapPos || endPos <= startTrapPos || (startTrapValue==0.0 && endTrapValue==0.0))	//check for no overlap... or no trapezoid
		return 0.0;

	//double equal is expensive, so try to avoid checking it
	if (DoubleEqual(startPos, endPos) || DoubleEqual(startTrapPos, endTrapPos))
		return 0.0;
	
	if (startPos < startTrapPos)
		startPos = startTrapPos;
	if (endPos > endTrapPos)
		endPos = endTrapPos;

	//note divide by 0 is impossible
	double slope = (endTrapValue - startTrapValue) / (endTrapPos - startTrapPos);

	double startValue = startTrapValue + (startPos - startTrapPos) * slope;
	double endValue = startTrapValue + (endPos - startTrapPos) * slope;

	double ret = (startValue + endValue) * (endPos - startPos) * 0.5;
	return ret;

}
