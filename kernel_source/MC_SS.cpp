#include "MC_SS.h"

#include <ctime>

#include "model.h"
#include "RateGeneric.h"
#include "physics.h"
#include "../gui_source/kernel.h"
#include "helperfunctions.h"

#include "SteadyState.h"
#include "outputdebug.h"
#include "output.h"
#include "contacts.h"
#include "DriftDiffusion.h"
#include "light.h"
#include "CalcBandStruct.h"
#include "transient.h"
#include "SaveState.h"


int MC_C_SS(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	Simulation* sim = mdesc.simulation;
	bool first = true;
	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	sim->TransientData->timestep = sim->TransientData->mints = sim->TransientData->maxts = sim->TransientData->avgts = 0.0;	//make sure data is not thrown off here
	GetStartCharge(mdl, mdesc);
	PartitionFunc<MC_ElevelPartition> ELevelChoose;
	double LPercent = 0.0;
	RecordMCTransfer(true, NULL, 0, 0, 0.0);
	for(unsigned int i=0; (i<sim->maxiter || sim->maxiter == 0); i++)
	{
		//if maxiter = 0, go until it is *done* according to the accuracy
		time(&end);	//load the end time for dif
		timeelapse = difftime(end, start);
		if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
			DisplayProgress(53, i);
			DisplayProgress(29, sim->maxiter);
			start = end;
		}

		PrepDevice(mdl, mdesc, first);	//only do on hard reset
		MC_CalcRateDerivs(mdl, mdesc);	//only do on hard reset
		MC_OutputState(mdl, mdesc);		//only do on hard reset
		
		LPercent = MC_C_BuildPFn(mdl, ELevelChoose);	//get partition function for choices
		if(LPercent < mdesc.simulation->accuracy)
			break;	//it is done!

		MC_C_Process(mdl, mdesc, ELevelChoose);	//this is the bulk of the calculation. Assume constant B.D.

		first = false;
		
//		OutputDataInternalPost(mdl, *(mdesc.simulation), 0);
//		sim->binctr++;
//		OutputDataToFile(mdl, mdesc);
		//debug - I need to see how the code changes

		

		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
			break;
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);
		if(kThread->shareData.GUIsaveState)
		{
			DisplayProgressDoub(12, mdesc.simulation->TransientData->iterEndtime);
			ProgressInt(42, mdesc.simulation->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			kThread->shareData.GUIsaveState = false;	//reset boolean
		}

		mdesc.simulation->curiter++;
		
	}
	return(0);
}

int MC_CalcRateDerivs(Model& mdl, ModelDescribe& mdesc)
{
	//loop through all the points and calculate the rates and their derivatives
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.cb.energies.begin(); eLev != curPt->second.cb.energies.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
	
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.vb.energies.begin(); eLev != curPt->second.vb.energies.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.acceptorlike.begin(); eLev != curPt->second.acceptorlike.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.donorlike.begin(); eLev != curPt->second.donorlike.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.CBTails.begin(); eLev != curPt->second.CBTails.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.VBTails.begin(); eLev != curPt->second.VBTails.end(); ++eLev)
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);

	}

	for(unsigned int i=0; i<mdesc.simulation->contacts.size(); i++)
	{
		ProcessContactTransient(mdl, mdesc.simulation->contacts[i], mdesc.simulation);
		for(unsigned int j=0; j<mdesc.simulation->contacts[i]->point.size(); j++)
		{
			
			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->cb.energies.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->cb.energies.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}

			
			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->vb.energies.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->vb.energies.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}

			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->donorlike.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->donorlike.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}

			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->acceptorlike.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->acceptorlike.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}

			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->VBTails.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->VBTails.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}

			for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = mdesc.simulation->contacts[i]->point[j]->CBTails.begin(); eLev != mdesc.simulation->contacts[i]->point[j]->CBTails.end(); ++eLev)
			{
				eLev->second.rateIn = eLev->second.RateSums.GetPosSum();
				eLev->second.rateOut = -eLev->second.RateSums.GetNegSum();
				eLev->second.netRate = eLev->second.RateSums.GetNetSum();
			}
		}
	}

	Light(mdl, mdesc, NULL);
	mdesc.simulation->TransientData->tStepControl.ReInit();	//make sure it doesn't keep having lots of data in here from the rate calculations
	return(0);
}

double MC_C_BuildPFn(Model& mdl, PartitionFunc<MC_ElevelPartition>& probs)
{
	probs.Clear();
	double largestPercentOff = 0.0;
	double tmpPercent = 0.0;

	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		tmpPercent = MC_C_PFnStateBuild(curPt->second.cb.energies, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;
		tmpPercent = MC_C_PFnStateBuild(curPt->second.vb.energies, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;
		tmpPercent = MC_C_PFnStateBuild(curPt->second.acceptorlike, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;
		tmpPercent = MC_C_PFnStateBuild(curPt->second.donorlike, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;
		tmpPercent = MC_C_PFnStateBuild(curPt->second.CBTails, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;
		tmpPercent = MC_C_PFnStateBuild(curPt->second.VBTails, probs);
		if(tmpPercent > largestPercentOff)
			largestPercentOff = tmpPercent;

	}

	return(largestPercentOff);
}


double MC_C_PFnStateBuild(std::multimap<MaxMin<double>, ELevel>& states, PartitionFunc<MC_ElevelPartition>& probs)
{
	
	MC_ElevelPartition tmp;
	double LPercent = 0.0;
	double tmpPercent = 0.0;

	for(std::multimap<MaxMin<double>,ELevel>::iterator it = states.begin(); it != states.end(); ++it)
	{
		double NRate = it->second.RateSums.GetNetSum();
		double deriv = it->second.derivsourceonly;
		if(deriv != 0.0 && NRate != 0.0)
		{
			tmp.sortVal = it->second.uniqueID;
			tmp.ptrELevel = &(it->second);
			tmp.ptrELevel->targetTransfer = -NRate / deriv;
			probs.AddProbability(fabs(tmp.ptrELevel->targetTransfer), tmp);
			double occ = tmp.ptrELevel->GetBeginOccStates();
			double num = tmp.ptrELevel->GetNumstates();

			if(NRate > 0.0)
			{
				tmp.ptrELevel->maxOcc = occ;
				tmp.ptrELevel->minOcc = 0.0;	//guaranteed to be true, though the first time
			}
			else
			{
				tmp.ptrELevel->minOcc = occ;
				tmp.ptrELevel->maxOcc = num;
			}
		
			if(occ > 0.5 * num)
				occ = num - occ;	//just take the smaller value that is closer to the limit - matters for impurities

			if(occ != 0.0)
				tmpPercent = fabs(tmp.ptrELevel->targetTransfer/occ);	//   |delta/occ| = percent change
			else
				tmpPercent = 1.0;

			if(tmpPercent > LPercent)
				LPercent = tmpPercent;
		}
	}

	return(LPercent);
}

int MC_C_Process(Model& mdl, ModelDescribe& mdesc, PartitionFunc<MC_ElevelPartition>& probability)
{
	unsigned long int sz = probability.GetSize();
	unsigned long int loopTot = (unsigned long int)(0.01 * sz) + 10;
	if(loopTot > sz)
		loopTot = sz;	//basically just do all of them... same as SS calculation.

	std::vector<ELevel*> chosen;
	chosen.resize(loopTot);
	MC_ElevelPartition choice;
	unsigned int hardReset = mdesc.simulation->curiter/5 + 1;
	unsigned long int cutOffUpdateRates =  ((loopTot << 1) + loopTot) >> 2;	//loopTot * 0.75. Unsigned, so safe. Save type casts as well
	double orig;
	double percentChange;
	if(hardReset > 10)
		hardReset = 10;
	for(unsigned int i=0; i<hardReset; i++)
	{
		chosen.clear();
		chosen.resize(loopTot);
		for(unsigned long int j=0; j < loopTot; j++)
		{
			if(probability.HasOptions() == false)	//this should ALWAYS be true because loopTot <= sz
			{
				for(unsigned long int k=j; k<loopTot; k++)
					chosen[k] = NULL;	//make sure that no inadvertent elevels get accidentally added
				break;
			}

			if(probability.Choose(choice))
			{
				orig = choice.ptrELevel->GetBeginOccStates();
				double adjust = MC_C_AdjustCarriers(choice.ptrELevel, mdl, mdesc.simulation);
				if(orig != 0.0)
					percentChange = adjust / orig;
				else if(adjust != 0.0)
					percentChange = MC_C_UPDATE_NEIGHBORS_UP+1;	//force a recalc
				else
					percentChange = 0.0;	//both adjust and orig were zero, so do nothing

				if((percentChange > MC_C_UPDATE_NEIGHBORS_UP || percentChange < MC_C_UPDATE_NEIGHBORS_DOWN) && j < cutOffUpdateRates)
					UpdateProbNeighbors(probability, choice.ptrELevel, mdesc.simulation, mdl);

				chosen[j] = choice.ptrELevel;	//just store the eLevel that was changed so it may be added back later
				RecordMCTransfer(false, choice.ptrELevel, adjust, mdesc.simulation->curiter, orig);	//debug output
				probability.RemoveProbability(choice, true);
				
				choice.ptrELevel = NULL;	//clear it out just in case for future references
				choice.sortVal = 0;
			}
		}
		//now do a soft reset (band diagram, fix P function)
		ReinsertELevelPartition(probability, chosen);
		mdesc.simulation->InitIteration(mdl, mdesc);	//reset band diagram, note mdl.poissonRho[] needs to be updated first (done in MC_C_AdjustCarriers)
	}
	chosen.clear();	//clear memory

	return(0);
}

int UpdateProbNeighbors(PartitionFunc<MC_ElevelPartition>& probs, ELevel* src, Simulation* sim, Model& mdl)
{
	if(src == NULL)
		return(0);
	MC_ElevelPartition recalc;
	ELevel* curReplace;

	src->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
	double curFermi = src->AbsFermiEnergy;
	double trgFermi;
	for(std::vector<ELevelRate>::iterator Rates = src->Rates.begin(); Rates != src->Rates.end(); ++Rates)
	{
		recalc.ptrELevel = Rates->target;
		if(recalc.ptrELevel == NULL || recalc.ptrELevel == src)	//this may have been from a contact
			continue;
		curReplace = recalc.ptrELevel;
		curReplace->UpdateAbsFermiEnergy(RATE_OCC_BEGIN);
		trgFermi = curReplace->AbsFermiEnergy;

		if(fabs(curFermi - trgFermi) < 0.2)
			continue;	//don't bother updating things that are already relatively close - only the major changes should have things updated

		recalc.sortVal = curReplace->uniqueID;
		if(probs.ItemExists(recalc))	//only update if it hasn't been processed yet
		{
			MC_C_UpdateRateData(recalc.ptrELevel, sim, mdl);

			double NRate = curReplace->RateSums.GetNetSum();
			double deriv = curReplace->derivsourceonly;
			double prevProb = probs.ExtractProb(recalc);
			double totProbI = 1.0/probs.GetTotalProb();
			if(deriv != 0.0 && NRate != 0.0)
			{
				double newProb = fabs(-NRate / deriv);
				double ratioNew = newProb * totProbI;
				double ratioOld = prevProb * totProbI;
				if(fabs(ratioNew - ratioOld) > MC_C_WORTH_RECALC_PROB) //it would be at least a 1% change compared to everything else
				{
					probs.RemoveProbability(recalc);
					probs.AddProbability(newProb, recalc);	//this will cause a recalculation! Try to avoid as big time sink
				}
			}
		}
	}
	return(0);
}

int ReinsertELevelPartition(PartitionFunc<MC_ElevelPartition>& probs, std::vector<ELevel*>& ELevs)
{
	MC_ElevelPartition tmp;
	unsigned long int sz = ELevs.size();
	for(unsigned long int i=0; i<sz; i++)
	{
		tmp.ptrELevel = ELevs[i];
		tmp.sortVal = ELevs[i]->uniqueID;
		double num = tmp.ptrELevel->GetNumstates();
		double max = num;
		double min = 0.0;
		double occ = tmp.ptrELevel->GetEndOccStates();
		if(tmp.ptrELevel->maxOccRate < 0.0 && tmp.ptrELevel->minOccRate > 0.0)
		{  //these are both valid, and the answer is likely somewhere in here
			max = tmp.ptrELevel->netRate <= 0.0 ? occ : tmp.ptrELevel->maxOcc;
			//the guess is < 0, but it is guaranteed to be closer to zero than max
			min = tmp.ptrELevel->netRate >= 0.0 ? occ : tmp.ptrELevel->minOcc;

			//if the guessoccrate is zero, then max=min, and this will essentially not be added back and computed for the rest of this iteration
		}
		else if(tmp.ptrELevel->maxOccRate < 0.0)
		{ 	//then it did not cross over, use larger of secant/newton method, the valid one was maxOcc
			max = tmp.ptrELevel->maxOcc;
			double /*slope,*/secantMin, NewtonMin;
			double difRates = tmp.ptrELevel->maxOccRate - tmp.ptrELevel->netRate;
			double difOcc = max - occ;	//guessOcc will always correlate with NetRate and occ
			if(difRates != 0.0)
			{
				//slope = difRates / difOcc;
				secantMin = occ - tmp.ptrELevel->netRate * difOcc / difRates; // /slope;
				if(secantMin < 0.0 && secantMin >= -occ)	//it is close to the actual value and is an OK approximation
					secantMin = 0.0;
			}
			else
				secantMin = 0.0;

			//the (1.0 to get to 0) 2.0 basically says make sure it likely gets to the other side
			if(tmp.ptrELevel->derivsourceonly != 0.0)
			{
				NewtonMin = occ - tmp.ptrELevel->netRate / tmp.ptrELevel->derivsourceonly;
				if(NewtonMin < 0.0 && NewtonMin >= -occ)	//it is close to the actual value and is an OK approximation
					NewtonMin = 0.0;
			}
			else
				NewtonMin = 0.0;

			//note, these min/max are used ONLY for a *fast* weighting
			if((secantMin <= NewtonMin || NewtonMin <= 0.0) && secantMin >= 0.0)
				min = secantMin;
			else if((NewtonMin <= secantMin || secantMin <= 0.0) && NewtonMin >= 0.0)
				min = NewtonMin;
			else
				min = 0.0;
		}
		else if(tmp.ptrELevel->minOccRate > 0.0)
		{ //it did not cross over, and this is from the minimum side
			min = tmp.ptrELevel->minOcc;
			double difRates = tmp.ptrELevel->netRate - tmp.ptrELevel->minOccRate;
			double difOcc = occ - min;
			double /*slope, */secantMax, NewtonMax;
			if(difRates != 0.0)
			{
				//slope = difRates / difOcc;
				secantMax = occ - tmp.ptrELevel->netRate * difOcc / difRates; // /slope;
				if(secantMax > num && secantMax <= num+(num-occ))	//it is close to the actual value and is an OK approximation
					secantMax = num;
			}
			else
				secantMax = num;

			if(tmp.ptrELevel->derivsourceonly != 0.0)
			{
				NewtonMax = occ - tmp.ptrELevel->netRate / tmp.ptrELevel->derivsourceonly;
				if(NewtonMax > num && NewtonMax <= num+(num-occ))	//it is close to the actual value and is an OK approximation
					NewtonMax = num;
			}
			else
				NewtonMax = num;

			if((secantMax >= NewtonMax || NewtonMax >= num) && secantMax <= num)
				max = secantMax;
			else if((NewtonMax >= secantMax || secantMax >= num) && NewtonMax <= num)
				max = NewtonMax;
			else
				max = num;
		}
		else //i'm not sure what the hell happened.
		{
			double delta;
			if(tmp.ptrELevel->derivsourceonly != 0.0)
				delta = -2.0*tmp.ptrELevel->netRate / tmp.ptrELevel->derivsourceonly;	//zero point is nominally right in the center
			else
				delta = (tmp.ptrELevel->netRate > 0.0) ? num - occ : -occ;
			
			min = (delta<0.0) ? occ + delta : occ;
			max = (delta>0.0) ? occ + delta : occ;
		}
		
		double difference = max-min;	//this should probably always be positive... but just make sure
		probs.AddProbability(fabs(difference), tmp);

	}
	return(0);
}

double MC_C_AdjustCarriers(ELevel* eLev, Model& mdl, Simulation* sim)
{
	if(eLev == NULL)
		return(0.0);

	MC_C_UpdateRateData(eLev, sim, mdl);
	//reset the rate data for the carrier

	if(eLev->netRate == 0.0)	//this is good... don't do anything
		return(0.0);

	
	if(MY_IS_FINITE(eLev->derivsourceonly) == false || eLev->derivsourceonly == 0.0)
		return(0.0);	//the derivative is invalid, so better skip and try changing something different

	//the derivative should always be negative. If you add carriers to it, it should move the net
	//rate in a more negative direction
	
	double origOcc = eLev->GetBeginOccStates();
	double num = eLev->GetNumstates();
	double minOcc = num;	//good flag value for min, because this will never be the min
	double maxOcc = 0.0;	//good flag value for max, because this is obviously incorrect
	double prevExtremumOcc = origOcc;	//used for secant rates when min/max does not exist
	double prevExtremumRate = 0.0;	//these will only apply to one side...
	double minOccRate = MC_C_INVALID_MINOCC_RATE;
	double maxOccRate = MC_C_INVALID_MAXOCC_RATE;
	double targetRate = fabs(0.5 * eLev->netRate);	//want to get the rate to be <= in magnitude to this rate
	double guessOccRate = eLev->netRate;
	double nextGuessOcc = origOcc;;
	double guessOcc = eLev->GetBeginOccStates();
	double percentChangeMinMax = 1.0;

	if(eLev->netRate > 0.0)	//there are not enough carriers currently in there
	{
		minOcc = origOcc;
		minOccRate = eLev->netRate;
		prevExtremumRate = minOccRate;
	}
	else
	{
		maxOcc = origOcc;
		maxOccRate = eLev->netRate;
		prevExtremumRate = maxOccRate;
	}

	while(fabs(guessOccRate) > targetRate && percentChangeMinMax >= sim->accuracy)
	{
				//first try and do a min/max, but it needs to be valid data
		double occMinMax = MC_C_INVALID_OCC;
		double occSecant = MC_C_INVALID_OCC;
		double occNewton = MC_C_INVALID_OCC;

		if(maxOcc != minOcc)	//it's converged as good as it's gonna get...
		{
			if(maxOccRate != MC_C_INVALID_MAXOCC_RATE && minOccRate != MC_C_INVALID_MINOCC_RATE)
			{
				double RateDif = maxOccRate - minOccRate;	//presumably negative (maxOccRate<0 out, minOccrate>0 in)
				double occDif = maxOcc - minOcc;	//positive
				double slope = RateDif / occDif;
				percentChangeMinMax = fabs(occDif/origOcc);	//the range it is bouncing through is to a certain number of digits accuracy
				occMinMax = maxOcc - maxOccRate / slope;	//try to be intelligent in the next guess
				if(occMinMax <= minOcc || occMinMax >= maxOcc)	//this isn't converging (somehow), so make the next guess be halfway
					occMinMax = 0.5 * (maxOcc + minOcc);
			}
			else if(maxOccRate != MC_C_INVALID_MAXOCC_RATE && !DoubleEqual(prevExtremumRate, maxOccRate) && !DoubleEqual(maxOcc, prevExtremumOcc))
			{ //there are two possible rates, but same side (haven't crossed zero), maxOccRate size
				//one of them is the maxOcc, and the other is the guessRate, assuming they aren't the same
				double RateDif = maxOccRate - prevExtremumRate;	//positive
				double occDif = maxOcc - prevExtremumOcc;	// negative
				double slope = RateDif / occDif;	//negative
				occSecant = maxOcc - maxOccRate / slope;	//- (-slope) (-rate) = -, moves left, good
				if(eLev->derivsourceonly != 0.0)
					occNewton = guessOcc - guessOccRate / eLev->derivsourceonly;
			
			}
			else if(minOccRate != MC_C_INVALID_MINOCC_RATE && !DoubleEqual(prevExtremumRate, minOccRate) && !DoubleEqual(minOcc, prevExtremumOcc))
			{
				double RateDif = prevExtremumRate - minOccRate;	//positive
				double occDif= prevExtremumOcc - minOcc;	//negative
				double slope = RateDif / occDif;	//negative
				occSecant = minOcc - minOccRate / slope;	// (-) (-slope)(+rate) = +, moves right, good
				if(eLev->derivsourceonly != 0.0)
					occNewton = guessOcc - guessOccRate / eLev->derivsourceonly;
			}
			else if(eLev->derivsourceonly != 0.0)
				occNewton = guessOcc - guessOccRate / eLev->derivsourceonly;
			//otherwise all the posibilities are bad...
		}
		else //minOcc == maxOcc. It's basically done converging, so update stuff and let 'em go.
		{
			occMinMax = maxOcc;
		}
		
		//reality check
		if(occMinMax <= 0.0 || occMinMax >= num)
			occMinMax = MC_C_INVALID_OCC;
		if(occSecant <= 0.0 || (occSecant <= minOcc && minOcc != num) ||
			occSecant >= num || (occSecant >= maxOcc && maxOcc != 0.0))
			occSecant = MC_C_INVALID_OCC;
		if(occNewton <= 0.0 || (occNewton <= minOcc && minOcc != num) ||
			occNewton >= num || (occNewton >= maxOcc && maxOcc != 0.0))
			occNewton = MC_C_INVALID_OCC;

		nextGuessOcc = MC_C_INVALID_OCC;

		if(occMinMax != MC_C_INVALID_OCC)	//preferred guess
			nextGuessOcc = occMinMax;
		else if(occSecant != MC_C_INVALID_OCC && occNewton != MC_C_INVALID_OCC)
		{ //botrh secant and newton are good
			//current occ is guessOcc.
			double difSec = fabs(occSecant - guessOcc);
			double difNewt = fabs(occNewton - guessOcc);
			nextGuessOcc = (difSec < difNewt) ? occSecant : occNewton;
		}
		else if(occSecant != MC_C_INVALID_OCC)
			nextGuessOcc = occSecant;
		else if(occNewton != MC_C_INVALID_OCC)
			nextGuessOcc = occNewton;
		else //it is invalid
		{
			//just try increasiong/decreasing by a percentage
			if(guessOccRate > 0.0)
				nextGuessOcc = guessOcc * (1.0 + eLev->percentTransfer);
			else if(guessOccRate < 0.0)
				nextGuessOcc = guessOcc * (1.0 - eLev->percentTransfer);

			if(nextGuessOcc > num)
				nextGuessOcc = guessOcc + eLev->percentTransfer * (num - guessOcc);
		}
		
	
		//update the occupancy
		eLev->SetBeginOccStates(nextGuessOcc);
		MC_C_UpdateRateData(eLev, sim, mdl);	//netrate will now have changed as well as derivSonly, etc.

		//do some post processing on this new data point
		if(eLev->netRate > 0.0)
		{
			//this is too small and needs more carriers. update min if needed.
			if(nextGuessOcc > minOcc || minOcc == num)
			{
				prevExtremumOcc = minOcc;
				prevExtremumRate = minOccRate;
				minOcc = nextGuessOcc;
				minOccRate = eLev->netRate;
			}
		}
		else if(eLev->netRate < 0.0)
		{
			if(nextGuessOcc < maxOcc || maxOcc == 0.0)
			{
				prevExtremumOcc = maxOcc;
				prevExtremumRate = maxOccRate;
				maxOcc = nextGuessOcc;
				maxOccRate = eLev->netRate;
			}
			//this has too many carriers and is trying to get rid of them
		}
		guessOccRate = eLev->netRate;
		
		guessOcc = nextGuessOcc;

	}	//end while loop
	
	Point* pt = &eLev->getPoint();
	/*	old code!

	double chargeChange = (eLev->carriertype == ELECTRON) ? -adjustCarrier : adjustCarrier;
	if(pt->EChangesCalc == false)
	{
		unsigned int sz = pt->PsiCf.size();
		pt->BandEnergyChange = 0.0;
		pt->EFieldEChange = 0.0;
		pt->EFieldESqChange = 0.0;
		for(unsigned int i=0; i<sz; i++)
		{
			Point* trgPt = pt->PsiCf[i].pt;
			double netCharge = trgPt->rho * pt->vol * QI;
			double adjust = -netCharge * pt->PsiCf[i].data;	//if data > 0, then it means the band diagram will move up for a positive charge
			//a hole moving up corresponds to a loss in energy, so this should be (-)
			//likewise, if net negative, if it goes up in the band diagram
			pt->BandEnergyChange += adjust;
			EFieldETrg(pt, trgPt);	//this function updates EFieldE(Sq)Change
		}
		//now need to get these values into eV values
		pt->EFieldEChange = pt->EFieldEChange * QI;	//was in Joules, so divide by q to get in eV)
		pt->EFieldESqChange = pt->EFieldESqChange * QI;
		pt->EChangesCalc = true;	//flag to not do this calculation again
		//band energy change is calculated as the net charge moving up in eV, so this should still be good
	}
	double EChangeBand = chargeChange * pt->BandEnergyChange;
	double EChangeEField = chargeChange * pt->EFieldESqChange;	//get the squared term
	EChangeEField = EChangeEField * EChangeEField;	//and actually square it
	EChangeEField += chargeChange * pt->EFieldEChange;	//now add in the linear term
	double EChange = EChangeBand + EChangeEField;

	if(EChange > 0.0)
	{
		double prob = SciRandom.Uniform01();
		double test = exp(-EChange * KBI / pt->temp);	//if no change in energy, this is 1
		if(test < prob)	//no change in energy: if 1 < (0,1), always false, so the following would not occur
			return(0.0);	//don't move the carriers
	}
	*/

	//at least one of these will be good. If both, this is what we want
	if(minOccRate != MC_C_INVALID_MINOCC_RATE && maxOccRate != MC_C_INVALID_MAXOCC_RATE)
	{
		eLev->minOcc = minOcc;
		eLev->minOccRate = minOccRate;
		eLev->maxOcc = maxOcc;
		eLev->maxOccRate = maxOccRate;
	}
	else if(minOccRate != MC_C_INVALID_MINOCC_RATE) //if only minOccRate was good
	{ //then this was solved via secants. minOcc was set to NextGuess, guess was set to next guess, and states was set to next guess
		//this means that using the secant value will be "Bad" for figuring out the secant
		eLev->minOcc = prevExtremumOcc;
		eLev->minOccRate = prevExtremumRate; //the actual minimum's are in occ and netrate
		eLev->maxOcc = maxOcc;
		eLev->maxOccRate = maxOccRate;
	}
	else
	{
		eLev->minOcc = minOcc;
		eLev->minOccRate = minOccRate;
		eLev->maxOcc = prevExtremumOcc;
		eLev->maxOccRate = prevExtremumRate;
	}
	eLev->SetEndOccStates(guessOcc);	//update the energy levels
	eLev->SetBeginOccStates(guessOcc);
	pt->ResetData();	//reset all necessary variables for band structure
	mdl.poissonRho[pt->adj.self] = pt->rho;	//and get the value needed for band structure updated

	return(guessOcc - origOcc);
}

//clear out non-light rates and recalculate the rates based on the new occupation levels
int MC_C_UpdateRateData(ELevel* eLev, Simulation* sim, Model& mdl)
{
	eLev->RateSums.Clear();
	eLev->derivsourceonly = 0.0;
	unsigned int sz = eLev->Rates.size();
	std::vector<std::vector<ELevelRate>::iterator> EraseRates;
	for(std::vector<ELevelRate>::iterator rate = eLev->Rates.begin(); rate != eLev->Rates.end(); ++rate)
	{
		if(rate->type >= RATE_LIGHTMIN && rate->type <= RATE_LIGHTMAX)
		{
			double NRate = DoubleSubtract(rate->RateIn, rate->RateOut);
			eLev->RateSums.AddValue(NRate);	//keep the light rates as they were (light long process - only update on hard resets)
		}
		else
		{
			EraseRates.push_back(rate);
			eLev->Rates.erase(rate);	//but get rid of the other rates
		}
	}
	for(std::vector<std::vector<ELevelRate>::iterator>::iterator it = EraseRates.begin(); it != EraseRates.end(); ++it)
		eLev->Rates.erase(*it);
	EraseRates.clear();

	MC_CalcELevelRate(eLev, sim, false);	//recalculate the rates, but don't clear out RateSums or Rates
	//those were "cleared" out by erasing everything that isn't light individually

	return(0);
}

int EFieldETrg(Point* srcPt, Point* trgPt)
{
	if(srcPt == NULL || trgPt == NULL)
		return(0);


	double linear, quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMX, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPX, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
/*	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMX2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPX2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;

	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMY, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPY, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMY2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPY2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;

	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMZ, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPZ, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjMZ2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
	EFieldEnergyCalc(srcPt, trgPt, trgPt->adj.adjPZ2, linear, quadratic);
	srcPt->EFieldEChange += linear;
	srcPt->EFieldESqChange += quadratic;
*/

	return(0);
}

//calculate the energy in the electric field resulting in the target point in going to it's adjacent point as a result to a change in charge in the source point
int EFieldEnergyCalc(Point* srcPt, Point* trgPt, Point* adjTrgPt, double& retLin, double& retQuad)
{
	retLin = 0.0;
	retQuad = 0.0;
	if(srcPt == NULL || trgPt == NULL || adjTrgPt == NULL)
		return(0);
	
	double trgAdjPsi, adjTrgAdjPsi;
	FindPoissonCoeff(srcPt, trgPt->adj.self, trgAdjPsi);
	FindPoissonCoeff(srcPt, adjTrgPt->adj.self, adjTrgAdjPsi);
	
	retQuad = (trgAdjPsi - adjTrgAdjPsi);
	retQuad = retQuad * retQuad;

	double difPsi = trgPt->Psi - adjTrgPt->Psi;
	retLin = (trgAdjPsi * difPsi - adjTrgAdjPsi * difPsi);	//normally a x2 in front, but E = 0.5 eps |E|^2, so cancel with that firs t0.5
	
	double dist = fabs(trgPt->position.x - adjTrgPt->position.x);
	double distanceSqI = 1.0/(dist * dist);

	retLin = retLin * EPSNOT * double(trgPt->eps) * distanceSqI * (trgPt->area.x * 0.5 * dist);
	//parenthesis is volume of that portion of the target point that the electric field is valid for
	retQuad = retQuad * 0.5 * EPSNOT * double(trgPt->eps) * distanceSqI * (trgPt->area.x * 0.5 * dist);
	return(0);
}



int MonteCarlo_SteadyState(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread)
{
	Simulation* sim = mdesc.simulation;
	bool first = true;
	time_t start = time(NULL);
	time_t end;
	double timeelapse = 0.0;
	sim->TransientData->timestep = 1.0;	//make sure data is not thrown off here
	GetStartCharge(mdl, mdesc);
	PartitionFunc<long int> ELevelChoose;
	std::vector<ELevel*> AllELevs;
	unsigned long long int ELevIndex = 0.0;
	RecordMCTransfer(true, NULL, 0, 0, 0.0);
	for(unsigned int i=0; i<sim->maxiter; i++)
	{
		time(&end);	//load the end time for dif
		timeelapse = difftime(end, start);
		if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
			DisplayProgress(53, i);
			DisplayProgress(29, sim->maxiter);
			start = end;
		}

		PrepDevice(mdl, mdesc, first);

		MC_CalcRates(mdl, mdesc, ELevelChoose);	//now all the rates are in
		MC_OutputState(mdl, mdesc);

		MC_Process(mdl, mdesc, ELevelChoose, AllELevs, ELevIndex);

		first = false;
		
//		OutputDataInternalPost(mdl, *(mdesc.simulation), 0);
//		sim->binctr++;
//		OutputDataToFile(mdl, mdesc);
		//debug - I need to see how the code changes

		

		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
			break;
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);
		if(kThread->shareData.GUIsaveState)
		{
			DisplayProgressDoub(12, mdesc.simulation->TransientData->iterEndtime);
			ProgressInt(42, mdesc.simulation->curiter);
			SaveState(mdl, mdesc);
			DisplayProgress(40, 0);
			kThread->shareData.GUIsaveState = false;	//reset boolean
		}

		mdesc.simulation->curiter++;
	}

	

	return(0);
}

int PrepDevice(Model& mdl, ModelDescribe& mdesc, bool first)
{
	mdesc.simulation->OpticalFluxOut->ClearAll();
	for(unsigned int i=0; i<mdesc.materials.size(); i++)
		mdesc.materials[i]->OpticalFluxOut->ClearAll();

	mdesc.simulation->InitIteration(mdl, mdesc);	//this will update the band structure

	
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		//not sure if this is actually necessary...
		curPt->second.Jn.x = curPt->second.Jn.y = curPt->second.Jn.z = curPt->second.Jp.x = curPt->second.Jp.y = curPt->second.Jp.z = 0.0;
		curPt->second.genth = curPt->second.recth = curPt->second.scatterR = 0.0;
		curPt->second.srhR1 = curPt->second.srhR2 = curPt->second.srhR3 = curPt->second.srhR4 = curPt->second.netCharge = 0.0;
		curPt->second.LightAbsorbDef = curPt->second.LightAbsorbGen = curPt->second.LightAbsorbScat = curPt->second.LightAbsorbSRH = 0.0;
		curPt->second. LightEmitDef = curPt->second.LightEmitRec = curPt->second.LightEmitScat = curPt->second.LightEmitSRH = 0.0;
		curPt->second.LightPowerEntry = curPt->second.LightEmission = 0.0;	//mW, photons/sec
		curPt->second.LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!
		curPt->second.largestOccCB = curPt->second.largestOccES = curPt->second.largestOccVB = curPt->second.largestUnOccES = 0.0;
		curPt->second.EChangesCalc = false;

		MC_PrepELevl(curPt->second.cb.energies, mdl, curPt->second, mdesc.simulation, first);
		MC_PrepELevl(curPt->second.vb.energies, mdl, curPt->second, mdesc.simulation, first);
		MC_PrepELevl(curPt->second.acceptorlike, mdl, curPt->second, mdesc.simulation, first);
		MC_PrepELevl(curPt->second.donorlike, mdl, curPt->second, mdesc.simulation, first);
		MC_PrepELevl(curPt->second.CBTails, mdl, curPt->second, mdesc.simulation, first);
		MC_PrepELevl(curPt->second.VBTails, mdl, curPt->second, mdesc.simulation, first);
	}
	return(0);
}

int MC_PrepELevl(std::multimap<MaxMin<double>,ELevel>& states, Model& mdl, Point& pt, Simulation* sim, bool first)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator st = states.begin(); st != states.end(); ++st)
	{
		if(first)
			st->second.percentTransfer = MC_C_MAXPERCENT;// * 0.125;

		st->second.TransferEndToBegin();
		st->second.Rates.clear();
		st->second.OpticalRates.clear();
		st->second.RateSums.Clear();
		st->second.rateIn = 0.0;
		st->second.rateOut = 0.0;
		st->second.netRateM2 = st->second.netRateM1;
		st->second.netRateM1 = st->second.netRate;
		st->second.netRate = 0.0;
		st->second.targetTransfer = 0.0;
//		FindAllTargets(st->second, &pt, mdl); -  this is only done once now at the very beginning
	}
	return(0);
}

int MC_CalcRates(Model& mdl, ModelDescribe& mdesc, PartitionFunc<long int>& ELevelChoose)
{
	long int index = 0;
	ELevelChoose.Clear();
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.cb.energies.begin(); eLev != curPt->second.cb.energies.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.vb.energies.begin(); eLev != curPt->second.vb.energies.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.acceptorlike.begin(); eLev != curPt->second.acceptorlike.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.donorlike.begin(); eLev != curPt->second.donorlike.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.CBTails.begin(); eLev != curPt->second.CBTails.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = curPt->second.VBTails.begin(); eLev != curPt->second.VBTails.end(); ++eLev)
		{
			MC_CalcELevelRate(&(eLev->second), mdesc.simulation);
			ELevelChoose.AddProbability(fabs(eLev->second.netRate), index);
			index++;
		}
	}

	Light(mdl, mdesc, NULL);
	mdesc.simulation->TransientData->tStepControl.ReInit();	//make sure it doesn't keep having lots of data in here from the rate calculations
	return(0);
}

int MC_Process(Model& mdl, ModelDescribe& mdesc, PartitionFunc<long int>& probability, std::vector<ELevel*> eLevs, unsigned long long int maxInd)
{
	unsigned long long int loopTot = maxInd/1024 + 10;
	if(loopTot > maxInd)
		loopTot = maxInd;	//basically just do all of them...

	long int choice;

	for(unsigned long long int j=0; j < loopTot; j++)
	{
		if(probability.HasOptions() == false)
			break;
		if(probability.Choose(choice))
		{
			UpdateNeighborsPre(mdl, mdesc, eLevs[choice]);	//updates all the neighboring states to make the local structure is up to date (rate wise as well)
			MC_CalcELevelRate(eLevs[choice], mdesc.simulation);	//updates the rate in the current energy level
			MC_TransferCarriers(eLevs[choice], mdl, mdesc.simulation->curiter);
			MC_UpdatePost(eLevs[choice], mdl);

			probability.RemoveProbability(choice);
		}
		

	}



	return(0);
}

int MC_UpdatePost(ELevel* src, Model& mdl)
{
	if(src == NULL)
		return(0);
	Point* pt = &src->getPoint();
	pt->ResetData();
	mdl.poissonRho[pt->adj.self] = pt->rho;
	if(pt->adj.adjMX)
	{
		pt->adj.adjMX->ResetData();
		mdl.poissonRho[pt->adj.adjMX->adj.self] = pt->adj.adjMX->rho;
	}
	if(pt->adj.adjMX2)
	{
		pt->adj.adjMX2->ResetData();
		mdl.poissonRho[pt->adj.adjMX2->adj.self] = pt->adj.adjMX2->rho;
	}
	if(pt->adj.adjPX)
	{
		pt->adj.adjPX->ResetData();
		mdl.poissonRho[pt->adj.adjPX->adj.self] = pt->adj.adjPX->rho;
	}
	if(pt->adj.adjPX2)
	{
		pt->adj.adjPX2->ResetData();
		mdl.poissonRho[pt->adj.adjPX2->adj.self] = pt->adj.adjPX2->rho;
	}
	if(pt->adj.adjMY)
	{
		pt->adj.adjMY->ResetData();
		mdl.poissonRho[pt->adj.adjMY->adj.self] = pt->adj.adjMY->rho;
	}
	if(pt->adj.adjMY2)
	{
		pt->adj.adjMY2->ResetData();
		mdl.poissonRho[pt->adj.adjMY2->adj.self] = pt->adj.adjMY2->rho;
	}
	if(pt->adj.adjPY)
	{
		pt->adj.adjPY->ResetData();
		mdl.poissonRho[pt->adj.adjPY->adj.self] = pt->adj.adjPY->rho;
	}
	if(pt->adj.adjPY2)
	{
		pt->adj.adjPY2->ResetData();
		mdl.poissonRho[pt->adj.adjPY2->adj.self] = pt->adj.adjPY2->rho;
	}
	if(pt->adj.adjMZ)
	{
		pt->adj.adjMZ->ResetData();
		mdl.poissonRho[pt->adj.adjMZ->adj.self] = pt->adj.adjMZ->rho;
	}
	if(pt->adj.adjMZ2)
	{
		pt->adj.adjMZ2->ResetData();
		mdl.poissonRho[pt->adj.adjMZ2->adj.self] = pt->adj.adjMZ2->rho;
	}
	if(pt->adj.adjPZ)
	{
		pt->adj.adjPZ->ResetData();
		mdl.poissonRho[pt->adj.adjPZ->adj.self] = pt->adj.adjPZ->rho;
	}
	if(pt->adj.adjPZ2)
	{
		pt->adj.adjPZ2->ResetData();
		mdl.poissonRho[pt->adj.adjPZ2->adj.self] = pt->adj.adjPZ2->rho;
	}

	return(0);
}

int UpdateNeighborsPre(Model& mdl, ModelDescribe& mdesc, ELevel* source)
{
	
	//first update the appropriate band structore
	Point* pt = &source->getPoint();
	CalcPointBand(*pt, mdl, *(mdesc.simulation));
	//and all the neighboring points that the targets could go to
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMX);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMX2);	//return 0 instantly...
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPX);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPX2);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMY);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMY2);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPY);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPY2);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMZ);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjMZ2);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPZ);
	MC_NeighborBand(mdl, *(mdesc.simulation),pt, pt->adj.adjPZ2);
	

//	source->TargetELev.clear();  The targets don't actually contain any information anymore
//	source->TargetELevDD.clear();
//	FindAllTargets(*source, pt, mdl);	//reset probabilities
	//update the neighboring rates and targets now that this one has changed
	for(std::vector<TargetE>::iterator elIt = source->TargetELevDD.begin(); elIt != source->TargetELevDD.end(); ++elIt)
	{
//		FindAllTargets(*(elIt->second.target), elIt->second.target->ptrPoint, mdl);
		MC_CalcELevelRate(elIt->target, mdesc.simulation);
	}
	for(unsigned int i=0; i<source->TargetELev.size(); ++i)
	{
//		FindAllTargets(*(source->TargetELev[i]), source->TargetELev[i]->ptrPoint, mdl);
		MC_CalcELevelRate(source->TargetELev[i], mdesc.simulation);
	}



	return(0);
}

int MC_NeighborBand(Model& mdl, Simulation& sim, Point* skipPt, Point* pt)
{
	if(pt == NULL)
		return(0);

	if(pt->adj.adjMX != skipPt)	//don't recalculate the main point
		CalcPointBand(*(pt->adj.adjMX), mdl, sim);
	if(pt->adj.adjMX2 != skipPt)
		CalcPointBand(*pt->adj.adjMX2, mdl, sim);
	if(pt->adj.adjPX != skipPt)
		CalcPointBand(*pt->adj.adjPX, mdl, sim);
	if(pt->adj.adjPX2 != skipPt)
		CalcPointBand(*pt->adj.adjPX2, mdl, sim);
	if(pt->adj.adjMY != skipPt)
		CalcPointBand(*pt->adj.adjMY, mdl, sim);
	if(pt->adj.adjMY2 != skipPt)
		CalcPointBand(*pt->adj.adjMY2, mdl, sim);
	if(pt->adj.adjPY != skipPt)
		CalcPointBand(*pt->adj.adjPY, mdl, sim);
	if(pt->adj.adjPY2 != skipPt)
		CalcPointBand(*pt->adj.adjPY2, mdl, sim);
	if(pt->adj.adjMZ != skipPt)
		CalcPointBand(*pt->adj.adjMZ, mdl, sim);
	if(pt->adj.adjMZ2 != skipPt)
		CalcPointBand(*pt->adj.adjMZ2, mdl, sim);
	if(pt->adj.adjPZ != skipPt)
		CalcPointBand(*pt->adj.adjPZ, mdl, sim);
	if(pt->adj.adjPZ2 != skipPt)
		CalcPointBand(*pt->adj.adjPZ2, mdl, sim);

	return(0);
}


int MC_CalcELevelRate(ELevel* elev, Simulation* sim, bool clearRates)
{
	if(clearRates)
	{
		elev->Rates.clear();
		elev->OpticalRates.clear();
		elev->RateSums.Clear();
	}
	PosNegSum derivs;
	derivs.Clear();	//just be safe
	for(std::vector<ELevel*>::iterator trg = elev->TargetELev.begin(); trg != elev->TargetELev.end(); ++trg)
	{
		std::vector<ELevelRate>::iterator ret = CalcRate(elev, *trg, sim);
		if(ret != elev->Rates.end())
			derivs.AddValue(ret->derivsourceonly);
	}
	for(std::vector<TargetE>::iterator trg = elev->TargetELevDD.begin(); trg != elev->TargetELevDD.end(); ++trg)
	{
		std::vector<ELevelRate>::iterator ret = CalcRate(elev, trg->target, sim);
		if(ret != elev->Rates.end())
			derivs.AddValue(ret->derivsourceonly);
	}
	elev->rateIn = elev->RateSums.GetPosSum();
	elev->rateOut = -elev->RateSums.GetNegSum();
	elev->netRate = elev->RateSums.GetNetSum();
	elev->derivsourceonly = derivs.GetNetSumClear();
	return(0);
}

int MC_TransferCarriers(ELevel* eLev, Model& mdl, unsigned long iter)
{
	PosNegSum derivs, derivs2;
	derivs.Clear();	//just be safe
	MC_SumDerivativeRates(eLev->Rates, derivs, derivs2);
	double deriv = derivs.GetNetSumClear();
	double deriv2 = derivs2.GetNetSumClear();
	double ret1, ret2;
	double carrierChange = eLev->targetTransfer * eLev->percentTransfer;

	if(deriv == 0.0 && deriv2 == 0.0)
		return(0);

	if(TaylorSecondOrder(0.0, eLev->netRate, deriv, deriv2, ret1, ret2))	//does linear and quadratic solution
	{
		//check for a good return concept wise
		if(eLev->netRate > 0.0)	// talking about a gain of negative charge, so Ef increases
		{
			if(ret1 > 0 && ret2 > 0.0)
				carrierChange = (ret1 < ret2) ? ret1 : ret2;	//choose the smaller change to hit zero
			else if(ret1 > 0)
				carrierChange = ret1;
			else if(ret2 > 0)
				carrierChange = ret2;

		}
		else if(eLev->netRate < 0.0)
		{
			if(ret1 < 0 && ret2 < 0)
				carrierChange = (ret1 > ret2) ? ret1 : ret2;	//grab the smaller magnitude
			else if(ret1 < 0)
				carrierChange = ret1;
			else if(ret2 < 0)
				carrierChange = ret2;

		}

	}
	
//	double carrierChange = -eLev->netRate * eLev->percentTransfer / deriv;
	if(carrierChange == 0.0)	//that was nice and easy
		return(0);

	if(carrierChange > 0.0)
	{
		double avail = eLev->GetNumstates() - eLev->GetBeginOccStates();
		if(carrierChange > avail)
			carrierChange = eLev->percentTransfer * avail;
	}
	else
	{
		double occ = eLev->GetBeginOccStates();
		if(-carrierChange > occ)	//it is trying to take out more than there is
			carrierChange = -eLev->percentTransfer * occ;
	}
	double A = -carrierChange;
	double B= eLev->netRate;
	double C = 0.5 * eLev->netRate * deriv;

	double time;	//a factor to maintain stuff...
	if(SolveQuadratic(A, B, C, ret1, ret2))
	{
		if(ret1 > 0.0 && ret2 > 0.0)
			time = ret1 < ret2 ? ret1:ret2;
		else if(ret1 > 0.0)
			time = ret1;
		else if(ret2 > 0.0)
			time = ret2;
		else time = 0.0;
	}
	else
		time = carrierChange / eLev->netRate;
	
	double actualTransfer = MC_TransferRate(eLev->Rates, time);

	//the time should have been chosen to be small enough that the actualTransfer won't be bad
	if(carrierChange * actualTransfer < 0.0)	//it went in the opposite direction...?
	{
		ProgressDouble(41, actualTransfer);
		ProgressDouble(42, carrierChange);
	}

	RecordMCTransfer(false, eLev, actualTransfer, iter, eLev->GetBeginOccStates());

	eLev->AddEndOccStates(carrierChange);
	eLev->SetBeginOccStates(eLev->GetEndOccStates());

	return(0);


}

int MC_SumDerivativeRates(std::vector<ELevelRate>& rate, PosNegSum& derivs, PosNegSum& derivs2)
{
	for(std::vector<ELevelRate>::iterator it = rate.begin(); it != rate.end(); ++it)
	{
		derivs.AddValue(it->deriv);
		derivs2.AddValue(it->deriv2);
	}
	return(0);
}


double MC_TransferRate(std::vector<ELevelRate>& rates, double time)	//need to update the targets
{
	double transferred = 0.0;
	for(std::vector<ELevelRate>::iterator rt = rates.begin(); rt != rates.end(); ++rt)
	{
		ELevel* trgE = rt->target;
	
		double RateIn, RateOut, netRate, deriv1, deriv2;
		MC_TranslateSourceERateToTarget(*rt, RateIn, RateOut, netRate, deriv1, deriv2);
		//takes the source rate data and figures out what the target rates would have been

		//now see how much of this should actually change in the target due to rates from neighboring points potentially cancelling it out
		if((netRate >= 0.0 && trgE->netRate <= 0.0) ||
			(netRate <= 0.0 && trgE->netRate >= 0.0))	//the rate from this is completely cancelled out by other rates or does nothing itself
			continue;
		double transfer = 0.0;
	
		if(netRate > 0.0)	//now lets find the proportion that should get cancelled
		{
			//this rate is trying to put carriers into the target energy level
			double proportion = RateIn / trgE->rateIn;	//if netrate>0, then rateIn exists, and total rateIn exists
			//proportion represents how much of the rate in this particular rate is
			//we want to take that proportion of the rate out and remove it from the rate in
			deriv1 = deriv1 * proportion;
			deriv2 = deriv2 * proportion;	//make sure these are adjusted appropriately as well
			transfer = (RateIn - trgE->rateOut * proportion);
			transfer = transfer * time //taylor expansion of dR/dt, but derivis dR/dc, so dR/dt = dR/dc * dc/dt, where dc/dt = R.
				+ 0.5 * deriv1 * transfer * time * time;	//say R(t) is the taylor series, and transfer is integral of R(t)dt
			//	+ transfer * transfer * time * time * time * deriv2 / 6.0; I don't feel like writing a cubic solver right now...
			if(transfer <= 0.0)	//this transfer is easily compensated by the other states' rates
				continue;
		}
		else //netRate < 0.0
		{
			double proportion = RateOut / trgE->rateOut;	//if netrate < 0, then rateOut exists, as does the sum
			//proportion represents how much of the rate this is in the totaal
			//we want to take that proportion of the total rate in, and remove that from the rate out
			deriv1 = deriv1 * proportion;
			deriv2 = deriv2 * proportion;	//make sure these are adjusted appropriately as well
			transfer = (trgE->rateIn * proportion - RateOut);
			transfer = transfer * time //taylor expansion of dR/dt, but derivis dR/dc, so dR/dt = dR/dc * dc/dt, where dc/dt = R.
				+ 0.5 * deriv1 * transfer * time * time	//say R(t) is the taylor series, and transfer is integral of R(t)dt
				+ transfer * transfer * time * time * time * deriv2 / 6.0;
			if(transfer >= 0.0)
				continue;
		}

		//this rate is from the viewpoint of the source,
		double fail = FailTransfer(trgE, transfer);
		transfer -= fail;
		if(transfer != 0.0 && trgE != NULL)
		{
			trgE->AddEndOccStates(transfer);
			trgE->SetBeginOccStates(trgE->GetEndOccStates());
		}

		//now get this to be the correct value for the source... which is significantly easier
		if(rt->netrate > 0.0)
			transfer = fabs(transfer);
		else
			transfer = -fabs(transfer);

		transferred += transfer;
	}
	return(transferred);
}

int MC_TranslateSourceERateToTarget(ELevelRate& rate, double& RateIn, double& RateOut, double& netRate, double& deriv1, double& deriv2)
{
	RateIn = RateOut = netRate = 0.0;
	if(rate.carrier == rate.targetcarrier && rate.carrier == rate.sourcecarrier)
	{
		//everything is the same - simplest. Swap Rout, Rin,
		RateIn = rate.RateOut;
		RateOut = rate.RateIn;
		netRate = DoubleSubtract(RateIn, RateOut);
	}
	else if(rate.carrier == rate.targetcarrier && rate.carrier != rate.sourcecarrier)
	{
		//this is like generation. The carrier type causes creation in both source and target
		//as well as loss in both source and target
		RateIn = rate.RateIn;
		RateOut = rate.RateOut;
		netRate = DoubleSubtract(RateIn, RateOut);
	}
	else if(rate.carrier != rate.targetcarrier && rate.carrier == rate.sourcecarrier)
	{
		//this is like recombination. The carrier type
		RateIn = rate.RateIn;
		RateOut = rate.RateOut;
		netRate = DoubleSubtract(RateIn, RateOut);
	}
	else if(rate.carrier != rate.targetcarrier && rate.carrier != rate.sourcecarrier)
	{
		//this is like wtf? this implies the source and target carrier are the same
		//so whatever is a rate out will be a rate in for the other
		RateIn = rate.RateOut;
		RateOut = rate.RateIn;
		netRate = DoubleSubtract(RateIn, RateOut);
	}
	deriv1 = rate.deriv;	//swapping S & t results in this being valid for SRH1/2, 3/4, Gen/rec, scatter, current
	switch(rate.type)
	{
		case RATE_SRH1:
		case RATE_SRH2:
		case RATE_SCATTER:
		case RATE_DRIFTDIFFUSE:
		case RATE_CONTACT_CURRENT_EXT:	//not entirely certain, but this is the case for drift diffuse, so probably.
			//but contact current has no target, so this shouldn't ever load?
		case RATE_CONTACT_CURRENT_INT:
		case RATE_CONTACT_CURRENT_INTEXT:
		case RATE_CONTACT_CURRENT_LINK:
			deriv2 = -rate.deriv2;
			break;
		case RATE_GENERATION:
		case RATE_RECOMBINATION:
		case RATE_SRH3:
		case RATE_SRH4:
		default:	//undef, light, stimulated emission, tunneling (not implemented yet)
			deriv2 = rate.deriv2;
			break;

	}
	
	return(0);
}

double FailTransfer(ELevel* eLev, double attempt)
{
	if(eLev == NULL)
		return(attempt);

	double fail = 0.0;
	double occ = eLev->GetBeginOccStates();
	double numSt = eLev->GetNumstates();
	double avail = numSt - occ;

	if(attempt > 0.0)
	{
		if(attempt > avail)
		{
			double newattempt = avail * eLev->percentTransfer;
			fail = attempt - newattempt;
		}
	}
	if(attempt < 0.0)
	{
		if(-attempt > occ)
		{
			double newattempt = -occ * eLev->percentTransfer;
			fail = attempt - newattempt;	//big negative number - small neg number = neg number
		}
	}

	return(fail);
}

int MC_OutputState(Model& mdl, ModelDescribe& mdesc)
{
	PosNegSum charge;

	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.cb.energies.begin(); el != curPt->second.cb.energies.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.vb.energies.begin(); el != curPt->second.vb.energies.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.donorlike.begin(); el != curPt->second.donorlike.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.acceptorlike.begin(); el != curPt->second.acceptorlike.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.CBTails.begin(); el != curPt->second.CBTails.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
		for(std::multimap<MaxMin<double>,ELevel>::iterator el = curPt->second.VBTails.begin(); el != curPt->second.VBTails.end(); ++el)
		{
			RecordELevelSSRates(el->second.Rates, curPt->second, mdesc.simulation);
			RecordELevelSSRates(el->second.OpticalRates, curPt->second, mdesc.simulation);
		}
	}
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		curPt->second.ResetData();
		curPt->second.Output.Allocate(1);	//allocate stuff for the last entry
		curPt->second.Output.Psi[0] = curPt->second.Psi;
		curPt->second.Output.Ec[0] = curPt->second.cb.bandmin;
		curPt->second.Output.Ev[0] = curPt->second.vb.bandmin;
		curPt->second.Output.fermi[0] = curPt->second.fermi;
		curPt->second.Output.fermin[0] = curPt->second.fermin;
		curPt->second.Output.fermip[0] = curPt->second.fermip ;
		curPt->second.Output.CarCB[0] = curPt->second.cb.GetNumParticles();
		curPt->second.Output.CarVB[0] = curPt->second.vb.GetNumParticles();
		curPt->second.Output.n[0] = curPt->second.n * curPt->second.voli;
		curPt->second.Output.p[0] = curPt->second.p * curPt->second.voli;
		curPt->second.Output.rho[0] = curPt->second.rho;
		curPt->second.Output.PointImpCharge[0] = (curPt->second.impdefcharge - curPt->second.elecindonor + curPt->second.holeinacceptor);
		curPt->second.Output.Jnx[0] = curPt->second.Jn.x * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jpx[0] = curPt->second.Jp.x * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jny[0] = curPt->second.Jn.y * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jpy[0] = curPt->second.Jp.y * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jnz[0] = curPt->second.Jn.z * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.Jpz[0] = curPt->second.Jp.z * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.genth[0] = curPt->second.genth * curPt->second.voli;
		curPt->second.Output.recth[0] = curPt->second.recth * curPt->second.voli;
		curPt->second.Output.R1[0] = curPt->second.srhR1 * curPt->second.voli;
		curPt->second.Output.R2[0] = curPt->second.srhR2 * curPt->second.voli;
		curPt->second.Output.R3[0] = curPt->second.srhR3 * curPt->second.voli;
		curPt->second.Output.R4[0] = curPt->second.srhR4 * curPt->second.voli;
		curPt->second.Output.scatter[0] = curPt->second.scatterR * curPt->second.voli;
		curPt->second.Output.Lgen[0] = curPt->second.LightAbsorbGen * curPt->second.voli;
		curPt->second.Output.Lsrh[0] = curPt->second.LightAbsorbSRH * curPt->second.voli;
		curPt->second.Output.Ldef[0] = curPt->second.LightAbsorbDef * curPt->second.voli;
		curPt->second.Output.Lscat[0] = curPt->second.LightAbsorbScat * curPt->second.voli;
		curPt->second.Output.Erec[0] = curPt->second.LightEmitRec * curPt->second.voli;
		curPt->second.Output.Esrh[0] = curPt->second.LightEmitSRH * curPt->second.voli;
		curPt->second.Output.Edef[0] = curPt->second.LightEmitDef * curPt->second.voli;
		curPt->second.Output.Escat[0] = curPt->second.LightEmitScat * curPt->second.voli;
		curPt->second.Output.LightEmit[0] = curPt->second.LightEmission * curPt->second.voli;
		curPt->second.Output.LPowerEnter[0] = curPt->second.LightPowerEntry;
		charge.AddValue(curPt->second.netCharge);
	}

	mdesc.simulation->negCharge = charge.GetNegSum() * QCHARGE;
	mdesc.simulation->posCharge = charge.GetPosSum() * QCHARGE;
	mdesc.simulation->TransientData->iterEndtime = mdesc.simulation->TransientData->curTargettime;	//move things up to the next target to process the next bit. Say this iteration took care of this section, which it did
	mdesc.simulation->TransientData->storevalueStartTime = mdesc.simulation->TransientData->iterEndtime;

	OutputCharge(mdesc.simulation, false);

	mdesc.simulation->binctr = 1;
	OutputDataToFileSS(mdl, mdesc);
	mdesc.simulation->binctr = 0;

	return(0);
}


