#ifndef SAAPD_BASIC_TYPES_H
#define SAAPD_BASIC_TYPES_H

#include <map>
#include <vector>

//used for determining how layers are applied
#define SHPPOINT 1			//the layer consists of a point... not a big layer...
#define SHPLINE 2			//the layer is situated as a line
#define TRIANGLE 3		//the layer is in the shape of a triangle
#define QUAD 4
#define RECTANGLE 5		//the layer is in the shape of a rectangle
#define PENTAGON 6		//the layer is in the shape of a pentagon
#define HEXAGON 7		//the " " " " "  hexagon
#define SQUARE 8		// " square
#define RIGID2D 9
#define ELLIPSE 10		//" " " " "  ellipse
#define CIRCLE 11		// " circle
#define SHAPE2DMAX 12			//just a reference to the highest shape defined
#define SHAPEMAX 13

#define EQUIVZERO_POSITION 1e-8	//1 angstrom...1 atom. so 0.5 - 0.4999999999998 (meaning 0.5) will be equiv to zero

#define LEFT 0	//when determining which thing to change
#define NX 0
#define RIGHT 1
#define PX 1
#define FORWARD 2
#define NY 2
#define BACKWARD 3
#define PY 3
#define UP 4
#define PZ 4
#define DOWN 5
#define NZ 5
#define NX2 6	//used in task sort along with what's above. Really only applies for tunneling.
#define PX2 7	//may be able to speed some other things with this info as well.
#define NY2 8
#define PY2 9
#define NZ2 10
#define PZ2 11

#define POSNEGDOUBLES 12	//don't change this value unless you change the AddValue/Subtract Value function

#define PNSUMALTER 1.0e10	//all the PNSUM's must vary by this magnitude!
#define PNSUMALTERI 1.0e-10	
#define PNSUM0 1.0e-60
#define PNSUM1 1.0e-50
#define PNSUM2 1.0e-40
#define PNSUM3 1.0e-30
#define PNSUM4 1.0e-20
#define PNSUM5 1.0e-10
#define PNSUM6 1.0
#define PNSUM7 1.0e10
#define PNSUM8 1.0e20
#define PNSUM9 1.0e30
#define PNSUM10 1.0e40
#define PNSUM11 1.0e50	//it needs to reset the values if larger than this!


class Vertex;
class Shape;
class XMLFileOut;

class XSpace
{
public:
	double x;
	double y;
	double z;
	XSpace();
	XSpace(double x1, double y1 = 0.0, double z1 = 0.0);

	XSpace& operator += (const XSpace &b);
	XSpace operator - (const XSpace &b) const;
	XSpace operator + (const XSpace &b) const;
	XSpace operator / (const double &b) const;
	XSpace operator * (const double &b) const;
	XSpace operator * (const XSpace &b) const;
	XSpace operator -();

	bool operator >(const XSpace &b) const;	//order of preference is x,y,z
	bool operator <(const XSpace &b) const;
	bool operator >=(const XSpace &b) const;
	bool operator <=(const XSpace &b) const;
	bool operator !=(const XSpace &b) const;
	bool operator ==(const XSpace &b) const;

	int Normalize(void);
	double Magnitude(void);
	double dot(XSpace b);
	void clear(void);
	void keepMax(XSpace b);	//the greater of the individual components is kept
	void keepMin(XSpace b);	//the lesser of the individual components is kept
	void SaveToXML(XMLFileOut& file, std::string topTag = "xspace", int precision=6);
};
class iXSpace
{
public:
	short int x;	//-32636 ... 32636?
	short int y;	//if the model has one dimension over ~32k grid points. wow.
	//unsigned short int z;
};
class PtRange
{
public:
	XSpace startpos;	//actual physical start (needs to be translated to a point from load)
	XSpace stoppos;		//physical end position (needs to be translated to a point from load)
	void SaveToXML(XMLFileOut& file);
};
class Vertex
{
public:
	XSpace pt;
	Vertex* next;
	Vertex* prev;
	Shape* parent;
	int id;
	void Remove(void);
};
class Line
{
public:
	XSpace pt1;
	XSpace pt2;
	double m;
	double b;
	double theta;		//angle wrt to Z. +Z = 0. [0,pi]
	double phi;			//angle wrt to XY plane. Counter Clockwise. +X = 0. [0,2pi]
	bool vertical;
	Line(XSpace Pta, XSpace Ptb);
	Line();
	bool Intersect(Line& lin);
	bool PointIn(XSpace pt);
	void CalcMB(void);
	void CalcAngles(void);
	double Magnitude(void);
	XSpace PtIn(double value, int coord);
};
class Shape
{
	friend class Vertex;
protected:
	int numpoints;
public:
	int type;	//note, for ellipse and circle
	
	XSpace center;
	XSpace max;
	XSpace min;
	Vertex* start;
	double GetXYArea();
	double GetYZArea();
	void AddPoint(XSpace pos);
	void RemovePoint(int id);
	void RemoveLastPoint();
	void SetMaxMin(void);
	bool InShape(XSpace pt, double deviation);
	XSpace getPoint(int index);
	int getNumVertices() { return(numpoints); }
	bool modifyVertex(int index, XSpace pos);

	void SaveToXML(XMLFileOut& file);
	Shape();
	~Shape();


};

class PosNegSum
{
	//	std::map<int,double> positive;	//only store positive values (8(start) + 4(size) + 8(fn table)) = 20
	//	std::map<int,double> negative;	//only store positive values (8(start) + 4(size) + 8(fn table)) = 20
	double positive[POSNEGDOUBLES];
	double negative[POSNEGDOUBLES];
	bool possumcalc;	//flag if the sums have been calculated and are accurate
	bool negsumcalc;
	bool netsumcalc;
	double possum; //+8
	double negsum; //+8
	double netsum; //+8 == 64
	double factor;	//how much each one is multiplied by. Essentially making it a floating point number
	double factorI;
	//	void ReduceMemoryFootprint(bool which);
	bool CalcFactorFromTerm(double newTermNormalized);
	bool AddTermCancel(int index, double val, bool pos=true, bool force=false);
public:
	double GetPosSum(void);
	double GetNegSum(void);
	double GetNetSum(void);
	double GetNetSumClear(void);
	void Clear(void);
	void SubtractValue(double val);
	void AddValue(double val);	//see if moving the function has any effect on the 0x0000040 being returned.
	//the offset to the call for this function may have been 0x40 (64).
	void AddValueCancel(double val);	//Add's a value, but cancels out existing numbers
	void SubtractValueCancel(double val);	//Add's a value, but cancels out existing numbers

	bool ChangedSinceNetSum(void) { return(!netsumcalc); }
	PosNegSum& operator += (const PosNegSum &b);
	PosNegSum& operator -= (const PosNegSum &b);
	PosNegSum operator - (const PosNegSum &b);
	PosNegSum operator + (const PosNegSum &b);
	PosNegSum& operator = (const PosNegSum &b);
	PosNegSum operator - ();
	bool operator == (const PosNegSum &b);

	PosNegSum();
	~PosNegSum();
};

class ProbPair
{
public:
	double RawProb;
	double Normalized;
};

//any 'negative' weights are considered 0, though the numbers are remembered
class VectorizedPFunc //this will be much quicker than the mapped version because everything is set/known beforehand
{
	std::vector<ProbPair> Probabilities;
	std::vector<unsigned int> Indices;	//the first index is the smallest probablility
	std::vector<double> NumberLine;	//the max for the particular index. Excludes all zeroes
	double totProb;
	double totProbi;
	double largestProb;		//so the largest normalized weight. Don't sift through computationally impossible values
	bool Normalized;
	bool ReadyforRandom;
	void Normalize(void);
	void SetNumberLine(void);

public:
	double AddProbability(double weight, long int index = -1);	//-1 means add it to end
	int SetProbability(double weight, long int index = -1);
	void Clear(void);
	bool Choose(unsigned int& RetIndex);
	double GetTotalProb(void);	//only looks at positive values
	double ExtractProb(long int index);
	double ExtractRaw(long int index);
	unsigned int GetSize(void) { return(Probabilities.size()); };
	int SetSize(unsigned int sz);
	VectorizedPFunc();
	~VectorizedPFunc();
};

template <typename T>
class MaxMin	//used to determine what the maximum and minimum values are for various coordinates
{
public:
	T max;
	T min;

	//order of preference is min, then max
	bool operator >(const MaxMin<T> &b) const
	{
		if (min > b.min)
			return(true);
		else if (min < b.min)
			return(false);

		if (max > b.max)
			return(true);

		return(false);
	}
	bool operator <(const MaxMin<T> &b) const
	{
		if (min < b.min)
			return(true);
		else if (min > b.min)
			return(false);

		if (max < b.max)
			return(true);

		return(false);
	}
	bool operator >=(const MaxMin<T> &b) const
	{
		if (min > b.min)
			return(true);
		else if (min < b.min)
			return(false);

		if (max > b.max)
			return(true);
		else if (max < b.max)
			return(false);

		return(true);
	}
	bool operator <=(const MaxMin<T> &b) const
	{
		if (min < b.min)
			return(true);
		else if (min > b.min)
			return(false);

		if (max < b.max)
			return(true);
		else if (max > b.max)
			return(false);

		return(true);
	}
	bool operator !=(const MaxMin<T> &b) const
	{
		if (min != b.min || max != b.max)
			return(true);
		return(false);
	}
	bool operator ==(const MaxMin<T> &b) const
	{
		if (min == min && max == max)
			return(true);
		return(false);
	}
};


template <class retType>
class PartitionFunc	//partition function for each
{
public:
	PartitionFunc();
	void Clear(void);		//clears out everything stored in the probability trees
	bool Choose(retType& retVal);	//choose a variable, do after ProbToNumLine is called
	void ProbToNumLine(void);	//convert everything onto the number line.
	void AddProbability(double p, retType addhere);	//add a number to the tree.
	void SetProbability(double p, retType addhere);
	bool HasOptions(void);
	int RemoveProbability(retType remove, bool dirty = false);	//removes a probability
	unsigned int GetSize(void) { return(Probs.size()); };
	bool ItemExists(retType item);	//checks to see if the item exists
	double ExtractProb(retType item);	//what is the probability of the item
	double GetTotalProb(void) { return(TotalProb); };

private:
	bool ReadyToChoose;	//make code safe. Now all that's needed to do is AddProbability and Choose.
	double TotalProb;	//the total probability before normalization
	std::map<double, retType> ProbsSort; //Used for the number line.
	std::map<retType, double> Probs;	 	//for adding probabilities to everything. Keeps the raw data.
};


template <class retType>
PartitionFunc<retType>::PartitionFunc()
{
	Clear();
}

template <class retType>
void PartitionFunc<retType>::Clear(void)
{
	Probs.clear();
	ProbsSort.clear();
	ReadyToChoose = false;
	TotalProb = 0.0;												//set the total prob. to 0
	return;
}

template <class retType>
bool PartitionFunc<retType>::Choose(retType& retVal)	//choose where this electron is going to go
{
	if (!ReadyToChoose)
		ProbToNumLine();	//make it ready!
	double random = SciRandom.Uniform01();//(double)rand()/(double)RAND_MAX;	//random number 0...1
	std::map<double, retType>::iterator Spot = ProbsSort.lower_bound(random); // ProbsSort.FindFirstGreaterEqual(random);
	if (Spot != ProbsSort.end())
	{
		retVal = Spot->second;	//return the associated data
		return(true);
	}
	return(false);

	/*
	Sample: Probs = 0.2, 0.25, 0.15, 0.4
	Generated num line: 0.2, 0.45, 0.6, 1.0  --- [0-0.2, 0.2-0.45, 0.45-0.6, 0.6-1.0]
	Random number (0 to 1) generated: 0.49
	is 0.49 > 0.2? Yes. Keep going
	is 0.49 > 0.45? Yes. Keep going
	is 0.49 < 0.6? No. Stop.  It chooses the .45-.6 range.
	Return that spot index.
	*/
}
template <class retType>
bool PartitionFunc<retType>::ItemExists(retType item)
{

	if (Probs.count(item) > 0)
		return(true);
	return(false);

	/*Tree<double,retType>* exist = Probs.Find(item);
	if(exist == NULL)
	return(false);

	return(true);
	*/
}

template <class retType>
int PartitionFunc<retType>::RemoveProbability(retType remove, bool dirty)
{
	int removed = Probs.erase(remove);	//remove it. There can only be one... If none are removed, erase returns zero so it returns an error of -1
	/*	Tree<double, retType>* probRemove = Probs.Find(remove);
	if(probRemove == NULL)
	return(-1);

	probRemove->Remove(remove, true);
	*/
	if (removed > 0 && (dirty == false || HasOptions() == false))	//there's nothing to choose from, so say not ready to choose
		ReadyToChoose = false;	//dirty is just a means of making calculations go significantly faster. The probability # line IS messed up
	if (removed == 0)
		return(-1);	//signal that none were returned
	return(0);	//as the value below will absorb the probability from the next item. But if there are a lot of numbers, we don't want to be
	//recalculating it EVERY time.

}

template <class retType>
double PartitionFunc<retType>::ExtractProb(retType item)
{
	std::map<retType, double>::iterator node = Probs.find(item);
	if (node == Probs.end())
		return(0.0);

	if (!ReadyToChoose)
		ProbToNumLine();	//make it ready!

	return(node->second);
}

template <class retType>
void PartitionFunc<retType>::ProbToNumLine(void)	//Normalize the probabilities onto a number line from 0...1
{										//so they are actual percentages and can be chosen randomly
	TotalProb = 0.0;	//reset this for a new claculation of the number line.
	ProbsSort.clear();
	std::multimap<double, retType> tmpSort;	//used to get lowest probabilities first in a std::map
	for (std::map<retType, double>::iterator itP = Probs.begin(); itP != Probs.end(); ++itP)
	{
		TotalProb += itP->second;	//need the total sum to normalize
		tmpSort.insert(std::pair<double, retType>(itP->second, itP->first));	//want to do lowest probabilities first
	}

	if (TotalProb <= 0.0)	//there is nothing...
	{
		tmpSort.clear();	//this should be empty, but be safe
		return;
	}
	TotalProb = 1.0 / TotalProb;
	double probIndiv = 0.0;
	double runningSum = 0.0;
	unsigned long int ct = 0;
	unsigned long int last = tmpSort.size() - 1;
	for (std::map<double, retType>::iterator it = tmpSort.begin(); it != tmpSort.end(); ++it)
	{
		runningSum += it->first;
		probIndiv = runningSum * TotalProb;
		if (ct == last)
			probIndiv = 1.0;
		ProbsSort.insert(std::pair<double, retType>(probIndiv, it->second));
		ct++;
	}
	tmpSort.clear();	//clear out the temporary memory used for the number line
	ReadyToChoose = true;
	return;
}

//adds a probability to an existing probability if its there. Otherwise, creates a new entry to vector
template <class retType>
void PartitionFunc<retType>::AddProbability(double p, retType addhere)
{

	ReadyToChoose = false;
	std::map<retType, double>::iterator ptr;

	ptr = MapAdd(Probs, addhere, p);	//will add if it exists, otherwise will create a new spot.

	if (ptr->second <= 0.0)	//if the probability goes less than zero, it will remove the probability.
		Probs.erase(ptr);

	return;
}

//adds a probability to an existing probability if its there. Otherwise, creates a new entry to vector
template <class retType>
void PartitionFunc<retType>::SetProbability(double p, retType addhere)
{

	ReadyToChoose = false;
	std::map<retType, double>::iterator ptr;

	ptr = MapInsertReplace(Probs, addhere, p);	//will add if it exists, otherwise will create a new spot.

	if (ptr->second <= 0.0)	//if the probability goes less than zero, it will remove the probability.
		Probs.erase(ptr);

	return;
}

template <class retType>
bool PartitionFunc<retType>::HasOptions(void)
{
	if (Probs.size() > 0)
		return(true);
	else
		return(false);
}



#endif