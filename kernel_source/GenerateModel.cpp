#include "GenerateModel.h"
//has vector, map, basicTypes, and model.h in GenerateModel.h - needed for function def's
#include <list>

#include "transient.h"
#include "ss.h"
#include "contacts.h"
#include "outputdebug.h"
#include "output.h"
#include "light.h"
#include "physics.h"
#include "srh.h"
#include "Materials.h"


/*
Need to tie impurity descriptors to impurities
Need to tie material descriptors to materials
Needs to be done for each layer

*/
int ModelDescribe::GenerateID()
{
	//first determine the highest id, and then go one higher.
	static int id;	//defaults to zero
	if(id == 0)	//it hasn't found the largest id yet
	{
		std::vector<Material*>::iterator itMat;
		std::vector<Impurity*>::iterator itImp;
		for(itMat = materials.begin(); itMat != materials.end(); itMat++)
		{
			if(id < (*itMat)->getID())
				id = (*itMat)->getID();
		}
		for(itImp = impurities.begin(); itImp != impurities.end(); itImp++)
		{
			if(id < (*itImp)->getID())
				id = (*itImp)->getID();
		}
	}
	//id now holds the largest id there is being used
	id++;	//so this one is guaranteed to be higher...
	return(id);
}
int ModelDescribe::LinkID()
{
	std::vector<Layer*>::iterator itLayer;
	std::vector<Material*>::iterator itMat;
	std::vector<Impurity*>::iterator itImp;
	std::vector<ldescriptor<Material*>*>::iterator itLMat;
	std::vector<ldescriptor<Impurity*>*>::iterator itLImp;
	int id;
	std::vector<int> RemoveIndices;

	for(itLayer = layers.begin(); itLayer != layers.end(); itLayer++)	//go through each layer
	{
		for(itLMat = (*itLayer)->matls.begin(); itLMat != (*itLayer)->matls.end(); itLMat++)
		{	//go through each material descriptor of each layer
			if ((*itLMat)->data == nullptr && (*itLMat)->id < 0) { //this is not associated with anything whatsoever
				//so best bet is to look for any materials that aren't associated with anything
				for (itMat = materials.begin(); itMat != materials.end(); ++itMat) {
					if ((*itMat)->needsNewID()) {	//we'll arbitrarily link these two.
						(*itLMat)->id = GenerateID();
						(*itMat)->SetID((*itLMat)->id);
						(*itLMat)->data = *itMat;
						break;
					}
				}

				if ((*itLMat)->data == nullptr) //didn't find anything it can link to that is not seemingly assigned
					RemoveIndices.push_back(itLMat - (*itLayer)->matls.begin()); //mark this layer descriptor for deletion
			}
			else if ((*itLMat)->id < 0) { //data cannot be null, and it doesn't know what this id is
				((*itLMat)->id = (*itLMat)->data->getID());
			}
			else if ((*itLMat)->data == nullptr) // there is a valid id, but it doesn't know what data is
			{ //it has an id, but doesn't know what the material is
				for (itMat = materials.begin(); itMat != materials.end(); itMat++) {	//search through all the materials for that particular id
					if ((*itMat)->isMaterial(id)) {	//match found. The descriptor should point to this material.
						(*itLMat)->data = *itMat;	//the value of the iterator is the address for the pointer
						break;		//stop searching for this descriptor.
					}
				}
				if ((*itLMat)->data == nullptr)	//it never found a material with a matching id
					RemoveIndices.push_back(itLMat - (*itLayer)->matls.begin());
			}
			else { //both data and id are assigned
				if (!(*itLMat)->data->isMaterial((*itLMat)->id)) {
					//there is a mismatch between the material assigned to this ldescriptor and the material's ids.
					//the safest course of action is to just reassign the id. looking for a new material that has
					//the descriptor's id might not exist
					(*itLMat)->id = (*itLMat)->data->getID();
				}
			}
		}
		for (unsigned int i = RemoveIndices.size() - 1; i < RemoveIndices.size(); --i)
			(*itLayer)->matls.erase((*itLayer)->matls.begin() + RemoveIndices[i]);
		RemoveIndices.clear();



		for (itLImp = (*itLayer)->impurities.begin(); itLImp != (*itLayer)->impurities.end(); itLImp++)
		{	//go through each material descriptor of each layer
			if ((*itLImp)->data == nullptr && (*itLImp)->id < 0) { //this is not associated with anything whatsoever
				//so best bet is to look for any impurities that aren't associated with anything
				for (itImp = impurities.begin(); itImp != impurities.end(); ++itImp) {
					if ((*itImp)->needsNewID()) {	//we'll arbitrarily link these two.
						(*itLImp)->id = GenerateID();
						(*itImp)->setID((*itLImp)->id);
						(*itLImp)->data = *itImp;
						break;
					}
				}

				if ((*itLImp)->data == nullptr) //didn't find anything it can link to that is not seemingly assigned
					RemoveIndices.push_back(itLImp - (*itLayer)->impurities.begin()); //mark this layer descriptor for deletion
			}
			else if ((*itLImp)->id < 0) { //data cannot be null, and it doesn't know what this id is
				((*itLImp)->id = (*itLImp)->data->getID());
			}
			else if ((*itLImp)->data == nullptr) // there is a valid id, but it doesn't know what data is
			{ //it has an id, but doesn't know what the material is
				for (itImp = impurities.begin(); itImp != impurities.end(); itImp++) {	//search through all the materials for that particular id
					if ((*itImp)->isImpurity(id)) {	//match found. The descriptor should point to this material.
						(*itLImp)->data = *itImp;	//the value of the iterator is the address for the pointer
						break;		//stop searching for this descriptor.
					}
				}
				if ((*itLImp)->data == nullptr) //didn't find anything it can link to that is not seemingly assigned
					RemoveIndices.push_back(itLImp - (*itLayer)->impurities.begin());
			}
			else { //both data and id are assigned
				if (!(*itLImp)->data->isImpurity((*itLImp)->id)) {
					//there is a mismatch between the material assigned to this ldescriptor and the material's ids.
					//the safest course of action is to just reassign the id. looking for a new material that has
					//the descriptor's id might not exist
					(*itLImp)->id = (*itLImp)->data->getID();
				}
			}
		}
		for (unsigned int i = RemoveIndices.size() - 1; i < RemoveIndices.size(); --i)
			(*itLayer)->impurities.erase((*itLayer)->impurities.begin() + RemoveIndices[i]);
		RemoveIndices.clear();
		
		//defects are automatically associated with a material, and become associated with a layer when the material
		//is associated with the layer.

	}
	
	return(0);
}

/*

The fun part.  Turning the hierarcheral data into discrete data points.
Method:
Generate a bleep ton of points that define where the model exists as per modeldescribe.
If that is not defined, look at exclusive layers to achieve a rough size, and order them so
the first layer in the list is the top layer.

For each point, determine what layers it is a member of.
Check for conflicting materials
If the materials conflict, use functions to weight the likelihood of generating a particular material.
Assign the point to said material.
calculate the band gap for this point in the material (dependencies on temperature and doping)
Add linked defects to the energy states appropriately for the point.
Determine likelihood/number of impurities present based on all overlapping layers
Add appropriate number of states and holes/electrons in proper positions.

Initialize the rest of the point data based on the material present.

If a layer appears that is exclusive, choose that layer and nothing else for the said points.

Afterwards, reduce the model...

Start coalescing points away from the layer boundaries or variables of interest based on the center and edge
differences within the layers (add the number of states, particles, etc. together), and update the adjacent to
field indexes.

After complete, double check each point on its number of states compared to the number of impurities present to
make sure, for example, if 15 points merged, each with a 1/5 probability of having a point, the new point has
roughly 3 states. - may be important if hundred of states merge, all with negligible probability into a state with
a good probability, but the negligible was so low it never occurred. Force it to match the described curves for the
model.

Mostly interested in iterating through all the points when solving, so after completion, form a dynamic array
of class point, put each element of the tree in the array based off the sort value (index).
To do this: take the size of the tree, create an array of all the old sort values.
Start assigning values to the new dynamic array of points. As this is done, put in the present index into
the old sort value aray[oldpoint], so oldsort[oldpoint] = index;
aka, oldsort[oldpoint] = NewPointContainingIt;

Delete the tree. Free a bleep-ton of memory.

Normalize the point indexes...

Go through each point and read its adjacent point value(s) (index adj) and reassign it to
the new point. so in generic terms:
MyPoint[index].adjPX[adj] = oldsort[MyPoint[index].adjPX[adj]];
(check for -1, or EXTERNAL to device before putting in array brackets. If it is external, then adjPX[#]=EXT; )

delete the oldsort array of integers.

This way everything will compress down to the proper number of points and no bad bound accesses should occur
when the point indexes are essentially normalized.

Ready Go!
*/

//make sure enough parameters are effectively set for the model to generate a point model
//otherwise, form defaults
int ModelDescribe::CheckModel()
{
	if(materials.size() == 0)
		return Error_NoMaterials;	//FAIL. Can't do anything without any material parameters!

	if (layers.size() == 0)
		return Error_NoLayers;

	if(NDim > 3 || NDim <= 0)
		NDim = 1;	//travels in x by default
	
	if(simulation == NULL)	//no simulation data loaded, generate new data
	{
		Simulation* simul = new Simulation;
		simulation = simul;
		simul->lightincidentdir = LEFT;	//from negative X direction 
	}
	if(simulation->mdesc == NULL)	//this will likely be the assignment - because I'm lazy
		simulation->mdesc = this;

	if(simulation->T == 0)
		simulation->T = 300;		//room temperature


	if(simulation->TransientData)
	{
		if(simulation->TransientData->timestep == 0)
		{
			simulation->TransientData->timestep = 0;
			simulation->TransientData->timestepi = 0;
		}
		if (simulation->TransientData->simendtime == 0.0)
			simulation->TransientData->simendtime = 1.0e-8;

		if (simulation->TransientData->ContactData.size() == 0) {
			//create two contacts
			int id1, id2;
			id1 = simulation->contacts[0]->id;
			id2 = simulation->contacts[1]->id;

			TransContact *tCt = new TransContact(simulation->getContact(id1));
			LinearTimeDependence tmplin;
			tmplin.SetValues(0.0, 0.0, 0.0, CONTACT_HOLD_INDEFINITELY, 0.0, CONTACT_VOLT_LINEAR, false);
			tCt->AddLinearDependence(tmplin, CTC_LIN_VOLT, 0.0);
			simulation->TransientData->ContactData.push_back(tCt);

			tCt = new TransContact(simulation->getContact(id2));
			tCt->AddLinearDependence(tmplin, CTC_LIN_VOLT, 0.0);
			simulation->TransientData->ContactData.push_back(tCt);

		}

		
	}

	
	if(simulation->statedivisions <= 0)
		simulation->statedivisions = 10;	//have 10 bands by default
	if(simulation->accuracy == 0)
		simulation->accuracy = 1.0e-6;	//get Ef within 10^-6 of actual value.
	
	
	//all simulation data has been generated roughly...

	if(Ldimensions.x < 0)
		Ldimensions.x = 0;
	if(Ldimensions.y < 0)
		Ldimensions.y = 0;

	if(Ldimensions.x == 0.0 && Ldimensions.y == 0.0)
	{	//the dimensions were not set. try and determine from layers. This means it was a 1D model.
		std::vector<Layer*>::iterator itLay;
		for(itLay = layers.begin(); itLay != layers.end(); itLay++)
		{
			if(Ldimensions.x < (*itLay)->shape->max.x)
				Ldimensions.x = (*itLay)->shape->max.x;
			if(Ldimensions.y < (*itLay)->shape->max.y)
				Ldimensions.y = (*itLay)->shape->max.y;
		}
	}
	//check for a resolution
	if(resolution.x < 1e-8)	//1 Angstrom...
		resolution.x = Ldimensions.x / 1000.0;	//set up for max 1000 points
	if(resolution.y < 1e-8 && NDim >= 2)
		resolution.y = Ldimensions.y / 1000.0;	//set up for 1000 points. Note: Ldimensions =0, mdlresolution = 0;

	if(NDim == 1)
	{
		if(Ldimensions.y == 0)
			Ldimensions.y = 1e-4;	//default to 1 micron
		if(Ldimensions.z == 0)
			Ldimensions.z = 1e-4;	//default to 1 micron

		resolution.y = Ldimensions.y;	//1D, so make sure the resolution matches the dimensions
		resolution.z = Ldimensions.z;		
	}
	if(NDim == 2)
	{
		if(Ldimensions.z == 0)
			Ldimensions.z = 1e-4;	//default to 1 micron
		resolution.z = Ldimensions.z;
	}

	//check each material
	double maxE = 0.0;
	
	for(auto itMat = materials.begin(); itMat != materials.end(); itMat++)
	{	
		//all variables verified during setters
		double tmpE = (*itMat)->getBandGap() + 2.0*(*itMat)->getAffinity();
		if(tmpE > maxE)
			maxE = tmpE;
	}
	
	if(simulation->EnabledRates.OpticalEMin < 0.0)
		simulation->EnabledRates.OpticalEMin = 0.0;
	if(simulation->EnabledRates.OpticalEMax < 0.0)	//give it the actual maximum energy that can be emitted or absorbed
		simulation->EnabledRates.OpticalEMax = maxE;
	if(simulation->EnabledRates.OpticalEResolution <= 0.0)
		simulation->EnabledRates.OpticalEResolution = 0.001;
	
	simulation->SetMaxOpticalSize();


	for(auto itImp = impurities.begin(); itImp != impurities.end(); itImp++)
	{		
		(*itImp)->testDefaultCharge();
		//all the other variables are verified during setters
	}

	//if going in the dark, there may not be a spectrum, so don't check for this existence.

	

	//now check all the layers for validity and correct as necessary
	//there must be at LEAST one layer and one material present.
	std::vector<Layer*>::iterator itLay;

	std::vector<ldescriptor<Impurity*>*>::iterator itLImp;
	bool linkedmaterial = false;

	for(itLay = layers.begin(); itLay != layers.end(); itLay++)
	{
		if((*itLay)->matls.size() > 0)
			linkedmaterial = true;		//make sure a material exists within the model.

		for(itLImp = (*itLay)->impurities.begin(); itLImp != (*itLay)->impurities.end(); itLImp++)
		{
			if((*itLImp)->multiplier < 0)
				(*itLImp)->multiplier = 1.0;
		}

		//a layer can exist outside of the device, so checking the center and position is pointless
		if((*itLay)->shape->type < SHPPOINT || ((*itLay)->shape->type > SHAPEMAX))
			(*itLay)->shape->type = SHPPOINT;

		if((*itLay)->spacingboundary < resolution.x)
			(*itLay)->spacingboundary = resolution.x;
		if((*itLay)->spacingcenter < resolution.x)
			(*itLay)->spacingcenter = resolution.x;

		if ((*itLay)->deviation.x < resolution.x)
			(*itLay)->deviation.x = resolution.x;
		if ((*itLay)->deviation.y < resolution.y)
			(*itLay)->deviation.y = resolution.y;
		if ((*itLay)->deviation.z < resolution.z)
			(*itLay)->deviation.z = resolution.z;
		
		

		if(NDim > 1)
		{
			if((*itLay)->spacingboundary < resolution.y)
				(*itLay)->spacingboundary = resolution.y;
			if((*itLay)->spacingcenter < resolution.y)
				(*itLay)->spacingcenter = resolution.y;

			if(NDim > 2)
			{
				if((*itLay)->spacingboundary < resolution.z)
					(*itLay)->spacingboundary = resolution.z;
				if((*itLay)->spacingcenter < resolution.z)
					(*itLay)->spacingcenter = resolution.z;
			}
		}

		//everything else can be just about anything.
	}	//end default layer setup




	if(linkedmaterial==false)
		return(Error_LayerMaterialNoLinks);	//FAIL. No way to know how materials are associated with layers.  If no layers specified,
					//then the layers are created based on the materials...
	return(Error_None);
}

Boundaries GenerateBoundaryLines(ModelDescribe& mdl)	//used to create a mesh
{
	Boundaries bounds;
	//want to iterate through each layer to find all the bounds...
	std::vector<Layer*>::iterator it;
	for(it = mdl.layers.begin(); it != mdl.layers.end(); it++)
	{
		if((*it)->shape != NULL && (*it)->AffectMesh == true)	//make sure the shape actually points somewhere
		{	//and make sure this mesh should be used to generate the mesh
			if((*it)->shape->type > RIGID2D && (*it)->shape->type < SHAPE2DMAX)	//curve 2D
			{
				bounds.curves.push_back((*it)->shape);
			}	//just push a reference to the entire shape in
			else if((*it)->shape->start != NULL)	//now it's an actual object
			{
				Line temp;
				Vertex* pt1 = (*it)->shape->start;
				if(mdl.NDim > 1)
				{
					Vertex* pt2 = pt1->prev;
					if(pt2 != pt1->next)	//not a 2 point system
					{
						do	//cycle through all the starting bits. for points registered as shapes,
						{	//this will generate a line of equal dimensions
							temp = Line(pt1->pt, pt2->pt);	//initialize everything in the structure.
							bounds.lines.push_back(temp);	//add it to the list
	
							pt2 = pt1;	//keep going until pt1 is the start again
							pt1 = pt1->next;
						}while(pt1 != (*it)->shape->start);
					}
					else	//only two points in the shape (or maybe 1?) - only record one set
					{
						temp = Line(pt1->pt, pt2->pt);
						bounds.lines.push_back(temp);
					}
				}
				else	//it's one dimensional. The points are the boundaries, not the line itself.
				{
					do	//cycle through all the starting bits. for points registered as shapes,
					{	//this will generate a line of equal dimensions
						temp = Line(pt1->pt, pt1->pt);	//initialize everything in the structure.
						bounds.lines.push_back(temp);	//add it to the list

						pt1 = pt1->next;
					}while(pt1 != (*it)->shape->start);
				}
			}
		}
	}

	return(bounds);
}
Boundaries::Boundaries(void)
{
	if(!lines.empty())
		lines.clear();
	if(!curves.empty())
		curves.clear();
}
Boundaries::~Boundaries(void)
{
	if(!lines.empty())
		lines.clear();
	if(!curves.empty())
		curves.clear();
}

double Magnitude(XSpace vect)
{
	return(pow(vect.x*vect.x + vect.y*vect.y + vect.z*vect.z, 0.5));
}

//check if a particular position is within a layer. Do a 2D implementation for now.
MiniPoint::MiniPoint(void)
{
	grow = true;	//set the point to want to grow
	active = true;
	adj.coordMx = -1;
	adj.coordMx2 = -1;
	adj.coordPx = -1;
	adj.coordPx2 = -1;
	adj.coordMy = -1;
	adj.coordMy2 = -1;
	adj.coordPy = -1;
	adj.coordPy2 = -1;
	adj.self = -1;
	priority = false;
	last = false;
}

//create layers and push them into the model.  they will have no physical bearing on the model except to
//restrict points from merging until the end.  This "should" help clean up the right of pieces.
int GenerateLastShapes(std::vector<Shape*>& lastShapes, Boundaries& bounds)
{
	std::vector<Line>::iterator itLine;
	std::vector<Shape*>::iterator itShape;	//first make sure it's empty
	for(itShape = lastShapes.begin(); itShape != lastShapes.end(); itShape++)
	{
		delete (*itShape);
		*itShape = NULL;
	}
	if(!lastShapes.empty())
		lastShapes.clear();
	lastShapes.reserve(bounds.lines.size() + bounds.curves.size());

	for(itLine = bounds.lines.begin(); itLine != bounds.lines.end(); itLine++)
	{
		if((*itLine).vertical == false)	//if vertical true, don't worry about it.
		{
			if((*itLine).m < 0)	//make sure it's not horizontal, which is also a freebie.
			{
				XSpace max;
				XSpace pt;
				Shape* shape = new Shape;

				pt = (*itLine).pt1;
				if(max.x < pt.x)
					max.x = pt.x;
				if(max.y < pt.y)
					max.y = pt.y;

				shape->AddPoint(pt);

				pt = (*itLine).pt2;
				if(max.x < pt.x)
					max.x = pt.x;
				if(max.y < pt.y)
					max.y = pt.y;

				shape->AddPoint(pt);
				shape->AddPoint(max);
				shape->type = TRIANGLE;

				lastShapes.push_back(shape);
			}
			else if((*itLine).m > 0)	//don't want m=0
			{				//do the same exact thing... EXCEPT now it's positively sloped, so the desired value
				XSpace max;	//for the point to even it off is the max x, but the minimum y.
				XSpace pt;
				Shape* shape = new Shape;

				pt = (*itLine).pt1;
				if(max.x < pt.x)
					max.x = pt.x;
				if(max.y > pt.y)
					max.y = pt.y;

				shape->AddPoint(pt);

				pt = (*itLine).pt2;
				if(max.x < pt.x)
					max.x = pt.x;
				if(max.y > pt.y)
					max.y = pt.y;

				shape->AddPoint(pt);
				shape->AddPoint(max);
				shape->type = TRIANGLE;

				lastShapes.push_back(shape);	//include this shape among the outputs.
			}
		}
	}

	for(itShape = bounds.curves.begin(); itShape != bounds.curves.end(); itShape++)
	{
		if((*itShape)->type == CIRCLE)
		{
			XSpace center = (*itShape)->center;	//should point to the center of the circle
			XSpace pt = (*itShape)->start->pt;	//the first point in the circle.
			//this way, 2 points can define a circle by diameter, or four points can by each radius, etc.
			double radius = AbsDistance(pt, center);
			
			Shape* shape = new Shape;
			shape->type = RECTANGLE;

			pt = center;	//start at center of circle
			pt.y += radius;	//now it points to the upper left corner of the box
			shape->AddPoint(pt);	//add upper left corner

			pt.x += radius;	//now it points to the upper right corner of the box
			shape->AddPoint(pt);

			pt.y += -2.0*radius;	//now it points to the lower right corner of the box
			shape->AddPoint(pt);

			pt.x -= radius;	//now it lpoints to the lower left corner of the box
			shape->AddPoint(pt);

			lastShapes.push_back(shape);	//add it to the list of shapes.			

		}
		else	//an ellipse...or something else weird. too many odd shape possibilities, best to go with max/min
		{		//coords of a rectangle.
			XSpace max = (*itShape)->max;
			XSpace min = (*itShape)->min;

			Shape* shape = new Shape;
			shape->type = RECTANGLE;

			XSpace pt = max;
			shape->AddPoint(pt);	//the back right point
			
			pt.x = min.x;		//the back left point
			shape->AddPoint(pt);

			pt.y = min.y;		//the front left point
			shape->AddPoint(pt);

			pt.x = max.x;		//the front right point
			shape->AddPoint(pt);

			lastShapes.push_back(shape);
		}
	}

	return(0);
}

//the descriptors are in reference to the layer they are in for the model.
//they all need to be adjusted by the starting position of the layer (mxmy of shape
int GlobalizeDescriptors(ModelDescribe& mdl)
{
	std::vector<Layer*>::iterator itLay;
	std::vector<ldescriptor<Impurity*>*>::iterator itLImp;
	std::vector<ldescriptor<Material*>*>::iterator itLMat;
	for(itLay = mdl.layers.begin(); itLay != mdl.layers.end(); itLay++)
	{
		for(itLImp = (*itLay)->impurities.begin(); itLImp != (*itLay)->impurities.end(); itLImp++)
		{
			(*itLImp)->range.startpos += (*itLay)->shape->min;
			(*itLImp)->range.stoppos += (*itLay)->shape->min;
		}
		for(itLMat = (*itLay)->matls.begin(); itLMat != (*itLay)->matls.end(); itLMat++)
		{
			(*itLMat)->range.startpos += (*itLay)->shape->min;
			(*itLMat)->range.stoppos += (*itLay)->shape->min;
		}
	}
	return(0);
}


/*
int GenerateMesh(ModelDescribe& mdl, Model& model)
{
	Boundaries bounds = GenerateBoundaryLines(mdl);
	int dimx = 0;
	int dimy = 0;
	int dimz = 0;
	if(mdl.resolution.x != 0)
		dimx = (int)(mdl.Ldimensions.x / mdl.resolution.x);
	if(mdl.resolution.y != 0)
		dimy = (int)(mdl.Ldimensions.y / mdl.resolution.y);
	if(mdl.resolution.z != 0)
		dimz = (int)(mdl.Ldimensions.z / mdl.resolution.z);
	
	//the numbers aren't necessarily exact, so if it's slightly off (say, 1e-12) - it won't get the last point
	if( (double)(dimx) * mdl.resolution.x < mdl.Ldimensions.x - 0.5*mdl.resolution.x)
		dimx++;
	if( (double)(dimy) * mdl.resolution.y < mdl.Ldimensions.y - 0.5*mdl.resolution.y)
		dimy++;
	if( (double)(dimz) * mdl.resolution.z < mdl.Ldimensions.z - 0.5*mdl.resolution.z)
		dimz++;

	if(dimx == 0)
		dimx = 1;
	if(dimy == 0)
		dimy = 1;

	if(mdl.NDim == 1)	//ensure there's only one point in the other dimensions if needed
	{
		dimy = 1;
		dimz = 1;
	}
	else if(mdl.NDim == 2)
		dimz = 1;


	long int numpoints = dimx * dimy;// * dimz;

	MiniPoint* allpoints = new MiniPoint[numpoints];

	int z=0;
	long int counter = 0;

	std::vector<Shape*> lastShapes;
	GenerateLastShapes(lastShapes, bounds);
	

	for(int y = 0; y<dimy; y++)
	{
		for(int x = 0; x<dimx; x++)
		{
			//set the positions defining this point

			allpoints[counter].center.x = ((double)(x)+0.5)*mdl.resolution.x;
			allpoints[counter].center.y = ((double)(y)+0.5)*mdl.resolution.y;
			allpoints[counter].center.z = ((double)(z)+0.5)*mdl.resolution.z;
			allpoints[counter].mxmy.x = (double)(x) * mdl.resolution.x;
			allpoints[counter].mxmy.y = (double)(y) * mdl.resolution.y;
			allpoints[counter].mxmy.z = (double)(z) * mdl.resolution.z;
			allpoints[counter].pxpy.x = ((double)(x) + 1.0)*mdl.resolution.x;
			allpoints[counter].pxpy.y = ((double)(y) + 1.0)*mdl.resolution.y;
			allpoints[counter].pxpy.z = ((double)(z) + 1.0)*mdl.resolution.z;
			//allow this point to grow by default
			allpoints[counter].grow = true;
			allpoints[counter].active = true;

			//now set all the adjacent coordinates. It may have at most two adjacent points in each direction
			allpoints[counter].adj.self = counter;
			if(x > 0)
			{
				allpoints[counter].adj.coordMx = counter-1;
				allpoints[counter].adj.coordMx2 = counter-1;
			}
			else if(mdl.NDim > 1 && dimy >=1)
			{
				allpoints[counter].adj.coordMx = counter + dimx - 1;
				allpoints[counter].adj.coordMx2 = counter + dimx - 1;
			}
			else
			{
				allpoints[counter].adj.coordMx = EXT;
				allpoints[counter].adj.coordMx2 = EXT;
			}
			if(x < dimx - 1)
			{
				allpoints[counter].adj.coordPx = counter + 1;
				allpoints[counter].adj.coordPx2 = counter + 1;
			}
			else if(mdl.NDim > 1 && dimy >=1)
			{
				allpoints[counter].adj.coordPx = counter - dimx + 1;
				allpoints[counter].adj.coordPx2 = counter - dimx + 1;
			}
			else
			{
				allpoints[counter].adj.coordPx = EXT;
				allpoints[counter].adj.coordPx2 = EXT;
			}
			if(y > 0)
			{
				allpoints[counter].adj.coordMy = counter - dimx;
				allpoints[counter].adj.coordMy2 = counter - dimx;
			}
			else
			{
				allpoints[counter].adj.coordMy = EXT;
				allpoints[counter].adj.coordMy2 = EXT;
			}
			if(y < dimy - 1)
			{
				allpoints[counter].adj.coordPy = counter + dimx;
				allpoints[counter].adj.coordPy2 = counter + dimx;
			}
			else
			{
				allpoints[counter].adj.coordPy = EXT;
				allpoints[counter].adj.coordPy2 = EXT;
			}

			//now set the resolution of this device...
			//first need to determine the finest resolution of this particular layer
			std::vector<Layer*>::iterator itLay;

			allpoints[counter].TargetRes.x = mdl.Ldimensions.x;	//set default size to length of device
			allpoints[counter].TargetRes.y = mdl.Ldimensions.y;	//if a layer demands a better resolution, make
			//it a better resolution...

			double MinBoundarySpacing = mdl.Ldimensions.x + mdl.Ldimensions.y;
			double MinCenterSpacing = MinBoundarySpacing;
			allpoints[counter].bdist.x = MinBoundarySpacing;
			allpoints[counter].bdist.y = MinBoundarySpacing;


			for(itLay = mdl.layers.begin(); itLay != mdl.layers.end(); itLay++)
			{
				if((*itLay)->AffectMesh == false)
					continue;

				if((*itLay)->InLayer(allpoints[counter].center) == false)
					continue;	//if it's not within the layer, move onto the next layer

				//if it gets past, then it is in the layer.
				//first determine the spacing resolutions...

				if(MinBoundarySpacing > (*itLay)->spacingboundary)
					MinBoundarySpacing = (*itLay)->spacingboundary;

				if(MinCenterSpacing > (*itLay)->spacingcenter)
					MinCenterSpacing = (*itLay)->spacingcenter;

				if((*itLay)->priority == true)
					allpoints[counter].priority = true;


				//also set the dimensions to compare for finding the center of the smallest layer it is within
				if(allpoints[counter].bdist.x > (*itLay)->shape->max.x - (*itLay)->shape->min.x)
					allpoints[counter].bdist.x = (*itLay)->shape->max.x - (*itLay)->shape->min.x;

				if(allpoints[counter].bdist.y > (*itLay)->shape->max.y - (*itLay)->shape->min.y)
					allpoints[counter].bdist.y = (*itLay)->shape->max.y - (*itLay)->shape->min.y;			
			}
			allpoints[counter].bdist.x = allpoints[counter].bdist.x / 2.0;	//distance to center
			allpoints[counter].bdist.y = allpoints[counter].bdist.y / 2.0;	//used to determine desired resolution

			if(allpoints[counter].priority == false)	//only check the point if its not in a special layer
			{
				double deviation = mdl.resolution.x/1.999;
				if(mdl.resolution.y < mdl.resolution.x && mdl.resolution.y != 0)
					deviation = mdl.resolution.y/1.999;	//deviation for lines and points, shouldn't happen

				std::vector<Shape*>::iterator itLast;	//now set the point to be last
				for(itLast = lastShapes.begin(); itLast != lastShapes.end(); itLast++)
				{
					if((*itLast)->InShape(allpoints[counter].center, deviation))
					{
						allpoints[counter].last = true;	//set last to true if its in the layer and not in a priority layer
					}
				}
			}

			//now determine the closest boundary it goes to
			if(fabs(MinCenterSpacing - MinBoundarySpacing) > 1e-11)	//they weren't meant to be the same number
			{
				std::vector<Line>::iterator itLine;
				Line distline;	//used to help find the distance of the vertex to a boundary
				XSpace mindist;	//used to track the minimum distance
				mindist.x = mdl.Ldimensions.x + mdl.Ldimensions.y;
				mindist.y = mindist.x;

				distline.pt1 = allpoints[counter].center;
				for(itLine = bounds.lines.begin(); itLine != bounds.lines.end(); itLine++)
				{
					if((*itLine).vertical == true)
					{
						double fuzzy = mdl.resolution.y / 2.0;
						if((distline.pt1.y >= (*itLine).pt1.y-fuzzy && distline.pt1.y <= (*itLine).pt2.y+fuzzy)
							|| (distline.pt1.y <= (*itLine).pt1.y+fuzzy && distline.pt1.y >= (*itLine).pt2.y-fuzzy))
						{
							distline.pt2 = (*itLine).pt1;	//only care about x value, so which point doesn't matter
							double dist = fabs(distline.pt1.x - distline.pt2.x);	//length of line
							if(mindist.x > dist)
								mindist.x = dist;
						}
						//else	//check each point on the line, and if the weighted distance is smaller
						//{		//than present values, put it in mindist.
						//	mindist = MinFactoredDistancetoLine(distline.pt1, mindist, *itLine);
						//}	//end if point is not perpendicular to line - note, above code is generic
					}	//end if line is vertical
					else if((*itLine).m == 0.0)	//this slope is zero, so the accompanying point is vertical.
					{
						double fuzzy = mdl.resolution.x / 2.0;
						if((distline.pt1.x >= (*itLine).pt1.x-fuzzy && distline.pt1.x <= (*itLine).pt2.x+fuzzy)
							|| (distline.pt1.x <= (*itLine).pt1.x+fuzzy && distline.pt1.x >= (*itLine).pt2.x-fuzzy))
						{	//is the point within the bounds of this new line?
							distline.pt2 = (*itLine).pt1;	//now only care about y, so doesn't matter which point
							double dist = fabs(distline.pt1.y - distline.pt2.y);	//length of line
							if(mindist.y > dist)
								mindist.y = dist;	//note, if m=0, dy =0
						}
						//else
						//{	//returns the min
						//	mindist = MinFactoredDistancetoLine(distline.pt1, mindist, *itLine);
						//}
					}
					else	//should not have any issues with horizontal or vertical lines
					{
						distline.m = -1.0/(*itLine).m;	//the perpendicular slope of the line of interest
						distline.b = distline.pt1.y - distline.m * distline.pt1.x;	//y = mx + b. find b.
						distline.pt2.x = (distline.b - (*itLine).b) / ((*itLine).m - distline.m);	//if y's =, what x is
						distline.pt2.y = distline.m * distline.pt2.x + distline.b;	//now that have x, calculate y
						
						
						if((*itLine).PointIn(distline.pt2))	//this point is present on the line segment
						{	//update the minimum with this point
								double dist;
								double trig;
								dist = AbsDistance(distline.pt1, distline.pt2);	//hypotenuse
	//if its close Xwise, but far Ywise, don't want to adjust it as much
	//create a factor cos(theta) of the line for the x, which is delX/Hypotenuse
								if(dist > 0.0)
								{
									double dx = fabs(distline.pt1.x - distline.pt2.x);
									double dy = fabs(distline.pt1.y - distline.pt2.y);
									double factor;
									trig = fabs(sin((*itLine).phi));
									if(trig != 0)
									{
										factor = dx/trig;// + dy*(dy/dx);
										if(factor < mindist.x)
											mindist.x = factor;
									}
									trig = fabs(cos((*itLine).phi));
									if(trig != 0)
									{
										factor = dy/trig;// + dx*(dx/dy);	//it may have an effect if near the boundary
										if(factor < mindist.y)
											mindist.y = factor;

									}
								}
								else	//the point lies ON a boundary, so it better reflect this minimum distance
								{
									mindist.x = 0;
									mindist.y = 0;
								}
						}	//it's outside the bounds of the line segment
						//else
						//{
						//	mindist = MinFactoredDistancetoLine(distline.pt1, mindist, *itLine);
						//}

					}
				}	//end for loop going through all the lines

				//now compare to all the shapes on the list - aka circles - not sure how to implement ellipses

				std::vector<Shape*>::iterator itShape;
				for(itShape = bounds.curves.begin(); itShape != bounds.curves.end(); itShape++)
				{
					if((*itShape)->type == CIRCLE)
					{
						double radius = AbsDistance((*itShape)->center, (*itShape)->start->pt);	//any point will do
						double distance = AbsDistance((*itShape)->center, allpoints[counter].center);
						double delta = fabs(radius - distance);	//absolute distance away from boundary
						double costheta = fabs(allpoints[counter].center.x - (*itShape)->center.x) / distance;	//cos(theta) = dx/total
						double sintheta = fabs(allpoints[counter].center.y - (*itShape)->center.y) / distance;	//sin(theta) = dy/total

						//now determine how much of an effect it has

						double dx = delta*costheta;	//total change in x is (distance to boundary)cos(theta)
						double dy = delta*sintheta;
						double factor;

						double theta = atan2(dy, dx);
						theta += PI/2.0;	//rotate it 90 degrees, so it points along the tangent of the circle
						sintheta = fabs(sin(theta));	//now sin(t) and cos(t) are used to determine
						costheta = fabs(cos(theta));	//the weight of the x and y components.

						

						if(sintheta != 0)
						{
							factor = dx/sintheta;// + dy*dy/dx;	//the relevance of the dx based on the x
							if(factor < mindist.x)	//note, don't want 
								mindist.x = factor;
						}
						if(costheta != 0)
						{
							factor = dy/costheta;// + dx*dx/dy;
							if(factor < mindist.y)
								mindist.y = factor;
						}
						if(delta == 0)	//it lies on the boundary, so adjust accordingly
						{
							mindist.x = 0;
							mindist.y = 0;
						}
					}
					if((*itShape)->type == ELLIPSE)
					{	//cry? First find magnitudes of A(major) and B(minor). They should be consecutive points
						double a=0;	//in the shape - 4 points
						double b=0;
						XSpace PtA = (*itShape)->start->pt;
						XSpace PtB = (*itShape)->start->next->pt;

						a = AbsDistance((*itShape)->center, PtA);
						b = AbsDistance((*itShape)->center, PtB);

						if(a < b)	//make a major axis, swap points and magnitudes
						{
							double temp = a;
							a = b;
							b = temp;	//swap them
							
							XSpace PtTemp = PtA;
							PtA = PtB;
							PtB = PtTemp;
						}
						double theta = atan2(PtA.y - (*itShape)->center.y, PtA.x - (*itShape)->center.x);	//degree change to main axis
						//if both negative, returns the opposing point of interest, which is still along major axis
						double t = atan2(allpoints[counter].center.y - (*itShape)->center.y,
							allpoints[counter].center.x - (*itShape)->center.x);
						t -= theta;	//t is now the relative angle off theta

						XSpace intersect;
						double cost = cos(t);	//just speed things up - calculate once.
						double sinth = sin(theta);
						double sint = sin(t);
						intersect.x = (*itShape)->center.x + a*cost*cos(theta) - b*sint*sinth;
						intersect.y = (*itShape)->center.y + a*cost*sinth + b*sint*sinth;
						//X = X_center + a*cos(t)cos(theta) - b*sin(t)sin(theta)
						//Y = Y_center + a*cos(t)sin(theta) + b*sin(t)cos(theta)
						//theta is the angle to the major axis
						//t is the angle of a point off the major axis. Thank you Wikipeida, Ellipses.

						//now that the intersection point is known... can calculate everything else

						double dx = fabs(allpoints[counter].center.x - intersect.x);
						double dy = fabs(allpoints[counter].center.y - intersect.y);
						double delta = AbsDistance(allpoints[counter].center, intersect);
						sint = fabs(sin(t+theta+PI/2.0));	//start at angle of direction, then rotate 90
						double factor;				//to make it perpendicular to this direction
						if(sint != 0)
						{
							factor = dx/sint;// + dy*dy/dx;	//the relevance of the dx based on the x
							if(factor < mindist.x)	//note, don't want 
								mindist.x = factor;
						}
						cost = fabs(cos(t+theta+PI/2.0));
						if(cost != 0)
						{
							factor = dy/cost;// + dx*dx/dy;	//if change in y is distant compared to x, it shouldn't affect it
							if(factor < mindist.y)
								mindist.y = factor;
						}
						if(delta == 0)	//it lies on the boundary, so adjust accordingly
						{
							mindist.x = 0;
							mindist.y = 0;
						}	//end else saying delta does not lie on the boundary
					}	//end if the shape is an ellipse
				}	//end for loop going through all the shapes

				//OK. Now mindist holds the distance to the nearest boundary line for every applicable layer
				//BDist holds the smallest X,Y components from the layer edge to the center of the layer this
				//point is in so simply take a ratio...
				if(mindist.x >= allpoints[counter].bdist.x)	//moved beyond center...?
					allpoints[counter].TargetRes.x = MinCenterSpacing;
				else if(allpoints[counter].bdist.x == 0)	//avoid divide by 0
					allpoints[counter].TargetRes.x = MinBoundarySpacing;
				else
					allpoints[counter].TargetRes.x = MinBoundarySpacing +
					(MinCenterSpacing - MinBoundarySpacing)*mindist.x/allpoints[counter].bdist.x;
				//start value + difference to end value * percentage of way to end value

				if(mindist.y >= allpoints[counter].bdist.y)
					allpoints[counter].TargetRes.y = MinCenterSpacing;
				else if(allpoints[counter].bdist.y == 0)	//avoid divide by 0
					allpoints[counter].TargetRes.y = MinBoundarySpacing;
				else
					allpoints[counter].TargetRes.y = MinBoundarySpacing +
					(MinCenterSpacing - MinBoundarySpacing)*mindist.y/allpoints[counter].bdist.y;

				//keep within the limits of the model
				if(allpoints[counter].TargetRes.x < mdl.resolution.x)
					allpoints[counter].TargetRes.x = mdl.resolution.x;

				if(allpoints[counter].TargetRes.y < mdl.resolution.y)
					allpoints[counter].TargetRes.y = mdl.resolution.y;

			}	//end if the spacing is NOT consistent through the layer
			else
			{	//they're basically the same number, so just set the target resolution throughout as MinBoundarySpacing
				allpoints[counter].TargetRes.x = MinBoundarySpacing;
				allpoints[counter].TargetRes.y = MinBoundarySpacing;
			}
			

			counter++;	//keep track of the current point
		}	//end for loop going through all x points
	}	//end for loop going through all y points

	std::vector<Shape*>::iterator itShape;	//clear out the last shapes
	for(itShape = lastShapes.begin(); itShape != lastShapes.end(); itShape++)
	{
		delete (*itShape);
		*itShape = NULL;
	}
	if(!lastShapes.empty())
		lastShapes.clear();

	DisplayProgress(20, 0);


	//at this point, every point has been defined with a preferred resolution.

	//
	int state = PRIORITYX;	//need it to start in the loop
	bool changed = true;
	bool last = false;
	long int newnumpoints = numpoints;	//size of new array after mesh is done
	long int start = 0;
	short dir=1;
	while(state != DONEREDUCE)	//keep going until the model finds nothing it can do...
	{
		changed = false;
		XSpace delta;
		double temp;
		long int adj;
		DisplayProgress(21, newnumpoints);
		if(last == true)
		{
			start = numpoints - 1;
			dir = -1;
		}

		for(long int i=start; i<numpoints && i>=0; i += dir)	//allow the loop to go forwards and backwards
		{
//			if(i == 0 || i == 100 || i == 2550 || i == 5575)
//			{
//				delta.x = 0;
//			}
			if(allpoints[i].active == false)	//move on to next point if this point has been eliminated
				continue;

			if(allpoints[i].last == true && last == false)	//don't evaluate this point if it should go last!
				continue;

			if(state > PRIORITY)	//any of the priority run throughs
			{
				if(allpoints[i].priority == false)	//only run through priority points.
					continue;
			}

			delta.x = allpoints[i].pxpy.x - allpoints[i].mxmy.x;	//used determine if it wants to grow!
			delta.y = allpoints[i].pxpy.y - allpoints[i].mxmy.y;

			if(delta.x < allpoints[i].TargetRes.x && (state == CHANGEX || state == YCLEAR || state == PRIORITYX || state == PRIORITYCLR))	//it wants to grow in the X direction
			{
				adj = allpoints[i].adj.coordPx;	//it will only be able to merge if exclusive, so don't check Px2
				//first try positive X direction
				if(adj >= 0 && adj < numpoints)	//adj is in bounds of the array
				{	//the adjacent point can't be in the last section unless last is being allowed.
					if((allpoints[adj].last == false || last == true) && ExclusiveAdjacent(allpoints[i], i, allpoints[adj], adj, RIGHT))
					{
						temp = fabs(allpoints[adj].pxpy.x - allpoints[i].mxmy.x);
						//fabs prevents merging over periodic device

						if(temp < allpoints[i].TargetRes.x && temp < allpoints[adj].TargetRes.x)
						{	//with the additional size, it is still within the resolution
							if(MaxTwoPerp(allpoints[i], allpoints[adj], FORWARD))
							{	//there are at most two points forward or behind the two points
								//combine them into point i. i takes on adj properties
							
							//update back right corner
								allpoints[i].pxpy = allpoints[adj].pxpy;
							//back left corner unchanged

							//update center coord
								allpoints[i].center.x = (allpoints[i].mxmy.x + allpoints[i].pxpy.x)/2.0;
							//center y unchanged

							//take on minimum target resolution
								if(allpoints[adj].TargetRes.x < allpoints[i].TargetRes.x)
									allpoints[i].TargetRes.x = allpoints[adj].TargetRes.x;
								if(allpoints[adj].TargetRes.y < allpoints[i].TargetRes.y)
									allpoints[i].TargetRes.y = allpoints[adj].TargetRes.y;

								//update adjacent points
								long int adj2;	//used to point to all the points adjacent to what was replaced except what its being merged with

								adj2 = allpoints[adj].adj.coordPy;	//what were the adjacent point(s)
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;	//make it point at the present point merged into
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}

								adj2 = allpoints[adj].adj.coordPy2;	//repeat for the Px2 point.
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}
								
								adj2 = allpoints[adj].adj.coordMy;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMy2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								allpoints[i].adj.coordPx = allpoints[adj].adj.coordPx;
								allpoints[i].adj.coordPx2 = allpoints[adj].adj.coordPx2;


								if(allpoints[i].adj.coordMx == adj)	//there are only two points wrapping around
								{	//adj.coordPx == adj and adj.coordMx == adj. Just merged with adj, all i
									allpoints[i].adj.coordMx = i;
									allpoints[i].adj.coordMx2 = i;
								}

							//there's a max of two - take the two extreme points
								allpoints[i].adj.coordPy2 = allpoints[adj].adj.coordPy2;
								allpoints[i].adj.coordMy2 = allpoints[adj].adj.coordMy2;
								//note, the (P/M)(x/y)2 will always point to the more positive point in coordinates.
								//therefore, [i].adj.coordPy will stay equal to itself because adj is more positive
								
							
							//bdist is now irrelevant - was used to get target res

							//disable adj point
								allpoints[adj].active = false;
								changed = true;	//a point was modified. Signify to keep looping
								newnumpoints--;	//the new array will have one less point...
								
							}	//end test to make sure only two perpendicular neighbors in each direction
						}	//end test to make sure new width doesn't go beyond either of the target dimensions
					}	//end test to make sure that the two points of interest are only adjacent to each other
						//on that particular side
				}	//end test for adj to be in bounds for a grid point

////////////////////////////////Try the (-x) to grow//////////////////////////////////////////////////////////////

				adj = allpoints[i].adj.coordMx;	//it will only be able to merge if exclusive, so don't check Px2
				//next try negative X direction
				if(adj >= 0 && adj < numpoints)	//adj is in bounds of the specific point
				{
					if((allpoints[adj].last == false || last == true) && ExclusiveAdjacent(allpoints[i], i, allpoints[adj], adj, LEFT))
					{
						temp = fabs(allpoints[i].pxpy.x - allpoints[adj].mxmy.x);
						//fabs prevents merging over periodic device

						if(temp < allpoints[i].TargetRes.x && temp < allpoints[adj].TargetRes.x)
						{	//with the additional size, it is still within the resolution
							if(MaxTwoPerp(allpoints[i], allpoints[adj], FORWARD))
							{	//there are at most two points forward or behind the two points
								//combine them into point i. i takes on adj properties
							
							//update back left corner
								allpoints[i].mxmy = allpoints[adj].mxmy;
							//front left corner unchanged

							//update center coord
								allpoints[i].center.x = (allpoints[i].mxmy.x + allpoints[i].pxpy.x)/2.0;
							//center y unchanged

							//take on minimum target resolution
								if(allpoints[adj].TargetRes.x < allpoints[i].TargetRes.x)
									allpoints[i].TargetRes.x = allpoints[adj].TargetRes.x;
								if(allpoints[adj].TargetRes.y < allpoints[i].TargetRes.y)
									allpoints[i].TargetRes.y = allpoints[adj].TargetRes.y;
	
							//update adjacent points
								long int adj2;	//used to point to all the points adjacent to what was replaced except what its being merged with

								adj2 = allpoints[adj].adj.coordPy;	//what were the adjacent point(s)
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;	//make it point at the present point merged into
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}
								
								adj2 = allpoints[adj].adj.coordPy2;	//repeat for the Px2 point.
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMy;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}
								
								adj2 = allpoints[adj].adj.coordMy2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								allpoints[i].adj.coordMx = allpoints[adj].adj.coordMx;
								allpoints[i].adj.coordMx2 = allpoints[adj].adj.coordMx2;

								if(allpoints[i].adj.coordPx == adj)	//there are only two points wrapping around
								{	//adj.coordPx == adj and adj.coordMx == adj. Just merged with adj, all i
									allpoints[i].adj.coordPx = i;
									allpoints[i].adj.coordPx2 = i;
								}



							//there's a max of two - take the two extreme points, the other two are already set
								allpoints[i].adj.coordMy = allpoints[adj].adj.coordMy;
								allpoints[i].adj.coordPy = allpoints[adj].adj.coordPy;
								//note, the (P/M)(x/y)2 will always point to the more positive point in coordinates.
								//therefore, [i].adj.coordPy will stay equal to itself because adj is more positive
								
							
							//bdist is now irrelevant - was used to get target res

							//disable adj point
								allpoints[adj].active = false;
								changed = true;	//a point was modified. Signify to keep looping
								newnumpoints--;	//the new array will have one less point...
								
							}	//end test to make sure only two perpendicular neighbors in each direction
						}	//end test to make sure new width doesn't go beyond either of the target dimensions
					}	//end test to make sure that the two points of interest are only adjacent to each other
						//on that particular side
				}	//end test for adj to be in bounds for a grid point
			}	//end test to see if the point wants to grow in the x dimension
				


////////////////////////Now test if it wants to grow in the y dimension!/////////////////////



			if(delta.y < allpoints[i].TargetRes.y && (state == CHANGEY || state == XCLEAR || state == PRIORITYY || state == PRIORITYCLR))	//it wants to grow in the Y direction
			{
				adj = allpoints[i].adj.coordPy;	//it will only be able to merge if exclusive, so don't check Py2
				//first try positive Y direction
				if(adj >= 0 && adj < numpoints)	//adj is in bounds of the specific point
				{
					if((allpoints[adj].last == false || last == true) && ExclusiveAdjacent(allpoints[i], i, allpoints[adj], adj, BACKWARD))
					{
						temp = fabs(allpoints[adj].pxpy.y - allpoints[i].mxmy.y);
						//fabs prevents merging over periodic device
						if(temp < allpoints[i].TargetRes.y && temp < allpoints[adj].TargetRes.y)
						{	//with the additional size, it is still within the resolution
							if(MaxTwoPerp(allpoints[i], allpoints[adj], LEFT))
							{	//there are at most two points to the left or right of the two points
								//combine i and adj into point i. i takes on adj properties
							
							//update back right corner
								allpoints[i].pxpy = allpoints[adj].pxpy;
							//back left corner unchanged

							//update center coord
								allpoints[i].center.y = (allpoints[i].mxmy.y + allpoints[i].pxpy.y)/2.0;
							//center x unchanged

							//take on minimum target resolution
								if(allpoints[adj].TargetRes.x < allpoints[i].TargetRes.x)
									allpoints[i].TargetRes.x = allpoints[adj].TargetRes.x;
								if(allpoints[adj].TargetRes.y < allpoints[i].TargetRes.y)
									allpoints[i].TargetRes.y = allpoints[adj].TargetRes.y;
	
							//update adjacent points
								long int adj2;	//used to point to all the points adjacent to what was replaced except what its being merged with

								adj2 = allpoints[adj].adj.coordPy;	//what were the adjacent point(s)
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;	//make it point at the present point merged into
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}

								adj2 = allpoints[adj].adj.coordPy2;	//repeat for the Px2 point.
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMy = i;
									if(allpoints[adj2].adj.coordMy2 == adj)
										allpoints[adj2].adj.coordMy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								allpoints[i].adj.coordPy = allpoints[adj].adj.coordPy;
								allpoints[i].adj.coordPy2 = allpoints[adj].adj.coordPy2;

								if(allpoints[i].adj.coordMy == adj)	//there are only two points wrapping around
								{	//adj.coordPy == adj and adj.coordMy == adj. Just merged with adj, all i
									allpoints[i].adj.coordMy = i;	//note, if edge of device, adj = -1 and it
									allpoints[i].adj.coordMy2 = i;	//never reaches here.
								}

							//there's a max of two - take the two extreme points
								allpoints[i].adj.coordPx2 = allpoints[adj].adj.coordPx2;
								allpoints[i].adj.coordMx2 = allpoints[adj].adj.coordMx2;
								//note, the (P/M)(x/y)2 will always point to the more positive point in coordinates.
								//therefore, [i].adj.coordPy will stay equal to itself because adj is more positive
								
							
							//bdist is now irrelevant - was used to get target res

							//disable adj point
								allpoints[adj].active = false;
								changed = true;	//a point was modified. Signify to keep looping
								newnumpoints--;	//the new array will have one less point...
								
							}	//end test to make sure only two perpendicular neighbors in each direction
						}	//end test to make sure new width doesn't go beyond either of the target dimensions
					}	//end test to make sure that the two points of interest are only adjacent to each other
						//on that particular side
				}	//end test for adj to be in bounds for a grid point

////////////////////////////////Try the (-y) to grow//////////////////////////////////////////////////////////////

				adj = allpoints[i].adj.coordMy;	//it will only be able to merge if exclusive, so don't check Px2
				//next try negative Y direction
				if(adj >= 0 && adj < numpoints)	//adj is in bounds of the specific point
				{
					if((allpoints[adj].last == false || last == true) && ExclusiveAdjacent(allpoints[i], i, allpoints[adj], adj, FORWARD))
					{
						temp = fabs(allpoints[i].pxpy.y - allpoints[adj].mxmy.y);	//growth is screwy on overlap...
						//prevent it from happening by taking fabs, which will ensure it's greater than the target res.


						if(temp < allpoints[i].TargetRes.y && temp < allpoints[adj].TargetRes.y)
						{	//with the additional size, it is still within the resolution
							if(MaxTwoPerp(allpoints[i], allpoints[adj], LEFT))
							{	//there are at most two points forward or behind the two points
								//combine them into point i. i takes on adj properties
							
							//update front left corner
								allpoints[i].mxmy = allpoints[adj].mxmy;
							//back left corner unchanged

							//update center coord
								allpoints[i].center.y = (allpoints[i].mxmy.y + allpoints[i].pxpy.y)/2.0;
							//center x unchanged

							//take on minimum target resolution
								if(allpoints[adj].TargetRes.x < allpoints[i].TargetRes.x)
									allpoints[i].TargetRes.x = allpoints[adj].TargetRes.x;
								if(allpoints[adj].TargetRes.y < allpoints[i].TargetRes.y)
									allpoints[i].TargetRes.y = allpoints[adj].TargetRes.y;
	
							//update adjacent points
								long int adj2;	//used to point to all the points adjacent to what was replaced except what its being merged with

								adj2 = allpoints[adj].adj.coordMy;	//what were the adjacent point(s)
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;	//make it point at the present point merged into
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMy2;	//repeat for the Px2 point.
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPy == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPy = i;
									if(allpoints[adj2].adj.coordPy2 == adj)
										allpoints[adj2].adj.coordPy2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								adj2 = allpoints[adj].adj.coordMx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordPx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordPx = i;
									if(allpoints[adj2].adj.coordPx2 == adj)
										allpoints[adj2].adj.coordPx2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								adj2 = allpoints[adj].adj.coordPx2;
								if(adj2 != EXT)
								{
									if(allpoints[adj2].adj.coordMx == adj)	//update the next point if neccessary
										allpoints[adj2].adj.coordMx = i;
									if(allpoints[adj2].adj.coordMx2 == adj)
										allpoints[adj2].adj.coordMx2 = i;
								}

								
								allpoints[i].adj.coordMy = allpoints[adj].adj.coordMy;
								allpoints[i].adj.coordMy2 = allpoints[adj].adj.coordMy2;

								if(allpoints[i].adj.coordPy == adj)	//there are only two points wrapping around
								{	//adj.coordPx == adj and adj.coordMx == adj. Just merged with adj, all i
									allpoints[i].adj.coordPy = i;
									allpoints[i].adj.coordPy2 = i;
								}

								

							//there's a max of two - take the two extreme points, the other two are already set
								allpoints[i].adj.coordMx = allpoints[adj].adj.coordMx;
								allpoints[i].adj.coordPx = allpoints[adj].adj.coordPx;
								//note, the (P/M)(x/y)2 will always point to the more positive point in coordinates.
								//therefore, [i].adj.coordPy will stay equal to itself because adj is more positive
								
							
							//bdist is now irrelevant - was used to get target res

							//disable adj point
								allpoints[adj].active = false;
								changed = true;	//a point was modified. Signify to keep looping
								newnumpoints--;	//the new array will have one less point...
								
							}	//end test to make sure only two perpendicular neighbors in each direction
						}	//end test to make sure new width doesn't go beyond either of the target dimensions
					}	//end test to make sure that the two points of interest are only adjacent to each other
						//on that particular side
				}	//end test for adj to be in bounds for a grid point
			}	//end test to see if the point wants to grow in the y dimension

		}	//end for loop which iterates through all the ORIGINAL points

		if(changed == false)	//time to update the state
		{
			switch(state)
			{
				case DONEREDUCE:	//how is it still here...?
					state = DONEREDUCE;
					break;
				case CHANGEX:
					state = XCLEAR;
					break;
				case CHANGEY:
					state = YCLEAR;
					break;
				case XCLEAR:
				case YCLEAR:
					if(last == false)
					{
						state = CHANGEX;
						last = true;
						DisplayProgress(23,0);
					}
					else	//last was true, and it cleared out.
					{
						state = DONEREDUCE;
					}
					break;
				case PRIORITYX:
					state = PRIORITYY;
					break;
				case PRIORITYY:
					state = PRIORITYCLR;
					break;
				case PRIORITYCLR:
					state = CHANGEX;
					DisplayProgress(22, 0);
					break;
				default:
					state = CHANGEX;
					break;
			}
		}
		else
		{
			if((float)newnumpoints/(float)numpoints < 0.8)	//only take the time to iterate through everything if there will be
			{				//a fairly significant reduction in the number of points.
				allpoints = EliminateUnused(numpoints, newnumpoints, allpoints);	//numpoints and allpoints change
			}
			switch(state)
			{
				case DONEREDUCE:	//should never happen! but something was somehow changed, so keep going.
					state = CHANGEX;
					break;
				case XCLEAR:	//it was working on y, keep doing so
					state = CHANGEY;	//note, x is not neccessarily clear anymore of possibilities
					break;
				case YCLEAR:	//was working on x, keep doing so.
					state = CHANGEX;	//see above note
					break;
				case CHANGEX:
					state = CHANGEX;
					break;
				case CHANGEY:
					state = CHANGEY;
					break;
				case PRIORITYX:
					state = PRIORITYX;
					break;
				case PRIORITYY:
					state = PRIORITYY;
					break;
				case PRIORITYCLR:
					state = PRIORITYX;
					break;
				default:	//it if changed an x or a y, don't mess with it...
					state = CHANGEX;
					break;
			}
		}	//end bool changed==true


	}	//end while loop which will cause the for loop to continue until no changes are made

/ *
	mesh is now reduced to the optimal size defined by the layers
	need to restructure the points
	pass 1:
		if good point: copy it into next position in array.
						have array of long ints[numpoints], and store new array position in there
	pass 2:
		adjust all adjacent's by looking up the new index from the above mentioned array
* /
	long int* catalog = new long int[numpoints];
	model.gridp = new Point[newnumpoints];
	model.numpoints = newnumpoints;
	model.poissonRho = new double[newnumpoints];	//create an array for all the poisson solution data that may be clumped. +1 is for the constants on  RHS
	counter = 0;	//reset counter					//together as a pack

	for(long int i = 0; i<numpoints; i++)
	{
		if(allpoints[i].active == false)
		{
			catalog[i] = EXT;	//flag to say out of bounds now
			continue;
		}

		if(counter == newnumpoints)	//display warning of failure and prevent crash.
		{
			ProgressInt(16, newnumpoints);
			break;
		}

		model.gridp[counter].position = allpoints[i].center;	//used in actual calculations
		model.gridp[counter].mxmy = allpoints[i].mxmy;	//maintain the size of the box
		model.gridp[counter].pxpy = allpoints[i].pxpy;	//namely used in calculating areas/volumes
		model.gridp[counter].widthi = model.gridp[counter].pxpy - model.gridp[counter].mxmy;
		model.gridp[counter].widthi.x = 1.0 / model.gridp[counter].widthi.x;
		model.gridp[counter].widthi.y = 1.0 / model.gridp[counter].widthi.y;
		model.gridp[counter].widthi.z = 1.0 / model.gridp[counter].widthi.z;
		model.gridp[counter].adj = allpoints[i].adj;	//these are the old adjacent points
		catalog[i] = counter;		//lookup table for fixing all the adjacents later - what the number should
			//be replaced with...
		
		counter++;	//only increment the counter when a new point is added
	}

	for(int j=0; j<newnumpoints; j++)
	{
		model.gridp[j].adj.self = j;
		long int num = model.gridp[j].adj.coordMx;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordMx = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordMx2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordMx2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordPx;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordPx = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)
		
		num = model.gridp[j].adj.coordPx2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordPx2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordMy;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordMy = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordMy2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordMy2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordPy;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordPy = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = model.gridp[j].adj.coordPy2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			model.gridp[j].adj.coordPy2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)
	}

	long int id;
	for(long int i=0; i<newnumpoints; i++)
	{
		id = model.gridp[i].adj.coordMx;
		if(id != EXT)
		{
			model.gridp[i].adj.MxDistI = model.gridp[i].position.x - model.gridp[id].position.x;
			model.gridp[i].adj.MxDistI = 1.0 / model.gridp[i].adj.MxDistI;
		}
		else
			model.gridp[i].adj.MxDistI = 0.0;

		id = model.gridp[i].adj.coordMx2;
		if(id != EXT)
		{
			model.gridp[i].adj.Mx2DistI = model.gridp[i].position.x - model.gridp[id].position.x;
			model.gridp[i].adj.Mx2DistI = 1.0 / model.gridp[i].adj.Mx2DistI;
		}
		else
			model.gridp[i].adj.Mx2DistI = 0.0;

		id = model.gridp[i].adj.coordPx;
		if(id != EXT)
		{
			model.gridp[i].adj.PxDistI = model.gridp[id].position.x - model.gridp[i].position.x;
			model.gridp[i].adj.PxDistI = 1.0 / model.gridp[i].adj.PxDistI;
		}
		else
			model.gridp[i].adj.PxDistI = 0.0;

		id = model.gridp[i].adj.coordPx2;
		if(id != EXT)
		{
			model.gridp[i].adj.Px2DistI = model.gridp[id].position.x - model.gridp[i].position.x;
			model.gridp[i].adj.Px2DistI = 1.0 / model.gridp[i].adj.Px2DistI;
		}
		else
			model.gridp[i].adj.Px2DistI = 0.0;

		id = model.gridp[i].adj.coordMy;
		if(id != EXT)
		{
			model.gridp[i].adj.MyDistI = model.gridp[i].position.y - model.gridp[id].position.y;
			model.gridp[i].adj.MyDistI = 1.0 / model.gridp[i].adj.MyDistI;
		}
		else
			model.gridp[i].adj.MyDistI = 0.0;

		id = model.gridp[i].adj.coordMy2;
		if(id != EXT)
		{
			model.gridp[i].adj.My2DistI = model.gridp[i].position.y - model.gridp[id].position.y;
			model.gridp[i].adj.My2DistI = 1.0 / model.gridp[i].adj.My2DistI;
		}
		else
			model.gridp[i].adj.My2DistI = 0.0;

		id = model.gridp[i].adj.coordPy;
		if(id != EXT)
		{
			model.gridp[i].adj.PyDistI = model.gridp[id].position.y - model.gridp[i].position.y;
			model.gridp[i].adj.PyDistI = 1.0 / model.gridp[i].adj.PyDistI;
		}
		else
			model.gridp[i].adj.PyDistI = 0.0;

		id = model.gridp[i].adj.coordPy2;
		if(id != EXT)
		{
			model.gridp[i].adj.Py2DistI = model.gridp[id].position.y - model.gridp[i].position.y;
			model.gridp[i].adj.Py2DistI = 1.0 / model.gridp[i].adj.Py2DistI;
		}
		else
			model.gridp[i].adj.Py2DistI = 0.0;

	}

	if(mdl.NDim == 2)
	{
		for(unsigned long int i = 0; i < model.numpoints; i++)
		{
			model.gridp[i].PopulateAngleData2D(model);
		}
	}
		

	delete [] catalog;
	delete [] allpoints;
	return(0);
}
*/

MiniPoint* EliminateUnused(long int& numpoints, long int newnumpoints, MiniPoint* allpoints)
{
	long int* catalog = new long int[numpoints];
	MiniPoint* newpoints = new MiniPoint[newnumpoints];
	
	long int counter = 0;	//reset counter

	for(long int i = 0; i<numpoints; i++)
	{
		if(allpoints[i].active == false)
		{
			catalog[i] = EXT;	//flag to say out of bounds now
			continue;
		}

		if(counter == newnumpoints)	//display warning of failure and prevent crash.
		{
			ProgressInt(16, newnumpoints);
			break;
		}

		newpoints[counter] = allpoints[i];

		catalog[i] = counter;		//lookup table for fixing all the adjacents later - what the number should
			//be replaced with...
		
		counter++;	//only increment the counter when a new point is added
	}

	for(int j=0; j<newnumpoints; j++)
	{
		newpoints[j].adj.self = j;
		long int num = newpoints[j].adj.coordMx;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordMx = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordMx2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordMx2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordPx;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordPx = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)
		
		num = newpoints[j].adj.coordPx2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordPx2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordMy;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordMy = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordMy2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordMy2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordPy;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordPy = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)

		num = newpoints[j].adj.coordPy2;
		if(num >= 0 && num < numpoints)	//prevent bad accesses for out of bounds
			newpoints[j].adj.coordPy2 = catalog[num];	//otherwise don't change it. It refers to EXT, most likely, or some other future code (negative)
	}

	numpoints = newnumpoints;	//numpoints is by reference.  Make sure to only loop through the right number of points
	delete [] catalog;
	delete [] allpoints;	//clear up old set of points me mory
	return(newpoints);		//pass thew new set of memory
}

bool MaxTwoPerp(MiniPoint pt1, MiniPoint pt2, int dir)
{
	std::list<long int> adj;

	switch(dir)
	{
		case LEFT:		//(-x)
		case RIGHT:		//(+x)
			if(!adj.empty())
				adj.clear();
			adj.push_back(pt1.adj.coordMx);
			adj.push_back(pt1.adj.coordMx2);
			adj.push_back(pt2.adj.coordMx);
			adj.push_back(pt2.adj.coordMx2);
			adj.sort();	//sort the list
			adj.unique();	//remove consecutive entries in list so only one remains
			if(adj.size() > 2)	//failed going to the right
				return(false);
			if(!adj.empty())
				adj.clear();
			adj.push_back(pt1.adj.coordPx);
			adj.push_back(pt1.adj.coordPx2);
			adj.push_back(pt2.adj.coordPx);
			adj.push_back(pt2.adj.coordPx2);
			adj.sort();	//sort the list
			adj.unique();	//remove consecutive entries in list so only one remains
			if(adj.size() > 2)	//failed going to the left
				return(false);

			return(true);
			break;
		case FORWARD:	//(-y)
		case BACKWARD:	//(+y)
			if(!adj.empty())
				adj.clear();
			adj.push_back(pt1.adj.coordMy);
			adj.push_back(pt1.adj.coordMy2);
			adj.push_back(pt2.adj.coordMy);
			adj.push_back(pt2.adj.coordMy2);
			adj.sort();	//sort the list
			adj.unique();	//remove consecutive entries in list so only one remains
			if(adj.size() > 2)	//failed going to the front
				return(false);

			if(!adj.empty())
				adj.clear();
			adj.push_back(pt1.adj.coordPy);
			adj.push_back(pt1.adj.coordPy2);
			adj.push_back(pt2.adj.coordPy);
			adj.push_back(pt2.adj.coordPy2);
			adj.sort();	//sort the list
			adj.unique();	//remove consecutive entries in list so only one remains
			if(adj.size() > 2)	//failed going to the back
				return(false);

			return(true);
			break;
		default:
			break;
	}
	return(false);
}

bool ExclusiveAdjacent(MiniPoint pt1, int pt1index, MiniPoint pt2, int pt2index, int dir)
{
	if(pt1.active==false || pt2.active == false)
		return(false);	//these points shouldn't be compared to begin with!

	switch(dir)
	{
		case LEFT:		//(-x)
			if(pt1.adj.coordMx == pt2index && pt1.adj.coordMx2 == pt2index)	//both go to pt2
			{
				if(pt2.adj.coordPx == pt1index && pt2.adj.coordPx2 == pt1index)	//both go to pt1
				{
					return(true);
				}
			}
			break;
		case RIGHT:		//(+x)
			if(pt1.adj.coordPx == pt2index && pt1.adj.coordPx2 == pt2index)	//both go to pt2
			{
				if(pt2.adj.coordMx == pt1index && pt2.adj.coordMx2 == pt1index)	//both go to pt1
				{
					return(true);
				}
			}
			break;
		case FORWARD:	//(-y)
			if(pt1.adj.coordMy == pt2index && pt1.adj.coordMy2 == pt2index)	//both go to pt2
			{
				if(pt2.adj.coordPy == pt1index && pt2.adj.coordPy2 == pt1index)	//both go to pt1
				{
					return(true);
				}
			}
			break;
		case BACKWARD:	//(+y)
			if(pt1.adj.coordPy == pt2index && pt1.adj.coordPy2 == pt2index)	//both go to pt2
			{
				if(pt2.adj.coordMy == pt1index && pt2.adj.coordMy2 == pt1index)	//both go to pt1
				{
					return(true);
				}
			}
			break;
		default:
			ProgressInt(15, dir);
			//fail
			break;

	}
	return(false);
}

//returns the minimum weighted distance from a point to a line's vertices, given that they
//are smaller than the present minimum
//if it is proportionally far from the end point, it should have less of an effect, so it needs a larger number
XSpace MinFactoredDistancetoLine(XSpace pt1, XSpace mindist, Line itLine)
{
	double /*dist1, dist2, */factor;
//	dist1 = AbsDistance(pt1, itLine.pt1);   set but never used	//hypotenuse
//	dist2 = AbsDistance(pt1, itLine.pt2);   set but never used	//for both points
	//if its close Xwise, but far Ywise, don't want to adjust it as much
	//create a factor cos(theta) of the line for the x, which is delX/Hypotenuse
	double dx = fabs(pt1.x - itLine.pt1.x);
	double dy = fabs(pt1.y - itLine.pt1.y);
	double sint = fabs(sin(itLine.phi));
	itLine.CalcAngles();	//make sure angles are calculated
	if(sint != 0)
	{
		factor = dx/sint;// + dy*dy/dx;	//note, don't want to say it's a small dx if it's on the opposite end via y
		if(factor < mindist.x)	//but keep it on the scale of ~dy by multiplying by dy
			mindist.x = factor;
	}

	double cost = fabs(cos(itLine.phi));
	
	if(cost != 0)
	{
		factor = dy/cost;// + dx*dx/dy;	//it may have an effect if near the boundary
		if(factor < mindist.y)
			mindist.y = factor;
	}

	//now do it all again for point 2
	dx = fabs(pt1.x - itLine.pt2.x);
	dy = fabs(pt1.x - itLine.pt2.y);

	if(sint != 0)
	{
		factor = dx/sint;// + dy*dy/dx;
		if(factor < mindist.x)
			mindist.x = factor;
	}
	
	if(cost != 0)
	{
		factor = dy/cost;// + dx*dx/dy;
		if(factor < mindist.y)
			mindist.y = factor;
	}
	return(mindist);
}


/*
This is the new mesh generation.

1) Build meshes for each layer localized at the layer
2) Determine device boundaries by the dimensions.
3) STart adding the points from the translated layers
4) Stitch layers together by appropriately lining up points at the boundaries
5) Always choose the layer which has the best resolution
6) Voids: Fill with 1-3 points or merge with two adjacent layers if the void is "small"

Note: The spacing on either side of a boundary MUST be the same.  That was the flaw with the previous mesh generation
*/

int GenerateMesh1D(ModelDescribe& mdesc, Model& mdl)
{
	unsigned int numlayers = mdesc.layers.size();
	LayerMesh* meshes = new LayerMesh[numlayers];

	

	for(unsigned int i=0; i<numlayers; i++)
		BuildLayerMesh(meshes[i], mdesc.layers[i]);
	
	
	//now the meshes need to be stitched together into one mesh
	
	std::map<XSpace, MinPoint> WholeMesh;	//short represents # of layers it is in
	//find any overlapping areas
	//note the layers are in real space, nothing is translated
	CombineLayerMeshes(meshes, numlayers, mdesc, WholeMesh);

	FillBasicMeshInfo(WholeMesh, mdesc, mdl);

	WholeMesh.clear();
	delete [] meshes;	//make sure the old data gets deleted
	meshes = NULL;
	DisplayProgress(44, mdl.numpoints);

	return(0);
}

int FillBasicMeshInfo(std::map<XSpace, MinPoint>& mesh, ModelDescribe& mdesc, Model& mdl)
{
	//first, make sure the model has everything properly cleared out.
	mdl.gridp.clear();
	delete [] mdl.poissonRho;

	//allocate the proper memory
//	mdl.gridp = new Point[mesh.size];
	mdl.poissonRho = new double[mesh.size()];
	mdl.numpoints = mesh.size();
	mdl.NDim = 1;


//	double negyz = 0.0; unused
	double plusy = mdesc.Ldimensions.y;
	double plusz = mdesc.Ldimensions.z;
	double posy = 0.5 * plusy;
	double posz = 0.5 * plusz;
	double widthiy = 1.0 / plusy;
	double widthiz = 1.0 / plusz;

	
	double prevXBoundary = 0.0;
	double nextXBoundary;


	long int sz = (long int)mesh.size();
	Point* prevPoint = NULL;	//a pointer to the previous one for making links
	Point tmpPt(mdl);	//a data holder for inserting into the tree
	std::map<long int, Point>::iterator retPt;

	mdl.boundaries.push_back(0);	//these are the points on the boundaries of the device
	mdl.boundaries.push_back(sz-1);	//and are applicable entry points for light
	long int i=0;
	{
		std::map<XSpace, MinPoint>::iterator itNext = mesh.begin();
		if(itNext != mesh.end())
			itNext++;

		for (std::map<XSpace, MinPoint>::iterator it = mesh.begin(); it != mesh.end(); ++it)
		{
			if(i < sz - 1)	//there's still another point above
				nextXBoundary = 0.5 * (it->first.x + itNext->first.x);
			else
				nextXBoundary = mdesc.Ldimensions.x;

			tmpPt.position.x = it->first.x;
			tmpPt.position.y = posy;
			tmpPt.position.z = posz;
			tmpPt.mxmy.x = prevXBoundary;
			tmpPt.mxmy.y = 0.0;
			tmpPt.mxmy.z = 0.0;
			tmpPt.pxpy.x = nextXBoundary;
			tmpPt.pxpy.y = plusy;
			tmpPt.pxpy.z = plusz;

			//if the shape is a point and it starts at zero, all hell will break lose with divide by 0

			if (tmpPt.position.x == tmpPt.mxmy.x || tmpPt.position.x == tmpPt.pxpy.x)
				tmpPt.position.x = 0.5 * (tmpPt.mxmy.x + tmpPt.pxpy.x);
			

			tmpPt.adj.self = i;

			tmpPt.adj.adjN = prevPoint;
			tmpPt.adj.adjP = NULL;

			tmpPt.adj.adjMX = prevPoint;
			tmpPt.adj.adjMX2 = NULL;
		
			tmpPt.adj.adjPX = NULL;
			tmpPt.adj.adjPX2 = NULL;
		

			tmpPt.adj.adjPY = NULL;
			tmpPt.adj.adjPY2 = NULL;
			tmpPt.adj.adjMY = NULL;
			tmpPt.adj.adjMY2 = NULL;

			tmpPt.adj.adjPZ = NULL;
			tmpPt.adj.adjPZ2 = NULL;
			tmpPt.adj.adjMZ = NULL;
			tmpPt.adj.adjMZ2 = NULL;

			tmpPt.widthi.x = 1.0 / (tmpPt.pxpy.x - tmpPt.mxmy.x);
			tmpPt.widthi.y = widthiy;
			tmpPt.widthi.z = widthiz;

			tmpPt.voli = tmpPt.widthi.x * tmpPt.widthi.y * tmpPt.widthi.z;
			tmpPt.vol = 1.0 / tmpPt.voli;
		
			retPt = mdl.gridp.insert(std::pair<long int, Point>(i, tmpPt)).first;	//put it in the tree and return the proper pointer with it

			if(prevPoint)	//add the previous points pointers in to go to the proper values in the tree
			{
				prevPoint->adj.adjPX = &(retPt->second);
				prevPoint->adj.adjP = prevPoint->adj.adjPX;
			}

			prevPoint = &(retPt->second);	//and update the previous point for the next iteration

			//in this new mesh, the spacing on either side of the boundary is equal... so the distance to the adjacent point
			//is twice the distance from the boundary

			prevXBoundary = nextXBoundary;
			if(itNext != mesh.end())
				++itNext;

			i++;
		}
	}

	return(0);
}

int CombineLayerMeshes(LayerMesh* lyrmeshes, unsigned int numlayers, ModelDescribe& mdesc, std::map<XSpace, MinPoint>& mesh)
{
	for(unsigned int i=0; i<numlayers; i++)
		AddPoints(lyrmeshes[i].Points, mdesc, mesh, numlayers, lyrmeshes[i].layer);
	
	//now set the exterior points
	if(mesh.size() > 0)
	{
		mesh.begin()->second.exterior = true;
		mesh.rbegin()->second.exterior = true;
	}

	//only points from the lowest mesh points were added.
	//now check for voids in the mesh - these would occur as gaps in the layers
	//thse should by default be vacuum material...

	std::vector<VoidSpace> voids;
	FindVoids(mdesc, voids, numlayers);

	//at this point, only the necessary voids are around...
	//now need to handle these voids... which presumably occur at the boundaries of layers.
	VoidMesh(mesh, voids);	//done.

	//now the individual layers need to be stitched together such that the boundaries of the layers do not move.

	StitchMesh(mesh);

	return(0);
}

int StitchMesh(std::map<XSpace, MinPoint>& mesh)
{
	//if the mesh comes across a void, it has already been stitched into the overall mesh.
	//only care about points adjacent to one another of defined layers.
	//if the layers overlap, then the boundary can be ignored as long as it is between the two layers
	//if it is in an exclusive layer, the boundary must be at the edge of the exclusive layer.

	//this will be easiest with a for loop, and then inserting into the tree as needed.

	//Also, remove duplicate points!

	unsigned int size= mesh.size()-1;	//compare pairs of points
	XSpace BoundaryUse;	//the boundary that actually gets set (separate variable made easier in the case exclusive)
	XSpace LessBoundary;	//boundary in negative direction
	XSpace GreatBoundary;	//boundary in positive direction
	double distGreat;
	double distLess;
	XSpace AddPt;
	MinPoint tmpMP;

	{
		std::map<XSpace, MinPoint>::iterator itNext = mesh.begin();
		std::map<XSpace, MinPoint>::iterator itLast = mesh.end();
		if(mesh.size() > 0)
		{
			++itNext;
			--itLast;	//this now points to the last value, which will be the last one itNext will point to
		}

		for (std::map<XSpace, MinPoint>::iterator it = mesh.begin(); itNext != mesh.end();)	//size ignores the possibility of adding a point beyond the dimensions of the device
		{ //the iterator increment is taken care of within loop, as sometimes it gets invalidated
			if(it->second.remove)
			{
				mesh.erase(it);	//get rid of the point
				it = itNext;
				if(itNext != mesh.end())
					++itNext;
				continue;
			}

			if(XSpaceEqual(it->first, itNext->first))
			{
				//these are the same point... not sure how this would happen... but take care of it anyway.
				if(it->second.layer->exclusive && !(itNext->second.layer->exclusive))
					itNext->second.remove = true;	//flag it to be removed on the next iteration
				else if(!(it->second.layer->exclusive) && itNext->second.layer->exclusive)
				{
					mesh.erase(it);
					it = itNext;
					if(itNext != mesh.end())
						++itNext;
					continue;
				}
				else	//they're both exclusive or neither are exclusive
				{
					mesh.erase(it);
					it = itNext;
					if(itNext != mesh.end())
						++itNext;
					continue;
				}
			}
	
			if(it->second.layer != itNext->second.layer)
			{
			
				if(it->second.layer == NULL || itNext->second.layer == NULL)
				{
					if(itNext != mesh.end())
						++itNext;
					continue;	//check the next point if it is a void. In future, voids will have own material...
				}

				if(it->second.layer->exclusive && it->second.layer->shape->type == SHPPOINT)
				{	//make just the one point appear as it should not be multiple points
					//it's a point, the max is it->first
					BoundaryUse = it->second.layer->shape->max;
					XSpace dist = BoundaryUse - it->first;
					XSpace Cutoff = BoundaryUse + dist;
					bool allowRemove = true;
					if(it->second.exterior)	//this point can have it's position shifted
					{
						if(itNext->first < Cutoff)	//the next point is inside the cutoff
						{
							dist = itNext->first - BoundaryUse;	//see how much space there is to the next point
							//it->first = BoundaryUse - dist;	//and position the old mesh point adjacent to this
							Cutoff = itNext->first;	//update the cutoff
							allowRemove = false;	//prevent removing this next point
							AddPt = BoundaryUse - dist;
							tmpMP = it->second;
							mesh.erase(it);
							mesh[AddPt] = tmpMP;	//can't modify the sort value...
							//reset the iterator
							it = itNext;
							--it;
						}
					}
				
					std::map<XSpace, MinPoint>::iterator itup = mesh.upper_bound(Cutoff);	//returns first >, so this one will not be erased[low,up)
					//start with the next one on the list
					mesh.erase(itNext,itup);
					//this invalidated itNext, but it is safe. Fix 'em up
					itNext = it;
					++itNext;
				}
				else if(itNext->second.layer->exclusive && itNext->second.layer->shape->type == SHPPOINT)
				{
					BoundaryUse = itNext->second.layer->shape->min;
					XSpace dist = itNext->first - BoundaryUse;
					XSpace Cutoff = BoundaryUse - dist;
					bool allowRemove = true;
					if(itNext->second.exterior)
					{
						if(it->first>Cutoff)
						{
							dist = BoundaryUse - it->first;
							
							AddPt = BoundaryUse + dist;
							tmpMP = itNext->second;	//can't modify the key...
							mesh.erase(itNext);
							mesh[AddPt] = tmpMP;
							Cutoff = it->first;
							allowRemove = false;
							//restore itNext key
							itNext = it;
							itNext++;
						}
					}
					std::map<XSpace, MinPoint>::iterator lb = mesh.lower_bound(Cutoff);
					mesh.erase(lb,itNext);
					it = itNext;
					--it;	//if the front boundary, this probably does invalidate it!
				}
				else	//this is not a situation with an exclusive point
				{
				

					//otherwise, we have two points that need stitching together.
					LessBoundary = GetClosestBoundary(it->second.layer, it->first);
					GreatBoundary = GetClosestBoundary(itNext->second.layer, itNext->first);

					if(it->second.layer->exclusive && !(itNext->second.layer->exclusive))
						BoundaryUse = LessBoundary;
					else if(!(it->second.layer->exclusive) && itNext->second.layer->exclusive)
						BoundaryUse= GreatBoundary;
					else	//they're both exclusive or neither are exclusive
						BoundaryUse = (LessBoundary + GreatBoundary)*0.5;
					//either the boundaries were the same, they were too far apart and were calculated as a void, or they're really
				//close together.

				//this might not actually result in a boundary that is between the two points... that really screws up the mesh
					if(BoundaryUse < it->first || BoundaryUse > itNext->first)
					{
						if(LessBoundary > it->first && LessBoundary < itNext->first)
							BoundaryUse = LessBoundary;
						else if(GreatBoundary > it->first && GreatBoundary < itNext->first)
							BoundaryUse = GreatBoundary;
						else
							BoundaryUse = (it->first+itNext->first)*0.5;
					}
	
					distGreat = (itNext->first-BoundaryUse).Magnitude();
					distLess = (BoundaryUse - it->first).Magnitude();
	
					if(distLess <= 1.01*distGreat && distLess >= 0.99*distGreat)	//the boundary is close enough that no point is needed to be added
					{
						it = itNext;
						if(itNext != mesh.end())
							++itNext;
						continue;
					}
				//now add a point into the mesh into the larger side

					bool useNormal = true;
					if(it->second.exterior)
					{
						//this is the first point in the mesh
						if(distGreat < BoundaryUse.x)	//and the point can be positioned wherever
						{
							AddPt = it->first;	//change the key...
							AddPt.x = BoundaryUse.x - distGreat;
							tmpMP = it->second;
							mesh.erase(it);
							mesh[AddPt] = tmpMP;
							useNormal = false;	//no need to enter any points
							//fix iterator
							it = itNext;
							--it;
						}
					}
					else if(itNext->second.exterior)
					{
						//this is the last point in the mesh
						if(distLess < 2.0 * distGreat)	//the last point will be equally spaced if it is it's own layer
						{
							AddPt = itNext->first;
							AddPt.x = BoundaryUse.x + distLess;
							tmpMP = itNext->second;
							mesh.erase(itNext);
							mesh[AddPt] = tmpMP;
							useNormal = false;
							//revalidate iterator
							itNext = it;
							++itNext;
						}
					}
				
					if(useNormal)
					{
						if(distLess > distGreat)
						{	//add a point on the i side.
		
							AddPt = BoundaryUse - (itNext->first - BoundaryUse);	//make point same distance on both sides so center doesn't move
							if(AddPt.x < 0.0)
								ProgressCheck(47, false);
							mesh[AddPt] = it->second;	//want the data from the left side of the boundary
						}
						else	//then distgreat must be greater and needs the point added
						{
							AddPt = BoundaryUse + (BoundaryUse - it->first);	//opposite direction...
							if(AddPt.x < 0.0)
								ProgressCheck(47, false);
							mesh[AddPt] = itNext->second;
						}
					}
				}
			}

			it = itNext;
			if(itNext != mesh.end())
				++itNext;
		}	//end for loop
	}	//end containier for also having itNext
	return(0);
}

int VoidMesh(std::map<XSpace, MinPoint>& mesh, std::vector<VoidSpace>& voids)
{
	XSpace center;
	std::map<XSpace, MinPoint>::iterator Greater;
	std::map<XSpace, MinPoint>::iterator Lesser;
	Layer* LyrGreater = NULL;
	Layer* LyrLesser = NULL;
	XSpace BoundaryGreat;
	XSpace BoundaryLess;
	XSpace desirePoint;
	MinPoint tmpMinPt;
	
	for(unsigned int i=0; i < voids.size(); i++)
	{
		tmpMinPt.layer = NULL;
		tmpMinPt.spacing = 1.0;	//HUGE. Basically make it not be forced to shrink, though spacing is kinda useless at this point
		tmpMinPt.remove = false;
		center = (voids[i].max + voids[i].min) * 0.5;
		mesh[center] = tmpMinPt;	//put a vertex at the center

		Greater = mesh.upper_bound(center);	//get the two points on either side of the void. (first greater)
		Lesser = MapFindFirstLess(mesh,center);

		if(Greater != mesh.end())
		{
			LyrGreater = Greater->second.layer;
			BoundaryGreat = GetClosestBoundary(LyrGreater, Greater->first);
			desirePoint = BoundaryGreat - (Greater->first - BoundaryGreat);
			tmpMinPt.layer = NULL;
			tmpMinPt.spacing = LyrGreater->spacingboundary;
			tmpMinPt.remove = false;
			if(desirePoint > center)	//it's put in the void space requiring no changes in the previous mesh
			{
				mesh[desirePoint] = tmpMinPt;
			}
			else if(desirePoint < center)	//then a point is needed to maintain the boundary line
			{	//put a point in the mesh, and it should be able to go the void distance in
				tmpMinPt.layer = LyrGreater;
				tmpMinPt.spacing = LyrGreater->spacingboundary;
				tmpMinPt.remove = false;
				desirePoint = BoundaryGreat + (BoundaryGreat - center);
				mesh[desirePoint] = tmpMinPt;
			}
			//else they are equal, and nothing needs to be done. The center point is the good vertex point
		}	//end adding a vertex on the larger side of the void if needed

		if(Lesser != mesh.end())
		{
			LyrLesser = Lesser->second.layer;
			BoundaryLess = GetClosestBoundary(LyrLesser, Lesser->first);
			desirePoint = BoundaryLess + (BoundaryLess - Lesser->first);
			tmpMinPt.layer = NULL;
			tmpMinPt.spacing = LyrLesser->spacingboundary;
			tmpMinPt.remove = false;
			if(desirePoint < center)
			{	//it is going in the vacuum/void area
				mesh[desirePoint] = tmpMinPt;
			}
			else if(desirePoint > center)	//then a point is needed in the mesh. Should fit fine
			{
				tmpMinPt.layer = LyrLesser;
				tmpMinPt.spacing = LyrLesser->spacingboundary;
				tmpMinPt.remove = false;
				desirePoint = BoundaryLess - (center - BoundaryLess);
				mesh[desirePoint] = tmpMinPt;
			}
		}	//end adding a vertex to the negative void side if needed.
	}
	return(0);
}

XSpace GetClosestBoundary(Layer* lyr, XSpace pos)
{
	if(lyr->shape == NULL)
	{
		XSpace tmp;
		tmp.x = 0.0;
		tmp.y = 0.0;
		tmp.z = 0.0;
		return(tmp);
	}
	XSpace distX = lyr->shape->max - pos;
	XSpace distN = pos - lyr->shape->min;
	double magX = distX.Magnitude();
	double magN = distN.Magnitude();
	if(magX < magN)
		return(lyr->shape->max);
	else
		return(lyr->shape->min);
}

int FindVoids(ModelDescribe& mdesc, std::vector<VoidSpace>& voids, unsigned int numlayers)
{
	VoidSpace temp;
	XSpace zero;
	zero.x = 0.0;
	zero.y = 0.0;
	zero.z = 0.0;
	temp.min = zero;
	temp.max = zero;
	temp.max.x = mdesc.Ldimensions.x;


	voids.reserve(numlayers);	//avoid lots of memory reallocations.
	voids.push_back(temp);	//do not really expect this to be a large vector
	//could not do tree because of for loop and modifications within gets really messy when traversing
	XSpace layerMax;
	XSpace layerMin;
	

	for(unsigned int i=0; i<numlayers; i++)
	{
		if(mdesc.layers[i] == NULL)
			continue;
		if(mdesc.layers[i]->AffectMesh == false)
			continue;
		layerMax = mdesc.layers[i]->shape->max;
		layerMin = mdesc.layers[i]->shape->min;

		unsigned int size = voids.size();
		for(unsigned int j=0; j<size; j++)
		{
			if(layerMin <= voids[j].min && layerMax >= voids[j].max) //it surrounds the void
			{
//				voids[j].min = zero;
//				voids[j].max = zero;	//basically flag for removal
				voids.erase(voids.begin()+j);
				j--;	//this will either put j at a huge number, which will then go back to 0
				size--;	//if size goes to zero, it will default false
			}
			else if(layerMin > voids[j].min && layerMax < voids[j].max)	//entirely contained in the void
			{
				XSpace tmpXS = voids[j].max;
				voids[j].max = layerMin;

				temp.min = layerMax;
				temp.max = tmpXS;
				voids.push_back(temp);
			}
			else if(layerMin <= voids[j].min && layerMax > voids[j].min && layerMax < voids[j].max)
			{	//min is too left, but max is within the void area
				voids[j].min = layerMax;
			}
			else if(layerMin > voids[j].min && layerMin < voids[j].max && layerMax >= voids[j].max)
			{
				voids[j].max = layerMin;
			}
		}	//end go through all voids.
	}	//end go through all layers

	//now get rid of voids that are too small to care about

	unsigned int size= voids.size();
	double minspacing = mdesc.resolution.x * 0.01;
	for(unsigned int i=0; i<size; i++)
	{
		XSpace dist = voids[i].max - voids[i].min;
		if(dist.Magnitude() < minspacing)	//it's a negligible void as far as the model is concerned.
		{
			voids.erase(voids.begin() + i);	//get rid of this useless void
			i--;	//maintain position in for loop, and don't allow going over
			size--;
		}
	}

	return(0);

}


int AddPoints(std::map<XSpace, double>& data, ModelDescribe& mdesc, std::map<XSpace, MinPoint>& mesh, unsigned int numlayers, Layer* srcLayer)
{
	MinPoint tmp;

	for (std::map<XSpace, double>::iterator Node = data.begin(); Node != data.end(); ++Node)
	{
		tmp.layer = srcLayer;
		tmp.spacing = Node->second;
		tmp.remove = false;
		tmp.exterior = false;

		if(srcLayer->exclusive == false)
		{
			for(unsigned int i=0; i<numlayers; i++)	//try to avoid placing the point in
			{
				if(mdesc.layers[i]->AffectMesh == false)	//skip layers that do not affect mesh
					continue;
				if(mdesc.layers[i] == srcLayer)
					continue;	//don't count the source layer. Move on to next search
				
				if(mdesc.layers[i]->InLayer(Node->first))	//is this vertex within the layer?
				{
					if(mdesc.layers[i]->exclusive == true)	//then ignore this point, it doesn't matter. Another layer has priority.
						continue;

				//now check if it is the lowest spacing point...
					double SpacingInOtherLayer = GetDesiredSpacing(XDIR, mdesc.layers[i]->shape->max,
						mdesc.layers[i]->shape->min, Node->first, mdesc.layers[i]->spacingboundary, mdesc.layers[i]->spacingcenter);
					if(SpacingInOtherLayer < Node->second)	//the spacing for this point
						continue;	//then don't add this point to the mesh!
				}		
			}
		}

		if(Node->first.x < 0.0 || Node->first.x > mdesc.Ldimensions.x)
		{
			ProgressCheck(47, false);
		}

		//at this point, it is a good point to be added - now let's see if it's going to be an exterior point or not
		//this should be placing points within the boundary spacing on the edge

		mesh[Node->first] = tmp;	//now add the point, with all the additional info needed. This is a legitimate point

	}
	return(0);
}
int BuildLayerMesh(LayerMesh& mesh, Layer* lyr)
{
	if(lyr->AffectMesh == false)	//if it doesn't affect the mesh... who cares?
	{
		mesh.layer = NULL;
		return(0);
	}
	
	mesh.layer = lyr;	//get that done right away...
	mesh.exclusive = lyr->exclusive;
	double edgeSpacing = lyr->spacingboundary;
	double centerSpacing = lyr->spacingcenter;

	if(lyr->shape->type != SHPPOINT && lyr->shape->type != SHPLINE)
	{
		DisplayProgress(43, 0);
	}

	//at this point, the mesh must be a line... not a line segment. two vertices. These are conveniently located in max/min...

	mesh.max = lyr->shape->max;
	mesh.min = lyr->shape->min;

	if(mesh.max == mesh.min || lyr->shape->type == SHPPOINT)	//max and min will be assigned the same value if there's just a single point
	{
		XSpace middle = (mesh.max + mesh.min) * 0.5;
		mesh.Points[middle] = centerSpacing;	//that was simple...
		return(0);
	}

	if(mesh.max < mesh.min)	//make sure that we are moving in the proper direction
	{
		XSpace tmp = mesh.max;
		mesh.max = mesh.min;
		mesh.min = tmp;
	}
	
	//otherwise, we have a bit more complicated bits to do...

	XSpace direction = mesh.max - mesh.min;	//this is the direction we will be expanding out from...
	XSpace center = (mesh.max + mesh.min) * 0.5;
	double distance = 0.5 * direction.Magnitude();	//direction must be nonzero due to previous if statement

	double slope = (centerSpacing - edgeSpacing) / distance;
	direction.Normalize();	//get it in terms of a unit vector. This should likely just be <1,0,0> but attempting to semi-future proof
		
	std::map<XSpace, double>::iterator prevPt;	//will be needed for adding future points.
	XSpace Adjust;
	XSpace AddPt;
	XSpace Boundary;
	XSpace NextPt;
	XSpace NextBoundary;
	if(centerSpacing == edgeSpacing)	//equal spacing through out.
	{
		Adjust = direction * centerSpacing;
		if(Adjust > mesh.max - mesh.min)
			Adjust = mesh.max - mesh.min;
		AddPt = mesh.min + Adjust*0.5;
		mesh.Points[AddPt]=centerSpacing;
		NextBoundary = mesh.min + Adjust;
		NextPt = AddPt + Adjust;
		while(NextBoundary < center && NextPt <= center)
		{
			AddPt = NextPt;
			mesh.Points[AddPt]=centerSpacing;
			NextPt += Adjust;
			NextBoundary += Adjust;
		}
	}
	else if(slope > 0.0)	//the center has larger spacing, expand from min to center so each point gets successively larger
	{
		//get the first point done...
		double spacing = edgeSpacing;
		Adjust = direction * spacing;
		if(Adjust > center - mesh.min)
		{
			Adjust = center - mesh.min;	//force it to have at least two points...
		}
		AddPt = mesh.min + Adjust*0.5;
		prevPt = mesh.Points.insert(std::pair<XSpace, double>(AddPt, spacing)).first;
		Boundary = mesh.min + Adjust;

		Adjust = Boundary - mesh.min;	//vector from the boundary to the start
		spacing = edgeSpacing + slope * Adjust.Magnitude();	//get the spacing for the next point

		NextPt = Boundary + Boundary - AddPt;	//the boundary + the distance from the point to the boundary
		NextBoundary = Boundary + direction*spacing;

		while(NextBoundary < center && NextPt <= center)
		{
			AddPt = NextPt;
			Boundary = NextBoundary;
			prevPt = mesh.Points.insert(std::pair<XSpace, double>(AddPt, spacing)).first;
			
			//now get the next point's data loaded
			Adjust = Boundary - mesh.min;	//vector from the boundary to the start
			spacing = edgeSpacing + slope * Adjust.Magnitude();	//get the spacing for the next point
			NextPt = Boundary + Boundary - AddPt;	//the boundary + the distance from the point to the boundary
			NextBoundary = Boundary + direction*spacing;
		}
	}
	else	//slope < 0.0, center has smaller spacing than edge, expand from center to max so each point still gets successively larger
	{	//then modify edge and adjust the rest of the points
		double spacing = centerSpacing;
		slope = -slope;	//get slope positive (mirrored across center)
		Adjust = direction * spacing;
		AddPt = center + Adjust*0.5;
		prevPt = mesh.Points.insert(std::pair<XSpace, double>(AddPt, centerSpacing)).first;
		Boundary = center + Adjust;

		Adjust = Boundary - center;	//vector from the boundary to the start
		spacing = centerSpacing + slope * Adjust.Magnitude();	//get the spacing for the next point

		NextPt = Boundary + Boundary - AddPt;	//the boundary + the distance from the point to the boundary
		NextBoundary = Boundary + direction*spacing;

		while(NextBoundary < mesh.max && NextPt <= mesh.max)
		{
			AddPt = NextPt;
			Boundary = NextBoundary;
			prevPt = mesh.Points.insert(std::pair<XSpace, double>(AddPt, spacing)).first;
			
			//now get the next point's data loaded
			Adjust = Boundary - center;	//vector from the boundary to the start
			spacing = centerSpacing + slope * Adjust.Magnitude();	//get the spacing for the next point
			NextPt = Boundary + Boundary - AddPt;	//the boundary + the distance from the point to the boundary
			NextBoundary = Boundary + direction*spacing;
		}

		//now adjust everything... to get a nice boundary edge...
		XSpace oldPos = prevPt->first;
		Adjust = direction * edgeSpacing;
		XSpace newPos = mesh.max - Adjust;
		XSpace difference = newPos - oldPos;
		double delta = difference.Magnitude();	//how much the point changed by
		//all points need to change by this much
		difference = oldPos - center;
		double width = difference.Magnitude();
		double normalizeDi = delta / width;

		ShiftPointsProportionally(mesh.Points, width, normalizeDi, oldPos, direction);
		
		prevPt = mesh.Points.begin();	//get the point loaded that's the closest to the center and may be over
		if(prevPt != mesh.Points.end())
		{
			if(prevPt->first < center)	//the push made it go beyond the center point...
				mesh.Points.erase(prevPt);
		}
	}

	//now mirror the solution over to the other side...
	
	for (std::map<XSpace, double>::iterator it = mesh.Points.begin(); it != mesh.Points.end(); ++it)
	{
		if(it->first != center)
		{
			AddPt = mesh.min + mesh.max - it->first;
			mesh.Points[AddPt] = it->second;
		}
	}

	//now determine if a point is needed in the middle.
	//it's either going to be the first or the last point in the vector
	if(mesh.Points.size() > 1)
	{
		Adjust = mesh.Points.begin()->first - center;
		double tempdist = Adjust.Magnitude();
		Adjust = mesh.Points.rbegin()->first - center;
		double tdist2 = Adjust.Magnitude();	//whichever is smaller is going to be the correct point that's near the center

		if(tempdist > tdist2)
		{
			tempdist = tdist2;
		}

		if(tempdist > centerSpacing)	//add a point in the center of the layer so the maximum is not exceeded
		{	//note, half the spacing goes to the other point as well, so it all works out evenly
			mesh.Points[center] = centerSpacing;
		}
	}

	//and the layer should have all the mesh points inserted...

	return(0);
}

int ShiftPointsProportionally(std::map<XSpace, double>& data, double width, double normalize, XSpace OldPos, XSpace direction)
{
	std::map<XSpace, double> tmpMap;
	XSpace key;
	
	for (std::map<XSpace, double>::iterator it = data.begin(); it != data.end(); ++it)
	{
		XSpace dif = OldPos - it->first;
		double dist = dif.Magnitude();
		double adjust = normalize * (width - dist);	//weight the difference, note, the delta is in normalize
		key = it->first - direction*adjust;	//now shift all the points 
		tmpMap[key] = it->second;
	}

	data.swap(tmpMap);	//now the data is in tmpMap and the old irrelevant data is in tmpMap
	tmpMap.clear();
	return(0);

}

double GetDesiredSpacing(short direction, XSpace max, XSpace min, XSpace pos, double edge, double center)
{
	double mx, mn, ps;
	switch(direction)
	{
		case XDIR:
			mx = max.x;
			mn = min.x;
			ps = pos.x;
			break;
		case YDIR:
			mx = max.y;
			mn = min.y;
			ps = pos.y;
			break;
		case ZDIR:
			mx = max.z;
			mn = min.z;
			ps = pos.z;
			break;
		default:
			mx = mn = ps = 0.0;
	}
	double mag = mx - mn;	//magnitude of the line
	if(mag == 0.0)
		return(0.5 * (edge + center));

	double cpos = 0.5*(mx + mn);	//center position...
	double dist = 0.5 * mag;	//the total length to balance between
	dist = 1.0 / dist;	//invierse for quick division

	double ret = 0.0;

	if(ps == cpos)
		return(center);
	else if(ps >= mx || ps <= mn)	//is it outside the max or min?
		return(edge);	//then go with the edge spacing
	else if(ps > cpos)
	{
		double distc = ps - cpos;	//distance to center
		double diste = mx - ps;		//distance to edge
		ret = dist * (center * diste + edge * distc);	//appropriate balancing
	}
	else	//ps < cpos
	{
		double distc = cpos - ps;
		double diste = ps - mn;
		ret = dist * (center * diste + edge * distc);
	}

	return(ret);
}

LayerMesh::LayerMesh()
{
	layer = NULL;
	Points.clear();
	exclusive = false;
}

LayerMesh::LayerMesh(Layer* lyr)
{
	layer = lyr;
	Points.clear();
	exclusive = false;
}

LayerMesh::~LayerMesh()	//don't want to actually delete the layer...
{
	layer = NULL;
	Points.clear();
	exclusive = false;
}
