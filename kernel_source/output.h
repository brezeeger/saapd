#ifndef OUTPUT_H
#define OUTPUT_H

#include <fstream>
#include <map>
#include <vector>

class DataOut
{
public:
	double mean;
	double variance;
	double stddev;
	double standarderror;
	DataOut();
};

class Simulation;
class ELevelRate;
class Point;
class Model;
class ModelDescribe;
class OpSort;
class BulkOpticalData;
class SS_SimData;

int OutputPointOpticalDetailedInit(Simulation* sim, Point& pt);
int OutputPointOpticalDetailedELev(Simulation* sim, ELevelRate& rate);
int OutputPointOpticalDetailedSplit(Simulation* sim, long int ptID, double photonEnergy, double numPhotons);
int OutputCharge(Simulation* sim, bool initialize);
int PrepOutputDoubleToFile(double data, std::ofstream& file, bool integer=false);
int OutputDataToFileSS(Model& mdl, ModelDescribe& mdesc);	//output.cpp
//int OutputDataToFileTransient(Model& mdl, ModelDescribe& mdesc);	//output.cpp
double FillDataOut(DataOut& retval, double* dataset, Simulation* sim, unsigned int binctr);
int OutputDataFinal(Model& mdl, Simulation& sim);
int OutputDataInternalPre(Model& mdl, double timeweight, unsigned int binctr);
int OutputDataInternalPost(Model& mdl, Simulation& sim, unsigned int binctr);
void DisplayProgress(int check, int num);
void DisplayProgressDoub(unsigned int message, double value);
int OutputContacts(Simulation* sim, bool initialize);
int OutputFileList(Simulation* sim, std::vector<std::string>& FileOut, std::vector<std::string>& groups);
int OutputContactJV(Simulation* sim, SS_SimData* ss, std::string groupName, int mainCtc, int secCtc);
int OutputEnvironmentSummary(ModelDescribe& mdesc, SS_SimData* ss);
template <class Output>
int OutputPartToFile(Output data, std::ofstream& file, unsigned int width, unsigned int precision=0, bool doprecision=false);
double FillDataOut(DataOut& retval, double* dataset, unsigned int total);

int OutputQERowSubsection(std::string fileprepend, std::string name, std::map<double, double>& data, int firstcolSize, double tStepi, double areai, double binWidth, Simulation* sim);
int BinWaveData(std::map<double, double>& wLengthData, std::map<OpSort, double>& fullData, int numBins);
int OutputQERows(std::string file, BulkOpticalData* lightData, ModelDescribe& mdesc, double tStepi);
int FinalOutputQEDataSS(ModelDescribe& mdesc);
int FinalOutputQEDataTransient(ModelDescribe& mdesc);
void DisplayProgressStr(std::string str, int check=-1);	//display a custom string

int OutputEnvironments(Simulation* sim, SS_SimData* ss);
#endif