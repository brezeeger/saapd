#ifndef MODEL_H
#define MODEL_H

#include <map>
#include <vector>
#include <set>
#include <list>
#include "random.h"	//defines class MyRand.
#include "helperfunctions.h"
#include "BasicTypes.h"
#include "srh.h"
#include "Layers.h"	//just separated this out, because I don't want to include model.h for EVERYTHING gui related.


extern MyRand SciRandom;	//global variable defined elsewhere


class Point;
class ELevel;
class Corrections;
class Model;
class angleMatch;
class TaskList;
class EMotionTask;
class Contact;
class ModelDescribe;
class PtMCConstants;
class BulkOpticalData;
class Material;
class Simulation;
class TargetE;
class XMLFileOut;
class ContactLink;

class TransientSimData;	//transient.h
class TimeStepControl;	//transient.h
class MonitorTStep;	//transient.h
class SS_SimData;	//ss.h
class LightSource;	//light.h
class Optical;		//light.h
class TStepELevel;	//transient.h
class OpticalWeightELevel;	//light.h
class ELevelRate;	//rategeneric.h
class OpticalTransitionData;	//light.h
class OpticalStorage;	//light.h
class SRHRateConstants;	//srh.h


double FermiFn(double Ef, double E, double kTi, bool inverse, bool AbsZero, double degen);	//helperfunctions.cpp
double DoubleSubtract(double A, double B);	//helperfunctions.cpp
bool DoubleEqual(double A, double B, double accuracy);	//helperfunctions.cpp
int CalculateRho(Point& pt);	//CalcDensities.cpp
void FindFermiPt(Point& pt, double accuracy);	//fermiband.cpp
bool FindQuasiFermiPt(Point& pt, double accuracy, bool which);	//fermiband.cpp

//#include "DriftDiffusion.h"	//get the right ones in with the default parameters


//#include "contacts.h"	//this needs to be at the end to get all the defines for model.cpp, but if model.h calls contacts which relies
//on model.h and the model.h classes have not been defined yet, hell ensues with compiler errors.
//extern double CalculateDensStateFactor(ELevel* eLev, double mincurEnergy, double maxcurEnergy);



#define RATE_OCC_BEGIN 0	//rategeneric.h



#define DIMENSIONS 2	//number of dimensions in the program.  Energy depends on n/2 kT

#define PROBPOSCUT 0.05	//minimum probability cut off for position

#define ELECTRON	true	//used for type of carrier in band
#define CONDUCTION	true	//basically used to differentiate between holes/electrons, VB/CB, et.
#define HOLE		false
#define VALENCE		false
#define SHALLOW		false	//impurities. shallow or deep.
#define DEEP		true
#define ACCEPTORLIKE false
#define ACCEPTOR false
#define DONORLIKE	true
#define DONOR true

#define DISCRETE 0	//just a single energy level
#define BANDED 1	//uniform across a band
#define GAUSSIAN 2	//distributed as a gaussian uniformly perpendicularly along the other two dimensions
#define GAUSSIAN2 3	//a two dimensional gaussian distributed uniformly along the other dimension
#define GAUSSIAN3 4	//a three dimensional gaussian
#define EXPONENTIAL 5	//e levels distributed as an exponential ->only in one direction...
#define EXPONENTIAL2 6	//exponential going out in 2D
#define EXPONENTIAL3 7	//exponential going out in 3D
#define LINEAR 8	//linear distribution. would not expect to happen...
#define PARABOLIC 9	//a parabolic distribution of states, also would not expect often...
#define PARABOLIC2 10	//a parabolic distribution going out in 2D
#define PARABOLIC3 11	//a parabolic distribution going out in 3D

//do not change these numbers or it will screw up matching with the GUI indices
#define UNDEF 0			//used in type of material. undefined
#define SEMICONDUCTOR 1	//material is a semiconductor
#define METAL 2			//material should be treated as a metal
#define DIELECTRIC 3	//material should be treated as a dielectric or insulator
#define VACUUM 4		//is this material actually a lack of material?

#define THERMALIZE_NONE 0
#define THERMALIZE_CB 1
#define THERMALIZE_VB 2
#define THERMALIZE_ACP 4
#define THERMALIZE_DON 8
#define THERMALIZE_CBT 16
#define THERMALIZE_VBT 32
#define THERMALIZE_ALL (THERMALIZE_CB | THERMALIZE_VB | THERMALIZE_ACP | THERMALIZE_DON | THERMALIZE_CBT | THERMALIZE_VBT)


#define CHECKNEGATIVE -1e-30	// for some odd reason, my checks for <0.0 are returning true when it is = 0.0. Kind of obnoxious.
#define THERMALVELOCITY 2.0e7	//just put a generic cap on the thermal velocity for now.

#define CONTACT -2	//points to a contact. in layer id's, all contacts will have -# and be differentiated.
#define EXT -1

#define NO -1

#define SIMTYPE_UNKNOWN 0
#define SIMTYPE_TRANSIENT 1
#define SIMTYPE_SS 2


struct PtData
{
	double data;
	unsigned long int pt;
};

struct PtDataPtr
{
	double data;
	Point* pt;
};


/*
array of integers to navigate through the device so other points in the physical device may be
found quickly
*/
class PointAdjSort
{
public:
	long int self;	//an ID number to sort things
	Point* adjP;	//pointer to point in the positive direction (index)
	Point* adjN;	//pointer to point in the negative direction (index)
	Point* adjPX;	//pointer to adjacent point in positive X direction
	Point* adjPX2;
	Point* adjMX;
	Point* adjMX2;
	Point* adjPY;
	Point* adjPY2;
	Point* adjMY;
	Point* adjMY2;
	Point* adjPZ;
	Point* adjPZ2;
	Point* adjMZ;
	Point* adjMZ2;

	bool operator >(const PointAdjSort &b);
	bool operator <(const PointAdjSort &b);
	bool operator >=(const PointAdjSort &b);
	bool operator <=(const PointAdjSort &b);
	bool operator !=(const PointAdjSort &b);
	bool operator ==(const PointAdjSort &b);
	
};

class Adjacent
{
public:
	long int self;		//what is this one's grid point value...
	long int coordPx;	//vector's array value immediately next door in positive X (right)
	double PxDistI;
	long int coordPx2;	//allow boxes to divide up, but at most two in any given direction. - points to +y of two x's
	double Px2DistI;
	long int coordMx;	// left
	double MxDistI;
	long int coordMx2;	//make sure the two represents the further positive choice
	double Mx2DistI;
	long int coordPy;	//forward - represents the -x point in +y
	double PyDistI;
	long int coordPy2;	//so Py2 represents the +x in +y
	double Py2DistI;
	long int coordMy;	//backward
	double MyDistI;
	long int coordMy2;
	double My2DistI;
/*
	unsigned int coordPz;	//up
	unsigned int coordMz;	//down
*/
};



class Impurity
{
	std::string name;	//string for identifying the impurity in GUI
	double energy;	//the main point of the energy
	double energyvar;	//coefficient for how the states vary (std. dev, exponent, etc.)
	double density;	//density of states in cm^-3
	double sigman;	//capture cross section of impurity for electrons at 300k.
	double sigmap;	//capture cross section for holes at 300k
	double OpticalEfficiencyCB;	//how well does this generate light during SRH with CB
	double OpticalEfficiencyVB;	//how well does this generate light during SRH with VB
	float degeneracy;	//when in the fermi function, what is the degeneracy factor for this impurity? Donors generally 2, Acceptors generally 4. Note, donors are inverted to
	Material* defectMaterial;	//if this impurity is a defect, what material is it specific to?
	//maintain consistency in calculating the fermi function, so if a 2 is input by the user for a donor, a 0.5 is recorded.
	int ID;	//an id for the impurity.
	short int charge;	//what's the charge of the ion when it has given up the electron or hole?
	short distribution;	//how are the states distributed in the energy band? Discrete, Gaussian, Exponential, Linear
	bool depth;		//shallow or deep
	bool reference;	//is this referenced to CB or VB - donorlike or acceptorlike

public:
	static const short discrete = 0;	//these line up with previous defines. Maintain
	static const short banded = 1;	//compatibility with older stuff and any potential code hickups.
	static const short gaussian = 2;
	static const short exponential = 5;
	static const short linear = 8;
	static const short parabolic = 9;

	static const bool shallow = false;
	static const bool deep = true;
	static const bool acceptor = false;
	static const bool donor = true;
	
	Impurity(int id=-1, std::string nm="", Material* defectMat=nullptr, double e=5e-3, double eVar=5e-4,
		double dens=1e10, bool dep=shallow, bool ref=acceptor, short chrg=-1, int dist=0, double sign=1e-16, double sigp=1e-14, 
		float degen=0.0f, double optEffCB=1.0, double optEffVB=1.0);
	~Impurity();

	bool isDefect() const { return(defectMaterial != nullptr); }
	Material* getDefectMaterial() const { return(defectMaterial); }
	bool inMaterial(Material* mat) const { return mat == defectMaterial; }
	void setDefectMaterial(Material* mat = nullptr) { defectMaterial = mat; }

	bool isImpurity(int id) { return ID>=0 && ID == id; }
	bool isImpurity(std::string nm) { return name == nm; }
	bool isImpurity(int id, std::string nm) { return (name == nm && id == ID); }
	bool isImpurity(Impurity* imp) { return this == imp; }
	bool needsNewID() { return ID < 0; }
	std::string getName() const { return name; }
	void setName(std::string nm) {
		if (nm.size() > 19)
			nm.resize(19);
		name = nm;
	}

	bool isShallow() const { return (depth == shallow); }
	bool isDeep() const { return (depth == deep); }
	bool getDepth() const { return depth; }
	void setShallow() { depth = shallow; }
	void setDeep() { depth = deep; }
	void setDepth(bool d) { depth = d; }

	bool isDonor() const { return (reference == donor); }
	bool isCBReference() const { return (reference == donor); }	//alias
	bool isAcceptor() const { return (reference == acceptor); }
	bool isVBReference() const { return (reference == acceptor); }	//alias
	bool getReference() const { return reference; }
	void setDonor() { reference = donor; }
	void setAcceptor() { reference = acceptor; }
	void setReference(bool ref) { reference = ref; }

	void setRelativeEnergy(double e) { energy = e; }
	double getRelativeEnergy() const { return energy; }

	void setEnergyVariation(double e) { energyvar = e; }
	double getEnergyVariation() const { return energyvar; }

	void setDensity(double d) { density = (d >= 0.0) ? d : 0.0; }
	double getDensity() const { return density; }
	bool isUsed() const { return (density > 0.0); }

	bool isEnergyDiscrete() const { return (distribution == discrete); }
	bool isEnergyBanded() const { return (distribution == banded); }
	bool isEnergyGaussian() const { return (distribution == gaussian); }
	bool isEnergyExponential() const { return (distribution == exponential); }
	bool isEnergyLinear() const { return (distribution == linear); }
	bool isEnergyParabolic() const { return (distribution == parabolic); }
	short getEnergyDistribution() const { return distribution; }

	void setEnergyDiscrete() { distribution = discrete; }
	void setEnergyBanded() { distribution = banded; }
	void setEnergyGaussian() { distribution = gaussian; }
	void setEnergyExponential() { distribution = exponential; }
	void setEnergyLinear() { distribution = linear; }
	void setEnergyParabolic() { distribution = parabolic; }

	//sets distribution to input. If bad, keeps current value. If that's also bad, sets to discrete
	bool setEnergyDistribution(short d) {
		switch (d) {
		case discrete:
		case banded:
		case gaussian:
		case exponential:
		case linear:
		case parabolic:
			distribution = d;
			return true;
		default:
			break;
		}
		//an invalid value was passed.
		//check if there is currently an invalid value in it. if so, default to discrete
		switch (distribution) {
		case discrete:
		case banded:
		case gaussian:
		case exponential:
		case linear:
		case parabolic:
			break;
		default:
			distribution = discrete;
		}
		return false;
		
	}
	double getElecCapture() const { return sigman; }
	void setElecCapture(double d) { sigman = (d >= 0.0) ? d : 0.0; }
	
	double getHoleCapture() const { return sigmap; }
	void setHoleCapture(double d) { sigmap = (d >= 0.0) ? d : 0.0; }
	
	double getDegeneracy() const { return (double)degeneracy; }
	void setDegeneracy(float d) { degeneracy = (d > 0.0f) ? d : 1.0f; }	//must be positive. a degeneracy of 0 means no states!
	void setDefaultDegeneracy() {
		degeneracy = (reference == donor) ? 0.5f : 4.0f;
		//fd = 1/[1+0.5*exp((E-Ef)/kT)]	in general
		//fa = 1/[1+4*exp((E-Ef)/kT)] in general.
	}

	//fermi level statistics to allow a factor multiplied by to get e- concentration
	//in a donor, the degeneracy is used to calculate the ionized donor concentration,
	// so when you do (1-Nd+) to get e- concentration, it inverts the degeneracy factor
	void adjustDonorDegeneracyForOccupationProbability() {
		if (reference == donor)
			degeneracy = 1.0f / degeneracy;	//degeneracy can never equal zero
	}


	bool canGenerateLightCBTransition() { return (OpticalEfficiencyCB > 0.0); }
	double getOpticalEfficiencyCB() const { return OpticalEfficiencyCB; }
	void setOpticalEfficiencyCB(double o) {
		if (o > 1.0) o = 1.0;
		if (o < 0.0) o = 0.0;
		OpticalEfficiencyCB = o;
	}

	bool canGenerateLightVBTransition() { return (OpticalEfficiencyVB > 0.0); }
	double getOpticalEfficiencyVB() const { return OpticalEfficiencyVB; }
	void setOpticalEfficiencyVB(double o) {
		if (o > 1.0) o = 1.0;
		if (o < 0.0) o = 0.0;
		OpticalEfficiencyVB = o;
	}

	int getID() const { return ID; }
	void setID(int id) { ID = id; }
	int* getIDPtr() { return &ID; }	//careful how used!

	short getCharge() const { return charge; }
	void setCharge(short c) { charge = c; }
	void testDefaultCharge();


	void SaveToXML(XMLFileOut& file);
	void Validate(ModelDescribe& mdesc);

	void LoadFromFile(std::ifstream& file);	//xml
	int LoadFromStateFile(std::ifstream& file, std::stringstream& str);	//loadstate.cpp
	int WriteStateFile(std::ofstream& file);	//savestate.cpp
};




class RefTrans
{
public:
	ELevel* ptrTargetState;	//the state receiving the carriers
	ELevel* ptrTargetFromState;	//level carrier scatters into to get to an adjacent state
	Point* ptrTargetPoint;	// the point which may receive the carriers 
	double reflection;		//reflection coefficient.
	double transmission;	//transmission coefficient (1-R)
	double percentto;	//how many of the electrons of the total are going to this state? In event of overlapping states.
	bool band;	//which band the recipient of carriers is in. impurities/defects always conduction
	bool tunnel;	//must it tunnel to get here (impurity/defect)? Or is there no states and it must tunnel through?
	short direction;	//the direction the carrier is moving in
	
	RefTrans();	//constructor/destructor
	~RefTrans();
};



/*
class ELevelOpticalRate	//this is part of a tree and acts as a sort value where the data is the actual rate
{
public:
	ELevel* source;	//this presumably is going to be irrelevant info, but might be good to ahve
	ELevel* target;
	int type;	//is this SRH, etc.
	bool carrier;	//what kind of carrier is being transferred (eletron or hole)

	bool sourcecarrier;	//is it coming from a state that uses electrons or targets
	bool targetcarrier;	//is it going into a state that uses electrons or targets
	double netrate;			//the net rate for the type of rate this is
	double RateOut;		//this might not actually line up with netrate as In-Out
	double RateIn;	//this might not actually line up with netrate as In-Out
	double RateType;	//what the rate should be as viewed by the type
	double desiredTStep;	//what this rate wants the timestep to be
	double deriv;		//what is the derivative of this rate with respect to the source carrier
	double deriv2;	//what is the second derivative of this rate with respect to the source carrier
	double derivsourceonly;	//derivative of rate based solely on just the carrier moving. Assuming the target does not change.
	//will stop transferring or bounce back and forth (otherwise diverge).
	
	bool DetermineNumCarrierTransfer(double& numCar, double NRate);	//how many carriers should be used to do the transfer, returns whether to set eLev->percentTransfer
	ELevelOpticalRate operator +(const ELevelOpticalRate &b);
	ELevelOpticalRate& operator +=(const ELevelOpticalRate &b);
	ELevelOpticalRate();
	~ELevelOpticalRate();

	bool operator ==(const ELevelOpticalRate &b) const;
	bool operator !=(const ELevelOpticalRate &b) const;
	bool operator <=(const ELevelOpticalRate &b) const;
	bool operator >=(const ELevelOpticalRate &b) const;
	bool operator <(const ELevelOpticalRate &b) const;
	bool operator >(const ELevelOpticalRate &b) const;
};
*/


class TunnelTarget
{
public:
	ELevel* targeteLev;
	double reflection;
	double transmission;
	std::vector<long int> tunnelPts;	//all the points the target tunnels through
	
};

class ELevelTransfer
{
public:
	ELevel* source;
	bool carrier;

	bool operator ==(const ELevelTransfer &b) const;
	bool operator !=(const ELevelTransfer &b) const;
	bool operator <=(const ELevelTransfer &b) const;
	bool operator >=(const ELevelTransfer &b) const;
	bool operator <(const ELevelTransfer &b) const;
	bool operator >(const ELevelTransfer &b) const;
};





#define ELEVEL_DENS_MIN 1
#define ELEVEL_DENS_USE 2
#define ELEVEL_DENS_MAX 4
#define ELEVEL_DENS_ALL 7

class ELevel
{
protected:
	Point& pt;	//a link to the point
private:
	double beginoccstates;	//number of states occupied at the start of the iteration - no need to save. Updated during init
	double numstates;		//the total number of states in this particular level
	double endoccstates;	//number of occupied states at the end of the iteration
	double prevOcc;			//what was the occupation on the previous iteration - fall back in casetime step was too large
	double thermalOcc;		//what is the thermal equilibrium occupancy
	double thermalAvail;	//what is the thermal equilibrium availability
	bool prevOccisCarrierType;	//
	bool BegoccStatesisCarrierType;	//most notably for impurities - might be mostly full
	bool EndoccStatesisCarrierType;	//begin and end may be out of sync, so it gets screwed up
	bool updateRelFermi;	//is this particular value up to date?
	bool SwapOccCarrierType(int which);	//does the occupancy carrier type need to be changed? Only test the beginOccStates!
	double densityMin;	//what is the relative density at energy min
	double densityUse;	//what is the relative density at energy use
	double densityMax;	//what is the relative density at energy max
public:
	double rateIn;	//the total rate going into the state - ignoring equilibrium values (they will not significantly affect the rate used for determining timestep)
	double rateOut;	//the total rate of carriers going out of the state - ignore eq values. - note, if e- in VB, this counts as a negative or positive rate in
	double percentTransfer;	//how many carriers this particular state wants to have transferred
//	double difEf;		//sum Ef_src - Ef_trg for all rates
//	double prevdifEf;	//the previous one
//	double changedifEf;	//the difference between difEf and prevdifEf.
//	double prevChangeDifEf;	//previous difference.  When it's the opposite sign as changedifEf, it is now moving in a different direction and should be reduced
//	double unalteredROut;	//the total rate out, including eq values and rates that have been cut-off to allow slower rates
//	double unalteredRIn;	//the total rate out, including eq values and rates that have been cut-off to allow slower rates
//	double IntraPtRateIn;	//the rate in coming from within the point (so carrier distribution may change within a point).
//	double IntraPtRateOut;	//same as above, but what is leaving the point.
//	double pEQRateIn;	//the rate in from close to equilibrium but not quite (ignore in net rate timesteps)
//	double pEQRateOut;	//the rate out from close to equilibrium but not quite (ignore in net rate timesteps)
	double netRate;	//net rate for this energy level on this iteration
	double netRateM1;	//net rate for this energy level on previous iteration
	double netRateM2;	//and net rate for this energy level two iterations ago.
	double partialNRate;
	double partialNRateM1;	//the last rate during the maximum timestep.
	bool partialFlippedSignMax;	//did the partial rate flip sign (indicating steady state). Basically used to ignore any large changes that may be occurring.
	bool partialFlippedSignFall;	//it flipped signs when falling. Gotta get out of an infinite loop when the corrections are too large and it just bounces around zero
	double relFermi;	//what is the relative fermi? Positive numbers indicate large amounts of carriers. Negative #'s typical
	
//	double EminEfM1;	//what is E minus Ef in previous iteration
//	double EminEfM2;	//what is E minus Ef two iterations ago
//	double RateEfIn;	//sum of the rate in  times |difference| in Ef
//	double RateEfOut;	//sum of the rate out times the |difference| in Ef
//	double maxREfIn;	//what was the largest for Rin
//	double maxREfOut;	//and the largest for Rout
//	double occM1; //used for steady state calculation
//	double occM2; //used for steady state calculation
//	double desireChange;	//how much does the carrier desire to change. The derivative should always be negative for the rates
	double maxOcc;	//the maximum occupancy that can be done to maintain stability for large changing transfers, (if not MC: ref to beginocc)
	double minOcc;	//the minimum occupancy that can be done to maintain stability for large changing transfers, (if not MC: ref to beginocc)
	double minAvail;
	double maxAvail;
	double minEf;	//assuming it doe snot interact with any rates to push it away from equilibrium (currently only light & contacts), what is the lowest actual Ef (not relative) this state can change to this iteration
	double maxEf;	//assuming it doe snot interact with any rates to push it away from equilibrium (currently only light & contacts), what is the largest actual Ef (not relative) this state can change to this iteration
	double targetOcc;	//what is the actual target occupancy it is trying to get according to the SS relEf
	double targetAvail;	//what is the actual target availability it is trying to get according to the SS relEf
	double tOccMin;	//at what point is it 'good enough' to say it's at the target occupancy?
	double tAvailMin;	//at what point is it 'good enough' to say it's at the target occupancy?
	double tOccMax;	//at what point is it 'good enough' to say it's at the target occupancy?
	double tAvailMax;	//at what point is it 'good enough' to say it's at the target occupancy?
	double maxOccRate;	//what is the associated rate for maxOcc (a negative net rate - rate out)
	double minOccRate;	//associated rate for minOcc (a positive net rate - rate in)
//	double RateLimit;	//this limits the rates to a particular maximum. It goes down when the net rate goes down.
//	double maxAbsRate;	//what is the largest magnitude net rate?
//	double minNonZeroAbsRate;	//what is the smallest nonzero magnitude net rate? Zero means no limit/assign a value. Negative means it's in an iteration where it may be adjusted. Positive means hold value.
//	double nextRateLimit;	//look at rates to see what might potentially be the next rate limit. Just figure these things out as they happen
//	double prevChange;	//how much this changed on the previous iteration
	double deriv;	//the derivative to how much the rate changes based on carriers going in
	double deriv2;	//2nd deriviative of how much the rate changes based on carriers entering
	double derivsourceonly;	//first derivative based on the change from the source only.
	double adjustCarrier;	//how many carriers this wants to adjust to get to neutral. Add to occ states.
//	bool allowTransferReduction;
	bool LimitByAvailability;	//when active, sets the limit for transfer (and timestep) by availability
	bool prevAdjust;	//true if the energy level was adjusted in the previous iteration
	int SSTargetCleared;	//if it reaches a target, mark that it reached the spot so we know it might not be going to a previous Ef
	
	//the whole concept of equilibrium transfers is probably a dumb one...
	//above gets reset every iteration and below must be saved
	
	double targetTransfer;	//how many carriers this level wants to transfer.
	bool carriertype;	//is it an electron or a hole?
	bool bandtail;	//is this energy state a band tail?
	double min;	//minimum energy in the band
	double max;	//maximum energy in the band
	double eUse;	//what energy should be used (for now, just half min and max)
	double boltzeUse;	//used in calculating scatter partition
	double boltzmax;	//integral of boltzmann distribution over the energy range from min to max, divided by -kT. (when seeing proportion of carriers, the -kT's cancel)
	double carrierspectrumcutoff;	//if this state is involved in generation of light, the number of carriers must be greater than this to justify doing it by function instead of a simple value
	double efm;	//effective mass at this level. EXACT. not relative to mass electron
	double tau;	//mean scattering time for particle at this energy level.
	double taui;	//inverse of scattering time...
	double velocity;	//the base velocity for a particle at this energy level
	double fermi;	//the fermi probability for this energy level times the number of states (partition function useful...)
	double FermiProb;	//what the probability of occupancy should be for this state based on the point's fermi level
	double AbsFermiEnergy;	//the absolute fermi energy of for this particular energy state
	bool includeFermiBelow;	//for instances of no or fully occupied energy levels, the absolute fermi energy should impose a limit
	bool includeFermiAbove; //on energies and "be happy" with anything above the stated values. If the overall energy is not above the threshold energy, then allow it to have influence
	short int charge;	//elementary charge on the state if it is NOT occupied, not occupied means no electrons
						//present. electrons are used for ALL states (even acceptors), EXCEPT the valence band
	unsigned long long int uniqueID;	//a unique id for this energy level.
	Impurity* imp;	//pointer to the impurity so things may be deciphered easier
	
//	TStepELevel* tStepCtrl;
	OpticalWeightELevel* opWeights;
	long int ssLink;

	XSpace scatter;	//the percentage of scattering in the three directions based on tauI and the time it would take to cross the point in each direction
	XSpace time;	//the time it takes for an electron to travel in each direction
	
	std::vector<SRHRateConstants> SRHConstant;	//these are all calculated once per simulation. Everything is a constant except occupancy...
	//maps are slow unless you have LOTS of data

	//TreeRoot<TargetE, unsigned long long> TargetELev;	//double is percent to this energy level (in event of overlaps)
	std::vector<TargetE> TargetELevDD;
	std::vector<ELevel*> NeighborEquivs;	//when things get all out of whack, determine the neighbor equivalents to try and do a smoothing, corrective behavior
	std::vector<ELevel*> TargetELev;
	std::vector<ELevelRate> OpticalRates;	//data is the actual rate - note, this is RATES OUT!
	std::vector<ELevelRate> Rates;	//these should be inserted simply.
	std::vector<double> TargetEfs;	//solved by steady state, what the transient rates should be approaching as a target fermi level.
	//the following is for fast calculation rates that may cancel with a different rate
	std::vector<ELevelRate*> ExcessiveNetPRates;
	std::vector<ELevelRate*> ExcessiveNetNRates;
	std::vector<double> NetRateP;
	std::vector<double> NetRateN;
	PosNegSum RateSums;
	PosNegSum unusedRates;

	ELevel(Point& point, unsigned long long int ID);
	ELevel(Point& point);
	~ELevel();
	bool operator ==(const ELevel &b);
	ELevel& operator +=(const ELevel &b);

	Point& getPoint();
	Model& getModel();
	ModelDescribe& getMdesc();
	Simulation* getSim();

//	vector<ELevelOpticalRate>::iterator AddELevelRate(ELevelOpticalRate& rate);
	std::vector<ELevelRate>::iterator ELevel::AddELevelRate(ELevelRate& rate, bool optical);

	double SetNumstates(double value);
	double GetNumstates(void);
	
	double GetEndOccStates(void);
	//double GetEndOccOpposite(void);
	double SetEndOccStates(double value);
	//double SetEndOccOpposite(double value);
	double AddEndOccStates(double value);	//always adds the carriertype
	double RemoveEndOccStates(double value);	//always adds the carrier type
	double SetEndOccStatesOpposite(double value);

	double SetBeginOccStates(double value);
	double SetThermalOcc(double value);
	double SetThermalAvail(double value);
	double GetThermalOcc(void) { return(thermalOcc); };
	double GetThermalAvail(void) { return(thermalAvail); };
	//double SetBeginOccOpposite(double value);
	double AddBeginOccStates(double value);
	double RemoveBeginOccStates(double value);	//always adds the carrier type
	double MultipltyBeginOccStates(double value);	//always adds the carrier type
	double SetBeginOccStatesOpposite(double value);
	double GetBeginOccStates(void);
	double GetBeginElec(void);
	double GetEndElec(void);
	double GetBeginHole(void);
	double GetEndHole(void);
	double GetBeginAvailable(void);
	double GetEndAvailable(void);
	double GetBeginOccVariable(void) {return(beginoccstates);};
	bool GetBeginOccMatch(void) { return(BegoccStatesisCarrierType); };
	double SetPrevOcc(double value);
	double SetPrevOccOpposite(double value);
	double GetPrevOcc(void);
	double GetPrevOccOpposite(void);
	double AddOccPrevToEnd(double value);
	double AddAvailPrevToEnd(double value);
	int SetOccFromRelEf(int which, double relEf);
	int OccFromAbsEf(double absEf, double& occ, double& avail);
	double getAbsEUse(void);
	double CalcChangeFromAdjustRelFermi(double adjRelEf);
	double TransferCarriersTransient(double& staticCharge);
	//double GetBeginOccOpposite(void);

	double GetOccupancy();	//returns the time integrated occupancy
//	double GetOppOccupancy();	//returns the other carrier type occupancy
	
	void UpdateAbsFermiEnergy(int typeocc);
	double CalcRelFermiEnergy(int typeocc, bool update = true);
	double CalcRelFermiEnergy(double occ, double avail);
	double CalcAbsEUseEnergy(void);
	double CalcProbFromFermi(double fermi, bool relative);
	void CalculateAbsoluteHighLowEnergy(double& highEnergy, double& lowEnergy, double& useEnergy);
	void CalculateAbsoluteHighLowEnergy(double& highEnergy, double& lowEnergy);
	void CalculateRelativeHighLowEnergy(double& highEnergy, double& lowEnergy);
	double CalcDesiredTStep(int curSSTarget);
	void SetDensity(double min, double use, double max, int which = ELEVEL_DENS_ALL);
	double GetMinDensity(void) { return(densityMin); };
	double GetMaxDensity(void) { return(densityMax); };
	double GetUseDensity(void) { return(densityUse); };
		//calculates the absolute energies of a particular state

	double PercentOverlap(ELevel* trgE);	//returns how much percentage of this state overlaps with the adjacent state - NOT boltzmann adjusted, just 0...1

	int GetOccupancyEH(int whichsource, double& elec, double& hole);
	int GetOccupancyOcc(int whichsource, double& occ, double& avail);
	int TransferEndToBegin(void);
	int TransferBeginToEnd(void);
	int GetThermalType(void);	//returns what thermal type the energy level is
	bool IsContactSink(void);
	int UpdateMaxMinTargets(double relEf, double EfSpread);	//relEf is assumed that >0 means large occupancy

	double CalculateDensStateFactor(double mincurEnergy, double maxcurEnergy);
	double CalcFermiSplitConc(bool setRelEF, double& relEf, double absEMax, double absEMin, double absESrcMax, double absESrcMin);

	int FindAdjEquivELevels();
	int CheckAdj(Point* trgPt, int type);

	int AdjacentPoint(ELevel* trg);
	int DetermineRateType(ELevel* trgeLev);

	bool AllowRate(double netRate, int which = RATE_OCC_BEGIN);
	int TestTotalRate(int which);

	ELevel& operator =(const ELevel& rhs);

	Contact* GetContact();
	double FastELevelContactProcess(int whichcarrierSrc, int whichcarrierOther);
	double FastELevelContactLink(ContactLink* link, int whichCarrierSrc, int whichcarrierOther);
	int InitTransIteration(bool ResetPercentTransfer, double percentTransfer, bool IncreaseTransfer);



	int CalculateRates();

};						//determine the probability.
class Band
{
private:
	double numparticles;	//obnoxious... but it needs to be determined why this is going negative...
public:
	bool type;	//CONDUCTION(ELECTRON) OR VALENCE(HOLE)
	double bandmin;	//absolute band energy at edge of band for point
	std::multimap<MaxMin<double>, ELevel> energies;
//	TreeRoot<ELevel, double> energies;	//relative energies to bandmin.
	double numparticlesold;	//the previous number of particles in this band
	double partitioni;	//the inverse of the sum of all the fermi's for this band
	double totalstates;	//the total number of states included in the band
	double EffectiveDensity;	//the effective DOS at the band edge

	double GetNumParticles(void);
	double SetNumParticles(double value);
	double AddNumParticles(double value);
	double RemoveNumParticles(double value);
	ELevel* FindEnergy(double energy);
	Band();
	~Band();

};	//means coding will have to find the difference between the two relative sides to compare.

class TargetE
{
public:
	ELevel* target;
	double percentOverlap;	//how much of this state overlaps for direct transport in
	double percentScatter;	//how much of this state receives current due to scattering
	TargetE();
	~TargetE();
};

class Outputs
{
public:
	//general
	bool fermi;
	bool fermin;
	bool fermip;
	bool Ec;
	bool Ev;
	bool Psi;
	bool CarrierCB;
	bool CarrierVB;
	bool impcharge;
	bool n;
	bool p;
	bool Lpow;
	bool Lgen;
	bool Lsrh;
	bool Ldef;
	bool Lscat;
	bool Erec;
	bool Esrh;
	bool Edef;
	bool Escat;
	bool genth;
	bool SRH;
	bool recth;
	bool rho;
	bool scatter;
	bool Jp;	//net hole current in each direction
	bool Jn;	//net electron current in each direction
	bool CBNRin;
	bool VBNRin;
	bool DonNRin;
	bool AcpNRin;
	bool QE;
	bool LightEmission;
	bool EmptyQEFiles;
	bool OutputAllIndivRates;	//INPUT - generally false - used mostly for debugging
	bool OutputRatePlots;	//INPUT - generally false - outputs Net Rate(occupancy) and calculated derivative for arandom energy state
	//transient
	bool stddev;	//standard deviation of the data
	bool error;	//error of mean
	bool chgError;	//deprecated. output the various charge 'errors' for the point (aka, huge timestep, restricted occupancy, what is the difference?)
	bool contactCharge;	//frankly don't remember
	unsigned int numinbin;		//number of binning to do!
	//steady state
	bool jvCurves;
	bool envSummaries;
	bool envResults;
	bool contactStates;
	bool capacitance;
	
	void ClearAll();
	void SaveToXML(XMLFileOut& file);
	Outputs();
};




class Calculations
{
public:
	bool SRH1;
	bool SRH2;
	bool SRH3;
	bool SRH4;
	bool ThermalGen;
	bool ThermalRec;
	bool Scatter;
	bool Current;
	bool Tunnel;
	bool Lgen;	//light causing generation
	bool Lsrh;	//light causing recombination
	bool Lscat;	//light scattering in the band -- going to nix this guy
	bool Ldef;	//light motion between defects
	bool Lpow;	//the power in the light (requires all the dang calculations)
	bool SErec;	//light causing generation
	bool SEsrh;	//light causing recombination
	bool SEscat;	//light scattering in the band
	bool SEdef;	//light motion between defects
	bool LightEmissions;	//light emitted resulting from falling electrons (ignore scattering)
	bool RecyclePhotonsAffectRates;	//if there are light emissions, those wreak numerical havoc. Add a flag to prevent some of that havoc
	int Thermalize;
	double OpticalEResolution;	//the spacing for the energy
	double OpticalEMin;		//the starting energy for optical transitions, default zero
	double OpticalEMax;		//the ending energy for optical transitions - anything above is cut off. if <0, there is no limit
	double OpticalIntensityCutoff;	//ignore everything less than this intensity

	void DisableAll(void);
	void EnableAll(void);
	void SetDefaultValues(void);

	void SaveToXML(XMLFileOut& file);

	void GetOpticalEnergies(double &min, double &max, double &res) {
		min = OpticalEMin;
		max = OpticalEMax;
		res = OpticalEResolution;
	}

	Calculations();
};


class MiscData
{
public:
	unsigned int largestMatNameSize;
};



class Simulation
{
	int maxOpticalSize;	//what is the maximum optical index?
public:
	std::string OutputFName;	//INPUT - the filename to set as the output
	unsigned long int curiter;	//current iteration of the monte carlo simulation
	unsigned long int maxiter;	//INPUT - maximum number of iterations allowed - 0 = unlimited
	unsigned int binctr;	//counter for binning
	
	double timeSaveState;	//INPUT - save the state every timeSaveState seconds (avoid multiply to get minutes, hours, etc.) - 0 means don't
	
	bool SignificantMilestone;	//Internal flag for transient calculations. when on, it should write the outputs - keep speed up. Only write outputs at milestones in the simulation.

	bool transient;	//is the simulation currently in a transient mode or is it steady state
	//this will be replaced by running simulations by checking for a null pointer in Transient or SteadyState
	
	
	ModelDescribe* mdesc;	//just make apointer there to begin with so I can access all data if simulation is passed. DO NOT DELETE FROM HERE!
	TransientSimData* TransientData;	//INPUT - place to store all the transient data about the simulation
	SS_SimData* SteadyStateData;	//INPUT - place to store all relevant steady state data
//nsigned int numinbin;	//INPUT - when taking in data, how many variables to do data analysis on when merging points
	int simType;	//is the simulation currently doing SS or transient?
	unsigned int curSSTarget;	//what is the current steady state target that the energy levels should use
	
	double T;		//INPUT - temperature of the device
	double Vol;	//volume of entire device
	
	int lightincidentdir;	//INPUT - light is incident from this direction. (LEFT(-X),RIGHT(+X),BACK(+Y),FRONT(-Y),UP(+Z),DOWN(-Z))
	int maxLightReflections;		//INPUT - how many reflections can the light go through? Negative number is unlimited. Default to something like 30.
	int statedivisions;	//INPUT - the number of divisions to break the states into
	
	double accuracy;	//INPUT - how close to get Ef within before calling it acceptable
	Calculations EnabledRates;	//INPUT - which rates are allowed to be calculated
	bool ThEqDeviceStart;	//INPUT - should the device be put in thermodynamic equilibrium to start?
	bool ThEqPtStart;		//INPUT - should the device make each point be in local equilibrium, though not the device.
	
	
	std::vector<std::string> OutFiles;	//output files for the entire simulation
	std::vector<std::string> groupNames;	//group names for the various output files

	std::vector<Contact*> contacts;	//pointer to the description of the contacts
	std::map<int, int> ContactPoisson;	//what contacts and aspects were used for calculating the poisson equation. (second is flags, first is contact ID)
	double posCharge;	//used to determine capacitance CV=Q ... C=dQ/dV - this charge is multiplied by Q
	double negCharge;	//used to determine capacitance CV=Q ... C=dQ/dV - this charge is multiplied by Q
	Outputs outputs;	//INPUT - which data should be outputted to the user?
	Model* mdlData;	//just a pointer to the model data to make life simpler... don't delete from here
	
	
	std::map<unsigned int, int> holecurrent;	//number of holes leaving/entering device, curiter. Quicker insert if lots of iterations
	std::map<unsigned int, int> ecurrent;	//" " electrons "/" ", ". " " "...
	BulkOpticalData* _OpticalFluxOut;	//stores the iteration data for carrier/sec for entire device
	BulkOpticalData* _OpticalAggregateOut;	//adds up all the iterations based on carriers for entire device
//	vector<LightVariation*> LightVarData;	//delete this after binning. This acts as the "data" storage of all the various data until it is output
	MiscData misc;	//just miscellaneous data that it seems stupid to recalculate over and over again
	

	std::map<long int, int> NoSS;	//keep track of which points(first) are in steady state or not and what the offending type of rate doing so is.

	bool AggregateOpticalData(double factor = 1.0);

	void ResetOpticalFlux(bool includeAggregate = false);

	double InitIteration(Model& mdl, ModelDescribe& mdesc);
	int FinalizeValues(Model& mdl);
	int PrepPointBandStructure(Model& mdl, Point& pt);
	int PrepBoundaryConditions(Model& mdl);
	bool AddOpticalData(unsigned char which, double Energy, double photons);
	double GetBinnedEnergy(double energy);
	int GetOpticalIndex(double energy);	//used to stay consistent for finding the transition energies and the index
	double GetEnergyIndex(int index);
	int SetMaxOpticalSize();
	int GetMaxOpticalSize() { return(maxOpticalSize); }
	Contact* getContact(int id);
	int getContactIndex(Contact* ct);
	int AddOutFile(std::string fname);

	void SaveToXML(XMLFileOut& file);
	void Validate();

	Simulation();
	~Simulation();

};
class ModelDescribe	//model in generic terms (such as saying this area has a density of 1e15 states
{					//whereas the point model will say, this translates to 100 states in this volume.
public:				//holds all the generic data, which the point model may reference
	std::vector<Layer*> layers;		//holds all the data concerning how everything fits together
	std::vector<LightSource*> spectrum;	//holds the spectrum of light
	std::vector<Material*> materials;	//holds all the properties of the individual materials
	std::vector<Impurity*> impurities;	//holds all the properties of each impurity
	
	//now hold all the simulation variables
	Simulation* simulation;
	

	int NDim;	//number of dimensions

	XSpace Ldimensions;	//dimension of entire device
	XSpace resolution;	//the smallest distance allowed between points

	static const int Error_None = 0;
	static const int Error_NoMaterials = -1;
	static const int Error_NoLayers = -2;
	static const int Error_LayerMaterialNoLinks = -3;

	void ClearAll(void);
	~ModelDescribe();
	ModelDescribe();

	LightSource* getSpectra(int id);
	Impurity* getImpurity(int id);
	Material* getMaterial(int id);
	Contact* getContact(int id);
	Simulation *getSimulation() { return simulation; }

	bool addUniqueImpurity(Impurity* imp);
	bool removeImpurity(Impurity* imp);	//this is the master, so it also deletes the memory

	void SetUnknownIDs();
	int ReassociateAll();	//resets all the pointers based on the ID's, attempts to match everything that is orphaned, and deletes all excess bits.
	void ReassociateContacts(int& refRemoved, int& refAdded, int &refUnknown);
	void Validate();
	bool isIDUsed(int ID);
	int LinkID();
	int GenerateID();
	int CheckModel();

	bool RemoveImpurity(Impurity* imp);
	bool RemoveMaterial(Material* mat);
	bool RemoveContact(Contact *ctc);
	bool RemoveSpectrum(LightSource* spc);

	bool HasImpurity(Impurity* imp);
	bool HasMaterial(Material* mat);
	bool HasContact(Contact *ctc);
	bool HasSpectrum(LightSource* spc);
	bool HasLayer(Layer* lyr);

	void SaveToXML(XMLFileOut& file);

	void UpdateDimensions();
};
class PointOutputs
{
public:
	double* fermi;
	double* fermin;
	double* fermip;
	double* Psi;
	double* Ec;
	double* Ev;
	double* CarCB;
	double* CarVB;
	double* n;
	double* p;
	double* PointImpCharge;
	double* genth;
	double* recth;
	double* R1;
	double* R2;
	double* R3;
	double* R4;
	double* rho;
	double* scatter;
	double* Jpx;	//net hole current in each direction
	double* Jpy;	//net hole current in each direction
	double* Jpz;	//net hole current in each direction
	double* Jnx;	//net electron current in each direction
	double* Jny;	//net electron current in each direction
	double* Jnz;	//net electron current in each direction
	double* CBNRIn;
	double* VBNRIn;
	double* DonNRin;
	double* AcpNRin;
	double* Lgen;
	double* Lsrh;
	double* Ldef;
	double* Lscat;
	double* Erec;
	double* Esrh;
	double* Edef;
	double* Escat;
	double* LPowerEnter;
	double* LightEmit;	//how many photons the light is emitting
	bool DetailedOptical;

	int Deallocate(void);
	int Allocate(unsigned long int num);
	
	PointOutputs();
	~PointOutputs();
};

class ELevelDebugOut   //currently used in outputdebug.cpp
{
public:
	double genth[2];
	double genlight[2];	//number of carriers generated (vb->cb) from this point via light
	double deflight[2];	//number of carriers moved to/from an impurity
	double condlight[2];	//number of carriers which thermally moved higher in their band (cb->cb or vb->vb)
	double recth[2];
	double R1[2];
	double R2[2];
	double R3[2];
	double R4[2];
	double scatter[2];
	XSpace JnP[2];	//transfer of electrons in the positive direction
	XSpace JnN[2];	//transfer of electrons in the negative direction
	XSpace JpP[2];	//transfer of holes in the positive direction
	XSpace JpN[2];	//transfer of holes in the negative direction
	double Lgen[2];
	double Lsrh[2];
	double Ldef[2];
	double Lscat[2];
	double Erec[2];
	double Esrh[2];
	double Edef[2];
	double Escat[2];
	double tunnel[2];
	double contact_extN[2];
	double contact_intN[2];
	double contact_linkN[2];
	double contact_intextN[2];
	double contact_extP[2];
	double contact_intP[2];
	double contact_linkP[2];
	double contact_intextP[2];
	double LightEmit[2];

};

class ESSort	//used for sorting extra states
{
public:
	double val;
	bool ref;

	bool operator <(const ESSort &b) const;
	bool operator >(const ESSort &b) const;
	bool operator <=(const ESSort &b) const;
	bool operator >=(const ESSort &b) const;
	bool operator ==(const ESSort &b) const;
};


class DerivRateIn
{
public:
	double value;
	ELevel* src;
	ELevel* wrt;
	DerivRateIn();
	~DerivRateIn();
};

class GroupThermalizationData
{
	Impurity* imp;
	double minE;	//relative to VB max in point
	double maxE;	//relative to VB max in point
	double fermi;	//what is the fermi energy (in relation to VB max=0)
	double elecSum;
	double holeSum;
	double EfHighSum;	//note inverted
	double EfLowSum;	//note inverted
	unsigned long long int iterUpdate;	//what iteration was the elec/hole last updated
	int typeStates;	//what is the type of the states?
	std::vector<ELevel*> eLevs;	//what energy levels are in this group?
	bool useElecPartition;	//should the partition used be set by the number of electrons or the EfHighLow values (aka, zeroes)
public:
	std::vector<double> elec;	//number of thermalized electrons in eLevs index
	std::vector<double> hole;	//number of thermalized holes in eLevs index
	std::vector<double> EfHighHole;	//probability distribution if Ef is high for holes
	std::vector<double> EfLowElec;	//probability distribution if Ef is low for electrons

	GroupThermalizationData();
	~GroupThermalizationData();
	void Initialize(int type, int thermalAllow, Point* pt, Impurity* srcImp=NULL);	//figure out what the states are
	bool SetNormalize(unsigned long long int inter);	//figure out what the thermalize electron/hole concentrations are
	bool Valid(double minEsrc, double maxEsrc);	//check if this is a valid energy
	bool AssignNormalize(unsigned long long int iter, int which);	//put the normalized values into their particular energy states
	bool CheckType(int type);
	int FindUniqueID(unsigned long long int id, unsigned int guess=-1);
	ELevel* GetELev(unsigned int index);
	double GetESum(void) { return(elecSum); };
	double GetHSum(void) { return(holeSum); };
	double GetEfHighSum(void) { return(EfHighSum); };
	double GetEfLowSum(void) { return(EfLowSum); };
	bool UseElecPart(void) { return(useElecPartition); };
	void Clear();
};

class ChargeError
{
public:
	double cbP, vbP, cbTP, vbTP, donP, acpP, cbN, vbN, cbTN, vbTN, donN, acpN;
	void Reset();
	ChargeError();
	~ChargeError();
};
//poisson solver for just a single point
class Point	//all the information stored at each point
{
private:
	unsigned int iterCtr;
protected:
	std::vector<DerivRateIn> derivs;
	Model& mdl;
	public:
//		iXSpace grid;		//grid point in material - now irrelevant because uniform spacing gone and up3,left5 may not be the same as left5, up3.
		PointAdjSort adj;	//pointers to adjacent points in both position spatially and index for for loops
		//Adjacent adj;		//adjacent grid points. Point will be a vector. Therefore, access is via []	
		XSpace mxmy;		//bottom left corner of device (or left front)
		XSpace position;	//position in material (cm) - center
		XSpace pxpy;		//right top corner of device (or right back)
		//next set of positional data can easily be calculated from the above
		XSpace widthi;	//the inverse width of the point in all dimensions
		XSpace width;
		XSpace area;		//the area of the device used for determining things like current density
		XSpace areai;		//the inverse of area because doing division is much slower than multiplication.
		XSpace EField;	//the overall electric field of the point...
		double vol;		//volume of this particular point
		double voli;	//inverse volume of this particular point
		double sizeScaling;	//used to adjust # of states, optical stuff, etc. Typically 1. Not for actual changes in volume/mxmy, etc.

		//this block also needs to be stored
		//basic characteristics of the point
		double Psi;			//local vacuum level (eV) - Absolute!
		double eg;			//band gap for this particular point
		float eps;					//relative permitivity
		double niCarrier;	//the intrinsic carrier concentration, except multiplied by volume to get the number of carriers.
		double n;			//electron time concentration at this point (never store divided by volume - remove rounding errors). 
		double p;			//hole time concentration at this point (never store divided by volume - remove rounding errors)
		double rho;					//charge density for this point
		double fermi;		//fermi level. Absolute
		double fermin;		//quasi electron fermi level
		double fermip;		//quasi hole fermi level
		double heat;		//excess energy lost that hasn't been picked up by an electron
		double temp;	//the temperature of the point in kelvin
		PointOutputs Output;	//the output of select variables considered to be outputs
		ChargeError cError;

		//now the meat and bones of the point internal workings data		
		Material* MatProps;	//pointer to material properties.
		Band cb;			//conduction band
		double largestOccCB;	//what is the value of the largest number of ocucpied electrons in the conduction band? Used for figuring out whether I should really care about certain rates
		double scatterCBConst;	//1/sum{unocc * exp(-betaE)} --- conduction band
		Band vb;			//valence band
		double largestOccVB;	//how many holes are in the largest number of occupied holes grouping for the valence band. Do I really care about changing 10^-48 carriers in a point that has over 100? over 10^-20? Not really.
		double scatterVBConst;	//1/sum{unocc * exp(-betaE)} --- valence band

		std::vector<Impurity*> impurity;	//impurities present - pointer to generic data of impurity
		double impdefcharge;	//the charge in the point as a result of impurities and defects. Constant after init.
		double largestOccES;	//what is the largest occupancy of electrons in any impruties
		double largestUnOccES;	//and the largest occupancy of holes in any impurities

		std::multimap<MaxMin<double>, ELevel> donorlike;	//impurites and defects that are donorlike
		std::multimap<MaxMin<double>, ELevel> acceptorlike;	//impurites and defects that are donorlike
		std::multimap<MaxMin<double>, ELevel> CBTails;	//band tails in the cb
		std::multimap<MaxMin<double>, ELevel> VBTails;	//band tails in the vb
		double elecindonor;		//current number of electrons in the impurities
		double holeinacceptor;	//current number of holes in the acceptors
		double elecTails;		//electrons in the CB band tail
		double holeTails;		//holes in the VB band tail

		OpticalStorage* OpticalOut;	//mid processing optical data. Finding and adding to rates is taking WAY too much time.
	//	std::map<double, OpticalTransitionData> OpticTransitions;	//don't build the transitions every time! MEMORY HOG - CRASHES SIMULATION
		OpticalTransitionData* OpticTransitions;	//Only store data for one energy, and process all the emissions at that energy, then move on to the next
		
		XSpace mintime;		//the minimum time allowed for a task, as set by thermal velocity and point distance.
		
		PtData* poissonDependency;	//the poisson dependencies needed for solving everything. How all other rho's/BC affect this point
		unsigned long int pDsize;	//the size of the above pointer
		std::vector<PtDataPtr> PsiCf;	//imrpoved psi coefficients... Do all of them. How this point affects every other point -- can probably remove, but it may be convenient in the future - multiplied by charge & volume so only needs number of positive charges
		double largestPsiCf;	//used for stability purposes in steady state calculation

		Contact* contactData;	//pointer to info about its contact data. If not a contact, this is NULL. No delete from here!
		std::vector<angleMatch> AngleData;	//used in simulation for angle data and conserving momentum for 2D and 3D (1D trivial)

		//misc cleared for each iteration
		bool EChangesCalc;	//reset to false for each iteration. Do the following two variables need to be calculated first?
		double BandEnergyChange;	//how much the energy is changed for every positive charge added to this point (1 hole = 1, not 1q)
		double EFieldEChange;	//how much energy is changed via electric field for each positive charge added to this point (1 hole = 1, not 1q)
		double EFieldESqChange;	//this is dependent on the square as well
		std::vector<GroupThermalizationData> ThermData;

		//outputs cleared for each iteration
		LightSource* LightEmissions;	//used to allow reabsorption 
		ELevel* limitsTStep;	//debugging - determine where the timestep is going awry.
		double desireTStep;		//what does this point want the timestep to be?
		XSpace Jn;			//used to determine the net electrons which enter this point CB
		XSpace Jp;			//used to determine the net number of holes which enter this points VB
		double genth;		//number of carriers generated in this point thermally
		double recth;		//number of carriers recombined in this point thermally
		double srhR1;		//Shockley-Reed-Hall Rate 1. Electrons captured from CB
		double srhR2;		//Shockley-Reed-Hall Rate 2. Electrons emitted from state to CB
		double srhR3;		//Shockley-Reed-Hall Rate 3. Holes captured from VB (e- falls)
		double srhR4;		//Shockley-Reed-Hall Rate 4. Hole emitted from state to VB (e- up)
		double scatterR;	//the scatter rate
		double LightPowerEntry;	//the total amount of power that has entered the point
		double LightAbsorbSRH;	//rate from absorbing light between defects/impurities and bands
		double LightAbsorbDef;	//rate from absorbing light and moving between defects/impurities
		double LightAbsorbGen;	//rate from absorbing light and e- VB->CB
		double LightAbsorbScat;	//rate from absorbing light and carriers becoming more energetic 
		double LightEmitSRH;	//rate from emitting light and electrons dropping in energy (due to light)
		double LightEmitDef;	//rate from electrons falling to from impurities to lower impurities (due to light)
		double LightEmitRec;	//rate from carriers recombining and generating light (due to light)
		double LightEmitScat;	//rate from carriers getting lower in energy in band (due to light)
		double LightEmission;	//rate from electrons dropping in energy - NOT due to light
		double netCharge;	//how much charge (not multiplied by q) is in the point
		double chargeRate;	//the rate of charge change for this point
		double CBNRin;	//the net rate in carriers/sec of electrons entering this CB
		double VBNRin;	//the net rate in carriers/sec of electrons entering this VB
		double DonNRin;//the net rate in carriers/sec of electrons entering the donors
		double AcpNRin;//the net rate in carriers/sec of electrons entering the acceptors
	
		
		int PopulateAngleData2D(Model& mdl);
		
		ELevel* FindEnergy(double energy);
		int FindELevels(double energyMin, double energyMax, std::vector<ELevel*>& eLevs);
		int ResetFermiEnergies(bool RecalcELevels, int occtype, double accuracy, bool CalcPoint);
		int ResetData();	//looks at the values in the energy levels and resets all the points appropriately
		int CalcEField();
		double DeterminePsiEdge(int direction);
		int Clear();	//clears out all the variables so it is fresh.
		int Thermalize(unsigned long long int iter, int which=RATE_OCC_BEGIN);
		int ThermalizeBand(bool band);
		int ThermalizeAllES(void);
		int ThermalizeES(int which);
		int TransferEndToBegin(void);
		int TransferBeginToEnd(void);
		int FindThermalizedELevel(unsigned int& thermIndex, unsigned int& eLevIndex, ELevel* eLev);
		double GetDeriv(ELevel* src, ELevel* wrt);
		double AddDeriv(ELevel* src, ELevel* wrt, double val);
		int RemoveDeriv(ELevel* src, ELevel* wrt);
		int ClearDerivs(void) { derivs.clear(); return(0); };
		int SetLocalVacuum(double newpsi, bool updateFermi=false);
		bool IsContactSink(void);
//		double tau;			//mean scattering time of an electron.
					//used to help maintain conservation of energy
		~Point();	//destructor
		Point(Model& md);

		Model& getModel();
		ModelDescribe& getMdesc();
		Simulation* getSim();


		int Populate(int vacID, unsigned int numinbin, double beta);
		void InitTransIteration();

		void UpdateELevelFermi(void);	//updates the probabilities for each energy level in this point
		int PointDefects(Impurity* imp);
		int PointImpurities(Layer& lyr);
		double GetOverlapArea(Point* target);
		void ApplyRates();
};

class angleMatch
{
public:
	Point* targetPt;
	double theta[4];	//in 2D, only use theta[0,1]
	double sin01;		//in 2D, area of triange depends on sin of angle between theta[0] and [1]
	double cos01;		//in 2D, determining the position on the momentum circle requires cos angle theta[0] and [1]
	//just saving MANY cos() and sin() evaluations. Unfortunately, the others will change as momentum circle changes...
	double phi[4];	//prep for 3D
	double proportion;	//2D, theta1-theta0/2PI (normalized angle difference), 3D, normalized surface area of cone end
	angleMatch();
};

//contact class needs a bit of an overhaul...
//Have a bunch of contact classes in the overall model
//then link pointers to they're particular contact
//have the contact keep a record of all the points
//do NOT give the ability for contacts to link...
//otherwise, it would force the model into doing something it might
//not otherwise do.


class BoundaryCondition
{
public:
	Point* pt;			//the point this boundary condition applies to
	unsigned long int ID;	//an ID used for when solving poisson's equation. Same as the sort value
	int type;		//is this a voltage value, electric field w/in device, or electric field entering device?
	double value;	//the actual value which will be needed in doing the band calculation
	int ProcessBC(Simulation* sim, double ForceValue=0.0, bool force=false);
	double MinEField(Simulation* sim);
	double OppEField(Simulation* sim);
};

class Model	//describes the model generically on a point by point basis
{
	ModelDescribe& mdesc;
public:
	std::map<long int, Point> gridp;	//the data is now stored as a map
	//future: add a vector of all energy states. Easy iterations through everything. cached. It would be beautiful.
	std::map<long int, int> MergeSplitPoints;	//second is groupings of points needing to be merged (positive merge, negative split), first is which points need merging
	std::vector<long int> boundaries;			//points that are adjacent to the external environment
//	Point* ptZero;	//just a pointer to the first point as an initialization aid - NO LONGER NEEDED WITH stdMap
	double* poissonRho;	//rho or the boundary condition for each point - faster solve if these are all sequential
	std::map<unsigned long int, BoundaryCondition> poissonBC;	//the boundary condition values. Sort value
	bool PoissonDependencyCalculated;	//used to prevent recalculating the same solution over and over again...
	bool disableTunneling;	//should tunneling be disabled?
	long unsigned int numpoints;
	unsigned long long int uniqueIDctr;	//used as a unique identifier for energy states. Every single assignment should be as uniqueIDctr++
	XSpace width;		//average dimensions of each cube in material (cm)
	int NDim;	//number of dimensions of the deivce
	Model(ModelDescribe& md);
	Model(ModelDescribe& md, long unsigned int size);
	ModelDescribe& getMDesc();
	Simulation* getSim();

	Point* getPoint(long int index);
	~Model();
};


#endif