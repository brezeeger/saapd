#ifndef SAVESTATE_H
#define SAVESTATE_H

#include <fstream>
#include <map>
#include <vector>

class Model;
class ModelDescribe;
class Material;
class Optical;
class Layer;
class LightSource;
class Impurity;
class Simulation;
class Contact;
class Point;
class Band;
class ELevel;

template <typename T>
class MaxMin;

int SaveState(Model& mdl, ModelDescribe& mdesc);
int WriteMatlData(std::ofstream& file, Material* mat);
int WriteOpticalData(std::ofstream& file, Optical* opt);
int WriteLayerData(std::ofstream& file, Layer* lay);
int WriteSpectrumData(std::ofstream& file, std::vector<LightSource*>& spec);
//int WriteImpurityData(std::ofstream& file, Impurity* imp);
int WriteSimulationData(std::ofstream& file, Simulation* sim);
int WriteContactData(std::ofstream& file, Contact* ct);
//int Contact::SavePrivateData(std::ofstream& file);
int WriteMDescData(std::ofstream& file, ModelDescribe& mdesc);
int WritePointData(std::ofstream& file, Point& pt);
int WriteBand(std::ofstream& file, Band& band);
int WriteEState(std::ofstream& file, ELevel& node);
int WriteImpStates(std::ofstream& file, std::multimap<MaxMin<double>,ELevel>& states);
int WriteModelData(std::ofstream& file, Model& mdl);




#endif