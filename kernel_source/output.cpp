#include "output.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cstring>
#include "model.h"
#include "physics.h"
#include "helperfunctions.h"
#include "transient.h"
#include "contacts.h"
#include "light.h"
#include "outputdebug.h"
#include "ss.h"

int OutputPointOpticalDetailedInit(Simulation* sim, Point& pt)
{
	if(pt.Output.DetailedOptical == false)
		return(-1);

	if(sim == NULL)
		return(-1);

	std::string loadfile = sim->OutputFName;
	std::ofstream file;
	std::stringstream filename;
	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_";

	unsigned long useIter = sim->curiter;

	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
	{
		if (sim->binctr != sim->outputs.numinbin - 1)
			return(-1);

		if(sim->TransientData->simtime < sim->TransientData->timeignore)
			return(-1);

		if(useIter < 1e9)
			filename << "0";
		if(useIter < 1e8)
			filename << "0";
		if(useIter < 1e7)
			filename << "0";
		if(useIter < 1e6)
			filename << "0";
		if(useIter < 1e5)
			filename << "0";
		if(useIter < 1e4)
			filename << "0";
		if(useIter < 1e3)
			filename << "0";
		if(useIter < 1e2)
			filename << "0";
		if(useIter < 1e1)
			filename << "0";
		filename << useIter;
		filename << "_Pt" << pt.adj.self << ".ptopt";
	}
	else if(sim->SteadyStateData && sim->simType==SIMTYPE_SS)
	{
		filename << sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut;
		filename << "_Pt" << pt.adj.self << ".ptopt";
		
	}
	
	
	
	
	

	
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	sim->AddOutFile(filename.str());

	//what we want -- ID, Energy level absolute min/max/width, num states, occupancy, absFermiLevel
	double absmin, absmax, width;
	
	if (sim->TransientData && sim->simType == SIMTYPE_TRANSIENT)
		file << sim->TransientData->iterEndtime << std::endl;
	else if (sim->SteadyStateData && sim->simType == SIMTYPE_SS)
		file << sim->SteadyStateData->CurrentData << std::endl;
	file << std::setw(10) << std::right << "Type" << " ";
	file << std::setw(10) << std::right << "State_ID" << " ";
	file << std::setw(16) << std::right << "E_Min" << " ";
	file << std::setw(16) << std::right << "E_Max" << " ";
	file << std::setw(20) << std::right << "E_Width" << " ";
	file << std::setw(16) << std::right << "NumStates" << " ";
	file << std::setw(16) << std::right << "OccStates" << " ";
	file << std::setw(16) << std::right << "AbsFermi" << std::endl;

	for(std::map<MaxMin<double>, ELevel>::reverse_iterator it = pt.cb.energies.rbegin(); it != pt.cb.energies.rend(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "CB" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.CBTails.begin(); it != pt.CBTails.end(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "CBTail" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.donorlike.begin(); it != pt.donorlike.end(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "Donor" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}
	for(std::map<MaxMin<double>, ELevel>::reverse_iterator it = pt.acceptorlike.rbegin(); it != pt.acceptorlike.rend(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "Acceptor" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}
	for(std::map<MaxMin<double>, ELevel>::reverse_iterator it = pt.VBTails.rbegin(); it != pt.VBTails.rend(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "VBTail" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.vb.energies.begin(); it != pt.vb.energies.end(); ++it)
	{
		it->second.CalculateAbsoluteHighLowEnergy(absmax, absmin);
		width = it->second.max - it->second.min;
		file << std::setw(10) << std::right << "VB" << " ";
		file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << it->second.uniqueID << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmin << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << absmax << " ";
		file << std::setw(20) << std::right << std::scientific << std::setprecision(10) << width << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetNumstates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.GetBeginOccStates() << " ";
		file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << it->second.AbsFermiEnergy << std::endl;
	}

	//want SrcID->TrgID, type, minDist, maxDist, netRate
	file << std::endl;
	file << std::setw(10) << std::right << "SrcID" << " ";	//first subheader
	file << std::setw(10) << std::right << "TrgID" << " ";
	file << std::setw(16) << std::right << "Type" << " ";
	file << std::setw(16) << std::right << "Min_eV_Dist" << " ";
	file << std::setw(20) << std::right << "Max_eV_Dist" << " ";
	file << std::setw(16) << std::right << "Rate" << std::endl;
	file << std::setw(38) << " " << " ";
	file << std::setw(16) << std::right << "eV" << " ";	//second subheader
	file << std::setw(20) << std::right << "PhotonFlux" << std::endl;


	file.close();
	return(0);
}

int OutputPointOpticalDetailedELev(Simulation* sim, ELevelRate& rate)
{
	if(sim == NULL || rate.source == NULL || rate.target == NULL)
		return(-1);
	Point* pt = &rate.source->getPoint();
	if(pt == NULL)
		return(-1);

	if(pt->Output.DetailedOptical == false)
		return(-1);

	std::string loadfile = sim->OutputFName;
	std::ofstream file;
	std::stringstream filename;
	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_";

	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
	{
		if (sim->TransientData->simtime < sim->TransientData->timeignore)
			return(-1);

		if (sim->binctr != sim->outputs.numinbin - 1)
			return(-1);

		if(sim->curiter < 1e9)
			filename << "0";
		if(sim->curiter < 1e8)
			filename << "0";
		if(sim->curiter < 1e7)
			filename << "0";
		if(sim->curiter < 1e6)
			filename << "0";
		if(sim->curiter < 1e5)
			filename << "0";
		if(sim->curiter < 1e4)
			filename << "0";
		if(sim->curiter < 1e3)
			filename << "0";
		if(sim->curiter < 1e2)
			filename << "0";
		if(sim->curiter < 1e1)
			filename << "0";

		filename << sim->curiter;
		filename << "_Pt" << pt->adj.self << ".ptopt";
	}
	else if(sim->SteadyStateData && sim->simType==SIMTYPE_SS)
	{
		filename << sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut;
		filename << "_Pt" << pt->adj.self << ".ptopt";
		
	}

	
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out|std::fstream::app);
	if(file.is_open() == false)
		return(-1);

	sim->AddOutFile(filename.str());

	file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << rate.source->uniqueID << " ";
	file << std::setw(10) << std::right << std::fixed << std::setprecision(0) << rate.target->uniqueID << " ";
	file << std::setw(16) << std::right << std::fixed << std::setprecision(0) << rate.type << " ";
	double srcmin, srcmax, trgmin, trgmax;
	rate.source->CalculateAbsoluteHighLowEnergy(srcmax, srcmin);
	rate.target->CalculateAbsoluteHighLowEnergy(trgmax, trgmin);
	if(srcmin < trgmax)	//these are reversed, potentially from SRH3/4
	{
		double tmp;
		tmp = srcmax;
		srcmax = trgmax;
		trgmax = tmp;

		tmp = srcmin;
		srcmin = trgmin;
		trgmin = tmp;
	}
	file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << srcmin - trgmax << " ";
	file << std::setw(20) << std::right << std::scientific << std::setprecision(6) << srcmax - trgmin << " ";
	file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << rate.RateType << std::endl;


	file.close();
	return(0);
}

int OutputPointOpticalDetailedSplit(Simulation* sim, long int ptID, double photonEnergy, double numPhotons)
{
	if(ptID < 0)	//sent if the point does not want detailed optical output
		return(-1);

	if(sim == NULL || photonEnergy <= 0.0 || numPhotons <= 0.0)
		return(-1);

	std::string loadfile = sim->OutputFName;
	std::ofstream file;
	std::stringstream filename;
	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_";

	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
	{
		if (sim->TransientData->simtime < sim->TransientData->timeignore)
			return(-1);

		if (sim->binctr != sim->outputs.numinbin - 1)
			return(-1);



		unsigned long useIter = sim->curiter;

		if(useIter < 1e9)
			filename << "0";
		if(useIter < 1e8)
			filename << "0";
		if(useIter < 1e7)
			filename << "0";
		if(useIter < 1e6)
			filename << "0";
		if(useIter < 1e5)
			filename << "0";
		if(useIter < 1e4)
			filename << "0";
		if(useIter < 1e3)
			filename << "0";
		if(useIter < 1e2)
			filename << "0";
		if(useIter < 1e1)
			filename << "0";

		filename << useIter;
	}
	else if(sim->SteadyStateData && sim->simType==SIMTYPE_SS)
	{
		filename << sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut;
		
	}

	filename << "_Pt" << ptID << ".ptopt";
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out|std::fstream::app);
	if(file.is_open() == false)
		return(-1);

	sim->AddOutFile(filename.str());

	file << std::setw(38) << " " << " ";
	file << std::setw(16) << std::right << std::setprecision(6) << photonEnergy << " ";
	file << std::setw(20) << std::right << std::setprecision(10) << numPhotons << std::endl;

	file.close();
	return(0);
}


int OutputCharge(Simulation* sim, bool initialize)
{
	if(sim->TransientData==NULL)
		return(0);
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << ".charge";
	file.clear();
	if(initialize)
	{
		file.open(filename.str().c_str(), std::fstream::out);
	
		if(file.is_open() == false)
			return(-1);

		file << std::setw(11) << std::left << "Iteration";
//		file << std::setw(16) << std::left << "Iter_End_Time";
		file << std::setw(25) << std::left << "Pos_Charge";
		file << std::setw(25) << std::left << "Neg_Charge";
		file << std::setw(16) << std::left << "Net_Charge";

		file << "Volts-->";
		unsigned int csize = sim->contacts.size();
		for(unsigned int i=0; i<csize; i++)
		{
			file << " | " << std::setw(16) << std::left << sim->contacts[i]->name;	//if there's 1,000 contacts, dang...
		}
		file << std::endl;
		file.close();

		return(0);
	}

	file.open(filename.str().c_str(), std::fstream::out|std::fstream::app);	//open the file for appending.
	
	if(file.is_open() == false)
		return(-1);

	file << std::setw(11) << std::left << std::fixed << sim->curiter;
//	file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << sim->TransientData->iterEndtime;
	file << std::setw(25) << std::left << std::scientific << std::setprecision(15) << sim->posCharge;
	file << std::setw(25) << std::left << std::scientific << std::setprecision(15) << sim->negCharge;
	file << std::setw(24) << std::left << std::scientific << std::setprecision(15) << std::showpos << sim->posCharge + sim->negCharge;
	file << std::noshowpos;	//cancel this out for the rest of the file...
	unsigned int csize = sim->contacts.size();

	for(unsigned int ctc = 0; ctc < csize; ctc++)
	{

		file << " | ";
		file << std::setw(16) << std::left << std::scientific << std::setprecision(6) << sim->contacts[ctc]->GetVoltage();
		//now that the data has been recorded, clear it out for the next round...	
	}
	file << std::endl;


	file.close();
	return(0);
}

int OutputContacts(Simulation* sim, bool initialize)
{
	std::ofstream file;
	std::stringstream filename;

	if(sim->simType == SIMTYPE_SS)
		return(0);	//there is no contact output data that is really relevant unless it's at the very end of the iteration

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << ".ctc";
	file.clear();
	if(initialize)
	{
		file.open(filename.str().c_str(), std::fstream::out);
	
		if(file.is_open() == false)
			return(-1);

		file << std::setw(12) << std::right << "Iteration";
		file << std::setw(16) << std::right << "Iter_End_Time";
		file << std::setw(18) << std::right << "Time_Difference";


		unsigned int csize = sim->contacts.size();
		for(unsigned int i=0; i<csize; i++)
		{
			if(sim->contacts[i]->point.size() == 0)
				continue;
//			filename.clear();	//not really filename, but just use the same stream as temp
//			filename.str("");
//			filename << "Contact_" << sim->contacts[i]->id;	//make sure the entire string is 16 characters
			file << " | " << std::setw(16) << std::right << sim->contacts[i]->name;	//if there's 1,000 contacts, dang...
			file << std::setw(16) << std::right << "Voltage";
//			file << std::setw(16) << std::right << "Net_Holes_Enter";
			if(sim->contacts[i]->GetCurrentActive() & CONTACT_SINK)
			{
				file << std::setw(16) << std::right << "Jp";
	//			file << std::setw(16) << std::right << "Net_Elect_Enter";
				file << std::setw(16) << std::right << "Jn";
			}
			else
			{
				file << std::setw(16) << std::right << "Jp_Enter";
				file << std::setw(16) << std::right << "Jn_Enter";
			}
		}
		file << std::endl;
		file.close();

		return(0);
	}

	file.open(filename.str().c_str(), std::fstream::out|std::fstream::app);	//open the file for appending.
	
	if(file.is_open() == false)
		return(-1);

	file << std::setw(12) << std::right << std::fixed << sim->curiter;
	file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << sim->TransientData->iterEndtime;
	file << std::setw(18) << std::right << std::scientific << std::setprecision(8) << 1.0 / sim->TransientData->storevalueStartTime;	//this function is called in output to file
	//which takes the storevalueStartTime, does iterend - store to get net, and then inverses it for normalization purposes

	unsigned int csize = sim->contacts.size();

	for(unsigned int ctc = 0; ctc < csize; ctc++)
	{
		if(sim->contacts[ctc]->point.size() == 0)
			continue;

		double voltage = -sim->contacts[ctc]->point[0]->fermi;
		file << " | " << std::setw(16) << " ";	//12 blank spaces where it says the contact name
		file << std::setw(16) << std::right << std::scientific << std::setprecision(7) << voltage;// sim->contacts[ctc]->GetVoltage();
//		file << std::setw(16) << std::right << std::scientific << std::setprecision(7) << sim->contacts[ctc]->netholetime;
//		if(sim->contacts[ctc]->GetCurrentActive() & CONTACT_SINK)
//		{
//			sim->contacts[ctc]->netholetime = sim->contacts[ctc]->netholetime * sim->storevalueStartTime;	//get the averaging correct. These were already in terms of Jn and Jp exactly
//			sim->contacts[ctc]->netelectrontime = sim->contacts[ctc]->netelectrontime * sim->storevalueStartTime;
//		}
//		else
//		{
		sim->contacts[ctc]->netholetime = sim->contacts[ctc]->netholetime * sim->TransientData->storevalueStartTime * QCHARGE * sim->contacts[ctc]->point[0]->areai.x;
		sim->contacts[ctc]->netelectrontime = sim->contacts[ctc]->netelectrontime * sim->TransientData->storevalueStartTime * QCHARGE * sim->contacts[ctc]->point[0]->areai.x;
//		}
		
		file << std::setw(16) << std::right << std::scientific << std::setprecision(7) << sim->contacts[ctc]->netholetime;
//		file << std::setw(16) << std::right << std::scientific << std::setprecision(7) << sim->contacts[ctc]->netelectrontime;
		
		file << std::setw(16) << std::right << std::scientific << std::setprecision(7) << sim->contacts[ctc]->netelectrontime;
		//now that the data has been recorded, clear it out for the next round...
		sim->contacts[ctc]->netelectrontime = 0.0;
		sim->contacts[ctc]->netholetime = 0.0;
		sim->contacts[ctc]->holetime.clear();
		sim->contacts[ctc]->electrontime.clear();
			
	}
	file << std::endl;


	file.close();
	return(0);
}


int FinalOutputQEDataSS(ModelDescribe& mdesc)
{
	
	if(mdesc.simulation->outputs.Lgen == false
		&& mdesc.simulation->outputs.Ldef == false
		&& mdesc.simulation->outputs.Lscat == false
		&& mdesc.simulation->outputs.Lsrh == false
		&& mdesc.simulation->outputs.Lpow == false
		&& mdesc.simulation->outputs.Edef == false
		&& mdesc.simulation->outputs.Erec == false
		&& mdesc.simulation->outputs.Escat == false
		&& mdesc.simulation->outputs.Esrh == false
		&& mdesc.simulation->outputs.LightEmission == false)
		return(0);

	if(mdesc.simulation->OpticalAggregateOut==NULL)	//there is no data...
		return(0);


	if(mdesc.simulation->OpticalAggregateOut->LightIncident.size() == 0 
		&& mdesc.simulation->OpticalAggregateOut->GeneratedEg.size() == 0
		&& mdesc.simulation->OpticalAggregateOut->GeneratedSRH.size() == 0)	//no light ever hit/emitted, don't bother w/ useless output file
		return(0);

	Simulation* sim = mdesc.simulation;	//just make code cleaner to read...
	
	std::string wherefrom;

	if(sim->SteadyStateData)
	{
		wherefrom = sim->OutputFName
			+ "_" + std::to_string(sim->SteadyStateData->CurrentData)
			+ "_" + sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut;

	}
	else
	{
		wherefrom = sim->OutputFName
			+ "_" + std::to_string(sim->curiter);

		
	}


	
	//get contact data for calculating the QE
	/*
	double carOut= 0.0;
	unsigned int csize = sim->contacts.size();
	for(unsigned int ctc = 0; ctc < csize; ctc++)
	{
		carOut += sim->contacts[ctc]->netelectrontime;	//how many electrons entered the contact from an external source
		carOut += sim->contacts[ctc]->netholetime;	//how many holes entered the contact from an external source
	}

	carOut = carOut * 50.0;	//only really want to count all electrons or all holes, not both (double count current), but get it in
	//percentages, so /2 * 100 = *50 --- there may be more than two contacts, but the only relevant current is going to be from the two

	double incPhotons = MapSum(sim->OpticalAggregateOut->LightIncident);
	double EntryPhotons = MapSum(sim->OpticalAggregateOut->LightEnter);
//	double LeavePhotons = incPhotons - sim->OpticalAggregateOut.LightLeave.SumData(); unused

	double EQE = 0.0;
	double IQE = 0.0;
	*/

	/*
	file << "External QE: ";
	if(incPhotons > 0.0 && carOut >= 0.0)
	{
		EQE =  carOut / incPhotons;
		file << std::fixed << std::setprecision(8) << EQE << "%" << std::endl;
	}
	else
		file << "0.0%" << std::endl;

	file << "Internal QE: ";
	if(EntryPhotons > 0.0 && carOut >= 0.0)
	{
		IQE = carOut / EntryPhotons;
		file << std::fixed << std::setprecision(8) << IQE << "%" << std::endl;
	}
	else
		file << "0.0%" << std::endl;
*/

	if(sim->misc.largestMatNameSize <= 0)
	{
		sim->misc.largestMatNameSize = 22;	//"Stimulated Emission Eg" = 22 (+1)
		for(unsigned int i=0; i<mdesc.materials.size(); i++)
		{
			if(mdesc.materials[i]->name.size() > sim->misc.largestMatNameSize)
				sim->misc.largestMatNameSize = mdesc.materials[i]->name.size();
		}
		sim->misc.largestMatNameSize++;
	}


	//photons incident should cover every possible wavelength used in free carrier, defect, etc.
	OutputQERows(wherefrom+"_All", sim->OpticalAggregateOut, mdesc, 1.0);
	sim->OpticalAggregateOut->ClearAll();

	

	for(unsigned int i=0; i<mdesc.materials.size(); i++)
	{
		if (mdesc.materials[i]->type == VACUUM || mdesc.materials[i]->optics->suppressOpticalOutputs)
		{
			if (mdesc.materials[i]->OpticalAggregateOut)
				mdesc.materials[i]->OpticalAggregateOut->ClearAll();
			continue;
		}

		if(sim->SteadyStateData)
		{
			if(mdesc.materials[i]->name != "")
				wherefrom = sim->OutputFName
					+ "_" + std::to_string(sim->SteadyStateData->CurrentData)
					+ "_" + sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut
					+ "_" + mdesc.materials[i]->name;
			else
				wherefrom = wherefrom = sim->OutputFName 
					+ "_" + std::to_string(sim->SteadyStateData->CurrentData)
					+ "_" + sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut
					+ "_" + std::to_string(mdesc.materials[i]->id);

		}
		else
			wherefrom = sim->OutputFName + "_" + std::to_string(sim->curiter);


		OutputQERows(wherefrom, mdesc.materials[i]->OpticalAggregateOut, mdesc, 1.0);
		mdesc.materials[i]->OpticalAggregateOut->ClearAll();

	}
	
	
	//now all the light incident for this particular group has been done, and we don't want that screwing up the future ones
	//so all light incident has to be removed. Easy way - clear the memory.
	return(0);
}

int FinalOutputQEDataTransient(ModelDescribe& mdesc)
{
	
	if(mdesc.simulation->outputs.Lgen == false
		&& mdesc.simulation->outputs.Ldef == false
		&& mdesc.simulation->outputs.Lscat == false
		&& mdesc.simulation->outputs.Lsrh == false
		&& mdesc.simulation->outputs.Lpow == false
		&& mdesc.simulation->outputs.Edef == false
		&& mdesc.simulation->outputs.Erec == false
		&& mdesc.simulation->outputs.Escat == false
		&& mdesc.simulation->outputs.Esrh == false
		&& mdesc.simulation->outputs.LightEmission == false)
		return(0);

	if(mdesc.simulation->OpticalAggregateOut->LightIncident.size() == 0
		&& mdesc.simulation->OpticalAggregateOut->GeneratedEg.size() == 0
		&& mdesc.simulation->OpticalAggregateOut->GeneratedSRH.size() == 0)	//no light ever hit/emitted, don't bother w/ useless output file
		return(0);

	Simulation* sim = mdesc.simulation;	//just make code cleaner to read...
	std::ofstream file;
	std::stringstream filename;

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_";
	
	if(sim->curiter < 1e9)
		filename << "0";
	if(sim->curiter < 1e8)
		filename << "0";
	if(sim->curiter < 1e7)
		filename << "0";
	if(sim->curiter < 1e6)
		filename << "0";
	if(sim->curiter < 1e5)
		filename << "0";
	if(sim->curiter < 1e4)
		filename << "0";
	if(sim->curiter < 1e3)
		filename << "0";
	if(sim->curiter < 1e2)
		filename << "0";
	if(sim->curiter < 1e1)
		filename << "0";

	filename << sim->curiter << "_Optic";

	double tStep = 0.0;
	double tStepi = 1.0;

	for (unsigned int i = 0; i<sim->TransientData->timesteps.size(); i++)
		tStep += sim->TransientData->timesteps[i];

	if(tStep > 0.0)
		tStepi = 1.0 / tStep;

		/*
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);
	file << "Data from: " << sim->OutputFName << std::endl;
	file << "Data Duration: " << tStep << " seconds starting on " << sim->TransientData->simtime << std::endl;
	
	
	//get contact data for calculating the QE
	double carOut= 0.0;
	unsigned int csize = sim->contacts.size();
	for(unsigned int ctc = 0; ctc < csize; ctc++)
	{
		carOut += sim->contacts[ctc]->netelectrontime;	//how many electrons entered the contact from an external source
		carOut += sim->contacts[ctc]->netholetime;	//how many holes entered the contact from an external source
	}

	carOut = carOut * 50.0;	//only really want to count all electrons or all holes, not both (double count current), but get it in
	//percentages, so /2 * 100 = *50 --- there may be more than two contacts, but the only relevant current is going to be from the two

	double incPhotons = MapSum(sim->OpticalAggregateOut->LightIncident);
	double EntryPhotons = MapSum(sim->OpticalAggregateOut->LightEnter);
//	double LeavePhotons = incPhotons - sim->OpticalAggregateOut.LightLeave.SumData(); unused

	double EQE = 0.0;
	double IQE = 0.0;

	file << "External QE: ";
	if(incPhotons > 0.0 && carOut >= 0.0)
	{
		EQE =  carOut / incPhotons;
		file << std::fixed << std::setprecision(8) << EQE << "%" << std::endl;
	}
	else
		file << "0.0%" << std::endl;

	file << "Internal QE: ";
	if(EntryPhotons > 0.0 && carOut >= 0.0)
	{
		IQE = carOut / EntryPhotons;
		file << std::fixed << std::setprecision(8) << IQE << "%" << std::endl;
	}
	else
		file << "0.0%" << std::endl;
		*/
	if(sim->misc.largestMatNameSize <= 0)
	{
		sim->misc.largestMatNameSize = 22;	//"Stimulated Emission Eg" = 22 (+1)
		for(unsigned int i=0; i<mdesc.materials.size(); i++)
		{
			if(mdesc.materials[i]->name.size() > sim->misc.largestMatNameSize)
				sim->misc.largestMatNameSize = mdesc.materials[i]->name.size();
		}
		sim->misc.largestMatNameSize++;
	}

	/*
	file << std::setw(sim->misc.largestMatNameSize) << std::left << "Name";
	file << std::setw(16) << std::right << "Wavelength(nm)";
	file << std::setw(16) << std::right << "Energy(eV)";
	file << std::setw(16) << std::right << "Photons";
	file << std::setw(16) << std::right << "Photon_Flux";
	file << std::setw(16) << std::right << "Power(mW)";	//percent absorbed in device among light incident
	file << std::setw(19) << std::right << "Intensity(mW/cm^2)" << std::endl;	//how much light turned into a free carrier
	
	file << "Entire Device" << std::endl;
	*/
	//photons incident should cover every possible wavelength used in free carrier, defect, etc.
	OutputQERows(filename.str(), sim->OpticalAggregateOut, mdesc, tStepi);
	sim->OpticalAggregateOut->ClearAll();
	for(unsigned int i=0; i<mdesc.materials.size(); i++)
	{
		if(mdesc.materials[i]->type == VACUUM || mdesc.materials[i]->optics->suppressOpticalOutputs)
			continue;
//		file << mdesc.materials[i]->name << std::endl;
		OutputQERows(filename.str() + mdesc.materials[i]->name, mdesc.materials[i]->OpticalAggregateOut, mdesc, tStepi);
		mdesc.materials[i]->OpticalAggregateOut->ClearAll();
	}
//	file.close();
	
	//now all the light incident for this particular group has been done, and we don't want that screwing up the future ones
	//so all light incident has to be removed. Easy way - clear the memory.
	return(0);
}

int OutputQERowSubsection(std::string fileprepend, std::string name, std::map<double, double>& data, int firstcolSize, double tStepi, double areai, double binWidth, Simulation* sim)
{
	double flux;
	double power;
	double intensity;
	double totPhoton=0.0;
	double totFlux=0.0;
	double totPower=0.0;
	double totIntensity=0.0;
	double wavelength;
	bool printblankforName = false;
	std::string fname = fileprepend + "_" + name + ".qe";

	if (data.size() == 1) //want to have multiple data points to plot! SAAPD doesn't show one point plots
	{
		power = data.begin()->first;	//the energy (eV) -- ignore variable name power. Just a temp variable
		data.insert(std::pair<double, double>(power - binWidth, 0.0));
		data.insert(std::pair<double, double>(power + binWidth, 0.0));
	}
	else if (data.size() == 0)
	{
		power = sim->EnabledRates.OpticalEMin;	//temp min value
		flux = sim->EnabledRates.OpticalEMax;	//temp max value
		if (power < 0.0)	//min energy can't be <0
			power = 0.0;
		if (flux <= power)	//max energy can't be less <= min if going to plot
			flux = power + 5.0;

		data.insert(std::pair<double, double>(power, 0.0));
		data.insert(std::pair<double, double>(flux, 0.0));
	}

	for(std::map<double,double>::iterator it = data.begin(); it != data.end(); ++it)
	{
		if(it->second == 0.0)	//don't print out a bunch of zeros!
			continue;

		flux = it->second * tStepi;
//		energy = PHOTONENERGYCONST / double(wavelength[i]);	//  eV*nm / nm
		power = it->first * flux * QCHARGE * 1000;	//eV * phot/sec * J/eV = J/s = W * mW/W = mW
		intensity = power * areai;	//mW/cm^2

		totPhoton += it->second;
		totFlux += flux;
		totPower += power;
		totIntensity += intensity;
	}
//	if(totPhoton == 0.0)	//nothing happened. Don't output a file
//		return(0);
	
	std::ofstream file;
	file.clear();
	file.open(fname.c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	if(sim)
		sim->AddOutFile(fname);

	

	file << "Optical Data: " << fileprepend << std::endl;
	file << name << std::endl << std::endl;

	//	double flux = ray->powerLeft * 0.001 * QI / ray->waveData->energyphoton;
	
	double prev = 0.0;
	double binWidthT = 4.0 * binWidth;	//if the binning is really fine, there will be lot sof zeroes.
	/*
	//this is a first order methodology of determining if the lack of data is due to sucky bin widths.
	//if there is a lot of space between them, put in some zeroes,
	
	Want to avoid something like:
	The data is for discrete energies, but the jumps in those energies may not be representative
	of the bin width
	0.00001		1.5
	0.00002		0.0
	0.00003		0.0
	0.00004		0.0
	0.00005		0.0
	0.00006		1.3
	0.00007		0.0
	0.00008		0.0
	0.00009		0.0
	0.00010		0.9
	0.00011		0.0
	0.00012		0.0
	0.00013		0.7
	0.00014		0.0
	0.00015		0.0
	0.00016		0.0
	0.00017		0.2
	0.00018		0.0
	0.00019		0.0

	would much rather have - where the curve is semi continuous as is more likely to match the real world
	0.00001		1.5
	0.00006		1.3
	0.00010		0.9
	0.00013		0.7
	0.00017		0.2
	*/
	

	//first get the totals on the top row. Easy read output that doesn't interfere with loading data into spreadsheets


	file << std::setw(32) << std::left << "Totals:";	//this will be treated as a header line
			
	PrepOutputDoubleToFile(totPhoton, file);
	file << std::setw(16) << std::right << totPhoton;

	PrepOutputDoubleToFile(totFlux, file);
	file << std::setw(16) << std::right << totFlux;

	PrepOutputDoubleToFile(totPower, file);
	file << std::setw(16) << std::right << totPower;

	PrepOutputDoubleToFile(totIntensity, file);
	file << std::setw(18) << std::right << totIntensity << std::endl << std::endl;	//add a space to show next section more clearly

	
	file << std::setw(16) << std::right << "Wavelength(nm)";
	file << std::setw(16) << std::right << "Energy(eV)";
	file << std::setw(16) << std::right << "Photons";
	file << std::setw(16) << std::right << "Photon_Flux";
	file << std::setw(16) << std::right << "Power(mW)";	//percent absorbed in device among light incident
	file << std::setw(19) << std::right << "Intensity(mW/cm^2)" << std::endl;	//how much light turned into a free carrier


	for(std::map<double,double>::iterator it = data.begin(); it != data.end(); ++it)
	{
		if(it->second == 0.0 && data.size() > 3)	//don't print out a bunch of zeros!
			continue;
		if(it->first - prev > binWidthT && prev != 0.0)	//need the plots to know it goes to zero between the data!
		{
			//need to add two rows of zeros
			file << std::setw(16) << std::right << PHOTONENERGYCONST / (prev+binWidth);
			file << std::setw(16) << std::right << prev+binWidth;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(19) << std::right << 0.0 << std::endl;

			file << std::setw(16) << std::right << PHOTONENERGYCONST / (it->first - binWidth);
			file << std::setw(16) << std::right << it->first - binWidth;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(16) << std::right << 0.0;
			file << std::setw(19) << std::right << 0.0 << std::endl;
		}
		//first is energies, second is the number of photons
//		if(it->second <= 0.0)	//skip nonexistent data
//			continue;
		flux = it->second * tStepi;
//		energy = PHOTONENERGYCONST / double(wavelength[i]);	//  eV*nm / nm
		wavelength = PHOTONENERGYCONST / it->first;
		power = it->first * flux * QCHARGE * 1000;	//eV * phot/sec * J/eV = J/s = W * mW/W = mW
		intensity = power * areai;	//mW/cm^2

		
		if(it->first > 0.0)
		{
			PrepOutputDoubleToFile(wavelength, file);
			file << std::setw(16) << std::right << wavelength;
		}
		else
			file << std::setw(16) << std::right << "Infinite";

		PrepOutputDoubleToFile(it->first, file);
		file << std::setw(16) << std::right << it->first;

		PrepOutputDoubleToFile(it->second, file);
		file << std::setw(16) << std::right << it->second;

		PrepOutputDoubleToFile(flux, file);
		file << std::setw(16) << std::right << flux;

		PrepOutputDoubleToFile(power, file);
		file << std::setw(16) << std::right << power;

		PrepOutputDoubleToFile(intensity, file);
		file << std::setw(19) << std::right << intensity << std::endl;

		printblankforName = true;
		prev = it->first;
	}

	file.close();

	return(0);
}

int BinWaveData(std::map<double, double>& wLengthData, std::map<OpSort, double>& fullData, int numBins)
{
	wLengthData.clear();

	double Emin = 1e10;		//if we're doing solar with 10's of MeV, what the hell is going on
	double Emax = 0;
	float waveMin = float(1.0e20);	//huge... that energy is associated 10^11 m, or trillions of km. It just ain't gonna happen
	float waveMax = 0.0;
	if(numBins < 1)
		numBins = 1;

	std::vector<SourceSpectrumDistribution*> ProcessingClass;
	
	//first, we need to see how much the data needs to be spread.
	
	for(std::map<OpSort,double>::iterator energyData = fullData.begin(); energyData != fullData.end(); ++energyData)
	{
		if(energyData->first.LSpec == NULL)
		{
			if(energyData->first.wavelength < waveMin)
				waveMin = energyData->first.wavelength;
			if(energyData->first.wavelength > waveMax)
				waveMax = energyData->first.wavelength;
		}
		else //we need to do a lotta work here...
		{
			SourceSpectrumDistribution* tmpSSD = new SourceSpectrumDistribution;
			ProcessingClass.push_back(tmpSSD);
			tmpSSD->Init(energyData->first.LSpec);
			double start = tmpSSD->GetStart();
			double end = tmpSSD->GetEnd();
			if(start < Emin)
				Emin = start;
			if(end > Emax)
				Emax = end;
		}
	}
	//convert wavelengths to energies
	double tmpE = PHOTONENERGYCONST / double(waveMin);
	if(tmpE > Emax)
		Emax = tmpE;
	tmpE = PHOTONENERGYCONST / double(waveMax);
	if(tmpE < Emin)
		Emin = tmpE;

	double width = Emax - Emin;
	double binWidth = width / double(numBins);
	std::vector<double> choiceLSpec;
	unsigned int PClassCtr = 0;
	for(std::map<OpSort,double>::iterator energyData = fullData.begin(); energyData != fullData.end(); ++energyData)
	{
		if(energyData->first.LSpec == NULL)
		{
			tmpE = PHOTONENERGYCONST / double(energyData->first.wavelength);	//energy associated with wavelength
			//now to find which energy "bin" to put it in.
			double difference = tmpE - Emin;
			if(difference > 0.0 && binWidth > 0.0)
				difference = floor(difference / binWidth);
			tmpE = Emin + binWidth * difference;	//this should output consistent values, even if it is doubles...
			MapAdd(wLengthData, tmpE, energyData->second);
		}
		else
		{
			ProcessingClass[PClassCtr]->GenerateFunction(Emin, binWidth, choiceLSpec);	//create all the necessary data
			for(unsigned int c=0; c<choiceLSpec.size(); c++)
			{
				double val = energyData->second * ProcessingClass[PClassCtr]->AcquireValue(choiceLSpec[c]);
				if(MY_IS_FINITE(val) == false)
					ProgressCheck(53, false);

				//dang that's a nightmare of array indicies. The intensity is the total intensity.
				//PClass is synced with the previous for loop to match the initialized data
				//c goes through all the generated data from generatefunction to get the proper value
				//now let's just make sure it gets recorded into a valid bin
				double difference = choiceLSpec[c] - Emin;
				if(difference > 0.0 && binWidth > 0.0)
					difference = floor(difference/binWidth);
				tmpE = Emin + binWidth * difference;
				MapAdd(wLengthData, tmpE, val);	//this is the output data that we care about
			}
			delete ProcessingClass[PClassCtr];	//and get rid of all the data now that it's done
			ProcessingClass[PClassCtr] = NULL;
			PClassCtr++;	//next time, move on to the correct processed data.
			
		}
	}
	if(PClassCtr != ProcessingClass.size())
	{
		ProgressCheck(58, false);
	}
	ProcessingClass.clear();
	

	return(0);
}

int OutputQERows(std::string file, BulkOpticalData* lightData, ModelDescribe& mdesc, double tStepi)
{
	////////
	Simulation* sim = mdesc.simulation;
	
	double areai = 1.0/(mdesc.Ldimensions.y * mdesc.Ldimensions.z);
	
//	lightData->BinOpticalData(0.0);	//set delta to 0, which means be smart and make 1,000 entires
//	this is now taken care of by the function BinWaveData

//	std::map<double, double> ProcessedEnergies;
	
	if(sim->outputs.QE)
	{
//		BinWaveData(ProcessedEnergies,lightData->LightIncident , 100);
		if (lightData->LightIncident.size() > 0 || sim->outputs.EmptyQEFiles)
			OutputQERowSubsection(file, "Light Incident", lightData->LightIncident, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
		if (lightData->LightEnter.size() > 0 || sim->outputs.EmptyQEFiles)
			OutputQERowSubsection(file, "Light Entered", lightData->LightEnter, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
		if (lightData->LightLeavePos.size() > 0 || sim->outputs.EmptyQEFiles)
			OutputQERowSubsection(file, "Light Emissions+", lightData->LightLeavePos, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
		if (lightData->LightLeaveNeg.size() > 0 || sim->outputs.EmptyQEFiles)
			OutputQERowSubsection(file, "Light Emissions-", lightData->LightLeaveNeg, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}

	if (sim->outputs.Lgen && (lightData->LightCarrierGen.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "Light Absorbed Eg", lightData->LightCarrierGen, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Lsrh && (lightData->LightSRH.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "LA SRH", lightData->LightSRH, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Ldef && (lightData->LightDef.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "LA Defects Impurities", lightData->LightDef, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Lscat && (lightData->LightScat.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "LA Scattering", lightData->LightScat, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Erec && (lightData->StimEmisRec.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "Stimulated Emission Eg", lightData->StimEmisRec, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Esrh && (lightData->StimEmisSRH.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "SE SRH", lightData->StimEmisSRH, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Edef && (lightData->StimEmisDef.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "SE Defects Impurities", lightData->StimEmisDef, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.Escat && (lightData->StimEmisScat.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "SE Scattering", lightData->StimEmisScat, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.LightEmission && (lightData->GeneratedEg.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "Eg Light Generated", lightData->GeneratedEg, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
	if (sim->outputs.LightEmission && (lightData->GeneratedSRH.size() > 0 || sim->outputs.EmptyQEFiles))
	{
		OutputQERowSubsection(file, "SRH Light Generated", lightData->GeneratedSRH, sim->misc.largestMatNameSize, tStepi, areai, sim->EnabledRates.OpticalEResolution, sim);
	}
//	ProcessedEnergies.clear();	//make sure to delete any memory leaks!
	
	return(0);
}

//allow passing in string, or double data...
template <class Output>
int OutputPartToFile(Output data, std::ofstream& file, unsigned int width, unsigned int precision, bool doprecision)
{

	file << " " << std::setw(width) << std::right;
	if(doprecision)
		file << std::setprecision(precision);
	file << data;

	return(0);
}

int PrepOutputDoubleToFile(double data, std::ofstream& file, bool integer)
{
	if(data == 0.0 || data == -0.0)
		file << std::fixed << std::setprecision(0);
	else if(integer && (data <=1.0e10 && data >=-1e10))
	{
		file << std::fixed << std::setprecision(0);
	}
	else if(fabs(data) >= 0.001 && fabs(data) <= 1000.0)
		file << std::fixed << std::setprecision(9);
	else
		file << std::scientific << std::setprecision(6);
		
	return(0);
}

int OutputDataToFileSS(Model& mdl, ModelDescribe& mdesc)
{
	Simulation* sim = mdesc.simulation;
	std::string loadfile = sim->OutputFName;
	int NDim = mdesc.NDim;

	std::ofstream file;
	std::stringstream filename;
		

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_";
	if(sim->SteadyStateData)	//this should pretty much always happen unless something bizarre happens
	{
		filename << sim->SteadyStateData->CurrentData << "_" << sim->SteadyStateData->ExternalStates[sim->SteadyStateData->CurrentData]->FileOut << ".bd";
		
	}
	else
		filename << sim->curiter << ".bd";

	

	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	sim->AddOutFile(filename.str());

	file << "Data from: " << loadfile << std::endl;
	file << "Iteration: " << sim->curiter << std::endl;
	
	//now create the header file 
	OutputPartToFile("Point", file, 10, 0, true);

	//need to know the position of each point, or later have the mesh put it together
	OutputPartToFile("X", file, 14, 0, true);
	
	if(NDim > 1)
		OutputPartToFile("Y", file, 14, 0, true);
	if(NDim > 2)
		OutputPartToFile("Z", file, 14, 0, true);

	//all this data is stored in the init file that was written to check the mesh
	if(sim->outputs.Psi == true)
	{
		OutputPartToFile("Psi", file, 14, 0, true);
	}
	if(sim->outputs.Ec == true)
	{
		OutputPartToFile("Ec", file, 14, 0, true);
	}
	if(sim->outputs.Ev == true)
	{
		OutputPartToFile("Ev", file, 14, 0, true);
	}
	if(sim->outputs.fermi == true)
	{
		OutputPartToFile("Ef", file, 14, 0, true);
	}
	if(sim->outputs.fermin == true)
	{
		OutputPartToFile("Efn", file, 14, 0, true);
	}
	if(sim->outputs.fermip == true)
	{
		OutputPartToFile("Efp", file, 14, 0, true);
	}
	if(sim->outputs.n == true)
	{
		OutputPartToFile("n", file, 14, 0, true);
	}
	if(sim->outputs.p == true)
	{
		OutputPartToFile("p", file, 14, 0, true);
	}
	if(sim->outputs.p && sim->outputs.n)
	{
		OutputPartToFile("n*p", file, 14, 0, true);
	}
	if(sim->outputs.rho == true)
	{
		OutputPartToFile("Rho", file, 14, 0, true);
	}
	if(sim->outputs.CarrierCB == true)
	{
		OutputPartToFile("Ec_Carriers", file, 14, 0, true);
	}
	if(sim->outputs.CarrierVB == true)
	{
		OutputPartToFile("Ev_Carriers", file, 14, 0, true);
	}
	if(sim->outputs.impcharge == true)
	{
		OutputPartToFile("Imp_Charge", file, 14, 0, true);
	}
	if(sim->outputs.genth == true)
	{
		OutputPartToFile("Generation_Th", file, 14, 0, true);
	}
	if(sim->outputs.recth == true)
	{
		OutputPartToFile("Recomb_Thermal", file, 14, 0, true);
	}
	if(sim->outputs.SRH == true)
	{
		OutputPartToFile("SRH_Rate1", file, 14, 0, true);

		OutputPartToFile("SRH_Rate2", file, 14, 0, true);

		OutputPartToFile("SRH_Rate3", file, 14, 0, true);

		OutputPartToFile("SRH_Rate4", file, 14, 0, true);
	}
	if(sim->outputs.scatter == true)
	{
		OutputPartToFile("Scatter", file, 14, 0, true);
	}
	if(sim->outputs.Jn == true)
	{
		OutputPartToFile("Jn_x", file, 14, 0, true);

		if(NDim > 1)
		{
			OutputPartToFile("Jn_y", file, 14, 0, true);
		}
		if(NDim > 2)
		{
			OutputPartToFile("Jn_z", file, 14, 0, true);
		}
	}
	if(sim->outputs.Jp == true)
	{
		OutputPartToFile("Jp_x", file, 14, 0, true);

		if(NDim > 1)
		{
			OutputPartToFile("Jp_y", file, 14, 0, true);
		}
		if(NDim > 2)
		{
			OutputPartToFile("Jp_z", file, 15, 0, true);
		}
	}
	if(sim->outputs.CBNRin == true)
	{
		OutputPartToFile("CB_NetRate", file, 14, 0, true);
	}
	if(sim->outputs.VBNRin == true)
	{
		OutputPartToFile("VB_NetRate", file, 14, 0, true);
	}
	if(sim->outputs.DonNRin == true)
	{
		OutputPartToFile("Don_NetRate", file, 14, 0, true);
	}
	if(sim->outputs.AcpNRin == true)
	{
		OutputPartToFile("Acp_NetRate", file, 14, 0, true);
	}

	if(sim->outputs.Lpow == true)
	{
		OutputPartToFile("LightPower_mW", file, 14, 0, true);
	}
	if(sim->outputs.Lgen == true)
	{
		OutputPartToFile("Optical_Gen", file, 14, 0, true);
	}
	if(sim->outputs.Erec == true)
	{
		OutputPartToFile("Stim_Emis_Rec", file, 14, 0, true);
	}
	if(sim->outputs.Lsrh == true)
	{
		OutputPartToFile("A_SRH", file, 14, 0, true);
	}
	if(sim->outputs.Esrh == true)
	{
		OutputPartToFile("SE_SRH", file, 14, 0, true);
	}
	if(sim->outputs.Ldef == true)
	{
		OutputPartToFile("Absorb_defect", file, 14, 0, true);
	}
	if(sim->outputs.Edef == true)
	{
		OutputPartToFile("SE_def", file, 14, 0, true);
	}
	if(sim->outputs.Lscat == true)
	{
		OutputPartToFile("Absorb_scat", file, 14, 0, true);
	}
	if(sim->outputs.Escat == true)
	{
		OutputPartToFile("SE_scat", file, 14, 0, true);
	}
	if(sim->outputs.LightEmission == true)
	{
		OutputPartToFile("Lt_Emit", file, 14, 0, true);
	}

	OutputPartToFile("Width_X", file, 14, 0, true);
	OutputPartToFile("Width_Y", file, 14, 0, true);
	OutputPartToFile("Width_Z", file, 14, 0, true);


	file << std::endl;
		
	
	

	//need to do data analysis for the bins. Only concern with mean, variance, and stddev
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		file << std::fixed;
		OutputPartToFile(curPt->second.adj.self, file, 10, 0, true);
		file << std::scientific;
		OutputPartToFile(curPt->second.position.x, file, 14, 6, true);
		if(NDim > 1)
			OutputPartToFile(curPt->second.position.y, file, 14, 6, true);
		if(NDim > 2)
			OutputPartToFile(curPt->second.position.z, file, 14, 6, true);

		if(sim->outputs.Psi == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Psi[0], file);
			OutputPartToFile(curPt->second.Output.Psi[0], file, 14, 9, false);
		}
		if(sim->outputs.Ec == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Ec[0], file);
			OutputPartToFile(curPt->second.Output.Ec[0], file, 14, 9, false);
		}
		if(sim->outputs.Ev == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Ev[0], file);
			OutputPartToFile(curPt->second.Output.Ev[0], file, 14, 9, false);
		}
		if(sim->outputs.fermi == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.fermi[0], file);
			OutputPartToFile(curPt->second.Output.fermi[0], file, 14, 9, false);
		}
		if(sim->outputs.fermin == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.fermin[0], file);
			OutputPartToFile(curPt->second.Output.fermin[0], file, 14, 9, false);
		}
		if(sim->outputs.fermip == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.fermip[0], file);
			OutputPartToFile(curPt->second.Output.fermip[0], file, 14, 9, false);
		}
		if(sim->outputs.n == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.n[0], file);
			OutputPartToFile(curPt->second.Output.n[0], file, 14, 6, false);
		}
		if(sim->outputs.p == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.p[0], file);
			OutputPartToFile(curPt->second.Output.p[0], file, 14, 6, false);
		}
		if(sim->outputs.p && sim->outputs.n)
		{
			PrepOutputDoubleToFile(curPt->second.Output.p[0] * curPt->second.Output.n[0], file);
			OutputPartToFile(curPt->second.Output.p[0] * curPt->second.Output.n[0], file, 14, 6, false);
		}
		if(sim->outputs.rho == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.rho[0], file);
			OutputPartToFile(curPt->second.Output.rho[0], file, 14, 6, false);
		}
		if(sim->outputs.CarrierCB == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.CarCB[0], file);
			OutputPartToFile(curPt->second.Output.CarCB[0], file, 14, 6, true);
		}
		if(sim->outputs.CarrierVB == true)
		{			
			PrepOutputDoubleToFile(curPt->second.Output.CarVB[0], file);
			OutputPartToFile(curPt->second.Output.CarVB[0], file, 14, 6, true);
		}
		if(sim->outputs.impcharge == true)
		{			
			PrepOutputDoubleToFile(curPt->second.Output.PointImpCharge[0], file, false);
			OutputPartToFile(curPt->second.Output.PointImpCharge[0], file, 14, 6, true);
		}
		if(sim->outputs.genth == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.genth[0], file);
			OutputPartToFile(curPt->second.Output.genth[0], file, 14, 6, false);
		}
		if(sim->outputs.recth == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.recth[0], file);
			OutputPartToFile(curPt->second.Output.recth[0], file, 14, 6, false);
		}
		if(sim->outputs.SRH == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.R1[0], file);
			OutputPartToFile(curPt->second.Output.R1[0], file, 14, 6, false);

			PrepOutputDoubleToFile(curPt->second.Output.R2[0], file);
			OutputPartToFile(curPt->second.Output.R2[0], file, 14, 6, false);
			
			PrepOutputDoubleToFile(curPt->second.Output.R3[0], file);
			OutputPartToFile(curPt->second.Output.R3[0], file, 14, 6, false);

			PrepOutputDoubleToFile(curPt->second.Output.R4[0], file);
			OutputPartToFile(curPt->second.Output.R4[0], file, 14, 6, false);
		}
		
		if(sim->outputs.scatter)
		{
			PrepOutputDoubleToFile(curPt->second.Output.scatter[0], file);
			OutputPartToFile(curPt->second.Output.scatter[0], file, 14, 6, false);
		}

		if(sim->outputs.Jn == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Jnx[0], file);
			OutputPartToFile(curPt->second.Output.Jnx[0], file, 14, 6, false);
			
			if(NDim > 1)
			{
				PrepOutputDoubleToFile(curPt->second.Output.Jny[0], file);
				OutputPartToFile(curPt->second.Output.Jny[0], file, 14, 6, false);
			}
			if(NDim > 2)
			{
				PrepOutputDoubleToFile(curPt->second.Output.Jnz[0], file);
				OutputPartToFile(curPt->second.Output.Jnz[0], file, 14, 6, false);
			}
		}
		if(sim->outputs.Jp == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Jpx[0], file);
			OutputPartToFile(curPt->second.Output.Jpx[0], file, 14, 6, false);
			if(NDim > 1)
			{
				PrepOutputDoubleToFile(curPt->second.Output.Jpy[0], file);
				OutputPartToFile(curPt->second.Output.Jpy[0], file, 14, 6, false);
			}
			if(NDim > 2)
			{
				PrepOutputDoubleToFile(curPt->second.Output.Jpz[0], file);
				OutputPartToFile(curPt->second.Output.Jpz[0], file, 14, 6, false);
			}
		}
		if(sim->outputs.CBNRin)
		{
			PrepOutputDoubleToFile(curPt->second.Output.CBNRIn[0], file);
			OutputPartToFile(curPt->second.Output.CBNRIn[0], file, 14, 6, false);
		}
		if(sim->outputs.VBNRin)
		{
			PrepOutputDoubleToFile(curPt->second.Output.VBNRIn[0], file);
			OutputPartToFile(curPt->second.Output.VBNRIn[0], file, 14, 6, false);
		}
		if(sim->outputs.DonNRin)
		{
			PrepOutputDoubleToFile(curPt->second.Output.DonNRin[0], file);
			OutputPartToFile(curPt->second.Output.DonNRin[0], file, 14, 6, false);
		}
		if(sim->outputs.AcpNRin)
		{
			PrepOutputDoubleToFile(curPt->second.Output.AcpNRin[0], file);
			OutputPartToFile(curPt->second.Output.AcpNRin[0], file, 14, 6, false);
		}

		if(sim->outputs.Lpow == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.LPowerEnter[0], file);
			OutputPartToFile(curPt->second.Output.LPowerEnter[0], file, 14, 6, false);
		}
		if(sim->outputs.Lgen == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Lgen[0], file);
			OutputPartToFile(curPt->second.Output.Lgen[0], file, 14, 6, false);
		}
		if(sim->outputs.Erec == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Erec[0], file);
			OutputPartToFile(curPt->second.Output.Erec[0], file, 14, 6, false);
		}
		if(sim->outputs.Lsrh == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Lsrh[0], file);
			OutputPartToFile(curPt->second.Output.Lsrh[0], file, 14, 6, false);
		}
		if(sim->outputs.Esrh == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Esrh[0], file);
			OutputPartToFile(curPt->second.Output.Esrh[0], file, 14, 6, false);
		}
		if(sim->outputs.Ldef == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Ldef[0], file);
			OutputPartToFile(curPt->second.Output.Ldef[0], file, 14, 6, false);
		}
		if(sim->outputs.Edef == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Edef[0], file);
			OutputPartToFile(curPt->second.Output.Edef[0], file, 14, 6, false);
		}
		if(sim->outputs.Lscat == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Lscat[0], file);
			OutputPartToFile(curPt->second.Output.Lscat[0], file, 14, 6, false);
		}
		if(sim->outputs.Escat == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.Escat[0], file);
			OutputPartToFile(curPt->second.Output.Escat[0], file, 14, 6, false);
		}
		if(sim->outputs.LightEmission == true)
		{
			PrepOutputDoubleToFile(curPt->second.Output.LightEmit[0], file);
			OutputPartToFile(curPt->second.Output.LightEmit[0], file, 14, 6, false);
		}

		PrepOutputDoubleToFile(curPt->second.width.x, file);
		OutputPartToFile(curPt->second.width.x, file, 14, 6, false);
		
		PrepOutputDoubleToFile(curPt->second.width.y, file);
		OutputPartToFile(curPt->second.width.y, file, 14, 6, false);

		PrepOutputDoubleToFile(curPt->second.width.z, file);
		OutputPartToFile(curPt->second.width.z, file, 14, 6, false);
		

		file << std::endl;	//end the line for this point data entry
		
		curPt->second.Output.Allocate(1);	//this will delete all the old data for this point	
		//and reallocate space for new data		
	}

	file.close();

	FinalOutputQEDataSS(mdesc);
	OutputContacts(sim, false);	//output contact data - THIS MUST BE AFTER FINAL OUTPUT QE DATA


	return(0);
}

int TransientSimData::OutputDataToFile()
{
	Model &mdl = *sim->mdlData;
	ModelDescribe& mdesc = *sim->mdesc;

	int binctr = sim->binctr;
	std::string loadfile = sim->OutputFName;
	int NDim = mdesc.NDim;

	std::ofstream file;
	std::stringstream filename;
	std::string tstepFile;
	bool includestats = false;
	if(binctr > 1)
		includestats = sim->outputs.stddev;		///only include stddev if there is more than one...

	filename.str("");
	filename.clear();
	filename << sim->OutputFName << "_trans_";

	if (outputCtr < 1e9)
		filename << "0";
	if (outputCtr < 1e8)
		filename << "0";
	if (outputCtr < 1e7)
		filename << "0";
	if (outputCtr < 1e6)
		filename << "0";
	if (outputCtr < 1e5)
		filename << "0";
	if (outputCtr < 1e4)
		filename << "0";
	if (outputCtr < 1e3)
		filename << "0";
	if (outputCtr < 1e2)
		filename << "0";
	if (outputCtr < 1e1)
		filename << "0";
	filename << outputCtr;
	tstepFile = filename.str() + ".tstep";
	filename << ".bd";
	file.clear();
	file.open(filename.str().c_str(), std::fstream::out);

	if(file.is_open() == false)
		return(-1);

	sim->AddOutFile(filename.str());

	file << "Data from: " << loadfile << std::endl;
	file << "Simulation time: " << sim->TransientData->tStepControl.GetStartTimeData() << " to " << sim->TransientData->iterEndtime << std::endl;
	file << "Timestep: see new file, this deprecated" << sim->TransientData->mints << "  " << sim->TransientData->avgts << "  " << sim->TransientData->maxts << std::endl;
	file << "Number of substeps: " << sim->TransientData->timesteps.size() << std::endl;
	file << "Iteration: " << sim->curiter << std::endl;

	//sim->storevalueStartTime = sim->iterEndtime - sim->storevalueStartTime;
	sim->TransientData->storevalueStartTime = 1.0;
/*	for (unsigned int i = 0; i<sim->outputs.numinbin; ++i)
		sim->TransientData->storevalueStartTime += sim->TransientData->timesteps[i];
	
	if (sim->TransientData->storevalueStartTime > 0.0)
		sim->TransientData->storevalueStartTime = 1.0 / sim->TransientData->storevalueStartTime;	//turn the starttime into the normalization factor
	else
		sim->TransientData->storevalueStartTime = 1.0;
	*/

	
	DataOut Psi;
	DataOut Ec;	//hold the outputs of each data bit...
	DataOut Ev;
	DataOut CarCB;
	DataOut CarVB;
	DataOut fermi;
	DataOut fermin;
	DataOut fermip;
	DataOut impcharge;
	DataOut genth;
	DataOut recth;
	DataOut R1;
	DataOut R2;
	DataOut R3;
	DataOut R4;
	DataOut Lgen;
	DataOut Lsrh;
	DataOut Ldef;
	DataOut Lscat;
	DataOut Erec;
	DataOut Esrh;
	DataOut Edef;
	DataOut Escat;
	DataOut Jnx;
	DataOut Jny;
	DataOut Jnz;
	DataOut Jpx;
	DataOut Jpy;
	DataOut Jpz;
	DataOut n;
	DataOut p;
	DataOut rho;
	DataOut scatter;
	DataOut LPower;
	DataOut LightEmit;


	//now create the header file 
	OutputPartToFile("Point", file, 10, 0, true);

	//need to know the position of each point, or later have the mesh put it together
	OutputPartToFile("X", file, 14, 0, true);
	
	if(NDim > 1)
		OutputPartToFile("Y", file, 14, 0, true);
	if(NDim > 2)
		OutputPartToFile("Z", file, 14, 0, true);

	//all this data is stored in the init file that was written to check the mesh
	if(sim->outputs.Psi == true)
	{
		OutputPartToFile("Psi", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Psi_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Ec == true)
	{
		OutputPartToFile("Ec", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Ec_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Ev == true)
	{
		OutputPartToFile("Ev", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Ev_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.fermi == true)
	{
		OutputPartToFile("Ef", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Ef_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.fermin == true)
	{
		OutputPartToFile("Efn", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Efn_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.fermip == true)
	{
		OutputPartToFile("Efp", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Efp_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.n == true)
	{
		OutputPartToFile("n", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("n_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.p == true)
	{
		OutputPartToFile("p", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("p_std_dev", file, 14, 0, true);
	}
	OutputPartToFile("n*p", file, 14, 0, true);

	if(sim->outputs.rho == true)
	{
		OutputPartToFile("Rho", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Rho_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.CarrierCB == true)
	{
		OutputPartToFile("Ec_Carriers", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("EcC_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.CarrierVB == true)
	{
		OutputPartToFile("Ev_Carriers", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("EvC_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.impcharge == true)
	{
		OutputPartToFile("Imp_Charge", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Imp_Q_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.genth == true)
	{
		OutputPartToFile("Generation_Th", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("GenTh_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.recth == true)
	{
		OutputPartToFile("Recomb_Thermal", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("RecTh_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.SRH == true)
	{
		OutputPartToFile("SRH_Rate1", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SRH1_std_dev", file, 14, 0, true);

		OutputPartToFile("SRH_Rate2", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SRH2_std_dev", file, 14, 0, true);

		OutputPartToFile("SRH_Rate3", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SRH3_std_dev", file, 14, 0, true);

		OutputPartToFile("SRH_Rate4", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SRH4_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.scatter == true)
	{
		OutputPartToFile("Scatter", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Sctr_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Jn == true)
	{
		OutputPartToFile("Jn_x", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Jn_x_std_dev", file, 14, 0, true);

		if(NDim > 1)
		{
			OutputPartToFile("Jn_y", file, 14, 0, true);
			if(includestats)
				OutputPartToFile("Jn_y_std_dev", file, 14, 0, true);
		}
		if(NDim > 2)
		{
			OutputPartToFile("Jn_z", file, 14, 0, true);
			if(includestats)
				OutputPartToFile("Jn_z_std_dev", file, 14, 0, true);
		}
	}
	if(sim->outputs.Jp == true)
	{
		OutputPartToFile("Jp_x", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Jp_x_std_dev", file, 14, 0, true);

		if(NDim > 1)
		{
			OutputPartToFile("Jp_y", file, 14, 0, true);
			if(includestats)
				OutputPartToFile("Jp_y_std_dev", file, 15, 0, true);
		}
		if(NDim > 2)
		{
			OutputPartToFile("Jp_z", file, 15, 0, true);
			if(includestats)
				OutputPartToFile("Jp_z_std_dev", file, 15, 0, true);
		}
	}
	if(sim->outputs.Lpow == true)
	{
		OutputPartToFile("LightPower_mW", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("LP_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Lgen == true)
	{
		OutputPartToFile("Optical_Gen", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("OGen_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Erec == true)
	{
		OutputPartToFile("Stim_Emis_Rec", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SErec_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Lsrh == true)
	{
		OutputPartToFile("A_SRH", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Asrh_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Esrh == true)
	{
		OutputPartToFile("SE_SRH", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SEsrh_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Ldef == true)
	{
		OutputPartToFile("Absorb_defect", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Adef_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Edef == true)
	{
		OutputPartToFile("SE_def", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SEdef_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Lscat == true)
	{
		OutputPartToFile("Absorb_scat", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("Asct_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.Escat == true)
	{
		OutputPartToFile("SE_scat", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("SEsct_std_dev", file, 14, 0, true);
	}
	if(sim->outputs.LightEmission == true)
	{
		OutputPartToFile("Lt_Emit", file, 14, 0, true);
		if(includestats)
			OutputPartToFile("LtEmt_std_dev", file, 14, 0, true);
	}

	OutputPartToFile("Width_X", file, 14, 0, true);
	OutputPartToFile("Width_Y", file, 14, 0, true);
	OutputPartToFile("Width_Z", file, 14, 0, true);

	if(sim->outputs.chgError==true)
	{
		OutputPartToFile("CB_Pos_Err", file,14,0,true);
		OutputPartToFile("CB_Neg_Err", file,14,0,true);
		OutputPartToFile("VB_Pos_Err", file,14,0,true);
		OutputPartToFile("VB_Neg_Err", file,14,0,true);
		OutputPartToFile("CBT_Pos_Err", file,14,0,true);
		OutputPartToFile("CBT_Neg_Err", file,14,0,true);
		OutputPartToFile("VBT_Pos_Err", file,14,0,true);
		OutputPartToFile("VBT_Neg_Err", file,14,0,true);
		OutputPartToFile("Don_Pos_Err", file,14,0,true);
		OutputPartToFile("Don_Neg_Err", file,14,0,true);
		OutputPartToFile("Acp_Pos_Err", file,14,0,true);
		OutputPartToFile("Acp_Neg_Err", file,14,0,true);
	}
	file << std::endl;
		
	
	

	//need to do data analysis for the bins. Only concern with mean, variance, and stddev
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		file << std::fixed;
		OutputPartToFile(curPt->second.adj.self, file, 10, 0, true);
		file << std::scientific;
		OutputPartToFile(curPt->second.position.x, file, 14, 6, true);
		if(NDim > 1)
			OutputPartToFile(curPt->second.position.y, file, 14, 6, true);
		if(NDim > 2)
			OutputPartToFile(curPt->second.position.z, file, 14, 6, true);

		if(sim->outputs.Psi == true)
		{
			FillDataOut(Psi, curPt->second.Output.Psi, sim, binctr);
			PrepOutputDoubleToFile(Psi.mean, file);
			OutputPartToFile(Psi.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Psi.stddev, file);
				OutputPartToFile(Psi.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Ec == true)
		{
			FillDataOut(Ec, curPt->second.Output.Ec, sim, binctr);
			PrepOutputDoubleToFile(Ec.mean, file);
			OutputPartToFile(Ec.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Ec.stddev, file);
				OutputPartToFile(Ec.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Ev == true)
		{
			FillDataOut(Ev, curPt->second.Output.Ev, sim, binctr);
			PrepOutputDoubleToFile(Ev.mean, file);
			OutputPartToFile(Ev.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Ev.stddev, file);
				OutputPartToFile(Ev.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.fermi == true)
		{
			FillDataOut(fermi, curPt->second.Output.fermi, sim, binctr);
			PrepOutputDoubleToFile(fermi.mean, file);
			OutputPartToFile(fermi.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(fermi.stddev, file);
				OutputPartToFile(fermi.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.fermin == true)
		{
			FillDataOut(fermin, curPt->second.Output.fermin, sim, binctr);
			PrepOutputDoubleToFile(fermin.mean, file);
			OutputPartToFile(fermin.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(fermin.stddev, file);
				OutputPartToFile(fermin.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.fermip == true)
		{
			FillDataOut(fermip, curPt->second.Output.fermip, sim, binctr);
			PrepOutputDoubleToFile(fermip.mean, file);
			OutputPartToFile(fermip.mean, file, 14, 9, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(fermip.stddev, file);
				OutputPartToFile(fermip.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.n == true)
		{
			FillDataOut(n, curPt->second.Output.n, sim, binctr);
			PrepOutputDoubleToFile(n.mean, file);
			OutputPartToFile(n.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(n.stddev, file);
				OutputPartToFile(n.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.p == true)
		{
			FillDataOut(p, curPt->second.Output.p, sim, binctr);
			PrepOutputDoubleToFile(p.mean, file);
			OutputPartToFile(p.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(p.stddev, file);
				OutputPartToFile(p.stddev, file, 14, 6, false);
			}
		}
		
		PrepOutputDoubleToFile(p.mean * n.mean, file);
		OutputPartToFile(p.mean * n.mean, file, 14, 6, false);

		if(sim->outputs.rho == true)
		{
			FillDataOut(rho, curPt->second.Output.rho, sim, binctr);
			PrepOutputDoubleToFile(rho.mean, file);
			OutputPartToFile(rho.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(rho.stddev, file);
				OutputPartToFile(rho.stddev, file, 14, 6, false);
			}


		}
		if(sim->outputs.CarrierCB == true)
		{
			FillDataOut(CarCB, curPt->second.Output.CarCB, sim, binctr);
			PrepOutputDoubleToFile(CarCB.mean, file);
			
//			if(binctr == 1)
				OutputPartToFile(CarCB.mean, file, 14, 6, true);
//			else
//				OutputPartToFile(CarCB.mean, file, 14, 2, true);
			if(includestats)
			{
				PrepOutputDoubleToFile(CarCB.stddev, file);
				OutputPartToFile(CarCB.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.CarrierVB == true)
		{
			FillDataOut(CarVB, curPt->second.Output.CarVB, sim, binctr);
			PrepOutputDoubleToFile(CarVB.mean, file);
			//file << std::fixed;
			//if(binctr == 1)
				OutputPartToFile(CarVB.mean, file, 14, 6, true);
			//else
			//	OutputPartToFile(CarVB.mean, file, 14, 2, true);
			if(includestats)
			{
				PrepOutputDoubleToFile(CarVB.stddev, file);
				OutputPartToFile(CarVB.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.impcharge == true)
		{
			FillDataOut(impcharge, curPt->second.Output.PointImpCharge, sim, binctr);
			PrepOutputDoubleToFile(impcharge.mean, file, false);
			OutputPartToFile(impcharge.mean, file, 14, 6, true);

			if(includestats)
			{
				PrepOutputDoubleToFile(impcharge.stddev, file);
				OutputPartToFile(impcharge.stddev, file, 14, 6, false);
			}
		}
		
		if(sim->outputs.genth == true)
		{
			FillDataOut(genth, curPt->second.Output.genth, sim, binctr);
			PrepOutputDoubleToFile(genth.mean, file);
			OutputPartToFile(genth.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(genth.stddev, file);
				OutputPartToFile(genth.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.recth == true)
		{
			FillDataOut(recth, curPt->second.Output.recth, sim, binctr);
			PrepOutputDoubleToFile(recth.mean, file);
			OutputPartToFile(recth.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(recth.stddev, file);
				OutputPartToFile(recth.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.SRH == true)
		{
			FillDataOut(R1, curPt->second.Output.R1, sim, binctr);
			PrepOutputDoubleToFile(R1.mean, file);
			OutputPartToFile(R1.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(R1.stddev, file);
				OutputPartToFile(R1.stddev, file, 14, 6, false);
			}

			FillDataOut(R2, curPt->second.Output.R2, sim, binctr);
			PrepOutputDoubleToFile(R2.mean, file);
			OutputPartToFile(R2.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(R2.stddev, file);
				OutputPartToFile(R2.stddev, file, 14, 6, false);
			}

			FillDataOut(R3, curPt->second.Output.R3, sim, binctr);
			PrepOutputDoubleToFile(R3.mean, file);
			OutputPartToFile(R3.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(R3.stddev, file);
				OutputPartToFile(R3.stddev, file, 14, 6, false);
			}

			FillDataOut(R4, curPt->second.Output.R4, sim, binctr);
			PrepOutputDoubleToFile(R4.mean, file);
			OutputPartToFile(R4.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(R4.stddev, file);
				OutputPartToFile(R4.stddev, file, 14, 6, false);
			}
		}
		
		if(sim->outputs.scatter)
		{
			FillDataOut(scatter, curPt->second.Output.scatter, sim, binctr);
			PrepOutputDoubleToFile(scatter.mean, file);
			OutputPartToFile(scatter.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(scatter.stddev, file);
				OutputPartToFile(scatter.stddev, file, 14, 6, false);
			}
		}

		if(sim->outputs.Jn == true)
		{
			FillDataOut(Jnx, curPt->second.Output.Jnx, sim, binctr);
			PrepOutputDoubleToFile(Jnx.mean, file);
			OutputPartToFile(Jnx.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Jnx.stddev, file);
				OutputPartToFile(Jnx.stddev, file, 14, 6, false);
			}
			if(NDim > 1)
			{
				FillDataOut(Jny, curPt->second.Output.Jny, sim, binctr);
				PrepOutputDoubleToFile(Jny.mean, file);
				OutputPartToFile(Jny.mean, file, 14, 6, false);
				if(includestats)
				{
					PrepOutputDoubleToFile(Jny.stddev, file);
					OutputPartToFile(Jny.stddev, file, 14, 6, false);
				}
			}
			if(NDim > 2)
			{
				FillDataOut(Jnz, curPt->second.Output.Jnz, sim, binctr);
				PrepOutputDoubleToFile(Jnz.mean, file);
				OutputPartToFile(Jnz.mean, file, 14, 6, false);
				if(includestats)
				{
					PrepOutputDoubleToFile(Jnz.stddev, file);
					OutputPartToFile(Jnz.stddev, file, 14, 6, false);
				}
			}
		}
		if(sim->outputs.Jp == true)
		{
			FillDataOut(Jpx, curPt->second.Output.Jpx, sim, binctr);
			PrepOutputDoubleToFile(Jpx.mean, file);
			OutputPartToFile(Jpx.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Jpx.stddev, file);
				OutputPartToFile(Jpx.stddev, file, 14, 6, false);
			}
			if(NDim > 1)
			{
				FillDataOut(Jpy, curPt->second.Output.Jpy, sim, binctr);
				PrepOutputDoubleToFile(Jpy.mean, file);
				OutputPartToFile(Jpy.mean, file, 14, 6, false);
				if(includestats)
				{
					PrepOutputDoubleToFile(Jpy.stddev, file);
					OutputPartToFile(Jpy.stddev, file, 14, 6, false);
				}
			}
			if(NDim > 2)
			{
				FillDataOut(Jpz, curPt->second.Output.Jpz, sim, binctr);
				PrepOutputDoubleToFile(Jpz.mean, file);
				OutputPartToFile(Jpz.mean, file, 14, 6, false);
				if(includestats)
				{
					PrepOutputDoubleToFile(Jpz.stddev, file);
					OutputPartToFile(Jpz.stddev, file, 14, 6, false);
				}
			}
		}

		if(sim->outputs.Lpow == true)
		{
			FillDataOut(LPower, curPt->second.Output.LPowerEnter, sim, binctr);
			PrepOutputDoubleToFile(LPower.mean, file);
			OutputPartToFile(LPower.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(LPower.stddev, file);
				OutputPartToFile(LPower.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Lgen == true)
		{
			FillDataOut(Lgen, curPt->second.Output.Lgen, sim, binctr);
			PrepOutputDoubleToFile(Lgen.mean, file);
			OutputPartToFile(Lgen.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Lgen.stddev, file);
				OutputPartToFile(Lgen.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Erec == true)
		{
			FillDataOut(Erec, curPt->second.Output.Erec, sim, binctr);
			PrepOutputDoubleToFile(Erec.mean, file);
			OutputPartToFile(Erec.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Erec.stddev, file);
				OutputPartToFile(Erec.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Lsrh == true)
		{
			FillDataOut(Lsrh, curPt->second.Output.Lsrh, sim, binctr);
			PrepOutputDoubleToFile(Lsrh.mean, file);
			OutputPartToFile(Lsrh.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Lsrh.stddev, file);
				OutputPartToFile(Lsrh.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Esrh == true)
		{
			FillDataOut(Esrh, curPt->second.Output.Esrh, sim, binctr);
			PrepOutputDoubleToFile(Esrh.mean, file);
			OutputPartToFile(Esrh.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Esrh.stddev, file);
				OutputPartToFile(Esrh.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Ldef == true)
		{
			FillDataOut(Ldef, curPt->second.Output.Ldef, sim, binctr);
			PrepOutputDoubleToFile(Ldef.mean, file);
			OutputPartToFile(Ldef.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Ldef.stddev, file);
				OutputPartToFile(Ldef.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Edef == true)
		{
			FillDataOut(Edef, curPt->second.Output.Edef, sim, binctr);
			PrepOutputDoubleToFile(Edef.mean, file);
			OutputPartToFile(Edef.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Edef.stddev, file);
				OutputPartToFile(Edef.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Lscat == true)
		{
			FillDataOut(Lscat, curPt->second.Output.Lscat, sim, binctr);
			PrepOutputDoubleToFile(Lscat.mean, file);
			OutputPartToFile(Lscat.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Lscat.stddev, file);
				OutputPartToFile(Lscat.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.Escat == true)
		{
			FillDataOut(Escat, curPt->second.Output.Escat, sim, binctr);
			PrepOutputDoubleToFile(Escat.mean, file);
			OutputPartToFile(Escat.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(Escat.stddev, file);
				OutputPartToFile(Escat.stddev, file, 14, 6, false);
			}
		}
		if(sim->outputs.LightEmission == true)
		{
			FillDataOut(LightEmit, curPt->second.Output.LightEmit, sim, binctr);
			PrepOutputDoubleToFile(LightEmit.mean, file);
			OutputPartToFile(LightEmit.mean, file, 14, 6, false);
			if(includestats)
			{
				PrepOutputDoubleToFile(LightEmit.stddev, file);
				OutputPartToFile(LightEmit.stddev, file, 14, 6, false);
			}
		}

		PrepOutputDoubleToFile(curPt->second.width.x, file);
		OutputPartToFile(curPt->second.width.x, file, 14, 6, false);
		
		PrepOutputDoubleToFile(curPt->second.width.y, file);
		OutputPartToFile(curPt->second.width.y, file, 14, 6, false);

		PrepOutputDoubleToFile(curPt->second.width.z, file);
		OutputPartToFile(curPt->second.width.z, file, 14, 6, false);
		
		if(sim->outputs.chgError==true)
		{
			PrepOutputDoubleToFile(curPt->second.cError.cbP, file);
			OutputPartToFile(curPt->second.cError.cbP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.cbN, file);
			OutputPartToFile(curPt->second.cError.cbN, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.vbP, file);
			OutputPartToFile(curPt->second.cError.vbP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.vbN, file);
			OutputPartToFile(curPt->second.cError.vbN, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.cbTP, file);
			OutputPartToFile(curPt->second.cError.cbTP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.cbTN, file);
			OutputPartToFile(curPt->second.cError.cbTN, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.vbTP, file);
			OutputPartToFile(curPt->second.cError.vbTP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.vbTN, file);
			OutputPartToFile(curPt->second.cError.vbTN, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.donP, file);
			OutputPartToFile(curPt->second.cError.donP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.donN, file);
			OutputPartToFile(curPt->second.cError.donN, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.acpP, file);
			OutputPartToFile(curPt->second.cError.acpP, file,14,6,false);
			PrepOutputDoubleToFile(curPt->second.cError.acpN, file);
			OutputPartToFile(curPt->second.cError.acpN, file,14,6,false);
			curPt->second.cError.Reset();
		}

		file << std::endl;	//end the line for this point data entry
		
		curPt->second.Output.Allocate(sim->outputs.numinbin);	//this will delete all the old data for this point	
		//and reallocate space for new data		
	}

	file.close();

	OutputTStepsToFile(tstepFile);


	FinalOutputQEDataTransient(mdesc);
	OutputContacts(sim, false);	//output contact data - THIS MUST BE AFTER FINAL OUTPUT QE DATA
	OutputCharge(mdesc.simulation, false);

	
//	sim->TransientData->timesteps = NULL;
//	sim->TransientData->timesteps = new double[sim->outputs.numinbin]();	//parenthesis makes sure init to zero

	return(0);

}

void TransientSimData::OutputTStepsToFile(std::string fname) {
	if (timesteps.size() == 0)
		return;

	std::ofstream file;
	file.open(fname.c_str(), std::fstream::out);
	if (file.is_open()) {
		file << "Start Time: " << std::scientific << std::setprecision(12) << tStepControl.GetStartTimeData() << std::endl;
		file << "End Time: " << std::scientific << std::setprecision(12) << iterEndtime << std::endl;
		DataOut tSteps;
		FillDataOut(tSteps, &timesteps[0], timesteps.size());
		file << "Number Steps: " << std::fixed << std::setprecision(0) << timesteps.size() << std::endl;
		file << "Average Step: " << std::scientific << std::setprecision(12) << tSteps.mean << std::endl;
		for (unsigned int i = 0; i < timesteps.size(); ++i)
			file << timesteps[i] << std::endl;
		file.close();
		sim->AddOutFile(fname);
	}

	timesteps.clear();
	tStepControl.UpdateLastOutTime();
	tStepControl.SetStartTimeData(iterEndtime);
}

double FillDataOut(DataOut& retval, double* dataset, unsigned int total) {
	if (dataset == nullptr)
		return 0.0;
	if (total == 0)
		return 0.0;

	if (total == 1) {
		retval.mean = dataset[0];
		retval.variance = retval.standarderror = retval.stddev = 0.0;
		return 0.0;
	}

	double mean = 0.0;
	double var = 0.0;
	for (unsigned int i = 0; i<total; i++)
		mean += dataset[i];	//all these are time normalized

	retval.mean = mean / double(total);	//storevalueStartTime is converted to: 1 / (endtime - storeStartTime);
	//retval.mean now has the expectation value of X
	
	double sq;	//unfortunately, the weighting must apply to the difference squared, and the weighting is the timestep
	for (unsigned int i = 0; i<total; i++)
	{
		sq = dataset[i] - retval.mean;	//don't want the time normalized value when calculating variance
		var += sq * sq;
	}
	
	retval.variance = var / double(total);

	if (retval.variance >= 0)
		retval.stddev = sqrt(retval.variance);	//it should always be positive, but just in case...
	else //not really sure how this would happen...
		retval.stddev = sqrt(-retval.variance);

	double den = sqrt(double(total));
	retval.standarderror = retval.stddev / den;
	return 0.0;
}

double FillDataOut(DataOut& retval, double* dataset, Simulation* sim, unsigned int binctr)
{
	if(binctr <= 1)
	{
		retval.mean = dataset[0] * sim->TransientData->storevalueStartTime;
		retval.variance = retval.standarderror = retval.stddev = 0.0;
		return(0);
	}
	double mean = 0.0;
	double var = 0.0;
	double weightSq = 0.0;
	double weights = 0.0;	//this will sum up to endtime - storeStartTime
//	double inv = sim.inv[binctr];	//store 1.0 / numinset once
	for(unsigned int i=0; i<binctr; i++)
	{
		mean += dataset[i];	//all these are time normalized
		weightSq += sim->TransientData->timesteps[i] * sim->TransientData->timesteps[i];
		weights += sim->TransientData->timesteps[i];

	}
	retval.mean = mean * sim->TransientData->storevalueStartTime;	//storevalueStartTime is converted to: 1 / (endtime - storeStartTime);
	//retval.mean now has the expectation value of X
	if(binctr > 1)
	{
		double sq;	//unfortunately, the weighting must apply to the difference squared, and the weighting is the timestep
		for(unsigned int i=0; i<binctr; i++)
		{
			sq = dataset[i] / sim->TransientData->timesteps[i] - retval.mean;	//don't want the time normalized value when calculating variance
			var += sim->TransientData->timesteps[i] * sq * sq;
		}
	
		retval.variance = var * weights / (weights * weights - weightSq);

		if(retval.variance >= 0)
			retval.stddev = sqrt(retval.variance);	//it should always be positive, but just in case...
		else
			retval.stddev = sqrt(-retval.variance);

		double den = sqrt(double(binctr));
		retval.standarderror = retval.stddev / den;
	}
	else
	{
		retval.variance = 0.0;
		retval.stddev = 0.0;
		retval.standarderror = 0.0;
	}

	return(0);
}

int OutputDataFinal(Model& mdl, Simulation& sim)
{
	
/*	sim.TransientData->timestep = 0.0;
	sim.TransientData->mints = 0.0;
	sim.TransientData->maxts = 0.0;
	sim.TransientData->avgts = 0.0;
*/	

	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		curPt->second.Output.Allocate(1);	//allocate stuff for the last entry

		curPt->second.Output.Psi[0] = curPt->second.Psi;
		curPt->second.Output.Ec[0] = curPt->second.cb.bandmin;
		curPt->second.Output.Ev[0] = curPt->second.vb.bandmin;
		curPt->second.Output.fermi[0] = curPt->second.fermi;
		curPt->second.Output.fermin[0] = curPt->second.fermin;
		curPt->second.Output.fermip[0] = curPt->second.fermip ;
		curPt->second.Output.CarCB[0] = curPt->second.cb.GetNumParticles();
		curPt->second.Output.CarVB[0] = curPt->second.vb.GetNumParticles();
		curPt->second.Output.n[0] = curPt->second.n * curPt->second.voli;
		curPt->second.Output.p[0] = curPt->second.p * curPt->second.voli;
		curPt->second.Output.rho[0] = curPt->second.rho;
		curPt->second.Output.PointImpCharge[0] = (curPt->second.impdefcharge - curPt->second.elecindonor + curPt->second.holeinacceptor);
		curPt->second.Output.Jnx[0] = curPt->second.Jn.x * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jpx[0] = curPt->second.Jp.x * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jny[0] = curPt->second.Jn.y * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jpy[0] = curPt->second.Jp.y * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jnz[0] = curPt->second.Jn.z * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.Jpz[0] = curPt->second.Jp.z * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.genth[0] = curPt->second.genth;
		curPt->second.Output.recth[0] = curPt->second.recth;
		curPt->second.Output.R1[0] = curPt->second.srhR1;
		curPt->second.Output.R2[0] = curPt->second.srhR2;
		curPt->second.Output.R3[0] = curPt->second.srhR3;
		curPt->second.Output.R4[0] = curPt->second.srhR4;
		curPt->second.Output.scatter[0] = curPt->second.scatterR;
		curPt->second.Output.Lgen[0] = curPt->second.LightAbsorbGen;
		curPt->second.Output.Lsrh[0] = curPt->second.LightAbsorbSRH;
		curPt->second.Output.Ldef[0] = curPt->second.LightAbsorbDef;
		curPt->second.Output.Lscat[0] = curPt->second.LightAbsorbScat;
		curPt->second.Output.Erec[0] = curPt->second.LightEmitRec;
		curPt->second.Output.Esrh[0] = curPt->second.LightEmitSRH;
		curPt->second.Output.Edef[0] = curPt->second.LightEmitDef;
		curPt->second.Output.Escat[0] = curPt->second.LightEmitScat;
		curPt->second.Output.LPowerEnter[0] = curPt->second.LightPowerEntry;
		curPt->second.Output.LightEmit[0] = curPt->second.LightEmission;
	}

	sim.TransientData->storevalueStartTime = sim.TransientData->iterEndtime;
	sim.binctr = 1;

	return(0);
}

int OutputDataInternalPre(Model& mdl, double timeweight, unsigned int binctr)	//data to be stored before everything moves around
{	
	//all the data regarding as to the current state of the simulation
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		curPt->second.Output.Psi[binctr] = curPt->second.Psi * timeweight;
		curPt->second.Output.Ec[binctr] = curPt->second.cb.bandmin * timeweight;
		curPt->second.Output.Ev[binctr] = curPt->second.vb.bandmin * timeweight;
		curPt->second.Output.fermi[binctr] = curPt->second.fermi * timeweight;
		curPt->second.Output.fermin[binctr] = curPt->second.fermin * timeweight;
		curPt->second.Output.fermip[binctr] = curPt->second.fermip * timeweight;
		curPt->second.Output.CarCB[binctr] = curPt->second.cb.GetNumParticles() * timeweight;
		curPt->second.Output.CarVB[binctr] = curPt->second.vb.GetNumParticles() * timeweight;
		curPt->second.Output.n[binctr] = curPt->second.n * timeweight * curPt->second.voli;
		curPt->second.Output.p[binctr] = curPt->second.p * timeweight * curPt->second.voli;
		curPt->second.Output.rho[binctr] = curPt->second.rho * timeweight;
		curPt->second.Output.PointImpCharge[binctr] = (curPt->second.impdefcharge - curPt->second.elecindonor + curPt->second.holeinacceptor) * timeweight;
	}

	return(0);
}

int OutputDataInternalPost(Model& mdl, Simulation& sim, unsigned int binctr)
{
//	double timeweight = sim.TransientData->timestep;	//end time for the iteration - current time of the simulation
	//essentially delta_time for the iteration, but the timestep may have already changed based on
	//psi changing too much!
	double timeweight = 1.0;
	if(binctr <= 1)
	{
		sim.TransientData->mints = sim.TransientData->timestep;
		sim.TransientData->maxts = sim.TransientData->timestep;
		sim.TransientData->avgts = sim.TransientData->timestep;
		sim.TransientData->timesteps[binctr] = timeweight;
	}
	else
	{
		if (sim.TransientData->timestep < sim.TransientData->mints)
			sim.TransientData->mints = sim.TransientData->timestep;
		if (sim.TransientData->timestep > sim.TransientData->maxts)
			sim.TransientData->maxts = sim.TransientData->timestep;
		sim.TransientData->avgts = sim.TransientData->avgts * (double)binctr + sim.TransientData->timestep;	//get the total timestep in this particular bin
		sim.TransientData->avgts = sim.TransientData->avgts / (double)(binctr + 1);	//now re-average the data
		sim.TransientData->timesteps[binctr] = timeweight;
	}
	for(std::map<long int, Point>::iterator curPt = mdl.gridp.begin(); curPt != mdl.gridp.end(); ++curPt)
	{
		curPt->second.Output.Jnx[binctr] = curPt->second.Jn.x * timeweight * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jpx[binctr] = curPt->second.Jp.x * timeweight * curPt->second.areai.x * QCHARGE;
		curPt->second.Output.Jny[binctr] = curPt->second.Jn.y * timeweight * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jpy[binctr] = curPt->second.Jp.y * timeweight * curPt->second.areai.y * QCHARGE;
		curPt->second.Output.Jnz[binctr] = curPt->second.Jn.z * timeweight * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.Jpz[binctr] = curPt->second.Jp.z * timeweight * curPt->second.areai.z * QCHARGE;
		curPt->second.Output.genth[binctr] = curPt->second.genth * timeweight;
		curPt->second.Output.recth[binctr] = curPt->second.recth * timeweight;
		curPt->second.Output.R1[binctr] = curPt->second.srhR1 * timeweight;
		curPt->second.Output.R2[binctr] = curPt->second.srhR2 * timeweight;
		curPt->second.Output.R3[binctr] = curPt->second.srhR3 * timeweight;
		curPt->second.Output.R4[binctr] = curPt->second.srhR4 * timeweight;
		curPt->second.Output.scatter[binctr] = curPt->second.scatterR * timeweight;
		curPt->second.Output.Lgen[binctr] = curPt->second.LightAbsorbGen * timeweight;
		curPt->second.Output.Lsrh[binctr] = curPt->second.LightAbsorbSRH * timeweight;
		curPt->second.Output.Ldef[binctr] = curPt->second.LightAbsorbDef * timeweight;
		curPt->second.Output.Lscat[binctr] = curPt->second.LightAbsorbScat * timeweight;
		curPt->second.Output.Erec[binctr] = curPt->second.LightEmitRec * timeweight;
		curPt->second.Output.Esrh[binctr] = curPt->second.LightEmitSRH * timeweight;
		curPt->second.Output.Edef[binctr] = curPt->second.LightEmitDef * timeweight;
		curPt->second.Output.Escat[binctr] = curPt->second.LightEmitScat * timeweight;
		curPt->second.Output.LPowerEnter[binctr] = curPt->second.LightPowerEntry * timeweight;
		curPt->second.Output.LightEmit[binctr] = curPt->second.LightEmission * timeweight;

		OutputRatesPoint(curPt->second, &sim);	//debug thorough output. tests flag to do output within function
	}

	return(0);
}

void DisplayProgressStr(std::string str, int check)
{
	switch(check)
	{
		case 0:
			std::cout << "Files will be saved to: " << str << std::endl;
			break;
		default:
			std::cout << str << std::endl;
	}
	return;
}
void DisplayProgress(int check, int num)
{
	unsigned long long int nbyte;
	double bt;
	switch(check)
	{
		case 0:
			std::cout << num;
			break;
		case 1:
			std::cout << "Additional 10% of carriers has been moved. On Point " << num << std::endl;
			break;
		case 2:
			std::cout << "Calculating probabilities of motion around each grid point" << std::endl;
			break;
		case 3:
			std::cout << "Forming approximations for pinpoint charges at long distances" << std::endl;
			break;
		case 4:
			std::cout << "Calculating E-Field at each point in device" << std::endl;
			break;
		case 5:
			std::cout << "Simulation is finished!" << std::endl;
			break;
		case 6:
			std::cout << "Updating Band Edges" << std::endl;
			break;
		case 7:
			std::cout << "Initializing Edges" << std::endl;
			break;
		case 8:
			std::cout << "Starting \"exact\" symbolic precalculation of poisson's equation for all " << num << " points." << std::endl;
			break;
		case 9:
			std::cout << "Finished precalculation of poisson's equation." << std::endl;
			break;
		case 10:
			std::cout << "Calculating Band Edges and new carrier concentrations" << std::endl;
			break;
		case 11:
			std::cout << "Writing data output file: " << num << std::endl;
			break;
		case 12:
			std::cout << "Check trace file for details. Unlikely model will correct itself. Recommend abort program." << std::endl;
			break;
		case 13:
			std::cout << "Eliminated additional 10% of variables from lower equations (#" << num << ")" << std::endl;
			break;
		case 14:
			std::cout << "Additional 10% variables solved. (#" << num << ")" << std::endl;
			break;
		case 15:
			std::cout << "Done moving carriers" << std::endl;
			break;
		case 16:
			std::cout << "Additional 10% of carrier densities calculated. On point " << num << std::endl;
			break;
		case 17:
			std::cout << "Recommended smaller dimensions for model. Tracking over 2 million electrons" <<
				" will significantly decrease the speed." << std::endl;
			break;
		case 18:
			std::cout << "Recommend larger dimensions for model. Less than 1 electron per 100 points." << std::endl;
			std::cout << "Determining carrier density will move slowly." << std::endl;
			break;
		case 19:
			std::cout << "Could not generate a model based on the load file. Load file corrupted or lacking data. Error: " << num << std::endl;
			break;
		case 20:
			std::cout << "Finished initializing mesh data for individual points" << std::endl;
			break;
		case 21:
			std::cout << "Starting mesh reduction iteration with " << num << " points." << std::endl;
			break;
		case 22:
			std::cout << "Finished mesh reduction on priority points within layers." << std::endl;
			break;
		case 23:
			std::cout << "Beginning mesh reduction including smallest priority points" << std::endl;
			break;
		case 24:
			std::cout << "Unknown material distribution found: " << num << std::endl;
			break;
		case 25:
			std::cout << "Done populating individual points in model with data." << std::endl;
			break;
		case 26:
			std::cout << "Carrier was thermally generated." << std::endl;
			break;
		case 27:
			std::cout << "Executing task " << num << std::endl;
			break;
		case 28:
			std::cout << "Starting iteration " << num << ". Time: ";
			break;
		case 29:
			std::cout << num << "." << std::endl;
			break;
		case 30:
			std::cout << "Eliminated " << num << " equations. ";
			break;
		case 31:
			std::cout << "Variables Known: " << num << " ";
			break;
		case 32:
			std::cout << "Populating model with data." << std::endl;
			break;
		case 33:
			std::cout << "Done populating model with data." << std::endl;
			break;
		case 34:
			std::cout << "Simulation beginning iterations." << std::endl;
			break;
		case 35:
			std::cout << " for iteration " << num << std::endl;
			break;
		case 36:
			std::cout << "See trace file for debugging purposes. Entered numerically unstable infinite loop." << std::endl;
			break;
		case 37:
			std::cout << "Loading model description data" << std::endl;
			break;
		case 38:
			std::cout << "Loading model grid point data" << std::endl;
			break;
		case 39:
			std::cout << "Saving entire state of device after iteration " << num << std::endl;
			break;
		case 40:
			std::cout << "Done saving entire state of device." << std::endl;
			break;
		case 41:
			std::cout << "The simulation is toast. Please send your input file to the developer for debugging." << std::endl;
//			std::cin >> check;
			break;
		case 42:
			std::cout << "You are potentially in an infinite loop with task type: " << num << std::endl;
			break;
		case 43:
			std::cout << "Simulation currently does not support shapes beyond lines or points" << std::endl;
			break;
		case 44:
			std::cout << "Generated mesh with " << num << " points." << std::endl;
			break;
		case 45:
			std::cout << num;
			break;
		case 46:
			std::cout << "Generating a guess solution. This may take awhile." << std::endl;
			break;
		case 47:
			std::cout << "Done generating a guess solution. Applying guess to model." << std::endl;
			break;
		case 48:
			std::cout << "Thermal Equilibrium guess is diverging!" << std::endl;
			break;
		case 49:
			std::cout << "Finishing up accept tasks in iteration: " << num << " tasks std::left." << std::endl;;
			break;
		case 50:
			std::cout << "Finished adaptive mesh refinement: " << num << " points in final mesh." << std::endl;
			break;
		case 51:
			std::cout << "Adaptive Mesh refinement started for " << num << " points." << std::endl;
			break;
		case 52:
			std::cout << "Forced Reduction in carriers moved and timing reset" << std::endl;
			break;
		case 53:
			std::cout << "Steady State Calculation. Iteration: " << num << " of ";
			break;
		case 54:
			std::cout << "Processed " << num << " wavelengths in spectrum of ";
			break;
		case 55:
			std::cout << "Processed " << num << " point emissions of ";
			break;
		case 56:
			std::cout << "Starting Steady State calculation with new input values" << std::endl;
			break;
		case 57:
			nbyte =num;
			nbyte = 8*(nbyte+nbyte+nbyte*nbyte);
			if(nbyte > 1073741824)
			{
				bt = double(nbyte)/1073741824.0;
				std::cout << "With " << num << " states, not enough memory to solve steady state solution. Requires " << std::fixed << std::setprecision(2) << bt << "GB  for solving system of equations" << std::endl;
			}
			else if(nbyte > 1048576)
			{
				bt = double(nbyte)/1048576.0;
				std::cout << "With " << num << " states, not enough memory to solve steady state solution. Requires " << std::fixed << std::setprecision(2) << bt << "MB for solving system of equations" << std::endl;
			}
			else if(nbyte > 1024)
			{
				bt = double(nbyte)/1024.0;
				std::cout << "With " << num << " states, not enough memory to solve steady state solution. Requires " << std::fixed << std::setprecision(2) << bt << "KB for solving system of equations" << std::endl;
			}
			else
			{
				std::cout << "With " << num << " states, not enough memory to solve steady state solution. Requires " << std::fixed << std::setprecision(2) << nbyte << " bytes for solving system of equations" << std::endl;
			}
			break;
		case 58:
			std::cout << "Failed to solve system of equations for steady state" << std::endl;
			break;
		case 59:
			std::cout << "Calculating dense Jacobian matrix of size " << num << "x" << num << "  (" << num*num << " elements)" << std::endl;
#ifdef NDEBUG
			num = ((((num*num + (num<<1))<<3))>>20);
#else
			num = ((((num*num + (num<<1)+num)<<3))>>20);
#endif
			std::cout << "Approximate additional memory to solve: ~" << num << "MB" << std::endl;
			break;
		case 60:
			std::cout << num << " elements in Jacobian calculated thus far." << std::endl;
			break;
		case 61:
			std::cout << "Solving the matrix. On variable " << num << std::endl;
			break;
		case 62:
			std::cout << "Back substituting matrix (reverse order). On Varialbe " << num << std::endl;
			break;
		case 63:
			std::cout << "Simulation took " << std::fixed;
			break;
		case 64:
			std::cout << "Updating current output file." << std::endl;
			break;
		case 65:
			std::cout << "Moving onto next set of conditions" << std::endl;
			break;
		case 66:
			std::cout << "Finished environment " << std::fixed << num << " of ";
			break;
		case 67:
			std::cout << num << " after ";
			break;
		case 68:
			std::cout << "Simulation stopped early (outputs generated)" << std::endl;
			break;
		case 69:
			std::cout << "Simulation aborted (no outputs)" << std::endl;
			break;
		case 70:
			std::cout << "Simulation environment stopped early" << std::endl;
			break;
		default:
			std::cout << "Invalid call to DisplayProgress: " << check << std::endl;
			break;
	}
	return;
}

void DisplayProgressDoub(unsigned int message, double value)
{
	bool rest = false;
	switch(message)
	{
		case 1:
			std::cout << "(" << std::fixed << std::setprecision(2) << value << "%)" << std::endl;
			break;
		case 2:
			std::cout << std::setprecision(5) << std::scientific << value << " sec of ";
			break;
		case 3:
			std::cout << std::setprecision(5) << std::scientific << value << "." << std::endl;
			break;
		case 4:
			std::cout << "Timestep currently at: " << std::setprecision(5) << std::scientific << value << std::endl;
			break;
		case 5:
			std::cout << std::scientific << std::setprecision(3) << value << " seconds std::left in iteration of ";
			break;
		case 6:
			std::cout << std::scientific << std::setprecision(3) << value << " seconds on task ";
			break;
		case 7:
			std::cout << "(" << std::fixed << std::setprecision(2) << value*100.0 << "%)" << std::endl;
			break;
		case 8:
			std::cout << "Finding thermal equilibrium. Max difference in Ef is currently " << value << ". Want <= 0.001" << std::endl;
			break;
		case 9:
			std::cout << "Approximately " << std::fixed << std::setprecision(2) << value << "% of the way to calculating thermal equilibrium." << std::endl;
			break;
		case 10:
			std::cout << "Approximately " << std::fixed << std::setprecision(0) << value << " more seconds std::left." << std::endl;
			break;
		case 11:
			std::cout << "Minimizing Ef. Greatest change in band diagram currently: " << std::scientific << std::setprecision(3) << value << std::endl;
			break;
		case 12:
			std::cout << "Saving state at end time " << std::scientific << std::setprecision(8) << value << " seconds." << std::endl;
			break;
		case 13:
			if(value > 1e-5)
				std::cout << "Attempting to move " << std::fixed << std::setprecision(3) << value*100.0 << "% of the carriers." << std::endl;
			else
				std::cout << "Attempting to move " << std::scientific << std::setprecision(3) << value << " of the carriers." << std::endl;
			break;
		case 14:
			std::cout << "SS: Max Net Rate (" << std::scientific << std::setprecision(2) << value << ") - ";
			break;
		case 15:
			std::cout << "Largest Carrier Adjustment (" << std::scientific << std::setprecision(2) << value << ") ";
			break;
		case 16:
			std::cout << "Band Fluctuation (" << std::scientific << std::setprecision(2) << value << ")" << std::endl;
			break;
		case 17:
			std::cout << " %(" << std::scientific << std::setprecision(2) << value << ") - ";
			break;
		case 18:
			std::cout << "Current maximum percent change in occupancy (1.0 = 100%): " << value << std::endl;
			break;
		case 19:
			std::cout << "Simulation took " << value << " seconds to run." << std::endl;
			break;
		case 20:
			//60 seconds = 1 minute
			//3600 seconds = 1 hour
			//86400 seconds = 1 day
			//604800 seconds = 1 week
			if(value >= 604800.0)
			{
				double weeks = floor(value/604800.0);
				value -= weeks * 604800.0;
				std::cout << std::setprecision(0) << std::fixed << weeks << " weeks, ";
				rest = true;
			}
			if(value >= 86400 || rest)
			{
				double days = floor(value/86400.0);
				value -= days * 86400.0;
				std::cout << std::setprecision(0) << std::fixed << days << " days, ";
				rest = true;
			}
			if(value >= 3600 || rest)
			{
				double hours = floor(value/3600);
				value -= hours * 3600.0;
				std::cout << std::setprecision(0) << std::fixed << hours << " hours, ";
				rest = true;
			}
			if(value >= 60.0 || rest)
			{
				double minutes = floor(value/60.0);
				value -= minutes * 60.0;
				if(rest)
					std::cout << std::setprecision(0) << std::fixed << minutes << " minutes, and ";
				else
					std::cout << std::setprecision(0) << std::fixed << minutes << " minutes and ";


			}
			std::cout << std::setprecision(0) << std::fixed << value << " seconds." << std::endl;
			break;
		case 21:
			std::cout << "Tolerance set to " << std::scientific << std::setprecision(2) << value << std::endl;
			break;
		case 22:
			std::cout << "The maximum current supported by: the thermal velocity and number of carriers in a thermally equilibrated contact is roughly " << std::setprecision(4) << std::fixed;
			if (value < 1.0e-12)
				std::cout << value*1.0e15 << " fA" << std::endl;
			else if (value < 1.0e-9)
				std::cout << value*1.0e12 << " pA" << std::endl;
			else if (value < 1.0e-6)
				std::cout << value*1.0e9 << " nA" << std::endl;
			else if (value < 1.0e-3)
				std::cout << value*1.0e6 << " uA" << std::endl;
			else if (value < 1.0)
				std::cout << value*1.0e3 << " mA" << std::endl;
			else if (value > 1.0e3)
				std::cout << value*1.0e-3 << " kA" << std::endl;
			else
				std::cout << value << " A" << std::endl;
			break;
		case 23:
			std::cout << "Decrease the current or increase the number of carriers at the edge (while maintaining the same total current) by a factor of roughly " << std::setprecision(4) << std::scientific << value << std::endl;
			std::cout << "This may be done by increasing the area of the device or the contact doping." << std::endl;
			std::cout << "Simulation proceeding in numerically inconsistent manner. It may crash, seemingly freeze, or output weird results." << std::endl;
			break;
		default:
			std::cout << std::endl;
			break;
	}
	return;
}

int OutputFileList(Simulation* sim, std::vector<std::string>& FileOut, std::vector<std::string>& groups)
{
	std::string fName = sim->OutputFName + ".flist";
	
	std::ofstream file;
	file.open(fName.c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	file << "Simulation Groups" << std::endl;
	for(unsigned int i=0; i<groups.size(); ++i)
		file << groups[i] << std::endl;
	file << std::endl;

	file << "Simulation Output Files" << std::endl;
	for(unsigned int i=0; i<FileOut.size(); ++i)
		file << FileOut[i] << std::endl;
	
	file.close();
	return(0);
}

int OutputContactJV(Simulation* sim, SS_SimData* ss, std::string groupName, int mainCtc, int secCtc)
{
	if (sim->outputs.jvCurves == false)
		return(0);
	bool incY = false;	//should Y and Z data be output for each bit
	bool incZ = false;
	bool incpExt = false;
	bool incnExt = false;

	int ctcSize = sim->contacts.size();
	if(mainCtc < 0 || mainCtc >= ctcSize)	//these are bad contacts
		return(0);

	if(secCtc < 0 || secCtc >= ctcSize)	//these are bad contacts
		return(0);

	//now see if they have good SS environment data

	int CEmainCtc = sim->contacts[mainCtc]->ContactEnvironmentIndex;
	int CEsecCtc = sim->contacts[secCtc]->ContactEnvironmentIndex;

	ctcSize = ss->ExternalStates[0]->ContactEnvironment.size();	//the CEnvironment should be the same all the way through

	if(CEmainCtc < 0 || CEmainCtc >= ctcSize)	//these are bad contacts
		return(0);

	if(CEsecCtc < 0 || CEsecCtc >= ctcSize)	//these are bad contacts
		return(0);

	ctcOutputs* ctM = &(ss->ExternalStates[0]->output.contacts[CEmainCtc]);
	ctcOutputs* ctS = &(ss->ExternalStates[0]->output.contacts[CEsecCtc]);

	//now go through and check to see if there should be any y, z, or exterior data
	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		if(ss->ExternalStates[i]->FileOut == groupName)	//make sure this is an environment that should be used
		{
			if(ss->ExternalStates[i]->output.calculated)
			{
				ctM = &(ss->ExternalStates[i]->output.contacts[CEmainCtc]);
				ctS = &(ss->ExternalStates[i]->output.contacts[CEsecCtc]);

				if(ctM->JnOut.y != 0.0 || ctM->JpOut.y != 0.0 || ctS->JnOut.y != 0.0 || ctS->JpOut.y != 0.0)
					incY=true;
				if(ctM->JnOut.z != 0.0 || ctM->JpOut.z != 0.0 || ctS->JnOut.z != 0.0 || ctS->JpOut.z != 0.0)
					incZ=true;
				if(ctM->pExtEnter != 0.0 || ctS->pExtEnter != 0.0)
					incpExt = true;
				if(ctM->nExtEnter != 0.0 || ctS->nExtEnter != 0.0)
					incnExt = true;
			}
		}
		if(incY && incZ && incpExt && incnExt)	//they're all included, stop looping
			break;
	}

	std::string fName = sim->OutputFName + "_" + groupName + "_" + sim->contacts[mainCtc]->name + "_" + sim->contacts[secCtc]->name + ".jv";
	
	std::ofstream file;
	file.open(fName.c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	sim->OutFiles.push_back(fName);


	file << groupName << std::endl;
	file << "Contact(1): " << sim->contacts[mainCtc]->name << std::endl;
	file << "Contact(2): " << sim->contacts[secCtc]->name << std::endl;
	file << "Voltage = (" << sim->contacts[mainCtc]->name << ") - (" << sim->contacts[secCtc]->name << ")" << std::endl;
	file << "Fermi Level Accuracy: " << std::setprecision(2) << std::scientific << sim->accuracy << std::endl;

	file << std::endl;
	std::string divider;
	int szDiv = 16*15+10;
	if(incY)
		szDiv += 16*12;
	if(incZ)
		szDiv += 16*12;
	if(incnExt)
		szDiv+=32;
	if(incpExt)
		szDiv += 32;

	divider.resize(szDiv,'-');
	file << divider << std::endl;
	file << std::setw(16) << std::right << "Voltage";
	file << std::setw(16) << std::right << "J_ctc_x(1)";
	file << std::setw(16) << std::right << "J_ctc_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "J_ctc_y(1)";
		file << std::setw(16) << std::right << "J_ctc_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "J_ctc_z(1)";
		file << std::setw(16) << std::right << "J_ctc_z(2)";
	}
	file << std::setw(16) << std::right << "J_x(1)";
	file << std::setw(16) << std::right << "J_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "J_y(1)";
		file << std::setw(16) << std::right << "J_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "J_z(1)";
		file << std::setw(16) << std::right << "J_z(2)";
	}

	file << "   | ";

	file << std::setw(16) << std::right << "Voltage_N";
	file << std::setw(16) << std::right << "Jn_ctc_x(1)";
	file << std::setw(16) << std::right << "Jn_ctc_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "Jn_ctc_y(1)";
		file << std::setw(16) << std::right << "Jn_ctc_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "Jn_ctc_z(1)";
		file << std::setw(16) << std::right << "Jn_ctc_z(2)";
	}
	file << std::setw(16) << std::right << "Jn_x(1)";
	file << std::setw(16) << std::right << "Jn_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "Jn_y(1)";
		file << std::setw(16) << std::right << "Jn_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "Jn_z(1)";
		file << std::setw(16) << std::right << "Jn_z(2)";
	}
	

	if(incnExt)
	{
		file << std::setw(16) << std::right << "In_External(1)";
		file << std::setw(16) << std::right << "In_External(2)";
	}

	file << "   | ";

	file << std::setw(16) << std::right << "Voltage_P";
	file << std::setw(16) << std::right << "Jp_ctc_x(1)";
	file << std::setw(16) << std::right << "Jp_ctc_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "Jp_ctc_y(1)";
		file << std::setw(16) << std::right << "Jp_ctc_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "Jp_ctc_z(1)";
		file << std::setw(16) << std::right << "Jp_ctc_z(2)";
	}
	
	file << std::setw(16) << std::right << "Jp_x(1)";
	file << std::setw(16) << std::right << "Jp_x(2)";
	if(incY)
	{
		file << std::setw(16) << std::right << "Jp_y(1)";
		file << std::setw(16) << std::right << "Jp_y(2)";
	}
	if(incZ)
	{
		file << std::setw(16) << std::right << "Jp_z(1)";
		file << std::setw(16) << std::right << "Jp_z(2)";
	}


	if(incpExt)
	{
		file << std::setw(16) << std::right << "Ip_External(1)";
		file << std::setw(16) << std::right << "Ip_External(2)";
	}

	file << std::endl;
	file << divider << std::endl;

	//quick sort the voltages from low to high for easier reading if the user input it all weird
	std::vector<double> Voltages;
	std::vector<unsigned int> Order;
	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		if(ss->ExternalStates[i]->FileOut == groupName)	//make sure this is an environment that should be used
		{
			if(ss->ExternalStates[i]->output.calculated)	//be safe in event simulation stopped early
			{
				Voltages.push_back(ctM->voltageOut - ctS->voltageOut);
				Order.push_back(i);
			}
		}
	}

	unsigned int sz = Voltages.size();
	for(unsigned int i=0; i<sz; ++i)
	{
		for(unsigned int j=i+1; j<sz; ++j)
		{
			if(Voltages[j] < Voltages[i])	//swap 'em
			{
				unsigned int tmpi = Order[j];
				double tmpd = Voltages[j];
				Order[j] = Order[i];
				Voltages[j] = Voltages[i];
				Order[i] = tmpi;
				Voltages[i] = tmpd;
			}
		}
	}

	bool MainCtcPosEnter = false;//, SecCtcPosEnter;	//true = add current, false = subtract current
	double tmpVal;
	for(unsigned int i=0; i<sz; ++i)
	{
			ctM = &(ss->ExternalStates[Order[i]]->output.contacts[CEmainCtc]);
			ctS = &(ss->ExternalStates[Order[i]]->output.contacts[CEsecCtc]);
			//determine the direction flip for x
/* I was being stupid
			if(ctM->voltageOut >= ctS->voltageOut)
			{
				MainCtcPosEnter = false;	//higher voltage = current (pos charge) leaving. Flip the sign
//				SecCtcPosEnter = true;
			}
			else
			{
				MainCtcPosEnter = true;
//				SecCtcPosEnter = false;
			}
*/			
			//primary IV Curve
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->voltageOut - ctS->voltageOut;
			tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.x + ctM->JpOutEnter.x : -(ctM->JnOutEnter.x + ctM->JpOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.x + ctS->JpOutEnter.x : -(ctS->JnOutEnter.x + ctS->JpOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			if(incY)
			{
				tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.y + ctM->JpOutEnter.y : -(ctM->JnOutEnter.y + ctM->JpOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.y + ctS->JpOutEnter.y : -(ctS->JnOutEnter.y + ctS->JpOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}

			if(incZ)
			{
				tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.z + ctM->JpOutEnter.z : -(ctM->JnOutEnter.z + ctM->JpOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.z + ctS->JpOutEnter.z : -(ctS->JnOutEnter.z + ctS->JpOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}

			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.x + ctM->JpOut.x;
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.x + ctS->JpOut.x;
			if(incY)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.y + ctM->JpOut.y;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.y + ctS->JpOut.y;
			}
			if(incZ)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.z + ctM->JpOut.z;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.z + ctS->JpOut.z;
			}
			file << "   | ";	//divider to make life easier
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->voltageNOut - ctS->voltageNOut;

			tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.x : -(ctM->JnOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.x : -(ctS->JnOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			if(incY)
			{
				tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.y : -(ctM->JnOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.y : -(ctS->JnOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}
			if(incZ)
			{
				tmpVal = MainCtcPosEnter ? ctM->JnOutEnter.z : -(ctM->JnOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JnOutEnter.z : -(ctS->JnOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}

			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.x;
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.x;
			if(incY)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.y;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.y;
			}
			if(incZ)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JnOut.z;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JnOut.z;
			}

			if(incnExt)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->nExtEnter;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->nExtEnter;
			}
			file << "   | ";	//divider to make life easier
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->voltagePOut - ctS->voltagePOut;

			tmpVal = MainCtcPosEnter ? ctM->JpOutEnter.x : -(ctM->JpOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			tmpVal = !MainCtcPosEnter ? ctS->JpOutEnter.x : -(ctS->JpOutEnter.x);
			file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

			if(incY)
			{
				tmpVal = MainCtcPosEnter ? ctM->JpOutEnter.y : -(ctM->JpOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JpOutEnter.y : -(ctS->JpOutEnter.y);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}

			if(incZ)
			{
				tmpVal = MainCtcPosEnter ? ctM->JpOutEnter.z : -(ctM->JpOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;

				tmpVal = !MainCtcPosEnter ? ctS->JpOutEnter.z : -(ctS->JpOutEnter.z);
				file << std::setw(16) << std::setprecision(6) << std::right << tmpVal;
			}

			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JpOut.x;
			file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JpOut.x;
			if(incY)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JpOut.y;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JpOut.y;
			}
			if(incZ)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->JpOut.z;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->JpOut.z;
			}
			if(incpExt)
			{
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctM->pExtEnter;
				file << std::setw(16) << std::setprecision(6) << std::right << std::scientific << ctS->pExtEnter;
			}
			file << std::endl;
	}

	file.close();

	return(0);
}

int OutputEnvironmentSummary(ModelDescribe& mdesc, SS_SimData* ss)
{
	std::string fName = mdesc.simulation->OutputFName + ".envin";
	std::string divider;
	int dividerLength=0;
	std::ofstream file;

	file.open(fName.c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	ss->getSim()->OutFiles.push_back(fName);
	//show all contacts, lights, and how they are active in each simulation

	file << mdesc.simulation->OutputFName << " steady state analysis environment summary for each condition" << std::endl;	//just a bit on top saying what it is
	file << "Fermi level accuracy: " << std::setprecision(2) << mdesc.simulation->accuracy << std::endl;
	file << std::endl;

	int numCtc = mdesc.simulation->contacts.size();
	int numSpectra = mdesc.spectrum.size();
	dividerLength = 4+27+21+35*numCtc+21*numSpectra;
	divider.resize(dividerLength,'-');
	file << divider << std::endl;
	file << std::setw(4) << std::left << "ID";
	file << std::setw(27) << std::left << "Name";
	file << std::setw(21) << std::left << "Flags";
	
	for(int i=0; i<numCtc; ++i)
		file << std::setw(35) << std::left << mdesc.simulation->contacts[i]->name;

	for(int i=0; i<numSpectra; ++i)
		file << std::setw(21) << std::left << mdesc.spectrum[i]->name;

	file << std::endl << divider << std::endl;
	int OutInputs;
	Environment* curEnv;
	Environment* ctcEnv;
	Environment* ltEnv;
	SSContactData* curCtc;
	std::string tmpStr;
	int extSz = ss->ExternalStates.size();
	OutInputs = CONTACT_INACTIVE;
	bool incSuppression = false;

	for(int i=0; i<extSz; ++i)
	{
		
		curEnv = ss->ExternalStates[i];

		file << std::setw(4) << std::left << std::fixed << ss->ExternalStates[i]->id;
		file << std::setw(27) << std::left << ss->ExternalStates[i]->FileOut;
		if(ss->ExternalStates[i]->flags==0)
			file << std::setw(21) << std::left << "None";
		else
			file << std::setw(21) << std::left << std::fixed << ss->ExternalStates[i]->flags;

		int AllContactActive = CONTACT_INACTIVE;

		if(ss->ExternalStates[i]->reusePrevContactEnvironment >= 0 && ss->ExternalStates[i]->reusePrevContactEnvironment < extSz)
			ctcEnv = (ss->ExternalStates[ss->ExternalStates[i]->reusePrevContactEnvironment]);
		else
			ctcEnv = curEnv;

		if(ss->ExternalStates[i]->reusePrevLightEnvironment >= 0 && ss->ExternalStates[i]->reusePrevLightEnvironment < extSz)
			ltEnv = (ss->ExternalStates[ss->ExternalStates[i]->reusePrevLightEnvironment]);
		else
			ltEnv = curEnv;


		for(int j=0; j<numCtc; ++j)
		{
			int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
			int envSz = ctcEnv->ContactEnvironment.size();
			if(envInd <= -1 || envInd >= envSz)
			{
				file << std::setw(35) << std::left << "Inactive";
				continue;
			}
			curCtc = ctcEnv->ContactEnvironment[envInd];
			if(curCtc->currentActiveValue == CONTACT_INACTIVE || curCtc->currentActiveValue == CONTACT_SS)
			{
				file << std::setw(35) << std::left << "Inactive";
				continue;
			}
			int act = curCtc->currentActiveValue;
			tmpStr="";	//get it null
			if(act & CONTACT_SINK)
			{
				tmpStr += "T";
				AllContactActive = AllContactActive | CONTACT_SINK;
			}
			if(act & CONTACT_CONNECT_EXTERNAL)
			{
				tmpStr += "J";
				AllContactActive = AllContactActive | CONTACT_CONNECT_EXTERNAL;
			}
			if(act & CONTACT_VOLT_LINEAR)
			{
				if(curCtc->suppressVoltagePoisson)
				{
					tmpStr += "(S";
					incSuppression = true;
				}
				tmpStr += "V";
				if(curCtc->suppressVoltagePoisson)
					tmpStr += ")";
				AllContactActive = AllContactActive | CONTACT_VOLT_LINEAR;
			}
			if(act & CONTACT_CURRENT_LINEAR)
			{
				if(curCtc->suppressCurrentPoisson)
				{
					tmpStr += "(S";
					incSuppression = true;
				}
				tmpStr += "C";
				if(curCtc->suppressCurrentPoisson)
					tmpStr += ")";
				AllContactActive = AllContactActive | CONTACT_CURRENT_LINEAR;
			}
			if(act & CONTACT_CUR_DENSITY_LINEAR)
			{
				if(curCtc->suppressCurrentPoisson)
				{
					tmpStr += "(S";
					incSuppression = true;
				}
				tmpStr += "D";
				if(curCtc->suppressCurrentPoisson)
					tmpStr += ")";
				AllContactActive = AllContactActive | CONTACT_CUR_DENSITY_LINEAR;
			}
			if(act & CONTACT_EFIELD_LINEAR)
			{
				if(curCtc->suppressEFieldPoisson)
					tmpStr += "(S";
				tmpStr += "E";
				if(curCtc->suppressEFieldPoisson)
					tmpStr += ")";
				AllContactActive = AllContactActive | CONTACT_EFIELD_LINEAR;
			}
			if(act & CONTACT_CURRENT_LINEAR_ED)
			{
				tmpStr += "(OC)";
				AllContactActive = AllContactActive | CONTACT_CURRENT_LINEAR_ED;
			}
			if(act & CONTACT_CUR_DENSITY_LINEAR_ED)
			{
				tmpStr += "(OD)";
				AllContactActive = AllContactActive | CONTACT_CUR_DENSITY_LINEAR_ED;
			}
			if(act & CONTACT_EFIELD_LINEAR_ED)
			{
				tmpStr += "(OE)";
				AllContactActive = AllContactActive | CONTACT_EFIELD_LINEAR_ED;
			}
			file << std::setw(35) << std::left << tmpStr;
		}
		bool active;
		for(int j=0; j<numSpectra; ++j)
		{
			//see if this index is located in the active light environment
			active = false;
			for(unsigned int k=0; k<ltEnv->ActiveLightEnvironment.size(); ++k)
			{
				if(ltEnv->ActiveLightEnvironment[k] == j)
				{
					active=true;
					break;
				}
			}
			if(active)
				file << std::setw(21) << std::left << "On";
			else
				file << std::setw(21) << std::left << "Off";
		}
		file << std::endl;

		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_VOLT_LINEAR)
		{
			file << std::setw(52) << std::left << "Voltage Input (V)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_VOLT_LINEAR)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->voltage;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}
		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_CURRENT_LINEAR)
		{
			file << std::setw(52) << std::left << "Current Input (A)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_CURRENT_LINEAR)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->current;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}
		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_CUR_DENSITY_LINEAR)
		{
			file << std::setw(52) << std::left << "Current Density Input (A/cm^2)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_CUR_DENSITY_LINEAR)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->currentDensity;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}

		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_EFIELD_LINEAR)
		{
			file << std::setw(52) << std::left << "Electric Field Input (V/cm)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_EFIELD_LINEAR)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->eField;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}

		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_CURRENT_LINEAR_ED)
		{
			file << std::setw(52) << std::left << "External Current Input (A)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_CURRENT_LINEAR_ED)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->current_ED;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}

		//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_CUR_DENSITY_LINEAR_ED)
		{
			file << std::setw(52) << std::left << "External Current Density Input (A/cm^2)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_CUR_DENSITY_LINEAR_ED)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->currentDensity_ED;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}
//now put the given inputs if it had one to give
		if(AllContactActive & CONTACT_EFIELD_LINEAR_ED)
		{
			file << std::setw(52) << std::left << "External Electric Field Input (V/cm)";	//all the space before getting to the contact inputs
			for(int j=0; j<numCtc; ++j)
			{
				int envInd = mdesc.simulation->contacts[j]->ContactEnvironmentIndex;
				int envSz = ctcEnv->ContactEnvironment.size();
				if(envInd <= -1 || envInd >= envSz)
				{
					file << std::setw(35) << std::left << "Undefined";	//just fill in a bunch of spaces
					continue;
				}
				curCtc = ctcEnv->ContactEnvironment[envInd];
				if(curCtc->currentActiveValue & CONTACT_EFIELD_LINEAR_ED)
					file << std::setw(35) << std::left << std::setprecision(6) << std::scientific << curCtc->eField_ED;
				else
					file << std::setw(35) << "Undefined";
			}
			file << std::endl;
		}
		OutInputs = OutInputs | AllContactActive;
		file << divider << std::endl;
	}	//loop through all the external states

	file << std::endl;
	if(OutInputs & CONTACT_SINK)
		file << "T = Contact Thermal Bath" << std::endl;
	if(OutInputs & CONTACT_CONNECT_EXTERNAL)
		file << "J = Calculate Contact Current Exit Device" << std::endl;
	if(OutInputs & CONTACT_VOLT_LINEAR)
		file << "V = Voltage Input" << std::endl;
	if(OutInputs & CONTACT_CURRENT_LINEAR)
		file << "C = Current Input" << std::endl;
	if(OutInputs & CONTACT_CUR_DENSITY_LINEAR)
		file << "D = Current Density Input" << std::endl;
	if(OutInputs & CONTACT_EFIELD_LINEAR)
		file << "E = Electric Field Input" << std::endl;
	if(OutInputs & (CONTACT_CURRENT_LINEAR_ED | CONTACT_CUR_DENSITY_LINEAR_ED | CONTACT_EFIELD_LINEAR_ED))
		file << "(O C/D/E) = Interjecting input from outside the device (such as an electron beam injection)" << std::endl;
	if(incSuppression)
		file << "(S V/C/D/E) = This input was suppressed when calculating the band structure" << std::endl;

	file.close();

	return(0);
}

int OutputEnvironments(Simulation* sim, SS_SimData* ss)
{
	if(ss==NULL)
		return(0);

	if(ss->ExternalStates.size() == 0)
		return(0);

	std::string fName = sim->OutputFName + ".envout";

	std::ofstream file;
	
	file.clear();
	file.open(fName.c_str(), std::fstream::out);
	if(file.is_open() == false)
		return(-1);

	sim->OutFiles.push_back(fName);
	std::string divider;

	bool doY = false;
	bool doZ = false;
	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		Environment* env = ss->ExternalStates[i];
		for(unsigned int j=0; j<env->output.contacts.size(); ++j)
		{
			ctcOutputs* curOut = &(env->output.contacts[j]);
			if(curOut->JnOut.y != 0.0)
				doY=true;
			if(curOut->JpOut.y != 0.0)
				doY=true;
			if(curOut->JnOut.z != 0.0)
				doZ=true;
			if(curOut->JpOut.z != 0.0)
				doZ=true;
			if(doY && doZ)
				break;
		}
		if(doY && doZ)
			break;
	}
	int divsz=30+16*5;
	
	file << std::setw(20) << std::right << "Name" << " ";
	file << std::setw(4) << std::right << "ID" << " ";
	file << std::setw(16) << std::right << "Voltage" << " ";
	file << std::setw(16) << std::right << "Voltage_n" << " ";
	file << std::setw(16) << std::right << "Voltage_p" << " ";
	file << std::setw(16) << std::right << "Jn_x" << " ";
	file << std::setw(16) << std::right << "Jp_x";
	if(doY)
	{
		file << " " << std::setw(16) << std::right << "Jn_y";
		file << " " <<  std::setw(16) << std::right << "Jp_y";
		divsz+=34;
	}
	if(doZ)
	{
		file << " " <<  std::setw(16) << std::right << "Jn_z";
		file << " " <<  std::setw(16) << std::right << "Jp_z";
		divsz+=34;
	}
	file << std::endl;
	divider.resize(divsz,'-');
	file << divider << std::endl;
	for(unsigned int i=0; i<ss->ExternalStates.size(); ++i)
	{
		Environment* env = ss->ExternalStates[i];
		file << std::setw(20) << std::right << env->FileOut << " ";
		file << std::setw(4) << std::right << env->id << " " << std::endl;

		for(unsigned int j=0; j<env->output.contacts.size(); ++j)
		{
			ctcOutputs* curOut = &(ss->ExternalStates[i]->output.contacts[j]);
			if(unsigned int(ss->ExternalStates[i]->reusePrevContactEnvironment) < ss->ExternalStates.size())
				file << std::setw(20) << std::right << ss->ExternalStates[ss->ExternalStates[i]->reusePrevContactEnvironment]->ContactEnvironment[curOut->ctcID]->ctc->name << " ";
			else
				file << std::setw(20) << std::right << env->ContactEnvironment[curOut->ctcID]->ctc->name << " ";
			file << std::setw(4) << std::right << std::fixed << std::setprecision(0) << curOut->ctcID << " ";
			file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->voltageOut << " ";
			file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->voltageNOut << " ";
			file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->voltagePOut << " ";
			file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JnOut.x << " ";
			file << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JpOut.x;
			if(doY)
			{
				file << " " << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JnOut.y;
				file << " " << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JpOut.y;
			}
			if(doZ)
			{
				file << " " << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JnOut.z;
				file << " " << std::setw(16) << std::right << std::scientific << std::setprecision(6) << curOut->JpOut.z;
			}
			file << std::endl;
		}
		file << divider << std::endl;
	}

	file.close();

	return(0);
}








DataOut::DataOut()
{
	mean = 0.0;
	variance = 0.0;
	stddev = 0.0;
	standarderror = 0.0;
}