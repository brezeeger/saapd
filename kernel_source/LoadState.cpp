#include "LoadState.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>

#include "model.h"
#include "contacts.h"
#include "output.h"
#include "GenerateModel.h"
#include "contacts.h"
#include "light.h"
#include "transient.h"
#include "ss.h"
#include "srh.h"
#include "PopulateModel.h"

int LoadState(std::string fname, Model& mdl, ModelDescribe& mdesc)
{

	std::ifstream file;	//the massive file to be loaded...
	std::stringstream myStream;	//a buffer stream for each line
	std::string buffer;	//used to load each line in
	DataLinks postProcess;
	unsigned int loadversion=0;	//used to keep track of changes in the load file, and make sure it is possible to load it!
	file.open(fname.c_str(), std::fstream::in);
	if(file.is_open() == false)
		return(-1);

	//make sure everything is empty...
	mdesc.ClearAll();
	mdl.gridp.clear();

	myStream.clear();
	myStream.str("");

	//first, make sure this is the expected file type

	getline(file, buffer);
	myStream << buffer;	//put the entire contents of the line into the file
	myStream >> buffer;
	if(buffer != "SAAPDState")
	{
		file.close();
		return(-1);
	}
	myStream >> loadversion;
	if(loadversion > 1)	//1 is the current load version compatibility
	{
		file.close();
		return(-1);
	}

	//the rest of this stream is just for the user's benefit when reading it.

	//notes about std::stringstream:
	/*
	must clear the flag before inputting as when it reaches the end of the buffer, it gets set to bad.
	put a std::string in via stream << string;
	To display everything in the buffer stream.str() --- this will include old stuff, pointer of data is moved.
	to put the stream into a variable: stream >> variable --- NOT variable << stream.
	A stream cannot be passed into a stream. It will only pass the variable reference.
	using the .str() does not move the pointer along.

	best way to load data:
	getline(filestream, stringbffer).
	clear stream flags .clear();
	empty stream contents .str("");
	stream << buffer;	this will pass the ENTIRE std::string over
	stream >> buffer;	this will put the first term back in the buffer(killing everything else in buffer)
	a stream hsa the bad flag set of eof flag set when it reads the last item in it. So if there's one item, and you read it, the system is now bad.

	testfing to see if stream is safe
	stream.good() ... so loop to read data is while(filestream.good()) { getline(filestream, buffer); }

	*/

	while(file.good())
	{
		myStream.clear();	//clear all flags (such as when read last viable operation
		myStream.str("");	//clear out the data in the stream - otherwise it would get very large...
		getline(file, buffer);
		myStream << buffer;
		myStream >> buffer;	//figure out what kind of data to start loading
		if(buffer == "ModelDescription")	//get ready to do mdesc loading...
		{
			DisplayProgress(37,0);
			LoadMDescData(file, mdesc, postProcess);
			CheckModel(mdesc);	//get things all verified/processsed/setup good. This needs to be done before the model data is loaded as it sets the max optical size, which is needed when the points are loaded for optical output data
			//there is no other data attached to this buffer
		}
		else if(buffer == "Model")
		{
			DisplayProgress(38,0);
			LoadModelData(file, mdesc, mdl, myStream, postProcess);
		}
		else if(buffer == "End")
			break;
	}

	file.close();
	//now need to and get everything to match up!
	LinkID(mdesc);	//make sure all the ID's link up for the layers, impurities, materials, etc
	LinkPoints(mdl, mdesc, postProcess);	//put the mesh back together
//	SetandCalcVars(mdesc, mdl); currently just return(0);	//do any calculations that could not be completed beforehand
//	CheckModel(mdesc);	//make sure all the values given are valid
	

	return(1);
}

int LinkPoints(Model& mdl, ModelDescribe& mdesc, DataLinks postP)
{
	std::map<long int, Point>::iterator ptNode;
	Point* curPt = NULL;
	for(std::vector<PointLinks>::iterator it = postP.PtLink.begin(); it != postP.PtLink.end(); it++)
	{
		ptNode = mdl.gridp.find(it->self);
		if(ptNode != mdl.gridp.end())
			curPt = &(ptNode->second);
		else
			continue;	//shouldn't happen... but you never know

		ptNode = mdl.gridp.find(it->adjP);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjP = &(ptNode->second);
		else
			curPt->adj.adjP = NULL;

		ptNode = mdl.gridp.find(it->adjN);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjN = &(ptNode->second);
		else
			curPt->adj.adjN = NULL;

		ptNode = mdl.gridp.find(it->adjMX);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjMX = &(ptNode->second);
		else
		{
			curPt->adj.adjMX = NULL;
			mdl.boundaries.push_back(it->self);
		}

		ptNode = mdl.gridp.find(it->adjMX2);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjMX2 = &(ptNode->second);
		else
			curPt->adj.adjMX2 = NULL;

		ptNode = mdl.gridp.find(it->adjPX);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjPX = &(ptNode->second);
		else
		{
			curPt->adj.adjPX = NULL;
			mdl.boundaries.push_back(it->self);
		}

		ptNode = mdl.gridp.find(it->adjPX2);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjPX2 = &(ptNode->second);
		else
			curPt->adj.adjPX2 = NULL;

		ptNode = mdl.gridp.find(it->adjMY);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjMY = &(ptNode->second);
		else
			curPt->adj.adjMY = NULL;

		ptNode = mdl.gridp.find(it->adjMY2);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjMY2 = &(ptNode->second);
		else
			curPt->adj.adjMY2 = NULL;

		ptNode = mdl.gridp.find(it->adjPY);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjPY = &(ptNode->second);
		else
			curPt->adj.adjPY = NULL;

		ptNode = mdl.gridp.find(it->adjPY2);
		if(ptNode != mdl.gridp.end())
			curPt->adj.adjPY2 = &(ptNode->second);
		else
			curPt->adj.adjPY2 = NULL;

		int sz = it->AngleData.size();
		for(int j=0; j<sz; j++)
		{
			ptNode = mdl.gridp.find(it->AngleData[j]);
			if(ptNode != mdl.gridp.end())
				curPt->AngleData[j].targetPt = &(ptNode->second);
			else
				curPt->AngleData[j].targetPt = NULL;
		}
		it->AngleData.clear();

	}
	postP.PtLink.clear();

	unsigned int sz = postP.SpLink.size();
	unsigned int specsz = mdesc.spectrum.size();
	for(unsigned int i=0; i<sz; i++)
	{
		ptNode = mdl.gridp.find(postP.SpLink[i].ptID);
		if(ptNode != mdl.gridp.end())
		{
			for(unsigned int j=0; j<specsz; j++)
			{
				if(mdesc.spectrum[j]->ID == postP.SpLink[i].specID)
				{
					mdesc.spectrum[j]->IncidentPoints.push_back(&(ptNode->second));	//link the spectrum to the point
					break;	//get out of for loop and move on to next link
				}	//end if they match
			}	//end loop through all spectrums
		}	//end if this link goes to a valid point
	}	//end loop through all spectrum point links
	postP.SpLink.clear();

	sz = postP.PsiCF.size();
	for(unsigned int i=0; i<sz; i++)
	{
		ptNode = mdl.gridp.find(postP.PsiCF[i].ptID);
		if(ptNode != mdl.gridp.end())
		{
			curPt = &(ptNode->second);
			unsigned int szPt = postP.PsiCF[i].values.size();
			for(unsigned int j=0; j < szPt; j++)
			{
				ptNode = mdl.gridp.find(postP.PsiCF[i].values[j].pt);
				if(ptNode != mdl.gridp.end())
				{
					curPt->PsiCf[j].data = postP.PsiCF[i].values[j].data;
					curPt->PsiCf[j].pt = &(ptNode->second);
					if(fabs(curPt->PsiCf[j].data) > curPt->largestPsiCf)
						curPt->largestPsiCf = fabs(curPt->PsiCf[j].data);
				}
			}
		}
		postP.PsiCF[i].values.clear();
	}
	postP.PsiCF.clear();

	//now do all the different contacts

	sz = postP.CLinks.size();
	unsigned int CSize = mdesc.simulation->contacts.size();
	int sourceIndex, count;
	ContactLink tmpLink;
	for(unsigned int link=0; link<sz; link++)
	{
		count = 0;
		tmpLink.targetContact = NULL;
		for(unsigned int i=0; i < CSize; i++)
		{
			if(mdesc.simulation->contacts[i]->id == postP.CLinks[link].srcID)
			{
				count = count | 1;	//found the correct source index
				sourceIndex = i;
			}
			if(mdesc.simulation->contacts[i]->id == postP.CLinks[link].trgID)
			{
				count = count | 2;
				tmpLink.targetContact = mdesc.simulation->contacts[i];
			}
			if(count == 3)	//found both source and target, get out of loop
				break;
		}

		if(count == 3)	//make sure it's valid!
		{
			tmpLink.current = postP.CLinks[link].current;
			tmpLink.resistance = postP.CLinks[link].resistance;
			mdesc.simulation->contacts[sourceIndex]->AddCLink(tmpLink);
		}

	}

	//fill in all the target data
	for(std::map<long int, Point>::iterator pt = mdl.gridp.begin(); pt != mdl.gridp.end(); ++pt)
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator cb = pt->second.cb.energies.begin(); cb != pt->second.cb.energies.end(); ++cb)
			FindAllTargets(cb->second, &(pt->second), mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator vb = pt->second.vb.energies.begin(); vb != pt->second.vb.energies.end(); ++vb)
			FindAllTargets(vb->second, &(pt->second), mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->second.donorlike.begin(); es != pt->second.donorlike.end(); ++es)
			FindAllTargets(es->second, &(pt->second), mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->second.acceptorlike.begin(); es != pt->second.acceptorlike.end(); ++es)
			FindAllTargets(es->second, &(pt->second), mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->second.CBTails.begin(); es != pt->second.CBTails.end(); ++es)
			FindAllTargets(es->second, &(pt->second), mdl);
		for(std::multimap<MaxMin<double>,ELevel>::iterator es = pt->second.VBTails.begin(); es != pt->second.VBTails.end(); ++es)
			FindAllTargets(es->second, &(pt->second), mdl);

		//test loading of thermalization data
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_CB)
		{
			GroupThermalizationData tmp;
			pt->second.ThermData.push_back(tmp);
			pt->second.ThermData.back().Initialize(THERMALIZE_CB, mdesc.simulation->EnabledRates.Thermalize, &pt->second);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_CBT)
		{
			GroupThermalizationData tmp;
			pt->second.ThermData.push_back(tmp);
			pt->second.ThermData.back().Initialize(THERMALIZE_CBT, mdesc.simulation->EnabledRates.Thermalize, &pt->second);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_VB)
		{
			GroupThermalizationData tmp;
			pt->second.ThermData.push_back(tmp);
			pt->second.ThermData.back().Initialize(THERMALIZE_VB, mdesc.simulation->EnabledRates.Thermalize, &pt->second);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_VBT)
		{
			GroupThermalizationData tmp;
			pt->second.ThermData.push_back(tmp);
			pt->second.ThermData.back().Initialize(THERMALIZE_VBT, mdesc.simulation->EnabledRates.Thermalize, &pt->second);
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_DON)
		{
			GroupThermalizationData tmp;
			for(unsigned int i=0; i<pt->second.impurity.size(); ++i)
			{
				if(pt->second.impurity[i]->isCBReference())
				{
					pt->second.ThermData.push_back(tmp);
					pt->second.ThermData.back().Initialize(THERMALIZE_DON, mdesc.simulation->EnabledRates.Thermalize, &pt->second, pt->second.impurity[i]);
				}
			}
			for(unsigned int i=0; i<pt->second.MatProps->defects.size(); ++i)
			{
				if(pt->second.MatProps->defects[i]->isCBReference())
				{
					pt->second.ThermData.push_back(tmp);
					pt->second.ThermData.back().Initialize(THERMALIZE_DON, mdesc.simulation->EnabledRates.Thermalize, &pt->second, pt->second.MatProps->defects[i]);
				}
			}
		}
		if(mdesc.simulation->EnabledRates.Thermalize & THERMALIZE_ACP)
		{
			GroupThermalizationData tmp;
			for(unsigned int i=0; i<pt->second.impurity.size(); ++i)
			{
				if(pt->second.impurity[i]->isVBReference())
				{
					pt->second.ThermData.push_back(tmp);
					pt->second.ThermData.back().Initialize(THERMALIZE_ACP, mdesc.simulation->EnabledRates.Thermalize, &pt->second, pt->second.impurity[i]);
				}
			}
			for(unsigned int i=0; i<pt->second.MatProps->defects.size(); ++i)
			{
				if(pt->second.MatProps->defects[i]->isVBReference())
				{
					pt->second.ThermData.push_back(tmp);
					pt->second.ThermData.back().Initialize(THERMALIZE_ACP, mdesc.simulation->EnabledRates.Thermalize, &pt->second, pt->second.MatProps->defects[i]);
				}
			}
		}
	}

	mdesc.simulation->mdlData = &mdl;

	//now go through all the contacts and create any external points necessary for boundary conditions
	for(std::vector<Contact*>::iterator ctc = mdesc.simulation->contacts.begin(); ctc != mdesc.simulation->contacts.end(); ++ctc)
		CreateExternalContactPoints(*ctc, mdl);


	return(0);
}

int SetandCalcVars(ModelDescribe& mdesc, Model& mdl)
{
	//not much anything right now...
	return(0);
}

int LoadModelData(std::ifstream& file, ModelDescribe& mdesc, Model& mdl, std::stringstream& str, DataLinks& dLink)
{
	std::string buffer;
	int tempint;

	str >> mdl.numpoints;
	str >> mdl.NDim;
	str >> tempint;
	
	if(tempint == 1)
		mdl.disableTunneling = true;
	else
		mdl.disableTunneling = false;

	str >> mdl.width.x;
	str >> mdl.width.y;
	str >> mdl.width.z;

	str.clear();
	str.str("");
	getline(file, buffer);	//load the next line
	str << buffer;
	str >> tempint;
	if(tempint == 1)
	{
		mdl.PoissonDependencyCalculated = true;
		
		delete [] mdl.poissonRho;
		mdl.poissonRho = new double[mdl.numpoints];
		for(unsigned int i=0; i<mdl.numpoints; i++)
			str >> mdl.poissonRho[i];
	}
	else
	{
		mdl.PoissonDependencyCalculated = false;
		delete [] mdl.poissonRho;
		mdl.poissonRho = NULL;
	}

	//clear out all the old memory if needed
	mdl.gridp.clear();

	while(file.good())
	{
		str.clear();
		str.str("");
		getline(file, buffer);	//load the next line
		str << buffer;
		str >> buffer;

		if(buffer == "End_Model")	//done...
			return(0);
		else if(buffer == "Point")
			LoadPointData(file, mdesc, mdl, str, dLink);
	}

	mdl.MergeSplitPoints.clear();


	return(0);
}

int LoadPointData(std::ifstream& file, ModelDescribe& mdesc, Model& mdl, std::stringstream& str, DataLinks& PtrData)
{
	std::string buffer;
	unsigned long int ID;
	int tempint;

	str >> ID;
	Point tmppt(mdl);
	PointLinks link;

	std::map<long int, Point>::iterator ptIter = mdl.gridp.insert(std::pair<long int, Point>(ID, tmppt)).first;
	Point* pt = &(ptIter->second);

	pt->adj.self = ID;
	pt->adj.adjP = NULL;
	pt->adj.adjN = NULL;

	pt->adj.adjMX = NULL;
	pt->adj.adjMX2 = NULL;
	pt->adj.adjPX = NULL;
	pt->adj.adjPX2 = NULL;
	pt->adj.adjMY = NULL;
	pt->adj.adjMY2 = NULL;
	pt->adj.adjPY = NULL;
	pt->adj.adjPY2 = NULL;
	pt->adj.adjMZ = NULL;
	pt->adj.adjMZ2 = NULL;
	pt->adj.adjPZ = NULL;
	pt->adj.adjPZ2 = NULL;

	link.self = pt->adj.self;
	str >> link.adjN;
	str >> link.adjP;
	str >> link.adjMX;
	str >> link.adjMX2;
	str >> link.adjPX;
	str >> link.adjPX2;
	str >> link.adjMY;
	str >> link.adjMY2;
	str >> link.adjPY;
	str >> link.adjPY2;

	str >> tempint;
	if(tempint == 1)
		pt->Output.DetailedOptical = true;
	else
		pt->Output.DetailedOptical = false;

	PtrData.PtLink.push_back(link);
		
		
	str.clear();	//load the next line. position stuff
	str.str("");
	getline(file, buffer);
	str << buffer;

	str >> pt->position.x;
	str >> pt->position.y;
	str >> pt->position.z;
	str >> pt->pxpy.x;
	str >> pt->pxpy.y;
	str >> pt->pxpy.z;
	str >> pt->mxmy.x;
	str >> pt->mxmy.y;
	str >> pt->mxmy.z;
	str >> pt->sizeScaling;
	
	pt->width.x = pt->pxpy.x - pt->mxmy.x;
	pt->width.y = pt->pxpy.y - pt->mxmy.y;
	pt->width.z = pt->pxpy.z - pt->mxmy.z;
	
	pt->area.x = pt->width.y * pt->width.z;	//same for calculating various densities...
	pt->area.y = pt->width.x * pt->width.z;
	pt->area.z = pt->width.y * pt->width.x;

	pt->vol =pt->width.x * pt->width.y * pt->width.z;	//there are waaay to many calls to Volume() to end up dividing by volume
	pt->vol = pt->vol * pt->sizeScaling;
	
	pt->EField.x = pt->EField.y = pt->EField.z = 0.0;

	if(pt->width.x != 0.0)
		pt->widthi.x = 1.0 / pt->width.x;
	else
		pt->widthi.x = 0.0;
	if(pt->width.y != 0.0)
		pt->widthi.y = 1.0 / pt->width.y;
	else
		pt->widthi.y = 0.0;
	if(pt->width.z != 0.0)
		pt->widthi.z = 1.0 / pt->width.z;
	else
		pt->widthi.z = 0.0;
	if(pt->vol != 0.0)
		pt->voli = 1.0 / pt->vol;			//so just store that number...
	else
		pt->voli = 0.0;
	if(pt->area.x != 0.0)
		pt->areai.x = 1.0 / pt->area.x;
	else
		pt->areai.x = 0.0;
	if(pt->area.y != 0.0)
		pt->areai.y = 1.0 / pt->area.y;
	else
		pt->areai.y = 0.0;
	if(pt->area.z != 0.0)
		pt->areai.z = 1.0 / pt->area.z;
	else
		pt->areai.z = 0.0;

	str.clear();	//load the next line. misc. stuff
	str.str("");
	getline(file, buffer);
	str << buffer;

	str >> pt->Psi;
	str >> pt->eg;
	str >> pt->eps;
	str >> pt->niCarrier;
	str >> pt->n;
	str >> pt->p;
	str >> pt->rho;
	str >> pt->fermi;
	str >> pt->fermin;
	str >> pt->fermip;
	str >> pt->temp;
	str >> pt->heat;
	str >> pt->mintime.x;
	str >> pt->mintime.y;
	str >> pt->mintime.z;
	str >> pt->scatterCBConst;
	str >> pt->scatterVBConst;

	pt->Output.Deallocate();	//make sure it is empty and cleared out.
	
	str.clear();	//load the next line
	str.str("");
	getline(file, buffer);
	str << buffer;
	str >> buffer;

	if(buffer == "PtMatl")
	{
		str >> tempint;
		for(unsigned int i=0; i<mdesc.materials.size(); i++)
		{
			if(tempint == mdesc.materials[i]->id)
			{
				pt->MatProps = mdesc.materials[i];
				break;
			}
		}

		str.clear();	//load the next line.
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtImp")
	{
		while(str.good())
		{
			str >> tempint;
			for(unsigned int i=0; i<mdesc.impurities.size(); i++)
			{
				if(mdesc.impurities[i]->isImpurity(tempint))
				{
					pt->impurity.push_back(mdesc.impurities[i]);
					break;
				}
			}
		}
		str.clear();	//load the next line
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtCt")
	{
		str >> tempint;
		for(unsigned int i=0; i<mdesc.simulation->contacts.size(); i++)
		{
			if(tempint == mdesc.simulation->contacts[i]->id)
			{
				pt->contactData = mdesc.simulation->contacts[i];
				pt->contactData->point.push_back(pt);
				break;
			}
		}
		str.clear();	//load the next line. 
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtPoisDep")
	{
		str >> pt->pDsize;
		delete [] pt->poissonDependency;

		pt->poissonDependency = new PtData[pt->pDsize];
		
		for(unsigned int ctr = 0; ctr < pt->pDsize; ctr++)
		{
			str >> pt->poissonDependency[ctr].pt;
			str >> pt->poissonDependency[ctr].data;
		}

		str.clear();	//load the next line. 
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtPsiCoeff")	//just to maintain some compatibility
	{
		str.clear();	//load the next line. 
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtPsiCf")
	{
		unsigned int ctr;
		str >> ctr;
		PtData tmpData;
		PsiCFLinks tmp;

		pt->PsiCf.clear();
		pt->PsiCf.resize(ctr);
		pt->largestPsiCf = 0.0;		
		PtrData.PsiCF.push_back(tmp);
		unsigned int last = PtrData.PsiCF.size()-1;
		PtrData.PsiCF[last].ptID = ID;
		PtrData.PsiCF[last].values.resize(ctr);
		for(unsigned int i=0; i<ctr; i++)
		{
			str >> tmpData.pt;
			str >> tmpData.data;
			PtrData.PsiCF[last].values[i] = tmpData;

		}

		str.clear();	//load the next line. 
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "PtAngle")
	{
		unsigned int ctr=0;
		unsigned int temppt;
		angleMatch ang;
		while(str.good())
		{
			str >> temppt;
			link.AngleData.push_back(temppt);
			str >> ang.theta[0];
			str >> ang.theta[1];
			str >> ang.theta[2];
			str >> ang.theta[3];
			str >> ang.sin01;
			str >> ang.cos01;
			str >> ang.phi[0];
			str >> ang.phi[1];
			str >> ang.phi[2];
			str >> ang.phi[3];
			str >> ang.proportion;
			pt->AngleData.push_back(ang);
			ctr++;
		}

		str.clear();	//load the next line. likely CB
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "CB")
		LoadBand(file, pt, mdesc, str, CONDUCTION);

	while(file.good())
	{
		str.clear();	//load the next line. likely CB
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;	//should be the valence band now...
	
		if(buffer == "VB")
			LoadBand(file, pt, mdesc, str, VALENCE);
		else if(buffer == "EImpState")	//impurity must occur after CB/VB
			LoadEState(file, pt, mdesc, str, CONDUCTION);	//CONDUCTION just says it's an electron. It will grab the proper reference
		else if(buffer == "End_Point")
			break;
	}

	if (mdesc.simulation->outputs.numinbin <= 0)
		mdesc.simulation->outputs.numinbin = 1;

	LinkSRH(pt);

	pt->Output.Allocate(mdesc.simulation->outputs.numinbin);	//prep the outputs
	if(pt->LightEmissions==NULL)
		pt->LightEmissions = new LightSource;
	pt->LightEmissions->light.clear();
	pt->LightEmissions->enabled = mdesc.simulation->EnabledRates.LightEmissions;
	pt->LightEmissions->times.insert(std::pair<double,bool>(0.0,pt->LightEmissions->enabled));
	pt->LightEmissions->center = pt->position;
	pt->LightEmissions->direction.x = pt->LightEmissions->direction.y = pt->LightEmissions->direction.z = 0.0;	//all directions
	pt->LightEmissions->name = "PointLEmission";
	pt->LightEmissions->centerinit = true;
	pt->LightEmissions->sourceShape = NULL;
	pt->LightEmissions->ID = -pt->adj.self;	//should be a unique ID...
	pt->LightEmissions->IncidentPoints.clear();

	if(mdesc.simulation->EnabledRates.Lgen == true		//there is no light whatsoever
			|| mdesc.simulation->EnabledRates.Ldef == true
			|| mdesc.simulation->EnabledRates.Lscat == true
			|| mdesc.simulation->EnabledRates.Lsrh == true
			|| mdesc.simulation->EnabledRates.Lpow == true
			|| mdesc.simulation->EnabledRates.SEdef == true
			|| mdesc.simulation->EnabledRates.SErec == true
			|| mdesc.simulation->EnabledRates.SEscat == true
			|| mdesc.simulation->EnabledRates.SEsrh == true
			|| mdesc.simulation->EnabledRates.LightEmissions == true)
	{
		pt->OpticalOut = new OpticalStorage;
		pt->OpticalOut->Allocate(mdesc.simulation->GetMaxOpticalSize());	//prep the optical outputs
	}
	pt->ResetData();	//load everything up good and proper...

	return(0);
}

int LoadBand(std::ifstream& file, Point* pt, ModelDescribe& mdesc, std::stringstream& str, bool band)
{
	Band* bd = NULL;
	double temp;
	std::string buffer;

	if(band == CONDUCTION)
		bd = &(pt->cb);
	else
		bd = &(pt->vb);

	bd->type = band;

	str >> bd->bandmin;
	str >> bd->partitioni;
	str >> temp;
	bd->SetNumParticles(temp);
	str >> bd->numparticlesold;
	str >> bd->totalstates;
	str >> bd->EffectiveDensity;

	while(file.good())
	{
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;

		if(buffer == "EState")
			LoadEState(file, pt, mdesc, str, band);
		else if(buffer == "End_band")
			break;
	}

	return(0);
}

int LoadEState(std::ifstream& file, Point* pt, ModelDescribe& mdesc, std::stringstream& str, bool band)
{
	std::string buffer;
	int tempint;
	double tempd;
	ELevel elev(*pt);
	bool ref;
	
	
	str >> tempint;
	if(tempint == 1)
		elev.carriertype = ELECTRON;
	else
		elev.carriertype = HOLE;

	str >> tempint;
	if(tempint == 1)
		elev.includeFermiAbove = true;
	else
		elev.includeFermiAbove = false;

	str >> tempint;
	if(tempint == 1)
		elev.includeFermiBelow = true;
	else
		elev.includeFermiBelow = false;

	str >> elev.uniqueID;

	elev.imp = NULL;

	ref = elev.carriertype;	//just set a default value. This should not matter
	elev.bandtail = false;
	str.peek();	//need to initiate setting the flag!

	if(str.good())	//this is somehow a defect/impurity?
	{
		str >> tempint;
		if(tempint >= 0)
		{
			for(unsigned int i=0; i<mdesc.impurities.size(); i++)
			{
				if(mdesc.impurities[i]->isImpurity(tempint))
				{
					elev.imp = mdesc.impurities[i];
					break;
				}
			}
		}
		else
			elev.bandtail = true;

		str >> tempint;
		if(tempint == 1)	//conduction
			ref = CONDUCTION;
		else
			ref = VALENCE;
	}
	
	//now start loading some real data...
	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;

	str >> elev.min;
	str >> elev.max;
	str >> elev.eUse;
	str >> tempd;
	elev.SetNumstates(tempd);
	str >> tempd;
	elev.SetEndOccStates(tempd);
	elev.SetBeginOccStates(tempd);	//not sure if this actually matters, but something is failing to load somewhere.
	str >> elev.targetTransfer;
	str >> elev.charge;
	str >> elev.boltzeUse;
	str >> elev.boltzmax;
	str >> elev.efm;
	str >> elev.tau;
	if(elev.tau > 0)
		elev.taui = 1.0 / elev.tau;
	else
		elev.taui = 0.0;
	str >> elev.velocity;
	str >> elev.fermi;
	str >> elev.FermiProb;
	str >> elev.AbsFermiEnergy;
	str >> elev.scatter.x;
	str >> elev.scatter.y;
	str >> elev.scatter.z;
	str >> elev.time.x;
	str >> elev.time.y;
	str >> elev.time.z;
	if(str.good())	//I can't easily add in all the correct percentTransfers to a file I want to load :/
		str >> elev.percentTransfer;
	else
		elev.percentTransfer = mdesc.simulation->TransientData->percentCarrierTransfer * 0.015625;

	if(str.good())
	{
		double occ[4];
		double r[4];
		bool type[4];
		int tmp;
		str >> occ[0];
		str >> occ[1];
		str >> occ[2];
		str >> occ[3];
		str >> r[0];
		str >> r[1];
		str >> r[2];
		str >> r[3];

		str >> tmp;
		type[0] = tmp==1?true:false;
		str >> tmp;
		type[1] = tmp==1?true:false;
		str >> tmp;
		type[2] = tmp==1?true:false;
		str >> tmp;
		type[3] = tmp==1?true:false;

/*		elev.tStepCtrl->AddROcc(occ[3], r[3], type[3], 1.0e-10);	//goes into occ0, but then gets pushed back
		elev.tStepCtrl->AddROcc(occ[2], r[2], type[2], 1.0e-10);
		elev.tStepCtrl->AddROcc(occ[1], r[1], type[1], 1.0e-10);
		elev.tStepCtrl->AddROcc(occ[0], r[0], type[0], 1.0e-10);

		str >> elev.tStepCtrl->minOcc;
		str >> elev.tStepCtrl->maxOcc;
		
		str >> tmp;
		elev.tStepCtrl->minMatch = tmp==1?true:false;

		str >> tmp;
		elev.tStepCtrl->maxMatch = tmp==1?true:false;

		elev.tStepCtrl->SetRets();	//and initialize the rest of the data structure
		*/
	}

	
	
	MaxMin<double> range;
	range.max = elev.max;
	range.min = elev.min;
	//now actually put the state into the point
	if(band == CONDUCTION && elev.imp == NULL && elev.bandtail == false)
		pt->cb.energies.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
	else if(band == VALENCE && elev.imp == NULL  && elev.bandtail == false)	//the && elev.imp==NULL is kinda redundant, but be safe if code ever changes.
		pt->vb.energies.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
	else	//it is an impurity or band tail
	{
		std::multimap<MaxMin<double>,ELevel>::iterator node;
		
		if(elev.carriertype == VALENCE)
		{
			if(elev.bandtail == true)
				node = pt->VBTails.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
			else
				node = pt->acceptorlike.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
		}
		else
		{
			if(elev.bandtail == true)
				node = pt->CBTails.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
			else
				node = pt->donorlike.insert(std::pair<MaxMin<double>,ELevel>(range, elev));
		}
		CalcSRH(node->second, elev.imp, pt, pt->temp);
	}
	elev.TargetELev.clear();
	elev.TargetELevDD.clear();
	elev.Rates.clear();
	elev.OpticalRates.clear();

	return(0);
}

int LoadMDescData(std::ifstream& file, ModelDescribe& mdesc, DataLinks& postProcess)
{
	std::stringstream myStream;	//a buffer stream for each line
	std::string buffer;	//used to load each line in
	myStream.clear();
	myStream.str("");

	getline(file, buffer);	//the first line after Model description
	myStream << buffer;	//put all that data in the stream.
	//now load it up. ORDER IS IMPORTANT!!
	myStream >> mdesc.NDim;
	myStream >> mdesc.Ldimensions.x;
	myStream >> mdesc.Ldimensions.y;
	myStream >> mdesc.Ldimensions.z;
	myStream >> mdesc.resolution.x;
	myStream >> mdesc.resolution.y;
	myStream >> mdesc.resolution.z;	//this should completely empty out the buffer

	while(file.good())
	{
		myStream.clear();
		myStream.str("");
		getline(file, buffer);
		myStream << buffer;	//store the next line
		myStream >> buffer;	//grab the first line
		if(buffer == "End_ModelDescribe")
			return(0);	//done with this line
		else if(buffer == "Simulation")
			LoadSimulationData(file, mdesc, myStream, postProcess);
		else if(buffer == "Impurity")
			LoadImpurityData(file, mdesc, myStream);
		else if(buffer == "Layer")
			LoadLayerData(file, mdesc);
		else if(buffer == "Material")
			LoadMaterialData(file, mdesc);
		else if(buffer == "Spectrum")
			LoadSpectrumData(file, mdesc, myStream, postProcess);
	
	}	//end loop going through model description data.

	return(0);
}

int LoadSpectrumData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str, DataLinks& postProcess) //the entire spectrum data is stored in str...
{
	std::string buffer;
	long int tempint;
	double tempdoub;
	LightSource* spec = new LightSource;
	SpecLinks tmpSpecLink;

	str >> spec->ID;
	tmpSpecLink.specID = spec->ID;	//used to link what points this light starts out incident on

	str >> tempint;
	if(tempint == 1)
		spec->enabled = true;
	else
		spec->enabled = false;

	int sz;
	str >> sz;
	for(int i=0; i<sz; i++)
	{
		str >> tempint;
		str >> tempdoub;
		if(tempint == 1)
			spec->times.insert(std::pair<double,bool>(tempdoub,true));
		else
			spec->times.insert(std::pair<double,bool>(tempdoub,false));
	}

	str >> spec->direction.x;
	str >> spec->direction.y;
	str >> spec->direction.z;

	//load shape
	str >> sz;
	if(sz != 0)
	{
		if(spec->sourceShape)
			delete spec->sourceShape;

		spec->sourceShape = new Shape;
		str >> sz;	//number of points in the shape
		str >> spec->sourceShape->type;	//what kind of shape it is
		XSpace tmp;
		for(int i=0; i<sz; i++)
		{
			str >> tmp.x;
			str >> tmp.y;
			str >> tmp.z;
			spec->sourceShape->AddPoint(tmp);
		}

	}

	str >> sz;	//size of incident points
	
	spec->IncidentPoints.reserve(sz);
	for(int i=0; i<sz; i++)
	{
		str >> tempint;
		tmpSpecLink.ptID = tempint;
		postProcess.SpLink.push_back(tmpSpecLink);
	}

	str >> sz;
	

	for(int i=0; i<sz; i++)
	{
		Wavelength wave;
		str >> wave.lambda;
		str >> wave.intensity;
//		str >> wave.collimation; LIGHT_OLD_VARIABLES
//		str >> wave.coherence1;
//		str >> wave.coherence2;
		str >> wave.energyphoton;
		wave.cutOffPower = 0.0;
		wave.IsCutOffPowerInit = false;
		MapAdd(spec->light,wave.energyphoton, wave);

	}

	spec->name = "";
	while(str.good())
	{
		str >> buffer;
		spec->name = spec->name + buffer;
		if(str.good())	//add spaces if there is more to the name
			spec->name = spec->name + " ";
	}

	mdesc.spectrum.push_back(spec);
	return(0);
}

int LoadMaterialData(std::ifstream& file, ModelDescribe& mdesc)
{
	std::stringstream str;
	std::string buffer;
	int tempint;
	Material* mat = new Material;

	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;
	str >> mat->id;

	mat->name = "";
	while(str.good())
	{
		str >> buffer;
		mat->name = mat->name + buffer;
		if(str.good())	//add spaces if there is more to the name
			mat->name = mat->name + " ";
	}

	str.clear();
	str.str("");
	getline(file, mat->describe);	//this first line is the description...
	getline(file, buffer);	//now load all the specific data
	str << buffer;
	str >> mat->type;
	str >> mat->epsR;
	str >> mat->muR;
	str >> mat->ni;
	str >> mat->Nc;
	str >> mat->Nv;
	str >> mat->EFMeDens;
	str >> mat->EFMhDens;
	str >> mat->EFMeCond;
	str >> mat->EFMhCond;
	str >> mat->MuN;
	str >> mat->MuP;
	str >> mat->Eg;
	str >> mat->EcEfi;
	str >> mat->Phi;
	str >> mat->SpHeatCap;
	str >> mat->ThCond;
	str >> mat->tauThGenRec;
	str >> mat->urbachCB.energy;
	str >> mat->urbachCB.captureCB;
	str >> mat->urbachCB.captureVB;
	str >> mat->urbachVB.energy;
	str >> mat->urbachVB.captureCB;
	str >> mat->urbachVB.captureVB;
	str >> mat->cfactor.ni;
	str >> tempint;
	mat->direct = (tempint==1) ? true : false;
	

	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;
	str >> buffer;

	if(buffer == "Defects")	//this doesn't actually load the data for the defects. It just sets the appropriate ID
	{
		while(str.good())
		{
			str >> tempint;	//now I need to find the said defect in the impurity list---which has already been loaded
			for(unsigned int i=0; i < mdesc.impurities.size(); i++)
			{
				if(mdesc.impurities[i]->isImpurity(tempint))
				{
					mat->defects.push_back(mdesc.impurities[i]);
					break;	//found it, so now it's done
				}
			}
		}

		str.clear();	//prep for the next possible load
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if(buffer == "ScPrLoad")	//no longer used... but maintain for past versions
	{
		str.clear();	//prep for the next possible load
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;	//prep for the next if statement...
	}

	if(buffer == "Optical")
	{
		str.clear();	//prep for nkab values of optical
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> mat->optics->n;
		str >> mat->optics->a;
		str >> mat->optics->b;
		str >> tempint;
		mat->optics->useAB = (tempint==1)?true:false;
		if(str.good())
		{
			str >> tempint;
			mat->optics->suppressOpticalOutputs = (tempint == 1)?true:false;
			str >> tempint;
			mat->optics->disableOpticalEmissions = (tempint == 1)?true:false;
		}
		else
		{
			mat->optics->suppressOpticalOutputs=false;
			mat->optics->disableOpticalEmissions=false;
		}
	

		str.clear();	//get particular alpha values for each wavelength
		str.str("");
		getline(file, buffer);
		str << buffer;

		NK tmpNK;
		float wavelength;
		str.peek();	//must peek because it has not been read yet!
		while(str.good())
		{
			str >> wavelength;
			str >> tmpNK.n;
			str >> tmpNK.k;
			str >> tmpNK.alpha;
			mat->optics->WavelengthData.insert(std::pair<float,NK>(wavelength,tmpNK));
		}
		//it is now done loading optical data

		str.clear();	//prep for the next possible load
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;	//prep for the next if statement...
	}

	mat->OpticalAggregateOut->ClearAll();
	mat->OpticalFluxOut->ClearAll();

	if(buffer == "End_Material")
	{
		mdesc.materials.push_back(mat);
	}

	return(0);
}

int LoadLayerData(std::ifstream& file, ModelDescribe& mdesc)
{
	std::stringstream str;
	std::string buffer;
	Layer* lyr = new Layer;
	int tempint;

	//nothing on the first line that's sent in. Continue reading the file...

	getline(file, buffer);
	str.clear();
	str.str("");
	str << buffer;
	str >> lyr->id;
	str >> lyr->contactid;
	lyr->name = "";
	while(str.good())
	{
		str >> buffer;
		lyr->name = lyr->name + buffer;
		if(str.good())	//add a space if there are still more things to pull out of the stream
			lyr->name = lyr->name + " ";
	}

	getline(file, buffer);
	str.clear();
	str.str("");
	str << buffer;
	str >> lyr->spacingboundary;
	str >> lyr->spacingcenter;
	str >> lyr->deviation.x;
	str >> lyr->deviation.y;
	str >> lyr->deviation.z;

	//now load a bunch of booleans...
	str >> tempint;
	if(tempint == 1)
		lyr->exclusive = true;
	else
		lyr->exclusive = false;

	str >> tempint;
	if(tempint == 1)
		lyr->AffectMesh = true;
	else
		lyr->AffectMesh = false;

	str >> tempint;
	if(tempint == 1)
		lyr->priority = true;
	else
		lyr->priority = false;

	str >> tempint;
	if(tempint == 1)
		lyr->out.AllRates = true;
	else
		lyr->out.AllRates = false;

	str >> tempint;
	if(tempint == 1)
		lyr->out.OpticalPointEmissions = true;
	else
		lyr->out.OpticalPointEmissions = false;

	//now load a bunch of possibly optional data...

	while(file.good())
	{
		getline(file, buffer);
		str.clear();
		str.str("");
		str << buffer;
		str >> buffer;

		if(buffer == "End_Layer")
			break;	//exit the while loop
		else if(buffer == "RangePts")
		{
			PtRange PtR;
			while(str.good())
			{
				str >> PtR.startpos.x;
				str >> PtR.startpos.y;
				str >> PtR.startpos.z;
				str >> PtR.stoppos.x;
				str >> PtR.stoppos.y;
				str >> PtR.stoppos.z;
			}
		}
		else if(buffer == "Shape")
		{
			if(lyr->shape != NULL)
			{
				delete lyr->shape;	//prevent possible memory leak
				lyr->shape = NULL;
			}
			XSpace vertexPos;
			lyr->shape = new Shape;
			str >> lyr->shape->type;
			//then load all the vertices... no need for an id. They are automatically generated
			getline(file, buffer);
			str.clear();
			str.str("");
			str << buffer;	//now all the vertices are in the std::stringstream
			str.peek();	//set the str good flag! It hasn't been read yet.
			while(str.good())
			{
				str >> vertexPos.x;
				str >> vertexPos.y;
				str >> vertexPos.z;
				lyr->shape->AddPoint(vertexPos);
			}

		}
		else if(buffer == "ImpDesc")
		{
			ldescriptor<Impurity*>* ldesc = new ldescriptor<Impurity*>;
			str >> ldesc->id;
			str >> ldesc->flag;
			str >> ldesc->multiplier;
			str >> ldesc->range.startpos.x;
			str >> ldesc->range.startpos.y;
			str >> ldesc->range.startpos.z;
			str >> ldesc->range.stoppos.x;
			str >> ldesc->range.stoppos.y;
			str >> ldesc->range.stoppos.z;
			lyr->impurities.push_back(ldesc);
		}
		else if(buffer == "MatDesc")
		{
			ldescriptor<Material*>* ldesc = new ldescriptor<Material*>;
			str >> ldesc->id;
			str >> ldesc->flag;
			str >> ldesc->multiplier;
			str >> ldesc->range.startpos.x;
			str >> ldesc->range.startpos.y;
			str >> ldesc->range.startpos.z;
			str >> ldesc->range.stoppos.x;
			str >> ldesc->range.stoppos.y;
			str >> ldesc->range.stoppos.z;
			lyr->matls.push_back(ldesc);
		}
	}
	lyr->OutputLightCarrierGen.clear();
	lyr->OutputLightIncident.clear();

	mdesc.layers.push_back(lyr);
	return(0);
}

int LoadImpurityData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str)
{
	//first, need to pull off the other information...
	
	Impurity* imp = new Impurity;
	mdesc.impurities.push_back(imp);

	imp->LoadFromStateFile(file, str);
	return 0;
}
int Impurity::LoadFromStateFile(std::ifstream& file, std::stringstream& str) {

	int tempint;
	std::string buffer;

	str >> ID;
	
	str >> charge;
	str >> distribution;
	
	//now check depth/reference
	str >> tempint;
	if(tempint == 1)
		depth = true;
	else
		depth = false;

	str >> tempint;

	if(tempint == 1)
		reference = true;
	else
		reference = false;
	
	str >> energy;
	str >> energyvar;
	str >> density;
	str >> sigman;
	str >> sigmap;
	str >> degeneracy;	//should be the end of the impurity...

	name="";
	while(str.good())
	{
		str >> buffer;
		name += buffer;
		if(str.good())
			name += " ";	//add a space if there are still more to the name
	}

	

	return(0);
}

int LoadSteadyStateData(std::ifstream& file, std::stringstream& str, Simulation* sim)
{
	std::string buffer;
	sim->SteadyStateData = new SS_SimData(sim);
	SS_SimData* ss = sim->SteadyStateData;	//save on typing
	int ExtSz, ctcSz, ltSz;

	str >> ss->CurrentData;
	str >> ss->steadyStateClampReduction;
	str >> ExtSz;

	
	SSContactData ct;
	ct.Clear();	//make sure it's all zeroed out to begin with
	for(int i=0; i<ExtSz; ++i)
	{
		Environment* env = new Environment(ss);
		env->ActiveLightEnvironment.clear();	//make sure these don't contain info from the previous environment
		env->ContactEnvironment.clear();
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;	//now the entire line is stored in the stream

		str >> env->id;
		str >> env->flags;
		str >> env->reusePrevContactEnvironment;
		str >> env->reusePrevLightEnvironment;
		str >> ctcSz;
		str >> ltSz;
		env->FileOut = str.str();

		for(int j=0; j<ctcSz; ++j)
		{
			str.clear();
			str.str("");
			getline(file, buffer);
			str << buffer;	//now the entire contact line is stored in the stream

			ct.ctc = NULL;
			str >> ct.ctcID;
			str >> ct.currentActiveValue;
			str >> ct.voltage;
			str >> ct.current;
			str >> ct.currentDensity;
			str >> ct.eField;
			str >> ct.current_ED;
			str >> ct.eField_ED;
			str >> ct.currentDensity_ED;
			str >> ct.eField_ED_Leak;
			str >> ct.current_ED_Leak;
			str >> ct.currentD_ED_Leak;

			//the contacts should have already been loaded into sim.
			for(unsigned int k=0; k<sim->contacts.size(); ++k)
			{
				if(ct.ctcID == sim->contacts[k]->id)
				{
					ct.ctc = sim->contacts[k];	//they are now appropriately linked
					ct.ctc->ContactEnvironmentIndex = i;
					break;
				}
			}
			env->ContactEnvironment.push_back(new SSContactData(ct));

		}
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;	//now the entire light stream is in stringstream
		str >> buffer;	//this should put "light" into buffer and leave str with all the proper numbers
		for(int j=0; j<ltSz; ++j)
		{
			str >> ctcSz;	//can use this as a temp buffer now
			env->ActiveLightEnvironment.push_back(ctcSz);
		}

		ss->ExternalStates.push_back(env);

	}

	return(0);
}

int LoadTransientData(std::ifstream& file, std::stringstream& str, Simulation* sim, DataLinks& postProcess)
{
	//str contains ALL the data
	int tempnum;
	double tempdoub;
	

	sim->TransientData = new TransientSimData(sim);
	TransientSimData* tr = sim->TransientData;	//less typing

	str >> tempnum;
	tr->SSByTransient = (tempnum==1)?true:false;

	str >> tempnum;
	tr->SmartSaveState = (tempnum==1)?true:false;

	str >> tr->IntermediateSteps;
	str >> tr->SSbyTTime;
	str >> tr->percentCarrierTransfer;
	str >> tr->timeignore;
	str >> tr->simtime;
	str >> tr->timestep;
	tr->timestepi = 1.0 / tr->timestep;	//don't want to forget this...
	str >> tr->nexttimestep;
	str >> tr->iterEndtime;
	str >> tr->simendtime;
	str >> tr->storevalueStartTime;
	str >> tr->mints;
	str >> tr->avgts;
	str >> tr->maxts;

	str >> tr->prevMNRate;
	str >> tr->tstepConsts.deltaTM1Inv;
	str >> tr->tstepConsts.deltaTM2Inv;
	str >> tr->tstepConsts.prev2timestep;
	str >> tr->tstepConsts.prevtimestep;
	str >> tr->tstepConsts.TM1PlusTM2;
	str >> tr->tstepConsts.TM1PlusTM2Inv;

	int sz;

	str >> sz;
	for(int i=0; i<sz; ++i)
	{
		//now load the target times
		str >> tempnum;
		str >> tempdoub;
		TargetTimeData ttd;
		ttd.SSTarget =-1;
		if(tempnum == 0)
		{
			ttd.SaveState=false;
			tr->TargetTimes.insert(std::pair<double,TargetTimeData>(tempdoub,ttd));
		}
		else
		{
			ttd.SaveState=true;
			tr->TargetTimes.insert(std::pair<double, TargetTimeData>(tempdoub, ttd));
		}
		
	}

	std::map<double, TargetTimeData>::iterator ptrNode = MapFindFirstGreater(tr->TargetTimes, tr->simtime);
	if (ptrNode != tr->TargetTimes.end())
		tr->curTargettime = ptrNode->first;
	else
		tr->curTargettime = tr->simendtime;

	if (sim->outputs.numinbin <= 0)
		sim->outputs.numinbin = 1;

//	tr->timesteps = new double[sim->outputs.numinbin];

	str >> sz;	//number of transient contacts
	for (int i = 0; i < sz; ++i)
		LoadTransContactData(file, sim, str, postProcess);

	return(0);
}

int LoadSimulationData(std::ifstream& file, ModelDescribe& mdesc, std::stringstream& str, DataLinks& postProcess)	//there's more data on the stream...
{
	std::string buffer;	//just a buffer
	int tempnum;	//can't just load a 0 as false or 1 as true into a boolean...

	//delete previously occupied simulation data
	if(mdesc.simulation != NULL)
		delete mdesc.simulation;
	mdesc.simulation = new Simulation;	//new data formed now.
	mdesc.simulation->TransientData->timesteps.clear();
	mdesc.simulation->binctr = 0;
	mdesc.simulation->holecurrent.clear();
	mdesc.simulation->ecurrent.clear();
	mdesc.simulation->OpticalAggregateOut->ClearAll();
	mdesc.simulation->OpticalFluxOut->ClearAll();
	mdesc.simulation->NoSS.clear();
	mdesc.simulation->ThEqDeviceStart = false;
	mdesc.simulation->ThEqPtStart = false;
	mdesc.simulation->TransientData->tStepControl.ReInit();	//basically zero everything out
	mdesc.simulation->TransientData->tStepControl.SetNumRepeat(10);

	//passed in the buffer name as well in the stream
	mdesc.simulation->OutputFName = "";	//make sure the outname is empty
	str.peek();
	while(str.good())
	{
		str >> buffer;	//put initial name in
		mdesc.simulation->OutputFName = mdesc.simulation->OutputFName + buffer;

		if(str.good())	//there's more words. add a space
			mdesc.simulation->OutputFName = mdesc.simulation->OutputFName + " ";
	}

	//now load other simulation data...
	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;	//now the entire line is stored in the stream
	str >> mdesc.simulation->curiter;
	str >> mdesc.simulation->maxiter;
	str >> mdesc.simulation->statedivisions;
	str >> mdesc.simulation->outputs.numinbin;

	str >> mdesc.simulation->lightincidentdir;

	//load up some double values...
	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;
	str >> mdesc.simulation->timeSaveState;
	
	
	str >> mdesc.simulation->T;
	str >> mdesc.simulation->Vol;
	str >> mdesc.simulation->accuracy;
	

	str.clear();
	str.str("");
	getline(file, buffer);
	str << buffer;	//now the string has a bunch of 1's and 0's to say what each output is.
	
	mdesc.simulation->outputs.OutputAllIndivRates = false;

	
	str >> tempnum;
	mdesc.simulation->SignificantMilestone = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->transient = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.fermi = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.fermin = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.fermip = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Ec = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Ev = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Psi = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.CarrierCB = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.CarrierVB = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.n = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.p = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.genth = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.SRH = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.recth = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.rho = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Jp = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Jn = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Lpow = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Lgen = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Lsrh = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Ldef = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Lscat = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Erec = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Esrh = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Edef = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.Escat = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.LightEmission = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.stddev = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.error = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->outputs.chgError = (tempnum==1)?true:false;

	str >> tempnum;
	mdesc.simulation->EnabledRates.SRH1 = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SRH2 = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SRH3 = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SRH4 = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.ThermalGen = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.ThermalRec = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Scatter = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Current = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Tunnel = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Lpow = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Lgen = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Lsrh = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Ldef = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.Lscat = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SErec = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SEsrh = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SEdef = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.SEscat = (tempnum==1)?true:false;
	str >> tempnum;
	mdesc.simulation->EnabledRates.LightEmissions = (tempnum==1)?true:false;
	str >> mdesc.simulation->EnabledRates.Thermalize;
	str >> tempnum;
	mdesc.simulation->outputs.OutputAllIndivRates = (tempnum == 1) ? true : false;

	str >> mdesc.simulation->EnabledRates.OpticalEMin;
	str >> mdesc.simulation->EnabledRates.OpticalEMax;
	str >> mdesc.simulation->EnabledRates.OpticalEResolution;
	str >> mdesc.simulation->EnabledRates.OpticalIntensityCutoff;
	mdesc.simulation->SetMaxOpticalSize();

	
	
	
	//end simulation outputs

	while(file.good())
	{
		getline(file, buffer);
		str.clear();
		str.str("");
		str << buffer;	//put the line read into the stream
		str >> buffer;	//put the first part back into the buffer

		if(buffer == "End_Simulation")	//done loading simulation data...
		{
			
			return(0);
		}
		else if(buffer == "Contact")	//time for some contact data...
			LoadContactData(file, mdesc.simulation, str, postProcess);
		else if(buffer == "Transient")
			LoadTransientData(file, str, mdesc.simulation, postProcess);
		else if(buffer=="SS")
			LoadSteadyStateData(file, str, mdesc.simulation);
	}

	
	
	return(0);
}

int LoadTransContactData(std::ifstream& file, Simulation* sim, std::stringstream& str, DataLinks& postProcess)	//there's more data on the stream...
{
	std::string buffer;
	TransContact * tC = new TransContact();
	int tmpI;
	str >> tmpI;
	tC->SetContactID(tmpI);

	str.clear();
	str.str("");
	getline(file, buffer);
	//first one is the current voltage, second one is the next time change...
	str << buffer;
	str >> buffer;
	if (buffer == "VCos")	//then load up all the cosine data
	{
		CosTimeDependence tempcos;
		double starttime;
		double endtime;
		double amp;
		double freq;
		double delta;
		double direct;
		double leak;
		int flags;
		bool sum;
		int tmpSum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> amp;
			str >> freq;
			str >> delta;
			str >> direct;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempcos.initialize(amp, freq, delta, direct, starttime, endtime, leak, sum, flags);
			tC->AddCosTimeDependence(tempcos, CTC_COS_VOLT, starttime, sim->TransientData->simtime);	//simulation time already loaded.
			//this will automatically update the next time
		}	//this will have loaded all the cos data.

		//prep for possible fixed data
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "VLin")
	{
		LinearTimeDependence tempLin;
		double starttime, endtime, initVal, slope, leak;
		int flags, tmpSum;
		bool sum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> initVal;
			str >> slope;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempLin.SetValues(initVal, slope, starttime, endtime, leak, flags, sum);
			tC->AddLinearDependence(tempLin, CTC_LIN_VOLT, starttime, sim->TransientData->simtime);

		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "ECos")	//then load up all the cosine data
	{
		CosTimeDependence tempcos;
		double starttime;
		double endtime;
		double amp;
		double freq;
		double delta;
		double direct;
		double leak;
		int flags;
		bool sum;
		int tmpSum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> amp;
			str >> freq;
			str >> delta;
			str >> direct;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempcos.initialize(amp, freq, delta, direct, starttime, endtime, leak, sum, flags);
			tC->AddCosTimeDependence(tempcos, CTC_COS_EFIELD, starttime, sim->TransientData->simtime);	//simulation time already loaded.
			//this will automatically update the next time
		}	//this will have loaded all the cos data.

		//prep for possible fixed data
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "ELin")
	{
		LinearTimeDependence tempLin;
		double starttime, endtime, initVal, slope, leak;
		int flags, tmpSum;
		bool sum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> initVal;
			str >> slope;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempLin.SetValues(initVal, slope, starttime, endtime, leak, flags, sum);
			tC->AddLinearDependence(tempLin, CTC_LIN_EFIELD, starttime, sim->TransientData->simtime);

		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "CDCos")	//then load up all the cosine data
	{
		CosTimeDependence tempcos;
		double starttime;
		double endtime;
		double amp;
		double freq;
		double delta;
		double direct;
		double leak;
		int flags;
		bool sum;
		int tmpSum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> amp;
			str >> freq;
			str >> delta;
			str >> direct;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempcos.initialize(amp, freq, delta, direct, starttime, endtime, leak, sum, flags);
			tC->AddCosTimeDependence(tempcos, CTC_COS_CURD, starttime, sim->TransientData->simtime);	//simulation time already loaded.
			//this will automatically update the next time
		}	//this will have loaded all the cos data.

		//prep for possible fixed data
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "CDLin")
	{
		LinearTimeDependence tempLin;
		double starttime, endtime, initVal, slope, leak;
		int flags, tmpSum;
		bool sum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> initVal;
			str >> slope;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempLin.SetValues(initVal, slope, starttime, endtime, leak, flags, sum);
			tC->AddLinearDependence(tempLin, CTC_LIN_CURD, starttime, sim->TransientData->simtime);

		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "CCos")	//then load up all the cosine data
	{
		CosTimeDependence tempcos;
		double starttime;
		double endtime;
		double amp;
		double freq;
		double delta;
		double direct;
		double leak;
		int flags;
		bool sum;
		int tmpSum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> amp;
			str >> freq;
			str >> delta;
			str >> direct;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempcos.initialize(amp, freq, delta, direct, starttime, endtime, leak, sum, flags);
			tC->AddCosTimeDependence(tempcos, CTC_COS_CUR, starttime, sim->TransientData->simtime);	//simulation time already loaded.
			//this will automatically update the next time
		}	//this will have loaded all the cos data.

		//prep for possible fixed data
		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "CLin")
	{
		LinearTimeDependence tempLin;
		double starttime, endtime, initVal, slope, leak;
		int flags, tmpSum;
		bool sum;
		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> initVal;
			str >> slope;
			str >> leak;
			str >> flags;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tempLin.SetValues(initVal, slope, starttime, endtime, leak, flags, sum);
			tC->AddLinearDependence(tempLin, CTC_LIN_CURD, starttime, sim->TransientData->simtime);

		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}

	if (buffer == "flgs")
	{

		double starttime, endtime, leak;
		int flag, tmpSum;
		bool sum;

		while (str.good())
		{
			str >> starttime;
			str >> endtime;
			str >> leak;
			str >> flag;
			str >> tmpSum;
			if (tmpSum)
				sum = true;
			else
				sum = false;

			tC->AddTimeDependence(TimeDependence(starttime, endtime, flag, sum, leak));

		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}
	if (buffer == "End_Trans_Ctc")	//just a way to make sure we don't run past the file ending
	{
		;
	}
	
	return 0;

}

int LoadContactData(std::ifstream& file, Simulation* sim, std::stringstream& str, DataLinks& postProcess)	//there's more data on the stream...
{
	std::string buffer;
	//first create a new contact to put stuff into
	Contact* ctc = new Contact;
	int tempnum;

	str >> tempnum;
	if(tempnum == 1)
		ctc->isolated = true;
	else
		ctc->isolated = false;

	//the stream still has information after the contact... load that up!
	str >> ctc->id;

	ctc->name = "";	//make sure the outname is empty
	
	while(str.good())
	{
		str >> buffer;	//put initial name in
		ctc->name = ctc->name + buffer;
		if(str.good())	//there's more words. add a space
			ctc->name = ctc->name + " ";
	}

	//now load the next few data bits
	if (buffer == "lnks")
	{
		CLinkPostProcess tmp;
		tmp.srcID = ctc->id;	//this has already been loaded --- one of the first things done

		while (str.good())
		{
			str >> tmp.current;
			str >> tmp.resistance;
			str >> tmp.trgID;
			postProcess.CLinks.push_back(tmp);
		}

		str.clear();
		str.str("");
		getline(file, buffer);
		str << buffer;
		str >> buffer;
	}
	
	if(buffer == "End_Contact")	//it better read end_contact...
	{
		sim->contacts.push_back(ctc);
	}
	return(0);
}