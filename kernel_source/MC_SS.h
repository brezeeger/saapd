#ifndef SAAPD_MC_SS_H
#define SAAPD_MC_SS_H

#include <map>
#include <vector>

#define MC_MAXPERCENT 0.0625
#define MC_MINPERCENT 1e-10
#define MC_C_MAXPERCENT 0.25
#define MC_C_INVALID_MINOCC_RATE -1.0
#define MC_C_INVALID_MAXOCC_RATE 1.0
#define MC_C_INVALID_OCC -1.0
#define MC_C_UPDATE_NEIGHBORS_UP 1e9
#define MC_C_UPDATE_NEIGHBORS_DOWN 1e-9	//inverse of up

#define MC_C_WORTH_RECALC_PROB 1e-2

class ELevel;
class Model;
class ModelDescribe;
class Simulation;
class kernelThread;
class ELevelRate;
class Point;
class PosNegSum;


template <class retType>
class PartitionFunc;

class MC_ElevelPartition
{
public:
	unsigned long long int sortVal;
	ELevel* ptrELevel;
	MC_ElevelPartition() { sortVal = 0; ptrELevel = NULL; };
	~MC_ElevelPartition() { sortVal = 0; ptrELevel = NULL; };

	MC_ElevelPartition& operator += (const MC_ElevelPartition &b) { return *this; };	//basically, don't allow these to change at all.
	MC_ElevelPartition operator - (const MC_ElevelPartition &b) { return *this; };
	MC_ElevelPartition operator + (const MC_ElevelPartition &b) { return *this; };
	MC_ElevelPartition operator * (const MC_ElevelPartition &b) { return *this; };
	MC_ElevelPartition operator / (const MC_ElevelPartition &b) { return *this; };
	
	bool operator >(const MC_ElevelPartition &b) const { return (sortVal > b.sortVal); };
	bool operator <(const MC_ElevelPartition &b) const { return (sortVal < b.sortVal); };
	bool operator >=(const MC_ElevelPartition &b) const { return (sortVal >= b.sortVal); };
	bool operator <=(const MC_ElevelPartition &b) const { return (sortVal <= b.sortVal); };
	bool operator !=(const MC_ElevelPartition &b) const { return (sortVal != b.sortVal); };
	bool operator ==(const MC_ElevelPartition &b) const { return (sortVal == b.sortVal); };
};


//new version (targets largest difference in carriers from zero)
int MC_C_SS(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
int MC_CalcRateDerivs(Model& mdl, ModelDescribe& mdesc);
double MC_C_BuildPFn(Model& mdl, PartitionFunc<MC_ElevelPartition>& probs);

template <typename S>
double MC_C_PFnStateBuild(std::multimap<S, ELevel>& states, PartitionFunc<MC_ElevelPartition>& probs);

int MC_C_Process(Model& mdl, ModelDescribe& mdesc, PartitionFunc<MC_ElevelPartition>& probability);
int UpdateProbNeighbors(PartitionFunc<MC_ElevelPartition>& probs, ELevel* src, Simulation* sim, Model& mdl);
int ReinsertELevelPartition(PartitionFunc<MC_ElevelPartition>& probs, std::vector<ELevel*>& ELevs);
double MC_C_AdjustCarriers(ELevel* eLev, Model& mdl, Simulation* sim);
int MC_C_UpdateRateData(ELevel* eLev, Simulation* sim, Model& mdl);
int EFieldETrg(Point* srcPt, Point* trgPt);
int EFieldEnergyCalc(Point* srcPt, Point* trgPt, Point* adjTrgPt, double& retLin, double& retQuad);

//old code vvv (tyhough some of it is still used - based on getting netrate to zero)

int MonteCarlo_SteadyState(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
int PrepDevice(Model& mdl, ModelDescribe& mdesc, bool first);
template <class S>
int MC_PrepELevl(std::multimap<S,ELevel>& states, Model& mdl, Point& pt, Simulation* sim, bool first);
int MC_CalcRates(Model& mdl, ModelDescribe& mdesc, PartitionFunc<long int>& ELevelChoose);
int MC_Process(Model& mdl, ModelDescribe& mdesc, PartitionFunc<long int>& probability, std::vector<ELevel*> eLevs, unsigned long long int maxInd);
int MC_UpdatePost(ELevel* src, Model& mdl);
int UpdateNeighborsPre(Model& mdl, ModelDescribe& mdesc, ELevel* source);
int MC_NeighborBand(Model& mdl, Simulation& sim, Point* skipPt, Point* pt);
int MC_CalcELevelRate(ELevel* elev, Simulation* sim, bool clearRates=true);
int MC_TransferCarriers(ELevel* eLev, Model& mdl, unsigned long iter);
int MC_SumDerivativeRates(std::vector<ELevelRate>& rate, PosNegSum& derivs, PosNegSum& derivs2);
double MC_TransferRate(std::vector<ELevelRate>& rates, double time);
int MC_TranslateSourceERateToTarget(ELevelRate& rate, double& RateIn, double& RateOut, double& netRate, double& deriv1, double& deriv2);
double FailTransfer(ELevel* eLev, double attempt);
int MC_OutputState(Model& mdl, ModelDescribe& mdesc);

#endif