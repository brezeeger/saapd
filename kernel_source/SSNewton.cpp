#include "SSNewton.h"

#include <ctime>
#include "BasicTypes.h"
#include "model.h"
#include "RateGeneric.h"
#include "SteadyState.h"
#include "light.h"
#include "../gui_source/kernel.h"
#include "DriftDiffusion.h"
#include "srh.h"
#include "ThGenRec.h"
#include "scatter.h"
#include "outputdebug.h"
#include "output.h"
#include "contacts.h"
#include "EquationSolver.h"
#include "SaveState.h"
#include "ss.h"
#include "transient.h"
#include "CalcBandStruct.h"
#include "physics.h"

//this assumes there is no current to modify this point. This means every finite element
//is isolated from all others, and there's no need to generate a ton of memory or do a ton of calculations
double SSNewtonPoint(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, Point* pt, SteadyState_SAAPD& data)
{
//	std::vector<ELevel*>	EStates;	//a std::vector of all energy states, that must be built at least once
	time_t start = time(NULL);
	time_t TotStart = start;
	double timeelapse = 0.0;
	double largestChange = 0.0;
#ifndef NDEBUG
	double* Rates = NULL;
#endif
	double* derivMatrix = NULL;
	double* delOccs = NULL;
	bool* useELevel = NULL;
	
	long int whichPt=-1;
	for(unsigned long int i=0; i<data.Pt.size(); ++i)
	{
		if(data.Pt[i].self == pt->adj.self)
		{
			whichPt = i;
			break;
		}
	}
	long startI = data.ELevelIndicesInPt[whichPt].min;
	long stopI = data.ELevelIndicesInPt[whichPt].max;
	long int ESz = data.EStates.size();
	if(startI > stopI || startI< 0 || stopI < 0 || stopI >= ESz)
		return(-1);

	unsigned long int sz = stopI - startI + 1;

	DisplayProgress(59, sz);
	unsigned long int matrixSz = sz*sz;


	try
	{
#ifndef NDEBUG
		Rates = new double[sz];
#endif
		useELevel = new bool[sz];
		delOccs = new double[sz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
		return(-1);
	}
	try
	{
		derivMatrix = new double[matrixSz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
#ifndef NDEBUG
		delete [] Rates;
#endif
		delete [] useELevel;
		delete [] delOccs;
		return(-1);
	}

	for(unsigned long int el=0;el<sz;++el)
	{
		if(data.EStates[el].OccMatches)
		{
			data.EStates[el].ptr->SetBeginOccStates(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStates(data.EStates[el].occupancy);
		}
		else
		{
			data.EStates[el].ptr->SetBeginOccStatesOpposite(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStatesOpposite(data.EStates[el].occupancy);
		}
	}




	//	double adjustFactor = 1.0e-8;
	double sourceDeriv, targetDeriv;
	SS_CurrentDerivFactors derivFactor;
	double maxPercentChange = 1.0;//mdesc.simulation->accuracy;
	double tmpVal;
	double maxChangeSecond = 1.0;
	bool announceNextIteration = false;
	double ClampValue = 10;	//it will cap out at 1 for the first iteration
	ELevel* limitRateState;
	ELevel* OccLimitState;
	double limitRate;
	double delOcc;

#ifndef NDEBUG
	DebugSteadyState(NULL, NULL, 0.0, 0.0, true);
#endif
	

	OutputPointOpticalDetailedInit(mdesc.simulation, *pt);

	while(largestChange >= data.tolerance)
	{
		pt->Jn.x = pt->Jn.y = pt->Jn.z = pt->Jp.x = pt->Jp.y = pt->Jp.z = 0.0;
		pt->genth = pt->recth = pt->scatterR = 0.0;
		pt->srhR1 = pt->srhR2 = pt->srhR3 = pt->srhR4 = pt->netCharge = 0.0;
		pt->LightAbsorbDef = pt->LightAbsorbGen = pt->LightAbsorbScat = pt->LightAbsorbSRH = 0.0;
		pt-> LightEmitDef = pt->LightEmitRec = pt->LightEmitScat = pt->LightEmitSRH = 0.0;
		pt->LightPowerEntry = pt->LightEmission = 0.0;	//mW
		if(pt->LightEmissions)
			pt->LightEmissions->light.clear();	//clear out all the wavelengths that might send light out!
		pt->largestOccCB = pt->largestOccES = pt->largestOccVB = pt->largestUnOccES = 0.0;

		PrepELevelSFirst(pt->cb.energies, mdl, *pt, mdesc.simulation);
		PrepELevelSFirst(pt->vb.energies, mdl, *pt, mdesc.simulation);
		PrepELevelSFirst(pt->acceptorlike, mdl, *pt, mdesc.simulation);
		PrepELevelSFirst(pt->donorlike, mdl, *pt, mdesc.simulation);
		PrepELevelSFirst(pt->CBTails, mdl, *pt, mdesc.simulation);
		PrepELevelSFirst(pt->VBTails, mdl, *pt, mdesc.simulation);
		
		pt->ResetData();	//get the data correct throughout the point
		pt->ClearDerivs();
		mdesc.simulation->PrepPointBandStructure(mdl, *pt);	//and update mdl.poisson rho with the correct charge

		limitRateState = OccLimitState = NULL;
		limitRate = delOcc = 0.0;
		//update band diagram

		//clear out old optical data
		if(mdesc.simulation->OpticalFluxOut)
			mdesc.simulation->OpticalFluxOut->ClearAll();
		for(unsigned int i=0; i<mdesc.materials.size(); ++i)
		{
			if(mdesc.materials[i]->OpticalFluxOut)
				mdesc.materials[i]->OpticalFluxOut->ClearAll();
		}

		if(pt->LightEmissions)
			pt->LightEmissions->light.clear();	//make sure there is no light intensities piling up

		for(unsigned long int src=0; src<sz; ++src)	//cycle through all the energy levels
		{
			ELevel* srcEptr = data.EStates[src+startI].ptr;
			useELevel[src] = false;
			//clear out some potential variables
			srcEptr->Rates.clear();
			srcEptr->OpticalRates.clear();
			srcEptr->RateSums.Clear();
			srcEptr->rateIn = srcEptr->rateOut = srcEptr->netRate = 0.0;
#ifndef NDEBUG
			Rates[src] = 0.0;
#endif
			delOccs[src] = 0.0;
			sourceDeriv = 0.0;
			//clear out the array for everything that depends on this source state
			for(unsigned long int trg=0; trg<sz; ++trg)
				derivMatrix[src*sz+trg]=0.0;	//make sure it gets cleared out first!
			unsigned int trgESz = srcEptr->TargetELev.size();
			for(unsigned int target=0; target < trgESz; ++target)
			{
				ELevel* trgEptr = srcEptr->TargetELev[target];
				unsigned long int trg = trgEptr->ssLink;
				if(CalcFastRate(srcEptr, trgEptr, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, tmpVal))
				{
#ifdef NDEBUG
				delOccs[src] -= tmpVal;	//store -Rates (Newton's equation)
#else
				Rates[src] += tmpVal;	//add in the rate
#endif			
					useELevel[src] = true;	//flag to say this is A-OK to use in the table
				}
				else
					continue;

				int type = CalculateELevelDeriv(srcEptr, trgEptr,derivFactor, sourceDeriv, targetDeriv, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN);
				
						
				derivMatrix[src*sz+trg] += targetDeriv;
				derivMatrix[src*sz+src] += sourceDeriv;

				if(type==RATE_SCATTER)
				{
					if(srcEptr->carriertype == ELECTRON)
						tmpVal = tmpVal * pt->scatterCBConst;
					else
						tmpVal = tmpVal * pt->scatterVBConst;

					//this assumes all the states in a band are adjacent to one another
					for(unsigned long int ddtrg = startI+src-1; ddtrg < sz; --ddtrg)
					{
						ELevel* ptrDDTrg = data.EStates[ddtrg].ptr;
						if(ptrDDTrg->carriertype != srcEptr->carriertype)
							break;	//no longer matching
						if(ptrDDTrg->imp)
							break;	//no longer in band
						if(ptrDDTrg->bandtail)
							break;	//no longer in band
						if(&ptrDDTrg->getPoint() != pt)
							break;
						if(ddtrg == trg)	//ignore if already calculated
							continue;

						derivMatrix[src*sz+ddtrg] += tmpVal * ptrDDTrg->boltzeUse;
					}
					for(unsigned long int ddtrg = startI+src+1; ddtrg < sz; ++ddtrg)
					{
						ELevel* ptrDDTrg = data.EStates[ddtrg].ptr;
						if(ptrDDTrg->carriertype != srcEptr->carriertype)
							break;	//no longer matching
						if(ptrDDTrg->imp)
							break;	//no longer in band
						if(ptrDDTrg->bandtail)
							break;	//no longer in band
						if (&ptrDDTrg->getPoint() != pt)
							break;
						if(ddtrg == trg)	//ignore if already calculated
							continue;

						derivMatrix[src*sz+ddtrg] += tmpVal * ptrDDTrg->boltzeUse;
					}
				}
			}	//loop throughall the different target states
		}	//loop through all the energy source states

		//process contacts
		//will assume the contact derivatives for the rates are zero. Bad assumption
		if(pt->contactData)
			ProcessContactSteadyState(mdl, pt->contactData, mdesc.simulation);
		//the above adds rates to the energy states, and updates RateSums.
		
		//process light
		//assume light derivatives for contacts are zero
		Light(mdl, mdesc, pt);
		//this also adds rates to energy states and Ratesums.

		//now preclear out any derivatives that are meaningless. If an energy level does not have it's rate
		//calculated, then it's change in carriers should be zero. If that is zero, then don't let it
		//screw up the other variables

		//now need to add this points derivatives for each bit that were updated in light
		for(unsigned int src=0; src<sz; ++src)
		{
			ELevel* srcEptr = data.EStates[src+startI].ptr;
			for(unsigned int trg=0; trg<sz; ++trg)
			{
				ELevel* trgEptr = data.EStates[trg+startI].ptr;
				derivMatrix[src*sz+trg] += pt->GetDeriv(srcEptr, trgEptr);
			}
		}



		//now go through and add in all the rates from processing the contacts & light. These are assumed to have zero derivative effects on the rates.

		for (unsigned long int i = 0; i < sz; ++i)
		{
#ifdef NDEBUG
			delOccs[i] -= data.EStates[i].ptr->RateSums.GetNetSumClear();
#else
			Rates[i] += data.EStates[i].ptr->RateSums.GetNetSumClear();	//update the rate to include contact and light, then clear out memory
			delOccs[i] = -Rates[i];	//prepwork for insertion into equation solver
#endif
		}

		double tmpChangeClamp;
		maxChangeSecond = 0.0;
		for(unsigned long int src=0; src<sz; ++src)
		{
			if(useELevel[src] == false)
			{
				for(unsigned long int term=src; term<matrixSz; term += sz)
					derivMatrix[term] = 0.0;	//this should zero out a "column" in the matrix
			}
#ifndef NDEBUG
			tmpChangeClamp = data.EStates[src].occupancy>0.0 ? fabs(Rates[src] / data.EStates[src].occupancy) : fabs(Rates[src]);
			if(tmpChangeClamp > maxChangeSecond)
			{
				maxChangeSecond = tmpChangeClamp;
				limitRate = Rates[src];
				limitRateState = data.EStates[src].ptr;
			}
#endif
		}
#ifndef NDEBUG
		ProgressDouble(47, maxChangeSecond);
#endif

		if(maxChangeSecond < SSN_EXIT_RATE_PERCENT_CHANGE_SEC)	//nominally 1.0e-3
			break;	//the rates are for the most part zero.

		//the following solves [Matrix][del_Occ]=[Rates], destroys [Matrix], and stores the answer [del_Occ] in [Rates]
		kThread->shareData.KernelcurInfoMsg = MSG_PROCESSJACOBIAN;
		if (SolveDenseOneTime(kThread, derivMatrix, delOccs, sz) == false)
		{
			ProgressCheck(64, false);	//send notification to trace file of failed output
			DisplayProgress(58, 0);		//tell user in dialogue box
			break;	//get out of the loop so it just outputs what it has
		}

		//at this point, rates now contains delOcc for each of the states!
		maxPercentChange = 0.0;
		double absMaxPChange = 0.0;
		
		double largestClamp = 0.0;
		double occ, avail;
		for(unsigned long int i=0; i<sz; ++i)
		{
			data.EStates[i].prevEf = data.EStates[i].curEf;

			double kT = KB * data.EStates[i].ptr->getPoint().temp;
			float degen = data.EStates[i].ptr->imp ? data.EStates[i].ptr->imp->getDegeneracy() : 1.0;

			if(data.EStates[i].OccMatches)
			{
				if(delOccs[i] < 0.0 && -delOccs[i] > data.EStates[i].occupancy)
					delOccs[i] = -0.5 * data.EStates[i].occupancy;
				else if(delOccs[i] > 0.0 && delOccs[i] > data.EStates[i].NStates - data.EStates[i].occupancy)
					delOccs[i] = 0.5 * (data.EStates[i].NStates - data.EStates[i].occupancy);

				occ= data.EStates[i].occupancy + delOccs[i];
				avail = data.EStates[i].NStates - occ;
			}
			else
			{
				//occupancy doesn't match
				if(delOccs[i] > 0.0 && delOccs[i] > data.EStates[i].occupancy)	//is it adding more than what's currently available
					delOccs[i] = 0.5 * data.EStates[i].occupancy;	//delOccs stays as if occupancy matches
				else if(delOccs[i] < 0.0 && -delOccs[i] > data.EStates[i].NStates - data.EStates[i].occupancy)	//losing carriers, but occ=avail, so to get actual occ do (-)
					delOccs[i] = -0.5 * (data.EStates[i].NStates - data.EStates[i].occupancy);

				avail = data.EStates[i].occupancy - delOccs[i];
				occ = data.EStates[i].NStates - avail;
			}

			data.EStates[i].curEf = RelEfFromOcc(occ, avail, data.EStates[i].ptr->max - data.EStates[i].ptr->min, kT, degen);
			if(data.EStates[i].curEf <= 0.0)
			{
				data.EStates[i].occupancy = occ;
				data.EStates[i].OccMatches = true;
				data.EStates[i].ptr->SetBeginOccStates(occ);	//make sure it is a valid value
				data.EStates[i].ptr->SetEndOccStates(occ);	//because the prep all puts end into begin before calculating... Want to be able to keep moving forward.
			}
			else
			{
				data.EStates[i].occupancy = avail;
				data.EStates[i].OccMatches = false;
				data.EStates[i].ptr->SetBeginOccStatesOpposite(avail);	//make sure it is a valid value
				data.EStates[i].ptr->SetEndOccStatesOpposite(avail);	//because the prep all puts end into begin before calculating... Want to be able to keep moving forward.
			}
			//check for clamping the occupancy adjustments
			//it must be done uniformly across everything, or the occupancies will result in divergence
			//due to the math not cancelling out properly. The matrix will short change some areas
			//that were meant to cancel other stuff out. Doing it on an individual basis very bad...
			tmpChangeClamp = fabs(data.EStates[i].curEf - data.EStates[i].prevEf);
			if(tmpChangeClamp > largestChange)
				largestChange = tmpChangeClamp;

#ifndef NDEBUG	//only do this in debug versions
			tmpChangeClamp = data.EStates[i].occupancy>0.0 ? delOccs[i] / data.EStates[i].occupancy: delOccs[i]*1e10;	//this is the percentage change. Note rates is del_occ here
			if(-tmpChangeClamp > absMaxPChange)
			{
				absMaxPChange = -tmpChangeClamp;	//update the maximum percent change desired
				maxPercentChange = tmpChangeClamp;
				delOcc = delOccs[i];
				OccLimitState = data.EStates[i].ptr;
			}
			else if(tmpChangeClamp > absMaxPChange)
			{
				absMaxPChange = tmpChangeClamp;	//update the maximum percent change desired
				maxPercentChange = tmpChangeClamp;
				delOcc = delOccs[i];
				OccLimitState = data.EStates[i].ptr;
			}
#endif

		}
		DisplayProgressDoub(18,maxPercentChange);
		
		if (mdesc.simulation->outputs.OutputAllIndivRates)
		{
			std::stringstream fname;
			fname << mdesc.simulation->OutputFName << "_Pt" << pt->adj.self << "_" << mdesc.simulation->curiter;
			WriteEnergyStates(mdl, fname.str());	//debug to write out states!
		}
	
	}

	//clear out the insane amount of memory used
	delete [] derivMatrix;
#ifndef NDEBUG
	delete [] Rates;
#endif
	delete [] useELevel;
	delete [] delOccs;

	return(largestChange);
}

unsigned int FindELevelInvector(std::vector<ELevel*> src, ELevel* lookfor, unsigned int start, unsigned int stopnoinclude)
{
	unsigned int sz = src.size();
	if(stopnoinclude > sz)
		stopnoinclude = sz;
	if(start >= stopnoinclude)
		start = 0;

	unsigned int index;
	for(index = start; index < stopnoinclude; ++index)
	{
		if(lookfor == src[index])
			return(index);
	}
	return(sz);	//an index that doesn't work - greater than size, so we know it was not found
}

double SSNewton(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, bool fullMatrix)
{
//	std::vector<ELevel*>	EStates;	//a std::vector of all energy states, that must be built at least once
	time_t TotStart = time(NULL);
	time_t predictTime = TotStart;
	time_t startRotate = TotStart;
	
	
	double timeelapse = 0.0;
	double largestChange = 0.0;
	/*
	if(mdesc.simulation->EnabledRates.Current==false)	//if this is false, we can make stuff go a lot quicker!
	{
		time_t now;
		CalcFirstSSRates(mdl, mdesc);	//get the updated rates
		OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately

		for(std::map<long int, Point>::iterator pt=mdl.gridp.begin(); pt!= mdl.gridp.end(); ++pt)
		{
			SSNewtonPoint(mdl, mdesc, kThread, &(pt->second), data);	//it should just solve it independently for each!
		}

		CalcBandStruct(mdl, mdesc);
		//clear out old optical data
		if(mdesc.simulation->OpticalFluxOut)
			mdesc.simulation->OpticalFluxOut->ClearAll();
		for(unsigned int i=0; i<mdesc.materials.size(); ++i)
		{
			if(mdesc.materials[i]->OpticalFluxOut)
				mdesc.materials[i]->OpticalFluxOut->ClearAll();
		}

		for(std::map<long int, Point>::iterator ptIt = mdl.gridp.begin(); ptIt != mdl.gridp.end(); ++ptIt)
		{
			if(ptIt->second.LightEmissions)
				ptIt->second.LightEmissions->light.clear();	//make sure there is no light intensities piling up
		}

		PrepAllSFirst(mdl, mdesc);	//now get the data properly setup for the current state
		CalcFirstSSRates(mdl, mdesc);	//get the updated rates
		OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately
		SaveState(mdl, mdesc);
		now = time(NULL);
		timeelapse = now - TotStart;
		DisplayProgressDoub(19,timeelapse);

		return(0);
	}
	*/
	unsigned long int sz = data.EStates.size();
	
	unsigned long int matrixSz = sz*sz;
#ifndef NDEBUG
	double* Rates = NULL;
	DisplayProgress(59, sz);
#endif
	double* derivMatrix = NULL;
	double* delOccs = NULL;
	bool* useELevel = NULL;
	try
	{
#ifndef NDEBUG
		Rates = new double[sz];
#endif
		useELevel = new bool[sz];
		delOccs = new double[sz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
		return(-1);
	}
	try
	{
		derivMatrix = new double[matrixSz];
	}
	catch (...)
	{
		ProgressCheck(63, false);
		DisplayProgress(57, sz);
#ifndef NDEBUG
		delete [] Rates;
#endif
		delete [] useELevel;
		delete [] delOccs;
		return(-1);
	}

	//CALC DERIV MATRIX
	CalcAnalyticalJacobian(data, mdl, mdesc, kThread, derivMatrix, delOccs, useELevel, sz, fullMatrix);



#ifndef NDEBUG
	for (unsigned long int i = 0; i < sz; ++i)
		Rates[i] = -delOccs[i];	//prepwork for insertion into equation solver
#endif

	
	kThread->shareData.KernelcurInfoMsg = (fullMatrix) ? MSG_PROCESSJACOBIANFULL : MSG_PROCESSJACOBIAN;
	if (SolveDenseOneTime(kThread, derivMatrix, delOccs, sz) == false)
	{
		ProgressCheck(64, false);	//send notification to trace file of failed output
		DisplayProgress(58, 0);		//tell user in dialogue box
		delete [] derivMatrix;
		delete [] useELevel;
		delete [] delOccs;
#ifndef NDEBUG
		delete [] Rates;
#endif
		return(-1.0);
	}

	////CLAMP
		
	double clamp = DetermineClamp(data, delOccs, sz, largestChange);	//largestChange is calculated in here
	

	//update everything that really matters
	for(unsigned int src=0; src<sz; ++src)
	{
		data.EStates[src].prevEf = data.EStates[src].curEf;
		data.EStates[src].AddOcc(delOccs[src] * clamp);	//updates occupancy and curEf
		data.EStates[src].UpdateModelOccupancy();	//updates mdl.EStates based on curEf.
	}
	/*
	All the code below should be taken care of in Determine clamp and the update model
	for(unsigned long int i=0; i<sz; ++i)
	{
		data.EStates[i].prevEf = data.EStates[i].curEf;
		
		double kT = KB * data.EStates[i].ptr->getPoint().temp;
		float degen = data.EStates[i].ptr->imp ? data.EStates[i].ptr->imp->degeneracy : 1.0;
		
		if(data.EStates[i].OccMatches)
		{
			if(delOccs[i] < 0.0 && -delOccs[i] > data.EStates[i].occupancy)
				delOccs[i] = -0.5 * data.EStates[i].occupancy;
			else if(delOccs[i] > 0.0 && delOccs[i] > data.EStates[i].NStates - data.EStates[i].occupancy)
				delOccs[i] = 0.5 * (data.EStates[i].NStates - data.EStates[i].occupancy);

			occ= data.EStates[i].occupancy + delOccs[i] * clamp;
			avail = data.EStates[i].NStates - occ;
		}
		else
		{
			//occupancy doesn't match
			if(delOccs[i] > 0.0 && delOccs[i] > data.EStates[i].occupancy)	//is it adding more than what's currently available
				delOccs[i] = 0.5 * data.EStates[i].occupancy;	//delOccs stays as if occupancy matches
			else if(delOccs[i] < 0.0 && -delOccs[i] > data.EStates[i].NStates - data.EStates[i].occupancy)	//losing carriers, but occ=avail, so to get actual occ do (-)
				delOccs[i] = -0.5 * (data.EStates[i].NStates - data.EStates[i].occupancy);

			avail = data.EStates[i].occupancy - delOccs[i] * clamp;
			occ = data.EStates[i].NStates - avail;
		}
		
		if(occ!=0.0 && avail!=0.0)
			data.EStates[i].curEf = RelEfFromOcc(occ, avail, data.EStates[i].ptr->max - data.EStates[i].ptr->min, kT, degen);
		else if(occ==0.0)
			data.EStates[i].curEf = -KB * data.EStates[i].ptr->getPoint().temp * log(ABSMINPOS);	//the closest resolvable zero occupancy available
		else if(avail==0.0)
			data.EStates[i].curEf = KB * data.EStates[i].ptr->getPoint().temp * log(ABSMINPOS);

		if(data.EStates[i].curEf <= 0.0)
		{
			data.EStates[i].occupancy = occ;
			data.EStates[i].OccMatches = true;
			data.EStates[i].ptr->SetBeginOccStates(occ);	//make sure it is a valid value
			data.EStates[i].ptr->SetEndOccStates(occ);	//because the prep all puts end into begin before calculating... Want to be able to keep moving forward.
		}
		else
		{
			data.EStates[i].occupancy = avail;
			data.EStates[i].OccMatches = false;
			data.EStates[i].ptr->SetBeginOccStatesOpposite(avail);	//make sure it is a valid value
			data.EStates[i].ptr->SetEndOccStatesOpposite(avail);	//because the prep all puts end into begin before calculating... Want to be able to keep moving forward.
		}
		//check for clamping the occupancy adjustments
		//it must be done uniformly across everything, or the occupancies will result in divergence
		//due to the math not cancelling out properly. The matrix will short change some areas
		//that were meant to cancel other stuff out. Doing it on an individual basis very bad...

#ifndef NDEBUG	//only do this in debug versions
		tmpChangeClamp = data.EStates[i].occupancy>0.0 ? delOccs[i] / data.EStates[i].occupancy: delOccs[i]*1e10;	//this is the percentage change. Note rates is del_occ here
		if(-tmpChangeClamp > absMaxPChange)
		{
			absMaxPChange = -tmpChangeClamp;	//update the maximum percent change desired
			maxPercentChange = tmpChangeClamp;
			delOcc = delOccs[i];
			OccLimitState = data.EStates[i].ptr;
		}
		else if(tmpChangeClamp > absMaxPChange)
		{
			absMaxPChange = tmpChangeClamp;	//update the maximum percent change desired
			maxPercentChange = tmpChangeClamp;
			delOcc = delOccs[i];
			OccLimitState = data.EStates[i].ptr;
		}
#endif
		
		
	}
#ifndef NDEBUG
	DisplayProgressDoub(18,maxPercentChange);
	DebugSteadyState(limitRateState, OccLimitState, limitRate, delOcc, false);
	DebugSteadyStatePlot(mdesc.simulation->curiter, Rates, delOccs, data);
	delete [] Rates;

#endif
	*/
	if (mdesc.simulation->outputs.OutputAllIndivRates)
	{
		std::stringstream fname;
		fname << mdesc.simulation->OutputFName << "_" << mdesc.simulation->curiter;
		WriteEnergyStates(mdl, fname.str());	//debug to write out states!
	}

	// } end initial while loop

	//clear out the insane amount of memory used
//	delete [] occ;
	delete [] derivMatrix;
	
	delete [] useELevel;
	delete [] delOccs;
//	EStates.clear();

	//now do one iteration for outputs.
	//pull these from the other steady state file.
//	PrepAllSFirst(mdl, mdesc);	//now get the data properly setup for the current state
//	CalcFirstSSRates(mdl, mdesc);	//get the updated rates
//	OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately
//	SaveState(mdl, mdesc);
//	end = time(NULL);
//	timeelapse = end - start;
//	DisplayProgressDoub(19,timeelapse);

	return(largestChange);
}

double DetermineClamp(SteadyState_SAAPD& data, double* delOccs, unsigned int sz, double& largestChange)
{
	//at this point, rates now contains delOcc for each of the states!
	double maxPercentChange = 0.0;
	double absMaxPChange = 0.0;
	double occ, avail;
	double largestClamp = 1.0;	//default to no clamp
	largestChange = 0.0;
	double tmpEf, tmpChangeClamp;
	for(unsigned long int i=0; i<sz; ++i)
	{
		float degen = data.EStates[i].ptr->imp ? data.EStates[i].ptr->imp->getDegeneracy() : 1.0;
		double kTi = KBI / data.EStates[i].ptr->getPoint().temp;
		if(data.tolerance > 0.0)
		{
			if(delOccs[i] > 0.0)
			{
			
				if(fabs(data.EStates[i].curEf) < 1.0)
					tmpEf = data.EStates[i].curEf + data.tolerance;
				else
					tmpEf = data.EStates[i].curEf * (1.0 + data.tolerance);

				occ = OccAvailFromRelEf(tmpEf, data.EStates[i].NStates, kTi, data.EStates[i].ptr->max - data.EStates[i].ptr->min, degen);
				double deltaOcc;
				if(tmpEf > 0.0)	//really the avail
				{
					avail = occ;
					occ = data.EStates[i].NStates - avail;
				}
				else
				{
					avail = data.EStates[i].NStates - occ;
				}
				if(data.EStates[i].OccMatches)
					deltaOcc = fabs(occ - data.EStates[i].occupancy);
				else
					deltaOcc = fabs(avail - data.EStates[i].occupancy);

				if(deltaOcc < delOccs[i] && deltaOcc > 0.0)	//noting delOccs > 0.0, so both are positive
				{
					//therefore, it is trying to change beyond the tolerance
					tmpEf = deltaOcc / delOccs[i];	//note delOccs != 0.0
					if(tmpEf < largestClamp)
						largestClamp = tmpEf;
				}
			}
			else if(delOccs[i] < 0.0)
			{
				if(fabs(data.EStates[i].curEf) < 1.0)
					tmpEf = data.EStates[i].curEf - data.tolerance;
				else
					tmpEf = data.EStates[i].curEf * (1.0 - data.tolerance);

				occ = OccAvailFromRelEf(tmpEf, data.EStates[i].NStates, kTi, data.EStates[i].ptr->max - data.EStates[i].ptr->min, degen);

				double deltaOcc;
				if(tmpEf > 0.0)	//really the avail
				{
					avail = occ;
					occ = data.EStates[i].NStates - avail;
				}
				else
				{
					avail = data.EStates[i].NStates - occ;
				}
				if(data.EStates[i].OccMatches)
					deltaOcc = fabs(occ - data.EStates[i].occupancy);
				else
					deltaOcc = fabs(avail - data.EStates[i].occupancy);

				if(deltaOcc < -delOccs[i] && deltaOcc > 0.0)	//noting delOccs < 0.0
				{
					//therefore, it is trying to change beyond the tolerance
					tmpEf = -deltaOcc / delOccs[i];	//note delOccs < 0.0
					if(tmpEf < largestClamp)
						largestClamp = tmpEf;
				}
			}	//end for negative deloccs
		}	//end there is a specified tolerance, thus a clamp is required

		//now determine what the largest change is without the clamp
		if(delOccs[i] != 0.0)	//if it didn't change, the delEf should be zero
		{
			if(data.EStates[i].OccMatches)
			{
				occ= data.EStates[i].occupancy + delOccs[i];
				avail = data.EStates[i].NStates - occ;
			}
			else
			{
				//occupancy doesn't match
				avail = data.EStates[i].occupancy - delOccs[i];
				occ = data.EStates[i].NStates - avail;
			}

			if (occ > 0.0 && avail > 0.0)
				tmpEf = RelEfFromOcc(occ, avail, data.EStates[i].ptr->max - data.EStates[i].ptr->min, 1.0 / kTi, degen);
			else if (occ == 0.0) //the below doesn't matter - these states may change a lot in fermi energy, but the amount of change in carriers is negligible
				tmpEf = data.EStates[i].curEf;//-KB * data.EStates[i].ptr->getPoint().temp * log(ABSMINPOS);	//the closest resolvable zero occupancy available
			else if(avail==0.0)
				tmpEf = data.EStates[i].curEf;//KB * data.EStates[i].ptr->getPoint().temp * log(ABSMINPOS);
			else if(occ < 0.0)
			{
				//the amount less than zero implies how much over it has gone.
				//occ = occupancy+delOccs OR NStates-occupancy+delOccs. This implies delOccs < 0 as occupancy = [0,NStates]
				//knowing delOccs is more negative than the original occupancy
				delOccs[i] -= occ;	//reducing delOccs(-) by occ(-) should set delOccs at a level to make occ=0.0
				delOccs[i] = delOccs[i] * 0.5;	//but we want it to only go half way there as an approximation
				tmpEf = data.EStates[i].curEf;
			}
			else if(avail < 0.0)
			{
				//by similar logic, delOccs[i] must be >0 for this to occur.
				delOccs[i] += avail;	//delOccs(+) + avail(-) should reduce delOccs so occ = NStates
				delOccs[i] = delOccs[i] * 0.5;	//once again, limit it to half
				tmpEf = data.EStates[i].curEf;
			}

			if(fabs(data.EStates[i].curEf) < 1.0)
				tmpChangeClamp = fabs(data.EStates[i].curEf - tmpEf);
			else
				tmpChangeClamp = fabs((data.EStates[i].curEf - tmpEf) / data.EStates[i].curEf);	//if Ef large, go by digits
			if(tmpChangeClamp > largestChange)
				largestChange = tmpChangeClamp;

		}
	}

	if(largestClamp <= 0.0)	//if it gets to zero somehow, don't let it cause any bad changes
		largestClamp = 1.0;

	return(largestClamp);
}

int UpdateOccupancy(SteadyState_SAAPD& data, double* delOccs, unsigned int sz)
{
	if(delOccs == NULL)
		return(-1);


	return(0);
}

int CalcAnalyticalJacobian(SteadyState_SAAPD& data, Model& mdl, ModelDescribe& mdesc, kernelThread* kThread, double* derivMatrix, double* rhs, bool* useELevel, unsigned int sz, bool fullMatrix)
{
	if(derivMatrix == NULL || rhs == NULL || sz == 0)
		return(1);

	time_t predictTime = time(NULL);
	time_t timeelapse = predictTime;
	time_t now = predictTime;
	time_t startRotate = predictTime;
	bool stop = false;
	bool save = false;
	//	double adjustFactor = 1.0e-8;
	double sourceDeriv, targetDeriv;
	Point* curPt = NULL;
	Point* trgPt = NULL;
	SS_CurrentDerivFactors derivFactor;
//	int type;
	double maxPercentChange = 1.0;//mdesc.simulation->accuracy;
	double tmpVal;
	double maxChangeSecond = 1.0;
	double ClampValue = 1.0e200;	//start off allowing very large changes
	ELevel* limitRateState;
	ELevel* OccLimitState;
	double limitRate;
	double delOcc;

	if(mdesc.simulation->OpticalFluxOut)
		mdesc.simulation->OpticalFluxOut->ClearAll();
	for(unsigned int i=0; i<mdesc.materials.size(); ++i)
	{
		if(mdesc.materials[i]->OpticalFluxOut)
			mdesc.materials[i]->OpticalFluxOut->ClearAll();
	}

	for(unsigned long int el=0;el<sz;++el)
	{
		if(data.EStates[el].OccMatches)
		{
			data.EStates[el].ptr->SetBeginOccStates(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStates(data.EStates[el].occupancy);
		}
		else
		{
			data.EStates[el].ptr->SetBeginOccStatesOpposite(data.EStates[el].occupancy);
			data.EStates[el].ptr->SetEndOccStatesOpposite(data.EStates[el].occupancy);
		}
	}
	
	PrepAllSFirst(mdl, mdesc);	//now get the data properly setup for the current state
	//also clears out optical data
//	CalcFirstSSRates(mdl, mdesc);	//get the updated rates
//	OutputSSRates(mdl, mdesc);	//output the steady state rates appropriately
#ifndef NDEBUG
	DebugSteadyState(NULL, NULL, 0.0, 0.0, true);
	DebugSSZeroEffect(true, false, -1);
#endif
	kThread->shareData.KernelcurInfoMsg = MSG_CALCJACOBIAN;
	kThread->shareData.KernelshortProgBarPosition = 0;
	kThread->shareData.KernelshortProgBarRange = sz;
	kThread->shareData.KernelTimeLeft = 0.0;
	kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
//	while(/*adjustFactor < 1.0 && */maxPercentChange >= mdesc.simulation->accuracy && stop==false)
//	{
	limitRateState = OccLimitState = NULL;
	limitRate = delOcc = 0.0;


	unsigned long int prevSrc=0;
	predictTime = time(NULL);	//basically make sure that moved!=0 in the coming time elapse
	for(unsigned long int src=0; src<sz; ++src)	//cycle through all the energy levels
	{
		ELevel* srcEptr = data.EStates[src].ptr;
		if(kThread->shareData.GUIstopSimulation)	//save the state and stop the simulation
		{
			stop = true;
			break;
		}
		if(kThread->shareData.GUIabortSimulation)	//just get out if possible
			return(0);
		
		time(&now);	//load the end time for dif
		timeelapse = difftime(now, predictTime);
		kThread->shareData.KernelshortProgBarPosition = src;
		if(timeelapse > 0.25)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//update the progress bar every 2 seconds or every iteration, whichever is longer
			
			long int moved = src-prevSrc;
			kThread->shareData.KernelTimeLeft = double(sz-src)/double(moved) * timeelapse;
			prevSrc = src;
				//					DisplayProgress(60, src*sz);
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
//			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
			predictTime = now;
		}
		

		useELevel[src] = false;
		//clear out some potential variables
		srcEptr->Rates.clear();
		srcEptr->OpticalRates.clear();
		srcEptr->RateSums.Clear();
		srcEptr->rateIn = srcEptr->rateOut = srcEptr->netRate = 0.0;

		rhs[src] = 0.0;

		sourceDeriv = 0.0;
		Point* srcPt = &srcEptr->getPoint();
		//clear out the array for everything that depends on this source state
		for(unsigned long int trg=0; trg<sz; ++trg)
			derivMatrix[src*sz+trg]=0.0;	//make sure it gets cleared out first!

		if(srcPt->IsContactSink())
			continue;

		unsigned int trgESz = srcEptr->TargetELev.size();
		for(unsigned int target=0; target < trgESz; ++target)
		{
			ELevel* trgEptr = srcEptr->TargetELev[target];
			trgPt = &trgEptr->getPoint();
			unsigned long int trg = trgEptr->ssLink;

			if(CalcFastRate(srcEptr, trgEptr, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, tmpVal, mdesc.simulation->EnabledRates.LightEmissions))
			{
				//the fast rate calculation only creates light if it is needed in calculations - not outputs (which will have them on by default)
				rhs[src] -= tmpVal;	//store -Rates (Newton's equation)
				useELevel[src] = true;	//flag to say this is A-OK to use in the table
			}
			else
				continue;

			time(&now);	//load the end time for dif
			timeelapse = difftime(now, startRotate);
			if(timeelapse > 2.0)	//if tasks are taking a LONG time, notify the user that it's still running every
			{	//update the progress bar every 2 seconds or every iteration, whichever is longer
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
				startRotate = now;

			}

			int typ = CalculateELevelDeriv(srcEptr, trgEptr,derivFactor, sourceDeriv, targetDeriv, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN);
			derivMatrix[src*sz+trg] += targetDeriv;
			derivMatrix[src*sz+src] += sourceDeriv;
			
			if(typ == RATE_SCATTER)
			{
				//need to take the net rate in (tmpVal), and multiply it by the scatter const and
				//the boltzeUse of the other states
				if(srcEptr->carriertype == ELECTRON)
					tmpVal = tmpVal * srcPt->scatterCBConst;
				else
					tmpVal = tmpVal * srcPt->scatterVBConst;

				//this assumes all the states in a band are adjacent to one another
				for(unsigned long int ddtrg = src-1; ddtrg < sz; --ddtrg)
				{
					ELevel* ptrDDTrg = data.EStates[ddtrg].ptr;
					if(ptrDDTrg->carriertype != srcEptr->carriertype)
						break;	//no longer matching
					if(ptrDDTrg->imp)
						break;	//no longer in band
					if(ptrDDTrg->bandtail)
						break;	//no longer in band
					if(&ptrDDTrg->getPoint() != srcPt)
						break;
					if(ddtrg == trg)	//ignore if already calculated
						continue;

					derivMatrix[src*sz+ddtrg] += tmpVal * ptrDDTrg->boltzeUse;
				}
				for(unsigned long int ddtrg = src+1; ddtrg < sz; ++ddtrg)
				{
					ELevel* ptrDDTrg = data.EStates[ddtrg].ptr;
					if(ptrDDTrg->carriertype != srcEptr->carriertype)
						break;	//no longer matching
					if(ptrDDTrg->imp)
						break;	//no longer in band
					if(ptrDDTrg->bandtail)
						break;	//no longer in band
					if(&ptrDDTrg->getPoint() != srcPt)
						break;
					if(ddtrg == trg)	//ignore if already calculated
						continue;

					derivMatrix[src*sz+ddtrg] += tmpVal * ptrDDTrg->boltzeUse;
				}
			}
		}
		//basically the same code all over again, except now we know it's drift-diffusion
		for(unsigned int target=0; target < srcEptr->TargetELevDD.size(); ++target)
		{
			ELevel* trgEptr = srcEptr->TargetELevDD[target].target;
			trgPt = &trgEptr->getPoint();
			unsigned long int trg = trgEptr->ssLink;

			if(CalcFastRate(srcEptr, trgEptr, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN, tmpVal))
			{
				rhs[src] -= tmpVal;
				useELevel[src] = true;	//flag to say this is A-OK to use in the table
			}
			else
				continue;

			time(&now);	//load the end time for dif
			timeelapse = difftime(now, startRotate);
			if(timeelapse > 2.0)	//if tasks are taking a LONG time, notify the user that it's still running every
			{	//update the progress bar every 2 seconds or every iteration, whichever is longer
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
				startRotate = now;

			}

			CalculateELevelDeriv(srcEptr, trgEptr,derivFactor, sourceDeriv, targetDeriv, mdesc.simulation, RATE_OCC_BEGIN, RATE_OCC_BEGIN);
			derivMatrix[src*sz+trg] += targetDeriv;
			derivMatrix[src*sz+src] += sourceDeriv;
			if(derivFactor.factor != 0.0 || derivFactor.factor_ci_srcband != 0.0 || derivFactor.factor_ci_trgband != 0.0)	//if it were negative, there's no need to flip poissontrg-poissonsrc
			{
				double PoissonSrc, PoissonTrg;
				Point* prevPt = NULL;

				for(unsigned long int ddtrg=0; ddtrg<sz; ++ddtrg)
				{
					if(ddtrg==src || ddtrg==trg)	//it's already calculating itself in the derivative - don't add an errant term
						continue;	//the target will be a drift-diffuse rate, so it will automatically be skipped
					ELevel* ptrDDTrg = data.EStates[ddtrg].ptr;
					if(&ptrDDTrg->getPoint() != prevPt)	//prevent looking up the same data over and over again
					{
						prevPt = &ptrDDTrg->getPoint();
						FindPoissonCoeff(prevPt, srcPt->adj.self, PoissonSrc);
						FindPoissonCoeff(prevPt, trgPt->adj.self, PoissonTrg);
					}
					//						type = DetermineRateType(EStates[src], EStates[ddtrg]);	//only looking at the source state, and want to make sure ther eis no other derivative calculated as it would already take this into account (or be zero)

					//for the factor, if the carrier of the odd state (ddtrg) matches the band of the DD rate (carrier of src)
					//then:			 P_ddtrg_s - P_ddtrg_t
					//if different:	 P_ddtrg_t - P_ddtrg_s

					if(ptrDDTrg->carriertype == srcEptr->carriertype)
					{
						derivMatrix[src*sz+ddtrg] += derivFactor.factor * (PoissonSrc - PoissonTrg);
						//the carrier types are the same, now let's check the points
						if(srcPt == prevPt)	//prevPt is the point of ddtrg
						{
							if(ptrDDTrg->imp == NULL && ptrDDTrg->bandtail == false)
							{
								//know the source is in a band, so now ddtrg is the same point, same carrier, not an imp, and not a tail
								//therefore the other derivFactor applies
								derivMatrix[src*sz+ddtrg] += derivFactor.factor_ci_srcband * ptrDDTrg->boltzeUse;
							}
						}
						if(trgPt == prevPt)	//prevPt is the point of ddtrg
						{
							if(ptrDDTrg->imp == NULL && ptrDDTrg->bandtail == false)
							{
								//know the source is in a band, so now ddtrg is the same point, same carrier, not an imp, and not a tail
								//therefore the other derivFactor applies
								derivMatrix[src*sz+ddtrg] += derivFactor.factor_ci_trgband * ptrDDTrg->boltzeUse;
							}
						}
					}
					else //just one part is guaranteed to happen
					{
						derivMatrix[src*sz+ddtrg] += derivFactor.factor * (PoissonTrg - PoissonSrc);
					}
				}				
			}	//this resulted in a current, which can be modified by every other damn energy state
		}	//loop throughall the different target states
	}	//loop through all the energy source states

	kThread->shareData.KernelshortProgBarPosition = sz;
	kThread->shareData.KernelTimeLeft = 0.0;
	kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);	//make the bar fill up...


	//process contacts
	//will assume the contact derivatives for the rates are zero. Bad assumption
	for(unsigned int j=0; j<mdesc.simulation->contacts.size(); j++)
		ProcessContactSteadyState(mdl, mdesc.simulation->contacts[j], mdesc.simulation);
	//the above adds rates to the energy states, and updates RateSums.

	//process light
	//assume light derivatives for contacts are zero
	Light(mdl, mdesc, NULL, kThread);
	//this also adds rates to energy states and Ratesums.

	for (unsigned long int i = 0; i < sz; ++i)
		rhs[i] -= data.EStates[i].ptr->RateSums.GetNetSumClear();	//this must be done after light and contacts are calculated

	//now preclear out any derivatives that are meaningless. If an energy level does not have it's rate
	//calculated, then it's change in carriers should be zero. If that is zero, then don't let it
	//screw up the other variables
	
	ClearUnusedLevels(derivMatrix, rhs, useELevel, sz);
	/*
	The following code has been essentially pushed into ClearUnusedLevels
	maxChangeSecond = 0.0;
	unsigned long int matrixSz = sz*sz;
	for(unsigned long int src=0; src<sz; ++src)
	{
		if(useELevel[src] == false)
		{
			for(unsigned long int term=src; term<matrixSz; term += sz)
				derivMatrix[term] = 0.0;	//this should zero out a "column" in the matrix
		}
#ifndef NDEBUG
		tmpChangeClamp = data.EStates[src].occupancy>0.0 ? fabs(Rates[src] / data.EStates[src].occupancy) : fabs(Rates[src]);
		if(tmpChangeClamp > maxChangeSecond)
		{
			maxChangeSecond = tmpChangeClamp;
			limitRate = Rates[src];
			limitRateState = data.EStates[src].ptr;
		}
#endif
	}
#ifndef NDEBUG
	ProgressDouble(47, maxChangeSecond);
#endif
	*/
	//now go through and add in all the rates from processing the contacts & light. These are assumed to have zero derivative effects on the rates.

	
	//if contactsink - only rates from ProcessContactSteadyState!

	kThread->shareData.KernelcurInfoMsg = MSG_PROCESSJACOBIAN;
	//the following solves [Matrix][del_Occ]=[Rates], destroys [Matrix], uses del_Occ as -Rates for input, and stores the answer in [del_Occ]

	
	//now go through and optimize the matrix
	if(!fullMatrix)
		OptimizeMatrix(data, derivMatrix, sz);

	return(0);
}

int ClearUnusedLevels(double* derivMatrix, double* rhs, bool* use, unsigned int sz)
{
	if(derivMatrix==NULL || rhs==NULL || use==NULL)
		return(-1);
	for(unsigned long int src=0; src<sz; ++src)
	{
		if(use[src] == false)
		{
			int eqStart = src*sz;
			for(unsigned long int i=0; i<sz; ++i)
			{
				//clear out the row and column of this unused matrix. delC=0, so the column doesn't matter
				//likewise, the rate doesn't actually matter either, so cause 0*0=0
				derivMatrix[eqStart+i]=0.0;	//zero out equation
				derivMatrix[src+i*sz] = 0.0;	//this shouldn't be allowed to affect any other states
			}
			rhs[src] = 0.0;	//ensure 0 = 0
		}
	}
	return(0);
}

int OptimizeMatrix(SteadyState_SAAPD& data, double* derivMatrix, unsigned int sz)
{
	double* occChange = new double[sz];	//smallest relative change that can occur
	double* EfChange = new double[sz];	//largest change that can be expected to occur
	double adjEf = data.tolerance!=0.0 ? data.tolerance : 0.5;	//a pretty large change in Ef approximation

	for(unsigned int eq=0; eq<sz; ++eq) //the equation
	{

		occChange[eq] = data.EStates[eq].occupancy * 1.0e-15;	//the smallest relative change that can occur in the occupancy

		float degen = (data.EStates[eq].ptr->imp) ? data.EStates[eq].ptr->imp->getDegeneracy() : 1.0;
		if(data.EStates[eq].curEf < -adjEf)
		{
			EfChange[eq] = OccAvailFromRelEf(data.EStates[eq].curEf + adjEf, data.EStates[eq].NStates, KBI / data.EStates[eq].ptr->getPoint().temp, data.EStates[eq].ptr->max - data.EStates[eq].ptr->min, degen);
			EfChange[eq] -= data.EStates[eq].occupancy;
		}
		else if(data.EStates[eq].curEf > adjEf)
		{
			EfChange[eq] = OccAvailFromRelEf(data.EStates[eq].curEf - adjEf, data.EStates[eq].NStates, KBI / data.EStates[eq].ptr->getPoint().temp, data.EStates[eq].ptr->max - data.EStates[eq].ptr->min, degen);
			EfChange[eq] -= data.EStates[eq].occupancy;
		}
		else if(data.EStates[eq].curEf > 0.0)
		{
			EfChange[eq] = OccAvailFromRelEf(data.EStates[eq].curEf - adjEf, data.EStates[eq].NStates, KBI / data.EStates[eq].ptr->getPoint().temp, data.EStates[eq].ptr->max - data.EStates[eq].ptr->min, degen);
			EfChange[eq] -= (data.EStates[eq].NStates - data.EStates[eq].occupancy);
			EfChange[eq] = fabs(EfChange[eq]);
		}
		else
		{
			EfChange[eq] = OccAvailFromRelEf(data.EStates[eq].curEf + adjEf, data.EStates[eq].NStates, KBI / data.EStates[eq].ptr->getPoint().temp, data.EStates[eq].ptr->max - data.EStates[eq].ptr->min, degen);
			EfChange[eq] -= (data.EStates[eq].NStates - data.EStates[eq].occupancy);
			EfChange[eq] = fabs(EfChange[eq]);
		}
	}
	for(unsigned int eq=0; eq<sz; ++eq) //the equation
	{
		unsigned long int termStart = eq*sz;
		double relevanceLimit = 0.0;
		//first, look at the smallest occupancy causing the largest change in the rate
		for(unsigned int i=0; i<sz; ++i)
		{
			unsigned long int term = termStart + i;
			double tmp = fabs(occChange[i] * derivMatrix[term]);
			if(tmp > relevanceLimit)
				relevanceLimit = tmp;
		}

		//this specifies a limit, which needs to be checked against how other terms are
		//expected to change
		for(unsigned int i=0; i<sz; ++i)
		{
			unsigned long int term = termStart + i;
			double tmp = fabs(EfChange[i] * derivMatrix[term]);
			if(tmp < relevanceLimit)
				derivMatrix[term] = 0.0;
		}
	}
	delete [] occChange;
	delete [] EfChange;

	return(0);
}

bool CalculateELevelRate(unsigned long int src, unsigned long int trg, std::vector<ELevel*> states, Simulation* sim, double& rate)
{
	rate = 0.0;
	if(states.size() <= src || states.size() <= trg)	//prevent out of bounds index
		return(false);

	if(src == trg)	//same state, who cares
		return(false);

	if(states[src] == NULL || states[trg] == NULL)
		return(false);

	return(CalcFastRate(states[src], states[trg], sim, RATE_OCC_BEGIN, RATE_OCC_BEGIN,rate));
}

int CalculateELevelDeriv(unsigned long int src, unsigned long int trg, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, std::vector<ELevel*>& states, Simulation* sim, int whichsourceSrc, int whichsourceTrg)
{
	derivFact.factor = 0.0;
	derivFact.factor_ci_srcband = derivFact.factor_ci_trgband = 0.0;
	derivSource = 0.0;
	derivTarget = 0.0;
	if(states.size() <= src || states.size() <= trg)	//prevent out of bounds index
		return(0.0);

	if(src == trg)	//this is REALLY complicated
		return(0.0);

	if(states[src] == NULL || states[trg] == NULL)
		return(0.0);

	int type = states[src]->DetermineRateType(states[trg]);
	switch(type)
	{

		case RATE_GENERATION:
			if(sim->EnabledRates.ThermalGen)
				CalcGenRecDerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_RECOMBINATION:
			if(sim->EnabledRates.ThermalRec)
				CalcGenRecDerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH1:
			if(sim->EnabledRates.SRH1)
				CalcSRH12DerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH2:
			if(sim->EnabledRates.SRH2)
				CalcSRH12DerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH3:
			if(sim->EnabledRates.SRH3)
				CalcSRH34DerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH4:
			if(sim->EnabledRates.SRH4)
				CalcSRH34DerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SCATTER:
			if(sim->EnabledRates.Scatter)
				CalcScatterDerivFast(states[src], states[trg], whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_DRIFTDIFFUSE:
			if(sim->EnabledRates.Current)
				CalculateCurrentDerivFast(states[src], states[trg], derivFact, derivSource, derivTarget, whichsourceSrc, whichsourceTrg);
			break;
		case RATE_UNDEFINED:
			//then it is not going to have any effect aside from how the band diagram changes near it
		default:
			break;
	}
	return(type);
}


int CalculateELevelDeriv(ELevel* src, ELevel* trg, SS_CurrentDerivFactors& derivFact, double& derivSource, double& derivTarget, Simulation* sim, int whichsourceSrc, int whichsourceTrg)
{
	derivFact.factor = 0.0;
	derivFact.factor_ci_srcband = derivFact.factor_ci_trgband = 0.0;
	derivSource = 0.0;
	derivTarget = 0.0;
	
	if(src == trg)	//this is REALLY complicated
		return(0.0);

	if(src == NULL || trg == NULL)
		return(0.0);

	int type = src->DetermineRateType(trg);
	switch(type)
	{

		case RATE_GENERATION:
			if(sim->EnabledRates.ThermalGen)
				CalcGenRecDerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_RECOMBINATION:
			if(sim->EnabledRates.ThermalRec)
				CalcGenRecDerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH1:
			if(sim->EnabledRates.SRH1)
				CalcSRH12DerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH2:
			if(sim->EnabledRates.SRH2)
				CalcSRH12DerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH3:
			if(sim->EnabledRates.SRH3)
				CalcSRH34DerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SRH4:
			if(sim->EnabledRates.SRH4)
				CalcSRH34DerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_SCATTER:
			if(sim->EnabledRates.Scatter)
				CalcScatterDerivFast(src, trg, whichsourceSrc, whichsourceTrg, derivSource, derivTarget);
			break;
		case RATE_DRIFTDIFFUSE:
			if(sim->EnabledRates.Current)
				CalculateCurrentDerivFast(src, trg, derivFact, derivSource, derivTarget, whichsourceSrc, whichsourceTrg);
			break;
		case RATE_UNDEFINED:
			//then it is not going to have any effect aside from how the band diagram changes near it
		default:
			break;
	}
	return(type);
}