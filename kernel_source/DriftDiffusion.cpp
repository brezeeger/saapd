#include "DriftDiffusion.h"
#include "BasicTypes.h"
#include "transient.h"
#include "model.h"
#include "physics.h"
#include "contacts.h"

double CalcCurrentFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, bool forceCalc, PosNegSum* AccuRate)
{

	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();
	if(!forceCalc)
	{
		if(srcPt->contactData)
		{
			if(srcPt->contactData->TestRecCurrent(srcPt, trgPt))
				return(0.0);
//			if(CalcRateViaContact(srcE, trgE, sim->mdesc->Ldimensions))
//				return(ret);
		}
		if(trgPt->contactData)
		{
			if(trgPt->contactData->TestRecCurrent(srcPt, trgPt))
				return(0.0);
		}
	}
//	if(srcPt->MatProps != trgPt->MatProps)	//this is a heterojunction point. Crazy schtuff happens that nobody really knows.
//	{
//		return(CalculateThermalEmissionRateFast(srcE, trgE, whichSourceSrc, whichSourceTrg));
//	}

	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);

//	if(sourceOcc == 0.0 || sourceOcc == sourceNum || targetOcc == 0.0 || targetOcc == targetNum)
//	{
//		return(CalculateExtremeDD(srcE, trgE, sim, whichSource));
//	}
//difEf no longer goes to infinity and there are checks built in now for empty/full issues

	
	double widthiS = srcPt->widthi.x;
	double widthiT = trgPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;
	double muS, muT, scatterPartS, scatterPartT;
	double difEf = -CalculateDifEf(srcE,trgE, whichSourceSrc, whichSourceTrg);	//returns positive if rate out. We want rate in positive
	double NRate;
	double vTarget, vSource;	//the drift velocities for each side
	double barrierSrc, barrierTrg;
	//note, difEf returns positive for carriers flowing out of the source regardless of CB or VB
	//we want it to return positive for carriers flowing INTO the source

	//first things first, what is the type...
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, ELECTRON);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, ELECTRON);
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, HOLE);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, HOLE);
	}

	vTarget = muT * difEf * distancei;	//drift velocity, just care about net rate, not even ratein/out
	
	//alters the E(k) distribution. Formulated such that RateOut-RateIn results in the net drift velocity
	// mobility = -v_drift / Electric field. Parallelized to difference in fermi function as by the simpler
	//proven equation J_n = n mu_n dEfn/dx (q, depending if Ef in eV or J)
/*
	if(vTarget > trgE->velocity)	//there's a max velocity - doubtful this will ever be reached too often, but current
	{
		vTarget = trgE->velocity;	//does saturate when the voltage is over loaded
	}
	if(vTarget < -trgE->velocity)
	{
		vTarget = -trgE->velocity;
	}
*/
	vSource = muS * difEf * distancei;
	/*
	if(vSource > srcE->velocity)
	{
		vSource = srcE->velocity;
	}
	if(vSource < -srcE->velocity)
	{
		vSource = -srcE->velocity;
	}
	*/
	if (!AccuRate)
	{
		NRate = 0.5 * (targetOcc * barrierTrg * widthiT * vTarget * (sourceAvail * srcE->boltzeUse * scatterPartS)
			+ sourceOcc * barrierSrc * widthiS * vSource * (targetAvail * trgE->boltzeUse * scatterPartT));
		//this is rate in (acp)
	}
	else
	{
		NRate = 0.0;
		bool splitTarget = (targetOcc > targetAvail && targetOcc * targetAvail <= targetNum * targetNum * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceAvail > sourceOcc && sourceOcc * sourceAvail <= sourceNum * sourceNum * 0.0009765625);	//just shift exponent bits
		//first part to check is the first line of nRate
		//ALWAYS ADD POSITIVE VALUES !!
		if (!splitTarget && !splitSource)
		{
			double tmpRate = 0.5 * (targetOcc * barrierTrg * widthiT * vTarget * (sourceAvail * srcE->boltzeUse * scatterPartS));
			AccuRate->AddValue(tmpRate);
			NRate += tmpRate;
		}
		else if (splitTarget && splitSource)
		{
			//targetOcc = targetNum - targetAvail;
			//sourceAvail = sourceNum - sourceOcc;

			double cnst = 0.5 * (barrierTrg * widthiT * vTarget * (srcE->boltzeUse * scatterPartS));
			AccuRate->AddValue(cnst * targetNum * sourceNum);
			AccuRate->AddValue(cnst * targetAvail * sourceOcc);
			AccuRate->SubtractValue(cnst * targetNum * sourceOcc);
			AccuRate->SubtractValue(cnst * sourceNum * targetAvail);
			NRate += cnst * targetOcc * sourceAvail;
		}
		else if (splitTarget)
		{
			//targetOcc = targetNum - targetAvail;
			double cnst = 0.5 * (barrierTrg * widthiT * vTarget * (sourceAvail * srcE->boltzeUse * scatterPartS));
			AccuRate->AddValue(cnst * targetNum);
			AccuRate->SubtractValue(cnst * targetAvail);
			NRate += cnst * targetOcc;
		}
		else //if (splitSource)
		{
			//sourceAvail = sourceNum - sourceOcc;
			double cnst = 0.5 * (targetOcc * barrierTrg * widthiT * vTarget * (srcE->boltzeUse * scatterPartS));
			AccuRate->AddValue(cnst * sourceNum);
			AccuRate->SubtractValue(cnst * sourceOcc);
			NRate += cnst * sourceAvail;
		}


		//now do the second part of the drift diffusion terms
		splitTarget = (targetAvail > targetOcc && targetOcc * targetAvail <= targetNum * targetNum * 0.0009765625);	//(1/1024) = just shift bits
		splitSource = (sourceOcc > sourceAvail && sourceOcc * sourceAvail <= sourceNum * sourceNum * 0.0009765625);	//just shift exponent bits
		//ALWAYS ADD POSITIVE VALUES !!
		if (!splitTarget && !splitSource)
		{
			double tmpRate = 0.5 * (sourceOcc * barrierSrc * widthiS * vSource * (targetAvail * trgE->boltzeUse * scatterPartT));
			AccuRate->AddValue(tmpRate);
			NRate += tmpRate;
		}
		else if (splitTarget && splitSource)
		{
			//targetAvail = targetNum - targetOcc;
			//sourceOcc = sourceNum - sourceAvail;

			double cnst = 0.5 * (barrierSrc * widthiS * vSource * (trgE->boltzeUse * scatterPartT));
			AccuRate->AddValue(cnst * targetNum * sourceNum);
			AccuRate->AddValue(cnst * targetOcc * sourceAvail);
			AccuRate->SubtractValue(cnst * targetNum * sourceAvail);
			AccuRate->SubtractValue(cnst * sourceNum * targetOcc);
			NRate += cnst * sourceOcc * targetAvail;
		}
		else if (splitTarget)
		{
			//targetAvail = targetNum - targetOcc;
			double cnst = 0.5 * (sourceOcc * barrierSrc * widthiS * vSource * (trgE->boltzeUse * scatterPartT));
			AccuRate->AddValue(cnst * targetNum);
			AccuRate->SubtractValue(cnst * targetOcc);
			NRate += cnst * targetAvail;
		}
		else //if (splitSource)
		{
			//sourceOcc = sourceNum - sourceAvail;
			double cnst = 0.5 * (barrierSrc * widthiS * vSource * (targetAvail * trgE->boltzeUse * scatterPartT));
			AccuRate->AddValue(cnst * sourceNum);
			AccuRate->SubtractValue(cnst * sourceAvail);
			NRate += cnst * sourceOcc;
		}
		
	}
	
	
	return(NRate);

	/*

	Point* srcPt = srcE->getPoint();
	Point* trgPt = trgE->getPoint();
	if(srcPt == NULL || trgPt == NULL || srcPt == trgPt)
	{
		ProgressCheck(60, false);
		return(0.0);
	}
	double distancei = 1.0/(srcPt->position - trgPt->position).Magnitude();
	double difEf = CalculateDifEf(srcE, trgE, srcE->carriertype, whichSourceSrc, whichSourceTrg);

	double sourceOcc, targetOcc, sourceAvail, targetAvail, difBandEdge, percentUse, widthi, mu;
	
	
	//positive Ef if carriers are leaving the source
	if(difEf == 0.0)
		return(0.0);
	else if(difEf > 0.0)
	{
		srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
		trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);

		widthi = srcPt->widthi.x;
		if(srcE->carriertype == ELECTRON)
		{
			mu = srcPt->MatProps->MuN;
			percentUse = trgE->boltzeUse * trgPt->scatterCBConst;
		}
		else
		{
			mu = srcPt->MatProps->MuP;
			percentUse = trgE->boltzeUse * trgPt->scatterVBConst;
		}

		difBandEdge = GetBarrierReduction(srcE, trgPt, srcE->carriertype);
	}
	else
	{
		widthi = trgPt->widthi.x;
		srcE->GetOccupancyOcc(whichSourceSrc, targetOcc, targetAvail);	//swap 'em
		trgE->GetOccupancyOcc(whichSourceTrg, sourceOcc, sourceAvail);	//currents only make sense as to what is leaving
		if(trgE->carriertype == ELECTRON)  //so that is the one that is chosen - the targe might be the source, but the output
		{									//still has to be in reference to the source
			mu = trgPt->MatProps->MuN;
			percentUse = srcE->boltzeUse * srcPt->scatterCBConst;
		}
		else
		{
			mu = trgPt->MatProps->MuP;
			percentUse = srcE->boltzeUse * srcPt->scatterVBConst;
		}

		difBandEdge = GetBarrierReduction(trgE, srcPt, trgE->carriertype);
	}

	double NRateOut = sourceOcc * mu * difEf * widthi * distancei * difBandEdge * percentUse * targetAvail;

	//the NRateOut will be calculated as what's happening with the original source, because difEf did not change.
	return(-NRateOut);
	*/

}

//if the source occupancy changes, what is the change in rate in the target
int CalculateCurrentDerivFast(ELevel* srcE, ELevel* trgE, SS_CurrentDerivFactors& derivFactor, double& derivSource, double& derivTarget, int whichSourceSrc, int whichSourceTrg)
{
	derivSource = derivTarget = 0.0;
	derivFactor.factor = 0.0;
	derivFactor.factor_ci_srcband = 0.0;
	derivFactor.factor_ci_trgband = 0.0;


	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();

//	if(srcPt->MatProps != trgPt->MatProps)	//this is a heterojunction point. Crazy schtuff happens that nobody really knows.
//	{
//		return(CalculateThermalEmissionRateDeriv(srcE, trgE, sim, whichSource));
//	}

	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);

	
	double widthiS = srcPt->widthi.x;
	double widthiT = trgPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;
	double muS, muT, scatterPartS, scatterPartT;
	double difEf = -CalculateDifEf(srcE,trgE, whichSourceSrc, whichSourceTrg);	//difEf is positive if rate in to source
	double vTarget, vSource;
	double barrierSrc, barrierTrg;

	//note, difEf returns positive for carriers flowing out of the source regardless of CB or VB
	//we want it to return positive for carriers flowing INTO the source

	//first things first, what is the type...
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, ELECTRON);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, ELECTRON);
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, HOLE);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, HOLE);
	}

	vTarget = muT * difEf * distancei;	//half go in one direction, but the electric field
	//alters the E(k) distribution. Formulated such that RateOut-RateIn results in the net drift velocity
	// mobility = -v_drift / Electric field. Parallelized to difference in fermi function as by the simpler
	//proven equation J_n = n mu_n dEfn/dx (q, depending if Ef in eV or J)
	bool constTVelocity = false;	//these are constant, so the derivative for the velocity term is zero
	bool constSVelocity = false;
	if(vTarget > trgE->velocity)	//there's a max velocity - doubtful this will ever be reached too often, but current
	{
		vTarget = trgE->velocity;	//does saturate when the voltage is over loaded
		constTVelocity = true;
	}
	if(vTarget < -trgE->velocity)
	{
		vTarget = -trgE->velocity;
		constTVelocity = true;
	}

	vSource = muS * difEf * distancei;
	if(vSource > srcE->velocity)
	{
		vSource = srcE->velocity;
		constSVelocity = true;
	}
	if(vSource < -srcE->velocity)
	{
		vSource = -srcE->velocity;
		constSVelocity = true;
	}


		//getting this derivative is a slight pain in the @$$. The transfer of carriers moves the fermi level with respect to the energy used
	//the transfer of carriers move the bands
	//the transfer of carriers changes the availability and number of carriers in the source
	//the transfer of carriers changes the partition function used to split up the current
	//this state may be considered either the source or the target
	//this state will have changes based on whether it is a hole or an electron
	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;

	FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
	FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
	FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
	FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);
//	double poisson = SourceAffectSource + TargetAffectTarget - (SourceAffectTarget + TargetAffectSource);	//Pss + Ptt - Pst - Pts
	//this is the same whether source or target are flipped
	//these calculate the effect on the band diagram as if a hole was added.
	//so if the source receives a hole, the target loses a hole.
	//therefore E_S = SS - TS, and E_T = ST - TT
	//however, this is looking at the difference in Ef, and we have Efs - Eft.
	//therefore, the difference says the energy levels affecting the target (2nd letter)
	//should have the opposite sign E_S - E_T = SS + TT - (ST + TS)
	//reverse the target/source, so swap S<==>T = TT + SS - (TS + ST)
	//for holes, we care about Eft - Efs, to have positive flow out of the source.
	//so hole flowing out of source causes a sign flip, and caring about the opposite energy
	//also causes a sign flip, so it cancels itself out.

	
	//the only thing we're going to worry about here is derivSOnly. All the other stuff
	//is only used in code that never worked (which isn't surprising) because the dependencies weren't all calculated
	//note, difEf for CB holds Eft-Efs and for VB holds Efs-Eft

	double dVs_s = 0.0;	//default as if constVelocity is set, at which point changes
	double dVt_s = 0.0;	//in band structure or occupancy will have no effect
	double dVs_t = 0.0;	//on the net velocity of the carrier
	double dVt_t = 0.0;
	double dVs_i = 0.0;
	double dVt_i = 0.0;

	

	double dEft_s, dEfs_s, dEft_t, dEfs_t;



	if(srcE->carriertype == ELECTRON) //if it's here, then both carrier types are electrons and it's in the CB.
	{
		//dealing with the conduction band. difEf is Efs-Eft
		dEfs_s = -SourceAffectSource + KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		dEft_s = -SourceAffectTarget;

		dEfs_t = -TargetAffectSource;
		dEft_t = -TargetAffectTarget + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);

		if(constSVelocity == true || MY_IS_FINITE(dEfs_s)==false)	//sourceocc/avail = 0 or close enough, then dEfs_S should be infinite, which would make the velocity constant
		{
			constSVelocity = true;	//just make sure the flag goes up that this velocity should actually be constant so we know the factor ought to be zero.
		}
		else
		{
			dVs_s = (dEft_s - dEfs_s) * muS * distancei;
			dVs_t = (dEft_t - dEfs_t) * muS * distancei;
			if(MY_IS_FINITE(dVs_s)==false)
				dVs_s=0.0;
			if(MY_IS_FINITE(dVs_t)==false)
				dVs_t=0.0;
			dVs_i = muS * distancei;
		}

		if(constTVelocity == true || MY_IS_FINITE(dEft_t)==false)
		{
			constTVelocity = true;	//just make sure if it somehow slipped past
		}
		else
		{
			dVt_t = (dEft_t - dEfs_t) * muT * distancei;
			dVt_s = (dEft_s - dEfs_s) * muT * distancei;
			if(MY_IS_FINITE(dVt_t)==false)
				dVt_t=0.0;
			if(MY_IS_FINITE(dVt_s)==false)
				dVt_s=0.0;
			dVt_i = muT * distancei;
		}
	}
	else
	{
		dEfs_s = SourceAffectSource - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		dEft_s = SourceAffectTarget;

		dEfs_t = TargetAffectSource;
		dEft_t = TargetAffectTarget - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);

		if(constSVelocity == true || MY_IS_FINITE(dEfs_s)==false)	//then dEfs_S should be infinite, which would make the velocity constant
		{
			constSVelocity = true;
		}
		else
		{
			dVs_s = (dEfs_s - dEft_s) * muS * distancei;
			dVs_t = (dEfs_t - dEft_t) * muS * distancei;
			if(MY_IS_FINITE(dVs_s)==false)
				dVs_s=0.0;
			if(MY_IS_FINITE(dVs_t)==false)
				dVs_t=0.0;
			dVs_i = muS * distancei;
		}

		if(constTVelocity == true || MY_IS_FINITE(dEft_t)==false)
		{
			constTVelocity = true;	//just make sure if it somehow slipped past
		}
		else
		{
			dVt_t = (dEfs_t - dEft_t) * muT * distancei;
			dVt_s = (dEfs_s - dEft_s) * muT * distancei;
			if(MY_IS_FINITE(dVt_t)==false)
				dVt_t=0.0;
			if(MY_IS_FINITE(dVt_s)==false)
				dVt_s=0.0;
			dVt_i = muT * distancei;
		}
	}
	double dct_t, dcsFact_t, dcsa_t, dcspart_t, dcsVel_t;
	double dcs_s, dctFact_s, dcta_s, dctpart_s, dctVel_s;	//factors for the first part of the net rate in
	
	dcs_s = 0.5 * widthiS * (targetAvail * trgE->boltzeUse * scatterPartT) * barrierSrc;

	dctFact_s = 0.5 * targetOcc * widthiT * srcE->boltzeUse * barrierTrg;
	dcta_s = -vTarget * scatterPartS;
	dctpart_s = vTarget * (sourceAvail * srcE->boltzeUse * scatterPartS) * scatterPartS;
	dctVel_s = sourceAvail * scatterPartS * dVt_s;
	if(MY_IS_FINITE(dctVel_s)==false)
		dctVel_s=0.0;	//dVt_s can still be nearly infinite, and the multiplication push it over infinite
	derivSource = dcs_s * (vSource + sourceOcc * dVs_s) + dctFact_s * (dcta_s + dctpart_s + dctVel_s);

	dct_t = 0.5*widthiT * (sourceAvail * srcE->boltzeUse * scatterPartS) * barrierTrg;
	dcsFact_t = 0.5 * sourceOcc * widthiS * trgE->boltzeUse  * barrierSrc;
	dcsa_t = -vSource * scatterPartT;
	dcspart_t = vSource * (targetAvail * trgE->boltzeUse * scatterPartT) * scatterPartT;
	dcsVel_t = targetAvail * scatterPartT * dVs_t;
	if(MY_IS_FINITE(dcsVel_t)==false)
		dcsVel_t=0.0;
	derivTarget = dct_t * (vTarget + targetOcc * dVt_t) + dcsFact_t * (dcsa_t + dcspart_t + dcsVel_t);

	//the derivatives expect to receive a factor in front of dEfs - dEft
	//the rate in is calculated as if Eft-Efs for electrons and Efs-Eft for holes.
	//we assume holes, which is what is expected by SS Newton.
	if(dVs_i != 0.0)
		derivFactor.factor += 0.5 * sourceOcc * widthiS * (targetAvail * trgE->boltzeUse * scatterPartT) * barrierSrc * dVs_i;
	if(dVt_i != 0.0)
		derivFactor.factor += 0.5 * targetOcc * widthiT * (sourceAvail * srcE->boltzeUse * scatterPartS) * barrierTrg * dVt_i;
	
	derivFactor.factor_ci_srcband = 0.5 * targetOcc * widthiT * (sourceAvail * srcE->boltzeUse * scatterPartS) * vTarget * scatterPartS;	//multiplied by e^-Ei beta
	derivFactor.factor_ci_trgband = 0.5 * sourceOcc * widthiS * (targetAvail * trgE->boltzeUse * scatterPartT) * vSource * scatterPartT;	//multiplied by e^-Ei beta
	//the derivative goes to the fermi function, and the difference is the same for both v_s and v_t. The factor
	//outside of this differs, so that can be summed up.

#ifndef NDEBUG
	//this corresponds to the valence band and the state is a hole, or the CB and the arb. state is an electron
	if(MY_IS_FINITE(derivSource) == false)
		int debug=0;
	if(MY_IS_FINITE(derivTarget) == false)
		int debug=0;
	if(MY_IS_FINITE(derivFactor.factor) == false)
		int debug=0;
	if(MY_IS_FINITE(derivFactor.factor_ci_srcband) == false)
		int debug=0;
	if(MY_IS_FINITE(derivFactor.factor_ci_trgband) == false)
		int debug=0;
#endif

	return(0);

/*

old code kept for reference as it had some mathematical fixes within it which I will run into again

	if(srcE == NULL || trgE == NULL)
		return(0);
	Point* srcPt = srcE->getPoint();
	Point* trgPt = trgE->getPoint();
	if(srcPt == NULL || trgPt == NULL || srcPt == trgPt)
	{
		ProgressCheck(60, false);
		return(0);
	}
	double distancei = 1.0/(srcPt->position - trgPt->position).Magnitude();
	double difEf = CalculateDifEf(srcE, trgE, srcE->carriertype, whichSourceSrc, whichSourceTrg);	//positive means carriers flow out of the source

	double sourceOcc = srcE->GetBeginOccStates();
	double targetOcc = trgE->GetBeginOccStates();
	double sourceAvail = srcE->GetBeginAvailable();
	double targetAvail = trgE->GetBeginAvailable();
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	double difBandEdge, widthi, mu, partition, derivEfsCs, derivEftCs, derivEfsCt, derivEftCt;
	
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);
	double poissonSAS, poissonSAT, poissonTAS, poissonTAT;
	FindPoissonCoeff(srcPt, trgPt->adj.self, poissonSAT);
	FindPoissonCoeff(srcPt, srcPt->adj.self, poissonSAS);
	FindPoissonCoeff(trgPt, trgPt->adj.self, poissonTAT);
	FindPoissonCoeff(trgPt, srcPt->adj.self, poissonTAS);

	//couple notes... The rate is calculated from whichever state will have the rate out
	//this results in very different derivatives as one has a partition function change
	//the differences in the fermi level move diffently based on whether it is a hole or electron
	//so sign flips for electron/hole, source/target, and we're calculating rate out, and want deriv of rate in
	
	if(difEf >= 0.0)	//the carriers are leaving the source, so the rate is calculated from the source
	{
		
		bool empty = false;
		widthi = srcPt->widthi.x;
		difBandEdge = GetBarrierReduction(srcE, trgPt, srcE->carriertype);
		if(srcE->carriertype == ELECTRON)
		{
			mu = srcPt->MatProps->MuN;
			partition = trgPt->scatterCBConst;
			derivEftCs = -poissonSAS;
			derivEfsCt = -poissonTAS;

			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEfsCs = -poissonSAS + KB*srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
			else
				derivEfsCs = KB*srcPt->temp;
						
			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEftCt = -poissonTAT + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
			else
				derivEftCt = KB * trgPt->temp;

			double prt1;
			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt1 = sourceOcc * (derivEfsCs - derivEftCs);

				derivSource = -mu * difBandEdge * distancei * widthi * (difEf + prt1) * (targetAvail * trgE->boltzeUse * partition);
			}
			else
			{
				prt1 = derivEfsCs;
				derivSource = -mu * difBandEdge * distancei * widthi * (difEf + prt1) * (targetAvail * trgE->boltzeUse * partition);
			}

			
			double prt2 = -difEf;
			double prt3 = difEf * targetAvail * trgE->boltzeUse * partition;
			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt1 = targetAvail * (derivEfsCt - derivEftCt);
				derivTarget = -sourceOcc * mu * (prt1 + prt2 + prt3) * distancei * widthi * difBandEdge * (trgE->boltzeUse * partition);
			}
			else
			{
				prt1 = -derivEftCt;
				derivTarget = -sourceOcc * mu * (prt1 + prt2 + prt3) * distancei * widthi * difBandEdge * (trgE->boltzeUse * partition);
			}
			
			derivFactor = -sourceOcc * mu * distancei * widthi * difBandEdge * (targetAvail * partition * trgE->boltzeUse);	//want dEft/dcx-dEfs/dcx, but returning dEfs/dcx-dEft/dcx because the 
			//future code assumes the latter in the calculation. The derivatives are + for holes and - for electrons, so putting the negative here essentially flips it around

		}
		else
		{
			mu = srcPt->MatProps->MuP;
			partition = trgPt->scatterVBConst;
			derivEftCs = poissonSAS;
			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEfsCs = poissonSAS - KB*srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
			else
				derivEfsCs = -KB * srcPt->temp;
			derivEfsCt = poissonTAS;
			if(targetOcc>DD_SAFEOCC_DIVIDE_ZERO && targetAvail>DD_SAFEOCC_DIVIDE_ZERO)
				derivEftCt = poissonTAT - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
			else
				derivEftCt = -KB * trgPt->temp;

			double prt1 = difEf;
			double prt2;
			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt2 = sourceOcc * (derivEftCs - derivEfsCs);
				derivSource = -mu * difBandEdge * distancei * widthi * (prt1 + prt2) * (targetAvail * trgE->boltzeUse * partition);
			}
			else
			{
				prt2 = -derivEfsCs;
				derivSource = -mu * difBandEdge * distancei * widthi * (prt1 + prt2) * (targetAvail * trgE->boltzeUse * partition);
			}

			prt2 = -difEf;
			double prt3 = difEf * targetAvail * trgE->boltzeUse * partition;

			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt1 = targetAvail * (derivEftCt - derivEfsCt);
				derivTarget = -sourceOcc * mu * distancei * widthi * difBandEdge * (prt1 + prt2 + prt3) * (partition * trgE->boltzeUse);
			}
			else
			{
				prt1 = derivEftCt;
				derivTarget = -sourceOcc * mu * distancei * widthi * difBandEdge * (prt1 + prt2 + prt3) * (trgE->boltzeUse * partition);
			}
			
			derivFactor = sourceOcc * mu * distancei * widthi * difBandEdge * (partition * targetAvail * trgE->boltzeUse);
		}

		//this is used as the factor to ALL the different energy states and their effect on the source state
		//no negative is Rate Out, so negative is rate in.
	}
	else //carriers are leaving the target, so the rate for the source is calculated from the target level
	{
		//deriv here is a bit more complicated, because the rate is actually calculated from the target and the source is changing
		widthi = trgPt->widthi.x;
		difBandEdge = GetBarrierReduction(trgE, srcPt, trgE->carriertype);

		if(trgE->carriertype == ELECTRON)  //so that is the one that is chosen - the targe might be the source, but the output
		{									//still has to be in reference to the source
			mu = trgPt->MatProps->MuN;
			partition = srcPt->scatterCBConst;
			derivEftCs = -poissonSAT;
			derivEfsCt = -poissonTAS;
			
			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEfsCs = -poissonSAS + KB*srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
			else
				derivEfsCs = KB*srcPt->temp;
			
			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEftCt = -poissonTAT + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
			else
				derivEftCt = KB * trgPt->temp;

			double prt1;
			double prt2 = difEf;
			double prt3 = -difEf * sourceAvail * srcE->boltzeUse * partition;

			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt1 = (derivEftCs - derivEfsCs) * sourceAvail;
				derivSource = targetOcc * mu * difBandEdge * distancei * widthi * (prt1+prt2+prt3) * (srcE->boltzeUse * partition);
			}
			else
			{
				prt1 = -derivEfsCs;
				derivSource = targetOcc * mu * difBandEdge * distancei * widthi * (prt1+prt2+prt3) * (srcE->boltzeUse * partition);
			}
			
			prt1 = -difEf;
			

			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt2 = targetOcc * (derivEftCt - derivEfsCt);
				derivTarget = mu * distancei * widthi * difBandEdge * (prt1 + prt2) * (sourceAvail * srcE->boltzeUse * partition);
			}
			else
			{
				prt2 = derivEftCt;
				derivTarget = mu * distancei * widthi * difBandEdge * (prt1 + prt2) * (sourceAvail * srcE->boltzeUse * partition);
			}
			
			derivFactor = -targetOcc * mu * widthi * distancei * difBandEdge * (partition * srcE->boltzeUse * sourceAvail);	//return the factor for (dEfs/dcx - dEft/dcx) (CB wants dEft/dcx - dEfs/dcx)
			//future code assumes the prior in the calculation. The derivatives are + for holes and - for electrons, so putting the negative here essentially flips it around
		}
		else
		{
			mu = trgPt->MatProps->MuP;
			partition = srcPt->scatterVBConst;
			derivEftCs = poissonSAT;
			derivEfsCt = poissonTAS;

			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
				derivEfsCs = poissonSAS - KB*srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
			else
				derivEfsCs = -KB * srcPt->temp;
			if(targetOcc>DD_SAFEOCC_DIVIDE_ZERO && targetAvail>DD_SAFEOCC_DIVIDE_ZERO)
				derivEftCt = poissonTAT - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
			else
				derivEftCt = -KB * trgPt->temp;

			double prt1;
			double prt2 = difEf;	//difEf = Eft- Efs
			double prt3 = -prt2 * sourceAvail * srcE->boltzeUse;

			if(sourceOcc > DD_SAFEOCC_DIVIDE_ZERO && sourceAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt1 = sourceAvail * (derivEfsCs - derivEftCs);
				derivSource = targetOcc * mu * distancei * widthi * difBandEdge * (prt1 + prt2 + prt3) * (srcE->boltzeUse * partition);
			}
			else
			{
				prt1 = derivEfsCs;
				derivSource = targetOcc * mu * distancei * widthi * difBandEdge * (prt1 + prt2 + prt3) * (srcE->boltzeUse * partition);
			}

			prt1 = -difEf;
			if(targetOcc > DD_SAFEOCC_DIVIDE_ZERO && targetAvail > DD_SAFEOCC_DIVIDE_ZERO)
			{
				prt2 = targetOcc * (derivEfsCt - derivEftCt);
				derivTarget = mu * distancei * widthi * difBandEdge * (prt1 + prt2) * (srcE->boltzeUse * sourceAvail * partition);
			}
			else
			{
				prt2 = -derivEftCt;
				derivTarget = mu * distancei * widthi * difBandEdge * (prt1 + prt2) * (srcE->boltzeUse * sourceAvail * partition);
			}

			
			
			derivFactor = targetOcc * mu * widthi * distancei * difBandEdge * (partition * srcE->boltzeUse * sourceAvail);	//return the factor for (dEfs/dcx - dEft/dcx), which is what VB wants
		}

		
		//this is the rate out from the target, which is the rate in to the source, so there is no negative needed
	}

	

	return(0);
	*/
}

std::vector<ELevelRate>::iterator CalculateThermalEmissionRate(ELevel* srcESent, ELevel* trgESent, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcESent->Rates.end();
	ELevel* srcE;
	ELevel* trgE;	
	//first, we ALWAYS want to treat the source as the state that is overcoming the barrier.
	//this is an absolute hack. We know how to calculate the current gonig over the barrier
	//well. We know at equilibrium that the rate coming back is equal.
	//the actual rate going back over is pretty much unknown. It is just "large"
	//any physically based treatments absolutely violate detailed balance (if the fermi levels are equal, the net rate better be zero!)
	//There is a lot of physical play going on between differences in density of states, and the band
	//diagrams already stop being ideal in the presence of large electric fields. At an interface, it
	//must go bonkers. Therefore, all the standard constants we use probably are fluctuating wildly
	//to make it actually work. Net result, we have no idea what the hell is going on.

	//therefore, we know the rate in one direction, so make the rate seem relatively physical in
	//the return direction with the generally same theory as drift diffusion with some slight changes
	//1) ignore states that are not energetic enough to surpass barrier, and clip those that are partial
	//2) make sure the rateOut/RateIn are identical when fermi levels are equal
	//3)tag on an exponential factor for the difference in fermi levels when doing the rate going over the barrier.
	//part 3 imo is absolute bull shit, as is using the source occupancy and target availability to calculate
	//transferring carriers from the target to the source, but as mentioned earlier, no idea what's going on,
	//and this is an approximation that gives a rate (who knows how right/wrong it is!) that other people have
	//accepted as worthy.
	
	double barrier;
	double difEf;
	//starting convention is we want a positive barrier going from source to target
	if(srcESent->carriertype == ELECTRON)
	{
		//just have to subtract electron affinities
		barrier = srcESent->getPoint().MatProps->Phi - trgESent->getPoint().MatProps->Phi;
	}
	else
	{
		barrier = (trgESent->getPoint().MatProps->Phi + trgESent->getPoint().eg) - (srcESent->getPoint().MatProps->Phi + srcESent->getPoint().eg);
	}

	//now if the barrier is negative, we would start from the wrong place in doing the calculation
	if(barrier >= 0.0)
	{
		srcE = srcESent;
		trgE = trgESent;
		difEf = CalculateDifEf(srcE,trgE, whichSource, whichSource);	
	}
	else
	{
		srcE = trgESent;
		trgE = srcESent;
		barrier = -barrier;	//make sure the barrier is positive
		difEf = CalculateDifEf(srcE,trgE, whichSource, whichSource);	
	}

	if(difEf == 0.0)
		return(ret);	//it all just cancels, who the hell cares about doing tons of additional math

	if(srcE->max <= barrier)	//this state can't get above the barrier!
		return(ret);


	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();

	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	srcE->GetOccupancyOcc(whichSource, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSource, targetOcc, targetAvail);

	//we pretty much only care what's going on in the source point...
	double widthi = srcPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;

	double muS, muT, scatterPartS, scatterPartT;
	double velocity = 0.5*(srcE->velocity + trgE->velocity);	//the maximum velocity of this rate between these two energy levels
	double vTarget, vSource;

	

	double beta = (srcPt->temp>0.0) ? KBI / srcPt->temp : 1.0e30;	//huge if infinite
	double CarrierReduction = (srcE->min < barrier) ? exp(beta * (srcE->min-barrier)) : 1.0;

	
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
	}

	//difEf returns positive for source rate out...
	vTarget = 0.5*(velocity - muT * distancei * difEf);
	vSource = 0.5*(velocity + muS * distancei * difEf);
	bool constTVelocity = false;	//these are constant, so the derivative for the velocity term is zero
	bool constSVelocity = false;
	
	if(vTarget > velocity)
	{
		constTVelocity = true;
		vTarget = velocity;
	}
	else if(vTarget < 0.0)
	{
		vTarget = 0.0;
		constTVelocity = true;
	}

	if(vSource > velocity)
	{
		constSVelocity = true;
		vSource= velocity;
	}
	else if(vSource < 0.0)
	{
		constSVelocity = true;
		vSource = 0.0;
	}

	double ROut = sourceOcc * widthi * targetAvail * scatterPartT * trgE->boltzeUse * CarrierReduction;
	double RIn = ROut * vTarget * exp(beta * -difEf);
	ROut = ROut * vSource;

	//now for the derivatives...
	//we are still going to assume that source is still the carriers going over the barrier

	//only need dRout/dcs and dRin/dcs

	double dRST_S, dRTS_S, dRST_T, dRTS_T;	//the actual derivatives. Need all four because we actually don't know which is source and which is target
	double dRST_S_factor = targetAvail * trgE->boltzeUse * scatterPartT * widthi * CarrierReduction;
	double dRTS_S_factor = dRST_S_factor;	//the two linear coefficients of the terms
	double dRST_T_factor = sourceOcc * widthi * trgE->boltzeUse * CarrierReduction;
	double dRTS_T_factor = dRST_T_factor;

	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;

	FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
	FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
	FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
	FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);

	double dEft_s, dEfs_s, dEft_t, dEfs_t;	//derivatives of fermi levels wrt to source/target occ

	double dRST_S_dcs, dRST_S_dVel;	//derivative terms wrt source for rate out
	double dRTS_S_dcs, dRTS_S_dVel, dRTS_S_dfermi;	//derivative terms wrt source for rate in

	double dRTS_T_dat, dRTS_T_dVel, dRTS_T_dpart, dRTS_T_fermi;	//derivative terms wrt target for rate in
	double dRST_T_dat, dRST_T_dVel, dRST_T_dpart;	//derivative terms wrt target for rate out

	double expReverse = exp(beta*-difEf);	//note, difEf = Efs-Eft for electrons, and Eft-Efs for holes (though we still want opposite)
	
	dRTS_S_dcs = vTarget * expReverse;
	

	if(srcE->carriertype == ELECTRON)
	{
		dEft_s = -SourceAffectTarget;
		dEfs_s = -SourceAffectSource + KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		if(/*constSVelocity ||*/ MY_IS_FINITE(dEfs_s) == false)
		{
			//if dEfs_s is infinite, then the velocity is "infinite", but it's capped
			dRST_S_dcs = vSource;
			dRST_S_dVel = 0.0;
			dRTS_S_dVel = 0.0;
			dRTS_S_dfermi = vTarget * beta * expReverse * (dEft_s - (-SourceAffectSource + KB * srcPt->temp));
			//the sourceocc cancels being multiplied by sourceocc, then sourceNum/sourceAvail = 1.0
		}
		else
		{
			dRST_S_dcs = vSource;
			dRST_S_dVel = sourceOcc * muS * distancei * (dEfs_s - dEft_s);
			dRTS_S_dVel = -sourceOcc * muT * distancei * (dEfs_s - dEft_s) * expReverse;
			dRTS_S_dfermi = sourceOcc * vTarget * beta * expReverse * (dEft_s - dEfs_s);
		}
	}
	else
	{
		dEft_s = SourceAffectTarget;
		dEfs_s = SourceAffectSource - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		if(/*constSVelocity ||*/ MY_IS_FINITE(dEfs_s) == false)
		{
			//if dEfs_s is infinite, then the velocity is "infinite", but it's capped
			dRST_S_dcs = vSource;
			dRST_S_dVel = 0.0;
			dRTS_S_dVel = 0.0;
			dRTS_S_dfermi = vTarget * beta * expReverse * ((-SourceAffectSource + KB * srcPt->temp) - dEft_s);
		}
		else
		{
			dRST_S_dcs = vSource;
			dRST_S_dVel = sourceOcc * muS * distancei * (dEft_s - dEfs_s);
			dRTS_S_dVel = -sourceOcc * muT * distancei * (dEft_s - dEfs_s) * expReverse;
			dRTS_S_dfermi = sourceOcc * vTarget * beta * expReverse * (dEfs_s - dEft_s);
		}
	}

	dRST_S = dRST_S_factor * (dRST_S_dcs + dRST_S_dVel);	//deriv rate out wrt cs
	dRTS_S = dRTS_S_factor * (dRTS_S_dcs + dRTS_S_dfermi + dRST_S_dVel);	//deriv rate in wrt cs

	//now to do it all over again for the target!
	dRTS_T_dat = -scatterPartT * vTarget * expReverse;


	dRST_T_dat = -vSource * scatterPartT;
	dRST_T_dpart = vSource * targetAvail * scatterPartT * scatterPartT * trgE->boltzeUse;
	dRTS_T_dpart = -targetAvail * scatterPartT * scatterPartT * trgE->boltzeUse * vTarget * expReverse;

	if(trgE->carriertype == ELECTRON)
	{
		dEfs_t = -TargetAffectSource;
		dEft_t = -TargetAffectSource + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);

		

		if(/*constTVelocity ||*/ MY_IS_FINITE(dEft_t) == false)
		{
			dRST_T_dVel = 0.0;	//the velocity is then constant, so the derivative of the velocity  is zero
			dRTS_T_dVel = 0.0;
			dRTS_T_fermi = vTarget * scatterPartT * beta * expReverse * (-TargetAffectSource * targetAvail + KB * trgPt->temp - dEfs_t * targetAvail);
		}
		else
		{
			dRST_T_dVel = targetAvail * scatterPartT * muS * distancei * (dEfs_t - dEft_t);
			
			
			dRTS_T_dVel = targetAvail * scatterPartT * muT * distancei * expReverse * (dEfs_t - dEft_t);
			dRTS_T_fermi = targetAvail * scatterPartT * vTarget * beta * expReverse * (dEft_t - dEfs_t);	//reversed fermi derivs
			
		}
	}
	else
	{
		dEfs_t = TargetAffectSource;
		dEft_t = TargetAffectSource - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
		if(/*constTVelocity ||*/ MY_IS_FINITE(dEft_t) == false)
		{
			dRST_T_dVel = 0.0;	//the velocity is then constant, so the derivative of the velocity  is zero
			dRTS_T_dVel = 0.0;
			dRTS_T_fermi = vTarget * scatterPartT * beta * expReverse * (dEfs_t * targetAvail - (TargetAffectSource * targetAvail - KB * trgPt->temp));

		}
		else
		{
			dRST_T_dVel = targetAvail * scatterPartT * muS * distancei * (dEft_t - dEfs_t);
			
			dRTS_T_dVel = targetAvail * scatterPartT * muT * distancei * expReverse * (dEft_t - dEfs_t);
			dRTS_T_fermi = targetAvail * scatterPartT * vTarget * beta * expReverse * (dEfs_t - dEft_t);	//reversed fermi derivs
		}
	}

	dRST_T = dRST_T_factor * (dRST_T_dat + dRST_T_dpart + dRST_T_dVel);
	dRTS_T = dRTS_T_factor * (dRTS_T_dat + dRTS_T_dpart + dRTS_T_dVel + dRTS_T_fermi);



	if(srcE == srcESent)
	{
		double dR_S = dRTS_S - dRST_S;	//derivative of rate in minus deriv. of rate out wrt source
		//everything is straightforward
		ELevelRate rateData(srcESent, trgESent, RATE_DRIFTDIFFUSE, srcESent->carriertype, srcESent->carriertype, trgESent->carriertype,DoubleSubtract(RIn,ROut),ROut,RIn,ROut,-1.0,0.0,0.0,dR_S);
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		ret = srcESent->AddELevelRate(rateData, false);	//want rate out, which is just rate
	}
	else
	{
		//the Rate in(TS) is actually the rate out
		//the source and target are swapped. So the derivative wrt source is actually wrt target
		double dR_S = dRST_T - dRTS_T;	//this should be the derivative of the rate into the true source(target)
		//wrt to the true source - the rate out of the true source
		//reverse all ROuts and Rin's below
		ELevelRate rateData(srcESent, trgESent, RATE_DRIFTDIFFUSE, srcESent->carriertype, srcESent->carriertype, trgESent->carriertype,DoubleSubtract(ROut,RIn),RIn,ROut,RIn,-1.0,0.0,0.0,dR_S);
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		ret = srcESent->AddELevelRate(rateData, false);	//want rate out, which is just rate
		
	}
	
	return(ret);

}

double CalculateThermalEmissionRateFast(ELevel* srcESent, ELevel* trgESent, int whichSourceSrc, int whichSourceTrg)
{
	//thermal emission, oi vay.
	ELevel* srcE;
	ELevel* trgE;	
	//first, we ALWAYS want to treat the source as the state that is overcoming the barrier.
	//this is an absolute hack. We know how to calculate the current gonig over the barrier
	//well. We know at equilibrium that the rate coming back is equal.
	//the actual rate going back over is pretty much unknown. It is just "large"
	//any physically based treatments absolutely violate detailed balance (if the fermi levels are equal, the net rate better be zero!)
	//There is a lot of physical play going on between differences in density of states, and the band
	//diagrams already stop being ideal in the presence of large electric fields. At an interface, it
	//must go bonkers. Therefore, all the standard constants we use probably are fluctuating wildly
	//to make it actually work. Net result, we have no idea what the hell is going on.

	//therefore, we know the rate in one direction, so make the rate seem relatively physical in
	//the return direction with the generally same theory as drift diffusion with some slight changes
	//1) ignore states that are not energetic enough to surpass barrier, and clip those that are partial
	//2) make sure the rateOut/RateIn are identical when fermi levels are equal
	//3)tag on an exponential factor for the difference in fermi levels when doing the rate going over the barrier.
	//part 3 imo is absolute bull shit, as is using the source occupancy and target availability to calculate
	//transferring carriers from the target to the source, but as mentioned earlier, no idea what's going on,
	//and this is an approximation that gives a rate (who knows how right/wrong it is!) that other people have
	//accepted as worthy.
	
	double barrier;
	double difEf;
	//starting convention is we want a positive barrier going from source to target
	if(srcESent->carriertype == ELECTRON)
	{
		//just have to subtract electron affinities
		barrier = srcESent->getPoint().MatProps->Phi - trgESent->getPoint().MatProps->Phi;
	}
	else
	{
		barrier = (trgESent->getPoint().MatProps->Phi + trgESent->getPoint().eg) - (srcESent->getPoint().MatProps->Phi + srcESent->getPoint().eg);
	}

	//now if the barrier is negative, we would start from the wrong place in doing the calculation
	if(barrier >= 0.0)
	{
		srcE = srcESent;
		trgE = trgESent;
		difEf = CalculateDifEf(srcE,trgE, whichSourceSrc, whichSourceTrg);	
	}
	else
	{
		srcE = trgESent;
		trgE = srcESent;
		barrier = -barrier;	//make sure the barrier is positive
		difEf = CalculateDifEf(srcE,trgE, whichSourceTrg, whichSourceSrc);	
	}

	if(difEf == 0.0)
		return(0.0);	//it all just cancels, who the hell cares about doing tons of additional math

	if(srcE->max <= barrier)	//this state can't get above the barrier!
		return(0.0);


	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();

	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);

	//we pretty much only care what's going on in the source point...
	double widthi = srcPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;

	double muS, muT, scatterPartS, scatterPartT;
	double velocity = 0.5*(srcE->velocity + trgE->velocity);	//the maximum velocity of this rate between these two energy levels
	double vTarget, vSource;

	

	double beta = (srcPt->temp>0.0) ? KBI / srcPt->temp : 1.0e30;	//huge if infinite
	double CarrierReduction = (srcE->min < barrier) ? exp(beta * (srcE->min-barrier)) : 1.0;

	
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
	}

	//difEf returns positive for source rate out...
	vTarget = 0.5*(velocity - muT * distancei * difEf);
	vSource = 0.5*(velocity + muS * distancei * difEf);

	if(vTarget > velocity)
		vTarget = velocity;
	else if(vTarget < 0.0)
		vTarget = 0.0;

	if(vSource > velocity)
		vSource= velocity;
	else if(vSource < 0.0)
		vSource = 0.0;
	
	

	double ROut = sourceOcc * widthi * targetAvail * scatterPartT * trgE->boltzeUse * CarrierReduction;
	double RIn = ROut * vTarget * exp(beta * -difEf);
	ROut = ROut * vSource;

	//if the source was in fact the source, the rates match. If it were flipped the rate out is actually the rate in and vice versa
	return((srcE == srcESent) ? DoubleSubtract(RIn, ROut) : DoubleSubtract(ROut, RIn));
}

int CalculateThermalEmissionDerivFast(ELevel* srcESent, ELevel* trgESent, SS_CurrentDerivFactors& derivFactor, double& derivSource, double& derivTarget, int whichSourceSrc, int whichSourceTrg)
{
	derivSource = derivTarget = 0.0;
	derivFactor.factor = 0.0;
	derivFactor.factor_ci_srcband = derivFactor.factor_ci_trgband = 0.0;
	ELevel* srcE;
	ELevel* trgE;	
	//first, we ALWAYS want to treat the source as the state that is overcoming the barrier.
	//this is an absolute hack. We know how to calculate the current gonig over the barrier
	//well. We know at equilibrium that the rate coming back is equal.
	//the actual rate going back over is pretty much unknown. It is just "large"
	//any physically based treatments absolutely violate detailed balance (if the fermi levels are equal, the net rate better be zero!)
	//There is a lot of physical play going on between differences in density of states, and the band
	//diagrams already stop being ideal in the presence of large electric fields. At an interface, it
	//must go bonkers. Therefore, all the standard constants we use probably are fluctuating wildly
	//to make it actually work. Net result, we have no idea what the hell is going on.

	//therefore, we know the rate in one direction, so make the rate seem relatively physical in
	//the return direction with the generally same theory as drift diffusion with some slight changes
	//1) ignore states that are not energetic enough to surpass barrier, and clip those that are partial
	//2) make sure the rateOut/RateIn are identical when fermi levels are equal
	//3)tag on an exponential factor for the difference in fermi levels when doing the rate going over the barrier.
	//part 3 imo is absolute bull shit, as is using the source occupancy and target availability to calculate
	//transferring carriers from the target to the source, but as mentioned earlier, no idea what's going on,
	//and this is an approximation that gives a rate (who knows how right/wrong it is!) that other people have
	//accepted as worthy.
	
	double barrier;
	double difEf;
	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	//starting convention is we want a positive barrier going from source to target
	if(srcESent->carriertype == ELECTRON)
	{
		//just have to subtract electron affinities
		barrier = srcESent->getPoint().MatProps->Phi - trgESent->getPoint().MatProps->Phi;
	}
	else
	{
		barrier = (trgESent->getPoint().MatProps->Phi + trgESent->getPoint().eg) - (srcESent->getPoint().MatProps->Phi + srcESent->getPoint().eg);
	}

	//now if the barrier is negative, we would start from the wrong place in doing the calculation
	if(barrier >= 0.0)
	{
		srcE = srcESent;
		trgE = trgESent;
		difEf = CalculateDifEf(srcE,trgE, whichSourceSrc, whichSourceTrg);
		srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceAvail);
		trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetAvail);
	}
	else
	{
		srcE = trgESent;
		trgE = srcESent;
		barrier = -barrier;	//make sure the barrier is positive
		difEf = CalculateDifEf(srcE,trgE, whichSourceTrg, whichSourceSrc);
		srcE->GetOccupancyOcc(whichSourceTrg, sourceOcc, sourceAvail);
		trgE->GetOccupancyOcc(whichSourceSrc, targetOcc, targetAvail);
	}


	if(srcE->max <= barrier)	//this state can't get above the barrier!
		return(0);	//there is no rate, so any derivatives are useless. just get out!


	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();

	
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	
	

	//we pretty much only care what's going on in the source point...
	double widthi = srcPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;

	double muS, muT, scatterPartS, scatterPartT;
	double velocity = 0.5*(srcE->velocity + trgE->velocity);	//the maximum velocity of this rate between these two energy levels
	double vTarget, vSource;

	//difEf returns positive for source rate out...


	double beta = (srcPt->temp>0.0) ? KBI / srcPt->temp : 1.0e30;	//huge if infinite
	double CarrierReduction = (srcE->min < barrier) ? exp(beta * (srcE->min-barrier)) : 1.0;

	
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
	}

	vTarget = 0.5*(velocity - muT * distancei * difEf);
	vSource = 0.5*(velocity + muS * distancei * difEf);
	bool constTVelocity = false;	//these are constant, so the derivative for the velocity term is zero
	bool constSVelocity = false;
	
	if(vTarget > velocity)
	{
		constTVelocity = true;
		vTarget = velocity;
	}
	else if(vTarget < 0.0)
	{
		vTarget = 0.0;
		constTVelocity = true;
	}

	if(vSource > velocity)
	{
		constSVelocity = true;
		vSource= velocity;
	}
	else if(vSource < 0.0)
	{
		constSVelocity = true;
		vSource = 0.0;
	}

	double ROut = sourceOcc * widthi * targetAvail * scatterPartT * trgE->boltzeUse * CarrierReduction;
	double RIn = ROut * vTarget * exp(beta * -difEf);
	ROut = ROut * vSource;

	//now for the derivatives...
	//we are still going to assume that source is still the carriers going over the barrier

	//only need dRout/dcs and dRin/dcs

	double dRST_S, dRTS_S, dRST_T, dRTS_T;	//the actual derivatives. Need all four because we actually don't know which is source and which is target
	double dRST_S_factor = targetAvail * trgE->boltzeUse * scatterPartT * widthi * CarrierReduction;
	double dRTS_S_factor = dRST_S_factor;	//the two linear coefficients of the terms
	double dRST_T_factor = sourceOcc * widthi * trgE->boltzeUse * CarrierReduction;
	double dRTS_T_factor = dRST_T_factor;

	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;

	FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
	FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
	FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
	FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);

	double dEft_s, dEfs_s, dEft_t, dEfs_t;	//derivatives of fermi levels wrt to source/target occ

	double dRST_S_dcs, dRST_S_dVel;	//derivative terms wrt source for rate out
	double dRTS_S_dcs, dRTS_S_dVel, dRTS_S_dfermi;	//derivative terms wrt source for rate in

	double dRTS_T_dat, dRTS_T_dVel, dRTS_T_dpart, dRTS_T_fermi;	//derivative terms wrt target for rate in
	double dRST_T_dat, dRST_T_dVel, dRST_T_dpart;	//derivative terms wrt target for rate out


	double expReverse = exp(beta*-difEf);	//note, difEf = Efs-Eft for electrons, and Eft-Efs for holes (though we still want opposite)
	
	dRTS_S_dcs = vTarget * expReverse;
	

	if(srcE->carriertype == ELECTRON)
	{
		dEft_s = -SourceAffectTarget;
		dEfs_s = -SourceAffectSource + KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		if(/*constSVelocity ||*/ MY_IS_FINITE(dEfs_s) == false)
		{
			//if dEfs_s is infinite, then the velocity is "infinite", but it's capped
			dRST_S_dcs = vSource;
			dRST_S_dVel = 0.0;
			dRTS_S_dVel = 0.0;
			dRTS_S_dfermi = vTarget * beta * expReverse * (dEft_s - (-SourceAffectSource + KB * srcPt->temp));
			//the sourceocc cancels being multiplied by sourceocc, then sourceNum/sourceAvail = 1.0
		}
		else
		{
			dRST_S_dcs = vSource;
			dRST_S_dVel = sourceOcc * muS * distancei * (dEfs_s - dEft_s);
			dRTS_S_dVel = -sourceOcc * muT * distancei * (dEfs_s - dEft_s) * expReverse;
			dRTS_S_dfermi = sourceOcc * vTarget * beta * expReverse * (dEft_s - dEfs_s);
			derivFactor.factor -= 0.5 * muS * distancei;
			//if the band can't change enough to alter the velocity, then the rate won't change, so it should only be added here
		}
		
	}
	else
	{
		dEft_s = SourceAffectTarget;
		dEfs_s = SourceAffectSource - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		if(/*constSVelocity ||*/ MY_IS_FINITE(dEfs_s) == false)
		{
			//if dEfs_s is infinite, then the velocity is "infinite", but it's capped
			dRST_S_dcs = vSource;
			dRST_S_dVel = 0.0;
			dRTS_S_dVel = 0.0;
			dRTS_S_dfermi = vTarget * beta * expReverse * ((-SourceAffectSource + KB * srcPt->temp) - dEft_s);
		}
		else
		{
			dRST_S_dcs = vSource;
			dRST_S_dVel = sourceOcc * muS * distancei * (dEft_s - dEfs_s);
			dRTS_S_dVel = -sourceOcc * muT * distancei * (dEft_s - dEfs_s) * expReverse;
			dRTS_S_dfermi = sourceOcc * vTarget * beta * expReverse * (dEfs_s - dEft_s);
			derivFactor.factor += 0.5 * muS * distancei;
		}
		
	}

	dRST_S = dRST_S_factor * (dRST_S_dcs + dRST_S_dVel);	//deriv rate out wrt cs
	dRTS_S = dRTS_S_factor * (dRTS_S_dcs + dRTS_S_dfermi + dRST_S_dVel);	//deriv rate in wrt cs

	//now to do it all over again for the target!
	dRTS_T_dat = -scatterPartT * vTarget * expReverse;


	dRST_T_dat = -vSource * scatterPartT;
	dRST_T_dpart = vSource * targetAvail * scatterPartT * scatterPartT * trgE->boltzeUse;
	dRTS_T_dpart = -targetAvail * scatterPartT * scatterPartT * trgE->boltzeUse * vTarget * expReverse;

	if(trgE->carriertype == ELECTRON)
	{
		dEfs_t = -TargetAffectSource;
		dEft_t = -TargetAffectSource + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);

		

		if(/*constTVelocity ||*/ MY_IS_FINITE(dEft_t) == false)
		{
			dRST_T_dVel = 0.0;	//the velocity is then constant, so the derivative of the velocity  is zero
			dRTS_T_dVel = 0.0;
			dRTS_T_fermi = vTarget * scatterPartT * beta * expReverse * (-TargetAffectSource * targetAvail + KB * trgPt->temp - dEfs_t * targetAvail);
		}
		else
		{
			dRST_T_dVel = targetAvail * scatterPartT * muS * distancei * (dEfs_t - dEft_t);
			
			
			dRTS_T_dVel = targetAvail * scatterPartT * muT * distancei * expReverse * (dEfs_t - dEft_t);
			dRTS_T_fermi = targetAvail * scatterPartT * vTarget * beta * expReverse * (dEft_t - dEfs_t);	//reversed fermi derivs
			
			derivFactor.factor -= 0.5 * muT * distancei * expReverse;
			derivFactor.factor -= vTarget * beta * expReverse;	
		}
		
	}
	else
	{
		dEfs_t = TargetAffectSource;
		dEft_t = TargetAffectSource - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
		if(/*constTVelocity ||*/ MY_IS_FINITE(dEft_t) == false)
		{
			dRST_T_dVel = 0.0;	//the velocity is then constant, so the derivative of the velocity  is zero
			dRTS_T_dVel = 0.0;
			dRTS_T_fermi = vTarget * scatterPartT * beta * expReverse * (dEfs_t * targetAvail - (TargetAffectSource * targetAvail - KB * trgPt->temp));

		}
		else
		{
			dRST_T_dVel = targetAvail * scatterPartT * muS * distancei * (dEft_t - dEfs_t);
			
			dRTS_T_dVel = targetAvail * scatterPartT * muT * distancei * expReverse * (dEft_t - dEfs_t);
			dRTS_T_fermi = targetAvail * scatterPartT * vTarget * beta * expReverse * (dEfs_t - dEft_t);	//reversed fermi derivs

			derivFactor.factor += 0.5 * muT * distancei * expReverse;
			derivFactor.factor += vTarget * beta * expReverse;
		}
		
	}

	dRST_T = dRST_T_factor * (dRST_T_dat + dRST_T_dpart + dRST_T_dVel);
	dRTS_T = dRTS_T_factor * (dRTS_T_dat + dRTS_T_dpart + dRTS_T_dVel + dRTS_T_fermi);

	derivFactor.factor = derivFactor.factor * sourceOcc * widthi * targetAvail * trgE->boltzeUse * scatterPartT * CarrierReduction;


	if(srcE == srcESent)
	{
		derivSource = dRTS_S - dRST_S;	//derivative of rate in minus deriv. of rate out wrt source
		derivTarget = dRTS_T - dRST_T;
		//everything is straightforward
	}
	else
	{
		//the source and target are swapped. So the derivative wrt source is actually wrt target
		//likewise, the rate in is now ST and the rate out is TS
		derivSource = dRST_T - dRTS_T;
		derivTarget = dRST_S - dRTS_S;
	}
	
	return(0);
}


std::vector<ELevelRate>::iterator CalculateCurrentRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource, bool forceCalc)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();	//return value (null equivalent default)
	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();

	if(!forceCalc)
	{
		if(srcPt->contactData)
		{
			if(srcPt->contactData->TestRecCurrent(srcPt, trgPt))
				return(ret);
//			if(CalcRateViaContact(srcE, trgE, sim->mdesc->Ldimensions))
//				return(ret);
		}
		if(trgPt->contactData)
		{
			if(trgPt->contactData->TestRecCurrent(srcPt, trgPt))
				return(ret);
		}
	}
//	if(srcPt->MatProps != trgPt->MatProps)	//this is a heterojunction point. Crazy schtuff happens that nobody really knows.
//	{
//		return(CalculateThermalEmissionRate(srcE, trgE, sim, whichSource));
//	}
	//the net result is to basically do what the current rate already does, but just apply a boltzmann factor to reduce the carriers appropriately

	double sourceOcc, targetOcc, sourceAvail, targetAvail;
	double sourceNum = srcE->GetNumstates();
	double targetNum = trgE->GetNumstates();
	srcE->GetOccupancyOcc(whichSource, sourceOcc, sourceAvail);
	trgE->GetOccupancyOcc(whichSource, targetOcc, targetAvail);

//	if(sourceOcc == 0.0 || sourceOcc == sourceNum || targetOcc == 0.0 || targetOcc == targetNum)
//	{
//		return(CalculateExtremeDD(srcE, trgE, sim, whichSource));
//	}
//difEf no longer goes to infinity and there are checks built in now for empty/full issues

	
	double widthiS = srcPt->widthi.x;
	double widthiT = trgPt->widthi.x;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	if(distancei > 0.0)
		distancei = 1.0 / distancei;
	
	double muS, muT, scatterPartS, scatterPartT;
	double difEf = CalculateDifEf(srcE,trgE, whichSource, whichSource);	//if positive, rate out
	double RateOut, RateIn, srcConst, trgConst, NRateIn;
	double vTarget, vSource;
	double barrierSrc, barrierTrg;

	//starting convention is we want a positive barrier going from source to target
	

	//note, difEf returns positive for carriers flowing out of the source regardless of CB or VB
	//we want it to return positive for carriers flowing INTO the source

	//first things first, what is the type...
	if(srcE->carriertype == ELECTRON)
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuN : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuN : 1.0;
		scatterPartS = srcPt->scatterCBConst;
		scatterPartT = trgPt->scatterCBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, ELECTRON);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, ELECTRON);
	}
	else
	{
		muS = (srcPt->MatProps) ? srcPt->MatProps->MuP : 1.0;
		muT = (trgPt->MatProps) ? trgPt->MatProps->MuP : 1.0;
		scatterPartS = srcPt->scatterVBConst;
		scatterPartT = trgPt->scatterVBConst;
		barrierSrc = GetBarrierReduction(srcPt, trgPt, HOLE);
		barrierTrg = GetBarrierReduction(trgPt, srcPt, HOLE);
	}

	srcConst = 0.5 * sourceOcc * widthiS * targetAvail * trgE->boltzeUse * scatterPartT * barrierSrc;
	trgConst = 0.5 * targetOcc * widthiT * sourceAvail * srcE->boltzeUse * scatterPartS * barrierTrg;

	vTarget = - muT * difEf * distancei;	//half go in one direction, but the electric field
	//alters the E(k) distribution. Formulated such that RateOut-RateIn results in the net drift velocity
	// mobility = -v_drift / Electric field. Parallelized to difference in fermi function as by the simpler
	//proven equation J_n = n mu_n dEfn/dx (q, depending if Ef in eV or J)
	bool constTVelocity = false;	//these are constant, so the derivative for the velocity term is zero
	bool constSVelocity = false;
	/*
	if(vTarget > trgE->velocity)	//there's a max velocity - doubtful this will ever be reached too often, but current
	{
		vTarget = trgE->velocity;	//does saturate when the voltage is over loaded
		constTVelocity = true;
	}
	if(vTarget < -trgE->velocity)
	{
		vTarget = -trgE->velocity;
		constTVelocity = true;
	}
	*/
	vSource = muS * difEf * distancei;
	/*
	if(vSource > srcE->velocity)
	{
		vSource = srcE->velocity;
		constSVelocity = true;
	}
	if(vSource < -srcE->velocity)
	{
		vSource = -srcE->velocity;
		constSVelocity = true;
	}
	*/
	RateOut = srcConst * (srcE->velocity + vSource) + trgConst * trgE->velocity;	//last term is to allow satisfying of detailed balance
	RateIn = trgConst * (trgE->velocity + vTarget) + srcConst * srcE->velocity;
	//the src/trgConst have a factor of half. This makes the rate going between the two equal by taking their average
	//as the rate. Not completely physical, but still allows the rate in and rate out concept.
	//rate in may be calculated by subtracting the above two, but it is more accurate to minimize the number of calculations and potential introduced error
	NRateIn = (trgConst * vTarget - srcConst * vSource);	//difEf returns positve carriers flowing out of source
	//which means vSource is drift velocity leaving, and vTarget is drift velocity entering
	
	

		//getting this derivative is a slight pain in the @$$. The transfer of carriers moves the fermi level with respect to the energy used
	//the transfer of carriers move the bands
	//the transfer of carriers changes the availability and number of carriers in the source
	//the transfer of carriers changes the partition function used to split up the current
	//this state may be considered either the source or the target
	//this state will have changes based on whether it is a hole or an electron
	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;

	FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
	FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
	FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
	FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);
//	double poisson = SourceAffectSource + TargetAffectTarget - (SourceAffectTarget + TargetAffectSource);	//Pss + Ptt - Pst - Pts
	//this is the same whether source or target are flipped
	//these calculate the effect on the band diagram as if a hole was added.
	//so if the source receives a hole, the target loses a hole.
	//therefore E_S = SS - TS, and E_T = ST - TT
	//however, this is looking at the difference in Ef, and we have Efs - Eft.
	//therefore, the difference says the energy levels affecting the target (2nd letter)
	//should have the opposite sign E_S - E_T = SS + TT - (ST + TS)
	//reverse the target/source, so swap S<==>T = TT + SS - (TS + ST)
	//for holes, we care about Eft - Efs, to have positive flow out of the source.
	//so hole flowing out of source causes a sign flip, and caring about the opposite energy
	//also causes a sign flip, so it cancels itself out.

	
	//the only thing we're going to worry about here is derivSOnly. All the other stuff
	//is only used in code that never worked (which isn't surprising) because the dependencies weren't all calculated
	//note, difEf for CB holds Eft-Efs and for VB holds Efs-Eft


	double dcs_s, dctFact_s, dcta_s, dctpart_s;	//factors for the first part of the net rate in

	double dEft_s, dEfs_s, dEft_t, dEfs_t, dEf_s, dEf_t;
	double dct_t, dcsFact_t, dcsa_t, dcspart_t;
	double overallFact = 0.5 * distancei;

	dcs_s = muS * widthiS * (targetAvail * trgE->boltzeUse * scatterPartT) * barrierSrc;
	dctFact_s = targetOcc * muT * widthiT * srcE->boltzeUse * barrierSrc;
	dcta_s = -scatterPartS;
	dctpart_s = (sourceAvail * srcE->boltzeUse * scatterPartS) * scatterPartS;

	dct_t = muT * widthiT * sourceAvail * srcE->boltzeUse * scatterPartS * barrierTrg;
	dcsFact_t = sourceOcc * muS * widthiS * trgE->boltzeUse * barrierTrg;
	dcsa_t = -scatterPartT;
	dcspart_t = (targetAvail * trgE->boltzeUse * scatterPartT) * scatterPartT;

	if(srcE->carriertype == ELECTRON)
	{
		//dealing with the conduction band. difEf is Efs-Eft
		dEfs_s = -SourceAffectSource + KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		dEft_s = -SourceAffectTarget;
		if(constSVelocity == true || MY_IS_FINITE(dEfs_s)==false)	//sourceocc/avail = 0 or close enough, then dEfs_S should be infinite, which would make the velocity constant
		{
			dEf_s = -KB * srcPt->temp;
			constSVelocity = true;	//just make sure the flag goes up that this velocity should actually be constant so we know the factor ought to be zero.
		}
		else
		{
			dEf_s = dEft_s - dEfs_s;
		}
	}
	else
	{
		dEfs_s = SourceAffectSource - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail);
		dEft_s = SourceAffectTarget;
		if(constSVelocity == true || MY_IS_FINITE(dEfs_s)==false)	//then dEfs_S should be infinite, which would make the velocity constant
		{
			dEf_s = -KB * srcPt->temp;
			constSVelocity = true;
		}
		else
		{
			dEf_s = dEfs_s - dEft_s;	//actually want source to be the upper part for determining rate flow
		}
	}

	if(trgE->carriertype == ELECTRON)
	{
		dEfs_t = -TargetAffectSource;
		dEft_t = -TargetAffectTarget + KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
		if(constTVelocity == true || MY_IS_FINITE(dEft_t))
		{
			dEf_t = KB * trgPt->temp;
			constTVelocity = true;	//just make sure if it somehow slipped past
		}
		else
		{
			dEf_t = dEft_t - dEfs_t;
		}
	}
	else
	{
		dEfs_t = TargetAffectSource;
		dEft_t = TargetAffectTarget - KB * trgPt->temp * targetNum / (targetOcc * targetAvail);
		if(constTVelocity == true || MY_IS_FINITE(dEft_t))
		{
			dEf_t = KB * trgPt->temp;
			constTVelocity = true;	//just make sure if it somehow slipped past
		}
		else
		{
			dEf_t = dEfs_t - dEft_t;
		}
	}

	double dcs = constSVelocity ? 
		overallFact * ((dcs_s + dctFact_s * (dcta_s + dctpart_s)) * (-difEf) + (dcs_s + muT * widthiT * (srcE->boltzeUse * scatterPartS) * targetOcc * barrierTrg) * dEf_s)	//source vail potentially zero
		:  overallFact * ((dcs_s + dctFact_s * (dcta_s + dctpart_s)) * (-difEf) + (dcs_s * sourceOcc + dct_t * targetOcc) * dEf_s);

	double dct = constTVelocity ? 
		overallFact * ((dct_t + dcsFact_t * (dcsa_t + dcspart_t)) * (-difEf) + (muS * widthiS * (trgE->boltzeUse * scatterPartT) * sourceOcc * barrierSrc + dct_t) * dEf_t)	//targetAvail potentially zero 
		: overallFact * ((dct_t + dcsFact_t * (dcsa_t + dcspart_t)) * (-difEf) + (dcs_s * sourceOcc + dct_t * targetOcc) * dEf_t);
	

	ELevelRate rateData(srcE, trgE, RATE_DRIFTDIFFUSE, srcE->carriertype, srcE->carriertype, trgE->carriertype,NRateIn,RateOut,RateIn,RateOut,-1.0,0.0,0.0,dcs);
	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;

	ret = srcE->AddELevelRate(rateData, false);	//want rate out, which is just rate

	return(ret);
	/*

	
	//select the data... if it's going from source->target, use the source data to calculate the rate
	//but if it's going from target->source, use the target data to calculate the rate
	if(srcE->carriertype == ELECTRON)
	{
		
		//poisson = poisson --- poisson is in terms of a hole entering the source, which is the same as an electron
		//leaving the source. That will be the case here whether the target/source are flipped or not
		difEf = CalculateDifEf(srcE, trgE, ELECTRON, whichSource, whichSource);//returns positive such that carriers flow out of source
		if(difEf > 0.0)	//that will not screw up if temperature=0 or states are completely empty/full
		{
			percentUse = trgE->boltzeUse * trgPt->scatterCBConst;
			srcPercent = srcE->boltzeUse * srcPt->scatterCBConst;
			mu = srcPt->MatProps->MuN;
			widthi = srcPt->widthi.x;	//assume in x for now
			sourceOcc = srcE->GetBeginOccStates();
			sourceNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				sourceOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				sourceOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				sourceOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				sourceOcc = srcE->minOcc;

			targetOcc = trgE->GetBeginOccStates();
			targetNum = trgE->GetNumstates();
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(srcE, trgPt, ELECTRON);
			SderivBandEdge = difBandEdge;


			derivSOnly = difEf * percentUse
				+ sourceOcc * percentUse * (SourceAffectSource - SourceAffectTarget - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail));
			//SAS <= 0, add an electron, the source goes up. This increases Ef_s. This presumably is larger than SourceAffectTarget
		}
		else if(difEf < 0.0)	//then the target is sending carriers into the source... describing a rate in
		{
			percentUse = srcE->boltzeUse * srcPt->scatterCBConst;
			srcPercent = trgE->boltzeUse * trgPt->scatterCBConst;
			mu = trgPt->MatProps->MuN;
			widthi = trgPt->widthi.x;
			sourceOcc = trgE->GetBeginOccStates();
			sourceNum = trgE->GetNumstates();
			targetOcc = srcE->GetBeginOccStates();
			targetNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				targetOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				targetOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				targetOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				targetOcc = srcE->minOcc;
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(trgE, srcPt, ELECTRON);	//note the target is acting as the source
			SderivBandEdge = GetBarrierReduction(srcE, trgPt, ELECTRON);	//note the target is acting as the source

			derivSOnly = difEf * srcPercent	//note all the occ, num, and avail are flipped, in addition to describing a rate into the source instead of the rate out of the source
				+ targetOcc * srcPercent * (SourceAffectSource - SourceAffectTarget - KB * srcPt->temp * targetNum / (targetOcc * targetAvail));
		}
	}
	else //the carrier type is a hole...
	{
		//poisson = -poisson;	//these coefficients are in terms of a hole entering the source
		//here, the source is supplying/losing holes, so the coefficient is reversed
		//however, the difference in Ef is the opposite for holes, so the sign actually
		//gets flipped twice
		difEf = CalculateDifEf(srcE, trgE, HOLE, whichSource, whichSource);	//these should be set to default values
		if(difEf > 0.0)	//that will not screw up if temperature=0 or states are completely empty/full
		{
			percentUse = trgE->boltzeUse * trgPt->scatterVBConst;
			srcPercent = srcE->boltzeUse * srcPt->scatterVBConst;
			mu = srcPt->MatProps->MuP;
			widthi = srcPt->widthi.x;	//assume in x for now
			sourceOcc = srcE->GetBeginOccStates();
			sourceNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				sourceOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				sourceOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				sourceOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				sourceOcc = srcE->minOcc;
			targetOcc = trgE->GetBeginOccStates();
			targetNum = trgE->GetNumstates();
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(srcE, trgPt, HOLE);
			SderivBandEdge = difBandEdge;

			derivSOnly = difEf * percentUse
				+ sourceOcc * percentUse * (SourceAffectSource - SourceAffectTarget - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail));

			
		}
		else if(difEf < 0.0)	//then the target is sending carriers into the source...
		{
			percentUse = srcE->boltzeUse * srcPt->scatterVBConst;
			srcPercent = trgE->boltzeUse * trgPt->scatterVBConst;
			mu = trgPt->MatProps->MuP;
			widthi = trgPt->widthi.x;
			sourceOcc = trgE->GetBeginOccStates();
			sourceNum = trgE->GetNumstates();
			targetOcc = srcE->GetBeginOccStates();
			targetNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				targetOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				targetOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				targetOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				targetOcc = srcE->minOcc;
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(trgE, srcPt, HOLE);	//note the target is acting as the source
			SderivBandEdge = GetBarrierReduction(srcE, trgPt, HOLE);

			derivSOnly = difEf * srcPercent	//note num,occ,avail have swapped source/target
				+ targetOcc * srcPercent * (SourceAffectSource - SourceAffectTarget - KB * srcPt->temp * targetNum / (targetOcc * targetAvail));
		}
	}

	if(difEf == 0.0)	//there was no rate, so don't do anything
		return(ret);
	
	//there must be states for the carriers to move into if there is going to be a rate.
	//percentUse is exp(eUse)/sum{unoccupied*exp(eUse)}, so multiplying by the unoccupied corrects this partiition function.
	//the term was not included in percentUse to make showing the detailed balance easier

	//this is already a net rate, but it is a net rate out (source may have swapped with target)
	double NRateOut = sourceOcc * mu * difEf * widthi * distancei * difBandEdge * percentUse * targetAvail;
	if(difEf < 0.0)
		difEf = -difEf;	//need to correct all the derivative terms below

	derivConst = mu * widthi * distancei * difBandEdge;
	derivSOnly = derivSOnly * derivConst * (SderivBandEdge / difBandEdge);
	derivcs = -difEf * targetAvail * percentUse;
	derivavail = -difEf * sourceOcc * percentUse;
	double src = srcPt->temp * sourceNum /(sourceOcc * sourceAvail);	//works with holes or elec
	double trg = trgPt->temp * targetNum / (targetOcc * targetAvail);	//works with holes or elec
	derivEf = sourceOcc * targetAvail * percentUse;	//sourceocc & targetavail here are not part of energy derivative
//	if(srcE->carriertype == ELECTRON)
	derivEf = derivEf * (-KB * (src + trg) + poisson);	//therefore the math would work out the same whether looking at electrons or holes
//	else
//		derivEf = derivEf * (trg + src + poisson);	//poisson stays the same due to flipping for change in carrier type (charge type)
	//trg + src stays the same because while flipping for Efs-Eft to Eft-Efs, there dc_s = -dc_t
	
	derivpartition = sourceOcc * difEf * targetAvail * percentUse * percentUse;	//I have a sneaking suspicion I can make this positive, or I have to prove that is less
	//than the other stuff
	//percent use is a partition without the target avail
	//targetavail * percentUse <= 1.0
	//derivpartition(magnitude) is derivavail * (targetavail * percentUSe) ==> -derivpartition <= derivavail
	// ==> derivavail(+) + derivpartition(-) >= 0, and everything else is > 0

	//only slight hiccup, difEf is calculated such that Efs-Eft > 0 means carriers
	//flow out of source whether holes or electrons. This should be OK for everything
	//but the derivative wrt Ef term. The Efs-Eft is just Eft-Efs.
	
	deriv = derivConst * (derivcs + derivEf + derivavail + derivpartition);	//probably not the fastest version with carrying/condensing terms, but it'll have to work...
	//this is the derivative of the net rate out from the energy level actually supplying the carriers
	double deriv2a = -2.0 * percentUse * deriv;
	double deriv2b = 2.0 * percentUse * ((poisson - KB * (src + trg)) * (targetAvail + sourceOcc) - 1.0);
	double deriv2c = sourceOcc * targetAvail * percentUse * KB * (
		srcPt->temp * sourceNum * (sourceAvail - sourceOcc) / (sourceAvail * sourceAvail * sourceOcc * sourceOcc)
		+ trgPt->temp * targetNum * (targetOcc - targetAvail) / (targetOcc * targetOcc * targetAvail * targetAvail)
		);
	double deriv2 = deriv2a + derivConst * (deriv2b + deriv2c);
	

	//future version:
	//double Rate = sourceOcc * mu * difEf * widthi * distancei * difBandEdge * percentUse * avail * percentScatter
	// + (1.0 - percentScatter) * (% Momentum...0.5) * sourceOcc * velocity * widthi * percentoverlap; for hot carrier effects and maintaining boltzmann
	double deriv2 = 0.0;	//just in case I need this again, I can just delete and uncomment stuff.
	

	if(NRateIn > 0.0)	//only ever care about the source energy level
	{  //				data, sourceE, rate in, rate out, number carriers,
		rateData.RateOut = 0.0;
		rateData.RateIn = NRateIn;
		rateData.netrate = NRateIn;
		rateData.RateType = 0.0;	//want this to be rate out to avoid double counting if rate ever changed to ratetype.
		//RecordRDataLimits(rateData, srcE, 0.0, Rate, sourceOcc, 0.0);
		rateData.desiredTStep = sim->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		ret = srcE->AddELevelRate(rateData, false);	//want rate out, which is just rate

	}
	else if(NRateIn < 0.0)
	{
		rateData.RateOut = -NRateIn;
		rateData.RateIn = 0.0;
		rateData.netrate = NRateIn;
		rateData.RateType = -NRateIn;	//want this to be rate out to avoid double counting if rate ever changed to ratetype.
		//RecordRDataLimits(rateData, srcE, 0.0, Rate, sourceOcc, 0.0); 		note, targetOcc below is actually for srcE.
		rateData.desiredTStep = sim->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		//RecordRDataLimits(rateData, srcE, -Rate, 0.0, sourceOcc, 0.0);
		ret = srcE->AddELevelRate(rateData, false);	//there is no rate out, just a rate in...
	}
	else
	{
		return(ret);
	}

	

	return(ret);
	*/
}

//returns the difference in Ef positive such that carriers flow out of the source
double CalculateDifEf(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg)
{
	//note, Efs-Eft = Es - kT_s ln(N/c-1) - [Et - kT_t ln(N/c-1)]
	//assuming electron above, for holes, do Eft - Efs for positive motion out
	double difSrc = CalcDistanceEf(srcE, whichSourceSrc);	//kT ln(N/c - 1)
	double difTrg = CalcDistanceEf(trgE, whichSourceTrg);	//these will go in the opposite direction for holes
	
	double ret = 0.0;
	if(srcE->carriertype == ELECTRON && trgE->carriertype==ELECTRON)
	{
		double difBand = DoubleSubtract(srcE->getPoint().cb.bandmin, trgE->getPoint().cb.bandmin);
		double difEnergy = DoubleSubtract(srcE->eUse, trgE->eUse);
		double difDist = DoubleSubtract(difTrg, difSrc);
		//above method tries to cancel out any bias from energy states being far from 0.
		ret = DoubleAdd(difBand + difEnergy, difDist);

	}
	else if(srcE->carriertype == HOLE && trgE->carriertype==HOLE)
	{  //this has been verified, though first glance suggests it is incorrect.
		double difBand = DoubleSubtract(trgE->getPoint().vb.bandmin, srcE->getPoint().vb.bandmin);
		double difEnergy = DoubleSubtract(srcE->eUse, trgE->eUse);	//stays same because eUse counts down now
		double difDist = DoubleSubtract(difTrg, difSrc);	//stays same because calculated as if e- but really holes
		//above method tries to cancel out any bias from energy states being far from 0.
		ret = DoubleAdd(difBand + difEnergy, difDist);
	}
	else if(srcE->carriertype == ELECTRON && trgE->carriertype==HOLE)
	{  //this has been verified, though first glance suggests it is incorrect.
		double difBand = DoubleSubtract(srcE->getPoint().cb.bandmin, trgE->getPoint().vb.bandmin);
		double difEnergy = DoubleAdd(srcE->eUse, trgE->eUse);	//stays same because eUse counts down now
		double difDist = DoubleAdd(difTrg, difSrc);	//stays same because calculated as if e- but really holes
		//above method tries to cancel out any bias from energy states being far from 0.
		ret = DoubleSubtract(difBand + difEnergy, difDist);
	}
	else if(srcE->carriertype == HOLE && trgE->carriertype==ELECTRON)
	{  //this has been verified, though first glance suggests it is incorrect.
		double difBand = DoubleSubtract(trgE->getPoint().cb.bandmin, srcE->getPoint().vb.bandmin);
		double difEnergy = DoubleAdd(srcE->eUse, trgE->eUse);	//stays same because eUse counts down now
		double difDist = DoubleAdd(difTrg, difSrc);	//stays same because calculated as if e- but really holes
		//above method tries to cancel out any bias from energy states being far from 0.
		ret = DoubleSubtract(difBand + difEnergy, difDist);
	}
	return(ret);
}

std::vector<ELevelRate>::iterator CalculateExtremeDD(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	double mu = 0.0;	//default to kill rate
	Point* srcPt = &srcE->getPoint();
	Point* trgPt = &trgE->getPoint();
	double sourceOcc; //= srcE->GetBeginOccStates();
	double sourceNum; //= srcE->GetNumstates();
	double targetOcc; //= trgE->GetBeginOccStates();
	double targetNum;// = trgE->GetNumstates();
	double sourceAvail;
	double targetAvail;
	double difEf;
	double widthi = 0.0;
	double distancei = (srcPt->position - trgPt->position).Magnitude();
	double difBandEdge = 1.0;
	if(distancei > 0.0)
		distancei = 1.0 / distancei;
	double percentUse = 0.0;
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();	//default to a "null" return value
	

//	ELevelOpticalRate rateData;
//	ELevelOpticalRateSort rateSort;
	//getting this derivative is a slight pain in the @$$. The transfer of carriers moves the fermi level with respect to the energy used
	//the transfer of carriers move the bands
	//the transfer of carriers changes the availability and number of carriers in the source
	//the transfer of carriers changes the partition function used to split up the current
	//this state may be considered either the source or the target
	//this state will have changes based on whether it is a hole or an electron
	double SourceAffectSource;
	double SourceAffectTarget;
	double TargetAffectSource;
	double TargetAffectTarget;

	FindPoissonCoeff(srcPt, srcPt->adj.self, SourceAffectSource);	//find the coefficients describing how the change
	FindPoissonCoeff(srcPt, trgPt->adj.self, SourceAffectTarget);	//in charge per carrier will affect
	FindPoissonCoeff(trgPt, trgPt->adj.self, TargetAffectTarget);	//the other points
	FindPoissonCoeff(trgPt, srcPt->adj.self, TargetAffectSource);
	double poisson = SourceAffectSource + TargetAffectTarget - (SourceAffectTarget + TargetAffectSource);
	//this is the same whether source or target are flipped
	//these calculate the effect on the band diagram as if a hole was added.
	//so if the source receives a hole, the target loses a hole.
	//therefore E_S = SS - TS, and E_T = ST - TT
	//however, this is looking at the difference in Ef, and we have Efs - Eft.
	//therefore, the difference says the energy levels affecting the target (2nd letter)
	//should have the opposite sign E_S - E_T = SS + TT - (ST + TS)
	//reverse the target/source, so swap S<==>T = TT + SS - (TS + ST)
	//for holes, we care about Eft - Efs, to have positive flow out of the source.
	//so hole flowing out of source causes a sign flip, and caring about the opposite energy
	//also causes a sign flip, so it cancels itself out.

	double derivConst = 0.0;
	double derivcs = 0.0;
	double derivEf = 0.0;	//includes change in the band
	double derivavail = 0.0;
	double derivpartition = 0.0;
	double deriv = 0.0;
	double derivSOnly = 0.0;
	double SderivBandEdge = 1.0;

	if(trgE->carriertype == ELECTRON)
		percentUse = trgE->boltzeUse * trgPt->scatterCBConst;
	else
		percentUse = trgE->boltzeUse * trgPt->scatterVBConst;

	//select the data... if it's going from source->target, use the source data to calculate the rate
	//but if it's going from target->source, use the target data to calculate the rate
	if(srcE->carriertype == ELECTRON)
	{
		
		difEf = DoubleSubtract(srcE->AbsFermiEnergy, trgE->AbsFermiEnergy);	//these should be set to default values
		if(difEf > 0.0)	//that will not screw up if temperature=0 or states are completely empty/full
		{
			mu = srcPt->MatProps->MuN;
			widthi = srcPt->widthi.x;	//assume in x for now
			sourceOcc = srcE->GetBeginOccStates();
			sourceNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				sourceOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				sourceOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				sourceOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				sourceOcc = srcE->minOcc;
			targetOcc = trgE->GetBeginOccStates();
			targetNum = trgE->GetNumstates();
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(srcE, trgPt, ELECTRON);
			SderivBandEdge = difBandEdge;
		
			
		}
		else if(difEf < 0.0)	//then the target is sending carriers into the source...
		{
			mu = trgPt->MatProps->MuN;
			widthi = trgPt->widthi.x;
			sourceOcc = trgE->GetBeginOccStates();
			sourceNum = trgE->GetNumstates();
			targetOcc = srcE->GetBeginOccStates();
			targetNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				targetOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				targetOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				targetOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				targetOcc = srcE->minOcc;
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(trgE, srcPt, ELECTRON);	//note the target is acting as the source
			SderivBandEdge = GetBarrierReduction(srcE, trgPt, ELECTRON);	//note the target is acting as the source
		
		}
	}
	else //the carrier type is a hole...
	{
		difEf = DoubleSubtract(trgE->AbsFermiEnergy, srcE->AbsFermiEnergy);	//these should be set to default values
		if(difEf > 0.0)	//that will not screw up if temperature=0 or states are completely empty/full
		{
			mu = srcPt->MatProps->MuP;
			widthi = srcPt->widthi.x;	//assume in x for now
			sourceOcc = srcE->GetBeginOccStates();
			sourceNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				sourceOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				sourceOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				sourceOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				sourceOcc = srcE->minOcc;
			targetOcc = trgE->GetBeginOccStates();
			targetNum = trgE->GetNumstates();
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(srcE, trgPt, HOLE);
			SderivBandEdge = difBandEdge;
	
		}
		else if(difEf < 0.0)	//then the target is sending carriers into the source...
		{
			mu = trgPt->MatProps->MuP;
			widthi = trgPt->widthi.x;
			sourceOcc = trgE->GetBeginOccStates();
			sourceNum = trgE->GetNumstates();
			targetOcc = srcE->GetBeginOccStates();
			targetNum = srcE->GetNumstates();
			if(whichSource == RATE_OCC_TARGET)
				targetOcc = srcE->targetOcc;
			else if(whichSource == RATE_OCC_END)
				targetOcc = srcE->GetEndOccStates();
			else if(whichSource == RATE_OCC_MAX)
				targetOcc = srcE->maxOcc;
			else if(whichSource == RATE_OCC_MIN)
				targetOcc = srcE->minOcc;
			targetAvail = targetNum - targetOcc;	//include detailed balance in partition function
			sourceAvail = sourceNum - sourceOcc;
			difBandEdge = GetBarrierReduction(trgE, srcPt, HOLE);	//note the target is acting as the source
			SderivBandEdge = GetBarrierReduction(srcE, trgPt, HOLE);

			
		}
	}
	
	

	if(difEf == 0.0)
		return(ret);	//there is no rate because they are both full or both empty

	//there must be states for the carriers to move into if there is going to be a rate.
	double Rate = sourceOcc * mu * difEf * widthi * distancei * difBandEdge * percentUse * targetAvail;
	if(Rate == 0.0)
		return(ret);

	//at this point, sourceOcc = numstates and/or targetAvail = 0.0.
	derivConst = mu * widthi * distancei * difBandEdge;	//OK
	
	derivcs = difEf * targetAvail * percentUse;	//OK

	//source avail = 0 or targetocc = 0.0. Just go with a small fixed value instead
	double src, trg;
	if(sourceAvail == 0.0)
		sourceAvail = sourceNum * 1e-20;
	if(targetOcc == 0.0)
		targetOcc = targetNum * 1e-20;
	if(sourceOcc == 0.0)	//this shouldn't ever happen here, but better safe than divide by zero...
		sourceOcc = sourceNum * 1e-20;
	if(targetOcc == 0.0)	//this shouldn't ever happen here, but better safe than divide by zero...
		targetOcc = targetNum * 1e-20;
	
	src = srcPt->temp * sourceNum /(sourceOcc * sourceAvail);
	trg = trgPt->temp * targetNum / (targetOcc * targetAvail);	//works with holes or elec
	derivEf = KB * sourceOcc * targetAvail * percentUse;	//sourceocc & targetavail here are not part of energy derivative
	if(srcE->carriertype == ELECTRON)
		derivEf = derivEf * (src - trg + poisson);	//therefore the math would work out the same whether looking at electrons or holes
	else
		derivEf = derivEf * (trg - src + poisson);	//poisson stays the same due to flipping for change in carrier type (charge type)
	//and also flips for reversing Efs-Eft to Eft-Efs. The trg and src must flip accordingly

	if(srcE->carriertype == ELECTRON)
	{
		if(difEf > 0.0)
		{
			derivSOnly = difEf * percentUse
				+ sourceOcc * percentUse * (-SourceAffectSource + KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail));
		}
		else if(difEf < 0.0)
		{
			derivSOnly = difEf * percentUse	//note all the occ, num, and avail are flipped
				+ targetOcc * percentUse * (-SourceAffectSource + KB * srcPt->temp * targetNum / (targetOcc * targetAvail));
		}
	}
	else
	{
		if(difEf > 0.0)
		{
			derivSOnly = difEf * percentUse
				+ sourceOcc * percentUse * (SourceAffectSource - KB * srcPt->temp * sourceNum / (sourceOcc * sourceAvail));
		}
		else if(difEf < 0.0)
		{
			derivSOnly = difEf * percentUse	//note num,occ,avail have swapped source/target
				+ targetOcc * percentUse * (SourceAffectSource - KB * srcPt->temp * targetNum / (targetOcc * targetAvail));
		}
	}
	derivSOnly = derivSOnly * derivConst * (SderivBandEdge / difBandEdge);

	derivavail = difEf * sourceOcc * percentUse;
	derivpartition = -sourceOcc * difEf * targetAvail * percentUse * percentUse;

	//only slight hiccup, difEf is calculated such that Efs-Eft > 0 means carriers
	//flow out of source whether holes or electrons. This should be OK for everything
	//but the derivative wrt Ef term. The Efs-Eft is just Eft-Efs.
	
	deriv = derivConst * (derivcs + derivEf + derivavail + derivpartition);	//probably not the fastest version with carrying/condensing terms, but it'll have to work...
	//this is the derivative of the net rate out from the energy level actually supplying the carriers

	double deriv2a = -2.0 * percentUse * deriv;
	double deriv2b = 2.0 * percentUse * ((poisson - KB * (src + trg)) * (targetAvail + sourceOcc) - 1.0);
	double deriv2c = sourceOcc * targetAvail * percentUse * KB * (
		srcPt->temp * sourceNum * (sourceAvail - sourceOcc) / (sourceAvail * sourceAvail * sourceOcc * sourceOcc)
		+ trgPt->temp * targetNum * (targetOcc - targetAvail) / (targetOcc * targetOcc * targetAvail * targetAvail)
		);
	double deriv2 = deriv2a + derivConst * (deriv2b + deriv2c);

	ELevelRate rateData(srcE,trgE,RATE_DRIFTDIFFUSE,srcE->carriertype,srcE->carriertype,trgE->carriertype,0.0,0.0,0.0,0.0,-1.0,deriv,(Rate>0)?deriv2:-deriv2,derivSOnly);
	/*
	rateData.netrate = Rate;
	rateSort.source = srcE;
	rateSort.target = trgE;
	rateData.targetcarrier = trgE->carriertype;
	rateData.sourcecarrier = srcE->carriertype;
	rateSort.carrier = srcE->carriertype;
	rateData.deriv = deriv;
	rateData.deriv2 = (Rate > 0) ? deriv2 : -deriv2;
	rateData.derivsourceonly = derivSOnly;
	*/
	//if rate < 0, the source and target were swapped, so the second derivative takes on
	//a negative sign when applied to the true source.
	
	if(Rate > 0.0)	//only ever care about the source energy level
	{
		
		rateData.RateOut = Rate;
		rateData.RateIn = 0.0;
		rateData.netrate = -Rate;
		rateData.RateType = Rate;	//want this to be rate out to avoid double counting
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		ret = srcE->AddELevelRate(rateData, false);	
		/*
		if(srcE->getPoint().contactData)
		{
			if(srcE->getPoint().contactData->isolated == false)
			{
		//in contacts, already have dRs/dcs calculated. Rs is flipped, but cs the same
		//therefore, this derivative should cancel out the other derivatives.
		//this is the one and only time a derivative can be positive. Increasing carriers does increase the rate in.
		//2nd derivative also needs to be cancelled out
				rateData.deriv = -deriv;
				rateData.deriv2 = -rateData.deriv2;
				rateData.equiltransfer = 0.0;
				rateData.target = NULL;	//needed to find
				rateData.targetcarrier = ELECTRON;	//just default value
				rateData.RateIn = Rate;	//set in case have to do add/insert
				rateData.RateOut = 0.0;	//set in case have to do add/insert
				rateData.netrate = Rate;	//set in case have to do add/insert
				rateData.type = RATE_CONTACTCURRENT;	//needed to find

				//carrier type and source should already be in there.
				//no need to do another updateActiveRate step.
				Tree<double, ELevelOpticalRate>* ptrConCurRate = srcE->Rates.Find(rateData);
				if(ptrConCurRate == NULL)
					srcE->AddELevelRate(0.0, rateData);
				else
				{
					ptrConCurRate->Sort.RateIn += Rate;
					ptrConCurRate->Sort.netrate += Rate;
					srcE->RateSums.AddValue(Rate);	//used for steady state calculation
				}
			}
		}
		*/
//		RecordRDataLimits(rateData, srcE, 0.0, Rate, sourceOcc, 0.0);
	}
	else if(Rate < 0.0)
	{
		rateData.RateOut = 0.0;
		rateData.RateIn = -Rate;	//positive number
		rateData.netrate = -Rate;	//positive number		note, targetOcc below is actually for srcE.
		rateData.RateType = 0.0; //want this to be rate out to avoid double counting if rate ever changed to ratetype.
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		ret = srcE->AddELevelRate(rateData, false);	
//		RecordRDataLimits(rateData, srcE, -Rate, 0.0, sourceOcc, 0.0);
/*
		if(srcE->getPoint().contactData)
		{
			if(srcE->getPoint().contactData->isolated == false)
			{
		//in contacts, already have dRs/dcs calculated. Rs is flipped, but cs the same
		//therefore, this derivative should cancel out the other derivatives.
		//this is the one and only time a derivative can be positive. Increasing carriers does increase the rate in.
		//2nd derivative also needs to be cancelled out
				rateData.deriv = -deriv;
				rateData.deriv2 = -rateData.deriv2;
				srcE->RateSums.AddValue(Rate);	//used for steady state calculation
				rateData.equiltransfer = 0.0;
				rateData.target = NULL;	//needed to find
				rateData.targetcarrier = ELECTRON;	//just default value
				rateData.RateIn = 0.0;	//set in case have to do add/insert
				rateData.RateOut = -Rate;	//set in case have to do add/insert
				rateData.netrate = Rate;	//set in case have to do add/insert
				rateData.type = RATE_CONTACTCURRENT;	//needed to find
				
				//carrier type and source should already be in there.
				//no need to do another updateActiveRate step.
				Tree<double, ELevelOpticalRate>* ptrConCurRate = srcE->Rates.Find(rateData);
				if(ptrConCurRate == NULL)
					srcE->AddELevelRate(0.0, rateData);
				else
				{
					ptrConCurRate->Sort.RateOut -= Rate;	//note Rate is negative.
					ptrConCurRate->Sort.netrate += Rate;
					srcE->RateSums.AddValue(Rate);	//used for steady state calculation
				}
			}
		}
		*/
	}
	else
	{
		return(ret);
	}



	return(ret);	//if ptr ends up being null, this will return a non-Null value.
}

//doesn't include the energy that it is already up.
double GetBarrierReduction(ELevel* srcE, Point* trgPt, bool carrierType)
{
	double ret = 1.0;
	if(srcE == NULL)
		return(1.0);
	if(trgPt == NULL)
		return(1.0);

	if(srcE->getPoint().MatProps != trgPt->MatProps)
	{
		double dif;
		double beta = 0.0;
		if(srcE->getPoint().temp > 0.0)
			beta = KBI / srcE->getPoint().temp;

		if(carrierType == ELECTRON)
			dif = DoubleSubtract(trgPt->MatProps->Phi + srcE->eUse, srcE->getPoint().MatProps->Phi);
		else
			dif = DoubleSubtract(srcE->getPoint().MatProps->Phi + srcE->getPoint().eg + srcE->eUse, trgPt->MatProps->Phi + trgPt->eg);
		
		if(dif < 0.0)
			ret = exp(beta * dif);
	}


	return(ret);
}

//from band minimum to band minimum
double GetBarrierReduction(Point* srcPt, Point* trgPt, bool carrierType)
{
	double ret = 1.0;

	if(srcPt->MatProps != trgPt->MatProps)
	{
		double dif;
		double beta = 0.0;
		if(srcPt->temp > 0.0)
			beta = KBI / srcPt->temp;

		if(carrierType == ELECTRON)
			dif = DoubleSubtract(trgPt->MatProps->Phi, srcPt->MatProps->Phi);
		else
			dif = DoubleSubtract(srcPt->MatProps->Phi + srcPt->eg, trgPt->MatProps->Phi + trgPt->eg);
		
		if(dif < 0.0)
			ret = exp(beta * dif);
	}


	return(ret);
}

//fails for holes... sigh.
double CalcCurrentEQTransfer(ELevel* srcE, ELevel* trgE)
{
	double temp = (srcE->getPoint().temp + trgE->getPoint().temp)*0.5;
	double boltz;
	if(srcE->carriertype == ELECTRON)
	{
		boltz = DoubleAdd(DoubleSubtract(srcE->eUse, trgE->eUse),DoubleSubtract(srcE->getPoint().cb.bandmin, trgE->getPoint().cb.bandmin));
	}
	else
	{
		boltz = DoubleSubtract(DoubleSubtract(srcE->eUse, trgE->eUse),DoubleSubtract(srcE->getPoint().vb.bandmin, trgE->getPoint().vb.bandmin));
		//(VB_Source - eUse_Source) - (VB_Target - eUse_Target)
		//(VB_Source - VB_Target) - (eUse_Source - eUse_Target)	.. keep comparable numbers together
		//except because it's holes, the math calls for E + kT ln(), which means flip the sign of the kT.
		//flipping the sign of the kT is equivalent to flipping the sign of the Es-Et, so do Et-Es.
	}
	double beta = KBI / temp;
	boltz = exp(beta * boltz);
	double sourceOcc = srcE->GetBeginOccStates();
	double sourceNum = srcE->GetNumstates();
	double targetOcc = trgE->GetBeginOccStates();
	double targetNum = trgE->GetNumstates();

	double A = DoubleSubtract(1, boltz);
	double B = targetOcc - sourceNum - sourceOcc;
	B = B * boltz;
	B += sourceNum - sourceOcc + targetOcc;
	double C = targetNum - targetOcc;
	C = C * boltz;
	C += sourceNum - sourceOcc;

	double retAdd;
	double retSubtract;
	double ret = 0.0;
	bool goodAdd = false;
	bool goodSubtract = false;
	if(SolveQuadratic(A, B, C, retAdd, retSubtract) == false)
	{
		return(0.0);
	}

	double newSOccA = sourceOcc - retAdd;
	double newTOccA = targetOcc + retAdd;
	
	if(newSOccA >= 0.0 && newSOccA <= sourceNum && newTOccA >= 0.0 && newTOccA <= targetNum)
	{
		goodAdd = true;
	}

	double newSOccS = sourceOcc - retSubtract;
	double newTOccS = targetOcc + retSubtract;
	
	if(newSOccS >= 0.0 && newSOccS <= sourceNum && newTOccS >= 0.0 && newTOccS <= targetNum)
	{
		goodSubtract = true;
	}

	if(goodAdd && !goodSubtract)
		ret = retAdd;
	else if(!goodAdd && goodSubtract)
		ret = retSubtract;
	else if(goodAdd && goodSubtract)	//now choose the smaller change, assuming it's close to EQ
	{
		if(fabs(retAdd) < fabs(retSubtract))
			ret = retAdd;
		else
			ret = retSubtract;
	}
	//else ret = 0.0
		
	return(ret);
}

//to get to this function, 
double CalcDistanceEf(ELevel* eLev, int whichSource)
{
	double ret = 0.0;
	double occ, avail;	//avail is just here to satisfy the function call
	eLev->GetOccupancyOcc(whichSource, occ, avail);
	
	double num = eLev->GetNumstates();
	if(occ == 0.0)	//just get out. This function should never be called in this instance anyway
		return(1000000.0);	//arbitrary huge
	if(occ==num)
		return(-1000000.0);	//arbitrary huge negative

	double lnterm = avail / occ;
	if(MY_IS_FINITE(lnterm))
		lnterm = log(lnterm);
	else
		lnterm = log(num) - log(occ);	//num-occ is obbiously num if lnterm was infinite. occ is not quite zero but negligible
	ret = KB * eLev->getPoint().temp * lnterm;

	return(ret);
}



int FindPoissonCoeff(Point* pt, long int targetindex, double& ret)
{
	ret = 0.0;
	if(targetindex < 0)
		return(0);	//there is no value - this is to an external point

	
//	int found = 0;
	long sizevector = pt->PsiCf.size();

	//hope to get lucky! Presumably, the indices will match with pt or be very close.
	if(targetindex < sizevector)
	{
		long int dif = targetindex - pt->PsiCf[targetindex].pt->adj.self;
		if(dif == 0)	//found it!
		{
			ret = pt->PsiCf[targetindex].data;
			return(1);
		}
		else
		{
			long int check = targetindex + dif;
			if(check >= 0 && check < sizevector)
			{
				if(pt->PsiCf[check].pt->adj.self == targetindex)	//found it!
				{
					ret = pt->PsiCf[check].data;
					return(1);
				}	//end if found it
			}	//end check to make sure in vector subscript
		}	//end not found on first try
	}	//end if starting targetindex is within vector size

	//brute force search - no idea where it could be
	for(long int i=0; i<sizevector; i++)
	{
		if(pt->PsiCf[i].pt->adj.self == targetindex)
		{
			ret = pt->PsiCf[i].data;
			return(1);
			break;
		}
	}

	return(1);	//if it doesn't find it, it should still return a value of 0 as it may b e acontact
}

