#include "scatter.h"

#include "model.h"
#include "RateGeneric.h"
#include "transient.h"
#include "physics.h"

double CalcScatterFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate)
{
	double carrierSource, carrierTarget, availSource, availTarget, consts;
	if(srcE->carriertype == ELECTRON)
		consts = srcE->getPoint().scatterCBConst;
	else
		consts = srcE->getPoint().scatterVBConst;

	//need to adjust the consts for a new scatter partition
	if(whichSourceSrc != RATE_OCC_BEGIN || whichSourceTrg != RATE_OCC_BEGIN)
	{
		consts = 1.0 / consts;	//unnormalize it
		srcE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierSource, availSource);
		trgE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierTarget, availTarget);
		consts = consts - availTarget * trgE->boltzeUse - availSource * srcE->boltzeUse;
		srcE->GetOccupancyOcc(whichSourceSrc, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSourceTrg, carrierTarget, availTarget);
		consts = consts + availTarget * trgE->boltzeUse + availSource * srcE->boltzeUse;
		consts = 1.0 / consts;
	}
	else
	{
		srcE->GetOccupancyOcc(whichSourceSrc, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSourceTrg, carrierTarget, availTarget);
	}
	

	double tauiT = trgE->taui;	//the rate scattering for this particular energy level, but divided by all the bands because it gets scattered into each
	double tauiS = srcE->taui;
	
	double rateOut;
	double rateIn;
	if (AccuRate == NULL)
	{
		rateOut = carrierSource * availTarget * tauiS * consts * trgE->boltzeUse;
		rateIn = carrierTarget * availSource * tauiT * consts * srcE->boltzeUse;
	}
	else if (AccuRate)
	{
		double nTarget = trgE->GetNumstates();
		double nSource = srcE->GetNumstates();

		bool splitTarget = (availTarget > carrierTarget && availTarget * carrierTarget <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (carrierSource > availSource && availSource * carrierSource <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//this is for calculating rate Out, so is the carrier source really close?
		if (!splitSource && !splitTarget)
		{
			rateOut = carrierSource * availTarget * tauiS * consts * trgE->boltzeUse;
			AccuRate->SubtractValue(rateOut);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = tauiS * consts * trgE->boltzeUse;	//the constants
			//cSource = nSource - availSource ~~~~~ availTarget = nTarget - carrierTarget
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource * nTarget);
			AccuRate->SubtractValue(cnst * availSource * carrierTarget);
			AccuRate->AddValue(cnst * nSource * carrierTarget);
			AccuRate->AddValue(cnst * availSource * nTarget);
			rateOut = cnst * carrierSource * availTarget;
		}
		else if (splitSource)
		{
			double cnst = availTarget * tauiS * consts * trgE->boltzeUse;	//the constants
			//cSource = nSource - availSource
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource);
			AccuRate->AddValue(cnst * availSource);
			rateOut = cnst * carrierSource;
		}
		else// if (splitTarget)
		{
			double cnst = carrierSource * tauiS * consts * trgE->boltzeUse;	//the constants
			//availTarget = nTarget - carrierTarget
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nTarget);
			AccuRate->AddValue(cnst * carrierTarget);
			rateOut = cnst * availTarget;
		}
		
		//now do the rate in!
		splitTarget = (carrierTarget > availTarget && availTarget * carrierTarget <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		splitSource = (availSource > carrierSource && availSource * carrierSource <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		if (!splitSource && !splitTarget)
		{
			rateIn = carrierTarget * availSource * tauiT * consts * srcE->boltzeUse;
			AccuRate->AddValue(rateIn);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = tauiT * consts * srcE->boltzeUse;
			//carrierTarget = nTarget - availTarget ~~~~~ availSource = nSource - carrierSource
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nSource * nTarget);
			AccuRate->AddValue(cnst * availTarget * carrierSource);
			AccuRate->SubtractValue(cnst * nSource * availTarget);
			AccuRate->SubtractValue(cnst * carrierSource * nTarget);
			rateIn = cnst * availSource * carrierTarget;
		}
		else if (splitSource)
		{
			double cnst = carrierTarget * tauiT * consts * srcE->boltzeUse;
			//availSource = nSource - carrierSource
			//this is rate out, so subtract the positive values
			AccuRate->AddValue(cnst * nSource);
			AccuRate->SubtractValue(cnst * carrierSource);
			rateIn = cnst * availSource;
		}
		else// if (splitTarget)
		{
			double cnst = availSource * tauiT * consts * srcE->boltzeUse;
			//carrierTarget = nTarget - availTarget
			//this is rate out, so subtract the positive values
			AccuRate->AddValue(cnst * nTarget);
			AccuRate->SubtractValue(cnst * availTarget);
			rateOut = cnst * carrierTarget;
		}
	}
	return(DoubleSubtract(rateIn, rateOut));

}

//if the source changes in occupancy, how does the target's net rate change.
int CalcScatterDerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget)
{
	double carrierSource, carrierTarget, availSource, availTarget, consts;
	if(srcE->carriertype == ELECTRON)
		consts = srcE->getPoint().scatterCBConst;
	else
		consts = srcE->getPoint().scatterVBConst;

	//need to adjust the consts for a new scatter partition
	if(whichSourceSrc != RATE_OCC_BEGIN || whichSourceTrg != RATE_OCC_BEGIN)
	{
		consts = 1.0 / consts;	//unnormalize it
		srcE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierSource, availSource);
		trgE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierTarget, availTarget);
		consts = consts - availTarget * trgE->boltzeUse - availSource * srcE->boltzeUse;
		srcE->GetOccupancyOcc(whichSourceSrc, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSourceTrg, carrierTarget, availTarget);
		consts = consts + availTarget * trgE->boltzeUse + availSource * srcE->boltzeUse;
		consts = 1.0 / consts;
	}
	else
	{
		srcE->GetOccupancyOcc(whichSourceSrc, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSourceTrg, carrierTarget, availTarget);
	}
	double tauiT = trgE->taui;	//the rate scattering for this particular energy level, but divided by all the bands because it gets scattered into each
	double tauiS = srcE->taui;

	double tmp1, tmp2;
	tmp1 = (availSource * srcE->boltzeUse * consts - 1.0) * consts * tauiT * carrierTarget * srcE->boltzeUse;
	tmp2 = (1.0 + carrierSource * srcE->boltzeUse * consts) * consts * availTarget * tauiS * trgE->boltzeUse;
	derivSource = tmp1 - tmp2;

	tmp1 = (carrierTarget * trgE->boltzeUse * consts + 1.0) * availSource * tauiT * srcE->boltzeUse * consts;
	tmp2 = (availTarget * trgE->boltzeUse * consts - 1.0) * consts * carrierSource * tauiS * trgE->boltzeUse;
	derivTarget = tmp1 - tmp2;
	
	return(0);
}

std::vector<ELevelRate>::iterator CalculateScatterRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//this should be mundanely simple, and both should be in the same type of state (CB or VB)
	double carrierSource, carrierTarget, availSource, availTarget, consts;
	if(srcE->carriertype == ELECTRON)
		consts = srcE->getPoint().scatterCBConst;
	else
		consts = srcE->getPoint().scatterVBConst;

	//need to adjust the consts for a new scatter partition
	if(whichSource != RATE_OCC_BEGIN)
	{
		consts = 1.0 / consts;	//flip the sum over so it can be modified (set by rate_occ_begin)
		srcE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierSource, availSource);
		trgE->GetOccupancyOcc(RATE_OCC_BEGIN, carrierTarget, availTarget);
		consts = consts - availTarget * trgE->boltzeUse - availSource * srcE->boltzeUse;
		srcE->GetOccupancyOcc(whichSource, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSource, carrierTarget, availTarget);
		consts = consts + availTarget * trgE->boltzeUse + availSource * srcE->boltzeUse;
		consts = 1.0 / consts;
	}
	else
	{
		srcE->GetOccupancyOcc(whichSource, carrierSource, availSource);
		trgE->GetOccupancyOcc(whichSource, carrierTarget, availTarget);
	}
	
	double tauiT = trgE->taui;	//the rate scattering for this particular energy level, but divided by all the bands because it gets scattered into each
	double tauiS = srcE->taui;
	

	double rateOut = carrierSource * availTarget * tauiS * consts * trgE->boltzeUse;
	double rateIn = carrierTarget * availSource * tauiT * consts * srcE->boltzeUse;

	
	double derivSource = tauiS * consts *  trgE->boltzeUse * (availTarget + carrierSource + availTarget * carrierSource * consts * (srcE->boltzeUse - trgE->boltzeUse));
	double derivTarget = tauiT * consts * srcE->boltzeUse * (availSource + carrierTarget + availSource * carrierTarget * consts * (trgE->boltzeUse - srcE->boltzeUse));
	double deriv = -(derivSource + derivTarget);
	double deriv2 = 2.0 * consts * (srcE->boltzeUse * (deriv + tauiT) - trgE->boltzeUse * (deriv + tauiS));
	double dSourceOnly = tauiT * consts * srcE->boltzeUse * carrierTarget * (availSource * consts * srcE->boltzeUse - 1.0)	//a_s exp(-Es) / SUM{a_k exp(-Ek) <= 1.0, so negative as expected
		- tauiS * consts * trgE->boltzeUse * availTarget * (carrierSource * consts * srcE->boltzeUse + 1.0);	//negative all around

	double nrate = DoubleSubtract(rateIn,rateOut);
	ELevelRate rateData(srcE, trgE, RATE_SCATTER,srcE->carriertype,srcE->carriertype,trgE->carriertype,nrate,rateOut,rateIn,rateOut,-1.0,deriv,deriv2,dSourceOnly);

	if(sim->TransientData && sim->simType == SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;

	//RecordRDataLimits(rateData, srcE, rateIn, rateOut, carrierSource + carrierTarget, -1.0);	//PosEQ is out(- change), netRate is in (+ change), need -1.0 for + timestep
	ret = srcE->AddELevelRate(rateData, false);	//add the rate... but this is kind of backwards, but is corrected for as rateData.carrier != rateData.sourcecarrier

	return(ret);
}

int CalcScatterConsts(double& sum, std::multimap<MaxMin<double>,ELevel>& states, double NegBeta)
{
	sum = 0.0;
	double occ, avail;
	for(std::multimap<MaxMin<double>,ELevel>::iterator elIt = states.begin(); elIt != states.end(); ++elIt)
	{
		elIt->second.GetOccupancyOcc(RATE_OCC_END, occ, avail);
		elIt->second.boltzeUse = exp(elIt->second.eUse * NegBeta);	//note, eUse is referenced to band minimum... which is good here. intraband scattering
		//node->Data.FermiProb = node->Data.CalcProbFromFermi(fermi);
		sum += elIt->second.boltzeUse * avail;
	}

	if(sum > 0.0)
		sum = 1.0 / sum;	//invert it for partition function

	return(0);
}
