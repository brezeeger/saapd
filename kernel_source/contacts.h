#ifndef SAAPD_CONTACTS_H
#define SAAPD_CONTACTS_H

#include <vector>
#include <map>
#include <list>
#include "BasicTypes.h"
#include "RateGeneric.h"


#define CONTACT_INACTIVE 0	//this contact isn't doing anything
#define CONTACT_SS 1	//flag to say this is steady state
#define CONTACT_CONNECT_EXTERNAL 2	//used primarily on in plane edges of devices where you want a current out - connected to external circuit so calculate current out- conflicts with contact sink
#define CONTACT_SINK 4	//flag that this contact acts as a sink. There is no external point that this connects to. It stays permanently in thermodynamic equilibrium with itself as if it were separate from the device.
#define CONTACT_MINIMIZE_E_ENERGY 8	//this contact should be trying to choose BC 'value' to minimize the energy stored in the electric field within the device. An isolated contact, overrides e-field or current calculations
#define CONTACT_OPP_DFIELD 16	//this contact will have the e-field set such the fields are equal and opposite. Should tend to zero the more neutral the device is
#define CONTACT_CONSERVE_CURRENT 32	//this contact (when implemented) will look at the net charge entering/leaving device, and will adjust the E-Field at the contact to ensure kirchoff's law is followed
#define CONTACT_VOLT_COS 64
#define CONTACT_VOLT_LINEAR 128	//the voltages MUST BE less than all those below
#define CONTACT_EFIELD_COS 256
#define CONTACT_EFIELD_LINEAR 512
#define CONTACT_CURRENT_COS 1024
#define CONTACT_CURRENT_LINEAR 2048
#define CONTACT_CUR_DENSITY_COS 4096
#define CONTACT_CUR_DENSITY_LINEAR 8192
#define CONTACT_EFIELD_COS_ED 16384
#define CONTACT_EFIELD_LINEAR_ED 32768
#define CONTACT_CURRENT_COS_ED 65536
#define CONTACT_CURRENT_LINEAR_ED 131072
#define CONTACT_CUR_DENSITY_COS_ED 262144
#define CONTACT_CUR_DENSITY_LINEAR_ED 524288
#define CONTACT_FLOAT_EFIELD_ESCAPE 1048576	//flag for poisson equation that basically adds an extra variable to compensate for another restriction. Mix with CALCULATE_CURRENT_OUT to allow current to leak out this point as well
#define CONTACT_LAST_BOUNDARY_TYPE 1048576

//the stuff below (except leak) are unused thus far
#define CONTACT_EXTERNAL_DIMENSION 2097152 //this contact is acquiring it's value from an area that cannot be part of the mesh (such as contact fingers, though it currently is the same material). Does not apply for voltage. Apply to Y for 1D, Z, for2D, and N/A for 3D
#define CONTACT_MULTIPLE_POINTS 4194304	//this contact may span over multiple points
#define CONTACT_DEFINE_NEW_POINT 8388608	//allow the contact to calculcate as if from a point with very specific material properties
#define CONTACT_NONED_EXTERNAL_INTERACTION 16777216	//this contact interacts with the outside world. If ED, it is assumed to be true. If not, it must be on the edge of the device.
#define CONTACT_LEAK 33554432	//this contact aspect should be gathered from the leakage out of the contact (exp: have 1000V/cm entering in, but 80% leaks out the same direction, so 800V/cm continues. Net 20 V/cm has to influence the surrounding band diagram)
#define CONTACT_RESET 67108864	//flag that this should be a complete reset and not be additional flags (applies only to additional flagS)

// COMBINATIONS OF THE ABOVE DEFINES TO SIMPLIFY LOGIC LATER

//VOLTAGE
#define CONTACT_MASK_VOLT (CONTACT_VOLT_COS | CONTACT_VOLT_LINEAR)
//CURRENTS
#define CONTACT_MASK_CURRENTS_NONED (CONTACT_CURRENT_COS|CONTACT_CURRENT_LINEAR|CONTACT_CUR_DENSITY_COS|CONTACT_CUR_DENSITY_LINEAR)
#define CONTACT_MASK_CURRENTS_ED (CONTACT_CURRENT_COS_ED|CONTACT_CURRENT_LINEAR_ED|CONTACT_CUR_DENSITY_COS_ED|+CONTACT_CUR_DENSITY_LINEAR_ED)
#define CONTACT_MASK_CURRENTS (CONTACT_MASK_CURRENTS_NONED | CONTACT_MASK_CURRENTS_ED)
//ELECTRIC FIELDS
#define CONTACT_MASK_EFIELD_NONED (CONTACT_EFIELD_COS | CONTACT_EFIELD_LINEAR)
#define CONTACT_MASK_EFIELD_ED (CONTACT_EFIELD_COS_ED | CONTACT_EFIELD_LINEAR_ED)
#define CONTACT_MASK_EFIELD (CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_EFIELD_ED)
//INLINE VS EXTERNAL FIELDS/CURRENTS
#define CONTACT_MASK_ED (CONTACT_MASK_CURRENTS_ED | CONTACT_MASK_EFIELD_ED)	//sum of above that is _ED
#define CONTACT_MASK_NONED (CONTACT_MASK_CURRENTS_NONED | CONTACT_MASK_EFIELD_NONED)	//sum of before _ED -- anything having fixed boundary conditions (ignore SS, cur out, E Energy)
//BOUNDARY CONDITION TYPE
#define CONTACT_MASK_SIMPLE_VEC (CONTACT_MASK_NONED | CONTACT_MASK_VOLT)	//Voltage/Electric Field/Current in line
#define CONTACT_MASK_COMPLEX_EFIELD (CONTACT_MINIMIZE_E_ENERGY | CONTACT_OPP_DFIELD | CONTACT_CONSERVE_CURRENT) //in line
//ALL B.C.
#define CONTACT_ALLBC (CONTACT_MASK_SIMPLE_VEC | CONTACT_MASK_COMPLEX_EFIELD)
//SIMPLIFIED FLAGS FOR POISSON PREP EQUATION TYPE
#define CONTACT_MASK_POISSON_VOLT (CONTACT_MASK_VOLT)
#define CONTACT_MASK_POISSON_EFIELD (CONTACT_MASK_EFIELD_NONED | CONTACT_MASK_CURRENTS_NONED | CONTACT_MASK_COMPLEX_EFIELD)
#define CONTACT_MASK_POISSON_EFIELD_ED (CONTACT_FLOAT_EFIELD_ESCAPE | CONTACT_MASK_EFIELD_ED)
//#define CONTACT_MASK_POISSON_E_CURRENT CONTACT_MASK_CURRENTS_NONED
//REDEFINES FOR BOUNDARY CONDITION CLAIRTY
#define BC_VOLT (CONTACT_MASK_VOLT)
#define BC_SIMPLE_EFIELD (CONTACT_MASK_EFIELD_NONED)
#define BC_EFIELD_ED (CONTACT_MASK_EFIELD_ED)
#define BC_SIMPLE_CURRENT (CONTACT_MASK_CURRENTS_NONED)
#define BC_CURRENT_ED (CONTACT_MASK_CURRENTS_ED)
#define BC_MIN_EFIELD_ENERGY (CONTACT_MINIMIZE_E_ENERGY)
#define BC_OPP_EFIELD (CONTACT_OPP_DFIELD)
#define BC_CURRENT_NEUTRAL (CONTACT_CONSERVE_CURRENT)
#define BC_LEAK (CONTACT_LEAK)

//CONTACT POISSON POINT USAGE
#define CONTACT_USE_PLANEREC (CONTACT_SINK | CONTACT_CONNECT_EXTERNAL)


#define SUPPRESS_SET_VOLT 1
#define SUPPRESS_SET_CURRENT 2
#define SUPPRESS_SET_EFIELD 4
#define SUPPRESS_SET_EMIN 8
#define SUPPRESS_SET_JNEUTRAL 16
#define SUPPRESS_SET_OPPD 32
#define SUPPRESS_SET_ALL 63

#define CONTACT_ADD_TO_EXISTING true	//for costimedependence - sum value
#define CONTACT_REPLACE false		//and linear time dependence - sum value

#define CONTACT_HOLD_INDEFINITELY -1.0	//a representative time value that is negative

#define CONTACT_NO_BD_CHANGE 0
#define CONTACT_BD_CHANGE 1

//used in determining calculating the current. What output should it apply to, and should it be calculated?
#define CONTACT_NONE 0 //note, these line up with RATE_CONTACT_SOURCE, RATE_CONTACT_TARGET in RateGeneric.h
#define CONTACT_SRC 1 //note, these line up with RATE_CONTACT_SOURCE, RATE_CONTACT_TARGET in RateGeneric
#define CONTACT_TRG 2 //note, these line up with RATE_CONTACT_SOURCE, RATE_CONTACT_TARGET in RateGeneric
#define CONTACT_BOTH 3 //note, these line up with RATE_CONTACT_SOURCE, RATE_CONTACT_TARGET in RateGeneric
#define CONTACT_CALCULATE 4	//the bit to actually do the calculation


enum ctcIDS{
	CTC_COS_VOLT,
	CTC_COS_EFIELD,
	CTC_COS_CUR,
	CTC_COS_CURD,
	CTC_LIN_VOLT,
	CTC_LIN_EFIELD,
	CTC_LIN_CUR,
	CTC_LIN_CURD,
	CTC_ADDITIONAL,
	CTC_ACTIVE,
	CTC_CUR_OUT,
	CTC_ED_LIN_CUR,
	CTC_ED_COS_CUR,
	CTC_ED_LIN_EFIELD,
	CTC_ED_COS_EFIELD,
	CTC_ED_LIN_CURD,
	CTC_ED_COS_CURD
};



//returns the difference in Ef positive such that carriers flow out of the source
//extern double CalculateDifEf(ELevel* srcE, ELevel* trgE, bool carrierType, int whichSource);	//Driftdiffusion.cpp

class ModelDescribe;
class Model;
class Simulation;
class Point;
class ELevel;
class Contact;
class CosTimeDependence;
class LinearTimeDependence;
class ContactLink;
class ContactCurOut;
class SSContactData;
class TimeDependence;
class TransDataDisplay;

Contact* GetContact(ELevel* eLev);
int CalcRateViaContact(ELevel* src, ELevel* trg, XSpace dim);
int CalcRateViaContact(Point* src, Point* trg, XSpace dim);
int IsRateContactOutput(ELevel* src, ELevel* trg, XSpace dim);
int IsRateContactOutput(Point* srcPt, Point* trgPt, XSpace dim);
int CreateNewContacts(Model& mdl, Simulation* sim, std::vector<Point*> edges);
int DetermineEdgePoints(Model& mdl, std::vector<Point*>& edges);
int Fast_Build_Contact_Current_PFn(std::map<MaxMin<double>, ELevel>& band, std::vector<double>& weight, double& tot, Point* pt, ELevel* src, int whichSource, int whichElse);

double FastELevelContactLink(Contact* curCtc, ContactLink* link, Simulation* sim, ELevel* eLev, int whichCarrierSrc, int whichcarrierOther);

int ProcessContactTransient(Model& mdl, Contact* contact, Simulation* sim, ELevel* oneonly = NULL);
int ProcessContactSteadyState(Model& mdl, Contact* contact, Simulation* sim, ELevel* oneonly = NULL);
int ProcessContactLink(Contact* curCtc, ContactLink* link, Simulation* sim, ELevel* oneonly=NULL);
bool DetermineEDCurrentFlow(Contact* ctc, int ActiveED);
int Build_Contact_Current_PFn(std::multimap<MaxMin<double>, ELevel>& band, std::vector<ELevel*>& state, std::vector<double>& weight, double& tot, Point* pt, std::vector<int>* contactOut = NULL, int COut=-1);
int UpdateContactRate(Contact* ctc, Simulation* sim);



class Contact	//have all the contacts be in the model, and have the points contain a pointer to contact
{
	
	double voltage;	//the present voltage of the contact
	double freqVoltage;	//the fastest frequency of the voltage that are active
	double freqCurrent;	//the fastest frequency of the currents that are active
	double freqEField;	//the fastest frequency of the electric field that are active
	double current;	//current entering the contact
	double eField;	//electric field entering the contact
	double currentDensity;	//current density entering the contact
	double current_ED;	//extra dimensional current - this will impact the band diagram (via efield), but it adds one equation and one known variable
	double eField_ED;	//extra dimensional electric field - this will impact the band diagram, but it adds one equation and one known variable
	double currentDensity_ED;	//extra dimensional current density - this will impact the band diagram (via efield), but it adds one equation and one known variable
	
	int currentActiveValue;	//this should always match up with ActiveValues though calculated independently

	bool suppressVoltagePoisson;	//let's say there are more than two voltages, should this voltage be excluded from solving poisson's equation
	bool suppressCurrentPoisson;	//prevent having such an overly defined system that there is no solution - which may come from currents
	bool suppressEFieldPoisson;		//same can be said for the electric field
	bool suppressEFieldMinimize;	//does this contact want to minimize the electric field? suppress if e field set, or enough stuff is already set
	bool suppressOppDField;		//boundary condition to basically try and get the exiting efields balanced and opposite (which should shoot for ~0, esp. if charge neutral)
	bool suppressChargeNeutral;	//boundary condition to make contacts charge neutral
	bool allowOutputMessages;	//don't spam the trace file or user with error messages resulting from this contact
	double eField_ED_Leak;	//percentage that the electric field will leak deeper out of device (continuing from ther end)
	double current_ED_Leak;	//percentage that the current causes the electric field to leek deeper into the device (continuing on other end).
	double currentD_ED_Leak;	//percentage

	std::map<int, ContactLink> CLinks;	//a link between contacts that allows external resistances to be specified. Useful for external circuits or shunts. Simply do V(fermi level of state)=I(find) R(specified). sort is contact id of the target
	Point* ExternalPoint;	//this will usually be NULL, but allow creating points OUTSIDE the mesh with different properties in the future
	
	double DeterminePsiByCurrent(Point* extPt, Point* curPt, bool forceValue = false, double value = 0.0);
	double DeterminePsiByEField(Point* extPt, Point* ctcPt, bool forceValue = false, double value = 0.0);
	double DeterminePsiByVoltage(Point* extPt, Point* curPt);

public:
	void InitNewIteration();	//run once each iteration.  Updates the contact voltage if needed
	void InitNewIteration(SSContactData* data);	//run once each iteration - updates for steady state values
	
	double GetVoltage(void);		//returns the voltage
	

	double GetCurDensity(void) { return(currentDensity); };
	double GetCurEDDensity(void) { return(currentDensity_ED); };
	double GetEDCurrent(void) { return(current_ED); };
	double GetCurrent(void) { return(current); };
	double GetEDEField(void) { return(eField_ED); };
	double GetEField(void) { return(eField); };
	double GetEFieldLeak(void) { return(eField_ED_Leak); };
	double GetCurrentEDLeak(void) { return(current_ED_Leak); };
	double GetCurrentDensityEDLeak(void) { return(currentD_ED_Leak); };

	
	
	int GetCurrentActive(void) { return(currentActiveValue); };
	int SetSuppressions(bool voltage, bool current, bool eField, bool eMin, bool eOpp, bool JNeutral, int which=SUPPRESS_SET_ALL);
	bool SuppressVoltage(void) { return(suppressVoltagePoisson); };
	bool SuppressCurrent(void) { return(suppressCurrentPoisson); };
	bool SuppressEField(void) { return(suppressEFieldPoisson); };
	bool SuppressMinEField(void) { return(suppressEFieldMinimize); };
	bool SuppressDFieldOpp(void) { return(suppressOppDField); };
	bool SuppressChargeNeutralCurrent(void) { return(suppressChargeNeutral); };
	
	bool GetCLinks(std::vector<ContactLink*>& CLinkVect);
	int AddCLink(ContactLink data);
	
	
	int DeterminePsiContactPts(bool forceValue = false, double value = 0.0);
	void SendErrorMessages(void) { allowOutputMessages = true; return; };
	void PreventErrorMessages(void) { allowOutputMessages = false; return; };
	bool IsErrorMessageOk(void) { return(allowOutputMessages); }
	bool IsContactSink(void);

	std::string name;	//INPUT - used to identify contact to user
	int id;	//used when creating the contact to tie it in with a specified layer so the points can find it
	int ContactEnvironmentIndex;	//INPUT - SS_SimData->ExternalStates[curiter]->ContactEnvironment[this index]->All the data
	bool isolated;		//INPUT. this seems deprecated.
	double netholetime;	//sum over all the various contact points for holes entering. Cleared on output to file.
	double netelectrontime;	//and same for electrons entering
	double netholeiter;
	double neteleciter;
	XSpace JnOutput, JpOutput;	//for steady state recording contact outputs. Positionally based
	XSpace JnOutputEnter, JpOutputEnter;	//for SS contacts, but positive value is positive charge (for Jn and Jp) entering contact (useful for JV curves to make voltages line up)
	double nEnterOutput, pEnterOutput;	//for carriers that non-descriptly leave
	std::map<long int, double> holetime;		//output data for the particular contact (+ = holes added, - = holes removed). Reset every iteration
	std::map<long int, double> electrontime;	//data is net per timestep. Sort is point ID for identification of where it entered/left (in case of currents)
	//which points are needed for keeping track of how netcarriercharge should be handled in the contact.
	std::vector<Point*> point;	//a list of all the points this contact points too
	std::vector<Point*> PlaneRecPoints;	//this gets assigned to easily determine which points the contact point should interact with to count as an output
	std::vector<Point> endPtCalcs;	//these points are created to duplicate point, but in thermal EQ for calculating rates
	int UpdatePlaneRecPoints(XSpace dim);

	bool TestRecCurrent(Point* pta, Point* ptb);
	bool isVoltageActive();
	bool isEFieldActive();
	bool isCurrentActive();
	bool isCurrentDensityActive();

	bool isInjectionEFieldActive();
	bool isInjectionCurrentActive();
	bool isInjectionCurrentDensityActive();

	int SavePrivateData(std::ofstream& file);
	double GetFrequency(void);
	double GetVoltageFrequency(void);
	double GetCurrentFrequency(void);
	double GetEfieldFrequency(void);
	double GetNextTimeChange(void);

	void SetVoltage(double val) { voltage = val; }
	void SetFreqVoltage(double val) {freqVoltage = val; }
	void SetFreqCurrent(double val) { freqCurrent= val; }
	void SetFreqEField(double val) { freqEField = val; }
	void SetCurrent(double val) { current= val; }
	void SetEField(double val) { eField= val; }
	void SetCurrentDensity(double val) { currentDensity= val; }
	void SetCurrent_ED(double val) { current_ED = val; }
	void SetEField_ED(double val) { eField_ED = val; }
	void SetCurrentDensity_ED(double val) { currentDensity_ED = val; }

	void SetCurrent_ED_leak(double val) { current_ED_Leak = val; }
	void SetEField_ED_leak(double val) { eField_ED_Leak = val; }
	void SetCurrentDensity_ED_leak(double val) { currentD_ED_Leak = val; }

	void SetCurrentActiveValue(int val) { currentActiveValue = val; }
	
	int UpdateSS(SSContactData& data);
	int SetThEq(void);
	int SetInactive(void);
	int TransferDataToNewContact(Contact* newctc);
	
	void SaveToXML(XMLFileOut& file);

	Contact();
	~Contact();

};


class ContactLink
{
public:
	double resistance;	//if negative, then use the specified current
	double current;	//rate of positive carge entering the contact
	Contact* targetContact;	//Null if void...
};

class ContactCurOut
{
public:
	double starttime;
	double endtime;
	int flags;
};


#endif