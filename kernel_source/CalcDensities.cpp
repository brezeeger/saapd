#include "CalcDensities.h"

#include "BasicTypes.h"
#include "helperfunctions.h"
#include "model.h"
#include "physics.h"

//want a snapshot, not a time dependence for the calculation of rho!  Otherwise, there would be weird averaging
//things going on which may skew the data.
int CalculateRho(Point& pt)
{	
	//following new method of calculating rho - maintains time integration if needed over the contacts.
	//pt.rho = QCHARGE * pt.voli * (pt.p - pt.n - pt.elecimptime + pt.impdefcharge + pt.netContactChargeCarriers);	//and this looks at a time averaged charge
	//if(pt.rho > 1.0 || pt.rho < -1.0)
	//	int debug=0;	//DEBUG

	//if rho is calculated by time integration, it is smoothed, yes. But charge may be created if carriers focus on one point but leave other points
	//empty. This will cause a distortion in the band diagram.  Go to an exact calculation (and this will also make verify fermi match)
	double elec = pt.cb.GetNumParticles();
	double holes = pt.vb.GetNumParticles();
	double netQ = 0.0;
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.acceptorlike.begin(); it != pt.acceptorlike.end(); ++it)
	{
		netQ += (double)it->second.charge * it->second.GetNumstates();	//charge on unoccupied states. Always full time.
		netQ = DoubleAdd(netQ, it->second.GetEndOccStates());	//in acceptors, holes are the carriers.
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.donorlike.begin(); it != pt.donorlike.end(); ++it)
	{
		netQ += (double)it->second.charge * it->second.GetNumstates();	//charge on unoccupied states. Always full time.
		netQ = DoubleSubtract(netQ, it->second.GetEndOccStates());	//electrons are present in the states for some amount of time...
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.CBTails.begin(); it != pt.CBTails.end(); ++it)
	{
		netQ += (double)it->second.charge * it->second.GetNumstates();	//charge on unoccupied states. Always full time.
		netQ = DoubleSubtract(netQ, it->second.GetEndOccStates());	//electrons are present in the states for some amount of time...
	}
	for(std::map<MaxMin<double>, ELevel>::iterator it = pt.VBTails.begin(); it != pt.VBTails.end(); ++it)
	{
		netQ += (double)it->second.charge * it->second.GetNumstates();	//charge on unoccupied states. Always full time.
		netQ = DoubleAdd(netQ, it->second.GetEndOccStates());	//holes are the occ states
	}

	pt.netCharge = DoubleAdd(DoubleSubtract(holes, elec), netQ);
	pt.rho = QCHARGE * pt.voli * pt.netCharge;

	return(0);
}


double CalculateN(Point* pt)
{
	return(pt->n);	//will just use pt.n in the future, but if I ever missed a function call, it will do this now.
}

double CalculateP(Point* pt)
{
	return(pt->p);
}