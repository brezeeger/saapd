#ifndef TUNNELING_H
#define TUNNELING_H



#define TUNNEL_UNDEFINED 0
#define TUNNEL_SAME_BAND_DONE 1
#define TUNNEL_CHANGE_BAND_DONE 2	//no WKB
#define TUNNEL_CONTINUE 3
#define TUNNEL_1_POINT_SAME 4
#define TUNNEL_1_POINT_CHANGE 5		//no WKB
#define TUNNEL_START 6
#define TUNNEL_ABORT 7


#define TUNNEL_NONE 0	//no tunneling
#define TUNNEL_WKB 1	//use the wkb approximation to calculate the tunneling
#define TUNNEL_WKB_SHARP 2	//use corrections for a sharp interface being present
#define TUNNEL_BAND_IMPURITY 3	//tunneling from a band into an impurity
#define TUNNEL_IMP_IMP 4	//carrier hopping
#define TUNNEL_INTERBAND 5	//tunnel carriers from CB->VB or VB->CB
//#define TUNNEL_INTERBAND_SHARP 6	//tunneling through the bands, encounters a sharp interface along the way

#define SHARP_BARRIER 1E5	//the slope for a barrier to be considered sharp enough to qualify as needing a more complete solution

class ELevel;
class Point;
class RefTrans;
class Model;


class WKBData
{
public:
	Point* prevpoint;
	Point* curpoint;
	double delta;		//half width of the current point
	double deltaPrev;	//half width of the previous point
	double EcCur;	//conduction band minimum of current point
	double EcPrev;	//" " " " previous "
	double EvCur;	//valence " " " current "
	double EvPrev;	//" " " " previous "
	double startpos;	//the position that the point starts
	double endpos;	//the position that the integral ends
	double curpos;	//the current position of the point
	double prevpos;	//the previous point's position
	double betweenpos;	//the position between the two previous points
	bool initBand;	//did the band start in CONDUCTION or VALENCE
	bool carrier;	//is the carrier ELECTRON or HOLE
	int dir;		//what direction is the particle moving (M/P)(X/Y)
	double Energy;	//what energy does the particle have
	double integral;	//the summation of the integral, without the constants
	bool SharpEdge;	//was a high slope or change of material encountered (if so, use T2, else use Tw2)
	double EnergyBarrier;	//if a sharp edge when entering/leaving, this is the barrier used to do the corrections
	double relEnergy;	//relative energy for calculating T1.
	char type;	//what kind of tunneling is this going to be (VB->VB, CB->CB, VB->CB, CB->VB, which corrections, etc.)

	WKBData();	//constructor

};

/*
When it does the tunneling, have it only check up to the previous point.  Can reduce the number of calculations, and
significantly simplify the code as it only has to check the current state. Then you can pass in the previous points band energies
*/

//Function Declarations
int TunnelingMain(int dir, RefTrans& Output, Model& mdl, ELevel* eLev, bool carrierType, bool disableTunneling);
ELevel* FindELevel(WKBData* wkb, Model& mdl);
ELevel* TunnelInterBand(WKBData* wkb, Model& mdl);
int TunnelWKBSharp(WKBData* wkb);
int TunnelWKB(Model& mdl, WKBData* wkb);
int FirstInitIterTWKB(Model& mdl, WKBData* wkb);
int InitIterTWKB(Model& mdl, WKBData* wkb);
int SharpBarrierSearchStartCB(WKBData* wkb, Model& mdl, double EnergyStart, Point* StartPoint);
int SharpBarrierSearchEndCB(WKBData* wkb, Model& mdl, double EnergyEnd, Point* EndPoint);
int SharpBarrierSearchStartVB(WKBData* wkb, Model& mdl, double EnergyStart, Point* StartPoint);
int SharpBarrierSearchEndVB(WKBData* wkb, Model& mdl, double EnergyEnd, Point* EndPoint);
int TunnelStart(Model& mdl, WKBData* wkb);

#endif