#ifndef SAAPD_SCATTER_H
#define SAAPD_SCATTER_H

#include <vector>
#include <map>

class ELevel;
class ELevelRate;
class Simulation;
class PosNegSum;

template <typename T>
class MaxMin;

int CalcScatterDerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget);
double CalcScatterFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate=NULL);
std::vector<ELevelRate>::iterator CalculateScatterRate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
int CalcScatterConsts(double& sum, std::multimap<MaxMin<double>,ELevel>& states, double NegBeta);

#endif