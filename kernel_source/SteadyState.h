#ifndef STEADYSTATE_H
#define STEADYSTATE_H

#include <map>
#include <vector>



/*
#include "wx/wxprec.h"


#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/file.h"
#include "wx/process.h"
#endif
*/

#define SS_BADRET false
#define SS_GOODRET true


#define SS_GOOD 0
#define SS_BAD_1A 1
#define SS_BAD_1B 2
#define SS_BAD_2 4
#define SS_DONE 8

#define SS_INCREASEPERCENT 2.0
#define SS_SOFTINCREASEPERCENT 1.25
#define SS_DECREASEPERCENT 0.25
#define SS_MINPERCENT 1e-7		//tiny in case of bad change
#define SS_MAXPERCENT 0.875	//large change in Ef...
#define SS_MAXCHANGEPREV 1.25	//how much more the change can be in magnitude form the previous change

class Model;
class ModelDescribe;
class kernelThread;
class ELevel;
template <typename T>
class MaxMin;
class Simulation;
class Point;
class ELevelRate;

int SteadyState(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
int OutputSSRates(Model& mdl, ModelDescribe& mdesc);
int UpdateOccupancy(ELevel& eLev);
int UpdateAllOccupancy(Model& mdl, double movePercent);
double FindSSOccupancyAll(Model& mdl, ModelDescribe& mdesc, double& largestNRate, double& maxPercentChange);
int CalcFirstSSERates(Model& mdl, std::multimap<MaxMin<double>, ELevel>& states, Simulation* sim);
int CalcFirstSSRates(Model& mdl, ModelDescribe& mdesc);
int PrepELevelSFirst(std::multimap<MaxMin<double>, ELevel>& states, Model& mdl, Point& pt, Simulation* sim);
double PrepAllSFirst(Model& mdl, ModelDescribe& mdesc);

int SteadyStateOld(Model& mdl, ModelDescribe& mdesc, kernelThread* kThread);
int GetStartCharge(Model& mdl, ModelDescribe& mdesc);
double PrepAllS(Model& mdl, ModelDescribe& mdesc, bool first);
int PrepELevelS(std::multimap<MaxMin<double>,ELevel>& states, Model& mdl, Point& pt, Simulation* sim, bool first);
int CalcAllSSRates(Model& mdl, ModelDescribe& mdesc);
double CalcSSERates(Model& mdl, Point& pt, std::multimap<MaxMin<double>,ELevel>& states, Simulation* sim);
double ProcessAllSSRates(Model& mdl, ModelDescribe& mdesc);
double ProcessELevel(ELevel* eLev, Simulation* sim);
int UpdatePercentTransfer(ELevel* elev);
int RecordAllSSRates(Model& mdl, ModelDescribe& mdesc);
//int RecordELevelSSRates(vector<ELevelOpticalRate>& rates, Point& pt, Simulation* sim);
int RecordELevelSSRates(std::vector<ELevelRate>& rates, Point& pt, Simulation* sim);



#endif