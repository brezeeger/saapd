#include "model.h"

#include <cmath>
#include <complex>
#include <cstring>
#include <string>

#include "physics.h"
#include "contacts.h"
#include "transient.h"
#include "ss.h"
#include "outputdebug.h"
#include "trig.h"
#include "light.h"
#include "CalcBandStruct.h"
#include "srh.h"






bool ELevel::operator==(const ELevel &b)
{
	if(fabs(min - b.min) > EQUIVZERO_POSITION)
		return(false);
	if(fabs(max - b.max) > EQUIVZERO_POSITION)
		return(false);
	if(b.imp != imp)
		return(false);
	if(b.bandtail != bandtail)
		return(false);
	if(b.charge != charge)
		return(false);
	if(b.carriertype != carriertype)
		return(false);

	return(true);
}

bool PointAdjSort::operator<(const PointAdjSort &b)
{
	if(self < b.self)
		return(true);

return(false);
}

bool PointAdjSort::operator>(const PointAdjSort &b)
{
	if (self > b.self)
		return(true);

	return(false);
}

bool PointAdjSort::operator<=(const PointAdjSort &b)
{
	if (self <= b.self)
		return(true);

	return(false);
}

bool PointAdjSort::operator>=(const PointAdjSort &b)
{
	if (self >= b.self)
		return(true);

	return(false);
}

bool PointAdjSort::operator==(const PointAdjSort &b)
{
	if (self == b.self)
		return(true);

	return(false);
}

bool PointAdjSort::operator!=(const PointAdjSort &b)
{
	if (self != b.self)
		return(true);

	return(false);
}


ELevel& ELevel::operator +=(const ELevel &b)	//presumably, energy levels will only add if they're at the same energy...
{
	numstates += b.numstates;
	endoccstates += b.endoccstates;
	//endoccstates now holds the new number of states

	return *this;
}





ModelDescribe::~ModelDescribe()
{
	ClearAll();	//kill everything
}

bool ModelDescribe::isIDUsed(int ID)
{
	for (unsigned int i = 0; i < layers.size(); ++i)
	{
		if (layers[i]->id == ID)
			return(true);
	}

	for (unsigned int i = 0; i < spectrum.size(); ++i)
	{
		if (spectrum[i]->ID == ID)
			return(true);
	}

	for (unsigned int i = 0; i < impurities.size(); ++i)
	{
		if (impurities[i]->isImpurity(ID))
			return(true);
	}

	for (unsigned int i = 0; i < materials.size(); ++i)
	{
		if (materials[i]->id == ID)
			return(true);
	}
	return(false);
}

LightSource* ModelDescribe::getSpectra(int id)
{
	for (unsigned int i = 0; i < spectrum.size(); ++i) {
		if (spectrum[i] && spectrum[i]->ID == id)
			return spectrum[i];
	}
	return nullptr;
}

Impurity* ModelDescribe::getImpurity(int id) {
	for (unsigned int i = 0; i < impurities.size(); ++i) {
		if (impurities[i]->isImpurity(id))
			return impurities[i];
	}
	return nullptr;
}

bool ModelDescribe::addUniqueImpurity(Impurity* imp) {
	if (imp == nullptr)
		return false;
	for (unsigned int i = 0; i < impurities.size(); ++i) {
		if (impurities[i] == imp)
			return false;
	}
	impurities.push_back(imp);
	return true;
}

bool ModelDescribe::removeImpurity(Impurity* imp) {	//this is the master, so it also deletes the memory
	if (imp == nullptr)
		return false;
	for (auto it = impurities.begin(); it != impurities.end(); ++it) {
		if (*it == imp) {
			impurities.erase(it);
			delete imp;
			return true;
		}
	}
	return false;
}

Material* ModelDescribe::getMaterial(int id) {
	for (unsigned int i = 0; i < materials.size(); ++i) {
		if (materials[i]->id == id)
			return materials[i];
	}
	return nullptr;
}

Contact* ModelDescribe::getContact(int id) {
	if (simulation == nullptr)
		return nullptr;
	for (unsigned int i = 0; i < simulation->contacts.size(); ++i) {
		if (simulation->contacts[i]->id == id)
			return simulation->contacts[i];
	}
	return nullptr;
}


void ModelDescribe::SetUnknownIDs()
{
	int largestID = 0;	//make sure layers can't get an id of zero to mess with the contact tests
	std::vector<int*> needsChanging;
	std::vector<int*> contactChanging;
	for (unsigned int i = 0; i < layers.size(); ++i)
	{
		if (layers[i]->id < 0 && layers[i]->contactid == 0)
			needsChanging.push_back(&layers[i]->id);
		else if (layers[i]->id > largestID)	//it already has a valid ID
			largestID = layers[i]->id;
		else if (layers[i]->id < 0 && layers[i]->contactid > 0) //and presumed contactid > 0
		{
			contactChanging.push_back(&layers[i]->id);
			contactChanging.push_back(&layers[i]->contactid);
		}
		
	}

	for (unsigned int i = 0; i < spectrum.size(); ++i)
	{
		if (spectrum[i]->ID < 0)
			needsChanging.push_back(&spectrum[i]->ID);
		else if (spectrum[i]->ID > largestID)
			largestID = spectrum[i]->ID;
	}

	for (unsigned int i = 0; i < impurities.size(); ++i)
	{
		if (impurities[i]->getID() < 0)
			needsChanging.push_back(impurities[i]->getIDPtr());
		else if (impurities[i]->getID() > largestID)
			largestID = impurities[i]->getID();
	}

	for (unsigned int i = 0; i < materials.size(); ++i)
	{
		if (materials[i]->id < 0)
			needsChanging.push_back(&materials[i]->id);
		else if (materials[i]->id > largestID)
			largestID = materials[i]->id;
	}

	largestID++;
	for (unsigned int i = 0; i < needsChanging.size(); ++i)
	{
		*needsChanging[i] = largestID;
		largestID++;
	}
	for (unsigned int i = 0; i < contactChanging.size(); i+=2)
	{
		*contactChanging[i] = largestID;	//the layer id
		*contactChanging[i+1] = largestID;	//the layer contact id
		largestID++;
	}
	return;
}

void ModelDescribe::ClearAll(void)
{
	if(this==NULL)
		return;

	std::vector<Layer*>::iterator itL;
	for(itL = layers.begin(); itL != layers.end(); itL++)
	{
		
		(*itL)->OutputLightCarrierGen.clear();	//delete all nodes. Free memory from layer outputs
		(*itL)->OutputLightIncident.clear();	//delete all nodes

			//deallocate all the ldsescriptors
		std::vector<ldescriptor<Material*>*>::iterator itLDM;
		for(itLDM = (*itL)->matls.begin(); itLDM != (*itL)->matls.end(); itLDM++)
		{
			delete *itLDM;	//delete the memory at this layer
			*itLDM = NULL;
		}
		if(!((*itL)->matls.empty()))
			(*itL)->matls.clear();	//now that memory has been deallocated, can clear out vector

		std::vector<ldescriptor<Impurity*>*>::iterator itLDI;
		for(itLDI = (*itL)->impurities.begin(); itLDI != (*itL)->impurities.end(); itLDI++)
		{
			delete *itLDI;	//delete the memory at this layer
			*itLDI = NULL;
		}

		if(!((*itL)->impurities.empty()))
			(*itL)->impurities.clear();	//note, these are all just pointers for specific layes

		delete *itL;	//clear the memory of this layer
		*itL = NULL;	//reset the pointer of this node.
	}

	if(!(layers.empty()))	
		layers.clear();	//now that all the relelvant memory has been freed here, erase all the pointers

	std::vector<Impurity*>::iterator itI;
	for(itI = impurities.begin(); itI != impurities.end(); itI++)
	{
		delete *itI;	//free memory, but don't screw up vector of pointers. Note, value is an address.
		*itI = NULL;	//null the pointer
	}
	if(!(impurities.empty()))
		impurities.clear();	//now that all the data has been freed, clean out the vector.

	std::vector<Material*>::iterator itM;
	for(itM = materials.begin(); itM != materials.end(); itM++)
	{
		if ((*itM)->optics)
			(*itM)->optics->WavelengthData.clear();	//free all the optical data for each material
		(*itM)->name="";	//just clear out the strings if they're stored somewhere
		(*itM)->describe="";
		delete *itM;	//delete the material data
		(*itM)=NULL;	//null the pointer
	}
	materials.clear();	//clear out the vectors which held the locations of the data

	NDim = 0;
	
	//clear out all data for the simulation
	if(simulation != NULL)
	{
		simulation->curiter = 0;
		delete simulation->TransientData;
		delete simulation->SteadyStateData;
		simulation->TransientData = NULL;
		simulation->SteadyStateData = NULL;
		
		simulation->lightincidentdir = 0;
		simulation->ecurrent.clear();	//Clear vs clear. Tree.Clear(); Vector.clear();
		simulation->holecurrent.clear();	//free memory for the binary trees
		delete simulation->OpticalAggregateOut;
		delete simulation->OpticalFluxOut;
		simulation->OpticalAggregateOut = NULL;
		simulation->OpticalFluxOut = NULL;

		simulation->T = 300;	//just default to 300K
		unsigned int sz = simulation->contacts.size();
		for(unsigned int i=0; i<sz; i++)
		{
			delete simulation->contacts[i];
			simulation->contacts[i] = NULL;
		}
		simulation->contacts.clear();
	}
	delete simulation;	//free memory made by simulation data.
	simulation = NULL;

	std::vector<LightSource*>::iterator itS;
	for(itS = spectrum.begin(); itS != spectrum.end(); itS++)
	{
		(*itS)->light.clear();
		(*itS)->times.clear();
		(*itS)->enabled = false;
		(*itS)->name = "";
		(*itS)->IncidentPoints.clear();
		delete *itS;
		(*itS) = NULL;
	}
	spectrum.clear();	//clear out all of the data for light spectrum
	//note, spectrum holds the actual data, not pointers.  Not new/delete for this variable.

		
	resolution.x = 0;
	resolution.y = 0;

	return;

}

void ModelDescribe::UpdateDimensions() {
	XSpace min(0.0), max(0.0);

	for (unsigned int i = 0; i < layers.size(); ++i) {
		if (layers[i]->shape) {
			max.keepMax(layers[i]->shape->max);
			min.keepMin(layers[i]->shape->min);
		}
	}

	Ldimensions = max - min;

}


void ModelDescribe::ReassociateContacts(int& refRemoved, int& refAdded, int &refUnknown) {
	std::vector<unsigned int> badIndices;	//associate with the layer
	std::vector<unsigned int> availIndices;	//associate with the contact
	std::vector<int> used;	//the contact ids
	if (simulation == nullptr)
		return;

	std::vector<unsigned int> nonContactLayerIndices;	//as a last resort, it will assign contacts to these layers
	for (unsigned int i = 0; i < layers.size(); ++i) {
		if (layers[i]->contactid > 0) {
			Contact *ct = simulation->getContact(layers[i]->contactid);
			if (ct == nullptr) {
				layers[i]->contactid = -layers[i]->contactid;
				badIndices.push_back(i);
			}
			else {
				used.push_back(ct->id);
			}
		}
		else
			nonContactLayerIndices.push_back(i);
	}

	for (unsigned int i = 0; i < simulation->contacts.size(); ++i) {
		if (FindValInVector(used, simulation->contacts[i]->id) >= used.size())	//it did not find this id in used
			availIndices.push_back(i);
	}

	//at this point, all the contacts that are not associated with a layer are in availIndices
	//all the layers that want a contact but could not find the correct ID are in badIndices.
	//all the contact ID's which have been assigned are in used.
	//all the layers that could have a contact but don't really want one are in nonContactLayerIndices

	//first, let's go through all the available contacts and try to pair them with a layer based on name.
	for (unsigned int i = 0; i < availIndices.size(); ++i) {
		Contact *ct = simulation->contacts[availIndices[i]];	//just simplify things a bit.
		for (unsigned int bd = 0; bd < badIndices.size(); ++bd) {
			Layer* lyr = layers[badIndices[bd]];
			std::string ctName = ct->name;
			ToLower(ctName);
			std::string lyrname = lyr->name;
			ToLower(lyrname);	//make sure case doesn't screw up the find function
			size_t findpos = ctName.find(lyrname);	//the generated contact name is Layer name + " contact"
			if (findpos != std::string::npos) {	//we'll consider this a match! 
				lyr->contactid = ct->id;
				availIndices.erase(availIndices.begin() + i);
				badIndices.erase(badIndices.begin() + bd);
				--i;
				break;	//found something, so stop searching through the bad indices
			}
		}	//end loop through bad layer indices
	}	//end loop through contacts needing an assignment

	//now just reassociate what layers are left that wants something with available contacts
	for (unsigned int i = 0; i < badIndices.size(); ++i) {
		if (i < availIndices.size()) {
			layers[badIndices[i]]->contactid = simulation->contacts[availIndices[i]]->id;
			refUnknown++;
		}
		else
			refRemoved++;	//these layers don't have any contacts to associate with, and you just can't create contacts!
	}
	if (availIndices.size() < badIndices.size()) {
		badIndices.erase(badIndices.begin(), badIndices.begin() + availIndices.size());
		availIndices.clear();
	}
	else { // if (availIndices.size() >= badIndices.size()) {
		availIndices.erase(availIndices.begin(), availIndices.begin() + badIndices.size());
		badIndices.clear();
	}

	//and finally, if we have more contacts defined than layers accepting contacts,
	//make some of the layers accept said contacts despite not really being associated with it.
	for (unsigned int i = 0; i < availIndices.size() && i < nonContactLayerIndices.size(); ++i) {
		if (i < nonContactLayerIndices.size()) {
			layers[nonContactLayerIndices[i]]->contactid = simulation->contacts[availIndices[i]]->id;
			refAdded++;
		}
	}

	if (availIndices.size() > nonContactLayerIndices.size()) {
		availIndices.erase(availIndices.begin(), availIndices.begin() + nonContactLayerIndices.size());
	}
	else
		availIndices.clear();

	//at this point, there are just waaay too many contacts defined. More contacts than layers. gotta delete the rest and say tough luck
	for (unsigned int i = 0; i < availIndices.size(); ++i) {
		delete simulation->contacts[availIndices[i]];
		simulation->contacts.erase(simulation->contacts.begin() + availIndices[i]);
		refRemoved++;
	}

	availIndices.clear();
	used.clear();
	badIndices.clear();

}


void Impurity::Validate(ModelDescribe& mdesc) {
	if (defectMaterial) {
		if (mdesc.HasMaterial(defectMaterial) == false)
			defectMaterial = nullptr;
	}
}

void ModelDescribe::Validate() {
	for (unsigned int i = 0; i < impurities.size(); ++i) {
		if (impurities[i] == nullptr) {
			impurities.erase(impurities.begin() + i);
			--i;
			continue;
		}
		impurities[i]->Validate(*this);
	}
	
	for (unsigned int i = 0; i < materials.size(); ++i) {
		if (materials[i] == nullptr) {
			materials.erase(materials.begin() + i);
			--i;
			continue;
		}
		materials[i]->Validate(*this);
	}

	for (unsigned int i = 0; i < spectrum.size(); ++i) {
		if (spectrum[i] == nullptr) {
			spectrum.erase(spectrum.begin() + i);
			--i;
		}
	}

	for (unsigned int i = 0; i < layers.size(); ++i) {
		if (layers[i] == nullptr) {
			layers.erase(layers.begin() + i);
			--i;
			continue;
		}
		layers[i]->Validate(*this);
	}

	if (simulation)
		simulation->Validate();
}

//this essentially makes the model data consistent within itself
int ModelDescribe::ReassociateAll() {
	//first, make sure everything gets a valid ID. No duplicates of something bad. Everything positive!
	SetUnknownIDs();
	int potentialUnknownCount = 0;
	int referencesRemoved = 0;
	int referencesAdded = 0;
	//first, let's do the contacts.
	ReassociateContacts(referencesRemoved, referencesAdded, potentialUnknownCount);

	//we can have any number of materials or impurities. Those get referenced by the other...
	//these are all pointer links without id's attached for rebuilding.

	//check to make sure all the material defect is in the master list. If not, then it needs to be removed.
	for (unsigned int m = 0; m < materials.size(); ++m) {
		Material* mat = materials[m];
		for (unsigned int i = 0; i < mat->defects.size(); ++i) {
			unsigned int index = FindValInVector(impurities, mat->defects[i]);
			if (index >= impurities.size()) { //it was not found in the primary list
				mat->defects.erase(mat->defects.begin() + i);
				--i;
				referencesRemoved++;
				//note, purposefully no delete. Material does NOT own the pointer. impurities did. poor cleanup on prior delete
			}
		}
	}

	//now all the layers need checking
	for (unsigned int i = 0; i < layers.size(); ++i) {
		for (unsigned int j = 0; j < layers[i]->impurities.size(); ++j) {
			int id = layers[i]->impurities[j]->id;
			Impurity *imp = getImpurity(id);
			layers[i]->impurities[j]->data = imp;
			if (imp == nullptr) {
				layers[i]->impurities[j]->id = -1;
				referencesRemoved++;
			}
		}

		for (unsigned int j = 0; j < layers[i]->matls.size(); ++j) {
			int id = layers[i]->matls[j]->id;
			Material *mat = getMaterial(id);
			layers[i]->matls[j]->data = mat;
			if (mat == nullptr) {
				layers[i]->matls[j]->id = -1;	//this will prevent a save from occurring in the xml file
				referencesRemoved++;
			}
		}
	}

	//now the fun part... the simulation stiff
	if (simulation) {
		TransientSimData *tsd = simulation->TransientData;
		if (tsd) {
			for (unsigned int i = 0; i < tsd->ContactData.size(); ++i) {
				Contact *ct = getContact(tsd->ContactData[i]->GetContactID());
				tsd->ContactData[i]->SetContact(ct);	//this will appropriately adjust hte id as well
				if (ct == nullptr)
					referencesRemoved++;
			}

			for (unsigned int i = 0; i < tsd->SpecTime.size(); ++i) {
				if (getSpectra(tsd->SpecTime[i].SpecID) == nullptr) {
					tsd->SpecTime.erase(tsd->SpecTime.begin() + i);
					--i;
					referencesRemoved++;
				}
			}
		}
		SS_SimData *ssd = simulation->SteadyStateData;
		if (ssd) {
			for (unsigned int i = 0; i < ssd->ExternalStates.size(); ++i) {
				for (unsigned int j = 0; j < ssd->ExternalStates[i]->ActiveLightEnvironment.size(); ++j) {
					if (getSpectra(ssd->ExternalStates[i]->ActiveLightEnvironment[j]) == nullptr) {
						ssd->ExternalStates[i]->ActiveLightEnvironment.erase(ssd->ExternalStates[i]->ActiveLightEnvironment.begin() + j);
						--j;
						referencesRemoved++;
					}
				}

				for (unsigned int j = 0; j < ssd->ExternalStates[i]->ContactEnvironment.size(); ++j) {
					Contact* ct = getContact(ssd->ExternalStates[i]->ContactEnvironment[j]->ctcID);
					if (ct) {
						ssd->ExternalStates[i]->ContactEnvironment[j]->ctc = ct;
					}
					else {
						ssd->ExternalStates[i]->ContactEnvironment[j]->ctc = ct;	//mark as an unknown link
						ssd->ExternalStates[i]->ContactEnvironment[j]->ctcID = -1;
						referencesRemoved++;
					}
				}
			}
		}
	}

	return referencesAdded + referencesRemoved + potentialUnknownCount;
	
}
bool ModelDescribe::HasImpurity(Impurity* imp) {
	if (imp == nullptr)
		return false;
	if (FindValInVector(impurities, imp) < impurities.size())
		return true;
	return false;
}
bool ModelDescribe::HasMaterial(Material* mat)
{
	if (mat == nullptr)
		return false;
	if (FindValInVector(materials, mat) < materials.size())
		return true;
	return false;
}

bool ModelDescribe::HasContact(Contact *ctc) {
	if (ctc == nullptr || simulation == nullptr)
		return false;
	if (FindValInVector(simulation->contacts, ctc) < simulation->contacts.size())
		return true;
	return false;
}
bool ModelDescribe::HasSpectrum(LightSource* spc) {
	if (spc == nullptr)
		return false;
	if (FindValInVector(spectrum, spc) < spectrum.size())
		return true;
	return false;
}

bool ModelDescribe::HasLayer(Layer* lyr) {
	if (lyr == nullptr)
		return false;
	if (FindValInVector(layers, lyr) < layers.size())
		return true;
	return false;
}

bool ModelDescribe::RemoveImpurity(Impurity* imp) {
	if (imp == nullptr)
		return false;
	
	int id = imp->getID();
	//go through and remove all possible references
	for (unsigned int i = 0; i < layers.size(); ++i) {
		for (unsigned int j = 0; j < layers[i]->impurities.size(); ++j) {
			if (layers[i]->impurities[j]->id == id)
				layers[i]->impurities[j]->id = -1;	//don't necessarily delete the data. Allow the user to reassign it with something else
		}
	}
	Material* mat = imp->getDefectMaterial();
	if (mat) {
		RemoveVectorValue(mat->defects, imp);
	}

	RemoveVectorValue(impurities, imp);
	
	delete imp;
	return true;
}

bool ModelDescribe::RemoveMaterial(Material* mat) {
	if (mat == nullptr)
		return false;

	int id = mat->id;
	//remove all references from the layers
	for (unsigned int i = 0; i < layers.size(); ++i) {
		for (unsigned int j = 0; j < layers[i]->matls.size(); ++j) {
			if (layers[i]->matls[j]->id == id)
				layers[i]->matls[j]->id = -1;	//don't necessarily delete the data. Allow the user to reassign it with something else
		}
	}
	//remove all it's defects
	for (unsigned int i = 0; i < mat->defects.size(); ++i) {
		Impurity* imp = mat->defects[i];
		RemoveVectorValue(impurities, imp);
		delete imp;
	}
	mat->defects.clear();

	delete mat;
	return true;
}

bool ModelDescribe::RemoveContact(Contact *ctc) {
	if (ctc == nullptr)
		return false;

	int id = ctc->id;
	//remove all layer references
	for (unsigned int i = 0; i < layers.size(); ++i) {
		if (layers[i]->contactid == id)
			layers[i]->contactid = -1;	//it has no contact association anymore
	}
	if (simulation) {
		if (simulation->TransientData) {
			for (unsigned int i = 0; i < simulation->TransientData->ContactData.size(); ++i) {
				TransContact *tc = simulation->TransientData->ContactData[i];
				if (tc->GetContact() == ctc) {
					std::vector<TransContact*>& v = simulation->TransientData->ContactData;
					v.erase(v.begin() + i);
					--i;
					delete tc;
					tc = nullptr;
				}
			}

		}
		if (simulation->SteadyStateData) {
			for (unsigned int i = 0; i < simulation->SteadyStateData->ExternalStates.size(); ++i) {
				std::vector<SSContactData*>& c = simulation->SteadyStateData->ExternalStates[i]->ContactEnvironment;
				for (unsigned int j = 0; j < c.size(); ++j) {
					if (c[j]->ctc == ctc) {
						delete c[j];
						c[j] = nullptr;
						c.erase(c.begin() + j);
						--j;
					}
				}
			}
		}
		RemoveVectorValue(simulation->contacts, ctc);
	}
	delete ctc;

	return true;
}

bool ModelDescribe::RemoveSpectrum(LightSource* spc) {
	if (spc == nullptr)
		return false;

	int id = spc->ID;
	if (simulation) {
		//remove all references in the transient data
		if (simulation->TransientData) {
			for (unsigned int i = 0; i < simulation->TransientData->SpecTime.size(); ++i) {
				if (simulation->TransientData->SpecTime[i].SpecID == id) {
					simulation->TransientData->SpecTime.erase(simulation->TransientData->SpecTime.begin() + i);
					--i;
				}
			}
		}
		//remove all in the steady state data
		if (simulation->SteadyStateData) {
			for (unsigned int i = 0; i < simulation->SteadyStateData->ExternalStates.size(); ++i) {
				RemoveVectorValue(simulation->SteadyStateData->ExternalStates[i]->ActiveLightEnvironment, id);
			}
		}
	}

	RemoveVectorValue(spectrum, spc);
	delete spc;

	return true;
}

ModelDescribe::ModelDescribe()
{
	layers.clear();
	spectrum.clear();
	materials.clear();
	impurities.clear();
	simulation = NULL;
	Ldimensions.x = 0;
	Ldimensions.y = 0;
	Ldimensions.z = 0.0;
	NDim = 1;
	resolution.x = 1.0e-10;
	resolution.y = 1.0e-4;
	resolution.z = 1.0e-4;
}

DerivRateIn::DerivRateIn()
{
	value = 0.0;
	src = wrt = NULL;
}

DerivRateIn::~DerivRateIn()
{
	value = 0.0;
	src = wrt = NULL;
}

Point::Point(Model& md) : mdl(md)
{
	poissonDependency = NULL;
	OpticalOut = NULL;
	LightEmissions = NULL;
	OpticTransitions = NULL;
	Clear();
}

Point::~Point()	//ensure everything is nulled and cleared out when a point is destroyed, which will happen when gridp.clear();
{
	Clear();
}

int Point::Clear()
{
	adj.adjN = NULL;
	adj.adjP = NULL;
	adj.adjMX = NULL;
	adj.adjMX2 = NULL;
	adj.adjPX = NULL;
	adj.adjPX2 = NULL;
	adj.adjMY = NULL;
	adj.adjMY2 = NULL;
	adj.adjPY = NULL;
	adj.adjPY2 = NULL;
	adj.adjMZ = NULL;
	adj.adjMZ2 = NULL;
	adj.adjPZ = NULL;
	adj.adjPZ2 = NULL;
	adj.self = -1;
	position.x = 0.0;
	position.y = 0.0;
	position.z=0.0;
	EField.x = 0.0;
	EField.y = 0.0;
	EField.z = 0.0;
	widthi.x=0;
	widthi.y=0;
	widthi.z=0;

	limitsTStep = NULL;

	MatProps = NULL;	//was a pointer to reference data. May no longer exist, but don't want to delete that data
	cb.energies.clear();
	vb.energies.clear();
	AngleData.clear();
	derivs.clear();
	PsiCf.clear();
	largestPsiCf = 0.0;
	contactData = NULL;
	vol = 0.0;
	voli = 0.0;
	elecindonor = 0.0;
	holeinacceptor = 0.0;
	holeTails = 0.0;
	elecTails = 0.0;
//	psicoeff.ClearAll();
	impdefcharge = 0.0;
	sizeScaling = 1.0;
	
	std::vector<Impurity*>::iterator itI;
	for(itI = impurity.begin(); itI != impurity.end(); itI++)
	{
		*itI = NULL;	//clear out all pointers. Once again, don't want to delete impurity data, just the point model
	}

	for(std::vector<GroupThermalizationData>::iterator it = ThermData.begin(); it!=ThermData.end(); ++it)
	{
		it->Clear();	//make sure no data leaks
	}
	ThermData.clear();
	
	impurity.clear();
	donorlike.clear();
	acceptorlike.clear();
	CBTails.clear();
	VBTails.clear();
	Psi = 0.0;
	fermi = 0.0;
	fermin = 0.0;
	fermip = 0.0;
	eps = 0.0;
	rho = 0.0;
	n = 0.0;
	p = 0.0;
	niCarrier = 0.0;
	heat = 0.0;
	delete OpticalOut;
	delete LightEmissions;
	delete OpticTransitions;
	//OpticTransitions.clear();
	delete [] poissonDependency;
	poissonDependency = NULL;
	OpticalOut = NULL;
	LightEmissions = NULL;
	OpticTransitions = NULL;
	pDsize = 0;
	//delete all the output data present
	Output.Deallocate();

	return(0);
}

//returns what Psi is at the edge on the side of the point in question
double Point::DeterminePsiEdge(int direction)
{
	Point* adjPt = NULL;
	double distTotal;
	double distBorder;
	switch (direction)
	{
	case NX:
		adjPt = adj.adjMX;
		distBorder = position.x - mxmy.x;
		if (adjPt)
		{
			distTotal = position.x - adjPt->position.x;
		}
		break;
	case NX2:
		adjPt = adj.adjMX2;
		distBorder = position.x - mxmy.x;
		if (adjPt)
		{
			distTotal = position.x - adjPt->position.x;
		}
		break;
	case PX:
		adjPt = adj.adjPX;
		distBorder = pxpy.x - position.x;
		if (adjPt)
		{
			distTotal = adjPt->position.x - position.x;
		}
		break;
	case PX2:
		adjPt = adj.adjPX2;
		distBorder = pxpy.x - position.x;
		if (adjPt)
		{
			distTotal = adjPt->position.x - position.x;
		}
		break;
	case NY:
		adjPt = adj.adjMY;
		distBorder = position.y - mxmy.y;
		if (adjPt)
		{
			distTotal = position.y - adjPt->position.y;
		}
		break;
	case NY2:
		adjPt = adj.adjMY2;
		distBorder = position.y - mxmy.y;
		if (adjPt)
		{
			distTotal = position.y - adjPt->position.y;
		}
		break;
	case PY:
		adjPt = adj.adjPY;
		distBorder = pxpy.y - position.y;
		if (adjPt)
		{
			distTotal = adjPt->position.y - position.y;
		}
		break;
	case PY2:
		adjPt = adj.adjPY2;
		distBorder = pxpy.y - position.y;
		if (adjPt)
		{
			distTotal = adjPt->position.y - position.y;
		}
		break;
	case NZ:
		adjPt = adj.adjMZ;
		distBorder = position.z - mxmy.z;
		if (adjPt)
		{
			distTotal = position.z - adjPt->position.z;
		}
		break;
	case NZ2:
		adjPt = adj.adjMZ2;
		distBorder = position.z - mxmy.z;
		if (adjPt)
		{
			distTotal = position.z - adjPt->position.z;
		}
		break;
	case PZ:
		adjPt = adj.adjPZ;
		distBorder = pxpy.z - position.z;
		if (adjPt)
		{
			distTotal = adjPt->position.z - position.z;
		}
		break;
	case PZ2:
		adjPt = adj.adjPZ2;
		distBorder = pxpy.z - position.z;
		if (adjPt)
		{
			distTotal = adjPt->position.z - position.z;
		}
		break;
	default:
		break;
	}
	if (adjPt == NULL)	//just say it is the same value, but this means it is always the same material, simplifying things (D is constant across interfaces)
	{
		double PsiOtherDirection;
		double difPsi = 0.0;
		distTotal = 1.0;	//prevent 0/0 if it fails
		if ((direction == NX || direction == NX2) && adj.adjPX)
		{
			PsiOtherDirection = DeterminePsiEdge(PX);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = pxpy.x - position.x;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}
		else if ((direction == PX || direction == PX2) && adj.adjMX)
		{
			PsiOtherDirection = DeterminePsiEdge(NX);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = position.x - mxmy.x;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}
		else if ((direction == PY || direction == PY2) && adj.adjMY)
		{
			PsiOtherDirection = DeterminePsiEdge(NY);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = position.y - mxmy.y;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}
		else if ((direction == NY || direction == NY2) && adj.adjPY)
		{
			PsiOtherDirection = DeterminePsiEdge(PY);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = pxpy.y - position.y;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}
		else if ((direction == NZ || direction == NZ2) && adj.adjPZ)
		{
			PsiOtherDirection = DeterminePsiEdge(PZ);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = pxpy.z - position.z;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}
		else if ((direction == PZ || direction == PZ2) && adj.adjMZ)
		{
			PsiOtherDirection = DeterminePsiEdge(NZ);	//guaranteed to work
			//distBorder is now the total distance to the opposite edge. Just keep continuing the EField unchanged without worrying about charge adjusting it
			distTotal = position.z - mxmy.z;	//always positive
			difPsi = Psi - PsiOtherDirection;	//this is flipped sign as moving in the other direction
		}

		return(Psi + difPsi * (distBorder / distTotal));
		
	}

	if (adjPt->MatProps == MatProps)
	{
		//then this is really easy
		double difPsi = adjPt->Psi - Psi;
		return(Psi + difPsi * (distBorder / distTotal));
	}
	double relEpsCur = MatProps!=NULL ? double(MatProps->epsR) : 1.0;
	double relEpsAdj = adjPt->MatProps!=NULL ? double(adjPt->MatProps->epsR): 1.0;
	
	double distAdj = distTotal - distBorder;
	double difPsi = adjPt->Psi - Psi;

	return(Psi + difPsi * (distBorder * relEpsCur) / (distBorder * relEpsCur + distAdj * relEpsAdj));
}

bool Point::IsContactSink(void)
{
	if(contactData)
		return(contactData->IsContactSink());
	return(false);
}

int Point::SetLocalVacuum(double newpsi, bool updateFermi)
{
	double change = newpsi - Psi;
	Psi = newpsi;
	if(MatProps)
	{
		cb.bandmin = Psi - MatProps->Phi;
		vb.bandmin = cb.bandmin - MatProps->Eg;
	}
	else
	{
		cb.bandmin += change;
		vb.bandmin += change;
	}
	if(updateFermi)
	{
		fermi += change;
		fermin += change;
		fermip += change;
	}

	return(0);
}

int Point::FindThermalizedELevel(unsigned int& thermIndex,unsigned int& eLevIndex, ELevel* eLev)
{
	unsigned int sz = ThermData.size();
	if(eLev == NULL || sz == 0)
	{
		thermIndex = -1;
		eLevIndex = -1;
		return(-1);
	}
	int type = eLev->GetThermalType();

	//let's see if we can do this quickly first!
	if(thermIndex >= 0 && thermIndex < sz)
	{
		if(ThermData[thermIndex].CheckType(type))
		{
			if(eLevIndex >= 0 && eLevIndex < ThermData[thermIndex].elec.size())
			{
				if(ThermData[thermIndex].GetELev(eLevIndex) == eLev)
					return(0);
			}
			//maybe this one is the right one, though it didn't start off well
			for(eLevIndex=0; eLevIndex < ThermData[thermIndex].elec.size(); ++eLevIndex)
			{ //now go through all the different possibilities for this part
				if(ThermData[thermIndex].GetELev(eLevIndex) == eLev)
				{
					return(0);
				}
			}
		}
	}

	for(thermIndex = 0; thermIndex < sz; ++thermIndex)
	{
		if(ThermData[thermIndex].CheckType(type)) //it's the right type
		{
			for(eLevIndex=0; eLevIndex < ThermData[thermIndex].elec.size(); ++eLevIndex)
			{ //now go through all the different possibilities for this part
				if(ThermData[thermIndex].GetELev(eLevIndex) == eLev)
				{
					return(0);
				}
			}
		}
	}

	thermIndex = eLevIndex = -1;
	return(-1);
}

double Point::GetDeriv(ELevel* src, ELevel* wrt)
{
	for(std::vector<DerivRateIn>::iterator it = derivs.begin(); it!=derivs.end(); ++it)
	{
		if(it->src == src && it->wrt == wrt)
			return(it->value);
	}
	return(0.0);
}

int Point::RemoveDeriv(ELevel* src, ELevel* wrt)
{
	for(std::vector<DerivRateIn>::iterator it = derivs.begin(); it!=derivs.end(); ++it)
	{
		if(it->src == src && it->wrt == wrt)
		{
			derivs.erase(it);
			return(1);
		}
	}
	return(0);
}

double Point::AddDeriv(ELevel* src, ELevel* wrt, double val)
{
	for(std::vector<DerivRateIn>::iterator it = derivs.begin(); it!=derivs.end(); ++it)
	{
		if(it->src == src && it->wrt == wrt)
		{
			it->value += val;
			return(it->value);
		}
	}
	DerivRateIn tmp;
	tmp.src = src;
	tmp.wrt = wrt;
	tmp.value = val;
	derivs.push_back(tmp);
	return(val);
}



int Point::TransferEndToBegin(void)
{
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
		it->second.TransferEndToBegin();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
		it->second.TransferEndToBegin();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
		it->second.TransferEndToBegin();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
		it->second.TransferEndToBegin();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
		it->second.TransferEndToBegin();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
		it->second.TransferEndToBegin();
	return(0);
}

int Point::TransferBeginToEnd(void)
{
	//this is mostly needed to make sure thermalized data that updates begin matches the end state (which begins should have initialized to)
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
		it->second.TransferBeginToEnd();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
		it->second.TransferBeginToEnd();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
		it->second.TransferBeginToEnd();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
		it->second.TransferBeginToEnd();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
		it->second.TransferBeginToEnd();
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
		it->second.TransferBeginToEnd();
	return(0);
}

int Point::ResetData()
{
	PosNegSum charges;
	double occ, impchg;
	n = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		charges.SubtractValue(occ);
		n += occ;
	}
	cb.SetNumParticles(n);

	p = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		charges.AddValue(occ);
		p += occ;
	}
	vb.SetNumParticles(p);
		
	impdefcharge = 0.0;
	holeinacceptor = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		impchg = it->second.GetNumstates() * double(it->second.charge);	//this was the difference. Some states had the charge applied

		charges.AddValue(occ);
		charges.AddValue(impchg);
		impdefcharge += impchg;		//but it wasn't found in the impurity.  This should now have been corrected so the imp will be corrected for bad data as well.
		holeinacceptor += occ;
	}
	
	elecindonor = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		impchg = it->second.GetNumstates() * double(it->second.charge);	//this was the difference. Some states had the charge applied
		charges.SubtractValue(occ);
		charges.AddValue(impchg);
		
		impdefcharge += impchg;		//but it wasn't found in the impurity.  This should now have been corrected so the imp will be corrected for bad data as well.
		elecindonor += occ;
	}
	elecTails = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		charges.SubtractValue(occ);
		elecTails += occ;
	}
	holeTails = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
	{
		occ = it->second.GetEndOccStates();
		charges.AddValue(occ);
		holeTails += occ;
	}

	netCharge = charges.GetNetSumClear();
	rho = netCharge * QCHARGE * voli;

	return(0);
}

int Point::CalcEField()	//we're assuming 1D for now just to get this up and running quickly...
{
	XSpace direction;
	double difPsi;
	double distance;
	XSpace tmpEField;
	tmpEField.clear();	//set all to zero
	int x=0;

/*	if(adj.coordMx != EXT && (adj.coordMx2 == EXT || adj.coordMx2 == adj.coordMx))
	{
		direction = mdl.gridp[adj.coordMx].position - position;	//presumably (-###,0,0)
		distance = direction.Magnitude();
		direction.Normalize();	//make it a vector such as (-1,0,0)...
		difPsi = mdl.gridp[adj.coordMx].Psi - Psi;
		tmpEField.x = -(difPsi / distance);

		x++;
	}
	*/

	if(adj.adjMX)
	{
		x++;
		distance = position.x - adj.adjMX->position.x;
		difPsi = Psi - adj.adjMX->Psi;
		tmpEField.x += difPsi / distance;
	}

	if(adj.adjPX)
	{
		x++;
		distance = adj.adjPX->position.x - position.x;
		difPsi = adj.adjPX->Psi - Psi;
		tmpEField.x += difPsi / distance;
	}

	if(x==2)
		tmpEField.x = tmpEField.x * 0.5;

	EField = tmpEField;
	return(0);
}

int Point::FindELevels(double energyMin, double energyMax, std::vector<ELevel*>& eLevs)
{
	//the values are ordered primarily by the minimum value within the bands, and then max value if the minimums are the same.
	//if they're both the same, then it should clearly just add the two together

	eLevs.clear();	//make sure it's empty to begin with
/*	if(energyMin == energyMax)
	{
		tmpPtr = FindEnergy(energyMin);
		if(tmpPtr != NULL)
			eLevs.push_back(tmpPtr);
		return(0);
	}
	else if(energyMin > energyMax)
	{
		double tmp = energyMin;
		energyMin = energyMax;
		energyMax = tmp;
	}
*/
	double absmin, absmax;
	if(energyMin <= vb.bandmin)
	{
		MaxMin<double> pair;
		pair.max = vb.bandmin - energyMax;
		pair.min = pair.max;	//guarantee that this is the smallest possible value that is acceptable
		//this loop should start at the top of the valence band and work it's way down on minimum hole energies
		std::multimap<MaxMin<double>,ELevel>::iterator vbStates;
		if(energyMax <= vb.bandmin)
			vbStates  = MapFindFirstLessEqual(vb.energies, pair);
		else
			vbStates = vb.energies.begin();

		for(; vbStates != vb.energies.end(); ++vbStates)
		{
			absmax = vb.bandmin - vbStates->second.min;	//minimum for this particular energy state (pos energy moving down)
			absmin = vb.bandmin - vbStates->second.max;	//maximum for this particular energy state (resulting in lowest energy)
			if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
				|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
				|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
				)
			{
				eLevs.push_back(&(vbStates->second));	//first is the key, second is the energy level
			}
			else if(absmax < energyMin)	//then it has moved beyond the scope of the energy levels
				break;
		}

	}

	if(energyMax >= cb.bandmin)
	{
		MaxMin<double> pair;
		pair.max = energyMin - cb.bandmin;	//guarantee that this starts at the lowest energy showing up in the CB
		pair.min = pair.max;
		std::multimap<MaxMin<double>,ELevel>::iterator cbStates;
		if(energyMin >= cb.bandmin)
			cbStates = MapFindFirstLessEqual(cb.energies, pair);
		else
			cbStates = cb.energies.begin();

		for(; cbStates != cb.energies.end(); ++cbStates)
		{
			absmax = cb.bandmin + cbStates->second.max;
			absmin = cb.bandmin + cbStates->second.min;
			if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
				|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
				|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
				)
			{
				eLevs.push_back(&(cbStates->second));
			}
			else if(absmin > energyMax)	//then no more states can possibly overlap
				break;	//get out of loop after it finds the overlap and then no longer overlaps
		}
	}

	//The impurities might be within the band edges, so always have to loop through the defect levels.
	for(std::map<MaxMin<double>, ELevel>::iterator eStates = acceptorlike.begin(); eStates != acceptorlike.end(); ++eStates)
	{
		absmax = vb.bandmin + eStates->second.max;
		absmin = vb.bandmin + eStates->second.min;

		if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
			|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
			|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
			)
			eLevs.push_back(&(eStates->second));
		else if(absmin >= energyMax)
			break;
		
	}

	for(std::map<MaxMin<double>, ELevel>::iterator eStates = VBTails.begin(); eStates != VBTails.end(); ++eStates)
	{
		absmax = vb.bandmin + eStates->second.max;
		absmin = vb.bandmin + eStates->second.min;

		if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
			|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
			|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
			)
			eLevs.push_back(&(eStates->second));
		else if(absmin >= energyMax)
			break;
		
	}

	for(std::map<MaxMin<double>, ELevel>::iterator eStates = donorlike.begin(); eStates != donorlike.end(); ++eStates)
	{
		absmax = cb.bandmin - eStates->second.min;
		absmin = cb.bandmin - eStates->second.max;

		if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
			|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
			|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
			)
			eLevs.push_back(&(eStates->second));
		else if(absmax <= energyMin)
			break;
		
	}

	for(std::map<MaxMin<double>, ELevel>::iterator eStates = CBTails.begin(); eStates != CBTails.end(); ++eStates)
	{
		absmax = cb.bandmin - eStates->second.min;
		absmin = cb.bandmin - eStates->second.max;

		if((energyMax <= absmax && energyMax >= absmin)	//is the max line of the range in the point
			|| (energyMin <= absmax && energyMin >= absmin)	//or is the min range line in thetargeted energy level
			|| (energyMax >= absmax && energyMin <= absmin)	//or is the range completely encompassing the energy level
			)
			eLevs.push_back(&(eStates->second));
		else if(absmax <= energyMin)
			break;
		
	}
	return(0);
}


Outputs::Outputs()
{
	fermi = true;
	fermin = true;
	fermip = true;
	Ec = true;
	Ev = true;
	Psi = true;
	CarrierCB = true;
	CarrierVB = true;
	impcharge = true;
	n = true;
	p = true;
	genth = true;
	SRH = true;
	recth = true;
	rho = true;
	scatter = true;
	Jp = true;
	Jn = true;
	stddev = true;
	error = false;
	Lpow = true;
	Lgen = true;
	Lsrh = true;
	Ldef = true;
	Lscat = true;
	Erec = true;
	Esrh = true;
	Edef = true;
	Escat = true;
	QE = true;
	LightEmission = true;
	chgError = false;
	CBNRin = false;
	VBNRin = false;
	DonNRin = false;
	AcpNRin = false;
	EmptyQEFiles = false;
	jvCurves = true;
	envSummaries = true;
	envResults = true;
	contactStates = true;
	capacitance = false;
	OutputAllIndivRates = false;
	OutputRatePlots = false;
	jvCurves = true;
	envSummaries = true;
	envResults = true;
	contactStates = true;
	capacitance = false;
	numinbin = 1;
}

void Outputs::ClearAll() {
	fermi=fermin=fermip=Ec=Ev=Psi=CarrierCB=CarrierVB
		=impcharge=n=p=Lpow=Lgen=Lsrh=Ldef=Lscat=Erec=Esrh
		=Edef=Escat=genth=SRH=recth=rho=scatter=Jp=Jn
		=CBNRin=VBNRin=DonNRin=AcpNRin=QE=LightEmission
		=EmptyQEFiles=OutputAllIndivRates=OutputRatePlots
		=stddev=error=chgError=contactCharge=jvCurves
		=envSummaries=envResults=contactStates=capacitance=false;
	numinbin=1;
}

void Point::UpdateELevelFermi(void)
{
	double Energy = 0.0;	//energy of the band
	bool tempZero = false;		//flag if temperature is absolute zero
	double kTi = 0.0;		//kT inverse.
	if (temp <= 0.0)
		tempZero = true;	//temperature is zero. set fermi to 0, 0.5, or 1.0
	else			//avoid divide by zero
		kTi = 1.0 / (KB * temp);

	//run through calculations for conduction band
	double invfermi = 0.0;
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it != cb.energies.end(); ++it)
	{
		Energy = cb.bandmin + it->second.eUse;	//energy is relative
		it->second.fermi = FermiFn(fermin, Energy, kTi, false, tempZero, 1.0) * it->second.GetNumstates();	//false means prob of electron
		invfermi += it->second.fermi;
	}
	cb.partitioni = 1.0 / invfermi;

	
	invfermi = 0.0;

	for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it != vb.energies.end(); ++it)
	{
		Energy = vb.bandmin - it->second.eUse;
		it->second.fermi = FermiFn(fermip, Energy, kTi, true, tempZero, 1.0) * it->second.GetNumstates();	//true means inverse, prob of hole
		invfermi += it->second.fermi;
	}
	vb.partitioni = 1.0 / invfermi;

	//Handle all the non-band states
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
	{
		Energy = vb.bandmin + it->second.eUse;
		it->second.fermi = FermiFn(fermi, Energy, kTi, true, tempZero, it->second.imp->getDegeneracy()) * it->second.GetNumstates();	//true means inverse, prob of hole
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
	{
		Energy = cb.bandmin - it->second.eUse;
		it->second.fermi = FermiFn(fermi, Energy, kTi, false, tempZero, it->second.imp->getDegeneracy()) * it->second.GetNumstates();	//false means prob of electron
	}

	for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
	{
		Energy = vb.bandmin + it->second.eUse;
		it->second.fermi = FermiFn(fermi, Energy, kTi, true, tempZero, 1.0) * it->second.GetNumstates();	//true means inverse, prob of hole
	}
	for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
	{
		Energy = cb.bandmin - it->second.eUse;
		it->second.fermi = FermiFn(fermi, Energy, kTi, false, tempZero, 1.0) * it->second.GetNumstates();	//false means prob of electron
	}
	
	return;
	
}

int Point::ThermalizeAllES(void)
{
	ThermalizeES(THERMALIZE_CBT);
	ThermalizeES(THERMALIZE_VBT);
	ThermalizeES(THERMALIZE_DON);
	ThermalizeES(THERMALIZE_ACP);
	return(0);
}

int Point::ThermalizeES(int which)
{
	std::vector<ELevel*> AlleLevs;
	double totOcc = 0.0;	//we'll either try to match this
	double totAvail = 0.0;	//or this - whichever is smaller (more accurate)
	double testTot = 0.0;
	double maxFermi, minFermi, testFermi, totDeriv, difference;
	double* occ = NULL;
	double* avail = NULL;
	double* energies = NULL;
	double* num = NULL;
	double* deriv = NULL;
	unsigned int sz;
	std::vector<ELevel*> eLevs;
	std::vector<Impurity*> foundImps;
	bool occType;
	//thermalize impurities individually. Their may be multiple donors present.
	//don't thermalize all of them together, but each one individually
	unsigned int numImpType = 0;
	maxFermi = -1e100;	//really big numbers in opposite direction
	minFermi = 1e100;
	double beta = KBI / temp;

	if(which == THERMALIZE_CBT)
	{
		occType = ELECTRON;
		sz = CBTails.size();
		if(sz==0)
			return(0);

		occ = new double[sz];
		avail = new double[sz];
		energies = new double[sz];
		num = new double[sz];
		deriv = new double[sz];
		eLevs.resize(sz);
		unsigned int i=0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it!=CBTails.end(); ++it)
		{
			it->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ[i], avail[i]);
			num[i] = it->second.GetNumstates();
			energies[i] = -it->second.eUse;
			eLevs[i] = &(it->second);

			difference = it->second.CalcRelFermiEnergy(RATE_OCC_BEGIN, false);
			//now returns positive Ef-E if large number of carriers (e- or holes)
			//it was E-Ef for electrons
			//returns positive E - Ef for electrons, Ef-E for holes
			//where E is the exact energy of the state. This is the CB Tails
			//we want to know where Ef is relative conduction band, so Ec - Ef.
			//we're assuming Ec=0, so E = -Euse
			//therefore the fermi level distance from Ec is Euse + difference
			//Ec - Ef = Euse + difference
			difference = energies[i] + difference;

			if(difference > maxFermi)
				maxFermi = difference;
			if(difference < minFermi)
				minFermi = difference;

			totOcc += occ[i];
			totAvail += avail[i];
			++i;
		}
		

		totDeriv = 0.0;
		difference = 1.0;
		bool avg = true;
		bool first = true;
		bool testedMax = false;
		bool testedMin = false;
		double ratio;
		testFermi = 0.5 * (minFermi + maxFermi);
		while(difference != 0.0)	//make sure we don't create/destroy any charge
		{
			if(!first)
			{
				if(totDeriv == 0.0 || avg)	//there is no good derivative data
					testFermi = 0.5*(minFermi + maxFermi);
				else
					testFermi -= difference / totDeriv;	//this is only any good if it's really close. 
			}
			first = false;
			if(testFermi > maxFermi || testFermi < minFermi || (testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin))
				testFermi = 0.5*(minFermi + maxFermi);

			testTot = totDeriv = 0.0;
			double tmp;
			for(unsigned int j=0; j<sz; ++j)	//calculate the new occupancy and the derivative for test Ef.
			{
				if(totOcc <= totAvail)
				{
					tmp = exp(beta * (energies[j] - testFermi));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					occ[j] = deriv[j] * num[j];
					deriv[j] = (tmp * occ[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += occ[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
				else
				{
					tmp = exp(beta * (testFermi - energies[j]));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					avail[j] = deriv[j] * num[j];
					deriv[j] = (tmp * avail[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += avail[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
			}

			if(totOcc <= totAvail)
			{
				difference = DoubleSubtract(testTot, totOcc);
				ratio = testTot / totOcc;
			}
			else
			{
				difference = DoubleSubtract(testTot, totAvail);
				ratio = testTot / totAvail;
			}
			avg = (ratio > 10 || ratio < 0.1) ? true : false;	//help it converge faster - the derivative is REALLY slow

			if((difference > 0.0 && minFermi < testFermi && totOcc <= totAvail) || 
			   (difference < 0.0 && minFermi < testFermi && totOcc > totAvail))	//then there were too many carriers, so set a new minimum - note this is the positive distance from the band into the gap
			{
				minFermi = testFermi;	//so it says the fermi level is further into the band gap and lowering the total number of carriers
				testedMin = true;
			}
			else if((difference < 0.0 && maxFermi > testFermi && totOcc <= totAvail) ||
				(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))
			{
				maxFermi = testFermi;
				testedMax = true;
			}
			else if(difference != 0.0 && ((testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin)))
				break;	//it is doing the same value because it is as close as it's going to get to an answer

			if(minFermi == maxFermi)
			{
				if(ratio > 1.01 || ratio <0.99)
				{
					ProgressDouble(44, totOcc);
					ProgressDouble(45, testTot);
				}
				break;
			}
		}	//end while loop trying to merge the two states

		if(totOcc <= totAvail)
		{
			for(unsigned int el=0; el<sz; ++el)
			{
				eLevs[el]->SetBeginOccStates(occ[el]);
				eLevs[el]->SetEndOccStates(occ[el]);
				eLevs[el]->AbsFermiEnergy = cb.bandmin - testFermi;
			}
		}
		else
		{
			for(unsigned int el=0; el<sz; ++el)
			{
				eLevs[el]->SetBeginOccStatesOpposite(avail[el]);
				eLevs[el]->SetEndOccStatesOpposite(avail[el]);
				eLevs[el]->AbsFermiEnergy = cb.bandmin - testFermi;
			}
		}

		delete [] occ;
		delete [] avail;
		delete [] energies;
		delete [] num;
		delete [] deriv;
		eLevs.clear();


	}
	else if(which == THERMALIZE_VBT)
	{
		occType = HOLE;
		sz = VBTails.size();
		if(sz==0)
			return(0);
		occ = new double[sz];
		avail = new double[sz];
		energies = new double[sz];
		deriv = new double[sz];
		num = new double[sz];
		eLevs.resize(sz);
		unsigned int i=0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it!=VBTails.end(); ++it)
		{
			it->second.GetOccupancyOcc(RATE_OCC_BEGIN, occ[i], avail[i]);
			num[i] = it->second.GetNumstates();
			energies[i] = it->second.eUse;
			eLevs[i] = &(it->second);

			difference = it->second.CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns positive for large number holes
			//where E is the exact energy of the state. This is the CB Tails
			//we want to know where Ef is relative conduction band, so Ec - Ef.
			//we're assuming Ec=0, so E = -Euse
			//therefore the fermi level distance from Ec is Euse + difference
			//Ec - Ef = Euse + difference
			difference = energies[i] - difference;

			if(difference > maxFermi)
				maxFermi = difference;
			if(difference < minFermi)
				minFermi = difference;

			totOcc += occ[i];
			totAvail += avail[i];
			++i;
		}
		

		totDeriv = 0.0;
		difference = 1.0;
		bool avg = true;
		bool first = true;
		bool testedMax = false;
		bool testedMin = false;
		double ratio;
		testFermi = 0.5 * (minFermi + maxFermi);
		while(difference != 0.0)	//make sure we don't create/destroy any charge
		{
			if(!first)
			{
				if(totDeriv == 0.0 || avg)	//there is no good derivative data
					testFermi = 0.5*(minFermi + maxFermi);
				else
					testFermi -= difference / totDeriv;	//this is only any good if it's really close. 
			}
			first = false;
			if(testFermi > maxFermi || testFermi < minFermi || (testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin))
				testFermi = 0.5*(minFermi + maxFermi);

			testTot = totDeriv = 0.0;
			double tmp;
			for(unsigned int j=0; j<sz; ++j)	//calculate the new occupancy and the derivative for test Ef.
			{
				if(totOcc <= totAvail)
				{
					tmp = exp(beta * (testFermi - energies[j]));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					occ[j] = deriv[j] * num[j];
					deriv[j] = (tmp * occ[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += occ[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
				else
				{
					tmp = exp(beta * (energies[j] - testFermi));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					avail[j] = deriv[j] * num[j];
					deriv[j] = (tmp * avail[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += avail[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
			}
			if(totOcc <= totAvail)
			{
				difference = DoubleSubtract(testTot, totOcc);
				ratio = testTot / totOcc;
			}
			else
			{
				difference = DoubleSubtract(testTot, totAvail);
				ratio = testTot / totAvail;
			}
			avg = (ratio > 10 || ratio < 0.1) ? true : false;	//help it converge faster - the derivative is REALLY slow

			if((difference > 0.0 && minFermi < testFermi && totOcc <= totAvail) ||	//then there were too many carriers, so set a new minimum - note this is the positive distance from the band into the gap
				(difference < 0.0 && minFermi < testFermi && totOcc > totAvail))
			{
				minFermi = testFermi;	//so it says the fermi level is further into the band gap and lowering the total number of carriers
				testedMin = true;
			}
			else if((difference < 0.0 && maxFermi > testFermi && totOcc <= totAvail) ||
				(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))
			{
				maxFermi = testFermi;
				testedMax = true;
			}
			else if(difference != 0.0 && ((testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin)))
					break;	//it is doing the same value because it is as close as it's going to get to an answer

			if(minFermi == maxFermi)
			{
				if(ratio > 1.01 || ratio <0.99)
				{
					ProgressDouble(44, totOcc);
					ProgressDouble(45, testTot);
				}
				break;
			}
		}	//end while loop trying to merge the two states

		//store the udpated energy states
		if(totOcc <= totAvail)
		{
			for(unsigned int el=0; el<sz; ++el)
			{
				eLevs[el]->SetBeginOccStates(occ[el]);
				eLevs[el]->SetEndOccStates(occ[el]);
				eLevs[el]->AbsFermiEnergy = vb.bandmin + testFermi;
			}
		}
		else
		{
			for(unsigned int el=0; el<sz; ++el)
			{
				eLevs[el]->SetBeginOccStatesOpposite(avail[el]);
				eLevs[el]->SetEndOccStatesOpposite(avail[el]);
				eLevs[el]->AbsFermiEnergy = vb.bandmin + testFermi;
			}
		}

		delete [] occ;
		delete [] avail;
		delete [] energies;
		delete [] num;
		delete [] deriv;
		eLevs.clear();

	}
	else if(which == THERMALIZE_DON)
	{
		occType = ELECTRON;
		numImpType = 0;
		sz = donorlike.size();
		if(sz==0)
			return(0);
		AlleLevs.resize(sz);
		unsigned int i=0;
		bool impFound;
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it!=donorlike.end(); ++it)
		{
			impFound = false;
			AlleLevs[i] = &(it->second);
			++i;

			//now determine if this is a unique impurity
			for(unsigned int j=0; j<foundImps.size(); ++j)
			{
				if(it->second.imp == foundImps[j])
				{
					impFound = true;	//it's found, stop searching through
					break;
				}
			}
			if(impFound==false)
			{
				foundImps.push_back(it->second.imp);
				numImpType++;
			}
		}
			//need to separate by impurity now.

		for(unsigned int imp=0; imp<numImpType; ++imp)
		{
			//now loop through all eLevs
			for(unsigned int all=0; all<AlleLevs.size(); ++all)
			{
				if(AlleLevs[all]->imp == foundImps[imp])
					eLevs.push_back(AlleLevs[all]);
			}

			//initialize for this impurity
			unsigned int thisSize = eLevs.size();
			occ = new double[thisSize];
			avail = new double[thisSize];
			energies = new double[thisSize];
			num = new double[thisSize];
			deriv = new double[thisSize];
			totOcc = 0.0;
			totAvail = 0.0;
			minFermi = 1e100;
			maxFermi = -1e100;

			for(unsigned int el=0; el<thisSize; ++el)
			{
				eLevs[el]->GetOccupancyOcc(RATE_OCC_BEGIN, occ[el], avail[el]);
				num[el] = eLevs[el]->GetNumstates();
				energies[el] = -eLevs[el]->eUse;
				difference = eLevs[el]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns E-Ef for donors
				//where E is the exact energy of the state. This is the CB Tails
				//we want to know where Ef is relative conduction band, so Ec - Ef.
				//we're assuming Ec=0, so E = -Euse
				//therefore the fermi level distance from Ec is Euse + difference
				//Ec - Ef = Euse + difference
				difference = energies[el] + difference;

				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;

				totOcc += occ[el];
				totAvail += avail[el];
			}
				
			totDeriv = 0.0;
			difference = 1.0;
			bool avg = true;
			bool first = true;
			bool testedMax = false;
			bool testedMin = false;
			double ratio;
			testFermi = 0.5 * (minFermi + maxFermi);
			while(difference != 0.0)	//make sure we don't create/destroy any charge
			{
				if(!first)
				{
					if(totDeriv == 0.0 || avg)	//there is no good derivative data
						testFermi = 0.5*(minFermi + maxFermi);
					else
						testFermi -= difference / totDeriv;	//this is only any good if it's really close. 
				}
				first = false;
				if(testFermi > maxFermi || testFermi < minFermi || (testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin))
					testFermi = 0.5*(minFermi + maxFermi);

				testTot = totDeriv = 0.0;
				double tmp;
				for(unsigned int j=0; j<thisSize; ++j)	//calculate the new occupancy and the derivative for test Ef.
				{
					if(totOcc <= totAvail)
					{
						tmp = foundImps[imp]->getDegeneracy() * exp(beta * (energies[j]-testFermi));
						if(MY_IS_FINITE(tmp) == false)
						{
							deriv[j] = 0.0;
							tmp = 0.0;
						}
						else
						{
							deriv[j] = 1.0 + tmp;
							deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
						}
						occ[j] = deriv[j] * num[j];
						deriv[j] = (tmp * occ[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
						testTot += occ[j];
						totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
					}
					else
					{
						tmp = exp(beta * (testFermi - energies[j])) / foundImps[imp]->getDegeneracy();
						if(MY_IS_FINITE(tmp) == false)
						{
							deriv[j] = 0.0;
							tmp = 0.0;
						}
						else
						{
							deriv[j] = 1.0 + tmp;
							deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
						}
						avail[j] = deriv[j] * num[j];
						deriv[j] = (tmp * avail[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
						testTot += avail[j];
						totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
					}
				}
				if(totOcc <= totAvail)
				{
					difference = DoubleSubtract(testTot, totOcc);
					ratio = testTot / totOcc;
				}
				else
				{
					difference = DoubleSubtract(testTot, totAvail);
					ratio = testTot / totAvail;
				}
				avg = (ratio > 10 || ratio < 0.1) ? true : false;	//help it converge faster - the derivative is REALLY slow

				if((difference > 0.0 && minFermi < testFermi && totOcc <= totAvail) || 
				   (difference < 0.0 && minFermi < testFermi && totOcc > totAvail))	//then there were too many carriers, so set a new minimum - note this is the positive distance from the band into the gap
				{
					minFermi = testFermi;	//so it says the fermi level is further into the band gap and lowering the total number of carriers
					testedMin = true;
				}
				else if((difference < 0.0 && maxFermi > testFermi && totOcc <= totAvail) ||
					(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))
				{
					maxFermi = testFermi;
					testedMax = true;
				}
				else if(difference != 0.0 && ((testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin)))
					break;	//it is doing the same value because it is as close as it's going to get to an answer

				if(minFermi == maxFermi)
				{
					if(ratio > 1.01 || ratio <0.99)
					{
						ProgressDouble(44, totOcc);
						ProgressDouble(45, testTot);
					}
					break;
				}
			}	//end while loop trying to merge the two states

			//store the udpated energy states
			if(totOcc <= totAvail)
			{
				for(unsigned int el=0; el<thisSize; ++el)
				{
					eLevs[el]->SetBeginOccStates(occ[el]);
					eLevs[el]->SetEndOccStates(occ[el]);
					eLevs[el]->AbsFermiEnergy = cb.bandmin - testFermi;
				}
			}
			else
			{
				for(unsigned int el=0; el<thisSize; ++el)
				{
					eLevs[el]->SetBeginOccStatesOpposite(avail[el]);
					eLevs[el]->SetEndOccStatesOpposite(avail[el]);
					eLevs[el]->AbsFermiEnergy = cb.bandmin - testFermi;
				}
			}

			delete [] occ;
			delete [] avail;
			delete [] energies;
			delete [] num;
			delete [] deriv;
			eLevs.clear();

		}	//end for loop going through each impurity
		AlleLevs.clear();
		foundImps.clear();
	}
	else if(which == THERMALIZE_ACP)
	{
		occType = HOLE;
		numImpType = 0;
		sz = acceptorlike.size();
		if(sz==0)
			return(0);

		AlleLevs.resize(sz);
		unsigned int i=0;
		bool impFound;
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it!=acceptorlike.end(); ++it)
		{
			impFound = false;
			AlleLevs[i] = &(it->second);
			++i;

			//now determine if this is a unique impurity
			for(unsigned int j=0; j<foundImps.size(); ++j)
			{
				if(it->second.imp == foundImps[j])
				{
					impFound = true;	//it's found, stop searching through
					break;
				}
			}
			if(impFound==false)
			{
				foundImps.push_back(it->second.imp);
				numImpType++;
			}
		}
			//need to separate by impurity now.

		for(unsigned int imp=0; imp<numImpType; ++imp)
		{
			//now loop through all eLevs
			for(unsigned int all=0; all<AlleLevs.size(); ++all)
			{
				if(AlleLevs[all]->imp == foundImps[imp])
					eLevs.push_back(AlleLevs[all]);
			}

			//initialize for this impurity
			unsigned int thisSize = eLevs.size();
			occ = new double[thisSize];
			avail = new double[thisSize];
			energies = new double[thisSize];
			num = new double[thisSize];
			deriv = new double[thisSize];
			totOcc = 0.0;
			totAvail = 0.0;
			minFermi = 1e100;
			maxFermi = -1e100;

			for(unsigned int el=0; el<thisSize; ++el)
			{
				eLevs[el]->GetOccupancyOcc(RATE_OCC_BEGIN, occ[el], avail[el]);
				num[el] = eLevs[el]->GetNumstates();
				energies[el] = eLevs[el]->eUse;
				difference = eLevs[el]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns E-Ef for acceptors.
				//typically, this is negative because the fermi level is above, being mostly unoccupied.
				//this means that the total distance above the valence band is going to be
				//Euse - difference
				difference = energies[el] - difference;

				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;

				totOcc += occ[el];
				totAvail += avail[el];
			}
				
			totDeriv = 0.0;
			difference = 1.0;
			bool avg = true;
			bool first = true;
			bool testedMax = false;
			bool testedMin = false;
			double ratio;
			testFermi = 0.5 * (minFermi + maxFermi);
			while(difference != 0.0)	//make sure we don't create/destroy any charge
			{
				if(!first)
				{
					if(totDeriv == 0.0 || avg)	//there is no good derivative data
						testFermi = 0.5*(minFermi + maxFermi);
					else
						testFermi -= difference / totDeriv;	//this is only any good if it's really close. 
				}
				first = false;
				if(testFermi > maxFermi || testFermi < minFermi || (testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin))
					testFermi = 0.5*(minFermi + maxFermi);

				testTot = totDeriv = 0.0;
				double tmp;
				for(unsigned int j=0; j<thisSize; ++j)	//calculate the new occupancy and the derivative for test Ef.
				{
					if(totOcc <= totAvail)
					{
						tmp = exp(beta * (testFermi - energies[j])) / foundImps[imp]->getDegeneracy();
						if(MY_IS_FINITE(tmp) == false)
						{
							deriv[j] = 0.0;
							tmp = 0.0;
						}
						else
						{
							deriv[j] = 1.0 + tmp;
							deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
						}
						occ[j] = deriv[j] * num[j];
						deriv[j] = (tmp * occ[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
						testTot += occ[j];
						totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
					}
					else
					{
						tmp = exp(beta * (energies[j] - testFermi)) * foundImps[imp]->getDegeneracy();
						if(MY_IS_FINITE(tmp) == false)
						{
							deriv[j] = 0.0;
							tmp = 0.0;
						}
						else
						{
							deriv[j] = 1.0 + tmp;
							deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
						}
						avail[j] = deriv[j] * num[j];
						deriv[j] = (tmp * avail[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
						testTot += avail[j];
						totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
					}
				}
				if(totOcc <= totAvail)
				{
					difference = DoubleSubtract(testTot, totOcc);
					ratio = testTot / totOcc;
				}
				else
				{
					difference = DoubleSubtract(testTot, totAvail);
					ratio = testTot / totAvail;
				}
				avg = (ratio > 10 || ratio < 0.1) ? true : false;	//help it converge faster - the derivative is REALLY slow

				if((difference > 0.0 && minFermi < testFermi && totOcc <= totAvail) ||	//then there were too many carriers, so set a new minimum - note this is the positive distance from the band into the gap
					(difference < 0.0 && minFermi < testFermi && totOcc > totAvail))
				{
					minFermi = testFermi;	//so it says the fermi level is further into the band gap and lowering the total number of carriers
					testedMin = true;
				}
				else if((difference < 0.0 && maxFermi > testFermi && totOcc <= totAvail) ||
					(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))
				{
					maxFermi = testFermi;
					testedMax = true;
				}
				else if(difference != 0.0 && ((testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin)))
					break;	//it is doing the same value because it is as close as it's going to get to an answer

				if(minFermi == maxFermi)
				{
					if(ratio > 1.01 || ratio <0.99)
					{
						ProgressDouble(44, totOcc);
						ProgressDouble(45, testTot);
					}
					break;
				}
			}	//end while loop trying to merge the two states

			//store the udpated energy states
			if(totOcc <= totAvail)
			{
				for(unsigned int el=0; el<thisSize; ++el)
				{
					eLevs[el]->SetBeginOccStates(occ[el]);
					eLevs[el]->SetEndOccStates(occ[el]);
					eLevs[el]->AbsFermiEnergy = vb.bandmin + testFermi;
				}
			}
			else
			{
				for(unsigned int el=0; el<thisSize; ++el)
				{
					eLevs[el]->SetBeginOccStatesOpposite(avail[el]);
					eLevs[el]->SetEndOccStatesOpposite(avail[el]);
					eLevs[el]->AbsFermiEnergy = vb.bandmin + testFermi;
				}
			}

			delete [] occ;
			delete [] avail;
			delete [] energies;
			delete [] num;
			delete [] deriv;
			eLevs.clear();

		}	//end for loop going through each impurity
		AlleLevs.clear();
		foundImps.clear();
	}

	return(0);
}

int Point::Thermalize(unsigned long long int iter, int which)
{
	for(unsigned int i=0; i<ThermData.size(); ++i)
	{
		ThermData[i].AssignNormalize(iter, which);	//checks to see if values are set, and if not, normalizes each group
	}
	TransferBeginToEnd();	//make sure the endocc matches the begin occ.
	ResetData();	//update the data for the point based on what just got updated

	return(0);
}

int Point::ThermalizeBand(bool band)	//essentially scatter everything instantaneously
{
	if(MatProps == NULL)
		return(-1);
	double totOcc = 0.0;
	double totAvail = 0.0;
	double testTot = 0.0;
	double maxFermi, minFermi, testFermi, totDeriv, difference;	//*fermi is relative to the band edge (Ec-Ef or Ef-Ev)
	double* occ = NULL;
	double* energies = NULL;
	double* num = NULL;
	double* deriv = NULL;
	double* avail = NULL;
	std::vector<ELevel*> eLevs;	//don't want to accidentally delete the energy levels!
	double beta = (temp>0) ? KBI / temp : 0.0;
	unsigned int sz;
	double EffectiveDOS;
	bool occType;

	minFermi = 1e100;	//these are relative to the band edge
	maxFermi = -1e100;

	if(band == CONDUCTION)
	{
		sz = cb.energies.size();
		if(sz == 0)
			return(0);
		occ = new double[sz];
		energies = new double[sz];
		num = new double[sz];
		deriv = new double[sz];
		avail = new double[sz];
		unsigned int i=0;
		eLevs.resize(sz);
		EffectiveDOS = cb.EffectiveDensity;
		occType = ELECTRON;
		for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it!=cb.energies.end(); ++it)
		{
			eLevs[i] = &(it->second);
			eLevs[i]->GetOccupancyOcc(RATE_OCC_BEGIN, occ[i], avail[i]);
			num[i] = it->second.GetNumstates();
			energies[i] = it->second.eUse;
			

			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//E - Ef
			//positive difference means lots of electrons
			difference = difference + energies[i];
			
			if(difference > maxFermi)
				maxFermi = difference;
			if(difference < minFermi)
				minFermi = difference;
			totOcc += occ[i];
			totAvail += avail[i];
			++i;
		}
		testFermi = -log(totOcc/EffectiveDOS)/beta;	//E_c - E_f
	}
	else
	{
		sz = vb.energies.size();
		if(sz == 0)
			return(0);
		occ = new double[sz];
		energies = new double[sz];
		num = new double[sz];
		deriv = new double[sz];
		avail = new double[sz];
		unsigned int i=0;
		eLevs.resize(sz);
		EffectiveDOS = vb.EffectiveDensity;
		occType = HOLE;

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it!=vb.energies.end(); ++it)
		{
			eLevs[i] = &(it->second);
			eLevs[i]->GetOccupancyOcc(RATE_OCC_BEGIN, occ[i], avail[i]);
			num[i] = it->second.GetNumstates();
			energies[i] = it->second.eUse;
			
			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);
			//we're going to treat this like the CB, except positive energies are going down
			//doing holes creates another negative, so we can get away with the same math
			difference = difference + energies[i];
			
			if(difference > maxFermi)
				maxFermi = difference;
			if(difference < minFermi)
				minFermi = difference;
			totOcc += occ[i];
			totAvail += avail[i];

			++i;
		}
		testFermi = -log(totOcc/EffectiveDOS)/beta;	//E_f - E_v
	}

	if(beta <= 0.0)	//zero temp
	{
		testTot = 0.0;
		unsigned int j=0;
		unsigned int coord=0;
		double fractional = 0.0;
		for(; j<sz; ++j)
		{
			difference = totOcc - testTot;
			if(difference > num[j])
			{
				occ[j] = num[j];
				testTot += occ[j];
			}
			else
			{
				occ[j] = difference;
				testTot = totOcc;
				fractional = occ[j]/num[j];
				coord = j;
				++j;
				fractional = fractional*(eLevs[j]->max - eLevs[j]->min);
				testFermi = eLevs[j]->min + fractional;
				break;
			}
		}	//end for loop

		for(;j<sz;++j)	//make sure the occupancy goes to zero for all these
			occ[j] = 0.0;
		
	}
	else //not zero temp
	{
		totDeriv = 0.0;
		difference = 1.0;
		bool avg = true;
		bool first = true;
		bool testedMax = false;
		bool testedMin = false;
		double ratio;
		while(difference != 0.0)	//make sure we don't create/destroy any charge
		{
			if(!first)
			{
				if(totDeriv == 0.0 || avg)	//there is no good derivative data
					testFermi = 0.5*(minFermi + maxFermi);
				else if(band == CONDUCTION)
					testFermi -= difference / totDeriv;	//this is only any good if it's really close. 
				else
					testFermi += difference / totDeriv;	//the derivative is the opposite for the VB
			}
			first = false;
			if(testFermi > maxFermi || testFermi < minFermi || (testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin))
				testFermi = 0.5*(minFermi + maxFermi);

			testTot = totDeriv = 0.0;
			double tmp;
			for(unsigned int j=0; j<sz; ++j)	//calculate the new occupancy and the derivative for test Ef.
			{
				if(totOcc <= totAvail)
				{
					tmp = exp(beta * (energies[j] - testFermi));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					occ[j] = deriv[j] * num[j];
					deriv[j] = (tmp * occ[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += occ[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
				else
				{
					tmp = exp(-beta * ( testFermi-energies[j] ));
					if(MY_IS_FINITE(tmp) == false)
					{
						deriv[j] = 0.0;
						tmp = 0.0;
					}
					else
					{
						deriv[j] = 1.0 + tmp;
						deriv[j] = 1.0 / deriv[j];	//currently holds fermi function
					}
					avail[j] = deriv[j] * num[j];
					deriv[j] = (tmp * avail[j]) * deriv[j] * beta;	//now holds the actual derivative - shift the order so tmp&occ cancel when tmp is big
					testTot += avail[j];
					totDeriv += deriv[j];	//still want this to go all the way, so don't stop short
				}
			}
			if(totOcc <= totAvail)
			{
				difference = DoubleSubtract(testTot, totOcc);
				ratio = testTot / totOcc;
			}
			else
			{
				difference = DoubleSubtract(testTot, totAvail);
				ratio = testTot / totAvail;
			}

			avg = (ratio > 10 || ratio < 0.1) ? true : false;	//help it converge faster - the derivative is REALLY slow

			if((difference > 0.0 && minFermi < testFermi && totOcc <= totAvail) || 
				(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))	//then there were too many carriers, so set a new minimum - note this is the positive distance from the band into the gap
			{
				minFermi = testFermi;	//so it says the fermi level is further into the band gap and lowering the total number of carriers
				testedMin = true;
			}
			else if((difference < 0.0 && maxFermi > testFermi && totOcc <= totAvail) ||
				(difference > 0.0 && maxFermi > testFermi && totOcc > totAvail))
			{
				maxFermi = testFermi;
				testedMax = true;
			}
			else if(difference != 0.0 && ((testFermi == maxFermi && testedMax) || (testFermi == minFermi && testedMin)))
				break;	//it is doing the same value because it is as close as it's going to get to an answer

			if(minFermi == maxFermi)
			{
				if(ratio > 1.01 || ratio <0.99)
				{
					if(totOcc <= totAvail)
						ProgressDouble(44, totOcc);
					else
						ProgressDouble(44, totAvail);

					ProgressDouble(45, testTot);

				}
				break;
			}
		}	
	}

	//now load the data up
	if(band == CONDUCTION)
	{
		testFermi = cb.bandmin + testFermi;	//go back to absolute value instead of relative
		unsigned int i=0;
		if(totOcc <= totAvail)
		{
			for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it!=cb.energies.end(); ++it)
			{
				it->second.SetBeginOccStates(occ[i]);
				it->second.SetEndOccStates(occ[i]);	//end was copied to begin, and it assumes end/begin are the same
				it->second.AbsFermiEnergy = testFermi;
				++i;
			}
		}
		else
		{
			for(std::multimap<MaxMin<double>,ELevel>::iterator it = cb.energies.begin(); it!=cb.energies.end(); ++it)
			{
				it->second.SetBeginOccStatesOpposite(avail[i]);
				it->second.SetEndOccStatesOpposite(avail[i]);	//end was copied to begin, and it assumes end/begin are the same
				it->second.AbsFermiEnergy = testFermi;
				++i;
			}
		}

	}
	else
	{
		testFermi = vb.bandmin - testFermi;
		unsigned int i=0;
		if(totOcc <= totAvail)
		{
			for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it!=vb.energies.end(); ++it)
			{
				it->second.SetBeginOccStates(occ[i]);
				it->second.SetEndOccStates(occ[i]);
				it->second.AbsFermiEnergy = testFermi;
				++i;
			}
		}
		else
		{
			for(std::multimap<MaxMin<double>,ELevel>::iterator it = vb.energies.begin(); it!=vb.energies.end(); ++it)
			{
				it->second.SetBeginOccStatesOpposite(avail[i]);
				it->second.SetEndOccStatesOpposite(avail[i]);
				it->second.AbsFermiEnergy = testFermi;
				++i;
			}
		}
	}
	
	//clear out any data
	delete [] deriv;
	delete [] occ;
	delete [] energies;
	delete [] num;
	delete [] avail;
	eLevs.clear();

	return(0);
}


ELevel::ELevel(Point& point, unsigned long long int ID) : pt(point)
{
	uniqueID = ID;
	BegoccStatesisCarrierType = true;
	EndoccStatesisCarrierType = true;
	min = 0;
	max = 0;
	numstates = 0;
	endoccstates = 0;
	beginoccstates = 0;
//	RateLimit = 0.0;
	netRate = 0.0;
	netRateM1 = 0.0;
	netRateM2 = 0.0;
	partialFlippedSignMax = partialFlippedSignFall = false;
	partialNRate = partialNRateM1 = 0.0;
//	occM1 = 0.0;
//	occM2 = 0.0;
//	minNonZeroAbsRate = 0.0;
//	maxAbsRate = 0.0;
	relFermi = 0.0;
//	EminEfM1 = 0.0;
//	EminEfM2 = 0.0;
	efm = 0;
	tau = 0;
	taui = 0.0;
	velocity = 0.0;
	AbsFermiEnergy = 0.0;
	charge = 0;
	imp = NULL;
	bandtail = false;
	fermi = 0.0;
	boltzmax = 0.0;
	SRHConstant.clear();
	TargetELev.clear();
	TargetELevDD.clear();
	Rates.clear();
	OpticalRates.clear();
//	allowTransferReduction = false;
	RateSums.Clear();
	prevAdjust = true;
//	prevChange = 0.0;
	
}

ELevel::ELevel(Point& point) : pt(point)
{
	uniqueID = 0;
	BegoccStatesisCarrierType = true;
	EndoccStatesisCarrierType = true;
	min = 0;
	max = 0;
	numstates = 0;
	endoccstates = 0;
	beginoccstates = 0;
//	RateLimit = 0.0;
	netRate = 0.0;
	netRateM1 = 0.0;
	netRateM2 = 0.0;
//	tStepCtrl = NULL;	//this isn't really used, but it's the easiest way to get stuff working.
	opWeights = NULL;
//	occM1 = 0.0;
//	occM2 = 0.0;
//	minNonZeroAbsRate = 0.0;
//	maxAbsRate = 0.0;
	relFermi = 0.0;
	partialFlippedSignMax = partialFlippedSignFall = false;
	partialNRate = partialNRateM1 = 0.0;
//	EminEfM1 = 0.0;
//	EminEfM2 = 0.0;
	efm = 0;
	tau = 0;
	taui = 0.0;
	velocity = 0.0;
	AbsFermiEnergy = 0.0;
	charge = 0;
	imp = NULL;
	fermi = 0.0;
	boltzmax = 0.0;
	SRHConstant.clear();
	TargetELev.clear();
	TargetELevDD.clear();
	Rates.clear();
	OpticalRates.clear();
//	allowTransferReduction = false;
	RateSums.Clear();
	prevAdjust = true;
//	prevChange = 0.0;
//	maxREfIn = 0.0;
//	maxREfOut = 0.0;
	carrierspectrumcutoff = 0.0;
	ExcessiveNetPRates.clear();
	ExcessiveNetNRates.clear();
	NetRateN.clear();
	NetRateP.clear();
	ssLink = -1;
	SSTargetCleared = 0;
	densityMax = densityMin = densityUse = 0.0;

}

ELevel::~ELevel()
{
	min = 0;
	max = 0;
	numstates = 0;
	endoccstates = 0;
	beginoccstates = 0;
	
	AbsFermiEnergy = 0.0;
	efm = 0;
	tau = 0;
	taui = 0.0;
	velocity = 0.0;
	charge = 0;
	fermi = 0.0;
	SRHConstant.clear();
	
	boltzmax = 0.0;

//	delete tStepCtrl;
	delete opWeights;
	
	TargetELev.clear();
	TargetELevDD.clear();
	NeighborEquivs.clear();
	Rates.clear();
	OpticalRates.clear();
	RateSums.Clear();
	ExcessiveNetPRates.clear();
	ExcessiveNetNRates.clear();
	NetRateN.clear();
	NetRateP.clear();
	ssLink = -1;
}

TargetE::TargetE()
{
	target = NULL;
	percentOverlap = 0.0;
	percentScatter = 0.0;
}
TargetE::~TargetE()
{
	target = NULL;
	percentOverlap = 0.0;
	percentScatter = 0.0;
}

std::vector<ELevelRate>::iterator ELevel::AddELevelRate(ELevelRate& rate, bool optical)
{
	double nRate = DoubleSubtract(rate.RateIn, rate.RateOut);
	if(MY_IS_FINITE(rate.RateIn)==false || MY_IS_FINITE(rate.RateOut)==false)
		ProgressInt(67, rate.type);
	
	
	if (rate.source != this)	//bug if this is true
		return(Rates.end());

	TimeStepControl* tsc = nullptr;
	if (getSim() && getSim()->TransientData)
		tsc = &getSim()->TransientData->tStepControl;
	else if (rate.target && rate.target->getSim() && rate.target->getSim()->TransientData)
		tsc = &rate.target->getSim()->TransientData->tStepControl;
	bool canAdd = true;
	if (GetContact() && (GetContact()->GetCurrentActive() & CONTACT_SINK) == CONTACT_SINK)
		canAdd = false;

	if (rate.IncludeRate(RATE_OCC_BEGIN) == false) {
		unusedRates.AddValue(DoubleSubtract(rate.RateIn, rate.RateOut));	//want to keep a record of the total value
		if (tsc && canAdd) 
			tsc->AppendState(TimeStepControl::RatesOmitted);	//serves as a flag that the time step can keep going down
	}
	else {
		RateSums.AddValue(DoubleSubtract(rate.RateIn, rate.RateOut));
		if (tsc && canAdd)
			tsc->AppendState(TimeStepControl::RatesAdded);	//serves as flag letting us know the time step isn't so large that we probe nothing
	}

	rate.RecordRate();	//this puts it in the point main values.

	if(optical)
	{
		for(std::vector<ELevelRate>::iterator it=OpticalRates.begin(); it!=OpticalRates.end(); ++it)
		{
			if(rate.target = it->target)	//they have to be the same source. If they're the same target, then the rate must be the same!
			{
				*it += rate;
				return(it);
			}
		}
		OpticalRates.push_back(rate);
		return(--OpticalRates.end());
	}
	else
	{
		Rates.push_back(rate);	//puts a copy of the element on back
/*
THIS IS BAD!!!!!! These pointers end up being invalid. Very invalid.
		if(fabs(nRate) * 2.0 > rate.RateIn + rate.RateOut)
		{
			if(nRate > 0.0)
			{
				ExcessiveNetPRates.push_back(&(Rates.back()));
				NetRateP.push_back(nRate);
			}
			else
			{
				ExcessiveNetNRates.push_back(&(Rates.back()));
				NetRateN.push_back(nRate);
			}
		}
*/
		return(--Rates.end());
	}
	
	
	
}
/*
std::vector<ELevelOpticalRate>::iterator ELevel::AddELevelRate(ELevelOpticalRate& rate)
{
	RateSums.AddValue(DoubleSubtract(rate.RateIn, rate.RateOut));
	bool insert = true;
	for(std::vector<ELevelOpticalRate>::iterator it=OpticalRates.begin(); it!=OpticalRates.end(); ++it)
	{
		if(rate.target = it->target)	//they have to be the same source. If they're the same target, then the rate must be the same!
		{
			*it += rate;
			return(it);
		}
	}
	OpticalRates.push_back(rate);
	//now add just this particular rate to the RateSums. Not the sum of all of them
	

	return(--OpticalRates.end());
}
*/
double ELevel::CalcProbFromFermi(double fermi, bool relative)
{
	double delFermi;
	if(!relative)
	{
		double E = CalcAbsEUseEnergy();
		delFermi = E-fermi;
		
		if(carriertype == HOLE)	//1 - f(E)
			delFermi = -delFermi;
	}
	else
		delFermi = fermi;

	double temp = pt.temp;
	double prob = 0.0;

	if(temp != 0.0)
	{
		double kTi = KBI / temp;
		prob = delFermi * kTi;	// (E-Ef)/kT
		if(imp)
		{
			if(carriertype == ELECTRON)	//main carrier type = electron
				prob = imp->getDegeneracy() * exp(prob) + 1.0;
			else //main carrier type = hole
				prob = exp(prob) / imp->getDegeneracy() + 1.0;
		}
		else
			prob = exp(prob) + 1.0;	// exp((E-Ef)/kT)+1
		prob = 1.0/prob;	// 1.0 / [ e^((E-Ef)/kT) + 1]
	}
	else	//temperature is zero...
	{
		if(delFermi > 0.0)	//positive exponent in denominator
			prob = 0.0;
		else if (delFermi < 0.0)
			prob = 1.0;	//1.0 / (0+1)
		else
		{
			if(imp)
			{
				if(carriertype == ELECTRON)	//the main carrier type is electron
					prob = 1.0 / (1.0 + imp->getDegeneracy());	//by definition, the probability is one half at E=Ef.
				else
					prob = imp->getDegeneracy() / (1.0 + imp->getDegeneracy());	//by definition, the probability is one half at E=Ef.
			}
			else
				prob = 0.5;
		}
	}
	return(prob);
}

double ELevel::CalcChangeFromAdjustRelFermi(double adjRelEf)
{
	//essentially calculating N(f(E)_new - f(E)_old) where the fermi level changes
	//going from the relative Ef convention where >0 = more carriers
	//it's been rearranged/simplified to minimize the number of exponentials and divisions (SLOW)
	if(adjRelEf == 0.0)
		return(0.0);

	double relEf;
	if(updateRelFermi)
		relEf = relFermi;
	else
		relEf = CalcRelFermiEnergy(RATE_OCC_BEGIN);

	double degen = imp ? imp->getDegeneracy() : 1.0;
	double beta = KBI / pt.temp;
	double orig = exp(-beta * relEf);
	double adj = exp(beta * adjRelEf);

	if(degen != 1.0)
	{
		double num = numstates * orig * degen * (adj - 1.0);
		double den = orig * adj;
		den = (orig + degen*adj)*(orig+degen);
		return(num/den);
	}
	else //degen=1
	{
		double num = numstates * orig * (adj-1.0);
		double den = orig * (orig + 2.0 * adj) + adj;
		return(num/den);
	}
}

double ELevel::CalcDesiredTStep(int curSSTarget)
{
	double desiredTStep = INFINITETSTEP;
	double occ,avail;
	GetOccupancyOcc(RATE_OCC_BEGIN, occ, avail);
	
	if(netRate==0.0 ||	//it's not changing
		(occ < tOccMax && occ > tOccMin && avail < tAvailMax && avail > tAvailMin) ||	//it's within the bounds
		(occ == tOccMax && netRate < 0.0) ||	//it's at the bounds and moving within the bounds
		(occ == tOccMin && netRate > 0.0) ||
		(avail == tAvailMax && netRate > 0.0) ||
		(avail == tAvailMin && netRate < 0.0)
		)
		return(INFINITETSTEP);	//it's in the target zone (or SS), so don't restrict the time step

	double change = 0.0;
	double curRelEf = relFermi;
	double absEUse = CalcAbsEUseEnergy();
	double maxRelEf = RelEfFromOcc(maxOcc, minAvail, max - min, KB*pt.temp, imp ? imp->getDegeneracy() : 1.0f);
	double minRelEf = RelEfFromOcc(minOcc, maxAvail, max - min, KB*pt.temp, imp ? imp->getDegeneracy() : 1.0f);
	
	double trgRelEf;
	if(unsigned int(curSSTarget) < TargetEfs.size())
		trgRelEf = TargetEfs[curSSTarget];
	else
		trgRelEf = RelEfFromOcc(targetOcc, targetAvail, max - min, KB*pt.temp, imp ? imp->getDegeneracy() : 1.0f);

	if(netRate > 0.0)	//trying to increase the occupancy
	{
		if(occ <= avail)
		{
			if(tOccMin >= occ && occ >= maxOcc)	//tfor some reason, it is trying to add more carriers
			{  //than the theoretical steady state, and there is not much around to kick it up
				
				//more carriers are being added. Going to allow a change by
				//increasing the relative Ef (>0 = more occ) by 0.001eV
				if(trgRelEf - curRelEf < 0.001)
					change = targetOcc - occ;
				else
					change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else if(tOccMin >= maxOcc && maxOcc >= occ)
			{
				//it is inside the bounds, but the target is outside the bounds
				//don't let it change to an occupancy that would exceed these bounds
				//at that case, it is stepping an unphysical amount
				change = (maxOcc - occ);	//it only goes 1/4 of the way up
				if(maxRelEf > curRelEf + 1.0e-4)	//it is far away
					change = change *0.25;
			}
			else if(maxOcc >= tOccMin && tOccMin >= occ)
			{
				//it is moving towards the solution zone
				change = targetOcc - occ;
				if(trgRelEf - curRelEf > 1.0e-4)	//it's farish away, so don't jump to solution
					change = change * 0.25;
			}
			else if(maxOcc >= occ && occ >= tOccMax)
			{
				//it is moving towards the maximum occupancy, but that is
				//also moving away from the implied destination
				change = CalcChangeFromAdjustRelFermi(0.001);
				if(change > 0.25*(maxOcc - occ))
					change = 0.25*(maxOcc - occ);
			}
			else if(occ >= maxOcc && maxOcc >= tOccMax)
			{
				//it is on the upper boundary, moving away inexplicably as no forces should be
				//pushing it, and diverging from the intended target. Adjust fermi small amount
				change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else if(occ >= tOccMax && tOccMax >= maxOcc)
			{
				//it is above the upper boundary, and moving away for no reason
				change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else
			{
				change = 0;
				ProgressInt(74,0);
				return(INFINITETSTEP);
				//ERROR.
			}
		}
		else //this is just the same thing as above, except occ->avail, Min<->max, and switch < and >
		{
			if(tAvailMax <= avail && avail <= minAvail)	//tfor some reason, it is trying to add more carriers
			{  //than the theoretical steady state, and there is not much around to kick it up
				
				//more carriers are being added. Going to allow a change by
				//increasing the relative Ef (>0 = more occ) by 0.001eV
				if(trgRelEf - curRelEf < 0.001)
					change = -(targetAvail - avail);
				else
					change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else if(tAvailMax <= minAvail && minAvail <= avail)
			{
				//it is inside the bounds, but the target is outside the bounds
				//don't let it change to an occupancy that would exceed these bounds
				//at that case, it is stepping an unphysical amount
				change = (avail - minAvail);	//it only goes 1/4 of the way up
				if(maxRelEf > curRelEf + 1.0e-4)	//it is far away
					change = change *0.25;
			}
			else if(minAvail <= tAvailMax && tAvailMax <= avail)
			{
				//it is moving towards the solution zone
				change = avail - targetAvail;
				if(trgRelEf - curRelEf > 1.0e-4)	//it's farish away, so don't jump to solution
					change = change * 0.25;
			}
			else if(minAvail <= avail && avail <= tAvailMin)
			{
				//it is moving towards the maximum occupancy, but that is
				//also moving away from the implied destination
				change = CalcChangeFromAdjustRelFermi(0.001);
				if(change > 0.25*(avail - minAvail))
					change = 0.25*(avail - minAvail);
			}
			else if(avail <= minAvail && minAvail <= tAvailMin)
			{
				//it is on the upper boundary, moving away inexplicably as no forces should be
				//pushing it, and diverging from the intended target. Adjust fermi small amount
				change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else if(avail <= tAvailMin && tAvailMin <= minAvail)
			{
				//it is above the upper boundary, and moving away for no reason
				change = CalcChangeFromAdjustRelFermi(0.001);
			}
			else
			{
				change = 0;
				ProgressInt(74,0);
				return(INFINITETSTEP);
				//ERROR.
			}
		}
	}
	else if(netRate < 0.0)	//trying to decrease the occupancy
	{
		if(occ <= avail)
		{
			if(tOccMax <= occ && occ <= minOcc)	
			{  //it is at the edge and trying to approach the target
				change = targetOcc - occ;	//should be negative!
				if(curRelEf - trgRelEf > 1.0e-4)	//if it's far away, only change a portion
					change = change * 0.25;	//instead of all the way
			}
			else if(tOccMax <= minOcc && minOcc <= occ)
			{
				//this is within the bounds, but the bounds don't seem to include the target yet
				//try to approach the minimum boundary
				change = (minOcc - occ);	//it only goes 1/4 of the way up
				if(curRelEf - minRelEf > 1.0e-4)	//it is far away
					change = change *0.25;	//so only go a portion of the distance
			}
			else if(minOcc <= tOccMin && tOccMax <= occ)
			{
				//both the occupancy and the target are within the bounds - try to get to the target
				//it is moving towards the solution zone
				change = targetOcc - occ;
				if(curRelEf - trgRelEf > 1.0e-4)	//it's farish away, so don't jump to solution
					change = change * 0.25;
			}
			else if(minOcc <= occ && occ <= tOccMin)
			{
				//it is below the target, but in the bounds. Only go down a smidge,
				//and make sure not to go at least a quarter of the way to the boundary
				change = CalcChangeFromAdjustRelFermi(-0.001);
				if(-change > 0.25*(occ - minOcc))
					change = 0.25*(minOcc - occ);
			}
			else if(occ <= minOcc && minOcc <= tOccMin)
			{
				//it is at the lower boundary, going down, and the target is above
				//not sure why, but just adjust it by a small amount because we don't know what's going on!
				change = CalcChangeFromAdjustRelFermi(-0.001);
			}
			else if(occ <= tOccMin && tOccMin <= minOcc)
			{
				//the target is above it and it is at the boundary going in the wrong direction
				//let it go the wrong direction a small amount
				change = CalcChangeFromAdjustRelFermi(-0.001);
			}
			else
			{
				change = 0;
				ProgressInt(74,0);
				return(INFINITETSTEP);
				//ERROR.
			}
		}
		else //this is just the same thing as above, except occ->avail, Min<->max, and switch < and >
		{
			if(tOccMin >= avail && avail >= maxAvail)	
			{  //it is at the edge and trying to approach the target
				change = avail - targetAvail;	//should be negative!
				if(curRelEf - trgRelEf > 1.0e-4)	//if it's far away, only change a portion
					change = change * 0.25;	//instead of all the way
			}
			else if(tOccMin >= maxAvail && maxAvail >= avail)
			{
				//this is within the bounds, but the bounds don't seem to include the target yet
				//try to approach the minimum boundary
				change = (avail - maxAvail);	//it only goes 1/4 of the way up
				if(curRelEf - minRelEf > 1.0e-4)	//it is far away
					change = change *0.25;	//so only go a portion of the distance
			}
			else if(maxAvail >= tAvailMin && tAvailMin >= avail)
			{
				//both the occupancy and the target are within the bounds - try to get to the target
				//it is moving towards the solution zone
				change = avail - targetAvail;
				if(curRelEf - trgRelEf > 1.0e-4)	//it's farish away, so don't jump to solution
					change = change * 0.25;
			}
			else if(maxAvail >= avail && avail >= tAvailMax)
			{
				//it is below the target, but in the bounds. Only go down a smidge,
				//and make sure not to go at least a quarter of the way to the boundary
				change = CalcChangeFromAdjustRelFermi(-0.001);
				if(-change > 0.25*(maxAvail - avail))
					change = 0.25*(avail - maxAvail);
			}
			else if(avail >= maxAvail && maxAvail >= tAvailMax)
			{
				//it is at the lower boundary, going down, and the target is above
				//not sure why, but just adjust it by a small amount because we don't know what's going on!
				change = CalcChangeFromAdjustRelFermi(-0.001);
			}
			else if(avail >= tAvailMax && tAvailMax >= maxAvail)
			{
				//the target is above it and it is at the boundary going in the wrong direction
				//let it go the wrong direction a small amount
				change = CalcChangeFromAdjustRelFermi(-0.001);
			}
			else
			{
				change = 0;
				ProgressInt(74,0);
				return(INFINITETSTEP);
				//ERROR.
			}
		}
	}

	desiredTStep = change / netRate;
	if(desiredTStep < 0.0)
		ProgressInt(75,0);

	return(desiredTStep);

	/*
	bool properDirection = (netRate>0.0 && (occ <= targetOcc && avail >= targetAvail)) ||
		(netRate<0.0 && (occ >= targetOcc && avail <= targetAvail));
	double TOcc = targetOcc;
	double TAvail = targetAvail;
	//this needs a new method.
	double mxOcc = maxOcc;
	double mxAvail = numstates - mxOcc;
	double mnOcc = minOcc;
	double mnAvail = numstates - mnOcc;
	
	if(BegoccStatesisCarrierType==false)
	{
		//maxOcc represents the lower number (more accuracy)
		//if the carrier type doesn't match, then it was actually storing avail
		//so swap the occ/avail
		double tmp = mxOcc;
		mxOcc = mxAvail;
		mxAvail = tmp;
		tmp = mnOcc;
		mnOcc = mnAvail;
		mnAvail = tmp;
	}

	//basically, we don't want the timestep to cause it to transfer more
	//carriers than it should. As a guarantee, the fermi level after the timestep
	//should be entirely contained within all fermi levels of states it interacts with.
	//the (mn/mx)(Occ/Avail) represent these limits.
	//however, funny things happen when dealing with outside influences, such
	//as light or contacts.

	//the contacts may be figured out relatively speaking as it may be
	//determined by drift-diffusion and the point properties.
	//but the light is tricky, it causes all the fermi levels to split and
	//is a driving force for that. In this case, the change should
	//be caught by the TOcc and TAvail, as those are the steady state values.

	if(!properDirection)
	{
		//look for all the old time steps
		bool found=false;
		if(unsigned int(curSSTarget-1) < TargetEfs.size())	//could it still be approaching a previous steady state value?
		{
			double relEf = CalcRelFermiEnergy(RATE_OCC_BEGIN);	//large occupancy = positive Ef

			//looking for the largest relative Ef
			double maxMagEf = relEf;	//start at the current relative Ef
			if(netRate > 0.0)	//look for the highest rel Ef
			{
				for(int testT = curSSTarget-1; testT > SSTargetCleared; --testT)
				{
					if(TargetEfs[testT] > maxMagEf)
						maxMagEf = TargetEfs[testT];
				}
			}
			else if(netRate < 0.0) //look for the lowest rel Ef.
			{
				for(int testT = curSSTarget-1; testT > SSTargetCleared; --testT)
				{
					if(TargetEfs[testT] < maxMagEf)
						maxMagEf = TargetEfs[testT];
				}
			}

			if(maxMagEf != relEf)	//it found a new possible target state that is consistent
			{
				SetOccFromRelEf(RATE_OCC_TARGET, maxMagEf);
				if(netRate > 0.0)
				{
					if(occ < targetOcc || avail > targetAvail)
						found = true;
				}
				else
				{
					if(occ > targetOcc || avail < targetAvail)
						found = true;
				}

				//restore the target occs from above
				double tmp = targetOcc;
				targetOcc = TOcc;	//restore the old target occ
				TOcc = tmp;	//put our new TOcc in

				tmp = targetAvail;
				targetAvail = TAvail;
				TAvail = tmp;
			}
		}

		properDirection = found;
	}

	double occUse, targUse, denomUse, dif;
	if(occ <= avail)
	{
		occUse = occ;
		targUse = TOcc;
	}
	else
	{
		occUse = avail;
		targUse = TAvail;
	}
	double percentChange = 1.0;
	if(properDirection)
	{
		
		

		if(occUse == 0.0 || targUse == 0.0)
		{
			denomUse = GetNumstates();
		}
		else 
		{
			denomUse = (occUse>targUse) ? occUse : targUse;
		}

		if(targUse == 0.0)
		{
			if(occUse < denomUse*1.0e-20)
			{
				dif = 0.0;
			}
			else
				dif = fabs(occUse - targUse);
		}
		else
		{
			dif = fabs(occUse - targUse);
		}

		percentChange = 1.0 - dif / denomUse;
		percentChange = percentChange * percentChange;

		if(percentChange <= 1.0e-10)	//dif cannot be 0.0 for this to be true.
		{
			percentChange = 0.1 * occUse / dif;
		}

	}

	if(percentChange < 1.0 && properDirection)
	{
		desiredTStep = fabs(dif / netRate * log(1.0 - percentChange));
	}
	else if(properDirection)
		desiredTStep = INFINITETSTEP;
	else
	{
		//a timestep to either adjust carrier concentration by 10% linearly or an abs. mag change of rho by 0.1.
		double rhoStep = fabs(pt.vol * QI / netRate * 0.1);
		double pStep = (occUse > 0.0) ? fabs(occUse * 0.1 / netRate) : fabs(GetNumstates() * 1e-15 / netRate);
		desiredTStep = (rhoStep < pStep) ? rhoStep : pStep;
	}

	return(desiredTStep);
	*/
}

void ELevel::UpdateAbsFermiEnergy(int typeocc)
{
	double delE = updateRelFermi ? relFermi : CalcRelFermiEnergy(typeocc);	//calculates E-Ef and stores it in difEf.
	//now convert to absolute fermi
	double AbsEUse = CalcAbsEUseEnergy();	//get eLev->eUse in the absolute scale - no shift due to charge changes
	AbsFermiEnergy = (carriertype==ELECTRON) ? AbsEUse + delE : AbsEUse - delE;
	//if electron, delE>0-->lots of electrons, higher fermi level (and large delE)
	//if hole, delE>0-->lots of holes, lower fermi level (but large delE)
	
	
	return;
}
/*
occ/total = f(E) = (1 + gamma * exp(B(E-Ef)))^-1
gamma * exp(B(E-Ef)) = tot/occ - 1
B(E-Ef) = ln[(tot/occ - 1)/gamma]
delE = E-Ef = kT * ln[(tot/occ - 1)/gamma]
Calc absolute E
Ef = E - delE
now returns delE such that low occ results in delE<0
*/

double ELevel::CalcRelFermiEnergy(double occ, double avail) {
	if (occ + avail != numstates) {
		if (avail < 0.0)
			avail = 0.0;
		if (occ < 0.0)
			occ = 0.0;
		if (occ > numstates)
			occ = numstates;
		if (avail > numstates)
			avail = numstates;

		if (avail < occ)	//avail is probably the more accurate
			occ = numstates - avail;
		else
			avail = numstates - occ;
	}

	double delE = 0.0;	//delE... E - Ef value
	double degen = (imp != NULL) ? imp->getDegeneracy() : 1.0;

	if (pt.temp == 0.0)
	{
		double eWidth = max - min;
		double percent = occ / (occ + avail);	//Ef is this percentage up from the bottom
		if (eWidth == 0.0)
			eWidth = 0.001;	//allow some variation...
		delE = eWidth * (percent - 0.5);
	}
	else if (occ > 0.0 && avail > 0.0)
	{
		double lnVal = degen * occ / avail;
		if (MY_IS_FINITE(lnVal) && lnVal != 0.0)
		{
			lnVal = log(lnVal);
			delE = KB * pt.temp * lnVal;
		}
		else
			delE = KB * pt.temp * (log(occ) - log(avail) + log(degen));	//same math, just separating everything out to prevent infinities

	}
	else if (occ == 0.0)
		delE = -KB * pt.temp * 800.0;	//800 ~ ln(10^350) which is to say the occupancy is about 350 orders magnitude smaller than the number of states
	else
		delE = KB * pt.temp * 800.0;

	return delE;

}
double ELevel::CalcRelFermiEnergy(int typeocc, bool update)
{
	double delE = 0.0;	//delE... E - Ef value
	double occ, avail;
	double num = GetNumstates();
	double degen = (imp != NULL) ? imp->getDegeneracy() : 1.0;
	includeFermiBelow = false;
	includeFermiAbove = false;

	GetOccupancyOcc(typeocc, occ, avail);
	delE = CalcRelFermiEnergy(occ, avail);
	
	if(update)
	{
		relFermi = delE;
		updateRelFermi = true;
	}

	return(delE);

//	if(carriertype == HOLE)
//		occ = num - occ;	//make sure we're counting number of electrons

	/*

	if(occ > avail && avail > 0.0)
	{
		delE = num / avail - 1.0;	//this line is NOT safe. If num is large, and occ is almost zero, delE can be infinite. This is a problem.
		if(imp != NULL)
		{
			if(carriertype==ELECTRON)
				delE = delE * imp->degeneracy;
			else
				delE = delE / imp->degeneracy;
		}

		if(MY_IS_FINITE(delE))	//safe math
			delE = -KB * pt.temp * log(delE);	// = E - Ef
		else if(imp != NULL)
		{
			if(carriertype==ELECTRON)
				delE = -KB * pt.temp * (log(num) - log(avail) + log(imp->degeneracy));	//same math, just separating everything out to prevent infinities
			else
				delE = -KB * pt.temp * (log(num) - log(avail) - log(imp->degeneracy));	//same math, just separating everything out to prevent infinities
		}
		else
			delE = -KB * pt.temp * (log(num) - log(avail));	//this is now Ef-E and we want E-Ef, so negatives
	}
	else if(occ <= avail && occ > 0.0)	//calculate it right...
	{
	
		delE = num / occ - 1.0;	//this line is NOT safe. If num is large, and occ is almost zero, delE can be infinite. This is a problem.
		if(imp != NULL)
		{
			if(carriertype==ELECTRON)
				delE = delE / imp->degeneracy;
			else
				delE = delE * imp->degeneracy;
		}

		if(MY_IS_FINITE(delE))	//safe math
			delE = KB * pt.temp * log(delE);	// = E - Ef
		else if(imp != NULL)
		{
			if(carriertype==ELECTRON)
				delE = KB * pt.temp * (log(num) - log(occ) - log(imp->degeneracy));	//same math, just separating everything out to prevent infinities
			else
				delE = KB * pt.temp * (log(num) - log(occ) + log(imp->degeneracy));	//same math, just separating everything out to prevent infinities
		}
		else
			delE = KB * pt.temp * (log(num) - log(occ));
	}
	else
	{
		if(occ == 0.0)
			delE = KB * pt.temp * 800.0;	//800 ~ ln(10^350) which is to say the occupancy is about 350 orders magnitude smaller than the number of states
		else if(avail == 0.0)
			delE = -KB * pt.temp * 800.0;	//800 ~ ln(10^350) which is to say the availability is about 350 orders magnitude smaller than the number of states
		//this is essentially going to result in a fermi level that will calculate the number of carriers to zero or maintain negligence
	}

	if(carriertype == HOLE)	//doing occ/total = 1-f(E), leaves an exponent in num that matches in den, so multiply by inverse means swapping sign on delE
		delE = -delE;
	*/


}



double ELevel::CalcAbsEUseEnergy(void)
{
	double AbsE;
	if(imp != NULL || bandtail)	//it's a band tail or an impurity
	{	
		//it is an impurity, so now we need to know if the energy level is in reference to the VB or the CB
		if(carriertype == HOLE)
		{	//it's in reference to the valence band, so start comparing...
			AbsE = pt.vb.bandmin + eUse;	//first get the absolute energies
		}
		else
		{	//say cb.bandmin = 0 as this is just an offset for high/low energy
			AbsE = pt.cb.bandmin - eUse;	//state.min = 0.1, high = -0.1

		}
	}
	else if(carriertype == ELECTRON)	//not an impurity, so it must be in the CB
	{
		AbsE = pt.cb.bandmin + eUse;

	}
	else	//not impurity/BT, not CB, so must be VB
	{
		AbsE = pt.vb.bandmin - eUse;

	}

	return(AbsE);
}

void ELevel::CalculateAbsoluteHighLowEnergy(double& highEnergy, double& lowEnergy, double& useEnergy)
{
	//first, check for impurity or band tail
	highEnergy = lowEnergy = useEnergy = 0.0;
	if(imp != NULL || bandtail)
	{	
		//it is an impurity, so now we need to know if the energy level is in reference to the VB or the CB
		if(carriertype == VALENCE)
		{	//it's in reference to the valence band, so start comparing...
			highEnergy = max + pt.vb.bandmin;	//first get the absolute energies
			lowEnergy = min + pt.vb.bandmin;
			double dif = max - useEnergy;	//min=0.1, max=0.5, eUse=0.2, dif=0.3
			useEnergy = highEnergy - dif;	//eUse=vb+0.2
		}
		else
		{	//say cb.bandmin = 0 as this is just an offset for high/low energy
			highEnergy = pt.cb.bandmin - min;	//state.min = 0.1, high = -0.1
			lowEnergy = pt.cb.bandmin - max;	//state.max = 0.5, low  = -0.5
			double dif = min - useEnergy;	//state.eUse=0.2, dif = -0.1
			useEnergy = highEnergy + dif;	//eUse = -0.2 as desired.
		}
	}
	else if(carriertype == ELECTRON)	//not an impurity, so it must be in the CB
	{
		highEnergy = pt.cb.bandmin + max;
		lowEnergy = pt.cb.bandmin + min;
		double dif = max - useEnergy;	//just like impurity refrenced to VB
		useEnergy = highEnergy - dif;
	}
	else	//not impurity, not CB, so must be VB
	{
		highEnergy = pt.vb.bandmin - min;
		lowEnergy = pt.vb.bandmin - max;
		double dif = min - useEnergy;	//just like impurity referenced to CB
		useEnergy = highEnergy + dif;
	}


	if(highEnergy < lowEnergy)	//just in case I screwed up somehow...
	{		//swap the two
		double temp = highEnergy;
		highEnergy = lowEnergy;
		lowEnergy = temp;
		ProgressInt(32, 0);	//notify in trace file that it screwed up somehow...
	}
	return;
}

void ELevel::CalculateAbsoluteHighLowEnergy(double& highEnergy, double& lowEnergy)
{
	//first, check for impurity or band tail
	highEnergy = lowEnergy = eUse;
	if (imp != NULL || bandtail)
	{
		//it is an impurity, so now we need to know if the energy level is in reference to the VB or the CB
		if (carriertype == VALENCE)
		{	//it's in reference to the valence band, so start comparing...
			highEnergy = max + pt.vb.bandmin;	//first get the absolute energies
			lowEnergy = min + pt.vb.bandmin;
		}
		else
		{	//say cb.bandmin = 0 as this is just an offset for high/low energy
			highEnergy = pt.cb.bandmin - min;	//state.min = 0.1, high = -0.1
			lowEnergy = pt.cb.bandmin - max;	//state.max = 0.5, low  = -0.5
		}
	}
	else if (carriertype == ELECTRON)	//not an impurity, so it must be in the CB
	{
		highEnergy = pt.cb.bandmin + max;
		lowEnergy = pt.cb.bandmin + min;
	}
	else	//not impurity, not CB, so must be VB
	{
		highEnergy = pt.vb.bandmin - min;
		lowEnergy = pt.vb.bandmin - max;
	}


	if (highEnergy < lowEnergy)	//just in case I screwed up somehow...
	{		//swap the two
		double temp = highEnergy;
		highEnergy = lowEnergy;
		lowEnergy = temp;
		ProgressInt(32, 0);	//notify in trace file that it screwed up somehow...
	}
	return;
}

//calculates the states energy relative to the valence band edge. Keeps numbers a little safer, and useful for optical transitions
void ELevel::CalculateRelativeHighLowEnergy(double& highEnergy, double& lowEnergy)
{
	
	highEnergy = lowEnergy = eUse;	//just some default values

	double eg = pt.eg;
	if (imp != NULL || bandtail)
	{
		//it is an impurity, so now we need to know if the energy level is in reference to the VB or the CB
		if (carriertype == VALENCE)
		{	//it's in reference to the valence band, so start comparing...
			highEnergy = max;	//first get the absolute energies
			lowEnergy = min;
		}
		else
		{	//say cb.bandmin = 0 as this is just an offset for high/low energy
			highEnergy = eg - min;	//state.min = 0.1, high = -0.1
			lowEnergy = eg - max;	//state.max = 0.5, low  = -0.5
		}
	}
	else if (carriertype == ELECTRON)	//not an impurity, so it must be in the CB
	{
		highEnergy = eg + max;
		lowEnergy = eg + min;
	}
	else	//not impurity, not CB, so must be VB
	{
		highEnergy = -min;
		lowEnergy = -max;
	}


	if (highEnergy < lowEnergy)	//just in case I screwed up somehow...
	{		//swap the two
		double temp = highEnergy;
		highEnergy = lowEnergy;
		lowEnergy = temp;
		ProgressInt(32, 0);	//notify in trace file that it screwed up somehow...
	}
	return;
}


int ELevel::TestTotalRate(int which) {
	Simulation* sim = getSim();
	if (sim == nullptr)
		return 1;	//default to OK.
	TransientSimData *tsd = sim->TransientData;
	if (tsd == nullptr)
		return 1;

	TimeStepControl& ts = tsd->tStepControl;	//just get here purrty quick
	double tmpPNR = RateSums.GetNetSum();

	if (ts.isDroppingStep()) {
		if (IS_OPPOSITE_SIGN(partialNRate, tmpPNR))
			partialFlippedSignFall = true;
	}

	partialNRate = tmpPNR;	//theoretically, this should only be the slower 'safe' rates that were included

	if (ts.IsHighestStep()) {
		if (IS_OPPOSITE_SIGN(partialNRate, partialNRateM1))
			partialFlippedSignMax = true;	//this basically is
		partialNRateM1 = partialNRate;	//it's been compared now. Don't need this. Save for the future max step
	}

	//individually, none of these would cause a change greater than the tolerance.
	netRate = partialNRate + unusedRates.GetNetSum();
	if (partialNRate == 0.0 || (GetContact() && (GetContact()->GetCurrentActive() & CONTACT_SINK) == CONTACT_SINK))
		return 1;	//it is definitely good.

	double deltaCar = partialNRate * ts.getTargetTStep();
	double occ, avail, Focc, Favail, FrelEf;
	GetOccupancyOcc(which, occ, avail);
	double relEf = CalcRelFermiEnergy(occ, avail);

	Focc = occ + deltaCar;
	Favail = avail - deltaCar;
	FrelEf = CalcRelFermiEnergy(Focc, Favail);

	double deltaEf = abs(DoubleSubtract(FrelEf, relEf));
	if (deltaEf == 0.0)	//just prevent some divide by zero's later.
		return 1;	//no change in fermi, it's good to progress.

	double percentDeltaEf = relEf != 0.0 ? abs(deltaEf / relEf) : abs(deltaEf / FrelEf);
	if (percentDeltaEf < deltaEf)	//uncommon, but possible
		deltaEf = percentDeltaEf;

	ts.UpdateUseTStep(deltaEf);


	return 1;
}

//net rate is concerned with what is entering the state of it's preferred carrier type
bool ELevel::AllowRate(double netRate, int which) {
	if (netRate == 0.0)
		return true;

	Simulation* sim = getSim();
	if (sim == nullptr)
		return true;	//default to OK.
	TransientSimData *tsd = sim->TransientData;
	if (tsd == nullptr)
		return true;

	TimeStepControl& ts = tsd->tStepControl;	//just get here purrty quick
	double tStep = ts.getTargetTStep();
	double tolerance = ts.getTolerance();

	double deltaCar = netRate * tStep;
	double occ, avail, Focc, Favail, FrelEf;
	GetOccupancyOcc(which, occ, avail);
	double relEf = CalcRelFermiEnergy(occ, avail);

	Focc = occ + deltaCar;
	Favail = avail - deltaCar;
	FrelEf = CalcRelFermiEnergy(Focc, Favail);

	double deltaEf = FrelEf - relEf;
	if (deltaEf == 0.0)	//just prevent some divide by zero's later.
		return true;

	
	/*
	the conditions for accepting rates in the slow-fast rate paradigm:
	The change in the fermi level must be less than the absolute value of the tolerance
		For sparse states, a small change in carriers may lead to massive swings in the femri level despite having really no effect
	Change in fermi level must be a percentage of the current one. For large relative fermi levels, this helps with the accuracy issue
	*/
	
	//generally the least strict
	if (abs(deltaEf) < tolerance)
		return true;

	//usually more strict than above, unless |relEf| > 1.0
	deltaEf = relEf != 0.0 ? abs(deltaEf / relEf) : abs(deltaEf / FrelEf);
	if (deltaEf < tolerance)
		return true;

	return false;
}

double ELevel::PercentOverlap(ELevel* trgE)
{
	double percent = 0.0;
	double absSrcMinE, absSrcMaxE, absTrgMinE, absTrgMaxE;
	CalculateAbsoluteHighLowEnergy(absSrcMaxE, absSrcMinE);
	trgE->CalculateAbsoluteHighLowEnergy(absTrgMaxE, absTrgMinE);

	double width = absSrcMaxE - absSrcMinE;
	if(width == 0.0)	//there is no width whatsoever.
	{
		if(absTrgMinE > absSrcMinE || absTrgMaxE < absSrcMinE)	//the minimum is over it, so no good. OR the maximum is under it, also no good
			return(0.0);

		//otherwise, 100% of the source is present in the target
		return(1.0);
	}

	double max = (absSrcMaxE < absTrgMaxE) ? absSrcMaxE : absTrgMaxE;	//choose the lowest max
	double min = (absSrcMinE > absTrgMinE) ? absSrcMinE : absTrgMinE;	//and the highest minimum
	double overlap = max - min;
	
	if(overlap < 0.0)
		percent = 0.0;
	else if(overlap > 0.0)
		percent = overlap / width;
	else if(overlap == 0.0)	//this implies that it is either just barely touching max to min, or the width of the target is zero and within the band edge
	{
		if(absTrgMaxE == absTrgMinE)
			percent = 1e-8;	//just a pretty small number. No physics went in to choosing this number
		else
			percent = 0.0;	//don't count touching as overlap
	}


	if(percent > 1.0)
		percent = 1.0;
	else if(percent < 0.0)
		percent = 0.0;


	return(percent);
}

Band::Band()	//always just zero everything
{
	type=false;
	bandmin = 0;
	numparticles = 0.0;
	partitioni = 0.0;
	totalstates = 0.0;
	energies.clear();
	EffectiveDensity = 0.0;
}
Band::~Band()	//always just zero everything
{
	type=false;
	bandmin = 0;
	numparticles = 0;
	totalstates= 0.0;
	partitioni = 0.0;
	energies.clear();
	EffectiveDensity = 0.0;
}


Impurity::Impurity(int id, std::string nm, Material* defectMat, double e, double eVar, 
	double dens, bool dep, bool ref, short chrg, int dist, double sign, double sigp,
	float degen, double optEffCB, double optEffVB)
{
	//error check the inputs by using the assignment stuff
	setName(nm);
	setID(id);

	setRelativeEnergy(e);
	setEnergyVariation(eVar);
	setDensity(dens);
	setDepth(dep);
	setReference(ref);
	setCharge(chrg);
	if (setEnergyDistribution(dist) == false)
		dist = discrete;
	setElecCapture(sign);
	setHoleCapture(sigp);
	
	if (degen > 0.0f)
		setDegeneracy(degen);
	else
		setDefaultDegeneracy();

	setOpticalEfficiencyCB(optEffCB);
	setOpticalEfficiencyVB(optEffVB);
	setDefectMaterial(defectMat);

}
Impurity::~Impurity()
{
	defectMaterial = nullptr;
}
void Impurity::testDefaultCharge() {
	if (reference == donor) {
		if (charge < 1)
			charge = 1;
	}
	else if (charge > -1) //it must be an acceptor, which should have negative charges to cancel additional holes
		charge = -1;
}


Layer::Layer()
{

	name="";
	spacingboundary = 0.0;
	spacingcenter = 0.0;
	id = 0;
	AffectMesh = true;	//default to say this layer affects the mesh
	exclusive = false;	//default to say this layer can overlap with other layers.
	OutputLightIncident.clear();
	OutputLightCarrierGen.clear();
	shape = NULL;
	priority = false;
	sizeScale = 1.0;
	impurities.clear();
	matls.clear();
	out.AllRates = false;
	out.OpticalPointEmissions = false;
}

Layer::Layer(const Layer &lyr) {
	name = lyr.name;
	spacingboundary = lyr.spacingboundary;
	spacingcenter = lyr.spacingcenter;
	id = lyr.id;
	AffectMesh = lyr.AffectMesh;	//default to say this layer affects the mesh
	exclusive = lyr.exclusive;	//default to say this layer can overlap with other layers.
	OutputLightIncident = lyr.OutputLightIncident;
	OutputLightCarrierGen = lyr.OutputLightIncident;
	
	priority = lyr.priority;
	sizeScale = lyr.sizeScale;
	
	out.AllRates = lyr.out.AllRates;
	out.OpticalPointEmissions = lyr.out.OpticalPointEmissions;

	if (lyr.shape) {
		shape = new Shape();
		shape->type = lyr.shape->type;
		int npoints = lyr.shape->getNumVertices();
		for (int i = 0; i < npoints; ++i) 
			shape->AddPoint(lyr.shape->getPoint(i));
	}
	else
		shape = nullptr;

	for (unsigned int i = 0; i < lyr.impurities.size(); ++i) 
		impurities.push_back(new ldescriptor<Impurity*>(*lyr.impurities[i]));
	
	for (unsigned int i = 0; i < lyr.matls.size(); ++i)
		matls.push_back(new ldescriptor<Material*>(*lyr.matls[i]));
	
}
Layer::~Layer()
{

	name="";
	spacingboundary = 0.0;
	spacingcenter = 0.0;
	id = 0;
	exclusive = true;
	AffectMesh = true;
	OutputLightIncident.clear();
	OutputLightCarrierGen.clear();

	delete shape;	//will call ~Shape(), which will free memory
	shape = NULL;
	impurities.clear();
	matls.clear();
}
Simulation::Simulation()
{
	maxOpticalSize = -1;	//it hasn't been set!
	timeSaveState = 0.0;
	curiter = 0;
	binctr = 0;
	maxiter = 0;
	contacts.clear();
	TransientData = nullptr;
	SteadyStateData = nullptr;
//	EnabledRates.SetDefaultValues();
	maxLightReflections = 30;
	mdesc = nullptr;

	T = 300.0;
	lightincidentdir = 0;
	statedivisions = 10;
	accuracy = 1.0e-6;
		SignificantMilestone = false;
	
	OutputFName = "defaultOut";
	mdlData = nullptr;
	ThEqDeviceStart = true;
	ThEqPtStart = true;
	
	//carrierTaskList is automatically set by its constructor
	misc.largestMatNameSize = 0;
	holecurrent.clear();
	ecurrent.clear();
	_OpticalFluxOut = nullptr;
	_OpticalAggregateOut = nullptr;
	ContactPoisson.clear();
	simType = SIMTYPE_UNKNOWN;
	
	NoSS.clear();
}
Simulation::~Simulation()
{
	curiter = 0;
	
	T = 0;
	mdesc = NULL;
	lightincidentdir = 0;
	statedivisions = 0;
	accuracy=0.0;


	unsigned int size = contacts.size();
	for(unsigned int it = 0; it < size; it++)
		delete contacts[it];	//the vector contains pointers, so delete each one of them

	delete TransientData;
	delete SteadyStateData;
	
	mdlData = NULL;	//this is just a pointer, don't delete this!

	contacts.clear();	//now free up this memory in the model

	holecurrent.clear();
	ecurrent.clear();
	delete _OpticalFluxOut;
	delete _OpticalAggregateOut;
	ContactPoisson.clear();

	NoSS.clear();
	delete TransientData;
	delete SteadyStateData;
}
/*
template <class ELevel, class S>
int Simulation::RecordandInitELevel(Tree<ELevel, S>* state, double& retVal)	//records the urrent occupancy and resets occupancy for the iteration
{
	if(state==NULL)	//in case there are no states when initially called.
		return(0);

	retVal += state->Data.GetOccupancy(simtime);	//needed for getting n,p,impurity concentration for generating band structure

	if(state->left != NULL)
		RecordandInitELevel(state->left, retVal);
	if(state->right != NULL)
		RecordandInitELevel(state->right, retVal);

	state->Data.SetMidOccTimeStates(0.0);	//needs to reset before running tasks.

	return(0);
}
*/

bool Simulation::AggregateOpticalData(double factor) {
	if (_OpticalFluxOut) {
		if (_OpticalAggregateOut == nullptr)
			_OpticalAggregateOut = new BulkOpticalData();
		_OpticalAggregateOut->AddExistingBulkData(_OpticalFluxOut, factor);
		return true;
	}
	return false;
}

void Simulation::ResetOpticalFlux(bool includeAggregate) {
	if (_OpticalFluxOut)
		_OpticalFluxOut->ClearAll();
	if (includeAggregate && _OpticalAggregateOut)
		_OpticalAggregateOut->ClearAll();
}

double Simulation::InitIteration(Model& mdl, ModelDescribe& mdesc)	//set the time, set the band structure, clear out intermediate variables.
{
		//Calculate Rho, set boundary conditions, solve poisson's equation, put fermi positions in
	//proper place. Also reduces simulation timestep as needed.
	if (TransientData && simType==SIMTYPE_TRANSIENT)
		return(TransientData->InitIteration());

	if (SteadyStateData && simType==SIMTYPE_SS)
		return(SteadyStateData->InitIteration(mdl, mdesc));

	return(CalcBandStruct(mdl, mdesc));	//returns the total of how much the band structure changed
}

Contact* Simulation::getContact(int id)
{
	if (id < 0)
		return nullptr;
	for (unsigned int i = 0; i < contacts.size(); ++i) {
		if (contacts[i] && contacts[i]->id == id)
			return contacts[i];
	}
	return nullptr;
}

int Simulation::getContactIndex(Contact* ct)
{
	if (ct == nullptr)
		return -1;
	for (unsigned int i = 0; i < contacts.size(); ++i) {
		if (contacts[i] == ct)
			return i;
	}
	return -1;
}

void Simulation::Validate()
{
	for (unsigned int i = 0; i < contacts.size(); ++i) {
		if (contacts[i] == nullptr) {
			contacts.erase(contacts.begin() + i);
			--i;
		}
	}

	if (TransientData)
		TransientData->Validate();
	if (SteadyStateData)
		SteadyStateData->Validate();
}

int Simulation::FinalizeValues(Model& mdl)
{
//	double volitimei;
//	double qti = QCHARGE * timestepi;	//do this calculation a fair amount in setting current densities...

//	CarriersOut(this, false);
/*
	for(unsigned long int i = 0; i < mdl.numpoints; i++)
	{
		//before updating the current densities to not be a counter, tab up the carriers transferred
		mdl.gridp[i].carriertransfer = 0.0;
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jn.x);
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jn.y);
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jn.z);
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jp.x);
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jp.y);
		mdl.gridp[i].carriertransfer += fabs(mdl.gridp[i].Jp.z);

		//update the current densities to proper values... They are currently just a number of carriers. Want current/area
		//which is charge/time/area
		mdl.gridp[i].Jn.x = mdl.gridp[i].Jn.x * mdl.gridp[i].areai.x * qti;
		mdl.gridp[i].Jn.y = mdl.gridp[i].Jn.y * mdl.gridp[i].areai.y * qti;
		mdl.gridp[i].Jn.z = mdl.gridp[i].Jn.z * mdl.gridp[i].areai.z * qti;
		mdl.gridp[i].Jp.x = mdl.gridp[i].Jp.x * mdl.gridp[i].areai.x * qti;
		mdl.gridp[i].Jp.y = mdl.gridp[i].Jp.y * mdl.gridp[i].areai.y * qti;
		mdl.gridp[i].Jp.z = mdl.gridp[i].Jp.z * mdl.gridp[i].areai.z * qti;

		volitimei = timestepi * mdl.gridp[i].voli;

//		mdl.gridp[i].MultiplyMidN(timei);	//make concentration and remove time dependency
//		mdl.gridp[i].MultiplyMidP(timei);
//		mdl.gridp[i].MultiplyMidN(mdl.gridp[i].voli);	//this is done as two separate calls because when calculating contribution from rho, it is
//		mdl.gridp[i].MultiplyMidP(mdl.gridp[i].voli);	//necessarily done as two separate calls. This causes the same rounding/inaccuracy to occur
		//so then they may cancel one another out instead of having a rounding error multiplied by 10^8+. Effect is negligible, but I prefer seeing
		//rho = 0 instead of 10^-17
		mdl.gridp[i].srhR1 = mdl.gridp[i].srhR1 * volitimei;	//get rates in normal units instead of just # of carriers
		mdl.gridp[i].srhR2 = mdl.gridp[i].srhR2 * volitimei;	//that moved. While useful for seeing where steps are,
		mdl.gridp[i].srhR3 = mdl.gridp[i].srhR3 * volitimei;	//having actual rates makes life a bit easier...
		mdl.gridp[i].srhR4 = mdl.gridp[i].srhR4 * volitimei;
		mdl.gridp[i].genth = mdl.gridp[i].genth * volitimei;
		mdl.gridp[i].recth = mdl.gridp[i].recth * volitimei;
		mdl.gridp[i].genlight = mdl.gridp[i].genlight * volitimei;
		mdl.gridp[i].deflight = mdl.gridp[i].deflight * volitimei;
		mdl.gridp[i].condlight = mdl.gridp[i].condlight * volitimei;

		//tally up the charge in the device point: n, p, impurity values

		mdl.gridp[i].n = 0.0;	//must reset the values first
		mdl.gridp[i].p = 0.0;
		mdl.gridp[i].elecimptime = 0.0;
		UpdatePostIterELevel(mdl.gridp[i].cb.energies.start, mdl.gridp[i].n);	//update occtime for all the states
		UpdatePostIterELevel(mdl.gridp[i].vb.energies.start, mdl.gridp[i].p);	//and clear out any heaped data that may exist
		UpdatePostIterELevel(mdl.gridp[i].extrastates.start, mdl.gridp[i].elecimptime);	//pertaining to the previous calculation (do post as it would
		// survive beyond the program otherwise. Calculate n, p, and elecimptime values normalized from time.

		

		if(mdl.gridp[i].n < 0.0 || mdl.gridp[i].p < 0.0)	//debug trace check
		{
			ProgressInt(20, i);
			ProgressInt(21, curiter);
		}

	}
*/
	


	return(0);
}

int Simulation::PrepPointBandStructure(Model& mdl, Point& pt)
{
	//now update the charge density
	CalculateRho(pt);	//this just looks at netchargecarriers to update rho.
	mdl.poissonRho[pt.adj.self] = pt.rho;	//put that rho into the main model bit...

	/*
	This is now taken care of in Simulation::PrepBoundaryConditions


	if(pt->contactData != NULL)	//it's a contact...
	{
//		if(pt.adj.self == 0)	//DEBUG
//			CalcLeastSquaresGraphData(pt, *this);
		pt->ResetFermiEnergies(false, RATE_OCC_END, accuracy, true);	//only update the main/quasi fermi levels, not individual energy states.
		double level = -pt->contactData->GetVoltage();	//the voltage Ef was set at: Chem potential = -Voltage
		//so Ef needs to get set to level, but everything else must change first....

			//now shift everything over
		double useFermi = pt->fermi;
		double weightCB = pt->cb.GetNumParticles();	//odds are, one will be orders of magnitude larger than the other...
		double weightVB = pt->vb.GetNumParticles();	
		double weight = weightCB + weightVB;
		if(weight > 0.5)	//won't divide by zero. Should be a whole number as is
		{	//this should provide a fairly accurate usage for the fermi level that matters.
			useFermi = (weightCB * pt->fermin + weightVB * pt->fermip) / weight;
		}	//otherwise it is just the normal fermi level.
		double difference = level - useFermi;	//what the band needs to shift by for the boundary condition
		pt->vb.bandmin = pt->vb.bandmin + difference;
		pt->cb.bandmin = pt->cb.bandmin + difference;
		pt->Psi = pt->Psi + difference;
		pt->fermi = pt->fermi + difference;	//curpt->fermi + difference	= level
		pt->fermin = pt->fermin + difference;
		pt->fermip = pt->fermip + difference;
		
		//make sure to either add the value or replace the value
		Tree<double, unsigned int>* ptrpoiBC = mdl.poissonBC.Find(pt->adj.self);
		if(ptrpoiBC == NULL)
			mdl.poissonBC.Insert(pt->Psi, pt->adj.self);	//Put the value of Psi into the given boundary condition
		else
			ptrpoiBC->Data = pt->Psi;
	}

	*/
	return(0);
}

int Simulation::SetMaxOpticalSize()
{
	if(EnabledRates.OpticalEMax < EnabledRates.OpticalEMin)	//the data is invalid at this point
	{
		maxOpticalSize = -1;
		return(-1);
	}
	if(EnabledRates.OpticalEMax == EnabledRates.OpticalEMin)
	{
		maxOpticalSize = 1;
		return(1);
	}
	double dif = EnabledRates.OpticalEMax - EnabledRates.OpticalEMin;
	maxOpticalSize = int(dif / EnabledRates.OpticalEResolution) + 1;
	return(maxOpticalSize);
}

bool Simulation::AddOpticalData(unsigned char which, double Energy, double photons) {
	Energy = GetBinnedEnergy(Energy);
	if (Energy <= 0.0 || which > BulkOpticalData::lastType || photons < 0.0)	//it wasn't in the resolution specified or was just bad.
		return false;

	if (_OpticalFluxOut == nullptr)	//nothing to store the data in yet.
		_OpticalFluxOut = new BulkOpticalData();

	_OpticalFluxOut->AddData(Energy, which, photons);
	return true;
}

double Simulation::GetBinnedEnergy(double energy)
{
	if(energy < EnabledRates.OpticalEMin || (energy > EnabledRates.OpticalEMax && EnabledRates.OpticalEMax > 0.0))
		return(0.0);	//flag that this is an invalid energy

	energy = EnabledRates.OpticalEMin + floor((energy - EnabledRates.OpticalEMin) / EnabledRates.OpticalEResolution + EnabledRates.OpticalEResolution * 1.0e-7) * EnabledRates.OpticalEResolution;
	return(energy);
}

int Simulation::GetOpticalIndex(double energy)
{
	energy = energy - EnabledRates.OpticalEMin;	//relative distance. A slight fudge factor below to get around
	int index = int(energy / EnabledRates.OpticalEResolution + EnabledRates.OpticalEResolution * 1.0e-7);
	if(index >= maxOpticalSize)  	//double's not being quite accurate
		index = -1;

	return(index);
}

double Simulation::GetEnergyIndex(int index)
{
	if(index < 0 || index >= maxOpticalSize)
		return(0.0);
	return(EnabledRates.OpticalEMin + double(index) * EnabledRates.OpticalEResolution);
}

//get all the boundary conditions loaded for calculating the band diagram
int Simulation::PrepBoundaryConditions(Model& mdl)
{
	//this updates the contact internal values which are needed to update the boundary conditions below
	if (SteadyStateData && simType == SIMTYPE_SS) {
		for (unsigned int i = 0; i < contacts.size(); i++) {
			for (unsigned int j = 0; j < contacts[i]->point.size(); j++)	//make sure contact data is correct for band structure
			{ //just calculates rho and puts it in mdl.poissonRho
				PrepPointBandStructure(mdl, *(contacts[i]->point[j]));		//though I'm honestly not certain if this is required...
			}
			contacts[i]->DeterminePsiContactPts();
		}
	}
	if (TransientData && simType == SIMTYPE_TRANSIENT) {
		TransientData->PrepBoundaryConditions(true);
	}
	
	
	//loop through all the boundary conditions
	//must do complicated ones last...
	for(std::map<unsigned long int, BoundaryCondition>::iterator BCs = mdl.poissonBC.begin(); BCs != mdl.poissonBC.end(); ++BCs)
	{  //figure out what all the values should be
		switch (BCs->second.type)
		{
			case BC_MIN_EFIELD_ENERGY:
			case BC_OPP_EFIELD:
			case BC_CURRENT_NEUTRAL:
				break;
			default:
				BCs->second.ProcessBC(this);	//default to not forcing any values
		}
	}	//end loop going through all the different boundary conditions available

	//now do the complicated one that must have everything else done first
	for (std::map<unsigned long int, BoundaryCondition>::iterator BCs = mdl.poissonBC.begin(); BCs != mdl.poissonBC.end(); ++BCs)
	{  //figure out what all the values should be
		switch (BCs->second.type)
		{
		case BC_MIN_EFIELD_ENERGY:
		case BC_OPP_EFIELD:
		case BC_CURRENT_NEUTRAL:
			BCs->second.ProcessBC(this);	//default to not forcing any values
			break;
		default:
			break;
		}
	}	//end loop going through all the different boundary conditions available

	return(0);
}

//this assumes electric field is along X direction
double BoundaryCondition::MinEField(Simulation* sim)
{
	if (sim == NULL)
		return(0.0);	//can't find it because no access to all the data

	Model* mdl = sim->mdlData;
	if (mdl == NULL)	//still can't access necessary data
		return(0.0);

	double* PsiCnst = new double[mdl->numpoints];
	double* BndCnst = new double[mdl->numpoints];
	double* LCnst = new double[mdl->numpoints];
	double* RCnst = new double[mdl->numpoints];
	double tmpEps;

	for (std::map<long int, Point>::iterator ptIt = mdl->gridp.begin(); ptIt != mdl->gridp.end(); ++ptIt)
	{
		Point* pt = &ptIt->second;

		unsigned long ptID = pt->adj.self;
		if (ptID > mdl->numpoints)
			continue;	//something is really screwy

		tmpEps = (pt->MatProps != NULL) ? double(pt->MatProps->epsR) : 1.0;
		LCnst[ptID] = RCnst[ptID] = 0.0;	//clear everything out
		PsiCnst[ptID] = BndCnst[ptID] = 0.0;

		double distL = pt->position.x - pt->mxmy.x;
		double distR = pt->pxpy.x - pt->position.x;

		for (unsigned int i = 0; i < pt->pDsize; i++)	//add up all the contributions to psi
		{
			if (pt->poissonDependency[i].pt < mdl->numpoints)
				PsiCnst[ptID] += pt->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[pt->poissonDependency[i].pt];	//what that rho/BC is
			/*			else if(ID != pt->poissonDependency[i].pt)	//this is a boundary condition. Just pull the particular boundary condition
			{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int, BoundaryCondition>::iterator BoundCond = mdl->poissonBC.find(pt->poissonDependency[i].pt);
			if (BoundCond != mdl->poissonBC.end())
			PsiCnst[ptID] += pt->poissonDependency[i].data * BoundCond->second.value;
			//the other boundary condition if this is called must be a voltage, which just shifts the band diagram up and down
			//therefore, the net effect of differences on Psi should be negligible. If it is a current or electric field that influences the band diagram,
			//then this setup will be suppressed and it can't occur.

			}*/
			else if (ID == pt->poissonDependency[i].pt)//how this particular boundary condition affects this point
			{
				BndCnst[ptID] = pt->poissonDependency[i].data;
			}
		}

		LCnst[ptID] = pt->area.x / distL * tmpEps;
		RCnst[ptID] = pt->area.x / distR * tmpEps;
	}

	PosNegSum cnst;
	PosNegSum linear;
	cnst.Clear();	//just make sure that these are cleared
	linear.Clear();
	for (unsigned long int i = 1; i < mdl->numpoints - 1; ++i)
	{
		cnst.AddValue(LCnst[i] * (PsiCnst[i] * BndCnst[i]));
		cnst.AddValue(LCnst[i] * (PsiCnst[i - 1] * BndCnst[i - 1]));
		cnst.SubtractValue(LCnst[i] * (PsiCnst[i] * BndCnst[i - 1]));
		cnst.SubtractValue(LCnst[i] * (PsiCnst[i - 1] * BndCnst[i]));

		cnst.AddValue(RCnst[i] * (PsiCnst[i] * BndCnst[i]));
		cnst.AddValue(RCnst[i] * (PsiCnst[i + 1] * BndCnst[i + 1]));
		cnst.SubtractValue(RCnst[i] * (PsiCnst[i + 1] * BndCnst[i]));
		cnst.SubtractValue(RCnst[i] * (PsiCnst[i] * BndCnst[i + 1]));

		tmpEps = BndCnst[i] - BndCnst[i - 1];	//just a temp value
		linear.AddValue(LCnst[i] * tmpEps * tmpEps);
		tmpEps = BndCnst[i] - BndCnst[i + 1];	//just a temp value
		linear.AddValue(RCnst[i] * tmpEps * tmpEps);
	}

	delete[] PsiCnst;
	delete[] BndCnst;
	delete[] LCnst;
	delete[] RCnst;

	double con = cnst.GetNetSumClear();
	double lin = linear.GetNetSumClear();
	if (lin == 0.0 || con == 0.0)
		return(0.0);

	double EField = -con / lin;
	//now update the external pt Psi and the points general properties - only critical if this also has an external connection

	if (pt->contactData)	//this is very necessary if there are any external outputs
		pt->contactData->SetEField(EField);

	return(EField);

}

//this assumes electric field is along X direction
double BoundaryCondition::OppEField(Simulation* sim)
{
	if (sim == NULL)
		return(0.0);	//can't find it because no access to all the data

	if (pt == NULL)	//how do this if we don't know where it's at?
		return(0.0);

	Model* mdl = sim->mdlData;
	if (mdl == NULL)	//still can't access necessary data
		return(0.0);

	double dField = 0.0;

	//first check if this point is on the outside
	bool ExtL = (pt->adj.adjMX==NULL)?true:false;	//probably don't need the ?, but better safe than sorry
	bool ExtR = (pt->adj.adjPX==NULL)?true:false;

	if (ExtL)
	{
		//the contact is on the left edge, so we need to determine the e-field on the right edge (more positive values)
		std::map<long int, Point>::reverse_iterator ptIt = mdl->gridp.rbegin();	//start on the presumably far side
		if (ptIt == mdl->gridp.rend())
			return(0.0);
		Point* RRpt = &ptIt->second;
		while (RRpt->adj.adjPX)
			RRpt = RRpt->adj.adjPX;
		Point* RIpt = RRpt->adj.adjMX;
		if (RRpt == NULL || RIpt == NULL)
			return(0.0);
		//everything has been verified. Now just need to run the calculations

		double PsiRR = 0.0;
		double PsiRI = 0.0;
		double BcRR = 0.0;
		double BcRi = 0.0;
		double widthRI = RIpt->pxpy.x - RIpt->position.x;
		double widthRR = RRpt->position.x - RRpt->mxmy.x;
		double epsRR = double(RRpt->eps); 
		double epsRI = double(RIpt->eps);

		for (unsigned int i = 0; i < RRpt->pDsize; i++)	//add up all the contributions to psi
		{
			if (RRpt->poissonDependency[i].pt < mdl->numpoints)
				PsiRR += RRpt->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[RRpt->poissonDependency[i].pt];	//what that rho/BC is
			/*			else if(ID != pt->poissonDependency[i].pt)	//this is a boundary condition. Just pull the particular boundary condition
			{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int, BoundaryCondition>::iterator BoundCond = mdl->poissonBC.find(pt->poissonDependency[i].pt);
			if (BoundCond != mdl->poissonBC.end())
			PsiCnst[ptID] += pt->poissonDependency[i].data * BoundCond->second.value;
			//the other boundary condition if this is called must be a voltage, which just shifts the band diagram up and down
			//therefore, the net effect of differences on Psi should be negligible. If it is a current or electric field that influences the band diagram,
			//then this setup will be suppressed and it can't occur.

			}*/
			else if (ID == RRpt->poissonDependency[i].pt)//how this particular boundary condition affects this point
			{
				BcRR = RRpt->poissonDependency[i].data;
			}
		}

		for (unsigned int i = 0; i < RIpt->pDsize; i++)	//add up all the contributions to psi
		{
			if (RIpt->poissonDependency[i].pt < mdl->numpoints)
				PsiRI += RIpt->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[RIpt->poissonDependency[i].pt];	//what that rho/BC is
			/*			else if(ID != pt->poissonDependency[i].pt)	//this is a boundary condition. Just pull the particular boundary condition
			{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int, BoundaryCondition>::iterator BoundCond = mdl->poissonBC.find(pt->poissonDependency[i].pt);
			if (BoundCond != mdl->poissonBC.end())
			PsiCnst[ptID] += pt->poissonDependency[i].data * BoundCond->second.value;
			//the other boundary condition if this is called must be a voltage, which just shifts the band diagram up and down
			//therefore, the net effect of differences on Psi should be negligible. If it is a current or electric field that influences the band diagram,
			//then this setup will be suppressed and it can't occur.

			}*/
			else if (ID == RIpt->poissonDependency[i].pt)//how this particular boundary condition affects this point
			{
				BcRi = RIpt->poissonDependency[i].data;
			}
		}
		double num, den;
		if (epsRI == epsRR)
		{
			num = epsRR * (PsiRI - PsiRR);
			den = epsRR * (BcRR - BcRi) + widthRI + widthRR;
			dField = num / den;
			pt->contactData->SetEField(dField * pt->eps);	//note, this was not quite D -- the eps_not was not divided out, so this should be proper eField
		}
		else
		{
			num = epsRR * epsRR;
			den = num * (BcRR - BcRi) + widthRR * epsRR + widthRI * epsRI;
			num = num * (PsiRI - PsiRR);
			dField = num / den;
			pt->contactData->SetEField(dField * pt->eps);	//note, this was not quite D -- the eps_not was not divided out, so this should be proper eField
		}


	}
	else if (ExtR)
	{
		//contact is the farthest right point!
		std::map<long int, Point>::iterator ptIt = mdl->gridp.begin();	//start on the presumably far side
		if (ptIt == mdl->gridp.end())
			return(0.0);
		Point* LLpt = &ptIt->second;
		while (LLpt->adj.adjMX)
			LLpt = LLpt->adj.adjMX;
		Point* LIpt = LLpt->adj.adjPX;
		if (LLpt == NULL || LIpt == NULL)
			return(0.0);
		//everything has been verified. Now just need to run the calculations

		double PsiLL = 0.0;
		double PsiLI = 0.0;
		double BcLL = 0.0;
		double BcLi = 0.0;
		double widthLI = LIpt->position.x - LIpt->mxmy.x;
		double widthLL = LLpt->pxpy.x - LLpt->position.x;
		double epsLL = double(LLpt->eps);
		double epsLI = double(LIpt->eps);

		for (unsigned int i = 0; i < LLpt->pDsize; i++)	//add up all the contributions to psi
		{
			if (LLpt->poissonDependency[i].pt < mdl->numpoints)
				PsiLL += LLpt->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[LLpt->poissonDependency[i].pt];	//what that rho/BC is
			/*			else if(ID != pt->poissonDependency[i].pt)	//this is a boundary condition. Just pull the particular boundary condition
			{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int, BoundaryCondition>::iterator BoundCond = mdl->poissonBC.find(pt->poissonDependency[i].pt);
			if (BoundCond != mdl->poissonBC.end())
			PsiCnst[ptID] += pt->poissonDependency[i].data * BoundCond->second.value;
			//the other boundary condition if this is called must be a voltage, which just shifts the band diagram up and down
			//therefore, the net effect of differences on Psi should be negligible. If it is a current or electric field that influences the band diagram,
			//then this setup will be suppressed and it can't occur.

			}*/
			else if (ID == LLpt->poissonDependency[i].pt)//how this particular boundary condition affects this point
			{
				BcLL = LLpt->poissonDependency[i].data;
			}
		}

		for (unsigned int i = 0; i < LIpt->pDsize; i++)	//add up all the contributions to psi
		{
			if (LIpt->poissonDependency[i].pt < mdl->numpoints)
				PsiLI += LIpt->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[LIpt->poissonDependency[i].pt];	//what that rho/BC is
			/*			else if(ID != pt->poissonDependency[i].pt)	//this is a boundary condition. Just pull the particular boundary condition
			{	//the boundary condition values area always stored with values > mdl.numpoints.
			std::map<unsigned long int, BoundaryCondition>::iterator BoundCond = mdl->poissonBC.find(pt->poissonDependency[i].pt);
			if (BoundCond != mdl->poissonBC.end())
			PsiCnst[ptID] += pt->poissonDependency[i].data * BoundCond->second.value;
			//the other boundary condition if this is called must be a voltage, which just shifts the band diagram up and down
			//therefore, the net effect of differences on Psi should be negligible. If it is a current or electric field that influences the band diagram,
			//then this setup will be suppressed and it can't occur.

			}*/
			else if (ID == LIpt->poissonDependency[i].pt)//how this particular boundary condition affects this point
			{
				BcLi = LIpt->poissonDependency[i].data;
			}
		}
		double num, den;
		if (epsLI == epsLL)
		{
			num = epsLL * (PsiLL - PsiLI);
			den = epsLL * (BcLi - BcLL) + widthLI + widthLL;
			dField = num / den;
			pt->contactData->SetEField(dField * pt->eps);	//note, this was not quite D -- the eps_not was not divided out, so this should be proper eField
		}
		else
		{
			num = epsLL * epsLL;
			den = num * (BcLi - BcLL) + widthLL * epsLL + widthLI * epsLI;
			num = num * (PsiLL - PsiLI);
			dField = num / den;
			pt->contactData->SetEField(dField * pt->eps);	//note, this was not quite D -- the eps_not was not divided out, so this should be proper eField
		}

	}
	else
	{
		//neither of these are outside edges, so must do it the complicated way. Four points to work through
		double PsiLL, PsiLI, PsiRR, PsiRI;	//the constant contributors towards psi
		double BcLL, BcLI, BcRR, BcRI;		//the boundary condition contribution towards psi
		double epsLL, epsLI, epsRR, epsRI;	//the relative permeabilities
		Point* ptLL = NULL;
		Point* ptLI = NULL;
		Point* ptRR = NULL;
		Point* ptRI = NULL;		//pointers to all the neccessary points
		double factL, factR;				//factors to determine stopping point for points
		double wLL, wLI, wRR, wRI;			//accompanying widths of the four points

		//get the furthest left
		std::map<long int, Point>::iterator ptIt = mdl->gridp.begin();	//start on the presumably far side
		if (ptIt == mdl->gridp.end())
			return(0.0);
		ptLL = &ptIt->second;
		while (ptLL->adj.adjMX)
			ptLL = ptLL->adj.adjMX;
		ptLI = ptLL->adj.adjPX;

		std::map<long int, Point>::reverse_iterator ptR = mdl->gridp.rbegin();
		if (ptR == mdl->gridp.rend())
			return(0.0);
		ptRR = &ptR->second;
		while (ptRR->adj.adjPX)
			ptRR = ptRR->adj.adjPX;
		ptRI = ptRR->adj.adjMX;

		if (ptLL == NULL || ptLI == NULL || ptRR == NULL || ptRI == NULL)
			return(0.0);

		epsLL = ptLL->eps;
		epsLI = ptLI->eps;
		epsRR = ptRR->eps;
		epsRI = ptRI->eps;
		wLL = ptLL->pxpy.x - ptLL->position.x;
		wLI = ptLI->position.x - ptLI->mxmy.x;
		wRR = ptRR->position.x - ptRR->mxmy.x;
		wRI = ptRI->pxpy.x - ptRI->position.x;
		factL = wLL * epsLL;
		factL = factL / (factL + wLI * epsLI);
		factR = wRR * epsRR;
		factR = factR / (factR + wRI * epsRI);

		//now get all the other values:
		PsiLL = PsiLI = PsiRR = PsiRI = BcLI = BcLL = BcRR = BcRI = 0.0;	//initialize to zero!

		for (unsigned int i = 0; i < ptLL->pDsize; i++)	//add up all the contributions to psi
		{
			if (ptLL->poissonDependency[i].pt < mdl->numpoints)
				PsiLL += ptLL->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[ptLL->poissonDependency[i].pt];	//what that rho/BC is
			else if (ID == ptLL->poissonDependency[i].pt)//how this particular boundary condition affects this point
				BcLL = ptLL->poissonDependency[i].data;
		}

		for (unsigned int i = 0; i < ptLI->pDsize; i++)	//add up all the contributions to psi
		{
			if (ptLI->poissonDependency[i].pt < mdl->numpoints)
				PsiLI += ptLI->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[ptLI->poissonDependency[i].pt];	//what that rho/BC is
			else if (ID == ptLI->poissonDependency[i].pt)//how this particular boundary condition affects this point
				BcLI = ptLI->poissonDependency[i].data;
		}

		for (unsigned int i = 0; i < ptRI->pDsize; i++)	//add up all the contributions to psi
		{
			if (ptRI->poissonDependency[i].pt < mdl->numpoints)
				PsiRI += ptRI->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[ptRI->poissonDependency[i].pt];	//what that rho/BC is
			else if (ID == ptRI->poissonDependency[i].pt)//how this particular boundary condition affects this point
				BcRI = ptRI->poissonDependency[i].data;
		}

		for (unsigned int i = 0; i < ptRR->pDsize; i++)	//add up all the contributions to psi
		{
			if (ptRR->poissonDependency[i].pt < mdl->numpoints)
				PsiRR += ptRR->poissonDependency[i].data *	//how much of rho/BC to use for this point
				mdl->poissonRho[ptRR->poissonDependency[i].pt];	//what that rho/BC is
			else if (ID == ptRR->poissonDependency[i].pt)//how this particular boundary condition affects this point
				BcRR = ptRR->poissonDependency[i].data;
		}

		double num = epsRR * factR * wLL * (PsiRI - PsiRR) + epsLL * factL * wRR * (PsiLL - PsiLI);
		double den = epsRR * factR * wLL * (BcRR - BcRI) + epsLL * factL * wRR * (BcLI - BcLL);
		dField = num / den;
		pt->contactData->SetEField(dField * pt->eps);

	}

	return(dField);

}

int BoundaryCondition::ProcessBC(Simulation* sim, double ForceValue, bool force)
{
	
	if(pt == NULL)	//not sure why this would ever happen!
	{
		ProgressCheck(49,false);	//leave trace file that the boundary condition has no point associated with it (this should always be true!)
		return(0);
	}
	if(pt->contactData == NULL)
	{
		ProgressCheck(50, false);
		return(0);
	}
	int flags = pt->contactData->GetCurrentActive();
//	bool SS = (flags & CONTACT_SS) ? true : false;

	if ((flags & CONTACT_ALLBC) == CONTACT_INACTIVE)
		return(0);

	//now start doing some processing...
	//note, the type is set in poisson prep, and it 
	//follows the transient convention whether it is steady state or transient
	if (type == BC_VOLT)
	{
		pt->ResetFermiEnergies(false, RATE_OCC_END, sim->accuracy, true);	//only update the main/quasi fermi levels, not individual energy states.
		double level = force ? ForceValue : -pt->contactData->GetVoltage();	//the voltage Ef was set at: Chem potential = -Voltage
	//so Ef needs to get set to level, but everything else must change first....
//		CalcLeastSquaresGraphData(*pt, *sim);
		//now shift everything over
		double useFermi = pt->fermi;
		double weightCB = pt->cb.GetNumParticles();	//odds are, one will be orders of magnitude larger than the other...
		double weightVB = pt->vb.GetNumParticles();	
		double weight = weightCB + weightVB;
		if(weight > 0.5)	//won't divide by zero. Should be a whole number as is
		{	//this should provide a fairly accurate usage for the fermi level that matters.
			useFermi = (weightCB * pt->fermin + weightVB * pt->fermip) / weight;
		}	//otherwise it is just the normal fermi level.
		double difference = level - useFermi;	//what the band needs to shift by for the boundary condition
		pt->vb.bandmin = pt->vb.bandmin + difference;
		pt->cb.bandmin = pt->cb.bandmin + difference;
		pt->Psi = pt->Psi + difference;
		pt->fermi = pt->fermi + difference;	//curpt->fermi + difference	= level
		pt->fermin = pt->fermin + difference;
		pt->fermip = pt->fermip + difference;
		
		value = pt->Psi;
	}
	else if (type == BC_SIMPLE_EFIELD)
	{
		value = force ? ForceValue : pt->contactData->GetEField();	//by far the easiest solution!
	}
	else if (type == BC_MIN_EFIELD_ENERGY)
	{
		value = force ? ForceValue : MinEField(sim);
	}
	else if (type == BC_OPP_EFIELD)
	{
		value = force ? ForceValue : OppEField(sim);
	}
	else if (type == BC_CURRENT_NEUTRAL)
	{
		value = force ? ForceValue : OppEField(sim);	//needs to be created yet!
	}
	else if (type == BC_EFIELD_ED)
	{
		if(flags & CONTACT_FLOAT_EFIELD_ESCAPE)	//then this isn't actually a boundary condition
		{
			value = force ? ForceValue : 0.0;	//make it have as little effect as possible
		}
		else if(flags & CONTACT_MASK_EFIELD_ED)
		{
			//very simple. This overwrites the current
			value = force ? ForceValue : pt->contactData->GetEDEField();
		}
	}
	else if (type == BC_SIMPLE_CURRENT)
	{
		//this is REALLY complicated to calculate correctly in a manner that will be consistent with the model
		//let's try and get the simple ones done first - is this point on the edge

		if(force)
		{
			pt->contactData->DeterminePsiContactPts(true, ForceValue);	//this would otherwise not line up
			//then the psi value selected was probably wrong and needs to be recalculated
		}
		//we are going to set the efield that gives the appropriate current
		//psi for an adjacent point has already been selected to give such a current

		//need to identify the correct external point in the contact
		unsigned int extPt = 0;
		for(unsigned int i=0; i<pt->contactData->point.size(); ++i)
		{
			if(pt->contactData->point[i] == pt)
				break;
//			if(pt->contactData->point[i]==NULL)
//				continue;	this is a one to one match now
			extPt++;	//this will match the index of the point of interest
		}
		//most likely, the above code will just hit the first if statement, break, and be done.

		if(extPt >= pt->contactData->PlaneRecPoints.size())
		{
			ProgressInt(66, extPt);
			value = 0.0;	//just get out with a non-garbage value but put trace notification
			return(0);
		}

		Point* ptExt = (pt->contactData->PlaneRecPoints[extPt]);
		double delta = ptExt->Psi - pt->Psi;
		double dist;
		DistancePtsMainCoord(pt, ptExt, dist);	//dist>0 equiv to adjPX==NULL
		value = delta / dist;
		
	}	//end boundary condition for calculating currents that determine the band diagram
	else if (type == BC_LEAK)
	{
		double leak = force ? ForceValue : pt->contactData->GetEFieldLeak();	//the leak stored in the contact
		if(leak != 0.0)
		{
			value = leak;	//only care about the efield, so this is would be true if there is
			return(0);	//any electric field specified to force the situation
		}
		//otherwise, there is another type of leak arriving from the current
		if(force)
		{
			value = ForceValue;
			return(0);
		}
		double Efield = 0.0;
		double current = pt->contactData->GetCurrentEDLeak();	//current in induces an electric field, and this much of that electric field leaks out
		current += pt->area.y * pt->contactData->GetCurrentDensityEDLeak();
		double conductivity = QCHARGE * (pt->cb.GetNumParticles() * pt->MatProps->MuN + pt->vb.GetNumParticles() * pt->MatProps->MuP);
		//I = q(n mu_n + p mu_p) E
		if(conductivity != 0.0)	//there should ALWAYs be electrons and holes
			Efield = current / conductivity;

		value = Efield;
	}
	else //this is not a recognized type of boundary condition.
	{
		ProgressInt(54, type);
		return(0);
	}
	return(0);
}

Calculations::Calculations()
{
	SetDefaultValues();
}

void Calculations::DisableAll(void)
{
	SRH1 = false;
	SRH2 = false;
	SRH3 = false;
	SRH4 = false;
	ThermalGen = false;
	ThermalRec = false;
	Scatter = false;
	Current = false;

	//disabled by default... because they are not implemented yet
	Tunnel = false;
	Lgen = false;
	Lsrh = false;
	Lscat = false;
	Ldef = false;
	SErec = false;
	SEsrh = false;
	SEscat = false;
	SEdef = false;
	Lpow = false;
	RecyclePhotonsAffectRates = false;	//this doesn't converge if this is true
	Thermalize = 0;
	

	LightEmissions = false;
	return;
}

void Calculations::EnableAll(void)
{
	SRH1 = true;
	SRH2 = true;
	SRH3 = true;
	SRH4 = true;
	ThermalGen = true;
	ThermalRec = true;
	Scatter = true;
	Current = true;

	//disabled by default... because they are not implemented yet
	Tunnel = false;	//no tunneling rates, don't let it try and do something crazy
	Lgen = true;
	Lsrh = true;
	Lscat = true;
	Ldef = true;
	SErec = true;
	SEsrh = true;
	SEscat = true;
	SEdef = true;
	Lpow = true;
	RecyclePhotonsAffectRates = true;	//this doesn't converge if this is true

	LightEmissions = true;
	return;
}

void Calculations::SetDefaultValues(void)
{
	SRH1 = true;
	SRH2 = true;
	SRH3 = true;
	SRH4 = true;
	ThermalGen = true;
	ThermalRec = true;
	Scatter = true;
	Current = true;

	//disabled by default... because they are not implemented yet
	Tunnel = false;
	Lgen = true;
	Lsrh = true;
	Lscat = false;
	Ldef = true;
	SErec = true;
	SEsrh = true;
	SEscat = false;
	SEdef = true;
	Lpow = true;
	RecyclePhotonsAffectRates = false;	//this doesn't converge if this is true

	LightEmissions = true;
	Thermalize = THERMALIZE_ALL & ~(THERMALIZE_CB | THERMALIZE_VB);	//default to hot carriers on

	OpticalEMax = -1.0;	//default to say that no limit on max energy
	OpticalEMin = 0.0;	//default to say that all energies may be emitted
	OpticalEResolution = 0.001;	//force all light to be a multiple of this value
	OpticalIntensityCutoff = 1e-4;

}

RefTrans::RefTrans()
{
	ptrTargetState = NULL;
	ptrTargetFromState = NULL;
	ptrTargetPoint = NULL;
	direction = 0;
}
RefTrans::~RefTrans()
{
	ptrTargetState = NULL;
	ptrTargetFromState = NULL;
	ptrTargetPoint = NULL;
}

PointOutputs::PointOutputs()
{
	fermi = NULL;
	fermin = NULL;
	fermip = NULL;
	Ec = NULL;
	Ev = NULL;
	Psi = NULL;
	CarCB = NULL;
	CarVB = NULL;
	n = NULL;
	p = NULL;
	rho = NULL;
	PointImpCharge = NULL;
	genth = NULL;
	recth = NULL;
	R1 = NULL;
	R2 = NULL;
	R3 = NULL;
	R4 = NULL;
	scatter = NULL;
	Jpx = NULL;
	Jnx = NULL;
	Jpy = NULL;
	Jny = NULL;
	Jpz = NULL;
	Jnz = NULL;
	Lgen = NULL;
	Lsrh = NULL;
	Ldef = NULL;
	Lscat = NULL;
	Erec = NULL;
	Esrh = NULL;
	Edef = NULL;
	Escat = NULL;
	LPowerEnter = NULL;
	LightEmit = NULL;
	CBNRIn = NULL;
	VBNRIn = NULL;
	DonNRin = NULL;
	AcpNRin = NULL;
	DetailedOptical = false;	//default to off
}
PointOutputs::~PointOutputs()
{
	Deallocate();
}
int PointOutputs::Deallocate(void)
{
	delete [] fermi;
	fermi = NULL;
	delete [] fermin;
	fermin = NULL;
	delete [] fermip;
	fermip = NULL;
	delete [] Ec;
	Ec = NULL;
	delete [] Ev;
	Ev = NULL;
	delete [] Psi;
	Psi = NULL;
	delete [] CarCB;
	CarCB = NULL;
	delete [] CarVB;
	CarVB = NULL;
	delete [] n;
	n = NULL;
	delete [] p;
	p = NULL;
	delete [] PointImpCharge;
	PointImpCharge = NULL;
	delete [] genth;
	genth = NULL;
	delete [] recth;
	recth = NULL;
	delete [] R1;
	R1 = NULL;
	delete [] R2;
	R2 = NULL;
	delete [] R3;
	R3 = NULL;
	delete [] R4;
	R4 = NULL;
	delete [] rho;
	rho = NULL;
	delete [] scatter;
	scatter = NULL;
	delete [] Jpx;
	Jpx = NULL;
	delete [] Jnx;
	Jnx = NULL;
	delete [] Jpy;
	Jpy = NULL;
	delete [] Jny;
	Jny = NULL;
	delete [] Jpz;
	Jpz = NULL;
	delete [] Jnz;
	Jnz = NULL;
	delete [] CBNRIn;
	CBNRIn = NULL;
	delete [] VBNRIn;
	VBNRIn = NULL;

	delete [] DonNRin;
	DonNRin = NULL;
	delete [] AcpNRin;
	AcpNRin = NULL;

	delete [] Lgen;
	Lgen = NULL;
	delete [] Lsrh;
	Lsrh = NULL;
	delete [] Ldef;
	Ldef = NULL;
	delete [] Lscat;
	Lscat = NULL;

	delete [] Erec;
	Erec = NULL;
	delete [] Esrh;
	Esrh = NULL;
	delete [] Edef;
	Edef = NULL;
	delete [] Escat;
	Escat = NULL;

	delete [] LPowerEnter;
	LPowerEnter = NULL;

	delete [] LightEmit;
	LightEmit = NULL;

	return(0);
}

int PointOutputs::Allocate(unsigned long int num)
{
	Deallocate();	//make sure all the memory gets cleared first
	fermi = new double[num];
	fermin = new double[num];
	fermip = new double[num];
	Ec = new double[num];
	Ev = new double[num];
	Psi = new double[num];
	CarCB = new double[num];
	CarVB = new double[num];
	n = new double[num];
	p = new double[num];
	genth = new double[num];
	recth = new double[num];
	PointImpCharge = new double[num];
	R1 = new double[num];
	R2 = new double[num];
	R3 = new double[num];
	R4 = new double[num];
	rho = new double[num];
	scatter = new double[num];
	Jpx = new double[num];
	Jnx = new double[num];
	Jpy = new double[num];
	Jny = new double[num];
	Jpz = new double[num];
	Jnz = new double[num];
	CBNRIn = new double[num];
	VBNRIn = new double[num];
	DonNRin = new double[num];
	AcpNRin = new double[num];
	Lgen = new double[num];
	Lsrh = new double[num];
	Ldef = new double[num];
	Lscat = new double[num];
	Erec = new double[num];
	Esrh = new double[num];
	Edef = new double[num];
	Escat = new double[num];
	LPowerEnter = new double[num];
	LightEmit = new double[num];
	return(0);
}


Model::Model(ModelDescribe& md) : mdesc(md)
{
	gridp.clear();
	MergeSplitPoints.clear();
	boundaries.clear();
	poissonRho = NULL;
	poissonBC.clear();
	PoissonDependencyCalculated = false;
	width.x = 0.0;
	width.y = 0.0;
	disableTunneling = false;
	uniqueIDctr = 1;
}
Model::Model(ModelDescribe& md, unsigned long size) : mdesc(md)
{
	gridp.clear();
	poissonRho = NULL;
	boundaries.clear();
	poissonBC.clear();
	MergeSplitPoints.clear();
	PoissonDependencyCalculated = false;
	width.x = 0;
	width.y = 0;
	disableTunneling = false;
	uniqueIDctr = 1;
	
}
Model::~Model()
{
	width.x = 0;
	width.y = 0;
	width.z = 0;
	uniqueIDctr = 1;
	PoissonDependencyCalculated = false;
	gridp.clear();	//memory that was here...
	MergeSplitPoints.clear();
	boundaries.clear();
	delete [] poissonRho;
	poissonBC.clear();
	poissonRho = NULL;
}


void Layer::Validate(ModelDescribe& mdesc) {
	if (contactid >= 0) {
		if (mdesc.getContact(contactid) == nullptr)
			contactid = -1;
	}

	for (unsigned int i = 0; i < impurities.size(); ++i) {
		impurities[i]->data = mdesc.getImpurity(impurities[i]->id);
		if (impurities[i]->data == nullptr) {
			impurities[i]->id = -1;
		}
	}

	for (unsigned int i = 0; i < matls.size(); ++i) {
		matls[i]->data = mdesc.getMaterial(matls[i]->id);
		if (matls[i]->data == nullptr) {
			matls[i]->id = -1;
		}
	}


}

/*
General premise is a line segment from this point of interest to each vertex of the shape
should cross an even number of the line segments making up the shape. Note, two will automatically
be the line segments attached to that vertex
*/
bool Layer::InLayer(XSpace pos)
{

	if(shape == NULL)
		return(false);	//this layer does not have a defined area

	if(shape->start == NULL)
		return(false);	//layer still doesn't have a defined area


	if(shape->type == SHPPOINT)	//it will have the resolution of the device
	{
		if(shape->getNumVertices() == 1)
		{
			XSpace ptCorner;
			ptCorner.x = shape->start->pt.x + deviation.x/1.999;
		
			if(pos.x > ptCorner.x)
				return(false);
		
			ptCorner.x = shape->start->pt.x - deviation.x/1.999;
			if(pos.x < ptCorner.x)
				return(false);

			ptCorner.y = shape->start->pt.y + deviation.y/1.999;
			if(pos.y > ptCorner.y)
				return(false);

			ptCorner.y = shape->start->pt.y - deviation.y/1.999;
			if(pos.y < ptCorner.y)
				return(false);

			return(true);
		}
		else	//assume there is two points defining this point
		{
			Line shapeboundary(shape->start->pt, shape->start->next->pt);
			//now check if this point lies on the line
			if(shapeboundary.vertical == true)
			{
				if(fabs(pos.x - shapeboundary.pt1.x) <= deviation.x/1.999)	//make sure the vertical line has
				{		//the right x coordinate
					if(pos.y <= shape->max.y + deviation.y/1.999 &&	//and that it is within
						pos.y >= shape->min.y - deviation.y/1.999)	//the y coordinates
						return(true);
				}
			}
			double y1 = shapeboundary.m * pos.x + shapeboundary.b;
			if(fabs(y1 - pos.y) <= deviation.y/1.999)	//given the X position of the point, it would have 
			{			//position y1 if it were on the line.  Is this within the spacing of the grid points?
				if((pos.x >= shapeboundary.pt1.x - deviation.x/1.999 &&	//need to include in case the line has slope
					pos.x <= shapeboundary.pt2.x + deviation.x/1.999) ||	//0, which cancels out x.
					(pos.x >= shapeboundary.pt2.x - deviation.x/1.999 &&
					pos.x <= shapeboundary.pt1.x + deviation.x/1.999))
					return(true);
			}
		}
	}
	else if(shape->type == SHPLINE)	//see if point lies on line...
	{
		Line shapeboundary(shape->start->pt, shape->start->next->pt);
		//now check if this point lies on the line
		if(shapeboundary.vertical == true)
		{
			if(fabs(pos.x - shapeboundary.pt1.x) <= deviation.x/1.999)	//make sure the vertical line has
			{		//the right x coordinate
				if(pos.y <= shape->max.y + deviation.y/1.999 &&	//and that it is within
					pos.y >= shape->min.y - deviation.y/1.999)	//the y coordinates
					return(true);
			}
		}
		double y1 = shapeboundary.m * pos.x + shapeboundary.b;
		if(fabs(y1 - pos.y) <= deviation.y/1.999)	//given the X position of the point, it would have 
		{			//position y1 if it were on the line.  Is this within the spacing of the grid points?
			if((pos.x >= shapeboundary.pt1.x - deviation.x/1.999 &&	//need to include in case the line has slope
				pos.x <= shapeboundary.pt2.x + deviation.x/1.999) ||	//0, which cancels out x.
				(pos.x >= shapeboundary.pt2.x - deviation.x/1.999 &&
				pos.x <= shapeboundary.pt1.x + deviation.x/1.999))
				return(true);
		}
		//else false
		
	}
	else if(shape->type <= RIGID2D)	//unknown number of vertices
	{
		Vertex* ptr = shape->start;
		
		Line PostoVertexPL;		//the angle to be added
		Line PostoVertexMI;		//angle to be subtracted
		PostoVertexPL.pt1 = pos;
		PostoVertexMI.pt1 = pos;
		double angle = 0;
		double delang = 0;
		do	//loop going around all the points for PostoVertex
		{
			PostoVertexPL.pt2 = ptr->pt;
			PostoVertexMI.pt2 = ptr->prev->pt;
			PostoVertexPL.CalcAngles();
			PostoVertexMI.CalcAngles();
			delang = PostoVertexPL.phi - PostoVertexMI.phi;
			//delang could be way off... if the distance is greter than pi, the next point swept all the way around
			while(delang > PI)	//therefore, it swept 2PI to far, so go back two pi
			{
				delang -= 2.0*PI;
			}
			while(delang <= -PI)	//same can be said for the other direction (vertices reverse order)
			{	//if the point lies on the line, (say shape is line...) 
				delang += 2.0*PI;
			}

			angle += delang;


			ptr = ptr->next;
		}while(ptr != shape->start);

		if(fabs(angle) > 6)	//angle can only equal 0 or +/- 2PI. If it's 2PI, it's within.
			return(true);	//don't want to be off due to rounding issues...
	}
	else if(shape->type == CIRCLE)
	{
//		XSpace distancevect = shape->center;	//originally center
		XSpace radiusvect = shape->start->pt;	//radius of the circle
		double radius = AbsDistance(shape->center, radiusvect);
		double distance = AbsDistance(shape->center, pos);
		
		if(distance <= radius)
			return(true);
	}

	return(false);	//else, it is not in the point
}


int Point::PopulateAngleData2D(Model& mdl)
{
	XSpace thispos = position;
	XSpace targetpos;
	Point* adj;
	Point* adj2;
	angleMatch tempAngle;

	AngleData.clear();	//empty the data if it is already there.

//////////////////////////// ADJ MX ////////////////

	adj = this->adj.adjMX;	//refers to the x coord with a lower y value
	adj2 = this->adj.adjMX2;	//now do the upper y value
	if(adj)
	{
		tempAngle.targetPt = adj;
		targetpos = adj->pxpy;
		if(targetpos.y > pxpy.y)
			targetpos.y = pxpy.y;
		
		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		targetpos.y = adj->mxmy.y;
		if(targetpos.y < mxmy.y)
			targetpos.y = mxmy.y;

		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;
		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}
		
	if(adj2 != adj && adj2)	//provided it wasn't just done and is not outside the model
	{
		tempAngle.targetPt = adj2;
		targetpos = adj2->pxpy;
		if(targetpos.y > pxpy.y)
			targetpos.y = pxpy.y;
		
		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		targetpos.y = adj2->mxmy.y;
		if(targetpos.y < mxmy.y)
			targetpos.y = mxmy.y;

		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;
		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}

	/////////////////////////////// ADJ PX ////////////////////

	adj = this->adj.adjPX;	//refers to the x coord with a lower y value
	adj2 = this->adj.adjPX2;	//and the higher y value
	if(adj)
	{
		tempAngle.targetPt =  adj;
		targetpos = adj->mxmy;
		if(targetpos.y < mxmy.y)
			targetpos.y = mxmy.y;
		
		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		targetpos.y =adj->pxpy.y;
		if(targetpos.y > pxpy.y)
			targetpos.y = pxpy.y;

		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;
		if(tempAngle.proportion < 0)		//deal with the crossover from 0 to 2 pi
			tempAngle.proportion += PI2;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}

	if(adj2 != adj && adj2)	//not the previous and not outside...
	{
		tempAngle.targetPt = adj2;
		targetpos = adj2->mxmy;
		if(targetpos.y < mxmy.y)
			targetpos.y = mxmy.y;
		
		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		targetpos.y = adj2->pxpy.y;
		if(targetpos.y > pxpy.y)
			targetpos.y = pxpy.y;

		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;
		if(tempAngle.proportion < 0)	// pi/6 - 11pi/6 --> 2pi/6. 
			tempAngle.proportion += PI2;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}

///////////////////////////////////// ADJ PY /////////

	adj = this->adj.adjPY;	//refers to the -y coord with a lower x value
	adj2 = this->adj.adjPY2;	//and the higher x value

	if(adj)
	{
		tempAngle.targetPt = adj;
		targetpos = adj->mxmy;
		if(targetpos.x < mxmy.x)
			targetpos.x = mxmy.x;
		
		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		targetpos.x = adj->pxpy.x;
		if(targetpos.x > pxpy.x)
			targetpos.x = pxpy.x;

		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}
	if(adj2 != adj && adj2)
	{
		
		tempAngle.targetPt = adj2;
		targetpos = adj2->mxmy;
		if(targetpos.x < mxmy.x)
			targetpos.x = mxmy.x;
		
		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		targetpos.x = adj2->pxpy.x;
		if(targetpos.x > pxpy.x)
			targetpos.x = pxpy.x;

		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}

	/////////////////////////// ADJ MY /////////////////////

	adj = this->adj.adjMY;	//refers to the -y coord with a lower x value
	adj2 = this->adj.adjMY2;	//and the higher x value

	if(adj)
	{
		tempAngle.targetPt = adj;
		targetpos = adj->pxpy;
		if(targetpos.x > pxpy.x)
			targetpos.x = pxpy.x;
		
		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		targetpos.x = adj->mxmy.x;
		if(targetpos.x < mxmy.x)
			targetpos.x = mxmy.x;

		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}
	if(adj2 != adj && adj2)
	{
		
		tempAngle.targetPt = adj2;
		targetpos = adj2->pxpy;
		if(targetpos.x > pxpy.x)
			targetpos.x = pxpy.x;
		
		tempAngle.theta[1] = FindTheta(thispos, targetpos);

		targetpos.x = adj2->mxmy.x;
		if(targetpos.x < mxmy.x)
			targetpos.x = mxmy.x;

		tempAngle.theta[0] = FindTheta(thispos, targetpos);

		tempAngle.proportion = (tempAngle.theta[1] - tempAngle.theta[0]) * PI2I;

		tempAngle.sin01 = sin(tempAngle.proportion);
		tempAngle.cos01 = sin(tempAngle.proportion);

		AngleData.push_back(tempAngle);
	}

	return(0);
}

angleMatch::angleMatch()
{
	targetPt = NULL;
	theta[0] = 0.0;	//in 2D, only use theta[0,1]
	theta[1] = 0.0;
	theta[2] = 0.0;
	theta[3] = 0.0;
	sin01 = 0.0;
	cos01 = 0.0;

	phi[0] = 0.0;
	phi[1] = 0.0;
	phi[2] = 0.0;
	phi[3] = 0.0;

	proportion = 0.0;	//2D, theta1-theta0, 3D, surface area of cone end
}

//these are needed for sorting the states in the binary tree.  Default via values, but if tied in value, defer to the band
bool ESSort::operator <(const ESSort &b) const
{
	if(val < b.val)
		return(true);
	else if(val > b.val)
		return(false);

	if(ref == b.ref)	//they are ==!
		return(false);

	if(ref == VALENCE)
		return(true);	//by default, say the valence band is less than the conduction band

	return(false);
}

bool ESSort::operator >(const ESSort &b) const
{
	if(val > b.val)
		return(true);
	else if(val < b.val)
		return(false);

	if(ref == b.ref)	//they are ==!
		return(false);

	if(ref == CONDUCTION)
		return(true);	//by default, say the conduction band is greater than the conduction band

	return(false);
}

bool ESSort::operator <=(const ESSort &b) const
{
	if(val < b.val)
		return(true);
	else if(val > b.val)
		return(false);

	if(ref == b.ref)	//they are ==!
		return(true);

	if(ref == VALENCE)
		return(true);	//by default, say the valence band is less than the conduction band

	return(false);
}

bool ESSort::operator >=(const ESSort &b) const
{
	if(val > b.val)
		return(true);
	else if(val < b.val)
		return(false);

	if(ref == b.ref)	//they are ==!
		return(true);

	if(ref == CONDUCTION)
		return(true);	//by default, say the conduction band is greater than the conduction band

	return(false);
	//otherwise they have the same value. Then sort by true/false of reference.
}

bool ESSort::operator ==(const ESSort &b) const
{
	if(val == b.val && ref == b.ref)
		return(true);

	return(false);
}




SRHRateConstants::SRHRateConstants()
{
	targetState = NULL;
	sigmavelocity = 0;
	rate = 0;
	invRate = NULL;
}

SRHRateConstants::~SRHRateConstants()
{
	targetState = NULL;
	sigmavelocity = 0;
	rate = 0;
	invRate = NULL;
}

int Point::ResetFermiEnergies(bool CalcELevels, int occtype, double accuracy, bool CalcPoint)
{
	
	//the following is based on the premise that it is OK to average the fermi energies of all the said points, and does a weighted
	//average based on the number of states in each.  This is INHERANTLY WRONG.  There is not such thing as a feri level
	//when the system is not in equilibrium. It just simply does not exist.  You can have quasi fermi levels because the electrons
	//and holes may be in equilibrium with one another seaparately, but if the system is not in equilibrium, it is really a failed concept.

	//therefore, just go back to the original way of calculating the fermi energies.



	if(CalcELevels)	//recalculate all the individual fermi energies for each of the bands. Sort of a reset to make sure no drifting erro=rs
	{
		for(std::multimap<MaxMin<double>,ELevel>::iterator itCB = cb.energies.begin(); itCB != cb.energies.end(); ++itCB)
			itCB->second.UpdateAbsFermiEnergy(occtype);

		for(std::multimap<MaxMin<double>,ELevel>::iterator itVB = vb.energies.begin(); itVB != vb.energies.end(); ++itVB)
			itVB->second.UpdateAbsFermiEnergy(occtype);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);


	}
	//the above is just to make sure the states have their fermi energies calculated appropriately.

	//and then there's this, making all the following code as irrelevant as it really is :/

	if(CalcPoint)
	{
		FindFermiPt(*this, accuracy);	//Load Ef

		//now see if we can outright avoid doing the Efn and Efp calculations once Ef is loaded
		bool limitEfn = FindQuasiFermiPt(*this, accuracy, CONDUCTION);
		bool limitEfp = FindQuasiFermiPt(*this, accuracy, VALENCE);


		//the eUSe needs to be updated appropriately
		//actually, that might be screwing everyhing up for steady state calcs, though it likely makes some smoother
		for(std::multimap<MaxMin<double>,ELevel>::iterator itCB = cb.energies.begin(); itCB != cb.energies.end(); ++itCB)
			itCB->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(itCB->second), fermin);

		for(std::multimap<MaxMin<double>,ELevel>::iterator itVB = vb.energies.begin(); itVB != vb.energies.end(); ++itVB)
			itVB->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(itVB->second), fermip);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = acceptorlike.begin(); it != acceptorlike.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(it->second), fermi);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = donorlike.begin(); it != donorlike.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(it->second), fermi);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = CBTails.begin(); it != CBTails.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(it->second), fermi);

		for(std::multimap<MaxMin<double>,ELevel>::iterator it = VBTails.begin(); it != VBTails.end(); ++it)
			it->second.UpdateAbsFermiEnergy(occtype);
			//SetabsEUseFromFermi(&(it->second), fermi);
		
		if(limitEfn)	//the fermi level may be anything lower than the current value - a good guess is probably Ef.
		{	//this assumes that the carrier was in the bottom state for at least 1e-19 seconds of the simulation (first timestep)
			if(fermi < fermin)	//first try to set it to the point's fermi level
				fermin = fermi;
			else if(fermip < fermin && limitEfp == false)	//assuming Efp is a fixed value, try to use that value if possible
				fermin = fermip;

			//otherwise, just leave it at its current value.
		}

		if(limitEfp)	//same as above, except for Efp and it must be higher than the current value.
		{
			if(fermi > fermip)
				fermip = fermi;
			else if(fermin > fermip && limitEfn == false)
				fermip = fermin;
		}


		UpdateELevelFermi();	//update all the energy states with new probabilities for this fermi level

	/*	

		*/

		/*
		if(cb.GetNumParticles() > 0.0)	//the conduction band is NOT empty
		{
			FindQuasiFermiPt(*this, accuracy, CONDUCTION, curtime);	//Load Efn as athe "best" answer
		}
		else	//there are no particles in the conduction band
		{
			if(cb.energies.start)	//there is a CB...
			{
				Tree<ELevel,double>* bandmin = cb.energies.start->SearchLeft();	//get the band minimum target
				fermin = bandmin->Data.AbsFermiEnergy;
			}
			else	//it's a vacuum or something funny.
				fermin = fermi;				
		}
		

		if(vb.GetNumParticles() > 0.0)	//the valence band is NOT empty
		{
			FindQuasiFermiPt(*this, accuracy, VALENCE, curtime);	//Load Efp as athe "best" answer
		}
		else	//there are no particles in the valence band, so do a "fast" simplified calculation.
		{
			if(vb.energies.start)	//there is a CB...
			{
				Tree<ELevel,double>* bandmin = vb.energies.start->SearchLeft();	//get the band minimum target AbsFermi. This will be the highest Efp.
				fermip = bandmin->Data.AbsFermiEnergy;	//note, energies are referenced to VB.
			}
			else	//it's a vacuum or something funny.
				fermip = fermi;
		}
		*/
	}		//end calculate point values

	return(0);
}

bool ELevel::IsContactSink(void)
{
	if(pt.contactData)
		return(pt.contactData->IsContactSink());

	return(false);
}

//////////////////////////////////// ELevel protected variables
void ELevel::SetDensity(double min, double use, double max, int which)
{
	if (which & ELEVEL_DENS_MIN)
		densityMin = min;
	if (which & ELEVEL_DENS_USE)
		densityUse = use;
	if (which & ELEVEL_DENS_MAX)
		densityMax = max;
	return;
}



bool ELevel::SwapOccCarrierType(int which)
{
	if(which == BEGINOCCTYPE)
	{
		if(beginoccstates > 0.99 * numstates)	//going to be losing accuracy! record the smaller number
		{
			//swap 'em
			BegoccStatesisCarrierType = !BegoccStatesisCarrierType;
			beginoccstates = numstates - beginoccstates;
			return(true);
		}
	}
	else if (which == ENDOCCTYPE)
	{
		if(endoccstates > 0.99 * numstates)	//going to be losing accuracy! record the smaller number
		{
			//swap 'em
			EndoccStatesisCarrierType = !EndoccStatesisCarrierType;
			endoccstates = numstates - endoccstates;
			return(true);
		}
	}
	else if (which == PREVOCCTYPE)
	{
		if (prevOcc > 0.99 * numstates)	//going to be losing accuracy! record the smaller number
		{
			//swap 'em
			prevOccisCarrierType = !prevOccisCarrierType;
			prevOcc = numstates - prevOcc;
			return(true);
		}
	}
	return(false);
}

double ELevel::SetNumstates(double value)
{
	numstates = value;
	//SetDoubleNearestInt(numstates);
	if(numstates < 0)
	{
		ProgressDouble(11,numstates);
		numstates = 0.0;
	}
	updateRelFermi = false;
	return(numstates);
}

double ELevel::GetNumstates(void)
{
	return(numstates);
}

double ELevel::GetEndOccStates(void)
{
	if(EndoccStatesisCarrierType)
		return(endoccstates);

	return(numstates - endoccstates);
}

double ELevel::SetEndOccStates(double value)
{
	endoccstates = value;
	EndoccStatesisCarrierType = true;

//	SetDoubleNearestInt(endoccstates);
	if(endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if(endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}
	updateRelFermi = false;

	if(SwapOccCarrierType(ENDOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - endoccstates);	
	return(endoccstates);
}

double ELevel::SetEndOccStatesOpposite(double value)
{
	endoccstates = value;
	EndoccStatesisCarrierType = false;

//	SetDoubleNearestInt(endoccstates);
	if(endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if(endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}
	updateRelFermi = false;

	if(SwapOccCarrierType(ENDOCCTYPE))	//tests to see if it needs to swap or not.
		return(endoccstates);	//if swapped, the occupancy matches endocc
	return(numstates -endoccstates); //occupancy doesn't match
}

double ELevel::AddEndOccStates(double value)
{
	if(EndoccStatesisCarrierType)
		endoccstates += value;
	else
		endoccstates -= value;

//	SetDoubleNearestInt(endoccstates);
	if(endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if(endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}
	updateRelFermi = false;
	if(SwapOccCarrierType(ENDOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - endoccstates);	
	return(endoccstates);
}

double ELevel::RemoveEndOccStates(double value)
{
	if(EndoccStatesisCarrierType)
		endoccstates -= value;
	else
		endoccstates += value;
//	SetDoubleNearestInt(endoccstates);
	if(endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if(endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}
	updateRelFermi = false;
	if(SwapOccCarrierType(ENDOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - endoccstates);	
	return(endoccstates);
}

double ELevel::SetThermalOcc(double value)
{
	thermalOcc = value;
	thermalAvail = numstates - value;
	return(thermalOcc);
}

double ELevel::SetThermalAvail(double value)
{
	thermalAvail = value;
	thermalOcc = numstates - value;
	return(thermalOcc);
}

double ELevel::SetBeginOccStates(double value)
{
	beginoccstates = value;
	BegoccStatesisCarrierType = true;

	if(beginoccstates < 0)
	{
		ProgressDouble(13, beginoccstates);
		beginoccstates = 0.0;
	}
	if(beginoccstates > numstates)
	{
		ProgressDouble(19, beginoccstates);
		beginoccstates = numstates;
	}
	updateRelFermi = false;

	if(SwapOccCarrierType(BEGINOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - beginoccstates);	
	return(beginoccstates);
}

double ELevel::SetBeginOccStatesOpposite(double value)
{
	beginoccstates = value;
	BegoccStatesisCarrierType = false;

	if(beginoccstates < 0)
	{
		ProgressDouble(13, beginoccstates);
		beginoccstates = 0.0;
	}
	if(beginoccstates > numstates)
	{
		ProgressDouble(19, beginoccstates);
		beginoccstates = numstates;
	}
	updateRelFermi = false;

	if(SwapOccCarrierType(BEGINOCCTYPE))	//tests to see if it needs to swap or not.
		return(beginoccstates);	//if swapped, beginocc now holds the actual occupancy
	return(numstates - beginoccstates);	//otherwise, it doesn't so send in opposing value
}

double ELevel::SetPrevOcc(double value)
{
	prevOcc = value;
	prevOccisCarrierType = true;

	if (prevOcc < 0)
	{
		ProgressDouble(13, prevOcc);
		prevOcc = 0.0;
	}
	if (prevOcc > numstates)
	{
		ProgressDouble(19, prevOcc);
		prevOcc = numstates;
	}
	if (SwapOccCarrierType(PREVOCCTYPE))
		return(numstates - prevOcc);
	return(prevOcc);
}

double ELevel::SetPrevOccOpposite(double value)
{
	prevOcc = value;
	prevOccisCarrierType = false;

	if (prevOcc < 0)
	{
		ProgressDouble(13, prevOcc);
		prevOcc = 0.0;
	}
	if (prevOcc > numstates)
	{
		ProgressDouble(19, prevOcc);
		prevOcc = numstates;
	}
	if (SwapOccCarrierType(PREVOCCTYPE))
		return(prevOcc);
	return(numstates - prevOcc);
}

double ELevel::GetPrevOcc(void)
{
	if (prevOccisCarrierType)
		return(prevOcc);
	return(numstates - prevOcc);
}

double ELevel::GetPrevOccOpposite(void)
{
	if (!prevOccisCarrierType)
		return(prevOcc);
	return(numstates - prevOcc);
}

double ELevel::AddOccPrevToEnd(double value)
{
	if (EndoccStatesisCarrierType == prevOccisCarrierType)
	{
		if (EndoccStatesisCarrierType == true) //does value match?
			endoccstates = prevOcc + value;
		else
			endoccstates = prevOcc - value;
	}
	else
	{
		if (EndoccStatesisCarrierType == true) //which means prevOcc==false
			endoccstates = numstates - prevOcc + value;
		else //which mean sprevOcc = true, but endocc is not. Change it so it is!
		{
			endoccstates = prevOcc + value;
			EndoccStatesisCarrierType = true;
		}
	}
	if (endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if (endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}

	SwapOccCarrierType(ENDOCCTYPE);
	if (EndoccStatesisCarrierType)
		return(endoccstates);
	return(numstates - endoccstates);
}

double ELevel::AddAvailPrevToEnd(double value)
{
	if (EndoccStatesisCarrierType == prevOccisCarrierType)
	{
		if (EndoccStatesisCarrierType == true) //does value match?
			endoccstates = prevOcc - value; //but were adding opposite...
		else
			endoccstates = prevOcc + value;
	}
	else
	{
		if (EndoccStatesisCarrierType == true) //which means prevOcc==false
			endoccstates = numstates - prevOcc - value;
		else //which mean sprevOcc = true, but endocc is not. Change it so it is!
		{
			endoccstates = prevOcc - value;
			EndoccStatesisCarrierType = true;
		}
	}
	if (endoccstates < 0)
	{
		ProgressDouble(12, endoccstates);
		endoccstates = 0.0;
	}
	if (endoccstates > numstates)
	{
		ProgressDouble(18, endoccstates);
		endoccstates = numstates;
	}

	SwapOccCarrierType(ENDOCCTYPE);
	if (EndoccStatesisCarrierType)
		return(endoccstates);
	return(numstates - endoccstates);
}

double ELevel::AddBeginOccStates(double value)
{
	if(BegoccStatesisCarrierType)
		beginoccstates += value;
	else
		beginoccstates -= value;

	if(beginoccstates < 0)
	{
		ProgressDouble(13, beginoccstates);
//		beginoccstates = 0.0;
	}
	if(beginoccstates > numstates)
	{
		ProgressDouble(19, beginoccstates);
//		beginoccstates = numstates;
	}
	updateRelFermi = false;

	if(SwapOccCarrierType(BEGINOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - beginoccstates);	
	return(beginoccstates);
}

double ELevel::RemoveBeginOccStates(double value)
{
	if(BegoccStatesisCarrierType)
		beginoccstates -= value;
	else
		beginoccstates += value;

	if(beginoccstates < 0)
	{
		ProgressDouble(13, beginoccstates);
//		beginoccstates = 0.0;
	}
	if(beginoccstates > numstates)
	{
		ProgressDouble(19, beginoccstates);
//		beginoccstates = numstates;
	}
	updateRelFermi = false;
	if(SwapOccCarrierType(BEGINOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - beginoccstates);	
	return(beginoccstates);
}

double ELevel::MultipltyBeginOccStates(double value)
{
	if(BegoccStatesisCarrierType)
		beginoccstates = beginoccstates*value;
	else
	{
		beginoccstates = numstates - (numstates - beginoccstates)*value;
	}
	if(beginoccstates < 0)
	{
		ProgressDouble(13, beginoccstates);
		beginoccstates = 0.0;
	}
	if(beginoccstates > numstates)
	{
		ProgressDouble(19, beginoccstates);
		beginoccstates = numstates;
	}
	updateRelFermi = false;
	if(SwapOccCarrierType(BEGINOCCTYPE))	//tests to see if it needs to swap or not. If swapped, beginoccstates is now holding the opposite type
		return(numstates - beginoccstates);	
	return(beginoccstates);
}

double ELevel::GetBeginOccStates(void)
{
	if(BegoccStatesisCarrierType)
		return(beginoccstates);
	return(numstates - beginoccstates);
}

double ELevel::GetBeginElec(void)
{
	if(BegoccStatesisCarrierType)
	{
		if(carriertype == ELECTRON)
			return(beginoccstates);
		else
			return(numstates-beginoccstates);
	}
	else //not storing the carrier type
	{
		if(carriertype == HOLE)
			return(beginoccstates);
		else
			return(numstates-beginoccstates);
	}

	return(numstates - beginoccstates);
}

double ELevel::GetEndElec(void)
{
	if(EndoccStatesisCarrierType)
	{
		if(carriertype == ELECTRON)
			return(endoccstates);
		return(numstates - endoccstates);
	}
	else
	{
		if(carriertype == ELECTRON)
			return(numstates - endoccstates);
		return(endoccstates);
	}
}

double ELevel::GetBeginHole(void)
{
	if(BegoccStatesisCarrierType)
	{
		if(carriertype == HOLE)
			return(beginoccstates);
		return(numstates - beginoccstates);
	}
	else
	{
		if(carriertype == HOLE)	//it's storing the opposite, or electrons
			return(numstates - beginoccstates);
		return(beginoccstates);	//the type is electrons, but it's storing the opposite
	}
}

double ELevel::GetEndHole(void)
{
	if(EndoccStatesisCarrierType)
	{
		if(carriertype == HOLE)
			return(endoccstates);
		return(numstates - endoccstates);
	}
	else
	{
		if(carriertype == HOLE)
			return(numstates - endoccstates);
		return(endoccstates);
	}
}

double ELevel::GetBeginAvailable(void)
{
	if(BegoccStatesisCarrierType)
		return(numstates - beginoccstates);
	else
		return(beginoccstates);
}

double ELevel::GetEndAvailable(void)
{
	if(EndoccStatesisCarrierType)
		return(numstates - endoccstates);
	else
		return(endoccstates);
}

double ELevel::GetOccupancy()
{
	if(EndoccStatesisCarrierType)
		return(endoccstates);	//return the instantaneous value of the levels occupancy
	else
		return(numstates-endoccstates);
}

int ELevel::GetOccupancyEH(int whichsource, double& elec, double& hole)
{
	if(whichsource == RATE_OCC_BEGIN)
	{
		if(carriertype == ELECTRON)
		{
			if(BegoccStatesisCarrierType)
			{
				elec = beginoccstates;
				hole  = numstates - beginoccstates;
			}
			else
			{
				hole = beginoccstates;
				elec = numstates - beginoccstates;
			}
		}
		else
		{
			if(BegoccStatesisCarrierType)
			{
				hole = beginoccstates;
				elec = numstates - beginoccstates;
			}
			else
			{
				elec = beginoccstates;
				hole  = numstates - beginoccstates;
			}
		}
	}
	else if(whichsource == RATE_OCC_END)
	{
		if(carriertype == ELECTRON)
		{
			if(EndoccStatesisCarrierType)
			{
				elec = endoccstates;
				hole  = numstates - endoccstates;
			}
			else
			{
				hole = endoccstates;
				elec = numstates - endoccstates;
			}
		}
		else
		{
			if(EndoccStatesisCarrierType)
			{
				hole = endoccstates;
				elec = numstates - endoccstates;
			}
			else
			{
				elec = endoccstates;
				hole  = numstates - endoccstates;
			}
		}
	}
	else if(whichsource == RATE_OCC_MAX)
	{
		if(carriertype == ELECTRON)
		{
			if(BegoccStatesisCarrierType)
			{
				elec = maxOcc;
				hole  = numstates - maxOcc;
			}
			else
			{
				hole = maxOcc;
				elec = numstates - maxOcc;
			}
		}
		else
		{
			if(BegoccStatesisCarrierType)
			{
				hole = maxOcc;
				elec = numstates - maxOcc;
			}
			else
			{
				elec = maxOcc;
				hole  = numstates - maxOcc;
			}
		}
	}
	else if(whichsource == RATE_OCC_MIN)
	{
		if(carriertype == ELECTRON)
		{
			if(BegoccStatesisCarrierType)
			{
				elec = minOcc;
				hole  = numstates - minOcc;
			}
			else
			{
				hole = minOcc;
				elec = numstates - minOcc;
			}
		}
		else
		{
			if(BegoccStatesisCarrierType)
			{
				hole = minOcc;
				elec = numstates - minOcc;
			}
			else
			{
				elec = minOcc;
				hole  = numstates - minOcc;
			}
		}
	}
	else if(whichsource == RATE_OCC_TARGET)
	{
		if(carriertype == ELECTRON)
		{
			if(BegoccStatesisCarrierType)
			{
				elec = targetOcc;
				hole  = targetAvail;
			}
			else
			{
				hole = targetOcc;
				elec = targetAvail;
			}
		}
		else
		{
			if(BegoccStatesisCarrierType)
			{
				hole = targetOcc;
				elec = targetAvail;
			}
			else
			{
				elec = targetOcc;
				hole  = targetAvail;
			}
		}
	}
	else if(whichsource == RATE_OCC_ENDADJUST)
	{
		if(carriertype == ELECTRON)
		{
			if(EndoccStatesisCarrierType)
			{
				elec = endoccstates + adjustCarrier;
				hole  = numstates - elec;
			}
			else
			{
				hole = targetOcc + adjustCarrier;
				elec = numstates - hole;
			}
		}
		else
		{
			if(EndoccStatesisCarrierType)
			{
				hole = targetOcc + adjustCarrier;
				elec = numstates - hole;
			}
			else
			{
				elec = endoccstates + adjustCarrier;
				hole  = numstates - elec;
			}
		}
	}
	else if(whichsource == RATE_OCC_PREV)
	{
		if(carriertype == ELECTRON)
		{
			if(prevOccisCarrierType)
			{
				elec = prevOcc;
				hole  = numstates - elec;
			}
			else
			{
				hole = prevOcc;
				elec = numstates - hole;
			}
		}
		else
		{
			if(prevOccisCarrierType)
			{
				hole = prevOcc;
				elec = numstates - hole;
			}
			else
			{
				elec = prevOcc;
				hole  = numstates - elec;
			}
		}
	}
	else if(whichsource == RATE_OCC_THEQ)
	{
		if(carriertype == ELECTRON)
		{
			elec = thermalOcc;
			hole = thermalAvail;
		}
		else
		{
			hole = thermalOcc;
			elec = thermalAvail;
		}
	}
	else
	{
		hole = elec = 0.0;
		return(-1);
	}

	if(hole > numstates || elec > numstates || hole < 0.0 || elec < 0.0)
	{
		hole = elec = 0.0;
		return(-1);
	}

	return(0);
}

int ELevel::OccFromAbsEf(double absEf, double& occ, double& avail)
{
	occ = avail = -1.0;	//indicate failure to calculate
	//need to get abs of eUse.
	double absUse = getAbsEUse();
	double degen = imp ? imp->getDegeneracy() : 1.0;
	if(absUse == absEf)
	{
		occ = numstates / (1.0+degen);
		avail = numstates - occ;
		return(0);
	}
	double kTi = (pt.temp> 0.0 ? KBI / pt.temp : 0.0);
	double width = max - min;
	double relEf = carriertype==ELECTRON ? absUse-absEf : absEf-absUse;
	if(kTi == 0.0)	//assume this is a 0K case. Everything above fermi level, unocc, all below, occ
	{
		if (width == 0.0)	//zero temp, completely discrete state, what the hell...
		{
			width = 0.001;	//just give it something to work with...
		}

		occ = 0.5 - relEf / width;
		if (occ > 1.0)
			occ = 1.0;
		if (occ < 0.0)
			occ = 0.0;

		occ = occ * numstates;
		avail = numstates - occ;
		return(0);
	}
	
	if (relEf > 0.0)	//this means occ is lower than avail, and we should calculate occupancy
	{
		relEf = relEf * kTi;
		relEf = degen * exp(-relEf) + 1.0;	//note negative relEf means less carriers
		occ = numstates / relEf;
		avail = numstates - occ;
	}
	else //relEf > 0.0, so that means occupancy is large, so we should calculate availability
	{
		relEf = relEf * kTi;
		relEf = exp(relEf)/degen + 1.0;	//note positive relEf means more carriers, so less availability
		avail = numstates / relEf;
		occ = numstates - avail;
	}
	return(0);
}

double ELevel::getAbsEUse(void)
{
	double absUse = eUse;
	if(carriertype == ELECTRON)
	{
		if(bandtail)
			return (pt.cb.bandmin - eUse);
		if(imp)
		{
			if(imp->isCBReference())
				return(pt.cb.bandmin - eUse);
			else
				return(pt.vb.bandmin + eUse);
		}
		return(pt.cb.bandmin + eUse);
	}
	else
	{
		if(bandtail)
			return (pt.vb.bandmin + eUse);
		if(imp)
		{
			if (imp->isCBReference())
				return(pt.cb.bandmin - eUse);
			else
				return(pt.vb.bandmin + eUse);
		}
		return(pt.vb.bandmin - eUse);
	}
}

int ELevel::UpdateMaxMinTargets(double relEf, double EfSpread)	//relEf is assumed that >0 means large occupancy
{
	double occupancy;
	double temp = pt.temp;
	double kTi = temp>0.0 ? KBI/temp : 0.0;
	if(EfSpread == 0.0)
		EfSpread = 1e-6;	//
	EfSpread = fabs(EfSpread);

	if(imp)
		occupancy = OccAvailFromRelEf(relEf+EfSpread, numstates, kTi, max-min, imp->getDegeneracy());
	else
		occupancy = OccAvailFromRelEf(relEf+EfSpread,numstates, kTi, max-min);

	if(relEf >= 0.0)
	{
		tAvailMax = occupancy;
		tOccMax = numstates - occupancy;
	}
	else
	{
		tOccMax = occupancy;
		tAvailMax = numstates-occupancy;
	}
	if(imp)
		occupancy = OccAvailFromRelEf(relEf - EfSpread, numstates, kTi, max - min, imp->getDegeneracy());
	else
		occupancy = OccAvailFromRelEf(relEf-EfSpread,numstates, kTi, max-min);

	if(relEf >= 0.0)
	{
		tAvailMin = occupancy;
		tOccMin = numstates - occupancy;
	}
	else
	{
		tOccMin = occupancy;
		tAvailMin = numstates-occupancy;
	}

	Swap(tOccMin, tOccMax);
	Swap(tAvailMin, tAvailMax);

	return(0);
}
//note: This assumes RelEf > 0 means occupancy is large, regardless of whether carrier type is e- or hole
//the calcrelative Ef takes carrier type into account. Legacy code, and is therefore slightly incompatible with this function
int ELevel::SetOccFromRelEf(int which, double relEf)
{
	double occupancy;
	bool OccMatches;
	double temp = pt.temp;
	double kTi = temp>0.0 ? KBI/temp : 0.0;
	if(imp)
		occupancy = OccAvailFromRelEf(relEf, numstates, kTi, max - min, imp->getDegeneracy());
	else
		occupancy = OccAvailFromRelEf(relEf,numstates, kTi, max-min);

	if (relEf >= 0.0) //it is mostly full of carriers, so occupancy holds the smaller value: avail
		OccMatches = false;
	else //typically true - curEf < 0, so the occupancy is smaller and that's what gets stored in occupancy
		OccMatches = true;
	
	if(which == RATE_OCC_BEGIN)
	{
		beginoccstates = occupancy;
		BegoccStatesisCarrierType = OccMatches;
	}
	else if(which == RATE_OCC_END)
	{
		EndoccStatesisCarrierType = OccMatches;
		endoccstates = occupancy;
	}
	else if(which == RATE_OCC_MAX)
	{
		maxOcc = occupancy;
	}
	else if(which == RATE_OCC_MIN)
	{
		minOcc = occupancy;
	}
	else if(which == RATE_OCC_TARGET)
	{
		if(OccMatches)
		{
			targetOcc = occupancy;
			targetAvail = numstates - occupancy;
		}
		else
		{
			targetAvail = occupancy;
			targetOcc = numstates - occupancy;
		}
	}
	else if(which == RATE_OCC_PREV)
	{
		prevOcc = occupancy;
		prevOccisCarrierType = OccMatches;
	}
	else
		return(0);

	return(1);
}

int ELevel::GetOccupancyOcc(int whichsource, double& occ, double& avail)
{
	if(whichsource == RATE_OCC_BEGIN)
	{
		if(BegoccStatesisCarrierType)
		{
			occ = beginoccstates;
			avail  = numstates - occ;
		}
		else
		{
			avail = beginoccstates;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_END)
	{
		if(EndoccStatesisCarrierType)
		{
			occ = endoccstates;
			avail  = numstates - occ;
		}
		else
		{
			avail = endoccstates;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_MAX)
	{
		if(BegoccStatesisCarrierType)
		{
			occ = maxOcc;
			avail  = numstates - occ;
		}
		else
		{
			avail = maxOcc;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_MIN)
	{
		if(BegoccStatesisCarrierType)
		{
			occ = minOcc;
			avail  = numstates - occ;
		}
		else
		{
			avail = minOcc;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_TARGET)
	{
		occ = targetOcc;
		avail  = targetAvail;
	}
	else if(whichsource == RATE_OCC_ENDADJUST)
	{
		if(EndoccStatesisCarrierType)
		{
			occ = endoccstates + adjustCarrier;
			avail  = numstates - occ;
		}
		else
		{
			avail = endoccstates + adjustCarrier;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_PREV)
	{
		if(prevOccisCarrierType)
		{
			occ = prevOcc;
			avail  = numstates - occ;
		}
		else
		{
			avail = prevOcc;
			occ  = numstates - avail;
		}
	}
	else if(whichsource == RATE_OCC_THEQ)
	{
		occ = thermalOcc;
		avail = thermalAvail;
	}
	else
	{
		occ = avail = 0.0;
		return(-1);
	}

	if(occ > numstates || avail > numstates || occ < 0.0 || avail < 0.0)
	{
		avail = occ = 0.0;
		return(-1);
	}

	return(0);
}

int ELevel::TransferEndToBegin(void)
{
//	if(BegoccStatesisCarrierType == EndoccStatesisCarrierType)
	beginoccstates = endoccstates;
	BegoccStatesisCarrierType = EndoccStatesisCarrierType;
//	else
//	{
//		beginoccstates = numstates - endoccstates;
//	}
//	SwapOccCarrierType(BEGINOCCTYPE);
	return(0);
}

int ELevel::TransferBeginToEnd(void)
{
	endoccstates = beginoccstates;
	EndoccStatesisCarrierType = BegoccStatesisCarrierType;
	/*
	if(BegoccStatesisCarrierType == EndoccStatesisCarrierType)
		endoccstates = beginoccstates;
	else
	{
		endoccstates = numstates - beginoccstates;
	}
	SwapOccCarrierType(ENDOCCTYPE);
	*/
	return(0);
}
int ELevel::GetThermalType(void)
{
	if(carriertype == ELECTRON)
	{
		if(imp)
			return(THERMALIZE_DON);
		else if(bandtail)
			return(THERMALIZE_CBT);
		else
			return(THERMALIZE_CB);
	}
	else
	{
		if(imp)
			return(THERMALIZE_ACP);
		else if(bandtail)
			return(THERMALIZE_VBT);
		else
			return(THERMALIZE_VB);
	}
	return(THERMALIZE_NONE);	//shouldn't ever run this, but some compilers are persnickety
}

double Band::AddNumParticles(double value)
{
	numparticles += value;
	if(value < 0)
	{
		ProgressDouble(22, value);
	}
	if(numparticles < 0)
	{
		ProgressDouble(21, numparticles);
		numparticles = 0.0;
	}

	//MORE DEBUG
	if(value != value)
	{
		ProgressCheck(28, false);
	}

	return(numparticles);
}

double Band::GetNumParticles(void)
{
	return(numparticles);
}

double Band::SetNumParticles(double value)
{
	numparticles = value;
	if(value < 0)
	{
		ProgressDouble(21, numparticles);
		numparticles = 0.0;
	}
	
	return(numparticles);
}

double Band::RemoveNumParticles(double value)
{
	numparticles -= value;
	if(numparticles < 0)
	{
		ProgressDouble(21, numparticles);
		numparticles = 0.0;
	}
	if(value != value)
	{
		ProgressCheck(28, false);
	}
	//DEBUG ONLY!
/*	double occmaxtemp = 0.0;
	std::vector<ELevel*> tempE;
	tempE.reserve(energies.size);
	energies.GenerateSortPtr(tempE);
	for(unsigned int i=0; i<energies.size; i++)
	{
		occmaxtemp += tempE[i]->GetOccMaxStates();
	}
	if(fabs(occmaxtemp - numparticles) > 0.5)
	{
		int i=0;
	}
	*/
	return(numparticles);
}

bool ELevelTransfer::operator ==(const ELevelTransfer &b) const
{
	if(source != b.source)
		return(false);

	if(carrier != b.carrier)
		return(false);

	return(true);
}

bool ELevelTransfer::operator !=(const ELevelTransfer &b) const
{
	if(source != b.source)
		return(true);

	if(carrier != b.carrier)
		return(true);

	return(false);
}

bool ELevelTransfer::operator >(const ELevelTransfer &b) const
{
	if(source && b.source)
	{
		if(source->uniqueID > b.source->uniqueID)
			return(true);
		else if(source->uniqueID < b.source->uniqueID)
			return(false);
	}
	else if(source && b.source == NULL)
		return(true);
	else if(source == NULL && b.source)
		return(false);

	if(carrier == true && b.carrier == false)
		return(true);
	else if(carrier == false && b.carrier == true)	//probably will be optimized out.
		return(false);	//but maintain in case of copy-paste issues


	return(false);	//they're equal
}

bool ELevelTransfer::operator <(const ELevelTransfer &b) const
{
	if(source && b.source)
	{
		if(source->uniqueID < b.source->uniqueID)
			return(true);
		else if(source->uniqueID > b.source->uniqueID)
			return(false);
	}
	else if(source && b.source == NULL)
		return(false);
	else if(source == NULL && b.source)
		return(true);

	if(carrier == false && b.carrier == true)
		return(true);
	else if(carrier == true && b.carrier == false)	//probably will be optimized out.
		return(false);	//but maintain in case of copy-paste issues

	return(false);	//they're equal
}

bool ELevelTransfer::operator <=(const ELevelTransfer &b) const
{
	if(source && b.source)
	{
		if(source->uniqueID < b.source->uniqueID)
			return(true);
		else if(source->uniqueID > b.source->uniqueID)
			return(false);
	}
	else if(source && b.source == NULL)
		return(false);
	else if(source == NULL && b.source)
		return(true);

	if(carrier == false && b.carrier == true)
		return(true);
	else if(carrier == true && b.carrier == false)	//probably will be optimized out.
		return(false);	//but maintain in case of copy-paste issues

	return(true);	//they're equal
}

bool ELevelTransfer::operator >=(const ELevelTransfer &b) const
{
	if(source && b.source)
	{
		if(source->uniqueID > b.source->uniqueID)
			return(true);
		else if(source->uniqueID < b.source->uniqueID)
			return(false);
	}
	else if(source && b.source == NULL)
		return(true);
	else if(source == NULL && b.source)
		return(false);

	if(carrier == true && b.carrier == false)
		return(true);
	else if(carrier == false && b.carrier == true)	//probably will be optimized out.
		return(false);	//but maintain in case of copy-paste issues


	return(true);	//they're equal
}


GroupThermalizationData::GroupThermalizationData()
{
	Clear();
}

GroupThermalizationData::~GroupThermalizationData()
{
	Clear();
}

void GroupThermalizationData::Clear()
{
	imp=NULL;
	minE = maxE = fermi = holeSum = elecSum = EfLowSum = EfHighSum = 0.0;
	iterUpdate = typeStates = 0;
	eLevs.clear();
	elec.clear();
	hole.clear();
	EfLowElec.clear();
	EfHighHole.clear();
	return;
}

bool GroupThermalizationData::Valid(double minEsrc, double maxEsrc)
{
	if(minEsrc <= maxE && maxEsrc >= minE)
		return(true);
	return(false);
}

void GroupThermalizationData::Initialize(int type, int thermalAllow, Point* pt, Impurity* srcImp)
{
	Clear();	//make sure all the data is cleared out
	if(pt==NULL)
		return;

	double localMin, localMax;
	bool PosEnergy;
	localMin = 1e100;
	localMax = -1e100;
	
	imp = srcImp;	//may be null
	typeStates = THERMALIZE_NONE;	//none default
	if(type == THERMALIZE_VB && (thermalAllow & THERMALIZE_VB))
	{
		maxE = minE = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator eLev = pt->vb.energies.rbegin(); eLev != pt->vb.energies.rend(); ++eLev)
		{
			eLevs.push_back(&(eLev->second));
			if(-eLev->second.max < minE)
				minE = -eLev->second.max;

			if(eLev->second.min < localMin)
				localMin = eLev->second.min;
			if(eLev->second.max > localMax)
				localMax = eLev->second.max;

		}
		PosEnergy = false;
		imp = NULL;
		typeStates = THERMALIZE_VB;
	}
	else if(type == THERMALIZE_CB && (thermalAllow & THERMALIZE_CB))
	{
		maxE = minE = pt->eg;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->cb.energies.begin(); eLev != pt->cb.energies.end(); ++eLev)
		{
			eLevs.push_back(&(eLev->second));
			if(eLev->second.max + minE > maxE)
				maxE = eLev->second.max + minE;

			if(eLev->second.min < localMin)
				localMin = eLev->second.min;
			if(eLev->second.max > localMax)
				localMax = eLev->second.max;
		}
		PosEnergy = true;
		imp = NULL;
		typeStates = THERMALIZE_CB;
	}
	else if(type == THERMALIZE_VBT && (thermalAllow & THERMALIZE_VBT))
	{
		maxE = minE = 0.0;
		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->VBTails.begin(); eLev != pt->VBTails.end(); ++eLev)
		{
			eLevs.push_back(&(eLev->second));
			if(eLev->second.max > maxE)
				maxE = eLev->second.max;

			if(eLev->second.min < localMin)
				localMin = eLev->second.min;
			if(eLev->second.max > localMax)
				localMax = eLev->second.max;
		}
		imp = NULL;
		PosEnergy = true;
		typeStates = THERMALIZE_VBT;
	}
	else if(type == THERMALIZE_CBT && (thermalAllow & THERMALIZE_CBT))
	{
		maxE = minE = pt->eg;
		for(std::multimap<MaxMin<double>,ELevel>::reverse_iterator eLev = pt->CBTails.rbegin(); eLev != pt->CBTails.rend(); ++eLev)
		{
			eLevs.push_back(&(eLev->second));
			if(pt->eg - eLev->second.max < minE)
				minE = pt->eg - eLev->second.max;

			if(eLev->second.min < localMin)
				localMin = eLev->second.min;
			if(eLev->second.max > localMax)
				localMax = eLev->second.max;
		}
		imp = NULL;
		PosEnergy = false;
		typeStates = THERMALIZE_CBT;
	}
	else if(type == THERMALIZE_ACP && (thermalAllow & THERMALIZE_ACP))
	{
		if(imp == NULL)
			return;

		maxE = -1e100;
		minE = 1e100;

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->acceptorlike.begin(); eLev != pt->acceptorlike.end(); ++eLev)
		{
			if(eLev->second.imp == imp)
			{
				eLevs.push_back(&(eLev->second));
				if(eLev->second.max > maxE)
					maxE = eLev->second.max;
				if(eLev->second.min < minE)
					minE = eLev->second.min;

				if(eLev->second.min < localMin)
					localMin = eLev->second.min;
				if(eLev->second.max > localMax)
					localMax = eLev->second.max;
			}
		}
		PosEnergy = true;
		typeStates = THERMALIZE_ACP;

	}
	else if(type == THERMALIZE_DON && (thermalAllow & THERMALIZE_DON))
	{
		if(imp == NULL)
			return;
		maxE = -1e100;
		minE = 1e100;

		for(std::multimap<MaxMin<double>,ELevel>::iterator eLev = pt->donorlike.begin(); eLev != pt->donorlike.end(); ++eLev)
		{
			if(eLev->second.imp == imp)
			{
				eLevs.push_back(&(eLev->second));
				if(pt->eg - eLev->second.min > maxE)
					maxE = pt->eg - eLev->second.min;
				if(pt->eg - eLev->second.max < minE)
					minE = pt->eg - eLev->second.max;

				if(eLev->second.min < localMin)
					localMin = eLev->second.min;
				if(eLev->second.max > localMax)
					localMax = eLev->second.max;
			}
		}
		typeStates = THERMALIZE_DON;
		PosEnergy = false;
	}

	unsigned int sz = eLevs.size();
	elec.resize(sz,0.0);	//create the electron/hole vectors with value 0
	hole.resize(sz,0.0);
	

	//now just set this as it should only be set once
	EfHighHole.resize(sz, 0.0);
	EfLowElec.resize(sz, 0.0);
	EfHighSum = EfLowSum = 0.0;
	if(PosEnergy) //the min is actually the min
	{
		double weight;
		double kTi = pt->temp>0.0 ? KBI / pt->temp : KBI *10.0;	//just say temp is 0.1 K
		for(unsigned int i=0; i<sz; ++i)
		{
			weight = eLevs[i]->GetNumstates() * exp(-(eLevs[i]->eUse - (localMin))*kTi);
			EfLowElec[i] = weight;
			EfLowSum += weight;
			weight = eLevs[i]->GetNumstates() * exp(-(localMax - eLevs[i]->eUse)*kTi);
			EfHighHole[i] = weight;
			EfHighSum += weight;
		}
	}
	else
	{
		double weight;
		double kTi = pt->temp>0.0 ? KBI / pt->temp : KBI *10.0;	//just say temp is 0.1 K
		for(unsigned int i=0; i<sz; ++i)
		{
			weight = eLevs[i]->GetNumstates() * exp(-(eLevs[i]->eUse - (localMin))*kTi);
			EfHighHole[i] = weight;
			EfHighSum += weight;
			weight = eLevs[i]->GetNumstates() * exp(-(localMax - eLevs[i]->eUse)*kTi);
			EfLowElec[i] = weight;
			EfLowSum += weight;
		}
	}

	EfHighSum = (EfHighSum>0.0) ? 1.0/EfHighSum : 0.0;
	EfLowSum = (EfLowSum>0.0) ? 1.0/EfLowSum : 0.0;

	iterUpdate = -1;	//signifiy that this hasn't been updated with good values (note, unsigned int so -1 = largest possible number)

	return;
}

bool GroupThermalizationData::SetNormalize(unsigned long long int iter)
{
	if(iter == iterUpdate)
		return(true);	//the data is good

	elecSum = 0.0;
	holeSum = 0.0;	//first we need to see what we have!

	double degenElec = 1.0;	//default to 1 so no effect
	double degenHole = 1.0;

	const unsigned int sz = eLevs.size();
	if(sz == 0)
		return(false);

	if((typeStates == THERMALIZE_ACP || typeStates == THERMALIZE_DON) && imp == NULL)
		return(false);	//there's no defect/impurity associated with this, so it's gonna be tough to figure out!

	if(elec.size() != hole.size() || hole.size() != sz)	//prevent bad accesses
	{
		elec.resize(sz,0.0);	//create the electron/hole vectors with value 0
		hole.resize(sz,0.0);
	}
	std::vector<double> NStates;	//just get them all loaded in memory right next to one another
	//now load up the initial data
	std::vector<double> Energies;
	Energies.reserve(sz);
	NStates.reserve(sz);

	for(unsigned int i=0; i<sz; ++i)
	{
		eLevs[i]->GetOccupancyEH(RATE_OCC_BEGIN, elec[i], hole[i]);
		NStates.push_back(eLevs[i]->GetNumstates());
		elecSum += elec[i];
		holeSum += hole[i];
	}

	//now things start to get a tad more difficult.
	//We want to quickly determine if the state is already thermalized or not. If so, then don't do a bunch of calculations, leave the elec/hole as they are, and get on with it.
	//otherwise, we want to converge in really fast on what the relative solution may be. Look at absolute min/max relative fermi levels. Convert those to reference the valence band max=0, and see how things stand.

	double maxFermi = -1e100;	//just two ridiculously large values
	double minFermi = 1e100;
	double difference;
	double testFermi = 0.0;
	Point* pt = &eLevs[0]->getPoint();
	//first, figure out the range of fermi levels
	if(typeStates == THERMALIZE_CBT || typeStates == THERMALIZE_DON)
	{
		double bandgap = pt->eg;
		for(unsigned int i=0; i<sz; ++i)
		{
			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns positive if lots of e-
			//returns E-Ef.
			//Need to translate the fermi level to be Ef - Ev
			//currently Ec - delE - Ef.
			//subtract it: -difference = [Ef - Ec + delE]
			//Ec = Ev + Eg, and Ev=0, so Add bandgap and subtract energy to get Ef-Ev
			Energies.push_back(bandgap - eLevs[i]->eUse);	//Store E-Ev
			if(eLevs[i]->GetBeginOccStates() != 0.0 && eLevs[i]->GetBeginAvailable() != 0.0)	//try to keep lots of iterations down
			{
				difference = Energies[i] + difference;

				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;
				testFermi += difference;
			}
		}

		if(typeStates == THERMALIZE_DON)
		{
			degenElec = imp->getDegeneracy();
			degenHole = 1.0 / degenElec;
		}
	}
	else if(typeStates == THERMALIZE_VBT || typeStates == THERMALIZE_ACP)
	{
		for(unsigned int i=0; i<sz; ++i)
		{
			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns positive if lots of h+
			//Need to translate the fermi level to be Ef - Ev
			//Ev + delE - Ef
			//want Ef - Ev, so -difference = Ef - Ev - delE, simply add delE to -idfference
			Energies.push_back(eLevs[i]->eUse);	//store E-Ev
			if(eLevs[i]->GetBeginOccStates() != 0.0 && eLevs[i]->GetBeginAvailable() != 0.0)	//try to keep lots of iterations down
			{
				difference = Energies[i] - difference;

				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;

				testFermi += difference;
			}
		}

		if(typeStates == THERMALIZE_ACP)
		{
			degenElec = imp->getDegeneracy();
			degenHole = 1.0 / degenElec;
		}
	}
	else if(typeStates == THERMALIZE_CB)
	{
		double bandgap = pt->eg;
		for(unsigned int i=0; i<sz; ++i)
		{
			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns positive lots of e-
			//Need to translate the fermi level to be Ef - Ev
			//currently Ec + delE - Ef.
			//subtract it: -difference = [Ef - Ec - delE] = [Ef - Ev - Eg - delE]
			//Add bandgap and energy, subtract difference
			Energies.push_back(bandgap + eLevs[i]->eUse);
			difference = Energies[i] + difference;

			if(eLevs[i]->GetBeginOccStates() != 0.0 && eLevs[i]->GetBeginAvailable() != 0.0)	//try to keep lots of iterations down
			{
				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;
				testFermi += difference;
			}
		}
	}
	else if(typeStates == THERMALIZE_VB)
	{
		for(unsigned int i=0; i<sz; ++i)
		{
			difference = eLevs[i]->CalcRelFermiEnergy(RATE_OCC_BEGIN, false);	//returns positive lots of h+
			//Need to translate the fermi level to be Ef - Ev
			//currently Ev - delE - Ef.
			//Want Ef-Ev
			//-difference = Ef + delE - Ev
			//subtract eUse as well
			Energies.push_back(-eLevs[i]->eUse);	//store E-Ev
			if(eLevs[i]->GetBeginOccStates() != 0.0 && eLevs[i]->GetBeginAvailable() != 0.0)	//try to keep lots of iterations down
			{
				difference = Energies[i] - difference;
					
				if(difference > maxFermi)
					maxFermi = difference;
				if(difference < minFermi)
					minFermi = difference;
				testFermi += difference;
			}
		}
	}
	else
		return(false);	//this is invalid initialization

	//see if we can avoid some calculations
	if(DoubleEqual(maxFermi, minFermi)==true)
	{
		iterUpdate = iter;
		//it is already well thermalized, and there is no need to adjust any of the states
		//leave elec and holes as they are, update that this has been calculated properly, and
		//get out of the function, indicating success
		NStates.clear();
		Energies.clear();
		return(true);
	}

	//now, try and find the fermi level. This should typically be relatively quick after the first iteration
	//so not going to do anything crazy in fast convergence.

	if(maxFermi == -1e100) //initial value
		maxFermi = (pt->MatProps) ? pt->eg + pt->MatProps->Phi : 2.0 * pt->eg + 5.0;
	if(minFermi == 1e100)
		minFermi = (pt->MatProps) ? -pt->MatProps->Phi : -2.0 * pt->eg - 5.0;

	testFermi = testFermi / double(sz);	//get the average fermi level as a first guess too really hone in
	if(testFermi > maxFermi || testFermi < minFermi)
		testFermi = 0.5 * (maxFermi + minFermi);

	double testElec, testHole;
	testElec = testHole = 0.0;	//they're both not going to equal zero, so the while should still pass
	
	double tmp;
	double beta = KBI / pt->temp;
	while(DoubleEqual(testElec, elecSum)==false || DoubleEqual(testHole, holeSum)==false)
	{
		testElec = testHole = 0.0;

		for(unsigned int i=0; i<sz; ++i)
		{
			tmp = degenElec * exp(beta * (Energies[i] - testFermi));	//E-Ef = (E-Ev) - (Ef-Ev)
			if(MY_IS_FINITE(tmp) == false)
				tmp = 0.0;
			else
			{
				tmp += 1.0;
				tmp = 1.0 / tmp;
			}
			elec[i] = NStates[i] * tmp;
			testElec += elec[i];

			//for holes, 1-f(E) leads to flipping the exponent
			tmp = degenHole * exp(beta * (testFermi-Energies[i]));	//Ef-E
			if(MY_IS_FINITE(tmp) == false)
				tmp = 0.0;
			else
			{
				tmp += 1.0;
				tmp = 1.0 / tmp;
			}
			hole[i] = NStates[i] * tmp;
			testHole += hole[i];
		}


		if(elecSum < holeSum)	//look at the minority carrier to determine
		{
			if(testElec > elecSum)	//then there are too many electrons, the Ef is too high
				maxFermi = testFermi;
			else  //not enough electrons, so push Ef higher
				minFermi = testFermi;
		}
		else
		{
			if(testHole > holeSum)	//there are too many holes, so Ef is too low
				minFermi = testFermi;
			else
				maxFermi = testFermi;
		}
		testFermi = 0.5 * (maxFermi + minFermi);
		if(DoubleEqual(maxFermi,minFermi))
			break;	//as good as we're gonna get.
	}
	
	unsigned int validElec = 0;
	unsigned int validHole = 0;
	for(unsigned int i=0;i<sz;++i)
	{
		if(elec[i]>0.0)
			validElec++;
		if(hole[i]>0.0)
			validHole++;
	}

	//if at least 75% ish is good, it should use the partition set up by the number of electrons (fermi function). Otherwise, it should use a boltzmann distribution
	unsigned int cutoff = (3*sz)/4+1;
	if(validElec < cutoff || validHole < cutoff)
		useElecPartition=false;
	else
		useElecPartition=true;

	iterUpdate = iter;	//flag that this calculation has already been done
	NStates.clear();
	Energies.clear();
	return(true);	//it did have to update the data
}

bool GroupThermalizationData::AssignNormalize(unsigned long long int iter, int which)
{
	if(SetNormalize(iter)==false)
		return(false);

	//the data could is properly set
	unsigned int sz = eLevs.size();
	if(sz != elec.size() || sz != hole.size())
		return(false);

	if(which == RATE_OCC_BEGIN)
	{
		for(unsigned int i=0; i<sz; ++i)
		{
			if(elec[i] < hole[i])	//want to assign by electrons
			{
				if(eLevs[i]->carriertype == ELECTRON)
					eLevs[i]->SetBeginOccStates(elec[i]);
				else
					eLevs[i]->SetBeginOccStatesOpposite(elec[i]);
			}
			else
			{
				if(eLevs[i]->carriertype == HOLE)
					eLevs[i]->SetBeginOccStates(hole[i]);
				else
					eLevs[i]->SetBeginOccStatesOpposite(hole[i]);
			}
		}
	}
	else if(which == RATE_OCC_END)
	{
		for(unsigned int i=0; i<sz; ++i)
		{
			if(elec[i] < hole[i])	//want to assign by electrons
			{
				if(eLevs[i]->carriertype == ELECTRON)
					eLevs[i]->SetEndOccStates(elec[i]);
				else
					eLevs[i]->SetEndOccStatesOpposite(elec[i]);
			}
			else
			{
				if(eLevs[i]->carriertype == HOLE)
					eLevs[i]->SetEndOccStates(hole[i]);
				else
					eLevs[i]->SetEndOccStatesOpposite(hole[i]);
			}
		}
	}
	else
		return(false);

	return(true);
}

bool GroupThermalizationData::CheckType(int type)
{
	return(type == typeStates);
}

int GroupThermalizationData::FindUniqueID(unsigned long long int id, unsigned int guess)
{
	if(guess < eLevs.size())	//avoid searching through by keeping track in loop, roughly
	{
		if(eLevs[guess]->uniqueID == id)
			return(guess);
	}
	for(unsigned int i=0; i<eLevs.size(); ++i)
	{
		if(eLevs[i]->uniqueID == id)
			return(i);
	}
	return(-1);
}

ELevel* GroupThermalizationData::GetELev(unsigned int index)
{
	if(index < eLevs.size())
		return(eLevs[index]);
	
	return(NULL);
}


void ChargeError::Reset()
{
	cbN=vbN=vbTN=cbTN=donN=acpN=cbP=vbP=vbTP=cbTP=donP=acpP=0.0;
	return;
}

ChargeError::ChargeError()
{
	Reset();
}

ChargeError::~ChargeError()
{
	Reset();
}

Point& ELevel::getPoint() { return pt; }
Model& ELevel::getModel() { return pt.getModel(); }
ModelDescribe& ELevel::getMdesc() { return pt.getMdesc(); }
Simulation* ELevel::getSim() { return pt.getSim(); }

Model& Point::getModel() { return mdl; }
ModelDescribe& Point::getMdesc() { return mdl.getMDesc(); }
Simulation* Point::getSim() { return mdl.getMDesc().simulation; }

ModelDescribe& Model::getMDesc() { return mdesc; }
Simulation* Model::getSim() { return mdesc.simulation; }

ELevel& ELevel::operator = (const ELevel& rhs) {
	if (this != &rhs) {
		beginoccstates = rhs.beginoccstates;
		numstates = rhs.numstates;
		endoccstates = rhs.endoccstates;
		prevOcc = rhs.prevOcc;
		thermalOcc = rhs.thermalOcc;
		thermalAvail = rhs.thermalAvail;
		prevOccisCarrierType = rhs.prevOccisCarrierType;
		BegoccStatesisCarrierType = rhs.BegoccStatesisCarrierType;
		EndoccStatesisCarrierType = rhs.EndoccStatesisCarrierType;
		updateRelFermi = rhs.updateRelFermi;
		densityMin = rhs.densityMin;
		densityUse = rhs.densityUse;
		densityMax = rhs.densityMax;

		rateIn = rhs.rateIn;
		rateOut = rhs.rateOut;
		percentTransfer = rhs.percentTransfer;
		netRate = rhs.netRate;
		netRateM1 = rhs.netRateM1;
		netRateM2 = rhs.netRateM2;
		relFermi = rhs.relFermi;
		maxOcc = rhs.maxOcc;
		minOcc = rhs.minOcc;
		minAvail = rhs.minAvail;
		maxAvail = rhs.maxAvail;
		minEf = rhs.minEf;
		maxEf = rhs.maxEf;
		targetOcc = rhs.targetOcc;
		targetAvail = rhs.targetAvail;
		tOccMin = rhs.tOccMin;
		tAvailMin = rhs.tAvailMin;
		tOccMax = rhs.tOccMax;
		tAvailMax = rhs.tAvailMax;
		maxOccRate = rhs.maxOccRate;
		minOccRate = rhs.minOccRate;
		deriv = rhs.deriv;
		deriv2 = rhs.deriv2;
		derivsourceonly = rhs.derivsourceonly;
		adjustCarrier = rhs.adjustCarrier;
		LimitByAvailability = rhs.LimitByAvailability;
		prevAdjust = rhs.prevAdjust;
		SSTargetCleared = rhs.SSTargetCleared;
		targetTransfer = rhs.targetTransfer;
		carriertype = rhs.carriertype;
		bandtail = rhs.bandtail;
		min = rhs.min;
		max = rhs.max;
		eUse = rhs.eUse;
		boltzeUse = rhs.boltzeUse;
		boltzmax = rhs.boltzmax;
		carrierspectrumcutoff = rhs.carrierspectrumcutoff;
		efm = rhs.efm;
		tau = rhs.tau;
		taui = rhs.taui;
		velocity = rhs.velocity;
		fermi = rhs.fermi;
		FermiProb = rhs.FermiProb;
		AbsFermiEnergy = rhs.AbsFermiEnergy;
		includeFermiBelow = rhs.includeFermiBelow;
		includeFermiAbove = rhs.includeFermiAbove;
		charge = rhs.charge;
		uniqueID = rhs.uniqueID;
		imp = rhs.imp;
		if (rhs.opWeights)
			opWeights = new OpticalWeightELevel(*rhs.opWeights);
		else
			opWeights = nullptr;
		ssLink = rhs.ssLink;

		scatter = rhs.scatter;
		time = rhs.time;

		SRHConstant = rhs.SRHConstant;
		TargetELevDD = rhs.TargetELevDD;
		NeighborEquivs = rhs.NeighborEquivs;
		TargetELev = rhs.TargetELev;
		OpticalRates = rhs.OpticalRates;
		Rates = rhs.Rates;
		TargetEfs = rhs.TargetEfs;
		ExcessiveNetPRates = rhs.ExcessiveNetPRates;
		ExcessiveNetNRates = rhs.ExcessiveNetNRates;
		NetRateP = rhs.NetRateP;
		NetRateN = rhs.NetRateN;
		RateSums = rhs.RateSums;
	}
	return *this;
}

Point* Model::getPoint(long int index) {
	auto it = gridp.find(index);
	if (it != gridp.end())
		return &it->second;
	return nullptr;
}