#ifndef SAAPD_SRH_H
#define SAAPD_SRH_H

#include <vector>

class ELevel;
class Simulation;
class ELevelRate;
class PosNegSum;

class SRHRateConstants
{
public:
	ELevel* targetState;	//the state the energy level is moving into
	SRHRateConstants* invRate;
	unsigned long long int trgID;	//for quick identification
	double sigmavelocity;	//the capture cross-section and velocity of the carriers
	double rate;	//rate used in calculation (only calculate once instead of many times!)
	
	SRHRateConstants();
	~SRHRateConstants();
};

double CalcSRH1Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim = NULL, bool GenLight = false, PosNegSum* AccuRate = NULL);
double CalcSRH2Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate = NULL);
double CalcSRH3Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim = NULL, bool GenLight = false, PosNegSum* AccuRate = NULL);
double CalcSRH4Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate = NULL);
int CalcSRH12DerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget);
int CalcSRH34DerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget);

std::vector<ELevelRate>::iterator CalculateSRH1Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
std::vector<ELevelRate>::iterator CalculateSRH2Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
std::vector<ELevelRate>::iterator CalculateSRH3Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
std::vector<ELevelRate>::iterator CalculateSRH4Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource);
SRHRateConstants* GetSRHConsts(ELevel* srcE, ELevel* trgE);
SRHRateConstants* GetSRHConsts(ELevel* srcE, unsigned long long int trgID);

#endif