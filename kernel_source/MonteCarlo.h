#ifndef MONTECARLO_H
#define MONTECARLO_H

#include <vector>
#include <cmath>
#include <climits>
#include "model.h"
#include "BinTree.h"
#include "physics.h"
#include "random.h"

#define MCZERO 0.0
#define MCONE 0.0
#define MCTWO 1.0
#define MCTHREE 2.0
#define MCFOUR 3.0
#define MCFIVE 4.0
#define MCSIX 5.0
#define MCSEVEN 6.0
#define MCEIGHT (6.0 + SQRT2I)
#define MCNINE (6.0 + 2.0*SQRT2I)
#define MCTEN (6.0 + 3.0*SQRT2I)
#define MCELEVEN (6.0 + 4.0*SQRT2I)
#define MCTWELVE (6.0 + 5.0*SQRT2I)
#define MCTHIRTEEN (6.0 + 6.0*SQRT2I)
#define MCFOURTEEN (6.0 + 7.0*SQRT2I)
#define MCFIFTEEN (6.0 + 8.0*SQRT2I)
#define MCSIXTEEN (6.0 + 9.0*SQRT2I)
#define MCSEVENTEEN (6.0 + 10.0*SQRT2I)
#define MCEIGHTTEEN (6.0 + 11.0*SQRT2I)
#define MCNINETEEN (6.0 + 12.0*SQRT2I)
#define MCTWENTY (6.0 + 12.0*SQRT2I + 1.0*SQRT3I)
#define MCTWENTYONE (6.0 + 12.0*SQRT2I + 2.0*SQRT3I)
#define MCTWENTYTWO (6.0 + 12.0*SQRT2I + 3.0*SQRT3I)
#define MCTWENTYTHREE (6.0 + 12.0*SQRT2I + 4.0*SQRT3I)
#define MCTWENTYFOUR (6.0 + 12.0*SQRT2I + 5.0*SQRT3I)
#define MCTWENTYFIVE (6.0 + 12.0*SQRT2I + 6.0*SQRT3I)
#define MCTWENTYSIX (6.0 + 12.0*SQRT2I + 7.0*SQRT3I)
#define MCTWENTYSEVEN (6.0 + 12.0*SQRT2I + 8.0*SQRT3I)
#define MCLARGE MCTWENTYSEVEN
#define RATIO 0
#define TOOSMALL 1
#define AVG 2


class MCData	//this should contain all the information needed to solve the equation (except varying pt data)
{
public:
	double attemptmove;	//number of carriers attempting to move
	double numfail;	//return value

	ELevel* StateS;	//pointer to source state
	ELevel* StateT;	//pointer to target state
	Point* PtS;	//source point
	Point* PtT;	//target point
	bool carrierType;	//type of carrier being transferred
	bool targetType;	//the kind of carriers the target takes
	bool sourceType;	//kind of carrier the source uses - may not match carrier type!
	double PCompare;	//-kTln(p) -- natural log of the acceptance probability
	double constantval;	// equiv of C in quadratic formula - except no -kT ln(p)
	double linDelCCoeff;	//overall coefficient for linear term - equiv of B in quadratic formula
	double quadDelCCoeff;	//overall coefficient for quadratic term - equiv of A in quadratic formula
	double TargetCoeff;	//
	double SourceCoeff;	//

	double sqrtterm;	//the term that goes into the square root for solving the Quadratic Equation
	double FirstTerm;	//first term when solving quadratic equation
	
	double minDelC;		//the minimum transfer of carriers
	double maxDelC;		//maximum number of carriers which may be transferred
	double solution;	//first solution to the equation


	

};

double MCNondegenFermiLevel(EMotionTask& ApplyTask);
double MCCarrierLevelizeFermis(EMotionTask& ApplyTask);
double MCCarrierPtFermiLevelize(EMotionTask& ApplyTask);
double MCCarrierFermiDifference(EMotionTask& ApplyTask);
double MCCarrierEnergyDeviceEnergy(EMotionTask& ApplyTask, Model& mdl);
double SamePointNoEnergyMCInRate(EMotionTask& ApplyTask);
double SamePointEnergyMCInRate(EMotionTask& ApplyTask);
Tree<PtMCConstants, unsigned long int>* CalculateMCConstants(Model& mdl, long int source, long int target);
int CalcEFieldCoeff(vector<PtData>& vect1, vector<PtData>& vect2, Point* curPt, Point* PtS, Point* PtT, int dir, double difJST, Model& mdl, double& quadVal);
double CalcVolEField(Point* PtS, Point* PtT, int dir, int NDim);
int FindPoissonCoeff(Point* pt, long int targetindex, double& ret);
double CalcDelEnergy(MCData& data, double numTransfer);
double GetChargeDensityFactor(double netCharge);
double GetChargeDensityFactor(long long int netCharge);
double MaxETransfer(MCData& data);
double MaxCarrierTransfer(ELevel* src, ELevel* trg, bool carrier);



extern MyRand SciRandom;
extern double ProbFraction(double num);	//helperfunctions.cpp
extern bool SolveQuadratic(double A, double B, double C, double& retAdd, double& retSubtract);	//helperfunctions.cpp
extern double CubeRoot(double num);	//helperfunctions.cpp
extern double FermiFnElev(ELevel* eLev, bool oneMinus);




#endif