#include "srh.h"

#include "model.h"
#include "RateGeneric.h"
#include "light.h"
#include "outputdebug.h"
#include "transient.h"

double CalcSRH1Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim, bool GenLight, PosNegSum* AccuRate)
{
	//electrons in CB to impurity/band/defect

	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0.0);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0.0);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceOpen, sourceElec, targetElec, targetOpen;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceOpen);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetOpen);
	double srh1, srh2;
	if (!AccuRate)
	{
		double avail1 = voli * sourceElec * targetOpen;	//Cm^-3
		srh1 = avail1 * useConsts->sigmavelocity;	//this is rate Out - sigmavelocity is cm^3/sec
		//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec
		double avail2 = voli * targetElec * sourceOpen;
		srh2 = avail2 * targetConsts->sigmavelocity;	//this is rate in
	}
	else
	{
		double nTarget = trgE->GetNumstates();
		double nSource = srcE->GetNumstates();

		bool splitTarget = (targetElec > targetOpen && targetElec * targetOpen <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceOpen > sourceElec && sourceOpen * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//this is for calculating rate in, so is the carrier source really close?
		if (!splitSource && !splitTarget)
		{
			srh2 = voli * targetConsts->sigmavelocity * targetElec * sourceOpen;
			AccuRate->AddValue(srh2);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = targetConsts->sigmavelocity * voli;
			//sourceOpen = nSource - sourceElec ~~~~~ targetElec = nTarget - targetOpen
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nSource * nTarget);
			AccuRate->AddValue(cnst * sourceElec * targetOpen);
			AccuRate->SubtractValue(cnst * nSource * targetOpen);
			AccuRate->SubtractValue(cnst * sourceElec * nTarget);
			srh2 = cnst * targetElec * sourceOpen;
		}
		else if (splitSource)
		{
			//sourceOpen = nSource - sourceElec
			double cnst = targetConsts->sigmavelocity * voli * targetElec;
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nSource);
			AccuRate->SubtractValue(cnst * sourceElec);
			srh2 = cnst * sourceOpen;
		}
		else// if (splitTarget)
		{
			//targetElec = nTarget - targetOpen
			double cnst = targetConsts->sigmavelocity * voli * sourceOpen;
			//availTarget = nTarget - carrierTarget
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nTarget);
			AccuRate->SubtractValue(cnst * targetOpen);
			srh2 = cnst * targetElec;
		}

		//now do SRH1
		 splitTarget = (targetOpen > targetElec && targetElec * targetOpen <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		 splitSource = (sourceElec > sourceOpen && sourceOpen * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//this is for calculating rate out, so is the source elec really close to full?
		if (!splitSource && !splitTarget)
		{
			srh1 = voli * sourceElec * targetOpen * useConsts->sigmavelocity;	//this is rate Out - sigmavelocity is cm^3/sec
			AccuRate->SubtractValue(srh1);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = useConsts->sigmavelocity * voli;
			//sourceElec = nSource - sourceOpen ~~~~~ targetOpen = nTarget - targetElec
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource * nTarget);
			AccuRate->SubtractValue(cnst * sourceOpen * targetElec);
			AccuRate->AddValue(cnst * nSource * targetElec);
			AccuRate->AddValue(cnst * sourceOpen * nTarget);
			srh1 = cnst * sourceElec * targetOpen;
		}
		else if (splitSource)
		{
			//sourceElec = nSource - sourceOpen
			double cnst = useConsts->sigmavelocity * voli * targetOpen;
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource);
			AccuRate->AddValue(cnst * sourceOpen);
			srh1 = cnst * sourceElec;
		}
		else// if (splitTarget)
		{
			//targetOpen = nTarget - targetElec
			double cnst = useConsts->sigmavelocity * voli * sourceElec;
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nTarget);
			AccuRate->AddValue(cnst * targetElec);
			srh1 = cnst * targetOpen;
		}

	}
	//the source is in the conduction band, so this net rate in is always valid

	if (GenLight)
	{
		ELevelRate rateData(srcE, trgE, RATE_SRH1, ELECTRON, srcE->carriertype, trgE->carriertype, DoubleSubtract(srh1, srh2), srh1, srh2, srh1, -1.0, 0.0, 0.0, 0.0);
		GenerateLight(sim, rateData);
	}

	return(DoubleSubtract(srh2,srh1));
}

//how does a carrier concentration change in the source affect the rate of the target
int CalcSRH12DerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget)
{
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;
	double voli = pt->voli;
	double targetOcc, targetOpen, sourceOcc, sourceOpen;
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetOpen);
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceOpen);

	if(srcE->imp == NULL && srcE->bandtail == false)
	{
		//it's the conduction band in the source
		if(trgE->bandtail)	//same situation as below, but if this is true, imp would be a null pointer and crash the code
		{
			//target is a CB Band tail, so treat like donor
			derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
			derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
		else if(trgE->imp->isDonor())
		{
			//target is a donor
			derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
			derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
		else
		{
			//the target is an acceptor
			derivSource = -voli * (targetOpen * targetConsts->sigmavelocity + targetOcc * useConsts->sigmavelocity);
			derivTarget = -voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
	}
	else if(srcE->bandtail)
	{
		//this is treated as SRH2 "donor" (CB Tail) going to CB
		derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
		derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
	}
	else if(srcE->imp->isDonor())
	{
		//this is treated as SRH2 "donor" (CB Tail) going to CB
		derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
		derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
	}
	else
	{
		//this is treated as SRH2 "acceptor" going to CB
		derivSource = -voli * (targetOpen * useConsts->sigmavelocity + targetOcc * targetConsts->sigmavelocity);
		derivTarget = -voli * (sourceOpen * useConsts->sigmavelocity + sourceOcc * targetConsts->sigmavelocity);
	}
	/*
	//is this a donor/CB transition? Results in a "positive" value
	if(trgE->carriertype == ELECTRON && srcE->carriertype == ELECTRON)
	{
		//both electrons, so an increase in occupancy in the target would increase the rate in the source
		//the derivative in the rate in the source state with respect to the target carrier concentration
		derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		//the derivative in the rate in the source state with respect to the source carrier concentration
		//an increase in the occupancy in the source would decrease the rate in in the source
		derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
	}
	else
	{
		//the derivative in the source state with respect to the target carrier concentration. The impurity is an acceptor
		//if CB, an increase in holes in the acceptor will cause the rate out of the CB to increase (-)
		//if Acp, an increase in electrons in CB will cause the rate out of acceptors to increase (-)
		derivTarget = -voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		//if CB, an increase in e- in CB will cause rate out of CB to increase (-)
		//if ACP, an increase in h+ in ACP will cause rate out of ACP to increase (-)
		derivSource = -voli * (targetOpen * targetConsts->sigmavelocity + targetOcc * useConsts->sigmavelocity);
	}
	*/
	//if acceptor/VB transition, results in a negative value
	return(0);
}


//returns the netrate in
double CalcSRH2Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate)
{
	//electrons in impurity going to the conduction band, source is impurity
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0.0);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0.0);
	}
	

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceOpen, sourceElec, targetElec, targetOpen;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceOpen);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetOpen);
	double srh2, srh1;
	double signAdjust = (srcE->carriertype == ELECTRON) ? 1.0 : -1.0;	//srh1 treated as rate in
	if (!AccuRate)
	{
		double avail2 = voli * sourceElec * targetOpen;	//Cm^-3
		srh2 = avail2 * useConsts->sigmavelocity;	//this is rate Out (donor) - sigmavelocity is cm^3/sec
		//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

		double avail1 = voli * targetElec * sourceOpen;
		srh1 = avail1 * targetConsts->sigmavelocity;	//this is rate in (if carrier type is electron)
	}
	else
	{
		double nTarget = trgE->GetNumstates();
		double nSource = srcE->GetNumstates();
		
		bool splitTarget = (targetOpen > targetElec && targetElec * targetOpen <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceElec > sourceOpen && sourceOpen * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//this is for calculating rate out, so is the carrier source really close?
		if (!splitSource && !splitTarget)
		{
			srh2 = voli * useConsts->sigmavelocity * sourceElec * targetOpen;
			AccuRate->SubtractValue(srh2 * signAdjust);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = voli * useConsts->sigmavelocity * signAdjust;
			//sourceElec = nSource - sourceOpen ~~~~~ targetOpen = nTarget - targetElec
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource * nTarget);
			AccuRate->SubtractValue(cnst * sourceOpen * targetElec);
			AccuRate->AddValue(cnst * nSource * targetElec);
			AccuRate->AddValue(cnst * sourceOpen * nTarget);
			srh2 = cnst * targetOpen * sourceElec * signAdjust;	//signAdjust guarantees positive by squaring the effect
		}
		else if (splitSource)
		{
			//sourceElec = nSource - sourceOpen
			double cnst = voli * useConsts->sigmavelocity * targetOpen * signAdjust;
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nSource);
			AccuRate->AddValue(cnst * sourceOpen);
			srh2 = cnst * sourceElec * signAdjust;
		}
		else// if (splitTarget)
		{
			//targetOpen = nTarget - targetElec
			double cnst = voli * useConsts->sigmavelocity * signAdjust * sourceElec;
			//availTarget = nTarget - carrierTarget
			//this is rate out, so subtract the positive values
			AccuRate->SubtractValue(cnst * nTarget);
			AccuRate->AddValue(cnst * targetElec);
			srh2 = cnst * targetOpen * signAdjust;
		}

		//now do SRH1
		splitTarget = (targetOpen > targetElec && targetElec * targetOpen <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		splitSource = (sourceElec > sourceOpen && sourceOpen * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//this is for calculating rate in, so is the source elec really close to full?
		if (!splitSource && !splitTarget)
		{
			srh1 = voli * targetElec * sourceOpen * targetConsts->sigmavelocity;	//this is rate Out - sigmavelocity is cm^3/sec
			AccuRate->AddValue(srh1 * signAdjust);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = targetConsts->sigmavelocity * voli * signAdjust;
			//sourceOpen = nSource - sourceElec ~~~~~ targetElec = nTarget - targetOpen
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nSource * nTarget);
			AccuRate->AddValue(cnst * sourceElec * targetOpen);
			AccuRate->SubtractValue(cnst * nSource * targetOpen);
			AccuRate->SubtractValue(cnst * sourceElec * nTarget);
			srh1 = cnst * sourceOpen * targetElec * signAdjust;
		}
		else if (splitSource)
		{
			//sourceOpen = nSource - sourceElec
			double cnst = targetConsts->sigmavelocity * voli * targetElec * signAdjust;
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nSource);
			AccuRate->SubtractValue(cnst * sourceElec);
			srh1 = cnst * sourceOpen * signAdjust;
		}
		else// if (splitTarget)
		{
			//targetElec = nTarget - targetOpen
			double cnst = targetConsts->sigmavelocity * voli * sourceOpen * signAdjust;
			//this is rate in, so add the positive values
			AccuRate->AddValue(cnst * nTarget);
			AccuRate->SubtractValue(cnst * targetOpen);
			srh1 = cnst * targetElec * signAdjust;
		}

	}

	if(srcE->carriertype == ELECTRON)
		return(DoubleSubtract(srh1,srh2));	//returns the net rate in
	

	return(DoubleSubtract(srh2,srh1));	//it's a hole, so electrons leaving (srh2) is actually the rate in

}

//returns the net rate in
double CalcSRH3Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, Simulation* sim, bool GenLight, PosNegSum* AccuRate)
{
	//calculates holes in VB falling into acceptor. Source is VB.
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0.0);
	}

	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0.0);
	}


	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceElec, sourceHole;
	double targetElec, targetHole;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetHole);

	double SVSource =  useConsts->sigmavelocity;	//for VB, no exp()
	double SVTarget = targetConsts->sigmavelocity;	//for imp, yes exp()

	

	double srh3, srh4;
	if (!AccuRate)
	{
		double avail3 = voli * sourceHole * targetElec;	//Cm^-3
		srh3 = avail3 * SVSource;	//this is rate Out - sigmavelocity is cm^3/sec
		//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

		double avail4 = voli * targetHole * sourceElec;
		srh4 = avail4 * SVTarget;	//this is rate in
	}
	else
	{
		double nTarget = trgE->GetNumstates();
		double nSource = srcE->GetNumstates();

		bool splitTarget = (targetHole > targetElec && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceElec > sourceHole && sourceHole * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//do SRH 4 first (rate in)

		if (!splitTarget && !splitSource)
		{
			srh4 = voli * targetHole * sourceElec * SVTarget;
			AccuRate->AddValue(srh4);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = voli * SVTarget;
			//targetHole = nTarget - targetElec
			//sourceElec = nSource - sourceHole
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nTarget * nSource);
			AccuRate->AddValue(cnst * targetElec * sourceHole);
			AccuRate->SubtractValue(cnst * nTarget * sourceHole);
			AccuRate->SubtractValue(cnst * nSource * targetElec);
			srh4 = cnst * sourceElec * targetHole;
		}
		else if (splitSource)
		{
			double cnst = voli * SVTarget * targetHole;
			//sourceElec = nSource - sourceHole
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nSource);
			AccuRate->SubtractValue(cnst * sourceHole);
			srh4 = cnst * sourceElec;
		}
		else //if(splitTarget)
		{
			double cnst = voli * SVTarget * sourceElec;
			//targetHole = nTarget - targetElec
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nTarget);
			AccuRate->SubtractValue(cnst * targetElec);
			srh4 = cnst * targetHole;
		}


		//now do the rate out (SRH3)

		splitTarget = (targetElec > targetHole && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		splitSource = (sourceHole > sourceElec && sourceHole * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//Rate out, so subtract positive values
		if (!splitTarget && !splitSource)
		{
			srh3 = voli * sourceHole * targetElec * SVSource;
			AccuRate->SubtractValue(srh3);
		}
		else if (splitSource && splitTarget)
		{
			double cnst = voli * SVSource;
			//targetElec = nTarget - targetHole
			//sourceHole = nSource - sourceElec
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nTarget * nSource);
			AccuRate->SubtractValue(cnst * targetHole * sourceElec);
			AccuRate->AddValue(cnst * nTarget * sourceElec);
			AccuRate->AddValue(cnst * nSource * targetHole);
			srh3 = cnst * targetElec * sourceHole;
		}
		else if (splitSource)
		{
			double cnst = voli * targetElec * SVSource;
			//sourceHole = nSource - sourceElec
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nSource);
			AccuRate->AddValue(cnst * sourceElec);
			srh3 = cnst * sourceHole;
		}
		else //if (splitTarget)
		{
			double cnst = voli * sourceHole * SVSource;
			//targetElec = nTarget - targetHole
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nTarget);
			AccuRate->AddValue(cnst * targetHole);
			srh3 = cnst * targetElec;
		}
	}

	if (GenLight)
	{
		ELevelRate rateData(srcE, trgE, RATE_SRH3, HOLE, srcE->carriertype, trgE->carriertype, DoubleSubtract(srh3, srh4), srh3, srh4, srh3, -1.0, 0.0, 0.0, 0.0);	//minimuze the number of initializations writing data
		GenerateLight(sim, rateData);
	}
	return(DoubleSubtract(srh4,srh3));
}


//how does a carrier concentration change in the source affect the rate of the target
int CalcSRH34DerivFast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, double& derivSource, double& derivTarget)
{
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;
	double voli = pt->voli;
	double targetOcc, targetOpen, sourceOcc, sourceOpen;
	trgE->GetOccupancyOcc(whichSourceTrg, targetOcc, targetOpen);
	srcE->GetOccupancyOcc(whichSourceSrc, sourceOcc, sourceOpen);


	if(srcE->imp == NULL && srcE->bandtail == false)
	{
		//VALENCE BAND - SRH3 calc
		if(trgE->bandtail)	//to VB Tail - treat as if it were an acceptor
		{
			derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
			derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
		else if(trgE->imp->isAcceptor())	//to acceptor
		{
			derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
			derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
		else  //to a donor
		{
			derivSource = -voli * (targetOpen * targetConsts->sigmavelocity + targetOcc * useConsts->sigmavelocity);
			derivTarget = -voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
		}
	}
	else if(srcE->bandtail)
	{
		//SRH4, bandtail to VB, treat as acceptor
		derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
		derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
	}
	else if(srcE->imp->isAcceptor())
	{
		//SRH4, acceptor to VB
		derivSource = -voli * (targetOcc * targetConsts->sigmavelocity + targetOpen * useConsts->sigmavelocity);
		derivTarget = voli * (sourceOpen * targetConsts->sigmavelocity + sourceOcc * useConsts->sigmavelocity);
	}
	else
	{
		//SRH4, donor to VB
		derivSource = -voli * (targetOpen * useConsts->sigmavelocity + targetOcc * targetConsts->sigmavelocity);
		derivTarget = -voli * (sourceOpen * useConsts->sigmavelocity + sourceOcc * targetConsts->sigmavelocity);	//note, sourceOpen is actually the occupancy(holes) used for the constant in the transition
	}

	/*
	//the acceptor/VB transition is positive values
	if(trgE->carriertype == HOLE && srcE->carriertype == HOLE)
	{
		derivSource = -voli * (targetOcc * useConsts->sigmavelocity + targetOpen * targetConsts->sigmavelocity);
		derivTarget = voli * (sourceOpen * useConsts->sigmavelocity + sourceOcc * targetConsts->sigmavelocity);
	}
	else
	{
		derivSource = -voli * (targetOpen * useConsts->sigmavelocity + targetOcc * targetConsts->sigmavelocity);
		derivTarget = -voli * (sourceOpen * useConsts->sigmavelocity + sourceOcc * targetConsts->sigmavelocity);
	}
	*/
	return(0);
}

//returns the netrate in.
double CalcSRH4Fast(ELevel* srcE, ELevel* trgE, int whichSourceSrc, int whichSourceTrg, PosNegSum* AccuRate)
{
	//calculate holes in imp going into the VB (srh4) - so this is a generation process. Source is defect
	//or electrons in VB going into imp
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(0.0);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(0.0);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;
	double sourceElec, sourceHole;
	double targetElec, targetHole;
	srcE->GetOccupancyEH(whichSourceSrc, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSourceTrg, targetElec, targetHole);

	double impSign = (srcE->carriertype == HOLE) ? 1.0 : -1.0;	//assuming impurity is acceptor by default

	double srh3, srh4;
	if (!AccuRate)
	{
		double avail4 = voli * sourceHole * targetElec;	//Cm^-3
		srh4 = avail4 * useConsts->sigmavelocity;	//this is rate Out (acp) - sigmavelocity is cm^3/sec includes exponential
		//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

		double avail3 = voli * targetHole * sourceElec;
		srh3 = avail3 * targetConsts->sigmavelocity;
		//this is rate in (acp)
	}
	else
	{
		double nTarget = trgE->GetNumstates();
		double nSource = srcE->GetNumstates();

		bool splitTarget = (targetHole > targetElec && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		bool splitSource = (sourceElec > sourceHole && sourceHole * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//do SRH 3 first (rate in)
		if (!splitTarget && !splitSource)
		{
			srh3 = targetConsts->sigmavelocity * voli * targetHole * sourceElec;
			AccuRate->AddValue(srh3 * impSign);
		}
		else if (splitTarget && splitSource)
		{
			double cnst = voli * targetConsts->sigmavelocity * impSign;
			//targetHole = nTarget - targetElec
			//sourceElec = nSource - sourceHole
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nTarget * nSource);
			AccuRate->AddValue(cnst * targetElec * sourceHole);
			AccuRate->SubtractValue(cnst * nTarget * sourceHole);
			AccuRate->SubtractValue(cnst * nSource * targetElec);
			srh3 = cnst * sourceElec * targetHole * impSign;	//cancel out cnst impSign
		}
		else if (splitSource)
		{
			double cnst = voli * targetConsts->sigmavelocity * targetHole * impSign;
			//sourceElec = nSource - sourceHole
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nSource);
			AccuRate->SubtractValue(cnst * sourceHole);
			srh3 = cnst * sourceElec * impSign;	//cancel out cnst impSign
		}
		else //if(splitTarget)
		{
			double cnst = voli * targetConsts->sigmavelocity * sourceElec * impSign;
			//targetHole = nTarget - targetElec
			//doing net rate in, so add positive values
			AccuRate->AddValue(cnst * nTarget);
			AccuRate->SubtractValue(cnst * targetElec);
			srh3 = cnst * targetHole * impSign;	//cancel out cnst impSign
		}

		//now do rate out on srh4
		splitTarget = (targetElec > targetHole && targetElec * targetHole <= nTarget * nTarget * 0.0009765625);	//(1/1024) = just shift bits
		splitSource = (sourceHole > sourceElec && sourceHole * sourceElec <= nSource * nSource * 0.0009765625);	//just shift exponent bits
		//do SRH 4 (rate out, so subtract positive values)
		if (!splitTarget && !splitSource)
		{
			srh4 = voli * sourceHole * targetElec * useConsts->sigmavelocity;
			AccuRate->SubtractValue(srh4 * impSign);
		}
		else if (splitTarget && splitSource)
		{
			double cnst = voli * useConsts->sigmavelocity * impSign;
			//targetElec = nTarget - targetHole
			//sourceHole = nSource - sourceElec
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nTarget * nSource);
			AccuRate->SubtractValue(cnst * sourceElec * targetHole);
			AccuRate->AddValue(cnst * nTarget * sourceElec);
			AccuRate->AddValue(cnst * nSource * targetHole);
			srh4 = cnst * sourceHole * targetElec * impSign;	//cancel out cnst impSign
		}
		else if (splitSource)
		{
			double cnst = voli * useConsts->sigmavelocity * targetElec * impSign;
			//sourceHole = nSource - sourceElec
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nSource);
			AccuRate->AddValue(cnst * sourceElec);
			srh4 = cnst * sourceHole * impSign;	//cancel out cnst impSign
		}
		else //if(splitTarget)
		{
			double cnst = voli * useConsts->sigmavelocity * sourceHole * impSign;
			//targetElec = nTarget - targetHole
			//doing net rate out, so subtract positive values
			AccuRate->SubtractValue(cnst * nTarget);
			AccuRate->AddValue(cnst * targetHole);
			srh4 = cnst * targetElec * impSign;	//cancel out cnst impSign
		}

	}

	if (srcE->carriertype == HOLE)	//it was an acceptor
		return(DoubleSubtract(srh3,srh4));

	return(DoubleSubtract(srh4, srh3));	//a donor with holes leaving means electrons entering
}


std::vector<ELevelRate>::iterator CalculateSRH1Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//this calculates electrons in CB --> IMP
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(ret);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(ret);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceElec, sourceOpen;
	double targetElec, targetOpen;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceOpen);
	trgE->GetOccupancyEH(whichSource, targetElec, targetOpen);
	
	double SVSource =  useConsts->sigmavelocity;
	double SVTarget = targetConsts->sigmavelocity;

	double avail1 = voli * sourceElec * targetOpen;	//Cm^-3
	double srh1 = avail1 * SVSource;	//this is rate Out - sigmavelocity is cm^3/sec
	//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

	double avail2 = voli * targetElec * sourceOpen;
	double srh2 = avail2 * SVTarget;	//this is rate in

	double deriv = -voli * (SVSource * (targetOpen + sourceElec) + SVTarget * (sourceOpen + targetElec));
	double deriv2 = 2.0 * voli * (SVTarget - SVSource);
	double derivSOnly = -voli * (targetOpen * SVSource + targetElec * SVTarget);

	ELevelRate rateData(srcE,trgE,RATE_SRH1,ELECTRON,srcE->carriertype, trgE->carriertype,DoubleSubtract(srh1,srh2),srh1,srh2,srh1,-1.0,deriv,deriv2,derivSOnly);

	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;
	//RecordRDataLimits(rateData, srcE, srh2, srh1, sourceElec + targetElec, 1.0); //EQ positive if transfer out, net rate is positve if transfer out --> +1.0
	ret = srcE->AddELevelRate(rateData, false);	//add the rate... but this is kind of backwards because we want to add rateOut
	//but we care about the generation rate!

	GenerateLight(sim, rateData);
	
	return(ret);
}


std::vector<ELevelRate>::iterator CalculateSRH2Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//this calculates electrons in IMP --> CB. As the math has already been reduced to completely mirror one another, nothing
	//changes as both are electron type carriers. The equil transfer would be negative, but because the elevel source/target changed
	//even that stays the same.  This is basically IDENTICAL code to SRH1, except swapping srh1 & 2
	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(ret);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(ret);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceElec, sourceOpen;
	double targetElec, targetOpen;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceOpen);
	trgE->GetOccupancyEH(whichSource, targetElec, targetOpen);

	double SVSource =  useConsts->sigmavelocity;	//capture cross section used for impurity - includes emission exponent
	double SVTarget = targetConsts->sigmavelocity;

	double avail2 = voli * sourceElec * targetOpen;	//Cm^-3
	double srh2 = avail2 * SVSource;	//this is rate Out - sigmavelocity is cm^3/sec
	//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

	double avail1 = voli * targetElec * sourceOpen;
	double srh1 = avail1 * SVTarget;	//this is rate in, assuming the impurity is a donor and carrier rtype is electron

	double netrate = DoubleSubtract(srh2, srh1);

	if(srcE->carriertype == ELECTRON)
	{
		double deriv = -voli * (SVSource * (targetOpen + sourceElec) + SVTarget * (sourceOpen + targetElec));
		double deriv2 = 2.0 * voli * (SVTarget - SVSource);
		double derivSOnly = -voli * (targetOpen * SVSource + targetElec * SVTarget);

		ELevelRate rateData(srcE, trgE, RATE_SRH2, ELECTRON, srcE->carriertype, trgE->carriertype, netrate, srh2, srh1, srh2, -1.0, deriv, deriv2, derivSOnly);
		if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
			rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		else
			rateData.desiredTStep = 0.0;
		ret = srcE->AddELevelRate(rateData, false);
	}
	else //the carrier type is a hole, so the rate out/in are different now
	{
		double deriv = -voli * (SVSource * (targetOpen + sourceElec) + SVTarget * (sourceOpen + targetElec));
		double deriv2 = 2.0 * voli * (SVSource - SVTarget);
		double derivSOnly = -voli * (targetOpen * SVSource + targetElec * SVTarget);
		ELevelRate rateData(srcE, trgE, RATE_SRH2, HOLE, srcE->carriertype, trgE->carriertype, netrate, srh1, srh2, srh2, -1.0, deriv, deriv2, derivSOnly);
		if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
			rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		else
			rateData.desiredTStep = 0.0;
		ret = srcE->AddELevelRate(rateData, false);
	}

	return(ret);
}


std::vector<ELevelRate>::iterator CalculateSRH3Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//this calculates holes being captured in imp from VB, note, srcE  is valence band
	//because of the constants in the beginning, this is also actually very similar to SRH1&2 despite dealing with the VB

	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(ret);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(ret);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;

	double sourceElec, sourceHole;
	double targetElec, targetHole;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSource, targetElec, targetHole);

	double SVSource =  useConsts->sigmavelocity;	//this is the valence band, which does NOT have the exponential applied to it
	double SVTarget = targetConsts->sigmavelocity;	//impurity which has exponential applied

	double avail3 = voli * sourceHole * targetElec;	//Cm^-3
	double srh3 = avail3 * SVSource;	//this is rate out - sigmavelocity is cm^3/sec
	//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

	double avail4 = voli * targetHole * sourceElec;
	double srh4 = avail4 * SVTarget;	//this is rate in, holes entering impurity (assuming impurity is acceptor)

	
	double deriv = -voli * (SVTarget * (sourceElec + targetHole) + SVSource * (targetElec + sourceHole));
	double deriv2 = 2.0 * voli * (SVTarget - SVSource);
	double derivSOnly = -voli * (targetElec * SVSource + targetHole * SVTarget);
	ELevelRate rateData(srcE, trgE, RATE_SRH3, HOLE, srcE->carriertype, trgE->carriertype, DoubleSubtract(srh3,srh4), srh3, srh4, srh3, -1.0, deriv, deriv2, derivSOnly);	//minimuze the number of initializations writing data
	if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
		rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
	else
		rateData.desiredTStep = 0.0;
	ret = srcE->AddELevelRate(rateData, false);	//add the rate... but this is kind of backwards because we want to add rateOut
	GenerateLight(sim, rateData);


	return(ret);
}


std::vector<ELevelRate>::iterator CalculateSRH4Rate(ELevel* srcE, ELevel* trgE, Simulation* sim, int whichSource)
{
	std::vector<ELevelRate>::iterator ret = srcE->Rates.end();
	//this calculates electrons in VB --> IMP. Once again, no real change beyond semantics in storing the rate and the (target/source)(open/num/elec)
	// or as holes being emitted from the imp into the VB
	

	Point* pt = &srcE->getPoint();
	if(pt == NULL)
	{
		ProgressCheck(60, false);
		return(ret);
	}
	SRHRateConstants* useConsts = GetSRHConsts(srcE, trgE);

	if(useConsts == NULL)
	{
		ProgressCheck(48, false);
		return(ret);
	}

	SRHRateConstants* targetConsts = useConsts->invRate;

	double voli = pt->voli;
	double sourceElec, sourceHole;
	double targetElec, targetHole;
	srcE->GetOccupancyEH(whichSource, sourceElec, sourceHole);
	trgE->GetOccupancyEH(whichSource, targetElec, targetHole);

	double SVSource =  useConsts->sigmavelocity;	//impurity sigma, has exponential
	double SVTarget = targetConsts->sigmavelocity;

	double avail4 = voli * sourceHole * targetElec;	//Cm^-3
	double srh4 = avail4 * SVSource;	//this is rate Out (acp) - sigmavelocity is cm^3/sec
	//note srh rate usually has cm^-6 * cm^2 * cm/sec to get 1/cm^3 /sec

	double avail3 = voli * targetHole * sourceElec;
	double srh3 = avail3 * SVTarget;	//this is rate in (acp)

	
	if (srcE->carriertype == ELECTRON)
	{
		double deriv = -voli * (SVSource * (sourceHole + targetElec) + SVTarget * (sourceElec + targetHole));
		double deriv2 = 2.0 * voli * (SVTarget - SVSource);
		double derivSOnly = -voli * (targetElec * SVSource + targetHole * SVTarget);
		ELevelRate rateData(srcE, trgE, RATE_SRH4, HOLE, srcE->carriertype, trgE->carriertype, DoubleSubtract(srh4, srh3), srh3, srh4, srh4, -1.0, deriv, deriv2, derivSOnly);	//minimuze the number of initializations writing data
		if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
			rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		else
			rateData.desiredTStep = 0.0;
		ret = srcE->AddELevelRate(rateData, false);
	}
	else //the carrier type is a hole, so the rate out/in are different now
	{
		double deriv = -voli * (SVSource * (sourceHole + targetElec) + SVTarget * (sourceElec + targetHole));
		double deriv2 = 2.0 * voli * (SVTarget - SVSource);
		double derivSOnly = -voli * (targetElec * SVSource + targetHole * SVTarget); 
		ELevelRate rateData(srcE, trgE, RATE_SRH4, HOLE, srcE->carriertype, trgE->carriertype, DoubleSubtract(srh4, srh3), srh4, srh3, srh4, -1.0, deriv, deriv2, derivSOnly);	//minimuze the number of initializations writing data
		if(sim->TransientData && sim->simType==SIMTYPE_TRANSIENT)
			rateData.desiredTStep = sim->TransientData->tStepControl.UpdateActiveRate(rateData);	//sets flag to do limit by availability for source as well (for weak potentially becoming strong)
		else
			rateData.desiredTStep = 0.0;
		ret = srcE->AddELevelRate(rateData, false);
	}

	//RecordRDataLimits(rateData, srcE, srh4, srh3, sourceHole + targetElec, 1.0); //EQ positive gains carriers, netRate positive gains carriers, +1.0
	
	return(ret);
}



SRHRateConstants* GetSRHConsts(ELevel* srcE, ELevel* trgE)
{
	/*
	temporary function to search through the vector to find the right target
	when the switch is made to trees, this will be pretty much replaced with treeNode = srcE.Find(trgE)

	maps are slow - data next to each other is quick access
	*/
	/*
	std::map<unsigned long long int, SRHRateConstants>::iterator it = srcE->SRHConstant.find(trgE->uniqueID);
	if(it != srcE->SRHConstant.end())
		return(&(it->second));
	*/

	unsigned int sz = srcE->SRHConstant.size();
	for(unsigned int i=0; i<sz; ++i)
	{
		if(srcE->SRHConstant[i].trgID == trgE->uniqueID)
			return(&(srcE->SRHConstant[i]));
	}
	
	return(NULL);
}

SRHRateConstants* GetSRHConsts(ELevel* srcE, unsigned long long int trgID)
{
	/*
	temporary function to search through the vector to find the right target
	when the switch is made to trees, this will be pretty much replaced with treeNode = srcE.Find(trgE)

	maps are slow - data next to each other is quick access
	*/
	/*
	std::map<unsigned long long int, SRHRateConstants>::iterator it = srcE->SRHConstant.find(trgE->uniqueID);
	if(it != srcE->SRHConstant.end())
		return(&(it->second));
	*/

	unsigned int sz = srcE->SRHConstant.size();
	for(unsigned int i=0; i<sz; ++i)
	{
		if(srcE->SRHConstant[i].trgID == trgID)
			return(&(srcE->SRHConstant[i]));
	}
	
	return(NULL);
}
