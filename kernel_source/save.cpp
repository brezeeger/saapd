#include "../gui_source/XML.h"
#include "../gui_source/GUIHelper.h"
#include "transient.h"
#include "model.h"
#include "light.h"
#include "ss.h"
#include "contacts.h"
#include "Materials.h"

void ModelDescribe::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;

	file.CreateCategory("device");
	if (NDim == 1)
		file.WriteTag("1d");
	else if (NDim == 2)
		file.WriteTag("2d");
	else if (NDim == 3)
		file.WriteTag("3d");

	resolution.SaveToXML(file, "resolution");
	Ldimensions.SaveToXML(file, "dimensions");

	for (unsigned int i = 0; i < materials.size(); ++i)
		materials[i]->SaveToXML(file);

	for (unsigned int i = 0; i < impurities.size(); ++i)
		impurities[i]->SaveToXML(file);

	for (unsigned int i = 0; i < layers.size(); ++i)
		layers[i]->SaveToXML(file);

	for (unsigned int i = 0; i < spectrum.size(); ++i)
		spectrum[i]->SaveToXML(file);

	if (simulation)
		simulation->SaveToXML(file);

	file.CloseLastCategory();
}

void Outputs::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;

	file.CreateCategory("validoutputs");
	file.WriteTag("numinbin", numinbin);

	if (CBNRin)
		file.WriteTag("nrcb");

	if (EmptyQEFiles)
		file.WriteTag("emptyqe");

	if (VBNRin)
		file.WriteTag("nrvb");

	if (AcpNRin)
		file.WriteTag("nracp");

	if (DonNRin)
		file.WriteTag("nrdon");

	if (LightEmission)
		file.WriteTag("lightemission");

	if (Lpow)
		file.WriteTag("opticpower");

	if (Lgen)
		file.WriteTag("opticgen");

	if (Lsrh)
		file.WriteTag("absorbsrh");

	if (Ldef)
		file.WriteTag("absorbdef");

	if (Lscat)
		file.WriteTag("absorbscat");

	if (Erec)
		file.WriteTag("stimemission");

	if (Esrh)
		file.WriteTag("sesrh");

	if (Edef)
		file.WriteTag("sedef");

	if (Escat)
		file.WriteTag("sescat");

	if (CarrierCB)
		file.WriteTag("cbcarrier");

	if (CarrierVB)
		file.WriteTag("vbcarrier");

	if (Ec)
		file.WriteTag("conductionband");
	if (error)
		file.WriteTag("stderr");
	if (Ev)
		file.WriteTag("valenceband");
	if (fermi)
		file.WriteTag("fermi");
	if (fermin)
		file.WriteTag("efn");
	if (fermip)
		file.WriteTag("efp");
	if (genth)
		file.WriteTag("genthermal");
	if (impcharge)
		file.WriteTag("impcharge");
	if (Jn)
		file.WriteTag("jn");
	if (Jp)
		file.WriteTag("jp");
	if (n)
		file.WriteTag("nconc");
	if (p)
		file.WriteTag("pconc");
	if (Psi)
		file.WriteTag("localvaccuum");
	if (recth)
		file.WriteTag("recthermal");
	if (rho)
		file.WriteTag("rho");
	if (SRH)
		file.WriteTag("srh");
	if (scatter)
		file.WriteTag("scatter");
	if (stddev)
		file.WriteTag("stddev");
	if (QE)
		file.WriteTag("qe");
	if (chgError)
		file.WriteTag("chgerr");
	if (OutputAllIndivRates)
		file.WriteTag("outputallrates");
	if (OutputRatePlots)
		file.WriteTag("rateplots");
	if (jvCurves)
		file.WriteTag("jvcurves");
	if (envSummaries)
		file.WriteTag("envsummaries");
	if (envResults)
		file.WriteTag("envresults");
	if (contactStates)
		file.WriteTag("contactstates");
	if (capacitance)
		file.WriteTag("capacitance");
	if (contactCharge)
		file.WriteTag("contactcharge");
	
	file.CloseLastCategory();
}

void Calculations::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;

	file.CreateCategory("calcrates");
	if (Current)
		file.WriteTag("current");
	if (LightEmissions)
		file.WriteTag("lightemission");

	if (Lpow)
		file.WriteTag("lightpower");
	if (Lgen)
		file.WriteTag("lightgen");
	if (Lscat)
		file.WriteTag("lightscat");
	if (Ldef)
		file.WriteTag("lightdef");
	if (Lsrh)
		file.WriteTag("lightsrh");
	if (SErec)
		file.WriteTag("serec");
	if (SEscat)
		file.WriteTag("sescat");
	if (SEdef)
		file.WriteTag("sedef");
	if (SEsrh)
		file.WriteTag("sesrh");
	if (Scatter)
		file.WriteTag("scatter");
	if (SRH1)
		file.WriteTag("srh1");
	if (SRH2)
		file.WriteTag("srh2");
	if (SRH3)
		file.WriteTag("srh3");
	if (SRH4)
		file.WriteTag("srh4");
	if (ThermalGen)
		file.WriteTag("thermalgen");
	if (ThermalRec)
		file.WriteTag("thermalrec");
	if (Tunnel)
		file.WriteTag("tunnel");
	if (RecyclePhotonsAffectRates)
		file.WriteTag("recyclephotons");

	if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_ALL))
		file.WriteTag("thermalizeall");
	else {
		if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_CB | THERMALIZE_VB))
			file.WriteTag("thermalizebands");
		else {
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_CB))
				file.WriteTag("thermalizecb");
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_VB))
				file.WriteTag("thermalizevb");
		}

		if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_CBT | THERMALIZE_VBT))
			file.WriteTag("thermalizetails");
		else {
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_CBT))
				file.WriteTag("thermalizecbtail");
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_VBT))
				file.WriteTag("thermalizevbtail");
		}

		if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_DON | THERMALIZE_ACP))
			file.WriteTag("thermalizeimps");
		else {
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_DON))
				file.WriteTag("thermalizedon");
			if (ALL_SET_BITS_ARE_ONE(Thermalize, THERMALIZE_ACP))
				file.WriteTag("thermalizeacp");
		}
	}
	file.WriteTag("opticalemin", OpticalEMin);
	file.WriteTag("opticalemax", OpticalEMax);
	file.WriteTag("opticaleres", OpticalEResolution);
	file.WriteTag("opticalintensity", OpticalIntensityCutoff);
	

	file.CloseLastCategory();
}

void Contact::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;
	if (id < 0)
		return;

	file.CreateCategory("contact");
	file.WriteTag("id", id);
	file.WriteTag("name", name);
	file.CloseLastCategory();
}

void Simulation::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;

	file.CreateCategory("simulation");
	file.WriteTag("outname", OutputFName);
	file.WriteTag("accuracy", accuracy, 4, true);
	file.WriteTag("statediv", statedivisions);
	file.WriteTag("temperature", T);
	file.WriteTag("maxlightreflect", maxLightReflections);
	file.WriteTag("maxiter", maxiter);
	if (!ThEqDeviceStart)
		file.WriteTag("disablestartdevicetheq");
	else
		file.WriteTag("enablestartdevicetheq");
	if (!ThEqPtStart)
		file.WriteTag("disablestartpointtheq");
	else
		file.WriteTag("enablestartpointtheq");

	EnabledRates.SaveToXML(file);
	outputs.SaveToXML(file);

	for (unsigned int i = 0; i < contacts.size(); ++i)
		contacts[i]->SaveToXML(file);

	if (TransientData)
		TransientData->SaveToXML(file);

	if (SteadyStateData)
		SteadyStateData->SaveToXML(file);

	file.CloseLastCategory();
}

void TransContact::XMLCos(XMLFileOut& file, std::multimap<double, CosTimeDependence>& dat)
{
	if (file.isOpen() == false)
		return;

	for (auto it = dat.begin(); it != dat.end(); ++it) {
		file.CreateCategory("cos");
		file.WriteTag("starttime", it->second.GetStartTime(), 13, true);
		file.WriteTag("endtime", it->second.GetEndTime(), 13, true);
		file.WriteTag("amplitude", it->second.GetAmp(), 13, true);
		file.WriteTag("frequency", it->second.GetAmp(), 13, true);
		file.WriteTag("delta", it->second.GetAmp(), 13, true);
		file.WriteTag("fixed", it->second.GetAmp(), 13, true);	//DC offset
		file.WriteTag("sum", it->second.IsSum());
		file.WriteTag("flag", it->second.GetFlags());
		file.CloseLastCategory();
	}
}

void TransContact::XMLTim(XMLFileOut& file, std::map<double, TimeDependence>& dat)
{
	if (file.isOpen() == false)
		return;
	for (auto it = dat.begin(); it != dat.end(); ++it) {
		file.CreateCategory("additional");
		file.WriteTag("starttime", it->second.GetStartTime(), 13, true);
		file.WriteTag("endtime", it->second.GetEndTime(), 13, true);
		file.WriteTag("sum", it->second.IsSum());
		file.WriteTag("flag", it->second.GetFlags());
		file.CloseLastCategory();
	}
}

void TransContact::XMLLin(XMLFileOut& file, std::multimap<double, LinearTimeDependence>& dat)
{
	if (file.isOpen() == false)
		return;

	for (auto it = dat.begin(); it != dat.end(); ++it) {
		file.CreateCategory("linear");
		file.WriteTag("starttime", it->second.GetStartTime(), 13, true);
		file.WriteTag("endtime", it->second.GetEndTime(), 13, true);
		file.WriteTag("initial", it->second.GetInitVal(), 13, true);
		file.WriteTag("slope", it->second.GetSlope(), 13, true);
		file.WriteTag("sum", it->second.IsSum());
		file.WriteTag("flag", it->second.GetFlags());
		file.CloseLastCategory();
	}
}

void TransContact::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;
	if (ctcID < 0)
		return;	//don't save bad information
	file.CreateCategory("contact");
	file.WriteTag("id", ctcID);

	XMLCos(file, VoltageCos);
	XMLLin(file, VoltageLin);
	XMLCos(file, EFieldCos);
	XMLLin(file, EFieldLin);
	XMLCos(file, CurrentDensityCos);
	XMLLin(file, CurrentDensityLin);
	XMLCos(file, CurrentCos);
	XMLLin(file, CurrentLin);
	XMLTim(file, AdditionalFlags);
	

	file.CloseLastCategory();
}

void TransientSimData::SaveToXML(XMLFileOut& file) {
	if (file.isOpen() == false)
		return;

	if (simendtime <= 0.0)
		return;

	file.CreateCategory("transient");

	file.WriteTag("simendtime", simendtime, 12, true);
	file.WriteTag("timeignore", timeignore, 12, true);
	file.WriteTag("maxtstep", tStepControl.GetUserMaxStep(), 12, true);
	std::string stCond;
	if (StartingCondition == START_SS_TZERO)
		file.WriteTag("startcond", "sstimezero");
	else if (StartingCondition == START_THEQ)
		file.WriteTag("startcond", "theq");
	else if (StartingCondition == START_ABSZERO)
		file.WriteTag("abszero");
	else if (StartingCondition == START_SS)
		file.WriteTag("startcond", START_SS);
	else {	//a specific steady state that it knows...
		file.WriteTag("startcond", StartingCondition);	//this is the environment ID
	}

	if (SmartSaveState)
		file.WriteTag("statesave", "smart");

	if (SpecTime.size() > 0) {
		file.CreateCategory("spectime");
		for (unsigned int i = 0; i < SpecTime.size(); ++i) {
			file.WriteTag("time", SpecTime[i].time, 6, true);
			file.WriteTag("id", SpecTime[i].SpecID);
			if (SpecTime[i].enable)
				file.WriteTag("enable");
			else
				file.WriteTag("disable");
		}
		file.CloseLastCategory();
	}

	for (unsigned int i = 0; i < ContactData.size(); ++i)
		ContactData[i]->SaveToXML(file);
		
	file.CloseLastCategory();
}

void SSContactData::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;
	if (ctcID < 0)
		return;

	file.CreateCategory("contact");

	file.WriteTag("id", ctcID);
	if (TEST_BIT(currentActiveValue, CONTACT_VOLT_LINEAR))
		file.WriteTag("volt", voltage, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_EFIELD_LINEAR))
		file.WriteTag("efield", eField, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_CURRENT_LINEAR))
		file.WriteTag("current", current, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_CUR_DENSITY_LINEAR))
		file.WriteTag("densitycurrent", currentDensity, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_EFIELD_LINEAR_ED))
		file.WriteTag("edefield", eField_ED, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_CURRENT_LINEAR_ED))
		file.WriteTag("edcurrent", current_ED, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_CUR_DENSITY_LINEAR_ED))
		file.WriteTag("eddenscurrent", currentDensity_ED, 13, true);

	if (TEST_BIT(currentActiveValue, CONTACT_SINK))
		file.WriteTag("thermalbath");

	if (TEST_BIT(currentActiveValue, CONTACT_CONNECT_EXTERNAL))
		file.WriteTag("external");

	if (TEST_BIT(currentActiveValue, CONTACT_MINIMIZE_E_ENERGY))
		file.WriteTag("minimizefieldenergy");

	if (TEST_BIT(currentActiveValue, CONTACT_OPP_DFIELD))
		file.WriteTag("oppdfield");

	if (TEST_BIT(currentActiveValue, CONTACT_SINK))
		file.WriteTag("thermalbath");

	file.CloseLastCategory();
}

void Environment::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;
	if (id < 0)
		return;

	file.CreateCategory("environment");
	file.WriteTag("id", id);
	file.WriteTag("outname", FileOut);

	file.WriteTag("flags", flags);

	if (reusePrevContactEnvironment >= 0)
		file.WriteTag("reusecontact", reusePrevContactEnvironment);

	if (reusePrevLightEnvironment >= 0)
		file.WriteTag("reuselight", reusePrevLightEnvironment);

	for (unsigned int i = 0; i < ActiveLightEnvironment.size(); ++i)
		file.WriteTag("spectraid", ActiveLightEnvironment[i]);
	
	for (unsigned int i = 0; i < ContactEnvironment.size(); ++i) {
		ContactEnvironment[i]->SaveToXML(file);
	}

	file.CloseLastCategory();
}

void SS_SimData::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	if (ExternalStates.size() == 0)
		return;

	file.CreateCategory("steadystate");
	file.WriteTag("ssclamp", steadyStateClampReduction);
	file.WriteTag("mcpgood", PercentGood);
	file.WriteTag("mctrack", NumTrack);
	file.CreateCategory("calcorder");
	for (unsigned int i = 0; i < CalculationOrder.size(); ++i) {
		switch (CalculationOrder[i]) {
		case SS_NO_SRH:
			file.CreateCategory("zero_no_srh");
			break;
		case SS_MATCH_IMPS:
			file.CreateCategory("matchimps");
			break;
		case SS_ALL:
			file.CreateCategory("zero_all");
			break;
		case SS_SLOWPTS:
			file.CreateCategory("zero_slow");
			break;
		case SS_TRANSLIKE:
			file.CreateCategory("zero_trans");
			break;
		case SS_NEWTON_NOLIGHT:
			file.CreateCategory("newton_nl");
			break;
		case SS_NEWTON_ALL:
			file.CreateCategory("newton");
			break;
		case SS_NEWTON_NL_FULL:
			file.CreateCategory("newton_f_nl");
			break;
		case SS_NEWTON_L_FULL:
			file.CreateCategory("newton_f");
			break;
		case SS_TRANS_ACTUAL:
			file.CreateCategory("transient");
			break;
		case SS_SMOOTH_BAD:
			file.CreateCategory("smooth_bad");
			break;
		case SS_NEWTON_NUMERICAL:
			file.CreateCategory("newton_num");
			break;
		case SS_TRANS_CUTOFF:
			file.CreateCategory("trans_cutoff");
			break;
		default: continue;
			break;
		}

		file.WriteTag("init", InitAccuracy[i], 9);
		file.WriteTag("end", EndAccuracy[i], 9);

		file.CloseLastCategory();
	}
	file.CloseLastCategory();	//calcorder close

	for (unsigned int i = 0; i < ExternalStates.size(); ++i) {
		ExternalStates[i]->SaveToXML(file);
	}

	file.CloseLastCategory();
}

void Wavelength::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;
	if (lambda <= 0.0)
		return;

	file.CreateCategory("wavelength");
	file.WriteTag("lambda", lambda);
	file.WriteTag("intensity", intensity,6, true);
	file.CloseLastCategory();
}

void LightSource::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	file.CreateCategory("spectrum");
	file.WriteTag("name", name);
	file.WriteTag("id", ID);
	if (inputType == SPECTRUM_INTENSITY)
		file.WriteTag("intensity");
	else
		file.WriteTag("wavederiv");

	direction.SaveToXML(file, "direction");
	
	if (sourceShape)
		sourceShape->SaveToXML(file);

	for (std::map<double, bool>::iterator it = times.begin(); it != times.end(); ++it)
	{
		file.CreateCategory("timeactive");
		file.WriteTag("time", it->first);
		if (it->second)
			file.WriteTag("enable");
		else
			file.WriteTag("disable");
		file.CloseLastCategory();
	}

	for (std::map<double, Wavelength>::iterator it = light.begin(); it != light.end(); ++it)
		it->second.SaveToXML(file);

	
	


	file.CloseLastCategory();
}

void PtRange::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	file.CreateCategory("ptrange");
	startpos.SaveToXML(file, "startpos");
	stoppos.SaveToXML(file, "stoppos");
	file.CloseLastCategory();
	return;
}

void XSpace::SaveToXML(XMLFileOut& file, std::string topTag, int precision)
{
	if (file.isOpen() == false)
		return;

	file.CreateCategory(topTag);
	file.WriteTag("x", x, x != 0.0 ? precision : 1, (abs(x) < 1.0e-4 || abs(x) > 1e6) && x!=0.0);
	file.WriteTag("y", y, y != 0.0 ? precision : 1, (abs(y) < 1.0e-4 || abs(y) > 1e6) && y != 0.0);
	file.WriteTag("z", z, z != 0.0 ? precision : 1, (abs(z) < 1.0e-4 || abs(z) > 1e6) && z != 0.0);
	file.CloseLastCategory();
	return;
}

void Shape::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;
	if (numpoints <= 0)
		return;

	file.CreateCategory("shape");
	file.WriteTag("type", type);
	
	Vertex* vert = start;
	if (vert)
	{
		for (int i = 0; i < numpoints; ++i)
		{
			vert->pt.SaveToXML(file, "vertex", 8);
//stored in cm, 8 digits precision should be ~1 Angstrom if the vertices are actually 1cm in!

			if (vert->next)
				vert = vert->next;
		}
	}


	file.CloseLastCategory();
}

void Layer::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	file.CreateCategory("layer");
	file.WriteTag("id", id);
	file.WriteTag("name", name);
	if (contactid > 0)
		file.WriteTag("contactid", contactid);

	file.WriteTag("affectmesh", AffectMesh);

	if (exclusive)
		file.WriteTag("exclusive");
	else
		file.WriteTag("inclusive");

	if (out.OpticalPointEmissions)
		file.WriteTag("outoptical");

	if (priority)
		file.WriteTag("priority");

	if (AffectMesh)
	{
		file.WriteTag("edge", spacingboundary*1e7);
		file.WriteTag("center", spacingcenter*1e7);
	}

	file.WriteTag("sizescale", sizeScale);

	if (shape)
		shape->SaveToXML(file);

	for (unsigned int i = 0; i < matls.size(); ++i)
	{
		if (matls[i])
			matls[i]->SaveToXML(file, "ldescmat");
	}

	for (unsigned int i = 0; i < impurities.size(); ++i)
	{
		if (impurities[i])
			impurities[i]->SaveToXML(file, "ldescimp");
	}


	file.CloseLastCategory();
}

void Material::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	file.CreateCategory("material");
	file.WriteTag("name", name);
	file.WriteTag("id", id);
	file.WriteTag("describe", describe);
	
	file.CreateCategory("electric");
	file.WriteTag("eg", Eg);
	file.WriteTag("nv", Nv, 6, true);
	file.WriteTag("nc", Nc, 6, true);
	file.WriteTag("ni", ni, 6, true);

	
	file.WriteTag("mobility_n", MuN);
	file.WriteTag("mobility_p", MuP);
	file.WriteTag("efmec", EFMeCond);
	file.WriteTag("efmhc", EFMhCond);
	file.WriteTag("efmed", EFMeDens);
	file.WriteTag("efmhd", EFMhDens);
	file.WriteTag("dielectric", epsR);
	file.WriteTag("affinity", Phi);

	file.WriteTag("thermalgenrectau", tauThGenRec);
	if (direct)
		file.WriteTag("direct");
	else
		file.WriteTag("indirect");

	std::vector<std::string> types;
	types.push_back("Unknown");
	types.push_back("semiconductor");
	types.push_back("metal");
	types.push_back("dielectric");
	types.push_back("vacuum");
	file.WriteTag("type", type, types);

	if (urbachCB.energy > 0.0)
	{
		file.CreateCategory("urbachcb");
		file.WriteTag("energy", urbachCB.energy);
		file.WriteTag("sign", urbachCB.captureCB);
		file.WriteTag("sigp", urbachCB.captureVB);
		file.CloseLastCategory();
	}
	if (urbachVB.energy > 0.0)
	{
		file.CreateCategory("urbachvb");
		file.WriteTag("energy", urbachVB.energy);
		file.WriteTag("sign", urbachVB.captureCB);
		file.WriteTag("sigp", urbachVB.captureVB);
		file.CloseLastCategory();
	}

	if (CBCustom.size() > 0)
	{
		file.CreateCategory("doscb");
		for (std::map<double, CustomDensStates>::iterator it = CBCustom.begin(); it != CBCustom.end(); ++it)
		{
			file.CreateCategory("state");
			file.WriteTag("energy", it->first);
			file.WriteTag("density", it->second.density);
			file.WriteTag("efm", it->second.EFMCond);
			file.CloseLastCategory();
		}
		file.CloseLastCategory();
	}

	if (VBCustom.size() > 0)
	{
		file.CreateCategory("dosvb");
		for (std::map<double, CustomDensStates>::iterator it = VBCustom.begin(); it != VBCustom.end(); ++it)
		{
			file.CreateCategory("state");
			file.WriteTag("energy", it->first);
			file.WriteTag("density", it->second.density);
			file.WriteTag("efm", it->second.EFMCond);
			file.CloseLastCategory();
		}
		file.CloseLastCategory();
	}
	file.CloseLastCategory();	//close the electric tag

	if (defects.size() > 0)
	{
		file.CreateCategory("defect");
		for (unsigned int i = 0; i < defects.size(); ++i)
		{
			if (defects[i] == nullptr)
				continue;
			
			defects[i]->SaveToXML(file);
		}
		file.CloseLastCategory();
	}

	

	if (optics)
		optics->SaveToXML(file);

	file.CloseLastCategory();	//the ending material tag
	
	return;
}

void Impurity::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	if (ID < 0)
		return;

	if (isDefect())
	{
		if (reference == DONORLIKE)
			file.CreateCategory("donorlike");
		else
			file.CreateCategory("acceptorlike");
	}
	else
	{
		file.CreateCategory("impurity");
		if (reference == DONOR)
			file.WriteTag("donor");
		else
			file.WriteTag("acceptor");
	}


	file.WriteTag("id", ID);
	file.WriteTag("name", name);
	file.WriteTag("energy", energy);
	file.WriteTag("width", energyvar);
	file.WriteTag("density", density, 6, true);
	file.WriteTag("distribution", distribution);

	file.WriteTag("sign", sigman, 6, true);
	file.WriteTag("sigp", sigmap, 6, true);

	file.WriteTag("degeneracy", degeneracy);
	file.WriteTag("charge", charge);
	file.WriteTag("depth", depth);

	if (OpticalEfficiencyCB >= 0.0)
		file.WriteTag("cbopticefficiency", OpticalEfficiencyCB);
	if (OpticalEfficiencyVB >= 0.0)
		file.WriteTag("vbopticefficiency", OpticalEfficiencyVB);

	file.CloseLastCategory();
}

void Optical::SaveToXML(XMLFileOut& file)
{
	if (file.isOpen() == false)
		return;

	if (isWavelengthNeedSort())
		SortWavelengths();

	file.CreateCategory("optical");

	if (suppressOpticalOutputs)
		file.WriteTag("suppressoptout");

	if (disableOpticalEmissions)
		file.WriteTag("disableemissions");

	file.WriteTag("n", n);

	if (opticEfficiency >= 0.0)
		file.WriteTag("opticefficiency", opticEfficiency);

	if (useAB)
	{
		file.WriteTag("a", a);
		file.WriteTag("b", b);
	}
	else
	{
		
		for (std::map<float, NK>::iterator it = WavelengthData.begin(); it != WavelengthData.end(); ++it)
		{
			file.CreateCategory("wavelength");
			file.WriteTag("lambda", it->first);
			file.WriteTag("n", it->second.n);
			file.WriteTag("k", it->second.k);
			file.CloseLastCategory();
		}
	}
	
	file.CloseLastCategory();	//optical
}