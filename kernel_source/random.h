#ifndef MYRANDOM
#define MYRANDOM

#define LN2 0.69314718055994530941723212145818	// ln(2)
#define LN2I (-LN2)	// ln(0.5)
/*
This code is addopted from:
"A Search for Good Multiple Recursive Random Number Generators"
by Pierre L'Ecuyer from Universite de Montreal
and
Francois Blouin, Raymond Couture from Universite Laval

ACM Transactions on Modeling and Computer Simulation, Vol. 3, No. 2, April 1993, Pages 87-98.

Code for Random() and Uniform01() is literally copied from the paper (but constants placed in)
*/

class MyRand
{
	int x1;
	int x2;
	int x3;
	int x4;
	int x5;
public:
	MyRand()
	{
		x1=154876242;	//no seed specified, so just generate a semiarbitrarily chosen seed.
		x2=423311702;
		x3=691747158;
		x4=960182614;
		x5=1228618070;
	};
	~MyRand()
	{
		x1=0;
		x2=0;
		x3=0;
		x4=0;
		x5=0;
	};
	MyRand(unsigned int start, unsigned int power)
	{
		Seed(start,power);
	};
	void Seed(unsigned int start, unsigned int power)
	{
		unsigned int difference = 1;
		unsigned int powcount=0;
		power = power % 30;	//limit it to semi-realistic numbers
		while(powcount<power)	//calculate 2^power and put it in difference
		{
			difference=2*difference;
			powcount++;
			if(power/powcount >= 2)	//calculate quickly
			{
				difference=difference*difference;
				powcount = powcount * 2;
			}
		}
		x1 = start;
		x2 = x1 + difference;
		while(x2 > 2147483645)
			x2 -= 2147483645;
		x3 = x2 + difference;
		while(x3 > 2147483645)
			x3 -= 2147483645;
		x4 = x3 + difference;
		while(x4 > 2147483645)
			x4 -= 2147483645;
		x5 = x4 + difference;
		while(x5 > 2147483645)
			x5 -= 2147483645;

	};
	//based off of x_n = [a_1* x_(n-1) + a_5 * x_(n-5)] mod m
	int Random(void)
	{
		int h, p1, p5;
		h = x5 / 20554;
		p5 = 104480 * (x5 - h * 20554) - h * 1727;
	//a5 * x5 mod q5 - 
		x5 = x4;	//shift everything down
		x4 = x3;
		x3 = x2;
		x2 = x1;
		h = x1/20;
		p1 = 107374182 * (x1 - h * 20) - h * 7;
		if(p1 < 0)
			p1 += 2147483647;
		if(p5 > 0)
			p5 -= 2147483647;
		x1 = p1 + p5;
		if(x1 < 0)
			x1 += 2147483647;
		return(x1);
	};

	double Uniform01(void)
	{
		int Z = Random();	//returns [0,(2^31)-2]
		if(Z == 0)
			Z = 2147483647;
		return(Z * 4.656612873077393e-10);	//inverse of 2^31-2, so value from (0,1), with minimum of value shown
	};

	double UniformLnN01(unsigned long int N)	//natural log of N independent random variables multiplied together
	{
		double prob=1.0;
		if(N <= 20)	//do all random variables
		{
			for(unsigned int i=0; i<N; i++)
				prob = prob * Uniform01();
			prob = log(prob);
		}
		else if(N <= 40)
		{
			for(unsigned int i=0; i<10; i++)	//do 10 random variables and let the rest be statistical average
				prob = prob * Uniform01();

			prob = log(prob) + double(N-10) * LN2I;	//average is 0.5, so include N-10 0.5's.
		}
		else
		{
			for(unsigned int i=0; i<20; i++)
				prob = prob * Uniform01();
			prob = log(prob) + double(N-20) * LN2I;
		}

		return(prob);
	}

	double UniformMinMax(double min, double max)	//perhaps on larger time steps, the random umber generated is usually ~0.5.
	{
		if(min > max)	//swap them
		{
			double temp = max;
			max = min;
			min = temp;
		}
		if(min == max)
			return(min);	//this isn't random at all...

		double uni01 = Uniform01();
		double dif = max - min;
		uni01 = uni01 * dif;	//scale it from 0...max range.
		uni01 = min + uni01;	//now set the 0 at the appropriate number
		return(uni01);
	}

	double LNMagEquilibrium(double magnitude)
	{
		if(magnitude < 0.0)
			magnitude = -magnitude;

		if(magnitude <= 1.0)
			magnitude = 1.0;
		else
		{
			magnitude = 1.0 / magnitude;
			magnitude = 0.5 * magnitude * magnitude;	//make it (0.5 / magnitude^2)
		}
		magnitude = 1.0 - magnitude;	//min = 1.0 - 0.5/mag^2

		double prob = UniformMinMax(magnitude, 1.0);
		prob = log(prob);
		return(prob);
	}

	double Gaussian(double min=0.0, double range=1.0, int gaussianAcc=3)
	{
		double rnd = 0.0;
		if(gaussianAcc <= 0)
			gaussianAcc = 3;

		for(int i=0; i<gaussianAcc; ++i)
			rnd += Uniform01();

		//rnd now has a value from [0,gaussianAcc] - want [0,range]
		if(range <= 0.0)
			range = 1.0;
		double scale = range / double(gaussianAcc);
		rnd = rnd * scale;	//it is now in from [0,range];
		//now get it to [min,min+range];
		rnd += min;
		return(rnd);

	}
	double GaussianAvg(double avg=0.5, double halfwidth=-1.0, int gaussianAcc=3)
	{
		double rnd = 0.0;
		if(gaussianAcc <= 0)
			gaussianAcc = 3;

		for(int i=0; i<gaussianAcc; ++i)
			rnd += Uniform01();

		double min = (halfwidth <= 0.0) ? avg : avg - halfwidth;
		double range = (halfwidth <= 0.0) ? 2.0*avg: halfwidth*2.0;
		//rnd now has a value from [0,gaussianAcc] - want [0,range]
			double scale = range / double(gaussianAcc);
		rnd = rnd * scale;	//it is now in from [0,range];
		//now get it to [min,min+range];
		rnd += min;
		return(rnd);
	}
};	//declare a global random, and it should only be created once due to #ifndef and #endif

#endif