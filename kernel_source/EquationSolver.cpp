#include "EquationSolver.h"

#include <cmath>
#include <vector>
#include <iostream>
#include <list>
#include <ctime>

#include "helperfunctions.h"
#include "outputdebug.h"
#include "output.h"
#include "../gui_source/kernel.h"




/*
Convention:
Variable refers to the variable number in the matrix
pos refers to the index it can be found in
*/

/*
This code solves the system by making each equation solve for the number variable it is in the system.
(the first equation solves for the first variable, second equation solves for 2nd variable, etc.)
If the equation does not have the specified variable, it searches all unsolved equations for one with the
appropriate variable.

*/

int SystemEq::BackSubstitute(unsigned long int variable)
{
	//first, find the equations which have that variable.
	//then find the equation where it is a single variable. If none exist, exit.
	//otherwise, start canclling it out of all the other equations

	unsigned long int eqUse = 0;	//make it beyond the scope for sure! (-1)
	bool found = false;
//	vector<unsigned int> EqList;
//	System.EquationsWithVariable[variable].GenerateSortS(EqList);

	for(std::map<unsigned long int, unsigned long int>::reverse_iterator rIt = EquationsWithVariable[variable].rbegin(); rIt != EquationsWithVariable[variable].rend(); ++rIt)
	{
		if(Equation.count(rIt->first) > 0)	//prevent accidental creation of an equation - it should always be there, but just be safe
		{
			if(Equation[rIt->first].LHS.size() == 1)
			{
				eqUse = rIt->first;
				found = true;
				break;	//get outta here! Found the solution
			}
		}
	}
	if(found==false)
		return(NO);	//there wer eno solutions

	//now we need to take that variable and back substitute it's answer into all the others...
	//first, make sure it's normalized.
	if(Equation[eqUse].LHS.begin()->second != 0.0)	//almost guaranteed to be true... If it is zero, it should have not been found
	{
		double norm = 1.0 / Equation[eqUse].LHS.begin()->second;	//it can only get here if it found an equation with LHS size = 1, so begin() is guaranteed to exist
		Equation[eqUse].Normalize(norm);
	}
	//therefore, we can the RHS needs no modification beyond what's in the other equation

	std::map<unsigned long int, double>::iterator node;	//temporary access to variables
	double coeff = 0.0;

	//now do the back substitutions

	std::map<unsigned long int, unsigned long int>::iterator Itnext = EquationsWithVariable[variable].begin();
	if(Itnext != EquationsWithVariable[variable].end())
		++Itnext;
	for(std::map<unsigned long int, unsigned long int>::iterator It = EquationsWithVariable[variable].begin(); It != EquationsWithVariable[variable].end(); )	//no increment on purpose
	{
		//being that it solves things linearly through, it will end with an equation with one variable.
		unsigned long int modifyEq = It->first;	//the sort is the equation
		if(modifyEq == eqUse)
		{
			It = Itnext;
			if(Itnext != EquationsWithVariable[variable].end())
				++Itnext;
			continue;	//don't kill yourself!
		}
		node = Equation[modifyEq].LHS.find(variable);
		if(node != Equation[modifyEq].LHS.end())	//it exists
		{
			coeff = -node->second;	//the original data. It is having a normalized coefficient (1), so
			//it needs to multiply the original equation by the negative of the value and add that to it.
			//this will result in changing the RHS appropriately, and the LHS will only see the one coefficient removed
			AddLorRHS(Equation[eqUse].RHS, coeff, Equation[modifyEq].RHS);
			Equation[modifyEq].LHS.erase(variable);
		}

		EquationsWithVariable[variable].erase(modifyEq);	//this equation definitely no longer has that variable in it
		//all zeroes should have been removed from the equation during AddLorRHS. On the right side, no book-keeping.
		//previous statement does bookkeeping on the left side

		It = Itnext;
		if(Itnext != EquationsWithVariable[variable].end())
			++Itnext;
	}

	return(eqUse);
}

int SystemEq::AddLorRHS(std::map<unsigned long int, double>& sideEq, double multiplier, std::map<unsigned long int, double>& ModifyEq)
{
	int counter=0;	//how many elements are effectively removed when doing this
	std::map<unsigned long int, double>::iterator temp;
	std::vector<std::map<unsigned long int, double>::iterator> RemoveNodes;
	//loop through all the variables (one side of equation only) and add them to another equation (also one side only).

	for(std::map<unsigned long int, double>::iterator it = sideEq.begin(); it != sideEq.end(); ++it)
	{
		double value = it->second * multiplier;
		temp = MapAdd(ModifyEq, it->first, value);	//first is variable number, second is coefficient
		if(fabs(temp->second) < value * DUBZERO)
		{
			temp->second = 0.0;	//it was close enough to zero (finite precision) that we should just set it to zero
			counter++;
			RemoveNodes.push_back(temp);
		}
	}

	for (std::vector<std::map<unsigned long int, double>::iterator>::iterator vit = RemoveNodes.begin(); vit != RemoveNodes.end(); ++vit)
		ModifyEq.erase(*vit);	//just get rid of the values instead of searching through them all
	//can't do it in the previous for loop, because we are iterating through it. Deleting the node would invalidate the iterator, which
	//would then make the iterator invalid and unable to find the next one

	return(counter);
}

int SystemEq::AddEqWVar(std::map<unsigned long int, unsigned long int>& EqWVar, Eq& eq)
{	//eq is the equation that is constant.	This goes through all eqwvar.


	//this loop is unsafe because EquationsWithVariable[var] is being looped through and modified. In particular, the iterator points to an address that becomes invalid, and cannot increment properly.
	//The modifyEqWVar will be removed, but that should not have any affect on the next equation. So make sure you know what the next iterator will be first.
	std::map<unsigned long int, unsigned long int>::iterator nextmodifyEqWVar = EqWVar.begin();
	if(nextmodifyEqWVar != EqWVar.end())
		nextmodifyEqWVar++;
	for(std::map<unsigned long int, unsigned long int>::iterator modifyEqWVar = EqWVar.begin(); modifyEqWVar != EqWVar.end();)	//not increment on purpose!
	{

	//the above cycles through each equation needing to be added.
	//now actually do some schtuff

		unsigned long int addeq = modifyEqWVar->first;	//sort points to the equation needing addition. Data is the variable of interest.
		unsigned long int variable = modifyEqWVar->second;	//this should be constant in any given tree!
		std::map<unsigned long int, double>::iterator LHSVal;
	//note, the equation is already normalized, so the variable of interest has a coefficient of 1.0.

//	if(System.Equation[addeq].LHS.start != eq.LHS.start)
	
//	System.Equation[addeq].LHS.DisplayTree();
//	cout << "Before Equation: " << addeq << endl;
		if(Equation[addeq].Computed == false)	// wasif(added != Eq), but the current equation is set to computed first, so this is A-OK
		{	//don't subtract equation from itself or if its already had a variable solved for in this equation
			LHSVal = Equation[addeq].LHS.find(variable);
			if(LHSVal == Equation[addeq].LHS.end())
			{
				modifyEqWVar = nextmodifyEqWVar;
				if(nextmodifyEqWVar != EqWVar.end())
					nextmodifyEqWVar++;
				continue;	//This equation didn't have the variable for some odd reason. Don't add them together and move on to the next one
			}

			double multiplier = -LHSVal->second;	//what needs to be added to the equation
		
			//reset the equationswithvariable. Make sure the old eqwithvar also get removed
			UncatalogEquation(Equation[addeq].LHS, addeq);	//uncatalog it before the variables are modified

			//these do not relay on the equation being catloged or not.
			AddLorRHS(eq.LHS, multiplier, Equation[addeq].LHS);	//now add the lhs
			//to the equation addeq. elim is the number of coefficients set to 0.0.
			//and also modify the RHS in the same manner...
			AddLorRHS(eq.RHS, multiplier, Equation[addeq].RHS);
		
			
			//the LHS variable should always be eliminated. It was multiplied by a normalized negative of itself
			//it's probably aready gone, but just make sure
			Equation[addeq].LHS.erase(variable);
			
			CatalogEquation(Equation[addeq].LHS, addeq);	
			//now put the new variables back in.
			//at most, one item is removed from each of the eqwvar trees. everything beneath these
			//variables will have already been added.  Adding the equations will put the variable
			//on the same side, and may cause rotations up to the point it was removed.  It should
			//then become stable, and the rest of the structure should not be modified.
		}	//don't add if its already been computed
		modifyEqWVar = nextmodifyEqWVar;
		if(nextmodifyEqWVar != EqWVar.end())
			nextmodifyEqWVar++;
//	System.Equation[addeq].LHS.DisplayTree();
//	cout << "After Equation: " << addeq << endl;
	}
	return(0);
}

int SystemEq::SolveSystem(void)
{
//	ProgressCheck(14,false);
	unsigned long int variable;

	unsigned int eq;
	
	std::map<unsigned long int, double>* EqRoot;	//pointer to one side of equation
	std::map<unsigned long int, double>::iterator EqLHS;

	time_t starttime = time(NULL);
	time_t endtime;
	double timeelapse = 0.0;

	//first step, make sure all the equations are properly cataloged
	for (std::map<unsigned long int, std::map<unsigned long int, unsigned long int>>::iterator it = EquationsWithVariable.begin(); it != EquationsWithVariable.end(); ++it)
		it->second.clear();	//delete all the Equations with variable data

	EquationsWithVariable.clear();	//and clear out the vector
	for(std::map<unsigned long int, Eq>::iterator eqIt = Equation.begin(); eqIt != Equation.end(); ++eqIt)
		CatalogEquation(eqIt->second.LHS, eqIt->first);


//	TraceEq(System);	//write the equations to the trace file.

	
	for (std::map<unsigned long int, std::map<unsigned long int, unsigned long int>>::iterator itEQWV = EquationsWithVariable.begin(); itEQWV != EquationsWithVariable.end(); ++itEQWV)	//iterate through each equation
	{	//and cancel the variable from all others that haven't been added symbolically
		variable = itEQWV->first;

		time(&endtime);	//load the end time for dif
		timeelapse = difftime(endtime, starttime);	//this function doesn't seem to behave properly in XCode. Diff of 1359147586(end) and 1359147854(start) yields #.####e86 ish.
//		timeelapse = endtime - starttime;
		if(timeelapse > 10.0)	//if tasks are taking a LONG time, notify the user that it's still running every
		{	//10 seconds
			DisplayProgress(30, variable);
			double percent = (double)(variable) / (double)(EquationsWithVariable.size()) * 100.0;
			DisplayProgressDoub(1, percent);
			DisplayProgress(31, Equation.size());
			percent = (double)(Equation.size()) / (double)(EquationsWithVariable.size()) * 100.0;
			DisplayProgressDoub(1, percent);
			starttime = endtime;
		}

		//figure out which equation we want to use.
//		ProgressInt(10, eq);

		std::map<unsigned long int, Eq>::iterator choice = ChooseEquation(EquationsWithVariable[variable]);
		if(choice == Equation.end())	//then there were no equations with that variable
			continue;
		eq = choice->first;

		EqRoot = &(Equation[eq].LHS);
		EqLHS = EqRoot->find(variable);	//this MUST work, or it would not have gotten this far
/*
		if(EqLHS == EqRoot->end() && EqRoot->size() != 0)	//does equation[Eq] have position[Eq] in it
		{	//nope, so swap the equation with one that does
			if(FindSwapEquation(System, eq, variable) == NO)	//zero equations have this variable left. Singular matrix
			{	//continue solving as normal, but skip this equation.  Flag this system as bad.
				System.flag = BADSYSTEM;
//				TraceEq(System);
				continue;		//move onto the next for loop. Don't solve for this variable.
			}
			//need to update pos for the new equation. It does exist because swap returns other than NO
			EqRoot = &(System.Equation[eq].LHS);
			EqLHS = EqRoot->Find(variable);
//			TraceEq(System);
		}
*/
		Equation[eq].Computed = true;	//this MUST occur after the swap equation, or it will erroneously mark which equation was computed.

		//the equation does have the proper variable in it, whether original or via a swap
		//normalize the equation
		if(NormalizeEq(Equation[eq],variable) == NO)	//normalize equation [Eq] by variable solving for.
		{
			//failed to normalize the equation, which means it did not have the position cataloged correctly
			//or the coefficient was zero,as can be the case
//			TraceEq(System);
			ProgressInt(14, variable);	//debug output
		}
//		TraceEq(System);

		//now the fun part, add the equations to remove all parts of the variable except the one

		if(EquationsWithVariable[variable].size() > 0)
			AddEqWVar(EquationsWithVariable[variable], Equation[eq]);		
	}

//	TraceEq(System);
	unsigned long int numunsolved = CheckSystem();
	if(flag != SOLVED)
	{
		unsigned int presize;
		do
		{
			presize = numunsolved;	//run loop in reverse to hopefully only go through it once as the last equation should have
			for (std::map<unsigned long int, std::map<unsigned long int, unsigned long int>>::reverse_iterator it = EquationsWithVariable.rbegin(); it != EquationsWithVariable.rend(); ++it)	//1 var, and 2nd to last have 2 var, etc.
			{	//check each variable to see if it's been solved for.  If it has, then it will clear out all the other
				BackSubstitute(it->first);	//equations with that variable. At the very least, it gets the variable i to have coefficient one
			}
			numunsolved = CheckSystem();
			if(numunsolved==0)	//prevent doing the loop twice
				break;
		}while(numunsolved != presize);	//go until it finds no additional solutions		
	}

	
	
//	ProgressCheck(15, false);
//	TraceEq(System);

	return(0);
}

std::map<unsigned long int, Eq>::iterator SystemEq::ChooseEquation(std::map<unsigned long int, unsigned long int>& EQwVar)
{
	if(EQwVar.size() == 0)
		return(Equation.end());

	std::map<unsigned long int, unsigned long int> Possibilities;
	//this loads all the equations into possibilities. second is the equation, first is how many items are in the LHS
	SelectGoodEq(EQwVar, Possibilities);
	if(Possibilities.size() > 0)	//begin is lowest number of LHS variables that is >0, and second pulls out the equation (which was the first/key of EqwVar (equation number)
		return(Equation.find(Possibilities.begin()->second));

	return(Equation.end());	//outside the scope of the equation
}

int SystemEq::SelectGoodEq(std::map<unsigned long int, unsigned long int>& EqwVar, std::map<unsigned long int, unsigned long int>& Choices)
{
	
	//loop through all the equations with a specified variable
	unsigned int sz;
	for(std::map<unsigned long int, unsigned long int>::iterator it = EqwVar.begin(); it != EqwVar.end(); ++it)
	{
		sz = Equation[it->first].LHS.size();
		if(sz == 1)
		{
			Choices[sz] = it->first;
			break;	//not going to find a better equation to work with!
		}
		else if(sz > 0)
			Choices[sz] = it->first;	//no need for lots of enties, just create a new or reassign as needed
		//this will create an index if it does not exist
		
	}
	return(0);
}
unsigned long int SystemEq::CheckSystem(void)
{
	std::map<unsigned long int, Eq>::iterator itEq;
	unsigned long int numunsolved = 0;

	flag = UNDEF; //0 - reset it
	for(itEq = Equation.begin(); itEq != Equation.end(); ++itEq)
	{
		if(itEq->second.LHS.size() > 1)
		{
			numunsolved++;
			flag = flag | UNSOLVED;
		}
		else if(itEq->second.LHS.size() == 0)	// 0 =
		{
		//	NetEQ++;
			if(itEq->second.RHS.size() != 0.0)	//not 0
			{
				flag = flag | CONFLICT;
				flag = flag | BADSYSTEM;
				numunsolved++;
			}
			//if 0==0, don't worry about it. A variable has no effect on the system. 
		}	//or was not actually a part of the system. variables are eliminated from their own equation to 0=0.
	}
	if(flag == UNDEF)
		flag = SOLVED;

	if(numunsolved > 0)
		flag = UNSOLVED;

	return(numunsolved);
}
/*
Find an equation containing the proper variable to swap with. Choose the best one.
*/

/*

This shouldn't actually be required with the new implementation with maps and the function ChooseEquation.

int FindSwapEquation(SystemEq& System, unsigned int Eq, unsigned int variable)
{
	vector<unsigned int> Possibilities;	//possible equations it could swap with
	vector<unsigned int>::iterator itPos;	//used to remove equations from the possibilities that already had their variables solved for
	vector<unsigned int> GoodSpots;		//Places the present equation would be happy to be at
	if(!Possibilities.empty())
		Possibilities.clear();
	if(variable < System.EquationsWithVariable.size())
		System.EquationsWithVariable[variable].GenerateSortS(Possibilities);

	//possiblities now holds locations to all equations containing the desired variable to be eliminated
	
	if(Possibilities.size() == 0)
		return(NO);		//singular!

	int counter = 0;
	for(itPos = Possibilities.begin(); itPos < Possibilities.end(); )
	{
		if(System.Equation[*itPos].Computed == true)
		{	//remove this position
			Possibilities.erase(itPos);
			itPos = Possibilities.begin()+counter;
		}
		else
		{
			counter++;
			itPos++;
		}
	}		

	if(!GoodSpots.empty())
		GoodSpots.clear();

	
	System.Equation[Eq].LHS.GenerateSortS(GoodSpots);	//good spots now has all the variables
	//that correlate to a good equation to switch with

	int spot = NO;
	bool found = false;

	for(unsigned int i=0; i<Possibilities.size(); i++)	//cycle through each possibile equation to switch with
	{
		if(System.Equation[Possibilities[i]].LHS.size == 1)
		{	//freebie. Equation is var = number.  Doesn't get much better than that...
			spot = Possibilities[i];
			break;	//stop looking. Exit for loop
		}

		if(!found)	//a variable was skipped, and consequently, and equation was skipped
		{			//once a solution is found, variables are then eliminated.
			spot = Possibilities[i];	//keep accepting legit trade spots until a best case scenario is found (1 of 3 scenarios)
			unsigned int size=GoodSpots.size();
			for(unsigned int j=0; j<size; j++)	//cycle through all the places the present equation would like to be
			{	//check if the possible equation to swap with needs a variable in the present equation
				//note: size = Goodspots.size()
				if(GoodSpots[j] == Possibilities[i])
				{//	Does the target's equations variables (goodspots) match with an equation position
					found = true;	//found 2nd best option. Ignore first options
					spot = Possibilities[i];
					break;
				}
			}	//end for - equations it want to switch with
		}	//end if not found
		
	}	//end for - equations it can switch with

	if(spot == NO)
		return(NO);	//could not find an acceptable equation to swap with

	//uncatalog the two equations - just wipe clean
	UncatalogEquation(System.Equation[Eq].LHS.start, Eq, System.EquationsWithVariable);
	//remove all references to target in EquationsWithVariable
	UncatalogEquation(System.Equation[spot].LHS.start, spot, System.EquationsWithVariable);
	//remove all references to swapped EQ in EquationsWithVariable

	//Now do the equation swap
	Tree<double, unsigned int>* tempLHS = System.Equation[Eq].LHS.start;
	System.Equation[Eq].LHS.start = System.Equation[spot].LHS.start;
	System.Equation[spot].LHS.start = tempLHS;	//just change the pointers to swap all LHS

	bool tempC = System.Equation[Eq].Computed;
	System.Equation[Eq].Computed = System.Equation[spot].Computed;
	System.Equation[spot].Computed = tempC;
	
	Tree<double, unsigned int>* tempRHS = System.Equation[Eq].RHS.start;
	System.Equation[Eq].RHS.start = System.Equation[spot].RHS.start;
	System.Equation[spot].RHS.start = tempRHS;

	unsigned int tempsize = System.Equation[Eq].LHS.size;
	System.Equation[Eq].LHS.size = System.Equation[spot].LHS.size;
	System.Equation[spot].LHS.size = tempsize;

	tempsize = System.Equation[Eq].RHS.size;
	System.Equation[Eq].RHS.size = System.Equation[spot].RHS.size;
	System.Equation[spot].RHS.size = tempsize;

	System.Equation[Eq].LHS.UpdateRoot();
	System.Equation[spot].LHS.UpdateRoot();

	System.Equation[Eq].RHS.UpdateRoot();
	System.Equation[spot].RHS.UpdateRoot();


	//recatlog the equations
	CatalogEquation(System.Equation[Eq].LHS.start, Eq, System.EquationsWithVariable);
		//add references to target in EquationsWithVariable
	CatalogEquation(System.Equation[spot].LHS.start, spot, System.EquationsWithVariable);
	//add references to swapped EQ in EquationsWithVariable
	

	return(spot);
}

*/

int SystemEq::UncatalogEquation(std::map<unsigned long int, double>& lhs, unsigned int eq)
{
	for(std::map<unsigned long int, double>::iterator lhsIT = lhs.begin(); lhsIT != lhs.end(); ++lhsIT)
		EquationsWithVariable[lhsIT->first].erase(eq);	//remove the equation from the list. Don't balance the tree

	return(1);
}

//add the equations to the vector list
int SystemEq::CatalogEquation(std::map<unsigned long int, double>& lhs, unsigned long int eq)
{
	
	for(std::map<unsigned long int, double>::iterator lhsIT = lhs.begin(); lhsIT != lhs.end(); ++lhsIT)
	{
		EquationsWithVariable[lhsIT->first].insert(std::pair<unsigned long int, unsigned long int>(eq,lhsIT->first));
		//if it already exists, it will just replace it... with presumably the same value.
		//The key (first) is the equation, and the dats is the variable again for easy access
		
		
		//EqWVar data = the variable - store the variable so it's held internally in addition to the vector list
		//EqWVar sort = the equation - what we're looking for.
	}
	return(1);
}

//add the equations to the vector list
//force uncatalog an equation associated with a variable
int SystemEq::NormalizeEq(Eq& eq, unsigned long int var)
{
	std::map<unsigned long int, double>::iterator node = eq.LHS.find(var);

	if(node==eq.LHS.end())	//make sure the variable is present in the equation
	{
		ProgressInt(11, var);
		return(NO);
	}

	double coeff = node->second;

	if(coeff == 0.0)
		return(NO);
	else if(coeff == 1.0)
		return(0);	//no need to normalize it. Already done.
	
	coeff = 1.0/coeff;	//want to normalize everything...
	eq.Normalize(coeff);	//normalize RHS

	return(0);
}


Eq::Eq(void)	//constructor
{
	clear();
}
Eq::~Eq(void)
{
	clear();
}

int Eq::Normalize(double norm)
{
	for(std::map<unsigned long int, double>::iterator it = LHS.begin(); it != LHS.end(); ++it)
		it->second = norm * it->second;

	for(std::map<unsigned long int, double>::iterator it = RHS.begin(); it != RHS.end(); ++it)
		it->second = norm * it->second;

	return(0);
}

void Eq::clear(void)
{
	LHS.clear();
	RHS.clear();
	Computed = false;
	return;
}

EqVar::EqVar(void)
{
	var = 0;
	eq = 0;
}

SystemEq::SystemEq(void)
{
	flag = UNSOLVED;
	//all the other equations are cleared by their constructors.
}

int SystemEq::AddTermEquation(unsigned long int eq, unsigned long int term, double value, bool side)
{
	std::map<unsigned long int, Eq>::iterator it = Equation.find(eq);
	std::map<unsigned long int, double>::iterator var;
	if(it == Equation.end())
	{ //this equation does not exist yet
		CreateClearEquation(eq);
		it = Equation.find(eq);	//this is guaranteed to work now
	}
	if(side == EQ_SOLVER_LHS)
	{
		var = Equation[eq].LHS.find(term);
		if(var != Equation[eq].LHS.end())
			var->second += value;	//add it to the term
		else
			Equation[eq].LHS[term] = value;	//this will create the term
	}
	else //side is EQ_SOLVER_RHS
	{
		var = Equation[eq].RHS.find(term);
		if(var != Equation[eq].RHS.end())
			var->second += value;	//add it to the term
		else
			Equation[eq].RHS[term] = value;	//this will create the term
	}
	flag = UNSOLVED;
	return(0);
}

int SystemEq::CreateClearEquation(unsigned long int eq)
{
	Eq tmpEquation;
	tmpEquation.clear();
	std::map<unsigned long int, Eq>::iterator it = Equation.find(eq);
	if(it != Equation.end())
		it->second.clear();	//it already exists. Clear it out, and it's there and ready to go
	else
		Equation.insert(std::pair<unsigned long int, Eq>(eq, tmpEquation));
	
	flag = UNSOLVED;
	return(0);
}

int SystemEq::RemoveEquation(unsigned long int eq)
{
	Equation.erase(eq);	//if it doesn't exist, it will do nothing
	flag = UNSOLVED;
	return(0);
}

int SystemEq::RemoveTermEquation(unsigned long int eq, unsigned long int term, bool side)
{
	std::map<unsigned long int, Eq>::iterator it = Equation.find(eq);
	if(it != Equation.end())
	{
		if(side == EQ_SOLVER_LHS)
			it->second.LHS.erase(term);
		else
			it->second.RHS.erase(term);
	}
	return(0);
}

int SystemEq::TraceEq(void)
{
	std::fstream trace;
	trace.open("trace.txt", std::fstream::out | std::fstream::app);	//open file for adding to it
	if(trace.is_open() == false)
	{
		std::cout << "Failed to open trace.txt" << std::endl;
		return(0);

	}
	
	trace << "Coefficient[variable]" << std::endl;
	for(std::map<unsigned long int, Eq>::iterator it = Equation.begin(); it != Equation.end(); ++it)
		it->second.DisplayEquation(trace);


	trace.close();
	return(0);
}

int Eq::DisplayEquation(std::fstream& file)
{
	if(file.is_open() == false)
	{
		std::cout << "Did not pass in an open file" << std::endl;
		return(0);
	}
	for(std::map<unsigned long int, double>::iterator lhsIt = LHS.begin(); lhsIt != LHS.end(); ++lhsIt)
	{
		if(lhsIt != LHS.begin())
			file << " + ";
		file << lhsIt->second << "[" << lhsIt->first << "]";
		
	}

	file << " = ";

	for(std::map<unsigned long int, double>::iterator rhsIt = RHS.begin(); rhsIt != RHS.end(); ++rhsIt)
	{
		if(rhsIt != RHS.begin())	//not the first one, add on the necessary plus sign
			file << " + ";
		file << rhsIt->second << "[" << rhsIt->first << "]";
	}
	file << std::endl;	//end line for equation

	return(0);
}

int SystemEq::clear(void)
{
	for(std::map<unsigned long int, Eq>::iterator eq = Equation.begin(); eq != Equation.end(); ++eq)
	{
		eq->second.LHS.clear();
		eq->second.RHS.clear();
	}
	Equation.clear();

	for(std::map<unsigned long int, std::map<unsigned long int, unsigned long int>>::iterator it = EquationsWithVariable.begin(); it != EquationsWithVariable.end(); ++it)
		it->second.clear();	//clear out the map in this map...
	EquationsWithVariable.clear();	//and clear out the vecotr
	flag = UNDEF;

	return(0);
}

SystemEq::~SystemEq()
{
	clear();
}

//copies the solutions over into a single data structure that is supplied
int SystemEq::GetReturns(std::map<unsigned long int, std::vector<SystemEqReturns>>& ret)
{
	if(flag == UNDEF)
		SolveSystem();
	if(flag != SOLVED)
		return(-1);	//failed to put in data

	//if flag is solved, then all the LHS have only one side, and if they have none, then there is nothing on the RHS.
	//additionally, because it backsubstitutes
	SystemEqReturns tmp;

	for (std::map<unsigned long int, Eq>::iterator eqIt = Equation.begin(); eqIt != Equation.end(); ++eqIt)
	{
		if(eqIt->second.LHS.size() == 1)	//make sure it's a valid equation
		{
			if(eqIt->second.LHS.begin()->second != 0.0)	//and the LHS is non-zero
			{
				if(eqIt->second.LHS.begin()->second != 1.0)	//if it's not one, normalize it
				{
					double norm = 1.0/eqIt->second.LHS.begin()->second;
					eqIt->second.Normalize(norm);
				}
				for (std::map<unsigned long int, double>::iterator rhs = eqIt->second.RHS.begin(); rhs != eqIt->second.RHS.end(); ++rhs)
				{
					tmp.data = rhs->second;
					tmp.which = rhs->first;
					if(tmp.data != 0.0)	//just in case there is an erroneous 0.0 that hasn't been cleared out
						ret[eqIt->second.LHS.begin()->first].push_back(tmp);
				}
			}
			
		}
	}

	return(0);
}

//////////////////////////////////////////////////////////////////////

//solver #2 - just a FAST numeric solver for DENSE matrices. No maps, just lots of contiguous memory. matrix[sz*sz], rhs[sz], then answer is stored answer in rhs[]. matrix[] data is destroyed
bool SolveDenseOneTime(kernelThread* kThread, double matrix[], double rhs[], long int sz)
{
	long int curEq = -1;
	unsigned long int matSz = sz*sz;
	ProcessedEquations EqData(sz);	//initialize the Equation control data
//	bool goodSolution = true;
	time_t start = time(NULL);
	time_t end;
	double telapse;
	float prevPos = 0;
	float curPos = 0;
	float fullway = (float(sz-1)*(float(sz-1)+1.0)*(float(sz-1)+2.0))/3.0;	//normally divide by 2, but there are two triangles
	long int halfway =  fullway;
	
	kThread->shareData.KernelshortProgBarRange = 1000;
	kThread->shareData.KernelshortProgBarPosition = 0;

	unsigned long long int numNoChange=0;
	unsigned long long int numCalc=0;
	double testNoChange;	//also a temp value

	for (long int curEq = 0; curEq < sz; ++curEq)
	{

		//update user that the code is still running
		end = time(NULL);
		telapse = difftime(end, start);
		if(kThread->shareData.GUIabortSimulation || kThread->shareData.GUIstopSimulation)
			return(false);

		if(telapse > 2.0)
		{
//			DisplayProgress(61,varSolve);
			
			
			curPos = (float(sz-1-curEq)*(float(sz-1-curEq)+1.0)*(float(sz-1-curEq)+2.0))/3.0;
			curPos = fullway - curPos;
			float moved = curPos - prevPos;
			prevPos = curPos;
			kThread->shareData.KernelshortProgBarPosition = int(curPos / fullway*1000.0);//curPos;
			kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED, UPDATE_SUBWINDOW, SIMHASNOTCRASHED);
			
			if(moved>0.0)
			{
				kThread->shareData.KernelTimeLeft = double(fullway-curPos)/double(moved) * telapse;
				start = end;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
			}
		}
		//end update user that code is still running


/*
Slightly major assumption. the variable lines up with the equation. If the rate for an energy state
does not depend on that states concentration, either this is a really awfully weird system, or other
derivatives have previously cancelled it out.  In order for the other derivatives to cancel it out,
the system has to have one particular rate dominate, meaning the derivatives for some states must
be large, and for others it must be small - so small that it is beyond the numerical accuracy.  This
means that if any change must occur in other rates, it likely must stem from the other energy states.
If it were to change in the main state that others have huge derivatives from, all those states
would be completely blown out of whack.

Two possible solutions:
Simple way: say this state will not have any effect on the outcome and cancel out all it's derivatives
- or simply say that the occupancy change is zero, and zero x something is zero, so simplify the problem.

Hard way: Increase the numerical accuracy.  Pretty much every state will only be calculated
to adjust the rate in one and only one way - though some will have two when drift diffusion
comes in and they are in the same point due to band bending and the original rate.  The derivatives
occurring from the actual rate will be ALL over the place. Potentially use the PosNegSum class,
which will store the number using 24 doubles (27 + 3 bools including overhead).

Have an array that defines the diagonal of the matrix, and have those be the posnegsums. Would
require additional support code in PosNegSum and additional logic.

If there is additional accuracy on the diagonal, additional accuracy will be needed for the RHS
values as well.  This will turn into a LARGE memory hog for decently sized systems.
Say 150 points, 30 states/point = 4500 states.
Matrix is 4500*4500 = 20,250,000 numbers * 8bytes = 162,000,000 bytes for the deriv matrix.
Suubtract 36,000 bytes and add 972,000 bytes for the diagonal, and 972,000 for the occupancy.
Total = 163,944,000 bytes

Get more accuracy in defects, so now say 60 states/point, so 9,000 states, and now looking
at 0.5GB just for this one bit.  This might fail to allocate the memory!

So we'll try the simple solution first.
*/

		if(matrix[curEq*sz+curEq]==0.0)
		{
#ifndef NDEBUG
			DebugSSZeroEffect(false, false, curEq);
#endif
			EqData.AddZeroEffect(curEq);
			continue;
		}
		/*
		while (matrix[curEq*sz + varSolve] == 0.0)
		{
			curEq = EqData.GetNextEq();
			if (curEq == -1)	//signal for no equation to look for when solving this variable
			{
//				goodSolution = false;
//				process = false;
//				break;
				//if an entire column is zero, that means no matter what the change
				//in carrier concentration is, it will have zero effect on all the rates.
				//therefore just assume that this particular carrier concentration change is going to be zero.
/ *				for(long int i=0; i<sz; ++i)
				{
					if(matrix[i*sz+varSolve] != 0.0)	//this system just can't be solved
						return(false);
				}
* /
				//if they were all zero, the variable can be anything and it will have no effect whatsoever
				EqData.AddZeroEffect(varSolve);
				process = false;
				break;
			}
		}
		if (!process)
			continue;	//maybe by a stroke of luck, it cancelled out because one of the equations read [0 0 0 0 0 ] = [0], so it's not necessarily bad
		//but I suppose if that's the case, the last equation is going to have multiple variables, and that is no good!
		*/
		//if it got this far, it's good and nonzero
		//all the variables before this should have been cancelled out
		//normalize this equation

		
		//		EqData.AddEqProcess(curEq);	//now it will no longer look at this equation when processing

		long int curEqMatIndex = curEq*sz;

		double norm = 1.0 / matrix[curEqMatIndex + curEq];
		matrix[curEqMatIndex + curEq] = 1.0;
		long int lastNonZero = curEq;

		for (long int normalize = curEq + 1; normalize < sz; ++normalize)
		{
			testNoChange = matrix[curEqMatIndex + normalize] * norm;
			matrix[curEqMatIndex + normalize] = testNoChange;
			if(testNoChange != 0.0)
			{
				lastNonZero = normalize;
			}
		}
		rhs[curEq] = rhs[curEq] * norm;

		//now we need to remove the terms from all remaining equations


		//this goes through each equation and adds terms, ignoring any equation that doesn't have much of an effect
		
		for(long int fixEq = curEq+1; fixEq < sz; fixEq++)
		{
			long int fixEqMatIndex=fixEq * sz;
			norm = -matrix[fixEqMatIndex + curEq];	//multiply this by the curEq and add it to the fix equation
			if (norm != 0.0)	//this equation does need to be modified to clear out the variable
			{
//				matrix[fixEqMatIndex + curEq] = 0.0;	//this is set to zero by default
// the previous line is true to keep the matrix correct, but the solver assumes it is zero and already skips it
				rhs[fixEq] += norm*rhs[curEq];

				for (long int cancelTerms = curEq+1; cancelTerms <= lastNonZero; ++cancelTerms)	//update the rest of the equation
				{
					matrix[fixEqMatIndex + cancelTerms] += norm*matrix[curEqMatIndex + cancelTerms];
#ifndef NDEBUG
					if(MY_IS_FINITE(matrix[fixEqMatIndex+cancelTerms])==false)
					{
						ProgressInt(59, curEq);
						ProgressInt(60, fixEq);
						ProgressInt(61,cancelTerms);
					}
#endif
				}

			}
		}

	}
	
	//now the back substitutions need to be done! If it has gotten this far, the matrix has been valid because it was able to do stuff for every variable
	long int ctr=-1;
	for (long int varBackSub = sz - 1; varBackSub >= 0; --varBackSub)
	{
		ctr++;
		if(EqData.hasZeroEffect(varBackSub))	//is this variable zeroed out
			continue;


		//update message to user that it's running code
		end = time(NULL);
		telapse = difftime(end, start);
		if(telapse > 2.0)
		{
			prevPos = curPos;
			curPos = halfway + sz - varBackSub;
			kThread->shareData.KernelshortProgBarPosition = curPos;
			
			long int moved = curPos - prevPos;
			if(moved>0)
			{
				kThread->shareData.KernelTimeLeft = double(fullway-curPos)/double(moved) * telapse;
				start = end;
				kThread->CreateParentEvent(wxEVT_COMMAND_TEXT_UPDATED,KERNEL_UPDATE,0);
			}
		}
		//end update message to user that it's running code

		double rhstmp = rhs[varBackSub];	//this will be the last equation processed,and more importantly, it's value in the matrix MUST be 1.
		for (long int modifyEq = 0; modifyEq <varBackSub; modifyEq++)
			rhs[modifyEq] -= rhstmp * matrix[modifyEq*sz + varBackSub];

	}

	EqData.ZeroOut(rhs);	//cancel out any changes in concentration that had no effect on all the rates
	//and I guess it's done...
	//note, this should basically run straight 1...N and then N...1 on backsubstitue, but 1...N for the RHS loop
#ifndef NDEBUG
	DebugSSZeroEffect(false, true, -1);
#endif
	return(true);
}

bool ProcessedEquations::hasZeroEffect(long int var)
{
	for(unsigned int i=0; i<ZeroEffect.size(); ++i)
	{
		if(ZeroEffect[i] == var)
			return(true);
	}
	return(false);
}

long int ProcessedEquations::StartLoopEqPOrder()
{
	if (EqProcessOrder.empty())
		return(-1);
	EqLoop = 0;
	return(EqProcessOrder[0]);
}
long int ProcessedEquations::LoopEqPOrder()
{
	EqLoop++;
	if (EqLoop >= EqProcessOrder.size())
		return(-1);
	return(EqProcessOrder[EqLoop]);
}

ProcessedEquations::ProcessedEquations()
{
	Available.clear();
	EqProcessOrder.clear();
}

ProcessedEquations::~ProcessedEquations()
{
	Available.clear();
	EqProcessOrder.clear();
}

ProcessedEquations::ProcessedEquations(long int startEq, long int stopEq)
{
	Available.clear();
	EqProcessOrder.clear();
	EqProcessOrder.reserve(stopEq - startEq + 1);
	Available.push_back(IntPair(startEq, stopEq));
}

void ProcessedEquations::Initialze(long int startEq, long int stopEq)
{
	Available.clear();
	EqProcessOrder.clear();
	EqProcessOrder.reserve(stopEq - startEq + 1);
	Available.push_back(IntPair(startEq, stopEq));
	whichAvail = 0;
	return;
}

ProcessedEquations::ProcessedEquations(long int numEq)
{
	Available.clear();
	EqProcessOrder.clear();
	EqProcessOrder.reserve(numEq);
	Available.push_back(IntPair(0, numEq-1));
}

void ProcessedEquations::Initialze(long int numEq)
{
	Available.clear();
	EqProcessOrder.clear();
	EqProcessOrder.reserve(numEq);
	Available.push_back(IntPair(0, numEq-1));
	whichAvail = 0;
	return;
}

int ProcessedEquations::AddEqProcess(long int eq)
{
	long int sz = Available.size();
	for (long int i = 0; i < sz; ++i)
	{
		if (eq == Available[i].start)
		{
			if (Available[i].start == Available[i].stop)
				Available.erase(Available.begin() + i);	//erase the node, because now nothing was left available in here
			else
				Available[i].start++;	//usual path!

			EqProcessOrder.push_back(eq);
			return(0);
		}
		else if (eq == Available[i].stop)
		{
			//now let's do special cases where no additional data must be created
			//no need to check if start == stop, because if it did, it would have been caught in the first loop
			Available[i].stop--;
			EqProcessOrder.push_back(eq);
			return(0);
		}
		else if (eq > Available[i].start && eq < Available[i].stop)
		{
			//need to modify one of them and create a new one
			long int oldStop = Available[i].stop;
			Available[i].stop = eq - 1;	//stop the one short
			Available.push_back(IntPair(eq + 1, oldStop));	//and create a new one to go beyond the path
			EqProcessOrder.push_back(eq);
			return(0);
		}
	}
	return(-1);	//equation was already invalid
}

bool ProcessedEquations::isAvail(long int eq)
{
	long int sz = Available.size();
	for (long int i = 0; i < sz; ++i)
	{
		if (eq >= Available[i].start && eq <= Available[i].stop)
			return(true);
	}
	return(false);
}

long int ProcessedEquations::GetStartEq()
{
	if (Available.size() > 0)
	{
		whichAvail = 0;
		curAvail = Available[0].start;
		return(curAvail);
	}
	whichAvail = -1;	//this is the LARGEST int size, which will flag as problem
	curAvail = -1;
	return(-1);
}

long int ProcessedEquations::GetNextEq()
{
	unsigned int sz = Available.size();
	if (whichAvail  < sz)	//then this is going to be a valid index
	{
		curAvail++;
		if (curAvail > Available[whichAvail].stop)
		{
			whichAvail++;
			if (whichAvail >= sz)
			{
				whichAvail = -1;
				curAvail = -1;
				return(-1);
			}
			curAvail = Available[whichAvail].start;
		}
		return(curAvail);
	}
	curAvail = -1;
	return(-1);
}

long int ProcessedEquations::GetLastProccessedEq()
{
	if (EqProcessOrder.empty())
		return(-1);
	long int ret = EqProcessOrder.back();	//get the last element value
	EqProcessOrder.pop_back();	//remove the last element
	return(ret);	//and return it
}

int ProcessedEquations::AddZeroEffect(long int var)
{
	unsigned int sz = ZeroEffect.size();
	for(unsigned int i=0; i<sz; i++)
	{
		if(ZeroEffect[i] == var)
			return(-1);
	}
	ZeroEffect.push_back(var);
	return(0);
}

int ProcessedEquations::ZeroOut(double rhs[])
{
	for(unsigned int i=0; i<ZeroEffect.size(); ++i)
		rhs[ZeroEffect[i]] = 0.0;
	return(0);
}

IntPair::IntPair()
{
	start = 0;
	stop = 0;
}
IntPair::IntPair(long int beg, long int end)
{
	start = beg;
	stop = end;
}